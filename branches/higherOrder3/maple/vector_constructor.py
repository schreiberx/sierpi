#/usr/bin/python

f=open("output/vectors.h","w")

import glob, os
import re

for vec in glob.glob("./mapledata/*.vector"):
    print ("Processing " +  vec + " ...")
    # collect stuff from file
    contents = []
    replacements = []
    for line in open(vec):
        if line.startswith("Warning: the following variable name replacements were made:"):
            matches = re.match(r'Warning.*?\[(.*?)\]\s=\s\[(.*?)\]', line)
            matches.group(1)
            s = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(1)).split(',')
            r = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(2)).split(',')
            for i in range(len(s)):
                replacements.append((s[i],r[i]))
            continue
        if line.startswith("Warning") or line.startswith(">"):
            continue
        parts = re.match(".*?(\[.*\]).*=(.*);",line).groups()
        index = [int(x) for x in re.search("\[(.*?)\]",parts[0]).groups()]
        value = parts[1]
        contents.append(index+[value,])
    # sorting
    maxv = max(contents, key=lambda x:x[0])[0]
    print maxv
    realcontents = [0 for x in xrange(maxv+1)]
    for [x,c] in contents:
        realcontents[x] = c
    # write to file
    replacements.extend([("vx", "qx"), ("vy","qy"), ("g", "gravity"), ("double", "T")])
    for i in range(len(realcontents)):
        linetowrite = "result[%d][_%s] = %s;\n"%(i,os.path.basename(vec).split(".")[0],realcontents[i])
        for (src,dest) in replacements:
            linetowrite = linetowrite.replace(src,dest)
        linetowrite = re.sub(r'0\.(\d+)e(\d+)', lambda match: match.group(1) if(len(match.group(1)) == int(match.group(2))) else float(match.group(0)), linetowrite)
        linetowrite = re.sub(r'pow\(\s*?(.*?),\s*?(\-\d+?)\s*?\)', lambda match: '(1./('+'*'.join([match.group(1)] * abs(int(match.group(2))))+'))', linetowrite)
        linetowrite = re.sub(r'pow\(\s*?(.*?),\s*?(\d+?)\s*?\)', lambda match: '('+'*'.join([match.group(1)] * int(match.group(2)))+')', linetowrite)
        f.write(linetowrite)
    f.write("\n")
f.close()

