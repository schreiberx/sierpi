/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_WIREFRAME_TSUNAMI_HPP_
#define COPENGL_VERTICES_WIREFRAME_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"

namespace sierpi
{
namespace kernels
{


class COpenGL_Vertices_Wireframe_Tsunami
{
public:
	typedef typename CTsunamiSimulationStacksAndTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar 			TVertexScalar;
	typedef TVertexScalar 	T;

	typedef sierpi::travs::CTraversator_VertexCoords<COpenGL_Vertices_Wireframe_Tsunami, CTsunamiSimulationStacksAndTypes> TRAV;

private:
	T *vertex_buffer;
	T *current_vertex_line;

	T *end_line;
	size_t max_vertices;

	T scale_min;
	T scale_factor;

	COpenGL_Vertices_Wireframe_Root_Tsunami *cOpenGL_Vertices_Wireframe_Root_Tsunami;

public:

	inline COpenGL_Vertices_Wireframe_Tsunami()
	{
		max_vertices = COPENGL_VERTICES_WIREFRAME_ROOT_TSUNAMI_VERTEX_COUNT;
		setup(0,1,NULL);

		// allocate 3 components per vertex
		vertex_buffer = new T[3*max_vertices];

		// since 6 vertices are emitted per triangle, the lines are rendered
		// when exceeding the last element
		end_line = vertex_buffer + 3*max_vertices;
	}

	virtual inline ~COpenGL_Vertices_Wireframe_Tsunami()
	{
		delete[] vertex_buffer;
	}


	inline void renderOpenGLVertexArray(
			const size_t p_vertex_count
	)
	{
		assert(cOpenGL_Vertices_Wireframe_Root_Tsunami != nullptr);

		cOpenGL_Vertices_Wireframe_Root_Tsunami->renderOpenGLVertexArray(vertex_buffer, p_vertex_count);
	}


	inline void op_cell(
			T vx1, T vy1,
			T vx2, T vy2,
			T vx3, T vy3)
	{
		current_vertex_line[0*3+0] = vx1;
		current_vertex_line[0*3+1] = 0;
		current_vertex_line[0*3+2] = -vy1;

		current_vertex_line[1*3+0] = vx2;
		current_vertex_line[1*3+1] = 0;
		current_vertex_line[1*3+2] = -vy2;

		current_vertex_line[2*3+0] = vx2;
		current_vertex_line[2*3+1] = 0;
		current_vertex_line[2*3+2] = -vy2;

		current_vertex_line[3*3+0] = vx3;
		current_vertex_line[3*3+1] = 0;
		current_vertex_line[3*3+2] = -vy3;

		current_vertex_line[4*3+0] = vx3;
		current_vertex_line[4*3+1] = 0;
		current_vertex_line[4*3+2] = -vy3;

		current_vertex_line[5*3+0] = vx1;
		current_vertex_line[5*3+1] = 0;
		current_vertex_line[5*3+2] = -vy1;

		current_vertex_line += 6*3;

		assert(current_vertex_line <= end_line);
		if (current_vertex_line >= end_line)
		{
			renderOpenGLVertexArray(max_vertices);
			current_vertex_line = vertex_buffer;
			return;
		}
	}

	inline void traversal_pre_hook()
	{
		current_vertex_line = vertex_buffer;
	}

	inline void traversal_post_hook()
	{
		if (current_vertex_line > vertex_buffer)
		{
			renderOpenGLVertexArray(
					(size_t)(current_vertex_line-vertex_buffer)/(3));
		}
	}


	inline void setup(
			const T p_min,
			const T p_max,
			COpenGL_Vertices_Wireframe_Root_Tsunami *p_cOpenGL_Vertices_Wireframe_Root_Tsunami
	)
	{
		scale_min = p_min;
		scale_factor = 0.2f/(p_max-p_min);

		cOpenGL_Vertices_Wireframe_Root_Tsunami = p_cOpenGL_Vertices_Wireframe_Root_Tsunami;

	}

	inline void setup_WithKernel(
			COpenGL_Vertices_Wireframe_Tsunami &parent
	)
	{
		scale_min = parent.scale_min;
		scale_factor = parent.scale_factor;

		cOpenGL_Vertices_Wireframe_Root_Tsunami = parent.cOpenGL_Vertices_Wireframe_Root_Tsunami;
	}

};

}
}

#endif
