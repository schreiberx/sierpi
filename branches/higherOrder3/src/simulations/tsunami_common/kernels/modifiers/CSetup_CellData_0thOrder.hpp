/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData_Depth.hpp"
#include "../common/CAdaptivity_0thOrder.hpp"
#include "../../CTsunamiSimulationScenarios.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename t_CSimulationStacksAndTypes>
class CSetup_CellData_0thOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_CellData_Depth<CSetup_CellData_0thOrder, t_CSimulationStacksAndTypes>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiDataScalar T;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CEdgeData CEdgeData;


	/*
	 * callback for terrain data
	 */
	CTsunamiSimulationScenarios *cSimulationScenarios;

	bool initial_grid_setup;



	CSetup_CellData_0thOrder()	:
		cSimulationScenarios(nullptr),
		initial_grid_setup(true)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			TTsunamiVertexScalar vertex_left_x,		TTsunamiVertexScalar vertex_left_y,
			TTsunamiVertexScalar vertex_right_x,	TTsunamiVertexScalar vertex_right_y,
			TTsunamiVertexScalar vertex_top_x,		TTsunamiVertexScalar vertex_top_y,
			int i_depth,

			CTsunamiSimulationCellData *io_cellData
	)
	{
		TVertexScalar mx, my;

		CAdaptivity_0thOrder::computeAdaptiveSamplingPoint(
			vertex_left_x, vertex_left_y,
			vertex_right_x, vertex_right_y,
			vertex_top_x, vertex_top_y,
			&mx, &my
		);

		io_cellData->dofs_center.b = cSimulationScenarios->getTerrainHeightByPosition(mx, my, i_depth);

		if (cSimulationScenarios->getUseMaximumFunctionForSurfaceHeightSetup() && !initial_grid_setup)
		{
			// fix height with bathymetry displacement
			io_cellData->dofs_center.h = CMath::max(io_cellData->dofs_center.h, -io_cellData->dofs_center.b + cSimulationScenarios->getWaterSurfaceHeightByPosition(mx, my, i_depth));

			if (io_cellData->dofs_center.h < 0)
				io_cellData->dofs_center.h = 0;
		}
		else
		{
			io_cellData->dofs_center.h = -io_cellData->dofs_center.b + cSimulationScenarios->getWaterSurfaceHeightByPosition(mx, my, i_depth);

			if (io_cellData->dofs_center.h < 0)
				io_cellData->dofs_center.h = 0;

			io_cellData->dofs_center.hu = 0;
			io_cellData->dofs_center.hv = 0;
		}

		io_cellData->cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void setup_Parameters(
			CTsunamiSimulationScenarios *i_cSimulationScenarios,
			bool i_initial_grid_setup
	)
	{
		cSimulationScenarios = i_cSimulationScenarios;
		initial_grid_setup = i_initial_grid_setup;
	}



	void setup_WithKernel(
			CSetup_CellData_0thOrder<t_CSimulationStacksAndTypes> &parent
	)
	{
		cSimulationScenarios = parent.cSimulationScenarios;
		initial_grid_setup = parent.initial_grid_setup;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */

