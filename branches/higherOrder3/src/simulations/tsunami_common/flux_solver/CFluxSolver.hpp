/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxSolver.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_HPP_
#define CFLUXSOLVER_HPP_

#include "../CTsunamiConfig.hpp"


/****************************************************************
 * SOLVER: LAX FRIEDRICHS CONSTANT FRICTION
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==0

#include "CFluxSolver_LaxFriedrichConstantFriction.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_LaxFriedrichConstantFriction<T>
{
};

#endif



/****************************************************************
 * SOLVER: LAX FRIEDRICHS
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==1

#include "CFluxSolver_LaxFriedrich.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_LaxFriedrich<T>
{
};

#endif



/****************************************************************
 * SOLVER: FWAVE
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==2


// suppress debug output
#define SIERPI_SUPPRESS_TSUNAMI_FLUX_DEBUG_OUTPUT 1

#include "CFluxSolver_FWave.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_FWave<T>
{
};

#endif



/****************************************************************
 * SOLVER: AUGUMENTED RIEMANN
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==3


// suppress debug output
#define SIERPI_SUPPRESS_TSUNAMI_FLUX_DEBUG_OUTPUT 1

#include "CFluxSolver_AugumentedRiemann.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_AugumentedRiemann<T>
{
};

#endif



/****************************************************************
 * SOLVER: HYBRID
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==4


// suppress debug output
#define SIERPI_SUPPRESS_TSUNAMI_FLUX_DEBUG_OUTPUT 1

#include "CFluxSolver_Hybrid.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_Hybrid<T>
{
};

#endif




/****************************************************************
 * SOLVER: GEOCLAW AUGUMENTED RIEMANN
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==5


// suppress debug output
#define SIERPI_SUPPRESS_TSUNAMI_FLUX_DEBUG_OUTPUT 1

#include "CFluxSolver_GeoClawAugumentedRiemann.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_GeoClawAugumentedRiemann<T>
{
};

#endif




/****************************************************************
 * SOLVER: VELOCITY UPWINDING
 ****************************************************************/
#if SIMULATION_TSUNAMI_FLUX_SOLVER==6

#include "CFluxSolver_VelocityUpwinding.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_VelocityUpwinding<T>
{
};

#endif


#endif /* CFLUXSOLVER_HPP_ */
