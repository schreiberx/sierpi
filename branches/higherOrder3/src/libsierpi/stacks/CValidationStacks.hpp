/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 *  Created on: June 22, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CVALIDATION_STACKS_HPP_
#define CVALIDATION_STACKS_HPP_

#include <stdlib.h>
#include <cassert>
#include "libmath/CVertex2d.hpp"
#include "CFBStacks.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "libsierpi/kernels/validate/CValidateTypes.hpp"

/**
 * \brief convenient storage for all stacks needed to run the validation functions
 */
class CValidationStacks
{
public:
	/***********************
	 * VALIDATION STACKS
	 */
	// element stacks (ELEMENT_STACK)
	CFBStacks<CValCellData> element_data_stacks;

	// communication for timestep (TIMESTEP_STACK)
	CStack<CValEdgeData> edge_data_comm_left_edge_stack;
	CStack<CValEdgeData> edge_data_comm_right_edge_stack;

	// stacks acting as buffers for parallelization (data exchange without conflicts)
	CStack<CValEdgeData> edge_data_comm_exchange_left_edge_stack;
	CStack<CValEdgeData> edge_data_comm_exchange_right_edge_stack;

	// stack to store temporary edge information for timestep (TIMESTEP_STACK)
	CStack<CValEdgeData> edge_comm_buffer;


public:
	/**
	 * the stacks are initialized with the following formulae:
	 *
	 * the easiest case is the number of the leaf elements.
	 * since each triangle is split up into 2 disjoint ones, the max. number of leaf-triangles is:
	 *
	 * | max_leaf_triangles = 2^max_depth
	 *
	 * when having a look at the number of triangles laying directly at the catheti,
	 * this number is doubled when refining 2 times:
	 *
	 * | max_comm_edges = 2^(max_depth/2)
	 */
	CValidationStacks(
			unsigned int p_max_depth
	)
	{
		p_max_depth++;

		unsigned int max_element_data = (1 << (p_max_depth));
		unsigned int max_structure_elements = (1 << (p_max_depth+1));

		unsigned int max_comm_edges = (1 << (p_max_depth/2+2));
		unsigned int max_edge_comm_buffer = max_structure_elements;


		element_data_stacks.resize(max_element_data);

		// communication for timestep (TIMESTEP_STACK)
		edge_data_comm_left_edge_stack.resize(max_comm_edges);
		edge_data_comm_right_edge_stack.resize(max_comm_edges);

		// stacks acting as buffers for parallelization (data exchange without conflicts)
		edge_data_comm_exchange_left_edge_stack.resize(max_comm_edges);
		edge_data_comm_exchange_right_edge_stack.resize(max_comm_edges);

		// stack to store temporary edge information for timestep (TIMESTEP_STACK)
		edge_comm_buffer.resize(max_edge_comm_buffer);
	}
};

#endif /* CSIERPINSKI_H_ */
