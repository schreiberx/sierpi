/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CStackReaderTopDown.hpp
 *
 *  Created on: Jun 5, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSTACKREADERTOPBOTTOM_HPP_
#define CSTACKREADERTOPBOTTOM_HPP_

template <typename T>
class CStackReaderTopDown;

#include "CStack.hpp"

/**
 * \brief non-destructive stack reader reading data elements from top to bottom
 */
template <typename T>
class CStackReaderTopDown
{
	friend class CStack<T>;

public:
	CStack<T> *stack;
	T *stack_ptr;

	inline void setup(CStack<T> *p_stack)
	{
		stack = p_stack;
		reset();
	}

	inline void setup(CStack<T> &p_stack)
	{
		stack = &p_stack;
		reset();
	}

	inline T getNextData()
	{
		stack_ptr--;
		assert(stack_ptr >= stack->stack_start_ptr);
		return *stack_ptr;
	}

	inline T* getNextDataPtr()
	{
		stack_ptr--;
		assert(stack_ptr >= stack->stack_start_ptr);
		return stack_ptr;
	}


	inline T& getNextDataRef()
	{
		stack_ptr--;
		assert(stack_ptr >= stack->stack_start_ptr);
		return *stack_ptr;
	}

	inline T& getPreviousDataRef()
	{
		return *stack_ptr;
	}

	inline T* getPreviousDataPtr()
	{
		return stack_ptr;
	}

	inline T* getPreviousPreviousDataPtr()
	{
		return stack_ptr+1;
	}

	/**
	 * reset to topmost stack element
	 */
	inline void reset()
	{
		stack_ptr = stack->stack_ptr;
	}

	/**
	 * return true if reader reached the bottom of the stack
	 */
	inline bool isEmpty()
	{
		return stack_ptr == stack->stack_start_ptr;
	}

	/**
	 * skip number_of_elements elements
	 */
	inline void skip(size_t p_number_of_elements)
	{
		stack_ptr -= p_number_of_elements;
	}
};


#endif
