#/usr/bin/python

f=open("output/matrices.h","w")

import glob, os
import re

for mat in glob.glob("./mapledata/*.matrix"):
    print ("Processing " +  mat + " ...")
    # collect stuff from file
    contents = []
    replacements = []
    for line in open(mat):
        if line.startswith("Warning: the following variable name replacements were made:"):
            matches = re.match(r'Warning.*?\[(.*?)\]\s=\s\[(.*?)\]', line)
            matches.group(1)
            s = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(1)).split(',')
            r = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(2)).split(',')
            for i in range(len(s)):
                replacements.append((s[i],r[i]))
            continue
        if line.startswith("Warning") or line.startswith(">"):
            continue
        parts = re.match(".*?(\[.*\]).*=(.*);",line).groups()
        index = [int(x) for x in re.search("\[(.*?)\]\[(.*)\]",parts[0]).groups()]
        value = parts[1]
        contents.append(index+[value,])
    # sort stuff
    maxx = max(contents, key=lambda x:x[0])[0]
    maxy = max(contents, key=lambda x:x[1])[1]
    print maxx
    print maxy
    realcontents = [[0 for y in xrange(maxy+1)] for x in xrange(maxx+1)]
    for [x,y,c] in contents:
        realcontents[x][y] = c
    # write stuff to matrices.h
    f.write ("T %s[%d][%d] = {\n"%(os.path.basename(mat).split(".")[0],maxx+1,maxy+1))
    replacements.extend([("vx", "qx"), ("vy","qy"), ("double", "T")])
    for els in realcontents:
        i = ['(T)('+el.strip()+')' if re.match(r'.*[\+\-\*\/\(\)]', el)  else el for el in els]
        linetowrite = "\t{%s},\n"%(",".join(i))
        for (src,dest) in replacements:
            linetowrite = linetowrite.replace(src,dest)
        linetowrite = re.sub(r'0\.(\d+)e(\d+)', lambda match: match.group(1)+"." if(len(match.group(1)) == int(match.group(2))) else float(match.group(0)), linetowrite)
        linetowrite = re.sub(r'pow\(\s*?(.*?),\s*?(\-\d+?)\.\s*?\)', lambda match: '(1./('+'*'.join([match.group(1)] * abs(int(match.group(2))))+'))', linetowrite)
        linetowrite = re.sub(r'pow\(\s*?(.*?),\s*?(\d+?)\.\s*?\)', lambda match: '('+'*'.join([match.group(1)] * int(match.group(2)))+')', linetowrite)
        f.write(linetowrite)

    f.write("};\n")
f.close()

