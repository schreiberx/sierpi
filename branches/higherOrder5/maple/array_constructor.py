#/usr/bin/python

f=open('output/arrays.h','w')

import glob, os
import re

replacements = [('vx', 'hu'), ('vy','hv'), ('g', 'gravitational_constant'), ('double', 'T')]

def prettify(linetowrite):
    for (src,dest) in replacements:
        line = linetowrite.replace(src,dest)
    line = re.sub(r'0\.(\d+)e(\d+)', lambda match: match.group(1)+'.' if(len(match.group(1)) == int(match.group(2))) else float(match.group(0)), line)
    line = re.sub(r'pow\(\s*?(.*?),\s*?(\-\d+?)\.?\s*?\)', lambda match: '(1./('+'*'.join([match.group(1)] * abs(int(match.group(2))))+'))', line)
    line = re.sub(r'pow\(\s*?(.*?),\s*?(\d+?)\.?\s*?\)', lambda match: '('+'*'.join([match.group(1)] * int(match.group(2)))+')', line)
    return line

for obj in glob.glob('./mapledata/*.matrix')+glob.glob('./mapledata/*.vector'):
    ext = os.path.splitext(obj)[1][1:]
    print "Processing File %s..." % obj
    contents = []
    # Read through lines and add relevant information to the contents lists
    for line in open(obj):
        if line.startswith("Warning: the following variable name replacements were made:"):
            matches = re.match(r'Warning.*?\[(.*?)\]\s=\s\[(.*?)\]', line)
            matches.group(1)
            s = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(1)).split(',')
            r = re.sub(r'\s?"(.*?)~?"', '\g<1>', matches.group(2)).split(',')
            for i in range(len(s)):
                replacements.append((s[i],r[i]))
            continue
        if line.startswith("Warning") or line.startswith(">"):
            continue
        parts = re.match(".*?(\[.*\]).*=(.*);",line).groups()
        if ext == 'vector':
            index = [int(x) for x in re.search("\[(.*?)\]",parts[0]).groups()]
        elif ext == 'matrix':
            index = [int(x) for x in re.search("\[(.*?)\]\[(.*)\]",parts[0]).groups()]
        value = parts[1]
        contents.append(index+[value])

    # Prepare lines for writing (format with proper syntax)
    lines = []
    if ext == 'vector':
        maxv = max(contents, key=lambda x:x[0])[0]
        realcontents = [0 for x in xrange(maxv+1)]
        for [x,c] in contents:
            realcontents[x] = c
        f.write ("T %s[%d] = {\n"%(os.path.basename(obj).split(".")[0],maxv+1))
        for el in realcontents:
            linetowrite = "\t%s"%el
            i = '(T)('+el.strip()+')' if re.match(r'.*[\+\-\*\/\(\)]', el) else el.strip()
            lines.append(prettify(linetowrite))
    elif ext == 'matrix':
        maxx = max(contents, key=lambda x:x[0])[0]
        maxy = max(contents, key=lambda x:x[1])[1]
        realcontents = [[0 for y in xrange(maxy+1)] for x in xrange(maxx+1)]
        for [x,y,c] in contents:
            realcontents[x][y] = c
        f.write ("T %s[%d][%d] = {\n" % (os.path.basename(obj).split(".")[0], maxx+1, maxy+1))
        for els in realcontents:
            i = ['(T)('+el.strip()+')' if re.match(r'.*[\+\-\*\/\(\)]', el)  else el.strip() for el in els]
            linetowrite = "\t{%s}" % ", ".join(i)
            lines.append(prettify(linetowrite))
    f.write(",\n".join(lines))
    f.write("\n};\n")

f.close()

def processFunctionsFile(filename, outputfile, functionname="phi_%d"):
    replacements = [("int", "T"), ("double", "T")]
    counter = 0
    for l in open(filename):
        if l.startswith("Warning") or l.startswith(">"):
            continue
        line = l
        for (src, tar) in replacements:
            line = line.replace(src, tar)
        line = line.split("=")[1].strip()
        line = "static inline T " + functionname%(counter) + "(T x, T y){ return %s }\n"%(line)
        outputfile.write(line)
        counter = counter + 1

def generatePhiHPP():
    phiFile = open("output/phi.hpp","w") 
    print ("Processing phi stuff")
    processFunctionsFile("mapledata/phi.function", phiFile)
    processFunctionsFile("mapledata/phi_dx.function", phiFile, "phi_%d_dx")
    processFunctionsFile("mapledata/phi_dy.function", phiFile, "phi_%d_dy")
    phiFile.close()

generatePhiHPP()
