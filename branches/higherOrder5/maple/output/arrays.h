T E[6][3] = {
	{(T)(c / 6.), 0, (T)(c / 6.)},
	{(T)(2. / 3. * c), 0, 0},
	{(T)(c / 6.), (T)(sqrt(2.) * c / 6.), 0},
	{0, (T)(2. / 3. * sqrt(2.) * c), 0},
	{0, (T)(sqrt(2.) * c / 6.), (T)(c / 6.)},
	{0, 0, (T)(2. / 3. * c)}
};
T Sy[6][6] = {
	{(T)(-c / 15.), (T)(-c / 10.), (T)(c / 30.), (T)(c / 30.), (T)(c / 30.), (T)(-c / 10.)},
	{(T)(c / 30.), (T)(-4. / 15. * c), (T)(-c / 15.), (T)(-4. / 15. * c), (T)(c / 30.), (T)(-2. / 15. * c)},
	{0, 0, 0, 0, 0, 0},
	{(T)(-c / 30.), (T)(4. / 15. * c), (T)(c / 15.), (T)(4. / 15. * c), (T)(-c / 30.), (T)(2. / 15. * c)},
	{(T)(-c / 30.), (T)(-c / 30.), (T)(-c / 30.), (T)(c / 10.), (T)(c / 15.), (T)(c / 10.)},
	{(T)(c / 10.), (T)(2. / 15. * c), 0, (T)(-2. / 15. * c), (T)(-c / 10.), 0}
};
T M[6][6] = {
	{(T)(72. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(-3. * (1./(c*c)))},
	{(T)(-3. * (1./(c*c))), (T)(39. / 2. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c)))},
	{(T)(12. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(72. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(12. * (1./(c*c)))},
	{(T)(12. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(39. / 2. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c)))},
	{(T)(12. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(72. * (1./(c*c))), (T)(-3. * (1./(c*c)))},
	{(T)(-3. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c))), (T)(12. * (1./(c*c))), (T)(-27. / 4. * (1./(c*c))), (T)(-3. * (1./(c*c))), (T)(39. / 2. * (1./(c*c)))}
};
T G[6][9] = {
	{(T)((10. * (1./(c, 2.) + 3. * c * sqrt(15.) - 15. * c + 8. - 2. * sqrt(15.)) * (c*c*c*c) + 3. * c * sqrt(15.) - 15. * c + 8. - 2. * sqrt(15.)) * (c)) / 10.), (T)((2. * (1./(c*c)) / 10.), (T)((2. * (1./(c) - 3. * c + 1.) * (c*c*c*c) - 3. * c + 1.) * (c)) / 2.), (T)(-(-10. * (1./(c*c)) / 2.), (T)(-(-10. * (1./(c) + 3. * c * sqrt(15.) + 15. * c - 8. - 2. * sqrt(15.)) * (c*c*c*c) + 3. * c * sqrt(15.) + 15. * c - 8. - 2. * sqrt(15.)) * (c)) / 10.), (T)(((1./(c*c)) / 10.), (T)(((1./(c) - 3. * c + 2.) * (c*c*c*c) - 3. * c + 2.) * (c))), (T)(((1./(c*c))), (T)(((1./(c) - 3. * c + 2.) * (c*c*c*c) - 3. * c + 2.) * (c))), (T)(((1./(c*c))), (T)(((1./(c) - 3. * c + 2.) * (c*c*c*c) - 3. * c + 2.) * (c))), (T)(-(-10. * (1./(c*c))), (T)(-(-10. * (1./(c) + 3. * c * sqrt(15.) + 15. * c - 8. - 2. * sqrt(15.)) * (c*c*c*c) + 3. * c * sqrt(15.) + 15. * c - 8. - 2. * sqrt(15.)) * (c)) / 10.), (T)((2. * (1./(c*c)) / 10.), (T)((2. * (1./(c) - 3. * c + 1.) * (c*c*c*c) - 3. * c + 1.) * (c)) / 2.), (T)((10. * (1./(c*c)) / 2.), (T)((10. * (1./(c) + 3. * c * sqrt(15.) - 15. * c + 8. - 2. * sqrt(15.)) * (c*c*c*c) + 3. * c * sqrt(15.) - 15. * c + 8. - 2. * sqrt(15.)) * pow(c)) / 10.)},
	{(T)(-(sqrt(15.) - 5.) * (10. * c + sqrt(15.) - 5.) * (1./(c*c)) / 25.), (T)((2. * c - 1.) * (1./(c*c))), (T)(-(sqrt(15.) + 5.) * (-10. * c + sqrt(15.) + 5.) * (1./(c*c)) / 25.), (T)(2. / 5. * (sqrt(15.) + 5.) * (c - 1.) * (1./(c*c))), (T)(2. * (c - 1.) * (1./(c*c))), (T)(-2. / 5. * (sqrt(15.) - 5.) * (c - 1.) * (1./(c*c))), 0, 0, 0},
	{(T)((sqrt(15.) - 5.) * (5. * c + sqrt(15.) - 5.) * (1./(c*c)) / 50.), (T)(-(c - 1.) * (1./(c*c)) / 2.), (T)((sqrt(15.) + 5.) * (-5. * c + sqrt(15.) + 5.) * (1./(c*c)) / 50.), (T)((sqrt(15.) + 5.) * (-5. * c + sqrt(15.) + 5.) * (1./(c*c)) / 50.), (T)(-(c - 1.) * (1./(c*c)) / 2.), (T)((sqrt(15.) - 5.) * (5. * c + sqrt(15.) - 5.) * (1./(c*c)) / 50.), 0, 0, 0},
	{0, 0, 0, (T)(2. / 5. * (1./(c*c))), (T)((1./(c*c))), (T)(2. / 5. * (1./(c*c))), 0, 0, 0},
	{0, 0, 0, (T)((sqrt(15.) - 5.) * (5. * c + sqrt(15.) - 5.) * (1./(c*c)) / 50.), (T)(-(c - 1.) * (1./(c*c)) / 2.), (T)((sqrt(15.) + 5.) * (-5. * c + sqrt(15.) + 5.) * (1./(c*c)) / 50.), (T)((sqrt(15.) + 5.) * (-5. * c + sqrt(15.) + 5.) * (1./(c*c)) / 50.), (T)(-(c - 1.) * (1./(c*c)) / 2.), (T)((sqrt(15.) - 5.) * (5. * c + sqrt(15.) - 5.) * (1./(c*c)) / 50.)},
	{0, 0, 0, (T)(-2. / 5. * (sqrt(15.) - 5.) * (c - 1.) * (1./(c*c))), (T)(2. * (c - 1.) * (1./(c*c))), (T)(2. / 5. * (sqrt(15.) + 5.) * (c - 1.) * (1./(c*c))), (T)(-(sqrt(15.) + 5.) * (-10. * c + sqrt(15.) + 5.) * (1./(c*c)) / 25.), (T)((2. * c - 1.) * (1./(c*c))), (T)(-(sqrt(15.) - 5.) * (10. * c + sqrt(15.) - 5.) * (1./(c*c)) / 25.)}
};
T Sx[6][6] = {
	{(T)(-c / 15.), (T)(-c / 10.), (T)(c / 30.), (T)(c / 30.), (T)(c / 30.), (T)(-c / 10.)},
	{(T)(c / 10.), 0, (T)(-c / 10.), (T)(-2. / 15. * c), 0, (T)(2. / 15. * c)},
	{(T)(-c / 30.), (T)(c / 10.), (T)(c / 15.), (T)(c / 10.), (T)(-c / 30.), (T)(-c / 30.)},
	{(T)(-c / 30.), (T)(2. / 15. * c), (T)(-c / 30.), (T)(4. / 15. * c), (T)(c / 15.), (T)(4. / 15. * c)},
	{0, 0, 0, 0, 0, 0},
	{(T)(c / 30.), (T)(-2. / 15. * c), (T)(c / 30.), (T)(-4. / 15. * c), (T)(-c / 15.), (T)(-4. / 15. * c)}
};
T w[3] = {
	 5. / 9.,
	 8. / 9.,
	 5. / 9.
};
