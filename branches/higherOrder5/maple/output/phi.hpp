T phi_0(T x, T y){ return 1 - 3 * x - 3 * y + 2 * x * x + 4 * x * y + 2 * y * y; }
T phi_1(T x, T y){ return 4 * x * (1 - x - y); }
T phi_2(T x, T y){ return -x * (1 - 2 * x); }
T phi_3(T x, T y){ return 4 * x * y; }
T phi_4(T x, T y){ return -y * (1 - 2 * y); }
T phi_5(T x, T y){ return 4 * y * (1 - x - y); }
T phi_0_dx(T x, T y){ return -3 + 4 * x + 4 * y; }
T phi_1_dx(T x, T y){ return 4 - 8 * x - 4 * y; }
T phi_2_dx(T x, T y){ return -1 + 4 * x; }
T phi_3_dx(T x, T y){ return 4 * y; }
T phi_4_dx(T x, T y){ return 0; }
T phi_5_dx(T x, T y){ return -4 * y; }
T phi_0_dy(T x, T y){ return -3 + 4 * x + 4 * y; }
T phi_1_dy(T x, T y){ return -4 * x; }
T phi_2_dy(T x, T y){ return 0; }
T phi_3_dy(T x, T y){ return 4 * x; }
T phi_4_dy(T x, T y){ return -1 + 4 * y; }
T phi_5_dy(T x, T y){ return 4 - 4 * x - 8 * y; }
