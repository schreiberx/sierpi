#! /bin/bash

cd ../..
make clean

scons --xml-config=./scenarios/netcdf_intel_release.xml --enable-exit-on-instability-thresholds=on -j 8
#scons --xml-config=./scenarios/netcdf_intel_release.xml -j 8

#scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-asagi=on --hyperbolic-flux-solver=5 --fp-default-precision=double -j8 --hyperbolic-runge-kutta-order=1 --hyperbolic-adaptivity-mode=2
