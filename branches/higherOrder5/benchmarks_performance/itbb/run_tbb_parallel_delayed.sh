#!/bin/bash

# application is running for 100 seconds in average

. params.sh

killall sierpi_intel_tbb_tsunami_parallel_release

EXEC="$EXEC_TBB"
PARAMS="$PARAMS_DEFAULT -N 40 -n 40"

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}


date
STARTSECONDS=`date +%s`

echo "App 1: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_parallel_delayed_1.txt" &
PA1=$!

sleep 10

echo "App 2: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_parallel_delayed_2.txt" &
PA2=$!

sleep 20

echo "App 3: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_parallel_delayed_3.txt" &
PA3=$!

sleep 10

echo "App 4: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_parallel_delayed_4.txt" &
PA4=$!


wait $PA1
wait $PA2
wait $PA3
wait $PA4


ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS" > "output_""$DATE""_tbb_parallel_delayed.txt"

sleep 1
