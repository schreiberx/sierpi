Executable + params: ../../build/sierpi_intel_tbb_sacsp_tsunami_parallel_release -o 32768 -A 1 -d 20 -a 8 -t 100
Problem size with different number of CPUs using function optimization
THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS
80	74.0718	100	0.000491318		1.9047e+07	1158.74
64	67.1902	100	0.000491318		1.9047e+07	1158.74
40	71.1767	100	0.000491318		1.9047e+07	1158.74
32	48.5284	100	0.000491318		1.9047e+07	1158.74
16	23.3275	100	0.000491318		1.9047e+07	1158.74
8	14.5997	100	0.000491318		1.9047e+07	1158.74
4	9.99811	100	0.000491318		1.9047e+07	1158.74
2	6.13664	100	0.000491318		1.9047e+07	1158.74
1	3.11153	100	0.000491318		1.9047e+07	1158.74
