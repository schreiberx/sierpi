#! /usr/bin/python

import sys
import subprocess
import os
import time
import commands

base_depth=10

parallel_compilation_processes = 4

parallel_simulation_processes = -1

default_run_params=" -A 1 -n "+str(parallel_simulation_processes)
default_compile_params=" --enable-exit-on-small-timestep=on --enable-exit-on-instability-thresholds=on"

def testGCC():
	#
	# TEST GCC VERSION
	#
	reqversion = [4,6,1]

	#
	# get gcc version using -v instead of -dumpversion since SUSE gnu compiler
	# returns only 2 instead of 3 digits with -dumpversion
	#
	gccv = commands.getoutput('g++ -v').splitlines()
	gccversion = gccv[-1].split(' ')[2].split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ERROR: At least GCC Version 4.6.1 necessary.'
			sys.exit(-1)


def testIntelCompiler():
	#
	# TEST INTEL VERSION
	#
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'At least ICPC Version 12.1 necessary.'
			Exit(1)



mainmode = None
if len(sys.argv) > 1:
	mainmode = sys.argv[1]

subtests = None
if len(sys.argv) > 2:
	subtests = int(sys.argv[2])


prev_compiler_cmd=''

#
# executed the unit tests given by i
#
def unitTest(i, subtests):
	global prev_compiler_cmd
	CRED = '\033[91m'
	CGREEN = '\033[92m'
	CDEFAULT = '\033[0m'

	def print_err(s):
		print CRED+s+CDEFAULT

	def print_ok(s):
		print CGREEN+s+CDEFAULT

	print "Running subtest "+str(subtests)

	compiler_cmd = i[0]+' -j '+str(parallel_compilation_processes)
	exec_cmd = i[1]

	if compiler_cmd != prev_compiler_cmd:
		print "COMPILING: "+compiler_cmd
		p = subprocess.Popen(['make clean 2>&1 > /dev/null'], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO MAKE CLEAN!")
			return

		prev_compiler_cmd = compiler_cmd

		p = subprocess.Popen([compiler_cmd+' 2>&1 > /dev/null'], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO COMPILE! ("+compiler_cmd+")")
			sys.exit(-1)
	else:
		print "Using previously compiled program"

	print "EXECUTING: "+exec_cmd

	startTime = time.time()

	p = subprocess.Popen([exec_cmd+' 2>&1 > /dev/null'], shell=True)
	p.wait()
	if p.returncode != 0:
		print_err(" > TEST FAILED")
		sys.exit(-1)
	else:
		print_ok(" > TEST OK ("+str(time.time() - startTime)+" Seconds)")

	print "*"*60


#
# GNU Compiler tests
#
if mainmode == None or mainmode=='gnu_compiler':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: gnu_compiler"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=off --simulation=tsunami_serial --mode=debug'+default_compile_params, './build/sierpi_gnu_tsunami_serial_b0_debug -d 9'],
		['scons --compiler=gnu --threading=off --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_hyperbolic_parallel_tsunami_b0_debug -d 9 -w 6'],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_debug -d 7'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_release -d 8 -o 4 '+default_run_params+' -a 6'],

	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# EVEN/ODD Triangle tests
#
if mainmode == None or mainmode=='even_odd':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: even_odd"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-4)+' -w 7'+default_run_params]
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# RK2 tests
#
if mainmode == None or mainmode=='rk2':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: rk2"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=2'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_rk2_b0_debug -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=2'+default_compile_params, './build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_rk2_b0_debug -d '+str(base_depth-4)+' -w 7'+default_run_params]
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# Special compile options
#
if mainmode == None or mainmode=='sco':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: sco"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on'+default_compile_params, './build/sierpi_intel_omp_scan_data_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on'+default_compile_params, './build/sierpi_intel_omp_scan_threading_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-based-split-and-join=on'+default_compile_params, './build/sierpi_intel_omp_scan_threading_scan_saj_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-based-split-and-join=on --enable-scan-force-threading-traversal=on'+default_compile_params, './build/sierpi_intel_omp_scan_force_threading_scan_saj_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-based-split-and-join=on --enable-scan-force-threading-traversal=on'+default_compile_params, './build/sierpi_intel_omp_scan_force_threading_scan_saj_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-force-threading-traversal=on'+default_compile_params, './build/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' -L 100 '+default_run_params],

		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-skip-adaptive-conforming-clusters=on'+default_compile_params, './build/sierpi_intel_omp_sacsp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-5)+' '+default_run_params],

		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-hyperbolic-cluster-local-time-stepping=on'+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-opencl=on'+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug  --hyperbolic-adaptivity-mode=2'+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_debug -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release --enable-skip-adaptive-conforming-clusters=on'+default_compile_params, './build/sierpi_intel_omp_sacsp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-3)+' -o 4098 '+default_run_params+' -a 6'],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# FLUX TESTS
#
if mainmode == None or mainmode=='fluxes':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: fluxes"
	print "*"*60

	tests = [
		# 1st order with fixed number of timesteps
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=0 --hyperbolic-degree-of-basis-functions=1'+default_compile_params,
				'./build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b1_release -d '+str(base_depth-4)+' -L 2000'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=1 --hyperbolic-degree-of-basis-functions=1'+default_compile_params,
				'./build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b1_release -d '+str(base_depth-2)+' -L 2000'+default_run_params],
		# 0th order
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=2 --hyperbolic-degree-of-basis-functions=0'+default_compile_params,
				'./build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=3 --hyperbolic-degree-of-basis-functions=0'+default_compile_params,
				'./build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=4 --hyperbolic-degree-of-basis-functions=0'+default_compile_params,
				'./build/sierpi_gnu_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=5 --hyperbolic-degree-of-basis-functions=0 --fp-default-precision=double'+default_compile_params,
				'./build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
	]


	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# Intel Compiler tests
#
if mainmode == None or mainmode=='intel_compiler':
	testIntelCompiler()

	print
	print "*"*60
	print "* MAIN TESTS: intel_compiler"
	print "*"*60

	tests = [
#		['scons --compiler=intel --threading=off --simulation=tsunami_serial --mode=release'+default_compile_params, './build/sierpi_intel_tsunami_serial_release -d '+str(base_depth+2)],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-4)+' -o 4'+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# TBB Tests
#
if mainmode == None or mainmode=='tbb':
	print
	print "*"*60
	print "* MAIN TESTS: tbb"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release'+default_compile_params, './build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release'+default_compile_params, './build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_b0_release -d '+str(base_depth-4)+' -o 4'+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)




#
# benchmarks
#
if mainmode == None or mainmode=='benchmarks':
	print
	print "*"*60
	print "* MAIN TESTS: benchmarks"
	print "*"*60

	tests = [
		['scons --xml-config=./scenarios/single_wave_on_simple_beach_1d.xml '+default_compile_params, './build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c ./scenarios/single_wave_on_simple_beach_1d.xml -b 9999999'],
		['scons --xml-config=./scenarios/single_wave_on_simple_beach_2d_strip.xml '+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_netcdf_b0_release -c ./scenarios/single_wave_on_simple_beach_2d_strip.xml -b 9999999'],
		['scons --xml-config=./scenarios/single_wave_on_composite_beach_1d.xml '+default_compile_params, './build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c ./scenarios/single_wave_on_composite_beach_1d.xml -b 9999999'],
		['scons --xml-config=./scenarios/single_wave_on_composite_beach_2d_strip.xml '+default_compile_params, './build/sierpi_intel_omp_hyperbolic_parallel_tsunami_netcdf_b0_release -c ./scenarios/single_wave_on_composite_beach_2d_strip.xml -b 9999999']
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# VALGRIND
# 
if mainmode == None or mainmode=='euler':
	print
	print "*"*60
	print "* MAIN TESTS: euler"
	print "*"*60

	tests = [
		['scons --simulation=hyperbolic_parallel --sub-simulation=euler --mode=debug', './build/sierpi_gnu_hyperbolic_parallel_euler_b0_debug -a 2 -d '+str(base_depth-4)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# VALGRIND
# 
if mainmode == None or mainmode=='valgrind':
	print
	print "*"*60
	print "* MAIN TESTS: valgrind"
	print "*"*60

	valgrind_prefix='valgrind --leak-check=full --show-reachable=yes --track-origins=yes --tool=memcheck --trace-children=yes --error-exitcode=1'

	tests = [
		['scons --simulation=hyperbolic_parallel --mode=debug', valgrind_prefix+' ./build/sierpi_gnu_hyperbolic_parallel_tsunami_b0_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
		['scons --simulation=hyperbolic_parallel --mode=debug --hyperbolic-degree-of-basis-functions=1', valgrind_prefix+' ./build/sierpi_gnu_hyperbolic_parallel_tsunami_b1_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
		['scons --simulation=hyperbolic_parallel --mode=debug --sub-simulation=euler', valgrind_prefix+' ./build/sierpi_gnu_hyperbolic_parallel_euler_b0_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
		['scons --simulation=tsunami_1d --mode=debug', valgrind_prefix+' ./build/sierpi_gnu_tsunami_1d_b0_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
		['scons --simulation=tsunami_serial --mode=debug', valgrind_prefix+' ./build/sierpi_gnu_tsunami_serial_b0_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],

#		['scons --compiler=intel --simulation=hyperbolic_parallel --threading=omp --mode=debug', valgrind_prefix+' ./build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_debug -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# terrain backends (asagi/netcdf)
#
if mainmode == None or mainmode=='terrain':
	print
	print "*"*60
	print "* MAIN TESTS: terrain"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release --enable-asagi=on'+default_compile_params, './build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_asagi_b0_release -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release --enable-netcdf=on'+default_compile_params, './build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_b0_release -d '+str(base_depth-2)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# GUI Tests
#

if not os.environ.has_key('DISPLAY'):
	print 'DISPLAY environment variable not set => skipping GUI tests'
else:

	if mainmode == None or mainmode=='gui':
		print
		print "*"*60
		print "* MAIN TESTS: gui"
		print "*"*60

		tests = [
			['scons --enable-gui=on --compiler=gnu --threading=off --simulation=tsunami_serial --mode=debug'+default_compile_params, './build/sierpi_gnu_gui_tsunami_serial_b0_debug -L 100 -d '+str(base_depth-2)],
			['scons --enable-gui=on --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_omp_gui_hyperbolic_parallel_tsunami_b0_debug -L 100 -d '+str(base_depth-2)+default_run_params],
			['scons --enable-gui=on --compiler=gnu --threading=tbb --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, './build/sierpi_gnu_tbb_gui_hyperbolic_parallel_tsunami_b0_debug -L 100 -d '+str(base_depth-2)+default_run_params],
		]

		if subtests != None:
			unitTest(tests[subtests], subtests)
			sys.exit(0)

		for i in range(0, len(tests)):
			unitTest(tests[i], i)


#
# MPI
#
if mainmode == None or mainmode=='mpi':
	print
	print "*"*60
	print "* MAIN TESTS: mpi"
	print "*"*60

	tests = [
		['scons --simulation=hyperbolic_parallel --enable-mpi=on --mode=release', './build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_b0_release -d '+str(base_depth-2)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)
