/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingOMP.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGOMP_HPP_
#define CMAINTHREADINGOMP_HPP_

#include <omp.h>
#include <iostream>
#include <cassert>


#include "CMainThreading_Interface.hpp"


bool CMainThreadingOMP_setAffinities(
		int i_num_threads,
		int i_max_cores,
		int i_distance,
		int i_start_id,
		int i_verbose_level
);



/**
 * Threading support for OpenMP
 */
class CMainThreading	:
	public CMainThreading_Interface
{
public:
	inline void threading_setup()
	{
		/*
		 * set number of threads which are initially used for simulation
		 */
		if (getMaxNumberOfThreads() <= 0)
			setMaxNumberOfThreads(omp_get_max_threads());

		/*
		 * maximum number of threads
		 */
		if (getNumberOfThreadsToUse() <= 0)
			setValueNumberOfThreadsToUse(getMaxNumberOfThreads());
		else
			omp_set_num_threads(getNumberOfThreadsToUse());


		if (getVerboseLevel() > 5)
		{
			std::cout << "getMaxNumberOfThreads(): " << (int)getMaxNumberOfThreads() << std::endl;
			std::cout << "getNumberOfThreadsToUse(): " << (int)getNumberOfThreadsToUse() << std::endl;
		}

		if (getThreadAffinityPadding() < 0)
			return;


		/*
		 * try to set the affinities
		 */
		CMainThreadingOMP_setAffinities(
				getNumberOfThreadsToUse(),
				getMaxNumberOfThreads(),
				getThreadAffinityPadding(),
				getThreadAffinityStartId(),
				getVerboseLevel()
			);
	}


	inline void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulation_loopIteration();

		} while(continue_simulation);
	}

	inline bool threading_simulationLoopIteration()
	{
		return simulation_loopIteration();
	}

	inline void threading_shutdown()
	{
	}

	inline void threading_setNumThreads(int i)
	{
		omp_set_num_threads(i);
	}


	inline int threading_getNumMaxThreads()
	{
		return omp_get_max_threads();
	}

	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
