/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGAUSSQUADRATURE1D_TRIANGLE_EDGE_HPP_
#define CGAUSSQUADRATURE1D_TRIANGLE_EDGE_HPP_


#include "CGaussQuadrature1D.hpp"


namespace sierpi
{
namespace pdelab
{


/**
 * return integration coordinates for edge
 *
 * |\
 * |  \
 * |    \
 * -------
 */
template <typename T, int t_degree>
class CGaussQuadrature1D_TriangleEdge
{
	static const T* getCoordsHypEdge();

	static const T* getCoordsRightEdge();

	static const T* getCoordsLeftEdge();

	static int getNumPoints();
};



/**
 * Degree 0
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 0>	:
	public CGaussQuadrature1D<T,0>
{
public:
	static const T* getCoordsHypEdge()
	{
		static const T values[] = {	0.5, 0.5 };
		return values;
	}

	static const T* getCoordsRightEdge()
	{
		static const T values[] = {	0.0, 0.5 };
		return values;
	}

	static const T* getCoordsLeftEdge()
	{
		static const T values[] = {	0.5, 0.0 };
		return values;
	}

	static int getNumPoints()
	{
		return 1;
	}
};



/**
 * Degree 1
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 1>	:
	CGaussQuadrature1D_TriangleEdge<T, 0>
{
};



/**
 * Degree 2
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 2>	:
	public CGaussQuadrature1D<T,2>
{
public:
	static const T* getCoordsHypEdge()
	{
		static const T *coords_1d = CGaussQuadrature1D<T,2>::getPositions();

		static const T coords_2d[] = {
				(T)0.5 - (T)0.5*coords_1d[0], (T)0.5 + (T)0.5*coords_1d[0],
				(T)0.5 - (T)0.5*coords_1d[1], (T)0.5 + (T)0.5*coords_1d[1]
		};

		return coords_2d;
	}

	static const T* getCoordsRightEdge()
	{
		static T coords_1d[] = {	-(T)std::sqrt(1.0/3.0),	(T)std::sqrt(1.0/3.0)	};

		static T coords_2d[] = {
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[0],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[1]
		};

		return coords_2d;
	}

	static const T* getCoordsLeftEdge()
	{
		static T coords_1d[] = {	-(T)std::sqrt(1.0/3.0),	(T)std::sqrt(1.0/3.0)	};

		static T coords_2d[] = {
				(T)0.5 + (T)0.5*coords_1d[0],	0.0,
				(T)0.5 + (T)0.5*coords_1d[1],	0.0
		};

		return coords_2d;
	}

	static int getNumPoints()
	{
		return 2;
	}
};



/**
 * Degree 3
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 3>	:
	CGaussQuadrature1D_TriangleEdge<T, 2>
{
};

/**
 * Degree 4
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 4> :
	public CGaussQuadrature1D<T, 4>
{
public:
	static const T* getCoordsHypEdge()
	{
		static const T *coords_1d = CGaussQuadrature1D<T,4>::getPositions();

		static const T coords_2d[] = {
				(T)0.5 - (T)0.5*coords_1d[0], (T)0.5 + (T)0.5*coords_1d[0],
				(T)0.5 - (T)0.5*coords_1d[1], (T)0.5 + (T)0.5*coords_1d[1],
				(T)0.5 - (T)0.5*coords_1d[2], (T)0.5 + (T)0.5*coords_1d[2],
		};

		return coords_2d;
	}

	static const T* getCoordsRightEdge()
	{
		static T coords_1d[] = {	-(T)std::sqrt(3.0/5.0), 0, (T)std::sqrt(3.0/5.0)	};

		static T coords_2d[] = {
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[0],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[1],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[2]
		};

		return coords_2d;
	}

	static const T* getCoordsLeftEdge()
	{
		static T coords_1d[] = {	-(T)std::sqrt(3.0/5.0), 0, (T)std::sqrt(3.0/5.0)	};

		static T coords_2d[] = {
				(T)0.5 + (T)0.5*coords_1d[0],	0.0,
				(T)0.5 + (T)0.5*coords_1d[1],	0.0,
				(T)0.5 + (T)0.5*coords_1d[2],	0.0
		};

		return coords_2d;
	}

	static int getNumPoints()
	{
		return 3;
	}
};

/**
 * Degree 5
 */
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 5>	:
	CGaussQuadrature1D_TriangleEdge<T, 4>
{
};

/**
 * Degree 6
 * This is experimental. I thought I would need (at least) 4 quadrature points, but if I use 4
 * of them, I get the message, that an assert is not fulfilled.
 */
#if 1
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 6> :
	public CGaussQuadrature1D<T, 6>
{
public:
	static const T* getCoordsHypEdge()
	{
		static const T *coords_1d = CGaussQuadrature1D<T,6>::getPositions();

		static const T coords_2d[] = {
				(T)0.5 - (T)0.5*coords_1d[0], (T)0.5 + (T)0.5*coords_1d[0],
				(T)0.5 - (T)0.5*coords_1d[1], (T)0.5 + (T)0.5*coords_1d[1],
				(T)0.5 - (T)0.5*coords_1d[2], (T)0.5 + (T)0.5*coords_1d[2],
				(T)0.5 - (T)0.5*coords_1d[3], (T)0.5 + (T)0.5*coords_1d[3],
		};

		return coords_2d;
	}

	static const T* getCoordsRightEdge()
	{
		static T coords_1d[] = {
				std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
		};

		static T coords_2d[] = {
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[0],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[1],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[2],
				(T)0.0, (T)0.5 - (T)0.5*coords_1d[3]
		};

		return coords_2d;
	}

	static const T* getCoordsLeftEdge()
	{
		static T coords_1d[] = {
				std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
		};

		static T coords_2d[] = {
				(T)0.5 + (T)0.5*coords_1d[0],	0.0,
				(T)0.5 + (T)0.5*coords_1d[1],	0.0,
				(T)0.5 + (T)0.5*coords_1d[2],	0.0,
				(T)0.5 + (T)0.5*coords_1d[3],	0.0
		};

		return coords_2d;
	}

	static int getNumPoints()
	{
		return 4;
	}
};
#else
template <typename T>
class CGaussQuadrature1D_TriangleEdge<T, 6>	:
	public CGaussQuadrature1D_TriangleEdge<T, 4>
{
};
#endif
}
}

#endif /* CGAUSSQUADRATURE_HPP_ */
