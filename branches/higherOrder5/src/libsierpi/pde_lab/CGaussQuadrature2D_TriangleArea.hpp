/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGAUSSQUADRATURE_2D_TRIANGLE_AREA_HPP_
#define CGAUSSQUADRATURE_2D_TRIANGLE_AREA_HPP_


namespace sierpi
{
namespace pdelab
{


/**
 * order of gaussian quadrature is equal to the number of sampling points used for the quadrature
 *
 * degree specifies the polynomial degree for which this integration accurate.
 */

template <typename T, int t_degree>
class CGaussQuadrature2D_TriangleArea
{
	static inline T* getCoords();
	static inline T* getWeights();
	static int getNumCoords();
};



/*
 * weights+positions: see e. g.
 *
 * Quadrature Formulas in Two Dimensions
 * Math 5172 - Finite Element Method
 * Section 001, Spring 2010
 *
 * shaodeng
 */

/**
 * Degree 0
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 0>
{
public:
	static T* getCoords()
	{
		static T values[] = {
				1.0/3.0, 1.0/3.0,
		};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {1.0/2.0};
		return values;
	}

	static int getNumCoords()
	{
		return 1;
	}
};



/**
 * Degree 1
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 1>	:
	public CGaussQuadrature2D_TriangleArea<T, 0>
{
};



/**
 * Degree 2
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 2>
{
public:
	static T* getCoords()
	{
		static T values[] = {
				1.0/2.0,	0.0,
				0.0,		1.0/2.0,
				1.0/2.0,	1.0/2.0
		};
		return values;
	}

	/*
	 * alternative integration points:
	 *
	 * {
	 * 	 {2/3, 1/6},
	 * 	 {1/6, 1/6},
	 * 	 {1/6, 2/4}
	 * }
	 *
	 * with weights w_i = 1/3
	 */

	static T* getWeights()
	{
		static T values[] = {
				1.0/6.0,
				1.0/6.0,
				1.0/6.0
		};
		return values;
	}

	static int getNumCoords()
	{
		return 3;
	}
};


/**
 * Degree 3
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 3>
{
public:
	static T* getCoords()
	{
		static T values[] = {
				1.0/3.0,	1.0/3.0,
				3.0/5.0,	1.0/5.0,
				1.0/5.0,	1.0/5.0,
				1.0/5.0,	3.0/5.0,
		};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {
				-27.0/96.0,
				25.0/96.0,
				25.0/96.0,
				25.0/96.0
		};
		return values;
	}

	static int getNumCoords()
	{
		return 4;
	}
};

/**
 * Degree 4
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 4>
{
public:
	static T* getCoords()
	{
		static T values[] = {
				0.44594849091597, 0.44594849091597,
				0.44594849091597, 0.10810301816807,
				0.10810301816807, 0.44594849091597,
				0.09157621350977, 0.09157621350977,
				0.09157621350977, 0.81684757298046,
				0.81684757298046, 0.09157621350977,
		};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {
				0.22338158967801/2.,
				0.22338158967801/2.,
				0.22338158967801/2.,
				0.10995174365532/2.,
				0.10995174365532/2.,
				0.10995174365532/2.,
		};
		return values;
	}

	static int getNumCoords()
	{
		return 6;
	}
};

/**
 * Degree 6
 */
template <typename T>
class CGaussQuadrature2D_TriangleArea<T, 6>
{
public:
	static T* getCoords()
	{
		static T values[] = {
				0.24928674517091, 0.24928674517091,
				0.24928674517091, 0.50142650965818,
				0.50142650965818, 0.24928674517091,
				0.06308901449150, 0.06308901449150,
				0.06308901449150, 0.87382197101700,
				0.87382197101700, 0.06308901449150,
				0.31035245103378, 0.63650249912140,
				0.63650249912140, 0.05314504984482,
				0.05314504984482, 0.31035245103378,
				0.63650249912140, 0.31035245103378,
				0.31035245103378, 0.05314504984482,
				0.05314504984482, 0.63650249912140,
		};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {
				0.11678627572638/2.,
				0.11678627572638/2.,
				0.11678627572638/2.,
				0.05084490637021/2.,
				0.05084490637021/2.,
				0.05084490637021/2.,
				0.08285107561837/2.,
				0.08285107561837/2.,
				0.08285107561837/2.,
				0.08285107561837/2.,
				0.08285107561837/2.,
				0.08285107561837/2.,
		};
		return values;
	}

	static int getNumCoords()
	{
		return 12;
	}
};

}
}


#endif
