/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGAUSSQUADRATURE_1D_HPP_
#define CGAUSSQUADRATURE_1D_HPP_


namespace sierpi
{
namespace pdelab
{



/*
 * weights+positions: see http://de.wikipedia.org/wiki/Gau%C3%9F-Quadratur
 *
 * Gaussian quadrature using N points can provide an accuracy for polynomials of degree up to 2N-1
 */

template <typename T, int t_desired_accuracy_order>
class CGaussQuadrature1D
{
public:
	static inline const T* getPositions();
	static inline const T* getWeights();

	static int getNumPoints();

	static T getIntegrationLength()
	{
		return 2.0;
	}
};



/**
 * Degree 0
 */
template <typename T>
class CGaussQuadrature1D<T, 0>
{
public:
	static inline const T* getPositions()
	{
		static const T values[] = {0.5};
		return values;
	}

	static inline const T* getWeights()
	{
		static const T values[] = {2.0};
		return values;
	}

	static int getNumPoints()
	{
		return 1;
	}
};



/**
 * Degree 1
 */
template <typename T>
class CGaussQuadrature1D<T, 1>	:
	public CGaussQuadrature1D<T, 0>
{
};



/**
 * Degree 2
 */
template <typename T>
class CGaussQuadrature1D<T, 2>
{
public:
	static inline const T* getPositions()
	{
		static const T values[] = {-std::sqrt(1.0/3.0), std::sqrt(1.0/3.0)};
		return values;
	}

	static inline const T* getWeights()
	{
		static const T values[] = {1.0, 1.0};
		return values;
	}

	static int getNumPoints()
	{
		return 2;
	}
};



/**
 * Degree 3
 */
template <typename T>
class CGaussQuadrature1D<T, 3>	:
	public CGaussQuadrature1D<T, 2>
{
};



/**
 * Degree 4
 */
template <typename T>
class CGaussQuadrature1D<T, 4>
{
public:
	static T* getPositions()
	{
		static T values[] = {-std::sqrt(3.0/5.0), 0, std::sqrt(3.0/5.0)};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {5.0/9.0, 8.0/9.0, 5.0/9.0};
		return values;
	}

	static int getNumPoints()
	{
		return 3;
	}
};



/**
 * Degree 5
 */
template <typename T>
class CGaussQuadrature1D<T, 5>	:
	public CGaussQuadrature1D<T, 4>
{
};

/**
 * Degree 6
 */
template <typename T>
class CGaussQuadrature1D<T, 6>
{
public:
	static T* getPositions()
	{
		static T values[] = {
				std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.-2.*std::sqrt(6./5.))/7.),
				std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
				-std::sqrt((3.+2.*std::sqrt(6./5.))/7.),
		};
		return values;
	}

	static T* getWeights()
	{
		static T values[] = {
				(18.+std::sqrt(30.))/36. ,
				(18.+std::sqrt(30.))/36. ,
				(18.-std::sqrt(30.))/36. ,
				(18.-std::sqrt(30.))/36. ,
		};
		return values;
	}

	static int getNumPoints()
	{
		return 4;
	}
};

/**
 * Degree 7
 */
template <typename T>
class CGaussQuadrature1D<T, 7>	:
	public CGaussQuadrature1D<T, 6>
{
};

}
}


#endif
