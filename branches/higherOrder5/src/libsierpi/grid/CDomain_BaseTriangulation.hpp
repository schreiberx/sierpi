/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CDomain_BaseTriangulation.hpp
 *
 *  Created on: Mar 31, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 *
 * 2012-06-05:
 *   implemented splitBaseTriangulation()
 *   Mueller Michael <muellmic@in.tum.de>
 */

#ifndef CDOMAIN_BASE_TRIANGULATION_HPP_
#define CDOMAIN_BASE_TRIANGULATION_HPP_


#include <list>
#include <string>
#include <stdint.h>
#include <cmath>
#include "libmath/CRegion2D.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "libsierpi/grid/CDomain_BaseTriangle.hpp"
#include "libsierpi/cluster/CCluster_EdgeComm_InformationAdjacentClusters.hpp"


namespace sierpi
{

/**
 * class storing all domain root triangles to assemble a valid domain built upon triangles
 */
template <typename CSimulation_Cluster_>
class CDomain_BaseTriangulation
{
public:
	/*
	 * type abbreviations for convenience
	 */
	typedef CDomain_BaseTriangle<CSimulation_Cluster_> CDomain_BaseTriangle_;

	typedef CCluster_TreeNode<CSimulation_Cluster_> CCluster_;
	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_> CEdgeComm_InformationAdjacentCluster_;

	typedef CTriangle_Factory::T T;

	/**
	 * fake a tree node which does not store any data and which is only useful for the traversals
	 */
	typedef CGenericTreeNode<CCluster_TreeNode<CSimulation_Cluster_> > CGenericTree_;


	/**
	 * fix applied for odd initial depth
	 */
public:
	bool oddInitialDepthFixApplied;


	/**
	 * number of catheti for current split to get unit length
	 */
public:
	T length_of_catheti_for_unit_domain;


public:
	/**
	 * information about base triangulation
	 */
	std::string information_string;

	/**
	 * used rectangular domain region
	 */
public:
	CRegion2D<CTriangle_Factory::T> region;


	/**
	 * can this triangulation be used for a distributed memory communication?
	 */
public:
	bool valid_distributed_memory_triangulation;


	/**
	 * storage for all base domain triangles
	 */
public:
	std::list<CDomain_BaseTriangle_ > cDomainBase_Triangle_List;



	/**
	 * constructor
	 */
public:
	CDomain_BaseTriangulation()
	{
		clear();
	}



	/**
	 * deconstructor
	 */
public:
	~CDomain_BaseTriangulation()
	{
		clear();
	}



	/**
	 * clear and free everything
	 */
public:
	void clear()
	{
		oddInitialDepthFixApplied = false;
		region.reset();
		cDomainBase_Triangle_List.clear();

		length_of_catheti_for_unit_domain = 0;
		information_string = "";
		valid_distributed_memory_triangulation = false;
	}



	/**
	 * this method removes those base triangles which create invalid
	 *
	 * edge communication information due to inconsistent hanging nodes
	 */
public:
	void removeInvalidCluster()
	{
		int evenCounter = 0;
		int oddCounter = 0;

		for (auto iter = cDomainBase_Triangle_List.begin(); iter != cDomainBase_Triangle_List.end(); iter++)
		{
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			if (cDomain_BaseTriangle.cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
				evenCounter++;

			if (cDomain_BaseTriangle.cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
				oddCounter++;
		}

		oddInitialDepthFixApplied = true;

		if (evenCounter == 0 || oddCounter == 0)
		{
			// no fix necessary
			return;
		}

		std::cerr << "WARNING: REMOVING ROOT TRIANGLES TO GET CONSISTENT EDGE COMM INFORMATION" << std::endl;

		if (evenCounter < oddCounter)
		{
			for (auto iter = cDomainBase_Triangle_List.begin(); iter != cDomainBase_Triangle_List.end(); /* no iter */ )
			{
				CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

				if (cDomain_BaseTriangle.cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
				{
					iter = cDomainBase_Triangle_List.erase(iter);
					continue;
				}
				iter++;
			}
		}
		else
		{
			for (auto iter = cDomainBase_Triangle_List.begin(); iter != cDomainBase_Triangle_List.end(); /* no iter */ )
			{
				CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

				if (cDomain_BaseTriangle.cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
				{
					iter = cDomainBase_Triangle_List.erase(iter);
					continue;
				}
				iter++;
			}
		}
	}



	/**
	 * insert a new triangle into the domain grid
	 *
	 * this implementation is quite slow and should be called only during initialization!!!
	 */
public:
	CDomain_BaseTriangle_ *insert_DomainTriangle(
			CTriangle_Factory &i_new_triangle_factory	///< factory for new triangle
	)	{
		bool i_auto_setup = true;

		// insert root triangle to list
		cDomainBase_Triangle_List.push_back(CDomain_BaseTriangle_(i_new_triangle_factory));

		// extend maximum border
		region.extend(i_new_triangle_factory.vertices[0][0], i_new_triangle_factory.vertices[0][1]);
		region.extend(i_new_triangle_factory.vertices[1][0], i_new_triangle_factory.vertices[1][1]);
		region.extend(i_new_triangle_factory.vertices[2][0], i_new_triangle_factory.vertices[2][1]);

		CDomain_BaseTriangle_ &cDomain_BaseTriangle = cDomainBase_Triangle_List.back();

		if (!i_auto_setup)
			return &cDomain_BaseTriangle;

		// load new triangle factory and setup invalid values to valid values
		CTriangle_Factory &cTriangleFactory = cDomain_BaseTriangle.cTriangleFactory;

		/*
		 * setup edge types to BORDER when not yet set
		 */
		if (cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_INVALID)
			cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_INVALID)
			cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_INVALID)
			cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		if (cTriangleFactory.triangleType == CTriangle_Enums::TRIANGLE_TYPE_INVALID)
			cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		if (cTriangleFactory.traversalDirection == CTriangle_Enums::DIRECTION_INVALID)
			cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;

		if (cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_INVALID)
			cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;


		/*
		 * setup normal
		 */
		if (cTriangleFactory.hypNormal == CTriangle_Enums::NORMAL_INVALID)
		{
			// compute the normal direction - x and y are exchanged!
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_y = -(cTriangleFactory.vertices[1][0] - cTriangleFactory.vertices[0][0]);
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_x = cTriangleFactory.vertices[1][1] - cTriangleFactory.vertices[0][1];

			cTriangleFactory.hypNormal = cTriangleFactory.getTriangleHypNormalIdx(hyp_vec_x, hyp_vec_y);
		}


		/*
		 * setup even/odd value
		 *
		 * normals on the hypotenuse which are aligned at cartesian grids are initialized
		 * to `even`, otherwise `odd`
		 *
		 * this also reduces the amount of the recursive function jump table for the sierpi traversators!!!
		 */
		if (cTriangleFactory.evenOdd == CTriangle_Enums::EVEN_ODD_INVALID)
		{
			if ((cTriangleFactory.hypNormal & 1) == 0)
				cTriangleFactory.evenOdd = CTriangle_Enums::ODD;
			else
				cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		}

#if DEBUG
		/*
		 * check whether the manual evenOdd initialization is valid
		 */
		if (cTriangleFactory.hypNormal != CTriangle_Enums::NORMAL_INVALID)
		{
			if (cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
				assert((cTriangleFactory.hypNormal & 1) == 1);
			if (cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
				assert((cTriangleFactory.hypNormal & 1) == 0);
		}
#endif

		return &cDomain_BaseTriangle;
	}



	/**
	 * scale all vertices with given scalar value
	 */
public:
	void multiplyVertexCoordsWithScalar(
			CTriangle_Factory::T scalar = (CTriangle_Factory::T)1.0
	)
	{
		for (	auto iter = cDomainBase_Triangle_List.begin();
				iter != cDomainBase_Triangle_List.end();
				iter++
		)
		{
			for (int i = 0; i < 3; i++)
			{
				iter->cTriangleFactory.vertices[i][0] *= scalar;
				iter->cTriangleFactory.vertices[i][1] *= scalar;
			}
		}

		/*
		 * also scale region border
		 */
		region.scale(scalar);
	}



	/**
	 * check if 2 Edges coincide
	 *
	 * return 1 if the edge vertices coincide
	 * return -1 if the coincide by exchanging the vertices of one edge
	 * return 0 if they don't coincide
	 */
private:
	int _isSameEdge(
			CTriangle_Factory::T a1[2], CTriangle_Factory::T a2[2],	// 2 vertices for edge A
			CTriangle_Factory::T b1[2], CTriangle_Factory::T b2[2]	// 2 vertices for edge B
	)
	{
		if (	a1[0] ==  b1[0]	&&	a1[1] ==  b1[1]	&&
				a2[0] ==  b2[0]	&&	a2[1] ==  b2[1]
		)
		{
			return 1;
		}

		// check with exchanged vertices for one edge
		if (	a1[0] ==  b2[0]	&&	a1[1] ==  b2[1]	&&
				a2[0] ==  b1[0]	&&	a2[1] ==  b1[1]
		)
		{
			return -1;
		}

		return 0;
	}



	/**
	 * check for adjacent triangles
	 *
	 * the check is based on the vertex coordinates.
	 *
	 * the following conventions exist:
	 *
	 * when rotating the triangle until the hypotenuse is aligned at the x-axis,
	 * the vertex#0 is on the left most corner, vertex#1 on the right most corner
	 * and vertex#2 on the top most corner.
	 *
	 * then, the returned edge index can be interpreted as:
	 * 	0: hypotenuse
	 * 	1: right edge
	 * 	2: left edge
	 *
	 * if the vertices have to be exchanged, the direction of both triangles is equal!!!
	 */
private:
	bool _isAdjacent(	CTriangle_Factory::TTriangleFactoryScalarType i_vertices1[3][2],	//< vertices of the first triangle
						CTriangle_Factory::TTriangleFactoryScalarType i_vertices2[3][2],	//< vertices of the second triangle
						CTriangle_Enums::EEdgeEnum *o_adj_edge_idx1,	///< adjacent edge of first triangle
						CTriangle_Enums::EEdgeEnum *o_adj_edge_idx2,	///< adjacent edge of second triangle
						bool *o_equal_traversal_direction			///< true if traversal direction is equal
					)
	{
		for (int i1 = 0; i1 < 3; i1++)
		{
			for (int i2 = 0; i2 < 3; i2++)
			{
				int d = _isSameEdge(
							i_vertices1[i1], i_vertices1[(i1+1)%3],
							i_vertices2[i2], i_vertices2[(i2+1)%3]
						);
				if (d != 0)
				{
					*o_adj_edge_idx1 = (CTriangle_Enums::EEdgeEnum)i1;
					*o_adj_edge_idx2 = (CTriangle_Enums::EEdgeEnum)i2;
					*o_equal_traversal_direction = (d == -1);
					return true;
				}
			}
		}
		return false;
	}


#if CONFIG_ENABLE_MPI

public:
	/**
	 * setup the mpi node enumeration
	 */
	void setup_MPINodeEnumeration(
			int i_mpi_rank,
			int i_mpi_size
	)
	{
		size_t size = cDomainBase_Triangle_List.size();

		size_t base_triangles_per_node = size / i_mpi_size;

		if (base_triangles_per_node*i_mpi_size != size)
			base_triangles_per_node++;

		size_t c = 0;
		for (	auto iter = cDomainBase_Triangle_List.begin();
				iter != cDomainBase_Triangle_List.end();
				iter++
		)
		{
			// load currentTriangleWithCluster
			CDomain_BaseTriangle_ &currentTriangleWithCluster = *iter;

			currentTriangleWithCluster.mpi_rank = c / base_triangles_per_node;

			c++;
		}
	}

#endif

	/**
	 * assemble the setup triangle to a triangle grid and setup the simulation cluster handlers!
	 *
	 * \return number of triangles in whole grid
	 */
public:
	void setup_AdjacencyInformation()
	{
		for (	auto iter = cDomainBase_Triangle_List.begin();
				iter != cDomainBase_Triangle_List.end();
				iter++
		)
		{
			// load currentTriangleWithCluster
			CDomain_BaseTriangle_ &currentTriangleWithCluster = *iter;

			/*
			 * if the vertices are valid, we search for an adjacent triangle for all borders
			 * which are not of type EDGE_TYPE_BOUNDARY
			 */
			for (	auto iter2 = cDomainBase_Triangle_List.begin();
					iter2 != cDomainBase_Triangle_List.end();
					iter2++
			)
			{
				CDomain_BaseTriangle_ &adjacentTriangle = *iter2;

				if(&adjacentTriangle == &currentTriangleWithCluster)
					continue;

				/*
				 * search for direct adjacent triangles
				 */
				CTriangle_Enums::EEdgeEnum adjEdgeIdxThisTriangle, adjEdgeIdxAdjacentTriangle;
				bool equal_traversal_direction;

				if (	_isAdjacent(
							currentTriangleWithCluster.cTriangleFactory.vertices,
							adjacentTriangle.cTriangleFactory.vertices,
							&adjEdgeIdxThisTriangle,			///< index of edge for this triangle
							&adjEdgeIdxAdjacentTriangle,		///< index of edge for adjacent triangle
							&equal_traversal_direction			///< true, if the triangles have the same traversal direction
				))
				{
					switch(adjEdgeIdxThisTriangle)
					{
					case CTriangle_Enums::EDGE_ENUM_HYP_EDGE:
						if (currentTriangleWithCluster.adjacent_triangles.hyp_edge == nullptr)
						{
							currentTriangleWithCluster.adjacent_triangles.hyp_edge = &adjacentTriangle;
							currentTriangleWithCluster.cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
						}

						break;

					case CTriangle_Enums::EDGE_ENUM_RIGHT_EDGE:
						if (currentTriangleWithCluster.adjacent_triangles.right_edge == nullptr)
						{
							currentTriangleWithCluster.adjacent_triangles.right_edge = &adjacentTriangle;
							currentTriangleWithCluster.cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
						}
						break;

					case CTriangle_Enums::EDGE_ENUM_LEFT_EDGE:
						if (currentTriangleWithCluster.adjacent_triangles.left_edge == nullptr)
						{
							currentTriangleWithCluster.adjacent_triangles.left_edge = &adjacentTriangle;
							currentTriangleWithCluster.cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
						}
						break;
					}
				}
			}
		}
	}


	/**
	 * setup all default values for a triangle factory
	 * these are:
	 * - edge types
	 * - triangle type
	 * - traversal direction
	 * - cluster tree node type
	 * - normal
	 * - odd/even value
	 */
private:
	void setup_TriangleFactory(
			CTriangle_Factory &o_cTriangleFactory		///< triangle factory to setup
	){
		/*
		 * setup edge types to BORDER when not yet set
		 */
		if (o_cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_INVALID)
			o_cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (o_cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_INVALID)
			o_cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (o_cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_INVALID)
			o_cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		if (o_cTriangleFactory.triangleType == CTriangle_Enums::TRIANGLE_TYPE_INVALID)
			o_cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		if (o_cTriangleFactory.traversalDirection == CTriangle_Enums::DIRECTION_INVALID)
			o_cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;

		if (o_cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_INVALID)
			o_cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;


		/*
		 * setup normal
		 */
		if (o_cTriangleFactory.hypNormal == CTriangle_Enums::NORMAL_INVALID)
		{
			// compute the normal direction - x and y are exchanged!
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_y = -(o_cTriangleFactory.vertices[1][0] - o_cTriangleFactory.vertices[0][0]);
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_x = o_cTriangleFactory.vertices[1][1] - o_cTriangleFactory.vertices[0][1];

			o_cTriangleFactory.hypNormal = o_cTriangleFactory.getTriangleHypNormalIdx(hyp_vec_x, hyp_vec_y);
		}


		/*
		 * setup even/odd value
		 *
		 * normals on the hypotenuse which are aligned at cartesian grids are initialized
		 * to `even`, otherwise `odd`
		 *
		 * this also reduces the amount of the recursive function jump table for the sierpi traversators!!!
		 */
		if (o_cTriangleFactory.evenOdd == CTriangle_Enums::EVEN_ODD_INVALID)
		{
			if ((o_cTriangleFactory.hypNormal & 1) == 0)
				o_cTriangleFactory.evenOdd = CTriangle_Enums::ODD;
			else
				o_cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		}

#if DEBUG
		/*
		 * check whether the manual evenOdd initialization is valid
		 */
		if (o_cTriangleFactory.hypNormal != CTriangle_Enums::NORMAL_INVALID)
		{
			if (o_cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
				assert((o_cTriangleFactory.hypNormal & 1) == 1);
			if (o_cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
				assert((o_cTriangleFactory.hypNormal & 1) == 0);
		}
#endif
	}


public:
	/**
	 * \brief split the base triangulation for improved load balancing over mpi nodes
	 *
	 */
	void splitBaseTriangulation(
			int i_initial_cluster_splittings
	)
	{
		if (i_initial_cluster_splittings == 0)
			return;

//		std::cout << "split base triangle to i_depth: " << i_depth << std::endl;
/*
		for (	auto iter = domainRootTriangles.begin();
				iter != domainRootTriangles.end();
				iter++
		){
			CDomain_BaseTriangle_ &triangle = *iter;
			std::cout << "before i_depth 0: " << std::endl;
			std::cout << "\t triangle: " << &triangle << std::endl;
			std::cout << "\t left: " << triangle.adjacent_triangles.left_edge << std::endl;
			std::cout << "\t right: " << triangle.adjacent_triangles.right_edge << std::endl;
			std::cout << "\t hyp: " << triangle.adjacent_triangles.hyp_edge << std::endl;
			std::cout << "" << std::endl;
		}
*/
		for(int i = 0; i < i_initial_cluster_splittings; i++){
			std::list<CDomain_BaseTriangle_> subDomainRootTriangles;

			for (	auto iter = cDomainBase_Triangle_List.begin();
					iter != cDomainBase_Triangle_List.end();
					iter++
			){
				CDomain_BaseTriangle_ &mainTriangle = *iter;

				CTriangle_Factory rightSubTriangleFactory;
				CTriangle_Factory leftSubTriangleFactory;

				// split triangle
				rightSubTriangleFactory.vertices[0][0] = mainTriangle.cTriangleFactory.vertices[1][0];
				rightSubTriangleFactory.vertices[0][1] = mainTriangle.cTriangleFactory.vertices[1][1];

				rightSubTriangleFactory.vertices[1][0] = mainTriangle.cTriangleFactory.vertices[2][0];
				rightSubTriangleFactory.vertices[1][1] = mainTriangle.cTriangleFactory.vertices[2][1];

				rightSubTriangleFactory.vertices[2][0] = (mainTriangle.cTriangleFactory.vertices[0][0] + mainTriangle.cTriangleFactory.vertices[1][0]) * (T)0.5;
				rightSubTriangleFactory.vertices[2][1] = (mainTriangle.cTriangleFactory.vertices[0][1] + mainTriangle.cTriangleFactory.vertices[1][1]) * (T)0.5;

				leftSubTriangleFactory.vertices[0][0] = mainTriangle.cTriangleFactory.vertices[2][0];
				leftSubTriangleFactory.vertices[0][1] = mainTriangle.cTriangleFactory.vertices[2][1];

				leftSubTriangleFactory.vertices[1][0] = mainTriangle.cTriangleFactory.vertices[0][0];
				leftSubTriangleFactory.vertices[1][1] = mainTriangle.cTriangleFactory.vertices[0][1];

				leftSubTriangleFactory.vertices[2][0] = (mainTriangle.cTriangleFactory.vertices[0][0] + mainTriangle.cTriangleFactory.vertices[1][0]) * (T)0.5;
				leftSubTriangleFactory.vertices[2][1] = (mainTriangle.cTriangleFactory.vertices[0][1] + mainTriangle.cTriangleFactory.vertices[1][1]) * (T)0.5;

				// setup edge_type, triangle_type, traversal direction,
				// cluster tree node type, normal and even/odd
				setup_TriangleFactory(rightSubTriangleFactory);
				setup_TriangleFactory(leftSubTriangleFactory);

				/*
				 * add children to list
				 * push left triangle first to care about the SFC order which is necessary for MPI!
				 */
				CDomain_BaseTriangle_ *rightSubTriangle, *leftSubTriangle;

				if (i & 1)
				{
					subDomainRootTriangles.push_back(CDomain_BaseTriangle_(leftSubTriangleFactory));
					leftSubTriangle = &(subDomainRootTriangles.back());

					subDomainRootTriangles.push_back(CDomain_BaseTriangle_(rightSubTriangleFactory));
					rightSubTriangle = &(subDomainRootTriangles.back());
				}
				else
				{
					subDomainRootTriangles.push_back(CDomain_BaseTriangle_(rightSubTriangleFactory));
					rightSubTriangle = &(subDomainRootTriangles.back());

					subDomainRootTriangles.push_back(CDomain_BaseTriangle_(leftSubTriangleFactory));
					leftSubTriangle = &(subDomainRootTriangles.back());
				}


				// connect subtriangles with parent
				rightSubTriangle->parent = &mainTriangle;
				rightSubTriangle->leftChild = nullptr;
				rightSubTriangle->rightChild = nullptr;
				leftSubTriangle->parent = &mainTriangle;
				leftSubTriangle->leftChild = nullptr;
				leftSubTriangle->rightChild = nullptr;
				mainTriangle.leftChild = leftSubTriangle;
				mainTriangle.rightChild = rightSubTriangle;


/*
				std::cout << "i_depth: " << i << std::endl;
				std::cout << "\t triangle: " << &mainTriangle << std::endl;
				std::cout << "\t left child: " << mainTriangle.leftChild << std::endl;
				std::cout << "\t right child: " << mainTriangle.rightChild << std::endl;
				std::cout << "\t parent: " << mainTriangle.parent << std::endl;


				std::cout << "" << std::endl;
*/
			}


			for (	auto iter = subDomainRootTriangles.begin();
					iter != subDomainRootTriangles.end();
					iter++
			){

				CDomain_BaseTriangle_ &subTriangle = *iter;

				CDomain_BaseTriangle_ &parent = *(subTriangle.parent);

				// connect with sibling
				if(&subTriangle == parent.leftChild){
					subTriangle.setupLeftEdgeConnection(parent.rightChild);
				}else if(&subTriangle == parent.rightChild){
					subTriangle.setupRightEdgeConnection(parent.leftChild);
				}else{
					std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
				}

				// connect with neighbors
				if(parent.adjacent_triangles.left_edge != nullptr){
					CDomain_BaseTriangle_ &leftNeighbour = *parent.adjacent_triangles.left_edge;
					if(&parent == leftNeighbour.adjacent_triangles.right_edge){
						if(&subTriangle == parent.leftChild){
							subTriangle.setupHypEdgeConnection(leftNeighbour.rightChild);
						}else if(&subTriangle == parent.rightChild){
							// do nothing
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}else if(&parent == leftNeighbour.adjacent_triangles.left_edge){
						if(&subTriangle == parent.leftChild){
							subTriangle.setupHypEdgeConnection(leftNeighbour.leftChild);
						}else if(&subTriangle == parent.rightChild){
							// do nothing
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}
				}

				if(parent.adjacent_triangles.right_edge != nullptr){
					CDomain_BaseTriangle_ &rightNeighbour = *parent.adjacent_triangles.right_edge;
					if(&parent == rightNeighbour.adjacent_triangles.left_edge){
						if(&subTriangle == parent.leftChild){
							// do nothing
						}else if(&subTriangle == parent.rightChild){
							subTriangle.setupHypEdgeConnection(rightNeighbour.leftChild);
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}else if(&parent == rightNeighbour.adjacent_triangles.right_edge){
						if(&subTriangle == parent.leftChild){
							// do nothing
						}else if(&subTriangle == parent.rightChild){
							subTriangle.setupHypEdgeConnection(rightNeighbour.rightChild);
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}
				}

				if(parent.adjacent_triangles.hyp_edge != nullptr){
					CDomain_BaseTriangle_ &hypNeighbour = *parent.adjacent_triangles.hyp_edge;
					if(&subTriangle == parent.leftChild){
						subTriangle.setupRightEdgeConnection(hypNeighbour.rightChild);
					}else if(&subTriangle == parent.rightChild){
						subTriangle.setupLeftEdgeConnection(hypNeighbour.leftChild);
					}else{
						std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
					}
				}
/*
				std::cout << "after i_depth: " << i << std::endl;
				std::cout << "\t triangle: " << &subTriangle << std::endl;
				std::cout << "\t left: " << subTriangle.adjacent_triangles.left_edge << std::endl;
				std::cout << "\t right: " << subTriangle.adjacent_triangles.right_edge << std::endl;
				std::cout << "\t hyp: " << subTriangle.adjacent_triangles.hyp_edge << std::endl;
				std::cout << "" << std::endl;
*/
			}

			// reset triangle list
			cDomainBase_Triangle_List.swap(subDomainRootTriangles);
			subDomainRootTriangles.clear();
		}

		/*
		 * fix catheti base triangulation length
		 */
		length_of_catheti_for_unit_domain /= (T)(1 << (i_initial_cluster_splittings >> 1));

		if (i_initial_cluster_splittings & 1)
			length_of_catheti_for_unit_domain *= (T)1.0 / (T)M_SQRT2;

		std::cout << length_of_catheti_for_unit_domain << std::endl;
	}


	/**
	 * scale and translate the base triangulation to fit into the
	 * unit square [-0.5]^2 x [0.5]^2
	 */
	void scaleAndTranslateToUnitSquare()
	{
		T center_x = (T)0.5*(region.x_max + region.x_min);
		T center_y = (T)0.5*(region.y_max + region.y_min);

		T scale_x = (T)1.0/region.getSizeX();
		T scale_y = (T)1.0/region.getSizeY();

		// keep ratio
		T scale = std::min(scale_x, scale_y);

		for (	typename std::list<CDomain_BaseTriangle_>::iterator iter = cDomainBase_Triangle_List.begin();
				iter != cDomainBase_Triangle_List.end();
				iter++
		)	{
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			for (int i = 0; i < 3; i++)
			{
				cDomain_BaseTriangle.cTriangleFactory.vertices[i][0] = (cDomain_BaseTriangle.cTriangleFactory.vertices[i][0]-center_x)*scale;
				cDomain_BaseTriangle.cTriangleFactory.vertices[i][1] = (cDomain_BaseTriangle.cTriangleFactory.vertices[i][1]-center_y)*scale;
			}
		}

		region.x_min = -0.5;
		region.y_min = -0.5;

		region.x_max = 0.5;
		region.y_max = 0.5;
	}


	/**
	 * scale and translate
	 */
	void scaleAndTranslate(
			T i_center_x,
			T i_center_y,
			T i_scale_x,
			T i_scale_y
	)
	{
		if (i_scale_x != i_scale_y)
		{
			std::cerr << "Scaling factors in X (" << i_scale_x << ") and Y (" << i_scale_y << ") directions have to be equal!" << std::endl;
			assert(false);
		}

		for (	typename std::list<CDomain_BaseTriangle_>::iterator iter = cDomainBase_Triangle_List.begin();
				iter != cDomainBase_Triangle_List.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			for (int i = 0; i < 3; i++)
			{
				cDomain_BaseTriangle.cTriangleFactory.vertices[i][0] = (cDomain_BaseTriangle.cTriangleFactory.vertices[i][0]*i_scale_x)+i_center_x;
				cDomain_BaseTriangle.cTriangleFactory.vertices[i][1] = (cDomain_BaseTriangle.cTriangleFactory.vertices[i][1]*i_scale_y)+i_center_y;
			}
		}
	}
};

}

#endif /* CTRIANGLE_GLUE_H_ */
