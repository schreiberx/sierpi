/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CDomain_BaseTriangulationSetup.hpp
 *
 *  Created on: Jun 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDOMAIN_BASETRIANGULATIONSETUP_HPP_
#define CDOMAIN_BASETRIANGULATIONSETUP_HPP_

#include "lib/CTriangulationFromSVG.hpp"
#include "libsierpi/grid/CDomain_BaseTriangulation.hpp"

#define DOMAIN_FILE_PATH	"data/domains/"


namespace sierpi
{

class CDomain_BaseTriangulationSetup
{
	typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_SingleTriangle(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)
	{
		o_cDomain_BaseTriangulation->information_string = "SingleTriangle";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 1.0;
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_QuadWith2Triangles(CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation)
	{
		o_cDomain_BaseTriangulation->information_string = "QuadWith2Triangles";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 1.0;
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_Strip_PeriodicBoundaries(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation,
			int i_strip_size = 1
		)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		o_cDomain_BaseTriangulation->information_string = "Strip";
#else
		o_cDomain_BaseTriangulation->information_string = "Strip_PeriodicBoundaries";
#endif

		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;

#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		int initial_displacement = -i_strip_size+1;


		/*
		 * !!! We have to use 4 triangles per column, to avoid adjacent triangle search problems for edge comm data exchange !!!
		 */
		assert(i_strip_size <= 1024);

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[4];
#endif

		int traversal_type = 0;

		/*
		 * different traversal types following the SFC
		 *  ___   ___
		 * |\3/| |\6/|
		 * |2+4| |5+7|
		 * |/1\| |/8\|
		 *  ---   ---
		 *   0     1   <--- type
		 */
		for (int i = 0; i < i_strip_size; i++)
		{
			if (traversal_type == 0)
			{
				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[0] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[1] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2]->adjacent_triangles.hyp_edge = baseTriangles[0];
				baseTriangles[2]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
				baseTriangles[0]->adjacent_triangles.hyp_edge = baseTriangles[2];
				baseTriangles[0]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

			}
			else
			{

				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[0] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[1] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3]->adjacent_triangles.hyp_edge = baseTriangles[1];
				baseTriangles[3]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
				baseTriangles[1]->adjacent_triangles.hyp_edge = baseTriangles[3];
				baseTriangles[1]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif
			}

			initial_displacement += 2;
			traversal_type = (traversal_type+1) & 1;
		}

		o_cDomain_BaseTriangulation->multiplyVertexCoordsWithScalar((T)1.0/(T)i_strip_size);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = (T)1.0/(std::sqrt((T)2.0)*(T)i_strip_size);
	}


	/**
	 * setup world cube using periodic boundary conditions
	 *
	 *     __
	 *    | /|
	 *  __|/_|__ __
	 * | /|\ | /|\ |
	 * |/_|_\|/_|_\|
	 *    | /|
	 *    |/_|
	 *
	 *
	 *     __
	 *    |5/|
	 *  __|/6|__ __
	 * |4/|\7|8/|\b|
	 * |/3|2\|/9|a\|
	 *    |1/|
	 *    |/0|
	 *
	 */
private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_2DCube_PeriodicBoundaries(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		o_cDomain_BaseTriangulation->information_string = "2DCube";
#else
		o_cDomain_BaseTriangulation->information_string = "2DCube_PeriodicBoundaries";
#endif

		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;

#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[12];
#endif

		static T vertex_data[12][6] =
		{
				{	// 0
					1.0,	-1.0,
					-1.0,	-3.0,
					1.0,	-3.0
				},
				{	// 1
					-1.0,	-3.0,
					1.0,	-1.0,
					-1.0,	-1.0
				},
				{	// 2
					1.0,	-1.0,
					-1.0,	1.0,
					-1.0,	-1.0
				},
				{	// 3
					-1.0,	1.0,
					-3.0,	-1.0,
					-1.0,	-1.0
				},
				{	// 4
					-3.0,	-1.0,
					-1.0,	1.0,
					-3.0,	1.0
				},
				{	// 5
					-1.0,	1.0,
					1.0,	3.0,
					-1.0,	3.0
				},
				{	// 6
					1.0,	3.0,
					-1.0,	1.0,
					1.0,	1.0
				},
				{	// 7
					-1.0,	1.0,
					1.0,	-1.0,
					1.0,	1.0
				},
				{	// 8
					1.0,	-1.0,
					3.0,	1.0,
					1.0,	1.0
				},
				{	// 9
					3.0,	1.0,
					5.0,	-1.0,
					5.0,	1.0
				},
				{	// 10
					5.0,	-1.0,
					3.0,	1.0,
					3.0,	-1.0
				},
				{	// 11
					3.0,	1.0,
					1.0,	-1.0,
					3.0,	-1.0
				},
		};

		for (int i = 0; i < 12; i++)
		{
			template_rootTriangle.setVertices(
					vertex_data[i][0],
					vertex_data[i][1],
					vertex_data[i][2],
					vertex_data[i][3],
					vertex_data[i][4],
					vertex_data[i][5]
				);

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i] =
#endif
					o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);
		}


#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[0]->setupRightEdgeConnection(baseTriangles[10]);
		baseTriangles[0]->setupLeftEdgeConnection(baseTriangles[11]);

		baseTriangles[1]->setupLeftEdgeConnection(baseTriangles[3]);

		baseTriangles[3]->setupRightEdgeConnection(baseTriangles[1]);

		baseTriangles[4]->setupLeftEdgeConnection(baseTriangles[9]);
		baseTriangles[4]->setupRightEdgeConnection(baseTriangles[5]);

		baseTriangles[5]->setupLeftEdgeConnection(baseTriangles[4]);
		baseTriangles[5]->setupRightEdgeConnection(baseTriangles[9]);

		baseTriangles[6]->setupLeftEdgeConnection(baseTriangles[8]);
		baseTriangles[8]->setupRightEdgeConnection(baseTriangles[6]);

		baseTriangles[9]->setupLeftEdgeConnection(baseTriangles[5]);
		baseTriangles[9]->setupRightEdgeConnection(baseTriangles[4]);


		baseTriangles[10]->setupLeftEdgeConnection(baseTriangles[0]);

		baseTriangles[11]->setupRightEdgeConnection(baseTriangles[0]);
#endif

		o_cDomain_BaseTriangulation->multiplyVertexCoordsWithScalar((T)0.25);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 4.0;
	}



private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_Quad_PeriodicBoundaries(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		o_cDomain_BaseTriangulation->information_string = "Quad_Boundaries";
#else
		o_cDomain_BaseTriangulation->information_string = "Quad_PeriodicBoundaries";
#endif

		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;

#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "Domains with periodic boundaries not activated!" << std::endl;
#endif


#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[4];
#endif

		CTriangle_Factory template_rootTriangle;


		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[0] =
#endif
			o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[1] =
#endif
			o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;


#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[2] =
#endif
			o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[3] =
#endif
			o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[2]->adjacent_triangles.hyp_edge = baseTriangles[0];
		baseTriangles[2]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangles[0]->adjacent_triangles.hyp_edge = baseTriangles[2];
		baseTriangles[0]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

		baseTriangles[1]->adjacent_triangles.hyp_edge = baseTriangles[3];
		baseTriangles[1]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangles[3]->adjacent_triangles.hyp_edge = baseTriangles[1];
		baseTriangles[3]->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = (T)1.0/std::sqrt((T)2.0);
	}



private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_2OddTriangles1(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)	{
		o_cDomain_BaseTriangulation->information_string = "2OddTriangles1";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;


		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = (T)1.0/std::sqrt((T)2.0);
	}



private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_2OddTriangles2(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)	{
		o_cDomain_BaseTriangulation->information_string = "2OddTriangles2";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = true;


		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 0.0;
		template_rootTriangle.vertices[0][1] = 0.0;
		template_rootTriangle.vertices[1][0] = -2.0;
		template_rootTriangle.vertices[1][1] = 0.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = std::sqrt((T)(4.0/18.0));
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_2EvenTriangles1(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)	{
		o_cDomain_BaseTriangulation->information_string = "2EvenTriangles1";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = false;


		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 2.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -2.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = (T)1.0/(T)2.0;
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_2EvenTriangles2(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)
	{
		o_cDomain_BaseTriangulation->information_string = "2EvenTriangles2";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = false;

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 1.0;
	}

private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_3Triangles(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation
	)
	{
		o_cDomain_BaseTriangulation->information_string = "3Triangles";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = false;


		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -3.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = (T)0.5;
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_QuadTiles(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation,
			int tiles_x,
			int tiles_y
	)
	{
		o_cDomain_BaseTriangulation->information_string = "QuadTiles";
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = false;

		CTriangle_Factory template_rootTriangle;
		// TODO: care about machine number inaccuracy

		for (T x = -1; x <= (T)tiles_x; x += 2.0)
		{
			for (T y = -1; y <= (T)tiles_y; y += 2.0)
			{
				template_rootTriangle.vertices[0][0] = (T)1.0+x;
				template_rootTriangle.vertices[0][1] = -(T)1.0+y;
				template_rootTriangle.vertices[1][0] = -(T)1.0+x;
				template_rootTriangle.vertices[1][1] = (T)1.0+y;
				template_rootTriangle.vertices[2][0] = -(T)1.0+x;
				template_rootTriangle.vertices[2][1] = -(T)1.0+y;

				o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = -(T)1.0+x;
				template_rootTriangle.vertices[0][1] = (T)1.0+y;
				template_rootTriangle.vertices[1][0] = (T)1.0+x;
				template_rootTriangle.vertices[1][1] = -(T)1.0+y;
				template_rootTriangle.vertices[2][0] = (T)1.0+x;
				template_rootTriangle.vertices[2][1] = (T)1.0+y;

				o_cDomain_BaseTriangulation->insert_DomainTriangle(template_rootTriangle);
			}
		}

	    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 0.5;
	}


private:
	template <typename CDomain_BaseTriangulation_>
	static void p_setupTriangulation_SVG(
			CDomain_BaseTriangulation_ *o_cDomain_BaseTriangulation,
			const char* i_filename = DOMAIN_FILE_PATH"test_domain.svg"
	)
	{
		o_cDomain_BaseTriangulation->information_string = "SVG File: ";
		o_cDomain_BaseTriangulation->information_string += i_filename;
		o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation = false;


		CTriangulationFromSVG triangulationSVG;


		if (!triangulationSVG.loadSVGFile(i_filename))
		{
			std::cerr << "Failed to load SVG File " << i_filename << std::endl;
			return;
		}

	    for (	std::vector<CTriangulationFromSVG::CTriangle2D>::iterator iter = triangulationSVG.triangles.begin();
	    		iter != triangulationSVG.triangles.end();
	    		iter++
	    )
	   	{
			CTriangle_Factory rootTriangle;

			for (int i = 0; i < 3; i++)
	    	{
	    		rootTriangle.vertices[i][0] = iter->vertices[i][0];
	    		rootTriangle.vertices[i][1] = iter->vertices[i][1];
	    	}

			o_cDomain_BaseTriangulation->insert_DomainTriangle(rootTriangle);
	   	}

	    o_cDomain_BaseTriangulation->scaleAndTranslateToUnitSquare();

	    if (o_cDomain_BaseTriangulation->cDomainBase_Triangle_List.size() > 0)
	    {
	    	typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &b = o_cDomain_BaseTriangulation->cDomainBase_Triangle_List.front();

	    	T dx = b.cTriangleFactory.vertices[0][0] - b.cTriangleFactory.vertices[2][0];
	    	T dy = b.cTriangleFactory.vertices[0][1] - b.cTriangleFactory.vertices[2][1];

	    	T d = std::sqrt(dx*dx + dy*dy);

	    	dx = b.cTriangleFactory.vertices[0][0] - b.cTriangleFactory.vertices[1][0];
	    	dy = b.cTriangleFactory.vertices[0][1] - b.cTriangleFactory.vertices[1][1];

	    	d = std::max(d, std::sqrt(dx*dx + dy*dy))/std::sqrt((T)2.0);

		    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = d;
	    }
	    else
	    {
		    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 0;
	    }
	}



public:
	template <typename CSimulation_Cluster_>
	static void setupBaseTriangulation(
			int i_world_scene_id,			///< base triangulation to set-up
			CDomain_BaseTriangulation<CSimulation_Cluster_> *o_cDomain_BaseTriangulation,		///< output for base triangulation
			int i_verbosity_level,					///< be a little bit verbose
			int i_initial_cluster_splittings = 0,		///< initial number of cluster splittings
			int i_simulation_mpi_rank = 0,				///< mpi rank
			int i_simulation_mpi_size = 1					///< mpi size
	)
	{
		o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = -1;

		switch(i_world_scene_id)
		{
			case -6:
				p_setupTriangulation_SVG(o_cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_3_triangles.svg");
				break;

			case -5:
				p_setupTriangulation_SVG(o_cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_small_even_and_odd.svg");
				break;

			case -4:
				p_setupTriangulation_2DCube_PeriodicBoundaries(o_cDomain_BaseTriangulation);
				break;

			case -3:
				p_setupTriangulation_Strip_PeriodicBoundaries(o_cDomain_BaseTriangulation, 8);
				break;

			case -2:
				p_setupTriangulation_Quad_PeriodicBoundaries(o_cDomain_BaseTriangulation);
				break;

			case -1:
				p_setupTriangulation_QuadWith2Triangles(o_cDomain_BaseTriangulation);
				break;

			case 0:
				p_setupTriangulation_SingleTriangle(o_cDomain_BaseTriangulation);
				break;

			case 1:
				p_setupTriangulation_2OddTriangles1(o_cDomain_BaseTriangulation);
				break;

			case 2:
				p_setupTriangulation_2OddTriangles2(o_cDomain_BaseTriangulation);
				break;

			case 3:
				p_setupTriangulation_QuadWith2Triangles(o_cDomain_BaseTriangulation);
				break;

			case 4:
				p_setupTriangulation_2EvenTriangles1(o_cDomain_BaseTriangulation);
				break;

			case 5:
				p_setupTriangulation_2EvenTriangles1(o_cDomain_BaseTriangulation);
				break;

			case 7:
				p_setupTriangulation_3Triangles(o_cDomain_BaseTriangulation);
				break;

			case 8:
				p_setupTriangulation_QuadTiles(o_cDomain_BaseTriangulation, 1, 1);
				break;

			case 9:
				p_setupTriangulation_SVG(o_cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_small_odd.svg");
				break;

			case 10:
				p_setupTriangulation_SVG(o_cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain.svg");
				break;

			case 11:
				p_setupTriangulation_SVG(o_cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_cross.svg");
				break;

			default:
				p_setupTriangulation_SingleTriangle(o_cDomain_BaseTriangulation);
				break;
		}

		assert(o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain != -1);

#if CONFIG_ENABLE_MPI

		if (i_verbosity_level >= 99)
			std::cout << "Valid DM triangulation: " << (o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation ? "true" : "false") << std::endl;

		/*
		 * remove triangles which are available for domain triangulations with odd depth
		 */
		if (!o_cDomain_BaseTriangulation->valid_distributed_memory_triangulation)
		{
			std::cerr << "ERROR: Triangulation is not valid for MPI usage!!! Setting up alternative domain!!!" << std::endl;
			o_cDomain_BaseTriangulation->clear();

			// set to value 1 to avoid assertion problems due to timestep issues
		    o_cDomain_BaseTriangulation->length_of_catheti_for_unit_domain = 1;
		}

#endif

		// always reduce base triangulation to consist either of even or odd base triangles
		// IMPORTANT: don't remove this method since it sets the flag oddInitialDepthFixApplied
		o_cDomain_BaseTriangulation->removeInvalidCluster();


		/*
		 * setup the adjacency informations
		 */
		o_cDomain_BaseTriangulation->setup_AdjacencyInformation();


		/*
		 * run further splitting here to have better load balancing for MPI nodes
		 */
		o_cDomain_BaseTriangulation->splitBaseTriangulation(i_initial_cluster_splittings);


		/*
		 * project to unit square
		 */
		o_cDomain_BaseTriangulation->scaleAndTranslateToUnitSquare();


#if CONFIG_ENABLE_MPI

		/*
		 * setup mpi node enumeration
		 */
		o_cDomain_BaseTriangulation->setup_MPINodeEnumeration(i_simulation_mpi_rank, i_simulation_mpi_size);
#endif
	}
};

}

#endif
