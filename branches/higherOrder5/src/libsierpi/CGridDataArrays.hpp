/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationTsunami_Parallel_FileOutput_Block.hpp
 *
 *  Created on: Jul 6, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTGRIDDATAARRAYS_HPP_
#define COUTPUTGRIDDATAARRAYS_HPP_


#include "mainthreading/CMainThreading.hpp"

class CGridDataArrays_Enums
{
public:
	enum EFLAGS
	{
		VERTICES	= (1 << 0),
		NORMALS		= (1 << 1),

		VALUE0	= (1 << 2),
		VALUE1	= (1 << 3),
		VALUE2	= (1 << 4),
		VALUE3	= (1 << 5),
		VALUE4 	= (1 << 6),
		VALUE5 	= (1 << 7),
		VALUE6 	= (1 << 8),

		ALL			= 0xffffff
	};
};


template <
	int t_vertices_per_cell,		///< vertices to be stored per cell (3 for 2D, 2 for 1D)
	int t_max_dof_variables
>
class CGridDataArrays
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	T *triangle_vertex_buffer;
	T *triangle_normal_buffer;

	T *dof_element[t_max_dof_variables];

	long long number_of_triangle_cells;
	size_t next_free_cell_id;


	CMainThreading_Lock lock;

	/**
	 * constructor for simulation data storeage
	 */
	CGridDataArrays(
		size_t i_number_of_triangle_cells,	///< number of triangle cells to write data for
		size_t i_vertex_multiplier = 1,		///< multiplier for vertex data. 2 for 1D (lines), 3 for 2D (triangles), 3 for 3D (triangles=3)
		size_t i_dof_variables_multiplier = 1,		///< variables per DOF
		int i_flags = CGridDataArrays_Enums::ALL
	)	:
		triangle_vertex_buffer(nullptr),
		triangle_normal_buffer(nullptr),


		number_of_triangle_cells(i_number_of_triangle_cells),
		next_free_cell_id(0)
	{
		if (i_flags & CGridDataArrays_Enums::VERTICES)
			triangle_vertex_buffer = new T[number_of_triangle_cells*t_vertices_per_cell*3*i_vertex_multiplier];

		if (i_flags & CGridDataArrays_Enums::NORMALS)
			triangle_normal_buffer = new T[number_of_triangle_cells*t_vertices_per_cell*3*i_vertex_multiplier];


		int f = CGridDataArrays_Enums::VALUE0;

		for (int i = 0; i < t_max_dof_variables; i++)
		{
			if (i_flags & f)
				dof_element[i] = new T[number_of_triangle_cells*i_dof_variables_multiplier];
			else
				dof_element[i] = nullptr;

			f <<= 1;
		}
	}



	size_t getNextTriangleCellStartId(
			size_t i_number_of_cells
	)
	{
		lock.lock();

		size_t n = next_free_cell_id;
		next_free_cell_id += i_number_of_cells;

		lock.unlock();

		assert(next_free_cell_id <= (size_t)number_of_triangle_cells);

		return n;
	}


	~CGridDataArrays()
	{
		if (triangle_vertex_buffer != nullptr)
			delete [] triangle_vertex_buffer;

		if (triangle_normal_buffer != nullptr)
			delete [] triangle_normal_buffer;

		for (int i = 0; i < t_max_dof_variables; i++)
		{
			if (dof_element[i])
				delete [] dof_element[i];
		}
	}
};


#endif /* CSIMULATIONTSUNAMI_PARALLEL_FILEOUTPUT_BLOCK_HPP_ */
