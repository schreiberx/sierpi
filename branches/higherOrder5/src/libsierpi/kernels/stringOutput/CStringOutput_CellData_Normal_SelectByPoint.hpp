/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 12, 2011
 *      Author: schreibm
 */

#ifndef KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_NORMAL_HPP_
#define KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_NORMAL_HPP_

#include <cmath>
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{

template <typename t_CSimulationStacksAndTypes>
class CStringOutput_CellData_Normal_SelectByPoint
{
public:
	typedef t_CSimulationStacksAndTypes CSimulationStacks_;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData	CCellData_;
	typedef float TVertexScalar;

private:
	typedef CVertex2d<TVertexScalar> TVertex;

	TVertexScalar px, py;

public:
	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<CStringOutput_CellData_Normal_SelectByPoint<t_CSimulationStacksAndTypes>, t_CSimulationStacksAndTypes> TRAV;

	CStringOutput_CellData_Normal_SelectByPoint()
	{
	}



	inline bool op_cell(
		TVertexScalar v0x, TVertexScalar v0y,
		TVertexScalar v1x, TVertexScalar v1y,
		TVertexScalar v2x, TVertexScalar v2y,

		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,

		const CCellData_ *i_cellData
	)
	{
		if (!CPointInTriangleTest<TVertexScalar>::test(v0x, v0y, v1x, v1y, v2x, v2y, px, py))
			return false;

		std::cout << std::endl;
		std::cout << "========================" << std::endl;
		std::cout << "| ELEMENT DATA (" << px << ", " << py << ")" << std::endl;
		std::cout << "========================" << std::endl;
		std::cout << "  Element Vertices: " << std::endl;
		std::cout << "   +  left: " << v0x << ", " << v0y << "    " << std::endl;
		std::cout << "   + right: " << v1x << ", " << v1y << "    " << std::endl;
		std::cout << "   +   top: " << v2x << ", " << v2y << "    " << std::endl;
		std::cout << std::endl;
		std::cout << "  Normals: " << std::endl;
		std::cout << "   +   hyp: " << hyp_normal_x << ", " << hyp_normal_y << std::endl;
		std::cout << "   + right: " << right_normal_x << ", " << right_normal_y << std::endl;
		std::cout << "   +  left: " << left_normal_x << ", " << left_normal_y << std::endl;
		std::cout << std::endl;
		std::cout << "  Data stored in element: " << std::endl;
		i_cellData->outputVerboseData(std::cout, -right_normal_x, -right_normal_y);
		std::cout << std::endl;
		return true;
	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}


	inline void setup(
				TVertexScalar p_px,
				TVertexScalar p_py
			)
	{
		px = p_px;
		py = p_py;
	}


	inline void setup_WithKernel(
			CStringOutput_CellData_Normal_SelectByPoint<t_CSimulationStacksAndTypes> &parent
	)
	{
		px = parent.px;
		py = parent.py;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
