/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRIANGLE_POINT_PROJECTIONS_HPP_
#define CTRIANGLE_POINT_PROJECTIONS_HPP_

#include <cmath>

/**
 * This class contains helper methods to project 2D points stored in
 * world-space to a 2D problem stored in an edge-space.
 */
class CTriangle_PointProjections
{
public:
	/**
	 * Project from reference space to world space
	 */
	template <typename T>
	inline static void referenceToWorld(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_reference_coord_u,	T i_reference_coord_v,

			T *o_world_coord_x,		T *o_world_coord_y
	)	{

		T m[2][2] = {
				{	i_vertex_left_x - i_vertex_top_x,	i_vertex_right_x - i_vertex_top_x	},
				{	i_vertex_left_y - i_vertex_top_y,	i_vertex_right_y - i_vertex_top_y	}
		};

		*o_world_coord_x =	m[0][0] * i_reference_coord_u	+ m[0][1] * i_reference_coord_v	+ i_vertex_top_x;
		*o_world_coord_y =	m[1][0] * i_reference_coord_u	+ m[1][1] * i_reference_coord_v	+ i_vertex_top_y;
	}



	/*
	 * project from left child to reference element
	 */
	template <typename T>
	inline static void rightChildToReference(
			T i_p[2],
			T o_p[2]
	)	{
		o_p[0] =	-(T)0.5 * i_p[0]	+ -(T)0.5 * i_p[1]	+ (T)0.5;
		o_p[1] =	 (T)0.5 * i_p[0]	+ -(T)0.5 * i_p[1]	+ (T)0.5;
	}



	/*
	 * project from right child to reference element
	 */
	template <typename T>
	inline static void leftChildToReference(
			T i_p[2],
			T o_p[2]
	)	{
		o_p[0] = -(T)0.5 * i_p[0]	+  (T)0.5 * i_p[1] + (T)0.5;
		o_p[1] = -(T)0.5 * i_p[0]	+ -(T)0.5 * i_p[1] + (T)0.5;
	}



	/*
	 * project from reference to left child
	 */
	template <typename T>
	inline static void referenceToLeftChild(
			T i_p[2],
			T o_p[2]
	)	{
		T np[2] = {
				i_p[0] - (T)0.5,
				i_p[1] - (T)0.5
		};

		o_p[0] = -(T)1.0 * np[0]	+ -(T)1.0 * np[1];
		o_p[1] =  (T)1.0 * np[0]	+ -(T)1.0 * np[1];
	}



	/*
	 * project from reference to right child
	 */
	template <typename T>
	inline static void referenceToRightChild(
			T i_p[2],
			T o_p[2]
	)	{
		T np[2] = {
				i_p[0] - (T)0.5,
				i_p[1] - (T)0.5
		};

		o_p[0] = -(T)1.0 * np[0]	+  (T)1.0 * np[1];
		o_p[1] = -(T)1.0 * np[0]	+ -(T)1.0 * np[1];
	}
};

#endif
