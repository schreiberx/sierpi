/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CPROJECTIONS_HPP_
#define CPROJECTIONS_HPP_

#include <cmath>

/**
 * This class contains helper methods to project 2D problems stored in
 * world-space to a 2D problem stored in an edge-space.
 *
 * All projects are taken from the viewpoint of the reference element.
 */
class CTriangle_VectorProjections
{
public:
	/************************************************
	 * Project from CELL space to another space by giving X-asis of new space
	 ************************************************/

	/**
	 * Project from CELL space to another space by giving X-asis of CELL reference space
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void referenceToWorld(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis reference vector in world-space (x component)
			T i_by		///< x-axis reference vector in world-space (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(-i_by);
		*io_hv =	(*io_hu)	*	(i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Project from WORLD space to REFERENCE space.
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void worldToReference(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Represent Vector from Reference space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementToBasisWithXAxis(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}



	/**
	 * represent Vector from CELL space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementWithXAxisToWorldSpace(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}



	/************************************************
	 * HYPOTENUSE STUFF
	 ************************************************/

	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = -*io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hu = -tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from hyp edge space
	 */
	template <typename T>
	inline static void fromHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}



	/************************************************
	 * LEFT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hv;
		*io_hv = *io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from left edge space
	 */
	template <typename T>
	inline static void fromLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}



	/************************************************
	 * RIGHT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hu;
		*io_hv = -*io_hv;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		// nothing to do
	}



	/**
	 * compute backprojected components of (hu,hv) from right edge space
	 */
	template <typename T>
	inline static void fromRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hu;
		*io_hv = -*io_hv;
		*io_hu = tmp;
	}

};


#endif /* CFLUXPROJECTIONS_HPP_ */
