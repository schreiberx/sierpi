/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRIANGLE_TOOLS_HPP_
#define CTRIANGLE_TOOLS_HPP_

namespace sierpi
{


class CTriangle_Tools
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:

	/**
	 * return level of detail
	 */
	static inline T getLODFromDepth(
			int i_depth
	)	{
		return ((T)i_depth + (T)0.5)*(T)0.5;
	}

	/**
	 * get center of weight for triangle
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPoint(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
#if 0
		// use midpoint on hypotenuse
		*o_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

#else

		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);

#endif
	}



	/**
	 * get center of weight for triangle for both children
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPointForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
#if 0
		// use midpoint on hypotenuse

		*o_left_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_left_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

		*o_right_mx = *o_left_mx;
		*o_right_my = *o_left_my;

#else

		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;

#endif
	}
};


}

#endif
