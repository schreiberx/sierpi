/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 1. August 2012
 *      Author: Breuer Alexander <breuera@in.tum.de>, Martin Schreiber <martin.schreiber@in.tum.de>
 */
#ifndef CTSUNAMI_SIMULATION_CELLDATA_1D_HPP_
#define CTSUNAMI_SIMULATION_CELLDATA_1D_HPP_



/**
 * extend cell data by additional information which is
 * usually provided by traversators
 */
class CCellData_1D	:
	public CTsunamiSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	T position_x;		///< center position
	T cell_size_x;		///< size of cell
	T timestep_size;	/// timestep size (for LTS)


	/**
	 * setup cell
	 */
	void setup(
		T i_cell_size_x,		///< size of cell
		T i_position_x,			///< midpoint of cell
		CDatasets *cTsunamiSimulationScenarios	///< simulation scenarios to get initial DOFs
	)
	{
		cell_size_x = i_cell_size_x;
		position_x = i_position_x;

		cTsunamiSimulationScenarios->getNodalData(position_x, 0, 0, &dofs[0]);
	}


	std::string toString() {
		std::ostringstream s;
		s << "pos: " << position_x;
		s << ", size: " << "cell_size: " << cell_size_x;
		s << ", h: " << this->dofs[0].h;
		s << ", hu: " << this->dofs[0].hu;
		s << ", hv: " << this->dofs[0].hv;
		s << ", b: " << this->dofs[0].b;
		return s.str();
	}


	inline T getWaterHeight()
	{
		return dofs[0].h;
	}

	inline T getMomentum()
	{
		return dofs[0].hu;
	}

	inline T getBathymetry()
	{
		return dofs[0].b;
	}

	inline T getPositionX()
	{
		return position_x;
	}

	inline T getCellSizeX()
	{
		return cell_size_x;
	}
};


#endif
