/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 1. August 2012
 *      Author: Breuer Alexander <breuera@in.tum.de>, Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_1D_HPP_
#define CEDGECOMM_TSUNAMI_1D_HPP_

#include "../hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Hyperbolic.hpp"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "CCellData_1D.hpp"


/**
 * edge communication kernel to use for 1D simulations
 */
class CEdgeComm_Tsunami_1D:
	public sierpi::kernels::CEdgeComm_Hyperbolic
{
public:
	void op_1d_cell_to_edge_left(
			CCellData_1D &i_cell_data,
			CTsunamiSimulationEdgeData *o_edge_data
	)
	{
		o_edge_data->dofs[0] = i_cell_data.dofs[0];
		o_edge_data->dofs[0].hu = -o_edge_data->dofs[0].hu;
		o_edge_data->CFL1_scalar = i_cell_data.cell_size_x;
	}

	void op_1d_cell_to_edge_right(
			CCellData_1D &i_cell_data,
			CTsunamiSimulationEdgeData *o_edge_data
	)
	{
		o_edge_data->dofs[0] = i_cell_data.dofs[0];
		o_edge_data->CFL1_scalar = i_cell_data.cell_size_x;
	}

	void op_1d_boundary_cell_to_edge_left(
			CCellData_1D &i_cell_data,
			CTsunamiSimulationEdgeData *o_edge_data
	)
	{
		o_edge_data->dofs[0] = i_cell_data.dofs[0];
		o_edge_data->CFL1_scalar = i_cell_data.cell_size_x;
	}

	void op_1d_boundary_cell_to_edge_right(
			CCellData_1D &i_cell_data,
			CTsunamiSimulationEdgeData *o_edge_data
	)
	{
		o_edge_data->dofs[0] = i_cell_data.dofs[0];
		o_edge_data->dofs[0].hu = -o_edge_data->dofs[0].hu;
		o_edge_data->CFL1_scalar = i_cell_data.cell_size_x;
	}


	void op_1d_cell(
			CCellData_1D *io_cell_data,
			CTsunamiSimulationEdgeData &i_left_netupdates,
			CTsunamiSimulationEdgeData &i_right_netupdates
	)
	{
		CONFIG_DEFAULT_FLOATING_POINT_TYPE scalar = timestep_size/io_cell_data->cell_size_x;

		//update with left net update
		io_cell_data->dofs[0].h  -= scalar * i_right_netupdates.dofs[0].h;
		io_cell_data->dofs[0].hu -= scalar * i_right_netupdates.dofs[0].hu;

		//update with right net update
		io_cell_data->dofs[0].h  -= scalar * i_left_netupdates.dofs[0].h;
		// ADD hu due to rotation of fluxes!
		io_cell_data->dofs[0].hu += scalar * i_left_netupdates.dofs[0].hu;
	}
};



#endif /* CEDGECOMM_TSUNAMI_1D_HPP_ */
