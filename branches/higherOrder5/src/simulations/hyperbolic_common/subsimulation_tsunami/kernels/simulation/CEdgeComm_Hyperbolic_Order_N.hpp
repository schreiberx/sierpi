/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_ORDER_N_HPP_
#define CEDGECOMM_TSUNAMI_ORDER_N_HPP_


#include <cmath>
#include <limits>

// generic types
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

// traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "../../../subsimulation_tsunami/flux_solver/CFluxSolver.hpp"

// edge projections
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
// triangle side lengths
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"
// triangle side lengths
#include "libsierpi/triangle/CTriangle_Tools.hpp"

// pde lab stuff
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"

// enum for boundary conditions
#include "libsierpi/grid/CBoundaryConditions.hpp"

// default parameters for tsunami edge comm
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Parameters.hpp"

#include "CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (op_cell)
 */

template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Hyperbolic_Order_N	:
	public CEdgeComm_Parameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_Parameters::setTimestepSize;
	using CEdgeComm_Parameters::setGravitationalConstant;
	using CEdgeComm_Parameters::setBoundaryDirichlet;
	using CEdgeComm_Parameters::setParameters;


	/**
	 * typedefs for convenience
	 */
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Hyperbolic_Order_N<t_storeElementUpdatesOnly>, CHyperbolicTypes > TRAV;

	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,2> CGaussTriangleEdge;
//	typedef sierpi::pdelab::CGaussQuadrature2D_TriangleArea<T,2> CGaussTriangleArea;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Hyperbolic_Order_N()
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			boundary_dirichlet.dofs[i].h = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs[i].hu = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs[i].hv = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs[i].b = std::numeric_limits<T>::infinity();
		}
	}


	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CCellData &i_f_t0,
			CCellData &i_f_slope_t0,
			CCellData *o_f_t1_approx,
			T i_timestep_size
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_f_t1_approx->dofs[i].h	= i_f_t0.dofs[i].h	+ i_timestep_size*i_f_slope_t0.dofs[i].h;
			o_f_t1_approx->dofs[i].hu	= i_f_t0.dofs[i].hu	+ i_timestep_size*i_f_slope_t0.dofs[i].hu;
			o_f_t1_approx->dofs[i].hv	= i_f_t0.dofs[i].hv	+ i_timestep_size*i_f_slope_t0.dofs[i].hv;
			o_f_t1_approx->dofs[i].b	= i_f_t0.dofs[i].b;
		}

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CCellData &i_f_slope_t0,
			CCellData &i_f_slope_t1,
			CCellData *io_f_t0_t1,
			T i_timestep_size
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			io_f_t0_t1->dofs[i].h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[i].h + i_f_slope_t1.dofs[i].h);
			io_f_t0_t1->dofs[i].hu	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[i].hu + i_f_slope_t1.dofs[i].hu);
			io_f_t0_t1->dofs[i].hv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[i].hv + i_f_slope_t1.dofs[i].hv);
		}

		// TODO: which CFL to use?
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Hyperbolic_Order_N &i_kernel
	)	{
		// copy configuration
		(CEdgeComm_Parameters&)(*this) = (CEdgeComm_Parameters&)(i_kernel);
	}


	/**
	 * basis functions for 1st order
	 *
	 * 0: nodal point at hypotenuse edge
	 * 1: nodal point at right edge
	 * 2: nodal point at left edge
	 */
	typedef T (*FBasisFunctions) (T x, T y);

//	class CPhi
//	{
//	public:
//		static inline T phi_0(T x, T y)	{	return 2.0*x + 2.0*y - 1.0;		}
//		// right
//		static inline T phi_1(T x, T y)	{	return 1.0-2.0*x;		}
//		// left
//		static inline T phi_2(T x, T y)	{	return 1.0-2.0*y;		}
//
//		static T eval(int id, T x, T y)
//		{
//			FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
//			{
//					phi_0,
//					phi_1,
//					phi_2
//			};
//			return fBasisFunctions[id](x, y);
//		}
//	};



	/**
	 * compute nodal data for edge based communication on hypotenuse
	 */
	static inline void compute_edge_hyp_comm_nodal_data(
			const CCellData *i_cCellData,
			CEdgeData *o_cEdgeData
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			/*
			 * compute DOF values at sampling points by computing the sum over all basis functions
			 */
			o_cEdgeData->dofs[i].h = 0;
			o_cEdgeData->dofs[i].hu = 0;
			o_cEdgeData->dofs[i].hv = 0;
			o_cEdgeData->dofs[i].b = 0;

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T weight = CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::edge_comm_data_nodal_weight_matrix[0][i][j];

				o_cEdgeData->dofs[i].h	+= weight * i_cCellData->dofs[j].h;
				o_cEdgeData->dofs[i].hu	+= weight * i_cCellData->dofs[j].hu;
				o_cEdgeData->dofs[i].hv	+= weight * i_cCellData->dofs[j].hv;
				o_cEdgeData->dofs[i].b	+= weight * i_cCellData->dofs[j].b;
			}

			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toHypEdgeSpace(
				&(o_cEdgeData->dofs[i].hu),
				&(o_cEdgeData->dofs[i].hv)
			);
		}
	}


	static inline void compute_edge_right_comm_nodal_data(
			const CCellData *i_cCellData,
			CEdgeData *o_cEdgeData
	)	{

		/*
		 * load the sampling coordinates for the hypotenuse edge
		 */

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			// compute DOF values at sampling points by computing the sum over all basis functions
			o_cEdgeData->dofs[i].h = 0;
			o_cEdgeData->dofs[i].hu = 0;
			o_cEdgeData->dofs[i].hv = 0;
			o_cEdgeData->dofs[i].b = 0;

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T weight = CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::edge_comm_data_nodal_weight_matrix[1][i][j];

				o_cEdgeData->dofs[i].h	+= weight * i_cCellData->dofs[j].h;
				o_cEdgeData->dofs[i].hu	+= weight * i_cCellData->dofs[j].hu;
				o_cEdgeData->dofs[i].hv	+= weight * i_cCellData->dofs[j].hv;
				o_cEdgeData->dofs[i].b	+= weight * i_cCellData->dofs[j].b;
			}

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toRightEdgeSpace(
				&(o_cEdgeData->dofs[i].hu),
				&(o_cEdgeData->dofs[i].hv)
			);
		}

	}



	static inline void compute_edge_left_comm_nodal_data(
			const CCellData *i_cCellData,
			CEdgeData *o_cEdgeData
	)	{

		/*
		 * load the sampling coordinates for the hypotenuse edge
		 */
		const T *sampling_coords = CGaussTriangleEdge::getCoordsLeftEdge();

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			// compute DOF values at sampling points by computing the sum over all basis functions
			o_cEdgeData->dofs[i].h = 0;
			o_cEdgeData->dofs[i].hu = 0;
			o_cEdgeData->dofs[i].hv = 0;
			o_cEdgeData->dofs[i].b = 0;

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T weight = CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::edge_comm_data_nodal_weight_matrix[2][i][j];

				o_cEdgeData->dofs[i].h	+= weight * i_cCellData->dofs[j].h;
				o_cEdgeData->dofs[i].hu	+= weight * i_cCellData->dofs[j].hu;
				o_cEdgeData->dofs[i].hv	+= weight * i_cCellData->dofs[j].hv;
				o_cEdgeData->dofs[i].b	+= weight * i_cCellData->dofs[j].b;
			}

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toLeftEdgeSpace(
				&(o_cEdgeData->dofs[i].hu),
				&(o_cEdgeData->dofs[i].hv)
			);

			sampling_coords += 2;
		}
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// compute edge comm nodal data on edge
		compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);

		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupHypEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// compute edge comm nodal data on edge
		compute_edge_right_comm_nodal_data(i_cCellData, o_cEdgeData);

		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRightEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// compute edge comm nodal data on edge
		compute_edge_left_comm_nodal_data(i_cCellData, o_cEdgeData);

		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupLeftEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
				o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
			}
			break;


		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &o_cEdgeData->dofs[i]))
				{
					CTriangle_VectorProjections::worldToReference(
									&o_cEdgeData->dofs[0].hu,
									&o_cEdgeData->dofs[1].hv,
									-i_right_normal_x,
									-i_right_normal_y
							);
				}
				else
				{
					// default: outflow boundary condition
					compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
				}
			}
			break;
		}


		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
			o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cCellData->validation.setupHypEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			compute_edge_right_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			compute_edge_right_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			compute_edge_right_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			compute_edge_right_comm_nodal_data(i_cCellData, o_cEdgeData);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
				o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
			}
			break;

		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &o_cEdgeData->dofs[i]))
				{
					CTriangle_VectorProjections::worldToReference(
									&o_cEdgeData->dofs[0].hu,
									&o_cEdgeData->dofs[1].hv,
									-i_right_normal_x,
									-i_right_normal_y
							);
				}
				else
				{
					// default: outflow boundary condition
					compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
			o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cCellData->validation.setupRightEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			compute_edge_left_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			compute_edge_left_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			compute_edge_left_comm_nodal_data(i_cCellData, o_cEdgeData);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			compute_edge_left_comm_nodal_data(i_cCellData, o_cEdgeData);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
				o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
			}
			break;

		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &o_cEdgeData->dofs[i]))
				{
					CTriangle_VectorProjections::worldToReference(
									&o_cEdgeData->dofs[0].hu,
									&o_cEdgeData->dofs[1].hv,
									-i_right_normal_x,
									-i_right_normal_y
							);
				}
				else
				{
					// default: outflow boundary condition
					compute_edge_hyp_comm_nodal_data(i_cCellData, o_cEdgeData);
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			o_cEdgeData->dofs[i].hu = -o_cEdgeData->dofs[i].hu;
			o_cEdgeData->dofs[i].hv = -o_cEdgeData->dofs[i].hv;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cCellData->validation.setupLeftEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * compute flux components
	 */
	inline void compute_flux(
		const CNodeData *i_node_data,	///< nodal data
		CNodeData *o_flux_data_x,		///< flux x-component
		CNodeData *o_flux_data_y		///< flux y-component
	)	{

#if SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS

		if (i_node_data->h <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
		{
			o_flux_data_x->h = 0;
			o_flux_data_x->hu = 0;
			o_flux_data_x->hv = 0;

			o_flux_data_y->h = 0;
			o_flux_data_y->hu = 0;
			o_flux_data_y->hv = 0;
			return;
		}

#endif

		o_flux_data_x->h = i_node_data->hu;
		o_flux_data_x->hu = (i_node_data->hu*i_node_data->hu)/i_node_data->h + (T)0.5*gravitational_constant*i_node_data->h*i_node_data->h;
		o_flux_data_x->hv = (i_node_data->hu*i_node_data->hv)/i_node_data->h;

		o_flux_data_y->h = i_node_data->hv;
		o_flux_data_y->hu = (i_node_data->hu*i_node_data->hv)/i_node_data->h;
		o_flux_data_y->hv = (i_node_data->hv*i_node_data->hv)/i_node_data->h + (T)0.5*gravitational_constant*i_node_data->h*i_node_data->h;
	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			int i_depth,

			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update		///< incoming flux from left edge
	)	{

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION

		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cCellData->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
			);

#endif

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			CTriangle_VectorProjections::fromHypEdgeSpace(
					&i_hyp_edge_net_update->dofs[i].hu,
					&i_hyp_edge_net_update->dofs[i].hv
				);

			CTriangle_VectorProjections::fromRightEdgeSpace(
					&i_right_edge_net_update->dofs[i].hu,
					&i_right_edge_net_update->dofs[i].hv
				);

			CTriangle_VectorProjections::fromLeftEdgeSpace(
					&i_left_edge_net_update->dofs[i].hu,
					&i_left_edge_net_update->dofs[i].hv
				);
		}

#if 0
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << *i_hyp_edge_net_update << std::endl;
		std::cout << *i_right_edge_net_update << std::endl;
		std::cout << *i_left_edge_net_update << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
#endif

		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);

#if DEBUG
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			if (std::isnan(io_cCellData->dofs[i].h))
			{
				instabilityDetected = true;
				if (!instabilityDetected)
					std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
				return;
			}
		}
#endif

		/*
		 * Conservation law:
		 *
		 *   U_t + U_x = 0
		 *
		 * Explicit Euler timestep:
		 *
		 *   U^{t+1} = U^{t} + \Delta t (- U_x)
		 *
		 *
		 * Update (See High Order ADER FV/DG Numerical Methods for Hyperbolic Equations, Cristobal Castro, page 42):
		 *
		 *   U^{t+1} = U^{t} + \Delta t * M^{-1} [ S_x F(U^{t}) + S_y G(U^{t}) - \sum_e (E_e X(U^{t})) ]
		 *
		 *
		 * We reformulate this formula by extracting the cathetus length c from the matrices
		 * to get the dimensionless matrices M, S_x, S_y and E_e
		 *
		 *   U^{t+1} = U^{t} + 1/{c*c} \Delta t * M^{-1} [ c * S_x F(U^{t}) + c * S_y G(U^{t}) - c * \sum_e (E_e X(U^{t})) ]
		 *
		 *
		 * By factoring c out of the brackets, we get
		 *
		 *   U^{t+1} = U^{t} + \alpha * M^{-1} [ S_x * F(U^{t}) + S_y * G(U^{t}) - \sum_e (E_e X_e) ]
		 *
		 *   with \alpha = 1/{c} \Delta t
		 *
		 */

		static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		/*
		 * setup updates (fill with 0)
		 */
		CNodeData dofUpdates[N];
		for (int i = 0; i < N; i++)
		{
			dofUpdates[i].h = 0;
			dofUpdates[i].hu = 0;
			dofUpdates[i].hv = 0;
		}

#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1

		/*
		 * compute flux
		 */
		CNodeData flux_x[N];
		CNodeData flux_y[N];
		for (int i = 0; i < N; i++)
			compute_flux(&io_cCellData->dofs[i], &flux_x[i], &flux_y[i]);


		/*
		 * compute stiffness terms
		 */
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
			{
				dofUpdates[i].h		+= CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_x[i][j]*flux_x[j].h		+ CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_y[i][j]*flux_y[j].h;
				dofUpdates[i].hu	+= CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_x[i][j]*flux_x[j].hu	+ CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_y[i][j]*flux_y[j].hu;
				dofUpdates[i].hv	+= CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_x[i][j]*flux_x[j].hv	+ CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::stiffness_y[i][j]*flux_y[j].hv;
			}
		}

#endif


		CEdgeData *edge_net_updates[3] = {
				i_hyp_edge_net_update,
				i_right_edge_net_update,
				i_left_edge_net_update
		};

		/*
		 * flux
		 */
		// iterate over edges
		for (int e = 0; e < 3; e++)
		{
			// for each DOF on the edge
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			{
				// update each cell data DOF
				for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				{
					T weight = CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::edge_flux_quadrature_weight_matrix[e][i][j];

					dofUpdates[j].h -= weight * edge_net_updates[e]->dofs[i].h;
					dofUpdates[j].hu -= weight * edge_net_updates[e]->dofs[i].hu;
					dofUpdates[j].hv -= weight * edge_net_updates[e]->dofs[i].hv;
				}
			}
		}

		T cat_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;


#if DEBUG
		if (
				std::isnan(dofUpdates[0].hu)			||
				std::isnan(dofUpdates[0].hv)
		)
		{
			std::cout << "op_cell update instability" << std::endl;
			std::cout << *io_cCellData << std::endl;
			std::cout << dofUpdates[0] << std::endl;

			std::cout << "net updates:" << std::endl;
			std::cout << *i_hyp_edge_net_update << std::endl;
			std::cout << *i_right_edge_net_update << std::endl;
			std::cout << *i_left_edge_net_update << std::endl;
			exit(-1);
		}
#endif

		/*
		 * compute timestep update
		 */
		if (t_storeElementUpdatesOnly)
		{
			// dimensional update
			T alpha = 1.0/cat_length;

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					io_cCellData->dofs[i].h		= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].h;
					io_cCellData->dofs[i].hu	= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].hu;
					io_cCellData->dofs[i].hv	= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].hv;
				}
			}
		}
		else
		{
			// dimensional update
			T alpha = timestep_size/cat_length;

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					io_cCellData->dofs[i].h		+= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].h;
					io_cCellData->dofs[i].hu	+= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].hu;
					io_cCellData->dofs[i].hv	+= alpha * CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::inv_mass_matrix[i][j] * dofUpdates[j].hv;

					if (io_cCellData->dofs[i].h < SIMULATION_TSUNAMI_DRY_THRESHOLD)
					{
						io_cCellData->dofs[i].h = 0;
						io_cCellData->dofs[i].hu = 0;
						io_cCellData->dofs[i].hv = 0;
					}
				}
			}


#if DEBUG
		if (
				std::isnan(io_cCellData->dofs[0].hu)			||
				std::isnan(io_cCellData->dofs[0].hv)
		)
		{
			std::cout << "op_cell instability" << std::endl;
			std::cout << *io_cCellData << std::endl;
			exit(-1);
		}
#endif
		}

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

		T m = 0;
		T h = 0;

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			if (i_hyp_edge_net_update->dofs[i].h > m)
			{
				m = i_hyp_edge_net_update->dofs[i].h;
				h = m;
			}
		}
		m *= (T)M_SQRT2;

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			if (i_right_edge_net_update->dofs[i].h > m)
			{
				m = i_right_edge_net_update->dofs[i].h;
				h = m;
			}
		}

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			if (i_left_edge_net_update->dofs[i].h > m)
			{
				m = i_left_edge_net_update->dofs[i].h;
				h = m;
			}
		}

		m *= cat_length;

		T inv_h;
		if (h > SIMULATION_TSUNAMI_DRY_THRESHOLD)
			inv_h = (T)1.0/h;
		else
			inv_h = SIMULATION_TSUNAMI_DRY_THRESHOLD;

		// rescale with height to get update for unit area over unit timestep
		io_cCellData->refine = (m*inv_h > refine_threshold);
		io_cCellData->coarsen = (m*inv_h < coarsen_threshold);
#endif

	}



	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			const CEdgeData &i_edgeFlux_left,		///< edge data on left edge
			const CEdgeData &i_edgeFlux_right,		///< edge data on right edge
			CEdgeData &o_edgeNetUpdates_left,		///< output for left flux
			CEdgeData &o_edgeNetUpdates_right		///< output for outer flux
	)	{
		/*
		 * fix edge normal space for right flux
		 */

		T max_wave_speed_left = -std::numeric_limits<T>::infinity();
		T max_wave_speed_right = -std::numeric_limits<T>::infinity();

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			CNodeData nodeData_right;
			nodeData_right.h	= i_edgeFlux_right.dofs[i].h;
			nodeData_right.hu	= -i_edgeFlux_right.dofs[i].hu;
			nodeData_right.hv	= -i_edgeFlux_right.dofs[i].hv;
			nodeData_right.b	= i_edgeFlux_right.dofs[i].b;

			T o_max_wave_speed_left;
			T o_max_wave_speed_right;

			fluxSolver.op_edge_edge(
					i_edgeFlux_left.dofs[(SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS-1)-i],
					nodeData_right,
					&o_edgeNetUpdates_left.dofs[(SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS-1)-i],
					&o_edgeNetUpdates_right.dofs[i],
					&o_max_wave_speed_left,
					&o_max_wave_speed_right,
					gravitational_constant
				);

			o_edgeNetUpdates_right.dofs[i].hu = -o_edgeNetUpdates_right.dofs[i].hu;
			o_edgeNetUpdates_right.dofs[i].hv = -o_edgeNetUpdates_right.dofs[i].hv;

			max_wave_speed_left = std::max(max_wave_speed_left, o_max_wave_speed_left);
			max_wave_speed_right = std::max(max_wave_speed_right, o_max_wave_speed_right);
		}

		if (max_wave_speed_left > 10000)
			std::cout << max_wave_speed_left << std::endl;

		if (max_wave_speed_right > 10000)
			std::cout << max_wave_speed_right << std::endl;

		o_edgeNetUpdates_left.CFL1_scalar = i_edgeFlux_left.CFL1_scalar / max_wave_speed_left;
		o_edgeNetUpdates_right.CFL1_scalar = i_edgeFlux_right.CFL1_scalar / max_wave_speed_right;

		if (	!(o_edgeNetUpdates_left.CFL1_scalar > 0) ||
				!(o_edgeNetUpdates_right.CFL1_scalar > 0)
		)
		{
			std::cout << o_edgeNetUpdates_left.CFL1_scalar << std::endl;
			std::cout << o_edgeNetUpdates_right.CFL1_scalar << std::endl;

			std::cout << i_edgeFlux_left << std::endl;
			std::cout << i_edgeFlux_right << std::endl;
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edgeNetUpdates_left.validation = i_edgeFlux_left.validation;
		o_edgeNetUpdates_right.validation = i_edgeFlux_right.validation;
#endif
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CEdgeData> *io_edge_comm_buffer
	)	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_stack(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_stack(
		CEdgeData *io_edge_data_array,
		size_t i_edge_comm_elements
	)	{
		for (size_t i = 0; i < i_edge_comm_elements; i+=2)
		{
			CEdgeData ed0 = io_edge_data_array[i];
			CEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}

	static void setupMatrices()
	{
		CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::setup();
	}
};

}
}



#endif
