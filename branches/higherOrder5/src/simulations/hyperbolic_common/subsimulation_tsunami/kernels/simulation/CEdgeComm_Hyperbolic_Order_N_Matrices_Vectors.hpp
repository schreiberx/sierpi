/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors_HPP_
#define CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors_HPP_

#include <string.h>

#include "../../../subsimulation_generic/CConfig.hpp"
#include "../../../subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature2D_TriangleArea.hpp"
#include "libmath/CMatrixOperations.hpp"

class CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	static T mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T stiffness_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T stiffness_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	static T edge_comm_data_nodal_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T edge_flux_quadrature_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];


	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_DEGREE> CGaussQuadratureTriangleEdge;
	typedef sierpi::pdelab::CGaussQuadrature2D_TriangleArea<T,SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE> CGaussQuadratureTriangleArea;
	typedef CHyperbolicTypes::CBasisFunctions CBasisFunctions;

	static void printMatrix(T matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS]){
		int w = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;
		int h = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;
		for(int y=0; y<h; ++y){
			for(int x=0; x<w; ++x){
				std::cout << matrix[x][y] << "\t ";
			}
			std::cout << std::endl;
		}
	}

	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
	static void setup()
	{
		std::cout << "Setting up matrices!" << std::endl;
#if 0
		for (int i = 0; i < 100000; i++)
		{
			std::cout << "." << std::flush;
			static const int N = 10;

			T im[N][N];

			for (int i = 0; i < N; i++)
				for (int j = 0; j < N; j++)
					im[i][j] = ((T)rand()/(T)RAND_MAX);

			T om[N][N];

			T tm[N][N];

			T d = CMatrixOperations::getInverse<T,N>(im, om);

			CMatrixOperations::multiply<T,N>(im, om, tm);

			T tol = 0.0001/std::abs(d);

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (i == j)
					{
						if (std::abs(tm[i][i] - 1.0) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
					else
					{
						if (std::abs(tm[i][j]) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
				}
			}

			continue;

	error:
			std::cout << "TOL: " << tol << std::endl;

			std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << "Determinant: " << d << std::endl;
			std::cout << std::endl;

			CMatrixOperations::print<T,N>(im);
			CMatrixOperations::print<T,N>(om);
			CMatrixOperations::print<T,N>(tm);


			goto ok;
		}
	ok:
		exit(-1);
#endif

		T *gauss_2d_weights = CGaussQuadratureTriangleArea::getWeights();
		T *gauss_2d_coords = CGaussQuadratureTriangleArea::getCoords();

		std::cout << "Setting up mass matrix ... " << std::endl;
		/*
		 * MASS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				mass_matrix[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					std::cout << i << ", " << j << ", " << k << std::endl;
					mass_matrix[i][j] += gauss_2d_weights[k] * (
							CBasisFunctions::eval(i, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1]) *
							CBasisFunctions::eval(j, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1])
						);
				}
			}
		}

		T determinant = CMatrixOperations::getInverse<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(mass_matrix, inv_mass_matrix);

		if (determinant < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
		{
			std::cout << "Emergency exit" << std::endl;
			std::cout << "Determinant " << determinant << " detected for mass matrix" << std::endl;
			std::cout << std::endl;
			std::cout << "Mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(mass_matrix);
			std::cout << "Inverse mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(inv_mass_matrix);
		}

#if 1
		/*
		 * cleanup matrices
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				if (std::abs(inv_mass_matrix[i][j]) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = 0;
				if (std::abs(inv_mass_matrix[i][j]-1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = 1;
				if (std::abs(inv_mass_matrix[i][j]+1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = -1;
			}
		}
#endif

		std::cout << "Setting up stiffness matrix ... " << std::endl;
		/*
		 * STIFFNESS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				stiffness_x[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					stiffness_x[i][j] += gauss_2d_weights[k] * (
							CBasisFunctions::eval_dx(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							CBasisFunctions::eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
						);
				}


				stiffness_y[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					stiffness_y[i][j] += gauss_2d_weights[k] * (
							CBasisFunctions::eval_dy(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							CBasisFunctions::eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
						);
				}
			}
		}
		std::cout << "Mass matrix:" << std::endl;
		printMatrix(inv_mass_matrix);
		std::cout << "Stiffness x:" << std::endl;
		printMatrix(stiffness_x);
		std::cout << "Stiffness y:" << std::endl;
		printMatrix(stiffness_y);


		/*
		 * Edge comm data nodal weights
		 */
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),	edge_comm_data_nodal_weight_matrix[0]);
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	edge_comm_data_nodal_weight_matrix[1]);
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),	edge_comm_data_nodal_weight_matrix[2]);


		/*
		 * flux matrices
		 */
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),		M_SQRT2,	edge_flux_quadrature_weight_matrix[0]);
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	1.0,		edge_flux_quadrature_weight_matrix[1]);
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),		1.0,		edge_flux_quadrature_weight_matrix[2]);
	}

	static void computeEdgeCommDataNodalWeights(
			const T *edge_sampling_coords,
			T o_edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS]
	)
	{
		std::cout << "Obacht: " << SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS << " == " << CGaussQuadratureTriangleEdge::getNumPoints() << std::endl;
		assert(SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS == CGaussQuadratureTriangleEdge::getNumPoints());

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				o_edge_comm_data_nodal_weight_matrix[i][j] = CBasisFunctions::eval(j, edge_sampling_coords[0], edge_sampling_coords[1]);
			}
			edge_sampling_coords += 2;
		}
	}


	static void computeEdgeFluxQuardatureWeights(
			const T *i_edge_sampling_coords,
			T i_edge_length,
			T o_edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS]
	)	{
		const T *gauss_1d_weights = CGaussQuadratureTriangleEdge::getWeights();

		assert(SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS == CGaussQuadratureTriangleEdge::getNumPoints());

		// for each DOF on the edge
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			// update each cell data DOF
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T weight = CBasisFunctions::eval(j, i_edge_sampling_coords[0], i_edge_sampling_coords[1]);

				// multiply weight with gauss quadrature coefficients
				weight *= gauss_1d_weights[i];

				// multiply weight with 0.5 since integration interval is not [-1,1] but [0,1]
				weight *= 0.5;

				// multiply with edge length
				weight *= i_edge_length;

				o_edge_comm_data_nodal_weight_matrix[i][j] = weight;
			}

			i_edge_sampling_coords += 2;
		}
	}
};


#endif /* CEDGECOMM_HYPERBOLIC_ORDER_1_MATRICES_HPP_ */
