/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMI_TYPES_ORDER_N_HPP
#define CTSUNAMI_TYPES_ORDER_N_HPP

#include <iostream>
#include <limits>

#include "../../subsimulation_generic/CConfig.hpp"

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../../subsimulation_generic/types/CValidation_EdgeData.hpp"



/**
 * Edge communication data during tsunami simulation
 */
class CTsunamiSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:

	CTsunamiSimulationNodeData dofs[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS];

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

	T CFL1_scalar;


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[])
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
			dofs[i].setupDefaultValues(i_default_values);
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			 const CTsunamiSimulationEdgeData &d
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DATA_DOFS; i++)
		{
			os << "Tsunami CEdgeData Information (" << i << "):" << std::endl;
			os << " + h: " << d.dofs[i].h << std::endl;
			os << " + hu: " << d.dofs[i].hu << std::endl;
			os << " + hv: " << d.dofs[i].hv << std::endl;
			os << " + b/max_wave_speed: " << d.dofs[i].b << std::endl;
			os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;
			os << " + vx: " << d.dofs[i].hu/d.dofs[i].h << std::endl;
			os << " + vy: " << d.dofs[i].hv/d.dofs[i].h << std::endl;
			os << " + horizon: " << (d.dofs[i].h + d.dofs[i].b) << std::endl;
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};


#include "../../subsimulation_generic/types/CValidation_CellData.hpp"

class CTsunamiSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * weights for basis functions
	 */
	CTsunamiSimulationNodeData dofs[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	T cfl_domain_size_div_max_wave_speed;

#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA

	bool refine;
	bool coarsen;

#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(T default_values[])
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			dofs[i].setupDefaultValues(default_values);

		cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
	}



	friend
	::std::ostream&
	operator<<
	(
			::std::ostream& os,
			 const CTsunamiSimulationCellData &d
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			os << "   > DOF (" << i << "):";
			os << d.dofs[i] << std::endl;
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << d.validation << std::endl;
#endif
		return os;
	}


	/**
	 * compute and store node data
	 */
	void computeNodeData(
			T i_ref_coord_x,	///< x-coordinate in reference system
			T i_ref_coord_y,	///< y-coordinate in reference system
			CTsunamiSimulationNodeData *o_cNodeData
	)	const {

		T weight = CTsunamiSimulationBasisFunctions::eval(0, i_ref_coord_x, i_ref_coord_y);

		o_cNodeData->h	= weight * dofs[0].h;
		o_cNodeData->hu	= weight * dofs[0].hu;
		o_cNodeData->hv	= weight * dofs[0].hv;
		o_cNodeData->b	= weight * dofs[0].b;

		/**
		 * NOTE ON BATHYMETRY DATA:
		 *
		 * This data may not be interpolated since constant functions are
		 * only available in modal (frequency) basis representations.
		 */

		for (int i = 1; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			weight = CTsunamiSimulationBasisFunctions::eval(i, i_ref_coord_x, i_ref_coord_y);

			o_cNodeData->h	+= weight * dofs[i].h;
			o_cNodeData->hu	+= weight * dofs[i].hu;
			o_cNodeData->hv	+= weight * dofs[i].hv;
			o_cNodeData->b	+= weight * dofs[i].b;
		}
	}


	void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			os << "DOF (" << i << "): ";
			dofs[i].outputVerboseData(os, x_axis_x, x_axis_y);
			std::cout << std::endl;
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << validation << std::endl;
#endif
	}
};


#endif
