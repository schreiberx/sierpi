/*
 * CBasisFunctions_Order_1.hpp
 *
 *  Created on: Sep 1, 2012
 *      Author: schreibm
 */

#ifndef CBASISFUNCTIONS_ORDER_1_HPP_
#define CBASISFUNCTIONS_ORDER_1_HPP_


/**
 * basis functions for 1st order
 *
 * 0: nodal point at hypotenuse edge
 * 1: nodal point at right edge
 * 2: nodal point at left edge
 */
class CTsunamiSimulationBasisFunctions
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:

	typedef T (*FBasisFunctions) (T x, T y);

	// hyp
	static inline T phi_0(T x, T y)		{	return 2.0*x + 2.0*y - 1.0;		}
	static inline T phi_0_dx(T x, T y)	{	return 2.0;			}
	static inline T phi_0_dy(T x, T y)	{	return 2.0;			}

	// right
	static inline T phi_1(T x, T y)		{	return 1.0-2.0*x;	}
	static inline T phi_1_dx(T x, T y)	{	return -2.0;		}
	static inline T phi_1_dy(T x, T y)	{	return 0.0;			}

	// left
	static inline T phi_2(T x, T y)		{	return 1.0-2.0*y;	}
	static inline T phi_2_dx(T x, T y)	{	return 0.0;			}
	static inline T phi_2_dy(T x, T y)	{	return -2.0;		}


	static T eval(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0,
				phi_1,
				phi_2
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dx(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dx,
				phi_1_dx,
				phi_2_dx
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dy(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dy,
				phi_1_dy,
				phi_2_dy
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T* getNodalCoords()
	{
		static T nodal_coords[] = {
				0.5, 0.5,
				0, 0.5,
				0.5, 0
		};

		return nodal_coords;
	}

	static int getNumberOfFunctions()
	{
		return 3;
	}
};



#endif /* CBASISFUNCTIONS_ORDER_1_HPP_ */
