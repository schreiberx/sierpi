/*
 * CTsunamiTypes.hpp
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMITYPES_HPP_
#define CTSUNAMITYPES_HPP_

#include <vector>

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"





#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
	#include "CBasisFunctions_Order_0.hpp"
#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==1
	#include "CBasisFunctions_Order_1.hpp"
#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==2
	#include "CBasisFunctions_Order_2.hpp"
#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==3
	#include "CBasisFunctions_Order_3.hpp"
#else
	#error "order of basis function not supported!"
#endif


/**
 * Degree of freedoms for one point in tsunami simulation
 */
class CTsunamiSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h;	// height
	T hu;	// x-component of momentum
	T hv;	// y-component of momentum

	union	{
		T b;	// bathymetry
		T cfl1_timestep_size;	// max wave speed for flux computations
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b+h;
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T default_values[4])
	{
		h = default_values[0];
		hu = 0;
		hv = 0;
		b = -default_values[0];
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			const CTsunamiSimulationNodeData &d
	)	{
		os << " DOFs: [" << d.h << ", " << d.hu << ", " << d.hv << ", " << d.b << "]";
		return os;

		os << "CTsunamiSimulationNodeData:" << std::endl;
		os << " + h: " << d.h << std::endl;
		os << " + hu: " << d.hu << std::endl;
		os << " + hv: " << d.hv << std::endl;
		os << " + b/max_wave_speed: " << d.b << std::endl;
		os << " + vx: " << d.hu/d.h << std::endl;
		os << " + vy: " << d.hv/d.h << std::endl;
		os << " + horizon: " << (d.h + d.b) << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,	///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
//		os << "Tsunami DOF Information:" << std::endl;
//		os << " + h: " << h << std::endl;

		CONFIG_DEFAULT_FLOATING_POINT_TYPE thu = hu;
		CONFIG_DEFAULT_FLOATING_POINT_TYPE thv = hv;
		CTriangle_VectorProjections::referenceToWorld(&thu, &thv, x_axis_x, x_axis_y);

		os << "[" << h << ", " << thu << ", " << thv << ", " << b << "]";

//		os << " + hu: " << thu << std::endl;
//		os << " + hv: " << thv << std::endl;
//		os << " + b/max_wave_speed: " << b << std::endl;

//		os << " + vx: " << thu/h << std::endl;
//		os << " + vy: " << thv/h << std::endl;
//		os << " + horizon: " << (h + b) << std::endl;

//		os << std::endl;
	}
};



#include "CTypes_Order_N.hpp"

#include "../../subsimulation_generic/types/CVisualizationVertexData.hpp"





/**
 * simulation types which
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CTsunamiSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CTsunamiSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CTsunamiSimulationEdgeData CEdgeData;
	};



	/**
	 * This class implements all types which are used for visualization
	 */
	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CTsunamiSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};


	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;

	/**
	 * basis functions
	 */
	typedef CTsunamiSimulationBasisFunctions CBasisFunctions;
};




#endif /* CTSUNAMITYPES_HPP_ */
