/*
 * CBasisFunctions_Order_3.hpp
 *
 *  Created on: Sep 1, 2012
 *      Author: schreibm
 */

#ifndef CBASISFUNCTIONS_ORDER_3_HPP_
#define CBASISFUNCTIONS_ORDER_3_HPP_

#include<math.h>

/*
 * The ordering of the nodal points is as follows:
 *
 * 4\
 * |  \
 * |    \
 * |      \
 * 5        3
 * |          \
 * |            \
 * |              \
 * 0------1--------2
 *
 * That is, we start at the origin, and do it counter-clockwise.
 *
 */

class CTsunamiSimulationBasisFunctions
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:

	typedef T (*FBasisFunctions) (T x, T y);

	static inline T phi_0(T x, T y){ return 0.1e1 - 0.11e2 / 0.2e1 * x - 0.11e2 / 0.2e1 * y + 0.9e1 * x * x + 0.18e2 * x * y + 0.9e1 * y * y - 0.9e1 / 0.2e1 * pow(x, 0.3e1) - 0.27e2 / 0.2e1 * x * x * y - 0.27e2 / 0.2e1 * x * y * y - 0.9e1 / 0.2e1 * pow(y, 0.3e1); }
	static inline T phi_0_dx(T x, T y){ return -0.11e2 / 0.2e1 + (T) (18 * x) + (T) (18 * y) - 0.27e2 / 0.2e1 * (T) x * (T) x - (T) (27 * x * y) - 0.27e2 / 0.2e1 * (T) y * (T) y; }
	static inline T phi_0_dy(T x, T y){ return -0.11e2 / 0.2e1 + (T) (18 * x) + (T) (18 * y) - 0.27e2 / 0.2e1 * (T) x * (T) x - (T) (27 * x * y) - 0.27e2 / 0.2e1 * (T) y * (T) y; }

	static inline T phi_1(T x, T y){ return 0.9e1 / 0.2e1 * (T) x * (T) (2 - 5 * x - 5 * y + 3 * x * x + 6 * x * y + 3 * y * y); }
	static inline T phi_1_dx(T x, T y){ return 0.9e1 - 0.45e2 / 0.2e1 * x - 0.45e2 / 0.2e1 * y + 0.27e2 / 0.2e1 * x * x + 0.27e2 * x * y + 0.27e2 / 0.2e1 * y * y + 0.9e1 / 0.2e1 * x * (-0.5e1 + 0.6e1 * x + 0.6e1 * y); }
	static inline T phi_1_dy(T x, T y){ return 0.9e1 / 0.2e1 * (T) x * (T) (-5 + 6 * x + 6 * y); }

	static inline T phi_2(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) (1 - 4 * x - y + 3 * x * x + 3 * x * y); }
	static inline T phi_2_dx(T x, T y){ return -0.9e1 / 0.2e1 + (T) (18 * x) + 0.9e1 / 0.2e1 * y - 0.27e2 / 0.2e1 * (T) x * (T) x - 0.27e2 / 0.2e1 * (T) x * y - 0.9e1 / 0.2e1 * (T) x * (-0.4e1 + (T) (6 * x) + 0.3e1 * y); }
	static inline T phi_2_dy(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) (-1 + 3 * x); }

	static inline T phi_3(T x, T y){ return (T) (x * (2 - 9 * x + 9 * x * x)) / 0.2e1; }
	static inline T phi_3_dx(T x, T y){ return 0.1e1 - 0.9e1 / 0.2e1 * x + 0.9e1 / 0.2e1 * x * x + x * (-0.9e1 + 0.18e2 * x) / 0.2e1; }
	static inline T phi_3_dy(T x, T y){ return 0; }

	static inline T phi_4(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) y * (T) (1 - 3 * x); }
	static inline T phi_4_dx(T x, T y){ return -0.9e1 / 0.2e1 * (T) y * (T) (1 - 3 * x) + 0.27e2 / 0.2e1 * (T) x * (T) y; }
	static inline T phi_4_dy(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) (1 - 3 * x); }

	static inline T phi_5(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) y * (T) (1 - 3 * y); }
	static inline T phi_5_dx(T x, T y){ return -0.9e1 / 0.2e1 * (T) y * (T) (1 - 3 * y); }
	static inline T phi_5_dy(T x, T y){ return -0.9e1 / 0.2e1 * (T) x * (T) (1 - 3 * y) + 0.27e2 / 0.2e1 * (T) x * (T) y; }

	static inline T phi_6(T x, T y){ return (T) (y * (2 - 9 * y + 9 * y * y)) / 0.2e1; }
	static inline T phi_6_dx(T x, T y){ return 0; }
	static inline T phi_6_dy(T x, T y){ return 0.1e1 - 0.9e1 / 0.2e1 * y + 0.9e1 / 0.2e1 * y * y + y * (-0.9e1 + 0.18e2 * y) / 0.2e1; }

	static inline T phi_7(T x, T y){ return -0.9e1 / 0.2e1 * (T) y * (T) (1 - x - 4 * y + 3 * x * y + 3 * y * y); }
	static inline T phi_7_dx(T x, T y){ return -0.9e1 / 0.2e1 * (T) y * (T) (-1 + 3 * y); }
	static inline T phi_7_dy(T x, T y){ return -0.9e1 / 0.2e1 + 0.9e1 / 0.2e1 * x + (T) (18 * y) - 0.27e2 / 0.2e1 * x * (T) y - 0.27e2 / 0.2e1 * (T) y * (T) y - 0.9e1 / 0.2e1 * (T) y * (-0.4e1 + 0.3e1 * x + (T) (6 * y)); }

	static inline T phi_8(T x, T y){ return 0.9e1 / 0.2e1 * (T) y * (T) (2 - 5 * x - 5 * y + 3 * x * x + 6 * x * y + 3 * y * y); }
	static inline T phi_8_dx(T x, T y){ return 0.9e1 / 0.2e1 * (T) y * (T) (-5 + 6 * x + 6 * y); }
	static inline T phi_8_dy(T x, T y){ return 0.9e1 - 0.45e2 / 0.2e1 * x - 0.45e2 / 0.2e1 * y + 0.27e2 / 0.2e1 * x * x + 0.27e2 * x * y + 0.27e2 / 0.2e1 * y * y + 0.9e1 / 0.2e1 * y * (-0.5e1 + 0.6e1 * x + 0.6e1 * y); }

	static inline T phi_9(T x, T y){ return 27 * x * y * (1 - x - y); }
	static inline T phi_9_dx(T x, T y){ return 27 * y * (1 - x - y) - 27 * x * y; }
	static inline T phi_9_dy(T x, T y){ return 27 * x * (1 - x - y) - 27 * x * y; }

	static T eval(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0,
				phi_1,
				phi_2,
				phi_3,
				phi_4,
				phi_5,
				phi_6,
				phi_7,
				phi_8,
				phi_9,
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dx(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dx,
				phi_1_dx,
				phi_2_dx,
				phi_3_dx,
				phi_4_dx,
				phi_5_dx,
				phi_6_dx,
				phi_7_dx,
				phi_8_dx,
				phi_9_dx,
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dy(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dy,
				phi_1_dy,
				phi_2_dy,
				phi_3_dy,
				phi_4_dy,
				phi_5_dy,
				phi_6_dy,
				phi_7_dy,
				phi_8_dy,
				phi_9_dy,
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T* getNodalCoords()
	{
		static T nodal_coords[] = {
				1./3., 0.,
				2./3., 1./3.,
				2./3., 0.,
				1., 0.,
				0., 2./3.,
				1./3., 1./3.,
				0., 1./3.,
				1./3., 2./3.,
				0., 1.,
				0., 0.,
		};

		return nodal_coords;
	}

	static int getNumberOfFunctions()
	{
		return 10;
	}
};



#endif /* CBASISFUNCTIONS_ORDER_3_HPP_ */
