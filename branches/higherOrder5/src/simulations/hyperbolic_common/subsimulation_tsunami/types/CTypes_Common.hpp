/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CTSUNAMITYPES_COMMON_HPP_
#define CTSUNAMITYPES_COMMON_HPP_

#include <iostream>
#include <vector>

// load typedef for CONFIG_DEFAULT_FLOATING_POINT_TYPE
#include "../CConfig.hpp"

#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"



/**
 * Degree of freedoms for one point in tsunami simulation
 */
class CTsunamiSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h;	// height
	T hu;	// x-component of momentum
	T hv;	// y-component of momentum

	union	{
		T b;	// bathymetry
		T cfl1_timestep_size;	// max wave speed for flux computations
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b+h;
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T default_values[4])
	{
		h = default_values[0];
		hu = 0;
		hv = 0;
		b = -default_values[0];
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			const CTsunamiSimulationNodeData &d
	)	{
		os << "CTsunamiSimulationNodeData:" << std::endl;
		os << " + h: " << d.h << std::endl;
		os << " + hu: " << d.hu << std::endl;
		os << " + hv: " << d.hv << std::endl;
		os << " + b/max_wave_speed: " << d.b << std::endl;
		os << " + vx: " << d.hu/d.h << std::endl;
		os << " + vy: " << d.hv/d.h << std::endl;
		os << " + horizon: " << (d.h + d.b) << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,	///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		os << "Tsunami DOF Information:" << std::endl;
		os << " + h: " << h << std::endl;

		CONFIG_DEFAULT_FLOATING_POINT_TYPE thu = hu;
		CONFIG_DEFAULT_FLOATING_POINT_TYPE thv = hv;
		CTriangle_Projections::referenceToWorld(&thu, &thv, x_axis_x, x_axis_y);

		os << " + hu: " << thu << std::endl;
		os << " + hv: " << thv << std::endl;

		os << " + b/max_wave_speed: " << b << std::endl;

		os << " + vx: " << thu/h << std::endl;
		os << " + vy: " << thv/h << std::endl;
		os << " + horizon: " << (h + b) << std::endl;

		os << std::endl;
	}
};



#endif /* CTSUNAMITYPES_COMMON_HPP_ */
