/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUP_CELLDATA_HPP_
#define CSETUP_CELLDATA_HPP_


#if CONFIG_SUB_SIMULATION_TSUNAMI
#	include "simulations/hyperbolic_common/subsimulation_tsunami/kernels/modifiers/CSetup_CellData.hpp"
#elif CONFIG_SUB_SIMULATION_EULER
#	include "simulations/hyperbolic_common/subsimulation_euler/kernels/modifiers/CSetup_CellData.hpp"
#else
#	error "unknown sub-simulation"
#endif


#endif /* CSETUP_CELLDATA_HPP_ */
