/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 28, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CADAPTIVITY_REFINE_HANDLERS_HPP_
#define CADAPTIVITY_REFINE_HANDLERS_HPP_



public:
	inline void refine_l_r(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_parent_depth,

			CCellData *i_cCellData,
			CCellData *o_left_cCellData,
			CCellData *o_right_cCellData
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		refine(
			i_vertex_left_x,	i_vertex_left_y,
			i_vertex_right_x,	i_vertex_right_y,
			i_vertex_top_x,		i_vertex_top_y,

			i_normal_hyp_x,		i_normal_hyp_y,
			i_normal_right_x,	i_normal_right_y,
			i_normal_left_x,	i_normal_left_y,

			i_parent_depth,

			i_cCellData,
			o_left_cCellData,
			o_right_cCellData
		);

/*
 NOTE! VALIDATION SETUP IS DONE IN refine().
 VALIDATION SETUP IS NOT ALLOOWED HERE DUE TO SPECIFIC ORDER TO SETUP REFINED ELEMENTS

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_left_cellData->validation.setupLeftElementFromParent(&i_cellData->validation);
		o_right_cellData->validation.setupRightElementFromParent(&i_cellData->validation);
#endif
*/
	}



	inline void refine_ll_r(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CCellData *i_cCellData,
			CCellData *i_left_left_cCellData,
			CCellData *i_left_right_cCellData,
			CCellData *i_right_cCellData
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		CCellData tmp_cCellData;

		// LEFT + RIGHT
		refine_l_r(
					i_vertex_left_x,	i_vertex_left_y,
					i_vertex_right_x,	i_vertex_right_y,
					i_vertex_top_x,		i_vertex_top_y,

					i_normal_hyp_x,		i_normal_hyp_y,
					i_normal_right_x,	i_normal_right_y,
					i_normal_left_x,	i_normal_left_y,

					i_depth,

					i_cCellData, &tmp_cCellData, i_right_cCellData
				);



		// LEFT children (LEFT + RIGHT)
		T vertex_hyp_mid_edge_x = (i_vertex_left_x+i_vertex_right_x)*(T)0.5;
		T vertex_hyp_mid_edge_y = (i_vertex_left_y+i_vertex_right_y)*(T)0.5;

		refine_l_r(
					i_vertex_top_x,		i_vertex_top_y,
					i_vertex_left_x,	i_vertex_left_y,
					vertex_hyp_mid_edge_x, vertex_hyp_mid_edge_y,

					i_normal_left_x,	i_normal_left_y,
					i_normal_hyp_x,		i_normal_hyp_y,
					-i_normal_hyp_y,	i_normal_hyp_x,

					i_depth+1,

					&tmp_cCellData, i_left_left_cCellData, i_left_right_cCellData
				);
	}

	inline void refine_l_rr(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int depth,

			CCellData *i_cCellData,
			CCellData *left_cCellData,
			CCellData *right_left_cCellData,
			CCellData *right_right_cCellData
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		CCellData tmp_cCellData;

		// LEFT + RIGHT
		refine_l_r(
				i_vertex_left_x, i_vertex_left_y,
				i_vertex_right_x, i_vertex_right_y,
				i_vertex_top_x, i_vertex_top_y,
				i_normal_hyp_x, i_normal_hyp_y,
				i_normal_right_x, i_normal_right_y,
				i_normal_left_x, i_normal_left_y,
				depth,
				i_cCellData, left_cCellData, &tmp_cCellData
			);

		// RIGHT children (LEFT + RIGHT)
		T hyp_mid_edge_x = (i_vertex_left_x+i_vertex_right_x)*(T)0.5;
		T hyp_mid_edge_y = (i_vertex_left_y+i_vertex_right_y)*(T)0.5;

		refine_l_r(
					i_vertex_right_x, i_vertex_right_y,
					i_vertex_top_x, i_vertex_top_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					i_normal_right_x, i_normal_right_y,
					i_normal_hyp_y, -i_normal_hyp_x,
					i_normal_hyp_x, i_normal_hyp_y,

					depth+1,

					&tmp_cCellData, right_left_cCellData, right_right_cCellData);
	}


	inline void refine_ll_rr(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x, T i_normal_hyp_y,
			T i_normal_right_x, T i_normal_right_y,
			T i_normal_left_x, T i_normal_left_y,

			int depth,

			CCellData *i_cellData,
			CCellData *left_left_element,
			CCellData *left_right_element,
			CCellData *right_left_element,
			CCellData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		CCellData tmp_left_cCellData;
		CCellData tmp_right_cCellData;

		// RIGHT + LEFT
		refine_l_r(
					i_vertex_left_x, i_vertex_left_y,
					i_vertex_right_x, i_vertex_right_y,
					i_vertex_top_x, i_vertex_top_y,
					i_normal_hyp_x, i_normal_hyp_y,
					i_normal_right_x, i_normal_right_y,
					i_normal_left_x, i_normal_left_y,
					depth,
					i_cellData, &tmp_left_cCellData, &tmp_right_cCellData
				);

		T hyp_mid_edge_x = (i_vertex_left_x+i_vertex_right_x)*(T)0.5;
		T hyp_mid_edge_y = (i_vertex_left_y+i_vertex_right_y)*(T)0.5;

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					i_vertex_top_x, i_vertex_top_y,
					i_vertex_left_x, i_vertex_left_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					i_normal_left_x, i_normal_left_y,
					i_normal_hyp_x, i_normal_hyp_y,
					-i_normal_hyp_y, i_normal_hyp_x,

					depth+1,
					&tmp_left_cCellData, left_left_element, left_right_element
				);

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					i_vertex_right_x, i_vertex_right_y,
					i_vertex_top_x, i_vertex_top_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					i_normal_right_x, i_normal_right_y,
					i_normal_hyp_y, -i_normal_hyp_x,
					i_normal_hyp_x, i_normal_hyp_y,

					depth+1,

					&tmp_right_cCellData, right_left_element, right_right_element
				);
	}


#endif /* CADAPTIVITY_REFINE_HANDLERS_HPP_ */
