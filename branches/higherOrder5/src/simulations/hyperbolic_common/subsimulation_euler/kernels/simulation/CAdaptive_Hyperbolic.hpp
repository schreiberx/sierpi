/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#include "simulations/hyperbolic_common/subsimulation_generic/CConfig.hpp"



#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0

#include "CAdaptive_Hyperbolic_0thOrder.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_0thOrder CAdaptive_Hyperbolic;
	}
}

#else
#	error "only 0th order simulation available"
#endif
