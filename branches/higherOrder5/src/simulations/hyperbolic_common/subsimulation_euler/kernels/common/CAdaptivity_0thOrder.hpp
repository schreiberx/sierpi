/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_

#include <limits>
#include <stdexcept>

#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_0thOrder
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;

	// typedef for CAdaptivity_default_handlers (see include at end of this file)
	typedef CAdaptivity_0thOrder CAdaptivity_Common;
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/common/CAdaptivity_default_handlers.hpp"


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CDatasets *cDatasets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * constructor
	 */
	CAdaptivity_0thOrder()	:
		cathetus_side_length(-1),
		cDatasets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}




	/**
	 * setup both refined elements
	 */
	inline void refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_parent_depth,

			CCellData *i_cCellData,
			CCellData *o_left_cCellData,
			CCellData *o_right_cCellData
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		assert (cDatasets != nullptr);

		T lmx, lmy, rmx, rmy;

		CTriangle_Tools::computeAdaptiveSamplingPointForLeftAndRightChild(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				&lmx, &lmy,
				&rmx, &rmy
			);


		/*
		 * |\_
		 * |  \_
		 * |    \_
		 * |    / \_
		 * |  /     \_
		 * |/_________\
		 *
		 * use X-Axis for projection
		 */
		o_left_cCellData->dofs[0] = i_cCellData->dofs[0];

		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&o_left_cCellData->dofs[0].ru,
				&o_left_cCellData->dofs[0].rv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);


		o_right_cCellData->dofs[0] = i_cCellData->dofs[0];

		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&o_right_cCellData->dofs[0].ru,
				&o_right_cCellData->dofs[0].rv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		o_left_cCellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
		o_right_cCellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();


#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_left_cCellData->refine = false;
		o_left_cCellData->coarsen = false;
		o_right_cCellData->refine = false;
		o_right_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRefineLeftAndRight(
				i_normal_hyp_x,		i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_parent_depth,
				&o_left_cCellData->validation,
				&o_right_cCellData->validation
			);
#endif

	}


	/**
	 * setup coarsed elements
	 */
	inline void coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CCellData *o_cCellData,
			CCellData *i_left_cCellData,
			CCellData *i_right_cCellData
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		{
			T mx = (i_vertex_left_x + i_vertex_right_x)*(T)0.5;
			T my = (i_vertex_left_y + i_vertex_right_y)*(T)0.5;

			i_left_cCellData->validation.testVertices(i_vertex_top_x, i_vertex_top_y, i_vertex_left_x, i_vertex_left_y, mx, my);
			i_right_cCellData->validation.testVertices(i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y, mx, my);
		}
#endif

		assert (cDatasets != nullptr);

		T mx, my;
		CTriangle_Tools::computeAdaptiveSamplingPoint(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,
				&mx, &my
			);


		T left_tmp_hu = i_left_cCellData->dofs[0].ru;
		T left_tmp_hv = i_left_cCellData->dofs[0].rv;

		/*
		 * project from left child to coarse cell basis
		 *
		 *
		 * |\
		 * |  \
		 * | L  \
		 * |------
		 * |    /
		 * |  /
		 * |/
		 */
		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&left_tmp_hu,
				&left_tmp_hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);


		T right_tmp_hu = i_right_cCellData->dofs[0].ru;
		T right_tmp_hv = i_right_cCellData->dofs[0].rv;

		/*
		 * project from right child to coarse cell basis
		 *
		 *      /|\
		 *    /  |  \
		 *  /    | R  \
		 * ------|------
		 */
		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&right_tmp_hu,
				&right_tmp_hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);

		o_cCellData->dofs[0].r = (i_left_cCellData->dofs[0].r + i_right_cCellData->dofs[0].r) * (T)0.5;
		o_cCellData->dofs[0].ru = (left_tmp_hu + right_tmp_hu) * (T)0.5;
		o_cCellData->dofs[0].rv = (left_tmp_hv + right_tmp_hv) * (T)0.5;
		o_cCellData->dofs[0].e = (i_left_cCellData->dofs[0].e + i_right_cCellData->dofs[0].e) * (T)0.5;

		o_cCellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();


#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_cCellData->refine = false;
		o_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cCellData->validation.setupCoarsen(
				i_normal_hyp_x,		i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_depth,
				&i_left_cCellData->validation,
				&i_right_cCellData->validation
		);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
