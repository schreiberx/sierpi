/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CEDGE_COMM_NORMALS_DEPTH_PARALLEL_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CEDGE_COMM_NORMALS_DEPTH_PARALLEL_HPP_


#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "../subsimulation_generic/kernels/simulation/CEdgeComm_Hyperbolic.hpp"
#include "../datasets_common/CParameters_Datasets.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_EdgeComm_Normals_Depth
{
	/// pointer to specialized traversator
	class cSpecialized_EdgeComm_Normals_Depth_Private *generic_traversator;

public:
	/// publish CKernelClass type to make it known to fluxCommTraversals
	typedef typename sierpi::kernels::CEdgeComm_Hyperbolic CKernelClass;


	/// reference to kernel class to call op_edge_edge
	CKernelClass *cKernelClass;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	typedef T TReduceValue;

public:
	CSpecialized_EdgeComm_Normals_Depth();

	virtual ~CSpecialized_EdgeComm_Normals_Depth();

	void actionFirstPass(
			CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks
		);

	void actionMiddlePass_computeClusterBorderCFL(
			CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
			TReduceValue *o_reduceValue
		);

	void actionSecondPass_Serial(
			CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
			T i_timestep_size
		);

	void actionSecondPass_Parallel(
			CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
			T i_timestep_size
		);

	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
	);

	void setup_Cluster(
			CSpecialized_EdgeComm_Normals_Depth &p_parent,
			CTriangle_Factory &p_triangleFactory
	);

	void setParameters(
			T i_delta_timestep,
			T i_square_side_length,
			T i_gravity,
			CDatasets *i_cDatasets
	);

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
	void setAdaptivityParameters(
			T i_refine_threshold,
			T i_coarsen_threshold
	);
#endif


	void setBoundaryCondition(
			EBoundaryConditions i_eBoundaryCondition
	);

	void setBoundaryDirichlet(
			const CHyperbolicTypes::CSimulationTypes::CEdgeData *i_value
		);

	static void setupMatrices();

	T getTimestepSize();
};


}
}

#endif
