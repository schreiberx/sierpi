// use opengl 3.3 core shaders
#version 330

in vec2 rast_texture_coord;
out vec4 frag_data;

uniform sampler2D sampler;


void main(void)
{
	vec2 texel = texelFetch(sampler, ivec2(gl_FragCoord.xy), 0).rg;

	frag_data.r = texel.r / texel.g;
}
