#! /bin/bash

cd ..

scons --xml-config=./scenarios/single_wave_on_simple_beach_1d.xml -j 4

for d in `seq 0 20`; do
	MAX=`./build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c scenarios/single_wave_on_simple_beach_1d.xml -d $d -a 0 | grep "MaxRunup:" | tail -n 1`
	echo "$d	$MAX"
done
