'''
Created on Feb 6, 2012

@author: flo
'''

import subprocess, sys, os, os.path
from datetime import date

date = str(date.today()).replace("-", "_")

# simulation optional arguments
options = "-a 0 -d %s -t %s"

# maximum testing depth
maxdepth = 12
if len(sys.argv) > 1:
	try:
		maxdepth = int(sys.argv[1])
	except:
		pass


#scons --cppcompiler=gnu --threading=off --gui=off --mode=release --simulation=tsunami_serial_regular -j8


# executables
serial_regular = "./../../build/sierpi_gnu_tsunami_serial_regular_release"
serial_regular_recursive = "./../../build/sierpi_gnu_tsunami_serial_regular_recursive_release"
serial_regular_table = "./../../build/sierpi_gnu_tsunami_serial_regular_table_release"
serial = "./../../../branches/singleFluxEvaluation/build/sierpi_gnu_tsunami_serial_release"

# remote paths for pproc
remdir = os.path.realpath(__file__).replace(__file__, "")
try:
	if len(sys.argv) > 3:
		remdir = sys.argv[3]
	if sys.argv[1] == "remote" or sys.argv[2] == "remote" or sys.argv[2] == "home":
		serial_regular_recursive = "%ssierpi_gnu_tsunami_serial_regular_recursive_release" % remdir
		serial_regular_table = "%ssierpi_gnu_tsunami_serial_regular_table_release" % remdir
		serial = "%ssierpi_gnu_tsunami_serial_release" % remdir
	# c++ library fix for pproc
	if sys.argv[1] == "remote" or sys.argv[2] == "remote":
		os.putenv("LD_LIBRARY_PATH", "/home_shared/kleinfl/lib/")
except:
	pass

# output
outputfile = open("%soutput_serial_regular_%s.txt" % (remdir, date), "w")

# repititions per depth
testrounds = 3

# adapt number of timesteps to current depth
def timesteps(depth):
	return max(2 ** (27 - depth), 10000)

# column headers
outputfile.write("# depth	MTPS_serial	SFS_serial	MTPS_regular_rec	SFS_regular_rec	MTPS_regular_table	SFS_regular_table	timesteps\n")

for depth in range(1, maxdepth + 1):
	result = [str(depth)]
	for exe in [serial, serial_regular_recursive, serial_regular_table]:
		print "depth %s: %s" % (depth, exe)
		# | grep " MTPS "
		mtps_sum = 0.0
		sfs_sum = 0.0
		mtps_string = ""
		for i in range(testrounds):
			output = subprocess.Popen(['%s %s' % (exe, options % (depth, timesteps(depth)))], stdout=subprocess.PIPE, shell=True).communicate()[0].splitlines()
			for line in output:
				# simulation time
				if " SFS " in line:
				#	result.append(line.split(" SFS ")[0] + "s")
					sfs_sum += float(line.split(" SFS ")[0])
				
				# megatriangles per second
				if " MTPS " in line:
					mtps_value = float(line.split(" MTPS")[0])
					mtps_sum += mtps_value
					mtps_string += "%s " % mtps_value
		mtps_string += "-> %s" % (mtps_sum / testrounds)
		print mtps_string
		result.append(mtps_sum / testrounds)
		result.append(sfs_sum / testrounds)
	
	result.append(timesteps(depth))
	outputfile.write("\t".join(str(element) for element in result) + "\n")


outputfile.close()