from glob import glob
import getopt, sys, os

def do_stuff(d):
    # Part I: resize images to a human format
    print ("Working in %s"%d)
    if not os.path.isdir(d):
        print ("%s is not a directory."%d)
    imgs = glob(os.path.abspath(os.path.dirname(d+"/"))+"/*.png")
    for i in imgs:
        print ("Processing %s..."%i)
        os.system("convert '%s' -trim '%s'"%(i,i))
        #os.system("convert '%s' -resize 50%% '%s'"%(i,i))
    print("Done.")
    # Part II: create an appropriate tex-file
    f = open(d+"/figure.tex", "w")
    f.write("\\begin{figure}[ht]\n  \\centering\n")
    for x in xrange(1, len(imgs)/2+1):
        f.write("  \subfigure[$%s_%d$: 1/0.04]{\n"%(["p","P"][x>len(imgs)/4],(x-1)%(len(imgs)/4)+1))
        f.write("    \includegraphics[scale=\\autozoomfactor]{%s/%d_h.png}\n"%(os.path.basename(d),x))
        f.write("    \includegraphics[scale=\\autozoomfactor]{%s/%d_u.png}\n"%(os.path.basename(d),x))
        f.write("  }\n")
    f.write("  \\caption{}\n")
    f.write("  \\label{}\n")
    f.write("\\end{figure}\n")
    

def main():
    opts, args = getopt.getopt(sys.argv[1:], "")
    if len(args)==0:
        print "No directory given. Leaving."
        return
    directory = args[0]
    do_stuff(directory)

if __name__=="__main__":
    main()
