# TODO:
# * in addition, make main routine such that it accepts a variable number of functions, plotting each one in a single gnuplot window
# * test if it would be faster to use gnuplot's "multipleplot" feature.
# * correct order of variables
# * possibility to choose domain for plot variables/parameters dynamically
# * 2D-Plot: a vertical line showing the average of the respective quantity might be helpful
# * could be nice: possibility to send custom command to (all instances of) gnuplot

from numpy import *
import Gnuplot, Gnuplot.funcutils
import pygtk
import gtk
import re
import optparse
import time

class SimplePlotter:
    """This class encapsules a whole bunch of things useful for "interactive" 
    gnuplotting. Amongst others:
        * self.window: Reference to the window used for interaction
        * self.sliders: Sliders for variables
        * self.xratios/self.yradios: Which variables to use as axes?
        * self.variables: Names of the variables
        * self.func: an array of the functions in their raw form
        * self.xyfunc: an array of the functions, x/y-coords already properly substituted"""

    def delete_event(self, widget, event, data=None):
        """Things to do when window is deleted."""
        return False

    def destroy(self, widget, data=None):
        """Things to do when window is destroyed."""
        gtk.main_quit()

    def redraw(self, adj, data=None):
        """This is invoked to generate a plot for *all* given functions.
        Delegates the work to the function drawspecific."""
        self.adjustvars()
        for i in xrange(len(self.func)):
            self.gnuplot[i]('replot')

    def adjustvars(self):
        for i in xrange(len(self.func)):
            for (varname, varval) in zip(self.variables, [s.get_value() for s in self.sliders]):
                self.gnuplot[i]('%s=%f' % (varname, varval))

    def adjuststuff(self, widget, data=None):
        """Determines which variables are plot-axes-variables and wich
        ones are parameters. Decides whether to use a 2D- or a 3D-plot."""
        subs = []
        xvar = self.variables[0]
        yvar = self.variables[0]
        for (i,r) in enumerate(self.xradios):
            if r.get_active():
                xvar = self.variables[i]
        for (i,r) in enumerate(self.yradios):
            if r.get_active():
                yvar = self.variables[i]
        subs.append((xvar, "x"))
        subs.append((yvar, "y"))
        self.xyfunc = []
        for f in self.func:
            tmp = f
            for (s,t) in subs:
                tmp = tmp.replace(s,t)
            self.xyfunc.append(tmp)
        for i in xrange(len(self.func)):
            gp = self.gnuplot[i]
            if xvar != yvar:
                gp("set xrange " + self.urange)
                gp("set yrange " + self.hrange)
                gp("set pm3d")
                self.adjustvars()
                gp('splot %s' % self.xyfunc[i])
            else:
                gp("set autoscale")
                gp("unset pm3d")
                self.adjustvars()
                gp('plot %s' % self.xyfunc[i])

    def adjustment_from_range(self,r):
        """Returns a gtk.Adjustment from a "gnuplot"-range.
        A gnuplot-range has the format [-3:67]."""
        _r = r[1:-1]
        _r = _r.split(":")
#        print _r
        l = float(_r[0])
        h = float(_r[1])
        return gtk.Adjustment((l+h)/2., l, h, step_incr=0.5)

    def do_settings(self, widget):
        """Determines the settings and applies them (these may be varied 
        for efficiency)."""
        for gp in self.gnuplot:
            gp("set samples %d, %d"%(self.samples_spin.get_value(),self.samples_spin.get_value()))
            gp("set isosamples %d, %d"%(self.isosamples_spin.get_value(),self.isosamples_spin.get_value()))
            gp("replot")

    def init_settings_page(self):
        """Fills the settings page."""
        # Adjust slider/plotter parameters
#        vargrid = gtk.Grid()
#        vargrid.add()
#        vargrid.attach()
        # samples
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("Samples: "))
        self.samples_spin = gtk.SpinButton(gtk.Adjustment(100,2,1000,1))
        hbox.pack_start(self.samples_spin)
        self.settings_box.pack_start(hbox)
        self.samples_spin.connect("value_changed", self.do_settings) 
        # isosamples
        hbox = gtk.HBox()
        hbox.pack_start(gtk.Label("Isosamples: "))
        self.isosamples_spin = gtk.SpinButton(gtk.Adjustment(10,2,1000,1))
        hbox.pack_start(self.isosamples_spin)
        self.settings_box.pack_start(hbox)
        self.isosamples_spin.connect("value_changed", self.do_settings)

    def init_variables_page(self):
        """Creates sliders and radio buttons for the single variables."""
        # create elements for variables!!
        self.variables = list(set(re.findall(
                    "[a-zA-Z_]+[1-9]*",self.func[0] +"+"+ self.func[1])))
                    #"[uhUH](\[.*?\])?",func1 +"+"+ func2))) # this was the original!
        self.variables = filter(lambda x: x not in ["abs", "sin", "cos", "log", "tan", "e"], self.variables)
        # an auxiliary function to sort the variables properly
        def varkey(x):
            tmp = x.split("_")
            return (int(tmp[1]),255-ord(tmp[0]))
            # return x
        self.variables.sort(key=varkey)
        self.sliders = []
        self.xradios = []
        self.yradios = []
        for v in self.variables:
            # a whole container for each variable is necessary
            newvbox = gtk.VBox()
            self.variable_hbox.add(newvbox)
            # description
            newvbox.pack_start(gtk.Label(v), False, False)
            # options for x
            group = None if len(self.xradios)==0 else self.xradios[0]
            newx = gtk.RadioButton(group, "")
            self.xradios.append(newx)
            newvbox.pack_start(newx, False, False)
            newx.connect("toggled", self.adjuststuff)
            # options for x
            group = None if len(self.yradios)==0 else self.yradios[0]
            newy = gtk.RadioButton(group, "")
            self.yradios.append(newy)
            newvbox.pack_start(newy, False, False)
            newy.connect("toggled", self.adjuststuff)
            # slider
            newadjustment = self.adjustment_from_range(self.urange)
            if v[0].lower() == "h":
                newadjustment = self.adjustment_from_range(self.hrange)
            newadjustment.connect("value_changed", self.redraw)
            self.sliders.append(gtk.VScale(newadjustment))
            self.sliders[-1].set_size_request(10,200)
            newvbox.add(self.sliders[-1])
    def init_gtk_layout(self):
        # toplevel window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.delete_event)
        self.window.connect("destroy", self.destroy)
        self.window.set_border_width(10)
        # user interface stuff!
        self.notebook = gtk.Notebook()
        self.window.add(self.notebook)
        self.variable_hbox = gtk.HBox()
        self.settings_box = gtk.VBox()
        self.notebook.append_page(self.variable_hbox, gtk.Label("Variables/Parameters"))
        self.notebook.append_page(self.settings_box, gtk.Label("Settings"))
        self.window.add(self.variable_hbox)
        # initialize single pages of the notebook
        self.init_variables_page()
        self.init_settings_page()
        self.window.show_all()

    def __init__(self, func1="h[1]+h[2]*h[3]", func2="h[1]+h[2]", urange="[-5:5]", hrange="[8:12]"):
        """Initialization of the window."""
        # Configure Gnuplot settings
        self.urange = urange
        self.hrange = hrange
        self.plotcommand = "plot"
        self.func = [func1, func2]
        # timestamp needed so that there are not too many gnuplot calls!
        self.timestamp = 0
        # initialize gnuplot for me
        self.gnuplot = []
        for f in self.func:
            newgp = Gnuplot.Gnuplot(debug=0)
            self.gnuplot.append(newgp)
            self.gnuplot[-1]("unset key")
            self.gnuplot[-1]("set xrange " + urange)
            self.gnuplot[-1]("set yrange " + hrange)
            self.gnuplot[-1]("set pm3d")
        # Initialize Gtk layout
        self.init_gtk_layout()
        self.adjuststuff(None)

    def main(self):
        """Just fires the gtk main loop."""
        gtk.main()

def prettify_function(f):
    """Brings functions from maple to gnuplot-syntax."""
    result = f
    result = re.sub("([a-z_]*)\[(.*?)\]", "\g<1>_\g<2>", result) 
    result = result.replace("^","**")
    return result

def make_range(start, end):
    return "[%d:%d]" % (start, end)

def get_range(range):
    result = re.match('\[(.*?):(.*?)\]', range)
    return result.group(1), result.group(2)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option("--urange", default="[-5:5]")
    parser.add_option("--hrange", default="[8:12]")
    opts, args = parser.parse_args()
    simpleplotter = SimplePlotter(prettify_function(args[0]), prettify_function(args[1]), 
                                  urange=opts.urange, hrange=opts.hrange
            )
    simpleplotter.main()

