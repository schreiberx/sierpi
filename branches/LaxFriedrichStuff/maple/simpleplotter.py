import matplotlib.pyplot as plt
import gtk
from matplotlib.backends.backend_gtk import FigureCanvasGTK as FigureCanvas
from matplotlib.figure import Figure
import random
import time
import gobject
import optparse
import sys
import re
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

functionstring="h[1]*u[2]-h[2]"

# We set a simple GTK window (not GTKAgg)
win = gtk.Window()
win.connect("destroy", lambda x: gtk.main_quit())
win.set_default_size(400,300)
win.set_title("Prueba de Matplot")

# We create the Figure and the plot
f = Figure(figsize=(5,4), dpi=72)
a = f.gca(projection="3d")

# We stablish the values for X (representig time)
x = 0
t = [0]

# We stablish the values for Y (representing any signal)
y = 0
ard1 = [0]

# Create a plot and set some parameters
a.plot(t, ard1 , '-')
a.grid(True)
a.set_xlabel('Time')
a.set_ylabel('Value')

# This is the function that waits 1 second to redraw, then sets a random
# value for y and adds it to the 'ard1' array. For 't' we add 1 to the x
# value so it will simulate seconds.
def counter():
    x = 0
    userinput = "h"
    while userinput!="q":
        # time.sleep(1)
        x = x+0.1;
        userinput = raw_input("Waiting for input.")
        y = random.randint(0,1024)
        t.append(x)
        ard1.append(y)
        X = np.arange(-5, 5, 0.25)
        Y = np.arange(-5, 5, 0.25)
        X, Y = np.meshgrid(X, Y)
        R = np.sqrt(x*X**2 + Y**2)
        Z = np.sin(R)
        a.plot_surface(X,Y,Z,rstride=1, cstride=1, cmap=cm.jet,
                       linewidth=0, antialiased=False)
        f.clear()
        f.canvas.draw()
    return

# Create the widget, a FigureCanvas containing our Figure
canvas = FigureCanvas(f)
win.add(canvas)

# This gobject tells GTK to run the function while idle
gobject.idle_add(counter)

# And here we go!
def main():
    parser = optparse.OptionParser()
    parser.add_option("-U")
    parser.add_option("-H")
    opts, args = parser.parse_args()
    global functionstring
    functionstring = args[0]
    global variables
    variables = list(set(re.findall(
                    "[uhUH]\[.*?\]","u[1]*h[2]-h[1]*h[62]-u[12]")))
    print args
    win.show_all()
    gtk.main()

if __name__ == "__main__":
    main()
