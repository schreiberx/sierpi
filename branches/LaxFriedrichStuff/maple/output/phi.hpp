static inline T phi_0(T x, T y){ return -2 * y + 1; }
static inline T phi_1(T x, T y){ return -1 + 2 * x + 2 * y; }
static inline T phi_2(T x, T y){ return 1 - 2 * x; }
static inline T phi_0_dx(T x, T y){ return 0; }
static inline T phi_1_dx(T x, T y){ return 2; }
static inline T phi_2_dx(T x, T y){ return -2; }
static inline T phi_0_dy(T x, T y){ return -2; }
static inline T phi_1_dy(T x, T y){ return 2; }
static inline T phi_2_dy(T x, T y){ return 0; }
