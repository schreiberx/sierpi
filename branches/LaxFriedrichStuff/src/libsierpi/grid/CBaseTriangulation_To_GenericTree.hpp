/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CBaseTriangulation_To_GenericTree.hpp
 *
 *  Created on: Oct 12, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CBASETRIANGULATION_TO_GENERICTREE_HPP_
#define CBASETRIANGULATION_TO_GENERICTREE_HPP_

#include "CDomain_BaseTriangulation.hpp"
#include "../cluster/CDomainClusters.hpp"


namespace sierpi
{

/**
 * \brief converts a base triangulation to a generic tree structure
 *
 * base triangulation is given via CDomain_BaseTriangulation.
 */
template <typename CSimulation_ClusterHandler>
class CBaseTriangulation_To_GenericTree
{
	typedef CDomain_BaseTriangle<CSimulation_ClusterHandler> CDomain_BaseTriangle_;
	typedef CDomain_BaseTriangulation<CSimulation_ClusterHandler> CDomain_BaseTriangulation_;

	typedef CCluster_TreeNode<CSimulation_ClusterHandler> CCluster_TreeNode_;
	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CEdgeComm_InformationAdjacentCluster_;
	typedef CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;


private:
	/**
	 * maximum allowed unique id depth for root triangles
	 */
	int max_unique_id_depth;


	/**
	 * unique id counter for initialization
	 */
	int unique_id_counter;


	/*
	 * edge comm elements along a single cathetus
	 */
	int cat_edge_comm_elements;

	/*
	 * edge comm elements along a hypotenuse
	 */
	int hyp_edge_comm_elements;

	/*
	 * initial depth and thus number of triangles to setup stacks
	 */
	int initial_refinement_depth;

	/*
	 * minimum relative refinement depth
	 */
	int min_relative_depth;

	/*
	 * maximum relative refinement depth
	 */
	int max_relative_depth;

	/**
	 * pointer to domain root triangulation
	 */
	CDomain_BaseTriangulation_ *cDomain_BaseTriangulation;


	/**
	 * mpi rank
	 */
	int mpi_rank;

public:
	/**
	 * number of base triangles
	 */
	unsigned long long number_of_initial_local_base_clusters;
	unsigned long long number_of_initial_global_base_clusters;

	/**
	 * constructor
	 */
	CBaseTriangulation_To_GenericTree()	:
		cat_edge_comm_elements(-1),
		hyp_edge_comm_elements(-1)
	{
	}


private:
	void _setupLeafCluster(
		CDomain_BaseTriangle_ &cDomain_BaseTriangle,
		CGenericTreeNode_ *i_ptr_parent_cGeneric_TreeNode,
		CGenericTreeNode_ *o_cGeneric_TreeNode
	)
	{
		/*
		 * setup unique id and increment unique id counter
		 */
		cDomain_BaseTriangle.uniqueId.setup(unique_id_counter + (1 << max_unique_id_depth), max_unique_id_depth);

#if CONFIG_ENABLE_MPI
		if (mpi_rank == cDomain_BaseTriangle.mpi_rank)
		{
#endif

			// then allocate the cluster tree node
			CCluster_TreeNode_ *cClusterTreeNode = new CCluster_TreeNode_(
					cDomain_BaseTriangle.cTriangleFactory	// triangle factory
				);

			// setup pointers
			o_cGeneric_TreeNode->cCluster_TreeNode = cClusterTreeNode;
			cClusterTreeNode->cGeneric_TreeNode = o_cGeneric_TreeNode;

			/*
			 * update cluster node's adaptive depth values!
			 */
			assert(cClusterTreeNode->cTriangleFactory.recursionDepthFirstRecMethod == 0);
			cClusterTreeNode->cTriangleFactory.minDepth = min_relative_depth;
			cClusterTreeNode->cTriangleFactory.maxDepth = max_relative_depth;

			/*
			 * setup unique id and increment unique id counter
			 */
			cClusterTreeNode->uniqueId = cDomain_BaseTriangle.uniqueId;

			// setup pointer to allocated cluster tree node
			cDomain_BaseTriangle.cCluster_user_ptr = cClusterTreeNode;

			// setup genericTreeNode's variable to the parent node
			o_cGeneric_TreeNode->parent_node = i_ptr_parent_cGeneric_TreeNode;

			/*
			 * convert edges of type OLD to type NEW due to parallelization
			 */
			if (cClusterTreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cClusterTreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cClusterTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

			number_of_initial_local_base_clusters++;

#if CONFIG_ENABLE_MPI
		}
#endif
	}

private:
	void _setup_GenericTreeParentAndClusters(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGenericTreeNode_ *io_parent_GenericTreeNode,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		assert(current_depth <= max_unique_id_depth);

		// left traversal
		io_parent_GenericTreeNode->first_child_node =
			_setup_GenericTreeChildAndClusters(
				iter,		// reference to iterator
				io_parent_GenericTreeNode,	// this (parent) node
				current_depth
			);

		if (iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end())
		{
			// right traversal
			io_parent_GenericTreeNode->second_child_node =
				_setup_GenericTreeChildAndClusters(
					iter,		// reference to iterator
					io_parent_GenericTreeNode,	// this (parent) node
					current_depth
				);
		}
	}

private:
	/**
	 * recursive method to insert the new generic tree-node given by `iter` below the given parent_node
	 */
	CGenericTreeNode_ *_setup_GenericTreeChildAndClusters(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGenericTreeNode_ *i_ptr_parent_GenericTreeNode,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;
		assert(current_depth <= max_unique_id_depth);

		// leaf reached => create cluster
		if (current_depth == max_unique_id_depth)
		{
			/****************************************************************
			 * allocate new cluster node
			 ****************************************************************/

			// first allocate the generic tree node
			CGenericTreeNode_ *cGeneric_TreeNode = new CGenericTreeNode_(i_ptr_parent_GenericTreeNode, true);

			_setupLeafCluster(cDomain_BaseTriangle, i_ptr_parent_GenericTreeNode, cGeneric_TreeNode);

			unique_id_counter++;

			// increment the iterator
			iter++;

			// return pointer to this node
			return cGeneric_TreeNode;
		}

		current_depth++;

		// create a new tree node with attached leaves
		/****************************************************************
		 * allocate new generic tree node
		 ****************************************************************/
		CGenericTreeNode_ *new_node = new CGenericTreeNode_(i_ptr_parent_GenericTreeNode, true);

		_setup_GenericTreeParentAndClusters(iter, new_node, current_depth);

		return new_node;
	}


	/**
	 * setup a root sub-cluster tree node for a given cluster
	 *
	 * the setup includes the information about the RLE encoded communication between the clusters
	 */
private:
	void _setup_ClusterEdgeCommunicationInformation(
			CCluster_TreeNode_ *io_clusterTreeNode,
			CDomain_BaseTriangle_ &i_cDomain_BaseTriangle
	)
	{
		/**
		 * setup hypotenuse adjacency information stack
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge != nullptr)
		{
			io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ &adjacentTriangleInformation = io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation.clusterTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.clusterTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->uniqueId;
			adjacentTriangleInformation.comm_elements = hyp_edge_comm_elements;
		}

		/**
		 * setup catheti adjacency information stack
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge != nullptr)
		{
			io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ &adjacentTriangleInformation = io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation.clusterTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.clusterTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->uniqueId;
			adjacentTriangleInformation.comm_elements = cat_edge_comm_elements;
		}


		if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge)
		{
			CEdgeComm_InformationAdjacentCluster_ *adjacentTriangleInformation;

			/**
			 * for odd traversal type, the adjacent cluster comm information has to be inserted in reversed order
			 */
			if (io_clusterTreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
			{
				io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.insert(
						io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin(),
						CEdgeComm_InformationAdjacentCluster_()
					);
				adjacentTriangleInformation = &(io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.front());
			}
			else
			{
				io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
						CEdgeComm_InformationAdjacentCluster_()
					);
				adjacentTriangleInformation = &(io_clusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back());
			}

			if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation->clusterTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation->clusterTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation->mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation->uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->uniqueId;
			adjacentTriangleInformation->comm_elements = cat_edge_comm_elements;
		}
	}



#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	/**
	 * search for the adjacent matching vertices and store them
	 */
	bool _setupVertexCommInformation(
			std::list<CEdgeComm_InformationAdjacentCluster_> &io_vertexInformationAdjacentClusters,	///< list to fill with information about adjacent clusters
			CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_baseTriangle,
			const CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_originBaseTriangle,			///< origin base triangle of this vertex search
			const CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_prevBaseTriangle,			///< base triangle triggering the execution of this function to find the matching edge and to avoid running backwards

			bool i_adjacentEdgeInClockwiseOrder						///< select vertex by taking the 1st vertex in clock order from the adjacent triangles point of view
	)
	{
		if (i_baseTriangle == nullptr)
			return false;

//		std::cout << "origin: " << i_originBaseTriangle->cCluster_user_ptr->uniqueId << std::endl;
//		std::cout << "base: " << i_baseTriangle->cCluster_user_ptr->uniqueId << std::endl;

		assert(io_vertexInformationAdjacentClusters.size() < 8);

		/*
		 * stop setting up the tree when we are back at the base triangle where we started the vertex comm search
		 */
		if (i_baseTriangle == i_originBaseTriangle)
		{
			/*
			 * remove topmost element!
			 */
			io_vertexInformationAdjacentClusters.pop_back();
			return true;
		}

#if DEBUG
		for(auto iter = io_vertexInformationAdjacentClusters.begin(); iter != io_vertexInformationAdjacentClusters.end(); iter++)
		{
			assert((*iter).uniqueId != i_baseTriangle->cCluster_user_ptr->uniqueId);
		}
#endif

		if (i_prevBaseTriangle != i_originBaseTriangle)
		{
			/*
			 * add vertex comm information
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				io_vertexInformationAdjacentClusters.push_back(
						CEdgeComm_InformationAdjacentCluster_(i_baseTriangle->cCluster_user_ptr, i_baseTriangle->cCluster_user_ptr->uniqueId, 0)
					);
			}
			else
			{
				io_vertexInformationAdjacentClusters.push_front(
						CEdgeComm_InformationAdjacentCluster_(i_baseTriangle->cCluster_user_ptr, i_baseTriangle->cCluster_user_ptr->uniqueId, 0)
					);
			}
		}


		if (i_baseTriangle->adjacent_triangles.hyp_edge == i_prevBaseTriangle)
		{
			/*
			 * search on hypotenuse edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.right_edge == i_prevBaseTriangle)
		{
			/*
			 * search on right edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.left_edge == i_prevBaseTriangle)
		{
			/*
			 * search on left edge for matching source edge
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		std::cerr << "INVALID STATE REACHED WHEN SEARCHING FOR VERTICES" << std::endl;
		assert(false);

		return false;
	}


	/*
	 * setup a root sub-cluster tree node for a given cluster after edge comm information was set-up !!!
	 *
	 * this setup inserts 0-length encoded edge communication information to account for vertex data
	 */
private:
	void _setup_ClusterVertexCommunicationInformation(
			CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_baseTriangle
	)
	{
		/*
		 * here we start setting up the 0-length encoded vertex data.
		 *
		 * for the vertex shared by the left and hyp edge or the right and hyp edge, either the hypotenuse
		 * or the left/right RLE encoded part has to be used for updating the vertex.
		 */

		/*
		 * storage containers for left, right and top vertices
		 */
		std::list<CEdgeComm_InformationAdjacentCluster_>
				leftVertexInformationAdjacentClusters,
				rightVertexInformationAdjacentClusters,
				topVertexInformationAdjacentClusters;

//		std::cout << "search on left edge for top vertices" << std::endl;
		// search on left edge for top vertices
		bool allLeftVerticesFound = _setupVertexCommInformation(
				leftVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.left_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
			);
//		std::cout << "search on left edge for top vertices end" << std::endl;

//		std::cout << "search on right edge for top vertices" << std::endl;
		// search on right edge for top vertices
		bool allTopVerticesFound = _setupVertexCommInformation(
				topVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
			);
//		std::cout << "search on right edge for top vertices end" << std::endl;

//		std::cout << "search on hyp edge for right vertices" << std::endl;
		// search on hyp edge for right vertices
		bool allRightVerticesFound = _setupVertexCommInformation(
				rightVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
		);
//		std::cout << "search on hyp edge for right vertices end" << std::endl;

//		std::cout << "search on right edge for more right vertices" << std::endl;
		if (!allRightVerticesFound)
		{
			// search on right edge for more right vertices
			_setupVertexCommInformation(
				rightVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}

//		std::cout << "search on right edge for more right vertices end" << std::endl;

//		std::cout << "search on left edge for more top vertices" << std::endl;
		if (!allTopVerticesFound)
		{
			// search on left edge for more top vertices
			_setupVertexCommInformation(
				topVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.left_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}
//		std::cout << "search on left edge for more top vertices end" << std::endl;

//		std::cout << "search on hyp edge for more left vertices" << std::endl;
		if (!allLeftVerticesFound)
		{
			// search on hyp edge for more left vertices
			_setupVertexCommInformation(
				leftVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}
//		std::cout << "search on hyp edge for more l vertices end" << std::endl;

#if 0
		std::cout << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << " TRIANGLE: " << i_baseTriangle->cCluster_user_ptr->uniqueId << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;
		if (leftVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " left vertex adjacent clusters:" << std::endl;

			for (auto iter = leftVertexInformationAdjacentClusters.begin();
					iter != leftVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}

		if (rightVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " right vertex adjacent clusters:" << std::endl;

			for (auto iter = rightVertexInformationAdjacentClusters.begin();
					iter != rightVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}

		if (topVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " top vertex adjacent clusters:" << std::endl;

			for (auto iter = topVertexInformationAdjacentClusters.begin();
					iter != topVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}
#endif

		/*
		 * insert data into RLE edge comm information
		 */
		CCluster_TreeNode_ *cCluster_TreeNode = i_baseTriangle->cCluster_user_ptr;


		/*
		 * Extend RLE edge comm information for hypotenuse.
		 *
		 * The vertices shared by both catheti and the hypotenuse have to be handled by either the RLE
		 * stored for both catheti or for the hypotenuse.
		 *
		 * We do this since the root sub-cluster triangle type is always set to 'V' to store the vertices
		 * close to the entrance and exit curve on the 'hypotenuse' stack.
		 *
		 * Another reason is also to have a unique storage thus increasing searches for adjacent
		 * communication vertex data.
		 */
		std::vector<CEdgeComm_InformationAdjacentCluster_> &hypEdgeCommInfo = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters;

		if (cCluster_TreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			for (auto iter = rightVertexInformationAdjacentClusters.begin();
					iter != rightVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				hypEdgeCommInfo.insert(hypEdgeCommInfo.begin(), *iter);
			}

			for (auto iter = leftVertexInformationAdjacentClusters.begin();
					iter != leftVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				hypEdgeCommInfo.push_back(*iter);
			}
		}
		else
		{
			for (auto iter = rightVertexInformationAdjacentClusters.begin();
					iter != rightVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				hypEdgeCommInfo.push_back(*iter);
			}

			for (auto iter = leftVertexInformationAdjacentClusters.begin();
					iter != leftVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				hypEdgeCommInfo.insert(hypEdgeCommInfo.begin(), *iter);
			}
		}


		/*
		 * extend RLE edge comm information for both catheti
		 */
		std::vector<CEdgeComm_InformationAdjacentCluster_> &catEdgeCommInfo = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters;

		auto catInsertIter = catEdgeCommInfo.begin();

		if (cCluster_TreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack())
		{
			if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
			{
				assert(catInsertIter != catEdgeCommInfo.end());
				catInsertIter++;
			}

			for (auto iter = topVertexInformationAdjacentClusters.begin();
					iter != topVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				catEdgeCommInfo.insert(catInsertIter, *iter);
			}
		}
		else
		{
			if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
			{
				assert(catInsertIter != catEdgeCommInfo.end());
				catInsertIter++;
			}

			for (auto iter = topVertexInformationAdjacentClusters.begin();
					iter != topVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				catEdgeCommInfo.insert(catInsertIter, *iter);
			}
		}
	}
#endif



public:
	bool _cleanupUnusedNodes(CGenericTreeNode_ *node)
	{
		if (node->first_child_node)
		{
			if (_cleanupUnusedNodes(node->first_child_node))
			{
				delete node->first_child_node;
				node->first_child_node = nullptr;
			}
		}

		if (node->second_child_node)
		{
			if (_cleanupUnusedNodes(node->second_child_node))
			{
				delete node->second_child_node;
				node->second_child_node = nullptr;
			}
		}

		return (	node->first_child_node == nullptr &&
					node->second_child_node == nullptr &&
					node->cCluster_TreeNode == nullptr
		);
	}


public:
	/**
	 * \brief setup a generic cluster tree given by a base triangulation.
	 *
	 * this also initialized the RLE edge communication information.
	 *
	 * \return return number of local cells
	 */
	unsigned long long setup_GenericTree_From_BaseTriangulation(
			CDomain_BaseTriangulation_ &io_cDomain_BaseTriangulation,	///< root triangulation

			int i_initial_refinement_depth,				///< initial depth of refinement
			int i_minimum_relative_refinement_depth,	///< minimum relative refinement depth to setup triangle factory
			int i_maximum_relative_refinement_depth,	///< maximum relative refinement depth to setup triangle factory

			int i_mpi_rank,						///< mpi rank for base triangles which should be setup

			CDomainClusters<CSimulation_ClusterHandler> *o_cDomainClusters		///< reference output of domain clusters
	)
	{
		o_cDomainClusters->freeChildren();

		// store mpi rank
		mpi_rank = i_mpi_rank;

		cDomain_BaseTriangulation = &io_cDomain_BaseTriangulation;

		number_of_initial_global_base_clusters = cDomain_BaseTriangulation->cDomainBase_Triangle_List.size();
		number_of_initial_local_base_clusters = 0;

		initial_refinement_depth = i_initial_refinement_depth;
		min_relative_depth = i_minimum_relative_refinement_depth;
		max_relative_depth = i_maximum_relative_refinement_depth;


		/**
		 * check for valid initial depth
		 */
		assert(i_initial_refinement_depth >= 0);
#if 0
		if ((i_initial_refinement_depth & 1) == 1)
		{
			if (!cDomain_BaseTriangulation->oddInitialDepthFixApplied)
			{
				std::cerr << "An odd initial depth value is not allowed due to restrictions of hanging nodes along catheti and hypotenuse" << std::endl;
				assert(false);
			}
		}
#endif

		/**
		 * compute edge communication elements for initial triangulation
		 */
		cat_edge_comm_elements = 1 << (i_initial_refinement_depth/2);
		hyp_edge_comm_elements = 1 << ((i_initial_refinement_depth+1)/2);


		/**
		 * determine max_unique_id_depth
		 */
		unsigned long long d = number_of_initial_global_base_clusters-1;
		max_unique_id_depth = 0;
		for (; d > 0; d >>= 1)
			max_unique_id_depth++;

		unique_id_counter = 0;

		if (number_of_initial_global_base_clusters == 0)
		{
			/*
			 * nothing to do for empty domain size
			 */
			o_cDomainClusters->setupDummyRoot();
			return 0;
		}

		{
			auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;
			CCluster_TreeNode_ *cCluster_TreeNode = cDomain_BaseTriangle.cCluster_user_ptr;

			if (number_of_initial_global_base_clusters == 1)
			{
				/*
				 * if there's only one initial root cluster, directly set it up in cCluster
				 */
	#if CONFIG_ENABLE_MPI
				if (cDomain_BaseTriangle.mpi_rank == mpi_rank)
	#endif
				{
					_setupLeafCluster(cDomain_BaseTriangle, nullptr, o_cDomainClusters->getGenericTreeNodePtr());
				}
			}
			else
			{
				/**
				 * assemble tree by allocating GenericTreeNodes and ClusterTreeNodes
				 */
				assert(cCluster_TreeNode == nullptr);

				/**
				 * setup the cluster tree nodes for the root triangulation.
				 *
				 * also setup the simulation cluster handler and create the unique IDs.
				 *
				 * the method _setup_GenericTreeAndClusters recursively sets up the whole tree!
				 */
				_setup_GenericTreeParentAndClusters(iter, o_cDomainClusters, 1);
			}

			if (number_of_initial_local_base_clusters == 0)
			{
				o_cDomainClusters->setupDummyRoot();
				return 0;
			}
		}


		/**
		 * setup clusters
		 */
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			CCluster_TreeNode_ *cCluster_TreeNode = cDomain_BaseTriangle.cCluster_user_ptr;

#if CONFIG_ENABLE_MPI
			if (cDomain_BaseTriangle.mpi_rank != mpi_rank)
			{
				assert(cCluster_TreeNode == nullptr);
				continue;
			}
#else
			assert(cCluster_TreeNode != nullptr);
#endif

			_setup_ClusterEdgeCommunicationInformation(
					cCluster_TreeNode,
					cDomain_BaseTriangle
			);

			cCluster_TreeNode->setup_SimulationClusterHandler(true, nullptr);
		}


#if CONFIG_ENABLE_MPI
		/*
		 * cleanup tree by removing unused nodes with both childs being a nullptr
		 */

		if (_cleanupUnusedNodes(o_cDomainClusters) == true)
		{
			std::cout << "RANK " << mpi_rank << ": Root cluster should be also deleted => more nodes than clusters available!" << std::endl;
		}
#endif

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		/**
		 * extend edge communication information for vertex data comm
		 */
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ *cDomain_BaseTriangle = &(*iter);

			_setup_ClusterVertexCommunicationInformation(cDomain_BaseTriangle);
		}
#endif

		_validate();

		cDomain_BaseTriangulation = nullptr;

		// return number of local cells
		return (unsigned long long)(1 << initial_refinement_depth)*number_of_initial_local_base_clusters;
	}








	/**
	 * start some validation process based on the properties stored in the triangleWithCluster classes
	 *
	 * return true, if the validation failed
	 */
private:
	bool _validate()
	{
		bool valid = true;

#if DEBUG
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
			)
		{
			CDomain_BaseTriangle_ &t = *iter;

			/**
			 * hypotenuse
			 */
			if (!t.adjacent_triangles.hyp_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle_ &a = *t.adjacent_triangles.hyp_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * left edge
			 */
			if (!t.adjacent_triangles.left_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (LEFT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_ClusterHandler> &a = *t.adjacent_triangles.left_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * right edge
			 */
			if (!t.adjacent_triangles.right_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (RIGHT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_ClusterHandler> &a = *t.adjacent_triangles.right_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cout << "triangle factory for local triangle: " << std::endl;
					std::cout << t.cTriangleFactory << std::endl;
					std::cout << "triangle factory for adjacent triangle: " << std::endl;
					std::cout << a.cTriangleFactory << std::endl;
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}

			if (!valid)
			{
				std::cout << "TriangleWithCluster id: " << t.cCluster_user_ptr->uniqueId << std::endl;
				break;
			}
		}
#endif

		return valid;
	}

};

}

#endif /* CBASETRIANGULATION_TO_GENERICTREE_HPP_ */
