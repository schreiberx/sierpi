Import('env')


#
# generic files
#
env.src_files.append(env.Object('main.cpp'))

for i in env.Glob('lib/*.cpp'):
	env.src_files.append(env.Object(i))


for i in env.Glob('lib/xmlconfig/*.cpp'):
	env.src_files.append(env.Object(i))

for i in env.Glob('libsierpi/triangle/*.cpp'):
	env.src_files.append(env.Object(i))

if env['hyperbolic_const_matrices_id'] == '-1':
	for i in env.Glob('simulations/hyperbolic_common/basis_functions_and_matrices/CComputation_Matrices_Nodal.cpp'):
		env.src_files.append(env.Object(i))
	for i in env.Glob('simulations/hyperbolic_common/basis_functions_and_matrices/CBasisFunctions_Nodal_Order_N.cpp'):
		env.src_files.append(env.Object(i))

else:
	for i in env.Glob('simulations/hyperbolic_common/basis_functions_and_matrices/matrices_const_'+env['hyperbolic_const_matrices_id']+'/CComputation_Matrices_Const.cpp'):
		env.src_files.append(env.Object(i))

#for i in env.Glob('libsierpi/pde_lab/*.cpp'):
#	env.src_files.append(env.Object(i))


#
# Datasets
#

if env['enable_asagi']:
	for i in env.Glob('simulations/hyperbolic_common/datasets_common/CAsagi.cpp'):
		env.src_files.append(env.Object(i))

if env['enable_netcdf']:
	for i in env.Glob('simulations/hyperbolic_common/datasets_common/CSimpleNetCDF.cpp'):
		env.src_files.append(env.Object(i))

#
# GUI
#

if env['enable_gui']:
	for i in env.Glob('libgl/*.cpp'):
		env.src_files.append(env.Object(i))

	for i in env.Glob('libgl/engine/*.cpp'):
		env.src_files.append(env.Object(i))

	for i in env.Glob('libgl/shaders/shader_color/*.cpp'):
		env.src_files.append(env.Object(i))

#
# THREADING
#

if env['threading'] == 'tbb':
	env.src_files.append(env.Object('mainthreading/CMainThreadingTBB.cpp'))

if env['threading'] == 'omp' or env['threading'] == 'iomp' or env['threading'] == 'ipmo':
	env.src_files.append(env.Object('mainthreading/CMainThreadingOMP.cpp'))


#
# Hyperbolic / tsunami_serial
#
if env['simulation'] in ['hyperbolic_parallel', 'tsunami_serial']:
	for i in env.Glob('simulations/hyperbolic_common/traversators/*.cpp'):
		env.src_files.append(env.Object(i))

	for i in env.Glob('libsierpi/parallelization/*.cpp'):
		env.src_files.append(env.Object(i))

	if env['sub_simulation'] == 'tsunami':
		for i in env.Glob('simulations/hyperbolic_common/subsimulation_tsunami/kernels/simulation/*.cpp'):
			env.src_files.append(env.Object(i))


#
# Fortran
#
if env['enable_fortran_source']:
	if env['sub_simulation'] == 'tsunami':
		for i in env.Glob('simulations/hyperbolic_common/subsimulation_tsunami/flux_solver/tsunami_solver/geoclaw/*.f*'):
			# remove .mod files
			allobjs = env.Object(i)
			objs = filter(lambda o: str(o)[-4:] != '.mod', allobjs)
			env.src_files.append(objs)


Export('env')
