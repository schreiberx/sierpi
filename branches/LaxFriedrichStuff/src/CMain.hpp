/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 17. April 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAIN_HPP
#define CMAIN_HPP


#include <iostream>
#include <string>

#if CONFIG_ENABLE_MPI
	#include <mpi.h>
#endif

#include "config.h"
#include "lib/CProcessMemoryInformation.hpp"
#include "lib/CStopwatch.hpp"

#include "mainthreading/CMainThreading.hpp"
#include "lib/xmlconfig/CXMLConfig.hpp"

#include "simulations/CSimulation.hpp"

#include "CSimulation_MainParameters.hpp"
#include "CSimulation_MainInterface.hpp"
#include "CSimulation_MainInterface_FileOutput.hpp"



/**
 * \brief main driver to run, setup and control simulation
 *
 * This is the main driver for the simulation including all kind of control when
 * a time-step is executed, when to output grid data, etc.
 */
class CMain	:
	public CMainThreading,			///< threading features
	public CXMLConfigInterface
{
public:
	/**
	 * information about process memory for debug purpose
	 */
	CProcessMemoryInformation cProcessMemoryInformation;

	/**
	 * Helper class to read XML configuration
	 */
	CXMLConfig cXMLConfig;

	/**
	 * number of timesteps when to quit simulation
	 */
	int simulation_run_for_fixed_timesteps;

	/**
	 * stop simulation after reaching this simulation time
	 */
	double simulation_run_for_fixed_simulation_time;

	/**
	 * fixed timestep size for simulation
	 */
	double simulation_run_with_fixed_timestepSize;

	/**
	 * terminate simulation after n timesteps with equal number of cells
	 */
	int simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation;

	/**
	 * maximum number of threads to use for simulation
	 */
	int threading_max_number_of_threads;

	/**
	 * number of threads to currently use for simulation
	 */
	int threading_number_of_threads_to_use;

	/**
	 * padding between cores where threads are pinned to
	 */
	int threading_affinity_padding;

	/**
	 * starting core nr to set affinities
	 */
	int threading_affinity_start_id;

	/**
	 * scalability graph id for iPMO invasive threading extension
	 */
	int threading_ipmo_scalability_graph_id;


	/**
	 * verbosity level
	 */
	int verbosity_level;

	/**
	 * output simulation data (either files or verbose information to console) after each n realtime seconds
	 */
	double output_simulation_data_after_each_n_realtime_seconds;

	/**
	 * output simulation data after each n-th timestep
	 */
	int output_simulation_data_each_nth_timestep;

	/**
	 * output simulation data after each n simulation seconds
	 */
	double output_simulation_data_after_each_n_simulation_seconds;


	/**
	 * name of file where to write simulation cell data
	 */
	std::string output_simulation_vtp_cell_data_filename;

	/**
	 * name of file where to write simulation cluster borders to
	 */
	std::string output_simulation_vtp_clusters_data_filename;

	/**
	 * update cluster after given number of timesteps are elapsed
	 */
	int clusters_update_split_join_size_after_elapsed_timesteps;

	/**
	 * magic number used to compute optimized split and join settings
	 */
	double clusters_update_split_join_size_after_elapsed_scalar;

	/**
	 * pin the current main process to the specified core
	 */
	int threading_pin_to_core_nr_after_one_timestep;

	/**
	 * Simulation handler
	 */
	CSimulation *cSimulation;

	/**
	 * Interface to simulations for main parameters
	 */
	CSimulation_MainParameters *cSimulation_MainParameters;

	/**
	 * Interface to simulation for main control
	 */
	CSimulation_MainInterface *cSimulation_MainInterface;

	/**
	 * Interface to simulation for writing file output
	 */
	CSimulation_MainInterface_FileOutput *cSimulation_MainInterface_FileOutput;



	/**
	 * For threading backend (CMainThreading): return the maximum humber of threads
	 */
	int getMaxNumberOfThreads()	{
		return threading_max_number_of_threads;
	}

	/**
	 * For threading backend (CMainThreading): set the maximum number of threads
	 */
	void setMaxNumberOfThreads(int i_max_number_of_threads)
	{
		threading_max_number_of_threads = i_max_number_of_threads;
	}

	/**
	 * For threading backend (CMainThreading): get the number of threads to use for smiulation
	 */
	int getNumberOfThreadsToUse()
	{
		return threading_number_of_threads_to_use;
	}

	/**
	 * For threading backend (CMainThreading): get the thread affinity padding
	 */
	int getThreadAffinityPadding()
	{
		return threading_affinity_padding;
	}


	/**
	 * For threading backend (CMainThreading): get the thread affinity start id
	 */
	int getThreadAffinityStartId()
	{
		return threading_affinity_start_id;
	}


	/**
	 * For threading backend (CMainThreading): get the id for the scalability graph to use for iPMO
	 */
	int getThreadIPMOScalabilityGraphId()
	{
		return threading_ipmo_scalability_graph_id;
	}


	/**
	 * For threading backend (CMainThreading): set the value to be used for the number of threads.
	 *
	 * this does not directly change the number of threads!
	 */
	void setValueNumberOfThreadsToUse(int i_number_of_threads_to_use)
	{
		threading_number_of_threads_to_use = i_number_of_threads_to_use;

		if (cSimulation_MainParameters != nullptr)
			cSimulation_MainParameters->simulation_threading_number_of_threads = i_number_of_threads_to_use;
	}


	/**
	 * For threading backend (CMainThreading): for a typical simulation, the approximated workload is given by the number
	 * of grid-cells
	 *
	 * \return simulation workload
	 */
	long long getSimulationWorkload()
	{
		if (cSimulation_MainParameters == nullptr)
			return 0;

		return cSimulation_MainParameters->number_of_local_cells;
	}


	/**
	 * For threading backend (CMainThreading): for a typical simulation, the approximated workload is given by the number
	 * of grid-cells. here we return the sum of the number of grid-cells over simulation time.
	 *
	 * \return simulation workload
	 */
	long long getSimulationSumWorkload()
	{
		return simloop_sum_number_of_local_cells;
	}


	/**
	 * return the verbose level
	 */
	int getVerboseLevel()
	{
		return verbosity_level;
	}


	/**
	 * program argument counter which can be modified by MPI_Init
	 */
	int main_argc;

	/**
	 * strings for program arguments
	 */
	char** main_argv;


	/**
	 * MPI rank associated with this simulation
	 */
	int mpi_rank;

	/**
	 * MPI size associated with this simulation
	 */
	int mpi_size;


	/**
	 * output vtp grid data
	 */
	bool output_data_vtp_grid;

	/**
	 * output vtp cluster data
	 */
	bool output_data_vtp_clusters;

	/**
	 * output simulation specific data
	 */
	bool output_simulation_specific_data;


	/**
	 * output simulation specific data identifier
	 *
	 * e. g. dart, swosb (to output the max height), etc.
	 */
	std::string output_simulation_specific_data_identifier;

	/**
	 * parameters for simulation output
	 */
	std::string output_simulation_specific_data_parameters;


	/**
	 * \brief CMain constructor: directly called from main()
	 *
	 * Simply setup some member variables and register this class at the xml configuration file loader
	 */
	CMain(
		int i_argc,			///< number of specified arguments
		char **i_argv		///< array with arguments
	)	:
		cSimulation(nullptr),
		cSimulation_MainParameters(nullptr),
		cSimulation_MainInterface(nullptr),
		cSimulation_MainInterface_FileOutput(nullptr)
	{
		main_argc = i_argc;
		main_argv = i_argv;

		mpi_rank = 0;
		mpi_size = 1;

		threading_max_number_of_threads = -1;
		threading_number_of_threads_to_use = -1;

		simulation_run_for_fixed_timesteps = -1;
		simulation_run_with_fixed_timestepSize = -1;
		simulation_run_for_fixed_simulation_time = -1;

		threading_affinity_padding = -1;
		threading_affinity_start_id = -1;
		threading_ipmo_scalability_graph_id = -1;

		simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation = -1;

		verbosity_level = 0;

		output_simulation_data_each_nth_timestep = -100;
		output_simulation_data_after_each_n_realtime_seconds = -1.0;
		output_simulation_data_after_each_n_simulation_seconds = -1.0;

		output_simulation_vtp_cell_data_filename = "frame_%08i_%08i.vtp";

		output_timing_flags = OUTPUT_TIMING_NULL;
		output_data_flags = OUTPUT_DATA_NULL;

		clusters_update_split_join_size_after_elapsed_timesteps = -1;
		clusters_update_split_join_size_after_elapsed_scalar = -1;

		threading_pin_to_core_nr_after_one_timestep = -1;

		output_data_vtp_grid = false;
		output_data_vtp_clusters = false;
		output_simulation_specific_data = false;

		// register main class. simulation class should be already registered
		cXMLConfig.registerConfigClass("sierpi", this);
		cXMLConfig.registerConfigClass("threading", this);
	}


	/**
	 * \brief Parse the program start parameters
	 *
	 * \return true when all parameters are valid
	 */
	bool setupProgramParameters()
	{
		char optchar;
		std::string options("A:b:B:c:fFG:v:L:n:N:t:p:P:S:T:X");
		options += cSimulation_MainParameters->config_command_line_getArgumentString();

		while ((optchar = getopt(main_argc, main_argv, options.c_str())) > 0)
		{
			switch(optchar)
			{
			case 'v':
				verbosity_level = atoi(optarg);
				if (verbosity_level > 2)
					output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
				break;

			case 'c':
				// load & forward xml configuration
				loadAndForwardXMLConfig(optarg);
				break;

			case 't':
				simulation_run_for_fixed_simulation_time = atof(optarg);
				break;

			case 'L':
				simulation_run_for_fixed_timesteps = atoi(optarg);
				break;

			case 'T':
				simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation = atoi(optarg);
				break;

			/*
			 * timings for simulation data
			 */
			case 'b':
				output_simulation_data_after_each_n_simulation_seconds = atof(optarg);
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;
				break;

			case 'B':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				break;

			case 'p':
				output_simulation_data_after_each_n_realtime_seconds = atof(optarg);
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;
				break;


			/*
			 * output VTP grid data
			 */
			case 'f':
				output_data_vtp_grid = true;
				break;


			/*
			 * output VTP clusters
			 */
			case 'F':
				output_data_vtp_clusters = true;
				break;


			/*
			 * output simulation specific data
			 */
			case 'X':
				output_simulation_specific_data = true;
				break;


			case 'D':
				output_simulation_specific_data_parameters = optarg;
				break;


			/*
			 * threading
			 */
			case 'n':
				threading_number_of_threads_to_use = atoi(optarg);
				break;

			case 'N':
				threading_max_number_of_threads = atoi(optarg);
				break;

			case 'A':
				threading_affinity_padding = atoi(optarg);
				break;

			case 'S':
				threading_affinity_start_id = atoi(optarg);
				break;

			case 'G':
				threading_ipmo_scalability_graph_id = atoi(optarg);
				break;


			case 'P':
				threading_pin_to_core_nr_after_one_timestep = atoi(optarg);
				break;

			case 'h':
				printParameterInfo(main_argc, main_argv);
				return false;

			default:
				if (!cSimulation_MainParameters->config_command_line_parseArgument(optchar, optarg))
				{
					printParameterInfo(main_argc, main_argv);
					return false;
				}

				break;
			}
		}

		if (!config_validateAndFixParameters())
			return false;

		if (!cSimulation_MainParameters->config_validateAndFixParameters())
			return false;

		return true;
	}


	/**
	 * \brief Callback to setup XML configuration parameters
	 */
	bool setupXMLParameter(
				const char *i_scope_name,	///< scope (e. g. threading/sierpi)
				const char *i_name,		///< name of parameter
				const char *i_value		///< value of parameter
	)	{
		if (strcmp(i_scope_name, "threading") == 0)
		{
			XML_CONFIG_TEST_AND_SET_INT("number-of-threads", threading_number_of_threads_to_use);
			XML_CONFIG_TEST_AND_SET_INT("max-number-of-threads", threading_max_number_of_threads);

			XML_CONFIG_TEST_AND_SET_INT("core-affinities-start", threading_affinity_start_id);
			XML_CONFIG_TEST_AND_SET_INT("core-affinities-padding", threading_affinity_padding);

			XML_CONFIG_TEST_AND_SET_INT("ipmo-scalability-graph-id", threading_ipmo_scalability_graph_id);

			XML_CONFIG_TEST_AND_SET_BOOL("pin-to-core-nr-after-one-timestep", threading_pin_to_core_nr_after_one_timestep);
			return false;
		}

		if (strcmp(i_scope_name, "sierpi") == 0)
		{
			XML_CONFIG_TEST_AND_SET_INT("verbosity-level", verbosity_level);

			XML_CONFIG_TEST_AND_SET_INT("simulation-run-for-fixed-timesteps", simulation_run_for_fixed_timesteps);
			XML_CONFIG_TEST_AND_SET_INT("simulation-terminate-after-n-timesteps-with-equal-number-of-cells-in-simulation", simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-run-for-fixed-simulation-time", simulation_run_for_fixed_simulation_time);

			XML_CONFIG_TEST_AND_SET_STRING("output-vtp-grid-filename", output_simulation_vtp_cell_data_filename);
			XML_CONFIG_TEST_AND_SET_STRING("output-vtp-clusters-filename", output_simulation_vtp_clusters_data_filename)

			// output timings
			XML_CONFIG_TEST_AND_SET_INT("output-data-after-each-nth-timestep", output_simulation_data_each_nth_timestep);
			XML_CONFIG_TEST_AND_SET_FLOAT("output-data-after-each-n-simulation-seconds", output_simulation_data_after_each_n_simulation_seconds);
			XML_CONFIG_TEST_AND_SET_FLOAT("output-data-after-each-n-realtime-seconds", output_simulation_data_after_each_n_realtime_seconds);

			XML_CONFIG_TEST_AND_SET_BOOL("output-vtp-grid", output_data_vtp_grid);
			XML_CONFIG_TEST_AND_SET_BOOL("output-vtp-clusters", output_data_vtp_clusters);
			XML_CONFIG_TEST_AND_SET_BOOL("output-simulation-specific-data", output_simulation_specific_data);

			XML_CONFIG_TEST_AND_SET_STRING("output-simulation-specific-data-identifier", output_simulation_specific_data_identifier);
			XML_CONFIG_TEST_AND_SET_STRING("output-simulation-specific-data-parameters", output_simulation_specific_data_parameters);

			return false;
		}
//		std::cout << output_simulation_data_after_each_n_simulation_seconds << std::endl;
		return false;
	}


	/**
	 * \brief Load and forward XML configuration
	 */
	bool loadAndForwardXMLConfig(
			const char *xml_configuration_file		///< string for xml configuration file to be loaded
	)	{
		bool xml_config_loaded = false;

		if (xml_configuration_file != nullptr)
			xml_config_loaded = cXMLConfig.loadConfig(xml_configuration_file);

		return xml_config_loaded;
	}


	/**
	 * \brief Validate and fix parameters
	 */
	bool config_validateAndFixParameters()
	{
		/**
		 * setup output data flags
		 */
		if (output_data_vtp_grid)
			output_data_flags |= OUTPUT_DATA_VTP_GRID;

		if (output_data_vtp_clusters)
			output_data_flags |= OUTPUT_DATA_VTP_CLUSTERS;

		if (output_simulation_specific_data)
			output_data_flags |= OUTPUT_DATA_SIMULATION_SPECIFIC;


		/**
		 * setup timing flags
		 */
		if (output_simulation_data_each_nth_timestep >= 0)
			output_timing_flags |= OUTPUT_TIMING_EACH_NTH_TIMESTEP;

		if (output_simulation_data_after_each_n_realtime_seconds >= 0)
			output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;

		if (output_simulation_data_after_each_n_simulation_seconds >= 0)
			output_timing_flags |= OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;


		/**
		 * if no timing is specified, use 100 simulation seconds as default
		 */
		if (output_data_flags != 0 && output_timing_flags == 0)
		{
			output_timing_flags = OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;

			if (output_simulation_data_after_each_n_simulation_seconds < 0)
				output_simulation_data_after_each_n_simulation_seconds = 100;
		}


		/**
		 * setup additional flags for specific verbosity level
		 */
		if (verbosity_level == -99 || verbosity_level >= 2)
		{
			if (
				output_simulation_data_after_each_n_realtime_seconds < 0.0 &&
				output_timing_flags == OUTPUT_TIMING_NULL
			)	{
				output_simulation_data_after_each_n_realtime_seconds = 1.0;
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;
			}
			else if (
					output_timing_flags == OUTPUT_TIMING_NULL
			)	{
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				if (output_simulation_data_each_nth_timestep < 0)
					output_simulation_data_each_nth_timestep = 100;
			}

			output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
			output_data_flags |= OUTPUT_DATA_STDOUT_MTPS_PER_FRAME;
		}


		/**
		 * specific simulation output
		 */
		if (output_simulation_specific_data_identifier != "")
		{
			output_data_flags |= OUTPUT_DATA_SIMULATION_SPECIFIC;
			output_simulation_specific_data = true;

			if (output_timing_flags == OUTPUT_TIMING_NULL)
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
		}

		cSimulation_MainParameters->setVerbosityLevel(verbosity_level);

		if (output_simulation_vtp_clusters_data_filename == "" && (output_data_flags & OUTPUT_DATA_VTP_CLUSTERS))
			output_simulation_vtp_clusters_data_filename = std::string("clusters_") + output_simulation_vtp_cell_data_filename;

		return true;
	}


	/**
	 * \brief Output information about program parameters
	 */
	void printParameterInfo(
			int argc,		///< number of specified arguments
			char *argv[]	///< array with arguments
	)
	{
		std::cout << "Usage: " << argv[0] << std::endl;

		std::cout << std::endl;
		std::cout << "CONFIGURATION:" << std::endl;
		std::cout << "	-c [string]: xml configuration file" << std::endl;

		std::cout << std::endl;
		std::cout << "VERBOSITY / BACKENDS:" << std::endl;
		std::cout << "	-v [int]: verbose mode (0-10, -99 for tabular output)" << std::endl;

		std::cout << "	-f: output .vtp files with grid information" << std::endl;
		std::cout << "	-F: output .vtp files with cluster information" << std::endl;
		std::cout << "	-X: output simulation specific data" << std::endl;

		std::cout << "	-b [double]: output data each [n] simulation seconds" << std::endl;
		std::cout << "	-B [int]: output data each [#nth] timestep" << std::endl;
		std::cout << "	-p [double]: output data each [n] realtime seconds" << std::endl;

//		std::cout << "	-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;

		std::cout << std::endl;
		std::cout << "PARALLELIZATION (if enabled):" << std::endl;
		std::cout << "	-n [int]: Number of threads to use" << std::endl;
		std::cout << "	-N [int]: Maximum number of cores installed in system" << std::endl;
		std::cout << "	-G [int]: Scalability graph for iPMO" << std::endl;
		std::cout << "	-S [int]: Set thread start affinity. default: -1 (disabled). This number represents the start of the core mapping." << std::endl;
		std::cout << "	-A [int]: Set thread affinities. default: -1 (disabled). This number represents the distance between 2 consecutive cores." << std::endl;

		std::cout << std::endl;
		std::cout << "GENERAL SIMULATION CONTROL:" << std::endl;
		std::cout << "	-t [double]: run for given simulation time" << std::endl;
		std::cout << "	-L [int]: timesteps" << std::endl;
		std::cout << "	-T [int]: terminate simulation after the given number of" << std::endl;
		std::cout << "			  timesteps was executed consecutively with the same number of cells" << std::endl;

		std::cout << std::endl;
		std::cout << "SIMULATION SPECIFIC:" << std::endl;

		cSimulation_MainParameters->config_command_line_getArgumentsHelp();
	}

	/**
	 * \brief Run simple MPI setup
	 */
	void mpi_setup()
	{
#if CONFIG_ENABLE_MPI
		MPI_Init(&main_argc, &main_argv);
		MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
		MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif
	}


	/**
	 * \brief Shutdown MPI
	 */
	void mpi_shutdown()
	{
#if CONFIG_ENABLE_MPI
		MPI_Finalize();
#endif
	}


	/**
	 * \brief Allocate the simulation and setup the XML configuration interface
	 */
	bool simulation_create()
	{
		/****************************
		 * ALLOCATE SIMULATION - DON'T SETUP FULL SIMULATION SO FAR!
		 */
		cSimulation = new CSimulation(verbosity_level);

		cSimulation_MainParameters = &(CSimulation_MainParameters&)(*cSimulation);
		cSimulation_MainInterface = &(CSimulation_MainInterface&)(*cSimulation);
		cSimulation_MainInterface_FileOutput = &(CSimulation_MainInterface_FileOutput&)(*cSimulation);


		/*
		 * setup xml configuration interface
		 */
		cSimulation_MainParameters->config_xml_file_setupInterface(cXMLConfig);

		return true;
	}



	/**
	 * \brief setup the simulation
	 *
	 * this step is separated from the creation of the simulation (simulation_create)
	 * to set the parameters (program arguments / XML) inbetween.
	 */
	bool simulation_setup()
	{
		/****************************
		 * SETUP SIMULATION
		 */

		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
		srand(0);

		cSimulation_MainParameters->simulation_threading_number_of_threads = threading_number_of_threads_to_use;

#if CONFIG_ENABLE_MPI
		cSimulation_MainParameters->simulation_mpi_rank = mpi_rank;
		cSimulation_MainParameters->simulation_mpi_size = mpi_size;
#endif


		if (threading_pin_to_core_nr_after_one_timestep != -1)
			cSimulation_MainParameters->threading_pin_to_core_nr_after_one_timestep = threading_pin_to_core_nr_after_one_timestep;


		simloop_cStopwatch.start();

		/***************************************************
		 * reset & therefore setup the simulation with flat water surface
		 */

		cSimulation_MainInterface->reset();


		/***************************************************
		 * remember initial number of cells
		 */
		if (verbosity_level > 2)
			std::cout << " + Initial number of triangles after base triangulation: " << cSimulation_MainParameters->number_of_local_initial_cells_after_domain_triangulation << std::endl;



		/*****************************************************
		 * setup adaptive simulation
		 */
		cSimulation_MainInterface->setup_GridDataWithAdaptiveSimulation();


		if (verbosity_level >= 5)
			std::cout << "SETUP TIME: " << simloop_cStopwatch.getTimeSinceStart() << std::endl;

		return true;
	}


	/**
	 * \brief shutdown simulation class
	 */
	void simulation_shutdown()
	{
		delete cSimulation;
	}


	/**
	 * \brief output verbose information for CMain and further classes
	 */
	void outputVerboseInformation()
	{
		if (verbosity_level > 2)
		{
			std::cout << " + Number of threads to use: " << getNumberOfThreadsToUse() << std::endl;
			std::cout << " + Max number of threads (system): " << getMaxNumberOfThreads() << std::endl;
			std::cout << " + Verbose: " << verbosity_level << std::endl;
			std::cout << " + Terminate simulation after #n equal timesteps: " << simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation << std::endl;
			std::cout << " + Timesteps: " << simulation_run_for_fixed_timesteps << std::endl;
			std::cout << " + Output data each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output data each n simulation seconds: " << output_simulation_data_after_each_n_simulation_seconds << std::endl;
			std::cout << " + Output data each n computation seconds: " << output_simulation_data_after_each_n_realtime_seconds << std::endl;

			std::cout << " + Output vtp grid data: " << (output_data_vtp_grid ? "on" : "off") << std::endl;
			std::cout << " + Output vtp cluster data: " << (output_data_vtp_clusters ? "on" : "off") << std::endl;
			std::cout << " + Output vtp cluster simulation specific: " << (output_simulation_specific_data ? "on" : "off") << std::endl;

			std::cout << " + Output simulation grid filename: " << output_simulation_vtp_cell_data_filename << std::endl;
			std::cout << " + Output simulation cluster filename: " << output_simulation_vtp_clusters_data_filename << std::endl;
			std::cout << " + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS " << (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS ? "[enabled]" : "[disabled]") << std::endl;
			std::cout << " +" << std::endl;

#if CONFIG_ENABLE_MPI
			std::cout << " + MPI Size: " << mpi_size << std::endl;
			std::cout << " + MPI Rank: " << mpi_rank << std::endl;
#endif

			/*
			 * also let the simulation output some information!
			 */
			cSimulation_MainInterface->outputVerboseInformation();
		}
	}



	/***************************************************
	 * SIMULATION LOOP
	 */
	CStopwatch simloop_cStopwatch;

	long long simloop_sum_number_of_local_cells;
	long long simloop_prev_number_of_triangles;
	long long simloop_sum_number_of_local_clusters;

	int simloop_outputVTKFrameCounter;
	int simloop_output_timing_each_nth_timestep_timestep_counter;
	int simloop_frame_counter;

	/*
	 * stopwatches for edge/adaptive/split-join traversals
	 */
	double simloop_edgeCommTime;
	double simloop_adaptiveTime;
	double simloop_splitJoinTime;
	double simloop_otherTime;

	/*
	 * output verbose information after output_verbose_information_after_each_n_seconds seconds
	 */
	double simloop_output_timing_each_nth_real_second__next_timestamp;
	double simloop_output_timing_each_nth_simulation_second__next_timestamp;
	double simloop_output_simulation_info_next_simulation_time;

	bool simloop_finishSimulation;
	int simloop_timestep_nr;

	double output_mtps_for_each_second_old_timestamp;
	long long output_mtps_for_each_second_old_timestep;
	long long output_mtps_for_n_seconds_number_of_processed_triangles;
	double output_mtps_for_simulation_frame_mtps;
	double output_average_cells_for_simulation_frame;



	/**
	 * how is writing of data triggered?
	 */
	enum
	{
		OUTPUT_TIMING_NULL	= 0,

		OUTPUT_TIMING_EACH_NTH_TIMESTEP = 1,
		OUTPUT_TIMING_EACH_NTH_REAL_SECOND = 2,
		OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND = 3
	};

	int output_timing_flags;



	/**
	 * which data should be written?
	 */
	enum
	{
		OUTPUT_DATA_NULL = 0,

		OUTPUT_DATA_STDOUT_VERBOSE = (1 << 1),
		OUTPUT_DATA_VTP_GRID = (1 << 2),
		OUTPUT_DATA_VTP_CLUSTERS = (1 << 3),
		OUTPUT_DATA_STDOUT_MTPS_PER_FRAME = (1 << 5),
		OUTPUT_DATA_SIMULATION_SPECIFIC = (1 << 4)
	};

	int output_data_flags;

	std::ofstream pvd_global_vtp_grid_description_file;
	std::ofstream pvd_global_vtp_cluster_description_file;

	/**
	 * this method is executed at the beginning of each simulation loop.
	 */
	void simulation_loopPrefix()
	{
		setValueNumberOfThreadsToUse(threading_number_of_threads_to_use);
		setMaxNumberOfThreads(threading_max_number_of_threads);

		if (verbosity_level > 1)
			std::cout << "[ START ]" << std::endl;

		simloop_cStopwatch.start();


		/***************************************************
		 * SIMULATION
		 */
		simloop_sum_number_of_local_cells = 0;
		simloop_prev_number_of_triangles = 0;
		simloop_sum_number_of_local_clusters = 0;

		simloop_outputVTKFrameCounter = 0;
		simloop_output_timing_each_nth_timestep_timestep_counter = 0;
		simloop_frame_counter = 0;

		/*
		 * stopwatches for edge/adaptive/split-join traversals
		 */
		simloop_edgeCommTime = 0;
		simloop_adaptiveTime = 0;
		simloop_splitJoinTime = 0;
		simloop_otherTime = 0;

		simloop_output_timing_each_nth_simulation_second__next_timestamp = 0;
		simloop_output_timing_each_nth_real_second__next_timestamp = 0;

		simloop_finishSimulation = false;
		simloop_timestep_nr = 0;

		output_mtps_for_each_second_old_timestamp = -1.0;
		output_mtps_for_each_second_old_timestep = 0;
		output_mtps_for_n_seconds_number_of_processed_triangles = 0;
		output_mtps_for_simulation_frame_mtps = 0;
		output_average_cells_for_simulation_frame = 0;

		if (output_data_flags & OUTPUT_DATA_VTP_GRID)
		{
			if (mpi_rank == 0)
			{
				pvd_global_vtp_grid_description_file.open("frames.pvd");

				pvd_global_vtp_grid_description_file << "<?xml version=\"1.0\"?>" << std::endl;
				pvd_global_vtp_grid_description_file << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">" << std::endl;
				pvd_global_vtp_grid_description_file << "  <Collection>" << std::endl;
			}
		}


		if (output_data_flags & OUTPUT_DATA_VTP_CLUSTERS)
		{
			if (mpi_rank == 0)
			{
				pvd_global_vtp_cluster_description_file.open("clusters.pvd");

				pvd_global_vtp_cluster_description_file << "<?xml version=\"1.0\"?>" << std::endl;
				pvd_global_vtp_cluster_description_file << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">" << std::endl;
				pvd_global_vtp_cluster_description_file << "  <Collection>" << std::endl;
			}
		}
	}



	/**
	 * simulation loop executing during each particular simulation step
	 *
	 * the separation into header/iteration/footer is necessary to be
	 * allowed to change the number of resources during each simulation when desired.
	 *
	 * \return
	 */
	bool simulation_loopIteration()
	{
		/***************************************************
		 * output some data/information?
		 */
		bool outputSimulationData = false;

		if (output_timing_flags != 0)
		{
			/*
			 * OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND)
			{
				if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation_MainParameters->simulation_timestamp_for_timestep)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_simulation_second__next_timestamp += output_simulation_data_after_each_n_simulation_seconds;

					if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation_MainParameters->simulation_timestamp_for_timestep)
					{
						simloop_output_timing_each_nth_simulation_second__next_timestamp = cSimulation_MainParameters->simulation_timestamp_for_timestep;
						std::cout << "WARNING: Timestep size for accurate data output is too small, fixing..." << std::endl;
					}
				}
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_TIMESTEP
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_TIMESTEP)
			{
				/*************************************************************************
				 * verbose information (e. g. vtk files) each nth timestep
				 *************************************************************************
				 * n timesteps are computed when simloop_outputVTKFrameCounter equals 0
				 */
				if (simloop_output_timing_each_nth_timestep_timestep_counter == 0)
				{
					outputSimulationData = true;

					if (verbosity_level > 6)
						cProcessMemoryInformation.outputUsageInformation();
				}

				simloop_output_timing_each_nth_timestep_timestep_counter = (simloop_output_timing_each_nth_timestep_timestep_counter+1) % output_simulation_data_each_nth_timestep;
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_REAL_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_REAL_SECOND)
			{
				double timestamp = simloop_cStopwatch.getTimeSinceStart();
				if (simloop_output_timing_each_nth_real_second__next_timestamp <= timestamp)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_real_second__next_timestamp += output_simulation_data_after_each_n_realtime_seconds;
					if (simloop_output_timing_each_nth_real_second__next_timestamp < timestamp)
					{
						std::cerr << "WARNING: Computation time lag. Simulation is running to slow to output information each " << output_simulation_data_after_each_n_realtime_seconds << " second(s)... fixing verbose output time" << std::endl;
						simloop_output_timing_each_nth_real_second__next_timestamp = timestamp;
					}
				}
			}

			/*
			 * update triangle throughput counter
			 */
			output_mtps_for_n_seconds_number_of_processed_triangles += cSimulation_MainParameters->number_of_local_cells;

			simloop_outputVTKFrameCounter = (simloop_outputVTKFrameCounter+1) % output_simulation_data_each_nth_timestep;


			if (outputSimulationData)
			{
				/*
				 * request to output some data.
				 * this is mainly intended to output buoy data.
				 */
				if (output_data_flags & OUTPUT_DATA_SIMULATION_SPECIFIC)
				{
					cSimulation_MainInterface->output_simulationSpecificData(output_simulation_specific_data_identifier, output_simulation_specific_data_parameters);
				}

				/*
				 * OUTPUT_DATA_VTK_GRID
				 */
				if (output_data_flags & OUTPUT_DATA_VTP_GRID)
				{
					char local_grid_file_buffer[1024];

					sprintf(local_grid_file_buffer, output_simulation_vtp_cell_data_filename.c_str(), simloop_frame_counter, mpi_rank);

					if (verbosity_level > 3)
					{
						std::cout << "=========================================" << std::endl;
						std::cout << "   + writing file " << local_grid_file_buffer << " with " << cSimulation_MainParameters->number_of_local_cells << " cells" << std::endl;
					}

					cSimulation_MainInterface_FileOutput->writeSimulationDataToFile(local_grid_file_buffer);

					/*
					 * output .PVD file
					 */
					if (mpi_rank == 0)
					{
						for (int l_mpi_rank = 0; l_mpi_rank < mpi_size; l_mpi_rank++)
						{
							char global_grid_file_buffer[1024];
							sprintf(global_grid_file_buffer, output_simulation_vtp_cell_data_filename.c_str(), simloop_frame_counter, l_mpi_rank, cSimulation_MainParameters->simulation_timestamp_for_timestep);

							pvd_global_vtp_grid_description_file << "    <DataSet ";
							pvd_global_vtp_grid_description_file << "timestep=\"" << cSimulation_MainParameters->simulation_timestamp_for_timestep << "\" ";
							pvd_global_vtp_grid_description_file << "group=\"\" ";
							pvd_global_vtp_grid_description_file << "part=\"" << l_mpi_rank << "\" ";
							pvd_global_vtp_grid_description_file << "file=\"" << global_grid_file_buffer << "\" ";
							pvd_global_vtp_grid_description_file << "/>" << std::endl;
						}
					}


					if (output_data_flags & OUTPUT_DATA_VTP_CLUSTERS)
					{
						char local_cluster_file_buffer[1024];

						sprintf(local_cluster_file_buffer, output_simulation_vtp_clusters_data_filename.c_str(), simloop_frame_counter, mpi_rank);

						cSimulation_MainInterface_FileOutput->writeSimulationClustersDataToFile(local_cluster_file_buffer);

#if CONFIG_ENABLE_MPI
						if (mpi_rank == 0)
						{
							for (int l_mpi_rank = 0; l_mpi_rank < mpi_size; l_mpi_rank++)
							{
								char global_cluster_file_buffer[1024];
								sprintf(global_cluster_file_buffer, output_simulation_vtp_clusters_data_filename.c_str(), simloop_frame_counter, l_mpi_rank, cSimulation_MainParameters->simulation_timestamp_for_timestep);

								pvd_global_vtp_cluster_description_file << "    <DataSet ";
								pvd_global_vtp_cluster_description_file << "timestep=\"" << cSimulation_MainParameters->simulation_timestamp_for_timestep << "\" ";
								pvd_global_vtp_cluster_description_file << "group=\"\" ";
								pvd_global_vtp_cluster_description_file << "part=\"" << l_mpi_rank << "\" ";
								pvd_global_vtp_cluster_description_file << "file=\"" << global_cluster_file_buffer << "\" ";
								pvd_global_vtp_cluster_description_file << "/>" << std::endl;
							}
						}
#endif
					}

					simloop_frame_counter++;
				}

				/*
				 * OUTPUT_DATA_STDOUT_VERBOSE
				 */
				if (output_data_flags & OUTPUT_DATA_STDOUT_VERBOSE)
				{
					double timestamp = simloop_cStopwatch.getTimeSinceStart();

					if (output_mtps_for_each_second_old_timestamp == -1)
					{
						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = 0;
					}

					double delta = timestamp - output_mtps_for_each_second_old_timestamp;
					if (delta >= output_simulation_data_after_each_n_simulation_seconds)
					{
						/*
						 * more than output_simulation_data_after_each_n_simulation_seconds second
						 */
						if (output_simulation_data_after_each_n_simulation_seconds >= 0)
							output_mtps_for_simulation_frame_mtps = ((double)output_mtps_for_n_seconds_number_of_processed_triangles / delta)*0.000001;
						if (cSimulation_MainParameters->simulation_timestep_nr != 0)
							output_average_cells_for_simulation_frame = output_mtps_for_n_seconds_number_of_processed_triangles/(double)(cSimulation_MainParameters->simulation_timestep_nr - output_mtps_for_each_second_old_timestep);

						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = cSimulation_MainParameters->simulation_timestep_nr;
						output_mtps_for_n_seconds_number_of_processed_triangles = 0;
					}


					if (verbosity_level == -99)
					{
						/*
						 * create OUTPUT in tabular format
						 */
						if (simloop_timestep_nr == 0)
						{
							/*
							 * print header
							 */
							std::cout << "MTPS\t";
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "MTPS_FRAME\t";
							std::cout << "TIMESTEP\t";
							std::cout << "SIMTIME\t";
							std::cout << "CELLS\t";
							std::cout << "CELLS_FRAME\t";
							std::cout << "MB_PER_TIMESTEP\t";
							std::cout << "TIMESTEP_SIZE\t";
							std::cout << "CLUSTERS\t";
							std::cout << std::endl;
						}

						/*
						 * print rows
						 */
						if (simloop_timestep_nr == 0)
						{
							std::cout << "0" << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "0" << '\t';
						}
						else
						{
							std::cout << ((double)simloop_sum_number_of_local_cells/simloop_cStopwatch.getTimeSinceStart())*0.000001 << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << output_mtps_for_simulation_frame_mtps << '\t';
						}

						std::cout << simloop_timestep_nr << '\t';
						std::cout << cSimulation_MainParameters->simulation_timestamp_for_timestep << '\t';
						std::cout << cSimulation_MainParameters->number_of_local_cells << '\t';
						std::cout << output_average_cells_for_simulation_frame << '\t';
						double elementdata_megabyte_per_timestep = ((double)sizeof(CHyperbolicTypes::CSimulationTypes::CCellData)*2.0)*((double)cSimulation_MainParameters->number_of_local_cells)/(1024.0*1024.0);
						std::cout << elementdata_megabyte_per_timestep << '\t';
						std::cout << cSimulation_MainParameters->simulation_parameter_global_timestep_size << '\t';
						std::cout << cSimulation_MainParameters->number_of_local_clusters << '\t';
						std::cout << std::endl;
					}
					else
					{
						std::cout << "=========================================" << std::endl;

						if (simloop_timestep_nr != 0)
						{
							std::cout << "   + " << ((double)simloop_sum_number_of_local_cells/simloop_cStopwatch.getTimeSinceStart())*0.000001 << " Overall MTPS (Mega Triangles per second)" << std::endl;
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "   + " << output_mtps_for_simulation_frame_mtps << " Last Second MTPS (Mega Triangles per second)" << std::endl;
						}

						std::cout << "   + " << simloop_timestep_nr << "\tTIMESTEP" << std::endl;
						std::cout << "   + " << cSimulation_MainParameters->simulation_timestamp_for_timestep << "\tSIMULATION_TIME" << std::endl;
						std::cout << "   + " << cSimulation_MainParameters->number_of_local_cells << "\tCELLS" << std::endl;
						std::cout << "   + " << simloop_cStopwatch.getTimeSinceStart() << " RT (REAL_TIME)" << std::endl;
						double elementdata_megabyte_per_timestep = ((double)sizeof(CHyperbolicTypes::CSimulationTypes::CCellData)*2.0)*((double)cSimulation_MainParameters->number_of_local_cells)/(1024.0*1024.0);
						std::cout << "   + " << elementdata_megabyte_per_timestep << "\tCellData Megabyte per time-step (RW)" << std::endl;
						std::cout << "   + " << cSimulation_MainParameters->simulation_parameter_global_timestep_size << "\tTIMESTEP SIZE" << std::endl;
						std::cout << "   + " << cSimulation_MainParameters->number_of_local_clusters << "\tCLUSTERS" << std::endl;
						std::cout << "   + " << simloop_edgeCommTime << "/" << simloop_adaptiveTime << "/" << simloop_splitJoinTime << "\tTIMINGS (edgeComm/adaptive/splitJoin)" << std::endl;
					}
				}
			}
		}


		/*
		 * single simulation simloop_timestep_id
		 */
		if (verbosity_level > 2)
		{
			cSimulation_MainInterface->runSingleTimestepDetailedBenchmarks(
					&simloop_edgeCommTime,
					&simloop_adaptiveTime,
					&simloop_splitJoinTime
				);
		}
		else
		{
			cSimulation_MainInterface->runSingleTimestep();
		}


		/*
		 * increment timestep id
		 */
		simloop_timestep_nr++;


		/*
		 * increment counter of number of triangles processed so far
		 */
		simloop_sum_number_of_local_cells += cSimulation_MainParameters->number_of_local_cells;
		simloop_sum_number_of_local_clusters += cSimulation_MainParameters->number_of_local_clusters;


		/*
		 * verbose points
		 */
		if (verbosity_level > 5)
		{
			std::cout << "." << std::flush;
		}

		/*
		 * if timesteps are not set, we quit as soon as the initial number of triangles was reached
		 */
		if (simulation_run_for_fixed_timesteps == -1)
		{
			if (simulation_run_for_fixed_simulation_time != -1)
			{
				if (cSimulation_MainParameters->simulation_timestamp_for_timestep > simulation_run_for_fixed_simulation_time)
					return false;
			}
			else if (simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation != -1)
			{
				static int consecutive_timesteps_with_equal_triangle_number_counter = 0;

				if (simloop_prev_number_of_triangles == cSimulation_MainParameters->number_of_local_cells)
				{
					consecutive_timesteps_with_equal_triangle_number_counter++;

					if (consecutive_timesteps_with_equal_triangle_number_counter > simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation)
					{
						std::cout << std::endl;
						std::cout << " + Terminating after " << simulation_terminate_after_n_timesteps_with_equal_number_of_cells_in_simulation << " timesteps with equal number of triangles -> EXIT" << std::endl;
						std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
						std::cout << std::endl;
						return false;
					}
				}

				simloop_prev_number_of_triangles = cSimulation_MainParameters->number_of_local_cells;
			}
#if !CONFIG_SIERPI_ENABLE_GUI
			else
			{
				if (cSimulation_MainParameters->number_of_local_initial_cells_after_domain_triangulation == cSimulation_MainParameters->number_of_local_cells)
				{
					std::cout << std::endl;
					std::cout << " + Initial number of triangles reached -> EXIT" << std::endl;
					std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
					std::cout << std::endl;
					return false;
				}
			}
#endif
		}
		else
		{
			if (simloop_timestep_nr >= simulation_run_for_fixed_timesteps)
				return false;
		}

		return true;
	}


	/**
	 * this method is executed at the end of the simulation
	 */
	void simulation_loopSuffix()
	{
		simloop_cStopwatch.stop();

		if (output_data_flags & OUTPUT_DATA_VTP_GRID)
		{
			if (mpi_rank == 0)
			{
				pvd_global_vtp_grid_description_file << "  </Collection>" << std::endl;
				pvd_global_vtp_grid_description_file << "</VTKFile>" << std::endl;
				pvd_global_vtp_grid_description_file.close();
			}
		}

		if (output_data_flags & OUTPUT_DATA_VTP_CLUSTERS)
		{
			if (mpi_rank == 0)
			{
				pvd_global_vtp_cluster_description_file << "  </Collection>" << std::endl;
				pvd_global_vtp_cluster_description_file << "</VTKFile>" << std::endl;
				pvd_global_vtp_cluster_description_file.close();
			}
		}


		if (verbosity_level > 1)
		{
			std::cout << "[ END ]" << std::endl;
			std::cout << std::endl;
		}


		if (verbosity_level > 2)
		{
			std::cout << std::endl;
			std::cout << "Timings for simulation phases:" << std::endl;
			std::cout << " + EdgeCommTime: " << simloop_edgeCommTime << std::endl;
			std::cout << " + AdaptiveTime: " << simloop_adaptiveTime << std::endl;
			std::cout << " + SplitJoinTime: " << simloop_splitJoinTime << std::endl;
			std::cout << std::endl;
		}

		double real_stoptime = simloop_cStopwatch();

		std::cout << simloop_timestep_nr << " TS (Timesteps)" << std::endl;
		std::cout << cSimulation_MainParameters->simulation_timestamp_for_timestep << " ST (SIMULATION_TIME)" << std::endl;
		std::cout << cSimulation_MainParameters->simulation_parameter_global_timestep_size << " TSS (Timestep size)" << std::endl;
		std::cout << real_stoptime << " RT (REAL_TIME)" << std::endl;
		std::cout << simloop_sum_number_of_local_cells << " CP (Cells processed)" << std::endl;
		std::cout << (double)real_stoptime/(double)simloop_timestep_nr << " ASPT (Averaged Seconds per Timestep)" << std::endl;
		std::cout << (double)simloop_sum_number_of_local_cells/(double)simloop_timestep_nr << " CPST (Cells Processed in Average per Simulation Timestep)" << std::endl;

		std::cout << (double)simloop_sum_number_of_local_clusters/(double)simloop_timestep_nr << " ACPST (Averaged number of clusters per Simulation Timestep)" << std::endl;

		double MCPS = ((double)simloop_sum_number_of_local_cells/real_stoptime)*0.000001;
		std::cout << MCPS << " MCPS (Million Cells per Second) (local)" << std::endl;
		std::cout << ((double)simloop_sum_number_of_local_cells/(simloop_cStopwatch()*(double)threading_number_of_threads_to_use))*0.000001 << " MCPSPT (Million Clusters per Second per Thread)" << std::endl;

		double elementdata_megabyte_per_timestep = ((double)sizeof(CHyperbolicTypes::CSimulationTypes::CCellData)*2.0)*((double)simloop_sum_number_of_local_cells/(double)simloop_timestep_nr)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_timestep << " EDMBPT (CellData Megabyte per Timestep (RW))" << std::endl;

		double elementdata_megabyte_per_second = ((double)sizeof(CHyperbolicTypes::CSimulationTypes::CCellData)*2.0)*((double)simloop_sum_number_of_local_cells/(double)real_stoptime)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_second << " EDMBPS (CellData Megabyte per Second (RW))" << std::endl;

		if (cSimulation_MainParameters->flops_cell_update >= 0)
		{
			std::cout << (double)cSimulation_MainParameters->flops_cell_update << " Flops per cell update (Matrix Multiplication)" << std::endl;
			std::cout << ((double)cSimulation_MainParameters->flops_cell_update*MCPS*0.001) << " GFLOPS" << std::endl;
		}


		if (verbosity_level > 1)
			std::cout << "[ END ]" << std::endl;
	}


	/**
	 * Deconstructor
	 */
	virtual ~CMain()
	{
		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
	}

};

#endif
