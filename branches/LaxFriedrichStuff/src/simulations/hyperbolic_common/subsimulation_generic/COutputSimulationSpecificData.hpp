/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef COUTPUT_SIMULATION_SPECIFIC_DATA_HPP
#define COUTPUT_SIMULATION_SPECIFIC_DATA_HPP


#include "../../../config.h"

#if CONFIG_SUB_SIMULATION_TSUNAMI
#	define CONFIG_SUBSIMULATION_STRING "tsunami"
#	include "../subsimulation_tsunami/COutputSimulationSpecificData.hpp"
#elif CONFIG_SUB_SIMULATION_EULER
#	define CONFIG_SUBSIMULATION_STRING "euler"
#	include "../subsimulation_euler/COutputSimulationSpecificData.hpp"
#else
#	error "unknown sub-simulation"
#endif


#endif
