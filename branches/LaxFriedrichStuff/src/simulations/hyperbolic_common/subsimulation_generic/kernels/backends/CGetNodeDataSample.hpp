/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHYPERBOLIC_GET_NODE_DATA_SAMPLE_HPP_
#define CHYPERBOLIC_GET_NODE_DATA_SAMPLE_HPP_


#if CONFIG_SUB_SIMULATION_TSUNAMI
#	include "simulations/hyperbolic_common/subsimulation_tsunami/kernels/backends/CGetNodeDataSample.hpp"
#elif CONFIG_SUB_SIMULATION_EULER
#	include "simulations/hyperbolic_common/subsimulation_euler/kernels/backends/CGetNodeDataSample.hpp"
#else
#	error "unknown sub-simulation"
#endif

#endif
