/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CBASISFUNCTIONS_ORDER_0_HPP_
#define CBASISFUNCTIONS_ORDER_0_HPP_
/**
 * basis functions for 1st order
 *
 * 0: nodal point at hypotenuse edge
 * 1: nodal point at right edge
 * 2: nodal point at left edge
 */
class CBasisFunctions
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:

	typedef T (*FBasisFunctions) (T x, T y);

	static void setup(
			T *i_nodalCoords,	///< nodal coordinates
			int i_verbosity_level
	)
	{
		return;
	}

	static inline T eval(
			int id,
			T x,
			T y
	)	{
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return 1.0;
	}

	static inline T eval_dx(
			int id,
			T x,
			T y
	)	{
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return 0.0;
	}

	static inline T eval_dy(
			int id,
			T x,
			T y
	)	{
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return 0.0;
	}

	static T* getNodalCoords()
	{
		static T nodal_coords[] = {
				1.0/3.0, 1.0/3.0
		};

		return nodal_coords;
	}

	static int getNumberOfFunctions()
	{
		return 1;
	}
};



#endif /* CBASISFUNCTIONS_ORDER_0_HPP_ */
