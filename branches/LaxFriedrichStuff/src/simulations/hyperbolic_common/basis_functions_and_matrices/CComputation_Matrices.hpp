/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION_MATRICES_HPP_
#define CCOMPUTATION_MATRICES_HPP_



#if SIMULATION_HYPERBOLIC_CONST_MATRICES_ID == -1

#	include "CComputation_Matrices_Nodal.hpp"

#else

#	include "CComputation_Matrices_Const.hpp"

#endif


#endif /* CCOMPUTATION_MATRICES_HPP_ */
