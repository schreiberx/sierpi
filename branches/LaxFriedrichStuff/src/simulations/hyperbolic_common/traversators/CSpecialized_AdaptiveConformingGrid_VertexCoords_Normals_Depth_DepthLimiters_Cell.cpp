/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData.hpp"
#include "../subsimulation_generic/kernels/simulation/CAdaptive_Hyperbolic.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/CDatasets.hpp"

namespace sierpi
{
namespace travs
{
	class cSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData_Private	:
		public CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<
			sierpi::kernels::CAdaptive_Hyperbolic,
			CHyperbolicTypes
		>
	{
	};


CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData()
{
	generic_traversator = new cSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData_Private;
}


CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::~CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData()
{
	delete generic_traversator;
}



void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_RootTraversator(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_RootTraversator(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}


void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_KernelClass(
		T i_square_side_length,

		T i_refine_parameter_0,
		T i_coarsen_parameter_0,

		T i_refine_parameter_1,
		T i_coarsen_parameter_1,

		CDatasets *i_cDatasets
)
{
	generic_traversator->cKernelClass.setup_WithParameters(
			i_square_side_length,

			i_refine_parameter_0,
			i_coarsen_parameter_0,

			i_refine_parameter_1,
			i_coarsen_parameter_1,

			i_cDatasets
		);
}


bool CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionFirstTraversal(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->firstTraversal.action(cStacks);
	return repeat_traversal;
}


bool CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionMiddleTraversals_Serial(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Serial(cStacks, repeat_traversal);
	return repeat_traversal;
}


bool CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionMiddleTraversals_Parallel(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Parallel(cStacks, repeat_traversal);
	return repeat_traversal;
}


void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionLastTraversal_Parallel(
		CHyperbolicTypes::CSimulationStacks *cStacks,
		CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionLastTraversal_Serial(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_RootCluster(p_triangleFactory);
}


/**
 * setup the initial cluster traversal for the given factory
 */
void CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_Cluster(
		CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Cluster(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->cKernelClass.setup_WithKernel(
			parent.generic_traversator->cKernelClass
		);
}



}
}
