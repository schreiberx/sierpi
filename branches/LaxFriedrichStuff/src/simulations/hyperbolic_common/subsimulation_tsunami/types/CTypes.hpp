/*
 * CTsunamiTypes.hpp
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMITYPES_HPP_
#define CTSUNAMITYPES_HPP_

#include <vector>
#include <iostream>
#include <limits>


#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"

#include "../../subsimulation_generic/types/CValidation_EdgeData.hpp"
#include "../../subsimulation_generic/types/CValidation_CellData.hpp"
#include "../../basis_functions_and_matrices/CComputation_Matrices.hpp"


/**
 * Degree of freedoms for one point in tsunami simulation
 */
class CTsunamiSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h;	// height
	T hu;	// x-component of momentum
	T hv;	// y-component of momentum

	union	{
		T b;	// bathymetry
		T cfl1_timestep_size;	// max wave speed for flux computations
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b+h;
	}

#if 0
	/**
	 * setup routine for simulation parameters default edge setup
	 */
	template <int SOAdisplacement>
	static void setupDefaultValues(
			T default_values[4],
			T *io_dof0
	)
	{
		*(io_dof0+0*SOAdisplacement) = default_values[0];
		*(io_dof0+1*SOAdisplacement) = 0;
		*(io_dof0+2*SOAdisplacement) = 0;
		*(io_dof0+3*SOAdisplacement) = -default_values[0];
	}
#endif

	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			const CTsunamiSimulationNodeData &d
	)	{
		os << "CTsunamiSimulationNodeData:" << std::endl;
		os << " + h: " << d.h << std::endl;
		os << " + hu: " << d.hu << std::endl;
		os << " + hv: " << d.hv << std::endl;
		os << " + b/max_wave_speed: " << d.b << std::endl;
		os << " + vx: " << d.hu/d.h << std::endl;
		os << " + vy: " << d.hv/d.h << std::endl;
		os << " + horizon: " << (d.h + d.b) << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,		///< cout
			T x_axis_x,				///< x-axis of new basis (x-component)
			T x_axis_y				///< x-axis of new basis (y-component)
	) const
	{
		os << "Tsunami DOF Information:" << std::endl;
		os << " + h: " << h << std::endl;

		T thu = hu;
		T thv = hv;
		CTriangle_VectorProjections::referenceToWorld(&thu, &thv, x_axis_x, x_axis_y);

		os << " + hu: " << thu << std::endl;
		os << " + hv: " << thv << std::endl;

		os << " + b/max_wave_speed: " << b << std::endl;

		os << " + vx: " << thu/h << std::endl;
		os << " + vy: " << thv/h << std::endl;
		os << " + horizon: " << (h + b) << std::endl;

		os << std::endl;
	}
};





/**
 * Degree of freedoms for one point in tsunami simulation
 */
template <int N>
class CTsunamiSimulationNodeDataSOA
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h[N];	// height
	T hu[N];	// x-component of momentum
	T hv[N];	// y-component of momentum

	union	{
		T b[N];	// bathymetry
		T cfl1_timestep_size[N];	// max wave speed for flux computations
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b[0]+h[0];
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T default_values[4])
	{
		for (int i = 0; i < N; i++)
		{
			h[i] = default_values[0];
			hu[i] = 0;
			hv[i] = 0;
			b[i] = -default_values[0];
		}
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			const CTsunamiSimulationNodeDataSOA &d
	)	{
		os << "CTsunamiSimulationNodeDataSOA [h, hu, hv, b/max_wav_speed, vx, vy, horizon]" << std::endl;
		for (int i = 0; i < N; i++)
		{
			std::cout << " + " << i << ": [" <<
				d.h[i] << ", " <<
				d.hu[i] << ", " <<
				d.hv[i] << ", " <<
				d.b[i] << ", " <<
				d.hu[i]/d.h[i] << ", " <<
				d.hv[i]/d.h[i] << ", " <<
				(d.h[i]+d.b[i]) << "]" << std::endl;
		}

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,	///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		os << "CTsunamiSimulationNodeDataSOA:" << std::endl;
		for (int i = 0; i < N; i++)
		{
			os << " + h: " << h[i] << std::endl;

			T thu = hu[i];
			T thv = hv[i];
			CTriangle_VectorProjections::referenceToWorld(&thu, &thv, x_axis_x, x_axis_y);

			os << " + hu: " << thu << std::endl;
			os << " + hv: " << thv << std::endl;

			os << " + b/max_wave_speed: " << b[i] << std::endl;

			os << " + vx: " << thu/h[i] << std::endl;
			os << " + vy: " << thv/h[i] << std::endl;
			os << " + horizon: " << (h[i] + b[i]) << std::endl;

			os << std::endl;
		}
	}
};


/**
 * Edge communication data during tsunami simulation
 */
class CTsunamiSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CTsunamiSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS> dofs;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

	T CFL1_scalar;


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[])
	{
		dofs.setupDefaultValues(i_default_values);
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			 const CTsunamiSimulationEdgeData &d
	)	{

		os << "Edge Data [h, hu, hv, b/max_wav_speed, vx, vy, horizon]" << std::endl;
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			std::cout << " + " << i << ": [" <<
				d.dofs.h[i] << ", " <<
				d.dofs.hu[i] << ", " <<
				d.dofs.hv[i] << ", " <<
				d.dofs.b[i] << ", " <<
				d.dofs.hu[i]/d.dofs.h[i] << ", " <<
				d.dofs.hv[i]/d.dofs.h[i] << ", " <<
				(d.dofs.h[i]+d.dofs.b[i]) << "]" << std::endl;
		}
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};


class CTsunamiSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * weights for basis functions
	 */
	CTsunamiSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS> dofs;

#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA

	bool refine;
	bool coarsen;

#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(T default_values[])
	{
		dofs.setupDefaultValues(default_values);

//		cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
	}



	friend
	::std::ostream&
	operator<<	(
		::std::ostream& os,
		const CTsunamiSimulationCellData &d
	)	{

		os << "Cell Data" << std::endl;
		os << d.dofs << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << d.validation << std::endl;
#endif
		return os;
	}

#if 0
	/**
	 * compute and store node data
	 */
	void computeNodeData(
			T i_ref_coord_x,	///< x-coordinate in reference system
			T i_ref_coord_y,	///< y-coordinate in reference system
			CTsunamiSimulationNodeData *o_cNodeData
	)	const {

		T weight = CBasisFunctions::eval(0, i_ref_coord_x, i_ref_coord_y);

		o_cNodeData->h	= weight * dofs.h[0];
		o_cNodeData->hu	= weight * dofs.hu[0];
		o_cNodeData->hv	= weight * dofs.hv[0];
		o_cNodeData->b	= weight * dofs.b[0];

		/**
		 * NOTE ON BATHYMETRY DATA:
		 *
		 * This data may not be interpolated since constant functions are
		 * only available in modal (frequency) basis representations.
		 */

		for (int i = 1; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			weight = CBasisFunctions::eval(i, i_ref_coord_x, i_ref_coord_y);

			o_cNodeData->h	+= weight * dofs.h[i];
			o_cNodeData->hu	+= weight * dofs.hu[i];
			o_cNodeData->hv	+= weight * dofs.hv[i];
			o_cNodeData->b	+= weight * dofs.b[i];
		}
	}
#endif


	void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		dofs.outputVerboseData(os, x_axis_x, x_axis_y);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << validation << std::endl;
#endif
	}
};

#include "../../subsimulation_generic/types/CVisualizationVertexData.hpp"



/**
 * simulation types which
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CTsunamiSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CTsunamiSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CTsunamiSimulationEdgeData CEdgeData;
	};



	/**
	 * This class implements all types which are used for visualization
	 */
	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CTsunamiSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};


	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;
};




#endif /* CTSUNAMITYPES_HPP_ */
