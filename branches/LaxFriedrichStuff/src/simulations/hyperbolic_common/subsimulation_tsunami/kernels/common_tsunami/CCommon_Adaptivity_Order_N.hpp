/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "simulations/hyperbolic_common/basis_functions_and_matrices/CComputation_Matrices.hpp"
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CCommon_Adaptivity_Order_N
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	// typedef for CAdaptivity_default_handlers (see include at end of this file)
	typedef CCommon_Adaptivity_Order_N CAdaptivity_Common;
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/common/CAdaptivity_default_handlers.hpp"

	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CDatasets *cDatasets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;

	/**
	 * constructor
	 */
	CCommon_Adaptivity_Order_N()	:
		cathetus_side_length(-1),
		cDatasets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}

#if 0
	/**
	 * get center of weight for triangle
	 */
	inline void computeCenterOfWeight(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);
	}



	/**
	 * get center of weight for triangle for both children
	 */
	inline void computeCenterOfWeightForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;
	}
#endif


	/**
	 * setup both refined elements
	 */
	inline void refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			const CTsunamiSimulationCellData *i_cCellData,
			CTsunamiSimulationCellData *o_left_cCellData,
			CTsunamiSimulationCellData *o_right_cCellData
	)	{
		assert(i_cCellData != o_right_cCellData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		/*
		 * LEFT
		 */
		// H
		CComputation_Matrices::mul_adaptivity_refine_left_child_matrix(i_cCellData->dofs.h, o_left_cCellData->dofs.h);

		// HU, HV
		CComputation_Matrices::mul_adaptivity_refine_left_child_matrix(i_cCellData->dofs.hu, o_left_cCellData->dofs.hu);
		CComputation_Matrices::mul_adaptivity_refine_left_child_matrix(i_cCellData->dofs.hv, o_left_cCellData->dofs.hv);
		CComputation_Matrices::mul_adaptivity_project_momentum_reference_to_left_child(o_left_cCellData->dofs.hu, o_left_cCellData->dofs.hv);

		// B
		CComputation_Matrices::mul_adaptivity_refine_left_child_matrix(i_cCellData->dofs.b, o_left_cCellData->dofs.b);


		/*
		 * RIGHT
		 */
		// H
		CComputation_Matrices::mul_adaptivity_refine_right_child_matrix(i_cCellData->dofs.h, o_right_cCellData->dofs.h);

		// HU, HV
		CComputation_Matrices::mul_adaptivity_refine_right_child_matrix(i_cCellData->dofs.hu, o_right_cCellData->dofs.hu);
		CComputation_Matrices::mul_adaptivity_refine_right_child_matrix(i_cCellData->dofs.hv, o_right_cCellData->dofs.hv);
		CComputation_Matrices::mul_adaptivity_project_momentum_reference_to_right_child(o_right_cCellData->dofs.hu, o_right_cCellData->dofs.hv);

		// B
		CComputation_Matrices::mul_adaptivity_refine_right_child_matrix(i_cCellData->dofs.b, o_right_cCellData->dofs.b);



#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_left_cCellData->refine = false;
		o_left_cCellData->coarsen = false;
		o_right_cCellData->refine = false;
		o_right_cCellData->coarsen = false;
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRefineLeftAndRight(
				i_normal_hyp_x,	i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_depth,
				&o_left_cCellData->validation,
				&o_right_cCellData->validation
			);
#endif
	}




	/**
	 * setup coarsed elements
	 */
	inline void coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CTsunamiSimulationCellData *o_cCellData,
			CTsunamiSimulationCellData *i_left_cCellData,
			CTsunamiSimulationCellData *i_right_cCellData
	)	{

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		{
			T mx = (i_vertex_left_x + i_vertex_right_x)*(T)0.5;
			T my = (i_vertex_left_y + i_vertex_right_y)*(T)0.5;

			i_left_cCellData->validation.testVertices(i_vertex_top_x, i_vertex_top_y, i_vertex_left_x, i_vertex_left_y, mx, my);
			i_right_cCellData->validation.testVertices(i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y, mx, my);
		}
#endif


		/*
		 * LEFT
		 */
		// H
		CComputation_Matrices::mul_adaptivity_coarsen_left_child_matrix(i_left_cCellData->dofs.h, o_cCellData->dofs.h);

		// HU,HV
		CComputation_Matrices::mul_adaptivity_coarsen_left_child_matrix(i_left_cCellData->dofs.hu, o_cCellData->dofs.hu);
		CComputation_Matrices::mul_adaptivity_coarsen_left_child_matrix(i_left_cCellData->dofs.hv, o_cCellData->dofs.hv);
		CComputation_Matrices::mul_adaptivity_project_momentum_left_child_to_reference(o_cCellData->dofs.hu, o_cCellData->dofs.hv);

		// B
		CComputation_Matrices::mul_adaptivity_coarsen_left_child_matrix(i_left_cCellData->dofs.b, o_cCellData->dofs.b);



		/*
		 * RIGHT
		 */
		// H
		T o_cCellData_dofs_buf1[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T o_cCellData_dofs_buf2[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

		CComputation_Matrices::mul_adaptivity_coarsen_right_child_matrix(i_right_cCellData->dofs.h, o_cCellData_dofs_buf1);
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.h[i] += o_cCellData_dofs_buf1[i];

		// HU, HV
		CComputation_Matrices::mul_adaptivity_coarsen_right_child_matrix(i_right_cCellData->dofs.hu, o_cCellData_dofs_buf1);
		CComputation_Matrices::mul_adaptivity_coarsen_right_child_matrix(i_right_cCellData->dofs.hv, o_cCellData_dofs_buf2);
		CComputation_Matrices::mul_adaptivity_project_momentum_right_child_to_reference(o_cCellData_dofs_buf1, o_cCellData_dofs_buf2);
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.hu[i] += o_cCellData_dofs_buf1[i];
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.hv[i] += o_cCellData_dofs_buf2[i];

		// B
		CComputation_Matrices::mul_adaptivity_coarsen_right_child_matrix(i_right_cCellData->dofs.b, o_cCellData_dofs_buf2);
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.b[i] += o_cCellData_dofs_buf2[i];



#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_cCellData->refine = false;
		o_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cCellData->validation.setupCoarsen(
				i_normal_hyp_x, i_normal_hyp_y,
				i_normal_right_x, i_normal_right_y,
				i_normal_left_x, i_normal_left_y,
				i_depth,
				&i_left_cCellData->validation,
				&i_right_cCellData->validation
		);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
