/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_COLUMN_ORDER_N_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_ORDER_N_TSUNAMI_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "../common_tsunami/CCommon_Adaptivity.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/CDatasets.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename CCellData, typename TSimulationStacks>
class CSetup_Column_Order_N	:
	public CCommon_Adaptivity
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<CSetup_Column_Order_N, CHyperbolicTypes>	TRAV;

	typedef T TVertexScalar;
	typedef CTsunamiSimulationEdgeData CEdgeData;

	/// center of column
	T column_center_x, column_center_y;

	// radius of column
	T column_radius;
	T column_radius_squared;


	CSetup_Column_Order_N()	:
		column_center_x(0),
		column_center_y(0),
		column_radius(0),
		column_radius_squared(0)
	{
	}

	/**
	 * compute and return squared distance to center
	 */
	inline T squaredDistToCenter(
			T i_px,
			T i_py
	)
	{
		TVertexScalar x = column_center_x-i_px;
		TVertexScalar y = column_center_y-i_py;
		return x*x+y*y;
	}


	/**
	 * check whether a point is inside the circle
	 */
	inline bool insideCircle(
			TVertexScalar px,
			TVertexScalar py
	)
	{
		return squaredDistToCenter(px, py) < column_radius_squared;
	}


	inline bool should_refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int depth,

			CTsunamiSimulationCellData *o_cellData
	)
	{

		/*
		 * check whether the line on the circle intersects with the triangle
		 */
		int counter = 0;
		counter += (int)insideCircle(i_vertex_left_x,	i_vertex_left_y);
		counter += (int)insideCircle(i_vertex_right_x,	i_vertex_right_y);
		counter += (int)insideCircle(i_vertex_top_x,	i_vertex_top_y);

		if (counter == 0 || counter == 3)
		{
			/*
			 * completely outside circle
			 */
			// nothing to to & no refinement request
			return false;
		}

		return true;
	}


	inline bool should_coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int depth,
			
			CTsunamiSimulationCellData *i_cCellData
	)
	{
		/**
		 * we use the coarsening callback to update the column inner elements data (even if i_cCellData data should not be accessed)
		 */
/*
		if (!insideCircle(i_vertex_left_x, i_vertex_left_y))
			return false;

		if (!insideCircle(i_vertex_right_x, i_vertex_right_y))
			return false;

		if (!insideCircle(i_vertex_top_x, i_vertex_top_y))
			return false;
*/
		return false;
	}


	void setup_Parameters(
			T i_column_center_x,
			T i_column_center_y,
			T i_column_radius,

			int i_setup_method_id,
			CDatasets *i_cDatasets
	)
	{
		column_center_x = i_column_center_x;
		column_center_y = i_column_center_y;
		column_radius = i_column_radius;
		column_radius_squared = column_radius*column_radius;

		cDatasets = i_cDatasets;
	}


	void setup_WithKernel(
			CSetup_Column_Order_N<CCellData, TSimulationStacks> &i_parent
	)
	{
		column_center_x = i_parent.column_center_x;
		column_center_y = i_parent.column_center_y;
		column_radius = i_parent.column_radius;
		column_radius_squared = i_parent.column_radius_squared;

		cDatasets = i_parent.cDatasets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
