/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#include "../../CConfig.hpp"
#include "CAdaptive_Hyperbolic_Order_N.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Hyperbolic_Order_N CAdaptive_Hyperbolic;
	}
}
