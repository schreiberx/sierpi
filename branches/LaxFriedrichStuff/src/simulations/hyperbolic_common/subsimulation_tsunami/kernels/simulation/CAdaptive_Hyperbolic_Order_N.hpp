/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_CADAPTIVE_HYPERBOLIC_ORDER_N_HPP_
#define KERNEL_CADAPTIVE_HYPERBOLIC_ORDER_N_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"
#include "../common_tsunami/CCommon_Adaptivity.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
class CAdaptive_Hyperbolic_Order_N
	: public CCommon_Adaptivity
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<CAdaptive_Hyperbolic_Order_N, CHyperbolicTypes>	TRAV;
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;

	/*
	 * refinement/coarsening parameters
	 */
	T refine_parameter_0;
	T coarsen_parameter_0;

	T refine_parameter_1;
	T coarsen_parameter_1;


	CAdaptive_Hyperbolic_Order_N()	:
		refine_parameter_0(-1),
		coarsen_parameter_0(-1),
		refine_parameter_1(-1),
		coarsen_parameter_1(-1)
	{
	}



	inline bool should_refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int depth,
			CTsunamiSimulationCellData *i_cCellData
	)
	{
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

		return i_cCellData->refine;

#else

		/**
		 * adaptive refinement triggered via height
		 */
		T h = 0;

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			h += i_cCellData->dofs.h[i]+i_cCellData->dofs.b[i];

		h /= (T)SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		return (h > refine_parameter_0);
			return true;
#endif

	}



	inline bool should_coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int depth,
			CTsunamiSimulationCellData *element
	)
	{
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

		return element->coarsen;

#else

		/**
		 * adaptive refinement triggered via height
		 */
		T h = 0;

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			h += element->dofs.h[i]+element->dofs.b[i];

		h /= (T)SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		return (h < coarsen_parameter_0);
			return true;

#endif

#if 0
		/**
		 * adaptive refinement triggered via slope
		 */
		T d1 = std::abs(element->hyp_edge.h - element->left_edge.h);
		T d2 = std::abs(element->right_edge.h - element->left_edge.h)*std::sqrt2<T>();
		T d3 = std::abs(element->hyp_edge.h - element->right_edge.h);

		// TODO: do this computation only once
		// we can reuse this value assuming that should_refine is always called before should_coarsen - TODO: is this true?!?
		T m = std::max(d1, std::max(d2, d3))/(getUnitCathetusLengthForDepth(depth)*cathetus_side_length);

		if (m > coarsen_parameter_1)
			return false;
#endif
		return true;
	}


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int depth,

			CTsunamiSimulationCellData *io_cellData
	)
	{
	}

	void setup_WithParameters(
			T i_cathetus_side_length,

			T i_refine_parameter_0,
			T i_coarsen_parameter_0,

			T i_refine_parameter_1,
			T i_coarsen_parameter_1,

			CDatasets *i_cDatasets
	)
	{
		refine_parameter_0 = i_refine_parameter_0;
		coarsen_parameter_0 = i_coarsen_parameter_0;

		refine_parameter_1 = i_refine_parameter_1;
		coarsen_parameter_1 = i_coarsen_parameter_1;

		cathetus_side_length = i_cathetus_side_length;

		cDatasets = i_cDatasets;
	}


	void setup_WithKernel(const CAdaptive_Hyperbolic_Order_N &parent)
	{
		refine_parameter_0 = parent.refine_parameter_0;
		coarsen_parameter_0 = parent.coarsen_parameter_0;

		refine_parameter_1 = parent.refine_parameter_1;
		coarsen_parameter_1 = parent.coarsen_parameter_1;

		cathetus_side_length = parent.cathetus_side_length;

		cDatasets = parent.cDatasets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
