/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 27, 2012
 *      Author: Alexander Breuer <breuera@in.tum.de>,
 *              Martin Schreiber <martin.schreiber@in.tum.de>,
 *              Aditya Ghantasala <shine.aditya@gmail.com>
 */



#ifndef CSINGLEWAVEONCOMPOUNDBEACH_HPP_
#define CSINGLEWAVEONCOMPOUNDBEACH_HPP_

#include <cmath>
#include "netcdfcpp.h"

#include "../../datasets_common/CDataSet_Interface.hpp"
#include "helper_routines/Variable.hpp"


#include "mainthreading/CMainThreading.hpp"


#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CConfig.hpp"
#	include "../types/CTypes.hpp"
#endif

template <typename T>
class CSingleWaveOnCompositeBeach	:
	public CDataSet_Interface
{
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	/**
	 * Single Wave on Composite Beach Benchmark problem.
	 *
	 * More information is available in the nthmp github repository
	 * https://github.com/rjleveque/nthmp-benchmark-problems
	 *
	 *   G10
	 *   |
	 *   | G9
	 * | | |
	 * | | |
	 * | | |
	 *  \| |
	 *   \ |    G8      G7
	 *    \|__  |       |
	 *        -----_____| G6
	 *                   \ |G5        G4
	 *                    \||         |
	 *                     \|____________________
	 *
	 * |---|------------|---|---------------------
	 *  0.9     2.93    4.36           L
	 *
	 *
	 *                      |
	 *                    Origin
	 *
	 * Contains all the functions needed for implementing the
	 *
	 * Also this Benchmark problem has three cases depending on the
	 * length(L) of the beach before the slope begins.
	 *
	 * Case A: L =  2.40 meters
	 * Case B: L =  0.98 meters
	 * Case C: L =  0.64 meters
	 *
	 */

private:
	// #################### VAIRABLES #######################
	T d;

	///< Undisturbed water Height. By default it is set to 0.218 meters(Also the maximum depth of water)
	T L;

	///< The length of the beach before the slope of the beach.
	T H;

	///< Total Height of water... h+bathymetry.
	T h;

	///< elevation of water surface above the mean height d (h is read from the .nc file according to the case)
	T gravity;

	///< Gravity in this problem is 9.81 m/s^2;
	T beach_slope_1;

	///< Slope of the first part of the beach.
	T beach_slope_2;

	///< Slope of the second/middle part of the beach.
	T beach_slope_3;

	///< Slope of the last part/part next to the wall of the beach.
	char simulation_case;

	///< This variable stores the case which is to be simulated.
	std::string filename;

	///< Name and path of the NetCDF file containing the input values
	long time_position;

	///< Variable to store the corresponding index of time from NetCDF file
	NcFile* dataFile;
	///< NetCDF File variable

	T* G4Vect;
	///< Pointer for storing the values of height at Guage G4

	T* tVect;
	///< Time vector from NetCDF file
	long tVectLength;
	/// benchmark data
	benchMark::CVerify<T> l_h;

	T time_displacement;

	///< Class object for


	// #################### Functions/Methods #######################

	/**
	 * Method to calculate the time index in the NetCDF file
	 * @param i_time 	- Global Simulation Time
	 * @param i_timeTol	- Time tolerance (OPTIONAL --> Set to 0.005 by Default)
	 */
	bool p_calculateTimePosition(
			T i_timestamp,
			T i_timeTol = T(0.005)
	)	{
		//searching for the corresponding time index
		for(long i = 0; i < tVectLength; i++) {
			//Calculating the limits for the time
			T timeLlim = (tVect[i]-i_timeTol*tVect[i]);
			T timeHlim = (tVect[i]+i_timeTol*tVect[i]);

			if(timeLlim < i_timestamp && i_timestamp < timeHlim) {
				time_position = i;
				return true;
			}
		}

		return false;
	}


public:
	void setSimulationCase(char i_case)
	{
		simulation_case = i_case;
	}


public:
	/**
	 * Constructor for the Single wave on Composite beach Benchmark Class
	 */
	CSingleWaveOnCompositeBeach(
			const T i_d = T(0.218),
			const T i_gravity = T(9.81)		///< gravity is the acceleration due to gravity. (optional -> set to 9.81)
	)	:
		simulation_case('A'),
		time_displacement(0)
	{
		setup(i_d, i_gravity);
	}


	/**
	 * get L value (distance of right boundary to beach)
	 */
	T getCaseDependentL()
	{
		switch(simulation_case)
		{
		case 'A':
			return 2.40;
		case 'B':
			return 0.98;
		case 'C':
			return 0.64;
		default:
			return -9999999;
		}
	}



	/**
	 * @param gravity is the acceleration due to gravity. (optional -> set to 9.81)
	 */
	void setup(
			const T i_d = (T)0.218,
			const T i_gravity = (T)9.81
	)	{

		//Variable Initialization
		d = i_d;
		gravity = i_gravity;

		time_position = 0;
		tVectLength = 0;
		tVect = NULL;

		L = getCaseDependentL();

		filename = "data/tsunami_benchmarks/SingleWaveonCompositeBeach_case_";
		filename += simulation_case;
		filename += ".nc";

		l_h.setup(filename, "time", "x", "y", "h");
		///< Setup for Verify Class object


		//########## NetCDF Stuff ##########
		//Reading the .nc file in
		dataFile = new NcFile(filename.c_str(),NcFile::ReadOnly);
		if(!dataFile->is_valid())
			std::cout<<"Could not open the file containing the input guage Readings"<<std::endl;

		//Obtaining Time vector length from the NetCDF file
		tVectLength = dataFile->get_dim("time")->size();

		//Nc Variable Declaration
		NcVar* l_G4Var = dataFile->get_var("G4");
		NcVar* l_tVar = dataFile->get_var("time");

		//Memory Allocation
		tVect = new T[dataFile->get_dim("time")->size()];
		G4Vect = new T[dataFile->get_dim("time")->size()];

		//Reading the vectors of time and G4
		l_G4Var->get(G4Vect, tVectLength );
		l_tVar->get(tVect, tVectLength );

		/*for(int i=0;i<tVectLength;i++)
		{
			std::cout<<"Value of G4 at "<<i<<" is:: "<<hVect[i]<<std::endl;
		
		}
		std::getchar();*/
	}



	/**
	 * Returns the Bathymetry at a given co ordinate
	 *
	 * @param i_x 				- X coordinate
	 * @param i_y 				- Y coordinate
	 * @param i_level_of_detail	- Grid Fineness
	 * @return 					- Returns the Bathymetry at the given X coordinate
	 */
	T p_getBathymetryData(
			T i_x,
			T i_y,
			T i_level_of_detail
	)	{

		T l_bathymetry=0.;

		/*
		 * setup bathymetry data from right to left
		 */
		/*
		 * https://github.com/rjleveque/nthmp-benchmark-problems
		 *
		 *   G10
		 *   |
		 *   | G9
		 * | | |
		 * | | |
		 * | | |
		 *  \| |
		 *   \ |    G8      G7
		 *    \|__  |       |
		 *        -----_____| G6
		 *                   \ |G5        G4
		 *                    \||         |
		 *                     \|____________________
		 *
		 * |---|------------|---|---------------------
		 *  0.9     2.93    4.36           L
		 */


		
		//If the location is in the first part of the slope
		if(i_x<0 && i_x>-4.36){
			l_bathymetry = -(d+i_x/53);
			return l_bathymetry;
		}
		//If the location is in the second part/middle part of the slope
		if(i_x<0 && i_x<-4.36 && i_x>-7.29){
			l_bathymetry = 4.36/53 - 4.36/150 - i_x/150 -d;
			return l_bathymetry;
		}
		//If the location is in the last part/next to the wall of the slope
		if(i_x<0 && i_x<-7.29 && i_x>=-8.19){
			l_bathymetry = 4.36/53 + 2.93/150 - 7.29/13 -i_x/13 -d;
			return l_bathymetry;
		}
		//If the location is not in the slope region
		if(i_x>=0){
			l_bathymetry = -d;
			return l_bathymetry;
		}
		if(i_x<=-8.19){
			l_bathymetry = 0.5;
		}

		return l_bathymetry;
	}


	/**
	 * Returns the Water height at any given time at the inflow boundary
	 */
	T p_getWaterElevationAtBoundary(
			T i_timestamp			///< simulation time
	)	{
		
		if (p_calculateTimePosition(i_timestamp))
			return G4Vect[time_position];
		else
			return 0;
	}

	/**
	 * Returns the Water height at any given time at the inflow boundary
	 */
	T p_getWaterHeightAtBoundary(
			T i_timestamp,			///< simulation time
			T i_level_of_detail		///< LOD
	)	{
		return p_getWaterElevationAtBoundary(i_timestamp) - p_getBathymetryData(L,L,i_level_of_detail);
	}


	/**
	 * Returns the water height at any given location at initial time.
	 *
	 * @param i_x 					- x Coordinate
	 * @param i_y 					- y Coordinate (in 1D case its Zero)
	 * @param i_level_of_detail 	- Grid Fineness
	 */
	T p_getWaterSufaceHeight(T i_x, T i_y, T i_level_of_detail)
	{
		if(i_x < -8.19)
			return 0;
		else
			return -p_getBathymetryData(i_x,i_y,i_level_of_detail);
	}

#if 0
	/**
	 * Returns the wave velocity at any given location at initial time.
	 *
	 * @param i_x 					- x coordinate
	 * @param i_y 					- y Coordinate (in 1D case its Zero)
	 * @param i_level_of_detail 	- Grid Fineness
	 */
	T p_getVelocity(
			T i_x,
			T i_y,
			T i_level_of_detail
	){
		T l_velocity;

		l_velocity = 2*(sqrt(gravity*p_getWaterSufaceHeight(i_x,i_y,i_level_of_detail)) - sqrt(gravity*d));

		return l_velocity;
	}
#endif

#if 0
	/**
	 * Get the initial momentum according to the formula for the velocity in the SWOSB-benchmark.
	 *
	 *   u(x,0) = - \sqrt{g / d} \eta(x,0)
	 *
	 * More information is available in the nthmp github repository
	 * https://github.com/rjleveque/nthmp-benchmark-problems
	 *
	 * \return 					- Returns the initial Momentum at a given X coordinate.
	 */
	T p_getMomentum(
			T i_x,					///< X Coordinate
			T i_y,					///< Y Coordinate
			T i_level_of_detail		///< LOD
	) {
		T elevation = p_getWaterSurfaceElevation(i_x, i_y, i_level_of_detail);
		T velocity = -std::sqrt(gravity / d) * elevation;

		T h = elevation - p_getBathymetryData(i_x, i_x, i_level_of_detail);
		T l_momentum = velocity * h;

		if (h < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
			return (T)0.;

		return l_momentum;
	}
#endif

	/**
	 * This function sets the value for the ghost cell according to the INFLOW boundary condition
	 *          @param  	i_ghostVelocity 	- A reference to the momentum value on the ghost cell
	 * 			@param		time 				- Simulation global time
	 * 			@param		i_ghostVelocity 	- A reference to the velocity on the ghost cell
	 * 			@param		i_ghostHeight  		- A reference to the total Height on the ghost cell
	 *
	 */
	bool getBoundaryData(
			T i_x,
			T i_y,
			T i_level_of_detail,
			T i_timestamp,
			CNodeData *o_cNodeData
	)	{

		i_timestamp += time_displacement;

		T l_elevation = p_getWaterElevationAtBoundary(i_timestamp);
		//std::cout<<"Elevation at time "<<i_timestamp<<"is :: "<<l_elevation<<std::endl;
		o_cNodeData->b = p_getBathymetryData(L,L,i_level_of_detail);
		o_cNodeData->h = l_elevation-p_getBathymetryData(L,L,i_level_of_detail);//p_getWaterHeightAtBoundary(i_timestamp, i_level_of_detail);
		//std::cout<<"Total Height at time "<<i_timestamp<<"is :: "<<o_cNodeData->h<<std::endl;
		o_cNodeData->hv = 0;	// 1D benchmark -> set y-velocity to zero

		/*
		 * compute momentum
		 */
		assert(o_cNodeData->b <= 0);
		T velocity = -std::sqrt(gravity / (-o_cNodeData->b)) * l_elevation;

		T l_momentum = velocity * o_cNodeData->h;

		if (o_cNodeData->h < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
			o_cNodeData->hu = 0;
		else
			o_cNodeData->hu = l_momentum;

		return true;
	}


	/**
	 * Method provides the initial data at given coordinates.
	 * @param i_x				- X Coordinate
	 * @param i_y 				- Y Coordinate
	 * @param i_level_of_detail - Fineness of Grid
	 * @param o_nodal_data 		- Pointer to the object containing all the data about the node.
	 */
	void getNodalData(
			T i_x,
			T i_y,
			T i_level_of_detail,
			CNodeData *o_nodal_data
	){

		o_nodal_data->h =  p_getWaterSufaceHeight(i_x,i_y,i_level_of_detail);
		o_nodal_data->b =  p_getBathymetryData(i_x,i_y,i_level_of_detail);
		o_nodal_data->hu = 0;
		o_nodal_data->hv = 0;

	}


	/**
	 * Method puts out the description of the current benchmark
	 */
	void outputVerboseInformation(){

	}


	/**
	 * return dataset value
	 *
	 * id 0: bathymetry
	 * id 1: displacements
	 */
	CHyperbolicTypes::CSimulationTypes::T getDatasetValue(
			int i_dataset_id,		///< id of dataset to get value from
			T i_x,					///< x-coordinate in model-space
			T i_y,					///< x-coordinate in model-space
			T i_level_of_detail		///< level of detail (0 = coarsest level)
	)	{
		if (i_dataset_id == 0)
			return p_getBathymetryData(i_x, i_y, i_level_of_detail);

		if (i_dataset_id == 1)
			return 0;

		throw(std::runtime_error("invalid dataset id"));
		return 1;
	}


	///< BenchMarking stuff ...


	/**
	 * Method to provide the Benchmark Data at a given location and time
	 */
	bool getBenchmarkNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CNodeData *o_cNodeData	///< nodal data is written to this position
	)	{
		i_timestamp += time_displacement;

		T l_dummy=0;
		l_h.getBenchMarkNodalData(i_timestamp,i_x,i_y,i_level_of_detail,&l_dummy);

		o_cNodeData->h = l_dummy;
		return true;
	}


	~CSingleWaveOnCompositeBeach(){

	}


	/**
	 * load the datasets specified via cSimulationParameters
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)	{
		return false;
	}


	/**
	 * return true if the dataset was successfully loaded
	 */
	bool isDatasetLoaded()
	{
		return false;
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T *o_translate_x,	///< origin of domain in world-space
			T *o_translate_y,	///< origin of domain in world-space

			T *o_size_x,	///< size of domain in world-space
			T *o_size_y		///< size of domain in world-space
	)	{
		/*
		 *   G10
		 *   |
		 *   | G9
		 * | | |
		 * | | |
		 * | | |
		 *  \| |
		 *   \ |    G8      G7
		 *    \|__  |       |
		 *        -----_____| G6
		 *                   \ |G5        G4
		 *                    \||         |
		 *                     \|____________________
		 *
		 * |---|------------|---|-----------------|---
		 *  0.9     2.93    4.36           L
		 *
		 *                      |
		 *                    Origin
		 *
		 * |----------------SIZE------------------|
		 */

		T size = 8.20;	// beach area
		T translate = -8.20*0.5;

		T l = getCaseDependentL();
		size += l;
		translate += l*0.5;

		*o_size_x = size;
		*o_size_y = size;

		*o_translate_x = translate;
		*o_translate_y = 0;

	}
};

#endif /* CSINGLEWAVEONCOMPOUNDBEACH_HPP_ */
