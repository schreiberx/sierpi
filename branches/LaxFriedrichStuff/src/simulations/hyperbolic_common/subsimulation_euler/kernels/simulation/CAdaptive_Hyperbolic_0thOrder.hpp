/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CKERNEL_CADAPTIVE_EULER_0THORDER_HPP_
#define CKERNEL_CADAPTIVE_EULER_0THORDER_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

#include "../common/CAdaptivity_0thOrder.hpp"



namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement/coarsening for tsunami simulation of 0th order
 */
class CAdaptive_0thOrder	:
		public CAdaptivity_0thOrder
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<CAdaptive_0thOrder, CHyperbolicTypes>	TRAV;
	typedef CHyperbolicTypes::CSimulationTypes::T T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	typedef T TVertexScalar;

	T cathetus_side_length;


	/*
	 * refinement/coarsening parameters
	 */
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1
	T refine_parameter_0;
	T coarsen_parameter_0;

	T refine_parameter_1;
	T coarsen_parameter_1;
#endif



	CAdaptive_0thOrder()
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1
	:
		refine_parameter_0(-1),
		coarsen_parameter_0(-1),
		refine_parameter_1(-1),
		coarsen_parameter_1(-1)
#endif
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	inline bool should_refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,
			CCellData *i_cCellData
	)
	{
		if (i_cCellData->dofs[0].r > refine_parameter_0)
			return true;

		return false;
	}



	inline bool should_coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,
			CCellData *i_cCellData
	)
	{
		if (i_cCellData->dofs[0].r < coarsen_parameter_0)
			return true;

		return false;
	}


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int depth,

			CCellData *io_cCellData
	)
	{
	}


	void setup_WithParameters(
			T i_cathetus_side_length,

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1
			T i_refine_parameter_0,
			T i_coarsen_parameter_0,

			T i_refine_parameter_1,
			T i_coarsen_parameter_1,
#endif

			CDatasets *i_cDatasets
	)
	{
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1
		refine_parameter_0 = i_refine_parameter_0;
		coarsen_parameter_0 = i_coarsen_parameter_0;

		refine_parameter_1 = i_refine_parameter_1;
		coarsen_parameter_1 = i_coarsen_parameter_1;
#endif

		cathetus_side_length = i_cathetus_side_length;

		cDatasets = i_cDatasets;
	}


	void setup_WithKernel(const CAdaptive_0thOrder &parent)
	{
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1
		refine_parameter_0 = parent.refine_parameter_0;
		coarsen_parameter_0 = parent.coarsen_parameter_0;

		refine_parameter_1 = parent.refine_parameter_1;
		coarsen_parameter_1 = parent.coarsen_parameter_1;
#endif

		cathetus_side_length = parent.cathetus_side_length;

		cDatasets = parent.cDatasets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
