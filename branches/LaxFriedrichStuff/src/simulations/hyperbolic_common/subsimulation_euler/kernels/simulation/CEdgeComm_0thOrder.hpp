/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_EULER_0TH_ORDER_HPP_
#define CEDGECOMM_EULER_0TH_ORDER_HPP_

#include <stdexcept>

#include <cmath>

// generic tsunami types
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

// enum for boundary conditions
#include "libsierpi/grid/CBoundaryConditions.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"

// traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "simulations/hyperbolic_common/subsimulation_generic/flux_solver/CFluxSolver.hpp"
// edge projections
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"

// default parameters for edge comm
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Parameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 */

/**
 * update scheme
 *
 * specifies how to update element data:
 * 0: default update
 * 1: store updates only
 *
 * this gets important for Runge-Kutta methods
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_0thOrder	:
	public CEdgeComm_Parameters		// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_Parameters::setTimestepSize;
	using CEdgeComm_Parameters::setGravitationalConstant;
	using CEdgeComm_Parameters::setBoundaryDirichlet;
	using CEdgeComm_Parameters::setParameters;
	using CEdgeComm_Parameters::getCFLCellFactor;
	using CEdgeComm_Parameters::getBoundaryCFLCellFactor;

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
	using CEdgeComm_Parameters::setAdaptivityParameters;
#endif

	/**
	 * typedefs vor convenience
	 */
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	typedef CHyperbolicTypes::CSimulationTypes::T T;
	typedef CHyperbolicTypes::CSimulationTypes::T TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_0thOrder<t_storeElementUpdatesOnly>, CHyperbolicTypes> TRAV;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_0thOrder()
	{
		boundary_dirichlet.dofs[0].r = std::numeric_limits<T>::infinity();
		boundary_dirichlet.dofs[0].ru = std::numeric_limits<T>::infinity();
		boundary_dirichlet.dofs[0].rv = std::numeric_limits<T>::infinity();
		boundary_dirichlet.dofs[0].e = std::numeric_limits<T>::infinity();
	}

	/**
	 * RK2 helper function - first step
	 *
	 * compute yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CEulerSimulationCellData &i_f_t0,
			CEulerSimulationCellData &i_f_slope_t0,
			CEulerSimulationCellData *o_f_t1_approx,
			T i_timestep_size
	)
	{
		assert(false);
#if 0
		o_f_t1_approx->dofs[0].h	=	i_f_t0.dofs[0].h	+ i_timestep_size*i_f_slope_t0.dofs[0].r;
		o_f_t1_approx->dofs[0].ru	=	i_f_t0.dofs[0].ru	+ i_timestep_size*i_f_slope_t0.dofs[0].ru;
		o_f_t1_approx->dofs[0].rv	=	i_f_t0.dofs[0].rv	+ i_timestep_size*i_f_slope_t0.dofs[0].rv;
		o_f_t1_approx->dofs[0].b	=	i_f_t0.dofs[0].e;

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CEulerSimulationCellData &i_f_slope_t0,
			CEulerSimulationCellData &i_f_slope_t1,
			CEulerSimulationCellData *io_f_t0_t1,
			T i_timestep_size
	)
	{
		assert(false);
#if 0
		io_f_t0_t1->dofs[0].h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[0].r + i_f_slope_t1.dofs[0].h);
		io_f_t0_t1->dofs[0].ru	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[0].ru + i_f_slope_t1.dofs[0].ru);
		io_f_t0_t1->dofs[0].rv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs[0].rv + i_f_slope_t1.dofs[0].rv);

		// TODO: which CFL to use?
//		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;

		if (io_f_t0_t1->dofs[0].r <= SIMULATION_EULER_DRY_THRESHOLD)
		{
			io_f_t0_t1->dofs[0].r = 0;
			io_f_t0_t1->dofs[0].ru = 0;
			io_f_t0_t1->dofs[0].rv = 0;
		}


#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
		if (	std::abs(io_f_t0_t1->dofs[0].ru) > io_f_t0_t1->dofs[0].h*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(io_f_t0_t1->dofs[0].rv) > io_f_t0_t1->dofs[0].h*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY
		)
		{
			std::cout << "op_cell: Velocity exceeded threshold value " << CONFIG_EXIT_THRESHOLD_MAX_VELOCITY << std::endl;

			std::cout << "cell data: " << std::endl;
			std::cout << *io_f_t0_t1 << std::endl;

			throw(std::runtime_error("op_cell: Velocity exceeded threshold value"));
		}
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
#endif
	}



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_0thOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_Parameters&)(*this) = (CEdgeComm_Parameters&)(i_kernel);
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CTriangle_VectorProjections::toHypEdgeSpace(&o_edge_comm_data->dofs[0].ru, &o_edge_comm_data->dofs[0].rv);


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cellData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,
			int i_depth,
			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CTriangle_VectorProjections::toRightEdgeSpace(&o_edge_comm_data->dofs[0].ru, &o_edge_comm_data->dofs[0].rv);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cellData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CTriangle_VectorProjections::toLeftEdgeSpace(&o_edge_comm_data->dofs[0].ru, &o_edge_comm_data->dofs[0].rv);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION

		i_cellData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = 0;
			o_edge_comm_data->dofs[0].rv = 0;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = -o_edge_comm_data->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = -o_edge_comm_data->dofs[0].rv;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toHypEdgeSpace(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs[0].r = i_cellData->dofs[0].r;
			o_edge_comm_data->dofs[0].ru = i_cellData->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = i_cellData->dofs[0].rv;
			o_edge_comm_data->dofs[0].e = i_cellData->dofs[0].e;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_DATASET:
			cDatasets->getBenchmarkNodalData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &(o_edge_comm_data->dofs[0]));

			CTriangle_VectorProjections::worldToReference(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv,
					-i_right_normal_x,
					-i_right_normal_y
				);
			break;
		}

		/**
		 * boundary flux computations are also stored on the buffer to allow SIMD computations.
		 *
		 * since this SIMD computations are not aware of whether a flux is part
		 * of the cluster or out of the domain, a CFL scalar value of infinity is stored here.
		 */
		o_edge_comm_data->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cellData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = 0;
			o_edge_comm_data->dofs[0].rv = 0;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = -o_edge_comm_data->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = -o_edge_comm_data->dofs[0].rv;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toRightEdgeSpace(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs[0].r = i_cellData->dofs[0].r;
			o_edge_comm_data->dofs[0].ru = i_cellData->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = i_cellData->dofs[0].rv;
			o_edge_comm_data->dofs[0].e = i_cellData->dofs[0].e;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_DATASET:
			cDatasets->getBenchmarkNodalData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &(o_edge_comm_data->dofs[0]));

			CTriangle_VectorProjections::worldToReference(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv,
					-i_right_normal_x,
					-i_right_normal_y
				);
			break;
		}

		o_edge_comm_data->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cellData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CEulerSimulationCellData *i_cellData,

			CEulerSimulationEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = 0;
			o_edge_comm_data->dofs[0].rv = 0;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs[0] = i_cellData->dofs[0];
			o_edge_comm_data->dofs[0].ru = -o_edge_comm_data->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = -o_edge_comm_data->dofs[0].rv;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toLeftEdgeSpace(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs[0].r = i_cellData->dofs[0].r;
			o_edge_comm_data->dofs[0].ru = i_cellData->dofs[0].ru;
			o_edge_comm_data->dofs[0].rv = i_cellData->dofs[0].rv;
			o_edge_comm_data->dofs[0].e = i_cellData->dofs[0].e;

			// rotate edge comm data to normal space
			CTriangle_VectorProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv
				);
			break;

		case BOUNDARY_CONDITION_DATASET:
			cDatasets->getBenchmarkNodalData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &(o_edge_comm_data->dofs[0]));

			CTriangle_VectorProjections::worldToReference(
					&o_edge_comm_data->dofs[0].ru,
					&o_edge_comm_data->dofs[0].rv,
					-i_right_normal_x,
					-i_right_normal_y
				);
			break;
		}

		o_edge_comm_data->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cellData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}


	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the cellData within one timestep.
	 */
	inline void op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CEulerSimulationCellData *io_cCellData,	///< element data

			CEulerSimulationEdgeData *i_hyp_edge_net_update,	///< incoming net update from hypotenuse
			CEulerSimulationEdgeData *i_right_edge_net_update,	///< incoming net update from right edge
			CEulerSimulationEdgeData *i_left_edge_net_update	///< incoming net update from left edge
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cCellData->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
			);
#endif

		CTriangle_VectorProjections::fromHypEdgeSpace(
				&i_hyp_edge_net_update->dofs[0].ru,
				&i_hyp_edge_net_update->dofs[0].rv
			);

		CTriangle_VectorProjections::fromRightEdgeSpace(
				&i_right_edge_net_update->dofs[0].ru,
				&i_right_edge_net_update->dofs[0].rv
			);

		CTriangle_VectorProjections::fromLeftEdgeSpace(
				&i_left_edge_net_update->dofs[0].ru,
				&i_left_edge_net_update->dofs[0].rv
			);

		/**
		 * store CFL to cell data
		 */
		io_cCellData->cfl_domain_size_div_max_wave_speed = std::min(i_hyp_edge_net_update->CFL1_scalar, std::min(i_right_edge_net_update->CFL1_scalar, i_left_edge_net_update->CFL1_scalar));

#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
		if (io_cCellData->cfl_domain_size_div_max_wave_speed < CONFIG_EXIT_THRESHOLD_CFL_SIZE)
		{
			std::cout << "Cell Data:" << std::endl;
			std::cout << *io_cCellData << std::endl;

			std::cout << "Hyp net update:" << std::endl;
			std::cout << *i_hyp_edge_net_update << std::endl;

			std::cout << "Right net update:" << std::endl;
			std::cout << *i_right_edge_net_update << std::endl;

			std::cout << "Left net update:" << std::endl;
			std::cout << *i_left_edge_net_update << std::endl;

			std::ostringstream ss;
			ss << "max wave speed undershot timestep size threshold: " << io_cCellData->cfl_domain_size_div_max_wave_speed << " < " << CONFIG_EXIT_THRESHOLD_CFL_SIZE;
			throw(std::runtime_error(ss.str()));
		}
#endif

		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);

		if (std::isnan(io_cCellData->dofs[0].r))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INVALID WATER HEIGHT DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
		T hyp_length = sierpi::CTriangle_SideLengths::getUnitHypotenuseLengthForDepth(i_depth)*cathetus_real_length;

		if (t_storeElementUpdatesOnly)
		{
			// store updates only - therefore we don't consider the timestep size
			T tmp = -(T)1.0/((T)0.5*cat_length*cat_length);
			io_cCellData->dofs[0].r =  tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].r + cat_length*(i_right_edge_net_update->dofs[0].r + i_left_edge_net_update->dofs[0].r));
			io_cCellData->dofs[0].ru = tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].ru + cat_length*(i_right_edge_net_update->dofs[0].ru + i_left_edge_net_update->dofs[0].ru));
			io_cCellData->dofs[0].rv = tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].rv + cat_length*(i_right_edge_net_update->dofs[0].rv + i_left_edge_net_update->dofs[0].rv));
			io_cCellData->dofs[0].e = tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].e + cat_length*(i_right_edge_net_update->dofs[0].e + i_left_edge_net_update->dofs[0].e));
		}
		else
		{
			/**
			 * maybe there's some instability in it?
			 */
			// apply updates
			T tmp = -(T)timestep_size/((T)0.5*cat_length*cat_length);
			io_cCellData->dofs[0].r +=  tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].r + cat_length*(i_right_edge_net_update->dofs[0].r + i_left_edge_net_update->dofs[0].r));
			io_cCellData->dofs[0].ru += tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].ru + cat_length*(i_right_edge_net_update->dofs[0].ru + i_left_edge_net_update->dofs[0].ru));
			io_cCellData->dofs[0].rv += tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].rv + cat_length*(i_right_edge_net_update->dofs[0].rv + i_left_edge_net_update->dofs[0].rv));
			io_cCellData->dofs[0].e += tmp * (hyp_length*i_hyp_edge_net_update->dofs[0].e + cat_length*(i_right_edge_net_update->dofs[0].e + i_left_edge_net_update->dofs[0].e));


#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS && 0
		if (	std::abs(io_cCellData->dofs[0].ru) > io_cCellData->dofs[0].h*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(io_cCellData->dofs[0].rv) > io_cCellData->dofs[0].h*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY
		)
		{
			std::cout << "op_cell: Velocity exceeded threshold value " << CONFIG_EXIT_THRESHOLD_MAX_VELOCITY << std::endl;

			std::cout << "cell data: " << std::endl;
			std::cout << *io_cCellData << std::endl;

			throw(std::runtime_error("op_cell: Velocity exceeded threshold value"));
		}
#endif
		}


#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

		// don't scale with timestep size to have normalized mass over one second
		T m =	std::max(
					std::abs(i_hyp_edge_net_update->dofs[0].h*hyp_length),
					std::abs(i_right_edge_net_update->dofs[0].h*cat_length),
					std::abs(i_left_edge_net_update->dofs[0].h*cat_length)
				);

		T inv_h;
		if (io_cCellData->dofs[0].r > SIMULATION_EULER_DRY_THRESHOLD)
			inv_h = (T)1.0/io_cCellData->dofs[0].r;
		else
			inv_h = SIMULATION_EULER_DRY_THRESHOLD;

		// rescale with height to get update for unit area over unit timestep
		io_cCellData->refine = (m*inv_h > refine_threshold);
		io_cCellData->coarsen = (m*inv_h < coarsen_threshold);

#endif


#if 0
		if (io_cCellData->dofs[0].e < 0)
		{
			if (io_cCellData->dofs[0].r + io_cCellData->dofs.e > 1.0)
			{
				std::cout << "timestep_size: " << timestep_size << std::endl;
				std::cout << "cat_length: " << cat_length << std::endl;

				std::cout << "net update h: " << i_hyp_edge_net_update->h << std::endl;
				std::cout << "net update hu: " << i_hyp_edge_net_update->hu << std::endl;
				std::cout << "net update hv: " << i_hyp_edge_net_update->hv << std::endl;
				std::cout << "net update b: " << i_hyp_edge_net_update->b << std::endl;

				std::cout << "cellData h: " << io_cCellData->dofs.r << std::endl;
				std::cout << "cellData hu: " << io_cCellData->dofs.ru << std::endl;
				std::cout << "cellData hv: " << io_cCellData->dofs.rv << std::endl;
				std::cout << "cellData b: " << io_cCellData->dofs[0].e << std::endl;

				std::cout << "tmp: " << -(T)timestep_size/((T)0.5*cat_length*cat_length) << std::endl;
				std::cout << std::endl;
			}
		}
#endif
	}





	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			const CEulerSimulationEdgeData &i_edgeFlux_left,	///< edge data on left edge
			const CEulerSimulationEdgeData &i_edgeFlux_right,	///< edge data on right edge
			CEulerSimulationEdgeData &o_edgeNetUpdates_left,		///< output for left flux
			CEulerSimulationEdgeData &o_edgeNetUpdates_right		///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		CEulerSimulationEdgeData io_edgeData_right = i_edgeFlux_right;

		io_edgeData_right.dofs[0].ru = -i_edgeFlux_right.dofs[0].ru;
		io_edgeData_right.dofs[0].rv = -i_edgeFlux_right.dofs[0].rv;
		io_edgeData_right.dofs[0].r = i_edgeFlux_right.dofs[0].r;
		io_edgeData_right.dofs[0].e = i_edgeFlux_right.dofs[0].e;

		T o_max_wave_speed_left;
		T o_max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeFlux_left.dofs[0],
				io_edgeData_right.dofs[0],
				&o_edgeNetUpdates_left.dofs[0],
				&o_edgeNetUpdates_right.dofs[0],
				&o_max_wave_speed_left,
				&o_max_wave_speed_right,
				gravitational_constant
			);

		o_edgeNetUpdates_right.dofs[0].ru = -o_edgeNetUpdates_right.dofs[0].ru;
		o_edgeNetUpdates_right.dofs[0].rv = -o_edgeNetUpdates_right.dofs[0].rv;

		o_edgeNetUpdates_left.CFL1_scalar = i_edgeFlux_left.CFL1_scalar / o_max_wave_speed_left;
		o_edgeNetUpdates_right.CFL1_scalar = i_edgeFlux_right.CFL1_scalar / o_max_wave_speed_right;


#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
		if (
			o_edgeNetUpdates_left.CFL1_scalar < CONFIG_EXIT_THRESHOLD_CFL_SIZE	||
			o_edgeNetUpdates_right.CFL1_scalar < CONFIG_EXIT_THRESHOLD_CFL_SIZE
		)
		{
			std::cout << "left edge flux:" << std::endl;
			std::cout << i_edgeFlux_left << std::endl;

			std::cout << "right edge flux:" << std::endl;
			std::cout << i_edgeFlux_right << std::endl;

			std::cout << "left net updates:" << std::endl;
			std::cout << o_edgeNetUpdates_left << std::endl;

			std::cout << "right net updates:" << std::endl;
			std::cout << o_edgeNetUpdates_right << std::endl;

			std::cout << " > left max wavespeed: " << o_max_wave_speed_left << std::endl;
			std::cout << " > right max wavespeed: " << o_max_wave_speed_right << std::endl;

			std::ostringstream ss;
			ss << "CFL1_scalar undershot" << std::endl;
			throw(std::runtime_error(ss.str()));
		}
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edgeNetUpdates_left.validation = i_edgeFlux_left.validation;
		o_edgeNetUpdates_right.validation = i_edgeFlux_right.validation;
#endif
	}




	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CEulerSimulationEdgeData> *io_edge_comm_buffer
	)
	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_array(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_array(
		CEulerSimulationEdgeData *io_edge_data_array,
		size_t i_edge_comm_elements
	)
	{
		for (size_t i = 0; i < i_edge_comm_elements; i+=2)
		{
			CEulerSimulationEdgeData ed0 = io_edge_data_array[i];
			CEulerSimulationEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}


	static void setupMatrices(int i_verbosity_level)
	{
	}
};

}
}



#endif
