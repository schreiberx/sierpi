/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CKERNEL_SETUP_ELEMENT_DATA_HPP_
#define CKERNEL_SETUP_ELEMENT_DATA_HPP_


#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==0
#	include "CSetup_CellData_0thOrder.hpp"

	namespace sierpi
	{
		namespace kernels
		{
			typedef CSetup_CellData_0thOrder CSetup_CellData;
		}
	}

#else
	# error "1st order euler not implemented"
#endif

#endif
