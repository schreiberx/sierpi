/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelVertexDataCommTraversals.hpp
 *
 *  Created on: 02 March, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_

#include "config.h"
#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CCluster_ExchangeVertexDataCommData.hpp"
#include "libsierpi/parallelization/CStackAccessorMethods.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * VERTEX COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) vertex communication
 *  c) last traversal
 */
class CHelper_GenericParallelVertexDataCommTraversals
{
	/**
	 * run the edge comm traversals
	 *
	 * return number of vertices to be rendered
	 */
public:
	template<
		typename CNodeData,
		typename TVertexDataCommTraversator,	/// Traversator including kernel
		typename CSimulation_Cluster,			/// simulation cluster
		typename CStackAccessors,				/// accessors to adjacent stacks
		typename CKernelClass,
		typename TAutoLambda						/// lambda function executed at first in firstPass with node as parameter
	>
	static void action(
			sierpi::CCluster_ExchangeVertexDataCommData<
				sierpi::CCluster_TreeNode<CSimulation_Cluster>,
				CNodeData,
				sierpi::CStackAccessorMethodsVisualizationNodeData<sierpi::CCluster_TreeNode<CSimulation_Cluster>, CNodeData>,
				CKernelClass
			>
			CSimulation_Cluster::*i_cCluster_ExchangeVertexDataCommData,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,
			TAutoLambda i_node_traversator_kernel_setup_func
	)
	{
		typedef sierpi::CCluster_TreeNode<CSimulation_Cluster> CCluster_TreeNode_;
		typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;
#if 0

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				/*
				 * setup traversator
				 */
				TVertexDataCommTraversator cOpenGL_VertexCommTraversator;
				cOpenGL_VertexCommTraversator.setup_sfcMethods(node->cTriangleFactory);
				i_node_traversator_kernel_setup_func(&cOpenGL_VertexCommTraversator);

				// setup stacks
				node->cStacks->vertex_data_comm_left_edge_stack.clear();
				node->cStacks->vertex_data_comm_right_edge_stack.clear();

				// run traversal
				cOpenGL_VertexCommTraversator.action_FirstPass(node->cStacks);
			}
		);


		/*
		 * Compute fluxes using uniqueIDs to avoid double flux evaluation:
		 *
		 * The sub-cluster with the relation 'uniqueID < adjacentUniqueID' is responsible to compute the fluxes
		 * also in a writing manner for the adjacent one.
		 *
		 * in FLUX COMM PASS:
		 * 1) The responsible sub-cluster first fetches the data from the adjacent cluster
		 *    to the exchange edge comm data stacks.
		 *
		 * 2) The fluxes are computed for all fetched edge communication data by the responsible sub-cluster.
		 *
		 * in SECOND PASS:
		 * 3) Storing the fluxes to the local_edge_comm_data_stack and exchange_edge_comm_data_stack, pulling
		 *    the edge communication data from the sub-clusters with 'uniqueID > adjacentUniqueID' fetches the
		 *    already computed fluxes.
		 */

		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		/*
		 * WARNING: this is a serial call since the visualization has to be driven on the 1st thread only!
		 */


		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					/**
					 * INTER-PARTITION-VERTEX DATA COMMUNICATION
					 */
					/// handler for edge communication with adjacent clusters
#if 1

#if 0
					std::cout << "LOCAL RIGHT STACK DATA: " << &(node->cStacks->vertex_data_comm_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT STACK DATA: " << &(node->cStacks->vertex_data_comm_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

#endif
					TVertexDataCommTraversator cOpenGL_VertexCommTraversator;

					cOpenGL_VertexCommTraversator.setup_sfcMethods(node->cTriangleFactory);
					i_node_traversator_kernel_setup_func(&cOpenGL_VertexCommTraversator);

					sierpi::CCluster_ExchangeVertexDataCommData<
							CCluster_TreeNode_,
							CNodeData,
							sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CNodeData>,
							TVertexDataCommTraversator
						> i_simulationVertexCommSubClass(
								node,
								&cOpenGL_VertexCommTraversator
							);

					i_simulationVertexCommSubClass.pullVertexCommData();

#if 0
					std::cout << "LOCAL RIGHT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;
#endif

#else

					std::cout << "LOCAL RIGHT STACK DATA: " << &(node->cStacks->vertex_data_comm_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT STACK DATA: " << &(node->cStacks->vertex_data_comm_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					// LEFT STACK
					node->cStacks->vertex_data_comm_exchange_left_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack());

					for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
					{
						TTsunamiVisualizationVertexData &v = node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i);

						TTsunamiVertexScalar inv = 1.0/v.normalization_factor;
						v.normal[0] *= inv;
						v.normal[1] *= inv;
						v.normal[2] *= inv;
						v.height *= inv;
						v.normalization_factor = 1.0;

						node->cStacks->vertex_data_comm_exchange_left_edge_stack.setElementAtIndex(i, v);
					}
					node->cStacks->vertex_data_comm_left_edge_stack.setStackElementCounter(0);

					// RIGHT STACK
					node->cStacks->vertex_data_comm_exchange_right_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack());

					for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
					{
						TTsunamiVisualizationVertexData &v = node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i);

						TTsunamiVertexScalar inv = 1.0/v.normalization_factor;
						v.normal[0] *= inv;
						v.normal[1] *= inv;
						v.normal[2] *= inv;
						v.height *= inv;
						v.normalization_factor = 1.0;

						node->cStacks->vertex_data_comm_exchange_right_edge_stack.setElementAtIndex(i, v);
					}
					node->cStacks->vertex_data_comm_right_edge_stack.setStackElementCounter(0);


					std::cout << "LOCAL RIGHT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

#endif


					// run computation based on newly set-up stacks (run parallel version)
					cOpenGL_VertexCommTraversator.action_SecondPass_Parallel(node->cStacks);

				}

		);
#endif

#endif
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
