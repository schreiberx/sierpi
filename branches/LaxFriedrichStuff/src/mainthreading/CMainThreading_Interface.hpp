/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreading_Interface.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_INTERFACE_HPP_
#define CMAINTHREADING_INTERFACE_HPP_


/**
 * Interfaces which have to be implemented by threading classes
 */
class CMainThreading_Interface
{
public:
	/*
	 * interfaces which has to be provided by the threading subclass
	 */

	/**
	 * return the affinity mode (distance between enumerated cores for sequential pinning)
	 */
	virtual int getThreadAffinityPadding() = 0;

	/**
	 * return the affinity start id
	 */
	virtual int getThreadAffinityStartId() = 0;

	/**
	 * return the scalability graph id to use for optimizations
	 */
	virtual int getThreadIPMOScalabilityGraphId() = 0;

	/**
	 * get maximum number of allowed threads (usually the number of cores on all cpus)
	 */
	virtual int getMaxNumberOfThreads() = 0;

	/**
	 * set maximum number of allowed threads (usually the number of cores on all cpus)
	 */
	virtual void setMaxNumberOfThreads(int i_max_number_of_threads) = 0;

	/**
	 * get number of threads which should be used by the simulation
	 */
	virtual int getNumberOfThreadsToUse() = 0;

	/**
	 * set number of threads which should be used by the simulation
	 */
	virtual void setValueNumberOfThreadsToUse(int i_number_of_threads_to_use) = 0;

	/**
	 * get verbosity level
	 */
	virtual int getVerboseLevel() = 0;

	/**
	 * return workload of simulation.
	 * this is important to provide an appropriate scalability graph
	 */
	virtual long long getSimulationWorkload() = 0;

	/**
	 * return the overall workload processed during simulation to compute efficiency
	 */
	virtual long long getSimulationSumWorkload() = 0;

	/// this handler actually executed the simulation loop
	virtual bool simulation_loopIteration() = 0;

	/**
	 * setup simulation
	 */
	virtual bool simulation_setup() = 0;



	/**
	 * interfaces which have to be provided by the CMainThreading* class
	 */

	/**
	 * setup the threading support (e. g. allocate computing resources)
	 */
	virtual void threading_setup() = 0;

	/**
	 * full simulation loop for benchmarks
	 */
	virtual void threading_simulationLoop() = 0;

	/**
	 * single iteration executed solely by GUI
	 */
	virtual bool threading_simulationLoopIteration() = 0;

	/**
	 * shutdown the threading support
	 */
	virtual void threading_shutdown() = 0;

	/**
	 * set number of threads
	 */
	virtual void threading_setNumThreads(int i) = 0;


	virtual ~CMainThreading_Interface()
	{
	}
};



#endif /* CMAINTHREADING_INTERFACE_HPP_ */
