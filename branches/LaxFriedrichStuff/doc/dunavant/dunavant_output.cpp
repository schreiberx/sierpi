#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstring>
#include "dunavant.hpp"


int main()
{
	int order;
	int order_num;
	int rule;
	int rule_num;
	double *wtab;
	double *xytab;

	rule_num = dunavant_rule_num ( );

	std::cout << std::setprecision(16);

	for (rule = 1; rule <= rule_num; rule++)
	{
		order_num = dunavant_order_num ( rule );

		xytab = new double[2*order_num];
		wtab = new double[order_num];

		dunavant_rule ( rule, order_num, xytab, wtab );

		std::cout << "/**" << std::endl;
		std::cout << " * Degree " << rule << std::endl;
		std::cout << " */" << std::endl;
		std::cout << "template <typename T>" << std::endl;
		std::cout << "class CGaussQuadrature2D_TriangleArea<T, " << rule << ">" << std::endl;
		std::cout << "{" << std::endl;

		/*
		 * COORDS
		 */
		std::cout << "public:" << std::endl;
		std::cout << "	static T* getCoords()" << std::endl;
		std::cout << "	{" << std::endl;
		std::cout << "		static T values[] = {" << std::endl;
		for (order = 0; order < order_num; order++)
		{
			std::cout << "\t\t\t" << xytab[order*2+0] << ",\t" << xytab[order*2+1];
			if (order != order_num-1)
				std::cout << ",";
			std::cout << std::endl;
		}
		std::cout << "		};" << std::endl;
		std::cout << "		return values;" << std::endl;
		std::cout << "	}" << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;

		/*
		 * WEIGHTS
		 */
		std::cout << "public:" << std::endl;
		std::cout << "	static T* getWeights()" << std::endl;
		std::cout << "	{" << std::endl;
		std::cout << "		static T values[] = {" << std::endl;
		for (order = 0; order < order_num; order++)
		{
			std::cout << "\t\t\t" << wtab[order];
			if (order != order_num-1)
				std::cout << ",";
			std::cout << std::endl;
		}
		std::cout << "		};" << std::endl;
		std::cout << "		return values;" << std::endl;
		std::cout << "	}" << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;

		/*
		 * num points
		 */
		std::cout << "	static int getNumCoords()" << std::endl;
		std::cout << "	{" << std::endl;
		std::cout << "		return " << order_num << ";" << std::endl;
		std::cout << "	}" << std::endl;
		std::cout << "};" << std::endl;
		std::cout << std::endl;

		delete [] wtab;
		delete [] xytab;
	}
	return 1;
}
