/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CSplitJoinTuning.hpp
 *
 *  Created on: Jan 01, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSPLITJOIN_TUNING_HPP_
#define CSPLITJOIN_TUNING_HPP_

#include <cmath>

template <typename CTuningTable>
class CSplitJoinTuning	: public CTuningTable
{
public:
	/**
	 * elements stored in tuning table
	 */


	/**
	 * tuning table.
	 *
	 * 1st element: number of triangles (in ascending order)
	 * 2nd element: recommended split size
	 */


	void updateSplitJoin(
		unsigned int &o_partition_split_workload_size,
		unsigned int &o_partition_join_workload_size,
		unsigned long long i_triangles_in_simulation_domain
	)
	{
		if (this->tuning_table[0][0] > i_triangles_in_simulation_domain)
		{
			o_partition_split_workload_size = this->tuning_table[0][1];
			o_partition_join_workload_size = o_partition_split_workload_size/2;
//			std::cout << "min - split: " << o_partition_split_workload_size << std::endl;
			return;
		}

		/*
		 * we simply assume that the tuning table is really small.
		 *
		 * it it's larger (>100 elements), a binary search would be better.
		 */

		int i;
		for (i = 1; i < this->tuning_table_size; i++)
		{
			if (this->tuning_table[i][0] > i_triangles_in_simulation_domain)
				break;
		}


		/*
		 * maximum reached
		 */
		if (i == this->tuning_table_size)
		{
			o_partition_split_workload_size = this->tuning_table[this->tuning_table_size-1][1];
			o_partition_join_workload_size = o_partition_split_workload_size/2;
//			std::cout << "max - split: " << o_partition_split_workload_size << std::endl;
			return;
		}

		double area = this->tuning_table[i][0] - this->tuning_table[i-1][0];
		double dist = i_triangles_in_simulation_domain - this->tuning_table[i-1][0];
		double scale = dist/area;

		double split_size =
				(double)this->tuning_table[i-1][1] +
				scale*(double)(this->tuning_table[i][1] - this->tuning_table[i-1][1]);

		o_partition_split_workload_size = std::floor(split_size+0.5);
		o_partition_join_workload_size = o_partition_split_workload_size/2;

#if 0
		std::cout << i_triangles_in_simulation_domain << std::endl;
		std::cout << i << std::endl;
		std::cout << this->tuning_table[i-1][0] << " " << this->tuning_table[i-1][1] << std::endl;
		std::cout << this->tuning_table[i][0] << " " << this->tuning_table[i][1] << std::endl;
		std::cout << std::endl;
		std::cout << "scale: " << scale << std::endl;
		std::cout << split_size << std::endl;
#endif
	}
};

#endif
