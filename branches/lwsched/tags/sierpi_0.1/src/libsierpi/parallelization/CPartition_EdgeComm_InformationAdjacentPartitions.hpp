/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_EdgeComm_InformationAdjacentPartitions.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_EDGECOMM_INFORMATIONADJACENTPARTITIONS_HPP_
#define CPARTITION_EDGECOMM_INFORMATIONADJACENTPARTITIONS_HPP_

#include <vector>
#include "CPartition_EdgeComm_InformationAdjacentPartition.hpp"
#include "CPartition_TreeNode.hpp"
#include "CPartition_UniqueId.hpp"


/**
 * informations about adjacent partitions encoded in a RLE format
 */
template <typename CPartition>
class CPartition_EdgeComm_InformationAdjacentPartitions
{
	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition> CEdgeComm_InformationAdjacentPartition_;

public:
	/***********************************************************************
	 * HYPOTENUSE
	 ***********************************************************************/

	/**
	 * current edge comm information of partitions aligned at the hypotenuse.
	 */
	std::vector<CEdgeComm_InformationAdjacentPartition_> hyp_adjacent_partitions;

	/**
	 * additional storage for edge comm information for partitions adjacent to the
	 * hypotenuse.
	 *
	 * this buffer is used to avoid race conditions when new items are inserted.
	 */
	std::vector<CEdgeComm_InformationAdjacentPartition_> new_hyp_adjacent_partitions;

	/**
	 * this variable is set to true whenever the information about the adjacent partitions
	 * was modified and thus new_hyp_adjacent_partitions is the storage of the new informations.
	 */
	bool hyp_swapOrNotToBeSwapped;

	/***********************************************************************
	 * CATHETI
	 ***********************************************************************/

	/**
	 * edge comm information of both catheti
	 */
	std::vector<CEdgeComm_InformationAdjacentPartition_> cat_adjacent_partitions;

	/**
	 * updated edge of catheti to avoid race conditions when new items are inserted
	 */
	std::vector<CEdgeComm_InformationAdjacentPartition_> new_cat_adjacent_partitions;

	/**
	 * swap catheti edge comm information
	 */
	bool cat_swapOrNotToBeSwapped;



	/**
	 * constructor
	 */
	CPartition_EdgeComm_InformationAdjacentPartitions()	:
		hyp_swapOrNotToBeSwapped(false),
		cat_swapOrNotToBeSwapped(false)
	{
		// preallocate larger vector to avoid resizing
		static const int CEdgeComm_InformationAdjacentPartitions_INITIAL_VECTOR_SIZE = 32;

		hyp_adjacent_partitions.reserve(CEdgeComm_InformationAdjacentPartitions_INITIAL_VECTOR_SIZE);
		cat_adjacent_partitions.reserve(CEdgeComm_InformationAdjacentPartitions_INITIAL_VECTOR_SIZE);

		new_hyp_adjacent_partitions.reserve(CEdgeComm_InformationAdjacentPartitions_INITIAL_VECTOR_SIZE);
		new_cat_adjacent_partitions.reserve(CEdgeComm_InformationAdjacentPartitions_INITIAL_VECTOR_SIZE);
	}



	/**
	 * After the edge communication was updated, maybe the edge information stacks have to be swapped.
	 *
	 * !!! This method has to be called after _all_ edge comms are finished with their updates !!!
	 */
	void swapAndCleanAfterUpdatingEdgeComm()
	{
		if (hyp_swapOrNotToBeSwapped)
		{
			// TODO: this swap operation is done on the std::vector instead of it's pointer

			hyp_adjacent_partitions.swap(new_hyp_adjacent_partitions);
			// clear new stacks to prepare them for further update edge communication calls
			new_hyp_adjacent_partitions.clear();
			hyp_swapOrNotToBeSwapped = false;
		}

		if (cat_swapOrNotToBeSwapped)
		{
			cat_adjacent_partitions.swap(new_cat_adjacent_partitions);
			// clear new stacks to prepare them for further update edge communication calls
			new_cat_adjacent_partitions.clear();
			cat_swapOrNotToBeSwapped = false;
		}
	}


	/**
	 * return the number of edge communicatino elements for the catheti
	 */
	unsigned int getSumOfEdgeCommElementsOnCatheti()
	{
		unsigned int sum = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cat_adjacent_partitions.begin();
				iter != cat_adjacent_partitions.end();
				iter++
		)
		{
			sum += (*iter).commElements;
		}

		return sum;
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, CPartition_EdgeComm_InformationAdjacentPartitions &m)
	{
		co << " Adjacent information: " << std::endl;

		co << " + adj information hyp:" << std::endl;

		if (m.hyp_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = m.new_hyp_adjacent_partitions.begin();
					iter != m.new_hyp_adjacent_partitions.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = m.hyp_adjacent_partitions.begin();
					iter != m.hyp_adjacent_partitions.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}

		}
		co << std::endl;


		co << " + adj information cat:" << std::endl;

		if (m.cat_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = m.new_cat_adjacent_partitions.begin();
					iter != m.new_cat_adjacent_partitions.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = m.cat_adjacent_partitions.begin();
					iter != m.cat_adjacent_partitions.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		co << std::endl;

		return co;
	}
};



#endif /* CADJACENTPARTITIONS_H_ */
