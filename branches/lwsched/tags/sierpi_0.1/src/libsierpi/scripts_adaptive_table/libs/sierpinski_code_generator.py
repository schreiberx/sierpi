#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

import sys
import math
import copy
import itertools
import normals
import std_code_blocks

from xor import xor



######################################################################
# default config values
#
# not all combinations are allowed so far
######################################################################
class CConfig:
	##############################
	######### PARAMETERS #########
	##############################
	
	# create normal information (normal on hypotenuse pointing inward)
	kernelParams_normals = False
	
	# create vertex informations
	kernelParams_vertexCoords = False
	
	# depth as parameter for the kernel callback functions
	kernelParams_depth = False
	
	# element data as parameter for kernel calls
	kernelParams_elementData = False
	
	# call kernel hooks for elemental data
	kernelElementActionCall = False


	#################################
	######### COMMUNICATION #########
	#################################

	# no edge communication? => traversals can be simplified
	edgeComm = False
	
	# fluxComm: special edge communication evaluating fluxes only once
	fluxComm = False
	
	# communication via vertex data (firstTouch, updateTouch, middleTouch) mainly visualization
	vertexDataComm = False


	####################################
	######### FORWARD/BACKWARD #########
	####################################
	
	# create forward code
	createForwardCode = False
	
	# create backward code
	createBackwardCode = False


	######################################
	######### STREAM/STACK NAMES #########
	######################################

	# structure input stack: the input stack can be modified if necessary (e. g. for edge communication)
	varname_structure_input_stream = 'structure_lifo_in'
	varname_structure_output_stream = 'structure_stack_out'

	# element data input stack: the element data input stack can be modified if necessary (e. g. for edge communication)
	varname_element_data_input_stream = 'element_data_lifo'

	# edge comm communication stacks
	varname_edge_data_comm_stacks = 'edge_data_comm_%s_edge_stack'

	# edge comm buffer stack
	varname_edge_data_comm_buffer_stack = 'edge_comm_buffer'


	########################
	######### MISC #########
	########################
	
	# create reversed structure stack - e. g. the backward stack during traversal of the forward stack
	createReversedStructureStack = False
	
	# assume that K, V and H types are equal and can be replaced by 'K'
	assumeKVHToBeEqual = False
	
	# even run the should_refine functions also when no refine operation is allowed due to depth restrictions
	adaptivity_callShouldRefineForAllLeaves = False


	##################################################
	######### RUNTIME CONF for code creation #########
	##################################################

	# create depth value for recursion table
	internal_traversatorMethodDepth = False
	
	# create edge types (edgeComm, fluxComm, vertexDataComm, adaptivity)
	internal_edgeTypes = False

	# create depth data for recursive calls depending on depthLimiters of if the kernel desires the depth as a parameter
	depth = False
	
	# common refine/coarsen method
	adaptivity_create_last_traversal_common_method_created = False
	
	# create normals by forwarding normal information
	internal_traversator_normal_as_int_parameter = True


	##########################################
	# ADAPTIIVE: read adaptive_info.txt for further information
	# adaptivity handling is not possible in combination with kernel computation calls
	##########################################
	
	# if 'adaptivity' is set to true, all other config variables from above are ignored so far!
	adaptivity = False

	# activate debug output to print data on stack, recursive calls and further information during traversal
	adaptivity_code_debug_output = False

	# depth limiters: avoid coarsening/refinement if the depth gets out of a specific range
	adaptivity_depthLimiterMax = False
	adaptivity_depthLimiterMin = False

	# update maxLeafTraversalDepth
	adaptivity_updateMaxDepth = False
	
	# flags to create only specific code pieces (useful for parallelization)
	adaptivity_create_first_traversal = False
	adaptivity_create_middle_traversal = False
	adaptivity_create_last_traversal = False
	
	# return type of kernel function is set to type 'char'
	adaptivity_char_return_variable = False
	
	# only for 'last' traversal: if True, output adaptivity information for edge to stack
	adaptivity_last_traversal_edge_comm_adaptive_information = False
	
	# call kernels elementAction also for unmodified triangles during last traversal  
	adaptiveKernelUnmodifiedElementActionCall = False

	##########################################
	# REGULAR STRUCTURE HANDLING - NOT IMPLEMENTED YET!!!
	#
	# 0: leaf element
	# 1: refine (irregular tree below)
	# -1: regular subtree with depth 1
	# -2: regular subtree with depth 2
	# ...: ...
	##########################################
	regular_structure_stack_handling = False

	
	##########################################
	# METHOD RECURSION TABLE
	##########################################

	# create recursion
	createRecursionTable = True

	# classname + to prefix recursion table variable
	recursionTableClass = 'TThisClass'


###################################
# just a simple handler to store several triangle properties
###################################
class CTriangleProps:
	# type of triangle ('K'/'H'/'V')
	type = None
	
	# hyp, edge_right and edge_left communication type ('old'/'new'/'b')
	hyp = None
	edge_right = None
	edge_left = None
	
	# normal at hypotenuse
	hyp_normal = None

	def setup(self, type, hyp, edge_right, edge_left, hyp_normal, even_odd = 'even'):
		self.type = type
		self.edge_hyp = hyp
		self.edge_right = edge_right
		self.edge_left = edge_left
		self.hyp_normal = hyp_normal
		self.even_odd = even_odd



##################################################
# TRIANGLE PROPERTIES
##################################################
# this class stores _all_ information about the current triangle which can be precomputed.
# also the information about the 2 further refinement is computed here to make it accessible in a convenient way.
##################################################
class CTriangleFactory:
	# member variables
	
	# traversal direction: ('forward'/'backward')
	direction = None
	
	# type of the parent triangle ('K'/'H'/'V')
	type_parent = None
	
	# even or odd traversal ('even'/'odd')
	even_odd = None
	
	# hyp, edge_right and edge_left type ('old'/'new'/'b')
	hyp = None
	edge_right = None
	edge_left = None
	
	# normal at hypotenuse
	hyp_normal = None
	
	# automatically setup variables
	inv_direction = None
	inv_even_odd = None
	x_format = None
	hyp_stack = None
	inv_hyp_stack = None
	edge_stack = None
	
	# childs
	child_first = None
	child_second = None


	##################################################
	# HELPER MEMBERS / VARIABLES
	##################################################
	
	# edge signature helper function to return the longer string 'old'/'new'/'b' from the compressed format 'o'/'n'/'b'
	def e(self, v):
		assert(v != None)
		return ('o' if v == 'old' else ('n' if v == 'new' else 'b'))

	# even_odd signature helper function to return the longer string 'even'/'odd' from the compressed format 'e'/'o'
	def d(self, v):
		assert(v != None)
		return 'e' if v == 'even' else 'o'
	


	##################################################
	# CONSTRUCTOR
	##################################################
	# !!! IMPORTANT !!!
	# compared to other pieces in the code, the first edge is the one closest to the entrance point.
	# the second edge the one closest to the exit point.
	#
	# edges ared named (edge_left and edge_right) by aligning their hypotenuse along the x-axis.
	# then edge_right is to the right hand and edge_left to the left hand.
	#
	# * child_first is the first traversed child
	# * child_second the second traversed child
	##################################################
	def __init__(self,
		direction,
		type_parent, even_odd,			# even or odd
		hyp, edge_right, edge_left,		# edge types (b/new/old) - edges are enumerated in anti-clockwise manner
		hyp_normal,						# normal along hypotenuse
		type_child_first, c1hyp, c1redge, c1ledge,	# first traversed child
		type_child_second, c2hyp, c2redge, c2ledge,	# second traversed child
		config							# configuration
		):

		internal_edgeTypes = config.internal_edgeTypes or config.edgeComm or config.fluxComm or config.vertexDataComm or config.adaptivity
#		sys.stderr.write(str(internal_edgeTypes)+"\n")

		# overwrite any edge related information to create similar signatures
		# if neither an edge communication nor a vertex oriented communication is desired
		if not internal_edgeTypes:
			hyp = 'x'
			edge_right = 'x'
			edge_left = 'x'
			c1hyp = 'x'
			c1redge = 'x'
			c1ledge = 'x'
			c2hyp = 'x'
			c2redge = 'x'
			c2ledge = 'x'

		##########################################
		# PARENT INITIALIZATION
		##########################################
		
		# copy parameters
		self.direction = direction
		self.type_parent = (type_parent if config.assumeKVHToBeEqual == False else 'V')
		self.even_odd = even_odd
		self.edge_hyp = hyp
		self.edge_right = edge_right
		self.edge_left = edge_left
		
		if config.internal_traversator_normal_as_int_parameter:
			self.hyp_normal = "hyp_normal"
		else:
			self.hyp_normal = hyp_normal
		
		# setup other parameters which are frequently used to avoid any recomputation
		
		# inverse direction string
		self.inv_direction = 'backward' if direction == 'forward' else 'backward'
		
		# inversed even/odd
		self.inv_even_odd = 'even' if even_odd == 'odd' else 'odd'
		
		# x_format is used frequently since traversing with forward+even state is similar to traversing backward+odd
		self.x_format = xor(direction == 'forward', even_odd == 'even')
		
		# stack to push/pop data along the hypotenuse
		# usually there's no problem to use one stack (e. g. the left one) always for push/pop operations for the hyp
		# but we'd like to create a better code with enhanced readability
		self.hyp_stack = 'right' if self.x_format else 'left'
		
		# inverse stack of hypotenuse
		self.inv_hyp_stack = 'left' if self.hyp_stack == 'right' else 'right'
		
		# stack for edges to push/pop data
		self.edge_stack = self.inv_hyp_stack

		# setup kernelParams_normals for edge_right and edge_left
		# these edges are enumerated anti-clockwise
		if config.internal_traversator_normal_as_int_parameter:
			self.edge_right_normal = "(hyp_normal+5)&7"
			self.edge_left_normal = "(hyp_normal+3)&7"
		else:
			self.edge_right_normal = (hyp_normal+5)%8
			self.edge_left_normal = (hyp_normal+3)%8

		##########################################
		# CHILD INITIALIZATION
		##########################################

		# first traversed child
		self.child_first = CTriangleProps()
		
		# second traversed child
		self.child_second = CTriangleProps()

		# setup child_first and child_second normal values
		if self.x_format:
			# e. g. for forward and even traversal:
			# the first triangle's hypotenuse is aligned at +3*45 degree from the current hyp
			# the second triangle's hypotenuse is aligned at +3*45+90 degree from the current hyp 

			if config.internal_traversator_normal_as_int_parameter:
				self.child_first.hyp_normal = "(hyp_normal+3)&7"
				self.child_second.hyp_normal = "(hyp_normal+5)&7"
			else:
				self.child_first.hyp_normal = (hyp_normal+3)%8
				self.child_second.hyp_normal = (hyp_normal+5)%8

		else:
			if config.internal_traversator_normal_as_int_parameter:
				self.child_first.hyp_normal = "(hyp_normal+5)&7"
				self.child_second.hyp_normal = "(hyp_normal+3)&7"
			else:
				self.child_first.hyp_normal = (hyp_normal+5)%8
				self.child_second.hyp_normal = (hyp_normal+3)%8

		self.child_first.type = (type_child_first if config.assumeKVHToBeEqual == False else 'V')
		self.child_first.edge_hyp = c1hyp
		self.child_first.edge_right = c1redge
		self.child_first.edge_left = c1ledge
		
		self.child_second.type = (type_child_second if config.assumeKVHToBeEqual == False else 'V')
		self.child_second.edge_hyp = c2hyp
		self.child_second.edge_right = c2redge
		self.child_second.edge_left = c2ledge

		# create signatures for methods to be used for recursive calls
		# parent signature
		self.sig_parent = self.type_parent
		self.sig_parent += '_'+self.d(self.even_odd)
		if internal_edgeTypes:
			self.sig_parent += '_'+self.e(hyp)+self.e(edge_right)+self.e(edge_left)
		self.sig_parent += '_'+direction

		# child_first signature
		self.sig_child_first = self.child_first.type
		self.sig_child_first += '_'+self.d(self.inv_even_odd)
		if internal_edgeTypes:
			self.sig_child_first += '_'+self.e(c1hyp)+self.e(c1redge)+self.e(c1ledge)
		self.sig_child_first += '_'+direction

		# child_second signature
		self.sig_child_second = self.child_second.type
		self.sig_child_second += '_'+self.d(self.inv_even_odd)
		if internal_edgeTypes:
			self.sig_child_second += '_'+self.e(c2hyp)+self.e(c2redge)+self.e(c2ledge)
		self.sig_child_second += '_'+direction

		# extend signatures by kernelParams_normals if requested
		if config.kernelParams_normals and not config.internal_traversator_normal_as_int_parameter:
			self.sig_parent += '_normal_'+normals.getString(self.hyp_normal)
			self.sig_child_first += '_normal_'+normals.getString(self.child_first.hyp_normal)
			self.sig_child_second += '_normal_'+normals.getString(self.child_second.hyp_normal)

		# unique key for function to avoid creating duplicate signatures
		self.key = self.sig_parent+' => ('+self.sig_child_first+', '+self.sig_child_second+')'



###################################
# the main SIERPINSKI CODE GENERATOR class
###################################
class CSierpinskiCodeGenerator:

	#
	# code blocks
	#
	std_code_blocks = std_code_blocks.CStdCodeBlocks()

	#
	# constructor
	#
	def __init__(self):
		# reset code generator
		self.reset()



	###################################
	# main code generator for specific triangle type given by d
	###################################
	def __output_traversator_code(self, triangleFactory):
		
		# override traversator depth computation when appropriate 			
		if self.config.adaptivity_depthLimiterMax or self.config.adaptivity_depthLimiterMin or self.config.adaptivity_updateMaxDepth:
			self.config.internal_traversatorMethodDepth = True


		##################################
		# CREATE RECURSIVE CALLS
		##################################
		if not self.config.adaptivity_create_last_traversal:
			self.std_code_blocks.create_function_header()

		
		##################################
		# LEAF HANDLER
		##################################	

		# override loading of element data for adaptivity
		if not self.config.adaptivity_create_last_traversal:
			self.std_code_blocks.createLoadElementData(self.config.adaptivity_create_first_traversal)	
		
		# setup element/edge method parameters
		kernel_hook_parameters_element_action = []
		self.std_code_blocks.createParametersForActionCalls(kernel_hook_parameters_element_action)
		
		# deepcopy
		kernel_hook_parameters_edge_comm = copy.deepcopy(kernel_hook_parameters_element_action)
		
		# parameters for the kernel element computation hook
		joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
		joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)
		
		# true, if the source for the kernel element action call should be created
		create_kernel_element_action = self.config.kernelParams_elementData
		
		
		#########################################################
		# ADAPTIVE FIRST PASS
		#########################################################
		if self.config.adaptivity_create_first_traversal:
			# only works for forward direction so far (maybe also for backward traversal)
			assert triangleFactory.direction == 'forward'

			self.std_code_blocks.create_adaptivity_incomingEdgeStateTransfer()

			self.std_code_blocks.create_adaptivity_old_state_var()
			
			#
			# REFINEMENT
			#
			# if the depth is larger than the maximum allowed value, don't check for adjacent
			# refinement requests since those cannot exist if every triangle limits the refinement
			# in the same way as this triangle
			#
			self.std_code_blocks.create_adaptivity_checkStateChangeForRefinement(kernel_hook_parameters_element_action)
			
			# COARSENING
			self.std_code_blocks.create_adaptivity_checkStateChangeForCoarsening(kernel_hook_parameters_element_action)
			
			# lookup new state due to state transfer
			self.std_code_blocks.create_adaptivity_lookupNewStateForStateTransfer()
			
			# COARSENING SPECIAL CODE
			self.std_code_blocks.create_adaptivity_coarseningAdjacentAgreement(False)
			
			# CREATE OUTGOING EDGE DATA
			self.std_code_blocks.createOutgoingEdgeStateTransfer()

		
		#########################################################
		# ADAPTIVE MIDDLE PASS
		#########################################################
		if self.config.adaptivity_create_middle_traversal:
			# setup incoming edge states
			self.std_code_blocks.create_adaptivity_incomingEdgeStateTransfer()
	
			# get old state
			self.std_code_blocks.create_adaptivity_loadOldStateAndEdgeTransferState()
	
			# lookup new state due to state transfer
			self.std_code_blocks.create_adaptivity_lookupNewStateForStateTransfer()
	
			# COARSENING SPECIAL CODE
			self.std_code_blocks.create_adaptivity_coarseningAdjacentAgreement(True)
	
			# also spread old refinement information
			self.std_code_blocks.create_adaptivity_updateOutgoingEdgeStateInformation()

			# CREATE OUTGOING EDGE DATA
			self.std_code_blocks.createOutgoingEdgeStateTransfer()



		#########################################################
		# ADAPTIVE LAST PASS
		#########################################################
		if self.config.adaptivity_create_last_traversal:
			assert triangleFactory.direction == 'backward'
			
			ext_joined_kernel_hook_parameters_element_action = joined_kernel_hook_parameters_element_action	
			joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
			
			[joined_traversator_signature, joined_traversator_parameters] = self.std_code_blocks.get_joined_kernel_parameters()

#			if not self.config.adaptivity_create_last_traversal_common_method_created:

#				self.config.adaptivity_create_last_traversal_common_method_created = True
#				self.std_code_blocks.create_adaptive_refine_coarsen_common_method(True, ext_joined_kernel_hook_parameters_element_action, joined_traversator_signature)
#				self.std_code_blocks.create_adaptive_refine_coarsen_common_method(False, ext_joined_kernel_hook_parameters_element_action, joined_traversator_signature)

			self.std_code_blocks.create_function_header_adaptivity_last_traversal(ext_joined_kernel_hook_parameters_element_action)

			self.std_code_blocks.create_adaptive_refine_code(ext_joined_kernel_hook_parameters_element_action)

			# NOT WORKING SO FAR!
#			self.std_code_blocks.create_adaptive_refine_code_templated(joined_traversator_parameters)


		#########################################################
		# EDGE BASED COMMUNICATION
		#########################################################
		if self.config.edgeComm:
			self.std_code_blocks.create_edge_comm_code(
												kernel_hook_parameters_element_action,
												kernel_hook_parameters_edge_comm
											)


		#########################################################
		# FLUX BASED COMMUNICATION
		#########################################################
		if self.config.fluxComm:
			self.std_code_blocks.create_flux_comm_code(
												kernel_hook_parameters_element_action,
												kernel_hook_parameters_edge_comm
											)


		#########################################################
		# VERTEX DATA BASED COMMUNICATION
		#########################################################		
		if self.config.vertexDataComm:
			create_kernel_element_action = create_kernel_element_action and self.std_code_blocks.createVertexCommCode(
																					kernel_hook_parameters_element_action,
																					kernel_hook_parameters_edge_comm
																				)



		if self.config.kernelElementActionCall and not self.config.adaptivity:
			# update joined parameters
			joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
			joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)

			print "	cKernelClass.elementAction("+joined_kernel_hook_parameters_element_action+");"

		print """}"""
		
		

	###################################
	# action
	###################################

	# list to store already created recursive function calls to avoid duplicates
	existing_code_signatures = []

	def __action_create_code(self, triangleFactory):
		triangleFactory.creation_recursion_depth = self.code_generator_depth

		# return if the code for this function was already created
		if self.existing_code_signatures.count(triangleFactory.key) != 0:
			return False

		self.existing_code_signatures.append(triangleFactory.key)

		self.__output_traversator_code(triangleFactory)
	
		# append code to recursion table
		if self.config.createRecursionTable:
				self.recursionTable.append(triangleFactory)

		return True


	###################################
	# recursive unspecified functions
	###################################

	# FORWARD
	def __K_even_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'K', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'V', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return

		self.code_generator_depth += 1
		self.__H_odd_forward(triangleFactory.child_first)
		self.__V_odd_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __H_even_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'H', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'K', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__V_odd_forward(triangleFactory.child_first)
		self.__K_odd_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __V_even_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'V', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'K', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__H_odd_forward(triangleFactory.child_first)
		self.__K_odd_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1



	def __K_odd_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'K', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'V', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__H_even_forward(triangleFactory.child_first)
		self.__V_even_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __H_odd_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'H', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'K', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__V_even_forward(triangleFactory.child_first)
		self.__K_even_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __V_odd_forward(self, triangle_props):
		triangleFactory = CTriangleFactory('forward', 'V', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'K', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__H_even_forward(triangleFactory.child_first)
		self.__K_even_forward(triangleFactory.child_second)
		self.code_generator_depth -= 1



	# BACKWARD
	def __K_even_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'K', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'H', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__V_odd_backward(triangleFactory.child_first)
		self.__H_odd_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __H_even_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'H', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'V', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return

		self.code_generator_depth += 1
		self.__K_odd_backward(triangleFactory.child_first)
		self.__V_odd_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __V_even_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'V', 'even', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_right, 'new', triangle_props.edge_hyp, 'H', triangle_props.edge_left, triangle_props.edge_hyp, 'old', self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__K_odd_backward(triangleFactory.child_first)
		self.__H_odd_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1


	def __K_odd_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'K', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'H', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__V_even_backward(triangleFactory.child_first)
		self.__H_even_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __H_odd_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'H', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'V', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__K_even_backward(triangleFactory.child_first)
		self.__V_even_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1

	def __V_odd_backward(self, triangle_props):
		triangleFactory = CTriangleFactory('backward', 'V', 'odd', triangle_props.edge_hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_left, triangle_props.edge_hyp, 'new', 'H', triangle_props.edge_right, 'old', triangle_props.edge_hyp, self.config)
		self.std_code_blocks.setTriangleFactory(triangleFactory)
		if not self.__action_create_code(triangleFactory):
			return
		self.code_generator_depth += 1
		self.__K_even_backward(triangleFactory.child_first)
		self.__H_even_backward(triangleFactory.child_second)
		self.code_generator_depth -= 1


	###################################
	# PRIVATE: Setup code creation
	###################################
	def __createCodeSetup(self, config):
		# setup the configuration
		self.config = config
		
		# output some copyright information
		print """
/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * This file was automatically generated by the sierpinski code generator
 */
"""

		# debug: restrict max. code generator depth
		# -111111111 to disable
		self.code_generator_depth = 0

	def __helperFunctionBorderTypeToEnumType(self, type):
		if type == 'old':
			return 'EDGE_TYPE_OLD'
		elif type == 'new':
			return 'EDGE_TYPE_NEW'
		elif type == 'b':
			return 'EDGE_TYPE_BOUNDARY'
		sys.stderr.write("UNKNOWN ENUM TYPE")
		assert(False)

	def __findRecursionPythonTableIdxByName(self, fun_name):
		for r in range(0, len(self.recursionTable)):
			if self.recursionTable[r].sig_parent == fun_name:
				return r
		sys.stderr.write("REC FUN NAME " + fun_name + " NOT FOUND!")
		raise Exception.MemoryError
		sys.exit()

	def __sfcMethodExists(self, fun_name):
		for r in range(0, len(self.recursionTable)):
			if self.recursionTable[r].sig_parent == fun_name:
				return True
		return False

	def __findRecursionTableIdx_TypeToK_EdgeType1ToType2(self, config, r, type_old, type_new):
		match_factory = copy.deepcopy(r)
		match_factory.type_parent = 'K'

		if config.edgeComm:
			if match_factory.edge_hyp == type_old:
				match_factory.edge_hyp = type_new
			if match_factory.edge_left == type_old:
				match_factory.edge_left = type_new
			if match_factory.edge_right == type_old:
				match_factory.edge_right = type_new

		for r in range(0, len(self.recursionTable)):
			x = self.recursionTable[r]
			
			if match_factory.type_parent != x.type_parent:
				continue
			if match_factory.even_odd != x.even_odd:
				continue
			if match_factory.direction != x.direction:
				continue
	
			if config.edgeComm:
				if match_factory.edge_hyp != x.edge_hyp:
					continue
				if match_factory.edge_left != x.edge_left:
					continue
				if match_factory.edge_right != x.edge_right:
					continue

			if config.kernelParams_normals:
				if match_factory.hyp_normal != x.hyp_normal:
					continue

			return self.recursionTable[r].idx

		sys.stdout = sys.__stdout__
		sys.stderr.write("REC FUNCTION NOT FOUND!\n")
		print "       hyp: "+match_factory.edge_hyp
		print "     right: "+match_factory.edge_right
		print "      left: "+match_factory.edge_left
		print "      type: "+match_factory.type_parent
		print "  even_odd: "+match_factory.even_odd
		print " direction: "+match_factory.direction
		print "hyp_normal: "+str(match_factory.hyp_normal)
		print "       idx: "+str(match_factory.idx)
		sys.exit()		




	###################################
	# CREATE CODE for K recursions
	###################################
	def __createCodeK(self, config, codeForAllEdgeTypes = []):
		self.__createCodeSetup(config)

		if len(codeForAllEdgeTypes) == 0:
			# create code only for recursive edge types
			
			# create the forward code
			if self.config.createForwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('K', 'new', 'b', 'b', normals.ne)
				self.__K_even_forward(triangle_props)
	
			# create the backward code
			if self.config.createBackwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('K', 'old', 'b', 'b', normals.ne)
				self.__K_even_backward(triangle_props)
		else:
			# create code for all edge types given in codeForAllEdgeTypes
			
			for i in codeForAllEdgeTypes:

				triangle_props = CTriangleProps()
				
				evenOdd = 'even'
				if len(i) <= 4:
					triangle_props.setup('K', i[0], i[1], i[2], i[3])
				else:
					# even/odd
					evenOdd = i[4]
					triangle_props.setup('K', i[0], i[1], i[2], i[3], evenOdd)

				if evenOdd == 'even':
					if self.config.createForwardCode:
						self.__K_even_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__K_even_backward(triangle_props)
				elif evenOdd == 'odd':
					
					# create forward code
					if self.config.createForwardCode:
						self.__K_odd_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__K_odd_backward(triangle_props)
						
				else:
					sys.stderr.write("ERROR evenOdd\n")



	###################################
	# CREATE CODE for V recursions
	###################################
	def __createCodeV(self, config, codeForAllEdgeTypes = []):
		self.__createCodeSetup(config)

		if len(codeForAllEdgeTypes) == 0:
			# create code only for recursive edge types
			
			# create the forward code
			if self.config.createForwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('V', 'new', 'b', 'b', normals.ne)
				self.__V_even_forward(triangle_props)
	
			# create the backward code
			if self.config.createBackwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('V', 'old', 'b', 'b', normals.ne)
				self.__V_even_backward(triangle_props)
		else:
			# create code for all edge types given in codeForAllEdgeTypes
			
			for i in codeForAllEdgeTypes:

				triangle_props = CTriangleProps()
				
				evenOdd = 'even'
				if len(i) <= 4:
					triangle_props.setup('V', i[0], i[1], i[2], i[3])
				else:
					# even/odd
					evenOdd = i[4]
					triangle_props.setup('V', i[0], i[1], i[2], i[3], evenOdd)

				if evenOdd == 'even':
					if self.config.createForwardCode:
						self.__V_even_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__V_even_backward(triangle_props)
				elif evenOdd == 'odd':
					
					# create forward code
					if self.config.createForwardCode:
						self.__V_odd_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__V_odd_backward(triangle_props)
						
				else:
					sys.stderr.write("ERROR evenOdd\n")




	#
	# return all kernelParams_normals which are available for an edge type
	#
	def __allNormalsForEdgeType(self, t1, t2, t3):
		#
		# to reduce the code size and thus to reduce the compilation
		# time, only specific traversal methods are created.
		#
		return [
#			[t1, t2, t3, kernelParams_normals.n, 'even'],
			[t1, t2, t3, normals.ne, 'even'],
#			[t1, t2, t3, kernelParams_normals.e, 'even'],
			[t1, t2, t3, normals.se, 'even'],
#			[t1, t2, t3, kernelParams_normals.s, 'even'],
			[t1, t2, t3, normals.sw, 'even'],
#			[t1, t2, t3, kernelParams_normals.w, 'even'],
			[t1, t2, t3, normals.nw, 'even'],
			
			[t1, t2, t3, normals.n, 'odd'],
#			[t1, t2, t3, kernelParams_normals.ne, 'odd'],
			[t1, t2, t3, normals.e, 'odd'],
#			[t1, t2, t3, kernelParams_normals.se, 'odd'],
			[t1, t2, t3, normals.s, 'odd'],
#			[t1, t2, t3, kernelParams_normals.sw, 'odd'],
			[t1, t2, t3, normals.w, 'odd'],
#			[t1, t2, t3, normals.nw, 'odd']
		]

	def outputRecursionTable(self, config):
		self.std_code_blocks.outputRecursionTable(self.__sfcMethodExists, self.recursionTable)

	#
	# create code for parallel traversals
	#
	def createCode(self, config):
		
		config.internal_traversatorMethodDepth = config.kernelParams_depth
		config.internal_edgeTypes = config.internal_edgeTypes or config.edgeComm or config.fluxComm or config.vertexDataComm or config.adaptivity

		self.std_code_blocks.setConfig(config)

		# reset signatures
		self.existing_code_signatures = []

		# setup edge type list for K traversals
		edgeTypeListV = []
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'b', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('new', 'new', 'new'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('old', 'old', 'old'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'new', 'new'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'old', 'old'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('new', 'b', 'new'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('old', 'b', 'old'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('new', 'new', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('old', 'old', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'b', 'new'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'b', 'old'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('new', 'b', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('old', 'b', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'new', 'b'))
		edgeTypeListV.extend(self.__allNormalsForEdgeType('b', 'old', 'b'))
		

		# create code for all K traversals
		self.__createCodeV(config, edgeTypeListV)



	###################################
	# RESET
	###################################
	def reset(self):
		self.existing_code_signatures = []
		self.recursionTable = []

