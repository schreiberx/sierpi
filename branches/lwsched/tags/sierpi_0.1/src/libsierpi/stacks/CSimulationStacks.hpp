/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_STACKS_HPP_
#define CSIMULATION_STACKS_HPP_

#include "config.h"
#include <assert.h>
#include "CFBStacks.hpp"
#include "CLRStacks.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "libsierpi/kernels/validate/CValidateTypes.hpp"

/**
 * \brief flags for storing the enumeration variables to create or avoid creating stacks for.
 */
class CSimulationStacks_Enums
{
public:
	enum
	{
		ELEMENT_STACKS		= 1 << 1,
		ADAPTIVE_STACKS		= 1 << 2,
		EDGE_COMM_STACKS	= 1	<< 3,
		EDGE_COMM_PARALLEL_EXCHANGE_STACKS	= 1 << 4,
		VERTEX_COMM_STACKS	= 1	<< 5,
		VERTEX_COMM_PARALLEL_EXCHANGE_STACKS	= 1 << 6
	};
};


/**
 * \brief this class is a handler for all stacks needed to run the simulation.
 */
template <typename CSimulationTypes>
class CSimulationStacks	: private CSimulationStacks_Enums
{
public:
	typedef typename CSimulationTypes::TSimulationElementData TElementData;
	typedef typename CSimulationTypes::TSimulationEdgeData TEdgeData;
	typedef typename CSimulationTypes::TVisualizationVertexData TVertexData;


	// flags specifying, which stacks have been initialized
	unsigned int flags;

	// storage for structure
	CFBStacks<char> structure_stacks;

	// element stacks (ELEMENT_STACK)
	CFBStacks<TElementData> element_data_stacks;

	// adaptive refinement left/right stacks (ADAPTIVE_STACKS)
	CLRStacks<char> adaptive_comm_edge_stacks;

	// adaptive extra stacks
	CLRStacks<char> adaptive_comm_exchange_edge_stacks;

	// stacks to store information about refinement and coarsening (ADAPTIVE_STACKS)
	CFBStacks<unsigned char> adaptivity_state_flag_stacks;

	/**
	 * EDGE BASED SIMULATION COMMUNICATION
	 */

	// communication for timestep (TIMESTEP_STACK)
	CLRStacks<TEdgeData> edge_data_comm_edge_stacks;

	// stacks acting as buffers for parallelization (data exchange without conflicts)
	CLRStacks<TEdgeData> edge_data_comm_exchange_edge_stacks;

	// stack to store temporary edge information for timestep (TIMESTEP_STACK)
	CStack<TEdgeData> edge_comm_buffer;

#if COMPILE_SIMULATION_WITH_GUI
	/**
	 * VERTEX BASED COMMUNICATION FOR VISUALIZATION
	 */

	// communication for timestep (TIMESTEP_STACK)
	CStack<TVertexData> vertex_data_comm_left_edge_stack;
	CStack<TVertexData> vertex_data_comm_right_edge_stack;

	// stacks acting as buffers for parallelization (data exchange without conflicts)
	CStack<TVertexData> vertex_data_comm_exchange_left_edge_stack;
	CStack<TVertexData> vertex_data_comm_exchange_right_edge_stack;

	// stack to store temporary edge information for timestep (TIMESTEP_STACK)
	CStack<TVertexData> vertex_data_comm_buffer;
#endif


#if ADAPTIVE_SUBPARTITION_STACKS
	size_t new_number_of_stack_elements;
#endif


public:
	/**
	 * The stacks are initialized with the following formulae:
	 *
	 * The easiest case is the number of the leaf elements.
	 * since each triangle is split up into 2 disjoint ones and assuming that the
	 * depth at coarsest level is equal to 0, the maximum number of leaf-triangles is:
	 *
	 * | max_leaf_triangles = 2^(max_depth+1)
	 *
	 * When having a look at the number of triangles laying directly at the catheti,
	 * this number is doubled when refining 2 times:
	 *
	 * | max_comm_edges = 2^(max_depth+1)
	 *
	 * In case that ADAPTIVE_SUBPARTITION_STACK_GRANULARITY != -1,
	 * p_max_depth_for_stack_element_allocation has to be set to the initial
	 * triangulation depth.
	 */

#if !ADAPTIVE_SUBPARTITION_STACKS
	CSimulationStacks(
			size_t p_max_depth_for_stack_element_allocation,	///< max depth for which stack elements are allocated
			unsigned int i_flags								///< specify which stacks have to be allocated
	)
	{
		flags = i_flags;

		/**
		 * we have to increase the maximum depth by 1 since by using
		 * arbitrary triangulation cases and attaching a catheti to a hypotenuse,
		 * the refinement of elements below the maximum permitted depth can be
		 * possible!!!
		 */
		p_max_depth_for_stack_element_allocation++;

		size_t max_element_data = (1 << (p_max_depth_for_stack_element_allocation));
		size_t max_structure_elements = (1 << (p_max_depth_for_stack_element_allocation+1));

		size_t max_comm_edges = (1 << (p_max_depth_for_stack_element_allocation/2+2));
		size_t max_edge_comm_buffer = max_structure_elements;

		structure_stacks.resize(max_structure_elements);

		if (flags & ELEMENT_STACKS)
		{
			element_data_stacks.resize(max_element_data);
		}

		if (flags & ADAPTIVE_STACKS)
		{
			adaptive_comm_left_edge_stack.resize(max_comm_edges);
			adaptive_comm_right_edge_stack.resize(max_comm_edges);
			adaptivity_state_flag_stacks.resize(max_element_data);
		}

		if (flags & EDGE_COMM_STACKS)
		{
			edge_data_comm_left_edge_stack.resize(max_comm_edges);
			edge_data_comm_right_edge_stack.resize(max_comm_edges);
			edge_comm_buffer.resize(max_edge_comm_buffer);
		}

		if (flags & EDGE_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			edge_data_comm_exchange_left_edge_stack.resize(max_comm_edges);
			edge_data_comm_exchange_right_edge_stack.resize(max_comm_edges);

			adaptive_comm_exchange_left_edge_stack.resize(max_comm_edges);
			adaptive_comm_exchange_right_edge_stack.resize(max_comm_edges);
		}

		if (flags & VERTEX_COMM_STACKS)
		{
			vertex_data_comm_left_edge_stack.resize(max_element_data*3);
			vertex_data_comm_right_edge_stack.resize(max_element_data*3);
			vertex_data_comm_buffer.resize(max_element_data*3);
		}

		if (flags & VERTEX_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			vertex_data_comm_exchange_left_edge_stack.resize(max_element_data*3);
			vertex_data_comm_exchange_right_edge_stack.resize(max_element_data*3);
		}
	}

#else

	CSimulationStacks(
			size_t p_max_elements_on_stack,						///< maximum number of elements which are stored on the stack
			unsigned int i_flags								///< specify which stacks have to be allocated
	)	:
		new_number_of_stack_elements(0)
	{
		flags = i_flags;

		/**
		 * we have to increase the maximum depth by 1 since by using
		 * arbitrary triangulation cases and attaching a catheti to a hypotenuse,
		 * the refinement of elements below the maximum permitted depth can be
		 * possible!!!
		 */

		unsigned int max_element_data = p_max_elements_on_stack;
		size_t max_structure_elements = max_element_data*2-1;

		size_t max_comm_edges = max_element_data/2+2;
		size_t max_edge_comm_buffer = max_structure_elements;

		structure_stacks.resize(max_structure_elements);

		if (flags & ELEMENT_STACKS)
		{
			element_data_stacks.resize(max_element_data);
		}

		if (flags & ADAPTIVE_STACKS)
		{
			adaptive_comm_edge_stacks.resize(max_comm_edges);
			adaptivity_state_flag_stacks.resize(max_element_data);
		}

		if (flags & EDGE_COMM_STACKS)
		{
			edge_data_comm_edge_stacks.resize(max_comm_edges);
			edge_comm_buffer.resize(max_edge_comm_buffer);
		}

		if (flags & EDGE_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			edge_data_comm_exchange_edge_stacks.resize(max_comm_edges);

			adaptive_comm_exchange_edge_stacks.resize(max_comm_edges);
		}

#if COMPILE_SIMULATION_WITH_GUI
		if (flags & VERTEX_COMM_STACKS)
		{
			vertex_data_comm_left_edge_stack.resize(max_element_data*3);
			vertex_data_comm_right_edge_stack.resize(max_element_data*3);
			vertex_data_comm_buffer.resize(max_element_data*3);
		}

		if (flags & VERTEX_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			vertex_data_comm_exchange_left_edge_stack.resize(max_element_data*3);
			vertex_data_comm_exchange_right_edge_stack.resize(max_element_data*3);
		}
#endif
	}

	/**
	 * this method has to be executed to change the stack size so that
	 * the given number of elements fits onto the new stack during the
	 * last adaptivity traversal.
	 */
	void changeStackSize_BeforeAdaptivityLastTraversal
	(
		size_t p_new_number_of_elementdata	///< requested number of elements to fit onto the new stacks
	)
	{
		size_t current_number_of_elements = element_data_stacks.forward.getNumberOfElementsOnStack();
		size_t max_number_of_elements = element_data_stacks.forward.getMaxNumberOfElements();

		/**
		 * no change necessary
		 */
		if (current_number_of_elements == p_new_number_of_elementdata)
		{
			new_number_of_stack_elements = 0;
			return;
		}

		if (max_number_of_elements < p_new_number_of_elementdata)
		{
			/**
			 * grow stacks and allocate an additional block
			 */
			new_number_of_stack_elements = p_new_number_of_elementdata + ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING_ELEMENTS(sizeof(TElementData));
		}
		else if (p_new_number_of_elementdata+ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING_ELEMENTS(sizeof(TElementData)) < max_number_of_elements)
		{
			/**
			 * shrink stack to new element data already fits onto the stack
			 */
			new_number_of_stack_elements = p_new_number_of_elementdata;
		}
		else
		{
			return;
		}
#if 0
		std::cout << "RESIZE from " << max_number_of_elements << " to " << new_number_of_stack_elements << std::endl;
#endif
		size_t max_element_data = new_number_of_stack_elements;
		size_t max_structure_elements = new_number_of_stack_elements*2-1;

		size_t max_comm_edges = new_number_of_stack_elements/2+4;

		structure_stacks.forward.resize(max_structure_elements);

		if (flags & ELEMENT_STACKS)
		{
			element_data_stacks.backward.resize(max_element_data);
		}

		if (flags & ADAPTIVE_STACKS)
		{
			// resize this stacks here since they are used to update the edge comm information!
			adaptive_comm_edge_stacks.resize(max_comm_edges);
		}


		return;
	}



	void changeStackSize_AfterAdaptivityLastTraversal()
	{
		if (new_number_of_stack_elements == 0)
			return;

		size_t max_element_data = new_number_of_stack_elements;
		size_t max_structure_elements = new_number_of_stack_elements*2-1;

		size_t max_comm_edges = new_number_of_stack_elements/2+4;
		size_t max_edge_comm_buffer = max_structure_elements;

		structure_stacks.backward.resize(max_structure_elements);

		if (flags & ELEMENT_STACKS)
		{
			// element data stacks have been switched
			element_data_stacks.backward.resize(max_element_data);
		}

		if (flags & ADAPTIVE_STACKS)
		{
			adaptivity_state_flag_stacks.resize(max_element_data);
		}

		if (flags & EDGE_COMM_STACKS)
		{
			assert(edge_data_comm_edge_stacks.left.isEmpty());
			assert(edge_data_comm_edge_stacks.right.isEmpty());

			edge_data_comm_edge_stacks.left.resize(max_comm_edges);
			edge_data_comm_edge_stacks.right.resize(max_comm_edges);
			edge_comm_buffer.resize(max_edge_comm_buffer);
		}

		if (flags & EDGE_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			assert(edge_data_comm_exchange_edge_stacks.left.isEmpty());
			assert(edge_data_comm_exchange_edge_stacks.right.isEmpty());

			edge_data_comm_exchange_edge_stacks.resize(max_comm_edges);

			assert(adaptive_comm_exchange_edge_stacks.left.isEmpty());
			assert(adaptive_comm_exchange_edge_stacks.right.isEmpty());

			adaptive_comm_exchange_edge_stacks.resize(max_comm_edges);
		}

#if COMPILE_SIMULATION_WITH_GUI
		if (flags & VERTEX_COMM_STACKS)
		{
			/*
			 * avoid assertion since the stack counter is set to 0
			 * at the beginning of vertex communication.
			 *
			 * the reason is that the number of elements on the adjacent
			 * vertex data stack has to be known for vertexDataExchange
			 */
//			assert(vertex_data_comm_left_edge_stack.isEmpty());
//			assert(vertex_data_comm_right_edge_stack.isEmpty());

			vertex_data_comm_left_edge_stack.resize(max_comm_edges);
			vertex_data_comm_right_edge_stack.resize(max_comm_edges);
			vertex_data_comm_buffer.resize(max_edge_comm_buffer);
		}

		if (flags & VERTEX_COMM_PARALLEL_EXCHANGE_STACKS)
		{
			assert(vertex_data_comm_exchange_left_edge_stack.isEmpty());
			assert(vertex_data_comm_exchange_right_edge_stack.isEmpty());

			vertex_data_comm_exchange_left_edge_stack.resize(max_comm_edges);
			vertex_data_comm_exchange_right_edge_stack.resize(max_comm_edges);
		}
#endif

	}
#endif


	std::string getMemoryInformationString()
	{
		std::ostringstream ss;

		ss << "Stack Memory Information: " << std::endl;

		/*
		 * STRUCTURE
		 */
		if (structure_stacks.isInitialized())
			ss << " + Structure Stacks (FWD): " << structure_stacks.forward.getMaxNumberOfElements() << " Elements, "
				<< structure_stacks.forward.getMaxNumberOfElements()*sizeof(char) << " Bytes" << std::endl;

		/*
		 * ADAPTIVE
		 */
		if (adaptive_comm_edge_stacks.left.isInitialized())
			ss << " + Adaptive Comm left edge stack: "
				<< adaptive_comm_edge_stacks.left.getMaxNumberOfElements() << " Elements, "
				<< adaptive_comm_edge_stacks.left.getMemSize() << " Bytes" << std::endl;

		if (adaptive_comm_edge_stacks.right.isInitialized())
			ss << " + Adaptive Comm right edge stack: "
				<< adaptive_comm_edge_stacks.right.getMaxNumberOfElements() << " Elements, "
				<< adaptive_comm_edge_stacks.right.getMemSize() << " Bytes" << std::endl;


		if (adaptive_comm_exchange_edge_stacks.left.isInitialized())
			ss << " + Adaptive Comm Exchange right edge stack: "
				<< adaptive_comm_exchange_edge_stacks.left.getMaxNumberOfElements() << " Elements, "
				<< adaptive_comm_exchange_edge_stacks.left.getMemSize() << " Bytes" << std::endl;

		if (adaptivity_state_flag_stacks.isInitialized())
			ss << " + Adaptive flag stack: " << adaptivity_state_flag_stacks.forward.getMaxNumberOfElements() << " Elements, "
				<< adaptivity_state_flag_stacks.forward.getMemSize() << " Bytes" << std::endl;



		/*
		 * ELEMENT DATA
		 */
		if (element_data_stacks.isInitialized())
			ss << " + Element Data Stacks (FWD): " << element_data_stacks.forward.getMaxNumberOfElements() << " Elements, "
				<< element_data_stacks.forward.getMemSize() << " Bytes" << std::endl;

		/**
		 * EDGE COMM
		 */

		if (edge_data_comm_edge_stacks.left.isInitialized())
			ss << " + Edge Data Comm left edge stack: " << edge_data_comm_edge_stacks.left.getMaxNumberOfElements() << " Elements, "
				<< edge_data_comm_edge_stacks.left.getMemSize() << " Bytes" << std::endl;

		if (edge_data_comm_edge_stacks.right.isInitialized())
			ss << " + Edge Data Comm right edge stack: " << edge_data_comm_edge_stacks.right.getMaxNumberOfElements() << " Elements, "
				<< edge_data_comm_edge_stacks.right.getMemSize() << " Bytes" << std::endl;


		if (edge_data_comm_exchange_edge_stacks.left.isInitialized())
			ss << " + Edge Data Comm exchange left edge stack: " << edge_data_comm_exchange_edge_stacks.left.getMaxNumberOfElements() << " Elements, "
				<< edge_data_comm_exchange_edge_stacks.left.getMemSize() << " Bytes" << std::endl;

		if (edge_data_comm_exchange_edge_stacks.right.isInitialized())
			ss << " + Edge Data Comm exchange right edge stack: " << edge_data_comm_exchange_edge_stacks.right.getMaxNumberOfElements() << " Elements, "
				<< edge_data_comm_exchange_edge_stacks.right.getMemSize() << " Bytes" << std::endl;


		if (edge_comm_buffer.isInitialized())
			ss << " + Edge Comm Buffer: " << edge_comm_buffer.getMaxNumberOfElements() << " Elements, "
				<< edge_comm_buffer.getMemSize() << " Bytes" << std::endl;

		return ss.str();
	}
};

#endif /* CSIERPINSKI_H_ */
