/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGDUMMY_HPP_
#define CMAINTHREADINGDUMMY_HPP_


#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
public:
	void threading_setup()
	{
	}


	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}

	bool threading_simulationLoopIteration()
	{
		return simulationLoopIteration();
	}

	void threading_shutdown()
	{
	}

	void threading_setNumThreads(int i)
	{
	}

	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGDUMMY_HPP_ */
