/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include "libmath/CMath.hpp"


#include <OR.hpp>
#include <AND.hpp>
#include <Claim.hpp>
#include <PEQuantity.hpp>
#include <ScalabilityHint.hpp>


#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
public:
	Claim claim;
	AND a;

	int max_cores;


	CMainThreading()
	{
		max_cores = 512;
	}

	void threading_setup()
	{
		int n = getNumberOfThreadsToUse();

		if (n == -1)
		{
			n = omp_get_max_threads();
			setNumberOfThreadsToUse(n);
		}
		else
		{
			omp_set_num_threads(n);
		}

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): " << (int)omp_get_max_threads() << std::endl;

		threading_updateRessourceUtilization();
	}


	/**
	 * update the ressource utilization
	 */
	void threading_updateRessourceUtilization()
	{
#if 0
		/*
		 * increase number of threads to maximum when exceeding twice the
		 * initial number of triangles
		 */
		int num_threads;
//		std::cout << initial_number_of_triangles*2 << " > " << cSimulation->number_of_triangles << std::endl;

		if (initial_number_of_triangles*2 > cSimulation->number_of_triangles)
			num_threads = 1;
		else
			num_threads = getNumberOfThreadsToUse();
#else
		int num_threads = getNumberOfThreadsToUse();
#endif
		float shTable[] = {	1,	0.957,	0.9423,	0.914,	0.8926,	0.8557,	0.821,	0.7868,	0.7532,	0.725,	0.6886,	0.6587	};

		ScalabilityHint sh;

		for (int i = 1; i <= sizeof(shTable)/sizeof(float); i++)
		{
			double scalability = shTable[i-1]*(double)i;
			std::cout << "iOMP: adding scalability hint " << i << ", " << scalability << std::endl;
			sh.insert(i, scalability);
		}

		a.add(new PEQuantity(1, max_cores));
		a.add(&sh);

		claim.invade(a);
	}

	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		/***************************************************
		 * we have to move the parallel region into the simulation
		 * timestep loop to control the amount of CPUs which are used
		 */

		do
		{
			continue_simulation = threadedSimulationLoopIteration();
		} while(continue_simulation);
	}


	bool threading_simulationLoopIteration()
	{
		claim.reinvade();

		bool retval;

#pragma omp parallel
#pragma omp single nowait
		retval = simulationLoopIteration();

		return retval;
	}


	void threading_shutdown()
	{
	}


	void threading_setNumThreads(int i)
	{
//		omp_set_num_threads(i);
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
