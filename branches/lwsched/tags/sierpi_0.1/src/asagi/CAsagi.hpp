/*
 * CAsagi.hpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#ifndef CASAGI_HPP_
#define CASAGI_HPP_


#include "../../MA_rettenb/trunk/include/asagi.h"
#include "../simulations/tsunami_common/types/CTsunamiTypes.hpp"

class CAsagi
{
public:
	static asagi::Grid *singleton;

	CAsagi(const char *filename);

	virtual ~CAsagi();
};



CTsunamiSimulationTypes::TVertexScalar getAsagiElementBathymetryMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar size
	);

#endif /* CASAGI_HPP_ */
