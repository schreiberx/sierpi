/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

//#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData.hpp"
#include "../common/CAdaptivity_0thOrder.hpp"
#include "../../CTsunamiSimulationDataSets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_Column_0thOrder	: public CAdaptivity_0thOrder
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData<CSetup_Column_0thOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiVertexScalar T;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;

	/*
	 * refinement/coarsening parameters
	 */
	TTsunamiDataScalar refine_threshold;
	TTsunamiDataScalar coarsen_threshold;

	// center of column
	TTsunamiVertexScalar columnCenterX, columnCenterY;

	// radius of column
	TTsunamiVertexScalar columnRadius;
	TTsunamiVertexScalar columnRadiusSquared;



	CSetup_Column_0thOrder()	:
		columnCenterX(0),
		columnCenterY(0),
		columnRadius(0)
	{
	}



private:
	inline bool insideColumn(
			TVertexScalar px, TVertexScalar py
	)
	{
		TVertexScalar x = columnCenterX-px;
		TVertexScalar y = columnCenterY-py;

		return x*x+y*y < columnRadiusSquared;
	}






public:
	inline bool should_refine(
			TVertexScalar vleft_x, TVertexScalar vleft_y,
			TVertexScalar vright_x, TVertexScalar vright_y,
			TVertexScalar vtop_x, TVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *o_elementData
	)
	{
		/*
		 * check whether the line on the circle intersects with the triangle
		 */
		int counter = 0;
		counter += (int)insideColumn(vleft_x, vleft_y);
		counter += (int)insideColumn(vright_x, vright_y);
		counter += (int)insideColumn(vtop_x, vtop_y);

		if (counter == 0)
		{
			/*
			 * completely outside circle
			 */
			// nothing to to & no refinement request
			return false;
		}

		// circle intersects triangle
		if (counter == 1 || counter == 2)
			return true;

		// nothing to do & no refinement request
		return false;
	}


	inline bool should_coarsen(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *element
	)
	{
		return false;
	}


	void setup_Parameters(
			TTsunamiVertexScalar p_columnCenterX,
			TTsunamiVertexScalar p_columnCenterY,
			TTsunamiVertexScalar p_columnRadius,

			int i_methodId,
			CTsunamiSimulationDataSets *i_cSimulationDataSets
	)
	{
		columnCenterX = p_columnCenterX;
		columnCenterY = p_columnCenterY;
		columnRadius = p_columnRadius;
		columnRadiusSquared = columnRadius*columnRadius;

		cSimulationDataSets = i_cSimulationDataSets;
	}



	void setup_WithKernel(
			CSetup_Column_0thOrder<TElementData, TSimulationStacks> &parent
	)
	{
		columnCenterX = parent.columnCenterX;
		columnCenterY = parent.columnCenterY;
		columnRadius = parent.columnRadius;
		columnRadiusSquared = parent.columnRadiusSquared;

		cSimulationDataSets = parent.cSimulationDataSets;
	}





public:
	/**
	 * COARSEN / REFINE
	 */
	inline void refine_l_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		setupRefinedElements(
			vleft_x,	vleft_y,
			vright_x,	vright_y,
			vtop_x,		vtop_y,

			normal_hypx,	normal_hypy,
			normal_rightx,	normal_righty,
			normal_leftx,	normal_lefty,

			depth,

			i_elementData,
			o_left_elementData,
			o_right_elementData
		);


/*
 NOTE! VALIDATION SETUP IS DONE IN setupRefinedElements.
 VALIDATION SETUP IS NOT ALLOOWED HERE DUE TO SPECIFIC ORDER TO SETUP REFINED ELEMENTS

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_left_elementData->validation.setupLeftElementFromParent(&i_elementData->validation);
		o_right_elementData->validation.setupRightElementFromParent(&i_elementData->validation);
#endif
*/
	}



	inline void refine_ll_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_element
				);

		// LEFT childs (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,

					depth+1,

					left_right_element, left_left_element, left_right_element
				);
	}



	inline void refine_l_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,

				depth,

				i_elementData, left_element, right_right_element
			);

		// RIGHT childs (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,

					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void refine_ll_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,
			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// RIGHT + LEFT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_right_element);

		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		// LEFT childs (LEFT + RIGHT)
		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
	
					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
			
					depth+1,

					left_right_element, left_left_element, left_right_element);


		// LEFT childs (LEFT + RIGHT)
		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
					
					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					
					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void coarsen(
			TVertexScalar vleft_x, TVertexScalar vleft_y,
			TVertexScalar vright_x, TVertexScalar vright_y,
			TVertexScalar vtop_x, TVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *coarsed_element,

			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element
	)
	{
		// there was never an agreement to coarsening
		assert(false);
	}


	void storeReduceValue(
			TTsunamiDataScalar *o_reduceValue
	)
	{
		*o_reduceValue = CMath::numeric_inf<TTsunamiDataScalar>();
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
