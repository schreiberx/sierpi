/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 23, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CBOUNDARYCONDITIONS_HPP_
#define CBOUNDARYCONDITIONS_HPP_


enum EBoundaryConditions
{
	BOUNDARY_CONDITION_VELOCITY_ZERO,
	BOUNDARY_CONDITION_NEUMANN,
	BOUNDARY_CONDITION_BOUNCE_BACK,
	BOUNDARY_CONDITION_DIRICHLET
};


#endif /* CBOUNDARYCONDITIONS_HPP_ */
