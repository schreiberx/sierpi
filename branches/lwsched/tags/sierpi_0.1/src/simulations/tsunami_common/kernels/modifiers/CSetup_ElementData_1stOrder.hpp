/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_1ST_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_1ST_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData_Depth.hpp"
#include "../common/CAdaptivity_1stOrder.hpp"
#include "../../CTsunamiSimulationDataSets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_ElementData_1stOrder	: public CAdaptivity_1stOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_ElementData_Depth<CSetup_ElementData_1stOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiVertexScalar T;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;


	/*
	 * callback for terrain data
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	/*
	 * use max function to set up height
	 */
	bool useMaxFunctionForHeight;


	CSetup_ElementData_1stOrder()	:
		cSimulationDataSets(nullptr),
		useMaxFunctionForHeight(false)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	void updateDOF(
			TTsunamiVertexScalar mx, TTsunamiVertexScalar my,
			CTsunamiEdgeData &io_edgeData,
			int i_depth
		)
	{
		io_edgeData.b = cSimulationDataSets->getTerrainHeightByPosition(mx, my, i_depth);

		if (!useMaxFunctionForHeight)
		{
			io_edgeData.h = -io_edgeData.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth);
		}
		else
		{
			// fix height with bathymetry displacement
			TTsunamiDataScalar oldb = io_edgeData.b;
			io_edgeData.h += (oldb - io_edgeData.b);

			io_edgeData.h = CMath::max(io_edgeData.h, -io_edgeData.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth));
		}
	}


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void elementAction(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			int i_depth,

			CTsunamiElementData *io_elementData
	)
	{
		TVertexScalar mx, my;

		// hyp edge
		mx = (TTsunamiVertexScalar)0.5*(vleft_x+vright_x);
		my = (TTsunamiVertexScalar)0.5*(vleft_y+vright_y);
		updateDOF(mx, my, io_elementData->hyp_edge, i_depth);

		// right edge
		mx = (TTsunamiVertexScalar)0.5*(vright_x+vtop_x);
		my = (TTsunamiVertexScalar)0.5*(vright_y+vtop_y);
		updateDOF(mx, my, io_elementData->right_edge, i_depth);

		// left edge
		mx = (TTsunamiVertexScalar)0.5*(vleft_x+vtop_x);
		my = (TTsunamiVertexScalar)0.5*(vleft_y+vtop_y);
		updateDOF(mx, my, io_elementData->left_edge, i_depth);

		io_elementData->cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void setup_Parameters(
			CTsunamiSimulationDataSets *i_cSimulationDataSets,
			bool i_useMaxFunctionForHeight = false
	)
	{
		cSimulationDataSets = i_cSimulationDataSets;
		useMaxFunctionForHeight = i_useMaxFunctionForHeight;
	}



	void setup_WithKernel(
			CSetup_ElementData_1stOrder<TElementData, TSimulationStacks> &parent
	)
	{
		cSimulationDataSets = parent.cSimulationDataSets;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
