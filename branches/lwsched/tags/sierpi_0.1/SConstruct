#! /usr/bin/python

import os
import commands
import re

# specify iOMP location here
#iomp_location=os.environ['HOME']+'/Downloads/iomp/'
iomp_location=os.environ['PWD']+'/../iomp/'

# specify iPMO location here
ipmo_location=os.environ['PWD']+'/../iPMO/'


###################################################################
# configuration ends here - don't modify any line below this one
###################################################################

env = Environment()


###################################################################
# fix environment vars (not imported by default)
###################################################################


#PATH=''
#if 'PATH' in os.environ:
#	PATH=os.environ['PATH']

#LD_LIBRARY_PATH=''
#if 'LD_LIBRARY_PATH' in os.environ:
#	LD_LIBRARY_PATH = os.environ['LD_LIBRARY_PATH']

#env = Environment(ENV = {'PATH' : PATH, 'LD_LIBRARY_PATH' : LD_LIBRARY_PATH})
env = Environment(ENV = os.environ)


###################################################################
# Command line options
###################################################################


#
# build directory
#
AddOption(	'--buildname',
		dest='buildname',
		type='string',
		nargs=1,
		action='store',
		help='build name (and output directory), default: \'./build\'')

env['buildname'] = GetOption('buildname')
if (env['buildname'] == None):
	env['buildname'] = 'build'



#
# simulation (parallel/serial/serial_regular)
#
AddOption(	'--simulation',
		dest='simulation',
		type='string',
		nargs=1,
		action='store',
		help='simulation to compile (tsunami_parallel/tsunami_serial/tsunami_serial_regular), default: tsunami_parallel')

env['simulation'] = GetOption('simulation')

if env['simulation']==None:
	env['simulation'] = 'tsunami_parallel'
elif env['simulation'] not in set(['tsunami_parallel', 'tsunami_serial', 'tsunami_serial_regular']):
	print 'Invalid option "'+env['simulation']+'" given for "--simulation"'
	Exit(1)



#
# serial regular type recursive/table
#
AddOption(	'--serial_regular_type',
		dest='serial_regular_type',
		type='string',
		nargs=1,
		action='store',
		help='type of serial regular implementation (recursive/table), default: \'table\'')

env['serial_regular_type'] = GetOption('serial_regular_type')


#
# periodic boundaries activated/deactivated
#
AddOption(	'--periodic-boundaries',
		dest='periodic_boundaries',
		type='string',
		nargs=1,
		action='store',
		help='activate periodic boundaries (on/off), default: \'on\'')

env['periodic_boundaries'] = GetOption('periodic_boundaries')

if env['periodic_boundaries']==None:
	env['periodic_boundaries'] = 'on'
elif env['periodic_boundaries'] not in set(['on', 'off']):
	print 'Invalid option "'+env['periodic_boundaries']+'" given for "--periodic_boundaries"'
	Exit(1)



#
# compile with mpicxx
#
AddOption(	'--use-mpicompiler',
		dest='use_mpicompiler',
		type='string',
		nargs=1,
		action='store',
		help='activate compilation with mpicompiler (true/false)')

env['use_mpicompiler'] = GetOption('use_mpicompiler')
if (env['use_mpicompiler'] == None or (env['use_mpicompiler'] not in ['true', 'false'])):
	env['use_mpicompiler'] = 'false'



#
# compile with fancy ASAGI
#
AddOption(	'--use-asagi',
		dest='use_asagi',
		type='string',
		nargs=1,
		action='store',
		help='compiler with asagi')

env['use_asagi'] = GetOption('use_asagi')
if (env['use_asagi'] == None or (env['use_asagi'] not in ['true', 'false'])):
	env['use_asagi'] = 'false'



#
# compiler (gnu/intel)
#
AddOption(	'--compiler',
		dest='compiler',
		type='string',
		nargs=1,
		action='store',
		help='specify compiler to use (gnu/intel), default: gnu')

env['compiler'] = GetOption('compiler')

if (env['compiler'] == None or (env['compiler'] not in ['gnu', 'intel', 'open64'])):
	env['compiler'] = 'gnu'




#
# enable fortran (only valid for intel compiler)
#
AddOption(	'--enable-fortran',
		dest='enable_fortran',
		type='string',
		nargs=1,
		action='store',
		help='enable fortran compiler (true/false), defaut: false')

env['enable_fortran'] = GetOption('enable_fortran')

if (env['enable_fortran'] == None or (env['enable_fortran'] not in ['true', 'false'])):
	env['enable_fortran'] = 'false'


#
# enable fortran (only valid for intel compiler)
#
AddOption(	'--tsunami-order-of-basisfunctions',
		dest='tsunami_order_of_basisfunctions',
		type='int',
		nargs=1,
		action='store',
		help='''order of basisfunction (0 and 1 supported so far)''')

env['tsunami_order_of_basis_functions'] = GetOption('tsunami_order_of_basisfunctions')

#
# enable fortran (only valid for intel compiler)
#
AddOption(	'--tsunami-flux-solver',
		dest='tsunami_flux_solver',
		type='int',
		nargs=1,
		action='store',
		help='''set flux solver to apply to riemann problems: (
			0: lax friedrich with constant numerical friction
			1: lax friedrich
			2: fwave
			3: augumented riemann
			4: hybrid
			5: augumented riemann (fortrancode from geoclaw)''')

env['tsunami_flux_solver'] = GetOption('tsunami_flux_solver')

#
# tsunami runge-kutta order
#
AddOption(	'--tsunami-runge-kutta-order',
		dest='tsunami-runge-kutta-order',
		type='string',
		nargs=1,
		action='store',
		help='tsunami runge kutta order (1/2), default: 1')

env['tsunami_runge_kutta_order'] = GetOption('tsunami-runge-kutta-order')


if env['tsunami_runge_kutta_order'] == None:
	env['tsunami_runge_kutta_order'] = '1'

if env['tsunami_runge_kutta_order'] not in ['1', '2']:
	print 'Invalid option '+env['tsunami_runge_kutta_order']+'" given for "--runge-kutta-order"'
	Exit(1)




#
# compile mode (debug/release)
#
AddOption(	'--mode',
		dest='mode',
		type='string',
		nargs=1,
		action='store',
		help='specify release or debug mode (release/debug), default: release')

env['mode'] = GetOption('mode')

if (env['mode'] == None or (env['mode'] not in ['release', 'debug'])):
	env['mode'] = 'release'



#
# GUI (true/false)
#
AddOption(	'--gui',
		dest='gui',
		type='string',
		action='store',
		help='build with enabled OpenGL GUI (off/on), default: off')

env['gui'] = GetOption('gui')

if (env['gui'] == None or (env['gui'] not in ['off','on'])):
	env['gui'] = 'off'



#
# skip adaptive conforming sub-partitions
#
AddOption(	'--sacsp',
		dest='sacsp',
		type='string',
		nargs=1,
		action='store',
		help='Skip adaptive conforming sub-partitions, default: on')

env['sacsp'] = GetOption('sacsp')

if (env['sacsp'] == None or (env['sacsp'] not in ['off', 'on'])):
	env['sacsp'] = 'on'

#
# threading
#
AddOption(	'--threading',
		dest='threading',
		type='string',
		nargs=1,
		action='store',
		help='Threading to use (off/omp/tbb/iomp/ipmo), default: omp')

env['threading'] = GetOption('threading')

if (env['threading'] == None or (env['threading'] not in ['off', 'omp', 'tbb', 'iomp', 'ipmo'])):
	env['threading'] = 'off'



#
# Adaptive Stack Size (true/false)
#
AddOption(	'--adaptiveStackSize',
		dest='adaptiveStackSize',
		type='int',
		action='store',
		help='build with enabled adaptiveStackSize (default: 0 - source code default)')

env['adaptiveStackSize'] = GetOption('adaptiveStackSize')

if env['adaptiveStackSize'] == None:
	env['adaptiveStackSize'] = 0


#
# floating point precision
#
AddOption(	'--fp-precision',
		dest='fp_precision',
		type='string',
		nargs=1,
		action='store',
		help='Floating point precision (single/double), default: single')

env['fp_precision'] = GetOption('fp_precision')

if env['fp_precision'] != None:
	if (env['fp_precision'] not in ['single', 'double']):
		print "invalid option given for fp-precision"
		Exit(-1)


###################################################################
# SETUP COMPILER AND LINK OPTIONS
###################################################################


###################################################################
# ASAGI
#
if env['use_asagi'] == 'true':
	env['use_mpicompiler'] = 'true'
	env.Append(CXXFLAGS=' -DUSE_ASAGI=1')

	# TODO: get pkg-config or default installation path for asagi
	env.Append(LINKFLAGS=' -L'+os.environ['HOME']+'/workspace/MA_rettenb/trunk/build/')
	env.Append(LIBS=['asagi'])
else:
	env.Append(CXXFLAGS=' -DUSE_ASAGI=0')



###################################################################
# Default libs
#

env.ParseConfig("pkg-config libxml-2.0 --cflags --libs")



###################################################################
# Tsunami flux solver to use
#
if env['tsunami_flux_solver'] != None:
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_FLUX_SOLVER='+str(env['tsunami_flux_solver']))

	if env['tsunami_flux_solver'] == 5:

		# enable fortran compiler
		env['enable_fortran']='true'

		if env['fp_precision'] != 'double':
			print 'Flux Solver 5 only available for double floating point precision (use --fp-precision=double)!'
			Exit(-1)



###################################################################
# Tsunami runge kutta order
#
if env['tsunami_runge_kutta_order'] != None:
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER='+str(env['tsunami_runge_kutta_order']))



###################################################################
# Tsunami flux solver to use
#
if env['tsunami_order_of_basis_functions'] != None:
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS='+str(env['tsunami_order_of_basis_functions']))



###################################################################
# Compiler stuff
#
if env['compiler'] == 'gnu':
	reqversion = [4,6,1]

	#
	# get gcc version using -v instead of -dumpversion since SUSE gnu compiler
	# returns only 2 instead of 3 digits with -dumpversion
	#
	gccv = commands.getoutput('g++ -v').splitlines()
	gccversion = gccv[-1].split(' ')[2].split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'At least GCC Version 4.6.1 necessary.'
			Exit(1)

	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate gnu C++ compiler

	# todo: fix me also for intel mpicxx compiler
	if env['use_mpicompiler'] == 'true':
		env.Replace(CXX = 'mpicxx')
	else:
		env.Replace(CXX = 'g++')


	if env['enable_fortran'] == 'true':
		print "GNU compiler not supported with fortran enabled"
		Exit(-1)

if env['compiler'] == 'open64':
	reqversion = [4,6,1]
	open64version = commands.getoutput('openCC -dumpversion').split('.')

	print "Open64 not supported so far..."
	Exit(-1)

#	for i in range(0, 3):
#		if (int(open64version[i]) < int(reqversion[i])):
#			print 'Open64 version ??!? necessary.'
#			Exit(1)

	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
#	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate open64 C++ compiler
	env.Replace(CXX = 'openCC')

if env['compiler'] == 'intel':
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ICPC Version 12.1 necessary.'
			Exit(1)

	env.Append(LINKFLAGS=' -static-intel')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# output more warnings
	env.Append(CXXFLAGS=' -w1')

	# compiler option which has to be appended for icpc 12.1 without update1
	env.Append(CXXFLAGS=' -U__GXX_EXPERIMENTAL_CXX0X__')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')

	# activate intel C++ compiler
	if env['use_mpicompiler'] == 'true':
		env.Replace(CXX = 'mpicxx')
		env.Replace(CXXFLAGS='')
	else:
		env.Replace(CXX = 'icpc')

	if env['enable_fortran'] == 'true':
		env.Replace(FORTRAN='ifort')
		env.Replace(F90='ifort')
		env.Replace(LINK='icpc')




if env['mode'] == 'debug':
	env.Append(CXXFLAGS=' -DDEBUG=1')

	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O0 -g3 -Wall')

	elif env['compiler'] == 'intel':
		env.Append(CXXFLAGS=' -O0 -g')
#		env.Append(CXXFLAGS=' -traceback')

	if env['enable_fortran'] == 'true':
		env.Append(FORTRANFLAGS=' -O0')
		env.Append(F90FLAGS=' -O0')

elif env['mode'] == 'release':
	env.Append(CXXFLAGS=' -DNDEBUG=1')

	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O3 -mtune=native')

	elif env['compiler'] == 'intel':
#		env.Append(CXXFLAGS=' -xHOST -O3 -fast -fno-alias')
#		env.Append(CXXFLAGS=' -O3 -fast -fno-alias')
		env.Append(CXXFLAGS=' -O2')

	if env['enable_fortran'] == 'true':
		env.Append(FORTRANFLAGS=' -O3')
		env.Append(F90FLAGS=' -O3')

else:
	print 'ERROR: mode'
	Exit(1)


###################################################################
# Skipping for adaptive conforming subpartitions
#
	
if env['sacsp'] == 'on':
	env.Append(CXXFLAGS=' -DADAPTIVE_CONFORMING_SUBPARTITION_SKIPPING_ACTIVE=1')
else:
	env.Append(CXXFLAGS=' -DADAPTIVE_CONFORMING_SUBPARTITION_SKIPPING_ACTIVE=0')

###################################################################
# Threading
#


###################################################################
# Additional Fortran options to create threadsafe code
#

if env['threading'] != 'off':
	if env['enable_fortran'] == 'true':
		if env['compiler'] == 'intel':
			env.Append(FORTRANFLAGS=' -openmp')
			env.Append(F90FLAGS=' -openmp')
		

if env['threading'] == 'omp':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1')
	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -fopenmp')
		env.Append(LINKFLAGS=' -fopenmp')
	elif env['compiler'] == 'intel':
		env.Append(CXXFLAGS=' -openmp')
		env.Append(LINKFLAGS=' -openmp -openmp-link=static')


elif env['threading'] == 'iomp':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1 -DCOMPILE_WITH_IOMP=1')
	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -fopenmp')
		env.Append(LINKFLAGS=' -fopenmp')
	elif env['compiler'] == 'intel':
		env.Append(CXXFLAGS=' -openmp')
		env.Append(LINKFLAGS=' -openmp -openmp-link=static')

	# include directories for iOMP
	env.Append(CXXFLAGS=' -I'+iomp_location+'/iomp/include/')
	env.Append(LINKFLAGS=' -L'+iomp_location+'/build/iomp/ -liomp')
	env.Append(LINKFLAGS=' -L'+os.environ['HOME']+'/local/lib')
	env.Append(LIBS=['iomp', 'boost_serialization'])
	
	env.Append(LINKFLAGS=' -lboost_serialization')	


elif env['threading'] == 'ipmo':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1 -DCOMPILE_WITH_IPMO=1')
	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -fopenmp')
		env.Append(LINKFLAGS=' -fopenmp')
	elif env['compiler'] == 'intel':
		env.Append(CXXFLAGS=' -openmp')
		env.Append(LINKFLAGS=' -openmp -openmp-link=static')

	# include directories for iPMO
	env.Append(CXXFLAGS=' -I'+ipmo_location+'/include/')


elif env['threading'] == 'tbb':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_TBB=1')
	env.Append(CXXFLAGS=' -I'+os.environ['TBBROOT']+'/include/')

	env.Append(LIBPATH=[os.environ['TBBROOT']+'/lib/'])
	env.Append(LIBPATH=os.environ['LD_LIBRARY_PATH'].split(':'))

	if env['mode'] == 'debug':
		env.Append(LIBS=['tbb_debug'])
	else:
		env.Append(LIBS=['tbb'])


###################################################################
# GUI
#

if env['gui'] == 'on':
	# compile flags
	env.Append(CXXFLAGS=' -I'+os.environ['HOME']+'/local/include')
	env.Append(CXXFLAGS=' -DCOMPILE_SIMULATION_WITH_GUI=1')

	# linker flags

	# add nvidia lib path when running on atsccs* workstation
	hostname = commands.getoutput('uname -n')
	if re.match("atsccs.*", hostname):
		env.Append(LIBPATH=['/usr/lib/nvidia-current/'])

	env.Append(LIBPATH=[os.environ['HOME']+'/local/lib'])
	env.Append(LIBS=['GL'])

	reqversion = [2,0,0]
	sdlversion = commands.getoutput('sdl2-config --version').split('.')

	for i in range(0, 3):
		if (int(sdlversion[i]) > int(reqversion[i])):
			break;
		if (int(sdlversion[i]) < int(reqversion[i])):
			print 'libSDL Version 2.0.0 necessary.'
			Exit(1)

	env.ParseConfig("sdl2-config --cflags --libs")
	env.ParseConfig("pkg-config freetype2 --cflags --libs")
else:
	env.Append(CXXFLAGS=' -DCOMPILE_SIMULATION_WITH_GUI=0')


###################################################################
# Simulation
#

if env['simulation'] != None:
	env.Append(CXXFLAGS=' -DSIMULATION_DEFINED=1')

	if env['simulation']=='tsunami_parallel':
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_PARALLEL=1')

	elif env['simulation']=='tsunami_serial':
		if env['threading']!='off':
			print 'Compiling serial version with threading activated is non-sense!'
			Exit(-1)
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL=1')

	elif env['simulation']=='tsunami_serial_regular':
		if env['threading']!='off':
			print 'Compiling serial version with threading activated is non-sense!'
			Exit(-1)
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR=1')

		if env['serial_regular_type'] == 'table':
			env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE=0')
		elif env['serial_regular_type'] == 'recursive':
			env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE=1')
		else:
			print 'Invalid serial regular type!'
			Exit(-1)

	else:
		print 'Invalid simulation specified'
		Exit(-1)


###################################################################
# periodic boundaries
#
if env['periodic_boundaries'] == 'on':
	env.Append(CXXFLAGS=' -DCONFIG_PERIODIC_BOUNDARIES=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_PERIODIC_BOUNDARIES=0')


###################################################################
# adaptive stacks
#

if int(env['adaptiveStackSize']) == 0:
	pass	# use default values in source code
elif int(env['adaptiveStackSize']) == -1:
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACKS=0')
else:
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACKS=1')
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING='+str(env['adaptiveStackSize']))
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING='+str(env['adaptiveStackSize']))


###################################################################
# Floating point precision
#

if env['fp_precision'] == None:
	pass
elif env['fp_precision'] == 'single':
	env.Append(CXXFLAGS=' -DDEFAULT_FLOATING_POINT_PRECISION_SINGLE=1')
elif env['fp_precision'] == 'double':
	env.Append(CXXFLAGS=' -DDEFAULT_FLOATING_POINT_PRECISION_DOUBLE=1')
	



###################################################################
# DEPENDENCIES
###################################################################

# search this paths for dependencies
env.Append(CPPPATH = ['/usr/local/include', '/usr/include'])
# also include the 'src' directory to search for dependencies
env.Append(CPPPATH = ['.', 'src/'])

if env['threading'] == 'ipmo':
	env.Append(CPPPATH = [ipmo_location+'/include/'])
	


######################
# INCLUDE PATH
######################

# ugly hack
for i in ['src/', 'src/include/']:
	env.Append(CXXFLAGS = ' -I'+os.environ['PWD']+'/'+i)


######################
# setup PROGRAM NAME base on parameters
######################
program_name = 'sierpi'

# compiler
program_name += '_'+env['compiler']

# threading
if env['threading'] != 'off':
	program_name += '_'+env['threading']

# sacsp
if env['simulation'] == 'tsunami_parallel':
	if env['sacsp'] == 'off':
		program_name += '_nosacsp'

# gui
if env['gui'] == 'on':
	program_name += '_gui'

# simulation
if env['simulation'] != None:
	program_name += '_'+env['simulation']

# serial_regular
if env['serial_regular_type'] != None:
	program_name += '_'+env['serial_regular_type']


if env['use_asagi'] == 'true':
	program_name += '_asagi'
	

# mode
program_name += '_'+env['mode']



#
# build directory
#
build_dir='build/build_'+program_name

#
# fortran special handling
#
if env['enable_fortran'] == 'true':
	env.Append(FORTRANFLAGS=' -module '+build_dir)
	env.Append(F90FLAGS=' -module '+build_dir)



######################
# get source code files
######################

env.src_files = []

Export('env')
SConscript('src/SConscript', variant_dir=build_dir, duplicate=0)
Import('env')

print
print 'Building program "'+program_name+'"'
print


env.Program('build/'+program_name, env.src_files)

#Exit(0)
