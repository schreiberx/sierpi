/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_


#include "config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
#include "libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// flux projections
#include "CFluxProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (elementAction)
 */
class CEdgeComm_Tsunami_0thOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::TReduceValue;
	using CEdgeComm_TsunamiParameters::getReduceValue;
	using CEdgeComm_TsunamiParameters::getTimestepSize;
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;

	using CEdgeComm_TsunamiParameters::updateCFLCondition3;
	using CEdgeComm_TsunamiParameters::updateCFLCondition2;
	using CEdgeComm_TsunamiParameters::updateCFLCondition1;


	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_0thOrder> TRAV;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_0thOrder()
	{
		boundary_dirichlet.h = CMath::numeric_inf<T>();
		boundary_dirichlet.qx = CMath::numeric_inf<T>();
		boundary_dirichlet.qy = CMath::numeric_inf<T>();
	}

	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_0thOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-partition
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-partition
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_elementData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_elementData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_elementData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}


	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void computeElementDataUpdate_WithFluxes(
			T i_hyp_normal_x, T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x, T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x, T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiEdgeData &i_hyp_edge_flux,		///< incoming flux from hypotenuse
			CTsunamiEdgeData &i_right_edge_flux,	///< incoming flux from right edge
			CTsunamiEdgeData &i_left_edge_flux,		///< incoming flux from left edge

			CTsunamiElementData *io_elementData
	)
	{
		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(square_side_length > 0);
#if 0
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << i_hyp_edge_flux << std::endl;
		std::cout << i_right_edge_flux << std::endl;
		std::cout << i_left_edge_flux << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
#endif
		if (CMath::isNan(io_elementData->dofs.h))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*square_side_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*square_side_length;

#if 0
//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config.delta_timestep/cathetus_length, (T)0.99);
		TTsunamiDataScalar damping = (T)1.0-(config.timestep_size/cat_length);

		io_elementData->dofs.qx *= damping;
		io_elementData->dofs.qy *= damping;
#endif

		TTsunamiDataScalar tmp = (TTsunamiDataScalar)timestep_size/((TTsunamiDataScalar)0.5*cat_length*cat_length);
		io_elementData->dofs.h -=  tmp * (hyp_length*i_hyp_edge_flux.h + cat_length*(i_right_edge_flux.h + i_left_edge_flux.h));
		io_elementData->dofs.qx -= tmp * (hyp_length*i_hyp_edge_flux.qx + cat_length*(i_right_edge_flux.qx + i_left_edge_flux.qx));
		io_elementData->dofs.qy -= tmp * (hyp_length*i_hyp_edge_flux.qy + cat_length*(i_right_edge_flux.qy + i_left_edge_flux.qy));
	}



	/**
	 * \brief CALLED by TRAVERSATOR: All outer edge data is available and this method
	 * is executed giving, among others, the outer edge data as parameters.
	 */
	inline void elementAction(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,

			int i_depth,								///< depth of recursion

			CTsunamiElementData *io_elementData,		///< pointer to element data

			CTsunamiEdgeData *i_hyp_edgeData_outer,		///< DOF on hyp edge from adjacent triangle
			CTsunamiEdgeData *i_right_edgeData_outer,	///< DOF on right edge from adjacent triangle
			CTsunamiEdgeData *i_left_edgeData_outer		///< DOF on left edge from adjacent triangle
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_hyp_edgeData_outer->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edgeData_outer->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edgeData_outer->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_elementData->validation.testEdgeMidpoints(
				i_hyp_edgeData_outer->validation,
				i_right_edgeData_outer->validation,
				i_left_edgeData_outer->validation
			);
#endif

		/*
		 * LOAD local flux DOFs
		 */
		T hyp_max_wave_speed, left_max_wave_speed, right_max_wave_speed;

		// HYPOTENUSE EDGE
		CTsunamiEdgeData edgeCommData_Hyp;
		computeEdgeCommData_Hyp(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &edgeCommData_Hyp
			);


		// RIGHT EDGE
		CTsunamiEdgeData right_edgeData_inner;
		computeEdgeCommData_Right(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &right_edgeData_inner
			);


		// LEFT EDGE
		CTsunamiEdgeData left_edgeData_inner;
		computeEdgeCommData_Left(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &left_edgeData_inner
			);

		/*
		 * compute fluxes
		 */
		CTsunamiEdgeData hyp_edge_flux_inner, left_edge_flux_inner, right_edge_flux_inner;
		CTsunamiEdgeData hyp_edge_flux_outer, left_edge_flux_outer, right_edge_flux_outer;


		fluxSolver.computeNetUpdates(
				edgeCommData_Hyp,		*i_hyp_edgeData_outer,
				hyp_edge_flux_inner,	hyp_edge_flux_outer,
				hyp_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(hyp_edge_flux_inner.qx, hyp_edge_flux_inner.qy, i_hyp_normal_x, i_hyp_normal_y);

		fluxSolver.computeNetUpdates(
				right_edgeData_inner,	*i_right_edgeData_outer,
				right_edge_flux_inner,	right_edge_flux_outer,
				right_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(right_edge_flux_inner.qx, right_edge_flux_inner.qy, i_right_normal_x, i_right_normal_y);

		fluxSolver.computeNetUpdates(
				left_edgeData_inner,	*i_left_edgeData_outer,
				left_edge_flux_inner,	left_edge_flux_outer,
				left_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(left_edge_flux_inner.qx, left_edge_flux_inner.qy, i_left_normal_x, i_left_normal_y);

		/*
		 * update element data using computed fluxes
		 */
		computeElementDataUpdate_WithFluxes(
				i_hyp_normal_x, i_hyp_normal_y,
				i_right_normal_x, i_right_normal_y,
				i_left_normal_x, i_left_normal_y,
				i_depth,

				hyp_edge_flux_inner,
				right_edge_flux_inner,
				left_edge_flux_inner,

				io_elementData
			);

		/*
		 * update CFL condition
		 */
		updateCFLCondition3(hyp_max_wave_speed, right_max_wave_speed, left_max_wave_speed, i_depth);
	}
};


}
}



#endif
