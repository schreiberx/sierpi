/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element.hpp"
#include "../kernels/simulation/CAdaptive_Tsunami.hpp"

namespace sierpi
{
namespace travs
{
class CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element_Private	:
	public CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element<sierpi::kernels::CAdaptive_Tsunami, CTsunamiSimulationStacks>
{
};


CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element()
{
	generic_traversator = new CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element_Private;
}


CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::~CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element()
{
	delete generic_traversator;
}



void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::setup_RootTraversator(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_RootTraversator(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::setup_KernelClass(
		TTsunamiDataScalar i_square_side_length,

		TTsunamiDataScalar i_refine_height_threshold,
		TTsunamiDataScalar i_coarsen_height_threshold,

		TTsunamiDataScalar i_refine_slope_threshold,
		TTsunamiDataScalar i_coarsen_slope_threshold
)
{
	generic_traversator->kernelClass.setup_WithParameters(
			i_square_side_length,

			i_refine_height_threshold,
			i_coarsen_height_threshold,

			i_refine_slope_threshold,
			i_coarsen_slope_threshold
		);
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::actionFirstTraversal(
		CTsunamiSimulationStacks *cStacks
)
{
	return generic_traversator->firstTraversal.action(cStacks);
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::actionMiddleTraversals_Serial(
		CTsunamiSimulationStacks *cStacks
)
{
	return generic_traversator->middleTraversals.action_Serial(cStacks);
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::actionMiddleTraversals_Parallel(
		CTsunamiSimulationStacks *cStacks
)
{
	return generic_traversator->middleTraversals.action_Parallel(cStacks);
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::actionLastTraversal_Parallel(
		CTsunamiSimulationStacks *cStacks,
		CPartition_SplitJoinInformation &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::actionLastTraversal_Serial(
		CTsunamiSimulationStacks *cStacks
)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_RootPartition(p_triangleFactory);
}


/**
 * setup the initial partition traversal for the given factory
 */
void CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element::setup_Partition(
		CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element &parent,
		CTriangle_Factory &p_triangleFactory
	)
{
	generic_traversator->setup_Partition(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->kernelClass.setup_WithKernel(
			parent.generic_traversator->kernelClass
		);
}



}
}
