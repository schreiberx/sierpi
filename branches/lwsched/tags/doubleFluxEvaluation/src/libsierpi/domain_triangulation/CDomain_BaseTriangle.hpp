/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CDomain_BaseTriangle.hpp
 *
 *  Created on: Mar 31, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CDOMAIN_BASE_TRIANGLE_WITH_PARTITION_HPP_
#define CDOMAIN_BASE_TRIANGLE_WITH_PARTITION_HPP_

#include <vector>
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "lib/iBase.hpp"
#include "lib/iRef.hpp"
#include "libsierpi/stacks/CStack.hpp"
#include "lib/MDebugClass.hpp"
#include "libsierpi/parallelization/CPartition_TreeNode.hpp"



/**
 * Storage for a root triangle used for the initial domain triangulation.
 *
 * this data structure is of no use after the domain root triangle tree is set up.
 */
template <typename CSimulation_PartitionHandler>
class CDomain_BaseTriangle
{
	typedef CDomain_BaseTriangle<CSimulation_PartitionHandler> CDomain_Triangle_;
	typedef CPartition_TreeNode<CSimulation_PartitionHandler> CPartition_;

public:
	/**
	 * sierpi triangle properties of the current triangle
	 */
	CTriangle_Factory triangleFactory;

	/**
	 * "anchors" to the adjacent triangles to setup e. g. the initial edge comm
	 */
	class CAdjacent_TrianglesWithPartition
	{
public:
		CDomain_Triangle_ *hyp_edge;
		CDomain_Triangle_ *left_edge;
		CDomain_Triangle_ *right_edge;

		CAdjacent_TrianglesWithPartition()
		{
			hyp_edge = nullptr;
			left_edge = nullptr;
			right_edge = nullptr;
		}

		CAdjacent_TrianglesWithPartition(const CAdjacent_TrianglesWithPartition &i)	:
			hyp_edge(i.hyp_edge),
			left_edge(i.left_edge),
			right_edge(i.right_edge)
		{

		}
	};


	/**
	 * pointers to adjacent triangles
	 */
	CAdjacent_TrianglesWithPartition adjacent_triangles;


	/**
	 * pointer to allocated root partition node
	 *
	 * this is used for convenience to speedup the initialization routines when
	 * converting the list to a binary tree and setting up the cPartitions
	 */
	CPartition_ *cPartition_user_ptr;


	/**
	 * setup connection with right edge
	 */
	void setupRightEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.right_edge = i_baseTriangle;
		triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	/**
	 * setup connection with left edge
	 */
	void setupLeftEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.left_edge = i_baseTriangle;
		triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	/**
	 * setup connection with hyp edge
	 */
	void setupHypEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.hyp_edge = i_baseTriangle;
		triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	CDomain_BaseTriangle(
			CTriangle_Factory &i_newTriangleFactory		///< factory for new triangle
	)	:
		triangleFactory(i_newTriangleFactory),
		cPartition_user_ptr(nullptr)
	{
	}


	/**
	 * copy constructor
	 */
	CDomain_BaseTriangle(
			const CDomain_BaseTriangle<CSimulation_PartitionHandler> &i		///< factory for new triangle
	)	:
		triangleFactory(i.triangleFactory),
		adjacent_triangles(i.adjacent_triangles),
		cPartition_user_ptr(i.cPartition_user_ptr)
	{
	}



	/**
	 * Setup the triangle factory to a given triangle factory.
	 *
	 * This method is useful when the triangleWithPartition was initialized previously
	 * without a given triangleFactory.
	 */
	void setTriangleFactory(CTriangle_Factory &p_triangleFactory)
	{
		triangleFactory = p_triangleFactory;
	}
};

#endif /* CTRIANGLE_GLUE_H_ */
