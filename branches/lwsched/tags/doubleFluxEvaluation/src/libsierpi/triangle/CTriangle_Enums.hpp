/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTriangle_Enums.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTRIANGLE_ENUMS_HPP_
#define CTRIANGLE_ENUMS_HPP_



/**
 * enumerations related to CTriangleFactory
 */
class CTriangle_Enums
{
public:
	/**
	 * enumerator for edges to access edge information via array indices
	 *
	 * the values of the enumeration are really important! don't change them!
	 * it's always assumed, that hyp == 0 and that the right edge is == 1 and the left edge == 1 !!!
	 */
	enum EEdgeEnum
	{
		// start with 0 for hypotenuse
		EDGE_ENUM_HYP_EDGE = 0,

		// a test for an edge can be simply done by comparing for edge enum > 0
		EDGE_ENUM_RIGHT_EDGE = 1,
		EDGE_ENUM_LEFT_EDGE = 2
	};


	/**
	 * different triangle types
	 */
	enum ETriangleTypes
	{
		TRIANGLE_TYPE_K = 0,
		TRIANGLE_TYPE_H = 1,
		TRIANGLE_TYPE_V = 2,

		TRIANGLE_TYPE_INVALID = -1
	};


	/**
	 * directions of traversal
	 */
	enum EDirection
	{
		DIRECTION_FORWARD = 0,
		DIRECTION_BACKWARD = 1,

		DIRECTION_INVALID = -1
	};


	/**
	 * different types of triangle edges
	 */
	enum EEdgeType
	{
		EDGE_TYPE_NEW = 0,
		EDGE_TYPE_OLD = 1,
		EDGE_TYPE_BORDER = 2,

		// inner edge type for regular processing
		EDGE_TYPE_INNER = 3,

		EDGE_TYPE_INVALID = -1
	};


	/**
	 * Normals of hypotenuse
	 */
	enum ENormals
	{
		NORMAL_N	= 0,
		NORMAL_NE	= 1,
		NORMAL_E	= 2,
		NORMAL_SE	= 3,
		NORMAL_S	= 4,
		NORMAL_SW	= 5,
		NORMAL_W	= 6,
		NORMAL_NW	= 7,

		NORMAL_INVALID = -1
	};


	/**
	 * Even of Odd
	 */
	enum EEvenOdd
	{
		EVEN = 0,
		ODD = 1,
		EVEN_ODD_INVALID = -1
	};


	/**
	 * Partition Tree types
	 */
	enum EPartitionTreeNode
	{
		NODE_FIRST_CHILD = 0,
		NODE_SECOND_CHILD = 1,
		NODE_ROOT_TRIANGLE = 2,
		NODE_JOINED = 3,
		NODE_INVALID = -1
	};
};

#endif /* CTRIANGLE_ENUMS_H_ */
