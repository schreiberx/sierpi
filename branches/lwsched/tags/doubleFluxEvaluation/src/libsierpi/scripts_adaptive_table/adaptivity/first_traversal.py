#! /usr/bin/python

#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

#
# this include file cares about the marking process of the adaptivity pass
#
# since the marking process does not modify the triangle structure, the
# structure stack is reversed only once for the 1st forward iteration.
# this traversal is implemented in this file
#

import sys
import lib_tools


#
# CODE FOR ADAPTIVE MARKING ITERATION:
#
# check for adaptive handling of triangles.
# forward information with curve traversal.
# store adaptive refinement bit for later usage (backward)
#
def code_generation(d):
	method_postfix = "_adaptive_marking_1st_traversal";

	if d.direction == 'forward':
		##################################
		# CREATE RECURSIVE CALLS
		##################################
#		lib_tools.create_function_header(d, d.config, method_postfix)
		lib_tools.create_function_header(d, d.config, '')

		##################################
		# LEAF HANDLER
		##################################

		if d.config.elementData:
			# load next element data from stack and put the element data in reversed order on the output stack
			# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			# TODO: avoid this, use a fifo stack and exchange forward/backward stacks
			# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			print """
	TElementData *element_ptr = element_lifo_in.getNextDataPtr();"""

		# setup incoming edge states
		print "	unsigned char incomingEdgeStateTransfer = 0;";
		lib_tools.createIncomingEdgeStateTransfer(d)

		#
		# for the first traversal, this triangle is in state '000'
		#
		parameters = []

		if d.config.depthParameter:
			parameters.append("depth")

		if d.config.vertices:
			parameters.append("vx0, vy0, vx1, vy1, vx2, vy2")

		if d.config.elementData:
			parameters.append("element_ptr")

		print "	unsigned char old_state = 0;"
		print ""

		#
		# REFINEMENT
		#
		# if the depth is larger than the maximum allowed value, don't check for adjacent
		# refinement requests since those cannot exist if every triangle limits the refinement
		# in the same way as this triangle
		#

		lib_tools.checkStateChangeForRefinement(d, parameters)

		#
		# COARSENING
		#
		lib_tools.checkStateChangeForCoarsening(d, parameters)

		#
		# NEW STATE
		#
		lib_tools.getNewStateForStateTransfer()

		#
		# COARSENING SPECIAL CODE
		#
		lib_tools.coarseningAdjacentAgreement(d, False)

		#
		# CREATE OUTGOING DATA
		#
		lib_tools.createOutgoingEdgeStateTransfer(d)
		print "}"

