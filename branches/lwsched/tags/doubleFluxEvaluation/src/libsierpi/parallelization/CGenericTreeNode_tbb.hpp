/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_tbb.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */



#ifndef CGENERICTREENODE_TBB_HPP_
#define CGENERICTREENODE_TBB_HPP_

#include "config.h"


/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE)
 */

/*
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_PartitionTreeNode_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_PartitionTreeNode_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{
	}

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		int count = 1;
		tbb::task_list list;

		if (this_node->first_child_node)
		{
			count++;
			list.push_back(*new(allocate_child()) CTraversalTask_PartitionTreeNode_Parallel<CLambdaFun>(this_node->first_child_node, lambda));
		}

		if (this_node->second_child_node)
		{
			count++;
			list.push_back(*new(allocate_child()) CTraversalTask_PartitionTreeNode_Parallel<CLambdaFun>(this_node->second_child_node, lambda));
		}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		return 0;
	}
};


template <typename CLambdaFun>
void traverse_PartitionTreeNode_Parallel(
		CLambdaFun p_lambda
)
{
	tbb::task::spawn_root_and_wait((*new(tbb::task::allocate_root()) CTraversalTask_PartitionTreeNode_Parallel<CLambdaFun>(this, p_lambda)));
}


/*
 * SERIAL
 */
template <
	typename CLambdaFun
>
class CTraversalTask_PartitionTreeNode_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_PartitionTreeNode_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		if (this_node->first_child_node)
			CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>(this_node->first_child_node, lambda).execute();

		if (this_node->second_child_node)
			CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>(this_node->second_child_node, lambda).execute();

		return 0;
	}
};



template <typename CLambdaFun>
void traverse_PartitionTreeNode_Serial(
		CLambdaFun p_lambda
)
{
	CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>(this, p_lambda).execute();
}


/*
 * serial/parallel
 */
template <bool parallelProcessing, typename CLambdaFun>
void traverse_PartitionTreeNode(
		CLambdaFun p_lambda
)
{
	if (parallelProcessing)
		traverse_PartitionTreeNode_Parallel<CLambdaFun>(p_lambda);
	else
		traverse_PartitionTreeNode_Serial<CLambdaFun>(p_lambda);
}


/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE) MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node->cPartition_TreeNode);
			return 0;
		}

		int count = 1;
		tbb::task_list list;

		if (this_node->first_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes));
		}

		if (this_node->second_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes));
		}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		if (this_node->cPartition_TreeNode)
			lambda_midnodes(this_node->cPartition_TreeNode);

		return 0;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes));
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}

	void execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node->cPartition_TreeNode);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes).execute();

		if (this_node->second_child_node)
			CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes).execute();

		if (this_node->cPartition_TreeNode)
			lambda_midnodes(this_node->cPartition_TreeNode);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}



/*
 * serial/parallel
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		traverse_PartitionTreeNode_MidAndLeafNodes_Parallel(p_lambda_leaves, p_lambda_midnodes);
	else
		traverse_PartitionTreeNode_MidAndLeafNodes_Serial(p_lambda_leaves, p_lambda_midnodes);
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		int count = 1;
		tbb::task_list list;

		if (this_node->first_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes));
		}

		if (this_node->second_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes));
		}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		lambda_midnodes(this_node);

		return 0;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes));
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}

	void execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes).execute();

//		if (this_node->cPartition_TreeNode)
		lambda_midnodes(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}



/*
 * serial/parallel
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		traverse_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
	else
		traverse_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, genericTreeDepth);
			return 0;
		}

		genericTreeDepth++;

		int count = 1;
		tbb::task_list list;

		if (this_node->first_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth));
		}

		if (this_node->second_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth));
		}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		lambda_midnodes(this_node, genericTreeDepth-1);

		return 0;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes, 0));
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
	}

	void execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, genericTreeDepth);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();

//		if (this_node->cPartition_TreeNode)
		lambda_midnodes(this_node, genericTreeDepth);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes, 0).execute();
}



/*
 * serial/parallel
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		traverse_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
	else
		traverse_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
}


/****************************************************************************
 * PARTITION TREE NODE (WITH REDUCE)
 ****************************************************************************/
/*
 * parallel
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_PartitionTreeNode_Reduce_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_PartitionTreeNode_Reduce_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			TReduceValue (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b),
			TReduceValue *p_rootReduceValue
	)	:
		this_node(p_this_node),
		lambda(p_lambda),
		reduceOperator(p_reduceOperator),
		rootReduceValue(p_rootReduceValue),
		reduceValueFirst(0),
		reduceValueSecond(0)
	{
	}

	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			*rootReduceValue = lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */

		int count = 1;
		tbb::task_list list;

		if (this_node->first_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst));
		}

		if (this_node->second_child_node)
		{
			count++;
			list.push_back(*new( allocate_child()) CTraversalTask_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond));
		}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		/**
		 * REDUCTION
		 */
		*rootReduceValue = reduceOperator(reduceValueFirst, reduceValueSecond);

		return 0;
	}
};

template <typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce_Parallel(
		CLambdaFun p_lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	TReduceValue reduceValue;
	tbb::task::spawn_root_and_wait((*new(tbb::task::allocate_root()) CTraversalTask_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this, p_lambda, reduceOperator, &reduceValue)));
	return reduceValue;
}


/*
 * serial
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_PartitionTreeNode_Reduce_Serial	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_PartitionTreeNode_Reduce_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			TReduceValue (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b),
			TReduceValue *p_rootReduceValue
	)	:
		this_node(p_this_node),
		lambda(p_lambda),
		reduceOperator(p_reduceOperator),
		rootReduceValue(p_rootReduceValue),
		reduceValueFirst(0),
		reduceValueSecond(0)
	{
	}

	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			*rootReduceValue = lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */

		if (this_node->first_child_node)
			CTraversalTask_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst).execute();

		if (this_node->second_child_node)
			CTraversalTask_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond).execute();

		/**
		 * REDUCTION
		 */
		*rootReduceValue = reduceOperator(reduceValueFirst, reduceValueSecond);

		return 0;
	}
};

template <typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce_Serial(
		CLambdaFun lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	TReduceValue reduceValue;
	CTraversalTask_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this, lambda, reduceOperator, &reduceValue).execute();
	return reduceValue;
}


/*
 * serial/parallel
 */
template <bool parallelProcessing, typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce(
		CLambdaFun lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	if (parallelProcessing)
		return traverse_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(lambda, reduceOperator);
	else
		return traverse_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(lambda, reduceOperator);
}




/****************************************************************************
 * SIMULATION SUB-PARTITION HANDLER (NO REDUCE)
 ****************************************************************************/

/**
 * parallel traversal
 */
template <
	typename CLambdaFun
>
class CTraversalTask_SimulationPartitionHandler_Parallel : public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_SimulationPartitionHandler_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{	}

	task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node->cPartition_TreeNode->cSimulation_SubPartitionHandler);
			return 0;
		}

			int count = 1;
			tbb::task_list list;

			if (this_node->first_child_node)
			{
				count++;
				list.push_back(*new( allocate_child()) CTraversalTask_SimulationPartitionHandler_Parallel(this_node->first_child_node, lambda));
			}

			if (this_node->second_child_node)
			{
				count++;
				list.push_back(*new( allocate_child()) CTraversalTask_SimulationPartitionHandler_Parallel(this_node->second_child_node, lambda));
			}

		set_ref_count(count);
		spawn_and_wait_for_all(list);

		return 0;
	}
};


template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Parallel(
		CLambdaFun p_lambda
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_SimulationPartitionHandler_Parallel<CLambdaFun>(this, p_lambda));
}



/**
 * serial traversal
 */
template <
	typename CLambdaFun
>
class CTraversalTask_SimulationPartitionHandler_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_SimulationPartitionHandler_Serial(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun i_lambda_function
	)	:
		this_node(i_this_node),
		lambda(i_lambda_function)
	{	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node->cPartition_TreeNode->cSimulation_SubPartitionHandler);
			return 0;
		}

		if (this_node->first_child_node)
			CTraversalTask_SimulationPartitionHandler_Serial<CLambdaFun>(this_node->first_child_node, lambda).execute();

		if (this_node->second_child_node)
			CTraversalTask_SimulationPartitionHandler_Serial<CLambdaFun>(this_node->second_child_node, lambda).execute();

		return 0;
	}
};


template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Serial(
		CLambdaFun p_lambda
)
{
	CTraversalTask_SimulationPartitionHandler_Serial<CLambdaFun>(this, p_lambda).execute();
}



/*
 * serial/parallel
 */
template <bool parallelProcessing, class CLambdaFun>
void traverse_SimulationPartitionHandler(
		CLambdaFun p_lambda
)
{
	if (parallelProcessing)
		traverse_SimulationPartitionHandler_Parallel(p_lambda);
	else
		traverse_SimulationPartitionHandler_Serial(p_lambda);
}
#endif
