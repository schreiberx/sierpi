/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTraversator_Setup2Pass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


TRecursiveMethod sfcRecursiveMethod_Forward;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory triangleFactory;


/**
 * setup the initial partition traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory	///< triangle factory to find the first method
)
{
	triangleFactory = p_triangleFactory;

	sfcRecursiveMethod_Forward = getSFCMethod(triangleFactory);
}


/**
 * setup the parameters with the one by the parent partition
 */
void setup_Partition(
		TThisClass &p_parent,					///< information of parent traversator
		CTriangle_Factory &i_triangleFactory	///< triangle factory to find the first method
)
{
	triangleFactory = i_triangleFactory;

	// make sure that this is really a root node
	assert(triangleFactory.partitionTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);


	setup_sfcMethods(triangleFactory);

	kernelClass.setup_WithKernel(p_parent.kernelClass);
}
