SCONS_OPTS:=-Q -j 8

all:	gnu	intel	serial


tests:	gnu intel iomp fancy fancy_icc gnu_serial_regular

# make targets for pproc servers
pproc:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=off --mode=release

dpproc:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=off --mode=debug

iomp:
	# version with iomp
	scons $(SCONS_OPTS) --threading=iomp --cppcompiler=gnu --gui=off --mode=release

omp:
	# version without iomp
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=release


dfancy:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=on --mode=debug

dfancy_intel:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=on --mode=debug
        

fancy:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=on --mode=release

fancy_intel:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=on --mode=release


gnu:
	scons $(SCONS_OPTS) --threading=off --cppcompiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --cppcompiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=on --mode=release


#
# SERIAL
#
serial:	gnu_serial	intel_serial

gnu_serial_gui:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=gnu --gui=on --mode=release

gnu_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=gnu --gui=on --mode=release

gnu_serial_debug:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=gnu --gui=on --mode=debug


intel_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=intel --gui=on --mode=release


#
# SERIAL REGULAR
#
serial_regular:	gnu_serial_regular_table intel_serial_regular_table gnu_serial_regular_recursive intel_serial_regular_recursive

gnu_serial_regular_table:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=off --mode=release --serial_regular_type=table
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=on --mode=release --serial_regular_type=table

gnu_serial_regular_recursive:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=off --mode=release --serial_regular_type=recursive
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=on --mode=release --serial_regular_type=recursive

intel_serial_regular_table:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=intel --gui=off --mode=release --serial_regular_type=table
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=intel --gui=on --mode=release --serial_regular_type=table

intel_serial_regular_recursive:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=intel --gui=off --mode=release --serial_regular_type=recursive
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=intel --gui=on --mode=release --serial_regular_type=recursive



#
# TEST
#
test_release:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=on --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=off --mode=release

test_debug:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=on --mode=debug
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --cppcompiler=gnu --gui=off --mode=debug

gnu_r:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=gnu --gui=off --mode=release

intel_r:
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=off --mode=release

intel:
	# OpenMP
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=off --mode=release
#	scons $(SCONS_OPTS) --threading=off --cppcompiler=intel --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --cppcompiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --cppcompiler=intel --gui=on --mode=release

tbb:
	# TBB
	scons $(SCONS_OPTS) --threading=tbb --cppcompiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=tbb --cppcompiler=intel --gui=on --mode=release


serial_intel:
	# serial
	scons $(SCONS_OPTS) --simulation=tsunami_serial --cppcompiler=intel --gui=off --mode=release



clean_gnu:
	rm -rf build/build_sierpi_gnu_*

clean_intel:
	rm -rf build/build_sierpi_intel_*

clean: clean_gnu clean_intel
	rm -f build/sierpi*
