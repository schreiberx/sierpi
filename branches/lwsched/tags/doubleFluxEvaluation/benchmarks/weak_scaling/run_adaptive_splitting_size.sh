#! /bin/bash

. ../tools.sh

# adaptive splitting size
PARAMS="-u 10 -U 5"

# set timesteps
SINGLE_TIMESTEPS=50

# override CPU list
THREADSLIST="1 2 4 8 12"
DEPTH[1]=0
DEPTH[2]=1
DEPTH[4]=2
DEPTH[8]=3

# WARNING: we would need 16 cores for a depth of 4!
DEPTH[12]=3


START_DEPTHS="12 14 16 18 20 22"
for START_DEPTH in $START_DEPTHS; do

	echo ""
	echo "Start depth: $START_DEPTH"
	echo "Problem size with different number of CPUs using function optimization"
	echo ""
	echo "THREADS	INITIAL_DEPTH	MTPS	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
	for THREADS in $THREADSLIST; do
		DEPTH_DISPLACEMENT=${DEPTH[$THREADS]}
		DEPTH=$((START_DEPTH+DEPTH_DISPLACEMENT))
		PARAMS_=" -d $DEPTH -n $THREADS $PARAMS $@"
		PARAMS_+=" -v 5 -t $((SINGLE_TIMESTEPS*THREADS))"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT="`$EXEC_CMD`"

		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo "\$$VARNAME")
		SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

		AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
		AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
		TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
		TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
		SECONDS_FOR_SIMULATION=`getValueFromString "$OUTPUT" "SFS"`

		echo "$THREADS	$DEPTH	$MTPS	$SPEEDUP_PER	$TIMESTEPS	$TIMESTEP_SIZE	$SECONDS_FOR_SIMULATION	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
	done
done
