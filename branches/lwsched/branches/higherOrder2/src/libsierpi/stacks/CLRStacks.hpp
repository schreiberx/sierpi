/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CLRStacks.hpp
 *
 *  Created on: Jan 11, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CLRSTACKS_HPP_
#define CLRSTACKS_HPP_

#include "CStack.hpp"

/**
 * \brief class to handle the 2 stacks simultaneously.
 *
 * this is usually the case when moving data from one stack to another (e. g. the
 * structure stack).
 *
 * since one stack is usually written during a forward traversal and the other one
 * in a backward traversal, this class is called FB to account for 'forward/backward'.
 */
template <typename T>
class CLRStacks
{
public:
	CStack<T> left;
	CStack<T> right;

	CLRStacks()
	{
	}

	CLRStacks(size_t size)	:
		left(size),
		right(size)
	{
	}

	/**
	 * resize both stacks to the given size
	 */
	inline void resize(
			size_t i_size	///< new maximum number of elements which can be stored on the stack
	)
	{
		left.resize(i_size);
		right.resize(i_size);
	}

	/**
	 * clear stacks
	 */
	inline void clear()
	{
		left.clear();
		right.clear();
	}

	/**
	 * swap left and right stack
	 */
	inline void swap()
	{
		left.swap(right);
	}

	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, CLRStacks<T> &stacks)
	{
		co << stacks.left << std::endl;
		co << stacks.right << std::endl;

		return co;
	}

};

#endif /* CLRSTACKS_HPP_ */
