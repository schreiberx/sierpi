/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CStackAccessorMethods.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */
#ifndef CSTACK_ACCESSOR_METHODS_HPP_
#define CSTACK_ACCESSOR_METHODS_HPP_

#include "config.h"

/**
 * \brief this methods are used to transparently access the edge data stacks of adjacent partitions
 */
template <typename CPartition_TreeNode_, typename CTsunamiEdgeData>
class CStackAccessorMethodsTsunamiEdgeData
{
public:
	inline static CStack<CTsunamiEdgeData>* leftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->edge_data_comm_edge_stacks.left);
	}

	inline static CStack<CTsunamiEdgeData>* rightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->edge_data_comm_edge_stacks.right);
	}

	inline static CStack<CTsunamiEdgeData>* exchangeLeftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->edge_data_comm_exchange_edge_stacks.left);
	}

	inline static CStack<CTsunamiEdgeData>* exchangeRightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->edge_data_comm_exchange_edge_stacks.right);
	}
};


/**
 * \brief this methods are used to transparently access the vertex data stacks of adjacent partitions
 */
template <typename CPartition_TreeNode_, typename CTsunamiVertexData>
class CStackAccessorMethodsTsunamiVertexData
{
public:
	inline static CStack<CTsunamiVertexData>* leftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->vertex_data_comm_left_edge_stack);
	}

	inline static CStack<CTsunamiVertexData>* rightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->vertex_data_comm_right_edge_stack);
	}

	inline static CStack<CTsunamiVertexData>* exchangeLeftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->vertex_data_comm_exchange_left_edge_stack);
	}

	inline static CStack<CTsunamiVertexData>* exchangeRightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->vertex_data_comm_exchange_right_edge_stack);
	}
};



/**
 * \brief this methods are used to transparently access the adaptivity stacks of adjacent partitions
 */
template <typename CPartition_TreeNode_>
class CStackAccessorMethodsAdaptivityEdgeData
{
public:
	inline static CStack<char>* leftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->adaptive_comm_edge_stacks.left);
	}

	inline static CStack<char>* rightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->adaptive_comm_edge_stacks.right);
	}

	inline static CStack<char>* exchangeLeftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->adaptive_comm_exchange_edge_stacks.left);
	}

	inline static CStack<char>* exchangeRightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cStacks->adaptive_comm_exchange_edge_stacks.right);
	}
};



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
/**
 * \brief this methods are used to transparently access the validation stacks of adjacent partitions
 */
template <typename CPartition_TreeNode_>
class CStackAccessorMethodsValidationEdgeData
{
public:
	inline static CStack<CValEdgeData>* leftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cValidationStacks->edge_data_comm_left_edge_stack);
	}

	inline static CStack<CValEdgeData>* rightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cValidationStacks->edge_data_comm_right_edge_stack);
	}

	inline static CStack<CValEdgeData>* exchangeLeftStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cValidationStacks->edge_data_comm_exchange_left_edge_stack);
	}

	inline static CStack<CValEdgeData>* exchangeRightStackAccessor(const CPartition_TreeNode_ *node)
	{
		return &(node->cValidationStacks->edge_data_comm_exchange_right_edge_stack);
	}
};
#endif


#endif /* CSTACKACCESSORMETHODS_HPP_ */
