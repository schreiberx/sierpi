/*
 * CAsagi.cpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#include "CAsagi.hpp"
#include <cassert>



CAsagi::CAsagi(
		const char *i_filename_bathymetry,
		const char *i_filename_displacements
)
{
	setup_ok = false;

	/*
	 * BATHYMETRY
	 */
	bathymetry_grid = asagi::Grid::create(asagi::Grid::FLOAT);

	if (bathymetry_grid->open(i_filename_bathymetry) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open bathymetry file '" << i_filename_bathymetry << std::endl;
		exit(-1);
	}

	bathymetry_min_x = bathymetry_grid->getXMin();
	bathymetry_min_y = bathymetry_grid->getYMin();

	bathymetry_max_x = bathymetry_grid->getXMax();
	bathymetry_max_y = bathymetry_grid->getYMax();

	bathymetry_size_x = bathymetry_max_x - bathymetry_min_x;
	bathymetry_size_y = bathymetry_max_y - bathymetry_min_y;

	/*
	 * domain size reduced to quad
	 */
	double min_size = CMath::min(bathymetry_size_x, bathymetry_size_y);

	double padding = min_size*0.001;
	double new_size = min_size - 2.0*padding;	// apply padding on both sides

	bathymetry_size_x = new_size;
	bathymetry_size_y = new_size;

	bathymetry_min_x += padding;
	bathymetry_min_y += padding;

	bathymetry_max_x = bathymetry_min_x + bathymetry_size_x;
	bathymetry_max_y = bathymetry_min_y + bathymetry_size_y;

	/**
	 * DISPLACEMENTS
	 */
	displacements_grid = asagi::Grid::create(asagi::Grid::FLOAT);

	if (displacements_grid->open(i_filename_displacements) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open file '" << i_filename_displacements << std::endl;
		exit(-1);
	}

	displacements_min_x = displacements_grid->getXMin();
	displacements_min_y = displacements_grid->getYMin();

	displacements_max_x = displacements_grid->getXMax();
	displacements_max_y = displacements_grid->getYMax();

	displacements_size_x = displacements_max_x - displacements_min_x;
	displacements_size_y = displacements_max_y - displacements_min_y;

	setup_ok = true;
}



CAsagi::~CAsagi()
{
	delete bathymetry_grid;
	delete displacements_grid;
}


void CAsagi::outputVerboseInformation()
{
	std::cout << "ASAGI:" << std::endl;
	std::cout << " + Bathymetry data-window: (" << bathymetry_grid->getXMin() << ", " << bathymetry_grid->getYMin() << 	") x (" << bathymetry_grid->getXMax() << ", " << bathymetry_grid->getYMax() << ")" << std::endl;
	std::cout << " + Bathymetry data-size: (" << (bathymetry_grid->getXMax() - bathymetry_grid->getXMin()) << ", " 	<< (bathymetry_grid->getYMax() - bathymetry_grid->getYMin())<< ")" << std::endl;

	std::cout << " + Bathymetry start: (" << bathymetry_min_x << ", " << bathymetry_min_y << ")" << std::endl;
	std::cout << " + Bathymetry size: (" << bathymetry_size_x << ", " << bathymetry_size_y << ")" << std::endl;

	std::cout << " + Displacement window: (" << displacements_grid->getXMin() << ", " << displacements_grid->getYMin() << 	") x (" << displacements_grid->getXMax() << ", " << displacements_grid->getYMax() << ")" << std::endl;
	std::cout << " + Displacement size: (" << (displacements_grid->getXMax() - displacements_grid->getXMin()) << ", " 	<< (displacements_grid->getYMax() - displacements_grid->getYMin())<< ")" << std::endl;
}



CTsunamiSimulationTypes::TVertexScalar CAsagi::getBathymetry(
		T i_mx,
		T i_my,
		T i_depth
	)
{
	T mx = (i_mx+(T)1.0)*(T)0.5;
	T my = (i_my+(T)1.0)*(T)0.5;

	return bathymetry_grid->getFloat2D(
			mx * bathymetry_max_x - bathymetry_min_x*(mx - (T)1.0),
			my * bathymetry_max_y - bathymetry_min_y*(my - (T)1.0),
			0
		);
}



CTsunamiSimulationTypes::TVertexScalar CAsagi::getDisplacement(
		T i_mx,
		T i_my,
		T i_depth
	)
{
	T mx = (i_mx+1.0)*0.5;
	T my = (i_my+1.0)*0.5;

	mx = mx * bathymetry_max_x - bathymetry_min_x*(mx - 1.0);
	my = my * bathymetry_max_y - bathymetry_min_y*(my - 1.0);

	if (mx < displacements_min_x)
		return 0;

	if (mx > displacements_max_x)
		return 0;

	if (my < displacements_min_y)
		return 0;

	if (my > displacements_max_y)
		return 0;

	return	displacements_grid->getFloat2D(mx, my, 0);
}
