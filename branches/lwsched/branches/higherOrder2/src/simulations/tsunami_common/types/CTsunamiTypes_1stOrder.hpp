/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_TYPES_1ST_ORDER_HPP
#define CTSUNAMI_TYPES_1ST_ORDER_HPP

#include "CTsunamiTypes_Validate.hpp"


class CTsunamiEdgeData
{
public:
	TTsunamiDataScalar h;	// height
	TTsunamiDataScalar qx;	// x-component of velocity
	TTsunamiDataScalar qy;	// y-component of velocity

	union
	{
		TTsunamiDataScalar b;	// bathymetry
		TTsunamiDataScalar max_wave_speed;	// max wave speed for flux computations
	};

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiEdgeData &d)
	{
		return os << d.h << " " << d.qx << " " << d.qy

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			<< d.validation
#endif
		;
	}
};



class CTsunamiElementData
{
public:
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData right_edge;
	CTsunamiEdgeData left_edge;

	CTsunamiEdgeData support_points[3];

	TTsunamiDataScalar cfl_domain_size_div_max_wave_speed;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
	#if 0
		unsigned refine:1;
		unsigned coarsen:1;
	#else
		bool refine;
		bool coarsen;
	#endif
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_ElementData validation;
#endif


	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiElementData &d)
	{
		os << d.hyp_edge << std::endl;
		os << d.right_edge << std::endl;
		os << d.left_edge << std::endl;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		std::cout << d.validation << std::endl;
#endif
		return os;
	}
};



#endif
