/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CKERNEL_SETUP_ELEMENT_DATA_TSUNAMI_HPP_
#define CKERNEL_SETUP_ELEMENT_DATA_TSUNAMI_HPP_


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CSetup_ElementData_0thOrder.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_ElementData_0thOrder<CTsunamiElementData, CTsunamiSimulationStacks> CSetup_ElementData;
	}
}

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1

#include "CSetup_ElementData_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_ElementData_1stOrder<CTsunamiElementData, CTsunamiSimulationStacks> CSetup_ElementData;
	}
}

#else

#include "CSetup_ElementData_2ndOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_ElementData_2ndOrder<CTsunamiElementData, CTsunamiSimulationStacks> CSetup_ElementData;
	}
}

#endif

#endif
