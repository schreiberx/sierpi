/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData_Depth.hpp"
#include "../common/CAdaptivity_0thOrder.hpp"
#include "../../CTsunamiSimulationDataSets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_ElementData_0thOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_ElementData_Depth<CSetup_ElementData_0thOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiDataScalar T;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;


	/*
	 * callback for terrain data
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;

	bool initial_grid_setup;



	CSetup_ElementData_0thOrder()	:
		cSimulationDataSets(nullptr),
		initial_grid_setup(true)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void elementAction(
			TTsunamiVertexScalar vertex_left_x,		TTsunamiVertexScalar vertex_left_y,
			TTsunamiVertexScalar vertex_right_x,	TTsunamiVertexScalar vertex_right_y,
			TTsunamiVertexScalar vertex_top_x,		TTsunamiVertexScalar vertex_top_y,
			int i_depth,

			CTsunamiElementData *io_elementData
	)
	{
		TVertexScalar mx, my;

		CAdaptivity_0thOrder::computeAdaptiveSamplingPoint(
			vertex_left_x, vertex_left_y,
			vertex_right_x, vertex_right_y,
			vertex_top_x, vertex_top_y,
			&mx, &my
		);

		io_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(mx, my, i_depth);

		if (cSimulationDataSets->getUseMaximumFunctionForSurfaceHeightSetup() && !initial_grid_setup)
		{
			// fix height with bathymetry displacement
			io_elementData->dofs.h = CMath::max(io_elementData->dofs.h, -io_elementData->dofs.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth));

			if (io_elementData->dofs.h < 0)
				io_elementData->dofs.h = 0;
		}
		else
		{
			io_elementData->dofs.h = -io_elementData->dofs.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth);

			if (io_elementData->dofs.h < 0)
				io_elementData->dofs.h = 0;

		}

		io_elementData->cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void setup_Parameters(
			CTsunamiSimulationDataSets *i_cSimulationDataSets,
			bool i_initial_grid_setup
	)
	{
		cSimulationDataSets = i_cSimulationDataSets;
		initial_grid_setup = i_initial_grid_setup;
	}



	void setup_WithKernel(
			CSetup_ElementData_0thOrder<TElementData, TSimulationStacks> &parent
	)
	{
		cSimulationDataSets = parent.cSimulationDataSets;
		initial_grid_setup = parent.initial_grid_setup;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */

