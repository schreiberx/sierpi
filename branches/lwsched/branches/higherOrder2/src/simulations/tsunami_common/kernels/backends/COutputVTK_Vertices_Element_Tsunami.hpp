/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COUTPUTVTK_VERTICES_ELEMENT_TSUNAMI_HPP_
#define COUTPUTVTK_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "lib/CLittleBigEndian.hpp"


namespace sierpi
{
namespace kernels
{


#define VISUALIZATION_SIMPLE						1

// TODO: code has to be fixed with bathymetry for this backend
#define VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS	0

#define VISUALIZATION_ALIGNED_TRIANGLES				0


template <typename p_CSimulationStacks, int i_visualizationType>
class COutputVTK_Vertices_Element_Tsunami
{
public:
	typedef typename p_CSimulationStacks::TElementData	TElementData;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_ElementData<COutputVTK_Vertices_Element_Tsunami<p_CSimulationStacks, i_visualizationType>, p_CSimulationStacks > TRAV;

	std::ofstream *vtk_file_output_stream;

	TVertexScalar domain_unit_size_x;
	TVertexScalar domain_unit_size_y;
	TVertexScalar domain_origin_x;
	TVertexScalar domain_origin_y;

	bool output_binary;
	bool swap_little_to_big_endian;

private:
	TVertexScalar *last_triangle;
	size_t max_triangles;


	inline void writeScalar(
			const TVertexScalar i_scalar
	)
	{
		if (output_binary)
		{
			if (swap_little_to_big_endian)
				CLittleBigEndian::swapBytes(i_scalar);

			vtk_file_output_stream->write((char*)&i_scalar, sizeof(TVertexScalar));
		}
		else
		{
			(*vtk_file_output_stream) << i_scalar << std::endl;
		}
	}


public:
	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TElementData *i_ElementData
	)
	{
#define fixHeight(h)	(h)

#if VISUALIZATION_SIMPLE

	if (i_visualizationType == 0)
	{
		if (output_binary)
		{
			TVertexScalar v[9] = {
				(domain_origin_x + (vx1*domain_unit_size_x)),
				(domain_origin_y + (vy1*domain_unit_size_y)),
				0,
				(domain_origin_x + (vx2*domain_unit_size_x)),
				(domain_origin_y + (vy2*domain_unit_size_y)),
				0,
				(domain_origin_x + (vx3*domain_unit_size_x)),
				(domain_origin_y + (vy3*domain_unit_size_y)),
				0
			};

			if (swap_little_to_big_endian)
				for (int i = 0; i < 9; i++)
					CLittleBigEndian::swapBytes(*(v+i));

			vtk_file_output_stream->write((char*)v, sizeof(TVertexScalar)*9);
		}
		else
		{
			(*vtk_file_output_stream)
					<< (domain_origin_x + (vx1*domain_unit_size_x)) << " "
					<< (domain_origin_y + (vy1*domain_unit_size_y)) << " "
					<< 0 << std::endl;

			(*vtk_file_output_stream)
					<< (domain_origin_x + (vx2*domain_unit_size_x)) << " "
					<< (domain_origin_y + (vy2*domain_unit_size_y)) << " "
					<< 0 << std::endl;

			(*vtk_file_output_stream)
					<< (domain_origin_x + (vx3*domain_unit_size_x)) << " "
					<< (domain_origin_y + (vy3*domain_unit_size_y)) << " "
					<< 0 << std::endl;
		}
	}
	else if (i_visualizationType == 1)
	{
		/*
		 * SURFACE coordinates
		 */
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		TVertexScalar h = i_ElementData->dofs.h;
#else
		TVertexScalar h = (1.0/3.0)*(i_ElementData->hyp_edge.b + i_ElementData->left_edge.b + i_ElementData->right_edge.b + i_ElementData->hyp_edge.h + i_ElementData->left_edge.h + i_ElementData->right_edge.h);
#endif

		writeScalar(h);
	}
	else if (i_visualizationType == 2)
	{
	/*
	 * MOMENTUM HU
	 */
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		TVertexScalar hu = i_ElementData->dofs.qx;
#else
		TVertexScalar hu = (1.0/3.0)*(i_ElementData->hyp_edge.qx + i_ElementData->left_edge.qx + i_ElementData->right_edge.qx);
#endif

		writeScalar(hu);
	}
	else if (i_visualizationType == 3)
	{
	/*
	 * MOMENTUM HV
	 */
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		TVertexScalar hv = i_ElementData->dofs.qy;
#else
		TVertexScalar hv = (1.0/3.0)*(i_ElementData->hyp_edge.qy + i_ElementData->left_edge.qy + i_ElementData->right_edge.qy);
#endif

		writeScalar(hv);
	}
	else if (i_visualizationType == 4)
	{
	/*
	 * BATHYMETRY
	 */
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		TVertexScalar b = i_ElementData->dofs.b;
#else
		TVertexScalar b = (1.0/3.0)*(i_ElementData->hyp_edge.b + i_ElementData->left_edge.b + i_ElementData->right_edge.b);
#endif

		writeScalar(b);
	}
	else if (i_visualizationType == 5)
	{
		/*
		 * cfl_domain_size_div_max_wave_speed
		 */
		TVertexScalar cfl_domain_size_div_max_wave_speed;
		if (i_ElementData->cfl_domain_size_div_max_wave_speed > 100000000000.0)
		{
			cfl_domain_size_div_max_wave_speed = 0;
		}
		else
		{
			cfl_domain_size_div_max_wave_speed = i_ElementData->cfl_domain_size_div_max_wave_speed;
		}

		writeScalar(cfl_domain_size_div_max_wave_speed);
	}


#endif

#if VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS

		TVertexScalar hyp_x = (vx1+vx2)*0.5;
		TVertexScalar hyp_y = (vy1+vy2)*0.5;

		TVertexScalar right_edge_x = (vx2+vx3)*0.5;
		TVertexScalar right_edge_y = (vy2+vy3)*0.5;

		TVertexScalar left_edge_x = (vx3+vx1)*0.5;
		TVertexScalar left_edge_y = (vy3+vy1)*0.5;

		(*vtk_file_output_stream)
				<<	hyp_x << " "
				<< i_ElementData->hyp_edge.h << " "
				<< -hyp_y << std::endl;

		(*vtk_file_output_stream)
				<< left_edge_x << " "
				<< i_ElementData->left_edge.h << " "
				<< -left_edge_y << std::endl;

		(*vtk_file_output_stream)
				<< right_edge_x << " "
				<< i_ElementData->right_edge.h << " "
				<< -right_edge_y << std::endl;
#endif

#if VISUALIZATION_ALIGNED_TRIANGLES
		TVertexScalar hyp_edge_x = (vx1+vx2)*0.5;
		TVertexScalar hyp_edge_y = (vy1+vy2)*0.5;

		TVertexScalar right_edge_x = (vx2+vx3)*0.5;
		TVertexScalar right_edge_y = (vy2+vy3)*0.5;

		TVertexScalar left_edge_x = (vx3+vx1)*0.5;
		TVertexScalar left_edge_y = (vy3+vy1)*0.5;

		TVertexScalar hyp_dx = right_edge_x - left_edge_x;
		TVertexScalar hyp_dy = right_edge_y - left_edge_y;
		TVertexScalar hyp_dh = i_ElementData->right_edge.h - i_ElementData->left_edge.h;

		TVertexScalar left_dx = right_edge_x - hyp_edge_x;
		TVertexScalar left_dy = right_edge_y - hyp_edge_y;
		TVertexScalar left_dh = i_ElementData->right_edge.h - i_ElementData->hyp_edge.h;

		(*vtk_file_output_stream)
					<< (hyp_edge_x-hyp_dx) << " "
					<< (i_ElementData->hyp_edge.h-hyp_dh) << " "
					<< -(hyp_edge_y-hyp_dy) << std::endl;

		(*vtk_file_output_stream)
					<< (hyp_edge_x+hyp_dx) << " "
					<< (i_ElementData->hyp_edge.h+hyp_dh) << " "
					<< -(hyp_edge_y+hyp_dy) << std::endl;

		(*vtk_file_output_stream)
					<< (left_edge_x+left_dx) << " "
					<< (i_ElementData->left_edge.h+left_dh) << " "
					<< -(left_edge_y+left_dy) << std::endl;
#endif
	}
#undef fixHeight

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(
			std::ofstream *io_vtk_file_output_stream,
			TVertexScalar i_domain_origin_x,
			TVertexScalar i_domain_origin_y,
			TVertexScalar i_domain_unit_size_x,
			TVertexScalar i_domain_unit_size_y,
			bool i_output_ascii,
			bool i_swap_little_to_big_endian
	)
	{
		vtk_file_output_stream = io_vtk_file_output_stream;
		domain_unit_size_x = i_domain_unit_size_x;
		domain_unit_size_y = i_domain_unit_size_y;
		domain_origin_x = i_domain_origin_x;
		domain_origin_y = i_domain_origin_y;
		output_binary = i_output_ascii;
		swap_little_to_big_endian = i_swap_little_to_big_endian;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputVTK_Vertices_Element_Tsunami &parent_kernel
	)
	{
	}


	inline void setup_WithParameters(
			std::ofstream *p_vtk_file_output_stream
		)
	{
	}
};


#undef VISUALIZATION_SIMPLE
#undef VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS
#undef VISUALIZATION_ALIGNED_TRIANGLES

}
}

#endif
;
