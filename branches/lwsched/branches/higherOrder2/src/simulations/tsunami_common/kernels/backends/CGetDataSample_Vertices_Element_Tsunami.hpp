/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CGETDATASAMPLE_VERTICES_ELEMENT_TSUNAMI_HPP_
#define CGETDATASAMPLE_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{



template <typename p_CSimulationStacks>
class CGetDataSample_Vertices_Element_Tsunami
{
public:
	typedef typename p_CSimulationStacks::TElementData	TElementData;

	typedef float TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_ElementData<CGetDataSample_Vertices_Element_Tsunami<p_CSimulationStacks>, p_CSimulationStacks > TRAV;

	double *result;
	const char *sample_information;
	double sample_pos_x;
	double sample_pos_y;

	TVertexScalar domain_unit_size_x;
	TVertexScalar domain_unit_size_y;
	TVertexScalar domain_origin_x;
	TVertexScalar domain_origin_y;



private:
	TVertexScalar *last_triangle;
	size_t max_triangles;

public:
	inline void elementAction(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			TElementData *i_element_data
	)
	{
		TVertexScalar real_v0x = (domain_origin_x + (v0x*domain_unit_size_x));
		TVertexScalar real_v0y = (domain_origin_y + (v0y*domain_unit_size_y));

		TVertexScalar real_v1x = (domain_origin_x + (v1x*domain_unit_size_x));
		TVertexScalar real_v1y = (domain_origin_y + (v1y*domain_unit_size_y));

		TVertexScalar real_v2x = (domain_origin_x + (v2x*domain_unit_size_x));
		TVertexScalar real_v2y = (domain_origin_y + (v2y*domain_unit_size_y));

		if (!CPointInTriangleTest<double>::test(
				real_v0x, real_v0y,
				real_v1x, real_v1y,
				real_v2x, real_v2y,
				sample_pos_x, sample_pos_y
			))
				return;

		if (strcmp(sample_information, "b+h"))
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

			*result = i_element_data->dofs.b + i_element_data->dofs.h;

#else

			std::cout << "unsupported so far!" << std::endl;
			assert(false);

#endif
			return;
		}

		std::cerr << "unknown sample information " << sample_information << std::endl;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(
			double *o_result,

			const char *i_sample_information,
			double i_sample_pos_x,
			double i_sample_pos_y,

			TVertexScalar i_domain_origin_x,
			TVertexScalar i_domain_origin_y,
			TVertexScalar i_domain_unit_size_x,
			TVertexScalar i_domain_unit_size_y
	)
	{
		result = o_result;

		sample_information = i_sample_information;
		sample_pos_x = i_sample_pos_x;
		sample_pos_y = i_sample_pos_y;

		domain_unit_size_x = i_domain_unit_size_x;
		domain_unit_size_y = i_domain_unit_size_y;
		domain_origin_x = i_domain_origin_x;
		domain_origin_y = i_domain_origin_y;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			CGetDataSample_Vertices_Element_Tsunami &parent_kernel
	)
	{
		assert(false);
	}
};


}
}

#endif
