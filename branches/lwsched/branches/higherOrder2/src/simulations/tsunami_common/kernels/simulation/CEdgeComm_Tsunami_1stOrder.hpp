/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_


#include "../../tsunami_config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
//#include "libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// flux projections
#include "CFluxProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (elementAction)
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Tsunami_1stOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;


	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<t_storeElementUpdatesOnly> > TRAV;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_1stOrder()
	{
		boundary_dirichlet.h = CMath::numeric_inf<T>();
		boundary_dirichlet.qx = CMath::numeric_inf<T>();
		boundary_dirichlet.qy = CMath::numeric_inf<T>();
	}


	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void computeElementDataRK2_1stStep(
			CTsunamiElementData &i_f_t0,
			CTsunamiElementData &i_f_slope_t0,
			CTsunamiElementData *o_f_t1_approx,
			T i_timestep_size
	)
	{
		o_f_t1_approx->left_edge.h		=	i_f_t0.left_edge.h	+ i_timestep_size*i_f_slope_t0.left_edge.h;
		o_f_t1_approx->left_edge.qx		=	i_f_t0.left_edge.qx	+ i_timestep_size*i_f_slope_t0.left_edge.qx;
		o_f_t1_approx->left_edge.qy		=	i_f_t0.left_edge.qy	+ i_timestep_size*i_f_slope_t0.left_edge.qy;
		o_f_t1_approx->left_edge.b		=	i_f_t0.left_edge.b;

		o_f_t1_approx->right_edge.h		=	i_f_t0.right_edge.h		+ i_timestep_size*i_f_slope_t0.right_edge.h;
		o_f_t1_approx->right_edge.qx	=	i_f_t0.right_edge.qx	+ i_timestep_size*i_f_slope_t0.right_edge.qx;
		o_f_t1_approx->right_edge.qy	=	i_f_t0.right_edge.qy	+ i_timestep_size*i_f_slope_t0.right_edge.qy;
		o_f_t1_approx->right_edge.b		=	i_f_t0.right_edge.b;

		o_f_t1_approx->hyp_edge.h		=	i_f_t0.hyp_edge.h	+ i_timestep_size*i_f_slope_t0.hyp_edge.h;
		o_f_t1_approx->hyp_edge.qx		=	i_f_t0.hyp_edge.qx	+ i_timestep_size*i_f_slope_t0.hyp_edge.qx;
		o_f_t1_approx->hyp_edge.qy		=	i_f_t0.hyp_edge.qy	+ i_timestep_size*i_f_slope_t0.hyp_edge.qy;
		o_f_t1_approx->hyp_edge.b		=	i_f_t0.hyp_edge.b;

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void computeElementDataRK2_2ndStep(
			CTsunamiElementData &i_f_slope_t0,
			CTsunamiElementData &i_f_slope_t1,
			CTsunamiElementData *io_f_t0_t1,
			T i_timestep_size
	)
	{
		io_f_t0_t1->left_edge.h		+= i_timestep_size*(T)0.5*(i_f_slope_t0.left_edge.h + i_f_slope_t1.left_edge.h);
		io_f_t0_t1->left_edge.qx	+= i_timestep_size*(T)0.5*(i_f_slope_t0.left_edge.qx + i_f_slope_t1.left_edge.qx);
		io_f_t0_t1->left_edge.qy	+= i_timestep_size*(T)0.5*(i_f_slope_t0.left_edge.qy + i_f_slope_t1.left_edge.qy);

		io_f_t0_t1->right_edge.h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.right_edge.h + i_f_slope_t1.right_edge.h);
		io_f_t0_t1->right_edge.qx	+= i_timestep_size*(T)0.5*(i_f_slope_t0.right_edge.qx + i_f_slope_t1.right_edge.qx);
		io_f_t0_t1->right_edge.qy	+= i_timestep_size*(T)0.5*(i_f_slope_t0.right_edge.qy + i_f_slope_t1.right_edge.qy);

		io_f_t0_t1->hyp_edge.h		+= i_timestep_size*(T)0.5*(i_f_slope_t0.hyp_edge.h + i_f_slope_t1.hyp_edge.h);
		io_f_t0_t1->hyp_edge.qx		+= i_timestep_size*(T)0.5*(i_f_slope_t0.hyp_edge.qx + i_f_slope_t1.hyp_edge.qx);
		io_f_t0_t1->hyp_edge.qy		+= i_timestep_size*(T)0.5*(i_f_slope_t0.hyp_edge.qy + i_f_slope_t1.hyp_edge.qy);

		// TODO: which CFL to use?
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}


	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_1stOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-partition
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-partition
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->hyp_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->right_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->left_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeBoundaryCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			*o_edge_comm_data = i_elementData->hyp_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->hyp_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			*o_edge_comm_data = i_elementData->hyp_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->hyp_edge.h;
			o_edge_comm_data->qx = -i_elementData->hyp_edge.qx;
			o_edge_comm_data->qy = -i_elementData->hyp_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_elementData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			*o_edge_comm_data = i_elementData->right_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->right_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			*o_edge_comm_data = i_elementData->right_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->right_edge.h;
			o_edge_comm_data->qx = -i_elementData->right_edge.qx;
			o_edge_comm_data->qy = -i_elementData->right_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_elementData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			*o_edge_comm_data = i_elementData->left_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->left_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			*o_edge_comm_data = i_elementData->left_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->left_edge.h;
			o_edge_comm_data->qx = -i_elementData->left_edge.qx;
			o_edge_comm_data->qy = -i_elementData->left_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_elementData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void elementAction(
				T i_hyp_normal_x, T i_hyp_normal_y,		///< normals for hypotenuse edge
				T i_right_normal_x, T i_right_normal_y,	///< normals for right edge
				T i_left_normal_x, T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
				int i_depth,

				CTsunamiElementData *io_elementData,	///< element data

				CTsunamiEdgeData *i_hyp_edge_flux,		///< incoming flux from hypotenuse
				CTsunamiEdgeData *i_right_edge_flux,	///< incoming flux from right edge
				CTsunamiEdgeData *i_left_edge_flux		///< incoming flux from left edge
		)
		{
	#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			i_hyp_edge_flux->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
			i_right_edge_flux->validation.testNormal(i_right_normal_x, i_right_normal_y);
			i_left_edge_flux->validation.testNormal(i_left_normal_x, i_left_normal_y);

			/*
			 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
			 */
			io_elementData->validation.testEdgeMidpoints(
					i_hyp_edge_flux->validation,
					i_right_edge_flux->validation,
					i_left_edge_flux->validation
				);
	#endif

			CFluxProjections::backprojectFromNormalSpace(i_hyp_edge_flux->qx, i_hyp_edge_flux->qy, i_hyp_normal_x, i_hyp_normal_y);
			CFluxProjections::backprojectFromNormalSpace(i_right_edge_flux->qx, i_right_edge_flux->qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::backprojectFromNormalSpace(i_left_edge_flux->qx, i_left_edge_flux->qy, i_left_normal_x, i_left_normal_y);
	#if 0
			std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << *i_hyp_edge_flux << std::endl;
			std::cout << *i_right_edge_flux << std::endl;
			std::cout << *i_left_edge_flux << std::endl;
			std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
	#endif
			computeAndUpdateCFLCondition3(
					i_hyp_edge_flux->max_wave_speed,
					i_right_edge_flux->max_wave_speed,
					i_left_edge_flux->max_wave_speed,
					i_depth,
					&(io_elementData->cfl_domain_size_div_max_wave_speed)
				);

			assert(timestep_size > 0);
			assert(gravitational_constant > 0);
			assert(cathetus_real_length > 0);

			if (CMath::isNan(io_elementData->hyp_edge.h + io_elementData->right_edge.h + io_elementData->left_edge.h))
			{
				instabilityDetected = true;
				if (!instabilityDetected)
					std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
				return;
			}

			T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
			T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*cathetus_real_length;

	#if 0
			TTsunamiDataScalar damping_per_second = (T)0.9999;
			TTsunamiDataScalar damping = (T)1.0-(((T)1.0-damping_per_second)*(timestep_size));

			damping = CMath::min(damping, (T)0.9999);
			damping = CMath::max(damping, (T)0.99);

			if (i != 0xfff)
			{
				std::cout << damping << std::endl;
				i = 0xfff;
			}

	//		std::cout << damping << std::endl;

	//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*delta_timestep/cat_length, (T)0.99);

	//		TTsunamiDataScalar damping = (T)1.0-(delta_timestep/cat_length);
	//		TTsunamiDataScalar damping = (T)1.0-(delta_timestep/cat_length);

			io_elementData->hyp_edge.qx *= damping;
			io_elementData->hyp_edge.qy *= damping;

			io_elementData->left_edge.qx *= damping;
			io_elementData->left_edge.qy *= damping;

			io_elementData->right_edge.qx *= damping;
			io_elementData->right_edge.qy *= damping;
	#endif

			// rotate flux
			CFluxProjections::projectToNormalSpaceAndInvert(i_hyp_edge_flux->qx, i_hyp_edge_flux->qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(i_right_edge_flux->qx, i_right_edge_flux->qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(i_left_edge_flux->qx, i_left_edge_flux->qy, i_right_normal_x, i_right_normal_y);

			// rotate element data
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->hyp_edge.qx, io_elementData->hyp_edge.qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->left_edge.qx, io_elementData->left_edge.qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->right_edge.qx, io_elementData->right_edge.qy, i_right_normal_x, i_right_normal_y);

			// rotate support point stuff
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->support_points[0].qx, io_elementData->support_points[0].qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->support_points[1].qx, io_elementData->support_points[1].qy, i_right_normal_x, i_right_normal_y);
			CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->support_points[2].qx, io_elementData->support_points[2].qy, i_right_normal_x, i_right_normal_y);

#define ORDER_ONE_METHOD 4

	#if ORDER_ONE_METHOD==0

			const int n=3;			// Number of basis functions (= number of support points)
			const T gravity = 9.81;					// Can remove this when globally controlled gravity settings are implemented
			const int _h=0, _qx=1, _qy=2;			// Indexer variables (properties)
			const int _left=0, _hyp=1, _right=2;	// Indexer variables (edges)

			CTsunamiEdgeData* origs = (CTsunamiEdgeData*)io_elementData;
			T result[n][3] = {{0,0,0},{0,0,0},{0,0,0}}; // nx3 matrix, columns being the support points, rows being the properties (h, qx, qy)

			// Helpers
			//T these change with basis functions
//			T pre = 6.*(1./(cat_length*cat_length));	// 6/c^2
//			T areapre = (1/3.)*cat_length;				// c/3
//			T areaweight = (1/2.)*cat_length*cat_length/n;
//			T negareapre = -areapre;					// -1*(c/3)

			TTsunamiDataScalar* hyp   = (TTsunamiDataScalar*)&(io_elementData->hyp_edge);
			TTsunamiDataScalar* left  = (TTsunamiDataScalar*)&(io_elementData->left_edge);
			TTsunamiDataScalar* right = (TTsunamiDataScalar*)&(io_elementData->right_edge);
			CTsunamiEdgeData* support_points = io_elementData->support_points;


	//0			// For clearer code
			T left_h = io_elementData->left_edge.h;
			T left_qx = io_elementData->left_edge.qx;
			T left_qy = io_elementData->left_edge.qy;
			T hyp_h = io_elementData->hyp_edge.h;
			T hyp_qx = io_elementData->hyp_edge.qx;
			T hyp_qy = io_elementData->hyp_edge.qy;
			T right_h = io_elementData->right_edge.h;
			T right_qx = io_elementData->right_edge.qx;
			T right_qy = io_elementData->right_edge.qy;

	//9 = 9*1	// Predefined square values
			T left_h_2 = left_h*left_h;
			T left_qx_2 = left_qx*left_qx;
			T left_qy_2 = left_qy*left_qy;
			T hyp_h_2 = hyp_h*hyp_h;
			T hyp_qx_2 = hyp_qx*hyp_qx;
			T hyp_qy_2 = hyp_qy*hyp_qy;
			T right_h_2 = right_h*right_h;
			T right_qx_2 = right_qx*right_qx;
			T right_qy_2 = right_qy*right_qy;

	//2			// Term to prefix all calculations
			T pre = 6./(cat_length*cat_length);

	//1			// Resulting area prefix (simplified)
			// Full
			//T area = cat_length*cat_length/2.;
			//T areapre = (1/3.)*area*(2./cat_length);                // This term differs with basis functions

			// Simplified
			T areapre = cat_length/3.;

	//3			// Assuming gravity 9.81 m/s^2
			T g2 = 4.905;        // g/2
			T gravity_potential = g2*(left_h_2 + hyp_h_2 + right_h_2);

	//4 = 2*2	// Terms S_x/S_y in the Schwaiger script
			T qx_sum = areapre*(hyp_qx + left_qx + right_qx);
			T qy_sum = areapre*(hyp_qy + left_qy + right_qy);

	//12 = 3*4	// Final calculations for h
			T left_edge_h = pre*(-qy_sum - cat_length*i_left_edge_flux->h);
			T hyp_edge_h = pre*(qx_sum + qy_sum - hyp_length*i_hyp_edge_flux->h);
			T right_edge_h = pre*(-qx_sum - cat_length*i_right_edge_flux->h);

	//12 = 2*6	// Sum (q^2/h + 0.5*g*h^2) over all edges (both for qx and qy)
			T qx2sum = left_qx_2/left_h + hyp_qx_2/hyp_h + right_qx_2/right_h + gravity_potential;
			T qy2sum = left_qy_2/left_h + hyp_qy_2/hyp_h + right_qy_2/right_h + gravity_potential;
	//8			// Sum (qx*qy/h) over all edges
			T qxqysum = left_qx*left_qy/left_h + right_qx*right_qy/right_h + hyp_qx*hyp_qy/hyp_h;

	//15 = 3*5	// Final calculations for qx
			T left_edge_qx = pre*(-areapre*qxqysum - cat_length*i_left_edge_flux->qx);
			T hyp_edge_qx = pre*(areapre*(qx2sum + qxqysum) - hyp_length*i_hyp_edge_flux->qx);
			T right_edge_qx = pre*(-areapre*qx2sum - cat_length*i_right_edge_flux->qx) ;

	//15 = 3*5	// Final calculations for qy
			T left_edge_qy = pre*(-areapre*qy2sum - cat_length*i_left_edge_flux->qy);
			T hyp_edge_qy = pre*(areapre*(qxqysum + qy2sum) - hyp_length*i_hyp_edge_flux->qy);
			T right_edge_qy = pre*(-areapre*qxqysum - cat_length*i_right_edge_flux->qy);

	#elif ORDER_ONE_METHOD==1 // this is basically exactly like Method 0, but uses some castings and indexing. Used as starting point for Vector-like implementation.
			T pre = 6./(cat_length*cat_length);
			T areapre = cat_length/3.;

			// ok, let's cast now!!!!
			TTsunamiDataScalar* hyp   = (TTsunamiDataScalar*)&(io_elementData->hyp_edge);
			TTsunamiDataScalar* left  = (TTsunamiDataScalar*)&(io_elementData->left_edge);
			TTsunamiDataScalar* right = (TTsunamiDataScalar*)&(io_elementData->right_edge);
			int h=0, qx=1, qy=2;

			T left_edge_h = pre*(-(areapre*(hyp[qy] + left[qy] + right[qy])) - cat_length*i_left_edge_flux->h);
			T hyp_edge_h = pre*((areapre*(hyp[qx] + left[qx] + right[qx] + hyp[qy] + left[qy] + right[qy])) - hyp_length*i_hyp_edge_flux->h);
			T right_edge_h = pre*(-(areapre*(hyp[qx] + left[qx] + right[qx])) - cat_length*i_right_edge_flux->h);

			T qx2sum = (left[qx]*left[qx])/left[h] + (hyp[qx]*hyp[qx])/hyp[h] + (right[qx]*right[qx])/right[h] + (9.81*0.5*((left[h]*left[h]) + (hyp[h]*hyp[h]) + (right[h]*right[h])));
			T qy2sum = (left[qy]*left[qy])/left[h] + (hyp[qy]*hyp[qy])/hyp[h] + (right[qy]*right[qy])/right[h] + (9.81*0.5*((left[h]*left[h]) + (hyp[h]*hyp[h]) + (right[h]*right[h])));
			T qxqysum = left[qx]*left[qy]/left[h] + right[qx]*right[qy]/right[h] + hyp[qx]*hyp[qy]/hyp[h];

			T left_edge_qx = pre*(-areapre*qxqysum - cat_length*i_left_edge_flux->qx);
			T hyp_edge_qx = pre*(areapre*(qx2sum + qxqysum) - hyp_length*i_hyp_edge_flux->qx);
			T right_edge_qx = pre*(-areapre*qx2sum - cat_length*i_right_edge_flux->qx) ;

			T left_edge_qy = pre*(-areapre*qy2sum - cat_length*i_left_edge_flux->qy);
			T hyp_edge_qy = pre*(areapre*(qxqysum + qy2sum) - hyp_length*i_hyp_edge_flux->qy);
			T right_edge_qy = pre*(-areapre*qxqysum - cat_length*i_right_edge_flux->qy);

	#elif ORDER_ONE_METHOD==2

			const int h=0, qx=1, qy=2; // Indexer variables
			const T gravity = 9.81; // Can remove this when globally controlled gravity settings are implemented

			// as one can see in the construction of method 2, we can derive the following stuff.
			// we pad all three-tuples with 0 so it "looks like" it has 4 elements (hopefully better for vectorization)
			TTsunamiDataScalar x[4] = {
				io_elementData->left_edge.qx, io_elementData->hyp_edge.qx, io_elementData->right_edge.qx, 0.
			};
			TTsunamiDataScalar y[4] = {
				io_elementData->left_edge.qy, io_elementData->hyp_edge.qy, io_elementData->right_edge.qy, 0.
			};
			TTsunamiDataScalar sh[4] = {
				1./sqrt(io_elementData->left_edge.h), 1./sqrt(io_elementData->hyp_edge.h), 1./sqrt(io_elementData->right_edge.h), 0.
			};
			TTsunamiDataScalar X[4];
			TTsunamiDataScalar Y[4];
			TTsunamiDataScalar H[4];
			for (int i=0; i<4; ++i){
				X[i] = x[i] * sh[i];
				Y[i] = y[i] * sh[i];
			}

			// helpers
			T pre = 6.*(1./(cat_length*cat_length));	// 6/c^2
			T areapre = (1/3.)*cat_length;				// c/3 (Area*)

			// h is computed like before
			TTsunamiDataScalar* hyp   = (TTsunamiDataScalar*)&(io_elementData->hyp_edge);
			TTsunamiDataScalar* left  = (TTsunamiDataScalar*)&(io_elementData->left_edge);
			TTsunamiDataScalar* right = (TTsunamiDataScalar*)&(io_elementData->right_edge);

			T sy=0, sx=0;
			for (int i=0; i<4; ++i){
				sx+=x[i];
				sy+=y[i];
			}
			sy*=areapre;
			sx*=areapre;
			T left_edge_h = pre*(-sy - cat_length*i_left_edge_flux->h);
			T hyp_edge_h = pre*(sx + sy - hyp_length*i_hyp_edge_flux->h);
			T right_edge_h = pre*(-sx - cat_length*i_right_edge_flux->h);


			T gravity_potential = (gravity*0.5)*(((left[h]*left[h]) + (hyp[h]*hyp[h]) + (right[h]*right[h])));

			// now to the other stuff -- here we can vectorize
			T qx2sum = gravity_potential;
			T qy2sum = gravity_potential;
			T qxqysum = 0;
			T XX[4] = {0,0,0,0};
			T YY[4] = {0,0,0,0};
			T XY[4] = {0,0,0,0};
			for (int i=0; i<4; ++i){ // can the intel compiler optimize this?
				XX[i] = X[i]*X[i];
				YY[i] = Y[i]*Y[i];
				XY[i] = X[i]*Y[i];
			}
			for (int i=0; i<4; ++i){
				qx2sum  += XX[i];
				qy2sum  += YY[i];
				qxqysum += XY[i];
			}


			T left_edge_qx = pre*(-areapre*qxqysum - cat_length*i_left_edge_flux->qx);
			T hyp_edge_qx = pre*(areapre*(qx2sum + qxqysum) - hyp_length*i_hyp_edge_flux->qx);
			T right_edge_qx = pre*(-areapre*qx2sum - cat_length*i_right_edge_flux->qx) ;

			T left_edge_qy = pre*(-areapre*qy2sum - cat_length*i_left_edge_flux->qy);
			T hyp_edge_qy = pre*(areapre*(qxqysum + qy2sum) - hyp_length*i_hyp_edge_flux->qy);
			T right_edge_qy = pre*(-areapre*qxqysum - cat_length*i_right_edge_flux->qy);
	#elif ORDER_ONE_METHOD==3
	/*		int order=2;							// Order of basis functions (predefined)
			const int n=order*(order+1)/2;			// Number of basis functions (= number of support points, predefined depending on order)*/
			const int order=1;							// Order of basis functions
			const int n=((order+2)*(order+1))/2;			// Number of basis functions (= number of support points)
			const T gravity = 9.81;					// Can remove this when globally controlled gravity settings are implemented
			const int _h=0, _qx=1, _qy=2;			// Indexer variables (properties)
			const int _left=0, _hyp=1, _right=2;	// Indexer variables (edges)

			T result[n][3] = {{0,0,0},{0,0,0},{0,0,0}}; // nx3 matrix, columns being the support points, rows being the properties (h, qx, qy)

			// Helpers
	//T these change with basis functions
			T pre = 6.*(1./(cat_length*cat_length));	// 6/c^2
			T areapre = (1/3.)*cat_length;				// c/3
			T areaweight = (1/2.)*cat_length*cat_length/n;
			T negareapre = -areapre;					// -1*(c/3)

			TTsunamiDataScalar* hyp   = (TTsunamiDataScalar*)&(io_elementData->hyp_edge);
			TTsunamiDataScalar* left  = (TTsunamiDataScalar*)&(io_elementData->left_edge);
			TTsunamiDataScalar* right = (TTsunamiDataScalar*)&(io_elementData->right_edge);
			CTsunamiEdgeData* support_points = io_elementData->support_points;
			CTsunamiEdgeData* origs = (CTsunamiEdgeData*)io_elementData;

	//		assert(support_points[0].h == origs[0].h);
	//		assert(support_points[0].qy == origs[0].qy);
	//		assert(support_points[0].qx == origs[0].qx);

			// if ( ((abs(support_points[0].h - origs[0].h) > 1) ||
			// 		(abs(support_points[0].qy - origs[0].qy) > 1) ||
			// 		(abs(support_points[0].qx - origs[0].qx) > 1))){
			//   cout << "Obacht. Hier stimmt's nicht!" << endl;
			// }

			for (int i=0; i<3; ++i){
				support_points[i].h = origs[i].h;
				support_points[i].qx = origs[i].qx;
				support_points[i].qy = origs[i].qy;
			}

			T gravity_potential = (gravity*0.5)*(((left[h]*left[h]) + (hyp[h]*hyp[h]) + (right[h]*right[h])));

			// now to the other stuff -- here we can vectorize
			T qx2sum = gravity_potential;
			T qy2sum = gravity_potential;
			T qxqysum = 0;
			T XX[4] = {0,0,0,0};
			T YY[4] = {0,0,0,0};
			T XY[4] = {0,0,0,0};
			for (int i=0; i<4; ++i){ // can the intel compiler optimize this?
				XX[i] = X[i]*X[i];
				YY[i] = Y[i]*Y[i];
				XY[i] = X[i]*Y[i];
			}
			for (int i=0; i<4; ++i){
				qx2sum  += XX[i];
				qy2sum  += YY[i];
				qxqysum += XY[i];
			}


			T left_edge_qx = pre*(-areapre*qxqysum - cat_length*i_left_edge_flux->qx);
			T hyp_edge_qx = pre*(areapre*(qx2sum + qxqysum) - hyp_length*i_hyp_edge_flux->qx);
			T right_edge_qx = pre*(-areapre*qx2sum - cat_length*i_right_edge_flux->qx) ;

			T left_edge_qy = pre*(-areapre*qy2sum - cat_length*i_left_edge_flux->qy);
			T hyp_edge_qy = pre*(areapre*(qxqysum + qy2sum) - hyp_length*i_hyp_edge_flux->qy);
			T right_edge_qy = pre*(-areapre*qxqysum - cat_length*i_right_edge_flux->qy);

	#elif ORDER_ONE_METHOD==4

			/*
			* Gegeben:
			* T: cat_length, hyp_length
			* Edge: io_elementData.hyp_edge, io_elementData.left_edge, io_elementData.right_edge
			*   .h, .qx, .qy
			* Flux: i_hyp_edge_flux, i_left_edge_flux, i_right_edge_flux
			*   .h, .qx, .qy
			* Ziel:
			* Berechne hyp_edge_h, hyp_edge_qx, etc. (alle Kombinationen der Kanten und der Komponenten h, qx, qy)
			*/

			const int n=3;			// Number of basis functions (= number of support points)
			const T gravity = 9.81;					// Can remove this when globally controlled gravity settings are implemented
			const int _h=0, _qx=1, _qy=2;			// Indexer variables (properties)
			const int _left=0, _hyp=1, _right=2;	// Indexer variables (edges)

			CTsunamiEdgeData* origs = (CTsunamiEdgeData*)io_elementData;
			T result[n][3] = {{0,0,0},{0,0,0},{0,0,0}}; // nx3 matrix, columns being the support points, rows being the properties (h, qx, qy)

			// Helpers
			//T these change with basis functions
			T pre = 6.*(1./(cat_length*cat_length));	// 6/c^2
			T areapre = (1/3.)*cat_length;				// c/3
			T areaweight = (1/2.)*cat_length*cat_length/n;
			T negareapre = -areapre;					// -1*(c/3)

			TTsunamiDataScalar* hyp   = (TTsunamiDataScalar*)&(io_elementData->hyp_edge);
			TTsunamiDataScalar* left  = (TTsunamiDataScalar*)&(io_elementData->left_edge);
			TTsunamiDataScalar* right = (TTsunamiDataScalar*)&(io_elementData->right_edge);
			CTsunamiEdgeData* support_points = io_elementData->support_points;

			T gravity_potential = (gravity*0.5)*(((left[_h]*left[_h]) + (hyp[_h]*hyp[_h]) + (right[_h]*right[_h])));

			// These matrices need to be computed, perhaps seperately (Maple for example)
			//T these change with basis functions
			// Mass matrix
			T E[3][3] = {
			        { cat_length, 0, 0},
			        { 0, sqrt(0.2e1) * cat_length, 0},
			        { 0, 0, cat_length},
			};
			T Sy[3][3] = {
			        { -cat_length / 0.3e1, -cat_length / 0.3e1, -cat_length / 0.3e1},
			        { cat_length / 0.3e1, cat_length / 0.3e1, cat_length / 0.3e1},
			        { 0, 0, 0},
			};
			T M[3][3] = {
			        { 0.6e1 * pow(cat_length, -0.2e1), 0, 0},
			        { 0, 0.6e1 * pow(cat_length, -0.2e1), 0},
			        { 0, 0, 0.6e1 * pow(cat_length, -0.2e1)},
			};
			T Sx[3][3] = {
			        { 0, 0, 0},
			        { cat_length / 0.3e1, cat_length / 0.3e1, cat_length / 0.3e1},
			        { -cat_length / 0.3e1, -cat_length / 0.3e1, -cat_length / 0.3e1},
			};
			T diff[3][2] = {
			        { 0, -2 / cat_length},
			        { 2 / cat_length, 2 / cat_length},
			        { -2 / cat_length, 0},
			};
//			T M[n][n] = {
//					{pre,0,0},
//					{0,pre,0},
//					{0,0,pre}
//			};
//			// Stiffness matrices (wrt x and y)
//			T Sx[n][n] = {
//					{0,0,0},
//					{areapre,areapre,areapre},
//					{negareapre,negareapre,negareapre}
//			};
//			T Sy[n][n] = {
//					{negareapre,negareapre,negareapre},
//					{areapre,areapre,areapre},
//					{0,0,0}
//			};
//			T E[n][3] = {
//					{cat_length,0,0},
//					{0,hyp_length,0},
//					{0,0,cat_length},
//			};
//			// Differentiated basis functions, applied to the support points
//			T diff[n][2] = {
//					{0,(T)-2./cat_length},
//					{(T)2./cat_length,(T)2./cat_length},
//					{(T)-2./cat_length,0}
//			};



//			static int testcounter = 0;
//			bool testme = false;
//			for (int i=0; i<3; ++i){
//				if (support_points[i].h != origs[i].h ||
//						support_points[i].qx != origs[i].qx ||
//						support_points[i].qy != origs[i].qy){
//					testme = true;
//				}
//			}
//			if (testme){
//				testcounter++;
//				std::cout << "testcounter : " << testcounter << std::endl;
//				for (int i=0; i<3; ++i){
//					std::cout << support_points[i] << "\t" << origs[i] << std::endl;
//				}
//				std::cout << "-------------------" << std::endl;
//				std::cout << io_elementData->support_points[2] << "\t" << io_elementData->support_points[0] << io_elementData->support_points[1] <<  std::endl;
//				std::cout << io_elementData->left_edge << "\t" << io_elementData->hyp_edge << io_elementData->right_edge << std::endl;
//				std::cout << "-------------------" << std::endl;
//				for (int i=0; i<3; ++i){
//					support_points[i].h = origs[i].h;
//					support_points[i].qx = origs[i].qx;
//					support_points[i].qy = origs[i].qy;
//				}
//			}
//			else{
//				std::cout << "Correct" << std::endl;
//			}





			T qx[n];// = {left[_qx], hyp[_qx], right[_qx]};
			T qy[n];// = {left[_qy], hyp[_qy], right[_qy]};
			T h[n];// = {left[_h], hyp[_h], right[_h]};
			// Do we need this?
			for(int i=0; i<n; i++) {
				qx[i] = support_points[i].qx;
				qy[i] = support_points[i].qy;
				h[i] = support_points[i].h;
			}
			T flux[3][3] = {
					{i_left_edge_flux->h,i_left_edge_flux->qx, i_left_edge_flux->qy},
					{i_hyp_edge_flux->h,i_hyp_edge_flux->qx, i_hyp_edge_flux->qy},
					{i_right_edge_flux->h,i_right_edge_flux->qx, i_right_edge_flux->qy}
			};
			T length[n][3];
			for(int k=0; k<3; k++){
				for(int i=0; i<n; i++) {
					length[i][k] = 0;
					for(int j=0; j<3; j++) {
						length[i][k] += E[i][j]*flux[j][k];
					}
				}
			}
			//T gravity_potential = (gravity*0.5)*(((left[_h]*left[_h]) + (hyp[_h]*hyp[_h]) + (right[_h]*right[_h])));

			// Computing h
			for(int i=0; i<n; i++) {
				T sqx = 0;
				T sqy = 0;
				for(int j=0; j<n;j++) {
					sqx += Sx[i][j]*qx[j];
					sqy += Sy[i][j]*qy[j];
				}
				result[i][_h] = sqx + sqy - length[i][_h];	// Adjust flux term
			}

			// These matrices need to be computed, perhaps seperately (Maple for example)
			//T these change with basis functions
			// Computing h
			for(int i=0; i<n; i++) {
				T sqx = 0;
				T sqy = 0;
				for(int j=0; j<n;j++) {
					sqx += Sx[i][j]*qx[j];
					sqy += Sy[i][j]*qy[j];
				}
				result[i][_h] = sqx + sqy - length[i][_h];	// Adjust flux term
			}

			// Computing qx and qy
			T qx2sum = gravity_potential;
			T qy2sum = gravity_potential;
			T qxqysum = 0;
			for(int j=0; j<n; j++) {
				qx2sum += qx[j]*qx[j]/h[j];
				qy2sum += qy[j]*qy[j]/h[j];
				qxqysum += qx[j]*qy[j]/h[j];
			}
			for(int i=0; i<n; i++) {
				result[i][_qx] = areaweight*(qx2sum*diff[i][_qx-1] + qxqysum*diff[i][_qy-1]) - length[i][_qx];
				result[i][_qy] = areaweight*(qy2sum*diff[i][_qy-1] + qxqysum*diff[i][_qx-1]) - length[i][_qy];
			}

			for(int i=0; i<n; i++) {
				for(int j=0; j<3; j++) {
					result[i][j] *= M[i][i];
				}
			}

			T left_edge_h = (result[0][_h]);
			T hyp_edge_h =  (result[1][_h]);
			T right_edge_h = (result[2][_h]);

			T left_edge_qx =  (result[0][_qx]);
			T hyp_edge_qx =  (result[1][_qx]);
			T right_edge_qx =  (result[2][_qx]);

			T left_edge_qy =  (result[0][_qy]);
			T hyp_edge_qy =  (result[1][_qy]);
			T right_edge_qy =  (result[2][_qy]);

#endif

			if (t_storeElementUpdatesOnly)
			{
				io_elementData->hyp_edge.h = hyp_edge_h;
				io_elementData->hyp_edge.qx = hyp_edge_qx;
				io_elementData->hyp_edge.qy = hyp_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->hyp_edge.qx, io_elementData->hyp_edge.qy, i_right_normal_x, i_right_normal_y);

				io_elementData->right_edge.h = right_edge_h;
				io_elementData->right_edge.qx = right_edge_qx;
				io_elementData->right_edge.qy = right_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->right_edge.qx, io_elementData->right_edge.qy, i_right_normal_x, i_right_normal_y);

				io_elementData->left_edge.h = left_edge_h;
				io_elementData->left_edge.qx = left_edge_qx;
				io_elementData->left_edge.qy = left_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->left_edge.qx, io_elementData->left_edge.qy, i_right_normal_x, i_right_normal_y);
			}
			else
			{
				io_elementData->hyp_edge.h += timestep_size*hyp_edge_h;
				io_elementData->hyp_edge.qx += timestep_size*hyp_edge_qx;
				io_elementData->hyp_edge.qy += timestep_size*hyp_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->hyp_edge.qx, io_elementData->hyp_edge.qy, i_right_normal_x, i_right_normal_y);

				io_elementData->right_edge.h += timestep_size*right_edge_h;
				io_elementData->right_edge.qx += timestep_size*right_edge_qx;
				io_elementData->right_edge.qy += timestep_size*right_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->right_edge.qx, io_elementData->right_edge.qy, i_right_normal_x, i_right_normal_y);

				io_elementData->left_edge.h += timestep_size*left_edge_h;
				io_elementData->left_edge.qx += timestep_size*left_edge_qx;
				io_elementData->left_edge.qy += timestep_size*left_edge_qy;
				CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->left_edge.qx, io_elementData->left_edge.qy, i_right_normal_x, i_right_normal_y);


			}

			for (int i=0; i<n; ++i){
				io_elementData->support_points[i].h = origs[i].h;
				io_elementData->support_points[i].qx = origs[i].qx;
				io_elementData->support_points[i].qy = origs[i].qy;
				//io_elementData->support_points[i].b = origs[i].b;
			}

		}



	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void computeNetUpdates(
			CTsunamiEdgeData &io_edgeFlux_left,		///< edge data on left edge
			CTsunamiEdgeData &io_edgeFlux_right,	///< edge data on right edge
			CTsunamiEdgeData &o_edgeNetUpdates_left,		///< output for left flux
			CTsunamiEdgeData &o_edgeFlux_right		///< output for outer flux
	)
	{
		T o_max_wave_speed_left;
		T o_max_wave_speed_right;

		/*
		 * fix edge normal space for right flux
		 */
		io_edgeFlux_right.qx = -io_edgeFlux_right.qx;
		io_edgeFlux_right.qy = -io_edgeFlux_right.qy;

		fluxSolver.computeNetUpdates(
				io_edgeFlux_left,
				io_edgeFlux_right,
				o_edgeNetUpdates_left,
				o_edgeFlux_right,
				o_max_wave_speed_left,
				o_max_wave_speed_right,
				gravitational_constant
			);

		o_edgeFlux_right.qx = -o_edgeFlux_right.qx;
		o_edgeFlux_right.qy = -o_edgeFlux_right.qy;

		// store max wave speeds to same storage as bathymetry data
		o_edgeNetUpdates_left.max_wave_speed = o_max_wave_speed_left;
		o_edgeFlux_right.max_wave_speed = o_max_wave_speed_right;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edgeNetUpdates_left.validation = io_edgeFlux_left.validation;
		o_edgeFlux_right.validation = io_edgeFlux_right.validation;
#endif
	}

};

}
}



#endif
