/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_


#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/parallelization/CPartition_SplitJoinInformation.hpp"
#include "../CTsunamiSimulationDataSets.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData
{
private:
	class CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData_Private *generic_traversator;

	bool repeat_traversal;

public:
	CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData();

	virtual ~CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData();

	void setSetupMethod(int i);

	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CTsunamiSimulationStacks *cStacks);

	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CTsunamiSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CTsunamiSimulationStacks *cStacks,
			CPartition_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	);

	/**
	 * setup the initial root domain partition
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);

	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_RootTraversator(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);

	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			TTsunamiDataScalar i_square_side_length,

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
			TTsunamiDataScalar i_refine_height_threshold,
			TTsunamiDataScalar i_coarsen_height_threshold,

			TTsunamiDataScalar i_refine_slope_threshold,
			TTsunamiDataScalar i_coarsen_slope_threshold,
#endif

			CTsunamiSimulationDataSets *i_cSimulationDataSets
		);

	/**
	 * setup the child partition based on it's parent traversator information and the child's factory
	 */
	void setup_Partition(
			CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData &parent,
			CTriangle_Factory &i_child_triangleFactory
		);

	void storeReduceValue(
			TTsunamiDataScalar *o_cflReduceValue
		);
};

}
}

#endif
