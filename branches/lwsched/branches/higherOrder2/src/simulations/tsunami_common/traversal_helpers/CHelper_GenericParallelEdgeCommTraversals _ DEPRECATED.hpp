/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelEdgeCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_

#include "config.h"

#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libmath/CMath.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */

#error "DEPRECATED"

class CHelper_GenericParallelEdgeCommTraversals
{
public:
	/**
	 * run the edge comm traversals
	 */
	template<
		typename CPartition_,				/// partition type offering necessary structures, edge comm data, etc.
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TEdgeCommTraversator,	/// Traversator including kernel
		typename TEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue				/// value to use for reduction (e. g. CFL condition)
	>
	static void action(
			TEdgeCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			CPartition_ExchangeEdgeCommData<CPartition_, TEdgeData, CStackAccessors> CSimulation_Cluster::*i_simulationEdgeCommSubClass,
			CGenericTreeNode<CPartition_> *i_node,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_node->traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode<CPartition_> *i_cGenericTreeNode)
				{
					CPartition_ *i_node = i_cGenericTreeNode->cPartition_TreeNode;
					(i_node->cSimulation_Cluster->*(i_simulationSubClass)).actionFirstPass(i_node->cStacks);
				}
		);

#if CONFIG_ENABLE_MPI
		*o_cfl_reduce_value = CMath::numeric_inf<TReduceValue>();
#endif

		// second pass: setting all edges to type 'old' reads data from adjacent partitions
		i_node->traverse_GenericTreeNode_Reduce_Parallel_Scan(
				[=](CGenericTreeNode<CPartition_> *i_cGenericTreeNode, TReduceValue *o_reduceValue)
				{
					CPartition_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					// firstly, pull edge communication data from adjacent sub-partitions
					(node->cSimulation_Cluster->*(i_simulationEdgeCommSubClass)).pullEdgeCommData();

					// run computation based on newly set-up stacks
					(node->cSimulation_Cluster->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, i_timestep_size, o_reduceValue);

					node->cSimulation_Cluster->cfl_domain_size_div_max_wave_speed_after_edge_comm = *o_reduceValue;
				},
				&(CReduceOperators::MIN<TReduceValue>),	// use minimum since the minimum timestep has to be selected
				o_cfl_reduce_value
			);

		i_node->cPartition_TreeNode->cSimulation_Cluster->cfl_domain_size_div_max_wave_speed_after_edge_comm = *o_cfl_reduce_value;
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
