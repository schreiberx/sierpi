/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationTsunami_DomainSetups.hpp
 *
 *  Created on: Jun 26, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATIONTSUNAMI_DOMAINS_HPP_
#define CSIMULATIONTSUNAMI_DOMAINS_HPP_

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "lib/CTriangulationFromSVG.hpp"

#define DOMAIN_FILE_PATH	"data/domains/"

class CSimulationTsunami_DomainSetups
{
	typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;

public:
	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Triangle(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Quad(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Strip_PeriodicBoundaries(
			CDomain_BaseTriangulation_ &o_cDomainBaseTriangulation,
			int i_strip_size = 1
		)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		int initial_displacement = -i_strip_size+1;


		/*
		 * !!! We have to use 4 triangles per column, to avoid adjacent triangle search problems for edge comm data exchange !!!
		 */
		assert(i_strip_size <= 1024);

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[4];
#endif

		int traversal_type = 0;

		/*
		 * different traversal types following the SFC
		 *  ___   ___
		 * |\3/| |\6/|
		 * |2+4| |5+7|
		 * |/1\| |/8\|
		 *  ---   ---
		 *   0     1   <--- type
		 */
		for (int i = 0; i < i_strip_size; i++)
		{
			if (traversal_type == 0)
			{
				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[0] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[1] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2]->adjacent_triangles.hyp_edge = baseTriangles[0];
				baseTriangles[2]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
				baseTriangles[0]->adjacent_triangles.hyp_edge = baseTriangles[2];
				baseTriangles[0]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

			}
			else
			{

				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[0] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = 1.0;
				template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[1] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

				template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = 1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[2] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
				template_rootTriangle.vertices[0][1] = -1.0;
				template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
				template_rootTriangle.vertices[1][1] = -1.0;
				template_rootTriangle.vertices[2][0] = (T)initial_displacement;
				template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3] = &
#endif
					o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
				baseTriangles[3]->adjacent_triangles.hyp_edge = baseTriangles[1];
				baseTriangles[3]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
				baseTriangles[1]->adjacent_triangles.hyp_edge = baseTriangles[3];
				baseTriangles[1]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif
			}

			initial_displacement += 2;
			traversal_type = (traversal_type+1) & 1;
		}

		o_cDomainBaseTriangulation.multiplyVertexCoordsWithScalar((T)1.0/(T)i_strip_size);

		return (T)i_strip_size;
	}


	/**
	 * setup world cube using periodic boundary conditions
	 *
	 *     __
	 *    | /|
	 *  __|/_|__ __
	 * | /|\ | /|\ |
	 * |/_|_\|/_|_\|
	 *    | /|
	 *    |/_|
	 *
	 *
	 *     __
	 *    |5/|
	 *  __|/6|__ __
	 * |4/|\7|8/|\b|
	 * |/3|2\|/9|a\|
	 *    |1/|
	 *    |/0|
	 *
	 */
	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2DCube_PeriodicBoundaries(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[12];
#endif

		static T vertex_data[12][6] =
		{
				{	// 0
					1.0,	-1.0,
					-1.0,	-3.0,
					1.0,	-3.0
				},
				{	// 1
					-1.0,	-3.0,
					1.0,	-1.0,
					-1.0,	-1.0
				},
				{	// 2
					1.0,	-1.0,
					-1.0,	1.0,
					-1.0,	-1.0
				},
				{	// 3
					-1.0,	1.0,
					-3.0,	-1.0,
					-1.0,	-1.0
				},
				{	// 4
					-3.0,	-1.0,
					-1.0,	1.0,
					-3.0,	1.0
				},
				{	// 5
					-1.0,	1.0,
					1.0,	3.0,
					-1.0,	3.0
				},
				{	// 6
					1.0,	3.0,
					-1.0,	1.0,
					1.0,	1.0
				},
				{	// 7
					-1.0,	1.0,
					1.0,	-1.0,
					1.0,	1.0
				},
				{	// 8
					1.0,	-1.0,
					3.0,	1.0,
					1.0,	1.0
				},
				{	// 9
					3.0,	1.0,
					5.0,	-1.0,
					5.0,	1.0
				},
				{	// 10
					5.0,	-1.0,
					3.0,	1.0,
					3.0,	-1.0
				},
				{	// 11
					3.0,	1.0,
					1.0,	-1.0,
					3.0,	-1.0
				},
		};

		for (int i = 0; i < 12; i++)
		{
			template_rootTriangle.setVertices(
					vertex_data[i][0],
					vertex_data[i][1],
					vertex_data[i][2],
					vertex_data[i][3],
					vertex_data[i][4],
					vertex_data[i][5]
				);

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);
		}


#if CONFIG_PERIODIC_BOUNDARIES
//		baseTriangles[0]->setupRightEdgeConnection(baseTriangles[10]);
//		baseTriangles[0]->setupLeftEdgeConnection(baseTriangles[11]);

//		baseTriangles[1]->setupLeftEdgeConnection(baseTriangles[3]);

//		baseTriangles[3]->setupRightEdgeConnection(baseTriangles[1]);

		baseTriangles[4]->setupLeftEdgeConnection(baseTriangles[9]);
//		baseTriangles[4]->setupRightEdgeConnection(baseTriangles[5]);

//		baseTriangles[5]->setupLeftEdgeConnection(baseTriangles[4]);
		baseTriangles[5]->setupRightEdgeConnection(baseTriangles[9]);

//		baseTriangles[6]->setupLeftEdgeConnection(baseTriangles[8]);
//		baseTriangles[8]->setupRightEdgeConnection(baseTriangles[6]);

		baseTriangles[9]->setupLeftEdgeConnection(baseTriangles[5]);
		baseTriangles[9]->setupRightEdgeConnection(baseTriangles[4]);


//		baseTriangles[10]->setupLeftEdgeConnection(baseTriangles[0]);

//		baseTriangles[11]->setupRightEdgeConnection(baseTriangles[0]);
#endif

		domainBaseTriangulation.multiplyVertexCoordsWithScalar((T)0.25);

		return (T)4;
	}



	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Quad_PeriodicBoundaries(CDomain_BaseTriangulation_ &o_cDomainBaseTriangulation)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "Domains with periodic boundaries not activated!" << std::endl;
#endif


#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[4];
#endif

		CTriangle_Factory template_rootTriangle;


		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[0] = &
#endif
			o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[1] = &
#endif
			o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;


#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[2] = &
#endif
			o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0;
		template_rootTriangle.vertices[2][1] = 0;

#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[3] = &
#endif
			o_cDomainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangles[2]->adjacent_triangles.hyp_edge = baseTriangles[0];
		baseTriangles[2]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangles[0]->adjacent_triangles.hyp_edge = baseTriangles[2];
		baseTriangles[0]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

		baseTriangles[1]->adjacent_triangles.hyp_edge = baseTriangles[3];
		baseTriangles[1]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangles[3]->adjacent_triangles.hyp_edge = baseTriangles[1];
		baseTriangles[3]->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

		return 2.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2OddTriangles1(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}



	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2OddTriangles2(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 0.0;
		template_rootTriangle.vertices[0][1] = 0.0;
		template_rootTriangle.vertices[1][0] = -2.0;
		template_rootTriangle.vertices[1][1] = 0.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2EvenTriangles1(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 2.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -2.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2EvenTriangles2(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_3Triangles(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -3.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_QuadTiles(CDomain_BaseTriangulation_ &domainBaseTriangulation, int tiles_x, int tiles_y)
	{

		CTriangle_Factory template_rootTriangle;
		// TODO: care about machine number inaccuracy

		for (double x = -1; x <= (double)tiles_x; x += 2.0)
		{
			for (double y = -1; y <= (double)tiles_y; y += 2.0)
			{
				template_rootTriangle.vertices[0][0] = 1.0+x;
				template_rootTriangle.vertices[0][1] = -1.0+y;
				template_rootTriangle.vertices[1][0] = -1.0+x;
				template_rootTriangle.vertices[1][1] = 1.0+y;
				template_rootTriangle.vertices[2][0] = -1.0+x;
				template_rootTriangle.vertices[2][1] = -1.0+y;

				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = -1.0+x;
				template_rootTriangle.vertices[0][1] = 1.0+y;
				template_rootTriangle.vertices[1][0] = 1.0+x;
				template_rootTriangle.vertices[1][1] = -1.0+y;
				template_rootTriangle.vertices[2][0] = 1.0+x;
				template_rootTriangle.vertices[2][1] = 1.0+y;

				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);
			}
		}

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_SVG(
			CDomain_BaseTriangulation_ &domainBaseTriangulation,
			const char* filename = DOMAIN_FILE_PATH"test_domain.svg",
			double scale_factor = 0.1
	)
	{

		CTriangulationFromSVG triangulationSVG;


		if (!triangulationSVG.loadSVGFile(filename))
		{
			return false;
		}

	    for (	std::vector<CTriangulationFromSVG::CTriangle2D>::iterator iter = triangulationSVG.triangles.begin();
	    		iter != triangulationSVG.triangles.end();
	    		iter++
	    )
	   	{
			CTriangle_Factory rootTriangle;

			for (int i = 0; i < 3; i++)
	    	{
	    		rootTriangle.vertices[i][0] = iter->vertices[i][0];
	    		rootTriangle.vertices[i][1] = iter->vertices[i][1];
	    	}

			domainBaseTriangulation.insert_DomainTriangle(rootTriangle);
	   	}

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(scale_factor);

		return 1.0/scale_factor;
	}
};

#endif /* CSIMULATIONTSUNAMI_DOMAINS_HPP_ */
