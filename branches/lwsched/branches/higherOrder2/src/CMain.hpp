/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 17. April 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CMAIN_HPP
#define CMAIN_HPP


#include <iostream>
#include <string>
#include "config.h"
#include "lib/iRef.hpp"
#include "simulations/CSimulation.hpp"
#include "lib/CProcessMemoryInformation.hpp"
#include "lib/CStopwatch.hpp"

// IPMO
#if COMPILE_WITH_IPMO
	#include "mainthreading/CMainThreadingIPMO.hpp"
#else

// IOMP
#if COMPILE_WITH_IOMP
	#include "mainthreading/CMainThreadingIOMP.hpp"
#else

// OMP
#if COMPILE_WITH_OMP
	#include "mainthreading/CMainThreadingOMP.hpp"
#else

// ITBB
#if COMPILE_WITH_ITBB
	#include "mainthreading/CMainThreadingITBB.hpp"
#else

// TBB
#if COMPILE_WITH_TBB
	#include "mainthreading/CMainThreadingTBB.hpp"
#else

// DUMMY
	#include "mainthreading/CMainThreadingDummy.hpp"
#endif
#endif
#endif
#endif
#endif

#include "xmlconfig/CXMLConfig.hpp"


#if CONFIG_ENABLE_MPI
#include <mpi.h>
#endif

#if 0
#include <mpi/mpi.h>
#endif


template <typename CSimulation>
class CMain	:
	public CMainThreading,
	public CXMLConfigInterface
{
public:
	CProcessMemoryInformation cProcessMemoryInformation;

	CXMLConfig cXMLConfig;

	/*
	 * simulation timestep control
	 */
	int simulation_run_for_fixed_timesteps;
	int simulation_run_for_fixed_simulation_time;
	double simulation_run_with_fixed_timestepSize;
	int simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation;

	/*
	 * threading
	 */
	int threading_max_number_of_threads;
	int threading_number_of_threads_to_use;
	int threading_affinity_padding;
	int threading_affinity_start_id;
	int threading_ipmo_scalability_graph_id;

#if CONFIG_ENABLE_MPI
#endif

	/*
	 * verbosity
	 */
	int verbosity_level;

	/*
	 * output
	 */
	double output_simulation_data_after_each_n_realtime_seconds;
	int output_simulation_data_each_nth_timestep;
	double output_simulation_data_after_each_n_simulation_seconds;


	std::string output_simulation_vtk_data_filename;
	bool output_simulation_data_clusters;

	int partition_update_split_join_size_after_elapsed_timesteps;
	double partition_update_split_join_size_after_elapsed_scalar;

	// pin the current thread to given core nr
	int threading_pin_to_core_nr_after_one_timestep;

	/**
	 * SIMULATION
	 */
	CSimulation *cSimulation;


	int getMaxNumberOfThreads()
	{
		return threading_max_number_of_threads;
	}

	void setMaxNumberOfThreads(int i_max_number_of_threads)
	{
		threading_max_number_of_threads = i_max_number_of_threads;

//		if (cSimulation != nullptr)
//			cSimulation->simulation_threading_max_number_of_threads = i_max_number_of_threads;
	}

	int getNumberOfThreadsToUse()
	{
		return threading_number_of_threads_to_use;
	}

	int getThreadAffinityPadding()
	{
		return threading_affinity_padding;
	}

	int getThreadAffinityStartId()
	{
		return threading_affinity_start_id;
	}

	int getThreadIPMOScalabilityGraphId()
	{
		return threading_ipmo_scalability_graph_id;
	}

	void setValueNumberOfThreadsToUse(int i_number_of_threads_to_use)
	{
		threading_number_of_threads_to_use = i_number_of_threads_to_use;

		if (cSimulation != nullptr)
			cSimulation->simulation_threading_number_of_threads = i_number_of_threads_to_use;
	}



	/**
	 * for a tsunami simulation, the approximated workload is given by the number
	 * of grid-cells
	 *
	 * \return simulation workload
	 */
	long long getSimulationWorkload()
	{
		if (cSimulation == nullptr)
			return 0;

		return cSimulation->number_of_triangles;
	}


	long long getSimulationSumWorkload()
	{
		if (cSimulation == nullptr)
			return 0;

		return simloop_sum_number_of_triangles;
	}


	int getVerboseLevel()
	{
		return verbosity_level;
	}

	int main_argc;
	char** main_argv;

#if CONFIG_ENABLE_MPI
	int mpi_rank;
	int mpi_size;
#endif

	/**
	 * constructor
	 */
	CMain(
		int i_argc,			///< number of specified arguments
		char **i_argv		///< array with arguments
	)	:
		cSimulation(nullptr)
	{
		main_argc = i_argc;
		main_argv = i_argv;

		threading_max_number_of_threads = -1;
		threading_number_of_threads_to_use = -1;

		simulation_run_for_fixed_timesteps = -1;
		simulation_run_with_fixed_timestepSize = -1;
		simulation_run_for_fixed_simulation_time = -1;

		threading_affinity_padding = -1;
		threading_affinity_start_id = -1;
		threading_ipmo_scalability_graph_id = -1;

		simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation = -1;

		verbosity_level = 0;

		output_simulation_data_each_nth_timestep = -100;
		output_simulation_data_after_each_n_realtime_seconds = -1.0;
		output_simulation_data_after_each_n_simulation_seconds = -1.0;

		output_simulation_vtk_data_filename = "frame_%08i.vtk";
		output_simulation_data_clusters = false;

		output_timing_flags = OUTPUT_TIMING_NULL;
		output_data_flags = OUTPUT_DATA_NULL;

		partition_update_split_join_size_after_elapsed_timesteps = -1;
		partition_update_split_join_size_after_elapsed_scalar = -1;

		threading_pin_to_core_nr_after_one_timestep = -1;


		// register main class. simulation class should be already registered
		cXMLConfig.registerConfigClass("sierpi", this);
		cXMLConfig.registerConfigClass("threading", this);
	}


	/**
	 * parse the program start parameters
	 *
	 * \return true when all parameters are valid
	 */
	bool setupProgramParameters()
	{
		char optchar;
		std::string options("A:b:c:B:f:F:G:g:v:L:n:N:t:pP:S:T:");
		options += cSimulation->config_command_line_getArgumentString();

		while ((optchar = getopt(main_argc, main_argv, options.c_str())) > 0)
		{
			switch(optchar)
			{
			case 'v':
				verbosity_level = atoi(optarg);
				break;

			case 'c':
				// load & forward xml configuration
				loadAndForwardXMLConfig(optarg);
				break;

			case 't':
				simulation_run_for_fixed_timesteps = atoi(optarg);
				break;

			case 'T':
				simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation = atoi(optarg);
				break;

			case 'L':
				simulation_run_for_fixed_simulation_time = atof(optarg);
				break;


			/*
			 * VTK filename
			 */
			case 'g':
				output_simulation_vtk_data_filename = optarg;
				break;


			/*
			 * VTK GRID
			 */
			case 'F':
				output_simulation_data_after_each_n_simulation_seconds = atof(optarg);
				break;

			case 'f':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				break;


			/*
			 * VTK clusters
			 */
			case 'p':
				output_simulation_data_clusters = true;
				break;


			/*
			 * stdout verbose
			 */
			case 'B':
				output_simulation_data_after_each_n_simulation_seconds = atof(optarg);
				break;

			case 'b':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				break;


#if SIMULATION_TSUNAMI_PARALLEL
			/*
			 * threading
			 */
			case 'n':
				threading_number_of_threads_to_use = atoi(optarg);
				break;

			case 'N':
				threading_max_number_of_threads = atoi(optarg);
				break;

			case 'A':
				threading_affinity_padding = atoi(optarg);
				break;

			case 'S':
				threading_affinity_start_id = atoi(optarg);
				break;

			case 'G':
				threading_ipmo_scalability_graph_id = atoi(optarg);
				break;
#endif

			case 'P':
				threading_pin_to_core_nr_after_one_timestep = atoi(optarg);
				break;

			case 'h':
				printParameterInfo(main_argc, main_argv);
				return false;

			default:
				if (!cSimulation->config_command_line_parseArgument(optchar, optarg))
				{
					printParameterInfo(main_argc, main_argv);
					return false;
				}
/*
				std::string parameter_string;
				if (optarg != nullptr)
					parameter_string = optarg;

				std::pair<char, std::string> parameter_pair(optchar, parameter_string);
				simulation_parameters.push_back(parameter_pair);
*/
				break;
			}
		}

		if (!config_validateAndFixParameters())
			return false;

		if (!cSimulation->config_validateAndFixParameters())
			return false;

		return true;
	}


	/**
	 * callback to setup configuration
	 */
	bool setupXMLParameter(
				const char *i_scope_name,
				const char *i_name,
				const char *i_value
	)
	{
		if (strcmp(i_scope_name, "threading") == 0)
		{
			XML_CONFIG_TEST_AND_SET_INT("number-of-threads", threading_number_of_threads_to_use);
			XML_CONFIG_TEST_AND_SET_INT("max-number-of-threads", threading_max_number_of_threads);

			XML_CONFIG_TEST_AND_SET_INT("core-affinities-start", threading_affinity_start_id);
			XML_CONFIG_TEST_AND_SET_INT("core-affinities-padding", threading_affinity_padding);

			XML_CONFIG_TEST_AND_SET_INT("ipmo-scalability-graph-id", threading_ipmo_scalability_graph_id);

			XML_CONFIG_TEST_AND_SET_BOOL("pin-to-core-nr-after-one-timestep", threading_pin_to_core_nr_after_one_timestep);
			return false;
		}

		if (strcmp(i_scope_name, "sierpi") == 0)
		{
			XML_CONFIG_TEST_AND_SET_INT("verbosity-level", verbosity_level);

			XML_CONFIG_TEST_AND_SET_INT("simulation-run-for-fixed-timesteps", simulation_run_for_fixed_timesteps);
			XML_CONFIG_TEST_AND_SET_INT("simulation-terminate-after-n-timesteps-with-equal-number-of-triangles-in-simulation", simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-run-for-fixed-simulation-time", simulation_run_for_fixed_simulation_time);

			XML_CONFIG_TEST_AND_SET_STRING("output-vtk-filename", output_simulation_vtk_data_filename);
			XML_CONFIG_TEST_AND_SET_INT("output-vtk-grid-after-each-n-simulation-seconds", output_simulation_data_after_each_n_simulation_seconds);
			XML_CONFIG_TEST_AND_SET_INT("output-vtk-grid-after-each-nth-timestep", output_simulation_data_each_nth_timestep);
			XML_CONFIG_TEST_AND_SET_BOOL("output-vtk-clusters", output_simulation_data_clusters);

			XML_CONFIG_TEST_AND_SET_INT("output-stdout-verbose-after-each-n-simulation-seconds", output_simulation_data_after_each_n_simulation_seconds);
			XML_CONFIG_TEST_AND_SET_INT("output-stdout-verbose-after-each-nth-timestep", output_simulation_data_each_nth_timestep);
			return false;
		}
		return false;
	}


	/**
	 * load and forward XML configuration
	 */
	bool loadAndForwardXMLConfig(
			const char *xml_configuration_file
	)
	{
		bool xml_config_loaded = false;

		if (xml_configuration_file != nullptr)
			xml_config_loaded = cXMLConfig.loadConfig(xml_configuration_file);

		return xml_config_loaded;
	}


	/**
	 * validate and fix parameters
	 */
	bool config_validateAndFixParameters()
	{
		if (output_simulation_data_after_each_n_simulation_seconds > 0)
		{
			output_data_flags |= OUTPUT_DATA_VTK_GRID;
			output_timing_flags = OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;
		}

		if (output_simulation_data_after_each_n_realtime_seconds > 0)
		{
			if (output_timing_flags == OUTPUT_TIMING_NULL)
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;
		}

		if (output_simulation_data_each_nth_timestep > 0)
		{
			output_data_flags |= OUTPUT_DATA_VTK_GRID;
			output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
		}

		if (output_simulation_data_clusters)
		{
			output_data_flags |= OUTPUT_DATA_VTK_CLUSTERS;
			output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
		}

		if (verbosity_level == -99 || verbosity_level >= 2)
		{
			if (output_simulation_data_after_each_n_realtime_seconds < 0 && output_timing_flags == OUTPUT_TIMING_NULL)
			{
				output_simulation_data_after_each_n_realtime_seconds = 1.0;
				output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;
			}
			else if (output_timing_flags == OUTPUT_TIMING_NULL)
			{
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				if (output_simulation_data_each_nth_timestep < 0)
					output_simulation_data_each_nth_timestep = 100;
			}

			output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
			output_data_flags |= OUTPUT_DATA_STDOUT_MTPS_PER_FRAME;
		}

		cSimulation->setVerbosityLevel(verbosity_level);

		return true;
	}

	void printParameterInfo(
			int argc,		///< number of specified arguments
			char *argv[]	///< array with arguments
	)
	{
		std::cout << "usage: " << argv[0] << std::endl;

		std::cout << std::endl;
		std::cout << "CONFIGURATION:" << std::endl;
		std::cout << "	-c [string]: xml configuration file" << std::endl;

		std::cout << std::endl;
		std::cout << "VERBOSITY / BACKENDS:" << std::endl;
		std::cout << "	-v [int]: verbose mode (0-10, -99 for tabular output)" << std::endl;
		std::cout << "	-f [int]: output .vtk files each [#nth] timestep" << std::endl;
		std::cout << "	-F [int]: output .vtk files each [n] simulation seconds" << std::endl;

		std::cout << "	-b [int]: output data each [#nth] timestep" << std::endl;
		std::cout << "	-B [double]: output data information each [n] simulation seconds" << std::endl;
		std::cout << "	-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;
		std::cout << "	-p : also write vtk files with partitions" << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << std::endl;
		std::cout << "PARALLELIZATION:" << std::endl;
		std::cout << "	-n [int]: Number of threads to use" << std::endl;
		std::cout << "	-N [int]: Maximum number of cores installed in system" << std::endl;
		std::cout << "	-G [int]: Scalability graph for iPMO" << std::endl;
		std::cout << "	-S [int]: Set thread start affinity. default: -1 (disabled). This number represents the start of the core mapping." << std::endl;
		std::cout << "	-A [int]: Set thread affinities. default: -1 (disabled). This number represents the distance between 2 consecutive cores." << std::endl;
#endif

		std::cout << std::endl;
		std::cout << "GENERAL SIMULATION CONTROL:" << std::endl;
		std::cout << "	-t [int]: timesteps" << std::endl;
		std::cout << "	-L [double]: run for given simulation time" << std::endl;
		std::cout << "	-T [int]: terminate simulation after the given number of" << std::endl;
		std::cout << "			  timesteps was executed consecutively with the same number of triangles" << std::endl;

		std::cout << std::endl;
		std::cout << "SIMULATION SPECIFIC:" << std::endl;

		cSimulation->config_command_line_getArgumentsHelp();
	}


	bool simulation_create()
	{
		/****************************
		 * ALLOCATE SIMULATION - DON'T SETUP FULL SIMULATION SO FAR!
		 */
		cSimulation = new CSimulation(verbosity_level);


		/*
		 * setup xml configuration interface
		 */
		cSimulation->config_xml_file_setupInterface(cXMLConfig);

		return true;
	}

	void mpi_setup()
	{
#if CONFIG_ENABLE_MPI
		MPI_Init(&main_argc, &main_argv);
		MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
		MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
/*
		int max_tag_value, info;
		MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_TAG_UB, &a, &b);

		if (max_tag_value < 1000000)
		{
			std::cerr << "WARNING: Expecting a tag size "
		}*/
#endif
	}

	/**
	 * create simulation and setup
	 */
	bool simulation_setup()
	{
		/****************************
		 * SETUP SIMULATION
		 */

		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
		srand(0);

#if SIMULATION_TSUNAMI_PARALLEL
		cSimulation->simulation_threading_number_of_threads = threading_number_of_threads_to_use;

#if CONFIG_ENABLE_MPI
		cSimulation->simulation_mpi_rank = mpi_rank;
		cSimulation->simulation_mpi_size = mpi_size;
#endif

#endif

#if SIMULATION_TSUNAMI_SERIAL
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			cSimulation->threading_pin_to_core_nr_after_one_timestep = threading_pin_to_core_nr_after_one_timestep;
#endif


		simloop_cStopwatch.start();

		/***************************************************
		 * reset & therefore setup the simulation with flat water surface
		 */

		cSimulation->reset_Simulation();


		/***************************************************
		 * remember initial number of triangles
		 */
		if (verbosity_level > 2)
			std::cout << " + Initial number of triangles after base triangulation: " << cSimulation->number_of_initial_triangles_after_domain_triangulation << std::endl;



		/*****************************************************
		 * setup adaptive simulation
		 */
		cSimulation->setup_GridDataWithAdaptiveSimulation();


		if (verbosity_level >= 5)
			std::cout << "SETUP TIME: " << simloop_cStopwatch.getTimeSinceStart() << std::endl;

		return true;
	}

	/**
	 * output verbose information for CMain and further classes
	 */
	void outputVerboseInformation()
	{
		if (verbosity_level > 2)
		{
			std::cout << " + Number of threads to use: " << getNumberOfThreadsToUse() << std::endl;
			std::cout << " + Max number of threads (system): " << getMaxNumberOfThreads() << std::endl;
			std::cout << " + Verbose: " << verbosity_level << std::endl;
			std::cout << " + Terminate simulation after #n equal timesteps: " << simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation << std::endl;
			std::cout << " + Timesteps: " << simulation_run_for_fixed_timesteps << std::endl;
			std::cout << " + Output .vtk files each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output .vtk files each n simulation seconds: " << output_simulation_data_after_each_n_simulation_seconds << std::endl;
			std::cout << " + Output data each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output data each n computation seconds: " << output_simulation_data_after_each_n_realtime_seconds << std::endl;
			std::cout << " + Output simulation data filename: " << output_simulation_vtk_data_filename << std::endl;
			std::cout << " + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS " << (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS ? "[enabled]" : "[disabled]") << std::endl;
			std::cout << " +" << std::endl;

/*
			std::cout << " + Timestep size: " << cSimulation->simulation_parameter_timestep_size << std::endl;
			std::cout << " + InitialDepth: " << cSimulation->grid_initial_recursion_depth << std::endl;
			std::cout << " + MinDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_min_relative_recursion_depth) << std::endl;
			std::cout << " + MaxDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_max_relative_recursion_depth) << std::endl;
			std::cout << " + WorldSceneId: " << cSimulation->simulation_world_scene_id << std::endl;
			std::cout << " + WaterSurfaceId: " << cSimulation->simulation_water_surface_scene_id << std::endl;
			std::cout << " + TerrainSceneId: " << cSimulation->simulation_terrain_scene_id << std::endl;
			std::cout << " + Cluster size when to request split: " << cSimulation->cluster_split_workload_size << std::endl;
			std::cout << " + Cluster size when to request join (both children have to request a join): " << cSimulation->cluster_join_workload_size << std::endl;
			std::cout << " + Cluster minimum size when to allow split by scan: " << cSimulation->cluster_scan_split_min_workload_size << std::endl;
			std::cout << " + Cluster maximum size when to allow join by scan: " << cSimulation->cluster_scan_join_max_workload_size << std::endl;

			if (cSimulation->cluster_update_split_join_size_after_elapsed_timesteps > 0)
			{
				std::cout << " + elapsed_timesteps_to_update_split_and_join_parameters: " << cSimulation->cluster_update_split_join_size_after_elapsed_timesteps << std::endl;
				std::cout << " + elapsed_timesteps_to_udpate_split_and_join_scalar: " << cSimulation->cluster_update_split_join_size_after_elapsed_scalar << std::endl;
			}

			std::cout << " + Timestep Size: adaptive CFL " << cSimulation->simulation_parameter_cfl << std::endl;
			std::cout << " + Timestep Size: " << cSimulation->simulation_parameter_timestep_size << std::endl;
*/

#if CONFIG_ENABLE_MPI
			std::cout << " + MPI Size: " << mpi_size << std::endl;
			std::cout << " + MPI Rank: " << mpi_rank << std::endl;
#endif
			if (verbosity_level > 2)
				cSimulation->outputVerboseInformation();
		}

	}


	/**
	 * shutdown simulation class
	 */
	void simulation_shutdown()
	{
		delete cSimulation;
	}

	void mpi_shutdown()
	{
#if CONFIG_ENABLE_MPI
		MPI_Finalize();
#endif
	}

	/***************************************************
	 * SIMULATION LOOP
	 */
	CStopwatch simloop_cStopwatch;

	long long simloop_sum_number_of_triangles;
	long long simloop_prev_number_of_triangles;
	long long simloop_sum_number_of_partitions;
	char simloop_char_buffer[1024];

	int simloop_outputVTKFrameCounter;
	int simloop_output_timing_each_nth_timestep_timestep_counter;
	int simloop_frameCounter;

	/*
	 * stopwatches for edge/adaptive/split-join traversals
	 */
	double simloop_edgeCommTime;
	double simloop_adaptiveTime;
	double simloop_splitJoinTime;
	double simloop_otherTime;

	/*
	 * output verbose information after output_verbose_information_after_each_n_seconds seconds
	 */
	double simloop_output_timing_each_nth_real_second__next_timestamp;
	double simloop_output_timing_each_nth_simulation_second__next_timestamp;
	double simloop_output_simulation_info_next_simulation_time;

	bool simloop_finishSimulation;
	int simloop_timestep_nr;

	double output_mtps_for_each_second_old_timestamp;
	long long output_mtps_for_each_second_old_timestep;
	long long output_mtps_for_n_seconds_number_of_processed_triangles;
	double output_mtps_for_simulation_frame_mtps;
	double output_average_cells_for_simulation_frame;



	/**
	 * how is writing of data triggered?
	 */
	enum
	{
		OUTPUT_TIMING_NULL	= 0,

		OUTPUT_TIMING_EACH_NTH_TIMESTEP = 1,
		OUTPUT_TIMING_EACH_NTH_REAL_SECOND = 2,
		OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND = 3,
	};

	int output_timing_flags;



	/**
	 * which data should be written?
	 */
	enum
	{
		OUTPUT_DATA_NULL = 0,

		OUTPUT_DATA_STDOUT_VERBOSE = (1 << 1),
		OUTPUT_DATA_VTK_GRID = (1 << 2),
		OUTPUT_DATA_VTK_CLUSTERS = (1 << 3),
		OUTPUT_DATA_BUOY = (1 << 4),
		OUTPUT_DATA_STDOUT_MTPS_PER_FRAME = (1 << 5)
	};

	int output_data_flags;


	/**
	 * this method is executed at the beginning of each simulation loop.
	 */
	void simulation_loopPrefix()
	{
		setValueNumberOfThreadsToUse(threading_number_of_threads_to_use);
		setMaxNumberOfThreads(threading_max_number_of_threads);

		if (verbosity_level > 1)
		{
			std::cout << "[ START ]" << std::endl;
		}

		simloop_cStopwatch.start();


		/***************************************************
		 * SIMULATION
		 */
		simloop_sum_number_of_triangles = 0;
		simloop_prev_number_of_triangles = 0;
		simloop_sum_number_of_partitions = 0;

		simloop_outputVTKFrameCounter = 0;
		simloop_output_timing_each_nth_timestep_timestep_counter = 0;
		simloop_frameCounter = 0;

		/*
		 * stopwatches for edge/adaptive/split-join traversals
		 */
		simloop_edgeCommTime = 0;
		simloop_adaptiveTime = 0;
		simloop_splitJoinTime = 0;
		simloop_otherTime = 0;

		simloop_output_timing_each_nth_simulation_second__next_timestamp = 0;
		simloop_output_timing_each_nth_real_second__next_timestamp = 0;

		simloop_finishSimulation = false;
		simloop_timestep_nr = 0;

		output_mtps_for_each_second_old_timestamp = -1.0;
		output_mtps_for_each_second_old_timestep = 0;
		output_mtps_for_n_seconds_number_of_processed_triangles = 0;
		output_mtps_for_simulation_frame_mtps = 0;
		output_average_cells_for_simulation_frame = 0;
	}



	/**
	 * simulation loop executing during each particular simulation step
	 *
	 * the separation into header/iteration/footer is necessary to be
	 * allowed to change the number of resources during each simulation when desired.
	 */
	bool simulation_loopIteration()
	{
		/***************************************************
		 * output some data/information?
		 */
		bool outputSimulationData = false;


		if (output_timing_flags != 0)
		{
			/*
			 * OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND)
			{
				if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation->simulation_timestamp_for_timestep)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_simulation_second__next_timestamp += output_simulation_data_after_each_n_simulation_seconds;

					if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation->simulation_timestamp_for_timestep)
					{
						simloop_output_timing_each_nth_simulation_second__next_timestamp = cSimulation->simulation_timestamp_for_timestep;
						std::cout << "WARNING: Timestep size for accurate data output is too small, fixing..." << std::endl;
					}
				}
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_TIMESTEP
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_TIMESTEP)
			{
				/*************************************************************************
				 * verbose information (e. g. vtk files) each nth timestep
				 *************************************************************************
				 * n timesteps are computed when simloop_outputVTKFrameCounter equals 0
				 */
				if (simloop_output_timing_each_nth_timestep_timestep_counter == 0)
				{
					outputSimulationData = true;

					if (verbosity_level > 6)
						cProcessMemoryInformation.outputUsageInformation();
				}

				simloop_output_timing_each_nth_timestep_timestep_counter = (simloop_output_timing_each_nth_timestep_timestep_counter+1) % output_simulation_data_each_nth_timestep;
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_REAL_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_REAL_SECOND)
			{
				double timestamp = simloop_cStopwatch.getTimeSinceStart();
				if (simloop_output_timing_each_nth_real_second__next_timestamp <= timestamp)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_real_second__next_timestamp += output_simulation_data_after_each_n_realtime_seconds;
					if (simloop_output_timing_each_nth_real_second__next_timestamp < timestamp)
					{
						std::cerr << "WARNING: Computation time lag. Simulation is running to slow to output information each " << output_simulation_data_after_each_n_realtime_seconds << " second(s)... fixing verbose output time" << std::endl;
						simloop_output_timing_each_nth_real_second__next_timestamp = timestamp;
					}
				}
			}

			/*
			 * update triangle throughput counter
			 */
			output_mtps_for_n_seconds_number_of_processed_triangles += cSimulation->number_of_triangles;

			simloop_outputVTKFrameCounter = (simloop_outputVTKFrameCounter+1) % output_simulation_data_each_nth_timestep;


			if (outputSimulationData)
			{
#if SIMULATION_TSUNAMI_PARALLEL
				/*
				 * request to output some data.
				 * this is mainly intended to output buoy data.
				 */
				cSimulation->output_bogusData();
#endif

				/*
				 * OUTPUT_DATA_VTK_GRID
				 */
				if (output_data_flags & OUTPUT_DATA_VTK_GRID)
				{
					sprintf(simloop_char_buffer, output_simulation_vtk_data_filename.c_str(), simloop_frameCounter);

					if (verbosity_level > 3)
					{
						std::cout << "=========================================" << std::endl;
						std::cout << "   + writing file " << simloop_char_buffer << std::endl;
					}

					cSimulation->writeTrianglesToVTKFile(simloop_char_buffer, true);


#if SIMULATION_TSUNAMI_PARALLEL
					if (output_data_flags & OUTPUT_DATA_VTK_CLUSTERS)
					{
						std::string partitionFile = "partitions_";
						partitionFile += simloop_char_buffer;

						cSimulation->writeClustersToVTKFile(partitionFile.c_str(), true);
					}
#endif
					simloop_frameCounter++;
				}

				/*
				 * OUTPUT_DATA_STDOUT_VERBOSE
				 */
				if (output_data_flags & OUTPUT_DATA_STDOUT_VERBOSE)
				{
					double timestamp = simloop_cStopwatch.getTimeSinceStart();

					if (output_mtps_for_each_second_old_timestamp == -1)
					{
						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = 0;
					}

					double delta = timestamp - output_mtps_for_each_second_old_timestamp;
					if (delta >= output_simulation_data_after_each_n_simulation_seconds)
					{
						/*
						 * more than output_simulation_data_after_each_n_simulation_seconds second
						 */
						if (output_simulation_data_after_each_n_simulation_seconds >= 0)
							output_mtps_for_simulation_frame_mtps = ((double)output_mtps_for_n_seconds_number_of_processed_triangles / delta)*0.000001;
						if (cSimulation->simulation_timestep_nr != 0)
							output_average_cells_for_simulation_frame = output_mtps_for_n_seconds_number_of_processed_triangles/(double)(cSimulation->simulation_timestep_nr - output_mtps_for_each_second_old_timestep);

						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = cSimulation->simulation_timestep_nr;
						output_mtps_for_n_seconds_number_of_processed_triangles = 0;
					}


					if (verbosity_level == -99)
					{
						/*
						 * create OUTPUT in tabular format
						 */
						if (simloop_timestep_nr == 0)
						{
							/*
							 * print header
							 */
							std::cout << "MTPS\t";
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "MTPS_FRAME\t";
							std::cout << "TIMESTEP\t";
							std::cout << "SIMTIME\t";
							std::cout << "CELLS\t";
							std::cout << "CELLS_FRAME\t";
							std::cout << "MB_PER_TIMESTEP\t";
							std::cout << "TIMESTEP_SIZE\t";
							std::cout << "PARTITIONS\t";
							std::cout << "SPLIT_SIZE\t";
							std::cout << "JOIN_SIZE";
							std::cout << std::endl;
						}

						/*
						 * print rows
						 */
						if (simloop_timestep_nr == 0)
						{
							std::cout << "0" << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "0" << '\t';
						}
						else
						{
							std::cout << ((double)simloop_sum_number_of_triangles/simloop_cStopwatch.getTimeSinceStart())*0.000001 << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << output_mtps_for_simulation_frame_mtps << '\t';
						}

						std::cout << simloop_timestep_nr << '\t';
						std::cout << cSimulation->simulation_timestamp_for_timestep << '\t';
						std::cout << cSimulation->number_of_triangles << '\t';
						std::cout << output_average_cells_for_simulation_frame << '\t';
						double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation->number_of_triangles)/(1024.0*1024.0);
						std::cout << elementdata_megabyte_per_timestep << '\t';
						std::cout << cSimulation->simulation_parameter_timestep_size << '\t';
						std::cout << cSimulation->number_of_simulation_clusters << '\t';
						std::cout << cSimulation->cluster_split_workload_size << '\t';
						std::cout << cSimulation->cluster_join_workload_size;
						std::cout << std::endl;
					}
					else
					{
						std::cout << "=========================================" << std::endl;

						if (simloop_timestep_nr != 0)
						{
							std::cout << "   + " << ((double)simloop_sum_number_of_triangles/simloop_cStopwatch.getTimeSinceStart())*0.000001 << " Overall MTPS (Mega Triangles per second)" << std::endl;
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "   + " << output_mtps_for_simulation_frame_mtps << " Last Second MTPS (Mega Triangles per second)" << std::endl;
						}

						std::cout << "   + " << simloop_timestep_nr << "\tTIMESTEP" << std::endl;
						std::cout << "   + " << cSimulation->simulation_timestamp_for_timestep << "\tSIMULATION_TIME" << std::endl;
						std::cout << "   + " << cSimulation->number_of_triangles << "\tTRIANGLES" << std::endl;
						std::cout << "   + " << simloop_cStopwatch.getTimeSinceStart() << " RT (REAL_TIME)" << std::endl;
						double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation->number_of_triangles)/(1024.0*1024.0);
						std::cout << "   + " << elementdata_megabyte_per_timestep << "\tElementData Megabyte per Timestep (RW)" << std::endl;
						std::cout << "   + " << cSimulation->simulation_parameter_timestep_size << "\tTIMESTEP SIZE" << std::endl;
						std::cout << "   + " << cSimulation->number_of_simulation_clusters << "\tPARTITIONS" << std::endl;
						std::cout << "   + " << simloop_edgeCommTime << "/" << simloop_adaptiveTime << "/" << simloop_splitJoinTime << "\tTIMINGS (edgeComm/adaptive/splitJoin)" << std::endl;
					}
				}
			}
		}


		/*
		 * single simulation simloop_timestep_id
		 */
		if (verbosity_level > 2)
		{
			cSimulation->runSingleTimestepDetailedBenchmarks(
					&simloop_edgeCommTime,
					&simloop_adaptiveTime,
					&simloop_splitJoinTime
				);
		}
		else
		{
			cSimulation->runSingleTimestep();
		}


		/*
		 * increment timestep id
		 */
		simloop_timestep_nr++;


		/*
		 * increment counter of number of triangles processed so far
		 */
		simloop_sum_number_of_triangles += cSimulation->number_of_triangles;
		simloop_sum_number_of_partitions += cSimulation->number_of_simulation_clusters;


		/*
		 * verbose points
		 */
		if (verbosity_level > 5)
		{
			std::cout << "." << std::flush;
		}

		/*
		 * if timesteps are not set, we quit as soon as the initial number of triangles was reached
		 */
		if (simulation_run_for_fixed_timesteps == -1)
		{
			if (simulation_run_for_fixed_simulation_time != -1)
			{
				if (cSimulation->simulation_timestamp_for_timestep >= simulation_run_for_fixed_simulation_time)
					return false;
			}
			else if (simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation != -1)
			{
				static int consecutive_timesteps_with_equal_triangle_number_counter = 0;

				if (simloop_prev_number_of_triangles == cSimulation->number_of_triangles)
				{
					consecutive_timesteps_with_equal_triangle_number_counter++;

					if (consecutive_timesteps_with_equal_triangle_number_counter > simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation)
					{
						std::cout << std::endl;
						std::cout << " + Terminating after " << simulation_terminate_after_n_timesteps_with_equal_number_of_triangles_in_simulation << " timesteps with equal number of triangles -> EXIT" << std::endl;
						std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
						std::cout << std::endl;
						return false;
					}
				}

				simloop_prev_number_of_triangles = cSimulation->number_of_triangles;
			}
#if !COMPILE_SIMULATION_WITH_GUI
			else
			{
				if (cSimulation->number_of_initial_triangles_after_domain_triangulation == cSimulation->number_of_triangles)
				{
					std::cout << std::endl;
					std::cout << " + Initial number of triangles reached -> EXIT" << std::endl;
					std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
					std::cout << std::endl;
					return false;
				}
			}
#endif
		}
		else
		{
			if (simloop_timestep_nr >= simulation_run_for_fixed_timesteps)
				return false;
		}

		return true;
	}


	/**
	 * this method is executed at the end of the simulation
	 */
	void simulation_loopSuffix()
	{
		simloop_cStopwatch.stop();

		if (verbosity_level > 1)
		{
			std::cout << "[ END ]" << std::endl;
			std::cout << std::endl;
		}


		if (verbosity_level > 2)
		{
			std::cout << std::endl;
			std::cout << "Timings for simulation phases:" << std::endl;
			std::cout << " + EdgeCommTime: " << simloop_edgeCommTime << std::endl;
			std::cout << " + AdaptiveTime: " << simloop_adaptiveTime << std::endl;
			std::cout << " + SplitJoinTime: " << simloop_splitJoinTime << std::endl;
			std::cout << std::endl;
		}

		double real_stoptime = simloop_cStopwatch();

		std::cout << simloop_timestep_nr << " TS (Timesteps)" << std::endl;
		std::cout << cSimulation->simulation_timestamp_for_timestep << " ST (SIMULATION_TIME)" << std::endl;
		std::cout << cSimulation->simulation_parameter_timestep_size << " TSS (Timestep size)" << std::endl;
		std::cout << real_stoptime << " RT (REAL_TIME)" << std::endl;
		std::cout << simloop_sum_number_of_triangles << " TP (Triangles processed)" << std::endl;
		std::cout << (double)real_stoptime/(double)simloop_timestep_nr << " ASPT (Averaged Seconds per Timestep)" << std::endl;
		std::cout << (double)simloop_sum_number_of_triangles/(double)simloop_timestep_nr << " TPST (Triangles Processed in Average per Simulation Timestep)" << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << (double)simloop_sum_number_of_partitions/(double)simloop_timestep_nr << " PPST (Partitions Processed in Average per Simulation Timestep)" << std::endl;
#endif

		double MTPS = ((double)simloop_sum_number_of_triangles/real_stoptime)*0.000001;
		std::cout << MTPS << " MTPS (Million Triangles per Second)" << std::endl;
		std::cout << ((double)simloop_sum_number_of_triangles/(simloop_cStopwatch()*(double)threading_number_of_threads_to_use))*0.000001 << " MTPSPT (Million Triangles per Second per Thread)" << std::endl;

		double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)simloop_timestep_nr)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_timestep << " EDMBPT (ElementData Megabyte per Timestep (RW))" << std::endl;

		double elementdata_megabyte_per_second = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)real_stoptime)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_second << " EDMBPS (ElementData Megabyte per Second (RW))" << std::endl;
	}



	virtual ~CMain()
	{
#ifdef DEBUG
		if (!debug_ibase_list.empty())
		{
			std::cout << "MEMORY LEAK: iBase class missing in action" << std::endl;
			std::cout << "  + number of classes: " << debug_ibase_list.size() << std::endl;
		}
#endif

		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
	}

};

#endif
