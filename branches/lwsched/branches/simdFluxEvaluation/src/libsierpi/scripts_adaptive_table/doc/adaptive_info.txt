
Informations about adaptivity:
==============================

general remarks:
the triangles edges are enumerated in anti-clockwise order.
starting from the hypotenuse, the first edge is edge1 and the 2nd one edge2



This implementation is split up into 2 passes:

* The first pass handles the adaptivity. It tags all triangles which have to be
  refined or coarsened.

* The 2nd pass actually inserts the additional edges or removes them.



REFINEMENT:
==========

to avoid hanging nodes, sometimes more than one traversal is necessary to
spread the refinement information forward and backwards.

  0: nothing to do

tags during refinement traversals:
  1: after refinement request from element
     => spread information about refinement to hyp in next traversal if hyp is of type 'old'
  3: refinement request from edge1
     => spread information about refinement to hyp in next traversal if hyp of type 'old'
  4: refinement request from edge2
     => spread information about refinement to hyp in next traversal if hyp of type 'old'
  5: refinement request from edge1 AND edge2.
     => spread information about refinement to hyp in next traversal if hyp is of type 'old'

tags during adaptive restruction
  -1: hyp only refinement
  -3: refinement along edge1 and hyp
  -4: refinement along edge2 and hyp
  -5: refinement along edge1 and edge2 and hyp

edge communication: 1 for refinement request


COARSENING:
==========

since we are lucky guys, the coarsening can be done by looking at 4 adjacent
triangles connected by their katetes. If all triangles agree in the coarsing,
the 4 triangles can be combined to 2 triangles with respect to the traversal
tree.

since all 4 local triangles have to agree in the coarsening, all of them have
to agree. If any triangles doesn't agree, the coarsening information is not spread
via the edges to the new triangles. Since all triangles which can agree to the
coarsening can communicate via the stack within one traversal, one traversal
is enough to know whether a coarsening can be done or not.
however, a 2nd traversal is necessary to spread the information about the
successful coarsening operation.

an important property of the coarsening is, that the information to the adjacent
triangles which have to agree in the coarsening has only to be transported along
cathetus.
the first triangle which initializes the first refinement has 2 cathetus of type
'new'. therefore a coarsening request has to be detected for this type only. the
2 following triangles have cathetus with different types (old/new, without loss
of generality). if a coarsening request was triggered on the old edge, the triangle
has to aggree in this coarsening and forward the appropriate information to the
next one.
the fourth triangle is determined by having 2 cathetus of type 'old'. it's allowed
to be coarsened only, if the previous traversed 3 triangles aggreed in the coarsening.
the backward traversal then spreads the coarsening information back to all the other
triangles if all four triangles aggreed in the coarsening.

tags:
   -6: request for coarsening

edge communication: -1 for coarsening request

even if all triangles have agreed to the coarsening, a refinement information can be
propagated to a coarsened cell after 2 traversals (1xforward, 1xbackward). therefore
everytime if such a coarsened cell is modified, another traversal is necessary to
propagate the information to the cells in the opposite direction of the current
traversal.



"RACE CONDITIONS" (Coarsening <=> Refinement):

* no race conditions are known so far if refinement has a higher priority than coarsening




NOTHING TO DO (so far):
======================
  tag: 0
