/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: July 19, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include <cassert>
#include <netcdf.hh>
#include </home/schreibm/local/include/netcdf.hh>


template <typename T>
class CSimpleNetCDF_Data_private
{
	class CSimpleNetCDF_DataLevel
	{
	public:
		size_t array_size_x;
		size_t array_size_y;

		T out_of_domain_value;

		T *data;

		T domain_start_x;
		T domain_start_y;
		T array_size_x_div_domain_length_x;
		T array_size_y_div_domain_length_y;

		T getTexelFloat2D(
				int i_x,
				int i_y
		)
		{
			if (i_x < 0 || i_x >= array_size_x)
				return out_of_domain_value;

			if (i_y < 0 || i_y >= array_size_y)
				return out_of_domain_value;

			return data[i_x + i_y*array_size_x];
		}


		T getFloat2D(
				T i_x,
				T i_y
		)	{
			T px = (i_x - domain_start_x)*array_size_x_div_domain_length_x;
			T py = (i_y - domain_start_y)*array_size_y_div_domain_length_y;

			int x = (int)(px + (T)0.5);
			int y = (int)(py + (T)0.5);

			return getTexelFloat2D(x, y);
		}


		/**
		 * setup multiresolution level
		 */
		void setup(
				T i_domain_start_x,		///< start x-coordinate of domain
				T i_domain_start_y,		///< start y-coordinate of domain
				T i_domain_length_x,	///< length of domain in x-direction
				T i_domain_length_y		///< length of domain in y-direction
		)	{
			domain_start_x = i_domain_start_x;
			domain_start_y = i_domain_start_y;

			array_size_x_div_domain_length_x = (T)array_size_x/(T)i_domain_length_x;
			array_size_y_div_domain_length_y = (T)array_size_y/(T)i_domain_length_y;
		}
	};


	CSimpleNetCDF_DataLevel *dataLevels;

	int number_of_data_levels;

	T xmin;
	T xmax;

	T ymin;
	T ymax;

	T domain_length_x;
	T domain_length_y;

	T array_size_x_div_domain_length_x;
	T array_size_y_div_domain_length_y;

	T inv_domain_length_x;
	T inv_domain_length_y;


public:

	void setOutOfDomainValue(T i_value)
	{
		for (int i = 0; i < number_of_data_levels; i++)
		{
			dataLevels[i].out_of_domain_value = i_value;
		}
	}

	T getXMin()
	{
		return xmin;
	}

	T getXMax()
	{
		return xmax;
	}

	T getYMin()
	{
		return ymin;
	}

	T getYMax()
	{
		return ymax;
	}

	T getFloat2D(
			T i_x,
			T i_y,
			int i_level
	)
	{
/*
		T px = (i_x - xmin)*array_size_x_div_domain_length_x;
		T py = (i_y - ymin)*array_size_y_div_domain_length_y;

		int x = (int)(px + (T)0.5);
		int y = (int)(py + (T)0.5);
*/
		return dataLevels[number_of_data_levels-1].getFloat2D(i_x, i_y);
	}

	CSimpleNetCDF_Data_private()
	{
		dataLevels = nullptr;
		number_of_data_levels = 0;
	}

	~CSimpleNetCDF_Data_private()
	{
		clear();
	}


	void clear()
	{
		if (dataLevels)
		{
			delete [] dataLevels;
			dataLevels = nullptr;
		}
	}


	/**
	 * read range attribute from given variable
	 */
	bool getRangeFromVariable(
			int ncid,
			int varid,
			T *o_min,
			T *o_max
	)
	{
		int id;
		int retval;
        int xtype;
        size_t len;

        if ((retval = nc_inq_attlen(ncid, varid, "actual_range", &len)) == -1)
        {
        	std::cerr << "attribute 'actual_range' not found: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }

        T *range = new T[len];


        if (sizeof(T) == 4)
        {
			if ((retval = nc_get_att_float(ncid, varid, "actual_range", (float*)&range[0])) == -1)
			{
				std::cerr << "attribute: " << nc_strerror(retval) << std::endl;
				nc_close(ncid);
				return false;
			}
        }
        else
        {
			if ((retval = nc_get_att_double(ncid, varid, "actual_range", (double*)&range[0])) == -1)
			{
				std::cerr << "attribute: " << nc_strerror(retval) << std::endl;
				nc_close(ncid);
				return false;
			}
        }

        *o_min = range[0];
        *o_max = range[1];

        delete [] range;
        return true;
	}


	/**
	 * setup the multiresolution map
	 */
	void setupMultiresolutionMap()
	{
		std::cout << "SETUP MULTIRESOLUTION MAP" << std::endl;


		for (int i = number_of_data_levels-1; i > 0; i--)
		{
//			dataLevels[i].out_of_domain_value = out_of_domain_value;
		}
	}


	/**
	 * load the dataset from a netcdf file
	 *
	 * \return true if everything is ok
	 */
	bool loadDataset(
			const char *i_dataset_filename
	)
	{
		int retval;
		int ncid;

		clear();

        if ((retval = nc_open(i_dataset_filename, NC_NOWRITE, &ncid)) != NC_NOERR)
        {
        	std::cerr << "Failed to open file " << i_dataset_filename << std::endl;
        	return false;
        }

        int ndims, nvars, ngatts, recdim;
        if ((retval = ncinquire(ncid, &ndims, &nvars, &ngatts, &recdim)) == -1)
		{
        	std::cerr << "Failed to inquire information: " << nc_strerror(retval) << std::endl;
            nc_close(ncid);
        	return false;
		}

        if (ndims != 2)
        {
        	std::cerr << "Expected 2 dimensions, got " << ndims << std::endl;
            nc_close(ncid);
        	return false;
        }

        char char_buffer[MAX_NC_NAME];
        long size;

        if ((retval = ncdiminq(ncid, 0, &char_buffer[0], &size)) == -1)
        {
        	std::cerr << "ERROR ncdiminq: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }
        size_t array_size_x = size;

        if ((retval = ncdiminq(ncid, 1, &char_buffer[0], &size)) == -1)
        {
        	std::cerr << "ERROR ncdiminq: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }
        size_t array_size_y = size;


        int var_x_id;
        if ((retval = nc_inq_varid(ncid, "x", &var_x_id)) == -1)
        {
        	std::cerr << "ERROR var x not found: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }

        if (!getRangeFromVariable(ncid, var_x_id, &xmin, &xmax))
        	return false;
        domain_length_x = xmax - xmin;

        int var_y_id;
        if ((retval = nc_inq_varid(ncid, "y", &var_y_id)) == -1)
        {
        	std::cerr << "ERROR var y not found: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }

        if (!getRangeFromVariable(ncid, var_y_id, &ymin, &ymax))
        	return false;
        domain_length_y = ymax - ymin;

        inv_domain_length_x = (T)1.0 / domain_length_x;
        inv_domain_length_y = (T)1.0 / domain_length_y;

        array_size_x_div_domain_length_x = (T)array_size_x / domain_length_x;
        array_size_y_div_domain_length_y = (T)array_size_y / domain_length_y;

        int var_z_id;
        if ((retval = nc_inq_varid(ncid, "z", &var_z_id)) == -1)
        {
        	std::cerr << "ERROR var z not found: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }

        number_of_data_levels = 1;
        int max_size = (array_size_x > array_size_y ? array_size_x : array_size_y);

        while (max_size >> number_of_data_levels)
        	number_of_data_levels++;

        int l = number_of_data_levels-1;
        dataLevels = new CSimpleNetCDF_DataLevel[number_of_data_levels];

        dataLevels[l].array_size_x = array_size_x;
        dataLevels[l].array_size_y = array_size_y;
        dataLevels[l].data = new T[array_size_x*array_size_y];

        if (sizeof(T) == 4)
        {
        	retval =  nc_get_var_float(ncid, var_z_id, (float*)dataLevels[l].data);
        }
        else
        {
        	retval =  nc_get_var_double(ncid, var_z_id, (double*)dataLevels[l].data);
        }

        if (retval == -1)
        {
        	std::cerr << "ERROR while loading variable data: " << nc_strerror(retval) << std::endl;
        	nc_close(ncid);
        	return false;
        }

        dataLevels[l].setup(xmin, ymin, domain_length_x, domain_length_y);

		setOutOfDomainValue(0);

        setupMultiresolutionMap();

        nc_close(ncid);

		return true;
	}
};
