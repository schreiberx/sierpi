/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_PARAMETERS_HPP_
#define CEDGECOMM_TSUNAMI_PARAMETERS_HPP_

#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"


/**
 * common parameters used for edge comm based tsunami simulation
 */
class CEdgeComm_TsunamiParameters
{
public:
	typedef TTsunamiDataScalar T;

	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar cathetus_real_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiSimulationEdgeData boundary_dirichlet;

	/**
	 * set-up boundary condition
	 */
	EBoundaryConditions eBoundaryCondition;

	/**
	 * damping factor when velocity damping boundary condition is used
	 */
	TTsunamiDataScalar eBoundaryConditionVelocityDampingFactor;


	/******************************************************
	 * CFL condition stuff
	 *
	 * TODO: the CFL condition also has to be built-in into the
	 * adaptive traversals (due to refine operations)
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;

	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;


	/**
	 * inner radius for rectangular triangle:
	 *
	 * r = ab/(a+b+c)
	 *
	 * a = i
	 * b = i
	 * c = i sqrt(2)
	 *
	 * r = i^2 / (2i + i sqrt(2)) = i / (2 + sqrt(2))
	 */
	TTsunamiDataScalar incircle_unit_diameter;


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
	/**
	 * refine and coarsen thresholds
	 */
	TTsunamiDataScalar refine_threshold;
	TTsunamiDataScalar coarsen_threshold;
#endif


	/**
	 * setup with default values
	 */
	CEdgeComm_TsunamiParameters()	:
		eBoundaryCondition(BOUNDARY_CONDITION_OUTFLOW),
		eBoundaryConditionVelocityDampingFactor(0.9)
	{
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
		timestep_size = -CMath::numeric_inf<T>();
		cathetus_real_length = -CMath::numeric_inf<T>();
		gravitational_constant = -CMath::numeric_inf<T>();

		incircle_unit_diameter = (TTsunamiDataScalar)(2.0/(2.0+CMath::sqrt(2.0)));


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		refine_threshold = 999999;
		coarsen_threshold = 0;
#endif
	}


	/**
	 * return the reduce value for the traversator
	 */
	inline void storeReduceValue(TTsunamiDataScalar *o_reduceValue)
	{
		*o_reduceValue = cfl_domain_size_div_max_wave_speed;
	}

	/**
	 * return the set-up timestep size
	 */
	inline TTsunamiDataScalar getTimestepSize()
	{
		return timestep_size;
	}




	/**
	 * \brief set the timestep size
	 */
	inline void setTimestepSize(T i_timestep_size)
	{
		timestep_size = i_timestep_size;
	}


	/**
	 * \brief set the gravitation
	 *
	 * WARNING: this value is not always used by the fluxes in the current state!
	 */
	inline void setGravitationalConstant(T i_gravitational_constant)
	{
		gravitational_constant = i_gravitational_constant;
	}


	/**
	 * \brief set the length of a catheti
	 */
	inline void setSquareSideLength(T i_square_side_length)
	{
		cathetus_real_length = i_square_side_length;
	}


	/**
	 * boundary condition
	 */
	void setBoundaryCondition(
			EBoundaryConditions i_eBoundaryCondition
	)
	{
		eBoundaryCondition = i_eBoundaryCondition;
	}


	/**
	 * setup parameter for dirichlet boundary condition
	 */
	void setBoundaryDirichlet(
			const CTsunamiSimulationEdgeData *p_boundary_dirichlet
	)
	{
		boundary_dirichlet = *p_boundary_dirichlet;
	}



	/**
	 * \brief set all parameters for simulation
	 */
	inline void setParameters(
			T i_timestep_size,					///< timestep size
			T i_square_side_length,				///< length of a square (a single catheti)
			T i_gravitational_constant = 9.81	///< gravitational constant
	)
	{
		timestep_size = i_timestep_size;
		gravitational_constant = i_gravitational_constant;
		cathetus_real_length = i_square_side_length;
	}

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2

	inline void setAdaptivityParameters(
			T i_refine_threshold,
			T i_coarsen_threshold
	)
	{
		refine_threshold = i_refine_threshold;
		coarsen_threshold = i_coarsen_threshold;
	}
#endif


#if 1
	/**
	 * \brief compute CFL, update sub-cluster local CFL and return local CFL
	 */
	inline void computeAndUpdateCFLCondition3(
			T i_max_wave_speed1,
			T i_max_wave_speed2,
			T i_max_wave_speed3,

			T i_depth,
			T *o_local_domain_size_div_max_wave_speed
	)
	{
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2, i_max_wave_speed3);

		T cfl_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;

		*o_local_domain_size_div_max_wave_speed = cfl_length/max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(*o_local_domain_size_div_max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}
#endif
};



#endif /* CEDGECOMM_TSUNAMI_CONFIG_HPP_ */
