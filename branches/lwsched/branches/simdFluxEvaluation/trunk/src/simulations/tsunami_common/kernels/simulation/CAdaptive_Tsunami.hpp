/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#include "../../CTsunamiConfig.hpp"



#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CAdaptive_Tsunami_0thOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Tsunami_0thOrder CAdaptive_Tsunami;
	}
}

#else

#include "CAdaptive_Tsunami_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Tsunami_1stOrder CAdaptive_Tsunami;
	}
}

#endif
