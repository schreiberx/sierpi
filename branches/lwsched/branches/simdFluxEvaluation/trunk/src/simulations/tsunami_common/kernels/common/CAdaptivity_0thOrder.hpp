/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "../../CProjections.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_0thOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiSimulationEdgeData CEdgeData;


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationScenarios *cSimulationScenarios;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;


	/**
	 * constructor
	 */
	CAdaptivity_0thOrder()	:
		cathetus_side_length(-1),
		cSimulationScenarios(nullptr),
		cfl_domain_size_div_max_wave_speed(-1)
	{

	}


	void traversal_pre_hook()
	{
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}



	/**
	 * get center of weight for triangle
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPoint(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
#if 0
		// use midpoint on hypotenuse
		*o_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

#else

		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);

#endif
	}



	/**
	 * get center of weight for triangle for both children
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPointForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
#if 0
		// use midpoint on hypotenuse

		*o_left_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_left_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

		*o_right_mx = *o_left_mx;
		*o_right_my = *o_left_my;

#else

		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;

#endif
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			T i_normal_hypx,	T i_normal_hypy,
			T i_normal_rightx,	T i_normal_righty,
			T i_normal_leftx,	T i_normal_lefty,

			int i_depth,

			CTsunamiSimulationCellData *i_cell_data,
			CTsunamiSimulationCellData *o_left_cell_data,
			CTsunamiSimulationCellData *o_right_cell_data
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cell_data->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		assert (cSimulationScenarios != nullptr);

		TTsunamiVertexScalar lmx, lmy, rmx, rmy;

		computeAdaptiveSamplingPointForLeftAndRightChild(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,

				&lmx, &lmy,
				&rmx, &rmy
			);

		if (i_cell_data->dofs_center.h <= 0)
		{
			if (i_cell_data->dofs_center.h < 0)
				std::cout << "Negative h: " << i_cell_data->dofs_center.h << std::endl;

			o_left_cell_data->dofs_center.b = cSimulationScenarios->getTerrainHeightByPosition(lmx, lmy, 0);
			o_left_cell_data->dofs_center.h = CMath::max((T)0.0,  -o_left_cell_data->dofs_center.b);
			o_left_cell_data->dofs_center.hu = 0;
			o_left_cell_data->dofs_center.hv = 0;

			o_right_cell_data->dofs_center.b = cSimulationScenarios->getTerrainHeightByPosition(rmx, rmy, 0);
			o_right_cell_data->dofs_center.h = CMath::max((T)0.0,  -o_right_cell_data->dofs_center.b);
			o_right_cell_data->dofs_center.hu = 0;
			o_right_cell_data->dofs_center.hv = 0;
		}
		else
		{
			T h_horizon = i_cell_data->dofs_center.b + i_cell_data->dofs_center.h;

			o_left_cell_data->dofs_center.b = cSimulationScenarios->getTerrainHeightByPosition(lmx, lmy, 0);
			o_left_cell_data->dofs_center.h = CMath::max((T)0.0,  -o_left_cell_data->dofs_center.b + h_horizon);

			o_right_cell_data->dofs_center.b = cSimulationScenarios->getTerrainHeightByPosition(rmx, rmy, 0);
			o_right_cell_data->dofs_center.h = CMath::max((T)0.0,  -o_right_cell_data->dofs_center.b + h_horizon);


			o_left_cell_data->dofs_center.hu = i_cell_data->dofs_center.hu;
			o_left_cell_data->dofs_center.hv = i_cell_data->dofs_center.hv;

			o_right_cell_data->dofs_center.hu = i_cell_data->dofs_center.hu;
			o_right_cell_data->dofs_center.hv = i_cell_data->dofs_center.hv;

			CProjections::changeBasisWithXAxis(
					&o_left_cell_data->dofs_center.hu,
					&o_left_cell_data->dofs_center.hv,
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);

			CProjections::changeBasisWithXAxis(
					&o_right_cell_data->dofs_center.hu,
					&o_right_cell_data->dofs_center.hv,
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);
		}

		assert(cfl_domain_size_div_max_wave_speed != -1.0);

		T local_cfl_domain_size_div_max_wave_speed = i_cell_data->cfl_domain_size_div_max_wave_speed*(T)(1.0/CMath::sqrt(2.0));

		o_left_cell_data->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_cell_data->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);

#if o_right_cell_data || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_left_cell_data->refine = false;
		o_left_cell_data->coarsen = false;
		o_right_cell_data->refine = false;
		o_right_cell_data->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cell_data->validation.setupRefineLeftAndRight(
				i_normal_hypx,		i_normal_hypy,
				i_normal_rightx,	i_normal_righty,
				i_normal_leftx,		i_normal_lefty,
				i_depth,
				&o_left_cell_data->validation,
				&o_right_cell_data->validation
			);
#endif

	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiSimulationCellData *o_cellData,
			CTsunamiSimulationCellData *i_left_cellData,
			CTsunamiSimulationCellData *i_right_cellData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			T mx = (vleft_x + vright_x)*(T)0.5;
			T my = (vleft_y + vright_y)*(T)0.5;

			i_left_cellData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_cellData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif


		assert (cSimulationScenarios != nullptr);

		T mx, my;

		computeAdaptiveSamplingPoint(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				&mx, &my
			);
#if 1
		TTsunamiVertexScalar b = (i_left_cellData->dofs_center.b + i_right_cellData->dofs_center.b)*(T)0.5;
		o_cellData->dofs_center.b = b;

		T sum_horizon = 0;
		T sum_hu = 0;
		T sum_hv = 0;
		T sum_div = 0;

		T tmp_hu = 0;
		T tmp_hv = 0;

		if (i_left_cellData->dofs_center.h > 0)
		{
			sum_horizon += i_left_cellData->dofs_center.h + i_left_cellData->dofs_center.b;

			tmp_hu = i_left_cellData->dofs_center.hu;
			tmp_hv = i_left_cellData->dofs_center.hv;

			/*
			 * project from right child to coarse cell basis
			 *
			 *      /|\
			 *    /  |  \
			 *  /    | R  \
			 * ------|------
			 */
			CProjections::changeBasisWithXAxis(
					&tmp_hu,
					&tmp_hv,
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);

			sum_hu += tmp_hu;
			sum_hv += tmp_hv;
			sum_div++;
		}

		if (i_right_cellData->dofs_center.h > 0)
		{
			sum_horizon += i_right_cellData->dofs_center.h + i_right_cellData->dofs_center.b;

			tmp_hu = i_right_cellData->dofs_center.hu;
			tmp_hv = i_right_cellData->dofs_center.hv;

			/*
			 * project from left child to coarse cell basis
			 *
			 *
			 * |\
			 * |  \
			 * | L  \
			 * |------
			 * |    /
			 * |  /
			 * |/
			 */
			CProjections::changeBasisWithXAxis(
					&tmp_hu,
					&tmp_hv,
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);

			sum_hu += tmp_hu;
			sum_hv += tmp_hv;

			sum_div++;
		}

		if (sum_div == 0)
		{
			o_cellData->dofs_center.h = 0;
			o_cellData->dofs_center.hu = 0;
			o_cellData->dofs_center.hv = 0;
		}
		else
		{
			o_cellData->dofs_center.h = sum_horizon/sum_div - o_cellData->dofs_center.b;

			if (o_cellData->dofs_center.h <= 0)
			{
				o_cellData->dofs_center.h = 0;
				o_cellData->dofs_center.hu = 0;
				o_cellData->dofs_center.hv = 0;
			}
			else
			{
				o_cellData->dofs_center.hu = sum_hu/sum_div;
				o_cellData->dofs_center.hv = sum_hv/sum_div;

				// TODO: REMOVE ME
				o_cellData->dofs_center.hu = 0;
				o_cellData->dofs_center.hv = 0;
			}
		}

		o_cellData->cfl_domain_size_div_max_wave_speed = (i_left_cellData->cfl_domain_size_div_max_wave_speed + i_right_cellData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*CMath::sqrt(2.0));


#else
		TTsunamiVertexScalar b = (i_left_cellData->dofs_center.b + i_right_cellData->dofs_center.b)*(T)0.5;

		o_cellData->dofs_center.b = b;

		o_cellData->dofs_center.h = (i_left_cellData->dofs_center.h + i_right_cellData->dofs_center.h)*(T)0.5 + (b - o_cellData->dofs_center.b);
		o_cellData->dofs_center.h = CMath::max((T)0.0, o_cellData->dofs_center.h);
		o_cellData->dofs_center.hu = (i_left_cellData->dofs_center.hu + i_right_cellData->dofs_center.hu)*(T)0.5;
		o_cellData->dofs_center.hv = (i_left_cellData->dofs_center.hv + i_right_cellData->dofs_center.hv)*(T)0.5;

		o_cellData->cfl_domain_size_div_max_wave_speed = (i_left_cellData->cfl_domain_size_div_max_wave_speed + i_right_cellData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*CMath::sqrt(2.0));
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_cellData->refine = false;
		o_cellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_cellData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_cellData->validation,
				&i_right_cellData->validation
		);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
