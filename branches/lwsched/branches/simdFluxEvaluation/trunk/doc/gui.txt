GUI keystrokes:
--------------

	a,s,d,w: forward/backward

	1-9: set used number of threads
	x,z: change world scene
	q: quit
	e: soft-reset
	r: reset

	o: switch visualization (only available with parallel visualization)
	p: display/hide sub-cluster borders (in red color)



Mouse:
-----

	Wheel up/down: Zoom in/out
	Left mouse button: Click&move mouse to rotate view
	Right mouse button: Set elements data to specific value
