#! /bin/bash

. ../tools.sh

PROBLEM_SIZES_IN_DEPTH=`seq 12 1 20`

EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

# no adaptive refinement
#PARAMS="-a 0"
PARAMS="-u 10 -U 10"


echo "CPUS: $CPULIST"

PROCLIST=$(echo $CPULIST | sed "y/ /,/")

echo "Problem size with different number of CPUs using function optimization"


for p in $PROBLEM_SIZES_IN_DEPTH; do
	echo
	echo "THREADS	INITIAL_DEPTH	MTPS	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
	for THREADS in $THREADLIST; do
		PARAMS_=" -d $p -n $THREADS $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT="`$EXEC_CMD`"

		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo "\$$VARNAME")
		SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

		AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
		AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
		TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
		TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`

		echo "$THREADS	$p	$MTPS	$SPEEDUP_PER	$TIMESTEPS	$TIMESTEP_SIZE	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
	done
done
