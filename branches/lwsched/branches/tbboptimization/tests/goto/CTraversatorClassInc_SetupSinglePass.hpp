/*
 * CTraversator_Setup2Pass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: schreibm
 */


#include "CTraversatorClassInc_DefaultInclude.hpp"

/**
 * idx of method to execute for given triangleFactory
 */
int recursionMethodForwardPassId;

/**
 * The triangle factory for this traversal
 */
CTriangle_Factory triangleFactory;

/**
 * Setup the parameters with the one by the parent partition
 */
void setup_ChildPartition(
		TThisClass &p_parent,					///< information of parent traversator
		CTriangle_Factory &p_triangleFactory	///< triangle factory
)
{
	// make sure that this is really a root node
	assert(triangleFactory.partitionTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	recursionTable = p_parent.recursionTable;

	triangleFactory = p_triangleFactory;

	if (triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_FIRST_CHILD)
	{
		recursionMethodForwardPassId = p_parent.recursionTable[p_parent.recursionMethodForwardPassId].first_child_method_type_to_k_and_old_to_new_idx;
	}
	else
	{
		assert(triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD);
		recursionMethodForwardPassId = p_parent.recursionTable[p_parent.recursionMethodForwardPassId].second_child_method_type_to_k_and_old_to_new_idx;
	}

	kernelClass.setup(p_parent.kernelClass);
}



/**
 * Setup the parameters by using the parameters of a child
 */
void setup_ParentPartition(
		TThisClass &p_childPartition,			///< Information of parent traversator
		CTriangle_Factory &p_triangleFactory	///< New triangle factory of parent partition
)
{
	recursionTable = p_childPartition.recursionTable;

	triangleFactory = p_triangleFactory;

	if (p_triangleFactory.recursionDepthFirstRecMethod == 0)
	{
		/*
		 * this is a root partition -> fix the partitionTreeNodeType
		 */
		triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_ROOT_TRIANGLE;
	}

	/**
	 * JOINING TWO TRIANGLES IS VERY EXPENSIVE!!!!!!!!!!!!
	 *
	 * TODO: Improve the search for an appropriate triangle (e. g. using a tree)
	 */
	recursionMethodForwardPassId = getRecursiveFunctionTableIndex(p_triangleFactory);

	kernelClass.setup(p_childPartition.kernelClass);
}



/**
 * setup the initial partition traversal for the given factory
 */
void setup_RootPartition(
		CTriangle_Factory &p_triangleFactory	///< triangle factory to find the first method
)
{
	triangleFactory = p_triangleFactory;

	setupRecursionTable();

	// make sure that this is really a root node
	assert(triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_ROOT_TRIANGLE);

	recursionMethodForwardPassId = getRecursiveFunctionTableIndex(p_triangleFactory);
}
