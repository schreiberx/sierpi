/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSETUP_TSUNAMI_ELEMENTDATA_VALIDATION_HPP_
#define CSETUP_TSUNAMI_ELEMENTDATA_VALIDATION_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData_Depth.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * setup the validation data included in the CTsunamiElementData to the according
 * vertex coordinates.
 */
class CSetup_TsunamiElementData_Validation
{
public:
	typedef TTsunamiVertexScalar TVertexScalar;
	typedef CTsunamiElementData TElementData;

	typedef sierpi::travs::CTraversator_VertexCoords_ElementData_Depth<CSetup_TsunamiElementData_Validation, CTsunamiSimulationStacks> TRAV;

public:

	inline CSetup_TsunamiElementData_Validation()
	{
	}

	virtual inline ~CSetup_TsunamiElementData_Validation()
	{
	}


	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			int i_depth,
			CTsunamiElementData *io_cElementData
		)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		io_cElementData->validation.set(vx1, vy1, vx2, vy2, vx3, vy3, i_depth);
#endif
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}


	inline void setup_WithKernel(
			CSetup_TsunamiElementData_Validation &parent
	)
	{
	}

};

}
}

#endif
