/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData_Depth.hpp"
#include "../common/CAdaptivity_0thOrder.hpp"
#include "../../CTsunamiSimulationDataSets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_ElementData_0thOrder	: public CAdaptivity_0thOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_ElementData_Depth<CSetup_ElementData_0thOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiDataScalar T;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;


	/*
	 * callback for terrain data
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	/*
	 * use max function to set up height
	 */
	bool useMaxFunctionForHeight;


	CSetup_ElementData_0thOrder()	:
		cSimulationDataSets(nullptr),
		useMaxFunctionForHeight(false)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void elementAction(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			int i_depth,

			CTsunamiElementData *io_elementData
	)
	{
		TVertexScalar mx, my;

		computeAdaptiveSamplingPoint(
			vleft_x, vleft_y,
			vright_x, vright_y,
			vtop_x, vtop_y,
			&mx, &my
		);

		io_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(mx, my, i_depth);


		if (!useMaxFunctionForHeight)
		{
			io_elementData->dofs.h = -io_elementData->dofs.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth);
		}
		else
		{
			// fix height with bathymetry displacement
			TTsunamiDataScalar oldb = io_elementData->dofs.b;
			io_elementData->dofs.h += (oldb - io_elementData->dofs.b);

			io_elementData->dofs.h = CMath::max(io_elementData->dofs.h, -io_elementData->dofs.b + cSimulationDataSets->getWaterSurfaceHeightByPosition(mx, my, i_depth));
		}

		io_elementData->cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void setup_Parameters(
			CTsunamiSimulationDataSets *i_cSimulationDataSets,
			bool i_useMaxFunctionForHeight = false
	)
	{
		cSimulationDataSets = i_cSimulationDataSets;
		useMaxFunctionForHeight = i_useMaxFunctionForHeight;
	}



	void setup_WithKernel(
			CSetup_ElementData_0thOrder<TElementData, TSimulationStacks> &parent
	)
	{
		cSimulationDataSets = parent.cSimulationDataSets;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
