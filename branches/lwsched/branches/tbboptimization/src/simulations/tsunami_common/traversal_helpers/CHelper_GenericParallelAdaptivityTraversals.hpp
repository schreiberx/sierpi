/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelAdaptivityTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_

#include <stdint.h>
#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libmath/CMath.hpp"

/**
 * \brief this class implements helper methods which abstract the different phases for adaptive traversals
 *
 * in particular:
 *  a) first forward traversal
 *  b) finished?
 *    c) edge communication
 *    d) backward traversal
 *       forward traversal
 *    finished? not->goto (b)
 *  e) last traversal
 */
class CHelper_GenericParallelAdaptivityTraversals
{
public:
	/**
	 * run the adaptive method
	 */
	template<
		typename CPartition_TreeNode_,
		typename CSimulation_Parallel_SubPartitionHandler,
		typename CAdaptiveTraversatorClass,
		typename CStackAccessors,
		typename TCFLReduceValue
	>
	static void action(
			CAdaptiveTraversatorClass CSimulation_Parallel_SubPartitionHandler::*p_adaptiveTraversator_SubClass,
			CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> CSimulation_Parallel_SubPartitionHandler::*p_simulationExchangeEdgeComm_SubClass,
			CGenericTreeNode<CPartition_TreeNode_> *p_node,

			unsigned long long i_partition_split_workload_size,
			unsigned long long i_partition_join_workload_size,

			unsigned long long *o_number_of_triangles,
			TCFLReduceValue *o_cfl_domain_size_div_max_wave_speed
	)
	{
		/**
		 * if this variable is set to true, there are still some hanging nodes to be deleted.
		 */
		bool remaining_hanging_node;

		/****************************************************
		 * FIRST PASS
		 ****************************************************/
		/**
		 * the first pass is executed to mark the corresponding triangles for begin split or coarsened.
		 * also edge refinement/coarsen information is pushed/pulled to the stack.
		 *
		 * finally, the data for the adjacent partitions is stored to the stacks and need to be exchanged.
		 */


		/**
		 * the first pass checks, whether a triangle should be refined or coarsened.
		 * therefore the element data is necessary. After this step, the elementdoTraversal_SimulationHandler_SubClass_Method_Reduce
		 * data has not to be touched in the middle passes.
		 */
		// first pass: setting all edges to type 'new' creates data to exchange with neighbors
		p_node->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_TreeNode_ *node, bool *o_reduceValue)
				{
					*o_reduceValue = (node->cSimulation_SubPartitionHandler->*(p_adaptiveTraversator_SubClass)).actionFirstTraversal(node->cStacks);
				},
			&(CReduceOperators::OR<bool>),
			&remaining_hanging_node
		);

		/****************************************************
		 * MIDDLE PASSES
		 ****************************************************/
		/**
		 * this is the loop over all partitions to avoid all hanging nodes
		 *
		 * there's already some data prepared on the stack from the first pass.
		 */
		int c = 0;
		while (remaining_hanging_node)
		{
			if (c > 10)
			{
				std::cout << "Warning: more than " << c << " adaptive middle traversals" << std::endl;
			}

			/*
			 * WARNING: moving pullEdgeCommData() to the next parallel reduce  traversal does not work!!!
			 *
			 * Since both middle traversals (forward/backward) write to the edgeComm but
			 * _ALSO_ the exchangeEdgeComm stacks, pulling the edge comm data within the next traversal is not allowed!
			 */

			// first pass: setting all edges to type 'new' creates data to exchange with neighbors
			p_node->traverse_SimulationPartitionHandler_Parallel(
					[=](CSimulation_Parallel_SubPartitionHandler *worker)
					{
						(worker->*p_simulationExchangeEdgeComm_SubClass).pullEdgeCommData();
					}
			);


			/*
			 * The second traversal actually exists of 2 passes:
			 *
			 * 1) The first BACKWARD TRAVERSAL reads data from the adjacent partitions.
			 *    all partition borders are set t type 'old'
			 *
			 * 2) The second FORWARD TRAVERSAL spreads data to adjacent partitions.
			 *    if callback_action_Adaptive_Tsunami_SecondPass returns true, there are
			 *    still refinements to do and we rerun this loop.
			 *    then all partition borders are set to type 'new'
			 */
			p_node->traverse_PartitionTreeNode_Reduce_Parallel(
					[=](CPartition_TreeNode_ *node, bool *o_reduceValue)
					{
						*o_reduceValue = (node->cSimulation_SubPartitionHandler->*(p_adaptiveTraversator_SubClass)).actionMiddleTraversals_Parallel(node->cStacks);
					},
				&(CReduceOperators::OR<bool>),
				&remaining_hanging_node
			);

			c++;
		}

		/****************************************************
		 * LAST PASS
		 ****************************************************/
		/**
		 * after we have the states to create a regular grid, the state-refinement information is stored on the stack.
		 *
		 * !!! this information is used later on to change the information about the adjacent partitions !!!
		 *
		 * this traversal do the actual refinement
		 */


		class CReduceDataClass
		{
		public:
			unsigned long long number_of_triangles;
			TCFLReduceValue cfl_domain_size_div_max_wave_speed;

			static void reduceOperator(
					const CReduceDataClass &i_reduceClass1,
					const CReduceDataClass &i_reduceClass2,
					CReduceDataClass *o_reduceClass
			)
			{
				o_reduceClass->number_of_triangles = i_reduceClass1.number_of_triangles + i_reduceClass2.number_of_triangles;
				o_reduceClass->cfl_domain_size_div_max_wave_speed = CMath::min<TCFLReduceValue>(i_reduceClass1.cfl_domain_size_div_max_wave_speed, i_reduceClass2.cfl_domain_size_div_max_wave_speed);
			}

		} reduceDataClass;


		p_node->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_TreeNode_ *i_node, CReduceDataClass *o_reduceDataClass)
				{
					CSimulation_Parallel_SubPartitionHandler *worker = i_node->cSimulation_SubPartitionHandler;
					CAdaptiveTraversatorClass &adaptiveSubClass = (worker->*(p_adaptiveTraversator_SubClass));
					CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> &simulationEdgeCommSubClass = (worker->*(p_simulationExchangeEdgeComm_SubClass));

					/**
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent partition, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							i_node->cStacks,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					unsigned long long number_of_triangles = i_node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

					if (i_partition_split_workload_size != 0)
					{
						if (number_of_triangles >= i_partition_split_workload_size)
						{
//							assert(i_partition_split_workload_size >= 2);
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
						}
						else if (number_of_triangles < i_partition_join_workload_size)
						{
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
						}
						else
						{
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::NO_OPERATION;
						}
					}

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&i_node->cStacks->adaptive_comm_edge_stacks.left,
							&i_node->cStacks->adaptive_comm_edge_stacks.right,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation,
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					i_node->cStacks->adaptive_comm_edge_stacks.clear();


					o_reduceDataClass->number_of_triangles = number_of_triangles;
					TCFLReduceValue reduceValue;
					adaptiveSubClass.storeReduceValue(&reduceValue);

					if (reduceValue == CMath::numeric_inf<TCFLReduceValue>())
					{
						/*
						 * if the CFL value was not updated, use the CFL value from edge comm.
						 */
						reduceValue = i_node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm;
					}
					else
					{
//						std::cout << i_node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm << " " << reduceValue << std::endl;
						/*
						 * if the CFL value was updated, find the minimum.
						 */
						reduceValue =
							CMath::min(
									i_node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm,
									reduceValue
								);
					}

					o_reduceDataClass->cfl_domain_size_div_max_wave_speed = reduceValue;
					i_node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_adaptivity = reduceValue;
			},

			&CReduceDataClass::reduceOperator,
			&reduceDataClass
		);

		*o_number_of_triangles = reduceDataClass.number_of_triangles;
		*o_cfl_domain_size_div_max_wave_speed = reduceDataClass.cfl_domain_size_div_max_wave_speed;
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
