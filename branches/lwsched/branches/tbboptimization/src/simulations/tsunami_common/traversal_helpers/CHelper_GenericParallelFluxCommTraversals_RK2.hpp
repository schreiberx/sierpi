/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK2_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK2_HPP_


#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CPartition_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals_RK2
{
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_SubPartitionHandler> CPartition_TreeNode_;

public:


	template<
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename TEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue,			/// value to use for reduction
		int i_rungeKuttaPass
	>
	static void singleAction(
			TFluxCommTraversator CSimulationTsunami_Parallel_SubPartitionHandler::*i_simulationSubClass,
			CPartition_ExchangeFluxCommData<CPartition_TreeNode_, TEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulationTsunami_Parallel_SubPartitionHandler::*i_simulationFluxCommSubClass,
			CGenericTreeNode<CPartition_TreeNode_> *i_node,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_node->traverse_PartitionTreeNode_Parallel(
				[=](CPartition_TreeNode_ *i_node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *subPartitionHandler = i_node->cSimulation_SubPartitionHandler;

					if (i_rungeKuttaPass == 1)
					{
						// allocate element stacks
						if (i_node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() > subPartitionHandler->cStackElementDataRK2_f_t0.getMaxNumberOfElements())
						{
							// resize cStackElementDataRK2 if all element data stack elements don't fit into it
							subPartitionHandler->cStackElementDataRK2_f_t0.resize(i_node->cStacks->element_data_stacks.forward.getMaxNumberOfElements());
							subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1.resize(i_node->cStacks->element_data_stacks.forward.getMaxNumberOfElements());

							// TODO: should we also shrink the stacks?!?
							// then this stacks should be moved to the simulation stacks class
						}

						// backup element data
						i_node->cStacks->element_data_stacks.forward.copyStackElements(&(subPartitionHandler->cStackElementDataRK2_f_t0));

						// store results to cStackElementData1stEulerUpdatesRK2
						i_node->cStacks->element_data_stacks.forward.swap(subPartitionHandler->cStackElementDataRK2_f_t0);


						/*
						 * y~ = y0 + h f(t0, y0)
						 *
						 * f(t0, y0) is computed and stored in cStackElementDataRK2_1stEulerUpdates
						 */
					}

					if (i_rungeKuttaPass == 2)
					{
						// store results to cStackElementData1stEulerUpdatesRK2
						i_node->cStacks->element_data_stacks.forward.swap(subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1);
					}

					// store updates (slope) only
					(subPartitionHandler->*(i_simulationSubClass)).actionFirstPass(i_node->cStacks, 1);
				}
		);



		/*
		 * Compute fluxes using uniqueIDs to avoid double flux evaluation:
		 *
		 * The sub-partition with the relation 'uniqueID < adjacentUniqueID' is responsible to compute the fluxes
		 * also in a writing manner for the adjacent one.
		 *
		 * in FLUX COMM PASS:
		 * 1) The responsible sub-partition first fetches the data from the adjacent partition
		 *    to the exchange edge comm data stacks.
		 *
		 * 2) The fluxes are computed for all fetched edge communication data by the responsible sub-partition.
		 *
		 * in SECOND PASS:
		 * 3) Storing the fluxes to the local_edge_comm_data_stack and exchange_edge_comm_data_stack, pulling
		 *    the edge communication data from the sub-partitions with 'uniqueID > adjacentUniqueID' fetches the
		 *    already computed fluxes.
		 */
		i_node->traverse_PartitionTreeNode_Parallel(
				[=](CPartition_TreeNode_ *i_node)
				{
					// pull edge data only in one direction using uniqueIDs as relation and compute the flux
					(i_node->cSimulation_SubPartitionHandler->*(i_simulationFluxCommSubClass)).pullEdgeCommData_1stPass();

				}
		);

		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent partitions
		 */
		if (i_rungeKuttaPass == 1)
		{
			i_node->traverse_PartitionTreeNode_Parallel(
				[=](CPartition_TreeNode_ *i_node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *subPartitionHandler = i_node->cSimulation_SubPartitionHandler;

					// firstly, pull edge communication data from adjacent sub-partitions
					(subPartitionHandler->*(i_simulationFluxCommSubClass)).pullEdgeCommData_2ndPass();

					// run computation based on newly set-up stacks
					TTsunamiDataScalar o_dummy_reduceValue;
					(subPartitionHandler->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, i_timestep_size, &o_dummy_reduceValue);

					// restore swapped stacks
					i_node->cStacks->element_data_stacks.forward.swap(subPartitionHandler->cStackElementDataRK2_f_t0);

					/*
					 * y~ = y0 + h * f(t0, y0)
					 *
					 * compute y~:
					 */

					size_t elementsOnStack = subPartitionHandler->cStackElementDataRK2_f_t0.getNumberOfElementsOnStack();

					subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1.setStackElementCounter(elementsOnStack);

					for (size_t i = 0; i < elementsOnStack; i++)
					{
						TFluxCommTraversator::CKernelClass::computeElementDataRK2_1stStep(
								i_node->cStacks->element_data_stacks.forward.getElementAtIndex(i),		// y_0
								subPartitionHandler->cStackElementDataRK2_f_t0.getElementAtIndex(i),	// f(t0, y0)
								subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1.getElementPtrAtIndex(i), // y~ = y0 + h f(t0, y0)
								i_timestep_size
							);
					}
				}
			);
		}



		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent partitions
		 */
		if (i_rungeKuttaPass == 2)
		{
			i_node->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_TreeNode_ *i_node, TReduceValue *o_reduceValue)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *subPartitionHandler = i_node->cSimulation_SubPartitionHandler;

					// firstly, pull edge communication data from adjacent sub-partitions
					(subPartitionHandler->*(i_simulationFluxCommSubClass)).pullEdgeCommData_2ndPass();

					// run computation based on newly set-up stacks
					(subPartitionHandler->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, i_timestep_size, o_reduceValue);

					subPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm = *o_reduceValue;


					// restore swapped stacks
					i_node->cStacks->element_data_stacks.forward.swap(subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1);


					/*
					 * apply update
					 */
					size_t elementsOnStack = subPartitionHandler->cStackElementDataRK2_f_t0.getNumberOfElementsOnStack();

					TTsunamiDataScalar cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<TTsunamiDataScalar>();

					for (size_t i = 0; i < elementsOnStack; i++)
					{
						CTsunamiElementData *e = i_node->cStacks->element_data_stacks.forward.getElementPtrAtIndex(i);

						TFluxCommTraversator::CKernelClass::computeElementDataRK2_2ndStep(
								subPartitionHandler->cStackElementDataRK2_f_t0.getElementAtIndex(i),
								subPartitionHandler->cStackElementDataRK2_yapprox_t1_AND_f_t1.getElementAtIndex(i),
								e,
								i_timestep_size
							);

						cfl_domain_size_div_max_wave_speed = CMath::min(
								cfl_domain_size_div_max_wave_speed,
								e->cfl_domain_size_div_max_wave_speed
								);

					}

					subPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm = cfl_domain_size_div_max_wave_speed;

					*o_reduceValue = cfl_domain_size_div_max_wave_speed;
				},
				&(CReduceOperators::MIN<TReduceValue>),	// use minimum since the minimum timestep has to be selected
				o_cfl_reduce_value
			);
		}
	}

	/**
	 * run the edge comm traversals
	 */
	template<
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename TEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue			/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulationTsunami_Parallel_SubPartitionHandler::*i_simulationSubClass,
			CPartition_ExchangeFluxCommData<CPartition_TreeNode_, TEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulationTsunami_Parallel_SubPartitionHandler::*i_simulationFluxCommSubClass,
			CGenericTreeNode<CPartition_TreeNode_> *i_node,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		singleAction
			<
				TFluxCommTraversator,	/// Traversator including kernel
				TEdgeData,				/// type of edge communication data
				CStackAccessors,		/// accessors to adjacent stacks
				TTimestepSize,			/// size of time-step to set-up
				TReduceValue,			/// value to use for reduction
				1						// first pass: compute updates for explicit euler
			>(i_simulationSubClass, i_simulationFluxCommSubClass, i_node, i_timestep_size, o_cfl_reduce_value);

		singleAction
			<
				TFluxCommTraversator,	/// Traversator including kernel
				TEdgeData,				/// type of edge communication data
				CStackAccessors,		/// accessors to adjacent stacks
				TTimestepSize,			/// size of time-step to set-up
				TReduceValue,			/// value to use for reduction
				2						// second pass: compute updates using explicit euler at (t_{i+1}, y_{i+1})
			>(i_simulationSubClass, i_simulationFluxCommSubClass, i_node, i_timestep_size, o_cfl_reduce_value);
	}
};

#endif
