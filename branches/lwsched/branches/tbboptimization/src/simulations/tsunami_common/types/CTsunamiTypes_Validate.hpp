/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_TYPES_VALIDATE_HPP
#define CTSUNAMI_TYPES_VALIDATE_HPP

#include "CValidation_EdgeData.hpp"
#include "CValidation_ElementData.hpp"
#include "CValidation_VertexData.hpp"

#endif
