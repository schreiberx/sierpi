/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../kernels/simulation/CEdgeComm_Tsunami.hpp"


namespace sierpi
{
namespace travs
{


/**
 * todo: avoid public - maybe using typedef
 */
class CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private	:
	public CFluxComm_Normals_Depth<sierpi::kernels::CEdgeComm_Tsunami>
{
};

CSpecialized_Tsunami_EdgeComm_Normals_Depth::CSpecialized_Tsunami_EdgeComm_Normals_Depth()
{
	generic_traversator = new CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private;
	cKernelClass = &(generic_traversator->cKernelClass);
}


CSpecialized_Tsunami_EdgeComm_Normals_Depth::~CSpecialized_Tsunami_EdgeComm_Normals_Depth()
{
	delete generic_traversator;
}

void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionFirstPass(
		CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks,
		int i_updateScheme
)
{
	generic_traversator->action_FirstPass(p_cSimulationStacks);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionSecondPass_Serial(
		CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks,
		TTsunamiDataScalar i_timestep_size,
		CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private::TReduceValue *o_reduceValue
)
{
	generic_traversator->action_SecondPass_Serial(p_cSimulationStacks, i_timestep_size, o_reduceValue);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionSecondPass_Parallel(
		CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks,
		TTsunamiDataScalar i_timestep_size,
		CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private::TReduceValue *o_reduceValue
)
{
	generic_traversator->action_SecondPass_Parallel(p_cSimulationStacks, i_timestep_size, o_reduceValue);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setParameters(
		TTsunamiDataScalar p_delta_timestep,
		TTsunamiDataScalar p_square_side_length,
		TTsunamiDataScalar p_gravity
)
{
	generic_traversator->cKernelClass.setParameters(p_delta_timestep, p_square_side_length, p_gravity);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setup_Partition(
		CSpecialized_Tsunami_EdgeComm_Normals_Depth &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Partition(*(parent.generic_traversator), p_triangleFactory);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_sfcMethods(p_triangleFactory);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setBoundaryDirichlet(
		const CTsunamiEdgeData *i_value
)
{
	generic_traversator->cKernelClass.setBoundaryDirichlet(i_value);
}


TTsunamiDataScalar CSpecialized_Tsunami_EdgeComm_Normals_Depth::getTimestepSize()
{
	return generic_traversator->cKernelClass.getTimestepSize();
}


}
}
