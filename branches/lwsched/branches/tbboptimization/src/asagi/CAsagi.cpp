/*
 * CAsagi.cpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#include "CAsagi.hpp"
#include <assert.h>


CAsagi::CAsagi(const char *filename)
{
	int argc = 1;
	char **argv;
	char *dummy[1];
	argv = dummy;
	argv[0] = (char*)"sierpi";

	MPI_Init(&argc, &argv);

	int size = 1;
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	assert(size == 1);

	singleton = asagi::Grid::create(asagi::Grid::FLOAT);

	if (!singleton->open(filename))
	{
		std::cerr << "It's Rettichs fault! He's not able to meat!" << std::endl;
		exit(-1);
	}
	std::cout << "YEHAA! it's working" << std::endl;
}

CAsagi::~CAsagi()
{
	delete singleton;
	MPI_Finalize();
}

asagi::Grid *CAsagi::singleton;



CTsunamiSimulationTypes::TVertexScalar getAsagiElementBathymetryMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar size
	)
{
	typedef CTsunamiSimulationTypes::TVisualizationVertexScalar T;

	T minx = CAsagi::singleton->getXMin();
	T miny = CAsagi::singleton->getYMin();

	T maxx = CAsagi::singleton->getXMax();
	T maxy = CAsagi::singleton->getYMax();

	mx = (mx+1.0)*0.5;
	my = (my+1.0)*0.5;

	assert(mx >= 0);
	assert(my >= 0);
	assert(mx <= 1);
	assert(my <= 1);

	return CAsagi::singleton->getFloat(mx * maxx - minx*(mx - 1.0), my * maxy - miny*(my - 1.0));
}
