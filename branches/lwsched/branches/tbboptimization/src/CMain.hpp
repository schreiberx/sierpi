/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 17. April 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CMAIN_HPP
#define CMAIN_HPP

#include <iostream>
#include "config.h"
#include "lib/iRef.hpp"
#include "simulations/CSimulation.hpp"
#include "lib/CProcessMemoryInformation.hpp"
#include "lib/CStopwatch.hpp"

// IPMO
#if COMPILE_WITH_IPMO
	#include "mainthreading/CMainThreadingIPMO.hpp"
#else

// IOMP
#if COMPILE_WITH_IOMP
	#include "mainthreading/CMainThreadingIOMP.hpp"
#else

// OMP
#if COMPILE_WITH_OMP
	#include "mainthreading/CMainThreadingOMP.hpp"
#else

// TBB
#if COMPILE_WITH_TBB
	#include "mainthreading/CMainThreadingTBB.hpp"
#else

// DUMMY
	#include "mainthreading/CMainThreadingDummy.hpp"
#endif
#endif
#endif
#endif


template <typename CSimulation>
class CMain	:
	public CMainThreading
{
public:
	CProcessMemoryInformation cProcessMemoryInformation;

	int simulation_fixed_timesteps;
	float simulation_fixed_timestepSize;

	int grid_initial_recursion_depth;
	int grid_min_relative_recursion_depth;
	int grid_max_relative_recursion_depth;

	int number_of_threads_to_use;

	bool divide_number_of_timesteps_by_timestep_size;

	int splitting_partition_split_workload_size;
	int splitting_partition_join_workload_size;

	int simulation_world_scene_id;
	int simulation_terrain_scene_id;
	int simulation_water_surface_scene_id;

	bool setup_initial_column;
	int terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation;

	int verbose_level;

	int output_verbose_information_each_nth_timestep;
	int output_simulation_data_each_nth_timestep;
	double output_verbose_information_after_each_n_seconds;

	const char *output_simulation_data_filename;
	bool output_partitions_to_vtk_file;

	int partition_update_split_join_size_after_elapsed_timesteps;
	double partition_update_split_join_size_after_elapsed_scalar;



	CSimulation *cSimulation;


	int getNumberOfThreadsToUse()
	{
		return number_of_threads_to_use;
	}

	void setNumberOfThreadsToUse(int i_number_of_threads_to_use)
	{
		number_of_threads_to_use = i_number_of_threads_to_use;
	}

	int getGridInitialRecursionDepth()
	{
		return grid_initial_recursion_depth;
	}


	unsigned long long getSimLoopSumNumberOfTriangles()
	{
		return simloop_sum_number_of_triangles;
	}


	int getVerboseLevel()
	{
		return verbose_level;
	}



	/**
	 * constructor
	 */
	CMain()
	{
		simulation_fixed_timesteps = -1;
		simulation_fixed_timestepSize = -1;

		grid_initial_recursion_depth = -1;
		grid_min_relative_recursion_depth = -1;
		grid_max_relative_recursion_depth = -1;

		number_of_threads_to_use = -1;

		divide_number_of_timesteps_by_timestep_size = false;

		splitting_partition_split_workload_size = -1;
		splitting_partition_join_workload_size = -1;

		simulation_world_scene_id = -666;
		simulation_terrain_scene_id = -666;
		simulation_water_surface_scene_id = -666;

		setup_initial_column = true;
		terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation = -1;

		verbose_level = 0;

		output_verbose_information_each_nth_timestep = -1;
		output_simulation_data_each_nth_timestep = -1;
		output_verbose_information_after_each_n_seconds = -1;

		output_simulation_data_filename = "frame_%08i.vtk";
		output_partitions_to_vtk_file = false;

		partition_update_split_join_size_after_elapsed_timesteps = -1;
		partition_update_split_join_size_after_elapsed_scalar = -1;
	}


	/**
	 * parse the program start parameters
	 *
	 * \return true when all parameters are valid
	 */
	bool setupProgramParameters(
			int argc,		///< number of specified arguments
			char *argv[]	///< array with arguments
	)
	{

		char optchar;
		while ((optchar = getopt(argc, argv, "cd:t:f:w:a:i:s:v:n:o:l:rpg:u:U:b:h:y:z:T:")) > 0)
		{
			switch(optchar)
			{
			case 'c':
				setup_initial_column = true;
				break;

			case 'r':
				divide_number_of_timesteps_by_timestep_size = true;
				break;

			case 'p':
				output_partitions_to_vtk_file = true;
				break;

			case 'd':
				grid_initial_recursion_depth = atoi(optarg);
				break;

			case 'v':
				verbose_level = atoi(optarg);
				break;

			case 't':
				simulation_fixed_timesteps = atoi(optarg);
				break;

			case 's':
				simulation_fixed_timestepSize = atof(optarg);
				break;

			case 'f':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				if (output_verbose_information_each_nth_timestep == -1)
					output_verbose_information_each_nth_timestep = output_simulation_data_each_nth_timestep;
				break;

			case 'B':
				output_verbose_information_after_each_n_seconds = atof(optarg);
				break;

			case 'b':
				output_verbose_information_each_nth_timestep = atoi(optarg);
				break;

			case 'g':
				output_simulation_data_filename = optarg;
				break;

			case 'n':
				number_of_threads_to_use = atoi(optarg);
				break;

	#if SIMULATION_TSUNAMI_PARALLEL
			case 'w':
				simulation_world_scene_id = atoi(optarg);
				break;

			case 'y':
				simulation_terrain_scene_id = atoi(optarg);
				break;

			case 'z':
				simulation_water_surface_scene_id = atoi(optarg);
				break;
	#endif

			case 'i':
				grid_min_relative_recursion_depth = atoi(optarg);
				break;

			case 'a':
				grid_max_relative_recursion_depth = atoi(optarg);
				break;

			case 'o':
				splitting_partition_split_workload_size = atoi(optarg);

				// deactivate auto-mode
				partition_update_split_join_size_after_elapsed_timesteps = 0;
				break;

			case 'l':
				splitting_partition_join_workload_size = atoi(optarg);

				// deactivate auto-mode
				partition_update_split_join_size_after_elapsed_timesteps = 0;
				break;

			case 'u':
				partition_update_split_join_size_after_elapsed_timesteps = atoi(optarg);
				break;

			case 'U':
				partition_update_split_join_size_after_elapsed_scalar = atof(optarg);
				break;

			case 'T':
				terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation = atoi(optarg);
				break;

			case 'h':
			default:
				goto parameter_error;
			}

		}


		/**
		 * fix parameters
		 */
		if (splitting_partition_split_workload_size > 0 && splitting_partition_split_workload_size < 2)
		{
			std::cout << "Invalid split size: " << splitting_partition_split_workload_size << "   setting split to 2" << std::endl;
			splitting_partition_split_workload_size = 2;
		}

		if (splitting_partition_join_workload_size == -1)
		{
			splitting_partition_join_workload_size = splitting_partition_split_workload_size/2;
		}
		else
		{
			if (splitting_partition_join_workload_size > splitting_partition_split_workload_size)
			{
				std::cerr << "ERROR: partition_join_workload_size > partition_split_workload_size" << std::endl;
				return -1;
			}
		}

		return true;

		parameter_error:
		std::cout << "usage: " << argv[0] << std::endl;
		std::cout << "	[-d [int]: initial recursion depth]" << std::endl;
		std::cout << "	[-v [int]: verbose mode (0-5)]" << std::endl;
		std::cout << "	[-n [int]: number of threads to use]" << std::endl;
		std::cout << "	[-t [int]: timesteps]" << std::endl;
		std::cout << "	[-c : setup column]" << std::endl;
		std::cout << "	[-T [int]: terminate simulation after the given number of" << std::endl;
		std::cout << "			timesteps was executed consecutively with the same number of triangles" << std::endl;
		std::cout << "	[-r : divide number of timesteps and frame output rate by timestep size]" << std::endl;
		std::cout << "	[-s [float]: timestep size]" << std::endl;
		std::cout << "	[-p : also write vtk files with partitions]" << std::endl;
		std::cout << "	[-f [int]: output .vtk files each #nth timestep]" << std::endl;
		std::cout << "	[-b [int]: output verbose information each #nth timestep]" << std::endl;
		std::cout << "	[-B [double]: output verbose information each n seconds]" << std::endl;
		std::cout << "	[-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;
		std::cout << "	[-i [int]: min relative recursion depth]" << std::endl;
		std::cout << "	[-a [int]: max relative recursion depth]" << std::endl;

	#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << "	[-n [int]: number of threads to use]" << std::endl;
		std::cout << "	[-w [int]: world scene]" << std::endl;
		std::cout << "	[-y [int]: terrain]" << std::endl;
		std::cout << "	[-z [int]: water surface initialization]" << std::endl;
		std::cout << "	[-o [int]: partition size when to request split]" << std::endl;
		std::cout << "	[-l [int]: partition size when to request join (both childs have to request a join)]" << std::endl;
		std::cout << "	[-u [int]: elapsed timesteps when to update split and join parameters with table hints]" << std::endl;
		std::cout << "	[-U [double]: factor used for automatic computation of split/join parameter. default: 0 (disabled)]" << std::endl;
	#endif

		return false;
	}


	/**
	 * setup the simulation class
	 */
	void setupSimulation()
	{
		cSimulation = new CSimulation;

		if (verbose_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
		srand(0);

		if (verbose_level > 2)
		{
			cSimulation->outputVerboseInformation();
		}



		/***************************************************
		 * SETUP
		 */
#if SIMULATION_TSUNAMI_PARALLEL

		if (simulation_world_scene_id != -666)
			cSimulation->simulation_world_scene_id = simulation_world_scene_id;

		if (simulation_terrain_scene_id != -666)
			cSimulation->simulation_terrain_scene_id = simulation_terrain_scene_id;

#endif

		if (grid_initial_recursion_depth != -1)
			cSimulation->grid_initial_recursion_depth = grid_initial_recursion_depth;

		if (grid_min_relative_recursion_depth != -1)
			cSimulation->grid_min_relative_recursion_depth = grid_min_relative_recursion_depth;

		if (grid_max_relative_recursion_depth != -1)
			cSimulation->grid_max_relative_recursion_depth = grid_max_relative_recursion_depth;

#if SIMULATION_TSUNAMI_PARALLEL
		if (splitting_partition_join_workload_size != -1)
			cSimulation->splitting_partition_join_workload_size = splitting_partition_join_workload_size;

		if (splitting_partition_split_workload_size != -1)
			cSimulation->splitting_partition_split_workload_size = splitting_partition_split_workload_size;

		if (partition_update_split_join_size_after_elapsed_scalar != -1)
			cSimulation->partition_update_split_join_size_after_elapsed_scalar = partition_update_split_join_size_after_elapsed_scalar;

		if (partition_update_split_join_size_after_elapsed_timesteps != -1)
			cSimulation->partition_update_split_join_size_after_elapsed_timesteps = partition_update_split_join_size_after_elapsed_timesteps;
#endif

		/***************************************************
		 * reset & therefore setup the simulation with flat water surface
		 */
		cSimulation->reset_Simulation();


		if (divide_number_of_timesteps_by_timestep_size)
		{
			simulation_fixed_timesteps = (int)((double)simulation_fixed_timesteps/(double)cSimulation->simulation_parameter_timestep_size);
			output_simulation_data_each_nth_timestep = (int)((double)output_simulation_data_each_nth_timestep/(double)cSimulation->simulation_parameter_timestep_size);

			if (verbose_level > 2)
			{
				std::cout << "Timesteps: " << simulation_fixed_timesteps << std::endl;
				std::cout << "Output_simulation_data_each_nth_timestep: " << output_simulation_data_each_nth_timestep;
				std::cout << std::endl;
			}
		}


		if (verbose_level > 2)
		{
			std::cout << " + Number of threads to use: " << number_of_threads_to_use << std::endl;
			std::cout << " + Verbose: " << verbose_level << std::endl;
			std::cout << " + Terminate simulation after #n equal timesteps: " << terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation << std::endl;
			std::cout << " + Timesteps: " << simulation_fixed_timesteps << std::endl;
			std::cout << " + Output .vtk files each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output simulation data filename: " << output_simulation_data_filename << std::endl;
			std::cout << " +" << std::endl;
			std::cout << " + Timestep size: " << cSimulation->simulation_parameter_timestep_size << std::endl;
			std::cout << " + InitialDepth: " << cSimulation->grid_initial_recursion_depth << std::endl;
			std::cout << " + MinDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_min_relative_recursion_depth) << std::endl;
			std::cout << " + MaxDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_max_relative_recursion_depth) << std::endl;
			std::cout << " + WorldSceneId: " << cSimulation->simulation_world_scene_id << std::endl;
			std::cout << " + WaterSurfaceId: " << cSimulation->simulation_water_surface_scene_id << std::endl;
			std::cout << " + TerrainSceneId: " << cSimulation->simulation_terrain_scene_id << std::endl;
			std::cout << " + Partition size when to request split: " << cSimulation->splitting_partition_split_workload_size << std::endl;
			std::cout << " + Partition size when to request join (both childs have to request a join): " << cSimulation->splitting_partition_join_workload_size << std::endl;

			if (cSimulation->partition_update_split_join_size_after_elapsed_timesteps > 0)
			{
				std::cout << " + elapsed_timesteps_to_update_split_and_join_parameters: " << cSimulation->partition_update_split_join_size_after_elapsed_timesteps << std::endl;
				std::cout << " + elapsed_timesteps_to_udpate_split_and_join_scalar: " << cSimulation->partition_update_split_join_size_after_elapsed_scalar << std::endl;
			}

			if (cSimulation->simulation_parameter_timestep_size == -1)
				std::cout << " + Timestep Size: adaptive (CFL)" << std::endl;
			else
				std::cout << " + Timestep Size: " << cSimulation->simulation_parameter_timestep_size << std::endl;

#if ADAPTIVE_SUBPARTITION_STACKS
			std::cout << " + ADAPTIVE_SUBPARTITION_STACKS enabled" << std::endl;
#endif
		}

		/***************************************************
		 * remember initial number of triangles
		 */
		if (verbose_level > 2)
			std::cout << " + Initial number of triangles before setting up column: " << cSimulation->number_of_initial_triangles_after_domain_triangulation << std::endl;


		/*****************************************************
		 * setup initial split of partitions
		 *
		 * initial splitting of partitions should be executed before
		 * setting up column to avoid any preprocessing adaptive effects
		 */
		{
			size_t t = std::numeric_limits<size_t>::max();

			while (cSimulation->number_of_simulation_handler_partitions != t)
			{
				t = cSimulation->number_of_simulation_handler_partitions;

#if SIMULATION_TSUNAMI_PARALLEL
				cSimulation->setup_SplitJoinPartitions();
#endif

				if (verbose_level > 1)
					std::cout << " + splitted to " << cSimulation->number_of_simulation_handler_partitions << " partitions with " << cSimulation->number_of_triangles << " triangles" << std::endl;
			}
		}


		/***************************************************
		 * setup column
		 */
		if (setup_initial_column && cSimulation->simulation_water_surface_scene_id == 0)
		{
			if (verbose_level > 0)
				std::cout << "[ SETUP COLUMN ]" << std::endl;

			cSimulation->number_of_triangles = cSimulation->setup_ColumnAt2DPosition();

			if (verbose_level > 1)
				std::cout << " + refined to " << cSimulation->number_of_triangles << " triangles" << std::endl;

			if (verbose_level > 0)
				std::cout << " + pre splitting partitions" << std::endl;
		}
		else
		{
#if SIMULATION_TSUNAMI_PARALLEL
			if (simulation_water_surface_scene_id != -666)
				cSimulation->simulation_water_surface_scene_id = simulation_water_surface_scene_id;

			cSimulation->reset_Simulation();
#endif
		}
	}


	/**
	 * shutdown simulation class
	 */
	void shutdownSimulation()
	{
		delete cSimulation;
	}


	/***************************************************
	 * SIMULATION LOOP
	 */
	CStopwatch simloop_cStopwatch;

	unsigned long long simloop_sum_number_of_triangles;
	unsigned long long simloop_prev_number_of_triangles;
	unsigned long long simloop_sum_number_of_partitions;
	char simloop_char_buffer[1024];

	int simloop_outputVTKFrameCounter;
	int simloop_outputVerboseFrameCounter;
	int simloop_frameCounter;

	/*
	 * stopwatches for edge/adaptive/split-join traversals
	 */
	double simloop_edgeCommTime;
	double simloop_adaptiveTime;
	double simloop_splitJoinTime;

	/*
	 * output verbose information after output_verbose_information_after_each_n_seconds seconds
	 */
	double simloop_verbose_output_time;
	double simloop_simulation_time;

	bool simloop_finishSimulation;
	int simloop_timestep_id;


	/**
	 * this method is executed at the beginning of each simulation loop
	 */
	void simulationLoopPrefix()
	{
		if (verbose_level > 1)
		{
			std::cout << "[ START ]" << std::endl;
		}

		simloop_cStopwatch.start();


		/***************************************************
		 * SIMULATION
		 */
		simloop_sum_number_of_triangles = 0;
		simloop_prev_number_of_triangles = 0;
		simloop_sum_number_of_partitions = 0;

		simloop_outputVTKFrameCounter = 0;
		simloop_outputVerboseFrameCounter = 0;
		simloop_frameCounter = 0;

		/*
		 * stopwatches for edge/adaptive/split-join traversals
		 */
		simloop_edgeCommTime = 0;
		simloop_adaptiveTime = 0;
		simloop_splitJoinTime = 0;

		/*
		 * output verbose information after output_verbose_information_after_each_n_seconds seconds
		 */
		simloop_verbose_output_time = CMath::numeric_inf<double>();
		if (output_verbose_information_after_each_n_seconds > 0)
			simloop_verbose_output_time = 0;

		if (output_verbose_information_each_nth_timestep == -1 && verbose_level > 3)
			output_verbose_information_each_nth_timestep = 100;

		simloop_simulation_time = 0;

		simloop_finishSimulation = false;
		simloop_timestep_id = 0;
	}



	/**
	 * simulation loop executing during each particular simulation step
	 *
	 * the separation into header/iteration/footer is necessary to be
	 * allowed to change the number of ressources during each simulation when desired.
	 */
	bool simulationLoopIteration()
	{
		/***************************************************
		 * output some data?
		 */
		bool outputInfo = false;

		if (output_simulation_data_each_nth_timestep > 0)
		{
			if (simloop_outputVTKFrameCounter == 0)
			{
				sprintf(simloop_char_buffer, output_simulation_data_filename, simloop_frameCounter);

				if (verbose_level > 3)
				{
					std::cout << "=========================================" << std::endl;
					std::cout << "   + writing file " << simloop_char_buffer << std::endl;
					outputInfo = true;
				}

				cSimulation->writeTrianglesToVTKFile(simloop_char_buffer);
				std::string partitionFile = "partitions_";
				partitionFile += simloop_char_buffer;

#if SIMULATION_TSUNAMI_PARALLEL
				if (output_partitions_to_vtk_file)
					cSimulation->writePartitionsToVTKFile(partitionFile.c_str());
#endif

				simloop_frameCounter++;
			}

			simloop_outputVTKFrameCounter = (simloop_outputVTKFrameCounter+1) % output_simulation_data_each_nth_timestep;
		}
		else if (
				verbose_level > 3 ||
				output_verbose_information_each_nth_timestep != -1
			)
		{
			if (simloop_outputVerboseFrameCounter == 0)
			{
				std::cout << "=========================================" << std::endl;
				outputInfo = true;

				if (verbose_level > 6)
				{
					cProcessMemoryInformation.outputUsageInformation();
				}
			}

			simloop_outputVerboseFrameCounter = (simloop_outputVerboseFrameCounter+1) % output_verbose_information_each_nth_timestep;
		}
		else if (simloop_verbose_output_time <= simloop_simulation_time)
		{
			std::cout << "=========================================" << std::endl;
			outputInfo = true;

			if (simloop_verbose_output_time < simloop_simulation_time)
				simloop_verbose_output_time += output_verbose_information_after_each_n_seconds;
		}


		if (outputInfo)
		{
			if (simloop_timestep_id != 0)
				std::cout << "   + " << ((double)simloop_sum_number_of_triangles/simloop_cStopwatch.getTimeSinceStart())*0.000001 << " MTPS(Mega Triangles per second)" << std::endl;

			std::cout << "   + " << simloop_timestep_id << "\tTIMESTEP" << std::endl;
			std::cout << "   + " << simloop_simulation_time << "\tSIMULATION_TIME" << std::endl;
			std::cout << "   + " << cSimulation->number_of_triangles << "\tTRIANGLES" << std::endl;
			double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation->number_of_triangles)/(1024.0*1024.0);
			std::cout << "   + " << elementdata_megabyte_per_timestep << "\tElementData Megabyte per Timestep (RW)" << std::endl;
			std::cout << "   + " << cSimulation->simulation_parameter_timestep_size << "\tTIMESTEP SIZE" << std::endl;
			std::cout << "   + " << cSimulation->number_of_simulation_handler_partitions << "\tPARTITIONS" << std::endl;
		}


		/*
		 * single simulation simloop_timestep_id
		 */
		if (verbose_level > 2)
		{
			cSimulation->runSingleTimestepDetailedBenchmarks(
					&simloop_edgeCommTime,
					&simloop_adaptiveTime,
					&simloop_splitJoinTime
				);
		}
		else
		{
			cSimulation->runSingleTimestep();
		}

		simloop_simulation_time += cSimulation->simulation_parameter_timestep_size;


		/*
		 * increment counter of number of triangles processed so far
		 */
		simloop_sum_number_of_triangles += cSimulation->number_of_triangles;
		simloop_sum_number_of_partitions += cSimulation->number_of_simulation_handler_partitions;


		/*
		 * verbose points
		 */
		if (verbose_level > 5)
		{
			std::cout << "." << std::flush;
		}

//		assert(cSimulation->number_of_initial_triangles_after_domain_triangulation <= cSimulation->number_of_triangles);

		/*
		 * if timesteps are not set, we quit as soon as the initial number of triangles was reached
		 */
		if (simulation_fixed_timesteps == -1)
		{
			if (terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation != -1)
			{
				static int consecutive_timesteps_with_equal_triangle_number_counter = 0;

				if (simloop_prev_number_of_triangles == cSimulation->number_of_triangles)
				{
					consecutive_timesteps_with_equal_triangle_number_counter++;

					if (consecutive_timesteps_with_equal_triangle_number_counter > terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation)
					{
						std::cout << std::endl;
						std::cout << " + Terminating after " << terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation << " timesteps with equal number of triangles -> EXIT" << std::endl;
						std::cout << " + Final simloop_timestep_id: " << simloop_timestep_id << std::endl;
						std::cout << std::endl;
						return false;
					}
				}

				simloop_prev_number_of_triangles = cSimulation->number_of_triangles;
			}
#if !COMPILE_SIMULATION_WITH_GUI
			else if (cSimulation->number_of_initial_triangles_after_domain_triangulation == cSimulation->number_of_triangles)
			{
				std::cout << std::endl;
				std::cout << " + Initial number of triangles reached -> EXIT" << std::endl;
				std::cout << " + Final simloop_timestep_id: " << simloop_timestep_id << std::endl;
				std::cout << std::endl;
				return false;
			}
#endif
		}

		simloop_timestep_id++;

		if (simulation_fixed_timesteps != -1)
			if (simloop_timestep_id >= simulation_fixed_timesteps)
				return false;

		return true;
	}


	/**
	 * this method is executed at the end of the simulation
	 */
	void simulationLoopSuffix()
	{
		simloop_cStopwatch.stop();

		if (verbose_level > 1)
		{
			std::cout << "[ END ]" << std::endl;
			std::cout << std::endl;
		}


		if (verbose_level > 2)
		{
			std::cout << std::endl;
			std::cout << "Timings for simulation phases:" << std::endl;
			std::cout << " + EdgeCommTime: " << simloop_edgeCommTime << std::endl;
			std::cout << " + AdaptiveTime: " << simloop_adaptiveTime << std::endl;
			std::cout << " + SplitJoinTime: " << simloop_splitJoinTime << std::endl;
			std::cout << std::endl;
		}

		double stoptime = simloop_cStopwatch();

		std::cout << simloop_timestep_id << " TS (Timesteps)" << std::endl;
		std::cout << simloop_simulation_time << " ST (SIMULATION_TIME)" << std::endl;
		std::cout << cSimulation->simulation_parameter_timestep_size << " TSS (Timestep size)" << std::endl;
		std::cout << stoptime << " SFS (Overall seconds for Simulation)" << std::endl;
		std::cout << simloop_sum_number_of_triangles << " TP (Triangles processed)" << std::endl;
		std::cout << (double)stoptime/(double)simloop_timestep_id << " ASPT (Averaged Seconds per Timestep)" << std::endl;
		std::cout << (double)simloop_sum_number_of_triangles/(double)simloop_timestep_id << " TPST (Triangles Processed in Average per Simulation Timestep)" << std::endl;

	#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << (double)simloop_sum_number_of_partitions/(double)simloop_timestep_id << " PPST (Partitions Processed in Average per Simulation Timestep)" << std::endl;
	#endif

		double MTPS = ((double)simloop_sum_number_of_triangles/stoptime)*0.000001;
		std::cout << MTPS << " MTPS (Million Triangles per Second)" << std::endl;
		std::cout << ((double)simloop_sum_number_of_triangles/(simloop_cStopwatch()*(double)number_of_threads_to_use))*0.000001 << " MTPSPT (Million Triangles per Second per Thread)" << std::endl;

		double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)simloop_timestep_id)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_timestep << " EDMBPT (ElementData Megabyte per Timestep (RW))" << std::endl;

		double elementdata_megabyte_per_second = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)stoptime)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_second << " EDMBPS (ElementData Megabyte per Second (RW))" << std::endl;
	}



	~CMain()
	{
#ifdef DEBUG
		if (!debug_ibase_list.empty())
		{
			std::cout << "MEMORY LEAK: iBase class missing in action" << std::endl;
			std::cout << "  + number of classes: " << debug_ibase_list.size() << std::endl;
		}
#endif

		if (verbose_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
	}

};

#endif
