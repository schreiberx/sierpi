/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include <stdlib.h>
#include <stdint.h>
#include <sstream>


#if COMPILE_SIMULATION_WITH_GUI

#include "CMainGui.hpp"

int main(int argc, char *argv[])
{
	CMainGui<CSimulation> cMainGui;

	if (!cMainGui.setupProgramParameters(argc, argv))
		return -1;

	// CMAIN: setup threading tools
	cMainGui.threading_setup();

	// CMAIN: setup the simulation
	cMainGui.setupSimulation();

	// setup gui stuff AFTER setting up the simulation
	cMainGui.setupGui();

	// run the simulation loop
	cMainGui.run();

	// CMAIN: shutdown the simulation
	cMainGui.shutdownSimulation();

	// CMAIN: shutdown threading tools
	cMainGui.threading_shutdown();

	return 0;
}


#else

#include "CMain.hpp"

int main(int argc, char *argv[])
{

	CMain<CSimulation> cMain;

	if (!cMain.setupProgramParameters(argc, argv))
		return -1;

	// setup threading tools
	cMain.threading_setup();

	// setup the simulation
	cMain.setupSimulation();

	// simulation loop prefix
	cMain.simulationLoopPrefix();

	// run the simulation
	cMain.threading_simulationLoop();

	// simulation loop suffix
	cMain.simulationLoopSuffix();

	// shutdown the simulation
	cMain.shutdownSimulation();

	// shutdown threading tools
	cMain.threading_shutdown();

	return 0;
}

#endif
