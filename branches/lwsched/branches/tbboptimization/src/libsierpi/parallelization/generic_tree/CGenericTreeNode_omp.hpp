/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_omp.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_OMP_HPP_
#define CGENERICTREENODE_OMP_HPP_

#include "config.h"


/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE)
 */

/**
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_Partition_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun p_lambda
	)
	{
		if (this_node->isLeaf())
		{
			p_lambda(this_node->cPartition_TreeNode);
#pragma omp flush
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, p_lambda);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, p_lambda);
		}

#pragma omp taskwait
	}
};


template <class CLambdaFun>
void traverse_PartitionTreeNode_Parallel(
		CLambdaFun lambda
)
{
	CTraversalTask_Partition_Parallel<CLambdaFun>::traverse(this, lambda);
}


/*
 * serial
 */
template <
	typename CLambdaFun
>
class CTraversalTask_PartitionTreeNode_Serial
{
public:
	static void traverse(CGenericTreeNode_ *this_node, CLambdaFun p_lambda)
	{
		if (this_node->isLeaf())
		{
			p_lambda(this_node->cPartition_TreeNode);
#pragma omp flush
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>::traverse(this_node->first_child_node, p_lambda);

		if (this_node->second_child_node)
			CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>::traverse(this_node->second_child_node, p_lambda);
	}
};

template <class CLambdaFun>
void traverse_PartitionTreeNode_Serial(
		CLambdaFun lambda
)
{
	CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>::traverse(this, lambda);
}


/*
 * parallel/serial
 */
template <bool parallelProcessing, class CLambdaFun>
void traverse_PartitionTreeNode(
		CLambdaFun p_lambda
)
{
	if (parallelProcessing)
		CTraversalTask_Partition_Parallel<CLambdaFun>::traverse(this, p_lambda);
	else
		CTraversalTask_PartitionTreeNode_Serial<CLambdaFun>::traverse(this, p_lambda);
}



/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE) MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node->cPartition_TreeNode);
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}

#pragma omp taskwait

		if (this_node->cPartition_TreeNode)
			lambda_midnodes(this_node->cPartition_TreeNode);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial
{

public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node->cPartition_TreeNode);
			return;
		}

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}


		if (this_node->cPartition_TreeNode)
			lambda_midnodes(this_node->cPartition_TreeNode);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/*
 * parallel/serial
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
	else
		CTraversalTask_PartitionTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}

#pragma omp taskwait

		lambda_midnodes(this_node);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}

		lambda_midnodes(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/*
 * parallel/serial
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		CTraversalTask_GenericTreeNode_MidAndLeafNodes_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
	else
		CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

#pragma omp taskwait

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes,
		int p_genericTreeDepth
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;
		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}



/*
 * parallel/serial
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	if (parallelProcessing)
		CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
	else
		CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}



/****************************************************************************
 * PARTITION TREE NODE (WITH REDUCE)
 ****************************************************************************/
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_PartitionTreeNode_Reduce_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
			)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			p_lambda(p_this_node->cPartition_TreeNode, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue;
		TReduceValue secondValue;

		if (p_this_node->first_child_node)
		{
#pragma omp task shared(firstValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
			{
				traverse(p_this_node->first_child_node, p_lambda, p_reduceOperator, &firstValue);
#pragma omp flush
			}
		}

		if (p_this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(secondValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
			{
#endif
				traverse(p_this_node->second_child_node, p_lambda, p_reduceOperator, &secondValue);
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp flush
			}
#endif
		}

#pragma omp taskwait

#pragma omp flush

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);
				return;
			}
			else
			{
				*o_reduceOutput = firstValue;
				return;
			}
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;
		return;
	}
};



template <typename TReduceValue, typename CLambdaFun>
void traverse_PartitionTreeNode_Reduce_Parallel(
		CLambdaFun p_lambda,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>::traverse(this, p_lambda, reduceOperator, o_reduceOutput);
}


template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_PartitionTreeNode_Reduce_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
			)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			p_lambda(p_this_node->cPartition_TreeNode, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue(0);
		TReduceValue secondValue(0);

		if (p_this_node->first_child_node)
		{
			traverse(p_this_node->first_child_node, p_lambda, p_reduceOperator, &firstValue);
		}

		if (p_this_node->second_child_node)
		{
			traverse(p_this_node->second_child_node, p_lambda, p_reduceOperator, &secondValue);
		}

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);
				return;
			}

			*o_reduceOutput = firstValue;
			return;
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;
		return;
	}
};

template <typename TReduceValue, typename CLambdaFun>
void traverse_PartitionTreeNode_Reduce_Serial(
		CLambdaFun p_lambda,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	return CTraversalTask_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>::traverse(this, p_lambda, reduceOperator, o_reduceOutput);
}


template <bool parallelProcessing, typename CLambdaFun, typename TReduceValue>
void traverse_PartitionTreeNode_Reduce(
		CLambdaFun i_lambda,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	if (parallelProcessing)
		CTraversalTask_PartitionTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>::traverse(this, i_lambda, i_reduceOperator, o_reduceOutput);
	else
		CTraversalTask_PartitionTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>::traverse(this, i_lambda, i_reduceOperator, o_reduceOutput);
}


/****************************************************************************
 * SIMULATION SUB-PARTITION HANDLER (NO REDUCE)
 ****************************************************************************/

/*
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_SimulationPartitionHandler_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)
	{
		if (p_this_node->isLeaf())
		{
			p_lambda(p_this_node->cPartition_TreeNode->cSimulation_SubPartitionHandler);
#pragma omp flush
			return;
		}

		if (p_this_node->first_child_node)
		{
#pragma omp task shared(p_this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
			traverse(p_this_node->first_child_node, p_lambda);
		}

		if (p_this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(p_this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(p_this_node->second_child_node, p_lambda);
		}

#pragma omp taskwait
	}
};


template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Parallel(
		CLambdaFun lambda
)
{
	CTraversalTask_SimulationPartitionHandler_Parallel<CLambdaFun>::traverse(this, lambda);
}


/*
 * serial
 */
template <
	typename CLambdaFun
>
class CTraversalTask_SimulationPartitionHandler_Serial
{
public:
	static void traverse(CGenericTreeNode_ *this_node, CLambdaFun p_lambda)
	{
		if (this_node->isLeaf())
		{
			p_lambda(this_node->cPartition_TreeNode->cSimulation_SubPartitionHandler);
#pragma omp flush
			return;
		}

		if (this_node->first_child_node)
			traverse(this_node->first_child_node, p_lambda);

		if (this_node->second_child_node)
			traverse(this_node->second_child_node, p_lambda);
	}
};


template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Serial(
		CLambdaFun lambda
)
{
	CTraversalTask_SimulationPartitionHandler_Serial<CLambdaFun>::traverse(this, lambda);
}


/*
 * parallel/serial
 */
template <bool parallelProcessing, class CLambdaFun>
void traverse_SimulationPartitionHandler(
		CLambdaFun p_lambda
)
{
	if (parallelProcessing)
		CTraversalTask_SimulationPartitionHandler_Parallel<CLambdaFun>::traverse(this, p_lambda);
	else
		CTraversalTask_SimulationPartitionHandler_Serial<CLambdaFun>::traverse(this, p_lambda);
}

#endif
