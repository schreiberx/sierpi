/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_SplitJoinAction.hpp
 *
 *  Created on: Jul 4, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_SPLITJOINACTIONS_HPP_
#define CPARTITION_SPLITJOINACTIONS_HPP_

#include "config.h"
#include "generic_tree/CGenericTreeNode.hpp"

template <typename CSimulation_SubPartitionHandler>
class CPartition_TreeNode;


/**
 * class to store the enumerator for the transfer states
 */
class CPartition_SplitJoinActions_Enums
{
public:
	/**
	 * these states account for the currently running transformation.
	 *
	 * after each timestep, the state of each sub-partition has to be NO_TRANSFER
	 */
	enum EPARTITION_TRANSFER_STATE
	{
		NO_TRANSFER = 0,
		SPLITTED_PARENT = 1,
		SPLITTED_CHILD = 2,
		JOINED_PARENT = 3,
		JOINED_CHILD = 4
	};
};



/**
 * \brief split and join actions which have to be executed in a particular order
 *
 * this class offers methods to split and join partition tree nodes.
 */
template <typename CSimulation_SubPartitionHandler>
class CPartition_SplitJoinActions	: public CPartition_SplitJoinActions_Enums
{
	typedef CPartition_TreeNode<CSimulation_SubPartitionHandler> CPartition_TreeNode_;
	typedef CGenericTreeNode<CPartition_TreeNode_> CGeneric_TreeNode_;

	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartition_;
	typedef CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartitions_;

	typedef typename CSimulation_SubPartitionHandler::CElementData CElementData_;


	/**
	 * triangle factory
	 */
	CTriangle_Factory &triangleFactory;

	/**
	 * reference to partition tree node
	 */
	CPartition_TreeNode_ &cPartition_TreeNode;

	/**
	 * split/join information which should be used for data exchange
	 */
	CPartition_SplitJoin_EdgeComm_Information &cPartition_SplitJoinInformation;

	/**
	 * this is the point where the adjacency information to other paritions is stored
	 */
	CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> &cEdgeComm_InformationAdjacentPartitions;

	/**
	 * partition's unique ID
	 */
	CPartition_UniqueId &uniqueId;

	/**
	 * triangle child factories already set-up
	 */
	bool triangleChildsFactorySetupDone;


public:
	/**
	 * one of the most important parts: the transfer state describes the current
	 * state of the partition tree node to run further actions during later processing.
	 */
	EPARTITION_TRANSFER_STATE transferState;


public:
	/**
	 * Constructor
	 */
	CPartition_SplitJoinActions(
			CPartition_TreeNode_ &p_cPartitionTreeNode	///< handler to partition tree node (e. g. to access stacks)
	)	:
		triangleFactory(p_cPartitionTreeNode.cTriangleFactory),
		cPartition_TreeNode(p_cPartitionTreeNode),
		cPartition_SplitJoinInformation(p_cPartitionTreeNode.cPartition_SplitJoinInformation),
		cEdgeComm_InformationAdjacentPartitions(p_cPartitionTreeNode.cEdgeComm_InformationAdjacentPartitions),
		uniqueId(p_cPartitionTreeNode.uniqueId),

		triangleChildsFactorySetupDone(false),
		transferState(NO_TRANSFER)
	{
	}



	/**
	 * split this sub-partition.
	 */
public:
	void pass1_splitPartition()
	{
		assert(transferState == NO_TRANSFER);
		assert(cPartition_SplitJoinInformation.splitting_permitted);
		assert(cPartition_TreeNode.cGeneric_TreeNode->isLeaf());

		/*
		 * STEP 1)
		 *
		 * allocate & create new child nodes
		 */

		if (!triangleChildsFactorySetupDone)
		{
			triangleFactory.setupChildFactories(
					cPartition_SplitJoinInformation.first_triangle.triangleFactory,
					cPartition_SplitJoinInformation.second_triangle.triangleFactory
				);
		}

		/**
		 * override type to reduce possible recursive methods without changing any traversals
		 */
		cPartition_SplitJoinInformation.first_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;
		cPartition_SplitJoinInformation.second_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		// mark triangle child setup to be finished
		triangleChildsFactorySetupDone = true;


		// setup childs

		// FIRST child
		CGeneric_TreeNode_ *firstGenericTreeNode = new CGeneric_TreeNode_(cPartition_TreeNode.cGeneric_TreeNode, false);
		CPartition_TreeNode_ *firstPartitionTreeNode = new CPartition_TreeNode_(cPartition_SplitJoinInformation.first_triangle.triangleFactory);

		firstPartitionTreeNode->cGeneric_TreeNode = firstGenericTreeNode;
		firstGenericTreeNode->cPartition_TreeNode = firstPartitionTreeNode;

		cPartition_TreeNode.cGeneric_TreeNode->first_child_node = firstGenericTreeNode;
		firstPartitionTreeNode->uniqueId.setupFirstChildFromParent(uniqueId);
		firstPartitionTreeNode->cPartition_SplitJoinActions.transferState = SPLITTED_CHILD;


		// SECOND child
		CGeneric_TreeNode_ *secondGenericTreeNode = new CGeneric_TreeNode_(cPartition_TreeNode.cGeneric_TreeNode, false);
		CPartition_TreeNode_ *secondPartitionTreeNode = new CPartition_TreeNode_(cPartition_SplitJoinInformation.second_triangle.triangleFactory);

		secondPartitionTreeNode->cGeneric_TreeNode = secondGenericTreeNode;
		secondGenericTreeNode->cPartition_TreeNode = secondPartitionTreeNode;

		cPartition_TreeNode.cGeneric_TreeNode->second_child_node = secondGenericTreeNode;
		secondPartitionTreeNode->uniqueId.setupSecondChildFromParent(uniqueId);
		secondPartitionTreeNode->cPartition_SplitJoinActions.transferState = SPLITTED_CHILD;


		/*
		 * STEP 2)
		 *
		 * setup new simulation partition handlers
		 */

		/*
		 * FIRST CHILD
		 */
		// setup simulation handler
		firstPartitionTreeNode->setup_SimulationPartitionHandler(
				CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS,
				cPartition_TreeNode.cSimulation_SubPartitionHandler
			);

		assert(cPartition_SplitJoinInformation.first_triangle.number_of_elements != -1);

		// setup stacks
		firstPartitionTreeNode->setup_SplittedChildStacks(
				cPartition_SplitJoinInformation,		/// split information
				false
		);

		/*
		 * SECOND CHILD
		 */
		/*
		 * reuse the stacks of this node since they are of no further use.
		 * this is done by handing over the current cSimulation_SubPartitionHandler as a parameter.
		 */
		secondPartitionTreeNode->setup_SimulationPartitionHandler(
				CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS,
				cPartition_TreeNode.cSimulation_SubPartitionHandler
			);

		assert(cPartition_SplitJoinInformation.second_triangle.number_of_elements != -1);

		// setup stacks
		secondPartitionTreeNode->setup_SplittedChildStacks(
				cPartition_SplitJoinInformation,		/// split information
				CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		);

		/*
		 * STEP 3)
		 */
		/*
		 * SETUP ADJACENT EDGE COMMUNICATION
		 *
		 * since the edge communication data is setup during the initialization by the root simulation handler,
		 * this has now to be right here - based on the split/join information and the information about the adjacent partitions
		 */

		_pass1_setupAdjacentPartitionInformationAfterSplit(
				cPartition_SplitJoinInformation,
				cEdgeComm_InformationAdjacentPartitions,
				cPartition_TreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode,
				cPartition_TreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode
			);

		// finally set joining and splitting permission to false to avoid any further conflicts
		cPartition_SplitJoinInformation.joining_permitted = false;
		cPartition_SplitJoinInformation.splitting_permitted = false;

#if !CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		cPartition_TreeNode.freeStacks();
#endif

		this->transferState = SPLITTED_PARENT;
	}



	/**
	 * \brief join both child nodes
	 *
	 * this method is declared as static since it is possible that partitions do not exist
	 * when DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION is activated.
	 */
	static void pass1_joinChildPartitions(CGeneric_TreeNode_ *cGeneric_TreeNode)
	{
#if !DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		assert(cGeneric_TreeNode->cPartition_TreeNode != nullptr);
		assert(cGeneric_TreeNode->cPartition_TreeNode->cPartition_SplitJoinActions.transferState == NO_TRANSFER);
#endif

		CPartition_TreeNode_* first_child_node = cGeneric_TreeNode->first_child_node->cPartition_TreeNode;
		CPartition_TreeNode_* second_child_node = cGeneric_TreeNode->second_child_node->cPartition_TreeNode;

		assert(first_child_node != NULL);
		assert(second_child_node != NULL);

#if DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		/*
		 * the partition tree node is assumed to be deleted
		 */
		assert (cGeneric_TreeNode->cPartition_TreeNode == nullptr);

		CTriangle_Factory parentTriangleFactory;

		parentTriangleFactory.setupParentFromChildFatories(first_child_node->cTriangleFactory, second_child_node->cTriangleFactory);
		cGeneric_TreeNode->cPartition_TreeNode = new CPartition_TreeNode_(parentTriangleFactory);

		CPartition_TreeNode_ &cPartition_TreeNode = *(cGeneric_TreeNode->cPartition_TreeNode);

		cPartition_TreeNode.cGeneric_TreeNode = cGeneric_TreeNode;

		cPartition_TreeNode.setup_SimulationPartitionHandler(
				false,
				cGeneric_TreeNode->cPartition_TreeNode->cSimulation_SubPartitionHandler
			);

		cPartition_TreeNode.uniqueId.setupParentFromChild(first_child_node->uniqueId);

#if 0
		std::cout << first_child_node->triangleFactory << std::endl;
		std::cout << second_child_node->triangleFactory << std::endl;
		std::cout << parentTriangleFactory << std::endl;
#endif

#else
		CPartition_TreeNode_ &cPartition_TreeNode = *(cGeneric_TreeNode->cPartition_TreeNode);
#endif



		CPartition_UniqueId &first_child_unique_id = first_child_node->uniqueId;
		CPartition_UniqueId &second_child_unique_id = second_child_node->uniqueId;

		assert(cGeneric_TreeNode == cPartition_TreeNode.cGeneric_TreeNode);
		assert(&cPartition_TreeNode == cPartition_TreeNode.cGeneric_TreeNode->cPartition_TreeNode);

		assert(first_child_node->cPartition_SplitJoinInformation.joining_permitted);
		assert(second_child_node->cPartition_SplitJoinInformation.joining_permitted);

		assert(first_child_node->cGeneric_TreeNode->isLeaf());
		assert(second_child_node->cGeneric_TreeNode->isLeaf());
		assert(first_child_node->cTriangleFactory.evenOdd == second_child_node->cTriangleFactory.evenOdd);

		CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> &cEdgeComm_InformationAdjacentPartitions = cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions;

		/*
		 * prepare parent's adjacency information
		 */
		cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.clear();
		cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.clear();


		{
			typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter_first, iter_second;

			size_t number_of_edge_comm_informations = first_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.size();
			assert(number_of_edge_comm_informations > 0);

			if (number_of_edge_comm_informations == 1)
			{
				/**
				 * the only edge comm information which exists is shared by both sub-triangles => nothing to do
				 */
			}
			else
			{
				/*************************************
				 * setup new EDGE COMM INFORMATION
				 *************************************/

				/*
				 * FIRST CHILD - CATHETUS
				 */
				iter_first = first_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();

				/*
				 * iterate to the next-to-last edge comm information
				 */
				for (	size_t i = 0;
						i < number_of_edge_comm_informations-2;
						iter_first++, i++
				)
				{
					CEdgeComm_InformationAdjacentPartition_ &informationAdjPartition = *iter_first;

					// another way would be simply to drop the last adjacency information
					if (informationAdjPartition.uniqueId == second_child_unique_id)
						continue;

					cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(informationAdjPartition);
				}


				/*
				 * SECOND CHILD - CATHETUS
				 */
				number_of_edge_comm_informations = second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.size();

				if (number_of_edge_comm_informations == 1)
				{
					/*
					 * only a single edge comm information is available which is shared by both child-triangles
					 */
					cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back((*iter_first));
#if DEBUG
					iter_second = second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
					assert((*iter_second).uniqueId == first_child_node->uniqueId);
					iter_first++;
					assert((*iter_first).uniqueId == second_child_node->uniqueId);
#endif
				}
				else
				{
					/*
					 * check whether it's possible to join the first edge comm information with the last one
					 */

					// setup second iterator
					iter_second = second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
					assert((*iter_second).uniqueId == first_child_node->uniqueId);
					iter_second++;	// skip first edge comm (shared by)

					/*
					 * join adj information
					 */
					if ((*iter_second).uniqueId == (*iter_first).uniqueId)
					{
						/*
						 * the information about the edge communication data has to be joined since there's only one
						 * adjacent triangle
						 */
						CEdgeComm_InformationAdjacentPartition_ &informationAdjPartition = *iter_first;

						cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(
								CEdgeComm_InformationAdjacentPartition_(
										informationAdjPartition.partitionTreeNode,
										informationAdjPartition.uniqueId,
										informationAdjPartition.commElements+(*iter_second).commElements
										)
								);
					}
					else
					{
						/*
						 * in case of a still splitted edge comm dataset, just push the edge comm information
						 */
						cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(*iter_first);
						cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(*iter_second);
					}

					iter_second++;

					/**
					 * SECOND CHILD - CATHETUS
					 */
					for (	;
							iter_second != second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
							iter_second++
					)
					{
						CEdgeComm_InformationAdjacentPartition_ &informationAdjPartition = *iter_second;

						if (informationAdjPartition.uniqueId == first_child_unique_id)
							continue;

						cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(informationAdjPartition);
					}
				}
			}
		}


		{
			typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter;

			/**
			 * FIRST CHILD - HYPOTENUSE
			 */
			for (	iter = first_child_node->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
					iter != first_child_node->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
					iter++)
			{
				CEdgeComm_InformationAdjacentPartition_ &informationAdjPartition = *iter;

				// another way would be simply to drop the last adjacency information
				if (informationAdjPartition.uniqueId == second_child_unique_id)
					continue;

				cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(informationAdjPartition);
			}

			/**
			 * SECOND CHILD - HYPOTENUSE
			 */
			for (	iter = second_child_node->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
					iter != second_child_node->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
					iter++)
			{
				CEdgeComm_InformationAdjacentPartition_ &informationAdjPartition = *iter;

				if (informationAdjPartition.uniqueId == first_child_unique_id)
					continue;

				cPartition_TreeNode.cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(informationAdjPartition);
			}
		}


		/***********************************************
		 * Reassemble stacks
		 ***********************************************/

#if !CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS && !DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		assert(cPartition_TreeNode.cStacks == NULL);
#endif

		cPartition_TreeNode.setup_JoinedStacks(
				cPartition_TreeNode.cPartition_SplitJoinInformation,
				CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS				//< reuse child's stack?
			);

#if DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		cPartition_TreeNode.cSimulation_SubPartitionHandler->setup(
				&cPartition_TreeNode,
				first_child_node->cSimulation_SubPartitionHandler
			);
#endif

		/**
		 * set child nodes of being joined during the last traversal
		 */
		first_child_node->cPartition_SplitJoinActions.transferState = JOINED_CHILD;
		second_child_node->cPartition_SplitJoinActions.transferState = JOINED_CHILD;


		cPartition_TreeNode.cPartition_SplitJoinActions.transferState = JOINED_PARENT;

		// finally set joining and splitting permission to false to avoid any further conflicts
		cPartition_TreeNode.cPartition_SplitJoinInformation.joining_permitted = false;
		cPartition_TreeNode.cPartition_SplitJoinInformation.splitting_permitted = false;

		cGeneric_TreeNode->setDelayedChildDeletion();
	}



	/**
	 * update the information about the adjacent information after a split operation
	 */
public:
	void _pass1_setupAdjacentPartitionInformationAfterSplit(
			CPartition_SplitJoin_EdgeComm_Information &p_splitJoinInformation,	///< split/join information
			CEdgeComm_InformationAdjacentPartitions_ &p_parentAdjacentPartitions,		///< handler to parent adjacent information
			CPartition_TreeNode_ *p_first_child_node,					///< first child node
			CPartition_TreeNode_ *p_second_child_node					///< second child node
	)
	{
		/**********************
		 * CATHETI
		 *
		 * splitting on the catheti should be quite easy since the information about the adjacent partitions only
		 * has to be split and no adjacent partition information has to be split up
		 *
		 * EVEN:
		 * |\
		 * |  \
		 * | 2  \
		 * |    / \
		 * |  /  1  \
		 * |/____ ____\
		 *
		 *
		 * ODD:
		 *     /|\
		 *   /2 | 1\
		 * /____|____\
		 *
		 */
		{
			// current child node which is set-up
			CPartition_TreeNode_*current_child_node = p_first_child_node;

			// remaining communication elements on right stack until switching to the second child node
			int counter_remaining_edge_comm_elements;

			if (triangleFactory.isCathetusDataOnLeftStack())
				counter_remaining_edge_comm_elements = p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			else
				counter_remaining_edge_comm_elements = p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						p_parentAdjacentPartitions.cat_adjacent_partitions.begin();
					iter != p_parentAdjacentPartitions.cat_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &i = *iter;

				/*
				 * switch to second sub triangle if updating the adj information of the first one is finished
				 */
				if (counter_remaining_edge_comm_elements == 0)
				{
					assert(current_child_node != p_second_child_node);

					current_child_node = p_second_child_node;

					if (triangleFactory.evenOdd == CTriangle_Enums::EVEN)
						counter_remaining_edge_comm_elements = p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
					else
						counter_remaining_edge_comm_elements = p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
				}

				// create & insert the information about the adjacent partition into the communication information
				current_child_node->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(i);

				counter_remaining_edge_comm_elements -= i.commElements;

				/*
				 * since splitting along the catheti does not split an adjacent partition information,
				 * the counter can never be below 0
				 */
				assert(counter_remaining_edge_comm_elements >= 0);
			}

			assert(counter_remaining_edge_comm_elements == 0);
		}



		/**********************
		 * HYPOTENUSE
		 *
		 * the splitting on the hypotenuse has to consider the new edge comm elements between both subtriangles
		 */
		{
			int counter_remaining_edge_comm_elements;

			/*
			 * setup remaining edge communication elements to consider for the first triangle
			 */
			if (triangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements =
					p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements =
					p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}

			/*
			 * iterate over the first adjacent partition information of the first sub-triangle
			 */
			typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
									p_parentAdjacentPartitions.hyp_adjacent_partitions.begin();

			for (	;
					iter != p_parentAdjacentPartitions.hyp_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.commElements;

				/*
				 * either this is the last adjacent information or we have to split this
				 * adjacency information
				 */
				if (counter_remaining_edge_comm_elements <= 0)
					break;

				/*
				 * insert adj information
				 */
				p_first_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(i);
			}

			assert(counter_remaining_edge_comm_elements <= 0);


			/*
			 * Now we are at the point where an adjacent Information has to be splitted to both sub-triangles
			 *
			 * The edge comm data marked with \\ or // is reconstructed during the next 4 steps
			 * |\
			 * |  \[D]
			 * | 2  \\
			 * |    //
			 * |  //[C]
			 * |//        // \\ [A]
			 *       [B]//  1  \
			 *        //____ ____\
			 */

			CEdgeComm_InformationAdjacentPartition_ &i = *iter;

			/*
			 * [A]
			 *
			 * add remaining adjacent edge comm information
			 */
			if (iter != p_parentAdjacentPartitions.hyp_adjacent_partitions.end())
			{
				// first sub-triangle: new adj information
				p_first_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(
						CEdgeComm_InformationAdjacentPartition_(
								i.partitionTreeNode,
								i.uniqueId,
								// counter_remaining_edge_comm_elements <= 0
								i.commElements + counter_remaining_edge_comm_elements
							)
						);
			}

			/*
			 * [B]
			 */
			// first sub-triangle: shared adj information
			p_first_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(
					CEdgeComm_InformationAdjacentPartition_(
							p_second_child_node,
							p_second_child_node->uniqueId,
							p_splitJoinInformation.number_of_shared_edge_comm_elements
						)
					);

			/*
			 * [C]
			 */
			// second sub-triangle: shared adj information
			p_second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(
					CEdgeComm_InformationAdjacentPartition_(
							p_first_child_node,
							p_first_child_node->uniqueId,
							p_splitJoinInformation.number_of_shared_edge_comm_elements
						)
					);

			/*
			 * [D]
			 */
			if (counter_remaining_edge_comm_elements != 0)
			{
				// second sub-triangle: new adj information to adj partition
				p_second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(
						CEdgeComm_InformationAdjacentPartition_(
								i.partitionTreeNode,
								i.uniqueId,
								-counter_remaining_edge_comm_elements
							)
						);
			}



			/*
			 * second sub-triangle
			 */

			/*
			 * setup remaining edge communication elements for second triangle
			 *
			 * since we already handled some edge comm data elements of the second sub-triangle,
			 * we modify `counter_remaining_edge_comm_elements` relatively to it's current value!
			 */
			if (triangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements +=
					p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements +=
					p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}


			if (iter != p_parentAdjacentPartitions.hyp_adjacent_partitions.end())
			{
				/*
				 * INCREMENT THE ITERATOR RIGHT HERE TO GO TO THE NEXT COMM ELEMENT!
				 */
				iter++;
			}

			for (	;
					iter != p_parentAdjacentPartitions.hyp_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.commElements;

				/*
				 * insert adj information
				 */
				p_second_child_node->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(i);
			}

			assert (counter_remaining_edge_comm_elements == 0);
		}
	}



public:
	/**
	 * update information about adjacent partitions whenever a split or join operation was done
	 */
	unsigned int pass2_updateAdjacentPartitionInformation()
	{
		assert(cEdgeComm_InformationAdjacentPartitions.hyp_swapOrNotToBeSwapped == false);
		assert(cEdgeComm_InformationAdjacentPartitions.cat_swapOrNotToBeSwapped == false);

		/*
		 * HYPOTENUSE
		 */
		bool hypDataOnClockwiseStack = triangleFactory.isHypotenuseDataOnAClockwiseStack();

		if (_pass2_updateAdjacentPartitionInformation_LeftOrRightBorder(
				cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions,
				cEdgeComm_InformationAdjacentPartitions.new_hyp_adjacent_partitions,
				hypDataOnClockwiseStack
		))
			cEdgeComm_InformationAdjacentPartitions.hyp_swapOrNotToBeSwapped = true;

		/*
		 * CATHETI
		 */
		bool catDataOnClockwiseStack = triangleFactory.isCathetusDataOnAClockwiseStack();

		if (_pass2_updateAdjacentPartitionInformation_LeftOrRightBorder(
				cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions,
				cEdgeComm_InformationAdjacentPartitions.new_cat_adjacent_partitions,
				catDataOnClockwiseStack
				))
			cEdgeComm_InformationAdjacentPartitions.cat_swapOrNotToBeSwapped = true;

		return 1;
	}


	/**
	 * iterate over local adjacent partitions for hyp or cat
	 *
	 * the idea is to avoid modification of the local information about adjacent parts
	 */
private:
	bool _pass2_updateAdjacentPartitionInformation_LeftOrRightBorder(
			std::vector<CEdgeComm_InformationAdjacentPartition_> &p_localInformationAdjacentPartitions,		///< cat or hyp adjacency information
			std::vector<CEdgeComm_InformationAdjacentPartition_> &p_new_localInformationAdjacentPartitions,	///< new cat or hyp adjacency information
			bool localStackClockwise
	)
	{
		/*
		 * iterate over all local adjacency informations
		 */
		int counter = 0;
		int remainingCommElements = 0;

		// access first and second child in reversed order?
		bool first_second_reversed = false;

		typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator local_iter = p_localInformationAdjacentPartitions.begin();

		for (	;
				local_iter != p_localInformationAdjacentPartitions.end();
				local_iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &local_InformationAdjacentPartition = *local_iter;

			/*
			 * if the adjacent partition tree node is not a leaf, the partition was split up
			 * during the last traversal.
			 *
			 *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 * maybe we have to modify the edge comm data and do a break here to avoid modifications of
			 * the edge comm data which could be accessed by other adjacent partitions.
			 * modifying this edge comm data can confuse parallel running programs.
			 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 */
			EPARTITION_TRANSFER_STATE transferState = local_InformationAdjacentPartition.partitionTreeNode->cPartition_SplitJoinActions.transferState;

			// splitted_child is possible due to split operation
			assert(transferState != JOINED_PARENT);

			if (	transferState == SPLITTED_PARENT ||
					transferState == JOINED_CHILD
			)
			{
				/*
				 * we call a break here to work on a copy of the adjacent information to avoid further race conditions
				 */
				goto splittedOrJoinedAdjacentPartition;
			}

			counter++;
		}
		return false;

splittedOrJoinedAdjacentPartition:
		/*
		 * this new vector is swapped with the old one (hopefully) after all (also the adjacent ones)
		 * edge communication information was updated
		 */

		// COPY adjacent informations!!!
		p_new_localInformationAdjacentPartitions = p_localInformationAdjacentPartitions;

		local_iter = p_new_localInformationAdjacentPartitions.begin();
		for (int i = 0; i < counter; i++)
				local_iter++;

		for (	;
				local_iter != p_new_localInformationAdjacentPartitions.end();
				local_iter++
		)
		{
for_loop_start:
			CEdgeComm_InformationAdjacentPartition_ &local_InformationAdjacentPartition = *local_iter;

			// this can happen for 2 splitted child nodes
			assert(local_InformationAdjacentPartition.partitionTreeNode->cPartition_SplitJoinActions.transferState != JOINED_PARENT);

			remainingCommElements = local_InformationAdjacentPartition.commElements;

			if (local_InformationAdjacentPartition.partitionTreeNode->cPartition_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   SPLITTED PARENT
				 ***********************************************************************************
				 ***********************************************************************************/

				// store parent partition tree node since this value could be modified
				//CPartitionTreeNode_ *parentPartitionTreeNode = local_InformationAdjacentPartition.partitionTreeNode;
				CPartition_TreeNode_ &adjacent_parentPartitionTreeNode = *(local_InformationAdjacentPartition.partitionTreeNode->cGeneric_TreeNode->cPartition_TreeNode);

				/**
				 * in case that the local triangle was splitted, we have to consider the correct order of edge comms.
				 *
				 * since we know that the adjacent partition can be split only along the hypotenuse, this simplifies our assumptions.
				 */
				first_second_reversed = (adjacent_parentPartitionTreeNode.cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == localStackClockwise);

				/**
				 * check wether the subpartition as well as the adjacent partition was splitted and the current splitted tree is the second child
				 */
				CPartition_TreeNode_ *firstChildToTraverse, *secondChildToTraverse;

				if (triangleFactory.isSecondChild())
					first_second_reversed = !first_second_reversed;

				if (first_second_reversed)
				{
					firstChildToTraverse = adjacent_parentPartitionTreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode;
					secondChildToTraverse = adjacent_parentPartitionTreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode;
				}
				else
				{
					firstChildToTraverse = adjacent_parentPartitionTreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode;
					secondChildToTraverse = adjacent_parentPartitionTreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode;
				}

				if (transferState == SPLITTED_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLITTED CHILD
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentPartition,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						if (remainingCommElements > 0)
						{
							/**
							 * search for remaining edge comm data
							 */

							// a new edge comm information element has to be inserted
							local_iter++;
							local_iter = p_new_localInformationAdjacentPartitions.insert(local_iter, CEdgeComm_InformationAdjacentPartition_());

							/**
							 * if an additional edge comm element was created, maybe those have to be reversed
							 */
							if (triangleFactory.isSecondChild())
							{
								typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator prev_iter = local_iter-1;
								*local_iter = *prev_iter;

								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
										*prev_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							else
							{
								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
										*local_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							continue;
						}

//						if (remainingCommElements < 0)
						{
							/**
							 * here we handle the special case when the adjacent edge comm data was larger than our local one.
							 * in this case, the local one was splitted without a split at the adjacent edge comm information.
							 *
							 * however, we are allowed to simply ignore this case by assuming that the adjacent partition
							 * updates it's edge information data to match to our splitted local edge comm information.
							 */
							continue;
						}
					}

					/*
					 * no match was found in the first partition.
					 *
					 * => simply take the first match of the second adjacent partition.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentPartitionTreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode : adjacent_parentPartitionTreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements <= 0);

					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentPartition,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						assert(remainingCommElements > 0);

						/**
						 * search for remaining edge comm data
						 */

						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = p_new_localInformationAdjacentPartitions.insert(local_iter, CEdgeComm_InformationAdjacentPartition_());

						/**
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (triangleFactory.isSecondChild())
						{
							typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first partition.
					 *
					 * => simply take the first match of the second adjacent partition.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentPartitionTreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode : adjacent_parentPartitionTreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements <= 0);

					continue;
				}

				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentPartition,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						assert(remainingCommElements >= 0);

						// perfect match
						if (remainingCommElements == 0)
							continue;

						/**
						 * search for remaining edge comm data
						 */

						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = p_new_localInformationAdjacentPartitions.insert(local_iter, CEdgeComm_InformationAdjacentPartition_());

						/**
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (triangleFactory.isSecondChild())
						{
							typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}

						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first partition.
					 *
					 * => simply take the first match of the second adjacent partition.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentPartitionTreeNode.cGeneric_TreeNode->first_child_node->cPartition_TreeNode : adjacent_parentPartitionTreeNode.cGeneric_TreeNode->second_child_node->cPartition_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements == 0);

					continue;
				}
			}

			if (local_InformationAdjacentPartition.partitionTreeNode->cPartition_SplitJoinActions.transferState == JOINED_CHILD)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   JOINED CHILD
				 ***********************************************************************************
				 ***********************************************************************************/

				if (transferState == SPLITTED_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLITTED CHILD
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CPartition_TreeNode_ *adjacent_partitionTreeNode = local_InformationAdjacentPartition.partitionTreeNode;

					remainingCommElements = local_InformationAdjacentPartition.commElements;

					// parent triangle
					pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
							local_InformationAdjacentPartition,
							adjacent_partitionTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentPartitions.end())
						break;

					CPartition_TreeNode_ *next_adjacent_parentPartitionTreeNode = (*next_local_iter).partitionTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentPartitionTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentPartition.uniqueId.rawUniqueId)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).commElements;

						pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentPartitionTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentPartition.commElements += (*next_local_iter).commElements;

						local_iter = p_new_localInformationAdjacentPartitions.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentPartitions.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CPartition_TreeNode_ *adjacent_partitionTreeNode = local_InformationAdjacentPartition.partitionTreeNode;

					remainingCommElements = local_InformationAdjacentPartition.commElements;

					// parent triangle
					pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
							local_InformationAdjacentPartition,
							adjacent_partitionTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentPartitions.end())
						break;

					CPartition_TreeNode_ *next_adjacent_parentPartitionTreeNode = (*next_local_iter).partitionTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentPartitionTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentPartition.uniqueId.rawUniqueId)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).commElements;

						pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentPartitionTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentPartition.commElements += (*next_local_iter).commElements;

						local_iter = p_new_localInformationAdjacentPartitions.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentPartitions.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}


				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CPartition_TreeNode_ *adjacent_partitionTreeNode = local_InformationAdjacentPartition.partitionTreeNode;

					remainingCommElements = local_InformationAdjacentPartition.commElements;

					// parent triangle
					pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
							local_InformationAdjacentPartition,
							adjacent_partitionTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentPartitions.end())
						break;

					CPartition_TreeNode_ *next_adjacent_parentPartitionTreeNode = (*next_local_iter).partitionTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */

					if (next_adjacent_parentPartitionTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentPartition.uniqueId.rawUniqueId)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).commElements;

						pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentPartitionTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentPartition.commElements += (*next_local_iter).commElements;

						local_iter = p_new_localInformationAdjacentPartitions.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentPartitions.end())
							break;

						assert(remainingCommElements == 0);
#if 1
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
				}

				continue;
			}
		}
		return true;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent partition
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool pass2_updateAdjacentPartitionInformation_ParentJoinedTriangle(
			CEdgeComm_InformationAdjacentPartition_ &p_localInformationAdjacentPartition,		///< local information about adjacent partitions (equal to *p_local_iter)
			CPartition_TreeNode_ *p_adjacentPartitionTreeNode,									///< adjacent first or second partition tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentPartitions
	)
	{
		if (_pass2_updateAdjacentPartitionInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentPartition,
				p_adjacentPartitionTreeNode->cGeneric_TreeNode->parent_node->cPartition_TreeNode,
				p_remainingCommElements,
				p_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions)
		)
			return true;

#if DEBUG
		bool retval =
#endif
		_pass2_updateAdjacentPartitionInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentPartition,
				p_adjacentPartitionTreeNode->cGeneric_TreeNode->parent_node->cPartition_TreeNode,
				p_remainingCommElements,
				p_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions
				);

		assert(retval);

		// true has to be always returned since some elements are still left
		return true;
	}


private:
	/**
	 * JOIN
	 *
	 * search for adjacency information on the adjacent edge comm information `p_edgeComm_InformationAdjacentPartition`
	 *
	 * return true, when some p_remainingCommElements is undershot and the local edge comm information has to be update once again
	 */
	bool _pass2_updateAdjacentPartitionInformation_ParentJoinedTriangles_SpecificChildEdge(
			CEdgeComm_InformationAdjacentPartition_ &p_localInformationAdjacentPartition,	///< local information about adjacent partitions (equal to *p_local_iter)
			CPartition_TreeNode_ *p_adjacentParentPartitionTreeNode,					///< adjacent parent partition tree node to update adjacency information
			int &p_remainingCommElements,												///< remaining edge comm elements which have to be considered for p_localInformationAdjacentPartitions
			typename std::vector<CEdgeComm_InformationAdjacentPartition_> &p_edgeComm_ChildInformationAdjacentPartition
	)
	{
		/**
		 * iterate over all edge comm data of the adjacently lying joined child triangle
		 */
		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = p_edgeComm_ChildInformationAdjacentPartition.begin();
				iter != p_edgeComm_ChildInformationAdjacentPartition.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_informationAdjacentPartition = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local partition
			 */

			/*
			 * test with currently local uniqueId but also with the child's uniqueId
			 */
			if (transferState == SPLITTED_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != (uniqueId.rawUniqueId >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentPartition.uniqueId.rawUniqueId >> 1) != uniqueId.rawUniqueId)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != uniqueId.rawUniqueId)
					continue;
			}


			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentPartition.uniqueId = p_adjacentParentPartitionTreeNode->uniqueId;
			p_localInformationAdjacentPartition.partitionTreeNode = p_adjacentParentPartitionTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentPartition.commElements)
			{
				p_localInformationAdjacentPartition.commElements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentPartition.commElements)
			{
				p_localInformationAdjacentPartition.commElements = adjacent_informationAdjacentPartition.commElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;


				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_ChildInformationAdjacentPartition.end())
					return true;
				CEdgeComm_InformationAdjacentPartition_ &adjacent_informationAdjacentPartition = *iter;

				if (transferState == SPLITTED_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != (uniqueId.rawUniqueId >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentPartition.uniqueId.rawUniqueId >> 1) != uniqueId.rawUniqueId)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != uniqueId.rawUniqueId)
						return true;
				}

				p_localInformationAdjacentPartition.commElements += adjacent_informationAdjacentPartition.commElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other sibling triangle cares about getting the right data
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentPartition.commElements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentPartition.commElements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;
				return true;
			}

			assert(false);
		}

		return false;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent partition
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle(
			CEdgeComm_InformationAdjacentPartition_ &p_localInformationAdjacentPartition,		///< local information about adjacent partitions (equal to *p_local_iter)
			CPartition_TreeNode_ *p_adjacentPartitionTreeNode,									///< adjacent first or second partition tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentPartitions
	)
	{
		if (_pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentPartition,
					p_adjacentPartitionTreeNode,
					p_remainingCommElements,
					p_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions
		))
			return true;

		return _pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentPartition,
					p_adjacentPartitionTreeNode,
					p_remainingCommElements,
					p_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions
				);
	}



private:
	/**
	 * update the local adjacency information along the cat or hyp
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentPartitionInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
			CEdgeComm_InformationAdjacentPartition_ &p_localInformationAdjacentPartition,		///< local information about adjacent partitions (equal to *p_local_iter)
			CPartition_TreeNode_ *p_adjacentPartitionTreeNode,									///< adjacent first or second partition tree node to search for adjacent edge comm information
			int &p_remainingCommElements,														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentPartitions
			typename std::vector<CEdgeComm_InformationAdjacentPartition_> &p_edgeComm_InformationAdjacentPartition
	)
	{
		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = p_edgeComm_InformationAdjacentPartition.begin();
				iter != p_edgeComm_InformationAdjacentPartition.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_informationAdjacentPartition = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local partition
			 */
			if (transferState == SPLITTED_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != (uniqueId.rawUniqueId >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentPartition.uniqueId.rawUniqueId >> 1) != uniqueId.rawUniqueId)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != uniqueId.rawUniqueId)
					continue;
			}

			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentPartition.uniqueId = p_adjacentPartitionTreeNode->uniqueId;
			p_localInformationAdjacentPartition.partitionTreeNode = p_adjacentPartitionTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentPartition.commElements)
			{
				p_localInformationAdjacentPartition.commElements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentPartition.commElements)
			{
				p_localInformationAdjacentPartition.commElements = adjacent_informationAdjacentPartition.commElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;

				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_InformationAdjacentPartition.end())
					return true;
				CEdgeComm_InformationAdjacentPartition_ &adjacent_informationAdjacentPartition = *iter;


				/*
				 * test whether the adjacent id is equal to the id of the local partition
				 */
				if (transferState == SPLITTED_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != (uniqueId.rawUniqueId >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentPartition.uniqueId.rawUniqueId >> 1) != uniqueId.rawUniqueId)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentPartition.uniqueId.rawUniqueId != uniqueId.rawUniqueId)
						return true;
				}

				p_localInformationAdjacentPartition.commElements += adjacent_informationAdjacentPartition.commElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other
			 * sibling triangle cares about getting the right data.
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentPartition.commElements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentPartition.commElements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentPartition.commElements;
				return true;
			}

			assert(false);
		}

		return false;
	}



public:
	/**
	 * this method has to be executed for all nodes!
	 */
	static void pass3_swapAndCleanAfterUpdatingEdgeComm(CGeneric_TreeNode_ *i_cGeneric_TreeNode)
	{
		if (i_cGeneric_TreeNode->cPartition_TreeNode == nullptr)
			return;

		CPartition_TreeNode_ *cPartition_TreeNode = i_cGeneric_TreeNode->cPartition_TreeNode;

		/*
		 * free child nodes due to a join operation
		 */
		if (i_cGeneric_TreeNode->delayed_child_deletion)
		{
			assert(cPartition_TreeNode->cPartition_SplitJoinActions.transferState == JOINED_PARENT);
			i_cGeneric_TreeNode->freeChilds();
		}
		else
		{
#if DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
			if (cPartition_TreeNode->cPartition_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				delete cPartition_TreeNode;
				i_cGeneric_TreeNode->cPartition_TreeNode = nullptr;
				return;
			}
#endif
		}

		cPartition_TreeNode->cEdgeComm_InformationAdjacentPartitions.swapAndCleanAfterUpdatingEdgeComm();

		cPartition_TreeNode->cPartition_SplitJoinActions.transferState = NO_TRANSFER;
	}



public:
	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CPartition_SplitJoinActions &p
	)
	{
		std::cout << " + transferState: " << p.transferState << std::endl;
		return co;
	}
};

#endif /* CPARTITION_SPLITJOINACTION_HPP_ */
