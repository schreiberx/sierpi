/*
 * CMainThreading_Interface.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADING_INTERFACE_HPP_
#define CMAINTHREADING_INTERFACE_HPP_

class CMainThreading_Interface
{
public:
	/*
	 * interfaces which has to be provided by the subclass
	 */
	virtual int getNumberOfThreadsToUse() = 0;
	virtual void setNumberOfThreadsToUse(int i_number_of_threads_to_use) = 0;
	virtual int getVerboseLevel() = 0;

	virtual int getGridInitialRecursionDepth() = 0;
	virtual unsigned long long getSimLoopSumNumberOfTriangles() = 0;

	/// this handler actually executed the simulation loop
	virtual bool simulationLoopIteration() = 0;



	/*
	 * interfaces which have to be provided by the CMainThreading* class
	 */

	/**
	 * setup the threading support (e. g. allocate ressources)
	 */
	virtual void threading_setup() = 0;

	/**
	 * full simulation loop for benchmarks
	 */
	virtual void threading_simulationLoop() = 0;

	/**
	 * single iteration executed solely by GUI
	 */
	virtual bool threading_simulationLoopIteration() = 0;

	/**
	 * shutdown the threading support
	 */
	virtual void threading_shutdown() = 0;

	/**
	 * set number of threads
	 */
	virtual void threading_setNumThreads(int i) = 0;


	virtual ~CMainThreading_Interface()
	{
	}
};



#endif /* CMAINTHREADING_INTERFACE_HPP_ */
