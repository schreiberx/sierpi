/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGTBB_HPP_
#define CMAINTHREADINGTBB_HPP_

#include <tbb/task_scheduler_init.h>

#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
    tbb::task_scheduler_init *tbb_task_scheduler_init;


public:
	void threading_setup()
	{
		int n = getNumberOfThreadsToUse();

		// setup scheduler
		if (n == -1)
			setNumberOfThreadsToUse(tbb::task_scheduler_init::default_num_threads());

		tbb_task_scheduler_init = new tbb::task_scheduler_init(n);

		if (getVerboseLevel() > 5)
			std::cout << "tbb::task_scheduler_init::default_num_threads(): " << tbb::task_scheduler_init::default_num_threads() << std::endl;
	}


	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}


	bool threading_simulationLoopIteration()
	{
		return simulationLoopIteration();
	}


	void threading_shutdown()
	{
		delete tbb_task_scheduler_init;
	}


	void threading_setNumThreads(int i)
	{
		delete tbb_task_scheduler_init;

		setNumberOfThreadsToUse(i);

		tbb_task_scheduler_init = new tbb::task_scheduler_init(i);
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGTBB_HPP_ */
