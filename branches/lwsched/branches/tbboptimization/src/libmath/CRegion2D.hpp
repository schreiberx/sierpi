/*
 * CRegion2D.hpp
 *
 *  Created on: Sep 5, 2011
 *
 *      Author: Martin Schreiber (schreiberx@gmail.com)
 */

#ifndef CREGION2D_HPP_
#define CREGION2D_HPP_

template <typename T>
class CRegion2D
{
public:
	// region limits along x axe
	T x_min, x_max;

	// region limits along y axe
	T y_min, y_max;

	CRegion2D()
	{
		reset();
	}

	void reset()
	{
		x_min = CMath::numeric_inf<T>();
		x_max = -CMath::numeric_inf<T>();
		y_min = CMath::numeric_inf<T>();
		y_max = -CMath::numeric_inf<T>();
	}

	void extend(
			const T pos_x,
			const T pos_y
	)
	{
		x_min = CMath::min(pos_x, x_min);
		x_max = CMath::max(pos_x, x_max);

		y_min = CMath::min(pos_y, y_min);
		y_max = CMath::max(pos_y, y_max);
	}

	void scale(
			const T scalar
	)
	{
		x_min *= scalar;
		x_max *= scalar;

		y_min *= scalar;
		y_max *= scalar;
	}
};



template <class T>
inline
::std::ostream&
operator<<(
		::std::ostream &co,
		 const CRegion2D<T> &r
)
{
	return co << "[" << r.x_min << ", " << r.x_max << "]x[" << r.y_min << ", " << r.y_max << "]";
}

#endif /* CREGION2D_HPP_ */
