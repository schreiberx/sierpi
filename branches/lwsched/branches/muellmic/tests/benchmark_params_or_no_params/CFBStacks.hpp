/*
 * CFBStacks.hpp
 *
 *  Created on: Jan 11, 2011
 *      Author: schreibm
 */

#ifndef CFBSTACKS_HPP_
#define CFBSTACKS_HPP_

#include "CStack.hpp"

/**
 * class to handle the 2 stacks simultaneously.
 * this is usually the case when moving data from
 * one stack to another (e. g. the structure stack)
 */
template <typename T>
class CFBStacks
{
public:
	CStack<T> forward;
	CStack<T> backward;

	typedef enum {
		FORWARD = 0,
		BACKWARD = 1
	} stack_format_t;

	bool direction;

	CFBStacks()	:
		direction(FORWARD)
	{
	}

	CFBStacks(size_t size)	:
		forward(size),
		backward(size),
		direction(FORWARD)
	{
	}

#ifndef NDEBUG
	size_t getSize()
	{
		if (direction == FORWARD)
			return forward.size;
		else
			return backward.size;
	}
#endif

	void resize(size_t size)
	{
		forward.resize(size);
		backward.resize(size);
	}

	unsigned int structure_getNumberOfTriangles()	const
	{
		if (direction == FORWARD)
			return (forward.getStackElementCounter()+3) / 2;
		else
			return (backward.getStackElementCounter()+3) / 2;
	}
};



inline
::std::ostream&
operator<<(::std::ostream &co, CFBStacks<char> &stacks)
{
	if (stacks.direction == CFBStacks<char>::FORWARD)
		for (unsigned int i = 0; i < stacks.forward.getStackElementCounter(); i++)
			co << (int)stacks.forward.getElement(i);
	else
		for (unsigned int i = 0; i < stacks.backward.getStackElementCounter(); i++)
			co << (int)stacks.backward.getElement(i);

	return co;
}

template <class T>
inline
::std::ostream&
operator<<(::std::ostream &co, CFBStacks<T> &stacks)
{
	if (stacks.direction == CFBStacks<T>::FORWARD)
		co << stacks.forward;
	else
		co << stacks.backward;

	return co;
}

#endif /* CFBSTACKS_HPP_ */
