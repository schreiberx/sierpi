#! /bin/bash

DEPTH=22
ITERS=10


# without parameters
echo
echo "---------------------------------------"
echo "| WITHOUT PARAMETERS (GCC)"
echo "---------------------------------------"
./Release/benchmark $DEPTH 0 10

# with parameters
echo
echo "---------------------------------------"
echo "| WITH PARAMETERS (GCC)"
echo "---------------------------------------"
./Release/benchmark $DEPTH 1 10



# without parameters
echo
echo "---------------------------------------"
echo "| WITHOUT PARAMETERS (ICC)"
echo "---------------------------------------"
./ReleaseICC/benchmark $DEPTH 0 10

# with parameters
echo
echo "---------------------------------------"
echo "| WITH PARAMETERS (ICC)"
echo "---------------------------------------"
./ReleaseICC/benchmark $DEPTH 1 10

