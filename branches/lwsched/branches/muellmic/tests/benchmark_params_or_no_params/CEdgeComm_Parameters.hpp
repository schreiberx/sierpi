/*
 * Edge Communication functions. This functions include the parameters
 * edge_type_hyp, edge_type_right_edge and edge_type_left_edge associated
 * to the three sides of the triangle. The values can be 'o', 'n' or 'b'
 * when they are an old edge, a new edge or a border edge, respectively.
 */

void Ke_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ho_forward(edge_type_left_edge, edge_type_hyp, 'n');
		Vo_forward(edge_type_right_edge, 'o', edge_type_hyp);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
}

void Ho_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ve_forward(edge_type_right_edge, 'n', edge_type_hyp);
		Ke_forward(edge_type_left_edge, edge_type_hyp, 'o');
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

}

void Ve_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ho_forward(edge_type_left_edge, edge_type_hyp, 'n');
		Ko_forward(edge_type_right_edge, 'o', edge_type_hyp);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
}

void Ko_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		He_forward(edge_type_right_edge, 'n', edge_type_hyp);
		Ve_forward(edge_type_left_edge, edge_type_hyp, 'o');
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
}

void He_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Vo_forward(edge_type_left_edge, edge_type_hyp, 'n');
		Ko_forward(edge_type_right_edge, 'o', edge_type_hyp);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
}

void Vo_forward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		He_forward(edge_type_right_edge, 'n', edge_type_hyp);
		Ke_forward(edge_type_left_edge, edge_type_hyp, 'o');
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
}

/*
 * Backward Traversators
 */

void Ke_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Vo_backward(edge_type_right_edge, 'n', edge_type_hyp);
		Ho_backward(edge_type_left_edge, edge_type_hyp, 'o');
		return;
	}
	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
						&right_edge_data, &left_edge_data);
				return;
			} else {
				kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
						&right_edge_data);
				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		}
	} else {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
	}

}
void Vo_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ke_backward(edge_type_left_edge, edge_type_hyp, 'n');
		He_backward(edge_type_right_edge, 'o', edge_type_hyp);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
						&right_edge_data, &left_edge_data);
				return;
			} else {
				kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
						&right_edge_data);
				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		}
	} else {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
	}

}

void Ho_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ke_backward(edge_type_left_edge, edge_type_hyp, 'n');
		Ve_backward(edge_type_right_edge, 'o', edge_type_hyp);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
						&right_edge_data, &left_edge_data);
				return;
			} else {
				kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
						&right_edge_data);
				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		}
	} else {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
	}

}

void He_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ko_backward(edge_type_right_edge, 'n', edge_type_hyp);
		Vo_backward(edge_type_left_edge, edge_type_hyp, 'o');
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
						&right_edge_data, &left_edge_data);
				return;
			} else {
				kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
						&right_edge_data);
				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		}
	} else {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
	}
}

void Ko_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ve_backward(edge_type_left_edge, edge_type_hyp, 'n');
		He_backward(edge_type_right_edge, 'o', edge_type_hyp);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	/*
	 * elementAction
	 */
	if (edge_type_hyp == 'b') {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
		return;
	} else if (edge_type_right_edge == 'b') {
		if (edge_type_left_edge == 'b') {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		}
	} else if (edge_type_left_edge == 'b') {
		kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
				&right_edge_data);
		return;
	} else {
		kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
				&right_edge_data, &left_edge_data);
		return;
	}

}

void Ve_backward(char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge) {

	if (structure_lifo_in.getNextData()) {
		Ko_backward(edge_type_right_edge, 'n', edge_type_hyp);
		Ho_backward(edge_type_left_edge, edge_type_hyp, 'o');
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp == 'b') {
		kernelClass.elementAction_BEE(element_data, &right_edge_data,
				&left_edge_data);
		return;
	} else if (edge_type_right_edge == 'b') {
		if (edge_type_left_edge == 'b') {
			kernelClass.elementAction_EBB(element_data, &hyp_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBE(element_data, &hyp_edge_data,
					&left_edge_data);
			return;
		}
	} else if (edge_type_left_edge == 'b') {
		kernelClass.elementAction_EEB(element_data, &hyp_edge_data,
				&right_edge_data);
		return;
	} else {
		kernelClass.elementAction_EEE(element_data, &hyp_edge_data,
				&right_edge_data, &left_edge_data);
		return;
	}

}

