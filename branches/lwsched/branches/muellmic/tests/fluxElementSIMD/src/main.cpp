#include "CEdgeComm_Tsunami_1stOrder.hpp"

#include <stdint.h>
#include "simulations/tsunami_common/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "lib/CStopwatch.hpp"
#include "libmath/CMath.hpp"
#include <xmmintrin.h>

// gnu/icc compiler
#define ALIGN16		__attribute__ ((aligned (16)))

// MS$!@#$ compiler
//#define ALIGN16	__declspec(align(16))


// convenient typedef
typedef TTsunamiDataScalar T;


/*******
 * TEST KERNEL
 */

#define MAX_ELEMENTS	(1024*1024*16+2)
//#define MAX_ELEMENTS	(1024*16+1)

sierpi::kernels::CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET> edgeComm_Tsunami_1stOrder(MAX_ELEMENTS);


float rand01()
{
	return ((float)std::rand()/(float)RAND_MAX);
}

void testrun(
		bool use_simd,
		int counter,
		bool output,
		int max_depth,
		CTsunamiElementData *elementData
)
{
	CStopwatch cStopwatch;

	edgeComm_Tsunami_1stOrder.use_simd = use_simd;

	CTsunamiEdgeData *hyp_edge = new CTsunamiEdgeData[counter];
	CTsunamiEdgeData *left_edge = new CTsunamiEdgeData[counter];
	CTsunamiEdgeData *right_edge = new CTsunamiEdgeData[counter];

	for (int i = 0; i < counter; i++)
	{
		hyp_edge[i].h = elementData[i].hyp_edge.h+0.5;
		hyp_edge[i].qx = elementData[i].hyp_edge.qx-2.5;
		hyp_edge[i].qy = elementData[i].hyp_edge.qy-1.5;

		left_edge[i].h = elementData[i].left_edge.h+0.23;
		left_edge[i].qx = elementData[i].left_edge.qx+2.5;
		left_edge[i].qy = elementData[i].left_edge.qy-2.5;

		right_edge[i].h = elementData[i].right_edge.h-0.1;
		right_edge[i].qx = elementData[i].right_edge.qx-1.5;
		right_edge[i].qy = elementData[i].right_edge.qy-2.5;

	}

	cStopwatch.start();

	int depth = 0;
	for (int i = 0; i < counter; i++)
	{
		edgeComm_Tsunami_1stOrder.elementAction_EEE(
				0, -1, 0.5, 0.5, -0.5, 0.5,
				depth,
				&elementData[i],
				&hyp_edge[i], &left_edge[i], &right_edge[i]
			);

		depth = (depth+1) % max_depth;
	}

	cStopwatch.stop();

	if (use_simd)
		edgeComm_Tsunami_1stOrder.flush();

	if (output)
	{
		if (counter < 10)
		{
			for (int i = 0; i < counter; i++)
			{
				std::cout << elementData[i] << std::endl;

			}
		}
		else
		{
			std::cout << elementData[counter/2] << std::endl;

			if (counter > 0)
				std::cout << elementData[counter/2+1] << std::endl;
		}

		std::cout << "TIME: " << cStopwatch.time << std::endl;
	}

	delete hyp_edge;
	delete right_edge;
	delete left_edge;
}

bool cmp(float a, float b, float epsilon, CTsunamiElementData *e1, CTsunamiElementData *e2, int i)
{
	if (std::abs((a-b)/(0.5*(a+b))) > epsilon)
	{
		std::cout << "DIFF r " << i << std::endl;
		std::cout << *e1;
		std::cout << *e2;
		return true;
	}

	return false;
}

int main()
{
	CStopwatch cStopwatch;
	int counter = MAX_ELEMENTS;

	CTsunamiElementData *elementData1 = new CTsunamiElementData[counter];
	CTsunamiElementData *elementData2 = new CTsunamiElementData[counter];


	for (int i = 0; i < counter; i++)
	{
		elementData1[i].hyp_edge.h = 50.5+rand01()*0.5;
		elementData1[i].hyp_edge.qx = 20+rand01()*1.5;
		elementData1[i].hyp_edge.qy = 10+rand01()*2.5;

		elementData1[i].left_edge.h = 50-rand01()*0.5;
		elementData1[i].left_edge.qx = 20-rand01()*1.5;
		elementData1[i].left_edge.qy = 10+rand01()*1.5;

		elementData1[i].right_edge.h = 51-rand01()*0.5;
		elementData1[i].right_edge.qx = 20-rand01()*0.5;
		elementData1[i].right_edge.qy = 10-rand01()*1.5;

		elementData2[i] = elementData1[i];
	}

	edgeComm_Tsunami_1stOrder.setup_RootPartition(0.1, 100, 9.81);
	int max_depth = 8;

	std::cout << "======== NO SIMD =========" << std::endl;
	testrun(false, counter, true, max_depth, elementData1);

	std::cout << "======== SIMD =========" << std::endl;
	testrun(true, counter, true, max_depth, elementData2);

	std::cout << "======== Comparing " << counter << " Elements =========" << std::endl;

	static T epsilon = 0.001;
	for (int i = 0; i < counter; i++)
	{
		if (!cmp(elementData1[i].hyp_edge.h, elementData2[i].hyp_edge.h, epsilon, &elementData1[i], &elementData2[i], i))
			if (!cmp(elementData1[i].right_edge.h, elementData2[i].right_edge.h, epsilon, &elementData1[i], &elementData2[i], i))
				if (!cmp(elementData1[i].left_edge.h, elementData2[i].left_edge.h, epsilon, &elementData1[i], &elementData2[i], i))

					if (!cmp(elementData1[i].hyp_edge.qx, elementData2[i].hyp_edge.qx, epsilon, &elementData1[i], &elementData2[i], i))
						if (!cmp(elementData1[i].right_edge.qx, elementData2[i].right_edge.qx, epsilon, &elementData1[i], &elementData2[i], i))
							if (!cmp(elementData1[i].left_edge.qx, elementData2[i].left_edge.qx, epsilon, &elementData1[i], &elementData2[i], i))

								if (!cmp(elementData1[i].hyp_edge.qy, elementData2[i].hyp_edge.qy, epsilon, &elementData1[i], &elementData2[i], i))
									if (!cmp(elementData1[i].right_edge.qy, elementData2[i].right_edge.qy, epsilon, &elementData1[i], &elementData2[i], i))
										cmp(elementData1[i].left_edge.qy, elementData2[i].left_edge.qy, epsilon, &elementData1[i], &elementData2[i], i);

	}

	delete elementData1;
	delete elementData2;
}
