// use opengl 3.2 core version!
#version 150

in vec4 vertex_position;
uniform mat4 pvm_matrix;
out vec4 gl_Position;


void main(void)
{
	gl_Position = pvm_matrix*vertex_position;
}
