tbb::task_scheduler_init::default_num_threads(): 40
AFFINITY: threads to use: 40
AFFINITY: max cores: 40
AFFINITY: setting affinities with distance of 1: [ 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39  ]
 + Flux solver: local lax friedrich
 + Bathymetry kernels: 0
 + CFL: 0.15
 + RK ORDER: 1
 + Order of basis functions: 1
 + Adaptive conforming subpartition skipping:  [DEACTIVATED]
 + Number of threads to use: 40
 + Verbose: 5
 + Terminate simulation after #n equal timesteps: -1
 + Timesteps: -1
 + Output .vtk files each #nth timestep: -1
 + Output .vtk files each n simulation seconds: -1
 + Output verbose information each #nth timestep: 100
 + Output verbose information each n computation seconds: 1
 + Output simulation data filename: frame_%08i.vtk
 +
 + Timestep size: 1e-08
 + InitialDepth: 4
 + MinDepth: 4
 + MaxDepth: 18
 + WorldSceneId: 3
 + WaterSurfaceId: 0
 + TerrainSceneId: 0
 + Partition size when to request split: 8192
 + Partition size when to request join (both childs have to request a join): 4096
 + Timestep Size: 1e-08
 + ADAPTIVE_SUBPARTITION_STACKS enabled
 + Initial number of triangles before setting up column: 32
 + splitted to 2 partitions with 32 triangles
[ SETUP COLUMN ]
.................. + refined to 18190 triangles
 + pre splitting partitions: 6 partitions
SETUP TIME: 0.026789
[ START ]
=========================================
   + 0	TIMESTEP
   + 0	SIMULATION_TIME
   + 18190	TRIANGLES
   + 1.80412	ElementData Megabyte per Timestep (RW)
   + 1e-08	TIMESTEP SIZE
   + 6	PARTITIONS
=========================================
   + 4.80262 Overall MTPS (Mega Triangles per second)
   + 0 Last Second MTPS (Mega Triangles per second)
   + 100	TIMESTEP
   + 1.55644	SIMULATION_TIME
   + 25400	TRIANGLES
   + 2.51923	ElementData Megabyte per Timestep (RW)
   + 0.0157195	TIMESTEP SIZE
   + 7	PARTITIONS
