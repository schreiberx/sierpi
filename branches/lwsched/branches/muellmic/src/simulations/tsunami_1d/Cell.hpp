/*
 * Cell.h
 *
 *  Created on: Apr 17, 2012
 *      Author: breuera
 */

#ifndef CELL_HPP_
#define CELL_HPP_

//dependencys for CTsunamiTypes_0thOrder.hpp
#include "../../libmath/CMath.hpp"
#include "../../config.h"
#include "../tsunami_common/CTsunamiConfig.hpp"

#include "../tsunami_common/types/CTsunamiTypes_0thOrder.hpp"

namespace oneDimensional {
  template <typename T> class Cell;
}

/**
 * Representation of a cell within the computational domain.
 * TODO: Currently limited to order zero.
 */
template <typename T> class oneDimensional::Cell {
  //private:
  //! cell width
  T cellWidth;

  //! holds the element data: water height, momentum and bathymetry
  CTsunamiElementData elementData;

  //! maximum timestep allowed due to the CFL-condition
  T timeStepWidth;


  public:
    /**
     * Constructor of a cell.
     *
     * @param i_cellWidth size of the cell.
     * @param i_h water height within the cell.
     * @param i_hu momentum within the cell.
     */
    Cell( const T i_cellWidth = (T)0.,
          const T i_h = (T)0.,
          const T i_hu = (T)0.,
          const T i_b = (T)0. ) {
      cellWidth = i_cellWidth;

      elementData.dofs.h = i_h;
      elementData.dofs.qx = i_hu;
      elementData.dofs.qy = (T)0.; // never used!
      elementData.dofs.b = i_b;


      timeStepWidth = (T)0.;
    }
    virtual ~Cell() {
      // TODO Auto-generated destructor stub
    }

    /**
     * Get the cell data.
     * @return cell data.
     */
    CTsunamiEdgeData& getCellData() {
      return elementData.dofs;
    }

    /**
     * Get the water height.
     * @return water height.
     */
    T const getWaterHeight() {
      return elementData.dofs.h;
    }

    /**
     * Get the momentum.
     * @return momentum.
     */
    T const getMomentum() {
      return elementData.dofs.qx;
    }

    /**
     * Get the bathymetry.
     * @return bathymetry.
     */
    T const getBathymetry() {
      return elementData.dofs.b;
    }

    /**
     * Get the cell width.
     * @return cell width.
     */
    T const getCellWidth() {
      return cellWidth;
    }

    /**
     * Set the cell width.
     * @param i_cellWidth cell width.
     */
    void setCellWidth(const T i_cellWidth) {
      assert(i_cellWidth > (T) 0);
      cellWidth = i_cellWidth;
    }

    /**
     * Set the cells maximum time step width.
     * @param i_timeStepWidth time step width.
     */
    void setTimeStepWidth(const T i_timeStepWidth) {
      timeStepWidth = i_timeStepWidth;
    }

    /**
     * Get the cells maximum time step width.
     *
     * @return time step width.
     */
    const T getTimeStepWidth() {
      return timeStepWidth;
    }
};
#endif /* CELL_HPP_ */
