/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelVertexDataCommTraversals.hpp
 *
 *  Created on: 02 March, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_


#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CPartition_ExchangeVertexDataCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * VERTEX COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) vertex communication
 *  c) last traversal
 */
class CHelper_GenericParallelVertexDataCommTraversals
{

	/**
	 * run the edge comm traversals
	 *
	 * return number of vertices to be rendered
	 */
public:
	template<
		typename CPartition_TreeNode_,		/// partition type offering necessary structures, edge comm data, etc.
		typename CSimulation_Cluster,		/// type of user-defined cluster handler
		typename TVertexDataCommTraversator,	/// Traversator including kernel
		typename TVertexData,					/// type of edge communication data
		typename CStackAccessors_,			/// accessors to adjacent stacks
		typename TAutoLambda					/// lambda function executed at first in firstPass with node as parameter
	>
	static void action(
			TVertexDataCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			CPartition_ExchangeVertexDataCommData<
				CPartition_TreeNode_,
				TVertexData,
				CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, TVertexData>,
				typename TVertexDataCommTraversator::CKernelClass
			>
			CSimulation_Cluster::*i_simulationVertexCommSubClass,
			CGenericTreeNode<CPartition_TreeNode_> *i_node,
			TAutoLambda node_lambda_func
	)
	{
#if COMPILE_WITH_PARALLEL_VERTEX_COMM

		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_node->traverse_GenericTreeNode_Parallel_Scan(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				node_lambda_func(node);

				node->cStacks->vertex_data_comm_left_edge_stack.empty();
				node->cStacks->vertex_data_comm_right_edge_stack.empty();

				(node->cCluster->*(i_simulationSubClass)).action_FirstPass(node->cStacks);
			}
		);


		/*
		 * Compute fluxes using uniqueIDs to avoid double flux evaluation:
		 *
		 * The sub-partition with the relation 'uniqueID < adjacentUniqueID' is responsible to compute the fluxes
		 * also in a writing manner for the adjacent one.
		 *
		 * in FLUX COMM PASS:
		 * 1) The responsible sub-partition first fetches the data from the adjacent partition
		 *    to the exchange edge comm data stacks.
		 *
		 * 2) The fluxes are computed for all fetched edge communication data by the responsible sub-partition.
		 *
		 * in SECOND PASS:
		 * 3) Storing the fluxes to the local_edge_comm_data_stack and exchange_edge_comm_data_stack, pulling
		 *    the edge communication data from the sub-partitions with 'uniqueID > adjacentUniqueID' fetches the
		 *    already computed fluxes.
		 */
		/*
		i_rootNode->traverse_PartitionTreeNode_Parallel_Scan(
				[=](CPartition_ *node)
				{
					// pull edge data only in one direction using uniqueIDs as relation and compute the flux
					(node->cCluster->*(i_simulationEdgeCommSubClass)).pullEdgeCommData_1stPass();
				}
		);
		*/

		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent partitions
		 */
		/*
		 * WARNING: this is a serial call since the visualization has to be driven on the 1st thread only!
		 */


		i_node->traverse_GenericTreeNode<false>(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					/**
					 * INTER-PARTITION-VERTEX DATA COMMUNICATION
					 */
					/// handler for edge communication with adjacent partitions
#if 1

#if 0
					std::cout << "LOCAL RIGHT STACK DATA: " << &(node->cStacks->vertex_data_comm_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT STACK DATA: " << &(node->cStacks->vertex_data_comm_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

#endif

					CPartition_ExchangeVertexDataCommData<
							CPartition_TreeNode_,
							TVertexData,
							CStackAccessors_,
							typename TVertexDataCommTraversator::CKernelClass
						> i_simulationVertexCommSubClass(
								node,
								&((node->cCluster->*(i_simulationSubClass)).cKernelClass)
							);

					i_simulationVertexCommSubClass.pullVertexCommData();

#if 0
					std::cout << "LOCAL RIGHT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;
#endif


/*					std::cout << "LOCAL RIGHT STACK DATA: " << &(node->cStacks->vertex_data_comm_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT STACK DATA: " << &(node->cStacks->vertex_data_comm_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;
*/

//					node->cStacks->vertex_data_comm_left_edge_stack.setStackElementCounter(0);
//					node->cStacks->vertex_data_comm_right_edge_stack.setStackElementCounter(0);

#else

					std::cout << "LOCAL RIGHT STACK DATA: " << &(node->cStacks->vertex_data_comm_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT STACK DATA: " << &(node->cStacks->vertex_data_comm_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					// LEFT STACK
					node->cStacks->vertex_data_comm_exchange_left_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack());

					for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
					{
						TTsunamiVisualizationVertexData &v = node->cStacks->vertex_data_comm_left_edge_stack.getElementAtIndex(i);

						TTsunamiVertexScalar inv = 1.0/v.normalization_factor;
						v.normal[0] *= inv;
						v.normal[1] *= inv;
						v.normal[2] *= inv;
						v.height *= inv;
						v.normalization_factor = 1.0;

						node->cStacks->vertex_data_comm_exchange_left_edge_stack.setElementAtIndex(i, v);
					}
					node->cStacks->vertex_data_comm_left_edge_stack.setStackElementCounter(0);

					// RIGHT STACK
					node->cStacks->vertex_data_comm_exchange_right_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack());

					for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
					{
						TTsunamiVisualizationVertexData &v = node->cStacks->vertex_data_comm_right_edge_stack.getElementAtIndex(i);

						TTsunamiVertexScalar inv = 1.0/v.normalization_factor;
						v.normal[0] *= inv;
						v.normal[1] *= inv;
						v.normal[2] *= inv;
						v.height *= inv;
						v.normalization_factor = 1.0;

						node->cStacks->vertex_data_comm_exchange_right_edge_stack.setElementAtIndex(i, v);
					}
					node->cStacks->vertex_data_comm_right_edge_stack.setStackElementCounter(0);


					std::cout << "LOCAL RIGHT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_right_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_right_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_right_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

					std::cout << "LOCAL LEFT EXCHANGE STACK DATA: " << &(node->cStacks->vertex_data_comm_exchange_left_edge_stack) << std::endl;
					for (size_t i = 0; i < node->cStacks->vertex_data_comm_exchange_left_edge_stack.getNumberOfElementsOnStack(); i++)
						std::cout << node->cStacks->vertex_data_comm_exchange_left_edge_stack.getElementAtIndex(i).validation << std::endl;
					std::cout << std::endl;

#endif


					// run computation based on newly set-up stacks (run parallel version)
					(node->cCluster->*(i_simulationSubClass)).action_SecondPass_Parallel(node->cStacks);


//					std::cout << "PROCESSING OF SUB-PARTITION FINISHED" << std::endl;
				}

		);
/*

		std::cout << "FIN" << std::endl;
		std::cout << "FIN" << std::endl;
		std::cout << "FIN" << std::endl;
		exit(-1);
*/

#endif
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
