/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelAdaptivityTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_

#include <stdint.h>
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libmath/CMath.hpp"

/**
 * \brief this class implements helper methods which abstract the different phases for adaptive traversals
 *
 * in particular:
 *  a) first forward traversal
 *  b) finished?
 *    c) edge communication
 *    d) backward traversal
 *       forward traversal
 *    finished? not->goto (b)
 *  e) last traversal
 */
class CHelper_GenericParallelAdaptivityTraversals
{
public:
	/**
	 * run the adaptive method
	 */
	template<
		bool i_avoid_scan_based_traversals,
		typename CPartition_TreeNode_,
		typename CSimulation_Parallel_Cluster,
		typename CAdaptiveTraversatorClass,
		typename CStackAccessors,
		typename TCFLReduceValue
	>
	static void action(
			CAdaptiveTraversatorClass CSimulation_Parallel_Cluster::*p_adaptiveTraversator_SubClass,
			CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> CSimulation_Parallel_Cluster::*p_simulationExchangeEdgeComm_SubClass,
			CGenericTreeNode<CPartition_TreeNode_> *i_node,

			unsigned long long *o_number_of_triangles,
			TCFLReduceValue *o_cfl_domain_size_div_max_wave_speed
	)
	{
		typedef CGenericTreeNode<CPartition_TreeNode_> CGenericTreeNode_;

		/**
		 * if this variable is set to true, there are still some hanging nodes to be deleted.
		 */
		bool remaining_hanging_node;

		/****************************************************
		 * FIRST PASS
		 ****************************************************/
		/**
		 * the first pass is executed to mark the corresponding triangles for begin split or coarsened.
		 * also edge refinement/coarsen information is pushed/pulled to the stack.
		 *
		 * finally, the data for the adjacent partitions is stored to the stacks and need to be exchanged.
		 */


		/**
		 * the first pass checks, whether a triangle should be refined or coarsened.
		 * therefore the element data is necessary. After this step, the elementdoTraversal_SimulationHandler_SubClass_Method_Reduce
		 * data has not to be touched in the middle passes.
		 */
		// first pass: setting all edges to type 'new' creates data to exchange with neighbors

		if (i_avoid_scan_based_traversals)
		{
			i_node->traverse_GenericTreeNode_Reduce_Parallel
			(
				[=](CGenericTreeNode_ *node, bool *o_reduceValue)
				{
					*o_reduceValue = (node->cPartition_TreeNode->cCluster->*(p_adaptiveTraversator_SubClass)).actionFirstTraversal(node->cPartition_TreeNode->cStacks);
				},
				&CReduceOperators::OR<bool>,
				&remaining_hanging_node
			);
		}
		else
		{
			i_node->traverse_GenericTreeNode_Reduce_Parallel_Scan
			(
				[=](CGenericTreeNode_ *node, bool *o_reduceValue)
				{
					*o_reduceValue = (node->cPartition_TreeNode->cCluster->*(p_adaptiveTraversator_SubClass)).actionFirstTraversal(node->cPartition_TreeNode->cStacks);
				},
				&CReduceOperators::OR<bool>,
				&remaining_hanging_node
			);
		}

		/****************************************************
		 * MIDDLE PASSES
		 ****************************************************/
		/**
		 * this is the loop over all partitions to avoid all hanging nodes
		 *
		 * there's already some data prepared on the stack from the first pass.
		 */
		int c = 0;
		while (remaining_hanging_node)
		{
			if (c > 10)
				std::cout << "Warning: more than " << c << " adaptive middle traversals" << std::endl;

			/*
			 * WARNING: moving pullEdgeCommData() to the next parallel reduce  traversal does not work!!!
			 *
			 * Since both middle traversals (forward/backward) write to the edgeComm but
			 * _ALSO_ the exchangeEdgeComm stacks, pulling the edge comm data within the next traversal is not allowed!
			 */

			// first pass: setting all edges to type 'new' creates data to exchange with neighbors


			if (i_avoid_scan_based_traversals)
			{
				i_node->traverse_GenericTreeNode_Parallel(
						[=](CGenericTreeNode_ *node)
						{
							(node->cPartition_TreeNode->cCluster->*p_simulationExchangeEdgeComm_SubClass).pullEdgeCommData();
						}
				);
			}
			else
			{
				i_node->traverse_GenericTreeNode_Parallel_Scan(
						[=](CGenericTreeNode_ *node)
						{
							(node->cPartition_TreeNode->cCluster->*p_simulationExchangeEdgeComm_SubClass).pullEdgeCommData();
						}
				);
			}


			/*
			 * The second traversal actually exists of 2 passes:
			 *
			 * 1) The first BACKWARD TRAVERSAL reads data from the adjacent partitions.
			 *    all partition borders are set t type 'old'
			 *
			 * 2) The second FORWARD TRAVERSAL spreads data to adjacent partitions.
			 *    if callback_action_Adaptive_Tsunami_SecondPass returns true, there are
			 *    still refinements to do and we rerun this loop.
			 *    then all partition borders are set to type 'new'
			 */

			if (i_avoid_scan_based_traversals)
			{
				i_node->traverse_GenericTreeNode_Reduce_Parallel(
						[=](CGenericTreeNode_ *node, bool *o_reduceValue)
						{
							*o_reduceValue = (node->cPartition_TreeNode->cCluster->*(p_adaptiveTraversator_SubClass)).actionMiddleTraversals_Parallel(node->cPartition_TreeNode->cStacks);
						},
					&(CReduceOperators::OR<bool>),
					&remaining_hanging_node
				);
			}
			else
			{
				i_node->traverse_GenericTreeNode_Reduce_Parallel_Scan(
						[=](CGenericTreeNode_ *node, bool *o_reduceValue)
						{
							*o_reduceValue = (node->cPartition_TreeNode->cCluster->*(p_adaptiveTraversator_SubClass)).actionMiddleTraversals_Parallel(node->cPartition_TreeNode->cStacks);
						},
					&(CReduceOperators::OR<bool>),
					&remaining_hanging_node
				);
			}

			c++;
		}

		/****************************************************
		 * LAST PASS
		 ****************************************************/
		/**
		 * after we have the states to create a regular grid, the state-refinement information is stored on the stack.
		 *
		 * !!! this information is used later on to change the information about the adjacent partitions !!!
		 *
		 * this traversal do the actual refinement
		 */
		class CReduceDataClass
		{
		public:
			unsigned long long number_of_triangles;
			TCFLReduceValue cfl_domain_size_div_max_wave_speed;

			static void reduceOperator(
					const CReduceDataClass &i_reduceClass1,
					const CReduceDataClass &i_reduceClass2,
					CReduceDataClass *o_reduceClass
			)
			{
				o_reduceClass->number_of_triangles = i_reduceClass1.number_of_triangles + i_reduceClass2.number_of_triangles;
				o_reduceClass->cfl_domain_size_div_max_wave_speed = CMath::min<TCFLReduceValue>(i_reduceClass1.cfl_domain_size_div_max_wave_speed, i_reduceClass2.cfl_domain_size_div_max_wave_speed);
			}

		} reduceDataClass;


		auto fun_midnodes_reduce =
			[=](CGenericTreeNode_ *i_cGenericTreeNode, CReduceDataClass *o_reduceDataClass)
				{
					i_cGenericTreeNode->workload_in_subtree = o_reduceDataClass->number_of_triangles;
				};


		if (i_avoid_scan_based_traversals || !CONFIG_FORCE_SCAN_TRAVERSAL)
		{
			auto fun_leaves_reduce =
				[=](	CGenericTreeNode_ *i_cGenericTreeNode,
						CReduceDataClass *o_reduceDataClass
				)
				{
					CPartition_TreeNode_ *i_node = i_cGenericTreeNode->cPartition_TreeNode;

					CSimulation_Parallel_Cluster *worker = i_node->cCluster;
					CAdaptiveTraversatorClass &adaptiveSubClass = (worker->*(p_adaptiveTraversator_SubClass));
					CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> &simulationEdgeCommSubClass = (worker->*(p_simulationExchangeEdgeComm_SubClass));

					/**
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent partition, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							i_node->cStacks,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					unsigned long long number_of_triangles = i_node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&i_node->cStacks->adaptive_comm_edge_stacks.left,
							&i_node->cStacks->adaptive_comm_edge_stacks.right,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation,
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					i_node->cStacks->adaptive_comm_edge_stacks.clear();

					TCFLReduceValue reduceValue;
					adaptiveSubClass.storeReduceValue(&reduceValue);

					if (reduceValue == CMath::numeric_inf<TCFLReduceValue>())
					{
						/*
						 * if the CFL value was not updated, use the CFL value from edge comm.
						 */
						reduceValue = i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm;
					}
					else
					{
						/*
						 * if the CFL value was updated, find the minimum.
						 */
						reduceValue =
							CMath::min(
									i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm,
									reduceValue
								);
					}

					i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_adaptivity = reduceValue;
					i_cGenericTreeNode->workload_in_subtree = number_of_triangles;

					o_reduceDataClass->number_of_triangles = number_of_triangles;
					o_reduceDataClass->cfl_domain_size_div_max_wave_speed = reduceValue;
			};

			i_node->traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
					fun_leaves_reduce,
				fun_midnodes_reduce,
				&CReduceDataClass::reduceOperator,
				&reduceDataClass
			);
		}
		else
		{
			auto fun_leaves =
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *i_node = i_cGenericTreeNode->cPartition_TreeNode;

					CSimulation_Parallel_Cluster *worker = i_node->cCluster;
					CAdaptiveTraversatorClass &adaptiveSubClass = (worker->*(p_adaptiveTraversator_SubClass));
					CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> &simulationEdgeCommSubClass = (worker->*(p_simulationExchangeEdgeComm_SubClass));

					/**
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent partition, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							i_node->cStacks,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					unsigned long long number_of_triangles = i_node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&i_node->cStacks->adaptive_comm_edge_stacks.left,
							&i_node->cStacks->adaptive_comm_edge_stacks.right,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation,
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					i_node->cStacks->adaptive_comm_edge_stacks.clear();

					TCFLReduceValue reduceValue;
					adaptiveSubClass.storeReduceValue(&reduceValue);

					if (reduceValue == CMath::numeric_inf<TCFLReduceValue>())
					{
						/*
						 * if the CFL value was not updated, use the CFL value from edge comm.
						 */
						reduceValue = i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm;
					}
					else
					{
						/*
						 * if the CFL value was updated, find the minimum.
						 */
						reduceValue =
							CMath::min(
									i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm,
									reduceValue
								);
					}

					i_node->cCluster->cfl_domain_size_div_max_wave_speed_after_adaptivity = reduceValue;
					i_cGenericTreeNode->workload_in_subtree = number_of_triangles;
			};

			i_node->traverse_GenericTreeNode_Parallel_Scan(
				fun_leaves
			);

			i_node->traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
				[](	CGenericTreeNode_ *i_cGenericTreeNode,
					CReduceDataClass *o_reduceDataClass
				)
				{
					o_reduceDataClass->number_of_triangles = i_cGenericTreeNode->workload_in_subtree;
					o_reduceDataClass->cfl_domain_size_div_max_wave_speed = i_cGenericTreeNode->cPartition_TreeNode->cCluster->cfl_domain_size_div_max_wave_speed_after_adaptivity;
				},
				fun_midnodes_reduce,
				&CReduceDataClass::reduceOperator,
				&reduceDataClass
			);
		}

		*o_number_of_triangles = reduceDataClass.number_of_triangles;
		*o_cfl_domain_size_div_max_wave_speed = reduceDataClass.cfl_domain_size_div_max_wave_speed;
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
