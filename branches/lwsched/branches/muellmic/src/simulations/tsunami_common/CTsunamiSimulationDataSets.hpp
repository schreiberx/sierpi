/*
 * CTsunamiSimulationDataSets.hpp
 *
 *  Created on: Mar 16, 2012
 *      Author: schreibm
 */

#ifndef CTSUNAMISIMULATION_DATASETS_HPP_
#define CTSUNAMISIMULATION_DATASETS_HPP_

#include "CTsunamiSimulationParameters.hpp"
#include "libmath/CMath.hpp"

#if USE_ASAGI
#include "asagi/CAsagi.hpp"
#endif


#include "benchmarks/CSingleWaveOnSingleBeach.hpp"



class CTsunamiSimulationDataSets
{
public:
	typedef CTsunamiSimulationTypes::TVertexScalar T;


	CSingleWaveOnSingleBeach<T> cSingleWaveOnSingleBeach;

	/*
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use asagi to determine water surface height
		 */
	#if USE_ASAGI
		SIMULATION_WATER_HEIGHT_ASAGI = -1,
	#endif

		/*
		 * set simulation water surface height to zero
		 */
		SIMULATION_WATER_DEFAULT_HEIGHT = 0,

		/*
		 * setup water surface height with cylinder parameters (simulation_dataset_cylinder*)
		 */
		SIMULATION_WATER_HEIGHT_CYLINDER = 1,
		SIMULATION_WATER_HEIGHT_LINEAR_SLOPE = 2,
		SIMULATION_WATER_HEIGHT_SQUARE = 3,
		SIMULATION_WATER_HEIGHT_SQUARE_DIST = 4,
		SIMULATION_WATER_HEIGHT_PARABOLA = 5,
		SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY = 6,
		SIMULATION_WATER_HEIGHT_CYLINDER_OUTER_CYLINDER_MINUS_INF = 7,
		SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 8
	};


	/**
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use asagi to determine water surface height
		 */
#if USE_ASAGI
		SIMULATION_TERRAIN_HEIGHT_ASAGI = -1,
#endif

		SIMULATION_TERRAIN_HEIGHT_BASE = 0,
		SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE = 1,
		SIMULATION_TERRAIN_HEIGHT_PARABOLA = 2,
		SIMULATION_TERRAIN_HEIGHT_FANCY = 3,
		SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 8
	};


	CTsunamiSimulationParameters &cTsunamiSimulationParameters;

public:
	CTsunamiSimulationDataSets(
			CTsunamiSimulationParameters &i_cTsunamiSimulationParameters
		)	:
			cTsunamiSimulationParameters(i_cTsunamiSimulationParameters)
	{

#if USE_ASAGI
		cAsagi = new CAsagi(
//				"/home/schreibm/workspace/tsunami/trunk/benchmarks/field/tohoku/input/example_etopo1_ucsb3_1850m/tohoku_1850m_bath.nc",
				"/home/schreibm/workspace/tsunami/trunk/geo_information/output/tohoku_gebco_ucsb3_500m_hawaii_bath.nc",
				"/home/schreibm/workspace/tsunami/trunk/geo_information/output/tohoku_gebco_ucsb3_500m_hawaii_displ.nc"
		);

		i_cTsunamiSimulationParameters.simulation_parameter_domain_length = cAsagi->bathymetry_size_x;
#endif
	}


public:
	~CTsunamiSimulationDataSets()
	{
#if USE_ASAGI
		delete cAsagi;
#endif
	}


public:
	/**
	 * terrain dimensions
	 */
	inline void getTerrainOriginAndSize(
			T *o_origin_x,	///< origins x-coordinate
			T *o_origin_y,	///< origins y-coordinate
			T *o_size_x,	///< size of terrain in x-dimension
			T *o_size_y		///< size of terrain in y-dimension
	)
	{
#if USE_ASAGI
		if (cTsunamiSimulationParameters.simulation_terrain_scene_id == -1)
		{
			*o_origin_x = cAsagi->bathymetry_min_x;
			*o_origin_y = cAsagi->bathymetry_min_y;

			*o_size_x = cAsagi->bathymetry_size_x;
			*o_size_y = cAsagi->bathymetry_size_y;
			return;
		}
#endif

		*o_origin_x = -1.0;
		*o_origin_y = -1.0;

		*o_size_x = 2.0;
		*o_size_y = 2.0;
	}

public:
	/**
	 * terrain setup
	 */
	inline T getTerrainHeightByPosition(
			T i_x,
			T i_y,
			T i_size
	)
	{
		switch(cTsunamiSimulationParameters.simulation_terrain_scene_id)
		{
#if USE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
		{
#if 0
			T a = getAsagiElementBathymetryMethod(i_x, i_y, i_size) + getAsagiElementDisplacementMethod(i_x, i_y, i_size);
			if (a > -100)
				a = -100;
			return a;
#endif
			return getAsagiElementBathymetryMethod(i_x, i_y, i_size) + getAsagiElementDisplacementMethod(i_x, i_y, i_size);
		}
#endif

		case SIMULATION_TERRAIN_HEIGHT_BASE:
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;

		case SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE:
			return i_x*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;

		case SIMULATION_TERRAIN_HEIGHT_PARABOLA:
			return -(T)2.0*((T)1.0-CMath::max(i_x*i_x, i_y*i_y))*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance +
					cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance*(T)0.3;

		case SIMULATION_TERRAIN_HEIGHT_FANCY:
		{
			T d = (T)-1.0;

			d = CMath::max(d, i_x*i_x*(T)0.9-(T)0.8);

			if (i_x*i_x + i_y*i_y < (T)0.05)
			{
				d = (T)0.15;
			}

			T nx = i_x-(T)1.0;
			d = CMath::max(d, nx*nx*nx*(T)0.3+nx*nx*(T)1.0+nx+(T)0.29);

			d -= 0.1;
			return d*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance*0.01;
		}

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
		{
			return cSingleWaveOnSingleBeach.getBathymetry(i_x);
		}

		default:
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;
		}
	}


public:
	/**
	 * return whether the maximum function should be used to update the water surface height
	 */
	inline bool getUseMaximumFunctionForSurfaceHeightSetup()
	{
		switch(cTsunamiSimulationParameters.simulation_water_surface_scene_id)
		{
		case SIMULATION_WATER_HEIGHT_CYLINDER:
			return true;

		case SIMULATION_WATER_HEIGHT_CYLINDER_OUTER_CYLINDER_MINUS_INF:
			return true;

		case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
			return true;

		default:
			return false;
		}
	}


public:
	/**
	 * return surface relative to horizon height by position
	 */
	inline CTsunamiSimulationTypes::TVertexScalar getWaterSurfaceHeightByPosition(
					CTsunamiSimulationTypes::TVertexScalar i_x,
					CTsunamiSimulationTypes::TVertexScalar i_y,
					CTsunamiSimulationTypes::TVertexScalar i_size
				)
	{
		switch(cTsunamiSimulationParameters.simulation_water_surface_scene_id)
		{

#if USE_ASAGI
		case SIMULATION_WATER_HEIGHT_ASAGI:	// setup with asagi
			return getAsagiElementDisplacementMethod(i_x, i_y, i_size);
#endif

		case SIMULATION_WATER_DEFAULT_HEIGHT:
		{
			return cTsunamiSimulationParameters.simulation_dataset_water_surface_default_displacement;
		}

		case SIMULATION_WATER_HEIGHT_CYLINDER:
		{
			CTsunamiSimulationTypes::TVertexScalar rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_cylinder_posx);
			CTsunamiSimulationTypes::TVertexScalar ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_cylinder_posy);

			if (rx*rx + ry*ry < cTsunamiSimulationParameters.simulation_dataset_cylinder_radius*cTsunamiSimulationParameters.simulation_dataset_cylinder_radius)
				return cTsunamiSimulationParameters.simulation_dataset_cylinder_height;
			else
				return 0;
		}

		case SIMULATION_WATER_HEIGHT_LINEAR_SLOPE:
		{
			return i_x;
		}

		case SIMULATION_WATER_HEIGHT_SQUARE:
		{
			if (i_x > -1.0+1.0/CMath::pow(2.0, 2.0)+0.0001)
				return 0;
			return cTsunamiSimulationParameters.simulation_dataset_cylinder_height;
		}

		case SIMULATION_WATER_HEIGHT_SQUARE_DIST:
		{
			if (i_x < -1.0+1.0/CMath::pow(2.0, 4.0)+0.0001 || i_x > -1.0+1.0/CMath::pow(2.0, 2.0)+0.0001)
				return 0;
			return cTsunamiSimulationParameters.simulation_dataset_cylinder_height;
		}

		case SIMULATION_WATER_HEIGHT_PARABOLA:
		{
			return 1.0-CMath::max(i_x*i_x, i_y*i_y);
		}


		/*
		 * setup a cylinder relative to bathymetry if bathymetry is higher than 0
		 */
		case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
		{
			CTsunamiSimulationTypes::TVertexScalar rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_cylinder_posx);
			CTsunamiSimulationTypes::TVertexScalar ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_cylinder_posy);

			if (rx*rx + ry*ry > cTsunamiSimulationParameters.simulation_dataset_cylinder_radius*cTsunamiSimulationParameters.simulation_dataset_cylinder_radius)
				return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method

			return cTsunamiSimulationParameters.simulation_dataset_cylinder_height;
		}

		case SIMULATION_WATER_HEIGHT_CYLINDER_OUTER_CYLINDER_MINUS_INF:
		{
			CTsunamiSimulationTypes::TVertexScalar rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_cylinder_posx);
			CTsunamiSimulationTypes::TVertexScalar ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_cylinder_posy);

			if (rx*rx + ry*ry < cTsunamiSimulationParameters.simulation_dataset_cylinder_radius*cTsunamiSimulationParameters.simulation_dataset_cylinder_radius)
				return cTsunamiSimulationParameters.simulation_dataset_cylinder_height;
			else
				return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method
		}

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
		{
			return cSingleWaveOnSingleBeach.getWaterHeight(i_x);
		}

		default:
			return 0;
		}
	}

};



#endif /* CTSUNAMISIMULATIONDATASETS_HPP_ */
