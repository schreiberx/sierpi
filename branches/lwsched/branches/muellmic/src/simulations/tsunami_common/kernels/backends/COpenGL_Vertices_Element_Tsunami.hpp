/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COPENGL_VERTICES_ELEMENT_TSUNAMI_HPP_
#define COPENGL_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "COpenGL_Vertices_Element_Root_Tsunami.hpp"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_ElementData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{


template <
	typename p_CSimulationTypes,
	int p_visualizationType		///< type of visualization
									///< 0: simple
									///< 1: surface triangles at edge midpoints
									///< 2: aligned surface triangles
									///< 3: simple surface triangles
									///< 4: aligned bathymetry triangles
>
class COpenGL_Vertices_Element_Tsunami
{
public:
	typedef typename p_CSimulationTypes::TSimulationElementData		TElementData;
	typedef typename p_CSimulationTypes::TVisualizationVertexScalar TVertexScalar;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexCoords_ElementData<COpenGL_Vertices_Element_Tsunami<p_CSimulationTypes, p_visualizationType>, CTsunamiSimulationStacks > TRAV;

private:
	GLfloat *vertex_attrib_buffer;
	GLfloat *current_vertex_attrib;

	GLfloat *vertex_attrib_buffer_end;
	size_t max_vertices;

	TVertexScalar scale_min;
	TVertexScalar scale_factor;

	COpenGL_Vertices_Element_Root_Tsunami<TVertexScalar> *cOpenGL_Vertices_Element_Root_Tsunami;

public:

	inline COpenGL_Vertices_Element_Tsunami()	:
			max_vertices(COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT)
	{
		// allocate space for VERTICES AND NORMALS!!!
		vertex_attrib_buffer = new GLfloat[3*3*max_vertices*2];
		vertex_attrib_buffer_end = vertex_attrib_buffer+(3*max_vertices*2);
	}

	virtual inline ~COpenGL_Vertices_Element_Tsunami()
	{
		delete[] vertex_attrib_buffer;
	}


	inline void renderOpenGLVertexArray(size_t p_vertices_count)
	{
		assert(cOpenGL_Vertices_Element_Root_Tsunami != nullptr);

		cOpenGL_Vertices_Element_Root_Tsunami->renderOpenGLVertexArray(
				vertex_attrib_buffer,
				p_vertices_count
			);
	}


	inline TTsunamiVertexScalar fixHeight(TTsunamiVertexScalar h)
	{
		return (((h)-scale_min)*scale_factor);
	}


	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TElementData *i_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vx1, vy1, vx2, vy2, vx3, vy3);
#endif
		if (p_visualizationType == 0)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

			if (i_elementData->dofs.h <= 0)
				return;

			TVertexScalar y = fixHeight(i_elementData->dofs.h + i_elementData->dofs.b);

#else
			TVertexScalar y = fixHeight((1.0/3.0)*(i_elementData->hyp_edge.h + i_elementData->hyp_edge.b + i_elementData->left_edge.h + i_elementData->left_edge.b + i_elementData->right_edge.h + i_elementData->right_edge.b));
#endif
			current_vertex_attrib[0*3+0] = vx1;
			current_vertex_attrib[0*3+1] = y;
			current_vertex_attrib[0*3+2] = -vy1;

			current_vertex_attrib[1*3+0] = 0;
			current_vertex_attrib[1*3+1] = 1;
			current_vertex_attrib[1*3+2] = 0;

			current_vertex_attrib[2*3+0] = vx2;
			current_vertex_attrib[2*3+1] = y;
			current_vertex_attrib[2*3+2] = -vy2;

			current_vertex_attrib[3*3+0] = 0;
			current_vertex_attrib[3*3+1] = 1;
			current_vertex_attrib[3*3+2] = 0;

			current_vertex_attrib[4*3+0] = vx3;
			current_vertex_attrib[4*3+1] = y;
			current_vertex_attrib[4*3+2] = -vy3;

			current_vertex_attrib[5*3+0] = 0;
			current_vertex_attrib[5*3+1] = 1;
			current_vertex_attrib[5*3+2] = 0;
		}

		if (p_visualizationType == 1)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS != 0
			TVertexScalar hyp_x = (vx1+vx2)*(TTsunamiDataScalar)0.5;
			TVertexScalar hyp_y = (vy1+vy2)*(TTsunamiDataScalar)0.5;

			TVertexScalar right_edge_x = (vx2+vx3)*(TTsunamiDataScalar)0.5;
			TVertexScalar right_edge_y = (vy2+vy3)*(TTsunamiDataScalar)0.5;

			TVertexScalar left_edge_x = (vx3+vx1)*(TTsunamiDataScalar)0.5;
			TVertexScalar left_edge_y = (vy3+vy1)*(TTsunamiDataScalar)0.5;

			current_vertex_attrib[0*3+0] = hyp_x;
			current_vertex_attrib[0*3+1] = fixHeight(i_elementData->hyp_edge.h+i_elementData->hyp_edge.b);
			current_vertex_attrib[0*3+2] = -hyp_y;

			current_vertex_attrib[1*3+0] = 0;
			current_vertex_attrib[1*3+1] = 1;
			current_vertex_attrib[1*3+2] = 0;

			current_vertex_attrib[2*3+0] = right_edge_x;
			current_vertex_attrib[2*3+1] = fixHeight(i_elementData->right_edge.h+i_elementData->right_edge.b);
			current_vertex_attrib[2*3+2] = -right_edge_y;

			current_vertex_attrib[3*3+0] = 0;
			current_vertex_attrib[3*3+1] = 1;
			current_vertex_attrib[3*3+2] = 0;

			current_vertex_attrib[4*3+0] = left_edge_x;
			current_vertex_attrib[4*3+1] = fixHeight(i_elementData->left_edge.h+i_elementData->left_edge.b);
			current_vertex_attrib[4*3+2] = -left_edge_y;

			current_vertex_attrib[5*3+0] = 0;
			current_vertex_attrib[5*3+1] = 1;
			current_vertex_attrib[5*3+2] = 0;
#endif
		}

		/*
		 * render aligned water surface
		 */
		if (p_visualizationType == 2)
		{
			TVertexScalar hyp_edge_x = (vx1+vx2)*(TTsunamiDataScalar)0.5;
			TVertexScalar hyp_edge_y = (vy1+vy2)*(TTsunamiDataScalar)0.5;

			TVertexScalar right_edge_x = (vx2+vx3)*(TTsunamiDataScalar)0.5;
			TVertexScalar right_edge_y = (vy2+vy3)*(TTsunamiDataScalar)0.5;

			TVertexScalar left_edge_x = (vx3+vx1)*(TTsunamiDataScalar)0.5;
			TVertexScalar left_edge_y = (vy3+vy1)*(TTsunamiDataScalar)0.5;

			TVertexScalar hyp_dx = right_edge_x - left_edge_x;
			TVertexScalar hyp_dy = right_edge_y - left_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS!=0
			TVertexScalar hyp_dhb = i_elementData->right_edge.h+i_elementData->right_edge.b - (i_elementData->left_edge.h+i_elementData->left_edge.b);
#endif

			TVertexScalar left_dx = right_edge_x - hyp_edge_x;
			TVertexScalar left_dy = right_edge_y - hyp_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS!=0
			TVertexScalar left_dhb = i_elementData->right_edge.h+i_elementData->right_edge.b - (i_elementData->hyp_edge.h+i_elementData->hyp_edge.b);
#endif

			current_vertex_attrib[0*3+0] = hyp_edge_x-hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[0*3+1] = fixHeight(i_elementData->dofs.h+i_elementData->dofs.b);
#else
			current_vertex_attrib[0*3+1] = fixHeight((i_elementData->hyp_edge.h+i_elementData->hyp_edge.b) - hyp_dhb);
#endif
			current_vertex_attrib[0*3+2] = -(hyp_edge_y-hyp_dy);

			current_vertex_attrib[2*3+0] = hyp_edge_x+hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[2*3+1] = fixHeight(i_elementData->dofs.h+i_elementData->dofs.b);
#else
			current_vertex_attrib[2*3+1] = fixHeight((i_elementData->hyp_edge.h+i_elementData->hyp_edge.b) + hyp_dhb);
#endif
			current_vertex_attrib[2*3+2] = -(hyp_edge_y+hyp_dy);

			current_vertex_attrib[4*3+0] = left_edge_x+left_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[4*3+1] = fixHeight(i_elementData->dofs.h+i_elementData->dofs.b);
#else
			current_vertex_attrib[4*3+1] = fixHeight((i_elementData->left_edge.h+i_elementData->left_edge.b)+left_dhb);
#endif

			current_vertex_attrib[4*3+2] = -(left_edge_y+left_dy);

			TVertexScalar bx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[2*3+0]);
			TVertexScalar by = (current_vertex_attrib[0*3+1]-current_vertex_attrib[2*3+1]);
			TVertexScalar bz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[2*3+2]);

			TVertexScalar cx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[4*3+0]);
			TVertexScalar cy = (current_vertex_attrib[0*3+1]-current_vertex_attrib[4*3+1]);
			TVertexScalar cz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[4*3+2]);

			TVertexScalar nx = by*cz-bz*cy;
			TVertexScalar ny = bz*cx-bx*cz;
			TVertexScalar nz = bx*cy-by*cx;

			TVertexScalar a = (TVertexScalar)1.0/CMath::sqrt<TVertexScalar>(nx*nx + ny*ny + nz*nz);

			nx *= a;
			ny *= a;
			nz *= a;

			current_vertex_attrib[1*3+0] = nx;
			current_vertex_attrib[1*3+1] = ny;
			current_vertex_attrib[1*3+2] = nz;

			current_vertex_attrib[3*3+0] = nx;
			current_vertex_attrib[3*3+1] = ny;
			current_vertex_attrib[3*3+2] = nz;

			current_vertex_attrib[5*3+0] = nx;
			current_vertex_attrib[5*3+1] = ny;
			current_vertex_attrib[5*3+2] = nz;
		}


		/*
		 * render simple bathymetry
		 */
		if (p_visualizationType == 3)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			TVertexScalar y = fixHeight(i_elementData->dofs.b);

#else
			TVertexScalar y = fixHeight((1.0/3.0)*(i_elementData->hyp_edge.b + i_elementData->left_edge.b + i_elementData->right_edge.b));
#endif
			current_vertex_attrib[0*3+0] = vx1;
			current_vertex_attrib[0*3+1] = y;
			current_vertex_attrib[0*3+2] = -vy1;

			current_vertex_attrib[1*3+0] = 0;
			current_vertex_attrib[1*3+1] = 1;
			current_vertex_attrib[1*3+2] = 0;

			current_vertex_attrib[2*3+0] = vx2;
			current_vertex_attrib[2*3+1] = y;
			current_vertex_attrib[2*3+2] = -vy2;

			current_vertex_attrib[3*3+0] = 0;
			current_vertex_attrib[3*3+1] = 1;
			current_vertex_attrib[3*3+2] = 0;

			current_vertex_attrib[4*3+0] = vx3;
			current_vertex_attrib[4*3+1] = y;
			current_vertex_attrib[4*3+2] = -vy3;

			current_vertex_attrib[5*3+0] = 0;
			current_vertex_attrib[5*3+1] = 1;
			current_vertex_attrib[5*3+2] = 0;
		}


		/*
		 * render aligned bathymetry
		 */
		if (p_visualizationType == 4)
		{
			TVertexScalar hyp_edge_x = (vx1+vx2)*(TTsunamiDataScalar)0.5;
			TVertexScalar hyp_edge_y = (vy1+vy2)*(TTsunamiDataScalar)0.5;

			TVertexScalar right_edge_x = (vx2+vx3)*(TTsunamiDataScalar)0.5;
			TVertexScalar right_edge_y = (vy2+vy3)*(TTsunamiDataScalar)0.5;

			TVertexScalar left_edge_x = (vx3+vx1)*(TTsunamiDataScalar)0.5;
			TVertexScalar left_edge_y = (vy3+vy1)*(TTsunamiDataScalar)0.5;

			TVertexScalar hyp_dx = right_edge_x - left_edge_x;
			TVertexScalar hyp_dy = right_edge_y - left_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
//			TVertexScalar hyp_dh = 0;
#else
			TVertexScalar hyp_dh = i_elementData->right_edge.h - i_elementData->left_edge.h;
#endif

			TVertexScalar left_dx = right_edge_x - hyp_edge_x;
			TVertexScalar left_dy = right_edge_y - hyp_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
	//		TVertexScalar left_dh = 0;
#else
			TVertexScalar left_dh = i_elementData->right_edge.h - i_elementData->hyp_edge.h;
#endif

			current_vertex_attrib[0*3+0] = hyp_edge_x-hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[0*3+1] = fixHeight(i_elementData->dofs.h);
#else
			current_vertex_attrib[0*3+1] = fixHeight(i_elementData->hyp_edge.h-hyp_dh);
#endif
			current_vertex_attrib[0*3+2] = -(hyp_edge_y-hyp_dy);

			current_vertex_attrib[2*3+0] = hyp_edge_x+hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[2*3+1] = fixHeight(i_elementData->dofs.h);
#else
			current_vertex_attrib[2*3+1] = fixHeight(i_elementData->hyp_edge.h+hyp_dh);
#endif
			current_vertex_attrib[2*3+2] = -(hyp_edge_y+hyp_dy);

			current_vertex_attrib[4*3+0] = left_edge_x+left_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[4*3+1] = fixHeight(i_elementData->dofs.h);
#else
			current_vertex_attrib[4*3+1] = fixHeight(i_elementData->left_edge.h+left_dh);
#endif

			current_vertex_attrib[4*3+2] = -(left_edge_y+left_dy);

			TVertexScalar bx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[2*3+0]);
			TVertexScalar by = (current_vertex_attrib[0*3+1]-current_vertex_attrib[2*3+1]);
			TVertexScalar bz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[2*3+2]);

			TVertexScalar cx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[4*3+0]);
			TVertexScalar cy = (current_vertex_attrib[0*3+1]-current_vertex_attrib[4*3+1]);
			TVertexScalar cz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[4*3+2]);

			TVertexScalar nx = by*cz-bz*cy;
			TVertexScalar ny = bz*cx-bx*cz;
			TVertexScalar nz = bx*cy-by*cx;

			TVertexScalar a = (TVertexScalar)1.0/CMath::sqrt<TVertexScalar>(nx*nx + ny*ny + nz*nz);

			nx *= a;
			ny *= a;
			nz *= a;

			current_vertex_attrib[1*3+0] = nx;
			current_vertex_attrib[1*3+1] = ny;
			current_vertex_attrib[1*3+2] = nz;

			current_vertex_attrib[3*3+0] = nx;
			current_vertex_attrib[3*3+1] = ny;
			current_vertex_attrib[3*3+2] = nz;

			current_vertex_attrib[5*3+0] = nx;
			current_vertex_attrib[5*3+1] = ny;
			current_vertex_attrib[5*3+2] = nz;
		}

		current_vertex_attrib += 3*3*2;


		assert(current_vertex_attrib <= vertex_attrib_buffer_end);
		if (current_vertex_attrib == vertex_attrib_buffer_end)
		{
			renderOpenGLVertexArray(max_vertices);
			current_vertex_attrib = vertex_attrib_buffer;
		}
	}

	inline void traversal_pre_hook()
	{
		current_vertex_attrib = vertex_attrib_buffer;
	}

	inline void traversal_post_hook()
	{
		if (current_vertex_attrib > vertex_attrib_buffer)
		{
			// involve stored vertices AND normals in primitive counting
			renderOpenGLVertexArray((size_t)(current_vertex_attrib-vertex_attrib_buffer)/(3*2));
			return;
		}
	}

	/**
	 * setup traversator based on parent triangle
	 */
	inline void setup_WithKernel(
			COpenGL_Vertices_Element_Tsunami &parent_kernel
	)
	{
		scale_min = parent_kernel.scale_min;
		scale_factor = parent_kernel.scale_factor;
		cOpenGL_Vertices_Element_Root_Tsunami = parent_kernel.cOpenGL_Vertices_Element_Root_Tsunami;
	}


	void setup(
			TVertexScalar i_scale_min,
			TVertexScalar i_scale_factor,
			COpenGL_Vertices_Element_Root_Tsunami<TVertexScalar> *p_cOpenGL_Vertices_Element_Root_Tsunami
	)
	{
		scale_min = i_scale_min;
		scale_factor = i_scale_factor;
		cOpenGL_Vertices_Element_Root_Tsunami = p_cOpenGL_Vertices_Element_Root_Tsunami;
	}
};


}
}

#endif
