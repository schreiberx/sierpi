/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_HPP_

#if SIMULATION_TSUNAMI_SERIAL_PINNING
	#include <pthread.h>
#endif


#include "../lib/CStopwatch.hpp"

#include "../tsunami_common/types/CTsunamiTypes.hpp"

#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Element.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

#if COMPILE_SIMULATION_WITH_GUI

	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Tsunami.hpp"

	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Tsunami.hpp"

	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Smooth_Element_Tsunami.hpp"

#endif


#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_ElementData_Normal_SelectByPoint.hpp"


#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"
//#include "libsierpi/kernels/specialized_tsunami/CSetup_Column.hpp"

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"
#include "lib/CStopwatch.hpp"

#include "../tsunami_common/CTsunamiSimulationDataSets.hpp"

#include "../tsunami_common/kernels/modifiers/CSetup_ElementData.hpp"


class CSimulationTsunami_Serial	: public CTsunamiSimulationParameters
{
public:
	CError error;

	/// traversator to setup the structure stack and element data stack by a given depth
	sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;

	/// to offer single-element modifications by coordinates, this traversal cares about it
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

	/// output element data at given point
	sierpi::kernels::CStringOutput_ElementData_Normal_SelectByPoint<CTsunamiSimulationStacks>::TRAV cStringOutput_ElementData_SelectByPoint;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	/// validation functions
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;
#endif

	/// exchange of edge communication data and timestep on fixed grid
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/// adaptive traversals to refine or coarsen grid cells without having hanging nodes
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData cTsunami_Adaptive;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Vertices_Element_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Element_Root_Tsunami;

	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV cOpenGL_Vertices_Element_Tsunami_simple;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,4>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned;

	sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV cOpenGL_Vertices_Smooth_Element_Tsunami;
	sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth;

	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;
	sierpi::kernels::COpenGL_Vertices_Wireframe_Tsunami<CTsunamiSimulationTypes>::TRAV cOpenGL_Vertices_Wireframe_Tsunami;
#endif

	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	sierpi::kernels::CSetup_TsunamiElementData_Validation::TRAV cSetup_TsunamiElementData_Validation;
#endif

public:
	CSimulationStacks<CTsunamiSimulationTypes> *cStacks;

	CTriangle_Factory cTriangleFactory;

	/*
	 * part of CFL condition returned by kernels
	 */
	TTsunamiDataScalar part_of_cfl_timestep_condition;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CTsunamiSimulationDataSets cTsunamiSimulationDataSets;

	/**
	 * when this value is not equal to -1, the thread is pinned to the given core
	 */
	int threading_pin_to_core_nr_after_one_timestep;

	/**
	 * verbosity level
	 */
	int verbosity_level;

public:
	/**
	 * constructor for serial tsunami simulation
	 */
	CSimulationTsunami_Serial(
			int i_verbosity_level
			)	:
		cStacks(nullptr),
		cTsunamiSimulationDataSets(*this),
		threading_pin_to_core_nr_after_one_timestep(-1),
		verbosity_level(i_verbosity_level)
	{
#if SIMULATION_TSUNAMI_SERIAL_PINNING
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			testPinToCore(-2);
#endif

		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = grid_initial_recursion_depth + grid_max_relative_recursion_depth;

		reset_Simulation();
	}


#if COMPILE_SIMULATION_WITH_GUI

	void render_surfaceDefault_simple(
			CGlProgram &cShaderBlinn,
			int i_mode
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}



	void render_surface_aligned(
			CGlProgram &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_aligned.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.cKernelClass.setup(visualization_terrain_default_displacement, visualization_terrain_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_aligned(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.cKernelClass.setup(visualization_terrain_default_displacement, visualization_terrain_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_smooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if 0
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.cKernelClass.setup(visualization_terrain_default_displacement, visualization_terrain_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.action_FirstPass(cStacks);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.action_SecondPass_Serial(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
#endif
	}

	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Element_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Wireframe_Tsunami.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Wireframe_Root_Tsunami);
		cOpenGL_Vertices_Wireframe_Tsunami.action(cStacks);
		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_surfaceSmooth(
			CGlProgram &cShaderBlinn
	)
	{
#if 0
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Smooth_Element_Tsunami.action_FirstPass(cStacks);
		cOpenGL_Vertices_Smooth_Element_Tsunami.action_SecondPass_Serial(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
#endif
	}


	void render_surfaceSmoothWithHeightColors(
			CGlProgram &cShaderBlinn
	)
	{
#if 0
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Element_Root_Tsunami);
		cOpenGL_Vertices_Smooth_Element_Tsunami.action_FirstPass(cStacks);
		cOpenGL_Vertices_Smooth_Element_Tsunami.action_SecondPass_Parallel(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
#endif
	}


#endif


	/**
	 * setup the stacks
	 */
	void setup_Stacks()
	{
		clean();

#if ADAPTIVE_CLUSTER_STACKS
		cStacks = new CSimulationStacks<CTsunamiSimulationTypes>(
					(1 << grid_initial_recursion_depth) + (ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CTsunamiElementData)),
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS		|
					CSimulationStacks_Enums::VERTEX_COMM_STACKS
				);
#else
		cStacks = new CSimulationStacks<CTsunamiSimulationTypes>(
				grid_initial_recursion_depth+grid_max_relative_recursion_depth,
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#endif

	}


	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 1.0;
		cTriangleFactory.vertices[0][1] = -1.0;
		cTriangleFactory.vertices[1][0] = -1.0;
		cTriangleFactory.vertices[1][1] = 1.0;
		cTriangleFactory.vertices[2][0] = -1.0;
		cTriangleFactory.vertices[2][1] = -1.0;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		cTriangleFactory.partitionTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
		cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
		cSetup_Column.setup_sfcMethods(cTriangleFactory);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

		cStringOutput_ElementData_SelectByPoint.setup_sfcMethods(cTriangleFactory);

#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Vertices_Element_Tsunami_simple.setup_sfcMethods(cTriangleFactory);
		cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(0, (TTsunamiDataScalar)0.2, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(cTriangleFactory);
		cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(0, (TTsunamiDataScalar)0.2, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.setup_sfcMethods(cTriangleFactory);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.setup_sfcMethods(cTriangleFactory);

		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.setup_sfcMethods(cTriangleFactory);

		cOpenGL_Vertices_Wireframe_Tsunami.setup_sfcMethods(cTriangleFactory);

		cOpenGL_Vertices_Smooth_Element_Tsunami.setup_sfcMethods(cTriangleFactory);
#endif

		cModify_OneElementValue_SelectByPoint.setup_sfcMethods(cTriangleFactory);
	}

	void setup()
	{
		reset_Simulation();
	}

	void reset_Simulation()
	{
		part_of_cfl_timestep_condition = 0;

		setup_Stacks();
		setup_TraversatorsAndKernels();
		p_setup_Parameters();
		p_setup_InitialTriangulation();

		reset_simulation_parameters();
	}


	void clean()
	{
		if (cStacks)
		{
			delete cStacks;
			cStacks = NULL;
		}
	}

	unsigned long long setup_ColumnAt2DPosition(
			float x,
			float y,
			float radius
	)
	{
		simulation_dataset_cylinder_posx = x;
		simulation_dataset_cylinder_posy = y;
		simulation_dataset_cylinder_radius = radius;

		return p_adaptive_traversal_setup_column();
	}
	unsigned long long setup_ColumnAt2DPosition()
	{
		return p_adaptive_traversal_setup_column();
	}


	unsigned long long p_adaptive_traversal_setup_column()
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
		cSetup_Column.setup_KernelClass(
				simulation_dataset_cylinder_posx,
				simulation_dataset_cylinder_posy,
				simulation_dataset_cylinder_radius,
				2,
				&cTsunamiSimulationDataSets
		);

#else
		cSetup_Column.setup_KernelClass(
				simulation_dataset_cylinder_posx,
				simulation_dataset_cylinder_posy,
				simulation_dataset_cylinder_radius,
				2,
				&cTsunamiSimulationDataSets
		);
#endif

		unsigned long long prev_number_of_triangles;
		do
		{
			prev_number_of_triangles = number_of_triangles;

			bool repeat_traversal = cSetup_Column.actionFirstTraversal(cStacks);

			while(repeat_traversal)
				repeat_traversal = cSetup_Column.actionMiddleTraversals_Serial(cStacks);

			cSetup_Column.actionLastTraversal_Serial(cStacks);

			number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
		} while (number_of_triangles != prev_number_of_triangles);

		number_of_triangles = prev_number_of_triangles;


		int backupSetupSurfaceMethod = simulation_water_surface_scene_id;
		simulation_water_surface_scene_id = CTsunamiSimulationDataSets::SIMULATION_WATER_HEIGHT_CYLINDER;


		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
		cSetup_ElementData.setup_sfcMethods(cTriangleFactory);
		cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets, false);
		cSetup_ElementData.action(cStacks);

		simulation_water_surface_scene_id = backupSetupSurfaceMethod;

		return number_of_triangles;
	}

private:
	void p_setup_Parameters()
	{
		cTsunami_EdgeComm.setParameters(simulation_parameter_timestep_size, simulation_parameter_domain_length, 9.81);
		cTsunami_EdgeComm.setBoundaryDirichlet(&dirichlet_boundary_edge_data);

		cTsunami_Adaptive.setup_KernelClass(
				simulation_parameter_domain_length,

				refine_threshold,
				coarsen_threshold,

				refine_slope_threshold,
				coarsen_slope_threshold,

				&cTsunamiSimulationDataSets
			);

		cTsunami_Adaptive.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);

		cSetup_Column.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);
	}


private:
	void p_setup_InitialTriangulation()
	{
		number_of_triangles = cSetup_Structure_ElementData.setup(cStacks, grid_initial_recursion_depth, &element_data_setup);
		number_of_initial_triangles_after_domain_triangulation = number_of_triangles;

		p_setup_initial_grid_data(false);
	}


private:
	void p_setup_initial_grid_data(
			bool i_initial_grid_setup
	)
	{
		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
		cSetup_ElementData.setup_sfcMethods(cTriangleFactory);
		cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets, true);
		cSetup_ElementData.action(cStacks);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.action(cStacks);
#endif
	}


private:
	void p_simulation_adaptive_traversals()
	{
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		TTsunamiDataScalar adaptCFL;
		cTsunami_Adaptive.storeReduceValue(&adaptCFL);

		if (adaptCFL != CMath::numeric_inf<TTsunamiDataScalar>())
		{
			part_of_cfl_timestep_condition = CMath::min(adaptCFL, part_of_cfl_timestep_condition);
		}

		number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
	}

public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
		unsigned long long prev_number_of_triangles;
		unsigned int prev_number_of_simulation_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		TTsunamiDataScalar coarsen_threshold_backup = coarsen_threshold;
		coarsen_threshold = -9999999;

//		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 10;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_triangles = number_of_triangles;
			prev_number_of_simulation_clusters = number_of_simulation_clusters;

			// setup grid data
			p_setup_initial_grid_data(true);

			// run single timestep
			p_simulation_edge_comm();

			// refine / coarsen grid
			p_simulation_adaptive_traversals();

			// split/join partitions
//			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_triangles << ", number_of_simulation_clusters: " << number_of_simulation_clusters << std::endl;

			if (	prev_number_of_triangles == number_of_triangles	&&
					prev_number_of_simulation_clusters == number_of_simulation_clusters
			)
				break;
		}

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		// update cluster parameters
		coarsen_threshold = coarsen_threshold_backup;
//		updateClusterParameters();
	}

private:
	/**
	 * timestep edge comm
	 */
	void p_simulation_edge_comm()
	{
		if (adaptive_timestep_size)
		{
			// adaptive timestep size

			simulation_parameter_timestep_size = part_of_cfl_timestep_condition*simulation_parameter_cfl;

			if (simulation_parameter_timestep_size < 0.000001)
				simulation_parameter_timestep_size = 0.000001;
		}

		cTsunami_EdgeComm.actionFirstPass(cStacks);
		cTsunami_EdgeComm.actionSecondPass_Serial(cStacks, simulation_parameter_timestep_size, &part_of_cfl_timestep_condition);
	}

public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		CTsunamiElementData element_data_modifier;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		element_data_modifier.dofs.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.dofs.qx = 0;
		element_data_modifier.dofs.qy = 0;
		element_data_modifier.dofs.b = -simulation_dataset_terrain_default_distance;

#else
		element_data_modifier.hyp_edge.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.hyp_edge.qx = 0;
		element_data_modifier.hyp_edge.qy = 0;
		element_data_modifier.hyp_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.left_edge.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.left_edge.qx = 0;
		element_data_modifier.left_edge.qy = 0;
		element_data_modifier.left_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.right_edge.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.right_edge.qx = 0;
		element_data_modifier.right_edge.qy = 0;
		element_data_modifier.right_edge.b = -simulation_dataset_terrain_default_distance;
#endif

		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_length, refine_threshold, -1, refine_slope_threshold, -999, &cTsunamiSimulationDataSets);
		p_simulation_adaptive_traversals();
		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_length, refine_threshold, coarsen_threshold, refine_slope_threshold, coarsen_slope_threshold, &cTsunamiSimulationDataSets);
	}


#if SIMULATION_TSUNAMI_SERIAL_PINNING
public:
	inline void testPinToCore(int pinning_core)
	{
		if (pinning_core == -1)
			return;


		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);

		// pin to first core for initialization to assure placement of allocated data
		if (pinning_core == -2)
			CPU_SET(0, &cpu_set);
		else
			CPU_SET(pinning_core, &cpu_set);

		std::cout << "Pinning application to core " << pinning_core << std::endl;

#if ADAPTIVE_CLUSTER_STACKS
		std::cout << std::endl;
		std::cout << "WARNING: Adaptive cluster stacks activated!!!" << std::endl;
		std::cout << std::endl;
#endif

		int err;
		err = pthread_setaffinity_np(
				pthread_self(),
				sizeof(cpu_set_t),
				&cpu_set
			);


		if (err != 0)
		{
			std::cout << err << std::endl;
			perror("pthread_setaffinity_np");
			assert(false);
			exit(-1);
		}

		if (pinning_core >= 0)
			threading_pin_to_core_nr_after_one_timestep = -1;
	}
#endif

public:
	void runSingleTimestep(bool dummy)
	{
		p_simulation_edge_comm();
		p_simulation_adaptive_traversals();

		timestep_nr++;
		timestamp_for_timestep += simulation_parameter_timestep_size;

#if SIMULATION_TSUNAMI_SERIAL_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime,
			bool dummy
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		timestep_nr++;
		timestamp_for_timestep += simulation_parameter_timestep_size;

#if SIMULATION_TSUNAMI_SERIAL_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/
	double getDataSample(
			double i_sample_pos_x,
			double i_sample_pos_y,
			const char *i_sample_information
		)
	{
		return 0;
	}

public:
	void debugOutput(CVector<2,float> planePosition)
	{
		cStringOutput_ElementData_SelectByPoint.cKernelClass.setup(planePosition[0], planePosition[1]);
		cStringOutput_ElementData_SelectByPoint.action(cStacks);
	}


	void writeTrianglesToVTKFile(
			const char *i_filename,		///< filename to write vtk files to
			bool i_output_binary = true	///< ascii format or binary
	)
	{
		/*
		 * little endian test -> vtk binary files expect data to be written in big-endian format
		 */
		int tmp1 = 1;
		unsigned char *tmp2 = (unsigned char *) &tmp1;
		bool swapLittleToBigEndian = (*tmp2 != 0);

		/**
		 * SURFACE + BATHYMETRY + MOMENTUM
		 */
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(i_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << grid_initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_clusters << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;
		vtkfile << "ASCII" << std::endl;
		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_triangles*3) << " float" << std::endl;

		TTsunamiVertexScalar origin_x = 0;
		TTsunamiVertexScalar origin_y = 0;
		TTsunamiVertexScalar size_x = 0;
		TTsunamiVertexScalar size_y = 0;

		cTsunamiSimulationDataSets.getTerrainOriginAndSize(
				&origin_x,
				&origin_y,
				&size_x,
				&size_y
			);

		/*
		 * map to [-1;1]^2
		 */
		size_x *= 0.5;
		origin_x += size_x;
		size_y *= 0.5;
		origin_y += size_y;


		// TRAVERSAL
		// We instantiate it right here to avoid any overhead due to split/join operations
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 0>::TRAV cOutputVTK_Vertices_Element_Tsunami;
		cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami.action(cStacks);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "POLYGONS " << number_of_triangles << " " << (number_of_triangles*4) << std::endl;
		for (unsigned long long i = 0; i < number_of_triangles; i++)
			vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

		// output height field
		vtkfile << std::endl;
		vtkfile << "CELL_DATA " << number_of_triangles << std::endl;
		vtkfile << "SCALARS h FLOAT" << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 1>::TRAV cOutputVTK_Vertices_Element_Tsunami1;
		cOutputVTK_Vertices_Element_Tsunami1.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami1.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami1.action(cStacks);

		vtkfile << "SCALARS hu FLOAT" << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 2>::TRAV cOutputVTK_Vertices_Element_Tsunami2;
		cOutputVTK_Vertices_Element_Tsunami2.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami2.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami2.action(cStacks);


		vtkfile << "SCALARS hv FLOAT" << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 3>::TRAV cOutputVTK_Vertices_Element_Tsunami3;
		cOutputVTK_Vertices_Element_Tsunami3.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami3.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami3.action(cStacks);

		vtkfile << "SCALARS b FLOAT" << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 4>::TRAV cOutputVTK_Vertices_Element_Tsunami4;
		cOutputVTK_Vertices_Element_Tsunami4.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami4.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami4.action(cStacks);

		vtkfile << "SCALARS cfl_value FLOAT" << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 5>::TRAV cOutputVTK_Vertices_Element_Tsunami5;
		cOutputVTK_Vertices_Element_Tsunami5.setup_sfcMethods(cTriangleFactory);
		cOutputVTK_Vertices_Element_Tsunami5.cKernelClass.setup(
				&vtkfile,
				origin_x,
				origin_y,
				size_x,
				size_y,
				true,
				swapLittleToBigEndian
			);
		cOutputVTK_Vertices_Element_Tsunami5.action(cStacks);

		vtkfile.close();
	}

	void keydown(int key)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep(true);
				break;

			case 'c':
				setup_ColumnAt2DPosition(-0.5, 0.4, 0.2);
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset_Simulation();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

		}
	}

	virtual ~CSimulationTsunami_Serial()
	{
		clean();
	}
};

#endif /* CTSUNAMI_HPP_ */
