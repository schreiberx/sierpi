/*
 * CMainThreadingIOMP.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include "libmath/CMath.hpp"


#include <OR.hpp>
#include <AND.hpp>
#include <Claim.hpp>
#include <PEQuantity.hpp>
#include <ScalabilityHint.hpp>


#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
public:
	int max_cores;
	Claim claim;

	CMainThreading()
	{
		max_cores = 512;
	}

	void threading_setup()
	{
		int n = getNumberOfThreadsToUse();

		if (n == -1)
		{
			n = omp_get_max_threads();
			setValueNumberOfThreadsToUse(n);
		}
		else
		{
			omp_set_num_threads(n);
		}

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): " << (int)omp_get_max_threads() << std::endl;

		threading_updateResourceUtilization();
	}


	/**
	 * update the ressource utilization
	 */
	void threading_updateResourceUtilization()
	{
		claim.invade(PEQuantity(1, max_cores));
	}


	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			threading_updateResourceUtilization();
			continue_simulation = threading_simulationLoopIteration();
		} while(continue_simulation);
	}


	/**
	 * FOR GUI ONLY
	 */
	bool threading_simulationLoopIteration()
	{
		return simulationLoopIteration();
	}


	void threading_shutdown()
	{
	}


	void threading_setNumThreads(int i)
	{
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
