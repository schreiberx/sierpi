/*
 * CAffinityMap.hpp
 *
 *  Created on: May 09, 2012
 *      Author: schreibm
 */
#ifndef CAFFINITY_MAP_HPP
#define CAFFINITY_MAP_HPP

class CAffinityMap
{
public:
	static bool setup(
			int i_num_threads,
			int i_max_cores,
			int i_displacement,
			int i_start_id,
			int i_verbose_level,

			int *affinityMap
	)
	{
		assert(i_num_threads < 1024);
		assert(i_num_threads >= 1);

		if (i_displacement < 0)
		{
			if (i_verbose_level >= 5)
				std::cout << "AFFINITY: No affinities for threads" << std::endl;

			return false;
		}

		for (int thread_id = 0; thread_id < i_num_threads; thread_id++)
		{
			/**
			 * apply padding
			 */
			int a = thread_id*i_displacement + i_start_id;
			while (a >= i_max_cores)
			{
				a -= i_max_cores;
				a++;
			}

			assert(a < i_max_cores);
			assert(a >= 0);

			affinityMap[thread_id] = a;
		}

		if (i_verbose_level >= 5)
		{
			std::stringstream ss;
			ss << "[ ";
			for (int i = 0; i < i_num_threads; i++)
				ss << affinityMap[i] << " ";
			ss << " ]";

			std::cout << "AFFINITY: threads to use: " << i_num_threads << std::endl;
			std::cout << "AFFINITY: max cores: " << i_max_cores << std::endl;
			std::cout << "AFFINITY: setting affinities with distance of " << i_displacement << ": " << ss.str() << std::endl;
		}

		if (i_displacement != 0)
		{
			for (int i = 0; i < i_num_threads; i++)
			{
				for (int j = 0; j < i_num_threads; j++)
				{
					if (i == j)
						continue;

					if (affinityMap[i] == affinityMap[j])
					{
						std::cerr << "Equal affinities for different threads found" << std::endl;
						exit(-1);
					}
				}
			}
		}

		return true;
	}

};

#endif
