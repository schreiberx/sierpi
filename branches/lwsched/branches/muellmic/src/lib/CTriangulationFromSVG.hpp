/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Apr 8, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTRIANGULATIONFROMSVG_HPP_
#define CTRIANGULATIONFROMSVG_HPP_

#include <stdint.h>
#include <vector>
#include <libxml/parser.h>
#include <string>

class CTriangulationFromSVG
{
public:
	class CTriangle2D
	{
	public:
		float vertices[3][2];
	};

	std::vector<CTriangle2D> triangles;

	CTriangulationFromSVG();

	bool loadSVGFile(const char *filename);

private:
	bool setupTransform(std::string transformString);

	bool searchDrawingArea(xmlNode * a_node);

	bool searchPath(xmlNode * a_node);

	bool newTriangleFromString(std::string triangleString);

	float transform_x, transform_y;
};

#endif /* CTRIANGULATIONFROMSVG_HPP_ */
