
evtl. immer CC=icc bei ./configure angeben!


* hdf5-1.8.8 kompilieren (standard optionen):

	CC=icc ./configure --prefix=$HOME/local

* netcdf-4.1.3 kompilieren:

	CPPFLAGS=-I$HOME/local/include LDFLAGS=-L$HOME/local/lib CC=icc ./configure --enable-shared --enable-cxx-4 --prefix=$HOME/local


* MA_rettb:
	mkdir build
	cd build
	export CMAKE_PREFIX_PATH=$HOME/local
	cmake -DNO_MPI=ON ..
