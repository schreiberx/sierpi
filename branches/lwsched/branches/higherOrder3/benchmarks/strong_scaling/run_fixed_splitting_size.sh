#! /bin/bash

. ../tools.sh

START_DEPTH=20

#export
#exit

NUM_CORES=80

# set splitting size to be fixed
PARAMS="-N $NUM_CORES -d $START_DEPTH -o $((1024*32)) -t 100 -A 2 -N 64"

# THREADING BUILDING BLOCKS!!!!!!!!!!!!!
# THREADING BUILDING BLOCKS!!!!!!!!!!!!!
# THREADING BUILDING BLOCKS!!!!!!!!!!!!!
# THREADING BUILDING BLOCKS!!!!!!!!!!!!!
EXEC="srun ../../build/sierpi_intel_tbb_tsunami_parallel_release"


THREAD_LIST=`seq $NUM_CORES -1 1`

echo "THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
for THREADS in $THREAD_LIST; do
	PARAMS_=" -n $THREADS $PARAMS $@"

	EXEC_CMD="$EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	VARNAME=MTPS$THREADS
	eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

	MTPS=$(eval echo "\$$VARNAME")

	AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
	TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
	TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
	SECONDS_FOR_SIMULATION=`getValueFromString "$OUTPUT" "SFS"`

	echo "$THREADS	$MTPS	$TIMESTEPS	$TIMESTEP_SIZE	$SECONDS_FOR_SIMULATION	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"

	echo "sleeping for 61 seconds" 1>&2
	sleep 61
done
