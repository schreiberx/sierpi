#! /bin/bash


DATE=2012_06_10


EXECS=../../build/sierpi_intel_*


#PARAMS="-o $((32*1024))"
PARAMS="-o $((32*1024)) -k 10"

if [ -z "$TBBROOT" ]; then
	echo "TBBROOT not set!"
	exit -1
fi

# TBB MALLOC PROXY
export LD_PRELOAD=libtbbmalloc_proxy.so.2:$LD_PRELOAD
export LD_LIBRARY_PATH=$TBBROOT/lib/intel64/cc4.1.0_libc2.4_kernel2.6.16.21/:$LD_LIBRARY_PATH


for e in $EXECS; do
	ENAME=${e/..\/..\/build\//}
	FILENAME=`echo "$e" | sed "s/.*\///"`
	./run_benchmark.sh $e $PARAMS -A 1  > "output_"$DATE"_"$ENAME"_d20_a8_t100.txt"
done
