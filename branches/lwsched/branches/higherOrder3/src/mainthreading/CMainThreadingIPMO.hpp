/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingIPMO.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include "libmath/CMath.hpp"

#include "CPMO_OMP.hpp"

#include "../simulations/tsunami_parallel/CSimulationTsunami_ScalabilityGraph.hpp"

#include "CMainThreading_Interface.hpp"

#define DELAYED_LEAVE_OMP_PARALLEL_REGION	0

/**
 * Threading support for iPMO
 */
class CMainThreading :
	public CMainThreading_Interface
{
	/**
	 * PMO handler
	 */
	CPMO_OMP *cPmo;

	/**
	 * initial maximum number of cores
	 */
	int initial_max_cores;

	/**
	 * previous id used for invade scalability data
	 */
	int prev_unique_invade_id;

public:
	CMainThreading() :
			initial_max_cores(1024), prev_unique_invade_id(-1) {
	}

	void threading_setup() {
#if DELAYED_LEAVE_OMP_PARALLEL_REGION
		cPmo = new CPMO_OMP(-1, true);
#else
		cPmo = new CPMO_OMP(-1, false);
#endif

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): "
					<< (int) omp_get_max_threads() << std::endl;

		threading_updateResourceUtilization();
	}

	/**
	 * update the ressource utilization
	 */
	bool threading_updateResourceUtilization() {
		unsigned long long workload = getSimulationWorkload();
		int scalability_graph_id = getThreadIPMOScalabilityGraphId();

		int max_cores;
		int scalability_graph_size;
		float *scalability_graph;
		const char *o_information_string;

		int id =
				CSimulationTsunami_ScalabilityGraph::getInvadeHintsAndConstraints(
						workload, scalability_graph_id, &max_cores,
						&scalability_graph_size, &scalability_graph,
						&o_information_string);

#if COMPILE_WITH_IPMO_ASYNC

		if (id != prev_unique_invade_id)
		{
			prev_unique_invade_id = id;
			cPmo->invade_nonblocking(1, max_cores, scalability_graph_size, scalability_graph);
		}

		bool resources_updated = cPmo->reinvade_nonblocking();

#else

		bool resources_updated;

		if (id != prev_unique_invade_id) {
			prev_unique_invade_id = id;
			resources_updated = cPmo->invade(1, max_cores,
					scalability_graph_size, scalability_graph);
		} else {
			resources_updated = cPmo->reinvade();
		}

#endif

		if (resources_updated) {
			setValueNumberOfThreadsToUse(cPmo->num_running_threads);
			return true;
		}
		setValueNumberOfThreadsToUse(cPmo->num_running_threads);

		return false;
	}

	/**
	 * run the simulation
	 */
	void threading_simulationLoop() {

#if DELAYED_LEAVE_OMP_PARALLEL_REGION

		bool continue_simulation = true;

		// REINVADE
		threading_updateResourceUtilization();

		do
		{
			// manually update number of threads since this update cannot be executed during a parallel region
			cPmo->delayedUpdateNumberOfThreads();

			/*
			 * this flag specified, whether the following loop should be quit or not
			 */
			bool resources_updated = false;

			do
			{
				continue_simulation = simulation_loopIteration();
				resources_updated = threading_updateResourceUtilization();
			}
			while (!resources_updated && continue_simulation);

		}while(continue_simulation);
#else

		/**
		 * OLD, SLOWER VERSION
		 */

		bool continue_simulation = true;

		do {
			// REINVADE
			threading_updateResourceUtilization();

			continue_simulation = simulation_loopIteration();

		} while (continue_simulation);

#endif
	}

	bool threading_simulationLoopIteration() {
		// REINVADE
		cPmo->reinvade();

		bool continue_simulation;

		continue_simulation = simulation_loopIteration();

		return continue_simulation;
	}

	void threading_shutdown() {
		cPmo->client_shutdown_hint = ((double) getSimulationSumWorkload())
				* 0.000001;
		cPmo->retreat();

		delete cPmo;
		cPmo = nullptr;
	}

	void threading_setNumThreads(int i) {
		initial_max_cores = i;
		threading_updateResourceUtilization();
	}

	virtual ~CMainThreading() {
		if (cPmo != nullptr)
			delete cPmo;
	}
};

#endif /* CMAINTHREADINGOMP_HPP_ */
