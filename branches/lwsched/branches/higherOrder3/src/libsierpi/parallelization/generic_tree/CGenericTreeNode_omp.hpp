/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_omp.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CGENERICTREENODE_OMP_HPP_
#define CGENERICTREENODE_OMP_HPP_


#define CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY	1
#include "CGenericTreeNode_serial.hpp"



/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGenericTreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
inline void specializedConstructorMethod()
{
}


#define OMP_TASK_FOR_ROOT_NODE	0



#if CONFIG_ENABLE_SCAN_DATA

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! assertions for scan workload distributions !!!!!!!!!!!!!!!!!!!!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
inline void scan_omp_asserts()
{
#if DEBUG
	assert(workload_thread_id_start != -1);
	assert(workload_thread_id_start == 0);

	assert(workload_thread_id_end != -1);
	assert(workload_thread_id_end >= 0);

#if 0
/*
 * DONT ACTIVATE THIS SINCE THE NUMBER OF THREADS ARE SET LATER ON
 */
#pragma omp parallel
	{
		if (workload_thread_id_end != (omp_get_num_threads()-1))
		{
			std::cout << "workload_thread_id_end != (omp_get_num_threads()-1)" << std::endl;
			std::cout << workload_thread_id_end << " " << omp_get_num_threads()-1 << std::endl;
			assert(false);
		}
	}
#endif
#endif
}

#endif


/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE)
 */

template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel(
		CLambdaFun i_lambda
)
{
	class CTraversalTask_GenericTreeNode_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun i_lambda_leaves
		)
		{
			if (i_this_node->isLeaf())
			{
				i_lambda_leaves(i_this_node);
				return;
			}

			if (i_this_node->first_child_node)
			{
#pragma omp task shared(i_this_node, i_lambda_leaves) OPENMP_EXTRA_TASK_CLAUSE
				traverse(i_this_node->first_child_node, i_lambda_leaves);
			}

			if (i_this_node->second_child_node)
			{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(i_this_node, i_lambda_leaves) OPENMP_EXTRA_TASK_CLAUSE
#endif
				traverse(i_this_node->second_child_node, i_lambda_leaves);
			}

#pragma omp taskwait
		}
	};


#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda)
#endif

	CTraversalTask_GenericTreeNode_Parallel::traverse(this, i_lambda);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
	}
}



#if CONFIG_ENABLE_SCAN_THREADING
/*
 * parallel scan
 */

template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda
)
{
	class CTraversalTask_GenericTreeNode_Parallel_Scan
	{
	public:
		static void traverse(
				int i_thread_id,
				CGenericTreeNode_ *i_this_node,
				CLambdaFun i_lambda_leaves
		)
		{
			assert(i_this_node->workload_thread_id_start >= 0 && i_this_node->workload_thread_id_end >= 0);

			if (i_this_node->workload_thread_id_start > i_thread_id || i_this_node->workload_thread_id_end < i_thread_id)
				return;

			if (i_this_node->isLeaf())
			{
				if (i_thread_id != i_this_node->workload_thread_id)
					return;

				i_lambda_leaves(i_this_node);
				return;
			}


			if (i_this_node->first_child_node)
			{
				traverse(i_thread_id, i_this_node->first_child_node, i_lambda_leaves);
			}

			if (i_this_node->second_child_node)
			{
				traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaves);
			}
		}
	};

	scan_omp_asserts();

#pragma omp parallel for schedule(static, 1)
	for (int i = 0; i <= workload_thread_id_end; i++)
	{
		assert(i == omp_get_thread_num());
		CTraversalTask_GenericTreeNode_Parallel_Scan::traverse(i, this, i_lambda);
	}
}


#else


template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda
)
{
	traverse_GenericTreeNode_Parallel(i_lambda);
}


#endif




/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */

/*
 * parallel
 */

template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *this_node,
				CLambdaFun1 lambda_leaves,
				CLambdaFun2 lambda_midnodes_preorder,
				CLambdaFun3 lambda_midnodes_postorder
		)
		{
			if (!this_node->isLeaf())
			{
				lambda_midnodes_preorder(this_node);
			}
			else
			{
				lambda_leaves(this_node);
				return;
			}

			if (this_node->first_child_node)
			{
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder) OPENMP_EXTRA_TASK_CLAUSE
				traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			}

			if (this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			}

	#pragma omp taskwait
	#pragma omp flush

			if (this_node)
			{
				lambda_midnodes_postorder(this_node);
			}
		}
	};



#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
	}
}




/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES
 *
 * mid and leaf node lambda functions can be called in arbitrary order
 */

/*
 * parallel
 */

template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *this_node,
				CLambdaFun1 lambda_leaves,
				CLambdaFun2 lambda_midnodes
		)
		{
			if (this_node->isLeaf())
			{
				lambda_leaves(this_node);
				return;
			}

			if (this_node->first_child_node)
			{
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
				traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
			}

			if (this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
			}

	#pragma omp taskwait
	#pragma omp flush

			if (this_node)
				lambda_midnodes(this_node);
		}
	};



#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(p_lambda_leaves, p_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel::traverse(this, p_lambda_leaves, p_lambda_midnodes);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
	}
}



/**
 * traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel_Scan not available due to postorder lambda functions!
 */
#if 0


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndMidNodes_Parallel_Scan(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	traverse_GenericTreeNode_LeafAndMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
}

#endif


/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * parallel
 */


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *this_node,
				CLambdaFun1 lambda_leaves,
				CLambdaFun2 lambda_midnodes,
				int p_genericTreeDepth
		)
		{
			if (!this_node->isLeaf())
			{
				lambda_midnodes(this_node, p_genericTreeDepth-1);
			}
			else
			{
				lambda_leaves(this_node, p_genericTreeDepth);
				return;
			}

			p_genericTreeDepth++;

			if (this_node->first_child_node)
			{
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
				traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
			}

			if (this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
			}

	#pragma omp taskwait
	#pragma omp flush
		}
	};


#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);

#pragma omp taskwait
#pragma omp flush
	}
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */




template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *this_node,
				CLambdaFun1 lambda_leaves,
				CLambdaFun2 lambda_midnodes,
				int p_genericTreeDepth
		)
		{
			if (this_node->isLeaf())
			{
				lambda_leaves(this_node, p_genericTreeDepth);
				return;
			}

			p_genericTreeDepth++;

			if (this_node->first_child_node)
			{
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
				traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
			}

			if (this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
			}

	#pragma omp taskwait
	#pragma omp flush

			lambda_midnodes(this_node, p_genericTreeDepth-1);
		}
	};


#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);

#pragma omp taskwait
#pragma omp flush
	}
}




/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/

/*
 * parallel
 */



template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel(
		CLambdaFun i_lambda_leaves,
		void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduce_output
)
{
	class CTraversalTask_GenericTreeNode_Reduce_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *p_this_node,
				CLambdaFun p_lambda,
				void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
				TReduceValue *o_reduceOutput
				)
		{
			/**
			 * LEAF COMPUTATION
			 */
			if (p_this_node->isLeaf())
			{
				p_lambda(p_this_node, o_reduceOutput);
				return;
			}

			/**
			 * PARALLEL TRAVERSALS
			 */
			TReduceValue firstValue;
			TReduceValue secondValue;

			if (p_this_node->first_child_node)
			{
	#pragma omp task shared(firstValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
				traverse(p_this_node->first_child_node, p_lambda, p_reduceOperator, &firstValue);
			}

			if (p_this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(secondValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(p_this_node->second_child_node, p_lambda, p_reduceOperator, &secondValue);
			}

	#pragma omp taskwait
	#pragma omp flush
			{
			}

			/**
			 * REDUCTION
			 */
			if (p_this_node->first_child_node)
			{
				if (p_this_node->second_child_node)
				{
					p_reduceOperator(firstValue, secondValue, o_reduceOutput);
					return;
				}
				else
				{
					*o_reduceOutput = firstValue;
					return;
				}
			}

#if !CONFIG_ENABLE_MPI

			assert(p_this_node->second_child_node != nullptr);

#else

			if (p_this_node->second_child_node != nullptr)
				*o_reduceOutput = secondValue;

#endif

			return;
		}
	};


#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves)
#endif

	CTraversalTask_GenericTreeNode_Reduce_Parallel::traverse(this, i_lambda_leaves, i_reduce_operator, o_reduce_output);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
	}
}



#if CONFIG_ENABLE_SCAN_THREADING

template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda_leaves,
		void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduce_output
)
{
	class CTraversalTask_GenericTreeNode_Reduce_Parallel_Scan
	{
	public:
		static bool traverse(
				int i_thread_id,
				CGenericTreeNode_ *i_this_node,
				CLambdaFun i_lambda_leaves,
				void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
				TReduceValue *o_reduce_output
			)
		{
			assert(i_this_node->workload_thread_id_start >= 0 && i_this_node->workload_thread_id_end >= 0);

			if (i_this_node->workload_thread_id_start > i_thread_id || i_this_node->workload_thread_id_end < i_thread_id)
				return false;


			/*
			 * LEAF COMPUTATION
			 */
			if (i_this_node->isLeaf())
			{
				if (i_thread_id != i_this_node->workload_thread_id)
					return false;

				i_lambda_leaves(i_this_node, o_reduce_output);
				return true;
			}


			TReduceValue firstValue;
			TReduceValue secondValue;

			/*
			 * run traversal on first child
			 */
			if (i_this_node->first_child_node)
			{
				if (!traverse(i_thread_id, i_this_node->first_child_node, i_lambda_leaves, i_reduce_operator, &firstValue))
				{
					// return value of 1st child is not valid -> only use return value of 2nd child

					if (!i_this_node->second_child_node)
						return false;

					if (!traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaves, i_reduce_operator, o_reduce_output))
						return false;	// return false if 2nd return value was also not valid

					// value o_reduce_output was written by 2nd child
					return true;
				}

				// 1st traversal was successful => try to append 2nd one

#if !CONFIG_ENABLE_MPI
				assert(i_this_node->second_child_node);
#else
				if (i_this_node->second_child_node)
#endif
				{
					if (!traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaves, i_reduce_operator, &secondValue))
					{
						// 2nd traversal with invalid data
						*o_reduce_output = firstValue;
						return true;
					}

					// both reduce values have been written
					i_reduce_operator(firstValue, secondValue, o_reduce_output);
				}

				return true;
			}


#if CONFIG_ENABLE_MPI
			if (i_this_node->second_child_node == nullptr)
				return false;
#endif

			/*
			 * first child does not exist => only run traversal on 2nd child
			 */
			return traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaves, i_reduce_operator, o_reduce_output);
		}
	};


	scan_omp_asserts();

	TReduceValue reduce_values[workload_thread_id_end+1];
	TReduceValue initial_reduce_output = *o_reduce_output;

#pragma omp parallel for schedule(static, 1)
	for (int i = 0; i <= workload_thread_id_end; i++)
	{
		assert(i == omp_get_thread_num());

		reduce_values[i] = initial_reduce_output;
		CTraversalTask_GenericTreeNode_Reduce_Parallel_Scan::traverse(i, this, i_lambda_leaves, i_reduce_operator, &(reduce_values[i]));
	}


	TReduceValue reduce_values_output[workload_thread_id_end+1];

	reduce_values_output[0] = reduce_values[0];
	for (int i = 1; i <= workload_thread_id_end; i++)
	{
		i_reduce_operator(reduce_values_output[i-1], reduce_values[i], &reduce_values_output[i]);
	}

	*o_reduce_output = reduce_values_output[workload_thread_id_end];
}

#else


template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda_leaves,
		void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduce_output
)
{
	traverse_GenericTreeNode_Reduce_Parallel(i_lambda_leaves, i_reduce_operator, o_reduce_output);
}

#endif


/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/

/*
 * parallel
 */



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel
	{
	public:
		static void traverse(
				CGenericTreeNode_ *p_this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes,
				void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
				TReduceValue *o_reduceOutput
			)
		{
			/**
			 * LEAF COMPUTATION
			 */
			if (p_this_node->isLeaf())
			{
				i_lambda_leaves(p_this_node, o_reduceOutput);
				return;
			}

			/**
			 * PARALLEL TRAVERSALS
			 */
			TReduceValue firstValue;
			TReduceValue secondValue;

			if (p_this_node->first_child_node)
			{
	#pragma omp task shared(firstValue, p_this_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
				traverse(p_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &firstValue);
			}

			if (p_this_node->second_child_node)
			{
	#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#pragma omp task shared(secondValue, p_this_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
	#endif
				traverse(p_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &secondValue);
			}

	#pragma omp taskwait
	#pragma omp flush
			{
			}

			/**
			 * REDUCTION
			 */
			if (p_this_node->first_child_node)
			{
				if (p_this_node->second_child_node)
				{
					p_reduceOperator(firstValue, secondValue, o_reduceOutput);

					i_lambda_midnodes(p_this_node, o_reduceOutput);
					return;
				}
				else
				{
					*o_reduceOutput = firstValue;

					i_lambda_midnodes(p_this_node, o_reduceOutput);
					return;
				}
			}

#if !CONFIG_ENABLE_MPI

			assert(p_this_node->second_child_node != nullptr);
			*o_reduceOutput = secondValue;

#else

			if (p_this_node->second_child_node)
				*o_reduceOutput = secondValue;

#endif


			i_lambda_midnodes(p_this_node, o_reduceOutput);
			return;
		}
	};


#pragma omp parallel
#pragma omp master
	{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes, o_reduceOutput)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel::traverse(this, i_lambda_leaves, i_lambda_midnodes, reduceOperator, o_reduceOutput);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#pragma omp flush(o_reduceOutput)
#endif

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
	}
}


/*
 * _Scan version not available due to postorder midnodes
 */
#if 0

template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(i_lambda_leaves, i_lambda_midnodes, reduceOperator, o_reduceOutput);
}

#endif



#endif
