/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRAVERSATOR_VERTEXCOORDS_ELEMENTDATA_HPP_
#define CTRAVERSATOR_VERTEXCOORDS_ELEMENTDATA_HPP_

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"

namespace sierpi
{
namespace travs
{

template <
	class t_CKernelClass,
	class t_CSimulationStacksAndTypes
>
class CTraversator_VertexCoords_CellData
{
public:
	/**
	 * implementation of kernel class
	 */
	t_CKernelClass cKernelClass;

	/**
	 * a few typedefs which could be utilized by the traversator to get more informations
	 */
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::T TVertexScalar;

	typedef CTraversator_VertexCoords_CellData<t_CKernelClass, t_CSimulationStacksAndTypes> TThisClass;

	/**
	 * stack and lifo handlers used in this traversator
	 */
	CStackReaderTopDown<char> structure_lifo_in;
	CStackReaderTopDown<CCellData> element_data_lifo;

	/**
	 * include automagically generated code
	 */
#include "auto/CTraversator_VertexCoords_CellData_autogenerated.hpp"
#include "../CTraversatorClassInc_SetupSinglePass.hpp"


	/**
	 * do the traversal!
	 */
	void action(
			t_CSimulationStacksAndTypes *cSimulationStacks
	)
	{
		assert(cSimulationStacks->structure_stacks.direction == cSimulationStacks->element_data_stacks.direction);
		assert(cSimulationStacks->structure_stacks.direction == CFBStacks<char>::FORWARD);

		structure_lifo_in.setup(cSimulationStacks->structure_stacks.forward);
		element_data_lifo.setup(cSimulationStacks->element_data_stacks.forward);

		cKernelClass.traversal_pre_hook();

		(this->*sfcRecursiveMethod_Forward)(
				cTriangleFactory.vertices[0][0], cTriangleFactory.vertices[0][1],
				cTriangleFactory.vertices[1][0], cTriangleFactory.vertices[1][1],
				cTriangleFactory.vertices[2][0], cTriangleFactory.vertices[2][1]
			);

		cKernelClass.traversal_post_hook();

		assert(element_data_lifo.isEmpty());
		assert(structure_lifo_in.isEmpty());
	}
};

}
}

#endif /* CSTRUCTURE_VERTEXDATA_H_ */
