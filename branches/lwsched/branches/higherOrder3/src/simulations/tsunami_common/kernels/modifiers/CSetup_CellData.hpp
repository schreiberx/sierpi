/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CKERNEL_SETUP_ELEMENT_DATA_TSUNAMI_HPP_
#define CKERNEL_SETUP_ELEMENT_DATA_TSUNAMI_HPP_


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CSetup_CellData_0thOrder.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_CellData_0thOrder<CTsunamiSimulationStacksAndTypes> CSetup_CellData;
	}
}

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1

#include "CSetup_CellData_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_CellData_1stOrder<CTsunamiSimulationStacksAndTypes> CSetup_CellData;
	}
}

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==2

#include "CSetup_CellData_2ndOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_CellData_2ndOrder<CTsunamiSimulationStacksAndTypes> CSetup_CellData;
	}
}

#endif

#endif
