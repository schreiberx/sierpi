/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGETDATASAMPLE_VERTICES_ELEMENT_TSUNAMI_HPP_
#define CGETDATASAMPLE_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libmath/CPointInTriangleTest.hpp"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{



template <typename t_CSimulationStacks>
class CGetDataSample_Vertices_Cell_Tsunami
{
public:
	typedef typename t_CSimulationStacks::CSimulationTypes::CCellData	CCellData_;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_CellData<CGetDataSample_Vertices_Cell_Tsunami<t_CSimulationStacks>, t_CSimulationStacks > TRAV;

	TTsunamiDataScalar *result;
	const char *sample_information;
	TTsunamiVertexScalar sample_pos_x;
	TTsunamiVertexScalar sample_pos_y;


private:
	TVertexScalar *last_triangle;
	size_t max_triangles;

public:
	inline void op_cell(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			CCellData_ *i_element_data
	)
	{
		if (!CPointInTriangleTest<double>::test(
				v0x, v0y,
				v1x, v1y,
				v2x, v2y,
				sample_pos_x, sample_pos_y
			))
				return;

		if (strcmp(sample_information, "b+h"))
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

			*result = i_element_data->dofs_center.b + i_element_data->dofs_center.h;

#else

			std::cout << "unsupported so far!" << std::endl;
			assert(false);

#endif
			return;
		}

		std::cerr << "unknown sample information " << sample_information << std::endl;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(
			TTsunamiDataScalar *o_result,

			const char *i_sample_information,
			TTsunamiVertexScalar i_sample_pos_x,
			TTsunamiVertexScalar i_sample_pos_y
	)
	{
		result = o_result;

		sample_information = i_sample_information;
		sample_pos_x = i_sample_pos_x;
		sample_pos_y = i_sample_pos_y;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			CGetDataSample_Vertices_Cell_Tsunami &parent_kernel
	)
	{
		assert(false);
	}
};


}
}

#endif
