/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../kernels/simulation/CEdgeComm_Tsunami.hpp"


namespace sierpi
{
namespace travs
{


class CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private	:
	public CFluxComm_Normals_Depth<sierpi::kernels::CEdgeComm_Tsunami, CTsunamiSimulationStacksAndTypes>
{
};

CSpecialized_Tsunami_EdgeComm_Normals_Depth::CSpecialized_Tsunami_EdgeComm_Normals_Depth()
{
	generic_traversator = new CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private;
	cKernelClass = &(generic_traversator->cKernelClass);
}


CSpecialized_Tsunami_EdgeComm_Normals_Depth::~CSpecialized_Tsunami_EdgeComm_Normals_Depth()
{
	delete generic_traversator;
}

void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionFirstPass(
		CTsunamiSimulationStacksAndTypes *io_cSimulationStacks
)
{
	generic_traversator->action_FirstPass(io_cSimulationStacks);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionSecondPass_Serial(
		CTsunamiSimulationStacksAndTypes *io_cSimulationStacks,
		TTsunamiDataScalar i_timestep_size,
		CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private::TReduceValue *o_reduceValue
)
{
	generic_traversator->action_SecondPass_Serial(io_cSimulationStacks, i_timestep_size, o_reduceValue);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::actionSecondPass_Parallel(
		CTsunamiSimulationStacksAndTypes *io_cSimulationStacks,
		TTsunamiDataScalar i_timestep_size,
		CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private::TReduceValue *o_reduceValue
)
{
	generic_traversator->action_SecondPass_Parallel(io_cSimulationStacks, i_timestep_size, o_reduceValue);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setParameters(
		TTsunamiDataScalar i_delta_timestep,
		TTsunamiDataScalar i_square_side_length,
		TTsunamiDataScalar i_gravity
)
{
	generic_traversator->cKernelClass.setParameters(i_delta_timestep, i_square_side_length, i_gravity);
}



#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setAdaptivityParameters(
		TTsunamiDataScalar i_refine_threshold,
		TTsunamiDataScalar i_coarsen_threshold
)
{
	generic_traversator->cKernelClass.setAdaptivityParameters(i_refine_threshold, i_coarsen_threshold);
}
#endif

void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setup_Cluster(
		CSpecialized_Tsunami_EdgeComm_Normals_Depth &i_parent,
		CTriangle_Factory &i_triangleFactory
)
{
	generic_traversator->setup_Cluster(*(i_parent.generic_traversator), i_triangleFactory);
}



void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setup_sfcMethods(
		CTriangle_Factory &i_triangleFactory
)
{
	generic_traversator->setup_sfcMethods(i_triangleFactory);
}


void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setBoundaryCondition(
		EBoundaryConditions i_eBoundaryCondition
)
{
	generic_traversator->cKernelClass.eBoundaryCondition = i_eBoundaryCondition;
}


void CSpecialized_Tsunami_EdgeComm_Normals_Depth::setBoundaryDirichlet(
		const CTsunamiSimulationEdgeData *i_value
)
{
	generic_traversator->cKernelClass.setBoundaryDirichlet(i_value);
}


TTsunamiDataScalar CSpecialized_Tsunami_EdgeComm_Normals_Depth::getTimestepSize()
{
	return generic_traversator->cKernelClass.getTimestepSize();
}


}
}
