/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: July 19, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMPLE_NETCDF_HPP_
#define CSIMPLE_NETCDF_HPP_

#include "../types/CTsunamiTypes.hpp"
#include "CDataSet_Interface.hpp"


template <typename T>
class CSimpleNetCDF_Data_private;

/**
 * Dataset interface to the simple netCDF data handler
 *
 * the netCDF data is used in a mipmap
 */
class CSimpleNetCDF	:
	public CDataSet_Interface
{
private:
	typedef CTsunamiSimulationTypes::T T;

public:
	void getOriginAndSize(
			T	*o_origin_x,
			T	*o_origin_y,
			T	*o_size_x,
			T	*o_size_y
	);

	CSimpleNetCDF_Data_private<TTsunamiDataScalar> *cSimpleNetCDF_TerrainData_private;

	CSimpleNetCDF_Data_private<TTsunamiDataScalar> *cSimpleNetCDF_SurfaceDisplacementData_private;

	/**
	 * get nodal data (surface height, momentums and bathymetry) for given coordinate
	 */
	void getNodalData(
			T i_x,
			T i_y,
			int i_level_of_detail,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	/**
	 * get displacement data for given coordinate
	 */
	T getDisplacementData(
			T i_x,		///< x-coordinate in model-space
			T i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);



	/**
	 * get bathymetry data for given coordinate
	 */
	T getBathymetryData(
			T i_x,		///< x-coordinate in model-space
			T i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);

	T getWaterSufaceData(
			T i_x,		///< x-coordinate in model-space
			T i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);

	/**
	 * get boundary data at given position
	 */
	void getBoundaryData(
			T i_x,
			T i_y,
			int i_level_of_detail,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	/**
	 * get benchmark (analytical) nodal data
	 */
	void getBenchmarkNodalData(
			T i_x,
			T i_y,
			int i_level_of_detail,
			T i_timestamp,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	bool isDatasetLoaded();

	void outputVerboseInformation();


	bool loadDataset(
			const char *i_filename_bathymetry,		///< filename for bathymetry data
			const char *i_filename_displacements	///< filename for displacement data
	);


	CSimpleNetCDF();

	virtual ~CSimpleNetCDF();


private:

//public:
	T domain_length_x;
	T domain_length_y;

	T domain_min_x;
	T domain_min_y;

	T domain_max_x;
	T domain_max_y;

	T domain_center_x;
	T domain_center_y;


	T displacements_size_x;
	T displacements_size_y;

	T displacements_min_x;
	T displacements_min_y;

	T displacements_max_x;
	T displacements_max_y;

	T displacements_center_x;
	T displacements_center_y;

	/**
	 * set to true when setup was successful
	 */
	bool is_dataset_loaded;

	/**
	 * singleton for bathymetry datasets
	 */
//	asagi::Grid *bathymetry_grid;

	/**
	 * singleton for displacements
	 */
//	asagi::Grid *displacements_grid;

};

#endif /* CASAGI_HPP_ */
