#! /bin/bash

. inc_parameters.sh
. ../inc_environment_vars.sh

echo "CPUS: $CPULIST"
echo "SPLIT SIZES: $SPLIT_SIZES"


for s in $SPLIT_SIZES; do
	echo "Split Size: $s"

	if [ $KMLAFFINITY ]; then
		echo "Threads	MTPS	SPEEDUP	SPEEDUP_PER	KML_SPEEDUP	KML_SPEEDUP_TO_NONAFFINITY"
	else
		echo "Threads	MTPS	SPEEDUP	SPEEDUP_PER"
	fi

	for n in $CPULIST; do
		THREADS=$((n+1))
		PARAMS_="-o $s -n $THREADS $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT=`$EXEC_CMD`
		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo \$$VARNAME)
		SPEEDUP=$(echo "scale=4; $MTPS/$MTPS1" | bc)
		SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

		OUTPUT_LINE="$THREADS	$MTPS	$SPEEDUP	$SPEEDUP_PER"

		if $KMLAFFINITY; then
			# compare with setting intel KMP affinities

			export KMP_AFFINITY="proclist=[$PROCLIST],explicit"
			KML_OUTPUT=`$EXEC $PARAMS_`
			unset KMP_AFFINITY;

			KML_VARNAME=KML_MTPS$THREADS
			eval $KML_VARNAME=`getValueFromString "$KML_OUTPUT" "MTPS"`

			KML_MTPS=$(eval echo \$$KML_VARNAME)
			KML_SPEEDUP=$(echo "scale=4; $KML_MTPS/$KML_MTPS1" | bc)
			KML_SPEEDUP_PER=$(echo "scale=4; $KML_MTPS/($KML_MTPS1*$THREADS)" | bc)
			KML_SPEEDUP_NONAFFINITY=$(echo "scale=4; ($KML_MTPS-$MTPS)/($MTPS)" | bc)

			OUTPUT_LINE="$OUTPUT_LINE	$KML_SPEEDUP	$KML_SPEEDUP_NONAFFINITY"
		fi

		echo "$OUTPUT_LINE"
	done
done
