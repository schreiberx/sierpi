. ../tools.sh

THREADS=`seq 1 12`
SPLIT_SIZES=`seqPow2 64 $((32*1024))`

#INITIAL_DEPTH=8
#TIMESTEPS=3400

#INITIAL_DEPTH=10
#TIMESTEPS=6500

#INITIAL_DEPTH=12
#TIMESTEPS=13000

#INITIAL_DEPTH=14
#TIMESTEPS=25600

INITIAL_DEPTH=16
TIMESTEPS=50600

EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

PARAMS="-d $INITIAL_DEPTH -t $TIMESTEPS -a 6"

CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`
CPULIST="0"
for i in `seq 1 $((CPUS-1))`; do
	CPULIST+=" $i"
done

PROCLIST=$(echo $CPULIST | sed "y/ /,/")

KMLAFFINITY=true
