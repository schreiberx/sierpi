#
# create a pow2 sequence
#   param1: start value
#   param2: value at which the sequence generation is stopped
#
seqPow2 () {
	S=$1
	while [ $S -le $2 ]; do
		OUTPUT="$OUTPUT $S"
		S=$(($S*2))
	done

	echo "$OUTPUT"
}

#
# read a value from a file
# values have to be available in the form "[floatvalue]\t[identifier].*"
#    param1: filename
#    param2: identifier
#
getValueFromFile () {
	echo "`cat \"$1\" | grep \" $2 \" | sed \"s/ $2 .*//\"`"
}



#
# read a value from a string
# values have to be available in the form "[floatvalue]\t[identifier].*"
#    param1: some string
#    param2: identifier
#
getValueFromString () {
	echo "`echo -en \"$1\" | grep \"[ 	]$2 \" | sed \"s/[	 ]$2 .*//\"`"
}


# Number of CPUs
CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`

# cpu list starting at 0
CPULIST="0"
for i in `seq 1 $((CPUS-1))`; do
	CPULIST+=" $i"
done

# reversed CPU list
CPULIST_REV=""
for i in `seq $((CPUS-1)) -1 1`; do
	CPULIST_REV+="$i "
done
CPULIST_REV+="0"


# cpu list separated by comma ','
PROCLIST=$(echo $CPULIST | sed "y/ /,/")



# affinity for intel compiled programs
#export KMP_AFFINITY="explicit,proclist=[$PROCLIST]"
export KMP_AFFINITY="compact"
#export KMP_AFFINITY="scatter"
#export KMP_AFFINITY="proclist=[$PROCLIST],explicit"

# activate verbose mode
#export KMP_AFFINITY="$KMP_AFFINITY,verbose"

# default executable
EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"


