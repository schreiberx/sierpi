#! /bin/bash

. inc_parameters.sh

TIMESTEPS=$TIMESTEPS_COARSEST_LEVEL

EXEC_S=../../build/sierpi_intel_tsunami_serial_release
EXEC_P=../../build/sierpi_intel_omp_tsunami_parallel_release

# -a 0: deactivate adaptivity
PARAMS_S=""

# -w 0: load world 0
# -o 999...: set splitting size to 999... to avoid any splitting
# -n 1: run on only one task
PARAMS_P1="-w 0 -n 1"
PARAMS_P2="-w 0 -n 1 -u 10 -U 11"
PARAMS_P3="-w 0 -n 2 -u 10 -U 11"

echo 
echo "Simulation on single triangle"
echo
echo "Serial: simply the serial version"
echo "Parallel1: parallel version without any splits"
echo "Parallel2: parallel version with any splits and joins"
echo 
echo "INITIAL_DEPTH	MTPS_SERIAL	MTPS_PARALLEL1	MTPS_PARALLEL2	MTPS_PARALLEL3	TIMESTEPS	AVERAGE_TRIANGLES_SERIAL	AVERAGE_TRIANGLES_PARALLEL1	AVERAGE_TRIANGLES_PARALLEL2	AVERAGE_TRIANGLES_PARALLEL3	AVERAGE_PARTITIONS_PARALLEL2"

for d in $PROBLEM_SIZES_IN_DEPTH; do

	REL_DEPTH=$((26-d))
	[ $REL_DEPTH -lt 0 ] && REL_DEPTH=0

	TIMESTEPS=$((2**REL_DEPTH))
	[ $TIMESTEPS -lt 4 ] && TIMESTEPS=4

	######################
	# SERIAL
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_S $@"

	EXEC_CMD="$EXEC_S $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_SERIAL=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_SERIAL=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_SERIAL=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL nosplit
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P1 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL1=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_PARALLEL1=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL1=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL split
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P2 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL2=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_PARALLEL2=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL2=`getValueFromString "$OUTPUT" "PPST"`


	######################
	# PARALLEL 2 threads
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P3 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL3=`getValueFromString "$OUTPUT" "MTPSPT"`
	AVERAGE_TRIANGLES_PARALLEL3=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL3=`getValueFromString "$OUTPUT" "PPST"`

	echo "$d	$MTPS_SERIAL	$MTPS_PARALLEL1	$MTPS_PARALLEL2	$MTPS_PARALLEL3	$TIMESTEPS	$AVERAGE_TRIANGLES_SERIAL	$AVERAGE_TRIANGLES_PARALLEL1	$AVERAGE_TRIANGLES_PARALLEL2	$AVERAGE_TRIANGLES_PARALLEL3	$AVERAGE_PARTITIONS_PARALLEL2"
done
