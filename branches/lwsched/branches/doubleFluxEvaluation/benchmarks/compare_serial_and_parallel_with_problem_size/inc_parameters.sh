. ../tools.sh
. ../inc_environment_vars.sh

CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`

# used number of threads
THREADS=$CPUS


# split sizes
SPLIT_SIZES=`seqPow2 64 $((128*1024))`

# problem size given in depth
PROBLEM_SIZES_IN_DEPTH=`seq 10 28`
#PROBLEM_SIZES_IN_DEPTH=`seq 22 24`

# executable
EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

# no adaptive refinement
PARAMS="$PARAMS -a 0"
