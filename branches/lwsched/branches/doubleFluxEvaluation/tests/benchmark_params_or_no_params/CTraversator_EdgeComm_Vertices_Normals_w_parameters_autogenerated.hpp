/*
 * Traversator functions, new version
 *
 */

/*
 * Forward traversators
 */
void Ke_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ho_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		Vo_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

}

void Ho_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ve_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Ke_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

}

void Ve_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ho_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		Ko_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

}

void Ko_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		He_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Ve_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

}

void He_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Vo_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		Ko_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());

	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

}

void Vo_forward_normal(TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1,
		TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2,
		char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		He_forward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Ke_forward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		structure_stack_out->push(1);
		return;
	}

	structure_stack_out->push(0);
	TElementData *element_data = element_data_lifo.getNextDataPtr();

	if (edge_type_right_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_right_edge == 'n') {
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_hyp == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_left_edge_stack->pop_ReturnPtr());
	} else if (edge_type_hyp == 'n') {
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		edge_comm_buffer->push_PtrValue(
				edge_data_comm_right_edge_stack->pop_ReturnPtr());
	} else if (edge_type_left_edge == 'n') {
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

}

/*
 * Backward traversators
 */

void Ke_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {
	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Vo_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Ho_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		return;
	}
	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data, &left_edge_data);

				return;
			} else {
				kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data);

				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);

			return;
		} else {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);

			return;
		}
	} else {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);
	}

}

void Vo_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {
	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ke_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		He_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data, &left_edge_data);

				return;
			} else {
				kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data);

				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);
			return;
		} else {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);
			return;
		}
	} else {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);
	}

}

void Ho_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {
	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ke_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		Ve_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		return;
	};
	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data, &left_edge_data);

				return;
			} else {
				kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data);

				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);

			return;
		} else {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);

			return;
		}
	} else {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);
	}

}

void He_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {

	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ko_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Vo_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		return;
	}
	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	/*
	 * elementAction
	 */
	if (edge_type_hyp != 'b') {
		if (edge_type_right_edge != 'b') {
			if (edge_type_left_edge != 'b') {
				kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data, &left_edge_data);

				return;
			} else {
				kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
						normalTable[(int) normal][0],
						normalTable[(int) normal][1], normalTable[((int) normal
								+ 5) % 8][0], normalTable[((int) normal + 5)
								% 8][1],
						normalTable[((int) normal + 3) % 8][0],
						normalTable[((int) normal + 3) % 8][1], element_data,
						&hyp_edge_data, &right_edge_data);

				return;
			}
		} else if (edge_type_left_edge != 'b') {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);

			return;
		} else {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);

			return;
		}
	} else {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);
	}

}

void Ko_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {
	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ve_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'n', (normal + 3) % 8);
		He_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'o', edge_type_hyp, (normal + 5) % 8);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	/*
	 * elementAction
	 */
	if (edge_type_hyp == 'b') {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);

		return;
	} else if (edge_type_right_edge == 'b') {
		if (edge_type_left_edge == 'b') {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);

			return;
		} else {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);

			return;
		}
	} else if (edge_type_left_edge == 'b') {
		kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&hyp_edge_data, &right_edge_data);

		return;
	} else {
		kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&hyp_edge_data, &right_edge_data, &left_edge_data);
	}

}

void Ve_backward_normal(TVertexScalar vx0, TVertexScalar vy0,
		TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2,
		TVertexScalar vy2, char edge_type_hyp, char edge_type_right_edge,
		char edge_type_left_edge, char normal) {
	if (structure_lifo_in.getNextData()) {
		TVertexScalar mx = (vx0 + vx1) * (TVertexScalar) 0.5;
		TVertexScalar my = (vy0 + vy1) * (TVertexScalar) 0.5;
		Ko_backward_normal(vx1, vy1, vx2, vy2, mx, my, edge_type_right_edge,
				'n', edge_type_hyp, (normal + 5) % 8);
		Ho_backward_normal(vx2, vy2, vx0, vy0, mx, my, edge_type_left_edge,
				edge_type_hyp, 'o', (normal + 3) % 8);
		return;
	}

	TElementData *element_data = element_data_fifo.getNextDataPtr();
	TEdgeData hyp_edge_data;
	TEdgeData left_edge_data;
	TEdgeData right_edge_data;

	if (edge_type_hyp == 'o') {
		hyp_edge_data = edge_data_comm_left_edge_stack->pop_ReturnRef();
	} else if (edge_type_hyp == 'n') {
		hyp_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeHypCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_left_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_right_edge == 'o') {
		right_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_right_edge == 'n') {
		right_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeRightEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}

	if (edge_type_left_edge == 'o') {
		left_edge_data = edge_data_comm_right_edge_stack->pop_ReturnRef();
	} else if (edge_type_left_edge == 'n') {
		left_edge_data = edge_comm_buffer->pop_ReturnRef();
		kernelClass.storeLeftEdgeCommData(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				edge_data_comm_right_edge_stack->fakePush_ReturnPtr());
	}
	/*
	 * elementAction
	 */
	if (edge_type_hyp == 'b') {
		kernelClass.elementAction_BEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&right_edge_data, &left_edge_data);

		return;
	} else if (edge_type_right_edge == 'b') {
		if (edge_type_left_edge == 'b') {
			kernelClass.elementAction_EBB(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data);

			return;
		} else {
			kernelClass.elementAction_EBE(vx0, vy0, vx1, vy1, vx2, vy2,
					normalTable[(int) normal][0], normalTable[(int) normal][1],
					normalTable[((int) normal + 5) % 8][0],
					normalTable[((int) normal + 5) % 8][1],
					normalTable[((int) normal + 3) % 8][0],
					normalTable[((int) normal + 3) % 8][1], element_data,
					&hyp_edge_data, &left_edge_data);

			return;
		}
	} else if (edge_type_left_edge == 'b') {
		kernelClass.elementAction_EEB(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&hyp_edge_data, &right_edge_data);

		return;
	} else {
		kernelClass.elementAction_EEE(vx0, vy0, vx1, vy1, vx2, vy2,
				normalTable[(int) normal][0], normalTable[(int) normal][1],
				normalTable[((int) normal + 5) % 8][0],
				normalTable[((int) normal + 5) % 8][1],
				normalTable[((int) normal + 3) % 8][0],
				normalTable[((int) normal + 3) % 8][1], element_data,
				&hyp_edge_data, &right_edge_data, &left_edge_data);

		return;
	}
}
