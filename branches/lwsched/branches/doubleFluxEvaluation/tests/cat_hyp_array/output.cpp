#include <iostream>
#include <cmath>


typedef double TTsunamiDataScalar;

inline TTsunamiDataScalar getUnitCathetusLengthForDepth(long long unsigned int i_depth)
{
	long long unsigned int depth_even = i_depth >> 1;

	TTsunamiDataScalar cathetus = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)((long long unsigned int)1<<depth_even);

	if (i_depth & 1)
		cathetus = (TTsunamiDataScalar)0.5*std::sqrt(2.0)*cathetus;

	return cathetus;
}

inline TTsunamiDataScalar getUnitHypotenuseLengthForDepth(long long unsigned int i_depth)
{
	i_depth += 1;
	long long unsigned int depth_even = i_depth >> 1;

	TTsunamiDataScalar hypotenuse = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)((long long unsigned int)1<<depth_even);

	if (i_depth & 1)
		hypotenuse = (TTsunamiDataScalar)0.5*std::sqrt(2.0)*hypotenuse;

	return hypotenuse*2.0;
}

int main()
{
	std::cout.precision(20);
	std::cout << "static TTsunamiDataScalar catheti_lengths[64] = {" << std::endl;
	for (int i = 0; i < 64; i++)
	{
		std::cout << "	" << getUnitCathetusLengthForDepth(i);
		if (1 != 63)
			std::cout << ",";
		std::cout << std::endl;
	}
	std::cout << "};";


	std::cout << "static TTsunamiDataScalar hypotenuse_lengths[64] = {" << std::endl;
	for (int i = 0; i < 64; i++)
	{
		std::cout << "	" << getUnitHypotenuseLengthForDepth(i);
		if (1 != 63)
			std::cout << ",";
		std::cout << std::endl;
	}
	std::cout << "};";

}
