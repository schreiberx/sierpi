/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CBaseTriangulation_To_GenericTree.hpp
 *
 *  Created on: Oct 12, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CBASETRIANGULATION_TO_GENERICTREE_HPP_
#define CBASETRIANGULATION_TO_GENERICTREE_HPP_

#include "CDomain_BaseTriangulation.hpp"


/**
 * \brief converts a base triangulation to a generic tree structure
 *
 * base triangulation is given via CDomain_BaseTriangulation.
 */
template <typename CSimulation_PartitionHandler>
class CBaseTriangulation_To_GenericTree
{
	typedef CDomain_BaseTriangle<CSimulation_PartitionHandler> CDomain_BaseTriangle_;
	typedef CDomain_BaseTriangulation<CSimulation_PartitionHandler> CDomain_BaseTriangulation_;

	typedef CPartition_TreeNode<CSimulation_PartitionHandler> CPartition_;
	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition_> CEdgeComm_InformationAdjacentPartition_;
	typedef CGenericTreeNode<CPartition_> CGenericTreeNode_;


private:
	/**
	 * maximum allowed unique id depth for root triangles
	 */
	int max_unique_id_depth;


	/**
	 * unique id counter for initialization
	 */
	int unique_id_counter;


	/*
	 * edge comm elements along a single cathetus
	 */
	int cat_edge_comm_elements;

	/*
	 * edge comm elements along a hypotenuse
	 */
	int hyp_edge_comm_elements;

	/*
	 * initial depth and thus number of triangles to setup stacks
	 */
	int initial_rec_depth;

	/*
	 * minimum relative refinement depth
	 */
	int min_depth;

	/*
	 * maximum relative refinement depth
	 */
	int max_depth;

	/**
	 * pointer to domain root triangulation
	 */
	CDomain_BaseTriangulation_ *cDomain_RootTriangulation;

	/**
	 * number of root triangles
	 */
	unsigned long long number_of_initial_root_partitions;

	/**
	 * root node for traversals
	 */
	CGenericTreeNode_ *root_node;


public:
	/**
	 * constructor
	 */
	CBaseTriangulation_To_GenericTree()	:
		cat_edge_comm_elements(-1),
		hyp_edge_comm_elements(-1)
	{
	}



private:
	/**
	 * recursive method to insert the new generic treenode given by `iter` below the given parent_node
	 */
	CGenericTreeNode_ *_setup_GenericTreeAndPartitions(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGenericTreeNode_ *parent_node,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		CDomain_BaseTriangle_ &cDomain_RootTriangle = *iter;
		assert(current_depth <= max_unique_id_depth);

		// leaf reached => create partition
		if (current_depth == max_unique_id_depth)
		{
			/****************************************************************
			 * allocate new partition node
			 ****************************************************************/
			CGenericTreeNode_ *cGeneric_TreeNode = new CGenericTreeNode_(parent_node, true);

			CPartition_ *cPartitionTreeNode = new CPartition_(
					cDomain_RootTriangle.triangleFactory	// triangle factory
				);

			cGeneric_TreeNode->cPartition_TreeNode = cPartitionTreeNode;
			cPartitionTreeNode->cGeneric_TreeNode = cGeneric_TreeNode;

			/*
			 * update partition node's adaptive depth values!
			 */
			assert(cPartitionTreeNode->triangleFactory.recursionDepthFirstRecMethod == 0);
			cPartitionTreeNode->triangleFactory.minDepth = min_depth;
			cPartitionTreeNode->triangleFactory.maxDepth = max_depth;

			/*
			 * setup unique id and increment unique id counter
			 */
			cPartitionTreeNode->uniqueId.setup(unique_id_counter + (1 << max_unique_id_depth), max_unique_id_depth);
			unique_id_counter++;

			cDomain_RootTriangle.cPartition_user_ptr = cPartitionTreeNode;

			// setup genericTreeNode's variable to the parent node
			cPartitionTreeNode->cGeneric_TreeNode->parent_node = parent_node;

			/*
			 * convert edges of type OLD to type NEW for parallelization reasons
			 */
			if (cPartitionTreeNode->triangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD)
				cPartitionTreeNode->triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cPartitionTreeNode->triangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD)
				cPartitionTreeNode->triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cPartitionTreeNode->triangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD)
				cPartitionTreeNode->triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

			// increment the iterator
			iter++;

			// return pointer to this node
			return cPartitionTreeNode->cGeneric_TreeNode;
		}

		current_depth++;

		// create a new tree node with attached leaves
		/****************************************************************
		 * allocate new generic tree node
		 ****************************************************************/
		CGenericTreeNode_ *new_node = new CGenericTreeNode_(parent_node, true);

		// left traversal
		new_node->first_child_node =
			_setup_GenericTreeAndPartitions(
				iter,		// reference to iterator
				new_node,	// this (parent) node
				current_depth
			);

		if (iter != cDomain_RootTriangulation->domainRootTriangles.end())
		{
			// right traversal
			new_node->second_child_node =
				_setup_GenericTreeAndPartitions(
					iter,		// reference to iterator
					new_node,	// this (parent) node
					current_depth
				);
		}

		return new_node;
	}


	/**
	 * setup the root node of the given partition
	 *
	 * the setup includes the information about the RLE encoded communication between the partitions
	 */
private:
	void _setup_BasePartitionAdjacencyInformation(
			CPartition_ *io_partitionTreeNode,

			CPartition_ *i_adjacentHypEdgePartition,
			CPartition_ *i_adjacentRightEdgePartition,
			CPartition_ *i_adjacentLeftEdgePartition
	)
	{
		/**
		 * setup catheti adjacency information stack
		 */
		if (i_adjacentLeftEdgePartition)
		{
			io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(CEdgeComm_InformationAdjacentPartition_());
			CEdgeComm_InformationAdjacentPartition_ &adjacentTriangleInformation = io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.back();

			adjacentTriangleInformation.partitionTreeNode = i_adjacentLeftEdgePartition;
			adjacentTriangleInformation.uniqueId = i_adjacentLeftEdgePartition->uniqueId;
			adjacentTriangleInformation.commElements = cat_edge_comm_elements;
		}

		if (i_adjacentRightEdgePartition)
		{
			CEdgeComm_InformationAdjacentPartition_ *adjacentTriangleInformation;

			if (io_partitionTreeNode->triangleFactory.evenOdd == CTriangle_Enums::ODD)
			{
				io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.insert(
						io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin(),
						CEdgeComm_InformationAdjacentPartition_()
					);
				adjacentTriangleInformation = &(io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.front());
			}
			else
			{
				io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.push_back(
						CEdgeComm_InformationAdjacentPartition_()
					);
				adjacentTriangleInformation = &(io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.back());
			}

			adjacentTriangleInformation->partitionTreeNode = i_adjacentRightEdgePartition;
			adjacentTriangleInformation->uniqueId = i_adjacentRightEdgePartition->uniqueId;
			adjacentTriangleInformation->commElements = cat_edge_comm_elements;
		}

		/**
		 * setup hypotenuse adjacency information stack
		 */
		if (i_adjacentHypEdgePartition)
		{
			io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.push_back(CEdgeComm_InformationAdjacentPartition_());
			CEdgeComm_InformationAdjacentPartition_ &adjacentTriangleInformation = io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.back();

			adjacentTriangleInformation.partitionTreeNode = i_adjacentHypEdgePartition;
			adjacentTriangleInformation.uniqueId = i_adjacentHypEdgePartition->uniqueId;
			adjacentTriangleInformation.commElements = hyp_edge_comm_elements;
		}

//		std::cout << io_partitionTreeNode->cEdgeComm_InformationAdjacentPartitions << std::endl;
	}


public:
	/**
	 * setup a generic tree given by a base triangulation
	 */
	unsigned long long setup_GenericTree_From_BaseTriangulation(
			CDomain_BaseTriangulation_ &io_cDomain_RootTriangulation,	///< root triangulation

			int i_initialRecDepth,	///< initial depth of refinement
			int i_minDepth,			///< minimum relative refinement depth
			int i_maxDepth,			///< maximum relative refinement depth

			CGenericTreeNode_ *&o_rootNode		///< reference to root node
	)
	{
		// there must not exist any preallocated tree structure
		assert(o_rootNode == nullptr);

		cDomain_RootTriangulation = &io_cDomain_RootTriangulation;

		number_of_initial_root_partitions = cDomain_RootTriangulation->domainRootTriangles.size();

		initial_rec_depth = i_initialRecDepth;
		min_depth = i_minDepth;
		max_depth = i_maxDepth;


		/**
		 * check for valid initial depth
		 */
		assert(i_initialRecDepth >= 0);

		if ((i_initialRecDepth & 1) == 1)
		{
			if (!cDomain_RootTriangulation->oddInitialDepthFixApplied)
			{
				std::cerr << "An odd initial depth value is not allowed due to restrictions of hanging nodes along catheti and hypotenuse" << std::endl;
				assert(false);
			}
		}

		cat_edge_comm_elements = 1 << (i_initialRecDepth/2);
		hyp_edge_comm_elements = 1 << ((i_initialRecDepth+1)/2);


		/**
		 * determine max_unique_id_depth
		 */
		unsigned long long d = number_of_initial_root_partitions-1;
		max_unique_id_depth = 0;
		for (; d > 0; d >>= 1)
			max_unique_id_depth++;


		/*
		 * nothing to do for empty domain size
		 */
		if (number_of_initial_root_partitions == 0)
		{
			o_rootNode = new CGenericTreeNode_(nullptr, true);
			return 0;
		}


		/**
		 * assemble tree by allocating GenericTreeNodes and PartitionTreeNodes
		 */
		auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();


		/**
		 * setup the partition tree nodes for the root triangulation
		 *
		 * also setup the simulation partition handler and create the unique IDs
		 */
		unique_id_counter = 0;

		o_rootNode = _setup_GenericTreeAndPartitions(
				iter,		// start with first element
				nullptr,	// no parent node
				0			// start with an initial tree depth of zero
		);

		root_node = o_rootNode;

		for (	auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();
				iter != cDomain_RootTriangulation->domainRootTriangles.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ &cDomain_RootTriangle = *iter;

			CPartition_ *cPartition = cDomain_RootTriangle.cPartition_user_ptr;

			assert(cPartition != nullptr);

			_setup_BasePartitionAdjacencyInformation(
					cPartition,
					(cDomain_RootTriangle.adjacent_triangles.hyp_edge ? cDomain_RootTriangle.adjacent_triangles.hyp_edge->cPartition_user_ptr : nullptr),
					(cDomain_RootTriangle.adjacent_triangles.right_edge ? cDomain_RootTriangle.adjacent_triangles.right_edge->cPartition_user_ptr : nullptr),
					(cDomain_RootTriangle.adjacent_triangles.left_edge ? cDomain_RootTriangle.adjacent_triangles.left_edge->cPartition_user_ptr : nullptr)
			);

			cPartition->setup_SimulationPartitionHandler(true, nullptr);
		}

		_validate();

		cDomain_RootTriangulation = nullptr;

		// return number of triangles
		return (unsigned long long)(1 << i_maxDepth)*io_cDomain_RootTriangulation.domainRootTriangles.size();
	}








	/**
	 * start some validation process based on the properties stored in the triangleWithPartition classes
	 *
	 * return true, if the validation failed
	 */
private:
	bool _validate()
	{
		bool valid = true;

#if DEBUG
		for (	auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();
				iter != cDomain_RootTriangulation->domainRootTriangles.end();
				iter++
			)
		{
			CDomain_BaseTriangle_ &t = *iter;

			/**
			 * hypotenuse
			 */
			if (!t.adjacent_triangles.hyp_edge)
			{
				if (	t.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_BORDER &&
						t.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle_ &a = *t.adjacent_triangles.hyp_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * left edge
			 */
			if (!t.adjacent_triangles.left_edge)
			{
				if (	t.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BORDER &&
						t.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (LEFT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_PartitionHandler> &a = *t.adjacent_triangles.left_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * right edge
			 */
			if (!t.adjacent_triangles.right_edge)
			{
				if (	t.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BORDER &&
						t.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (RIGHT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_PartitionHandler> &a = *t.adjacent_triangles.right_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cout << "triangle factory for local triangle: " << std::endl;
					std::cout << t.triangleFactory << std::endl;
					std::cout << "triangle factory for adjacent triangle: " << std::endl;
					std::cout << a.triangleFactory << std::endl;
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}

			if (!valid)
			{
				std::cout << "TriangleWithPartition id: " << t.cPartition_user_ptr->uniqueId << std::endl;
				break;
			}
		}
#endif

		return valid;
	}

};


#endif /* CBASETRIANGULATION_TO_GENERICTREE_HPP_ */
