/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSTACK_HPP_
#define CSTACK_HPP_

#include <iostream>
#include <assert.h>

template <typename T>
class CStack;

#include "CStackReaderTopDown.hpp"
#include "CStackReaderBottomUp.hpp"


/**
 * \brief generic stack with pop/push accessors
 */
template <typename T>
class CStack
{
	friend class CStackReaderBottomUp<T>;
	friend class CStackReaderTopDown<T>;

private:
	/**
	 * pointer to first (bottom most) element of the stack
	 */
	T *stack_start_ptr;

	/**
	 * pointer to the next element on the stack (e. g. the element which is written in the next push() operation)
	 */
	T *stack_ptr;

	/**
	 * maximum number of elements which can be stores on the stack
	 */
	size_t size;



	/**
	 * constructor
	 */
public:
	inline CStack()
	{
		stack_start_ptr = NULL;
		stack_ptr = NULL;
		size = 0;
	}


	/**
	 * deconstructor
	 */
public:
	virtual ~CStack()
	{
		if (stack_start_ptr)
			delete[] stack_start_ptr;
	}


	/**
	 * setup a stack with the given size
	 */
public:
	inline CStack(size_t i_size)
	{
		stack_start_ptr = new T[i_size];
		stack_ptr = stack_start_ptr;
		size = i_size;
	}

	/**
	 * returns true if the stack is initialized - otherwise false
	 */
public:
	inline bool isInitialized()
	{
		return (stack_start_ptr != NULL);
	}

	/**
	 * delete the existing elements on the stack and resize it
	 */
public:
	inline void resize(size_t p_size)
	{
		if (stack_start_ptr)
				delete[] stack_start_ptr;

		stack_start_ptr = new T[p_size];
		stack_ptr = stack_start_ptr;
		size = p_size;
	}

	/**
	 * return the number of bytes allocated by the stack element storage
	 */
public:
	inline size_t getMemSize()	const
	{
		return getMaxNumberOfElements()*sizeof(T);
	}


	/**
	 * return the number of elements currently stored on the stack
	 */
public:
	inline size_t getNumberOfElementsOnStack()	const
	{
		return (int)(stack_ptr - stack_start_ptr);
	}



	/**
	 * return the maximum number of elements which can be stored on the stack
	 */
public:
	size_t getMaxNumberOfElements()	const
	{
		return size;
	}


	/**
	 * return the current stack pointer
	 */
public:
	T *getStackPtr() const
	{
		return stack_ptr;
	}
	/**
	 * swap with another stack.
	 *
	 * this is done by swapping data pointers with those of the other stack
	 */
public:
	void swap(CStack &stack)
	{
		std::swap(stack_start_ptr, stack.stack_start_ptr);
		std::swap(stack_ptr, stack.stack_ptr);
		std::swap(size, stack.size);
	}

	/**
	 * set the stack element counter and thus the pointer to point to element `i_elementCounter`
	 */
public:
	inline void setStackElementCounter(size_t i_elementCounter)
	{
		stack_ptr = stack_start_ptr+i_elementCounter;
	}

	/**
	 * increment the elementCounter of the stack relatively
	 */
public:
	inline void incStackElementCounter(size_t elementCounter)
	{
		stack_ptr += elementCounter;
	}

	/**
	 * store the element at position `i_pos` to `o_elementData` with a copy operation (avoid this when possible)
	 */
public:
	inline void getElementAtIndex(size_t i_pos, T &o_ElementData)	const
	{
		o_ElementData = stack_start_ptr[i_pos];
		assert(i_pos < size);
	}

	/**
	 * store the element at position `i_pos` to `o_elementData`
	 */
public:
	inline void setElementAtIndex(size_t i_pos, const T &i_ElementData)	const
	{
		stack_start_ptr[i_pos] = i_ElementData;
	}

	/**
	 * return a reference to the element at index i_pos
	 */
public:
	inline T &getElementAtIndex(size_t i_pos)	const
	{
		return stack_start_ptr[i_pos];
	}

public:
	inline T &operator[](size_t i_pos) const
	{
		return stack_start_ptr[i_pos];
	}

	/**
	 * return a pointer to the element at index i_pos
	 */
public:
	inline T *getElementPtrAtIndex(size_t i_pos)	const
	{
		return &(stack_start_ptr[i_pos]);
	}


	/**
	 * get element on the top of the stack without modifying the stack
	 */
public:
	inline T *getTopElementPtr()	const
	{
		return (stack_ptr-1);
	}



	/**
	 * return the number of elements for which are equal to the given comparison value `i_value`
	 */
public:
	inline size_t getStackElementCounterEquals(const T &i_value)	const
	{
		size_t ret_val = 0;

		for (T *ptr = stack_start_ptr; ptr != stack_ptr; ptr++)
			if (i_value == *ptr)
				ret_val++;

		return ret_val;
	}


	/**
	 * clear all stack elements by moving the stack pointer to the very beginning
	 */
public:
	inline void clear()
	{
		stack_ptr = stack_start_ptr;
	}


	/**
	 * return true, if the stack is empty
	 */
public:
	inline bool isEmpty()	const
	{
		return stack_ptr == stack_start_ptr;
	}

	/*************************************************************************************
	 * PUSH OPERATIONS
	 *************************************************************************************/
#if DEBUG
	#define ASSERT_AFTER_PUSH()	{assert(stack_ptr != stack_start_ptr+size+1);}
#else
	#define ASSERT_AFTER_PUSH()
#endif

	/**
	 * push the value given by v to the stack
	 */
	inline void push(const T &v)
	{
		*stack_ptr = v;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push a value to the stack which is to by 'v'
	 */
	inline void push_PtrValue(T *v)
	{
		*stack_ptr = *v;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * act like pushing some data on the stack but don't do any push operation.
	 * \return	pointer to new stack element
	 */
	inline T *fakePush_ReturnPtr()
	{
		return stack_ptr++;
	}

	/**
	 * act like pushing some data on the stack but don't do any push operation.
	 * \return	reference to new stack element
	 */
	inline T &fakePush_ReturnRef()
	{
		return *(stack_ptr++);
	}

	/**
	 * push 2 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push 3 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push 4 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push 5 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push 6 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4, T v5)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}

	/**
	 * push 7 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4, T v5, T v6)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;
		*stack_ptr = v6;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}
#undef ASSERT_AFTER_PUSH



	/*************************************************************************************
	 * POP OPERATIONS
	 *************************************************************************************/
#define ASSERT_AFTER_POP()	{assert(stack_ptr >= stack_start_ptr);}
	inline void pop(T &v)
	{
		v = *stack_ptr--;
		ASSERT_AFTER_POP();
	}


	inline T pop()
	{
		stack_ptr--;

		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T pop2()
	{
		stack_ptr -= 2;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T pop3()
	{
		stack_ptr -= 3;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T pop4()
	{
		stack_ptr -= 4;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T pop5()
	{
		stack_ptr -= 5;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T pop6()
	{
		stack_ptr -= 6;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	/*
	 * do a pop operation on stack and return the pointer instead
	 * of the stack element
	 */
	inline T* pop_ReturnPtr()
	{
		stack_ptr--;
		ASSERT_AFTER_POP();
		return stack_ptr;
	}


	inline T& pop_ReturnRef()
	{
		stack_ptr--;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}
#undef ASSERT_AFTER_POP


/*********************************************************************************
 * QUERIES
 *********************************************************************************/


	/**
	 * return true, whenever any element is not equal to the given value
	 */
	inline bool query_anyElementNotEqualTo_Scalar(T v)	const
	{
		size_t c = getNumberOfElementsOnStack();
		for (size_t i = 0; i < c; i++)
			if (stack_start_ptr[i] != v)
				return true;
		return false;
	}

	/**
	 * return true, whenever any element is not equal to the given value
	 */
	inline bool query_anyElementEqualTo_Scalar(T v)	const
	{
		size_t c = getNumberOfElementsOnStack();
		for (size_t i = 0; i < c; i++)
			if (stack_start_ptr[i] == v)
				return true;
		return false;
	}



/*********************************************************************************
 * STRUCTURE
 *********************************************************************************/
	/**
	 * structure specific method
	 *
	 * return the number of leaf elements assuming that the structure stack is stored for a quad
	 */
	inline bool structure_isValidQuad()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = stack_start_ptr; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+2 == zeros;
	}


	/**
	 * structure specific method
	 *
	 * return the number of leaf elements assuming that the structure stack is stored for a triangle
	 */
	inline bool structure_isValidTriangle()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = stack_start_ptr; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+1 == zeros;
	}

	/**
	 * return the number of triangles on the leafes.
	 *
	 * This can be computed by the total number of stored bits in the structure stack:
	 *
	 * When a triangle is refined, a '0' is replaced by '100'.
	 * Assuming that the domains border is 'triangle-like', we get the following series:
	 *   # triangles: 1 2 3
	 *       # zeros: 2 3 4
	 *        # ones: 1 2 3
	 * sizeof(stack): 3 5 7
	 *
	 * for this situation, the number of leaf triangles (zeros) would be:
	 *   (sizeof(stack)+1)/2
	 *
	 * for 2 triangles setting up a rectangular domain, we get:
	 *   (sizeof(stack)+2)/2
	 *
	 * since the first refinement of the rectangle isn't stored explicitly
	 * (as it doesn't make sense), a further fix has to be done:
	 *   #leaf triangles = (sizeof(stack)+3)/2
	 */
	inline unsigned int structure_getNumberOfTrianglesInQuad()	const
	{
		return (getNumberOfElementsOnStack()+3) / 2;
	}

	inline unsigned int structure_getNumberOfTrianglesInTriangle()	const
	{
		return (getNumberOfElementsOnStack()+1) / 2;
	}


	/**
	 * raw push a chunk of another stack to this stack
	 *
	 * the order of the elements on the stack is not reversed!
	 *
	 * |   |
	 * |   |
	 * |   |
	 * |   |____ stack_ptr -> pointing to next, empty stack element
	 * | d |
	 * | w |
	 * | d |
	 * |_a_|____ data -> pointing to bottom of stack
	 */
	void pushChunksFrom(
			CStack<T> &src_stack,	///< source stack to copy data from
			size_t src_start,			///< start element
			size_t elements			///< not included
	)
	{
		assert((signed long long)src_stack.getNumberOfElementsOnStack() - (signed long long)elements - (signed long long)src_start >= 0);
		assert(getNumberOfElementsOnStack() + elements <= size);

		memcpy(	stack_ptr,						// dst data
				src_stack.stack_ptr - elements - src_start,	// src pointer
				elements*sizeof(T));			// size of chunk

		stack_ptr += elements;
	}
};


inline
::std::ostream&
operator<<(::std::ostream &co, CStack<char> &stack)
{
	for (int i = stack.getNumberOfElementsOnStack()-1; i >= 0 ; i--)
		co << (int)stack.getElementAtIndex(i);
	co << "|";

	return co;
}


template <class T>
inline
::std::ostream&
operator<<(::std::ostream &co, CStack<T> &stack)
{
	for (int i = stack.getNumberOfElementsOnStack()-1; i >= 0 ; i--)
		co << stack.getElementAtIndex(i) << ", ";
	co << "|";

	return co;
}




#endif /* CSTACK_H_ */
