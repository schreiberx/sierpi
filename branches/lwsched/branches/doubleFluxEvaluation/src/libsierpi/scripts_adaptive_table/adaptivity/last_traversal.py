#! /usr/bin/python

#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

import sys
import lib_tools

#
# output the refinement information on the edge stacks
#
# TODO: some pop/push operations can be avoided.
# e.g. popping an adaptive information from an unrefined edge and pushing a 0
# to the same stack can be avoided
#
def output_edge_comm_adaptive_information(
										d,					# state of current traversal
										value_edge_hyp,		# values to be pushed to the stack for the corresponding edge
										value_edge_right,
										value_edge_left,
										left_right_edge_stack_modification = True
									):
	if d.config.adaptive_last_traversal_edge_comm_adaptive_information:
		if d.hyp == 'old':
			print """			comm_"""+d.hyp_stack+"""_edge_stack->pop();"""
		elif d.hyp == 'new':
			print """			comm_"""+d.hyp_stack+"""_edge_stack->push("""+str(value_edge_hyp)+""");"""

		if left_right_edge_stack_modification:
			if d.x_format:
				if d.edge_left == 'new':
					print """			comm_"""+d.edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				if d.edge_left == 'old':
					print """			comm_"""+d.edge_stack+"""_edge_stack->pop();"""
	
				if d.edge_right == 'new':
					print """			comm_"""+d.edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				if d.edge_right == 'old':
					print """			comm_"""+d.edge_stack+"""_edge_stack->pop();"""
			else:
				if d.edge_right == 'new':
					print """			comm_"""+d.edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				if d.edge_right == 'old':
					print """			comm_"""+d.edge_stack+"""_edge_stack->pop();"""
	
				if d.edge_left == 'new':
					print """			comm_"""+d.edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				if d.edge_left == 'old':
					print """			comm_"""+d.edge_stack+"""_edge_stack->pop();"""

#
# output code to update maxLeafTraversalDepth
#
def output_updateMaxDepth(displacement, d):
	if d.config.updateMaxDepth:
		print """
			if (maxLeafTraversalDepth < (depth+"""+str(displacement)+"""))
				maxLeafTraversalDepth = depth+"""+str(displacement)+""";
		"""



def code_generation(d):
#	d.method_postfix = "_refine_or_coarsen"
	d.method_postfix = ''

	# parameters for application (e. g. normals, etc.)
	application_parameters = []

	# parameters for recursive traversals
	rec_parameters1 = []
	rec_parameters2 = []

	# parameter for the function signature
	parameters = []

	if d.config.vertices:
		#rec_parameters.append("vx0, vy0, vx1, vy1, vx2, vy2")
		application_parameters.append("vx0, vy0, vx1, vy1, vx2, vy2")
		parameters.append("TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2")

		if d.x_format:
			rec_parameters1.append("vx2, vy2, vx0, vy0, mx, my")
			rec_parameters2.append("vx1, vy1, vx2, vy2, mx, my")
		else:
			rec_parameters1.append("vx1, vy1, vx2, vy2, mx, my")
			rec_parameters2.append("vx2, vy2, vx0, vy0, mx, my")

	if d.config.normals:
		# TODO: more normals for each edge
		normalStr = d.normals.getNormal(d.hyp_normal)
		application_parameters.append(str(normalStr[0])+", "+str(normalStr[1]))
		normalStr = d.normals.getNormal(d.edge_right_normal)
		application_parameters.append(str(normalStr[0])+", "+str(normalStr[1]))
		normalStr = d.normals.getNormal(d.edge_left_normal)
		application_parameters.append(str(normalStr[0])+", "+str(normalStr[1]))

	if d.config.depthParameter:
		application_parameters.append("depth")
		
	if d.config.depth:
		rec_parameters1.append("depth+1")
		rec_parameters2.append("depth+1")
		parameters.append("int depth")
		
	joined_rec_parameters1 = ", ".join(rec_parameters1)
	joined_rec_parameters_no_following_comma1 = joined_rec_parameters1
	if joined_rec_parameters1 != '':
		joined_rec_parameters1 += ', '
		
	joined_rec_parameters2 = ", ".join(rec_parameters2)
	joined_rec_parameters_no_following_comma2 = joined_rec_parameters2
	if joined_rec_parameters2 != '':
		joined_rec_parameters2 += ', '

	joined_application_parameters = ", ".join(application_parameters)
	joined_application_parameters_no_following_comma = joined_application_parameters
	if joined_application_parameters != '':
		joined_application_parameters += ', '

	joined_parameters = ", ".join(parameters)

	if d.direction == 'backward':

		print """
char """+d.sig_parent+d.method_postfix+"""("""+joined_parameters+""")
{"""
		if d.config.code_debug_output:
			print "	std::cout << std::endl;"
			print "	std::cout << \""+d.key+"["+d.method_postfix+"]\" << std::endl;"
			print "	std::cout << \"  "+d.hyp_stack+" stack: \" << *comm_"+d.hyp_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  "+d.edge_stack+" stack: \" << *comm_"+d.edge_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  structure_stack_in: \" << *structure_stack_in << std::endl;"
			print "	std::cout << \"  structure_stack_out: \" << *structure_stack_out << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_in: \" << *adaptivity_state_flag_stack_in << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_out: \" << *adaptivity_state_flag_stack_out << std::endl;"

		print """
	if (structure_stack_in->pop())
	{
"""
		if d.config.vertices:
			print "    TVertexScalar mx = (vx0+vx1)*(TVertexScalar)0.5;"
			print "    TVertexScalar my = (vy0+vy1)*(TVertexScalar)0.5;"
		#
		# if both leaf triangle are in state 'coarsen' (-6), they return 0 to signal
		# the parent node to start the coarsening operation
		#
		if d.config.code_debug_output:
			print """
		// both functions have to return the same boolean value
		// (0 is returned, if all 2 leaf triangles agreed to the coarsening.
		// also the other 2 leaf nodes needed to agree to the coarsening)"""

		print """
		"""+d.sig_child_first+d.method_postfix+"""("""+joined_rec_parameters_no_following_comma1+""");
		if ("""+d.sig_child_second+d.method_postfix+"""("""+joined_rec_parameters_no_following_comma2+""") != 0)
		{
			// no coarsening to do
			structure_stack_out->push(1);
			return 1;
		}

		// coarsening
		structure_stack_out->push(0);"""

		output_updateMaxDepth(0, d)

		#
		# at this traversal point at the parent node of both children which have to be coarsened,
		# we have to create the triangle element-data based upon the 2 elements stored in the
		# incoming element data stack.
		#
		if d.config.elementData:
			print "		TElementData *element = element_data_stack_out->fakePush_ReturnPtr();"

			if d.x_format:
				print "		TElementData *right_element = element_fifo_in.getPreviousDataPtr();"
				print "		TElementData *left_element = element_fifo_in.getPreviousPreviousDataPtr();"
			else:
				print "		TElementData *left_element = element_fifo_in.getPreviousDataPtr();"
				print "		TElementData *right_element = element_fifo_in.getPreviousPreviousDataPtr();"
				
			print "		kernelClass.coarsen("+joined_application_parameters+"left_element, right_element, element);"

		print """		return 1;
	}
"""

			
		if d.config.elementData:
			print """	// get pointer to next element
	TElementData *new_left_left_element;
	TElementData *new_left_right_element;
	TElementData *new_right_left_element;
	TElementData *new_right_right_element;
	TElementData *element = element_fifo_in.getNextDataPtr();"""

		print """
	unsigned char state = adaptivity_state_flag_stack_in->pop();
	switch(state)
	{"""
		# CASE 0 - NOTHING TO DO
		print	"	case 0:"
		print	"		structure_stack_out->push(0);"

		if d.config.elementData:
			print	"		element_data_stack_out->push_PtrValue(element);"

		output_edge_comm_adaptive_information(d, 0, 0, 0)
		output_updateMaxDepth(0, d)

		print	"		return 1;"
		print	""

		# CASE 1 - ERROR
		print	"	case 1:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""

		# CASE 2 - COARSENING
		print	"	case 2:	// coarsening along edge_right and edge_left => nothing to do"
		output_edge_comm_adaptive_information(d, 0, -1, -1)
		# max depth if fixed by parent call 
		print	"		return 0;"
		print	""

		# CASE 3 - ERROR
		print	"	case 3:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""

		# CASE 4 - HYP REFINEMENT
		print	"	case 4:"
		print	"		structure_stack_out->push(0, 0, 1);"

		if d.config.elementData:
			if d.x_format:
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
			else:
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"

			print "		kernelClass.refine_l_r("+joined_application_parameters+"element, new_left_left_element, new_right_right_element);"

		output_edge_comm_adaptive_information(d, 1, 0, 0)
		output_updateMaxDepth(1, d)
		print "		return 1;"
		print	""

		# CASE 5 - HYP+LEFT REFINEMENT
		print	"	case 5:"
		print	"		structure_stack_out->push("+("0, 0, 0, 1, 1" if not d.x_format else "0, 0, 1, 0, 1")+");"

		if d.config.elementData:
			if d.x_format:
							# 00101f
				print "		new_left_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
			else:			# 00011b
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_ReturnPtr();"

			print "		kernelClass.refine_ll_r("+joined_application_parameters+"element, new_left_left_element, new_left_right_element, new_right_right_element);"

		output_edge_comm_adaptive_information(d, 1, 0, 1)
		output_updateMaxDepth(2, d)

		print "			return 1;"
		print	""


		# CASE 6 - HYP+RIGHT REFINEMENT
		print	"	case 6:"
		print	"		structure_stack_out->push("+("0, 0, 0, 1, 1" if d.x_format else "0, 0, 1, 0, 1")+");"

		if d.config.elementData:
			if d.x_format:
					# 00011b
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_ReturnPtr();"

			else:	# 00101f
				print "		new_right_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"

			print "		kernelClass.refine_l_rr("+joined_application_parameters+"element, new_left_left_element, new_right_left_element, new_right_right_element);"

		output_edge_comm_adaptive_information(d, 1, 1, 0)
		output_updateMaxDepth(2, d)
		print "		return 1;"
		print	""

		# CASE 6 - HYP+RIGHT+LEFT REFINEMENT
		print	"	case 7:"
		print	"		structure_stack_out->push(0, 0, 1, 0, 0, 1, 1);"

		if d.config.elementData:
			if d.x_format:
				print "		new_left_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_ReturnPtr();"
			else:
				print "		new_right_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_ReturnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_ReturnPtr();"

			print "		kernelClass.refine_ll_rr("+joined_application_parameters+"element, new_left_left_element, new_left_right_element, new_right_left_element, new_right_right_element);"

		output_edge_comm_adaptive_information(d, 1, 1, 1)
		output_updateMaxDepth(2, d)

		print	"		return 1;"
		print	""

		# CASE x - ERROR
		print	"	default:"
		print	"		assert(false);"
		print	"		return -1;"
		
		print """	}
}"""
