/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "CSpecialized_Tsunami_Setup_Column.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_Vertices_DepthLimiter_ElementData.hpp"
#include "../kernels/modifiers/CSetup_Column.hpp"

namespace sierpi
{
namespace travs
{
class CSpecialized_Tsunami_Setup_Column_Private	:
	public CAdaptiveConformingGrid_Vertices_DepthLimiter_ElementData<
		sierpi::kernels::CSetup_Column,	// setup of column
		CTsuanmiSimulationStacks		// tsunami element data
	>
{
};


CSpecialized_Tsunami_Setup_Column::CSpecialized_Tsunami_Setup_Column()
{
	generic_traversator = new CSpecialized_Tsunami_Setup_Column_Private;
}


CSpecialized_Tsunami_Setup_Column::~CSpecialized_Tsunami_Setup_Column()
{
	delete generic_traversator;
}



void CSpecialized_Tsunami_Setup_Column::setup_RootTraversator(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_RootTraversator(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}



void CSpecialized_Tsunami_Setup_Column::setup_KernelClass(
		TTsunamiDataScalar p_x,
		TTsunamiDataScalar p_y,
		TTsunamiDataScalar p_radius,
		CTsunamiElementData *p_inner_elementData,
		CTsunamiEdgeData *p_inner_elementEdgeData,
		CTsunamiElementData *p_outer_elementData,
		CTsunamiEdgeData *p_outer_elementEdgeData
)
{
	generic_traversator->kernelClass.setup_RootPartition(
			p_x,
			p_y,
			p_radius,
			p_inner_elementData,
			p_inner_elementEdgeData,
			p_outer_elementData,
			p_outer_elementEdgeData
		);
}


bool CSpecialized_Tsunami_Setup_Column::actionFirstTraversal(CTsuanmiSimulationStacks *cStacks)
{
	return generic_traversator->firstTraversal.action(cStacks);
}

bool CSpecialized_Tsunami_Setup_Column::actionMiddleTraversals_Parallel(CTsuanmiSimulationStacks *cStacks)
{
	return generic_traversator->middleTraversals.action_Parallel(cStacks);
}



bool CSpecialized_Tsunami_Setup_Column::actionMiddleTraversals_Serial(CTsuanmiSimulationStacks *cStacks)
{
	return generic_traversator->middleTraversals.action_Serial(cStacks);
}


void CSpecialized_Tsunami_Setup_Column::actionLastTraversal_Parallel(
		CTsuanmiSimulationStacks *cStacks,
		CPartition_SplitJoinInformation &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_Tsunami_Setup_Column::actionLastTraversal_Serial(CTsuanmiSimulationStacks *cStacks)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_Tsunami_Setup_Column::setup_sfcMethods(CTriangle_Factory &p_triangleFactory)
{
	generic_traversator->setup_sfcMethods(p_triangleFactory);
}


/**
 * setup the initial partition traversal for the given factory
 */
void CSpecialized_Tsunami_Setup_Column::setup_Partition(
		CSpecialized_Tsunami_Setup_Column &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Partition(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->kernelClass.setup_WithKernel(
			parent.generic_traversator->kernelClass
		);
}

}
}
