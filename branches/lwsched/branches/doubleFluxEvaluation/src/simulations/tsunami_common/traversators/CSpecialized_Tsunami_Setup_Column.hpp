/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_


#include "../../tsunami_common/types/CTsunamiTypes.hpp"
#include "libsierpi/stacks/CFBStacks.hpp"
#include "libsierpi/parallelization/CPartition_SplitJoinInformation.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Tsunami_Setup_Column
{
private:
	class CSpecialized_Tsunami_Setup_Column_Private *generic_traversator;

public:
	CSpecialized_Tsunami_Setup_Column();

	virtual ~CSpecialized_Tsunami_Setup_Column();

	void setup_Stacks(
			CFBStacks<char> *i_structure_stacks,
			CFBStacks<CTsunamiElementData> *i_element_data_stacks,
			CStack<char> *i_adaptive_comm_left_edge_stack,
			CStack<char> *i_adaptive_comm_right_edge_stack,
			CStack<char> *i_adaptive_comm_exchange_left_edge_stack,
			CStack<char> *i_adaptive_comm_exchange_right_edge_stack,
			CFBStacks<unsigned char> *i_adaptivity_state_flag_stacks,
			CPartition_SplitJoinInformation *i_splitJoinInformation
	);



	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CTsuanmiSimulationStacks *cStacks);



	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CTsuanmiSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CTsuanmiSimulationStacks *cStacks);



	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CTsuanmiSimulationStacks *cStacks);



	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CTsuanmiSimulationStacks *cStacks,
			CPartition_SplitJoinInformation &p_splitJoinInformation
	);



	/**
	 * setup the initial root domain partition
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);



	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_RootTraversator(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);



	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			TTsunamiDataScalar p_x,
			TTsunamiDataScalar p_y,
			TTsunamiDataScalar p_radius,
			CTsunamiElementData *p_inner_elementData,
			CTsunamiEdgeData *p_inner_elementEdgeData,
			CTsunamiElementData *p_outer_elementData,
			CTsunamiEdgeData *p_outer_elementEdgeData
	);



	/**
	 * setup the child partition based on it's parent traversator information and the child's factory
	 */
	void setup_Partition(
			CSpecialized_Tsunami_Setup_Column &parent,
			CTriangle_Factory &i_triangleFactory
		);
};

}
}

#endif
