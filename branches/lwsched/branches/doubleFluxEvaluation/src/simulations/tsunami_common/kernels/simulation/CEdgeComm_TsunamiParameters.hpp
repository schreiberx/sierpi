/*
 * CEdgeComm_Tsunami_Config.hpp
 *
 *  Created on: Jan 24, 2012
 *      Author: schreibm
 */

#ifndef CEDGECOMM_TSUNAMI_PARAMETERS_HPP_
#define CEDGECOMM_TSUNAMI_PARAMETERS_HPP_


class CEdgeComm_TsunamiParameters
{
public:
	typedef TTsunamiDataScalar T;

	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar square_side_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiEdgeData boundary_dirichlet;

	/**
	 * available boundary conditions
	 */
	enum EBoundaryCondition
	{
		BOUNDARY_CONDITION_DIRICHLET,
		BOUNDARY_CONDITION_VELOCITY_DAMPING,
		BOUNDARY_CONDITION_BOUNCE_BACK
	};


	/**
	 * set-up boundary condition
	 */
	EBoundaryCondition eBoundaryCondition;

	/**
	 * damping factor when velocity damping boundary condition is used
	 */
	TTsunamiDataScalar eBoundaryConditionVelocityDampingFactor;


	/******************************************************
	 * CFL condition stuff
	 *
	 * TODO: the CFL condition also has to be built-in into the
	 * adaptive traversals (due to refine operations)
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;

	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;



	/**
	 * setup with default values
	 */
	CEdgeComm_TsunamiParameters()	:
		eBoundaryCondition(BOUNDARY_CONDITION_DIRICHLET),
//		eBoundaryCondition(BOUNDARY_CONDITION_VELOCITY_DAMPING),
//		eBoundaryCondition(BOUNDARY_CONDITION_BOUNCE_BACK),
		eBoundaryConditionVelocityDampingFactor(0.9)
	{
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
		timestep_size = -CMath::numeric_inf<T>();
		square_side_length = -CMath::numeric_inf<T>();
		gravitational_constant = -CMath::numeric_inf<T>();
	}


	/**
	 * return the reduce value for the traversator
	 */
	inline TReduceValue getReduceValue()
	{
		return cfl_domain_size_div_max_wave_speed;
	}

	/**
	 * return the set-up timestep size
	 */
	inline TTsunamiDataScalar getTimestepSize()
	{
		return timestep_size;
	}




	/**
	 * \brief set the timestep size
	 */
	inline void setTimestepSize(T i_timestep_size)
	{
		timestep_size = i_timestep_size;
	}


	/**
	 * \brief set the gravitation
	 *
	 * WARNING: this value is not always used by the fluxes in the current state!
	 */
	inline void setGravitationalConstant(T i_gravitational_constant)
	{
		gravitational_constant = i_gravitational_constant;
	}



	/**
	 * \brief set the length of a cathetii
	 */
	inline void setSquareSideLength(T i_square_side_length)
	{
		square_side_length = i_square_side_length;
	}



	/**
	 * setup parameter for dirichlet boundary condition
	 */
	void setBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		boundary_dirichlet = *p_boundary_dirichlet;
	}



	/**
	 * \brief set all parameters for simulation
	 */
	inline void setParameters(
			T i_timestep_size,					///< timestep size
			T i_square_side_length,				///< length of a square (a single catheti)
			T i_gravitational_constant = 9.81	///< gravitational constant
	)
	{
		timestep_size = i_timestep_size;
		gravitational_constant = i_gravitational_constant;
		square_side_length = i_square_side_length;
	}



	/**
	 * \brief update the CFL condition with 3 given max wavespeeds
	 */
	inline void updateCFLCondition3(
			T i_max_wave_speed1,
			T i_max_wave_speed2,
			T i_max_wave_speed3,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2, i_max_wave_speed3);

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*square_side_length;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}



	/**
	 * \brief update the CFL condition with 2 given max wavespeeds
	 */
	inline void updateCFLCondition2(
			T i_max_wave_speed1,
			T i_max_wave_speed2,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2);

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*square_side_length;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}





	/**
	 * \brief update the CFL condition with 2 given max wavespeeds
	 */
	inline void updateCFLCondition1(
			T i_max_wave_speed1,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T cat_length = getUnitCathetusLengthForDepth(i_depth)*square_side_length;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/i_max_wave_speed1, cfl_domain_size_div_max_wave_speed);
	}

};



#endif /* CEDGECOMM_TSUNAMI_CONFIG_HPP_ */
