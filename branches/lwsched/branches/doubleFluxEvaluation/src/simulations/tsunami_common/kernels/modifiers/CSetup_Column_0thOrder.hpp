/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_Vertices_DepthLimiter_ElementData.hpp"



namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_Column_0thOrder
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_Vertices_DepthLimiter_ElementData<CSetup_Column_0thOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;

	/*
	 * refinement/coarsening parameters
	 */
	TTsunamiDataScalar refine_threshold;
	TTsunamiDataScalar coarsen_threshold;

	// square sidelength of a cathetus
	TTsunamiDataScalar cathetus_side_length;

	// edge value with which the node is setup in case that it's inside the column
	TElementData *inner_elementData;
	TEdgeData *inner_elementEdgeData;

	TElementData *outer_elementData;
	TEdgeData *outer_elementEdgeData;

	// center of column
	TTsunamiVertexScalar columnCenterX, columnCenterY;

	// radius of column
	TTsunamiVertexScalar columnRadius;
	TTsunamiVertexScalar columnRadiusSquared;



	CSetup_Column_0thOrder()
	{
		columnCenterX = 0;
		columnCenterY = 0;
		columnRadius = 0;
	}


	inline TTsunamiVertexScalar squaredDistToCenter(
			TVertexScalar px, TVertexScalar py
	)
	{
		TVertexScalar x = columnCenterX-px;
		TVertexScalar y = columnCenterY-py;
		return x*x+y*y;
	}


	inline bool insideColumn(
			TVertexScalar px, TVertexScalar py
	)
	{
		return squaredDistToCenter(px, py) < columnRadiusSquared;
	}


	inline bool should_refine(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *i_elementData
	)
	{
		int counter = 0;
		counter += (int)insideColumn(vx1, vy1);
		counter += (int)insideColumn(vx2, vy2);

		if (counter == 1)
		{
			overwriteEdgeDataWhenMidpointInsideOfColumn_EdgeCornerVertices(
					vx1, vy1,
					vx2, vy2,
					vx3, vy3,
					i_elementData
				);
			return true;
		}

		counter += (int)insideColumn(vx3, vy3);

		if (counter == 3)
		{
			*i_elementData = *inner_elementData;
			return false;
		}

		if (counter == 1 || counter == 2)
		{
			overwriteEdgeDataWhenMidpointInsideOfColumn_EdgeCornerVertices(
					vx1, vy1,
					vx2, vy2,
					vx3, vy3,
					i_elementData
				);
			return true;
		}

		return false;
	}


	inline bool should_coarsen(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *element
	)
	{
		/**
		 * we use the coarsening callback to update the column inner elements data (even if element data should not be accessed)
		 */

		if (!insideColumn(vx1, vy1))
			return false;

		if (!insideColumn(vx2, vy2))
			return false;

		if (!insideColumn(vx3, vy3))
			return false;

		*element = *inner_elementData;
		return false;
	}


	void traversal_pre_hook()
	{
	}

	void traversal_post_hook()
	{
	}


	void setup_RootPartition(
			TTsunamiVertexScalar p_columnCenterX,
			TTsunamiVertexScalar p_columnCenterY,
			TTsunamiVertexScalar p_columnRadius,

			TElementData *p_inner_elementData,
			TEdgeData *p_inner_elementEdgeData,

			TElementData *p_outer_elementData,
			TEdgeData *p_outer_elementEdgeData
	)
	{
		columnCenterX = p_columnCenterX;
		columnCenterY = p_columnCenterY;
		columnRadius = p_columnRadius;
		columnRadiusSquared = columnRadius*columnRadius;

		inner_elementData = p_inner_elementData;
		inner_elementEdgeData = p_inner_elementEdgeData;

		outer_elementData = p_outer_elementData;
		outer_elementEdgeData = p_outer_elementEdgeData;
	}


	void setup_WithKernel(
			CSetup_Column_0thOrder<TElementData, TSimulationStacks> &parent
	)
	{
		columnCenterX = parent.columnCenterX;
		columnCenterY = parent.columnCenterY;
		columnRadius = parent.columnRadius;
		columnRadiusSquared = parent.columnRadiusSquared;

		inner_elementData = parent.inner_elementData;
		inner_elementEdgeData = parent.inner_elementEdgeData;

		outer_elementData = parent.outer_elementData;
		outer_elementEdgeData = parent.outer_elementEdgeData;
	}


	inline void overwriteElementDataWhenInColumn(
			TVertexScalar px, TVertexScalar py,
			CTsunamiEdgeData *edgeData
	)
	{
		if (insideColumn(px, py))
			*edgeData = *inner_elementEdgeData;
		else
			*edgeData = *outer_elementEdgeData;
	}




	/**
	 * overwrite edge data of an element when the edge midpoint is within the column
	 */
	inline void overwriteElementDataWhenMidpointInsideOfColumn(
			TVertexScalar mid_x, TVertexScalar mid_y,
			CTsunamiElementData *elementData
			)
	{
		overwriteElementDataWhenInColumn(mid_x, mid_y, &(elementData->dofs));
	}



	/**
	 * overwrite edge data of an element when the edge midpoint is within the column
	 */
	inline void overwriteEdgeDataWhenMidpointInsideOfColumn_EdgeCornerVertices(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *elementData
			)
	{
		overwriteElementDataWhenMidpointInsideOfColumn(
				(vx1+vx2+vx3)*(TVertexScalar)(1.0/3.0),
				(vy1+vy2+vy3)*(TVertexScalar)(1.0/3.0),

				elementData
			);
	}


	/**
	 * COARSEN / REFINE
	 */
	inline void refine_l_r(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *element,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element
	)
	{
		*left_element = *element;
		*right_element = *element;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		left_element->validation.setupLeftElementFromParent(&element->validation);
		right_element->validation.setupRightElementFromParent(&element->validation);
#endif
	}



	inline void refine_ll_r(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *element,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
		/*
		 * 1st level split
		 */
		refine_l_r(
				vx1, vy1,
				vx2, vy2,
				vx3, vy3,
				element, left_right_element, right_element
			);

		/*
		 * resplit left triangle
		 */
		TVertexScalar hyp_mid_edge_x = (vx1+vx2)*(TVertexScalar)0.5;
		TVertexScalar hyp_mid_edge_y = (vy1+vy2)*(TVertexScalar)0.5;

		refine_l_r(
				vx1, vy1,
				hyp_mid_edge_x, hyp_mid_edge_y,
				vx3, vy3,
				left_right_element, left_left_element, left_right_element
			);
	}



	inline void refine_l_rr(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *element,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
		refine_l_r(
				vx1, vy1,
				vx2, vy2,
				vx3, vy3,
				element, left_element, right_right_element
			);


		TVertexScalar hyp_mid_edge_x = (vx1+vx2)*(TVertexScalar)0.5;
		TVertexScalar hyp_mid_edge_y = (vy1+vy2)*(TVertexScalar)0.5;

		refine_l_r(
				vx2, vy2,
				vx3, vy3,
				hyp_mid_edge_x, hyp_mid_edge_y,
				right_right_element, right_left_element, right_right_element
			);
	}



	inline void refine_ll_rr(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CTsunamiElementData *element,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
		refine_l_r(
				vx1, vy1,
				vx2, vy2,
				vx3, vy3,
				element, left_right_element, right_right_element
			);

		TVertexScalar hyp_mid_edge_x = (vx1+vx2)*(TVertexScalar)0.5;
		TVertexScalar hyp_mid_edge_y = (vy1+vy2)*(TVertexScalar)0.5;

		refine_l_r(
				vx3, vy3,
				vx1, vy1,
				hyp_mid_edge_x, hyp_mid_edge_y,
				left_right_element, left_left_element, left_right_element
			);

		refine_l_r(
				vx2, vy2,
				vx3, vy3,
				hyp_mid_edge_x, hyp_mid_edge_y,
				right_right_element, right_left_element, right_right_element
			);
	}



	inline void coarsen(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element,
			CTsunamiElementData *coarsed_element
	)
	{
		// there was never an agreement to coarsening
		assert(false);
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
