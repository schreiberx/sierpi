/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 5, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COPENGL_ELEMENT_SPLATS_ROOT_TSUNAMI_HPP_
#define COPENGL_ELEMENT_SPLATS_ROOT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libmath/CGlSlMath.hpp"
#include "libmath/CRegion2D.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlProgram.hpp"
#include "libgl/core/CGlViewport.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "lib/CError.hpp"

#include "libgl/tools/CGlNormalizeByGreenChannel.hpp"

// Has to be a multiple of 3
#define OPENGL_ELEMENT_SPLATS_MAX_VERTICES_IN_BUFFER	128*3


template <typename TVertexScalar>
class COpenGL_Element_Splats_Root_Tsunami
{
	/**
	 * texture storing the rendered splats
	 */
	CGlTexture cGlTexture;

	/**
	 * helper stuff
	 */
	CGlDrawTexturedQuad cGlDrawTexturedQuad;
	GLSL::mat4 postproc_matrix;

	/*
	 * variables related to rendering splats
	 */
	/// FBO to render the splats to a texture
	CGlFbo cGlFbo;

	/// viewport for FBO
	CGlViewport cGlViewport;

	/// pvm matrix
	GLSL::mat4 render_splats_pvm_matrix;

	/// program for rendering splats
	CGlProgram cGlProgramRenderSplats;

	/// uniform to access pvm matrix for splats rendering vertex shader
	CGlUniform uniform_pvm_matrix;

	/// scalar for splat size
	CGlUniform uniform_splat_size_scalar;

	/// translate surface height
	CGlUniform uniform_surface_height_translate;

	/// scale surface height (after translation)
	CGlUniform uniform_surface_height_scale;

	CGlNormalizeByGreenChannel cGlNormalizeByGreenChannel;

	CGlVertexArrayObject vao;
	CGlBuffer buffer;

public:
	CError error;

	COpenGL_Element_Splats_Root_Tsunami()
	{
		setupTexture(256, 256);
		postproc_matrix.loadIdentity();

		cGlProgramRenderSplats.initVertFragShadersFromDirectory("render_splats");
		CError_AppendReturn(cGlProgramRenderSplats);

		cGlProgramRenderSplats.link();
		if (cGlProgramRenderSplats.error())
		{
			error << "info Log: linking: " << cGlProgramRenderSplats.error.getString() << std::endl;
			return;
		}

		cGlProgramRenderSplats.setupUniform(uniform_pvm_matrix, "pvm_matrix");
		cGlProgramRenderSplats.setupUniform(uniform_splat_size_scalar, "splat_size_scalar");
		cGlProgramRenderSplats.setupUniform(uniform_surface_height_scale, "surface_height_scale");
		cGlProgramRenderSplats.setupUniform(uniform_surface_height_translate, "surface_height_translate");

		CGlErrorCheck();

		cGlProgramRenderSplats.use();
		uniform_splat_size_scalar.set1f(1.0f);
		uniform_surface_height_scale.set1f(50.0);
		uniform_surface_height_scale.set1f(1.0);
		cGlProgramRenderSplats.disable();

		CGlErrorCheck();

		vao.bind();
			buffer.bind();
			buffer.resize(OPENGL_ELEMENT_SPLATS_MAX_VERTICES_IN_BUFFER*sizeof(GLfloat)*4);
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);
		vao.unbind();

		CGlErrorCheck();
	}


	void setSplatSizeScalar(
			float p_splat_size_scalar
	)
	{
		cGlProgramRenderSplats.use();
		uniform_splat_size_scalar.set1f(p_splat_size_scalar);
		cGlProgramRenderSplats.disable();
	}

	void setupTranslationAndScalingParameters(
			float p_surface_height_translate,
			float p_surface_height_scale
	)
	{
		cGlProgramRenderSplats.use();
		uniform_surface_height_translate.set1f(p_surface_height_translate);
		uniform_surface_height_scale.set1f(p_surface_height_scale);
		cGlProgramRenderSplats.disable();
	}


	/**
	 * resize the texture for drawing the splats
	 */
	void resizeTexture(GLsizei p_width, GLsizei p_height)
	{
		cGlTexture.bind();
		cGlTexture.resize(p_width, p_height);
		cGlTexture.unbind();

		cGlNormalizeByGreenChannel.resizeTexture(p_width, p_height);
	}


	/**
	 * setup the texture size
	 */
	void setupTexture(
			GLint p_width,
			GLint p_height
	)
	{
		cGlTexture.bind();
		/**
		 * IMPORTANT: we can use GL_RG16F right here since we do the translation + scaling of the surface in the splat rendering vertex shader
		 *
		 * otherwise, due to the conversions to the GL_RG16F storage format and thus a loss of accuracy and due to the division operation, we
		 * have to use GL_RG32F here.
		 */
		cGlTexture.setTextureParameters(GL_TEXTURE_2D, GL_RG8, GL_RGBA, GL_FLOAT);
		cGlTexture.unbind();

		resizeTexture(p_width, p_height);

		cGlFbo.bind();
		cGlFbo.bindTexture(cGlTexture);
		cGlFbo.unbind();

		CError_AppendReturnThis(cGlNormalizeByGreenChannel);

		CGlErrorCheck();
	}



	/**
	 * setup the render region
	 */
	void setupRegion(
			CRegion2D<TVertexScalar> &p_cRegion2D
	)
	{
		GLfloat mid_x = (p_cRegion2D.x_max + p_cRegion2D.x_min)*0.5f;
		GLfloat mid_z = (p_cRegion2D.y_max + p_cRegion2D.y_min)*0.5f;

		GLfloat size_x = p_cRegion2D.x_max - p_cRegion2D.x_min;
		GLfloat size_z = p_cRegion2D.y_max - p_cRegion2D.y_min;

		postproc_matrix =
				GLSL::scale(size_x*0.5f, 1.0f, size_z*0.5f)*
				GLSL::translate(mid_x, 0, -mid_z)*
				GLSL::rotate(-90, 1, 0, 0);

		render_splats_pvm_matrix =
				GLSL::ortho(p_cRegion2D.x_min, p_cRegion2D.x_max, p_cRegion2D.y_min, p_cRegion2D.y_max, -1.0f, 1.0f);
	}



	/**
	 * initialize rendering the splats
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRenderSplats(
			GLSL::mat4 &p_pvm_matrix
	)
	{
		cGlFbo.bind();
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		cGlViewport.saveState();
		cGlViewport.setSize(cGlTexture.width, cGlTexture.height);

		cGlProgramRenderSplats.use();
		uniform_pvm_matrix.set(render_splats_pvm_matrix);

		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);

		glEnable(GL_PROGRAM_POINT_SIZE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		glBlendEquation(GL_FUNC_ADD);

		vao.bind();
		// also bind the buffer
		buffer.bind();
	}

	inline void renderOpenGLVertexArray(
			const GLfloat *p_vertex_attributes,	///< pointer to vertex attributes (4 GLfloat components)
			const size_t p_count				///< number of vertices
	)
	{
		buffer.subData(0, p_count*(4*sizeof(GLfloat)), p_vertex_attributes);
		glDrawArrays(GL_POINTS, 0, p_count);
	}


	inline void shutdownRenderSplats()
	{
		vao.unbind();

		glDisable(GL_BLEND);

		glDisable(GL_PROGRAM_POINT_SIZE);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		cGlProgramRenderSplats.disable();
		cGlViewport.restoreState();
		cGlFbo.unbind();
	}


	inline void renderWaterSurface(
			GLSL::mat4 &p_pvm_matrix
	)
	{
		cGlNormalizeByGreenChannel.normalizeByGreenChannel(cGlTexture);

#if 1
		cGlDrawTexturedQuad.render(p_pvm_matrix*postproc_matrix, cGlNormalizeByGreenChannel.cGlTexture);
#else
		cGlDrawTexturedQuad.render(p_pvm_matrix*postproc_matrix, cGlTexture);
#endif
	}
};



#endif
