/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CAdaptive_Tsunami_1stOrder_CoarsenRefine.hpp
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CADAPTIVE_TSUNAMI_1STORDER_COARSENREFINE_HPP_
#define CADAPTIVE_TSUNAMI_1STORDER_COARSENREFINE_HPP_

#include "config.h"
#include "../../types/CTsunamiTypes.hpp"

namespace sierpi
{
namespace kernels
{


class CAdaptive_Tsunami_1stOrder_CoarsenRefine
{

public:
	inline void refine_l_r(
			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,
			int depth,
			CTsunamiElementData *element,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element
	)
	{
		// left element
		left_element->hyp_edge = element->left_edge;
		left_element->right_edge = element->hyp_edge;
		left_element->left_edge.h = (element->left_edge.h+element->right_edge.h)*(TTsunamiDataScalar)0.5;
		left_element->left_edge.qx = (element->left_edge.qx+element->right_edge.qx)*(TTsunamiDataScalar)0.5;
		left_element->left_edge.qy = (element->left_edge.qy+element->right_edge.qy)*(TTsunamiDataScalar)0.5;
		left_element->left_edge.b = (element->left_edge.b+element->right_edge.b)*(TTsunamiDataScalar)0.5;
#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		left_element->refine = false;
		left_element->coarsen = false;
#endif

		// right element
		right_element->hyp_edge = element->right_edge;
		right_element->right_edge = left_element->left_edge;
		right_element->left_edge = element->hyp_edge;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		right_element->refine = false;
		right_element->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		element->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				depth,
				&left_element->validation,
				&right_element->validation
			);
#endif
	}



	inline void refine_ll_r(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,
			int depth,
			CTsunamiElementData *element,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
		// LEFT + RIGHT
		refine_l_r(	normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,
					depth,
					element, left_right_element, right_element
				);

		// LEFT childs (LEFT + RIGHT)
		refine_l_r(	normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
					depth+1,
					left_right_element, left_left_element, left_right_element
				);
	}



	inline void refine_l_rr(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,
			int depth,
			CTsunamiElementData *element,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
		// LEFT + RIGHT
		refine_l_r(	normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,
					depth,
					element, left_element, right_right_element);

		// RIGHT childs (LEFT + RIGHT)
		refine_l_r(	normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					depth+1,
					right_right_element, right_left_element, right_right_element);
	}



	inline void refine_ll_rr(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,
			int depth,
			CTsunamiElementData *element,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
		// RIGHT + LEFT
		refine_l_r(	normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,
					depth,
					element, left_right_element, right_right_element);

		// LEFT childs (LEFT + RIGHT)
		refine_l_r(	normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
					depth+1,
					left_right_element, left_left_element, left_right_element);

		// LEFT childs (LEFT + RIGHT)
		refine_l_r(	normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					depth+1,
					right_right_element, right_left_element, right_right_element);
	}


	inline void coarsen(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element,
			CTsunamiElementData *coarsed_element
	)
	{
		coarsed_element->left_edge = left_element->hyp_edge;
		coarsed_element->right_edge = right_element->hyp_edge;
		coarsed_element->hyp_edge.h = (left_element->right_edge.h+right_element->left_edge.h)*(TTsunamiDataScalar)0.5;
		coarsed_element->hyp_edge.qx = (left_element->right_edge.qx+right_element->left_edge.qx)*(TTsunamiDataScalar)0.5;
		coarsed_element->hyp_edge.qy = (left_element->right_edge.qy+right_element->left_edge.qy)*(TTsunamiDataScalar)0.5;
		coarsed_element->hyp_edge.b = (left_element->right_edge.b+right_element->left_edge.b)*(TTsunamiDataScalar)0.5;
#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		coarsed_element->refine = false;
		coarsed_element->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		coarsed_element->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				depth,
				&left_element->validation,
				&right_element->validation
		);
#endif
	}

};


}
}

#endif /* CADAPTIVE_TSUNAMI_1STORDER_COARSENREFINE_HPP_ */
