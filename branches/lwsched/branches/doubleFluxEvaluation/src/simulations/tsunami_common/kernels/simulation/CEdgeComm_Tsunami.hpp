/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */

#include "../../CTsunamiConfig.hpp"

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CEdgeComm_Tsunami_0thOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CEdgeComm_Tsunami_0thOrder CEdgeComm_Tsunami;
	}
}

#else

#include "CEdgeComm_Tsunami_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CEdgeComm_Tsunami_1stOrder CEdgeComm_Tsunami;
	}
}

#endif
