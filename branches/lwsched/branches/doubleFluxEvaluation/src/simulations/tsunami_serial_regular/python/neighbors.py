'''
Created on Dec 6, 2011

@author: flo
'''

from code_fragments import *


type_left = "LEFT"
type_right = "RIGHT"
type_inner = "INNER"
type_border = "BORDER"
edge_left = -1
edge_hyp = 0
edge_right = 1

code_file_path = "/home/flo/workspace/sierpi/src/simulations/tsunami_serial_regular/CSimulationTsunami_Serial_Regular_Traverse_Table.hpp"
#code_file_path = "/u/halle/kleinfl/home_at/Documents/BA/CSimulationTsunami_Serial_Regular_Traverse_Table.hpp"


############################################################
# functions for neighbor list generation
############################################################

def odd(n):
	return n % 2 == 1


def size(depth):
	return 2 ** depth


def get_normal_list(depth):
	""" get list of hypothenuse normal directions """
	# root triangle: always normal direction 1 (per definition)
	if depth == 0:
		return [1]
	
	# recursion call: get list of level above
	normal_list_above = get_normal_list(depth - 1)
	result = []
	
	# add 2 new normals for each triangle (-> refinement)
	# normal directions in interval [0, 7]
	for normal in normal_list_above:
		if odd(depth):
			result.append(normal + 5 & 7)
			result.append(normal + 3 & 7)
		else:
			result.append(normal + 3 & 7)
			result.append(normal + 5 & 7)
	return result


def border_gaps(depth):
	""" determine gaps between border cells """
	# depth 0: no gaps / just one element
	if depth <= 0:
		return []
	# depth 1: 2 border elements, no gap
	elif depth == 1:
		return [0]
	# different behaviour for odd/even depth
	elif odd(depth):
		result = [0]
		for gap in border_gaps(depth - 1):
			# even depth: add "old" gap, size doubled because of higher depth
			#	add zero-gap for neighboring element
			result.append(gap * 2)
			result.append(0)
		return result
	else:
		result = border_gaps(depth-1)
		for i in range(len(result)):
			# odd depth: double size of every gap
			#	turn zero-gaps to size 2 gaps
			result[i] = result[i] * 2
			if result[i] == 0: result[i] = 2
		return result


def border_elements(depth, right_border=False, index_offset=0):
	""" list of border elements """
	# edge types: left: -1, hyp: 0, right: 1
	if odd(depth):
		if right_border:
			edge_type = edge_left
		else:
			edge_type = edge_right
	else: 
		edge_type = edge_hyp
	# format [(element_index, edge_type), ...]
	result = [(index_offset, edge_type)]
	
	# counter
	current = index_offset
	# add element (index, edge type) for
	for gap in border_gaps(depth):
		current += 1 + gap
		# alternate between left and right edge
		edge_type = -1 * edge_type
		result.append((current, edge_type))
	return result


def border_indices(depth, start_index=0):
	""" just the indices of border elements """
	return [element[0] for element in border_elements(depth, index_offset=start_index)]


def border_full(depth):
	""" full border element list (left + right border) """
	# left border: equals right border for depth - 1, + second part with offset
	left_border = border_elements(depth - 1) + border_elements(depth - 1, index_offset=size(depth - 1))
	right_border = border_elements(depth, right_border=True)
	
	# change format from [(index, edge_type)] to {index: edge_type}
	#left_border = {element[0]: (left_border.index(element), element[1]) for element in left_border}
	left_border_new = {}
	for element in left_border:
		left_border_new[element[0]] = (left_border.index(element), element[1])
	right_border_new = {}
	for element in right_border:
		right_border_new[element[0]] = (right_border.index(element), element[1])
	# format: [ {left_border}, {right_border} ]
	return [left_border_new, right_border_new]


def get_inner_neighbor_gaps(depth):
	""" determine index "gap" to non-adjacent neighbor cell """
	if depth < 3:
		# no gaps for depth < 3
		return []
	elif depth == 3: 
		# edge types: left: -1, hyp: 0, right: 1
		# format: [(element_index, gap_size, +/-_index, edge_type)]
		return [(2, 2, 1, edge_left), (5, 2, -1, edge_right)] #(element,gap,+/-,edge)
	else:
		# determine middle line elements where new gaps arise
		mid_elements = border_indices(depth - 3, start_index=2 ** (depth-3))
		# corner element needs special treatment
		corner_element = mid_elements.pop()
		result = []
		for element in get_inner_neighbor_gaps(depth - 1):
			# switch edge numbers when refining
			edge_type = -1 * element[3]
			# add "old" element with switched edge number
			result.append((element[0], element[1], element[2], edge_type))
			
			# for mid elements: add additional elements
			if element[0] in mid_elements:
				# different formula for these elements depending on odd/even depth
				if odd(depth):
					# element_index * 2, gap_size * 2 + 2, +/-_index, left_edge
					result.append((element[0] * 2, element[1] * 2 + 2, element[2], edge_left))
					# element_index * 2 + 1, gap_size * 2, +/-_index, right_edge
					result.append((element[0] * 2 + 1, element[1] * 2, element[2], edge_right))
				else:
					if odd(mid_elements.index(element[0])):
						# element_index * 2 + 1, gap_size * 2, +/-_index, hyp_edge
						result.append((element[0] * 2 + 1, element[1] * 2, element[2], edge_hyp))
					else:
						# element_index * 2, gap_size * 2 + 2, +/-_index, hyp_edge
						result.append((element[0] * 2, element[1] * 2 + 2, element[2], edge_hyp))
		
		# corner element treatment
		if odd(depth):
			result.append((corner_element * 2, 2, 1, edge_left))
		
		# sort resulting list
		result.sort()
		
		# result yet contains half the domain
		# now adding axis-symmetric second half
		result_complete = []
		domain_size = size(depth) - 1
		for element in result:
			# switch edge type for additional elements (axis-symmetry)
			edge_type = -1 * element[3]
			# keep first half element
			result_complete.append(element)
			# index = domain_size - element_index, gap_size, -/+_index, edge_type
			result_complete.append((domain_size - element[0], element[1], (-1 * element[2]), edge_type))
		
		# sort list again
		result_complete.sort()
		return result_complete


def get_inner_neighbors(depth):
	""" determine neighbors with non-adjacent index """
	# iterate over neighbor gaps
	# return format: {element_index: (neighbor_index, edge_number)}
	# calculation: element_index, element_index + (gap + 1) * +/-_index, edge_number
	result = {}
	for elem in get_inner_neighbor_gaps(depth):
		result[elem[0]] = (elem[0] + (elem[1] + 1) * elem[2], elem[3])
	return result


############################################################
# functions for list combination, adding direct neighbors
# conversion into format needed for c++ code generation below
############################################################

def build_cpparray(depth):
	""" format: [ [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp] ]
	 	element_index = array_index """
	# special case depth = 0 returned directly for convenience...
	if depth == 0:
		return [[0, 0, 1, type_left, type_right, type_left, 1]]
	# border: [left, right]
	border = border_full(depth)
	left_border = border[0]
	right_border = border[1]
	inner_element_neighbor_list = get_inner_neighbors(depth)
	normals_list = get_normal_list(depth)
	result = []
	# edge_next_index := edge type of following element
	edge_next_index = -1
	if odd(depth):
		edge_next_index = 1
	for i in range(size(depth)):
		# dummy entry
		element_entry = [-1, -1, -1, type_inner, type_inner, type_inner, normals_list[i]]
		# check appearance on left border
		if i in left_border:
			element_entry[left_border[i][1] + 1] = left_border[i][0]
			element_entry[left_border[i][1] + 4] = type_left
		# check appearance on right border
		if i in right_border:
			element_entry[right_border[i][1] + 1] = right_border[i][0]
			element_entry[right_border[i][1] + 4] = type_right
		# check appearance on inner neighbors
		if i in inner_element_neighbor_list:
			element_entry[inner_element_neighbor_list[i][1] + 1] = inner_element_neighbor_list[i][0]
			element_entry[inner_element_neighbor_list[i][1] + 4] = type_inner
		# set missing indices (-1 dummy values) to previous/next index
		if i > 0:
			# assign previous index as neighbor
			element_entry[-1 * edge_next_index + 1] = i - 1
			# assign following index as neighbor
			for j in range(3):
				if element_entry[j] == -1:
					edge_next_index = j - 1
					element_entry[j] = i + 1
		# first element has no preceding index
		elif i == 0:
			element_entry[edge_next_index + 1] = i + 1
		#else:
		#	element_entry[edge_next_index + 1] = i - 1
		#result.append(reverse_indices(element_entry, depth, len(left_border), len(right_border)))
		result.append(element_entry)
	return result


def build_distinct_cpparrays(depth):
	""" format: [ [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp, element_index] ]
	 	returns 2 arrays, distinct for border and non-border elements """
	if depth == 0:
		return ([], [[0, 0, 1, type_left, type_right, type_left, 1, 0]])
	# border: [left, right]
	border = border_full(depth)
	left_border = border[0]
	right_border = border[1]
	
	inner_element_neighbor_list = get_inner_neighbors(depth)
	normals_list = get_normal_list(depth)
	
	result_border = []
	result_inner = []
	
	# edge_type_next_index := edge type (hyp/left/right) 
	edge_type_next_index = edge_left
	if odd(depth):
		edge_type_next_index = edge_right
	for i in range(size(depth)):
		# determines which array the element belongs to
		element_at_border = (i in left_border) or (i in right_border)
		# dummy entry
		element_entry = [-1, -1, -1, type_inner, type_inner, type_inner, normals_list[i], i]
		# check appearance on left border
		if i in left_border:
			element_entry[left_border[i][1] + 1] = left_border[i][0]
			element_entry[left_border[i][1] + 4] = type_left
		# check appearance on right border
		if i in right_border:
			element_entry[right_border[i][1] + 1] = right_border[i][0]
			element_entry[right_border[i][1] + 4] = type_right
		# check appearance on inner neighbors
		if i in inner_element_neighbor_list:
			element_entry[inner_element_neighbor_list[i][1] + 1] = inner_element_neighbor_list[i][0]
			element_entry[inner_element_neighbor_list[i][1] + 4] = type_inner
		
		# set missing indices (-1 dummy values) to previous/next index
		if i > 0:
			# assign preceding index as neighbor
			# offset +1: edge types for left/hyp/right are -1, 0, 1; element_entry indices are 0, 1, 2
			element_entry[-1 * edge_type_next_index + 1] = i - 1
			# assign following index as neighbor
			for j in range(3):
				if element_entry[j] == -1:
					edge_type_next_index = j - 1
					element_entry[j] = i + 1
		# first element has no preceding index
		elif i == 0:
			element_entry[edge_type_next_index + 1] = i + 1
		#else:
		#	element_entry[edge_type_next_index + 1] = i - 1
		
		# add element to the proper resulting list (border or inner)
		if element_at_border:
			result_border.append(element_entry)
		else:
			result_inner.append(element_entry)
	return (result_inner, result_border)


############################################################
# functions for generation of different c++ code versions
# used main code fragments are stored in a separate file
# yet a little messy and not properly commented...
############################################################

def build_code(max_depth):
	""" first version: one array + one loop per depth """
	code = """

public:
	// format: [ [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp] ]
	%s

};
}
}
"""
	code_array = """
	inline void traverse_depth_%s(){
		static int regular_array_depth_%s [%s][7] = { %s };
		for (int i = 0; i < %s; i++){
			call_elementAction(regular_array_depth_%s[i][EDGE_TYPE_HYP], regular_array_depth_%s[i][EDGE_TYPE_RIGHT], regular_array_depth_%s[i][EDGE_TYPE_LEFT],
				regular_array_depth_%s[i][NEIGHBOR_INDEX_HYP], regular_array_depth_%s[i][NEIGHBOR_INDEX_RIGHT], regular_array_depth_%s[i][NEIGHBOR_INDEX_LEFT],
				i, regular_array_depth_%s[i][NORMAL_HYP]);
		}
	}
	"""
	switch_statement = ""
	switch_case = """
		case %s: 
			this->traverse_depth_%s();
			break;"""
	code_array_line = """
			%s"""
	code_list = ""
	for i in range(max_depth):
		cpparray = build_cpparray(i)
		code_line = ""
		code_lines = ""
		for j in range(len(cpparray)):
			code_line += "%s, " % str(cpparray[j]).replace("'","").replace("[", "{").replace("]", "}")
			if (j > 0 and j % 3 == 0) or j == len(cpparray) - 1:
				code_lines += code_array_line % code_line
				code_line = ""
		code_lines = code_lines[0 : len(code_lines) - 2]
		code_list += code_array % (i, i, size(i), code_lines, size(i), i,i,i,i,i,i,i)
		switch_statement += switch_case % (i, i)
	
	# combine code elements
	return code_head + code_elementAction + code_traverse % (switch_statement) + code % (code_list)


def code_array(depth, element_array, inner=False):
	""" compose cpp array code for given depth """
	code_inner_array = "static int regular_array_inner_depth_%s [%s][7] = { %s  };"
	code_border_array = "static int regular_array_border_depth_%s [%s][7] = { %s };"
	code_array_result = "\n\t\t\t"
	for j in range(len(element_array)):
		# new line after 3 elements (for readable code)
		if (j > 1 and j % 3 == 0):
			code_array_result += "\n\t\t\t"
		# add element
		code_array_result += "%s, " % str(element_array[j][0 : 7]).replace("'","").replace("[", "{").replace("]", "}")
		
	# cut off last comma
	if len(element_array) > 0:
		code_array_result = code_array_result[0 : len(code_array_result) - 2]
	# return code for inner or border elements
	if inner:
		return code_inner_array % (depth, size(depth), code_array_result)
	else:
		return code_border_array % (depth, size(depth), code_array_result)


def build_code_2arrays(max_depth):
	""" build code using 2 arrays and loops per depth """
	code_function = """
	inline void traverse_depth_%s(){
		%s
		%s
		for (int i = 0; i < %s; i++){
			call_elementAction_inner(&regular_array_inner_depth_%s[i]);
		}
		for (int i = 0; i < %s; i++){
			call_elementAction(&regular_array_border_depth_%s[i]);
		}
	}
	"""
	switch_statement = ""
	switch_case = """
		case %s: 
			this->traverse_depth_%s();
			break;"""
	code_list = ""
	for i in range(max_depth):
		cpparray = build_distinct_cpparrays(i)
		cppinner = cpparray[0]
		cppborder = cpparray[1]
		
		inner_array = code_array(i, cppinner, inner=True)
		border_array = code_array(i, cppborder)
		
		code_list += code_function % (i, inner_array, border_array, len(cppinner), i, len(cppborder), i)
		switch_statement += switch_case % (i, i)
	
	# combine code elements
	return code_head + code_elementAction_pointer + code_elementAction_inner_pointer + code_traverse % (switch_statement) + code_body % (code_list)


def build_code_direct_call(max_depth):
	""" code with loops replaced by direct call_elementAction calls """
	code_function = """
	inline void traverse_depth_%s(){%s
	}
	"""
	code_call_inner = "\n		call_elementAction_inner(%s);"
	code_call_border = "\n		call_elementAction(%s);"
	switch_statement = ""
	switch_case = """
		case %s: 
			this->traverse_depth_%s();
			break;"""
	code_list = ""
	for i in range(max_depth):
		cpparray = build_distinct_cpparrays(i)
		cppinner = cpparray[0]
		cppborder = cpparray[1]
		code_lines = ""
		
		# format: [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp, element_index]
		# needed: [edge_type_hyp, edge_type_right, edge_type_left, neighbor_index_hyp, neighbor_index_right, neighbor_index_left, element_index, normal_hyp]
		for element in cppinner:
			element = [element[1], element[2], element[0], element[7], element[6]]
			code_lines += code_call_inner % str(element).replace("'","").replace("[", "").replace("]", "")
		for element in cppborder:
			element = [element[4], element[5], element[3], element[1], element[2], element[0], element[7], element[6]]
			code_lines += code_call_border % str(element).replace("'","").replace("[", "").replace("]", "")
		
		code_list += code_function % (i, code_lines)
		switch_statement += switch_case % (i, i)
	
	# combine code elements
	return code_head + code_elementAction + code_elementAction_inner + code_traverse % (switch_statement) + code_body % (code_list)


def build_code_direct_call_templates(max_depth):
	""" modified direct calls: edge types as template parameters """
	code_function = """
	inline void traverse_depth_%s(){%s
	}
	"""
	code_call_inner = "\n		call_elementAction_inner(%s);"
	code_call_border = "\n		call_elementAction<%s>(%s);"
	switch_statement = ""
	switch_case = """
		case %s: 
			this->traverse_depth_%s();
			break;"""
	code_list = ""
	for i in range(max_depth):
		cpparray = build_distinct_cpparrays(i)
		cppinner = cpparray[0]
		cppborder = cpparray[1]
		code_lines = ""
		
		# format: [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp, element_index]
		# needed: [edge_type_hyp, edge_type_right, edge_type_left, neighbor_index_hyp, neighbor_index_right, neighbor_index_left, element_index, normal_hyp]
		for element in cppinner:
			element = [element[1], element[2], element[0], element[7], element[6]]
			code_lines += code_call_inner % str(element).replace("'","").replace("[", "").replace("]", "")
		for element in cppborder:
			element = [element[4], element[5], element[3], element[1], element[2], element[0], element[7], element[6]]
			args_template = element[0 : 3]
			border_flags = int(element[0] != "INNER") * 4 + int(element[1] != "INNER") * 2 + int(element[2] != "INNER")
			args_template.append(border_flags)
			args_parameter = element[3 : len(element)]
			code_lines += code_call_border % (str(args_template).replace("'","").replace("[", "").replace("]", ""), str(args_parameter).replace("'","").replace("[", "").replace("]", ""))
		
		code_list += code_function % (i, code_lines)
		switch_statement += switch_case % (i, i)
	
	# combine code elements
	return code_head + code_elementAction_template + code_elementAction_inner + code_traverse % (switch_statement) + code_body % (code_list)

	
def action():
	""" write code to file """
	
	"""depth = 3
	print str(get_inner_neighbors(depth))
	print str(border_full(depth))
	#print str(get_inner_neighbor_gaps(depth))
	#print str(build_cpparray(depth)).replace("'", "")
	cpparray = build_cpparray(depth)
	for arr in cpparray:
		print str(cpparray.index(arr)) + str(arr).replace("'", "")
"""
	#for i in range(4):
	#	print str(build_cpparray(i)).replace("'", "")
	#print build_code(4)
	cpp_file = open(code_file_path, "w")
	#cpp_file.write(build_code_2arrays(6))
	cpp_file.write(build_code_direct_call_templates(13))
	cpp_file.close()


if __name__ == "__main__":
	action()





