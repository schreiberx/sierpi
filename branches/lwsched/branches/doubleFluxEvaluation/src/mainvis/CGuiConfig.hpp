/*
 * Copyright 2010 Martin Schreiber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * CConfig.hpp
 *
 *  Created on: Mar 22, 2010
 *      Author: Martin Schreiber (schreiberx@gmail.com)
 */

#ifndef CGUICONFIG_HPP_
#define CGUICONFIG_HPP_

#include "libgl/hud/CGlWindow.hpp"
#include "libgl/hud/CGlHudConfig.hpp"
#include "libgl/hud/CGlFreeType.hpp"
#include "libgl/hud/CGlRenderOStream.hpp"

/*
 * callback handler helper precompiler definitions
 *
 * usage:
 * {	// open scope for each callback handler
 * CCONFIG_CALLBACK_START(CMainClass)
 *
 */

/*
 * create prefix code for callbacks
 * \param TMainClass	class to which the user pointer is casted to
 *
 * the variable c is created to make the the class of type TMainClass accessible
 */
#define CGUICONFIG_CALLBACK_START(TMainClass)				\
	class CConfigCallback								\
	{													\
		public:											\
		static void callbackHandler(void *p_user_ptr)	\
		{												\
			TMainClass &c = *(TMainClass*)p_user_ptr;

/*
 * create postfix code for callbacks
 */
#define CGUICONFIG_CALLBACK_END()							\
		}												\
	};


/*
 * precompiler directive to handler
 */
#define CGUICONFIG_CALLBACK_HANDLER	(CConfigCallback::callbackHandler)


#define CGUICONFIG_CALLBACK_INSTALL(config_variable)		\
	cConfig.setCallbackHandler(&config_variable, CGUICONFIG_CALLBACK_HANDLER, this);


/**
 * main gui configuration section
 *
 * this class is intended to hold all configurable parameters for the visualization.
 */
class CGuiConfig
{
private:
	CGlWindow windowMain;
	CGlHudConfig cGlHudConfigMainLeft;
	CGlHudConfig cGlHudConfigMainRight;

	CGlWindow windowLights;
	CGlHudConfig cGlHudConfigLights;


	bool main_drag_n_drop_active;
	bool lights_drag_n_drop_active;

public:
	/////////////////////////////////////////////////////////////////////

	bool reset;
	bool quit;

	bool render_skybox;

	bool refraction_active;

	bool take_screenshot;
	bool take_screenshot_series;

	bool output_fps_and_parameters;
	bool output_world_position_at_mouse_cursor;

	bool hud_visible;


	/************************************
	 * SIMULATION
	 ************************************/
	bool simulation_run;
	bool simulation_reset;
	int simulation_world_scene_id;
	bool simulation_random_raindrops;

	float simulation_parameter_gravitation;
	float simulation_parameter_triangle_cat_length;
	float simulation_parameter_timestep_size;


	/************************************
	 * PARALLELIZATION
	 ************************************/
	int simulation_split_workload_size;
	int simulation_join_workload_size;
	int simulation_number_of_threads;

	/************************************
	 * VISUALIZATION
	 ************************************/
	bool visualization_enabled;
	int visualization_method;
	bool visualization_render_subpartition_borders;
	int visualization_simulation_steps_per_frame;


	/************************************
	 * LIGHT
	 ************************************/
	bool light0_rotation;
	GLSL::vec3 light0_world_position;

	bool light0_enabled;

	GLSL::vec3 light0_ambient_color3;
	GLSL::vec3 light0_diffuse_color3;
	GLSL::vec3 light0_specular_color3;

	// water material
	GLSL::vec3 water_ambient_color3;
	GLSL::vec3 water_diffuse_color3;
	GLSL::vec3 water_specular_color3;
	float water_specular_exponent;
	float water_reflectance_at_normal_incidence;

	float refraction_index;


	/**************************************
	 * SPLATS
	 */
	int splats_texture_width;
	int splats_texture_height;
	float splat_size_scalar;

	float surface_texel_height_translate;
	float surface_texel_height_scale;



	/////////////////////////////////////////////////////////////////////
private:
	int mouse_x;
	int mouse_y;

	CGlFreeType *free_type;


public:
	CGuiConfig()
	{
		main_drag_n_drop_active = false;
		lights_drag_n_drop_active = false;
		setHudVisibility(false);
	}

	void setHudVisibility(bool p_render_hud)
	{
		hud_visible = p_render_hud;
	}


	void setup(	CGlFreeType &p_free_type,
				CGlRenderOStream &rostream
			)
	{
		free_type = &p_free_type;

		mouse_x = -1;
		mouse_y = -1;

		CGlHudConfig::COption o;

		// main window
		windowMain.setPosition(GLSL::ivec2(3, 3));
		windowMain.setSize(GLSL::ivec2(530, 594));
		windowMain.setBackgroundColor(GLSL::vec4(74.0/256.0, 96.0/256.0, 150.0/256.0, 0.7));

		cGlHudConfigMainLeft.insert(o.setupText("|--- MAIN CONTROL ---|"));
		cGlHudConfigMainLeft.insert(o.setupBoolean("Reset visualization", &reset));		reset = false;
		cGlHudConfigMainLeft.insert(o.setupBoolean("Quit program", &quit));				quit = false;

		cGlHudConfigMainLeft.insert(o.setupLinebreak());
		cGlHudConfigMainLeft.insert(o.setupText("|--- BOGUS STUFF ---|"));
		cGlHudConfigMainLeft.insert(o.setupBoolean("Output FPS and parameters", &output_fps_and_parameters));	output_fps_and_parameters = true;
		cGlHudConfigMainLeft.insert(o.setupBoolean("Output world position at mouse cursor",						&output_world_position_at_mouse_cursor));				output_world_position_at_mouse_cursor = false;
		cGlHudConfigMainLeft.insert(o.setupBoolean("Screenshot (screenshot.bmp)", &take_screenshot));			take_screenshot = false;
		cGlHudConfigMainLeft.insert(o.setupBoolean("Screenshot series", &take_screenshot_series));				take_screenshot_series = false;
		cGlHudConfigMainLeft.insert(o.setupText("(screenshots/screenshot_#####.bmp)"));

		cGlHudConfigMainLeft.insert(o.setupLinebreak());
		cGlHudConfigMainLeft.insert(o.setupText("Press [SPACE] to show/hide HUDs!"));

		/************************************
		 * VISUALIZATION
		 ************************************/
		cGlHudConfigMainLeft.insert(o.setupText("|--- Visualization ---|"));
		cGlHudConfigMainLeft.insert(o.setupBoolean("Enabled", &visualization_enabled));							visualization_enabled = true;
		cGlHudConfigMainLeft.insert(o.setupInt("Visualization Method", &visualization_method, 1, CMath::numeric_min<int>(), CMath::numeric_max<int>()));		visualization_method = 0;
		cGlHudConfigMainLeft.insert(o.setupBoolean("Subpartition Borders", &visualization_render_subpartition_borders));	visualization_render_subpartition_borders = true;
		cGlHudConfigMainLeft.insert(o.setupInt("Simulationsteps per frame", &visualization_simulation_steps_per_frame, 1, 1, CMath::numeric_max<int>()));		visualization_simulation_steps_per_frame = 1;


		/************************
		 * SIMULATION
		 ************************/
		cGlHudConfigMainRight.insert(o.setupLinebreak());
		cGlHudConfigMainRight.insert(o.setupText("|--- SIMULATION CONTROL ---|"));
		cGlHudConfigMainRight.insert(o.setupBoolean("Run simulation", &simulation_run));						simulation_run = true;
		cGlHudConfigMainRight.insert(o.setupBoolean("Reset simulation fluid", &simulation_reset));				simulation_reset = false;
		cGlHudConfigMainRight.insert(o.setupInt("World Scene", &simulation_world_scene_id, 1, 0, 7));		simulation_world_scene_id = 3;
		cGlHudConfigMainRight.insert(o.setupBoolean("Insert Random Raindrops", &simulation_random_raindrops));	simulation_random_raindrops = false;

		cGlHudConfigMainRight.insert(o.setupFloat("Gravitation length: ", &simulation_parameter_gravitation, 0.1, -100, 0));																	simulation_parameter_gravitation = -9.81;
		cGlHudConfigMainRight.insert(o.setupFloatHI("Triangle cathetus length: ", &simulation_parameter_triangle_cat_length, 0.01, CMath::numeric_min<float>(), CMath::numeric_max<float>()));	simulation_parameter_triangle_cat_length = 0.03;
		cGlHudConfigMainRight.insert(o.setupFloatHI("Timestep size: ", &simulation_parameter_timestep_size, 0.01, CMath::numeric_min<float>(), CMath::numeric_max<float>()));					simulation_parameter_timestep_size = 0.0001;


		/************************
		 * PARALLELIZATION
		 ************************/
		cGlHudConfigMainRight.insert(o.setupLinebreak());
		cGlHudConfigMainRight.insert(o.setupText("|--- PARALLELIZATION ---|"));
		cGlHudConfigMainRight.insert(o.setupInt("Split Elements", &simulation_split_workload_size, 1, 2, CMath::numeric_max<int>()));		simulation_split_workload_size = 50;
		cGlHudConfigMainRight.insert(o.setupInt("Join Elements", &simulation_join_workload_size, 1, 0, CMath::numeric_max<int>()));			simulation_join_workload_size = 25;
		cGlHudConfigMainRight.insert(o.setupInt("Number of Threads", &simulation_number_of_threads, 1, 1, CMath::numeric_max<int>()));	simulation_number_of_threads = 1;


		int sx = 10;
		int sy = windowMain.size[1]-10;
		cGlHudConfigMainLeft.setup(*free_type, rostream, sx, sy);
		cGlHudConfigMainRight.setup(*free_type, rostream, sx + cGlHudConfigMainLeft.area_width-20, sy);



		/***********************
		 * LIGHT
		 ***********************/

		cGlHudConfigLights.insert(o.setupText("|--- LIGHT 0 ---|"));

		cGlHudConfigLights.insert(o.setupBoolean("enabled", &light0_enabled));					light0_enabled = true;
		cGlHudConfigLights.insert(o.setupFloat("position X", &light0_world_position[0], 0.1));	light0_world_position = GLSL::vec3(0.5, 4.5, 0.5);
		cGlHudConfigLights.insert(o.setupFloat("position Y", &light0_world_position[1], 0.1));
		cGlHudConfigLights.insert(o.setupFloat("position Z", &light0_world_position[2], 0.1));

		cGlHudConfigLights.insert(o.setupFloat("AMBIENT R", &light0_ambient_color3[0], 0.01, 0.0, 1.0));	light0_ambient_color3 = GLSL::vec3(1,1,1);
		cGlHudConfigLights.insert(o.setupFloat("AMBIENT G", &light0_ambient_color3[1], 0.01, 0.0, 1.0));
		cGlHudConfigLights.insert(o.setupFloat("AMBIENT B", &light0_ambient_color3[2], 0.01, 0.0, 1.0));
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE R", &light0_diffuse_color3[0], 0.01, 0.0, 1.0));	light0_diffuse_color3 = GLSL::vec3(1,1,1);
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE G", &light0_diffuse_color3[1], 0.01, 0.0, 1.0));
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE B", &light0_diffuse_color3[2], 0.01, 0.0, 1.0));
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR R", &light0_specular_color3[0], 0.01, 0.0, 1.0));	light0_specular_color3 = GLSL::vec3(1,1,1);
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR G", &light0_specular_color3[1], 0.01, 0.0, 1.0));
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR B", &light0_specular_color3[2], 0.01, 0.0, 1.0));

		cGlHudConfigLights.insert(o.setupBoolean("Light0 rotation", &light0_rotation));				light0_rotation = false;


		// water material
		cGlHudConfigLights.insert(o.setupLinebreak());
		cGlHudConfigLights.insert(o.setupText("|--- transparent WATER parameters ---|"));
		cGlHudConfigLights.insert(o.setupFloat("AMBIENT R", &water_ambient_color3[0], 0.01, 0.0, 111.0));		water_ambient_color3 = GLSL::vec3(0.78, 0.88, 1.0);
		cGlHudConfigLights.insert(o.setupFloat("AMBIENT G", &water_ambient_color3[1], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("AMBIENT B", &water_ambient_color3[2], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE R", &water_diffuse_color3[0], 0.01, 0.0, 111.0));		water_diffuse_color3 = GLSL::vec3(0.0f);
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE G", &water_diffuse_color3[1], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("DIFFUSE B", &water_diffuse_color3[2], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR exponent", &water_specular_exponent, 0.1, 0.0));	water_specular_exponent = 20.0f;
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR R", &water_specular_color3[0], 0.01, 0.0, 111.0));	water_specular_color3 = GLSL::vec3(6.5,6.3,6);
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR G", &water_specular_color3[1], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("SPECULAR B", &water_specular_color3[2], 0.01, 0.0, 111.0));
		cGlHudConfigLights.insert(o.setupFloat("Water normal reflection", &water_reflectance_at_normal_incidence, 0.001, 0.0, 1.0));	water_reflectance_at_normal_incidence = 0.07;

		cGlHudConfigMainRight.insert(o.setupLinebreak());
		cGlHudConfigMainRight.insert(o.setupText("|--- Water Visualization CONTROL ---|"));

		cGlHudConfigLights.insert(o.setupInt("Splats texture width", &splats_texture_width, 128, 128, 4096));	splats_texture_width = 512;
		cGlHudConfigLights.insert(o.setupInt("Splats texture height", &splats_texture_height, 128, 128, 4096));	splats_texture_height = 512;
		cGlHudConfigLights.insert(o.setupFloat("Splats scale size", &splat_size_scalar, 0.05, 0.0, 512.0));	splat_size_scalar = 2.0/3.0;

		cGlHudConfigLights.insert(o.setupFloat("Surface texel height translate", &surface_texel_height_translate, 0.01, -100.0, 10000.0));	surface_texel_height_translate = -50.f;
		cGlHudConfigLights.insert(o.setupFloat("Surface texel height scale", &surface_texel_height_scale, 0.01, 0.01, 10000.0));				surface_texel_height_scale = 1.f;

		windowLights.setPosition(GLSL::ivec2(530, 10));
		windowLights.setBackgroundColor(GLSL::vec4(128.0/256.0, 128.0/256.0, 128.0/256.0, 0.7));
		windowLights.setSize(GLSL::ivec2(260, 570));

		sx = 10;
		sy = windowLights.size[1]-10;
		cGlHudConfigLights.setup(*free_type, rostream, sx, sy);

		resetPerspectiveAndPosition();

	}


	/**
	 * setup a callback handler which is called, when the value changed
	 */
	void setCallbackHandler(
			void *value_ptr,							///< pointer to value
			void (*callback_handler)(void *user_ptr),	///< callback handler
			void *user_ptr
		)
	{
		cGlHudConfigMainLeft.set_callback(value_ptr, callback_handler, user_ptr);
		cGlHudConfigMainRight.set_callback(value_ptr, callback_handler, user_ptr);

		cGlHudConfigLights.set_callback(value_ptr, callback_handler, user_ptr);
	}

	/**
	 * reset some values
	 */
	void resetPerspectiveAndPosition()
	{
	}

	/**
	 * render windowMain with configuration information
	 */
	void render()
	{
		if (hud_visible)
		{
			free_type->viewportChanged(windowLights.size);
			windowLights.startRendering();
			cGlHudConfigLights.renderConfigContent();
			windowLights.finishRendering();

			free_type->viewportChanged(windowMain.size);
			windowMain.startRendering();
			cGlHudConfigMainLeft.renderConfigContent();
			cGlHudConfigMainRight.renderConfigContent();
			windowMain.finishRendering();
		}
	}

	bool mouse_wheel(int x, int y)
	{
		if (hud_visible)
		{
			if (windowMain.inWindow(mouse_x, mouse_y))
			{
				cGlHudConfigMainLeft.mouse_wheel(x, y);
				cGlHudConfigMainRight.mouse_wheel(x, y);
				return true;
			}
			else if (windowLights.inWindow(mouse_x, mouse_y))
			{
				cGlHudConfigLights.mouse_wheel(x, y);
				return true;
			}
		}

		return false;
	}

	bool mouse_button_down(int button)
	{
		if (hud_visible)
		{
			if (windowMain.inWindow(mouse_x, mouse_y))
			{
				cGlHudConfigMainLeft.mouse_button_down(button);
				cGlHudConfigMainRight.mouse_button_down(button);

				if (cGlHudConfigMainLeft.active_option == NULL && cGlHudConfigMainRight.active_option == NULL)
				{
					if (button == CRenderWindow::MOUSE_BUTTON_LEFT)
						main_drag_n_drop_active = true;
				}
				return true;
			}
			else if (windowLights.inWindow(mouse_x, mouse_y))
			{
				cGlHudConfigLights.mouse_button_down(button);

				if (cGlHudConfigLights.active_option == NULL && cGlHudConfigLights.active_option == NULL)
				{
					if (button == CRenderWindow::MOUSE_BUTTON_LEFT)
						lights_drag_n_drop_active = true;
				}
				return true;
			}
		}

		return false;
	}

	void mouse_button_up(int button)
	{
		if (hud_visible)
		{
			cGlHudConfigMainLeft.mouse_button_up(button);
			cGlHudConfigMainRight.mouse_button_up(button);

			cGlHudConfigLights.mouse_button_up(button);
		}

		if (button == CRenderWindow::MOUSE_BUTTON_LEFT)
		{
			// always disable drag_n_drop, when
			main_drag_n_drop_active = false;
			lights_drag_n_drop_active = false;
		}
	}

	void mouse_motion(int x, int y)
	{
		int dx = x - mouse_x;
		int dy = y - mouse_y;


		if (main_drag_n_drop_active)
			windowMain.setPosition(windowMain.pos + GLSL::ivec2(dx, dy));

		if (lights_drag_n_drop_active)
			windowLights.setPosition(windowLights.pos + GLSL::ivec2(dx, dy));

		mouse_x = x;
		mouse_y = y;

		if (hud_visible)
		{
			cGlHudConfigMainLeft.mouse_motion(mouse_x-windowMain.pos[0], mouse_y-windowMain.pos[1]);
			cGlHudConfigMainRight.mouse_motion(mouse_x-windowMain.pos[0], mouse_y-windowMain.pos[1]);

			cGlHudConfigLights.mouse_motion(mouse_x-windowLights.pos[0], mouse_y-windowLights.pos[1]);
		}
	}
};


#endif /* CCONFIG_HPP_ */
