
evtl. immer CC=icc bei ./configure angeben!



* hdf5-1.8.8:
	URL: http://www.hdfgroup.org/HDF5/

	CC=icc CXX=icpc ./configure --prefix=$HOME/local

* netcdf-4.1.3 kompilieren:
	URL: http://www.unidata.ucar.edu/software/netcdf/

	CPPFLAGS=-I$HOME/local/include LDFLAGS=-L$HOME/local/lib CC=icc CXX=icpc ./configure --enable-shared --enable-cxx-4 --prefix=$HOME/local


* ASAGI:
	URL: https://github.com/TUM-I5/ASAGI

	mkdir build
	cd build
	export CMAKE_PREFIX_PATH=$HOME/local
	CC=icc CXX=icpc cmake -DNO_MPI=ON ..
	make -j 8
