/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CAffinityMap.hpp
 *
 *  Created on: May 09, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */
#ifndef CAFFINITY_MAP_HPP
#define CAFFINITY_MAP_HPP


/**
 * setup the affinity map depending on the given parameters
 */
class CAffinityMap
{
public:
	static bool setup(
			int i_num_threads,		///< number of threads to create affinity map for
			int i_max_cores,		///< maximum number of cores available on the system
			int i_displacement,		///< displacement to increment affinity
			int i_start_id,			///< start id of core where set the first thread affinity to
			int i_verbose_level,	///< verbosity level

			int *affinityMap		///< pointer to output of affinity map
	)
	{
		assert(i_num_threads < 1024);
		assert(i_num_threads >= 1);

		if (i_displacement < 0)
		{
			if (i_verbose_level >= 5)
				std::cout << "AFFINITY: No affinities for threads" << std::endl;

			return false;
		}

		for (int thread_id = 0; thread_id < i_num_threads; thread_id++)
		{
			/**
			 * apply padding
			 */
			int a = thread_id*i_displacement + i_start_id;
			while (a >= i_max_cores)
			{
				a -= i_max_cores;
				a++;
			}

			assert(a < i_max_cores);
			assert(a >= 0);

			affinityMap[thread_id] = a;
		}

		if (i_verbose_level >= 5)
		{
			std::stringstream ss;
			ss << "[ ";
			for (int i = 0; i < i_num_threads; i++)
				ss << affinityMap[i] << " ";
			ss << " ]";

			std::cout << "AFFINITY: threads to use: " << i_num_threads << std::endl;
			std::cout << "AFFINITY: max cores: " << i_max_cores << std::endl;
			std::cout << "AFFINITY: setting affinities with distance of " << i_displacement << ": " << ss.str() << std::endl;
		}

		if (i_displacement != 0)
		{
			for (int i = 0; i < i_num_threads; i++)
			{
				for (int j = 0; j < i_num_threads; j++)
				{
					if (i == j)
						continue;

					if (affinityMap[i] == affinityMap[j])
					{
						std::cerr << "Equal affinities for different threads found" << std::endl;
						exit(-1);
					}
				}
			}
		}

		return true;
	}

};

#endif
