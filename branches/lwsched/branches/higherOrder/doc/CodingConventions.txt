

* Identation of blocks uses a single TAB for each block.

* Naming conventions:

  + Parameters for methods:
    - parameters of methods are prefixed by i_, o_ or io_.
    - i_ means that this parameter is accessed read/only (const).
    - o_ is used to declare this parameter as being an output reference to
      write some output values.
    - io_ is used to declare an input/output pointer or reference
      which is read and written.

  + Types
    - Types are distinguished whether they are used as a
    	class,
    	class with templates,
    	typedef followed by a specialized class
    	classes without templates
    	atomar variables (int, char, float, ...)
    - When classes are defined, they are always prefixed with a
      capital letter 'C' denoting that they are a class.
      Also classes with template parameters are prefixed with 
      the 'C' 
    - As soon as a class (with or without template parameters) is given
      as a template parameter or redefined via a typedef, the 'C'
      is replaced by a 'T' to account for a fixed type without necessity of
      template parameters. 



/**
 * comments preceeding functions should follow the doxygen (www.doxygen.org)
 * code-style
 *
 * \return description of return value 
 */
void foo(
	const int i_bar_var,	///< this is totally useless
	void *io_foo_var		///< this variable is not able to drink beer
) {
	while (true) {
		if (false) {
		}
	}
}
