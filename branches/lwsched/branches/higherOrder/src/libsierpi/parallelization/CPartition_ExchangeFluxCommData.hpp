/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_ExchangeFluxCommData.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITIONTREE_NODE_FLUXCOMM_HPP
#define CPARTITIONTREE_NODE_FLUXCOMM_HPP

#include "config.h"
#include "libsierpi/domain_triangulation/CDomain_BaseTriangle.hpp"
#include "CPartition_EdgeComm_InformationAdjacentPartitions.hpp"
#include <string.h>



/**
 * this class handles the communication with the adjacent edges based on the adjacency
 * information given in CPartitionTree_Node.
 */
template <	typename CPartition_TreeNode,			///< partition tree node
			typename TEdgeElement,					///< type of single edge element
			typename CStackAccessors,				///< access to stacks
			typename CKernelWith_computeNetUpdates	///< kernel type offering 'computeNetUpdates'
		>
class CPartition_ExchangeFluxCommData	: public CStackAccessors
{
private:
	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition_TreeNode> CEdgeComm_InformationAdjacentPartition_;

private:
	CStack<TEdgeElement> *localHypExchangeEdgeStack;
	CStack<TEdgeElement> *localCatExchangeEdgeStack;

	CStack<TEdgeElement> *localHypEdgeStack;
	CStack<TEdgeElement> *localCatEdgeStack;

	/**
	 * Handler to root partitionTreeNode
	 */
	CPartition_TreeNode *cPartitionTreeNode;

	/**
	 * true, when traversal along hyp/cat is clockwise
	 */
	bool hypClockwise, catClockwise;


	/**
	 * Pointer to kernel offering method 'computeNetUpdates' to
	 * compute the flux updates
	 */
	CKernelWith_computeNetUpdates *cKernelWith_computeNetUpdates;


	/**
	 * Constructor
	 */
public:
	CPartition_ExchangeFluxCommData(
			CPartition_TreeNode *p_cPartitionTreeNode,
			CKernelWith_computeNetUpdates *p_cKernelWith_computeNetUpdates
	)
	{
		cPartitionTreeNode = p_cPartitionTreeNode;
		cKernelWith_computeNetUpdates = p_cKernelWith_computeNetUpdates;
	}



	/**
	 * return the information about where to find the communication data at the adjacent triangle stack
	 */
private:
	void getEdgeCommDataPointersFromAdjacentPartition(
			CPartition_UniqueId &i_thisUniqueId,				///< unique id of partition to write data to
			CPartition_TreeNode *i_adjacentPartitionTreeNode,	///< pointer to adjacent partition
			TEdgeElement **o_src_edgeElements,					///< output: pointer to starting point of data to be exchanged
			int *o_edgeElementCount,							///< output: number of elements stored on the stack
			bool *o_clockwise									///< output: true, if edge elements are stored with clockwise traversal
	)	const
	{
		int pos;

		/*
		 * first of all, we search on the hyp communication information to find the appropriate subset
		 */
		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
					i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_info = *iter;
			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjHypStack;

				if (i_adjacentPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnLeftStack())
				{
					/*
					 * clockwise stack is always the left one
					 */
					adjHypStack = this->leftStackAccessor(i_adjacentPartitionTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjHypStack = this->rightStackAccessor(i_adjacentPartitionTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.commElements;
				*o_src_edgeElements = adjHypStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.commElements;
		}

		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
					i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_info = *iter;
			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjCatStack;
				if (i_adjacentPartitionTreeNode->cTriangleFactory.isCathetusDataOnLeftStack())
				{
					adjCatStack = this->leftStackAccessor(i_adjacentPartitionTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjCatStack = this->rightStackAccessor(i_adjacentPartitionTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.commElements;
				*o_src_edgeElements = adjCatStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.commElements;
		}

//		std::cout << "FATAL ERROR: Communication partner not found! 2" << std::endl;
		assert(false);
	}



	/**
	 * this method is executed for every adjacent partition simulation to
	 * pull the edge communication data from an adjacent partition
	 */
private:
	void pullEdgeCommDataFromPartition(
			CStack<TEdgeElement> *io_cLocalExchangeStack,	///< stack to store edge element data to
			bool i_clockwise,								///< order in which to store data
			const CEdgeComm_InformationAdjacentPartition_ &i_informationAdjacentPartition,	///< information about adjacent partitions
			unsigned long local_edge_comm_stack_index		///< index to local edge comm stack to start copying data
	)
	{
		TEdgeElement *src_edgeElements = nullptr;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		/*
		 * first we search for the adjacent partition
		 */
		getEdgeCommDataPointersFromAdjacentPartition(
					cPartitionTreeNode->uniqueId,
					i_informationAdjacentPartition.partitionTreeNode,
					&src_edgeElements,
					&edgeElementCount,
					&adj_clockwise
				);
#if DEBUG
		if (i_informationAdjacentPartition.commElements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + partition id: " << cPartitionTreeNode->uniqueId << std::endl;
			assert(false);
		}
#endif

		if (i_clockwise != adj_clockwise)
		{
			/*
			 * directly copy stack elements
			 */

			/*
			 * this does not work anymore since there may be gaps within the edge comm stack due to
			 * partially exchanged data
			 */
			//memcpy(i_cStack->getStackPtr(), src_edgeElements, sizeof(TEdgeElement)*edgeElementCount);

			memcpy(&(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index)), src_edgeElements, sizeof(TEdgeElement)*edgeElementCount);
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			size_t adj_i = edgeElementCount-1;

			for (int i = 0; i < edgeElementCount; i++)
			{
				memcpy(&(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index+i)), &(src_edgeElements[adj_i]), sizeof(TEdgeElement));
				adj_i--;
			}
		}

		io_cLocalExchangeStack->incStackElementCounter(edgeElementCount);
	}



	/**
	 * this method is executed for every adjacent partition simulation to
	 * pull the edge communication data from an adjacent partition.
	 */
private:
	template <bool updateAdjacentExchangeEdgeData>
	void pullEdgeCommDataFromPartition_AND_computeFlux(
			CStack<TEdgeElement> *io_cLocalExchangeStack,		///< stack to store edge element data to
			const CStack<TEdgeElement> *io_cLocalStack,		///< stack with previously written data needed to compute the flux
			bool i_clockwise,								///< order in which to store data
			const CEdgeComm_InformationAdjacentPartition_ &i_informationAdjacentPartition,	///< information about adjacent partitions
			unsigned long local_edge_comm_stack_index		///< index to local edge comm stack to start copying data
	)
	{
		TEdgeElement *adjacent_edgeCommElements = nullptr;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		/*
		 * first we search for the adjacent partition
		 */
		getEdgeCommDataPointersFromAdjacentPartition(
					cPartitionTreeNode->uniqueId,	///< unique id
					i_informationAdjacentPartition.partitionTreeNode,	///< pointer to partition tree node
					&adjacent_edgeCommElements,	///< pointer pointing into stack
					&edgeElementCount,			///< number of elements store don stack
					&adj_clockwise				///< adjacent data stored in clockwise order?
				);

#if DEBUG
		if (i_informationAdjacentPartition.commElements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + partition id: " << cPartitionTreeNode->uniqueId << std::endl;
			assert(i_informationAdjacentPartition.commElements != edgeElementCount);
		}
#endif

		TEdgeElement tmpEdgeElement;

		if (i_clockwise != adj_clockwise)
		{
			TEdgeElement *local_edgeCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			TEdgeElement *local_exchangeEdgeCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			for (int i = 0; i < edgeElementCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->computeNetUpdates(
						*local_edgeCommStackElement,					/// left edge comm element
						adjacent_edgeCommElements[i],					/// adjacent right edge comm element

						*local_exchangeEdgeCommStackElement,			/// output: local left flux
						tmpEdgeElement									/// output: adjacent right flux
					);

				/*
				 * update adjacent exchange edge data only for single-flux-evaluation
				 */
				if (updateAdjacentExchangeEdgeData)
				{
					memcpy(local_edgeCommStackElement, &tmpEdgeElement, sizeof(TEdgeElement));
				}

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
			}
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			size_t adj_i = edgeElementCount-1;

			TEdgeElement *local_edgeCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			TEdgeElement *local_exchangeEdgeCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			for (int i = 0; i < edgeElementCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->computeNetUpdates(
						*local_edgeCommStackElement,					/// left edge comm element
						adjacent_edgeCommElements[adj_i],				/// adjacent right edge comm element

						*local_exchangeEdgeCommStackElement,			/// output: local left flux
						tmpEdgeElement									/// output: adjacent right flux
					);

				if (updateAdjacentExchangeEdgeData)
				{
					// copy to local edge comm stack since this element is read by the adjacent sub-partition during the 2nd pass
					memcpy(local_edgeCommStackElement, &tmpEdgeElement, sizeof(TEdgeElement));
				}

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
				adj_i--;
			}
		}

		io_cLocalExchangeStack->incStackElementCounter(edgeElementCount);
	}



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

private:
	void validateEdgeCommDataLengthFromPartition(
			CStack<TEdgeElement> *stack,			///< stack to load edge element data from
			bool clockwise,							///< order in which to store data
			CEdgeComm_InformationAdjacentPartition_ &informationAdjacentPartition	///< information about adjacent partitions
	)	const
	{
		TEdgeElement *src_edgeElements = NULL;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		getEdgeCommDataPointersFromAdjacentPartition(
					cPartitionTreeNode->uniqueId,
					informationAdjacentPartition.partitionTreeNode,
					&src_edgeElements,
					&edgeElementCount,
					&adj_clockwise
				);
#if DEBUG
		if (informationAdjacentPartition.commElements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 1: comm Elements mismatch" << std::endl;
			std::cerr << " + partition id: " << cPartitionTreeNode->uniqueId << std::endl;
			assert(informationAdjacentPartition.commElements != edgeElementCount);
		}
#endif
	}
#endif



	/**
	 * this method is executed by the simulation running on the partition after global synch to
	 * read the communication stack elements from the adjacent partitions
	 *
	 * !!! only load adjacent edge comm data when localUniqueId < adjacentUniqueId !!!
	 */
public:
	void pullEdgeCommData_1stPass()
	{
		/**
		 * setup stacks to store data to
		 */
		if (cPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cPartitionTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cPartitionTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			// only load adjacent edge comm data when localUniqueId < adjacentUniqueId
			if (cPartitionTreeNode->uniqueId < cEdgeComm_InformationAdjacentPartition.uniqueId)
			{
				pullEdgeCommDataFromPartition_AND_computeFlux<true>(
						localHypExchangeEdgeStack,
						localHypEdgeStack,
						hypClockwise,
						cEdgeComm_InformationAdjacentPartition,
						local_edge_comm_stack_index
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			// only load adjacent edge comm data when localUniqueId < adjacentUniqueId
			if (cPartitionTreeNode->uniqueId < cEdgeComm_InformationAdjacentPartition.uniqueId)
			{
				pullEdgeCommDataFromPartition_AND_computeFlux<true>(
						localCatExchangeEdgeStack,
						localCatEdgeStack,
						catClockwise,
						cEdgeComm_InformationAdjacentPartition,
						local_edge_comm_stack_index
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}
	}



	/**
	 * this method is executed by the simulation running on the partition after global synch to
	 * read the communication stack elements from the adjacent partitions
	 *
	 * !!! only load adjacent edge comm data when localUniqueId > adjacentUniqueId !!!
	 */
public:
	void pullEdgeCommData_2ndPass()
	{
		/**
		 * TODO: remove me - this should be already setup
		 */
		if (cPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cPartitionTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cPartitionTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
			if (cPartitionTreeNode->uniqueId > cEdgeComm_InformationAdjacentPartition.uniqueId)
			{
				pullEdgeCommDataFromPartition(
						localHypExchangeEdgeStack,
						hypClockwise,
						cEdgeComm_InformationAdjacentPartition,
						local_edge_comm_stack_index
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
			if (cPartitionTreeNode->uniqueId > cEdgeComm_InformationAdjacentPartition.uniqueId)
			{
				pullEdgeCommDataFromPartition(
						localCatExchangeEdgeStack,
						catClockwise,
						cEdgeComm_InformationAdjacentPartition,
						local_edge_comm_stack_index
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}
	}



	/**
	 * this method is executed by the simulation running on a partition reading the adjacent
	 * edge communication.
	 *
	 * in this way the fluxes for the shared sub-partition borders are double evaluated
	 */
public:
	void pullEdgeCommData_doubleFluxEvaluation()
	{
		/**
		 * setup stacks to store data to
		 */
		if (cPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cPartitionTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cPartitionTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			// only load adjacent edge comm data when localUniqueId < adjacentUniqueId
			pullEdgeCommDataFromPartition_AND_computeFlux<false>(
					localHypExchangeEdgeStack,	// local destination
					localHypEdgeStack,			// local source
					hypClockwise,
					cEdgeComm_InformationAdjacentPartition,
					local_edge_comm_stack_index
				);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

			pullEdgeCommDataFromPartition_AND_computeFlux<false>(
					localCatExchangeEdgeStack,	// local destination
					localCatEdgeStack,			// local source
					catClockwise,
					cEdgeComm_InformationAdjacentPartition,
					local_edge_comm_stack_index
				);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
		}
	}



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

public:
	void validateCommDataLength()	const
	{
		bool hypClockwise = cPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			// hypEdgeStack was not initialized yet -> ignore
			if (localHypExchangeEdgeStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromPartition(localHypExchangeEdgeStack, hypClockwise, *iter);
		}


		bool catClockwise = cPartitionTreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			// catEdgeStack was not initialized yet -> ignore
			if (localCatExchangeEdgeStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromPartition(localCatExchangeEdgeStack, catClockwise, *iter);
		}
	}
#endif

	/**
	 * update the sizes of the edge pieces which have to be exchanged with the adjacent partitions
	 *
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * ! this method is intended to update the edge communication information for adaptivity !
	 * ! AND                                                                                 !
	 * ! for the split/join information which is given as a parameter                        !
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 *
	 *
	 * stack elements have to be of type 'char'.
	 * 	-1: coarsening done
	 * 	 0: nothing
	 * 	 1: refinement done
	 */
public:
	void updateEdgeCommSizeAndSplitJoinInformation(
			CStack<char>* p_leftEdgeStack,
			CStack<char>* p_rightEdgeStack,
			CPartition_SplitJoin_EdgeComm_Information &p_adaptive_splitJoinInformation,
			CPartition_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	)
	{
		CStack<char> *catEdgeStack, *hypEdgeStack;

		bool even = (p_adaptive_splitJoinInformation.triangleFactory.evenOdd == CTriangle_Enums::EVEN);

		if (even)
		{
			catEdgeStack = p_leftEdgeStack;
			hypEdgeStack = p_rightEdgeStack;
		}
		else
		{
			catEdgeStack = p_rightEdgeStack;
			hypEdgeStack = p_leftEdgeStack;
		}

		CStackReaderTopDown<char> readerTopDown;

		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);

		/*******************************************************
		 * COPY-COPY-COPY
		 *******************************************************/

		// copy fixed variables
		p_splitJoinInformation.splitting_permitted = p_adaptive_splitJoinInformation.splitting_permitted;
		p_splitJoinInformation.joining_permitted = p_adaptive_splitJoinInformation.joining_permitted;

		// splitting and joining requests can be either triggered by the adaptive process (p_adaptive_splitJoinInformation) or by the user (p_splitJoinInformation)
		if (p_splitJoinInformation.splitJoinRequests == CPartition_SplitJoinInformation_Enums::NO_OPERATION)
			p_splitJoinInformation.splitJoinRequests = p_adaptive_splitJoinInformation.splitJoinRequests;


		/*******************************************************
		 * HYPOTENUSE
		 *******************************************************/
		/*
		 * setup the adaptive edge comm updates for the hyp
		 */
		readerTopDown.setup(*hypEdgeStack);

		if (p_splitJoinInformation.splitting_permitted || p_splitJoinInformation.joining_permitted)
		{
			p_splitJoinInformation.number_of_shared_edge_comm_elements = p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

			// number of edge comm elements which have to be handled before switching to the second sub-triangle
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}


			if (cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.size() > 0)
			{
#if DEBUG
				bool secondTriangleActive = false;
#endif
				/*
				 * iterate over the adjacency information about partitions next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
							cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
						iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentPartition_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_partition_edge_comm_size = 0;

					/**
					 * variable to determine whether the previous edge comm element was
					 * one of a triangle which should be coarsened
					 */
					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent partition
					 */
					for (int c = 0; c < i.commElements; c++)
					{
						if (edgeCommRemainingCounter == 0)
						{
#if DEBUG
							assert(secondTriangleActive == false);
							// switch to second triangle
#endif
							/*
							 * compute the remaining edge elements which have to be considered for this edge.
							 * for the second triangle, this is used as a variable to check whether all edge information
							 * is popped from the adjacency stacks.
							 */
							if (even)
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
							}
							else
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
							}

							assert(edgeCommRemainingCounter != 0);
#if DEBUG
							secondTriangleActive = true;
#endif
						}

						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// one edge element less which we have to consider
						edgeCommRemainingCounter--;

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_partition_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_partition_edge_comm_size += 2;
							// comm elements are increased by one edgeComm element due to refinement
							*splitJoinInformationElementsOnStackPtr += 1;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_partition_edge_comm_size += 1;

						/*
						 * comm elements are reduced by one edgeComm element
						 */
						*splitJoinInformationElementsOnStackPtr -= 1;
					}

					/**
					 * setup new edge communication size for this edge
					 */
					i.commElements = new_partition_edge_comm_size;

					assert(within_coarsening == false);
				}

				assert(edgeCommRemainingCounter == 0);
			}
		}
		else
		{
			readerTopDown.setup(*hypEdgeStack);

			/*
			 * no further splitting possible
			 */

			if (cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.size() > 0)
			{
				/*
				 * iterate over the adjacency information about partitions next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
							cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
						iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentPartition_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_partition_edge_comm_size = 0;

					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent partition
					 */
					for (int c = 0; c < i.commElements; c++)
					{
						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_partition_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_partition_edge_comm_size += 2;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_partition_edge_comm_size += 1;
					}

					assert(within_coarsening == false);

					/**
					 * setup new edge communication size for this edge
					 */
					i.commElements = new_partition_edge_comm_size;
				}
			}
		}



		/*******************************************************
		 * CATHETUS
		 *******************************************************/
		if (p_splitJoinInformation.splitting_permitted)
		{
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}

			readerTopDown.setup(*catEdgeStack);
#if DEBUG
			bool secondTriangleActive = false;
#endif
			/*
			 * iterate over the adjacency information about partitions next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
					iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &i = *iter;

				// compute new partition communication size
				unsigned int new_partition_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.commElements; c++)
				{
					if (edgeCommRemainingCounter == 0)
					{
#if DEBUG
						assert(secondTriangleActive == false);
						// switch to second triangle
#endif
						/*
						 * compute the remaining edge elements which have to be considered for this edge.
						 * for the second triangle, this is used as a variable to check whether all edge information
						 * is popped from the adjacency stacks.
						 */
						if (even)
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
						}
						else
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
						}

						assert(edgeCommRemainingCounter != 0);
//						if (edgeCommRemainingCounter == 0)
//							break;
#if DEBUG
						secondTriangleActive = true;
#endif
					}

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// one edge element less which we have to consider
					edgeCommRemainingCounter--;

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_partition_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_partition_comm_size += 2;
						*splitJoinInformationElementsOnStackPtr += 1;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_partition_comm_size += 1;

					*splitJoinInformationElementsOnStackPtr -= 1;
				}

				i.commElements = new_partition_comm_size;

				assert(within_coarsening == false);
			}

			p_splitJoinInformation.first_triangle.number_of_elements = p_adaptive_splitJoinInformation.first_triangle.number_of_elements;
			p_splitJoinInformation.second_triangle.number_of_elements = p_adaptive_splitJoinInformation.second_triangle.number_of_elements;

			assert(edgeCommRemainingCounter == 0);
		}
		else
		{
			readerTopDown.setup(*catEdgeStack);

			/*
			 * iterate over the adjacency information about partitions next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter = cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
					iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &i = *iter;

				// compute new partition communication size
				unsigned int new_partition_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.commElements; c++)
				{

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_partition_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_partition_comm_size += 2;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_partition_comm_size += 1;
				}

				i.commElements = new_partition_comm_size;
			}
		}
	}
};

#endif
