/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_SplitJoinInformation.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_SPLITJOININFORMATION_HPP_
#define CPARTITION_SPLITJOININFORMATION_HPP_

#include "libsierpi/triangle/CTriangle_Factory.hpp"

class CPartition_SplitJoinInformation_Enums
{
public:
	enum ESPLIT_JOIN_REQUESTS
	{
		NO_OPERATION = 0,
		SPLIT = 1,
		JOIN = 2
	};
};


/**
 * split and join information
 */
class CPartition_SplitJoin_EdgeComm_Information	: public CPartition_SplitJoinInformation_Enums
{
public:
	/**
	 * information about how to split a single sub-triangle
	 */
	class CSubTriangleData
	{
	public:
		CTriangle_Factory triangleFactory;

		/**
		 * new number of elements within subtree
		 */
		int number_of_elements;

		/**
		 * information about edge communication to adjacent partitions
		 * when only considering edge comm element data
		 */
		int number_of_edge_comm_elements_left_stack;
		int number_of_edge_comm_elements_right_stack;

		CSubTriangleData()	:
			number_of_elements(-1),
			number_of_edge_comm_elements_left_stack(-1),
			number_of_edge_comm_elements_right_stack(-1)
		{
		}


		friend
		std::ostream&
		operator<<(std::ostream &os, const CSubTriangleData &d)
		{
			os << " + number_of_elements " << d.number_of_elements << std::endl;
			os << " + number_of_edge_comm_elements_left_stack: " << d.number_of_edge_comm_elements_left_stack << std::endl;
			os << " + number_of_edge_comm_elements_right_stack: " << d.number_of_edge_comm_elements_right_stack << std::endl;
			return os;
		}
	};

	CTriangle_Factory &triangleFactory;

	/**
	 * true, if the triangle factories were set up already
	 */
	bool triangleFactorySetupDone;


	/**
	 * this variable is set to false whenever no further splitting is possible
	 */
	bool splitting_permitted;



	/**
	 * this variable is set to false whenever no further joining with the
	 * appropriate adjacent child is possible
	 */
	bool joining_permitted;


	/**
	 * update of edge communication required
	 */
	bool update_edge_comm_required;

	/**
	 * this information is set by the application developer to trigger a join or coarsen operation
	 */
	ESPLIT_JOIN_REQUESTS splitJoinRequests;


	/**
	 * sub-triangle data about first and second traversed triangle
	 */
	CSubTriangleData first_triangle, second_triangle;

	/**
	 * information about shared edge communication elements
	 */
	int number_of_shared_edge_comm_elements;


	/**
	 * constructor
	 */
	CPartition_SplitJoin_EdgeComm_Information(
			CTriangle_Factory &p_triangleFactory
	)	:
		triangleFactory(p_triangleFactory),
		triangleFactorySetupDone(false),
		splitting_permitted(false),
		joining_permitted(false),
		update_edge_comm_required(false),
		splitJoinRequests(NO_OPERATION),
		number_of_shared_edge_comm_elements(-1)
	{
	}


	friend
	std::ostream&
	operator<<(	std::ostream &os, CPartition_SplitJoin_EdgeComm_Information &d)
	{
		os << "> number_of_shared_edge_comm_elements: " << d.number_of_shared_edge_comm_elements << std::endl;
		os << "> splitting permitted: " << d.splitting_permitted << std::endl;
		os << "> joining permitted: " << d.joining_permitted << std::endl;
		os << " first Triangle:" << std::endl;
		os << d.first_triangle;
		os << " second Triangle:" << std::endl;
		os << d.second_triangle;

		return os;
	}
};

#endif /* CPARTITION_SPLITJOININFORMATION_HPP_ */
