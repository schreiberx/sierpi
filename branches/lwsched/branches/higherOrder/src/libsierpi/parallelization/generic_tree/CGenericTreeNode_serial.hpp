/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_serial.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_SERIAL_HPP_
#define CGENERICTREENODE_SERIAL_HPP_

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGenericTreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

inline void specializedConstructorMethod()
{
}


/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE)
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_GenericTreeNode(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node);
			return 0;
		}

		CTraversalTask_GenericTreeNode<CLambdaFun> first(this_node->first_child_node, lambda);
		CTraversalTask_GenericTreeNode<CLambdaFun> second(this_node->second_child_node, lambda);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		return 0;
	}
};


template <typename CLambdaFun>
void traverse_GenericTreeNode_Parallel(
		CLambdaFun p_lambda
)
{
	CTraversalTask_GenericTreeNode<CLambdaFun>(this, p_lambda).execute();
}


template <typename CLambdaFun>
void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun p_lambda
)
{
	CTraversalTask_GenericTreeNode<CLambdaFun>(this, p_lambda).execute();
}


template <typename CLambdaFun>
void traverse_GenericTreeNode_Serial(
		CLambdaFun p_lambda
)
{
	CTraversalTask_GenericTreeNode<CLambdaFun>(this, p_lambda).execute();
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes_preorder;
	CLambdaFun3 lambda_midnodes_postorder;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes_preorder(i_lambda_midnodes_preorder),
		lambda_midnodes_postorder(i_lambda_midnodes_postorder)
	{
	}

	void execute()
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes_preorder(this_node);
		}
		else
		{
			lambda_leaves(this_node);
			return;
		}

		CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes<CLambdaFun1, CLambdaFun2, CLambdaFun3> first(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
		CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes<CLambdaFun1, CLambdaFun2, CLambdaFun3> second(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		lambda_midnodes_postorder(this_node);

		return;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder).execute();
}

template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder).execute();
}

template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder).execute();
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in POSTORDER
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes<CLambdaFun1, CLambdaFun2> first(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes<CLambdaFun1, CLambdaFun2> second(this_node->second_child_node, lambda_leaves, lambda_midnodes);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		if (this_node)
			lambda_midnodes(this_node);

		return 0;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			i_lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}
		else
		{
			i_lambda_midnodes_preorder(this_node, p_genericTreeDepth);
		}

		p_genericTreeDepth++;
		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, p_genericTreeDepth);
		}
	}
};




/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, 0);
}



/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, 0);
}


/*
 * parallel scan
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, 0);
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}

template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes,
		int p_genericTreeDepth
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}

template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes,
		int p_genericTreeDepth
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}




/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	void (*reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_Reduce(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun i_lambda,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_rootReduceValue
	)	:
		this_node(i_this_node),
		lambda(i_lambda),
		reduceOperator(i_reduceOperator),
		rootReduceValue(o_rootReduceValue)
	{
	}

	/**
	 * TASK
	 */
	void* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda(this_node, rootReduceValue);
			return 0;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */

		/**
		 * first and second child node exists
		 */
		if (this_node->first_child_node && this_node->second_child_node)
		{
			CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> first(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst);
			first.execute();

			CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> second(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond);
			second.execute();

			reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> first(this_node->first_child_node, lambda, reduceOperator, rootReduceValue);
			first.execute();

			return nullptr;
		}

		assert(this_node->second_child_node != nullptr);

		CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> second(this_node->second_child_node, lambda, reduceOperator, rootReduceValue);
		second.execute();

		return nullptr;
	}
};

template <
	typename CLambdaFun,
	typename TReduceValue
>
void traverse_GenericTreeNode_Reduce_Serial(
		CLambdaFun i_lambdaFun,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> t(this, i_lambdaFun, i_reduceOperator, o_reduceOutput);
	t.execute();
}

template <
	typename CLambdaFun,
	typename TReduceValue
>
void traverse_GenericTreeNode_Reduce_Parallel(
		CLambdaFun i_lambdaFun,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> t(this, i_lambdaFun, i_reduceOperator, o_reduceOutput);
	t.execute();
}


template <
	typename CLambdaFun,
	typename TReduceValue
>
void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambdaFun,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_Reduce<CLambdaFun, TReduceValue> t(this, i_lambdaFun, i_reduceOperator, o_reduceOutput);
	t.execute();
}




/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/

template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	void (*reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput);

public:
	TReduceValue reduceValue;

	inline CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*p_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput)
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		reduceOperator(p_reduceOperator)
	{
	}

	/**
	 * TASK
	 */
	void execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, &reduceValue);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */

		/**
		 * first and second child node exists
		 */
		if (this_node->first_child_node && this_node->second_child_node)
		{
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> first(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator);
			first.execute();

			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> second(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator);
			second.execute();

			reduceOperator(first.reduceValue, second.reduceValue, &reduceValue);

			lambda_midnodes(this_node, &reduceValue);

			return;
		}

		if (this_node->first_child_node)
		{
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> first(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator);
			first.execute();
			reduceValue = first.reduceValue;

			lambda_midnodes(this_node, &reduceValue);

			return;
		}

		assert(this_node->second_child_node != nullptr);

		CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> second(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator);
		second.execute();
		reduceValue = second.reduceValue;

		lambda_midnodes(this_node, &reduceValue);

		return;
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> t(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator);
	t.execute();

	*o_reduceOutput = t.reduceValue;

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> t(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator);
	t.execute();

	*o_reduceOutput = t.reduceValue;

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce<CLambdaFun1, CLambdaFun2, TReduceValue> t(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator);
	t.execute();

	*o_reduceOutput = t.reduceValue;

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}




#endif
