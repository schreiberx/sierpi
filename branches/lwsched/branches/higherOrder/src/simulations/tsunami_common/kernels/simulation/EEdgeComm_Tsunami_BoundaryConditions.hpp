/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 23, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef EBOUNDARYCONDITIONS_HPP_
#define EBOUNDARYCONDITIONS_HPP_


enum EBoundaryConditions
{
	BOUNDARY_CONDITION_VELOCITY_ZERO,
	BOUNDARY_CONDITION_VELOCITY_DAMPING,
	BOUNDARY_CONDITION_OUTFLOW,
	BOUNDARY_CONDITION_BOUNCE_BACK,
	BOUNDARY_CONDITION_DIRICHLET
};

class CBoundaryConditions
{
public:
	static const char* getString(EBoundaryConditions eBoundaryConditions)
	{
		switch(eBoundaryConditions)
		{
		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			return "velocity zero";

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			return "velocity damping";

		case BOUNDARY_CONDITION_OUTFLOW:
			return "outflow";

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			return "bounce back";

		case BOUNDARY_CONDITION_DIRICHLET:
			return "dirichlet";

		default:
			return "[invalid]";
		}
	}
};


#endif /* CBOUNDARYCONDITIONS_HPP_ */
