/*
 * Edge.hpp
 *
 *  Created on: Apr 18, 2012
 *      Author: breuera
 */

#ifndef EDGE_HPP_
#define EDGE_HPP_

//dependencys for CTsunamiTypes_0thOrder.hpp
#include "../../libmath/CMath.hpp"
#include "../tsunami_common/tsunami_config.h"
#include "../tsunami_common/CTsunamiConfig.hpp"

#include "../tsunami_common/types/CTsunamiTypes_0thOrder.hpp"

namespace oneDimensional {
  template <typename T> class Edge;
}

/**
 * Representation of a edge within the computational domain.
 */
template <typename T> class oneDimensional::Edge {
  //private:

  //! position of the edge within the computational domain
  T position;

  //! edge data, 1: cell on the left side of the edge, 2: cell on the right side of the edge
  CTsunamiEdgeData edgeData[2];


  public:
    /**
     * Constructor of a edge.
     *
     */
    Edge(const T i_position): position(i_position) {
      // set everything to zero
      edgeData[0].h = edgeData[1].h = (T)0;
      edgeData[0].qx = edgeData[1].qx = (T)0;
      edgeData[0].qy = edgeData[1].qy = (T)0;
      edgeData[0].b = edgeData[1].b = (T)0;
    }
    virtual ~Edge() {
      // TODO Auto-generated destructor stub
    }

    /**
     * Get a reference to the data for the left cell (stored on the edge).
     * @return reference to left cell data.
     */
    CTsunamiEdgeData& getLeftCellData() {
      return edgeData[0];
    }

    /**
     * Get a reference to the data for the right cell (stored on the edge).
     * @return reference to right cell data.
     */
    CTsunamiEdgeData& getRightCellData() {
      return edgeData[1];
    }

    /**
     * Dummy implementation of the assignment operator (throws an assertion)
     *
     * @param edge
     * @return
     */
//    Edge& operator=(const Edge& edge) {
//      assert(false);
//      return *this;
//    }

    /**
     * Get the position of the edge.
     * @return edge position.
     */
    const T getPosition() {
      return position;
    }

    /**
     * Set the position of the edge.
     * @param i_position edge position.
     */
    void setPosition(const T i_position) {
      position = i_position;
    }

};


#endif /* EDGE_HPP_ */
