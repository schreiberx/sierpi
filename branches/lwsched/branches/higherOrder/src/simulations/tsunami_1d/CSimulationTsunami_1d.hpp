/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATIONTSUNAMI_1D_HPP_
#define CSIMULATIONTSUNAMI_1D_HPP_


#include "../tsunami_common/tsunami_config.h"

//#include "../../lib/CStopwatch.hpp"

#include "../tsunami_common/types/CTsunamiTypes.hpp"

#include "../../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Element.hpp"
#include "../../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"


#if COMPILE_SIMULATION_WITH_GUI
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
#endif


#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_ElementData_Normal_SelectByPoint.hpp"


#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"
#include "lib/CStopwatch.hpp"

#include "../tsunami_common/CTsunamiSimulationDataSets.hpp"

#include "../tsunami_common/kernels/modifiers/CSetup_ElementData.hpp"


class CSimulationTsunami_1d	: public CTsunamiSimulationParameters
{
public:
	CError error;

	/// traversator to setup the structure stack and element data stack by a given depth
	sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;

	/// to offer single-element modifications by coordinates, this traversal cares about it
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

	/// output element data at given point
	sierpi::kernels::CStringOutput_ElementData_Normal_SelectByPoint<CTsunamiSimulationStacks>::TRAV cStringOutput_ElementData_SelectByPoint;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	/// validation functions
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;
#endif

	/// exchange of edge communication data and timestep on fixed grid
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/// adaptive traversals to refine or coarsen grid cells without having hanging nodes
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData cTsunami_Adaptive;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;
#endif

	/// VTK visualization
	sierpi::kernels::CGetDataSample_Vertices_Element_Tsunami<CTsunamiSimulationStacks>::TRAV cOutputVTK_Vertices_Element_Tsunami;

	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	sierpi::kernels::CSetup_TsunamiElementData_Validation::TRAV cSetup_TsunamiElementData_Validation;
#endif

public:
	CSimulationStacks<CTsunamiSimulationTypes> *cStacks;

	CTriangle_Factory cTriangleFactory;

	/*
	 * part of CFL condition returned by kernels
	 */
	TTsunamiDataScalar part_of_cfl_timestep_condition;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CTsunamiSimulationDataSets cTsunamiSimulationDataSets;


public:
	CSimulationTsunami_1d()	:
		cStacks(NULL),
		cTsunamiSimulationDataSets(*this)
	{
		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = grid_initial_recursion_depth + grid_max_relative_recursion_depth;

		reset_Simulation();
	}


#if COMPILE_SIMULATION_WITH_GUI

	void render_surfaceDefault_simple(
			CGlProgram &cShaderBlinn
	)
	{
	}


	void render_surface_aligned(
			CGlProgram &cShaderBlinn
	)
	{
	}


	void render_terrainBathymetry_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
	}


	void render_terrainBathymetry_aligned(
			CShaderBlinn &cShaderBlinn
	)
	{
	}


	void render_terrainBathymetry_smooth(
			CShaderBlinn &cShaderBlinn
	)
	{
	}


	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
	}


	void render_surfaceWireframe( //TODO: Visualisierung
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cShaderBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cShaderBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cShaderBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));

		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		/**
		 * setup data
		 */

		int num_data = 1024;
		float *dataArray = new float[num_data];

		for (int i = 0; i < num_data; i++)
		{
			dataArray[i] = CMath::sin((float)i/12.0);
		}


		/**
		 * render
		 */

//		int num_vertices = COPENGL_VERTICES_WIREFRAME_ROOT_TSUNAMI_VERTEX_COUNT;

		GLfloat vertexArray[6];
		for (int i = 0; i < num_data-1; i++)
		{
			float x1 = (float)i/(float)num_data - 0.5;
			float x2 = (float)(i+1)/(float)num_data - 0.5;

			vertexArray[0+0] = x1;
			vertexArray[0+1] = dataArray[i]+1.0;
			vertexArray[0+2] = 0;

			vertexArray[3+0] = x2;
			vertexArray[3+1] = dataArray[i+1]+1.0;
			vertexArray[3+2] = 0;

			cOpenGL_Vertices_Wireframe_Root_Tsunami.renderOpenGLVertexArray(vertexArray, 2);
		}


		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_surfaceSmooth(
			CGlProgram &cShaderBlinn
	)
	{
	}


	void render_surfaceSmoothWithHeightColors(
			CGlProgram &cShaderBlinn
	)
	{
	}


#endif


	/**
	 * setup the stacks
	 */
	void setup_Stacks()
	{
		clean();

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		cStacks = new CSimulationStacks<CTsunamiSimulationTypes>(
					(1 << grid_initial_recursion_depth) + (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CTsunamiElementData)),
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS		|
					CSimulationStacks_Enums::VERTEX_COMM_STACKS
				);
#else
		cStacks = new CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData>(
					initial_recursion_depth+max_relative_recursion_depth,
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#endif

	}


	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 1.0;
		cTriangleFactory.vertices[0][1] = -1.0;
		cTriangleFactory.vertices[1][0] = -1.0;
		cTriangleFactory.vertices[1][1] = 1.0;
		cTriangleFactory.vertices[2][0] = -1.0;
		cTriangleFactory.vertices[2][1] = -1.0;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		cTriangleFactory.partitionTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
		cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
		cSetup_Column.setup_sfcMethods(cTriangleFactory);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

		cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(cTriangleFactory);
		cStringOutput_ElementData_SelectByPoint.setup_sfcMethods(cTriangleFactory);

		cModify_OneElementValue_SelectByPoint.setup_sfcMethods(cTriangleFactory);
	}

	void setup()
	{
		reset_Simulation();
	}

	void reset_Simulation()
	{
		part_of_cfl_timestep_condition = 0;

		setup_Stacks();
		setup_TraversatorsAndKernels();
		p_setup_Parameters();
		p_setup_InitialTriangulation();

		reset_simulation_parameters();
	}


	void clean()
	{
		if (cStacks)
		{
			delete cStacks;
			cStacks = NULL;
		}
	}

	unsigned long long setup_ColumnAt2DPosition(
			float x,
			float y,
			float radius
	)
	{
		simulation_dataset_cylinder_posx = x;
		simulation_dataset_cylinder_posy = y;
		simulation_dataset_cylinder_radius = radius;

		return p_adaptive_traversal_setup_column();
	}
	unsigned long long setup_ColumnAt2DPosition()
	{
		return p_adaptive_traversal_setup_column();
	}


	unsigned long long p_adaptive_traversal_setup_column()
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
		cSetup_Column.setup_KernelClass(
				simulation_dataset_cylinder_posx,
				simulation_dataset_cylinder_posy,
				simulation_dataset_cylinder_radius,
				2,
				&cTsunamiSimulationDataSets
		);

#else
		cSetup_Column.setup_KernelClass(
				simulation_dataset_cylinder_posx,
				simulation_dataset_cylinder_posy,
				simulation_dataset_cylinder_radius,
				2,
				&cTsunamiSimulationDataSets
		);
#endif

		unsigned long long prev_number_of_triangles;
		do
		{
			prev_number_of_triangles = number_of_triangles;

			bool repeat_traversal = cSetup_Column.actionFirstTraversal(cStacks);

			while(repeat_traversal)
				repeat_traversal = cSetup_Column.actionMiddleTraversals_Serial(cStacks);

			cSetup_Column.actionLastTraversal_Serial(cStacks);

			number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
		} while (number_of_triangles != prev_number_of_triangles);

		number_of_triangles = prev_number_of_triangles;


		int backupSetupSurfaceMethod = simulation_water_surface_scene_id;
		simulation_water_surface_scene_id = CTsunamiSimulationDataSets::SIMULATION_WATER_HEIGHT_CYLINDER;


		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
		cSetup_ElementData.setup_sfcMethods(cTriangleFactory);
		cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets);
		cSetup_ElementData.action(cStacks);

		simulation_water_surface_scene_id = backupSetupSurfaceMethod;

		return number_of_triangles;
	}

private:
	void p_setup_Parameters()
	{
		cTsunami_EdgeComm.setParameters(simulation_parameter_timestep_size, simulation_parameter_domain_length, 9.81);
		cTsunami_EdgeComm.setBoundaryDirichlet(&dirichlet_boundary_edge_data);

		cTsunami_Adaptive.setup_KernelClass(
				simulation_parameter_domain_length,

				refine_height_threshold,
				coarsen_height_threshold,

				refine_slope_threshold,
				coarsen_slope_threshold,

				&cTsunamiSimulationDataSets
			);

		cTsunami_Adaptive.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);

		cSetup_Column.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);
	}


private:
	void p_setup_InitialTriangulation()
	{
		number_of_triangles = cSetup_Structure_ElementData.setup(cStacks, grid_initial_recursion_depth, &element_data_setup);
		number_of_initial_triangles_after_domain_triangulation = number_of_triangles;

		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
		cSetup_ElementData.setup_sfcMethods(cTriangleFactory);
		cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets);
		cSetup_ElementData.action(cStacks);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.action(cStacks);
#endif
	}


private:
	void p_adaptive_timestep()
	{
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		TTsunamiDataScalar adaptCFL;
		cTsunami_Adaptive.storeReduceValue(&adaptCFL);

		if (adaptCFL != CMath::numeric_inf<TTsunamiDataScalar>())
		{
			part_of_cfl_timestep_condition = CMath::min(adaptCFL, part_of_cfl_timestep_condition);
		}

		number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
	}


private:
	/**
	 * timestep
	 */
	void p_edge_comm_timestep()
	{
		if (adaptive_timestep_size)
		{
			// adaptive timestep size

			simulation_parameter_timestep_size = part_of_cfl_timestep_condition*simulation_parameter_cfl;

			if (simulation_parameter_timestep_size < 0.000001)
				simulation_parameter_timestep_size = 0.000001;
		}

		cTsunami_EdgeComm.actionFirstPass(cStacks);
		cTsunami_EdgeComm.actionSecondPass_Serial(cStacks, simulation_parameter_timestep_size, &part_of_cfl_timestep_condition);
	}

/*

public:
	void refineAt2DPosition(float x, float y, float radius_not_used = 0)
	{
		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_domain_length, refine_height_threshold, -1, refine_slope_threshold, -999, &cTsunamiSimulationDataSets);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(simulation_domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold, &cTsunamiSimulationDataSets);
	}


public:
	void coarsenAt2DPosition(float x, float y)
	{
		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_setup);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_domain_length, 99999, coarsen_height_threshold, 999, coarsen_slope_threshold, &cTsunamiSimulationDataSets);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(simulation_domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold, &cTsunamiSimulationDataSets);
	}

*/

public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		CTsunamiElementData element_data_modifier;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		element_data_modifier.dofs.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.dofs.qx = 0;
		element_data_modifier.dofs.qy = 0;
		element_data_modifier.dofs.b = -simulation_dataset_terrain_default_distance;

#else
		element_data_modifier.hyp_edge.h = simulation_dataset_surface_max_displacement;
		element_data_modifier.hyp_edge.qx = 0;
		element_data_modifier.hyp_edge.qy = 0;
		element_data_modifier.hyp_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.left_edge.h = simulation_dataset_surface_max_displacement;
		element_data_modifier.left_edge.qx = 0;
		element_data_modifier.left_edge.qy = 0;
		element_data_modifier.left_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.right_edge.h = simulation_dataset_surface_max_displacement;
		element_data_modifier.right_edge.qx = 0;
		element_data_modifier.right_edge.qy = 0;
		element_data_modifier.right_edge.b = -simulation_dataset_terrain_default_distance;
#endif

		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_length, refine_height_threshold, -1, refine_slope_threshold, -999, &cTsunamiSimulationDataSets);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold, &cTsunamiSimulationDataSets);
	}


public:
	void runSingleTimestep()
	{
		p_edge_comm_timestep();
		p_adaptive_timestep();

		timestep_nr++;
		time_for_timestep += simulation_parameter_timestep_size;
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_edge_comm_timestep();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_adaptive_timestep();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		timestep_nr++;
		time_for_timestep += simulation_parameter_timestep_size;
	}



public:
	void debugOutput(CVector<2,float> planePosition)
	{
		cStringOutput_ElementData_SelectByPoint.cKernelClass.setup(planePosition[0], planePosition[1]);
		cStringOutput_ElementData_SelectByPoint.action(cStacks->structure_stacks, cStacks->element_data_stacks);
	}


	void writeTrianglesToVTKFile(
			const char *p_filename,
			const char *additional_vtk_info_string = NULL
	)
	{
			std::ofstream vtkfile;
			vtkfile.open(p_filename);

			vtkfile << "# vtk DataFile Version 5.0" << std::endl;
			vtkfile << "Sierpi VTK File";
			if (additional_vtk_info_string != NULL)
				vtkfile << ": " << additional_vtk_info_string;
			vtkfile << std::endl;
			vtkfile << "ASCII" << std::endl;
			vtkfile << "DATASET POLYDATA" << std::endl;

			// output 3 x #triangles vertices
			vtkfile << "POINTS " << (number_of_triangles*3) << " float" << std::endl;

			// ACTION
			cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(&vtkfile);
//			cOutputVTK_Vertices_Element_Tsunami.setup_Stacks(&cStacks->structure_stacks, &cStacks->element_data_stacks);
			cOutputVTK_Vertices_Element_Tsunami.action(cStacks);

			// output 3 x #triangles vertices
			vtkfile << std::endl;
			vtkfile << "TRIANGLE_STRIPS " << (number_of_triangles) << " " << (number_of_triangles*4) << std::endl;
			for (unsigned long long i = 0; i < number_of_triangles; i++)
				vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

			vtkfile.close();
	}

	void keydown(int key)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_ColumnAt2DPosition(-0.5, 0.4, 0.2);
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset_Simulation();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

		}
	}

	virtual ~CSimulationTsunami_1d()
	{
		clean();
	}
};

#endif /* CTSUNAMI_HPP_ */
