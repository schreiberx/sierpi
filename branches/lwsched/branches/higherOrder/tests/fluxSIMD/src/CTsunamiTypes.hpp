/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */

#ifndef CTSUNAMI_TYPES_HPP
#define CTSUNAMI_TYPES_HPP

#include "libmath/CMath.hpp"


typedef float	TTsunamiDataScalar;

typedef float	TTsunamiVertexScalar;
//typedef double	TTsunamiScalar;

class CTsunamiEdgeData
{
public:
	TTsunamiDataScalar h;	// height
	TTsunamiDataScalar qx;	// x-component of velocity
	TTsunamiDataScalar qy;	// y-component of velocity

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	TTsunamiVertexScalar edge_midpoint_x;
	TTsunamiVertexScalar edge_midpoint_y;

	CTsunamiEdgeData()
	{
		edge_midpoint_x = -666;
		edge_midpoint_y = -666;
	}

	CTsunamiEdgeData(const CTsunamiEdgeData &d)
	{
		h = d.h;
		qx = d.qx;
		qy = d.qy;
		edge_midpoint_x = d.edge_midpoint_x;
		edge_midpoint_y = d.edge_midpoint_y;

		return;
	}

	CTsunamiEdgeData& operator=(const CTsunamiEdgeData &d)
	{
		h = d.h;
		qx = d.qx;
		qy = d.qy;

		if (d.edge_midpoint_x == -666)
			return *this;

		edge_midpoint_x = d.edge_midpoint_x;
		edge_midpoint_y = d.edge_midpoint_y;

		return *this;
	}
#endif

	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiEdgeData &d)
	{
		return os << d.h << " " << d.qx << " " << d.qy

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			<< " (" << d.edge_midpoint_x << ", " << d.edge_midpoint_y << ")"
#endif
		;
	}
};


class CTsunamiElementData
{
public:
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData right_edge;
	CTsunamiEdgeData left_edge;

#if 0
	unsigned refine:1;
	unsigned coarsen:1;
#else
	bool refine;
	bool coarsen;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	int depth;
	TTsunamiVertexScalar vertices[3][2];	// left(0) and right(1)vertex at triangle hypotenuse and top(2) vertex of triangle

	CTsunamiElementData& operator=(const CTsunamiElementData &t)
	{
		hyp_edge = t.hyp_edge;
		right_edge = t.right_edge;
		left_edge = t.left_edge;

		refine = t.refine;
		coarsen = t.coarsen;

		if (t.depth != -666)
			depth = t.depth;

		if (t.vertices[0][0] != -666)
			memcpy(vertices, t.vertices, sizeof(vertices));

		return *this;
	}


	CTsunamiElementData(const CTsunamiElementData &t)
	{
		hyp_edge = t.hyp_edge;
		right_edge = t.right_edge;
		left_edge = t.left_edge;

		refine = t.refine;
		coarsen = t.coarsen;

		if (t.depth != -666)
			depth = t.depth;

		if (t.vertices[0][0] != -666)
			memcpy(vertices, t.vertices, sizeof(vertices));
	}

	CTsunamiElementData()
	{
		vertices[0][0] = -666;
		depth = -666;
	}
#endif

	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiElementData &d)
	{
		os << d.hyp_edge << std::endl;
		os << d.right_edge << std::endl;
		os << d.left_edge << std::endl;
		return os;
	}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	void setupMidPointVertices()
	{
		hyp_edge.edge_midpoint_x = (vertices[0][0]+vertices[1][0])*0.5;
		hyp_edge.edge_midpoint_y = (vertices[0][1]+vertices[1][1])*0.5;

		right_edge.edge_midpoint_x = (vertices[1][0]+vertices[2][0])*0.5;
		right_edge.edge_midpoint_y = (vertices[1][1]+vertices[2][1])*0.5;

		left_edge.edge_midpoint_x = (vertices[2][0]+vertices[0][0])*0.5;
		left_edge.edge_midpoint_y = (vertices[2][1]+vertices[0][1])*0.5;
	}
#endif
};

/*
inline void getUnitCatAndHypLengthForDepth(
		int i_depth,
		TTsunamiDataScalar *o_catLength,
		TTsunamiDataScalar *hypLength
)
{
	int depth_even = i_depth >> 1;

	o_catLength = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		o_catLength = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*o_catLength;




	i_depth += 1;
	int depth_even = i_depth >> 1;

	TTsunamiDataScalar hypotenuse = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		hypotenuse = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*hypotenuse;

	return hypotenuse*2.0;
}*/



/**
 * TODO: maybe we can use a lookup table to speed up this methods
 */
inline TTsunamiDataScalar getUnitCathetusLengthForDepth(int i_depth)
{
	int depth_even = i_depth >> 1;

	TTsunamiDataScalar cathetus = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		cathetus = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*cathetus;

	return cathetus;
}


inline TTsunamiDataScalar getUnitHypotenuseLengthForDepth(int i_depth)
{
	i_depth += 1;
	int depth_even = i_depth >> 1;

	TTsunamiDataScalar hypotenuse = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		hypotenuse = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*hypotenuse;

	return hypotenuse*2.0;
}
#endif
