#ifndef KERNELS_H
#define KERNELS_H

#include <string.h>

void memCpyBlock(float *dst, float *src, size_t size);

void typeFloatMulBlock(float *dst, float *src, size_t size);

void typeFloatBlock(float *dst, float *src, size_t size);

void typeVec4SIMDBlock(float *dst, float *src, size_t size);

void floatBlockAsInt4(float *dst, float *src, size_t size);


#endif
