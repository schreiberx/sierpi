/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef CTRAVERSATORS_CSTRUCTURE_SETUP_H_
#define CTRAVERSATORS_CSTRUCTURE_SETUP_H_
/*
 * Include a string for making strlen works
 */
#include <string.h>
#include "CFBStacks.hpp"

namespace sierp
{
namespace travs
{

class CSetup_Structure
{
public:
	CStack<char> *structure_stack;

	void setup(CFBStacks<char> &p_stacks, size_t depth)
	{
		if (p_stacks.direction == p_stacks.FORWARD)
		{
			structure_stack = &p_stacks.forward;
		}
		else
		{
			structure_stack = &p_stacks.backward;
		}
		assert(structure_stack->isEmpty());

		pushStackElement(depth);
		pushStackElement(depth);
	}

	void setup(
			CFBStacks<char> &p_stacks,
			const char *structure_string,
			bool reversed = false
	)
	{
		if (p_stacks.direction == p_stacks.FORWARD)
		{
			structure_stack = &p_stacks.forward;
		}
		else
		{
			structure_stack = &p_stacks.backward;
		}


		if (!reversed)
		{
			for (const char *c = structure_string; *c != '\0'; c++)
				structure_stack->push((*c == '0' ? 0 : 1));
		}
		else
		{
			size_t length = strlen(structure_string);

			for (int i = length-1; i >= 0; i--)
				structure_stack->push((structure_string[i] == '0' ? 0 : 1));
		}

		assert(structure_stack->structure_isValidQuad());
	}

private:
	void pushStackElement(size_t depth)
	{
		assert(depth >= 1);
		depth--;

		if (depth == 0)
		{
			structure_stack->push(0);
			return;
		}

		pushStackElement(depth);
		pushStackElement(depth);

		// postfix push
		structure_stack->push(1);
	}
};

}
}

#endif /* CSTRUCTURE_SETUP_H_ */
