/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTriangle_Normals.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRIANGLE_NORMALS_HPP_
#define CTRIANGLE_NORMALS_HPP_

#include "CTriangle_Factory.hpp"

class CTriangle_Normals
{
public:
	static CTriangle_Factory::TTriangleFactoryScalarType normal_table[8][2];
};

#endif /* CTRIANGLE_NORMALS_H_ */
