/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_TSUNAMI_PARALLEL_FILE_OUTPUT_HPP_
#define CSIMULATION_TSUNAMI_PARALLEL_FILE_OUTPUT_HPP_


#include "config.h"

#include "../tsunami_common/kernels/backends/CSimulationTsunami_OutputGridDataArrays.hpp"
#include "../tsunami_common/kernels/backends/CGetDataSample_Vertices_Cell_Tsunami.hpp"

#include "CSimulationTsunami_Cluster.hpp"
#include "libsierpi/parallelization/CDomainClusters.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

#include "CSimulation_MainInterface_FileOutput.hpp"
#include "../tsunami_common/CSimulationTsunami_GridDataArrays.hpp"
#include "lib/CVtkXMLTrianglePolyData.hpp"


class CSimulationTsunami_Parallel_FileOutput	:
	public CSimulation_MainInterface_FileOutput
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationTsunami_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

	/**
	 * typedef sfor domain clusters
	 */
	typedef sierpi::CDomainClusters<CSimulationTsunami_Cluster> CDomainClusters_;


public:
	CTsunamiSimulationParameters &cTsunamiSimulationParameters;
	sierpi::CDomainClusters<CSimulationTsunami_Cluster> &cDomainClusters;
	CTsunamiSimulationScenarios &cTsunamiSimulationScenarios;

	/**
	 * constructor for parallel tsunami simulation
	 */
	CSimulationTsunami_Parallel_FileOutput(
			CTsunamiSimulationParameters &i_cTsunamiSimulationParameters,
			CDomainClusters_ &i_cDomainClusters,
			CTsunamiSimulationScenarios &i_cTsunamiSimulationScenarios
	)	:
			cTsunamiSimulationParameters(i_cTsunamiSimulationParameters),
			cDomainClusters(i_cDomainClusters),
			cTsunamiSimulationScenarios(cTsunamiSimulationScenarios)
	{
	}

/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	/**
	 * write simulation data to file
	 *
	 * if some information can be included in the file, also write i_information_string to the file
	 */
	void writeSimulationDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		int flags =
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::H		|
				CSimulationTsunami_GridDataArrays::HU		|
				CSimulationTsunami_GridDataArrays::HV		|
				CSimulationTsunami_GridDataArrays::B		|
				CSimulationTsunami_GridDataArrays::CFL_VALUE_HINT;

		CSimulationTsunami_GridDataArrays cGridDataArrays(
				cTsunamiSimulationParameters.number_of_local_cells,
				1,
				1,
				flags
			);

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = cGridDataArrays.getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

					cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cOutputGridDataArrays.cKernelClass.setup(
							&cGridDataArrays,
							offset,
							flags
						);

					cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);

		assert(cGridDataArrays.number_of_triangle_cells == cTsunamiSimulationParameters.number_of_local_cells);

		/*
		 * postprocessing
		 */
		if (	cTsunamiSimulationParameters.visualization_domain_scale_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_z != 1.0 ||

				cTsunamiSimulationParameters.visualization_domain_translate_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_z != 1.0
		)
		{
			for (int i = 0; i < cTsunamiSimulationParameters.number_of_local_cells; i++)
			{
				TTsunamiVertexScalar *v = &(cGridDataArrays.triangle_vertex_buffer[3*3*i]);

				for (int vn = 0; vn < 3; vn++)
				{
					v[3*vn+0] = (v[3*vn+0]-cTsunamiSimulationParameters.simulation_parameter_domain_origin_x + cTsunamiSimulationParameters.visualization_domain_translate_x)*cTsunamiSimulationParameters.visualization_domain_scale_x + cTsunamiSimulationParameters.simulation_parameter_domain_origin_x;
					v[3*vn+1] = (v[3*vn+1]-cTsunamiSimulationParameters.simulation_parameter_domain_origin_y + cTsunamiSimulationParameters.visualization_domain_translate_y)*cTsunamiSimulationParameters.visualization_domain_scale_y + cTsunamiSimulationParameters.simulation_parameter_domain_origin_y;
					v[3*vn+2] = (v[3*vn+2] + cTsunamiSimulationParameters.visualization_domain_translate_y)*cTsunamiSimulationParameters.visualization_domain_scale_z;
				}
			}
		}

		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;
			cVtkXMLTrianglePolyData.setup();
			cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
			cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.h);
			cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.hv);
			cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.hu);
			cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.b);
			cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.cfl_value_hint);
		cVtkXMLTrianglePolyData.write(i_filename);
	}



	/**
	 * output clusters to vtk file
	 */

	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		TTsunamiVertexScalar origin_x = 0;
		TTsunamiVertexScalar origin_y = 0;
		TTsunamiVertexScalar size_x = 0;
		TTsunamiVertexScalar size_y = 0;

		cTsunamiSimulationScenarios.getTerrainOriginAndSize(
				&origin_x,
				&origin_y,
				&size_x,
				&size_y
			);

		TTsunamiVertexScalar *cluster_vertex_buffer = new TTsunamiVertexScalar[cTsunamiSimulationParameters.number_of_local_clusters*3*3];
		size_t local_cluster_offset = 0;

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t offset = local_cluster_offset;
					local_cluster_offset++;

					TTsunamiVertexScalar *v = &(cluster_vertex_buffer[offset*3*3]);

					v[0*3+0] = node->cTriangleFactory.vertices[0][0];
					v[0*3+1] = node->cTriangleFactory.vertices[0][1];
					v[0*3+2] = 0;

					v[1*3+0] = node->cTriangleFactory.vertices[1][0];
					v[1*3+1] = node->cTriangleFactory.vertices[1][1];
					v[1*3+2] = 0;

					v[2*3+0] = node->cTriangleFactory.vertices[2][0];
					v[2*3+1] = node->cTriangleFactory.vertices[2][1];
					v[2*3+2] = 0;
				}
		);


		assert(local_cluster_offset == (size_t)cTsunamiSimulationParameters.number_of_local_clusters);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;
			cVtkXMLTrianglePolyData.setup();
			cVtkXMLTrianglePolyData.setTriangleVertexCoords(cluster_vertex_buffer, cTsunamiSimulationParameters.number_of_local_clusters);
			delete cluster_vertex_buffer;

#if CONFIG_ENABLE_SCAN_DATA
			int *cluster_id_buffer = new int[cTsunamiSimulationParameters.number_of_local_clusters];
			size_t local_cluster_info_offset;

			cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = local_cluster_info_offset;
					local_cluster_info_offset++;

					cluster_id_buffer[offset] = i_cGenericTreeNode->workload_thread_id;
				}
			);

			assert(local_cluster_info_offset == (size_t)cTsunamiSimulationParameters.number_of_local_clusters);

			cVtkXMLTrianglePolyData.setCellDataInt("cluster_id", cluster_id_buffer);
			delete cluster_id_buffer;
#endif


		cVtkXMLTrianglePolyData.write(i_filename);
	}
};


#endif
