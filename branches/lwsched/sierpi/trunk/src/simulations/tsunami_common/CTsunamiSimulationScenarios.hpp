/*
 * CTsunamiSimulationDataSets.hpp
 *
 *  Created on: Mar 16, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMISIMULATION_DATASETS_HPP_
#define CTSUNAMISIMULATION_DATASETS_HPP_

#include "CTsunamiSimulationParameters.hpp"
#include "libmath/CMath.hpp"

#if CONFIG_ENABLE_ASAGI
#include "datasets/CAsagi.hpp"
#endif


#if CONFIG_ENABLE_SIMPLE_NETCDF
#include "datasets/CSimpleNetCDF.hpp"
#endif

#include "benchmarks/CSingleWaveOnSingleBeach.hpp"



class CTsunamiSimulationScenarios	:
	public CTsunamiSimulationTypes	///< import tsunami simulation types
{
public:
	CSingleWaveOnSingleBeach<T> cSingleWaveOnSingleBeach;

	/*
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use simple netcdf
		 */
		SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF = -2,

		/*
		 * use asagi to determine water surface height
		 */
		SIMULATION_WATER_HEIGHT_ASAGI = -1,

		/*
		 * set simulation water surface height to zero
		 */
		SIMULATION_WATER_DEFAULT_HEIGHT = 0,

		/*
		 * setup water surface height with cylinder parameters (simulation_dataset_cylinder*)
		 */
		SIMULATION_WATER_HEIGHT_CYLINDER = 1,
		SIMULATION_WATER_HEIGHT_LINEAR_SLOPE = 2,
		SIMULATION_WATER_HEIGHT_SQUARE = 3,
		SIMULATION_WATER_HEIGHT_SQUARE_DIST = 4,
		SIMULATION_WATER_HEIGHT_PARABOLA = 5,
		SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY = 6,
		SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF = 7,
		SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK = 8,
		SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 9
	};


	/**
	 * possible choices to setup water height
	 */
	enum
	{
		SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF = -2,
		SIMULATION_TERRAIN_HEIGHT_ASAGI = -1,
		SIMULATION_TERRAIN_HEIGHT_BASE = 0,
		SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE = 1,
		SIMULATION_TERRAIN_HEIGHT_PARABOLA = 2,
		SIMULATION_TERRAIN_HEIGHT_FANCY = 3,
		SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 8
	};


	CTsunamiSimulationParameters &cTsunamiSimulationParameters;

	int verbosity_level;


	/**
	 * global singleton for asagi
	 */
#if CONFIG_ENABLE_ASAGI
	CAsagi *cAsagi;
#endif

#if CONFIG_ENABLE_SIMPLE_NETCDF
	CSimpleNetCDF *cSimpleNetCDF;
#endif



public:
	CTsunamiSimulationScenarios(
			CTsunamiSimulationParameters &i_cTsunamiSimulationParameters,
			int i_verbosity_level
	)	:
			cTsunamiSimulationParameters(i_cTsunamiSimulationParameters),
			verbosity_level(i_verbosity_level)
#if CONFIG_ENABLE_ASAGI
			,
			cAsagi(nullptr)
#endif
#if CONFIG_ENABLE_SIMPLE_NETCDF
			,
			cSimpleNetCDF(nullptr)
#endif
	{
	}


	void setup()
	{
#if CONFIG_ENABLE_ASAGI
		// avoid double setup
		if (cAsagi == nullptr)
		{
			if (	cTsunamiSimulationParameters.simulation_dataset_terrain_file.size() > 0		&&
					cTsunamiSimulationParameters.simulation_dataset_water_surface_displacement_file.size() > 0
			)
			{
				cAsagi = new CAsagi();

				cAsagi->loadDataset(
						cTsunamiSimulationParameters.simulation_dataset_terrain_file.c_str(),
						cTsunamiSimulationParameters.simulation_dataset_water_surface_displacement_file.c_str()
				);

				if (!cAsagi->isDatasetLoaded())
				{
					delete cAsagi;
					cAsagi = nullptr;
				}
				else
				{
					cAsagi->getOriginAndSize(
							&cTsunamiSimulationParameters.simulation_parameter_domain_origin_x,
							&cTsunamiSimulationParameters.simulation_parameter_domain_origin_y,
							&cTsunamiSimulationParameters.simulation_parameter_domain_size_x,
							&cTsunamiSimulationParameters.simulation_parameter_domain_size_y
						);
				}
			}
		}
#endif

#if CONFIG_ENABLE_SIMPLE_NETCDF
		// avoid double setup
		if (cSimpleNetCDF == nullptr)
		{
			if (	cTsunamiSimulationParameters.simulation_dataset_terrain_file.size() > 0		&&
					cTsunamiSimulationParameters.simulation_dataset_water_surface_displacement_file.size() > 0
			)
			{
				cSimpleNetCDF = new CSimpleNetCDF();

				cSimpleNetCDF->loadDataset(
						cTsunamiSimulationParameters.simulation_dataset_terrain_file.c_str(),
						cTsunamiSimulationParameters.simulation_dataset_water_surface_displacement_file.c_str()
				);

				if (!cSimpleNetCDF->isDatasetLoaded())
				{
					delete cSimpleNetCDF;
					cSimpleNetCDF = nullptr;
				}
				else
				{
					cSimpleNetCDF->getOriginAndSize(
							&cTsunamiSimulationParameters.simulation_parameter_domain_origin_x,
							&cTsunamiSimulationParameters.simulation_parameter_domain_origin_y,
							&cTsunamiSimulationParameters.simulation_parameter_domain_size_x,
							&cTsunamiSimulationParameters.simulation_parameter_domain_size_y
						);
				}
			}
		}
#endif
	}


public:
	~CTsunamiSimulationScenarios()
	{
#if CONFIG_ENABLE_ASAGI
		if (cAsagi != nullptr)
		{
			cAsagi = nullptr;
			delete cAsagi;
		}
#endif


#if CONFIG_ENABLE_SIMPLE_NETCDF
		if (cSimpleNetCDF != nullptr)
		{
			cSimpleNetCDF = nullptr;
			delete cSimpleNetCDF;
		}
#endif
	}



public:
	/**
	 * terrain dimensions
	 */
	inline void getTerrainOriginAndSize(
			T *o_origin_x,	///< origins x-coordinate
			T *o_origin_y,	///< origins y-coordinate
			T *o_size_x,	///< size of terrain in x-dimension
			T *o_size_y		///< size of terrain in y-dimension
	)
	{
#if CONFIG_ENABLE_ASAGI
		if (cTsunamiSimulationParameters.simulation_terrain_scene_id == -1 && cAsagi != nullptr)
		{
			cAsagi->getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			return;
		}
#endif

#if CONFIG_ENABLE_SIMPLE_NETCDF
		if (cTsunamiSimulationParameters.simulation_terrain_scene_id == -2 && cSimpleNetCDF != nullptr)
		{
			cSimpleNetCDF->getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			return;
		}
#endif

		*o_origin_x = 0;
		*o_origin_y = 0;

		*o_size_x = cTsunamiSimulationParameters.simulation_parameter_domain_size_x;
		*o_size_y = cTsunamiSimulationParameters.simulation_parameter_domain_size_y;
	}

public:
	/**
	 * terrain setup
	 */
	inline T getTerrainHeightByPosition(
			T i_x,
			T i_y,
			T i_level_of_detail
	)
	{
		switch(cTsunamiSimulationParameters.simulation_terrain_scene_id)
		{
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
#if CONFIG_ENABLE_ASAGI
				if (cAsagi != nullptr)
					return cAsagi->getBathymetryData(i_x, i_y, i_level_of_detail);
#endif
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;


		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
#if CONFIG_ENABLE_SIMPLE_NETCDF
				if (cSimpleNetCDF != nullptr)
					return cSimpleNetCDF->getBathymetryData(i_x, i_y, i_level_of_detail);
#endif
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;


		case SIMULATION_TERRAIN_HEIGHT_BASE:
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;

		case SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE:
			return (i_x-cTsunamiSimulationParameters.simulation_parameter_domain_origin_x)/(cTsunamiSimulationParameters.simulation_parameter_domain_size_x*0.5)*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;

		case SIMULATION_TERRAIN_HEIGHT_PARABOLA:
			return -(T)2.0*((T)1.0-CMath::max(i_x*i_x, i_y*i_y))*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance +
					cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance*(T)0.3;

		case SIMULATION_TERRAIN_HEIGHT_FANCY:
		{
			T d = (T)-1.0;

			d = CMath::max(d, i_x*i_x*(T)0.9-(T)0.8);

			if (i_x*i_x + i_y*i_y < (T)0.05)
				d = (T)0.15;

			T nx = i_x-(T)1.0;
			d = CMath::max(d, nx*nx*nx*(T)0.3+nx*nx*(T)1.0+nx+(T)0.29);

			d -= 0.1;
			return d*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance*0.01;
		}

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			return cSingleWaveOnSingleBeach.getBathymetry(i_x);

		default:
			return -cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;
		}
	}


public:
	/**
	 * return whether the maximum function should be used to update the water surface height
	 */
	inline bool getUseMaximumFunctionForSurfaceHeightSetup()
	{
		switch(cTsunamiSimulationParameters.simulation_water_surface_scene_id)
		{
//		case SIMULATION_WATER_HEIGHT_CYLINDER:
//			return true;

		case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF:
			return true;

//		case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
//			return true;

		default:
			return false;
		}
	}


public:
	/**
	 * return surface relative to horizon height by position
	 */
	inline CTsunamiSimulationTypes::T getWaterSurfaceHeightByPosition(
					CTsunamiSimulationTypes::T i_x,
					CTsunamiSimulationTypes::T i_y,
					CTsunamiSimulationTypes::T i_level_of_detail
				)
	{
		switch(cTsunamiSimulationParameters.simulation_water_surface_scene_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
			if (cAsagi != nullptr)
				return cAsagi->getDisplacementData(i_x, i_y, i_level_of_detail);
#endif
			return 0;


#if CONFIG_ENABLE_SIMPLE_NETCDF
		case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
			if (cSimpleNetCDF != nullptr)
				return cSimpleNetCDF->getDisplacementData(i_x, i_y, i_level_of_detail);
#endif
			return 0;


		case SIMULATION_WATER_DEFAULT_HEIGHT:
			return cTsunamiSimulationParameters.simulation_dataset_water_surface_default_displacement;

		case SIMULATION_WATER_HEIGHT_CYLINDER:
		{
			CTsunamiSimulationTypes::T rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posx);
			CTsunamiSimulationTypes::T ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry < cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius*cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius)
				return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
			else
				return 0;
		}

		case SIMULATION_WATER_HEIGHT_LINEAR_SLOPE:
			return (i_x-cTsunamiSimulationParameters.simulation_parameter_domain_origin_x)/(cTsunamiSimulationParameters.simulation_parameter_domain_size_x*0.5)*cTsunamiSimulationParameters.simulation_dataset_terrain_default_distance;

		case SIMULATION_WATER_HEIGHT_SQUARE:
		{
			if (i_x > -1.0+1.0/CMath::pow(2.0, 2.0)+0.0001)
				return 0;
			return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
		}

		case SIMULATION_WATER_HEIGHT_SQUARE_DIST:
		{
			if (i_x < -1.0+1.0/CMath::pow(2.0, 4.0)+0.0001 || i_x > -1.0+1.0/CMath::pow(2.0, 2.0)+0.0001)
				return 0;
			return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
		}

		case SIMULATION_WATER_HEIGHT_PARABOLA:
			return 1.0-CMath::max(i_x*i_x, i_y*i_y);


		/*
		 * setup a cylinder relative to bathymetry if bathymetry is higher than 0
		 */
		case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
		{
			CTsunamiSimulationTypes::T rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posx);
			CTsunamiSimulationTypes::T ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry > cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius*cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius)
				return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method

			return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
		}

		case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF:
		{
			CTsunamiSimulationTypes::T rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posx);
			CTsunamiSimulationTypes::T ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry < cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius*cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius)
				return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
			else
				return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method
		}


		case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK:
		{
			CTsunamiSimulationTypes::T rx = (i_x - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posx);
			CTsunamiSimulationTypes::T ry = (i_y - cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry < cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius*cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius)
				return cTsunamiSimulationParameters.simulation_dataset_breaking_dam_height;
			else
				return 0;
		}

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
		{
			return cSingleWaveOnSingleBeach.getWaterHeight(i_x);
		}

		default:
			return 0;
		}
	}


	void outputVerboseInformation()
	{
#if CONFIG_ENABLE_ASAGI
		if (cAsagi != nullptr)
			cAsagi->outputVerboseInformation();
#endif

#if CONFIG_ENABLE_SIMPLE_NETCDF
		if (cSimpleNetCDF != nullptr)
			cSimpleNetCDF->outputVerboseInformation();
#endif
	}
};



#endif /* CTSUNAMISIMULATIONDATASETS_HPP_ */
