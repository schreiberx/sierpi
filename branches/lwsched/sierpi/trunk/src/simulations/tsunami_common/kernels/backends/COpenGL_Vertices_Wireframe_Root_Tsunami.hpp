/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_WIREFRAME_ROOT_TSUNAMI_HPP_
#define COPENGL_VERTICES_WIREFRAME_ROOT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlViewport.hpp"

class COpenGL_Vertices_Wireframe_Root_Tsunami
{
	typedef CTsunamiSimulationStacksAndTypes::CVisualizationTypes::T	T;

private:
	CGlVertexArrayObject vao;
	CGlBuffer vertex_buffer;

	const size_t max_number_of_vertices;

public:
	CError error;

	COpenGL_Vertices_Wireframe_Root_Tsunami()	:
		max_number_of_vertices(128*3)
	{
		GLint glfloat = (sizeof(T) == 4 ? GL_FLOAT : GL_DOUBLE);



		vao.bind();
			vertex_buffer.bind();
			vertex_buffer.resize(max_number_of_vertices*sizeof(T)*3);

			// vertex coordinates
			glVertexAttribPointer(0, 3, glfloat, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);
		vao.unbind();
	}




	/**
	 * initialize rendering
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRendering()
	{
		/**
		 * !!! We have to do the setup right here since other threads are not allowed to access the context !!!
		 */
		vao.bind();

		glDisable(GL_CULL_FACE);
		glClear(GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);

		CGlErrorCheck();
	}



	inline void render(
		T *i_vertex_buffer_data,	///< pointer to vertices
		size_t i_vertices_count		///< number of vertices
	)
	{
		for (size_t i = 0; i < i_vertices_count; i += max_number_of_vertices)
		{
			size_t block_size = CMath::min<size_t>(max_number_of_vertices, i_vertices_count - i);

			vertex_buffer.bind();
			vertex_buffer.subData(0, block_size*3*sizeof(T), i_vertex_buffer_data);

			glDrawArrays(GL_LINES, 0, block_size);

			i_vertex_buffer_data += 3*block_size;
		}
	}



	inline void shutdownRendering()
	{
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		vao.unbind();
		CGlErrorCheck();
	}

};


#endif
