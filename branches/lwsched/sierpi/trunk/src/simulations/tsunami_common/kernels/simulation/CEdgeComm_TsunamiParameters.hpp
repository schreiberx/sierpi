/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_PARAMETERS_HPP_
#define CEDGECOMM_TSUNAMI_PARAMETERS_HPP_

#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"


/**
 * common parameters used for edge comm based tsunami simulation
 */
class CEdgeComm_TsunamiParameters
{
public:
	typedef TTsunamiDataScalar T;

	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar cathetus_real_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiSimulationEdgeData boundary_dirichlet;

	/**
	 * set-up boundary condition
	 */
	EBoundaryConditions eBoundaryCondition;

	/**
	 * damping factor when velocity damping boundary condition is used
	 */
	TTsunamiDataScalar eBoundaryConditionVelocityDampingFactor;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;



	/******************************************************
	 * CFL condition stuff
	 *
	 * TODO: the CFL condition also has to be built-in into the
	 * adaptive traversals (due to refine operations)
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;

	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
//	TReduceValue cfl_domain_size_div_max_wave_speed;

	TTsunamiDataScalar cfl1_max_value;
	TTsunamiDataScalar cfl1_min_value;

	/**
	 * inner radius for rectangular triangle:
	 *
	 * r = ab/(a+b+c)
	 *
	 * a = i
	 * b = i
	 * c = i sqrt(2)
	 *
	 * r = i^2 / (2i + i sqrt(2)) = i / (2 + sqrt(2))
	 */
	TTsunamiDataScalar incircle_unit_diameter;


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
	/**
	 * refine and coarsen thresholds
	 */
	TTsunamiDataScalar refine_threshold;
	TTsunamiDataScalar coarsen_threshold;
#endif


	/**
	 * setup with default values
	 */
	CEdgeComm_TsunamiParameters()	:
		eBoundaryCondition(BOUNDARY_CONDITION_OUTFLOW),
		eBoundaryConditionVelocityDampingFactor(0.9)
	{
		cfl1_max_value = CMath::numeric_inf<T>();
		cfl1_min_value = CMath::numeric_inf<T>();

		timestep_size = -CMath::numeric_inf<T>();
		cathetus_real_length = -CMath::numeric_inf<T>();
		gravitational_constant = -CMath::numeric_inf<T>();

		incircle_unit_diameter = (TTsunamiDataScalar)(2.0/(2.0+CMath::sqrt(2.0)));


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		refine_threshold = 999999;
		coarsen_threshold = 0;
#endif
	}





	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-cluster
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl1_min_value = CMath::numeric_inf<T>();
		cfl1_max_value = 0;
	}


	/**
	 * update the cluster timestep size data with net updates from adjacent triangles
	 */
	inline void updateTimestepSizeWithNetUpdates(
			CStack<CTsunamiSimulationEdgeData> &i_edge_data
	)
	{
		size_t s = i_edge_data.getNumberOfElementsOnStack();
		for (size_t i = 0; i < s; i++)
		{
			updateCFL1Value(i_edge_data.getElementAtIndex(i).CFL1_scalar);
		}
	}


	inline void updateCFL1Value(
			TTsunamiDataScalar i_cfl1_value
	)
	{
		assert(i_cfl1_value > 0);

		cfl1_min_value = std::min(cfl1_min_value, i_cfl1_value);
		cfl1_max_value = std::max(cfl1_max_value, i_cfl1_value);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-cluster
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * return the reduce value for the traversator
	 */
	inline void storeReduceValue(TTsunamiDataScalar *o_reduceValue)
	{
		*o_reduceValue = cfl1_min_value;
	}


	/**
	 * return the set-up timestep size
	 */
	inline TTsunamiDataScalar getTimestepSize()
	{
		return cfl1_min_value;
	}




	/**
	 * \brief set the timestep size
	 */
	inline void setTimestepSize(T i_timestep_size)
	{
		timestep_size = i_timestep_size;
	}


	/**
	 * \brief set the gravitation
	 *
	 * WARNING: this value is not always used by the fluxes in the current state!
	 */
	inline void setGravitationalConstant(T i_gravitational_constant)
	{
		gravitational_constant = i_gravitational_constant;
	}


	/**
	 * \brief set the length of a catheti
	 */
	inline void setSquareSideLength(T i_square_side_length)
	{
		cathetus_real_length = i_square_side_length;
	}


	/**
	 * boundary condition
	 */
	void setBoundaryCondition(
			EBoundaryConditions i_eBoundaryCondition
	)
	{
		eBoundaryCondition = i_eBoundaryCondition;
	}


	/**
	 * setup parameter for dirichlet boundary condition
	 */
	void setBoundaryDirichlet(
			const CTsunamiSimulationEdgeData *p_boundary_dirichlet
	)
	{
		boundary_dirichlet = *p_boundary_dirichlet;
	}



	/**
	 * \brief set all parameters for simulation
	 */
	inline void setParameters(
			T i_timestep_size,					///< timestep size
			T i_square_side_length,				///< length of a square (a single catheti)
			T i_gravitational_constant = 9.81	///< gravitational constant
	)
	{
		timestep_size = i_timestep_size;
		gravitational_constant = i_gravitational_constant;
		cathetus_real_length = i_square_side_length;
	}

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2

	inline void setAdaptivityParameters(
			T i_refine_threshold,
			T i_coarsen_threshold
	)
	{
		refine_threshold = i_refine_threshold;
		coarsen_threshold = i_coarsen_threshold;
	}
#endif


	/**
	 * \brief compute CFL, update sub-cluster local CFL and return local CFL
	 */
	inline T getCFLCellFactor(
			int i_depth
	)
	{
		return getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;
	}
};



#endif /* CEDGECOMM_TSUNAMI_CONFIG_HPP_ */
