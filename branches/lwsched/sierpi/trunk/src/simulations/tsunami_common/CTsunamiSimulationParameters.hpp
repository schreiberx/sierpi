/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationParameters.hpp
 *
 *  Created on: Jul 5, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMI_SIMULATION_PARAMETERS_HPP_
#define CTSUNAMI_SIMULATION_PARAMETERS_HPP_

#include <stdint.h>

#include "xmlconfig/CXMLConfig.hpp"
#include "types/CTsunamiTypes.hpp"
#include "libmath/CMath.hpp"

#include "kernels/simulation/EEdgeComm_Tsunami_BoundaryConditions.hpp"


#include "../../CSimulation_MainParameters.hpp"

#if CONFIG_SIERPI_ENABLE_GUI
	#include "../../CSimulation_MainGuiParameters.hpp"

	#include "libgl/hud/CGlHudConfig.hpp"
	#include "mainvis/CGuiConfig.hpp"
#endif

/**
 * simulation parameters used by serial and parallel simulation
 */
class CTsunamiSimulationParameters	:
	public CXMLConfigInterface,
	public CSimulation_MainParameters
#if CONFIG_SIERPI_ENABLE_GUI
	,
	public CSimulation_MainGuiParameters
#endif
{
	typedef TTsunamiDataScalar T;

public:

	/**
	 * reset simulation
	 */
	bool simulation_reset;

	/**
	 * initial recursion depth during setup
	 */
	int grid_initial_recursion_depth;

	/**
	 * initial recursion depth during setup including update with cluster split value
	 */
	int grid_initial_recursion_depth_with_initial_cluster_splits;

	/**
	 * minimum and maximum recursion depth during refinements / coarsenings
	 */
	int grid_min_relative_recursion_depth;
	int grid_max_relative_recursion_depth;


	/**
	 * initial splittings of clusters to refine base triangulation
	 */
	int grid_initial_cluster_splits;

	/**
	 * thresholds for coarsening/refinement
	 */
	T adaptive_coarsen_threshold;
	T adaptive_refine_threshold;

	T adaptive_coarsen_slope_threshold;
	T adaptive_refine_slope_threshold;


	/**
	 * element data during setup
	 */
	CTsunamiSimulationCellData simulation_parameter_cell_data_setup;

	/**
	 * boundary conditions
	 */
	int simulation_domain_boundary_condition;

	/**
	 * Dirichlet boundary edge data
	 */
	CTsunamiSimulationEdgeData simulation_domain_boundary_dirichlet_edge_data;


	/**
	 * adaptively set the timestep size by using CFL condition
	 */
	bool simulation_parameter_adaptive_timestep_size_with_cfl;


	/**
	 * Root clusters per unit domain in the direction of the x axis.
	 *
	 * This is important to setup the domain length parameters for root triangles
	 * when more than 2 triangles are used to set-up the domain
	 */
	T simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain;

	/**
	 * Gravitation for simulation (not implemented everywhere)
	 */
	T simulation_parameter_gravitation;

	/**
	 * CFL condition for adaptive timestep size
	 */
	T simulation_parameter_cfl;

	/**
	 * Minimum time-step allowed for simulation
	 */
	T simulation_parameter_minimum_timestep_size;


	/**
	 * dry threshold when to assume that all cells are dryed
	 */
	T simulation_parameter_dry_cell_threshold;

	/**
	 * number of triangles when to split the cluster
	 */
	long long cluster_split_workload_size;


	/**
	 * number of overall triangles of 2 clusters when to join the cluster
	 */
	long long cluster_join_workload_size;

	long long cluster_scan_split_min_workload_size;
	long long cluster_scan_join_max_workload_size;


	/**
	 * Set the number of triangles in a single cluster when to split/join
	 */
	long long cluster_update_split_join_size_after_elapsed_timesteps;

	/**
	 * Scalar to use for non-table based computation of optimal split/join size
	 */
	int cluster_update_split_join_size_after_elapsed_scalar;

	/**
	 * split and join each nth timestep
	 */
	int cluster_split_and_join_every_nth_timestep;



	/**
	 * world scene (base triangulation)
	 */
	int simulation_world_scene_id;

	/**
	 * terrain scene id
	 */
	int simulation_terrain_scene_id;

	/**
	 * water surface scene id
	 */
	int simulation_water_surface_scene_id;

	/**
	 * emit random raindrops for testing purpose
	 */
	bool simulation_random_raindrops_activated;



	/*******************************************************
	 * DATASET RELATED PARAMETERS
	 */

	/**
	 * parameters for default setup of column
	 */
	T simulation_dataset_breaking_dam_posx;
	T simulation_dataset_breaking_dam_posy;
	T simulation_dataset_breaking_dam_radius;
	T simulation_dataset_breaking_dam_height;

	/**
	 * value to setup the fluid surface height
	 */
	T simulation_dataset_terrain_default_distance;

	/**
	 * value to setup the default water surface height(should be usually close to 0)
	 */
	T simulation_dataset_water_surface_default_displacement;

	/**
	 * files for terrain data
	 */
	std::string simulation_dataset_terrain_file;

	/**
	 * files for water surface displacement
	 */
	std::string simulation_dataset_water_surface_displacement_file;

	/**
	 * join/split all clusters during next step
	 */
	bool joinAllClustersInNextStep;
	bool splitAllClustersInNextStep;

	/**
	 * storage for dart stations which are still encoded
	 *
	 * after decoding, those are stored to dart_sample_points
	 */
	std::string dart_stations;

	class CDartSamplings
	{
public:
		double position_x;
		double position_y;
		std::string output_file;
	};


	CDartSamplings *dart_sample_points;
	int dart_samplings_size;

	std::ofstream dartfile_stream;


#if CONFIG_SIERPI_ENABLE_GUI
	TTsunamiVertexScalar visualization_water_surface_scale_factor;
	TTsunamiVertexScalar visualization_terrain_scale_factor;
#endif

#if SIMULATION_TSUNAMI_PARALLEL
	virtual void updateClusterParameters() = 0;
#endif

	virtual void reset() = 0;



public:
	CTsunamiSimulationParameters()	:
		simulation_reset(false),
		grid_initial_recursion_depth(6),
		grid_min_relative_recursion_depth(0),
		grid_max_relative_recursion_depth(8),

		grid_initial_cluster_splits(0),

		simulation_domain_boundary_condition(BOUNDARY_CONDITION_DIRICHLET),

		simulation_parameter_adaptive_timestep_size_with_cfl(true),

		simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain(1),

		simulation_parameter_gravitation(-1),
		simulation_parameter_cfl(-1),
		simulation_parameter_minimum_timestep_size(-1),
		simulation_parameter_dry_cell_threshold(-1),


		cluster_split_workload_size(512),
		cluster_join_workload_size(cluster_split_workload_size/2),

		cluster_scan_split_min_workload_size(0),
		cluster_scan_join_max_workload_size(CMath::numeric_max<unsigned int>()),

		cluster_update_split_join_size_after_elapsed_timesteps(0),
		cluster_update_split_join_size_after_elapsed_scalar(0.0),

		cluster_split_and_join_every_nth_timestep(1),

		simulation_world_scene_id(3),
		simulation_terrain_scene_id(0),
		simulation_water_surface_scene_id(1),

		simulation_random_raindrops_activated(false),

		simulation_dataset_breaking_dam_posx(-1.0),
		simulation_dataset_breaking_dam_posy(-1.0),
		simulation_dataset_breaking_dam_radius(-1.0),
		simulation_dataset_breaking_dam_height(1.0),

		simulation_dataset_terrain_default_distance(-1),
		simulation_dataset_water_surface_default_displacement(-1),

		joinAllClustersInNextStep(false),
		splitAllClustersInNextStep(false),

		dart_sample_points(nullptr),
		dart_samplings_size(0)

#if CONFIG_SIERPI_ENABLE_GUI
		,
		visualization_water_surface_scale_factor((TTsunamiDataScalar)-1.0),
		visualization_terrain_scale_factor((TTsunamiDataScalar)-1.0)
#endif
	{
		simulation_parameter_cell_data_setup.setup(-CMath::numeric_inf<TTsunamiDataScalar>(), -CMath::numeric_inf<TTsunamiDataScalar>());
		simulation_domain_boundary_dirichlet_edge_data.setup(-CMath::numeric_inf<TTsunamiDataScalar>(), -CMath::numeric_inf<TTsunamiDataScalar>());


#if 0
		simulation_parameter_cell_data_setup.dofs_hyp_edge.h = -CMath::numeric_inf<double>();
		simulation_parameter_cell_data_setup.dofs_hyp_edge.hu = 0;
		simulation_parameter_cell_data_setup.dofs_hyp_edge.hv = 0;
		simulation_parameter_cell_data_setup.dofs_hyp_edge.b = -CMath::numeric_inf<double>();

		simulation_parameter_cell_data_setup.dofs_left_edge.h = -CMath::numeric_inf<double>();
		simulation_parameter_cell_data_setup.dofs_left_edge.hu = 0;
		simulation_parameter_cell_data_setup.dofs_left_edge.hv = 0;
		simulation_parameter_cell_data_setup.dofs_left_edge.b = -CMath::numeric_inf<double>();

		simulation_parameter_cell_data_setup.dofs_right_edge.h = -CMath::numeric_inf<double>();
		simulation_parameter_cell_data_setup.dofs_right_edge.hu = 0;
		simulation_parameter_cell_data_setup.dofs_right_edge.hv = 0;
		simulation_parameter_cell_data_setup.dofs_right_edge.b = -CMath::numeric_inf<double>();

		simulation_domain_boundary_dirichlet_edge_data.dofs_center.h = -CMath::numeric_inf<double>();
		simulation_domain_boundary_dirichlet_edge_data.dofs_center.hu = 0;
		simulation_domain_boundary_dirichlet_edge_data.dofs_center.hv = 0;
		simulation_domain_boundary_dirichlet_edge_data.dofs_center.b = -CMath::numeric_inf<double>();
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		simulation_parameter_cell_data_setup.refine = false;
		simulation_parameter_cell_data_setup.coarsen = false;
#endif

		/*
		 * adaptive stuff
		 */

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

		adaptive_refine_slope_threshold = -1;
		adaptive_coarsen_slope_threshold = -1;

#else

		/*
		 * threshold for 1st order is given by surface slope
		 */
		TTsunamiDataScalar unit_slope = CMath::abs((T)1.0/simulation_dataset_water_surface_default_displacement);
		adaptive_coarsen_slope_threshold	= 0.002*unit_slope;
		adaptive_refine_slope_threshold	= 0.003*unit_slope;

#endif


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
		adaptive_coarsen_threshold = 0.01;
		adaptive_refine_threshold = 0.02;
#endif


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		adaptive_coarsen_threshold = 3;
		adaptive_refine_threshold = 24;
#endif

		if (simulation_parameter_cfl == -1)
			simulation_parameter_cfl = SIMULATION_TSUNAMI_CFL;


		simulation_size_of_cell_data = sizeof(CTsunamiSimulationCellData);

		reset_simulation_parameters();
	}



	virtual ~CTsunamiSimulationParameters()
	{
		if (dart_sample_points)
		{
			delete []dart_sample_points;
			dart_sample_points = nullptr;
		}
	}



	/**
	 * set the verbosity level
	 */
	void setVerbosityLevel(
			int i_verbosity_level
	)
	{
		verbosity_level = i_verbosity_level;
	}



	/**
	 * setup dart sampling points
	 */
	bool setupDartSamplingPoints(
			const char *i_dart_sampling_points_string
	)
	{
		std::string s = i_dart_sampling_points_string;
		return setupDartSamplingPoints(s);
	}



	/**
	 * setup dart sampling points
	 *
	 * Format:
	 *
	 * [double]/[double]/[outputfile],[double]/[double]/[outputfile],[double]/[double]/[outputfile],...
	 *
	 * [posx]/[posy]/[outputfile]
	 */
	bool setupDartSamplingPoints(
			const std::string &i_dart_sampling_points_string
	)
	{
		dart_samplings_size = 1;
		for (unsigned int i = 0; i < i_dart_sampling_points_string.size(); i++)
			if (i_dart_sampling_points_string[i] == ',')
				dart_samplings_size++;

		size_t delimiter_pos;
		size_t old_pos = 0;

		if (dart_sample_points != nullptr)
			delete [] dart_sample_points;

		dart_sample_points = new CDartSamplings[dart_samplings_size];

		for (int i = 0; i < dart_samplings_size; i++)
		{
			// search for next delimiter
			delimiter_pos = i_dart_sampling_points_string.find(',', old_pos);

			std::string sampling_point_string;

			// if not found, we reached the end of the point array
			if (delimiter_pos == std::string::npos)
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos);
			else
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos, delimiter_pos-old_pos);

			old_pos = delimiter_pos+1;


			/*
			 * split single point
			 */
			size_t pos = sampling_point_string.find('/');
			if (pos == std::string::npos)
			{
				std::cout << "Invalid format for -D " << std::endl;
				std::cout << "   use e. g. -D 834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt " << std::endl;
				exit(-1);
			}
			std::string l = sampling_point_string.substr(0, pos);
			std::string r = sampling_point_string.substr(pos+1);


			/*
			 * search for appended filename
			 */
			pos = r.find('/', 1);
			if (pos != std::string::npos)
			{
				dart_sample_points[i].output_file = r.substr(pos+1);
				r = r.substr(0, pos);
			}

			dart_sample_points[i].position_x = std::atof(l.c_str());
			dart_sample_points[i].position_y = std::atof(r.c_str());
		}

		return true;
	}



	bool setupTwoInts(
			const char *i_optstring,
			int *o_integer1,
			int *o_integer2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_integer1 = atoi(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_integer1 = std::atoi(l.c_str());
		*o_integer2 = std::atoi(r.c_str());

		return true;
	}



	bool setupTwoLongLongs(
			const char *i_optstring,
			long long *o_integer1,
			long long *o_integer2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_integer1 = atoll(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_integer1 = std::atoll(l.c_str());
		*o_integer2 = std::atoll(r.c_str());

		return true;
	}



	template <typename T>
	bool setupTwoFloats(
			const char *i_optstring,
			T *o_float1,
			T *o_float2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_float1 = atof(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_float1 = std::atof(l.c_str());
		*o_float2 = std::atof(r.c_str());

		return true;
	}


	/**********************************
	 * XML CONFIG FILE BASED CONFIGURATION
	 */
	void config_xml_file_setupInterface(
			CXMLConfig &i_cXMLConfig
	)
	{
		i_cXMLConfig.registerConfigClass("swe", this);
		i_cXMLConfig.registerConfigClass("tsunami", this);
	}



	bool setupXMLParameter(
			const char *i_scope_name,
			const char *i_name,
			const char *i_value
	)
	{
		if (	strcmp("swe", i_scope_name) == 0		 ||
				strcmp("tsunami", i_scope_name) == 0
		)
		{
			XML_CONFIG_TEST_AND_SET_INT("grid-initial-recursion-depth", grid_initial_recursion_depth);
			XML_CONFIG_TEST_AND_SET_INT("grid-min-relative-recursion-depth", grid_min_relative_recursion_depth);
			XML_CONFIG_TEST_AND_SET_INT("grid-max-relative-recursion-depth", grid_max_relative_recursion_depth);

			XML_CONFIG_TEST_AND_SET_INT("grid-initial-cluster-splits", grid_initial_cluster_splits);


			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-coarsen-threshold", adaptive_coarsen_threshold);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-refine-threshold", adaptive_refine_threshold);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-coarsen-slope-threshold", adaptive_coarsen_slope_threshold);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-refine-slope-threshold", adaptive_refine_slope_threshold)

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-gravitation", simulation_parameter_gravitation);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-cfl", simulation_parameter_cfl);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-adaptive-timestep-size-with-cfl", simulation_parameter_adaptive_timestep_size_with_cfl);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-minimum-timestep", simulation_parameter_minimum_timestep_size);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-timestep-size", simulation_parameter_global_timestep_size);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-dry-cell-threshold", simulation_parameter_dry_cell_threshold);

			XML_CONFIG_TEST_AND_SET_INT("cluster-split-workload-size", cluster_split_workload_size);
			XML_CONFIG_TEST_AND_SET_INT("cluster-join-workload-size", cluster_join_workload_size);

			XML_CONFIG_TEST_AND_SET_INT("cluster-scan-split-min-workload-size", cluster_scan_split_min_workload_size);
			XML_CONFIG_TEST_AND_SET_INT("cluster-scan-join-max-workload-size", cluster_scan_join_max_workload_size);

			XML_CONFIG_TEST_AND_SET_FLOAT("cluster-update-split-join-size-after-elapsed-timesteps", cluster_update_split_join_size_after_elapsed_timesteps);
			XML_CONFIG_TEST_AND_SET_FLOAT("cluster-update-split-join-size-after-elapsed-scalar", cluster_update_split_join_size_after_elapsed_scalar);

			XML_CONFIG_TEST_AND_SET_INT("cluster-split-and-join-every-nth-timestep", cluster_split_and_join_every_nth_timestep);

			XML_CONFIG_TEST_AND_SET_INT("simulation-world-scene-id", simulation_world_scene_id);
			XML_CONFIG_TEST_AND_SET_INT("simulation-terrain-scene-id", simulation_terrain_scene_id);
			XML_CONFIG_TEST_AND_SET_INT("simulation-water-surface-scene-id", simulation_water_surface_scene_id);

			XML_CONFIG_TEST_AND_SET_BOOL("simulation-random-raindrops-activated", simulation_random_raindrops_activated);

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-terrain-default-distance", simulation_dataset_terrain_default_distance);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-water-surface-default-displacement", simulation_dataset_water_surface_default_displacement);

			XML_CONFIG_TEST_AND_SET_INT("simulation-domain-boundary-condition", simulation_domain_boundary_condition);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-domain-length", simulation_parameter_domain_size_x)

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-posx", simulation_dataset_breaking_dam_posx);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-posy", simulation_dataset_breaking_dam_posy);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-radius", simulation_dataset_breaking_dam_radius);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-height", simulation_dataset_breaking_dam_height);

			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-terrain-file", simulation_dataset_terrain_file);
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-displacement-file", simulation_dataset_water_surface_displacement_file);

			XML_CONFIG_TEST_AND_SET_STRING("dart-stations", dart_stations);
		}

		return false;
	}


	/**********************************
	 * COMMAND LINE CONFIGURATION
	 */

	/**
	 * return string to be used for command line parameter parsing
	 */
	const char* config_command_line_getArgumentString()
	{
		return "a:C:D:d:F:h:i:I:k:l:o:r:s:u:U:v:w:y:z:";
	}


	/**
	 * output help about command line arguments
	 */
	void config_command_line_getArgumentsHelp()
	{

#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << "  DOMAIN SETUP:" << std::endl;
		std::cout << "	-w [int]: world scene" << std::endl;
		std::cout << "	-y [int]: terrain" << std::endl;
		std::cout << "	-z [int]: water surface initialization" << std::endl;
#endif

		std::cout << std::endl;
		std::cout << "  CLUSTERS: SPLITS/JOINS" << std::endl;
		std::cout << "	-o [int]: cluster size when to request split" << std::endl;
		std::cout << "	-l [int]: cluster size when to request join (both children have to request a join)" << std::endl;
		std::cout << "	-k [int]: run split- and join-operations only every n-th timestep" << std::endl;
		std::cout << "	-u [int]: elapsed timesteps when to update split and join parameters with table hints" << std::endl;
		std::cout << "	-U [double]: factor used for automatic computation of split/join parameter. default: 0 (disabled)" << std::endl;


		std::cout << std::endl;
		std::cout << "  SIMULATION SPECIFIC:" << std::endl;
		std::cout << "	-d [int]: initial recursion depth" << std::endl;
		std::cout << "	-i [int]: min relative recursion depth" << std::endl;
		std::cout << "	-a [int]: max relative recursion depth" << std::endl;
		std::cout << "	-D [int]: initial cluster splits" << std::endl;
		std::cout << "	-C [double]: cfl condition" << std::endl;
		std::cout << "	-s [double]: timestep size" << std::endl;
		std::cout << "	-r [double]/[double]: adaptivity parameters (refine/coarsen)" << std::endl;
		std::cout << "	-D [double]/[double]/[outputfile],...: comma separated list of dart station points [x]/[y]/[outputfile]" << std::endl;
	}



	/**
	 * parse a command line argument
	 */
	bool config_command_line_parseArgument(
			char i_command_line_argument_option,	///< incoming argument character (e. g. 'e' for command line argument '-e')
			const char* i_command_line_argument		///< command line argument when additional value for argument is allowed
		)
	{
		switch(i_command_line_argument_option)
		{
		case 'D':
			dart_stations = i_command_line_argument;
			return true;


		/*
		 * CFL
		 */
		case 'C':
			simulation_parameter_cfl = atof(i_command_line_argument);
			return true;

		case 'w':
			simulation_world_scene_id = atoi(i_command_line_argument);
			return true;

		case 'y':
			simulation_terrain_scene_id = atoi(i_command_line_argument);
			return true;

		case 'z':
			simulation_water_surface_scene_id = atoi(i_command_line_argument);
			return true;


		/*
		 * GRID
		 */
		case 'd':
			grid_initial_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'i':
			grid_min_relative_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'a':
			grid_max_relative_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'I':
			grid_initial_cluster_splits = atoi(i_command_line_argument);
			return true;

		/*
		 * SPLITTING
		 */
		case 'o':
			setupTwoLongLongs(i_command_line_argument, &cluster_split_workload_size, &cluster_scan_split_min_workload_size);

			// deactivate auto-mode
			cluster_update_split_join_size_after_elapsed_timesteps = 0;
			return true;

		case 'r':
			// adaptivity information
			if (!setupTwoFloats(i_command_line_argument, &adaptive_refine_threshold, &adaptive_coarsen_threshold))
				exit(-1);
			return true;

		case 'u':
			cluster_update_split_join_size_after_elapsed_timesteps = atoi(i_command_line_argument);
			return true;

		case 'U':
			cluster_update_split_join_size_after_elapsed_scalar = atof(i_command_line_argument);
			return true;

		case 'k':
			cluster_split_and_join_every_nth_timestep = atoi(i_command_line_argument);
			return true;

		/*
		 * SCAN
		 */
		case 'l':
			setupTwoLongLongs(i_command_line_argument, &cluster_join_workload_size, &cluster_scan_join_max_workload_size);

			// deactivate auto-mode
			cluster_update_split_join_size_after_elapsed_timesteps = 0;
			return true;
		}

		return false;
	}



	/**
	 * validate or fix invalid parameters
	 */
	bool config_validateAndFixParameters()
	{
		/**
		 * fix parameters
		 */
		if (cluster_split_workload_size > 0 && cluster_split_workload_size < 2)
		{
			std::cout << "Invalid split size: " << cluster_split_workload_size << "   setting split to 2" << std::endl;
			cluster_split_workload_size = 2;
		}

		if (cluster_join_workload_size == -1)
		{
			if (cluster_split_workload_size != -1)
				cluster_join_workload_size = cluster_split_workload_size/2;
		}
		else
		{
			if (cluster_join_workload_size > cluster_split_workload_size)
			{
				std::cerr << "ERROR: cluster_join_workload_size > cluster_split_workload_size (fixing)" << std::endl;
				cluster_join_workload_size = cluster_split_workload_size / 2;
			}
		}

		if (simulation_parameter_minimum_timestep_size <= 0)
			simulation_parameter_minimum_timestep_size = 0.000001;

		if (simulation_parameter_dry_cell_threshold <= 0)
			simulation_parameter_dry_cell_threshold = 0.01;

		if (simulation_parameter_gravitation <= 0)
			simulation_parameter_gravitation = 9.81;

		if (simulation_parameter_domain_size_x <= 0)
			simulation_parameter_domain_size_x = 5000.0;

		simulation_parameter_domain_size_y = simulation_parameter_domain_size_x;

		if (simulation_dataset_terrain_default_distance < 0)
			simulation_dataset_terrain_default_distance = 300;

		if (simulation_dataset_water_surface_default_displacement < 0)
			simulation_dataset_water_surface_default_displacement = 0;

		if (simulation_parameter_global_timestep_size <= 0)
			simulation_parameter_global_timestep_size = simulation_parameter_minimum_timestep_size;

		if (grid_initial_recursion_depth < grid_initial_cluster_splits)
		{
			grid_initial_cluster_splits = grid_initial_recursion_depth;
			std::cout << "WARNING: initial cluster splits larger than initial recursion depth. Fixing to " << grid_initial_cluster_splits << std::endl;
		}

		if (dart_stations.size() > 0)
		{
			if (!setupDartSamplingPoints(dart_stations.c_str()))
				return false;
		}

#if CONFIG_SIERPI_ENABLE_GUI
		if (visualization_water_surface_scale_factor < 0)
			visualization_water_surface_scale_factor = 0.25;

		if (visualization_terrain_scale_factor < 0)
			visualization_terrain_scale_factor = 0.1/simulation_dataset_terrain_default_distance;
#endif


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		if (simulation_parameter_cell_data_setup.dofs_center.h < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_center.h = simulation_dataset_terrain_default_distance;

		if (simulation_parameter_cell_data_setup.dofs_center.b < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_center.b = -simulation_dataset_terrain_default_distance;

#else

		if (simulation_parameter_cell_data_setup.dofs_hyp_edge.h < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_hyp_edge.h = simulation_dataset_terrain_default_distance;
		if (simulation_parameter_cell_data_setup.dofs_hyp_edge.b < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_hyp_edge.b = -simulation_dataset_terrain_default_distance;

		if (simulation_parameter_cell_data_setup.dofs_left_edge.h < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_left_edge.h = simulation_dataset_terrain_default_distance;
		if (simulation_parameter_cell_data_setup.dofs_left_edge.b < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_left_edge.b = -simulation_dataset_terrain_default_distance;

		if (simulation_parameter_cell_data_setup.dofs_right_edge.h < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_right_edge.h = simulation_dataset_terrain_default_distance;
		if (simulation_parameter_cell_data_setup.dofs_right_edge.b < -999999999.0)
			simulation_parameter_cell_data_setup.dofs_right_edge.b = -simulation_dataset_terrain_default_distance;

		if (simulation_domain_boundary_dirichlet_edge_data.dofs_center.h < -999999999.0)
			simulation_domain_boundary_dirichlet_edge_data.dofs_center.h = simulation_dataset_terrain_default_distance;
		if (simulation_domain_boundary_dirichlet_edge_data.dofs_center.b < -999999999.0)
			simulation_domain_boundary_dirichlet_edge_data.dofs_center.b = -simulation_dataset_terrain_default_distance;

#endif

		if (simulation_domain_boundary_dirichlet_edge_data.dofs_center.h < -999999999.0)
			simulation_domain_boundary_dirichlet_edge_data.dofs_center.h = simulation_dataset_terrain_default_distance;

		if (simulation_domain_boundary_dirichlet_edge_data.dofs_center.b < -999999999.0)
			simulation_domain_boundary_dirichlet_edge_data.dofs_center.b = -simulation_dataset_terrain_default_distance;

		if (simulation_threading_number_of_threads <= 0)
			simulation_threading_number_of_threads = 1;

		if (simulation_dataset_breaking_dam_radius < 0)
		{
			simulation_dataset_breaking_dam_posx = (T)-0.4*simulation_parameter_domain_size_x;
			simulation_dataset_breaking_dam_posy = (T)0.5*simulation_parameter_domain_size_y;

			simulation_dataset_breaking_dam_radius = (T)0.1*simulation_parameter_domain_size_x;
		}
		return true;
	}

#if CONFIG_SIERPI_ENABLE_GUI

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigSimulation(
		CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		/************************
		 * SIMULATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- SIMULATION CONTROL ---|"));
		io_cGlHudConfig.insert(o.setupBoolean("Reset simulation fluid", &simulation_reset));
		io_cGlHudConfig.insert(o.setupBoolean("Insert Random Raindrops", &simulation_random_raindrops_activated));

		/**
		 * Tsunami simulation parameters
		 */
		io_cGlHudConfig.insert(o.setupText("|--- Setup / Dataset parameters ---|"));
		io_cGlHudConfig.insert(o.setupInt("World scene", &simulation_world_scene_id, 1, -999, 999));
		io_cGlHudConfig.insert(o.setupInt("Terrain scene", &simulation_terrain_scene_id, 1, -100, 100));
		io_cGlHudConfig.insert(o.setupInt("Water height scene", &simulation_water_surface_scene_id, 1, -100, 100));

		io_cGlHudConfig.insert(o.setupText("|--- EdgeComm parameters ---|"));
		io_cGlHudConfig.insert(o.setupFloat("Gravitation length (TODO): ", &simulation_parameter_gravitation, 0.1, -100, 0));
		io_cGlHudConfig.insert(o.setupFloatHI("Timestep size: ", &simulation_parameter_global_timestep_size, 0.01));
		io_cGlHudConfig.insert(o.setupFloatHI("CFL: ", &simulation_parameter_cfl, 0.001, 0, 1));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain size x: ", &simulation_parameter_domain_size_x, 0.01, 0, CMath::numeric_inf<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain size y: ", &simulation_parameter_domain_size_y, 0.01, 0, CMath::numeric_inf<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Current simulation timestamp: ", &simulation_timestamp_for_timestep, 0.01));
		io_cGlHudConfig.insert(o.setupLongLong("Current simulation timestep: ", &simulation_timestep_nr, 1, 0, 999999999999));




		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- DATASET PARAMETERS ---|"));
		io_cGlHudConfig.insert(o.setupText("Generic Parameters:"));
		io_cGlHudConfig.insert(o.setupFloatHI(" + Bathymetry default distance: ", &simulation_dataset_terrain_default_distance, 0.1, -CMath::numeric_max<T>(), CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Water surface default height: ", &simulation_dataset_water_surface_default_displacement, 0.01, -CMath::numeric_max<T>(), CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupText("Surface setup for column (1):"));
		io_cGlHudConfig.insert(o.setupFloat(" + X-Position: ", &simulation_dataset_breaking_dam_posx, 0.02, -CMath::numeric_max<T>(), CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Y-Position: ", &simulation_dataset_breaking_dam_posy, 0.02, -CMath::numeric_max<T>(), CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI(" + Radius: ", &simulation_dataset_breaking_dam_radius, 0.02, 0.000001, CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Height: ", &simulation_dataset_breaking_dam_height, 0.02, -CMath::numeric_max<T>(), CMath::numeric_max<T>()));

		/************************
		 * Coarsen/Refine
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- ADAPTIVE PARAMETERS ---|"));

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE==1
		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen threshold", &adaptive_coarsen_threshold, 0.01, 0.0000001, CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine threshold", &adaptive_refine_threshold, 0.02, 0.0000001, CMath::numeric_max<T>()));
#endif


#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE==2
		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen threshold", &adaptive_coarsen_threshold, 0.01, 0.0000001, CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine threshold", &adaptive_refine_threshold, 0.02, 0.0000001, CMath::numeric_max<T>()));
#endif

		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen slope threshold (1st order)", &adaptive_coarsen_slope_threshold, 0.02, 0.0000001, CMath::numeric_max<T>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine slope threshold (1st order)", &adaptive_refine_slope_threshold, 0.02, 0.0000001, CMath::numeric_max<T>()));



		/************************
		 * PARALLELIZATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- PARALLELIZATION ---|"));
		io_cGlHudConfig.insert(o.setupLongLong("Split Elements", &cluster_split_workload_size, 1, 2, CMath::numeric_max<long long>()));
		io_cGlHudConfig.insert(o.setupLongLong("Join Elements", &cluster_join_workload_size, 1, 0, CMath::numeric_max<long long>()));
		io_cGlHudConfig.insert(o.setupLongLong("Scan Split Elements", &cluster_scan_split_min_workload_size, 1, 2, CMath::numeric_max<long long>()));
		io_cGlHudConfig.insert(o.setupLongLong("Scan Join Elements", &cluster_scan_join_max_workload_size, 1, 0, CMath::numeric_max<long long>()));
		io_cGlHudConfig.insert(o.setupInt("Number of Threads to use", &simulation_threading_number_of_threads, 1, 1, CMath::numeric_max<int>()));
	}

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigVisualization(
		CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- Water Vis. CONTROL ---|"));

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("GENERAL"));
		io_cGlHudConfig.insert(o.setupFloatHI("Water surface scale: ", &visualization_water_surface_scale_factor, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Terrain surface scale: ", &visualization_terrain_scale_factor, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupLinebreak());

		io_cGlHudConfig.insert(o.setupFloatHI("Scale X: ", &visualization_domain_scale_x, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Scale Y: ", &visualization_domain_scale_y, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Scale Z: ", &visualization_domain_scale_z, 0.01, -999999, 999999));

		io_cGlHudConfig.insert(o.setupFloat("translate X: ", &visualization_domain_translate_x, 1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloat("translate Y: ", &visualization_domain_translate_y, 1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloat("translate Z: ", &visualization_domain_translate_z, 1, -999999, 999999));
	}



	/**
	 * setup callback handlers
	 */
	void setupCGuiCallbackHandlers(
			CGuiConfig &cGuiConfig
	)
	{
#if SIMULATION_TSUNAMI_PARALLEL
		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.updateClusterParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_global_timestep_size);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_gravitation);
			CGUICONFIG_CALLBACK_INSTALL(adaptive_refine_threshold);
			CGUICONFIG_CALLBACK_INSTALL(adaptive_coarsen_threshold);
		}

		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.simulation_parameter_adaptive_timestep_size_with_cfl = false;
				c.updateClusterParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_global_timestep_size);
		}
#endif

		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.reset();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_domain_size_x);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain);
			CGUICONFIG_CALLBACK_INSTALL(simulation_world_scene_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_terrain_scene_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_water_surface_scene_id);
		}
	}
#endif



public:
	inline void outputVerboseInformation()
	{
		std::cout << " + Compile options:" << std::endl;
		std::cout << "   - Flux solver: " << SIMULATION_TSUNAMI_FLUX_SOLVER_STRING << std::endl;
		std::cout << "   - RK ORDER: " << SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER << std::endl;
		std::cout << "   - Order of basis functions: " << SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << "   - Bathymetry kernels: " << (SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS ? "[enabled]" : "[disabled]") << std::endl;
		std::cout << "   - Adaptivity mode: " << SIMULATION_TSUNAMI_ADAPTIVITY_MODE << std::endl;
		std::cout << std::endl;
		std::cout << " + Grid options:" << std::endl;
		std::cout << "   - initial recursion depth: " << grid_initial_recursion_depth << std::endl;
		std::cout << "   - min relative recursion depth: " << grid_min_relative_recursion_depth << std::endl;
		std::cout << "   - max relative recursion depth: " << grid_max_relative_recursion_depth << std::endl;
		std::cout << std::endl;
		std::cout << " + Simulation options:" << std::endl;
		std::cout << "   - CFL: " << simulation_parameter_cfl << std::endl;
		std::cout << "   - Adaptive timestep size with CFL: " << (simulation_parameter_adaptive_timestep_size_with_cfl ? "[enabled]" : "[disabled]") << std::endl;
		std::cout << "   - Timestep size: " << simulation_parameter_global_timestep_size << std::endl;
		std::cout << "   - Minimum timestep size: " << simulation_parameter_minimum_timestep_size << std::endl;
//		std::cout << "   - Domain length: " << simulation_parameter_domain_size_x << std::endl;
		std::cout << "   - Gravitation: " << simulation_parameter_gravitation << std::endl;
		std::cout << "   - World Scene ID: " << simulation_world_scene_id << std::endl;
		std::cout << "   - Terrain Scene ID: " << simulation_terrain_scene_id << std::endl;
		std::cout << "   - Displacement Scene ID: " << simulation_water_surface_scene_id << std::endl;
		std::cout << std::endl;
		std::cout << " + Adaptive:" << std::endl;
		std::cout << "   - conforming cluster skipping: " << (CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE ? "[ACTIVE]" : "[DEACTIVATED]") << std::endl;
		std::cout << "   - refine threshold: " << adaptive_refine_threshold << std::endl;
		std::cout << "   - coarsen threshold: " << adaptive_coarsen_threshold << std::endl;
		std::cout << "   - domain boundary condition: " << CBoundaryConditions::getString((EBoundaryConditions)simulation_domain_boundary_condition) << std::endl;
	}



public:
	void reset_simulation_parameters()
	{
		simulation_timestep_nr = 0;
		simulation_timestamp_for_timestep = 0;
	}

};

#endif
