#! /bin/bash

. inc_parameters.sh
. ../inc_environment_vars.sh

TIMESTEPS=$TIMESTEPS_COARSEST_LEVEL

for d in $PROBLEM_SIZES_IN_DEPTH; do
	echo
	echo "Timesteps: $TIMESTEPS"
	echo "INITIAL_DEPTH	SPLITTING_SIZE	MTPS	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"

	REL_DEPTH=$((22-d))
	[ $REL_DEPTH -lt 0 ] && REL_DEPTH=0

	TIMESTEPS=$(((2**REL_DEPTH)*THREADS))
	[ $TIMESTEPS -lt 4 ] && TIMESTEPS=4

	for s in $SPLIT_SIZES; do
		PARAMS_="-t $TIMESTEPS -d $d -n $THREADS -o $s $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT="`$EXEC_CMD`"

		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo "\$$VARNAME")

		AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
		AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`

		echo "$d	$s	$MTPS	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
	done
done
