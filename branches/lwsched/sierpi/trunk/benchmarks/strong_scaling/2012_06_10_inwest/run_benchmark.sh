#! /bin/bash


if [ -z "$1" ]; then
	echo "Usage: $0 [executable] [parameters]"
	exit
fi

. ../tools.sh

START_DEPTH=20

#export
#exit


# set splitting size to be fixed
PARAMS="-d $START_DEPTH -a 8 -t 100"

#THREAD_LIST=`seq 80 -2 1`
#THREAD_LIST="1 2 4 8 16 32 40 64 80"
THREAD_LIST="80 64 40 32 16 8 4 2 1"

echo "Executable + params: $@ $PARAMS"

echo "Problem size with different number of CPUs using function optimization"

echo "THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
for THREADS in $THREAD_LIST; do
	PARAMS_=" -n $THREADS $PARAMS"

	EXEC_CMD="$@ $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	VARNAME=MTPS$THREADS
	eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

	MTPS=$(eval echo "\$$VARNAME")

	AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
	TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
	TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
	SECONDS_FOR_SIMULATION=`getValueFromString "$OUTPUT" "SFS"`

	echo "$THREADS	$MTPS	$TIMESTEPS	$TIMESTEP_SIZE	$SECONDS_FOR_SIMULATION	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"

	echo "sleeping for 61 seconds" 1>&2
#	sleep 61
done
