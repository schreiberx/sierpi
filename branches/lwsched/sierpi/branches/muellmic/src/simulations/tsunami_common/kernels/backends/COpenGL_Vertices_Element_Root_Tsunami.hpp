/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_HPP_
#define COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlViewport.hpp"

#define COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT	128*3
#if COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT % 3 != 0
	#error "COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT has to be a multiple of 3"
#endif

template <typename TVertexScalar>
class COpenGL_Vertices_Element_Root_Tsunami
{
public:
	CGlVertexArrayObject vao;
	CGlBuffer buffer;

public:
	CError error;

	COpenGL_Vertices_Element_Root_Tsunami()
	{
		vao.bind();
			buffer.bind();
			// allocate buffer for vertices AND normals!!!
			buffer.resize(COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT*sizeof(GLfloat)*3*2);

			// vertex coordinates
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat)*2, 0);
			glEnableVertexAttribArray(0);

			// normals
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat)*2, (void*)(3*sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
		vao.unbind();
	}




	/**
	 * initialize rendering the splats
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRendering()
	{
		/**
		 * !!! We have to do the setup right here since other threads are not allowed to access the context !!!
		 */
		vao.bind();
		buffer.bind();

		CGlErrorCheck();
	}

	inline void renderOpenGLVertexArray(
			GLfloat *p_vertex_attributes,	///< pointer to vertex attributes (3 GLfloat coord, 3 GL float normal)
			size_t p_vertices_count			///< number of vertices
	)
	{
		assert(p_vertices_count > 0);
		// copy VERTEX COORDINATE AND NORMALS!!!
		buffer.subData(0, p_vertices_count*(3*2*sizeof(GLfloat)), p_vertex_attributes);
		glDrawArrays(GL_TRIANGLES, 0, p_vertices_count);
	}


	inline void shutdownRendering()
	{
		vao.unbind();
		CGlErrorCheck();
	}

};


#endif
