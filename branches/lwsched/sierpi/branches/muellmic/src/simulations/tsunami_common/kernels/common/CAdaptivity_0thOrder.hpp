/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"



/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_0thOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiEdgeData TEdgeData;


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;


	/**
	 * constructor
	 */
	CAdaptivity_0thOrder()	:
		cathetus_side_length(-1),
		cSimulationDataSets(nullptr),
		cfl_domain_size_div_max_wave_speed(-1)
	{

	}


	void traversal_pre_hook()
	{
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}



	/**
	 * get center of weight for triangle
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPoint(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
#if 0
		// use midpoint on hypotenuse
		*o_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

#else

		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);

#endif
	}



	/**
	 * get center of weight for triangle for both childs
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPointForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
#if 0
		// use midpoint on hypotenuse

		*o_left_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_left_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

		*o_right_mx = *o_left_mx;
		*o_right_my = *o_left_my;

#else

		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;

#endif
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		assert (cSimulationDataSets != nullptr);

		TTsunamiVertexScalar lmx, lmy, rmx, rmy;

		computeAdaptiveSamplingPointForLeftAndRightChild(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,

				&lmx, &lmy,
				&rmx, &rmy
			);

		if (i_elementData->dofs.h <= 0)
		{
			if (i_elementData->dofs.h < 0)
				std::cout << "Negative h: " << i_elementData->dofs.h << std::endl;

			o_left_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(lmx, lmy, 0);
			o_left_elementData->dofs.h = CMath::max((T)0.0,  -o_left_elementData->dofs.b);
			o_left_elementData->dofs.qx = 0;
			o_left_elementData->dofs.qy = 0;

			o_right_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(rmx, rmy, 0);
			o_right_elementData->dofs.h = CMath::max((T)0.0,  -o_right_elementData->dofs.b);
			o_right_elementData->dofs.qx = 0;
			o_right_elementData->dofs.qy = 0;
		}
		else
		{
			T h_horizon = i_elementData->dofs.b + i_elementData->dofs.h;

			o_left_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(lmx, lmy, 0);
			o_left_elementData->dofs.h = CMath::max((T)0.0,  -o_left_elementData->dofs.b + h_horizon);
			o_left_elementData->dofs.qx = i_elementData->dofs.qx;
			o_left_elementData->dofs.qy = i_elementData->dofs.qy;

			o_right_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(rmx, rmy, 0);
			o_right_elementData->dofs.h = CMath::max((T)0.0,  -o_right_elementData->dofs.b + h_horizon);
			o_right_elementData->dofs.qx = i_elementData->dofs.qx;
			o_right_elementData->dofs.qy = i_elementData->dofs.qy;
		}

		assert(cfl_domain_size_div_max_wave_speed != -1);

		T local_cfl_domain_size_div_max_wave_speed = i_elementData->cfl_domain_size_div_max_wave_speed*(T)(1.0/CMath::sqrt(2.0));

		o_left_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_left_elementData->refine = false;
		o_left_elementData->coarsen = false;
		o_right_elementData->refine = false;
		o_right_elementData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				i_depth,
				&o_left_elementData->validation,
				&o_right_elementData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiElementData *o_elementData,
			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			T mx = (vleft_x + vright_x)*(TTsunamiVertexScalar)0.5;
			T my = (vleft_y + vright_y)*(TTsunamiVertexScalar)0.5;

			i_left_elementData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_elementData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif


		assert (cSimulationDataSets != nullptr);

		T mx, my;

		computeAdaptiveSamplingPoint(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				&mx, &my
			);
#if 1
		TTsunamiVertexScalar b = (i_left_elementData->dofs.b + i_right_elementData->dofs.b)*(T)0.5;
		o_elementData->dofs.b = b;

		T sum_horizon = 0;
		T sum_qx = 0;
		T sum_qy = 0;
		T sum_div = 0;

		if (i_left_elementData->dofs.h > 0)
		{
			sum_horizon += i_left_elementData->dofs.h + i_left_elementData->dofs.b;
			sum_qx += i_left_elementData->dofs.qx;
			sum_qy += i_left_elementData->dofs.qy;
			sum_div++;
		}

		if (i_right_elementData->dofs.h > 0)
		{
			sum_horizon += i_right_elementData->dofs.h + i_right_elementData->dofs.b;
			sum_qx += i_right_elementData->dofs.qx;
			sum_qy += i_right_elementData->dofs.qy;
			sum_div++;
		}

		if (sum_div == 0)
		{
			o_elementData->dofs.h = 0;
			o_elementData->dofs.qx = 0;
			o_elementData->dofs.qy = 0;
		}
		else
		{
			o_elementData->dofs.h = sum_horizon/sum_div - o_elementData->dofs.b;

			if (o_elementData->dofs.h <= 0)
			{
				o_elementData->dofs.h = 0;
				o_elementData->dofs.qx = 0;
				o_elementData->dofs.qy = 0;
			}
			else
			{
				o_elementData->dofs.qx = sum_qx/sum_div;
				o_elementData->dofs.qy = sum_qy/sum_div;
			}
		}


#else
		TTsunamiVertexScalar b = (i_left_elementData->dofs.b + i_right_elementData->dofs.b)*(T)0.5;

		o_elementData->dofs.b = b;
//		o_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(mx, my, 0);

// TODO: remove me
//		o_elementData->dofs.b = (i_left_elementData->dofs.b + i_right_elementData->dofs.b)*(T)0.5;
		o_elementData->dofs.h = (i_left_elementData->dofs.h + i_right_elementData->dofs.h)*(T)0.5 + (b - o_elementData->dofs.b);
		o_elementData->dofs.h = CMath::max((T)0.0, o_elementData->dofs.h);
		o_elementData->dofs.qx = (i_left_elementData->dofs.qx + i_right_elementData->dofs.qx)*(T)0.5;
		o_elementData->dofs.qy = (i_left_elementData->dofs.qy + i_right_elementData->dofs.qy)*(T)0.5;

		o_elementData->cfl_domain_size_div_max_wave_speed = (i_left_elementData->cfl_domain_size_div_max_wave_speed + i_right_elementData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*CMath::sqrt(2.0));
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_elementData->refine = false;
		o_elementData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_elementData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_elementData->validation,
				&i_right_elementData->validation
		);
#endif
	}
};


#endif /* CADAPTIVITY_0STORDER_HPP_ */
