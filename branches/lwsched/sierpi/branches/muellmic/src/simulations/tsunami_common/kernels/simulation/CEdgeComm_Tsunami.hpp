/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */

#include "../../CTsunamiConfig.hpp"

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CEdgeComm_Tsunami_0thOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 1
		// directly apply explicit euler
		typedef CEdgeComm_Tsunami_0thOrder<false> CEdgeComm_Tsunami;
#else
		typedef CEdgeComm_Tsunami_0thOrder<true> CEdgeComm_Tsunami;
#endif
	}
}

#else

#include "CEdgeComm_Tsunami_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 1
		// directly apply explicit euler
		typedef CEdgeComm_Tsunami_1stOrder<false> CEdgeComm_Tsunami;
#else
		typedef CEdgeComm_Tsunami_1stOrder<true> CEdgeComm_Tsunami;
#endif
	}
}

#endif
