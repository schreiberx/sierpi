/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CEdgeComm_InformationAdjacentPartition.hpp
 *
 *  Created on: Jul 19, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_EDGECOMM_INFORMATIONADJACENTPARTITION_HPP_
#define CPARTITION_EDGECOMM_INFORMATIONADJACENTPARTITION_HPP_

#include "CPartition_TreeNode.hpp"
#include "CPartition_UniqueId.hpp"



/**
 * information about a single adjacent partition
 */
template <typename CPartition_TreeNode>
class CPartition_EdgeComm_InformationAdjacentPartition
{
public:
	/**
	 * the partition tree node
	 */
	CPartition_TreeNode *partitionTreeNode;

	/**
	 * the uniqueId of the adjacent partition (should be equal to partitionTreeNode->uniqueId)
	 */
	CPartition_UniqueId uniqueId;

	/**
	 * the number of elements on the communication stack which have to be exchanged
	 */
	int commElements;


	/**
	 * constructor
	 */
	inline CPartition_EdgeComm_InformationAdjacentPartition()
	{
		partitionTreeNode = NULL;
		commElements = -1;
	}


	/**
	 * constructor to create edge communication information element
	 */
	inline CPartition_EdgeComm_InformationAdjacentPartition(
			CPartition_TreeNode *i_partitionTreeNode,	///< partition tree node
			CPartition_UniqueId &i_uniqueId,	///< unique id
			size_t i_commElements				///< number of communication elements
		)
	{
		partitionTreeNode = i_partitionTreeNode;
		uniqueId = i_uniqueId;
		commElements = i_commElements;
	}


	/**
	 * constructor
	 */
	inline CPartition_EdgeComm_InformationAdjacentPartition(
			CPartition_TreeNode *i_partitionTreeNode	///< partition tree node
	)
	{
		partitionTreeNode = i_partitionTreeNode;
		uniqueId = i_partitionTreeNode->uniqueId;
		commElements = -1;
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, const CPartition_EdgeComm_InformationAdjacentPartition &m)
	{
		co << "   + comm elements: " << m.commElements << std::endl;
		co << "   + uniqueId: " << m.uniqueId << std::endl;
		co << "   + adj uniqueId: " << m.partitionTreeNode->uniqueId << std::endl;
		return co;
	}
};



#endif /* CEDGECOMM_INFORMATIONADJACENTPARTITION_HPP_ */
