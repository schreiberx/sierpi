/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMI_TYPES_1ST_ORDER_HPP
#define CTSUNAMI_TYPES_1ST_ORDER_HPP


// load typedef for TTsunamiDataScalar
#include "../CTsunamiConfig.hpp"


/**
 * Edge communication data during tsunami simulation
 */

#include "CValidation_EdgeData.hpp"

class CTsunamiSimulationEdgeData
{
public:
	CTsunamiSimulationDOFs dofs_center;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setup(
			TTsunamiDataScalar i_h,
			TTsunamiDataScalar i_b
	)
	{
		dofs_center.setup(i_h, i_b);
	}




	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			 const CTsunamiSimulationEdgeData &d
	)	{
		os << "Tsunami CEdgeData Information:" << std::endl;
		os << " + h: " << d.dofs_center.h << std::endl;
		os << " + hu: " << d.dofs_center.hu << std::endl;
		os << " + hv: " << d.dofs_center.hv << std::endl;
		os << " + b/max_wave_speed: " << d.dofs_center.b << std::endl;
		os << " + vx: " << d.dofs_center.hu/d.dofs_center.h << std::endl;
		os << " + vy: " << d.dofs_center.hv/d.dofs_center.h << std::endl;
		os << " + horizon: " << (d.dofs_center.h + d.dofs_center.b) << std::endl;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};


#include "CValidation_CellData.hpp"

class CTsunamiSimulationCellData
{
public:
	CTsunamiSimulationDOFs dofs_hyp_edge;
	CTsunamiSimulationDOFs dofs_right_edge;
	CTsunamiSimulationDOFs dofs_left_edge;

	TTsunamiDataScalar cfl_domain_size_div_max_wave_speed;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
	#if 0
		unsigned refine:1;
		unsigned coarsen:1;
	#else
		bool refine;
		bool coarsen;
	#endif
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setup(
			TTsunamiDataScalar i_h,
			TTsunamiDataScalar i_b
	)
	{
		dofs_hyp_edge.setup(-CMath::numeric_inf<double>(), -CMath::numeric_inf<double>());
		dofs_right_edge.setup(-CMath::numeric_inf<double>(), -CMath::numeric_inf<double>());
		dofs_left_edge.setup(-CMath::numeric_inf<double>(), -CMath::numeric_inf<double>());

		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<double>();
	}



	friend
	::std::ostream&
	operator<<
	(
			::std::ostream& os,
			 const CTsunamiSimulationCellData &d
	)	{
		os << d.dofs_hyp_edge << std::endl;
		os << d.dofs_right_edge << std::endl;
		os << d.dofs_left_edge << std::endl;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		std::cout << d.validation << std::endl;
#endif
		return os;
	}
};



#endif
