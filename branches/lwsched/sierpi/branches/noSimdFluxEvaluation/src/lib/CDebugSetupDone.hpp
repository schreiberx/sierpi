/*
 * CDebugSetupDone.hpp
 *
 *  Created on: Sep 30, 2011
 *      Author: schreibm
 *
 * helper functions to check whether a setup was done for a class
 */

#ifndef CDEBUGSETUPDONE_HPP_
#define CDEBUGSETUPDONE_HPP_


#if DEBUG

#define DEBUG_SETUP_DONE_VAR			bool setupDone;
#define DEBUG_SETUP_DONE_CONSTRUCTOR	setupDone = false;
#define DEBUG_SETUP_DONE_SETUP			setupDone = true;
#define DEBUG_SETUP_DONE_CHECK()			assert(setupDone == true);

#else

#define DEBUG_SETUP_DONE_VAR
#define DEBUG_SETUP_DONE_CONSTRUCTOR
#define DEBUG_SETUP_DONE_SETUP
#define DEBUG_SETUP_DONE_CHECK()

#endif


#endif /* CDEBUGSETUPDONE_HPP_ */
