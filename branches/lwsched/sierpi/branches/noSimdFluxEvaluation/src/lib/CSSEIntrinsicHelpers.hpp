/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Nov 9, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSSEINTRINSICHELPERS_H_
#define CSSEINTRINSICHELPERS_H_



// gnu compiler
#define SSEALIGN16		__attribute__ ((aligned (16)))

// MS$!@#$ compiler
//#define SSEALIGN16	__declspec(align(16))




inline __m128 inv(__m128 vec)
{
	return _mm_xor_ps(vec, (__m128)_mm_set1_epi32(0x80000000));
}



#endif /* CSSEINTRINSICHELPERS_H_ */
