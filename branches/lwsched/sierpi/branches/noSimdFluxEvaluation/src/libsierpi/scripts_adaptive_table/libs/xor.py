

###################################
# xor helper function
###################################
def xor(a, b):
    r = True
    if a:
        r = not r
    if b:
        r = not r
    return r
