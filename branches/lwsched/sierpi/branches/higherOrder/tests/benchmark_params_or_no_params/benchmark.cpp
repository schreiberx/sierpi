/*
 * Benchmark for testing traversators with and without parameters.
 *
 * use: setup depth useParameters loops
 * depth: determines the amount of elements in the regular grid as 2^depth
 * useParameters: 0 for using the actual traversator functions (without parameters in the method
 * calls) and 1 for using the new traversator functions (with parameters in the method calls)
 * loops: amount of times the traversator is run across the grid.
 *
 *  Created on: June 25, 2011
 *  Last modified: July 03, 2011
 *      Author: David Azócar
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <assert.h>
#include "CStack.hpp"
#include "CFBStacks.hpp"
#include "CTraversator_Vertices_ElementData.hpp"
#include "CSetup_ElementData_Vertices_Validate.hpp"
#include "CSetup_Structure_ElementData.hpp"
#include "CValidateTypes.hpp"
//#include "CEdgeComm.hpp"
//#include "CEdgeComm_ValidateComm.hpp"
#include "CEdgeComm_BenchmarkComm.hpp"
#include "CEdgeComm_Vertices_Normals.hpp"
#include "CStopwatch.hpp"

using namespace std;
using namespace sierp;
using namespace travs;
using namespace kernels;
/*
 * MAIN METHOD
 * CEdgeComm.hpp is not used
 * CEdgeComm_Parameters.hpp is not used
 * CEdgeComm_ValidateComm.hpp is not used
 */
int main(int argc, const char* argv[]) {
	string input;
	string info;
	string gridFileName;
	string gridName;
	stringstream ss;
	int loops;
	int useParameters;
	int depth;

	/*
	 * Check if the arguments are correct.
	 */
	if (argc == 4) {
		ss << argv[1] << endl;
		ss >> depth;
		ss << argv[2] << endl;
		ss >> useParameters;
		ss << argv[3] << endl;
		ss >> loops;
	} else {
		cerr
				<< "Incorrect number of arguments. Use: benchmark depth useParameters loops"
				<< endl;
		return 1;
	}

	unsigned int max_leaf_triangles = (1 << (depth + 1));
	unsigned int max_comm_edges = (1 << depth);
	unsigned int max_edge_comm_buffer = max_leaf_triangles;
	/*
	 * Stacks for the traversator
	 */
	CFBStacks<char> p_structure_stacks(max_leaf_triangles);
	CFBStacks<CValElementData> p_element_data_stacks(max_leaf_triangles);
	CStack<CValEdgeData> p_edge_data_comm_left_edge_stack(max_comm_edges);
	CStack<CValEdgeData> p_edge_data_comm_right_edge_stack(max_comm_edges);
	CStack<CValEdgeData> p_edge_comm_buffer(max_edge_comm_buffer);
	std::cout << "Traversator Benchmark." << std::endl;

	/*
	 * Initialize the stacks with a regular grid with 2^depth element, and with empty elements
	 */
	CSetup_Structure_ElementData<CValElementData> initialValues;
	CValElementData initialElement;
	initialElement.hyp = 0.0;
	initialElement.left = 0.0;
	initialElement.right = 0.0;
	initialValues.setup(p_structure_stacks, p_element_data_stacks, depth,
			&initialElement);

	/*
	 * Check whether the bit stream corresponds to a valid grid. The function for
	 * square domains is used.
	 */
	if (!p_structure_stacks.forward.structure_isValidQuad()) {
		cerr << "Error: Grid structure wrong." << endl;
		return 1;
	}

	/*
	 * count the numbers of elements. The function for square domains is used.
	 */
	cout << "Number of elements: "
			<< p_structure_stacks.forward.structure_getNumberOfTrianglesInQuad()
			<< endl;

	/*
	 * Print out the number of loops
	 */
	cout << "Number of loops: " << loops << endl;
	cout << "Use Parameters: " << useParameters << endl;

	/*
	 * run traversator for set up the data. The ElementData is a data structure which contains
	 * 3 values, one for each edge of the triangle. In this values the midpoint of the edge
	 * is stored.
	 * I changed GLfloat by float in one of the defines.
	 */
	CTraversator_Vertices_ElementData<CSetup_ElementData_Vertices_Validate>
			traversator;
	traversator.action(p_structure_stacks, p_element_data_stacks);

	/*
	 * Call to the edge communication, "loops" times.
	 */
	CStopwatch clock;
	clock();
	CEdgeComm_Vertices_Normals<CEdgeComm_BenchmarkComm, CValElementData,
			CValEdgeData> communication;
	for (int i = 0; i < loops; i++)
		communication.action(p_structure_stacks, p_element_data_stacks,
				p_edge_data_comm_left_edge_stack,
				p_edge_data_comm_right_edge_stack, p_edge_comm_buffer,
				useParameters);
	clock.stop();
	/*
	 * Only the time is sent to the standard output
	 */
	cout << clock.getTime() << " TIME" << endl;

	cout << "End." << endl;
	return 0;
}
