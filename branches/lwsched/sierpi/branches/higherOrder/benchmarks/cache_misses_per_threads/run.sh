#! /bin/bash

. ../tools.sh

START_DEPTH=14

TIMESTEPS=50

# adaptive splitting size
PARAMS="-u 10 -U 5"

echo "Problem size with different number of CPUs using function optimization"
echo "Depth: $START_DEPTH"
echo "Timesteps: $TIMESTEPS"

echo "THREADS	DEPTH	MTPS	CACHE_MISSES	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
for THREADS in $CPULIST; do
	THREADS=$((THREADS+1))
	DEPTH=$START_DEPTH
	PARAMS_=" -d $DEPTH -n $THREADS $PARAMS -t $TIMESTEPS $@"

	EXEC_CMD="../../perftest $EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD 2>&1`"

	CACHE_MISSES=`echo -n "$OUTPUT" | grep "cache-misses" | tail -n 1 | sed "s/ cache-misses.*//"`
	echo $CACHE_MISSES

	MTPS=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
	TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
	TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
	SECONDS_FOR_SIMULATION=`getValueFromString "$OUTPUT" "SFS"`

	echo "$THREADS	$DEPTH	$MTPS	$CACHE_MISSES	$SPEEDUP_PER	$TIMESTEPS	$TIMESTEP_SIZE	$SECONDS_FOR_SIMULATION	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
done
