#! /bin/bash

. inc_parameters.sh

s=4096
THREADS="$CPUS"

OUTPUT_LINE="TIMESTEP	TRIANGLES"

PARAMS_="-o $s -n $THREADS $PARAMS -v 5 $@"

EXEC_CMD="$EXEC $PARAMS_"

echo "$EXEC_CMD"
$EXEC_CMD -t 1
$EXEC_CMD | grep "	TRIANGLES"
