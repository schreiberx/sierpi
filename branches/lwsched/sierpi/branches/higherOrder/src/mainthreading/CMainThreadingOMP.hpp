/*
 * CMainThreadingOMP.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGOMP_HPP_
#define CMAINTHREADINGOMP_HPP_

#include <omp.h>
#include <iostream>
#include <cassert>


#include "CMainThreading_Interface.hpp"


bool CMainThreadingOMP_setAffinities(
		int i_num_threads,
		int i_max_cores,
		int i_distance,
		int i_start_id,
		int i_verbose_level
);


class CMainThreading	:
	public CMainThreading_Interface
{
public:
	void threading_setup()
	{
		/*
		 * set number of threads which are initially used for simulation
		 */
		if (getMaxNumberOfThreads() <= 0)
			setMaxNumberOfThreads(omp_get_max_threads());

		/*
		 * maximum number of threads
		 */
		if (getNumberOfThreadsToUse() <= 0)
			setValueNumberOfThreadsToUse(getMaxNumberOfThreads());
		else
			omp_set_num_threads(getNumberOfThreadsToUse());

		if (getVerboseLevel() > 5)
		{
			std::cout << "getMaxNumberOfThreads(): " << (int)getMaxNumberOfThreads() << std::endl;
			std::cout << "getNumberOfThreadsToUse(): " << (int)getNumberOfThreadsToUse() << std::endl;
		}

		if (getThreadAffinityDistance() < 0)
			return;


		/*
		 * try to set the affinities
		 */
		CMainThreadingOMP_setAffinities(
				getNumberOfThreadsToUse(),
				getMaxNumberOfThreads(),
				getThreadAffinityDistance(),
				getThreadAffinityStartId(),
				getVerboseLevel()
			);
	}

	void threading_simulationSetup()
	{

#pragma omp parallel
#pragma omp master
		simulationSetup();
	}

	void threading_simulationLoop()
	{
		bool continue_simulation = true;

#pragma omp parallel
#pragma omp master
		do
		{
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}

	bool threading_simulationLoopIteration()
	{
		bool retval;

#pragma omp parallel
#pragma omp master
		retval = simulationLoopIteration();

		return retval;
	}

	void threading_shutdown()
	{
	}

	void threading_setNumThreads(int i)
	{
		omp_set_num_threads(i);
	}


	int threading_getNumMaxThreads()
	{
		return omp_get_max_threads();
	}

	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
