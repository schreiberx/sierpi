/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGTBB_HPP_
#define CMAINTHREADINGTBB_HPP_

#include <tbb/task_scheduler_init.h>
#include <iostream>
#include <stdlib.h>
#include <cassert>

#include "CMainThreading_Interface.hpp"


bool CMainThreadingTBB_setAffinities(
		int i_num_threads,
		int i_max_cores,
		int i_distance,
		int i_start_id,
		int i_verbose_level
);


class CMainThreading	:
	public CMainThreading_Interface
{
    tbb::task_scheduler_init *tbb_task_scheduler_init;

public:
	void threading_setup()
	{
		/**
		 * assume that default_num_threads also returns the maximum number of threads and thus logical cores
		 */
		if (getMaxNumberOfThreads() == -1)
			setMaxNumberOfThreads(tbb::task_scheduler_init::default_num_threads());

		if (getMaxNumberOfThreads() == -1)
		{
			std::cout << "Unable to determine maximum number of threads!" << std::endl;
			exit(-1);
		}

		// setup scheduler
		if (getNumberOfThreadsToUse() == -1)
			setValueNumberOfThreadsToUse(getMaxNumberOfThreads());

		if (getMaxNumberOfThreads() < getNumberOfThreadsToUse())
		{
			std::cerr << "ASSERT[ max_number_of_threads(" << getMaxNumberOfThreads() << ") > num_threads (" << getNumberOfThreadsToUse() << ") ] failed" << std::endl;
			exit(-1);
		}

		tbb_task_scheduler_init = new tbb::task_scheduler_init(getNumberOfThreadsToUse());

		if (getVerboseLevel() >= 5)
			std::cout << "tbb::task_scheduler_init::default_num_threads(): " << getMaxNumberOfThreads() << std::endl;

		if (getThreadAffinityDistance() < 0)
			return;

		if (getVerboseLevel() > 0)
			std::cerr << "WARNING: Setting Affinities in TBB is experimental since additional threads are sometimes created" << std::endl;

		CMainThreadingTBB_setAffinities(
				getNumberOfThreadsToUse(),
				getMaxNumberOfThreads(),
				getThreadAffinityDistance(),
				getThreadAffinityStartId(),
				getVerboseLevel()
			);

	}


	void threading_simulationSetup()
	{
		simulationSetup();
	}

	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}


	bool threading_simulationLoopIteration()
	{
		return simulationLoopIteration();
	}


	void threading_shutdown()
	{
		delete tbb_task_scheduler_init;
	}


	void threading_setNumThreads(int i)
	{
		delete tbb_task_scheduler_init;

		setValueNumberOfThreadsToUse(i);

		tbb_task_scheduler_init = new tbb::task_scheduler_init(i);
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGTBB_HPP_ */
