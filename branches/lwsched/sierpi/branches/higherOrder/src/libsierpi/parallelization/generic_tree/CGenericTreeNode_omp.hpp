/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_omp.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_OMP_HPP_
#define CGENERICTREENODE_OMP_HPP_

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGenericTreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

inline void specializedConstructorMethod()
{
}


#define OMP_TASK_FOR_ROOT_NODE	0


/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE)
 */

/*
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun p_lambda
	)
	{
		if (this_node->isLeaf())
		{
			p_lambda(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, p_lambda);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, p_lambda) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, p_lambda);
		}

#pragma omp taskwait
	}
};


template <class CLambdaFun>
inline void traverse_GenericTreeNode_Parallel(
		CLambdaFun i_lambda
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda)
#endif

	CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>::traverse(this, i_lambda);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
}

template <class CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda
)
{
	traverse_GenericTreeNode_Parallel<CLambdaFun>(i_lambda);
}


/*
 * serial
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode_Serial
{
public:
	static void traverse(CGenericTreeNode_ *this_node, CLambdaFun p_lambda)
	{
		if (this_node->isLeaf())
		{
			p_lambda(this_node);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_Serial<CLambdaFun>::traverse(this_node->first_child_node, p_lambda);

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_Serial<CLambdaFun>::traverse(this_node->second_child_node, p_lambda);
	}
};

template <class CLambdaFun>
inline void traverse_GenericTreeNode_Serial(
		CLambdaFun lambda
)
{
	CTraversalTask_GenericTreeNode_Serial<CLambdaFun>::traverse(this, lambda);
}


/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes_preorder,
			CLambdaFun3 lambda_midnodes_postorder
	)
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes_preorder(this_node);
		}
		else
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
		}

#pragma omp taskwait
#pragma omp flush

		if (this_node)
		{
			lambda_midnodes_postorder(this_node);
		}
	}
};


template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
}



template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
}




/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial
{

public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)
	{
		if (!this_node->isLeaf())
		{
			i_lambda_midnodes_preorder(this_node);
		}
		else
		{
			i_lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
		}

		if (this_node)
			i_lambda_midnodes_postorder(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2, CLambdaFun3>::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}

#pragma omp taskwait
#pragma omp flush

		if (this_node)
			lambda_midnodes(this_node);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(p_lambda_leaves, p_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
}



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{

	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(p_lambda_leaves, p_lambda_midnodes);
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial
{

public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}


		if (this_node)
			lambda_midnodes(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes(this_node, p_genericTreeDepth-1);
		}
		else
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

#pragma omp taskwait
#pragma omp flush
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);

#pragma omp taskwait
#pragma omp flush
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
	traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(i_lambda_leaves, i_lambda_midnodes, 0);
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int i_genericTreeDepth
	)
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes(this_node, i_genericTreeDepth);
		}
		else
		{
			lambda_leaves(this_node, i_genericTreeDepth);
			return;
		}

		i_genericTreeDepth++;
		if (this_node->first_child_node)
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, i_genericTreeDepth);

		if (this_node->second_child_node)
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, i_genericTreeDepth);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;

		if (this_node->first_child_node)
		{
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(this_node, lambda_leaves, lambda_midnodes) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

#pragma omp taskwait
#pragma omp flush

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);

#pragma omp taskwait
#pragma omp flush
}


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(i_lambda_leaves, i_lambda_midnodes, 0);
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;
		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		int i_genericTreeDepth
)
{
	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(i_lambda_leaves, i_lambda_midnodes, i_genericTreeDepth);
}



/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/

/*
 * parallel
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
			)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			p_lambda(p_this_node, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue;
		TReduceValue secondValue;

		if (p_this_node->first_child_node)
		{
#pragma omp task shared(firstValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
			traverse(p_this_node->first_child_node, p_lambda, p_reduceOperator, &firstValue);
		}

		if (p_this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(secondValue, p_this_node, p_lambda, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(p_this_node->second_child_node, p_lambda, p_reduceOperator, &secondValue);
		}

#pragma omp taskwait
#pragma omp flush
		{
		}

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);
				return;
			}
			else
			{
				*o_reduceOutput = firstValue;
				return;
			}
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;
		return;
	}
};



template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel(
		CLambdaFun i_lambda_leaves,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves)
#endif

	CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>::traverse(this, i_lambda_leaves, reduceOperator, o_reduceOutput);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#endif
}


template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda_leaves,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	traverse_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(i_lambda_leaves, reduceOperator, o_reduceOutput);
}



template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
			)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			p_lambda(p_this_node, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue(0);
		TReduceValue secondValue(0);

		if (p_this_node->first_child_node)
		{
			traverse(p_this_node->first_child_node, p_lambda, p_reduceOperator, &firstValue);
		}

		if (p_this_node->second_child_node)
		{
			traverse(p_this_node->second_child_node, p_lambda, p_reduceOperator, &secondValue);
		}

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);
				return;
			}

			*o_reduceOutput = firstValue;
			return;
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;
		return;
	}
};

template <typename TReduceValue, typename CLambdaFun>
inline void traverse_GenericTreeNode_Reduce_Serial(
		CLambdaFun p_lambda,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	return CTraversalTask_GenericTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>::traverse(this, p_lambda, reduceOperator, o_reduceOutput);
}



/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
		)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			i_lambda_leaves(p_this_node, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue;
		TReduceValue secondValue;

		if (p_this_node->first_child_node)
		{
#pragma omp task shared(firstValue, p_this_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
			traverse(p_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &firstValue);
		}

		if (p_this_node->second_child_node)
		{
#if OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#pragma omp task shared(secondValue, p_this_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator) OPENMP_EXTRA_TASK_CLAUSE
#endif
			traverse(p_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &secondValue);
		}

#pragma omp taskwait
#pragma omp flush
		{
		}

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);

				i_lambda_midnodes(p_this_node, o_reduceOutput);
				return;
			}
			else
			{
				*o_reduceOutput = firstValue;

				i_lambda_midnodes(p_this_node, o_reduceOutput);
				return;
			}
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;

		i_lambda_midnodes(p_this_node, o_reduceOutput);
		return;
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
#if OMP_TASK_FOR_ROOT_NODE
#pragma omp task shared(i_lambda_leaves, i_lambda_midnodes, o_reduceOutput)
#endif

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>::traverse(this, i_lambda_leaves, i_lambda_midnodes, reduceOperator, o_reduceOutput);

#if OMP_TASK_FOR_ROOT_NODE
#pragma omp taskwait
#pragma omp flush(o_reduceOutput)
#endif

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(i_lambda_leaves, i_lambda_midnodes, reduceOperator, o_reduceOutput);
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceOutput
			)
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (p_this_node->isLeaf())
		{
			i_lambda_leaves(p_this_node, o_reduceOutput);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		TReduceValue firstValue(0);
		TReduceValue secondValue(0);

		if (p_this_node->first_child_node)
		{
			traverse(p_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &firstValue);
		}

		if (p_this_node->second_child_node)
		{
			traverse(p_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, p_reduceOperator, &secondValue);
		}

		/**
		 * REDUCTION
		 */
		if (p_this_node->first_child_node)
		{
			if (p_this_node->second_child_node)
			{
				p_reduceOperator(firstValue, secondValue, o_reduceOutput);
				i_lambda_midnodes(p_this_node, o_reduceOutput);
				return;
			}

			*o_reduceOutput = firstValue;
			i_lambda_midnodes(p_this_node, o_reduceOutput);
			return;
		}

		assert(p_this_node->second_child_node != nullptr);

		*o_reduceOutput = secondValue;

		i_lambda_midnodes(p_this_node, o_reduceOutput);
		return;
	}
};

template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traversal_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial<CLambdaFun1, CLambdaFun2, TReduceValue>::traverse(this, i_lambda_leaves, i_lambda_midnodes, reduceOperator, o_reduceOutput);

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


#endif
