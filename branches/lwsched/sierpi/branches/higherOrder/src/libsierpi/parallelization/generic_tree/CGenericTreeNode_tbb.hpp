/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_tbb.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_TBB_HPP_
#define CGENERICTREENODE_TBB_HPP_

#include "config.h"

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGenericTreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

#if CONFIG_TBB_TASK_AFFINITIES
	tbb::task::affinity_id	affinity_id;

inline void specializedConstructorMethod()
{
	affinity_id = 0;
}

#else

inline void specializedConstructorMethod()
{
}

#endif

/****************************************************************************
 * GENERIC TREE NODE TRAVERSAL
 */

/*
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode_Parallel
	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda_leaves;

public:
	CTraversalTask_GenericTreeNode_Parallel(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun i_lambda_leaves
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->second_child_node, lambda_leaves);
			spawn(t2);

			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->first_child_node, lambda_leaves);
			spawn_and_wait_for_all(t1);

			return nullptr;
		}


		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->first_child_node, lambda_leaves);
			t1.execute();
			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->second_child_node, lambda_leaves);
			t2.execute();
			return nullptr;
//		}
	}
};


template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel(
		CLambdaFun p_lambda_leaves
)
{
	tbb::task::spawn_root_and_wait((*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this, p_lambda_leaves)));
}




/*
 * parallel scan
 */
class CTraversalTask_GenericTreeNode_Parallel_Scan
{
public:
	template <
		typename CLambdaFun
	>
	static void execute(
			CGenericTreeNode_ *i_this_node,
			int i_thread_id,
			CLambdaFun i_lambda_leaves
	)
	{
		assert(i_this_node->workload_thread_id_start >= 0);
		assert(i_this_node->workload_thread_id_end >= 0);

		if (	i_this_node->workload_thread_id_start > i_thread_id ||
				i_this_node->workload_thread_id_end < i_thread_id
		)
			return;

		if (i_this_node->isLeaf())
		{
			if (i_this_node->workload_thread_id_start == i_thread_id)
				i_lambda_leaves(i_this_node);
			return;
		}

		if (i_this_node->first_child_node != nullptr && i_this_node->second_child_node != nullptr)
		{
			execute(i_this_node->first_child_node, i_thread_id, i_lambda_leaves);
			execute(i_this_node->second_child_node, i_thread_id, i_lambda_leaves);
			return;
		}

		if (i_this_node->first_child_node)
		{
			execute(i_this_node->first_child_node, i_thread_id, i_lambda_leaves);
			return;
		}

		execute(i_this_node->second_child_node, i_thread_id, i_lambda_leaves);
		return;
	}
};


template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda_leaves
)
{
#if CONFIG_SCAN_SPLIT_AND_JOIN_DISTRIBUTION && 0

	assert(workload_thread_id_start == 0);
	assert(workload_thread_id_end >= 0);

	tbb::parallel_for(
		0, workload_thread_id_end, 1,
		[this,&i_lambda_leaves](int i)
		{
			CTraversalTask_GenericTreeNode_Parallel_Scan::execute(this, i, i_lambda_leaves);
		}
	);

#else

	tbb::task::spawn_root_and_wait((*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this, i_lambda_leaves)));

#endif

}


/*
 * SERIAL
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda_leaves;

public:
	CTraversalTask_GenericTreeNode_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda_leaves
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_Serial<CLambdaFun>(this_node->first_child_node, lambda_leaves).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_Serial<CLambdaFun>(this_node->second_child_node, lambda_leaves).execute();

		return 0;
	}
};



template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Serial(
		CLambdaFun p_lambda
)
{
	CTraversalTask_GenericTreeNode_Serial<CLambdaFun>(this, p_lambda).execute();
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel
	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes_preorder;
	CLambdaFun3 lambda_midnodes_postorder;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes_preorder(i_lambda_midnodes_preorder),
		lambda_midnodes_postorder(i_lambda_midnodes_postorder)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif

	tbb::task* execute()
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes_preorder(this_node);
		}
		else
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			spawn_and_wait_for_all(t1);

			lambda_midnodes_postorder(this_node);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			t1.execute();

			lambda_midnodes_postorder(this_node);

			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			t2.execute();

			lambda_midnodes_postorder(this_node);

			return nullptr;
//		}
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
	>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder));
}



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
	>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder));
}


/*
 * serial
 */
template <
typename CLambdaFun1,
typename CLambdaFun2,
typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes_preorder;
	CLambdaFun3 lambda_midnodes_postorder;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes_preorder(i_lambda_midnodes_preorder),
		lambda_midnodes_postorder(i_lambda_midnodes_postorder)
	{
	}


	void execute()
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes_preorder(this_node);
		}
		else
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder).execute();

		lambda_midnodes_postorder(this_node);
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder).execute();
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel
	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes);
			spawn_and_wait_for_all(t1);

			lambda_midnodes(this_node);

			return nullptr;
		}

		if (this_node->first_child_node != nullptr)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes);
			t1.execute();

			lambda_midnodes(this_node);

			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes);
			t2.execute();

			lambda_midnodes(this_node);

			return nullptr;
//		}
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes));
}



template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes));
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes)
	{
	}


	void execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes).execute();

		lambda_midnodes(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes).execute();
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel
	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_midnodes(this_node, genericTreeDepth);
		}
		else
		{
			lambda_leaves(this_node, genericTreeDepth);
			return 0;
		}

		genericTreeDepth++;

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn_and_wait_for_all(t1);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t1.execute();

			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t2.execute();

			return nullptr;
//		}
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));
}



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
	}

	void execute()
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes(this_node, genericTreeDepth);
		}
		else
		{
			lambda_leaves(this_node, genericTreeDepth);
			return;
		}

		genericTreeDepth++;

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0).execute();
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel
	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif

	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, genericTreeDepth);
			return 0;
		}

		genericTreeDepth++;

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn_and_wait_for_all(t1);

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t1.execute();

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t2.execute();

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
//		}

		return 0;
	}
};


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));

	if (!this->isLeaf())
		i_lambda_midnodes(this, 0);
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));

	if (!this->isLeaf())
		i_lambda_midnodes(this, 0);
}



/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
	}

	void execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, genericTreeDepth);
			return;
		}

		genericTreeDepth++;

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth).execute();

		lambda_midnodes(this_node, genericTreeDepth-1);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0).execute();

	if (!this->isLeaf())
		i_lambda_midnodes(this, 0);
}



/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/
 
/*
 * parallel
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_Reduce_Parallel(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun i_lambda,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_rootReduceValue
	)	:
		this_node(i_this_node),
		lambda(i_lambda),
		reduceOperator(i_reduceOperator),
		rootReduceValue(o_rootReduceValue)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif


	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda(this_node, rootReduceValue);

			return nullptr;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst);
			spawn_and_wait_for_all(t1);

			reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);
			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, rootReduceValue);
			t1.execute();

			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, rootReduceValue);
			t2.execute();

			return nullptr;
//		}
	}
};


template <
	typename CLambdaFun,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_Reduce_Parallel(
		CLambdaFun i_lambda,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this, i_lambda, i_reduceOperator, o_reduceOutput));
}



template <
	typename CLambdaFun,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this, i_lambda, i_reduceOperator, o_reduceOutput));
}



/*
 * serial
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *reduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_Reduce_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			void (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_reduceValue
	)	:
		this_node(p_this_node),
		lambda(p_lambda),
		reduceOperator(p_reduceOperator),
		reduceValue(o_reduceValue)
	{
	}


	/**
	 * TASK
	 */
	void execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda(this_node, reduceValue);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */

		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond).execute();

		/**
		 * REDUCTION
		 */
		reduceOperator(reduceValueFirst, reduceValueSecond, reduceValue);

		return;
	}
};

template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Serial(
		CLambdaFun i_lambda,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceValue
)
{
	CTraversalTask_GenericTreeNode_Reduce_Serial<CLambdaFun, TReduceValue>(this, i_lambda, i_reduceOperator, o_reduceValue).execute();
}



/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/
/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel	: public tbb::task
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
			CGenericTreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_rootReduceValue
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		reduceOperator(i_reduceOperator),
		rootReduceValue(o_rootReduceValue)
	{
#if CONFIG_TBB_TASK_AFFINITIES
		set_affinity(this_node->affinity_id);
#endif
	}


#if CONFIG_TBB_TASK_AFFINITIES
	/**
	 * this method is executed by TBB when a new affinity was set
	 */
	void note_affinity_DISABLED(affinity_id id)
	{
		this_node->affinity_id = id;
	}
#endif


	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, rootReduceValue);
			return nullptr;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueSecond);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueFirst);
			spawn_and_wait_for_all(t1);

			reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator, rootReduceValue);
			t1.execute();

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
		}

//		if (this_node->second_child_node)
//		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator, rootReduceValue);
			t2.execute();

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
//		}
	}
};


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduceOutput));

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel_Scan(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduceOutput));

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *io_reduceValue
	)	:
		this_node(p_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		reduceOperator(i_reduceOperator),
		rootReduceValue(io_reduceValue)
	{
	}


	/**
	 * TASK
	 */
	void execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, rootReduceValue);
			return;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		if (this_node->first_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueFirst).execute();

		if (this_node->second_child_node)
			CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueSecond).execute();

		/**
		 * REDUCTION
		 */
		reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);

		lambda_midnodes(this_node, rootReduceValue);

		return;
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceValue
)
{
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial<CLambdaFun1, CLambdaFun2, TReduceValue>(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduceValue).execute();

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceValue);
}




#endif
