/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CFLUXSOLVER_AUGRIE_HPP_
#define CFLUXSOLVER_AUGRIE_HPP_

#include "libmath/CMath.hpp"
#include <iostream>
#include <cassert>

#include "tsunami_solver/AugRie.hpp"

/**
 * \brief AugumentedRiemann flux solver
 */
template <typename T>
class CFluxSolver_AugumentedRiemann	: public solver::AugRie<T>
{
	using solver::AugRie<T>::computeNetUpdates;

	/**
	 * This method is executed by the elementData computing kernel method
	 * to get the net updates for given fluxes
	 */
public:
	void computeNetUpdates(
			const CTsunamiEdgeData &i_edgeData_left,		///< edge data on left (left) edge
			const CTsunamiEdgeData &i_edgeData_right,		///< edge data on right (outer) edge
			CTsunamiEdgeData &o_edgeFlux_left,		///< output for left flux
			CTsunamiEdgeData &o_edgeFlux_right,		///< output for outer flux
			T &o_max_wave_speed_left,					///< maximum wave speed
			T &o_max_wave_speed_right,					///< maximum wave speed
			T i_gravitational_constant				///< gravitational constant
	)
	{
		solver::AugRie<T>::computeNetUpdates(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.qx, i_edgeData_right.qx,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left.h, o_edgeFlux_right.h,
				o_edgeFlux_left.qx, o_edgeFlux_right.qx,
				o_max_wave_speed_left
		);

		o_max_wave_speed_right = o_max_wave_speed_left;


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1

#error "ugly hack"
//		T max_bathymetry = CMath::max(i_edgeData_left.b, i_edgeData_right.b);

//		T absolute_h_left = i_edgeData_left.b + i_edgeData_left.h;
//		T absolute_h_right = i_edgeData_right.b + i_edgeData_right.h;

//		T moving_h_left = absolute_h_left - max_bathymetry;
//		T moving_h_right = absolute_h_right - max_bathymetry;

		T left_vx = i_edgeData_left.qx/i_edgeData_left.h;
		T left_vy = i_edgeData_left.qy/i_edgeData_left.h;

		T right_vx = i_edgeData_right.qx/i_edgeData_left.h;
		T right_vy = i_edgeData_right.qy/i_edgeData_left.h;

		// remove fluxes for 0th order basis functions
/*
		o_edgeFlux_left.h += i_edgeData_left.qx;
		o_edgeFlux_left.qx += left_vx*i_edgeData_left.qx + (T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left);
		o_edgeFlux_left.qy += left_vx*i_edgeData_left.qx + left_vy*i_edgeData_left.qx;

		o_edgeFlux_right.h -= i_edgeData_right.qx;
		o_edgeFlux_right.qx -= right_vx*i_edgeData_right.qx + (T)0.5*i_gravitational_constant*(moving_h_right*moving_h_right);
		o_edgeFlux_right.qy -= right_vx*i_edgeData_right.qx + right_vy*i_edgeData_right.qx;
*/

		o_edgeFlux_left.h += i_edgeData_left.qx;
		o_edgeFlux_left.qx += left_vx*i_edgeData_left.qx + (T)0.5*i_gravitational_constant*(i_edgeData_left.h*i_edgeData_left.h);
		o_edgeFlux_left.qy += left_vx*i_edgeData_left.qx + left_vy*i_edgeData_left.qx;


		o_edgeFlux_right.h -= i_edgeData_right.qx;
		o_edgeFlux_right.qx -= right_vx*i_edgeData_right.qx + (T)0.5*i_gravitational_constant*(i_edgeData_right.h*i_edgeData_right.h);
		o_edgeFlux_right.qy -= right_vx*i_edgeData_right.qx + right_vy*i_edgeData_right.qx;
#endif

		o_edgeFlux_left.qy = 0;
		o_edgeFlux_right.qy = 0;
	}
};



#endif
