/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CEDGE_COMM_NORMALS_DEPTH_PARALLEL_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CEDGE_COMM_NORMALS_DEPTH_PARALLEL_HPP_


#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "../kernels/simulation/CEdgeComm_Tsunami.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Tsunami_EdgeComm_Normals_Depth
{
	/// pointer to specialized traversator
	class CSpecialized_Tsunami_EdgeComm_Normals_Depth_Private *generic_traversator;

public:

	/// publish CKernelClass type to make it known to fluxCommTraversals
	typedef typename sierpi::kernels::CEdgeComm_Tsunami CKernelClass;


	/// reference to kernel class to call computeNetUpdates
	CKernelClass *cKernelClass;

	typedef TTsunamiDataScalar TReduceValue;

public:
	CSpecialized_Tsunami_EdgeComm_Normals_Depth();

	virtual ~CSpecialized_Tsunami_EdgeComm_Normals_Depth();

	void actionFirstPass(
			CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks
		);

	void actionSecondPass_Serial(
			CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks,
			TTsunamiDataScalar timestep_size,
			TReduceValue *o_reduceValue
		);

	void actionSecondPass_Parallel(
			CSimulationStacks<CTsunamiSimulationTypes> *p_cSimulationStacks,
			TTsunamiDataScalar timestep_size,
			TReduceValue *o_reduceValue
		);

	/**
	 * setup the initial partition traversal for the given factory
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
	);

	void setup_Partition(
			CSpecialized_Tsunami_EdgeComm_Normals_Depth &p_parent,
			CTriangle_Factory &p_triangleFactory
	);

	void setParameters(
			TTsunamiDataScalar p_delta_timestep,
			TTsunamiDataScalar p_square_side_length,
			TTsunamiDataScalar p_gravity
	);

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
	void setAdaptivityParameters(
			TTsunamiDataScalar i_refine_threshold,
			TTsunamiDataScalar i_coarsen_threshold
	);
#endif


	void setBoundaryCondition(
			EBoundaryConditions i_eBoundaryCondition
	);

	void setBoundaryDirichlet(
			const CTsunamiEdgeData *i_value
		);

	TTsunamiDataScalar getTimestepSize();
};


}
}

#endif
