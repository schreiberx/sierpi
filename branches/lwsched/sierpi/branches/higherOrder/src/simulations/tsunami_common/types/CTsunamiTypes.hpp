/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 23, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_TYPES_HPP
#define CTSUNAMI_TYPES_HPP

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libmath/CMath.hpp"
#include "../tsunami_config.h"
#include "../CTsunamiConfig.hpp"


#if !DEBUG
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#error "COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION activated in non-debug mode!"
#endif
#endif


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
	#include "CTsunamiTypes_0thOrder.hpp"
#else
	#include "CTsunamiTypes_1stOrder.hpp"
#endif


#include "CTsunamiTypes_VisualizationVertexData.hpp"


class CTsunamiSimulationTypes
{
public:
	typedef CTsunamiElementData TSimulationElementData;
	typedef CTsunamiEdgeData TSimulationEdgeData;

	typedef TTsunamiDataScalar TDataScalar;
	typedef TTsunamiVertexScalar TVertexScalar;

	typedef TTsunamiVertexScalar TVisualizationVertexScalar;

	typedef TTsunamiVisualizationVertexData TVisualizationVertexData;

};


typedef CSimulationStacks<CTsunamiSimulationTypes> CTsunamiSimulationStacks;



inline TTsunamiDataScalar getUnitCathetusLengthForDepth(int i_depth)
{
#if 1
	/*
	 * TODO: increase precision!!!
	 */

	static TTsunamiDataScalar catheti_lengths[64] = {
		1,
		0.70710678118654757274,
		0.5,
		0.35355339059327378637,
		0.25,
		0.17677669529663689318,
		0.125,
		0.088388347648318446592,
		0.0625,
		0.044194173824159223296,
		0.03125,
		0.022097086912079611648,
		0.015625,
		0.011048543456039805824,
		0.0078125,
		0.005524271728019902912,
		0.00390625,
		0.002762135864009951456,
		0.001953125,
		0.001381067932004975728,
		0.0009765625,
		0.000690533966002487864,
		0.00048828125,
		0.000345266983001243932,
		0.000244140625,
		0.000172633491500621966,
		0.0001220703125,
		8.6316745750310983e-05,
		6.103515625e-05,
		4.31583728751554915e-05,
		3.0517578125e-05,
		2.157918643757774575e-05,
		1.52587890625e-05,
		1.0789593218788872875e-05,
		7.62939453125e-06,
		5.3947966093944364375e-06,
		3.814697265625e-06,
		2.6973983046972182188e-06,
		1.9073486328125e-06,
		1.3486991523486091094e-06,
		9.5367431640625e-07,
		6.7434957617430455469e-07,
		4.76837158203125e-07,
		3.3717478808715227734e-07,
		2.384185791015625e-07,
		1.6858739404357613867e-07,
		1.1920928955078125e-07,
		8.4293697021788069336e-08,
		5.9604644775390625e-08,
		4.2146848510894034668e-08,
		2.98023223876953125e-08,
		2.1073424255447017334e-08,
		1.490116119384765625e-08,
		1.0536712127723508667e-08,
		7.450580596923828125e-09,
		5.2683560638617543335e-09,
		3.7252902984619140625e-09,
		2.6341780319308771668e-09,
		1.8626451492309570312e-09,
		1.3170890159654385834e-09,
		9.3132257461547851562e-10,
		6.5854450798271929169e-10,
		4.6566128730773925781e-10,
		3.2927225399135964584e-10,
	};
	return catheti_lengths[i_depth];

#else

	TTsunamiDataScalar cathetus = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		cathetus = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*cathetus;

	return cathetus;
#endif
}


inline TTsunamiDataScalar getUnitHypotenuseLengthForDepth(int i_depth)
{
#if 1
static TTsunamiDataScalar hypotenuse_lengths[64] = {
		1.4142135623730951455,
		1,
		0.70710678118654757274,
		0.5,
		0.35355339059327378637,
		0.25,
		0.17677669529663689318,
		0.125,
		0.088388347648318446592,
		0.0625,
		0.044194173824159223296,
		0.03125,
		0.022097086912079611648,
		0.015625,
		0.011048543456039805824,
		0.0078125,
		0.005524271728019902912,
		0.00390625,
		0.002762135864009951456,
		0.001953125,
		0.001381067932004975728,
		0.0009765625,
		0.000690533966002487864,
		0.00048828125,
		0.000345266983001243932,
		0.000244140625,
		0.000172633491500621966,
		0.0001220703125,
		8.6316745750310983e-05,
		6.103515625e-05,
		4.31583728751554915e-05,
		3.0517578125e-05,
		2.157918643757774575e-05,
		1.52587890625e-05,
		1.0789593218788872875e-05,
		7.62939453125e-06,
		5.3947966093944364375e-06,
		3.814697265625e-06,
		2.6973983046972182188e-06,
		1.9073486328125e-06,
		1.3486991523486091094e-06,
		9.5367431640625e-07,
		6.7434957617430455469e-07,
		4.76837158203125e-07,
		3.3717478808715227734e-07,
		2.384185791015625e-07,
		1.6858739404357613867e-07,
		1.1920928955078125e-07,
		8.4293697021788069336e-08,
		5.9604644775390625e-08,
		4.2146848510894034668e-08,
		2.98023223876953125e-08,
		2.1073424255447017334e-08,
		1.490116119384765625e-08,
		1.0536712127723508667e-08,
		7.450580596923828125e-09,
		5.2683560638617543335e-09,
		3.7252902984619140625e-09,
		2.6341780319308771668e-09,
		1.8626451492309570312e-09,
		1.3170890159654385834e-09,
		9.3132257461547851562e-10,
		6.5854450798271929169e-10,
		4.6566128730773925781e-10,
	};

	return hypotenuse_lengths[i_depth];

#else

	i_depth += 1;
	int depth_even = i_depth >> 1;

	TTsunamiDataScalar hypotenuse = (TTsunamiDataScalar)1.0/(TTsunamiDataScalar)(1<<depth_even);

	if (i_depth & 1)
		hypotenuse = (TTsunamiDataScalar)0.5*CMath::sqrt2<TTsunamiDataScalar>()*hypotenuse;

	return hypotenuse*2.0;

#endif
}
#endif
