/*
 * Simulation.hpp
 *
 *  Created on: Apr 20, 2012
 *      Author: breuera
 */

#ifndef SIMULATION_HPP_
#define SIMULATION_HPP_

#include "Grid.hpp"

namespace oneDimensional {
  template <typename T> class Simulation;
}

/**
 * Representation of the simulation.
 */
template <typename T> class oneDimensional::Simulation {
  //private:
    //! computational grid
    Grid<T> grid;

    //! CFL number used in the simulation (depends on the solver)
    T cflNumber;

  public:
    /**
     * Constructor of a simulation.
     */
    Simulation( const T i_cflNumber ):
      grid( 5000, //number of cells
            -10., 60. //x_min, x_max
           ) {
      cflNumber = i_cflNumber;
      grid.setInitialCellValues();
    }

    virtual ~Simulation() {
      // TODO Auto-generated destructor stub
    }

    /**
     * Execute one global time step.
     * @return width of the executed time step.
     */
    T executeOneGlobalTimestep() {
      //set the edge data
      grid.copyCellValuesToEdges();
      grid.setGhostValues();

      //compute net updates
      grid.computeNetUpdates();

      T l_globalTimeStepWidth = grid.computeMaximumGlobalTimeStepWidth(cflNumber);

      //update cells
      grid.updateCellValues(l_globalTimeStepWidth);

      return l_globalTimeStepWidth;
    }

    /**
     * Call the verification routine for a specified time.
     *
     * @param i_simulationTime simulation time.
     */
    void callVerifactionForGlobalTimeStep( const T i_simulationTime ) {
      grid.callVerifactionForGlobalTimeStep(i_simulationTime);
    }

    /**
     * Write the current grid data to a VTK file.
     */
    void writeVtkFile(const std::string i_path, const int i_fileNumber) {
      grid.writeVtkFile(i_path, i_fileNumber);
    }
};

#endif /* SIMULATION_HPP_ */
