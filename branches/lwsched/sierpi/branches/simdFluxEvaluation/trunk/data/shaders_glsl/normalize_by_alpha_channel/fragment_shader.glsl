// use opengl 3.3 core shaders
#version 330

in vec2 rast_texture_coord;
out vec4 frag_data;
uniform sampler2D sampler;

void main(void)
{
	vec4 texel = texelFetch(sampler, ivec2(gl_FragCoord.xy), 0);
	float inv_a = 1.0/texel.a;
//	inv_a = 1.0;
	frag_data = vec4(texel.xyz*inv_a, 1.0);
//	frag_data.xyza = vec4(texel.a);
}
