#!/bin/bash

# application is running for 100 seconds in average

killall sierpi_intel_itbb_tsunami_parallel_release

EXEC="../../build/sierpi_intel_itbb_tsunami_parallel_release"
PARAMS=" -d 4 -o 8192 -B 1 -a 16 -v -99"

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}

date
STARTSECONDS=`date +%s`

killall iPMO_server_release

echo "Starting ipmo server"
../../../ipmo_2012_05_28/build/iPMO_server_release -n 40 -v 2 > "output_""$DATE""_itbb_server.txt" &

echo "App 1: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_1.txt" &
PA1=$!

sleep 10

echo "App 2: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_2.txt" &
PA2=$!

sleep 20

echo "App 3: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_3.txt" &
PA3=$!

sleep 10

echo "App 4: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_4.txt" &
PA4=$!


wait $PA1
wait $PA2
wait $PA3
wait $PA4



ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS" > "output_""$DATE""_itbb.txt"

sleep 1
