Problem size with different number of CPUs using function optimization
Depth: 20
Timesteps: 
Cache: LLC-load-misses
THREADS	DEPTH	MTPS	CACHE_MISSES	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS
255,978,156 LLC-load-misses (scaled from 23.53%)
1	20	5.63986	       255,978,156 LLC-load-misses           (scaled from 23.53%)		500	0.0346964	185.922	2.09715e+06	303.872
315,805,155 LLC-load-misses (scaled from 23.54%)
2	20	10.9821	       315,805,155 LLC-load-misses           (scaled from 23.54%)		500	0.0346964	95.4804	2.09715e+06	303.872
377,405,341 LLC-load-misses (scaled from 23.53%)
3	20	15.9371	       377,405,341 LLC-load-misses           (scaled from 23.53%)		500	0.0346964	65.7948	2.09715e+06	303.872
450,721,126 LLC-load-misses (scaled from 23.55%)
4	20	20.4739	       450,721,126 LLC-load-misses           (scaled from 23.55%)		500	0.0346964	51.2151	2.09715e+06	303.872
540,391,533 LLC-load-misses (scaled from 23.55%)
5	20	24.2351	       540,391,533 LLC-load-misses           (scaled from 23.55%)		500	0.0346964	43.2668	2.09715e+06	303.872
662,100,756 LLC-load-misses (scaled from 23.53%)
6	20	27.3473	       662,100,756 LLC-load-misses           (scaled from 23.53%)		500	0.0346964	38.343	2.09715e+06	303.872
617,307,312 LLC-load-misses (scaled from 23.44%)
7	20	31.4518	       617,307,312 LLC-load-misses           (scaled from 23.44%)		500	0.0346964	33.3391	2.09715e+06	303.872
584,816,577 LLC-load-misses (scaled from 23.53%)
8	20	35.7954	       584,816,577 LLC-load-misses           (scaled from 23.53%)		500	0.0346964	29.2936	2.09715e+06	303.872
661,919,046 LLC-load-misses (scaled from 23.55%)
9	20	38.3993	       661,919,046 LLC-load-misses           (scaled from 23.55%)		500	0.0346964	27.3072	2.09715e+06	303.872
641,879,538 LLC-load-misses (scaled from 23.53%)
10	20	42.0492	       641,879,538 LLC-load-misses           (scaled from 23.53%)		500	0.0346964	24.9369	2.09715e+06	303.872
662,145,795 LLC-load-misses (scaled from 23.54%)
11	20	44.6253	       662,145,795 LLC-load-misses           (scaled from 23.54%)		500	0.0346964	23.4973	2.09715e+06	303.872
759,787,350 LLC-load-misses (scaled from 23.55%)
12	20	46.3937	       759,787,350 LLC-load-misses           (scaled from 23.55%)		500	0.0346964	22.6017	2.09715e+06	303.872
