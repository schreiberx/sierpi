/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_CLUSTER_ROOT_TSUNAMI_HPP_
#define COPENGL_VERTICES_CLUSTER_ROOT_TSUNAMI_HPP_


#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlViewport.hpp"


class COpenGL_Vertices_Cluster_Root_Tsunami
{
	typedef CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar	T;

private:
	CGlVertexArrayObject vao;
	CGlBuffer vertex_buffer;
	size_t max_number_of_vertices;

public:
	CError error;


	COpenGL_Vertices_Cluster_Root_Tsunami()
	{
		GLint glfloat = (sizeof(T) == 4 ? GL_FLOAT : GL_DOUBLE);

		max_number_of_vertices = 128*3;

		vao.bind();
			vertex_buffer.bind();
			vertex_buffer.resize(max_number_of_vertices*sizeof(T)*3);

				// vertex coordinates
				glVertexAttribPointer(0, 3, glfloat, GL_FALSE, 3*sizeof(T), 0);
				glEnableVertexAttribArray(0);

		vao.unbind();
	}




	/**
	 * initialize rendering
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRendering()
	{
		/**
		 * !!! We have to do the setup right here since other threads are not allowed to access the context !!!
		 */
		vao.bind();

		CGlErrorCheck();
	}



	inline void render(
			T *i_vertex_buffer_data,		///< pointer to vertices
			size_t i_vertices_count			///< number of vertices
	)
	{
		for (size_t i = 0; i < i_vertices_count; i += max_number_of_vertices)
		{
			size_t block_size = CMath::min<size_t>(max_number_of_vertices, i_vertices_count - i);

			vertex_buffer.bind();
			vertex_buffer.subData(0, block_size*(3*sizeof(T)), i_vertex_buffer_data);

			glDrawArrays(GL_TRIANGLES, 0, block_size);

			i_vertex_buffer_data += 3*block_size;
		}
	}



	inline void shutdownRendering()
	{
		vao.unbind();
		CGlErrorCheck();
	}

};


#endif
