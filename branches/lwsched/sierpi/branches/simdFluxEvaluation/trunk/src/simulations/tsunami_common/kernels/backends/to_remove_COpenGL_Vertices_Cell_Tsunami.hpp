/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_ELEMENT_TSUNAMI_HPP_
#define COPENGL_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "COpenGL_Vertices_Cell_Root_Tsunami.hpp"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "../../CProjections.hpp"

namespace sierpi
{
namespace kernels
{


template <
	int t_visualizationType		///< type of visualization
									///< 0: simple
									///< 1: surface triangles at edge midpoints
									///< 2: aligned surface triangles
									///< 3: simple surface triangles
									///< 4: aligned bathymetry triangles
>
class COpenGL_Vertices_Cell_Tsunami
{
public:
	typedef typename CTsunamiSimulationStacksAndTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar 			TVertexScalar;
	typedef TVertexScalar 	T;

	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<COpenGL_Vertices_Cell_Tsunami<t_visualizationType>, CTsunamiSimulationStacksAndTypes> TRAV;

private:
	T *vertex_attrib_buffer;
	T *current_vertex_attrib;

	T *vertex_attrib_buffer_end;
	size_t max_vertices;

	T scale_min;
	T scale_factor;

	COpenGL_Vertices_Cell_Root_Tsunami *cOpenGL_Vertices_Cell_Root_Tsunami;

	int dof_id;

public:

	inline COpenGL_Vertices_Cell_Tsunami()	:
			max_vertices(COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT)
	{
		// allocate space for VERTICES AND NORMALS!!!
		vertex_attrib_buffer = new T[3*3*max_vertices*2];
		vertex_attrib_buffer_end = vertex_attrib_buffer+(3*max_vertices*2);

		dof_id = 0;
	}


	virtual inline ~COpenGL_Vertices_Cell_Tsunami()
	{
		delete[] vertex_attrib_buffer;
	}


	inline void renderOpenGLVertexArray(size_t p_vertices_count)
	{
		assert(cOpenGL_Vertices_Cell_Root_Tsunami != nullptr);

		cOpenGL_Vertices_Cell_Root_Tsunami->renderOpenGLVertexArray(
				vertex_attrib_buffer,
				p_vertices_count
			);
	}


	inline T fixHeight(T h)
	{
		return ((h-scale_min)*scale_factor);
	}


	inline void op_cell(
			T i_left_vertex_coord_x, T i_left_vertex_coord_y,
			T i_right_vertex_coord_x, T i_right_vertex_coord_y,
			T i_top_vertex_coord_x, T i_top_vertex_coord_y,

			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,

			CCellData *i_cell_data
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cell_data->validation.testVertices(
				i_left_vertex_coord_x,
				i_left_vertex_coord_y,
				i_right_vertex_coord_x,
				i_right_vertex_coord_y,
				i_top_vertex_coord_x,
				i_top_vertex_coord_y
			);

		i_cell_data->validation.testNormals(
				i_hyp_normal_x,
				i_hyp_normal_y,
				i_right_normal_x,
				i_right_normal_y,
				i_left_normal_x,
				i_left_normal_y
			);
#endif

		/*******************************8
		 * SIMPLE, CONSTANT DISPLACEMENT
		 */
		if (t_visualizationType == 0)
		{
			T y;
			T hu, hv;

			switch (dof_id)
			{
			/**
			 * SURFACE HEIGHT
			 */
			case 0:
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
				if (i_cell_data->dofs_center.h <= 0)
					return;

				y = fixHeight(i_cell_data->dofs_center.h + i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

				y = fixHeight((1.0/3.0)*(
						i_cell_data->dofs_hyp_edge.h + i_cell_data->dofs_hyp_edge.b +
						i_cell_data->dofs_left_edge.h + i_cell_data->dofs_left_edge.b +
						i_cell_data->dofs_right_edge.h + i_cell_data->dofs_right_edge.b
					));
#endif
				break;


			/**
			 * X- or Y-MOMENTUM
			 */
			case 1:
			case 2:

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

				hu = fixHeight(i_cell_data->dofs_center.hu);
				hv = fixHeight(i_cell_data->dofs_center.hv);

#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

				hu = fixHeight((1.0/3.0)*(
						i_cell_data->dofs_hyp_edge.hu +
						i_cell_data->dofs_left_edge.hu +
						i_cell_data->dofs_right_edge.hu
					));

				hv = fixHeight((1.0/3.0)*(
						i_cell_data->dofs_hyp_edge.hv +
						i_cell_data->dofs_left_edge.hv +
						i_cell_data->dofs_right_edge.hv
					));

#endif


				CProjections::rotateToBasisWithXAxis(
						&hu,
						&hv,
						-i_right_normal_x,
						-i_right_normal_y
					);

				if (dof_id == 1)
					y = hu;
				else
					y = hv;
				break;


			/**
			 * BATHYMETRY
			 */
			case 3:
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
				y = fixHeight(i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
				y = fixHeight((1.0/3.0)*(
						i_cell_data->dofs_hyp_edge.b +
						i_cell_data->dofs_left_edge.b +
						i_cell_data->dofs_right_edge.b
					));
#endif
				break;
			}

			current_vertex_attrib[0*3+0] = i_left_vertex_coord_x;
			current_vertex_attrib[0*3+1] = y;
			current_vertex_attrib[0*3+2] = -i_left_vertex_coord_y;

			current_vertex_attrib[1*3+0] = 0;
			current_vertex_attrib[1*3+1] = 1;
			current_vertex_attrib[1*3+2] = 0;

			current_vertex_attrib[2*3+0] = i_right_vertex_coord_x;
			current_vertex_attrib[2*3+1] = y;
			current_vertex_attrib[2*3+2] = -i_right_vertex_coord_y;

			current_vertex_attrib[3*3+0] = 0;
			current_vertex_attrib[3*3+1] = 1;
			current_vertex_attrib[3*3+2] = 0;

			current_vertex_attrib[4*3+0] = i_top_vertex_coord_x;
			current_vertex_attrib[4*3+1] = y;
			current_vertex_attrib[4*3+2] = -i_top_vertex_coord_y;

			current_vertex_attrib[5*3+0] = 0;
			current_vertex_attrib[5*3+1] = 1;
			current_vertex_attrib[5*3+2] = 0;
		}


		/*******************************8
		 * render aligned water surface
		 */
		if (t_visualizationType == 2)
		{
			T hyp_edge_x = (i_left_vertex_coord_x+i_right_vertex_coord_x)*(T)0.5;
			T hyp_edge_y = (i_left_vertex_coord_y+i_right_vertex_coord_y)*(T)0.5;

			T right_edge_x = (i_right_vertex_coord_x+i_top_vertex_coord_x)*(T)0.5;
			T right_edge_y = (i_right_vertex_coord_y+i_top_vertex_coord_y)*(T)0.5;

			T left_edge_x = (i_top_vertex_coord_x+i_left_vertex_coord_x)*(T)0.5;
			T left_edge_y = (i_top_vertex_coord_y+i_left_vertex_coord_y)*(T)0.5;

			T hyp_dx = right_edge_x - left_edge_x;
			T hyp_dy = right_edge_y - left_edge_y;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
			T hyp_dhb = i_cell_data->dofs_right_edge.h+i_cell_data->dofs_right_edge.b - (i_cell_data->dofs_left_edge.h+i_cell_data->dofs_left_edge.b);
#endif

			T left_dx = right_edge_x - hyp_edge_x;
			T left_dy = right_edge_y - hyp_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
			T left_dhb = i_cell_data->dofs_right_edge.h+i_cell_data->dofs_right_edge.b - (i_cell_data->dofs_hyp_edge.h+i_cell_data->dofs_hyp_edge.b);
#endif

			current_vertex_attrib[0*3+0] = hyp_edge_x-hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
			current_vertex_attrib[0*3+1] = fixHeight(i_cell_data->dofs_center.h+i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
			current_vertex_attrib[0*3+1] = fixHeight((i_cell_data->dofs_hyp_edge.h+i_cell_data->dofs_hyp_edge.b) - hyp_dhb);
#endif
			current_vertex_attrib[0*3+2] = -(hyp_edge_y-hyp_dy);

			current_vertex_attrib[2*3+0] = hyp_edge_x+hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
			current_vertex_attrib[2*3+1] = fixHeight(i_cell_data->dofs_center.h+i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
			current_vertex_attrib[2*3+1] = fixHeight((i_cell_data->dofs_hyp_edge.h+i_cell_data->dofs_hyp_edge.b) + hyp_dhb);
#endif
			current_vertex_attrib[2*3+2] = -(hyp_edge_y+hyp_dy);

			current_vertex_attrib[4*3+0] = left_edge_x+left_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
			current_vertex_attrib[4*3+1] = fixHeight(i_cell_data->dofs_center.h+i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1
			current_vertex_attrib[4*3+1] = fixHeight((i_cell_data->dofs_left_edge.h+i_cell_data->dofs_left_edge.b)+left_dhb);
#endif

			current_vertex_attrib[4*3+2] = -(left_edge_y+left_dy);

			T bx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[2*3+0]);
			T by = (current_vertex_attrib[0*3+1]-current_vertex_attrib[2*3+1]);
			T bz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[2*3+2]);

			T cx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[4*3+0]);
			T cy = (current_vertex_attrib[0*3+1]-current_vertex_attrib[4*3+1]);
			T cz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[4*3+2]);

			T nx = by*cz-bz*cy;
			T ny = bz*cx-bx*cz;
			T nz = bx*cy-by*cx;

			T a = (T)1.0/CMath::sqrt<T>(nx*nx + ny*ny + nz*nz);

			nx *= a;
			ny *= a;
			nz *= a;

			current_vertex_attrib[1*3+0] = nx;
			current_vertex_attrib[1*3+1] = ny;
			current_vertex_attrib[1*3+2] = nz;

			current_vertex_attrib[3*3+0] = nx;
			current_vertex_attrib[3*3+1] = ny;
			current_vertex_attrib[3*3+2] = nz;

			current_vertex_attrib[5*3+0] = nx;
			current_vertex_attrib[5*3+1] = ny;
			current_vertex_attrib[5*3+2] = nz;
		}


		/*******************************************
		 * render simple bathymetry
		 */
		if (t_visualizationType == 3)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			T y = fixHeight(i_cell_data->dofs_center.b);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			T y = fixHeight((1.0/3.0)*(i_cell_data->dofs_hyp_edge.b + i_cell_data->dofs_left_edge.b + i_cell_data->dofs_right_edge.b));
#endif
			current_vertex_attrib[0*3+0] = i_left_vertex_coord_x;
			current_vertex_attrib[0*3+1] = y;
			current_vertex_attrib[0*3+2] = -i_left_vertex_coord_y;

			current_vertex_attrib[1*3+0] = 0;
			current_vertex_attrib[1*3+1] = 1;
			current_vertex_attrib[1*3+2] = 0;

			current_vertex_attrib[2*3+0] = i_right_vertex_coord_x;
			current_vertex_attrib[2*3+1] = y;
			current_vertex_attrib[2*3+2] = -i_right_vertex_coord_y;

			current_vertex_attrib[3*3+0] = 0;
			current_vertex_attrib[3*3+1] = 1;
			current_vertex_attrib[3*3+2] = 0;

			current_vertex_attrib[4*3+0] = i_top_vertex_coord_x;
			current_vertex_attrib[4*3+1] = y;
			current_vertex_attrib[4*3+2] = -i_top_vertex_coord_y;

			current_vertex_attrib[5*3+0] = 0;
			current_vertex_attrib[5*3+1] = 1;
			current_vertex_attrib[5*3+2] = 0;
		}


		/*
		 * render aligned bathymetry
		 */
		if (t_visualizationType == 4)
		{
			T hyp_edge_x = (i_left_vertex_coord_x+i_right_vertex_coord_x)*(TTsunamiDataScalar)0.5;
			T hyp_edge_y = (i_left_vertex_coord_y+i_right_vertex_coord_y)*(TTsunamiDataScalar)0.5;

			T right_edge_x = (i_right_vertex_coord_x+i_top_vertex_coord_x)*(TTsunamiDataScalar)0.5;
			T right_edge_y = (i_right_vertex_coord_y+i_top_vertex_coord_y)*(TTsunamiDataScalar)0.5;

			T left_edge_x = (i_top_vertex_coord_x+i_left_vertex_coord_x)*(TTsunamiDataScalar)0.5;
			T left_edge_y = (i_top_vertex_coord_y+i_left_vertex_coord_y)*(TTsunamiDataScalar)0.5;

			T hyp_dx = right_edge_x - left_edge_x;
			T hyp_dy = right_edge_y - left_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			T hyp_dh = i_cell_data->dofs_right_edge.h - i_cell_data->dofs_left_edge.h;
#endif

			T left_dx = right_edge_x - hyp_edge_x;
			T left_dy = right_edge_y - hyp_edge_y;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			T left_dh = i_cell_data->dofs_right_edge.h - i_cell_data->dofs_hyp_edge.h;
#endif

			current_vertex_attrib[0*3+0] = hyp_edge_x-hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[0*3+1] = fixHeight(i_cell_data->dofs_center.h);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			current_vertex_attrib[0*3+1] = fixHeight(i_cell_data->dofs_hyp_edge.h-hyp_dh);
#endif
			current_vertex_attrib[0*3+2] = -(hyp_edge_y-hyp_dy);

			current_vertex_attrib[2*3+0] = hyp_edge_x+hyp_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[2*3+1] = fixHeight(i_cell_data->dofs_center.h);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			current_vertex_attrib[2*3+1] = fixHeight(i_cell_data->dofs_hyp_edge.h+hyp_dh);
#endif
			current_vertex_attrib[2*3+2] = -(hyp_edge_y+hyp_dy);

			current_vertex_attrib[4*3+0] = left_edge_x+left_dx;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			current_vertex_attrib[4*3+1] = fixHeight(i_cell_data->dofs_center.h);
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			current_vertex_attrib[4*3+1] = fixHeight(i_cell_data->dofs_left_edge.h+left_dh);
#endif

			current_vertex_attrib[4*3+2] = -(left_edge_y+left_dy);

			T bx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[2*3+0]);
			T by = (current_vertex_attrib[0*3+1]-current_vertex_attrib[2*3+1]);
			T bz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[2*3+2]);

			T cx = (current_vertex_attrib[0*3+0]-current_vertex_attrib[4*3+0]);
			T cy = (current_vertex_attrib[0*3+1]-current_vertex_attrib[4*3+1]);
			T cz = (current_vertex_attrib[0*3+2]-current_vertex_attrib[4*3+2]);

			T nx = by*cz-bz*cy;
			T ny = bz*cx-bx*cz;
			T nz = bx*cy-by*cx;

			T a = (T)1.0/CMath::sqrt<T>(nx*nx + ny*ny + nz*nz);

			nx *= a;
			ny *= a;
			nz *= a;

			current_vertex_attrib[1*3+0] = nx;
			current_vertex_attrib[1*3+1] = ny;
			current_vertex_attrib[1*3+2] = nz;

			current_vertex_attrib[3*3+0] = nx;
			current_vertex_attrib[3*3+1] = ny;
			current_vertex_attrib[3*3+2] = nz;

			current_vertex_attrib[5*3+0] = nx;
			current_vertex_attrib[5*3+1] = ny;
			current_vertex_attrib[5*3+2] = nz;
		}

		current_vertex_attrib += 3*3*2;


		assert(current_vertex_attrib <= vertex_attrib_buffer_end);
		if (current_vertex_attrib == vertex_attrib_buffer_end)
		{
			renderOpenGLVertexArray(max_vertices);
			current_vertex_attrib = vertex_attrib_buffer;
		}
	}

	inline void traversal_pre_hook()
	{
		current_vertex_attrib = vertex_attrib_buffer;
	}

	inline void traversal_post_hook()
	{
		if (current_vertex_attrib > vertex_attrib_buffer)
		{
			// involve stored vertices AND normals in primitive counting
			renderOpenGLVertexArray((size_t)(current_vertex_attrib-vertex_attrib_buffer)/(3*2));
			return;
		}
	}

	/**
	 * setup traversator based on parent triangle
	 */
	inline void setup_WithKernel(
			COpenGL_Vertices_Cell_Tsunami &parent_kernel
	)
	{
		scale_min = parent_kernel.scale_min;
		scale_factor = parent_kernel.scale_factor;
		cOpenGL_Vertices_Cell_Root_Tsunami = parent_kernel.cOpenGL_Vertices_Cell_Root_Tsunami;
	}


	void setup(
			T i_scale_min,
			T i_scale_factor,
			int i_dof_id,
			COpenGL_Vertices_Cell_Root_Tsunami *i_cOpenGL_Vertices_Cell_Root_Tsunami
	)
	{
		scale_min = i_scale_min;
		scale_factor = i_scale_factor;
		dof_id = i_dof_id;
		cOpenGL_Vertices_Cell_Root_Tsunami = i_cOpenGL_Vertices_Cell_Root_Tsunami;
	}
};


}
}

#endif
