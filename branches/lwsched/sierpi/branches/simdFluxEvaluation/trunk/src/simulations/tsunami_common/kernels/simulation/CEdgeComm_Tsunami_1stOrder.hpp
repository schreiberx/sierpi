/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_


#include "../../tsunami_config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// edge projections
#include "../../CProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (op_cell)
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Tsunami_1stOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;


	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiSimulationEdgeData CEdgeData;
	typedef CTsunamiSimulationCellData CCellData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<t_storeElementUpdatesOnly>, CTsunamiSimulationStacksAndTypes > TRAV;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_1stOrder()
	{
		boundary_dirichlet.dofs_center.h = CMath::numeric_inf<T>();
		boundary_dirichlet.dofs_center.hu = CMath::numeric_inf<T>();
		boundary_dirichlet.dofs_center.hv = CMath::numeric_inf<T>();
	}


	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CTsunamiSimulationCellData &i_f_t0,
			CTsunamiSimulationCellData &i_f_slope_t0,
			CTsunamiSimulationCellData *o_f_t1_approx,
			T i_timestep_size
	)
	{
		o_f_t1_approx->dofs_left_edge.h		=	i_f_t0.dofs_left_edge.h	+ i_timestep_size*i_f_slope_t0.dofs_left_edge.h;
		o_f_t1_approx->dofs_left_edge.hu		=	i_f_t0.dofs_left_edge.hu	+ i_timestep_size*i_f_slope_t0.dofs_left_edge.hu;
		o_f_t1_approx->dofs_left_edge.hv		=	i_f_t0.dofs_left_edge.hv	+ i_timestep_size*i_f_slope_t0.dofs_left_edge.hv;
		o_f_t1_approx->dofs_left_edge.b		=	i_f_t0.dofs_left_edge.b;

		o_f_t1_approx->dofs_right_edge.h		=	i_f_t0.dofs_right_edge.h		+ i_timestep_size*i_f_slope_t0.dofs_right_edge.h;
		o_f_t1_approx->dofs_right_edge.hu	=	i_f_t0.dofs_right_edge.hu	+ i_timestep_size*i_f_slope_t0.dofs_right_edge.hu;
		o_f_t1_approx->dofs_right_edge.hv	=	i_f_t0.dofs_right_edge.hv	+ i_timestep_size*i_f_slope_t0.dofs_right_edge.hv;
		o_f_t1_approx->dofs_right_edge.b		=	i_f_t0.dofs_right_edge.b;

		o_f_t1_approx->dofs_hyp_edge.h		=	i_f_t0.dofs_hyp_edge.h	+ i_timestep_size*i_f_slope_t0.dofs_hyp_edge.h;
		o_f_t1_approx->dofs_hyp_edge.hu		=	i_f_t0.dofs_hyp_edge.hu	+ i_timestep_size*i_f_slope_t0.dofs_hyp_edge.hu;
		o_f_t1_approx->dofs_hyp_edge.hv		=	i_f_t0.dofs_hyp_edge.hv	+ i_timestep_size*i_f_slope_t0.dofs_hyp_edge.hv;
		o_f_t1_approx->dofs_hyp_edge.b		=	i_f_t0.dofs_hyp_edge.b;

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CTsunamiSimulationCellData &i_f_slope_t0,
			CTsunamiSimulationCellData &i_f_slope_t1,
			CTsunamiSimulationCellData *io_f_t0_t1,
			T i_timestep_size
	)
	{
		io_f_t0_t1->dofs_left_edge.h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_left_edge.h + i_f_slope_t1.dofs_left_edge.h);
		io_f_t0_t1->dofs_left_edge.hu	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_left_edge.hu + i_f_slope_t1.dofs_left_edge.hu);
		io_f_t0_t1->dofs_left_edge.hv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_left_edge.hv + i_f_slope_t1.dofs_left_edge.hv);

		io_f_t0_t1->dofs_right_edge.h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_right_edge.h + i_f_slope_t1.dofs_right_edge.h);
		io_f_t0_t1->dofs_right_edge.hu	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_right_edge.hu + i_f_slope_t1.dofs_right_edge.hu);
		io_f_t0_t1->dofs_right_edge.hv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_right_edge.hv + i_f_slope_t1.dofs_right_edge.hv);

		io_f_t0_t1->dofs_hyp_edge.h		+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_hyp_edge.h + i_f_slope_t1.dofs_hyp_edge.h);
		io_f_t0_t1->dofs_hyp_edge.hu	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_hyp_edge.hu + i_f_slope_t1.dofs_hyp_edge.hu);
		io_f_t0_t1->dofs_hyp_edge.hv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_hyp_edge.hv + i_f_slope_t1.dofs_hyp_edge.hv);

		// TODO: which CFL to use?
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}


	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_1stOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-cluster
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-cluster
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs_center = i_cCellData->dofs_hyp_edge;

		// rotate edge comm data to normal space
		CProjections::toHypEdgeSpace(
			&(o_edge_comm_data->dofs_center.hu),
			&(o_edge_comm_data->dofs_center.hv)
		);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_cEdgeData
	)
	{
		// store edge comm data to output
		o_cEdgeData->dofs_center = i_cCellData->dofs_right_edge;

		// rotate edge comm data to normal space
		CProjections::toRightEdgeSpace(
				&o_cEdgeData->dofs_center.hu,
				&o_cEdgeData->dofs_center.hv
			);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRightEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_cEdgeData
	)
	{
		// store edge comm data to output
		o_cEdgeData->dofs_center = i_cCellData->dofs_left_edge;

		// rotate edge comm data to normal space
		CProjections::toLeftEdgeSpace(
				&o_cEdgeData->dofs_center.hu,
				&o_cEdgeData->dofs_center.hv
			);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupLeftEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_cEdgeData
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			o_cEdgeData->dofs_center = i_cCellData->dofs_hyp_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			o_cEdgeData->dofs_center = i_cCellData->dofs_hyp_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_cEdgeData->dofs_center = i_cCellData->dofs_hyp_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_cEdgeData->dofs_center.h = i_cCellData->dofs_hyp_edge.h;
			o_cEdgeData->dofs_center.hu = -i_cCellData->dofs_hyp_edge.hu;
			o_cEdgeData->dofs_center.hv = -i_cCellData->dofs_hyp_edge.hv;
			o_cEdgeData->dofs_center.b = -i_cCellData->dofs_hyp_edge.b;
			break;
		}

		// rotate edge comm data to normal space
		CProjections::toHypEdgeSpaceAndInvert(
				&o_cEdgeData->dofs_center.hu,
				&o_cEdgeData->dofs_center.hv
			);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cCellData->validation.setupHypEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_cEdgeData
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			o_cEdgeData->dofs_center = i_cCellData->dofs_right_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			o_cEdgeData->dofs_center = i_cCellData->dofs_right_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_cEdgeData->dofs_center = i_cCellData->dofs_right_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_cEdgeData->dofs_center.h = i_cCellData->dofs_right_edge.h;
			o_cEdgeData->dofs_center.hu = -i_cCellData->dofs_right_edge.hu;
			o_cEdgeData->dofs_center.hv = -i_cCellData->dofs_right_edge.hv;
			o_cEdgeData->dofs_center.b = -i_cCellData->dofs_right_edge.b;
			break;
		}

		// rotate edge comm data to edge space
		CProjections::toRightEdgeSpaceAndInvert(
				&o_cEdgeData->dofs_center.hu,
				&o_cEdgeData->dofs_center.hv
			);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cCellData->validation.setupRightEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cCellData,

			CTsunamiSimulationEdgeData *o_cEdgeData
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_cEdgeData->dofs_center = i_cCellData->dofs_left_edge;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			o_cEdgeData->dofs_center = i_cCellData->dofs_left_edge;
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_cEdgeData->dofs_center = i_cCellData->dofs_left_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_cEdgeData->dofs_center.h = i_cCellData->dofs_left_edge.h;
			o_cEdgeData->dofs_center.hu = -i_cCellData->dofs_left_edge.hu;
			o_cEdgeData->dofs_center.hv = -i_cCellData->dofs_left_edge.hv;
			o_cEdgeData->dofs_center.b = -i_cCellData->dofs_left_edge.b;
			break;
		}

		// rotate edge comm data to normal space
		CProjections::toLeftEdgeSpaceAndInvert(
				&o_cEdgeData->dofs_center.hu,
				&o_cEdgeData->dofs_center.hv
			);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cCellData->validation.setupLeftEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void op_cell(
			T i_hyp_normal_x, T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x, T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x, T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			int i_depth,

			CTsunamiSimulationCellData *io_cell_data,	///< element data

			CTsunamiSimulationEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CTsunamiSimulationEdgeData *i_right_edge_net_update,	///< incoming flux from right edge
			CTsunamiSimulationEdgeData *i_left_edge_net_update		///< incoming flux from left edge
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cell_data->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
			);
#endif

		CProjections::fromHypEdgeSpace(
				&i_hyp_edge_net_update->dofs_center.hu,
				&i_hyp_edge_net_update->dofs_center.hv
			);

		CProjections::fromRightEdgeSpace(
				&i_right_edge_net_update->dofs_center.hu,
				&i_right_edge_net_update->dofs_center.hv
			);

		CProjections::fromLeftEdgeSpace(
				&i_left_edge_net_update->dofs_center.hu,
				&i_left_edge_net_update->dofs_center.hv
			);

#if 0
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << *i_hyp_edge_net_update << std::endl;
		std::cout << *i_right_edge_net_update << std::endl;
		std::cout << *i_left_edge_net_update << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
#endif
		computeAndUpdateCFLCondition3(
				i_hyp_edge_net_update->dofs_center.max_wave_speed,
				i_right_edge_net_update->dofs_center.max_wave_speed,
				i_left_edge_net_update->dofs_center.max_wave_speed,
				i_depth,
				&(io_cell_data->cfl_domain_size_div_max_wave_speed)
			);

		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);

		if (CMath::isNan(io_cell_data->dofs_hyp_edge.h + io_cell_data->dofs_right_edge.h + io_cell_data->dofs_left_edge.h))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*cathetus_real_length;


//		T six_div_h2 = (T)6.0/(cat_length*cat_length);
//		T timestep_six_div_h2 = delta_timestep*six_div_h2;
		T six_div_h2 = (T)(6.0)/(cat_length*cat_length);
		T two_div_h = (T)2.0/cat_length;
		T h_div_3 = cat_length/(T)3.0;

// 4 ops
// sum: 224
		T inv_hyp_edge_h = (T)1.0/io_cell_data->dofs_hyp_edge.h;
		T inv_right_edge_h = (T)1.0/io_cell_data->dofs_right_edge.h;
		T inv_left_edge_h = (T)1.0/io_cell_data->dofs_left_edge.h;

//		std::cout <<timestep_six_div_h2 << " " << two_div_h << " " << h_div_3 << " " << inv_hyp_edge_h << " " << inv_right_edge_h << " " << inv_left_edge_h << std::endl;

// 3 ops
// sum: 227

		T elementData_sum_edge_hu = io_cell_data->dofs_left_edge.hu + io_cell_data->dofs_hyp_edge.hu + io_cell_data->dofs_right_edge.hu;
		T elementData_sum_edge_hv = io_cell_data->dofs_left_edge.hv + io_cell_data->dofs_hyp_edge.hv + io_cell_data->dofs_right_edge.hv;
// 4 ops
// sum: 231

		T triangle_area = cat_length*cat_length*(T)0.5;
// 2 ops
// sum: 233

		T elementData_hyp_edge_hu2_MUL_inv_hyp_edge_h = io_cell_data->dofs_hyp_edge.hu*io_cell_data->dofs_hyp_edge.hu*inv_hyp_edge_h;
		T elementData_right_edge_hu2_MUL_inv_right_edge_h = io_cell_data->dofs_right_edge.hu*io_cell_data->dofs_right_edge.hu*inv_right_edge_h;
		T elementData_left_edge_hu2_MUL_inv_left_edge_h = io_cell_data->dofs_left_edge.hu*io_cell_data->dofs_left_edge.hu*inv_left_edge_h;
// 6 ops
// sum: 239

		T sum_elementData_edges_hu2_MUL_inv_edges_h = elementData_left_edge_hu2_MUL_inv_left_edge_h + elementData_hyp_edge_hu2_MUL_inv_hyp_edge_h + elementData_right_edge_hu2_MUL_inv_right_edge_h;
// 2 ops
// sum: 241

		T elementData_hyp_edge_hv2_MUL_inv_hyp_edge_h = io_cell_data->dofs_hyp_edge.hv*io_cell_data->dofs_hyp_edge.hv*inv_hyp_edge_h;
		T elementData_right_edge_hv2_MUL_inv_right_edge_h = io_cell_data->dofs_right_edge.hv*io_cell_data->dofs_right_edge.hv*inv_right_edge_h;
		T elementData_left_edge_hv2_MUL_inv_left_edge_h = io_cell_data->dofs_left_edge.hv*io_cell_data->dofs_left_edge.hv*inv_left_edge_h;
// 6 ops
// sum: 247

		T sum_elementData_edges_hv2_MUL_inv_edges_h = elementData_left_edge_hv2_MUL_inv_left_edge_h + elementData_hyp_edge_hv2_MUL_inv_hyp_edge_h + elementData_right_edge_hv2_MUL_inv_right_edge_h;
// 2 ops
// sum: 249

		T elementData_hyp_edge_h2 = io_cell_data->dofs_hyp_edge.h*io_cell_data->dofs_hyp_edge.h;
		T elementData_right_edge_h2 = io_cell_data->dofs_right_edge.h*io_cell_data->dofs_right_edge.h;
		T elementData_left_edge_h2 = io_cell_data->dofs_left_edge.h*io_cell_data->dofs_left_edge.h;
// 3 ops
// sum: 252

		T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;
// 2 ops
// sum: 254


		T elementData_hyp_edge_hu_hv_MUL_inv_hyp_edge_h = io_cell_data->dofs_hyp_edge.hu*io_cell_data->dofs_hyp_edge.hv*inv_hyp_edge_h;
		T elementData_right_edge_hu_hv_MUL_inv_right_edge_h = io_cell_data->dofs_right_edge.hu*io_cell_data->dofs_right_edge.hv*inv_right_edge_h;
		T elementData_left_edge_hu_hv_MUL_inv_left_edge_h = io_cell_data->dofs_left_edge.hu*io_cell_data->dofs_left_edge.hv*inv_left_edge_h;
// 6 ops
// sum: 260

		T sum_elementData_edges_hu_hv_MUL_inv_edges_h = elementData_hyp_edge_hu_hv_MUL_inv_hyp_edge_h + elementData_right_edge_hu_hv_MUL_inv_right_edge_h + elementData_left_edge_hu_hv_MUL_inv_left_edge_h;
// 2 ops
// sum: 262

		T triangle_area_MUL_two_div_h_MUL_1_3 = (T)(1.0/3.0)*triangle_area*two_div_h;
// 2 ops
// sum: 264

		T gravity_potential = (T)0.5*gravitational_constant*sum_elementData_edges_h2;
// 2 ops
// sum: 266



		/*
		 * H
		 */
		T hyp_edge_h = six_div_h2*(
				h_div_3*(elementData_sum_edge_hu + elementData_sum_edge_hv)
				-hyp_length*i_hyp_edge_net_update->dofs_center.h
				);

		T right_edge_h = six_div_h2*(
				-h_div_3*elementData_sum_edge_hu
				-cat_length*i_right_edge_net_update->dofs_center.h
				);

		T left_edge_h = six_div_h2*(
				-h_div_3*elementData_sum_edge_hv
				-cat_length*i_left_edge_net_update->dofs_center.h
				);
// 13 ops
// sum: 279

		/*
		 * hu
		 */
		T hyp_edge_hu =
				six_div_h2*
				(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*
						(
							sum_elementData_edges_hu2_MUL_inv_edges_h +
							gravity_potential +
							sum_elementData_edges_hu_hv_MUL_inv_edges_h
						)
						- hyp_length*i_hyp_edge_net_update->dofs_center.hu
				);
// 6 ops
// sum: 285

		T right_edge_hu =
				six_div_h2*(
					(T)triangle_area_MUL_two_div_h_MUL_1_3*(
							- gravity_potential
							- sum_elementData_edges_hu2_MUL_inv_edges_h
						)
						- cat_length*i_right_edge_net_update->dofs_center.hu
				);
// 6 ops
// sum: 291

		T left_edge_hu =
				+ six_div_h2*(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*(
							-sum_elementData_edges_hu_hv_MUL_inv_edges_h
						)
						- cat_length*i_left_edge_net_update->dofs_center.hu
				);
// 5 ops
// sum: 296


		/*
		 * hv
		 */
		T hyp_edge_hv =
				six_div_h2*(
						triangle_area_MUL_two_div_h_MUL_1_3*(
							sum_elementData_edges_hu_hv_MUL_inv_edges_h +
							sum_elementData_edges_hv2_MUL_inv_edges_h +
							gravity_potential
						)
						- hyp_length*i_hyp_edge_net_update->dofs_center.hv
				);
// 6 ops
// sum: 302
		T right_edge_hv =
				six_div_h2*(
						- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_hu_hv_MUL_inv_edges_h
						- cat_length*i_right_edge_net_update->dofs_center.hv
				);
// 5 ops
// sum: 307

		T left_edge_hv =
				six_div_h2*(
						-triangle_area_MUL_two_div_h_MUL_1_3*(
								sum_elementData_edges_hv2_MUL_inv_edges_h +
								gravity_potential
						)
						- cat_length*i_left_edge_net_update->dofs_center.hv
				);

// 6 ops
// sum: 313
		if (t_storeElementUpdatesOnly)
		{
			io_cell_data->dofs_hyp_edge.h = hyp_edge_h;
			io_cell_data->dofs_hyp_edge.hu = hyp_edge_hu;
			io_cell_data->dofs_hyp_edge.hv = hyp_edge_hv;

			io_cell_data->dofs_right_edge.h = right_edge_h;
			io_cell_data->dofs_right_edge.hu = right_edge_hu;
			io_cell_data->dofs_right_edge.hv = right_edge_hv;

			io_cell_data->dofs_left_edge.h = left_edge_h;
			io_cell_data->dofs_left_edge.hu = left_edge_hu;
			io_cell_data->dofs_left_edge.hv = left_edge_hv;
		}
		else
		{
			io_cell_data->dofs_hyp_edge.h += timestep_size*hyp_edge_h;
			io_cell_data->dofs_hyp_edge.hu += timestep_size*hyp_edge_hu;
			io_cell_data->dofs_hyp_edge.hv += timestep_size*hyp_edge_hv;

			io_cell_data->dofs_right_edge.h += timestep_size*right_edge_h;
			io_cell_data->dofs_right_edge.hu += timestep_size*right_edge_hu;
			io_cell_data->dofs_right_edge.hv += timestep_size*right_edge_hv;

			io_cell_data->dofs_left_edge.h += timestep_size*left_edge_h;
			io_cell_data->dofs_left_edge.hu += timestep_size*left_edge_hu;
			io_cell_data->dofs_left_edge.hv += timestep_size*left_edge_hv;
		}
	}


	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			const CTsunamiSimulationEdgeData &i_edgeData_left,		///< edge data on left edge
			const CTsunamiSimulationEdgeData &i_edgeData_right,	///< edge data on right edge
			CTsunamiSimulationEdgeData &o_edgeFlux_left,		///< output for left flux
			CTsunamiSimulationEdgeData &o_edgeFlux_right		///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		CTsunamiSimulationEdgeData io_edgeData_right = i_edgeData_right;

		io_edgeData_right.dofs_center.hu = -i_edgeData_right.dofs_center.hu;
		io_edgeData_right.dofs_center.hv = -i_edgeData_right.dofs_center.hv;
		io_edgeData_right.dofs_center.h = i_edgeData_right.dofs_center.h;
		io_edgeData_right.dofs_center.b = i_edgeData_right.dofs_center.b;

		T o_max_wave_speed_left;
		T o_max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeData_left.dofs_center,
				io_edgeData_right.dofs_center,
				&o_edgeFlux_left.dofs_center,
				&o_edgeFlux_right.dofs_center,
				&o_max_wave_speed_left,
				&o_max_wave_speed_right,
				gravitational_constant
			);

		o_edgeFlux_right.dofs_center.hu = -o_edgeFlux_right.dofs_center.hu;
		o_edgeFlux_right.dofs_center.hv = -o_edgeFlux_right.dofs_center.hv;

		// store max wave speeds to same storage as bathymetry data
		o_edgeFlux_left.dofs_center.max_wave_speed = o_max_wave_speed_left;
		o_edgeFlux_right.dofs_center.max_wave_speed = o_max_wave_speed_right;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edgeFlux_left.validation = i_edgeData_left.validation;
		o_edgeFlux_right.validation = i_edgeData_right.validation;
#endif
	}

};

}
}



#endif
