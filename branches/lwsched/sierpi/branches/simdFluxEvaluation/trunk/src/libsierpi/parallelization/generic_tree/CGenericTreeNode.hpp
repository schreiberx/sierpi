/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode.hpp
 *
 *  Created on: Jun 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 */


#ifndef CGENERICTREENODE_HPP_
#define CGENERICTREENODE_HPP_

#include "config.h"
#include <assert.h>

/*
 * we have to include the header files here to stay outside the class scope
 */
#if COMPILE_WITH_TBB
	#include <tbb/tbb.h>
#endif

#if COMPILE_WITH_OMP
	#include <omp.h>
#endif



namespace sierpi
{

/**
 * \brief the generic tree node is used to allow tree traversals using a unified tree node system.
 *
 * in this way, the recursive tree traversals don't have to distinguish between domain triangulation nodes
 * and sub-trees of the root domain cluster triangles.
 */
template <typename CCluster_TreeNode>
class CGenericTreeNode
{

	typedef CGenericTreeNode<CCluster_TreeNode> CGenericTreeNode_;
public:
	/**
	 * pointer to parent node
	 */
	CGenericTreeNode *parent_node;

	/**
	 * first and second child node
	 */
	CGenericTreeNode *first_child_node, *second_child_node;

	/**
	 * set this node to be a new leaf node during the next traversal
	 */
	bool delayed_child_deletion;


public:
	/**
	 * pointer to tree node (avoids casting for cleaner programming, a static_case can also be used).
	 */
	CCluster_TreeNode *cCluster_TreeNode;

	/**
	 * this variable is set to true when this tree node is one of the nodes for the base triangulation.
	 */
	bool base_triangulation_node;


	/**
	 * approximated workload in this subtree
	 */
	long long workload_in_subtree;

#if CONFIG_ENABLE_SCAN_DATA
	/**
	 * scan start index (see MPI_scan)
	 */
	long long workload_scan_start_index;

	/**
	 * scan end index (see MPI_scan)
	 */
	long long workload_scan_end_index;


	/**
	 * thread id responsible for computation of first part of workload
	 */
	int workload_thread_id_start;

	/**
	 * thread id responsible for computation of last part of workload
	 */
	int workload_thread_id_end;

	/**
	 * thread id which should be uniquely associated with this cluster
	 */
	int workload_thread_id;

public:
	/**
	 * update scan data of workload
	 */
	inline void updateWorkloadScanTopDown()
	{
		/*
		 * test whether this is the root node
		 */
		if (parent_node == nullptr)
		{
			workload_scan_start_index = 0;
		}
		else
		{
			if (parent_node->first_child_node != nullptr)
			{
				if (parent_node->first_child_node == this)
				{
					// this is the first node -> copy start index
					workload_scan_start_index = parent_node->workload_scan_start_index;
				}
				else
				{
					workload_scan_start_index = parent_node->workload_scan_start_index + parent_node->first_child_node->workload_in_subtree;
				}
			}
			else
			{
				workload_scan_start_index = parent_node->workload_scan_start_index;
			}
		}

		workload_scan_end_index = workload_scan_start_index + workload_in_subtree;

		assert(workload_scan_start_index >= 0);
		assert(workload_scan_end_index >= 1);
		assert(workload_scan_start_index < workload_scan_end_index);
	}


	inline void updateWorkloadThreadAssignment(
			unsigned long long triangles_per_thread
	)
	{
		workload_thread_id_start = workload_scan_start_index / triangles_per_thread;
		workload_thread_id_end = (workload_scan_end_index-1) / triangles_per_thread;

		// use workload midpoint to associate to thread_id
		workload_thread_id = (workload_scan_start_index+(workload_scan_end_index-1)) / (triangles_per_thread*2);

		assert(workload_thread_id_start >= 0);
		assert(workload_thread_id_end >= 0);
	}

#endif

	/*
	 * constructor
	 */
	CGenericTreeNode(
			CGenericTreeNode *i_parent_node,	///< parent generic tree node
			bool i_base_triangulation_node
	)	:
		parent_node(i_parent_node),
		first_child_node(nullptr),
		second_child_node(nullptr),
		delayed_child_deletion(false),
		cCluster_TreeNode(nullptr),
		base_triangulation_node(i_base_triangulation_node),
		workload_in_subtree(0)
#if CONFIG_ENABLE_SCAN_DATA
	,
		workload_scan_start_index(-1),
		workload_scan_end_index(-1),
		workload_thread_id_start(-1),
		workload_thread_id_end(-1),
		workload_thread_id(-1)
#endif
	{
		specializedConstructorMethod();
	}


public:
	/**
	 * mark the children to be ready for deletion during the next traversal.
	 *
	 * this is necessary to have joined children still accessible by adjacent clusters until
	 * the updates of the edge comm information is finished.
	 */
	void setDelayedChildDeletion()
	{
		delayed_child_deletion = true;
	}

	void unsetDelayedChildDeletion()
	{
		delayed_child_deletion = false;
	}



	/**
	 * deconstructor which deletes the children which are only
	 */
	~CGenericTreeNode()
	{
		freeChildren();

		if (cCluster_TreeNode)
			delete cCluster_TreeNode;
	}



	/**
	 * free generic child tree nodes as well as specialized nodes if they exist
	 *
	 * the specialized nodes may not care about deleting their children!
	 */
	void freeChildren()
	{
		if (first_child_node != nullptr)
		{
			delete first_child_node;
			first_child_node = nullptr;
		}

		if (second_child_node != nullptr)
		{
			delete second_child_node;
			second_child_node = nullptr;
		}

		delayed_child_deletion = false;
	}


	/**
	 * setup dummy
	 */
	void setupDummyRoot()
	{
		first_child_node = nullptr;
		second_child_node = nullptr;
		parent_node = nullptr;
	}


	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool isLeaf()	const
	{
		if (delayed_child_deletion)
			return true;

#if CONFIG_ENABLE_MPI
		if (cCluster_TreeNode == nullptr)
			return false;

		if (cCluster_TreeNode->cCluster == nullptr)
			return false;
#endif

		return (first_child_node == nullptr) && (second_child_node == nullptr);
	}



	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool hasLeaves()	const
	{
		if (delayed_child_deletion)
			return false;

		return (first_child_node != nullptr) || (second_child_node != nullptr);
	}


#if COMPILE_WITH_TBB
	#include "CGenericTreeNode_tbb.hpp"
#endif

#if COMPILE_WITH_OMP
	#include "CGenericTreeNode_omp.hpp"
#endif

#if (!COMPILE_WITH_TBB) && (!COMPILE_WITH_OMP)
	#include "CGenericTreeNode_serial.hpp"
#endif

};

}

#endif
