/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
#ifndef CVALIDATE_TYPES_HPP__
#define CVALIDATE_TYPES_HPP__

#include "libmath/CMath.hpp"
#include "libmath/CVertex2d.hpp"

class CValEdgeData
{
public:
	float mx, my;	// triangle midpoint
	float nx, ny;	// edge outward normal

	float vx, vy;	// midpoint on edge for verification
};


class CValCellData
{
public:
	CVertex2d<float> hyp;
	CVertex2d<float> left;
	CVertex2d<float> right;
	bool refine;
};

/*
::std::ostream&
operator<<(::std::ostream& os, const CValEdgeData &d)
{
	std::cout << "triangle midpoint: " << d.mx << "  " << d.my << std::endl;
	std::cout << "edge normal: " << d.nx << "  " << d.ny << std::endl;
	return os;
}

::std::ostream&
operator<<(::std::ostream& os, const CValCellData &d)
{
	std::cout << "hyp: " << d.hyp << std::endl;
	std::cout << "left: " << d.left << std::endl;
	std::cout << "right: " << d.right << std::endl;
	std::cout << "refine: " << d.refine << std::endl;
	return os;
}
*/


#endif
