#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

import sys
import normals

class CStdCodeBlocks:

	# constructor
	def __init__(self):
		pass
	
	config = False
	triangleFactory = False


	# set config
	def setConfig(self, config):
		self.config = config


	# set triangle factory
	def setTriangleFactory(self, triangleFactory):
		self.triangleFactory = triangleFactory


	#########################################################################################
	# this is a helper function to handle all different kinds of parameters
	# for the recursive calls but also for the kernel hooks. 
	#########################################################################################
	def create_function_header(self):
		
		# current parameters for this recursively called function
		traversator_parameters = []
		
		# vertex information is created during traversal (e. g. for visualization) 
		if self.config.kernelParams_vertexCoords:
			traversator_parameters.append("TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2")
		
		if self.config.kernelParams_normals and self.config.internal_traversator_normal_as_int_parameter:
			traversator_parameters.append("int hyp_normal")
			
		# if the depth information has to be created, an 'int depth' is appended to the kernel parameters
		if self.config.internal_traversatorMethodDepth:
			traversator_parameters.append("int depth")
		
		print """
void """+self.triangleFactory.sig_parent+"("+", ".join(traversator_parameters)+""")
{"""
		# output function
		if self.config.adaptivity_code_debug_output:
			print "	std::cout << std::endl;"
			print "	std::cout << \""+self.triangleFactory.key+"["+self.triangleFactory.method_postfix+"]\" << std::endl;"
			print "	std::cout << \"  "+self.triangleFactory.hyp_stack+" stack: \" << *comm_"+self.triangleFactory.hyp_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  "+self.triangleFactory.edge_stack+" stack: \" << *comm_"+self.triangleFactory.edge_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  structure_stack_in: \" << *structure_stack_in << std::endl;"
			print "	std::cout << \"  structure_stack_out: \" << *structure_stack_out << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_in: \" << *adaptivity_state_flag_stack_in << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_out: \" << *adaptivity_state_flag_stack_out << std::endl;"
		
		
		print """
	if ("""+self.config.varname_structure_input_stream+""".getNextData())
	{"""
	
		# parameter list for recursive kernel calls
		traversator_parameters1 = []
		traversator_parameters2 = []
	
		if self.config.kernelParams_vertexCoords:
			print "		TVertexScalar mx = (vx0+vx1)*(TVertexScalar)0.5;"
			print "		TVertexScalar my = (vy0+vy1)*(TVertexScalar)0.5;"
			if self.triangleFactory.x_format:
				traversator_parameters1.append("vx2, vy2, vx0, vy0, mx, my")
				traversator_parameters2.append("vx1, vy1, vx2, vy2, mx, my")
			else:
				traversator_parameters1.append("vx1, vy1, vx2, vy2, mx, my")
				traversator_parameters2.append("vx2, vy2, vx0, vy0, mx, my")	

		if self.config.kernelParams_normals and self.config.internal_traversator_normal_as_int_parameter:
			traversator_parameters1.append(str(self.triangleFactory.child_first.hyp_normal))
			traversator_parameters2.append(str(self.triangleFactory.child_second.hyp_normal))

		# the depth information is stored to a depth variable and has to be modified
		if self.config.internal_traversatorMethodDepth:
			print "		depth++;"
			# append the depth to the recursive parameter calls
			traversator_parameters1.append("depth")
			traversator_parameters2.append("depth")
	
		# code for recursive calls
		print "		"+self.triangleFactory.sig_child_first+"("+', '.join(traversator_parameters1)+");"
		print "		"+self.triangleFactory.sig_child_second+"("+', '.join(traversator_parameters2)+");"
	
		# if a reversed structure stack should be created, we have to push the information to the output stack
		if self.config.createReversedStructureStack:
			print "		"+self.config.varname_structure_output_stream+"->push(1);"
	
		# if the kernel should be executed only for leaves, we're done at this point.
		# otherwise we decrease the depth
		print "		return;"
		print "	}"
	
		# if a reversed structure stack should be created, we have to push the information to the output stack
		if self.config.createReversedStructureStack:
			print "	"+self.config.varname_structure_output_stream+"->push(0);"


	def get_joined_kernel_parameters(self):
		# parameter for the function signature
		traversator_signature = []
		traversator_parameters = []
	
		if self.config.kernelParams_vertexCoords:
			traversator_signature.append("TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2")
			traversator_parameters.append("vx0, vy0, vx1, vy1, vx2, vy2")

		if self.config.internal_traversatorMethodDepth:
			traversator_signature.append("int depth")
			traversator_parameters.append("depth")

		joined_traversator_signature = ", ".join(traversator_signature)
		joined_traversator_parameters = ", ".join(traversator_parameters)
		
		return [joined_traversator_signature, joined_traversator_parameters]
		

	def create_function_header_adaptivity_last_traversal(
								self,
								kernel_parameters		# parameters for kernel execution
							):
	
		# traversator_parameters for recursive traversals
		traversator_parameters1 = []
		traversator_parameters2 = []
	
		# parameter for the function signature
		traversator_parameters = []
	
		if self.config.kernelParams_vertexCoords:
			traversator_parameters.append("TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2")

			if self.triangleFactory.x_format:
				traversator_parameters1.append("vx2, vy2, vx0, vy0, mx, my")
				traversator_parameters2.append("vx1, vy1, vx2, vy2, mx, my")
			else:
				traversator_parameters1.append("vx1, vy1, vx2, vy2, mx, my")
				traversator_parameters2.append("vx2, vy2, vx0, vy0, mx, my")
				
		if self.config.kernelParams_normals and self.config.internal_traversator_normal_as_int_parameter:
			traversator_parameters.append("int hyp_normal")
			traversator_parameters1.append(str(self.triangleFactory.child_first.hyp_normal))
			traversator_parameters2.append(str(self.triangleFactory.child_second.hyp_normal))

		if self.config.internal_traversatorMethodDepth:
			traversator_parameters1.append("depth+1")
			traversator_parameters2.append("depth+1")
			traversator_parameters.append("int depth")

		joined_rec_parameters1 = ", ".join(traversator_parameters1)
		joined_rec_parameters_no_following_comma1 = joined_rec_parameters1
		if joined_rec_parameters1 != '':
			joined_rec_parameters1 += ', '

		joined_rec_parameters2 = ", ".join(traversator_parameters2)
		joined_rec_parameters_no_following_comma2 = joined_rec_parameters2
		if joined_rec_parameters2 != '':
			joined_rec_parameters2 += ', '

		joined_parameters = ", ".join(traversator_parameters)
		
		print """
char """+self.triangleFactory.sig_parent+"""("""+joined_parameters+""")
{"""
	
		if self.config.adaptivity_code_debug_output:
			print "	std::cout << std::endl;"
			print "	std::cout << \""+self.triangleFactory.key+"\" << std::endl;"
			print "	std::cout << \"  "+self.triangleFactory.hyp_stack+" stack: \" << *comm_"+self.triangleFactory.hyp_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  "+self.triangleFactory.edge_stack+" stack: \" << *comm_"+self.triangleFactory.edge_stack+"_edge_stack << std::endl;"
			print "	std::cout << \"  structure_stack_in: \" << *structure_stack_in << std::endl;"
			print "	std::cout << \"  structure_stack_out: \" << *structure_stack_out << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_in: \" << *adaptivity_state_flag_stack_in << std::endl;"
			print "	std::cout << \"  adaptivity_state_flag_stack_out: \" << *adaptivity_state_flag_stack_out << std::endl;"
	
		print """
	if (structure_stack_in->pop())
	{
	"""
		if self.config.kernelParams_vertexCoords:
			print "		TVertexScalar mx = (vx0+vx1)*(TVertexScalar)0.5;"
			print "		TVertexScalar my = (vy0+vy1)*(TVertexScalar)0.5;"
		#
		# if both leaf triangle are in state 'coarsen' (-6), they return 0 to signal
		# the parent node to start the coarsening operation
		#
		if self.config.adaptivity_code_debug_output:
			print """
		// both functions have to return the same boolean value
		// (0 is returned, if all 2 leaf triangles agreed to the coarsening.
		// also the other 2 leaf nodes needed to agree to the coarsening)"""
	
		print """
		"""+self.triangleFactory.sig_child_first+"""("""+joined_rec_parameters_no_following_comma1+""");
		if ("""+self.triangleFactory.sig_child_second+"""("""+joined_rec_parameters_no_following_comma2+""") != 0)
		{
			// no coarsening to do
			"""+self.config.varname_structure_output_stream+"""->push(1);
			return 1;
		}

		// coarsening
		"""+self.config.varname_structure_output_stream+"""->push(0);"""
	
		self.output_updateMaxDepth(0)
	
		#
		# at this traversal point at the parent node of both children which have to be coarsened,
		# we have to create the triangle element-data based upon the 2 elements stored in the
		# incoming element data stack.
		#
		if self.config.kernelParams_cellData:
			print "		CCellData *element_data = element_data_stack_out->fakePush_returnPtr();"
	
			if self.triangleFactory.x_format:
				print "		CCellData *right_element = element_fifo_in.getPreviousDataPtr();"
				print "		CCellData *left_element = element_fifo_in.getPreviousPreviousDataPtr();"
			else:
				print "		CCellData *left_element = element_fifo_in.getPreviousDataPtr();"
				print "		CCellData *right_element = element_fifo_in.getPreviousPreviousDataPtr();"
				
			print "		cKernelClass.coarsen("+kernel_parameters+", left_element, right_element);"
	
		print """		return 1;
	}
	"""
		return joined_parameters
	
	
	def create_adaptive_refine_coarsen_common_method(
								self,
								x_format,
								joined_kernel_hook_parameters_element_action,		# parameters for kernel execution
								kernel_signature
							):

		print """

template <char edge_type_hyp, char edge_type_right, char edge_type_left>
char defaultAdaptiveRefinementMethod_""" + ("true" if x_format else "false") + """("""+kernel_signature+""")
{"""
		
		if self.config.kernelParams_cellData:
			print """	// get pointer to next element
	CCellData *new_left_left_element;
	CCellData *new_left_right_element;
	CCellData *new_right_left_element;
	CCellData *new_right_right_element;
	CCellData *element_data = element_fifo_in.getNextDataPtr();"""
	
		print """
	unsigned char state = adaptivity_state_flag_stack_in->pop();
	switch(state)
	{"""
		# CASE 0 - NOTHING TO DO
		print	"	case 0:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0);"
	
		if self.config.adaptiveKernelUnmodifiedElementActionCall:
			print ""
			print "		cKernelClass.op_cell("+joined_kernel_hook_parameters_element_action+");"
			print ""

		if self.config.kernelParams_cellData:
			print	"		element_data_stack_out->push_PtrValue(element_data);"
		
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 0, 0, 0)
		self.output_updateMaxDepth(0)
	
		print	"		return 1;"
		print	""
	
		# CASE 1 - ERROR
		print	"	case 1:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""
	
		# CASE 2 - COARSENING
		print	"	case 2:	// coarsening along edge_right and edge_left => nothing to do"
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 0, -1, -1)
		# max depth if fixed by parent call 
		print	"		return 0;"
		print	""
	
		# CASE 3 - ERROR
		print	"	case 3:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""
	
		# CASE 4 - HYP REFINEMENT
		print	"	case 4:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0, 0, 1);"
	
		if self.config.kernelParams_cellData:
			if x_format:
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
			else:
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_l_r("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 1, 0, 0)
		self.output_updateMaxDepth(1)
		
		print "		return 1;"
		print	""
	
		# CASE 5 - HYP+LEFT REFINEMENT
		print	"	case 5:"
		print	"		"+self.config.varname_structure_output_stream+"->push("+("0, 0, 0, 1, 1" if not x_format else "0, 0, 1, 0, 1")+");"
	
		if self.config.kernelParams_cellData:
			if x_format:
							# 00101f
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
			else:			# 00011b
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_ll_r("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_left_right_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 1, 0, 1)
		self.output_updateMaxDepth(2)
	
		print "			return 1;"
		print	""
	
	
		# CASE 6 - HYP+RIGHT REFINEMENT
		print	"	case 6:"
		print	"		"+self.config.varname_structure_output_stream+"->push("+("0, 0, 0, 1, 1" if x_format else "0, 0, 1, 0, 1")+");"
	
		if self.config.kernelParams_cellData:
			if x_format:
					# 00011b
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			else:	# 00101f
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_l_rr("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_right_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 1, 1, 0)
		self.output_updateMaxDepth(2)
		print "		return 1;"
		print	""
	
		# CASE 6 - HYP+RIGHT+LEFT REFINEMENT
		print	"	case 7:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0, 0, 1, 0, 0, 1, 1);"
	
		if self.config.kernelParams_cellData:
			if x_format:
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
			else:
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_ll_rr("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_left_right_element, new_right_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information_x_format(x_format, 1, 1, 1)
		self.output_updateMaxDepth(2)
	
		print	"		return 1;"
		print	""
	
		# CASE x - ERROR
		print	"	default:"
		print	"		assert(false);"
		print	"		return -1;"
		
		print	"	}"
		print	"}"


	def get_edge_type_char(self, long_edge_type):
		if long_edge_type == 'new':
			return '\'n\''
		elif long_edge_type == 'old':
			return '\'o\''
		elif long_edge_type == 'b':
			return '\'b\''
		else:
			sys.stderr.write("INVALID EDGE TYPE: " + long_edge_type + "\n")
			sys.exit(-1)
			
	
	def create_adaptive_refine_code_templated(
								self,
								joined_kernel_hook_parameters_element_action		# parameters for kernel execution
							):
		
		templates = [
					self.get_edge_type_char(self.triangleFactory.edge_hyp),
					self.get_edge_type_char(self.triangleFactory.edge_right),
					self.get_edge_type_char(self.triangleFactory.edge_left)
				]
		
		print """
	return defaultAdaptiveRefinementMethod_""" + ("true" if self.triangleFactory.x_format else "false") + "<" + ", ".join(templates)+">("+joined_kernel_hook_parameters_element_action+");"
		
		return


	def create_adaptive_refine_code(
								self,
								joined_kernel_hook_parameters_element_action		# parameters for kernel execution
							):

		if self.config.kernelParams_cellData:
			print """	// get pointer to next element
	CCellData *new_left_left_element;
	CCellData *new_left_right_element;
	CCellData *new_right_left_element;
	CCellData *new_right_right_element;
	CCellData *element_data = element_fifo_in.getNextDataPtr();"""
	
		print """
	unsigned char state = adaptivity_state_flag_stack_in->pop();
	switch(state)
	{"""
		# CASE 0 - NOTHING TO DO
		print	"	case 0:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0);"
	
		if self.config.adaptiveKernelUnmodifiedElementActionCall:
			print ""
			print "		cKernelClass.op_cell("+joined_kernel_hook_parameters_element_action+");"
			print ""

		if self.config.kernelParams_cellData:
			print	"		element_data_stack_out->push_PtrValue(element_data);"
	
		self.create_output_edge_comm_adaptive_information(0, 0, 0)
		self.output_updateMaxDepth(0)
	
		print	"		return 1;"
		print	""
	
		# CASE 1 - ERROR
		print	"	case 1:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""
	
		# CASE 2 - COARSENING
		print	"	case 2:	// coarsening along edge_right and edge_left => nothing to do"
		self.create_output_edge_comm_adaptive_information(0, -1, -1)
		# max depth if fixed by parent call 
		print	"		return 0;"
		print	""
	
		# CASE 3 - ERROR
		print	"	case 3:"
		print	"		assert(false);"
		print	"		return -1;"
		print	""
	
		# CASE 4 - HYP REFINEMENT
		print	"	case 4:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0, 0, 1);"
	
		if self.config.kernelParams_cellData:
			if self.triangleFactory.x_format:
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
			else:
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_l_r("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information(1, 0, 0)
		self.output_updateMaxDepth(1)
		
		print "		return 1;"
		print ""
	
		# CASE 5 - HYP+LEFT REFINEMENT
		print "	case 5:"
		print "		"+self.config.varname_structure_output_stream+"->push("+("0, 0, 0, 1, 1" if not self.triangleFactory.x_format else "0, 0, 1, 0, 1")+");"
	
		if self.config.kernelParams_cellData:
			if self.triangleFactory.x_format:
							# 00101f
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
			else:			# 00011b
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_ll_r("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_left_right_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information(1, 0, 1)
		self.output_updateMaxDepth(2)
	
		print "			return 1;"
		print ""
	
	
		# CASE 6 - HYP+RIGHT REFINEMENT
		print "	case 6:"
		print "		"+self.config.varname_structure_output_stream+"->push("+("0, 0, 0, 1, 1" if self.triangleFactory.x_format else "0, 0, 1, 0, 1")+");"
	
		if self.config.kernelParams_cellData:
			if self.triangleFactory.x_format:
					# 00011b
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			else:	# 00101f
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_l_rr("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_right_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information(1, 1, 0)
		self.output_updateMaxDepth(2)
		print "		return 1;"
		print	""
	
		# CASE 6 - HYP+RIGHT+LEFT REFINEMENT
		print	"	case 7:"
		print	"		"+self.config.varname_structure_output_stream+"->push(0, 0, 1, 0, 0, 1, 1);"
	
		if self.config.kernelParams_cellData:
			if self.triangleFactory.x_format:
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
			else:
				print "		new_right_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_right_right_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_left_element = element_data_stack_out->fakePush_returnPtr();"
				print "		new_left_right_element = element_data_stack_out->fakePush_returnPtr();"
	
			print "		cKernelClass.refine_ll_rr("+joined_kernel_hook_parameters_element_action+", new_left_left_element, new_left_right_element, new_right_left_element, new_right_right_element);"
	
		self.create_output_edge_comm_adaptive_information(1, 1, 1)
		self.output_updateMaxDepth(2)
	
		print	"		return 1;"
		print	""
	
		# CASE x - ERROR
		print	"	default:"
		print	"		assert(false);"
		print	"		return -1;"
		
		print	"	}"



	########################################################
	# CREATE PARAMETERS FOR ELEMENT OR EDGE ACTION CALLS
	########################################################
	def createParametersForActionCalls(
									self,
									kernel_hook_parameters		# parameters used to call op_cell methods
								):
		#
		# separate between element action parameters and edge comm parameters
		# since edge comm parameters forwarded to another cell are stored in
		# accessed with reversed normal
		#
	
		# vertex kernel_hook_parameters_element_action
		if self.config.kernelParams_vertexCoords:
			kernel_hook_parameters.append("vx0, vy0, vx1, vy1, vx2, vy2")


		# prepare normal kernel_hook_parameters_element_action for kernel hooks
		if self.config.kernelParams_normals:
			if self.config.internal_traversator_normal_as_int_parameter:
				# normals as int
				normalStr = normals.getArrayNormal(self.triangleFactory.hyp_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])
				
				normalStr = normals.getArrayNormal(self.triangleFactory.edge_right_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])
				
				normalStr = normals.getArrayNormal(self.triangleFactory.edge_left_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])
				
			else:
				# normals as string
				normalStr = normals.getNormal(self.triangleFactory.hyp_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])
				
				normalStr = normals.getNormal(self.triangleFactory.edge_right_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])
				
				normalStr = normals.getNormal(self.triangleFactory.edge_left_normal)
				kernel_hook_parameters.append(normalStr[0]+", "+normalStr[1])


		# depth parameter
		if self.config.kernelParams_depth:
			kernel_hook_parameters.append("depth")
	
		# element data
		if self.config.kernelParams_cellData:
			kernel_hook_parameters.append("element_data")
	
	
	
	###################################################
	# EDGE BASED COMMUNICATION
	###################################################
	def create_edge_comm_code(
								self,
								kernel_hook_parameters_element_action,		# parameters used to call op_cell methods
								kernel_hook_parameters_edge_comm
							):
	
		joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
		joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)
	
		#####################################################################
		#
		# from this starting point, we assume that the forward traversal is
		# applied at first, then the backward step
		#
		#####################################################################
		# STORE INCOMING EDGE COMMUNICATION DATA (FORWARD)
		#
		# we have to store the incoming edge communication data on a temporary
		# stack for the forward step
		#####################################################################
		if self.triangleFactory.direction == 'forward':
			if self.triangleFactory.even_odd == 'even':
				# store the element communication data of the left edge to the edge communication stack
				if self.triangleFactory.edge_left == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
				if self.triangleFactory.edge_right == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"

				# read the edge communication data and store it to the buffer
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
	
			elif self.triangleFactory.even_odd == 'odd':
				if self.triangleFactory.edge_right == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
				if self.triangleFactory.edge_left == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
	
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
	
			if self.triangleFactory.edge_hyp == 'old':
				print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->pop_returnPtr());"
			elif self.triangleFactory.edge_hyp == 'new':
				print "	cKernelClass.op_cell_to_edge_hyp("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->fakePush_returnPtr());"

		#####################################################################
		# using the backward traversal, previously stored edge communication is
		# read from the buffer stack as well as from the edge comm buffer
		# to start the computations for the element
		#####################################################################
		elif self.triangleFactory.direction == 'backward':
			#####################################################################
			# DEFAULT EDGE COMM TRAVERSATOR
			
			# edge parameters for computation kernel
			# [hyp, right, left] edges - e.g. ['b','n','n'] for the first K triangle
			computation_kernel_edge_parameters = ['', '', '']
			
			#####################################################################
			# A) IMPORTANT note! (EDGE COMM BUFFER)
			#
			# At this point, we have to copy the edge comm data to a buffer to avoid any race conditions.
			# This has to be done since the kernel element operations are executed on the element data and thus
			# only the modified element data values can be communicated to the upcoming triangles.
			# (see (B))
			#
			print "	CEdgeData hyp_edge_data;"
			print "	CEdgeData right_edge_data;"
			print "	CEdgeData left_edge_data;"

			if self.triangleFactory.edge_hyp == 'old':		# hyp data is stored in edge communication during recent traversal
				print "	hyp_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->pop_returnRef();"
				computation_kernel_edge_parameters[0] = '&hyp_edge_data';
			elif self.triangleFactory.edge_hyp == 'new':	# hyp data was stored to edge buffer during previous traversal
				print "	hyp_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
				computation_kernel_edge_parameters[0] = '&hyp_edge_data';

			if self.triangleFactory.even_odd == 'even':
				if self.triangleFactory.edge_right == 'old':
					print "	right_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
					computation_kernel_edge_parameters[1] = '&right_edge_data';
				if self.triangleFactory.edge_left == 'old':
					print "	left_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
					computation_kernel_edge_parameters[2] = '&left_edge_data';

				if self.triangleFactory.edge_right == 'new':
					print "	right_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
					computation_kernel_edge_parameters[1] = '&right_edge_data';
				if self.triangleFactory.edge_left == 'new':
					print "	left_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
					computation_kernel_edge_parameters[2] = '&left_edge_data';

			elif self.triangleFactory.even_odd == 'odd':
				if self.triangleFactory.edge_left == 'old':
					print "	left_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
					computation_kernel_edge_parameters[2] = '&left_edge_data';
				if self.triangleFactory.edge_right == 'old':
					print "	right_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
					computation_kernel_edge_parameters[1] = '&right_edge_data';

				if self.triangleFactory.edge_left == 'new':
					print "	left_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
					computation_kernel_edge_parameters[2] = '&left_edge_data';
				if self.triangleFactory.edge_right == 'new':
					print "	right_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
					computation_kernel_edge_parameters[1] = '&right_edge_data';


			#####################################################################
			# B) (EDGE COMM BUFFER)
			#
			# we have to push the information transport before kernel action to
			# avoid any race conditions.
			#
			if self.triangleFactory.edge_hyp == 'new':
				print "	cKernelClass.op_cell_to_edge_hyp("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->fakePush_returnPtr());"

			if self.triangleFactory.even_odd == 'even':
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
			elif self.triangleFactory.even_odd == 'odd':
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"


			#####################################################################
			# assemble kernel edge parameters and their EDGE/BORDER signatures
			#
			for i in range(0,3):
				if computation_kernel_edge_parameters[i] != '':
					kernel_hook_parameters_element_action.append(computation_kernel_edge_parameters[i]);
				else:
					if i == 0:
						print "	cKernelClass.op_boundary_cell_to_edge_hyp("+joined_kernel_hook_parameters_element_action+", &hyp_edge_data);"
						kernel_hook_parameters_element_action.append('&hyp_edge_data');
					elif i == 1:
						print "	cKernelClass.op_boundary_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", &right_edge_data);"
						kernel_hook_parameters_element_action.append('&right_edge_data');
					elif i == 2:
						print "	cKernelClass.op_boundary_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", &left_edge_data);"
						kernel_hook_parameters_element_action.append('&left_edge_data');

		

	###################################################
	# FLUX BASED COMMUNICATION
	###################################################
	def create_flux_comm_code(
								self,
								kernel_hook_parameters_element_action,		# parameters used to call op_cell methods
								kernel_hook_parameters_edge_comm
							):

		joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
		joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)
	
		#####################################################################
		#
		# from this starting point, we assume that the forward traversal is
		# applied at first, then the backward step
		#
		#####################################################################
		# STORE INCOMING EDGE COMMUNICATION DATA (FORWARD)
		#
		# we have to store the incoming edge communication data on a temporary
		# stack for the forward step
		#####################################################################
		if self.triangleFactory.direction == 'forward':
			if self.triangleFactory.even_odd == 'even':
				# store the element communication data of the left edge to the edge communication stack
				if self.triangleFactory.edge_left == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
				if self.triangleFactory.edge_right == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"

				# read the edge communication data and store it to the buffer
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
	
			elif self.triangleFactory.even_odd == 'odd':
				if self.triangleFactory.edge_right == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
				if self.triangleFactory.edge_left == 'old':
					print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnPtr());"
	
				if self.triangleFactory.edge_right == 'new':
					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
				if self.triangleFactory.edge_left == 'new':
					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->fakePush_returnPtr());"
	
			if self.triangleFactory.edge_hyp == 'old':
				print "	"+self.config.varname_edge_data_comm_buffer_stack+"->push_PtrValue("+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->pop_returnPtr());"
			elif self.triangleFactory.edge_hyp == 'new':
				print "	cKernelClass.op_cell_to_edge_hyp("+joined_kernel_hook_parameters_element_action+", "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.hyp_stack)+"->fakePush_returnPtr());"
	
	
		#####################################################################
		# using the backward traversal, previously stored edge communication is
		# read from the buffer stack as well as from the edge comm buffer
		# to start the computations for the element
		#####################################################################
		elif self.triangleFactory.direction == 'backward':

			#####################################################################
			# DEFAULT FLUX COMM TRAVERSATOR
			#####################################################################
			# A) IMPORTANT note! (EDGE COMM BUFFER)
			#
			# At this point, we have to copy the edge comm data to a buffer to avoid any race conditions.
			# This has to be done since the kernel element operations are executed on the element data and thus
			# only the modified element data values can be communicated to the upcoming triangles.
			# (see (B))
			#

			if self.triangleFactory.edge_hyp == 'old':
				# FLUXES
				# hyp flux data was stored to edge comm stack by previous method
				print "	CEdgeData flux_local_hyp_edge_data = edge_data_comm_"+self.triangleFactory.hyp_stack+"_edge_stack->pop_returnRef();"
			elif self.triangleFactory.edge_hyp == 'new':
				# EDGE COMM
				# hyp data was stored to edge buffer during previous traversal
				print "	CEdgeData adjacent_hyp_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"

			if self.triangleFactory.even_odd == 'even':
				# FLUXES
				if self.triangleFactory.edge_right == 'old':
					print "	CEdgeData flux_local_right_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
				if self.triangleFactory.edge_left == 'old':
					print "	CEdgeData flux_local_left_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"

				# EDGE COMM
				if self.triangleFactory.edge_right == 'new':
					print "	CEdgeData adjacent_right_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
				if self.triangleFactory.edge_left == 'new':
					print "	CEdgeData adjacent_left_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"

			elif self.triangleFactory.even_odd == 'odd':
				# FLUXES
				if self.triangleFactory.edge_left == 'old':
					print "	CEdgeData flux_local_left_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"
				if self.triangleFactory.edge_right == 'old':
					print "	CEdgeData flux_local_right_edge_data = "+(self.config.varname_edge_data_comm_stacks%self.triangleFactory.edge_stack)+"->pop_returnRef();"

				# EDGE COMM
				if self.triangleFactory.edge_left == 'new':
					print "	CEdgeData adjacent_left_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"
				if self.triangleFactory.edge_right == 'new':
					print "	CEdgeData adjacent_right_edge_data = "+self.config.varname_edge_data_comm_buffer_stack+"->pop_returnRef();"


			#####################################################################
			# BOUNDARIES
			#
			if self.triangleFactory.edge_hyp == 'b':
				print "	CEdgeData adjacent_hyp_edge_data;"
				print "	cKernelClass.op_boundary_cell_to_edge_hyp("+joined_kernel_hook_parameters_edge_comm+", &adjacent_hyp_edge_data);"
			if self.triangleFactory.edge_right == 'b':
				print "	CEdgeData adjacent_right_edge_data;"
				print "	cKernelClass.op_boundary_cell_to_edge_right("+joined_kernel_hook_parameters_edge_comm+", &adjacent_right_edge_data);"
			if self.triangleFactory.edge_left == 'b':
				print "	CEdgeData adjacent_left_edge_data;"
				print "	cKernelClass.op_boundary_cell_to_edge_left("+joined_kernel_hook_parameters_edge_comm+", &adjacent_left_edge_data);"


			#####################################################################
			# B) (EDGE COMM BUFFER)
			#
			# we have to push the information transport before kernel action to
			# avoid any race conditions.
			#
			if self.triangleFactory.edge_hyp == 'new' or self.triangleFactory.edge_hyp == 'b':
				# compute the flux for this edge data based on the streamed hyp_edge_data and the local hyp edge data
				print "	CEdgeData local_hyp_edge_data;"
				print "	CEdgeData flux_local_hyp_edge_data;"
				
				# for new edge, allocate storage on communication stack, otherwise a dummy value
				if self.triangleFactory.edge_hyp == 'new':
					print "	CEdgeData &flux_adjacent_hyp_edge_data = edge_data_comm_"+self.triangleFactory.hyp_stack+"_edge_stack->fakePush_returnRef();"
				else:
					print "	CEdgeData flux_adjacent_hyp_edge_data;"

				print "	cKernelClass.op_cell_to_edge_hyp("+joined_kernel_hook_parameters_element_action+", &local_hyp_edge_data);"
				print "	cKernelClass.op_edge_edge(local_hyp_edge_data, adjacent_hyp_edge_data, flux_local_hyp_edge_data, flux_adjacent_hyp_edge_data);"

			if self.triangleFactory.even_odd == 'even':
				if self.triangleFactory.edge_right == 'new' or self.triangleFactory.edge_right == 'b':
					print "	CEdgeData local_right_edge_data;"
					print "	CEdgeData flux_local_right_edge_data;"

					# for new edge, allocate storage on communication stack, otherwise a dummy value
					if self.triangleFactory.edge_right == 'new':
						print "	CEdgeData &flux_adjacent_right_edge_data = edge_data_comm_"+self.triangleFactory.edge_stack+"_edge_stack->fakePush_returnRef();"
					else:
						print "	CEdgeData flux_adjacent_right_edge_data;"

					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", &local_right_edge_data);"
					print "	cKernelClass.op_edge_edge(local_right_edge_data, adjacent_right_edge_data, flux_local_right_edge_data, flux_adjacent_right_edge_data);"

				if self.triangleFactory.edge_left == 'new' or self.triangleFactory.edge_left == 'b':
					print "	CEdgeData local_left_edge_data;"
					print "	CEdgeData flux_local_left_edge_data;"
					
					# for new edge, allocate storage on communication stack, otherwise a dummy value
					if self.triangleFactory.edge_left == 'new':
						print "	CEdgeData &flux_adjacent_left_edge_data = edge_data_comm_"+self.triangleFactory.edge_stack+"_edge_stack->fakePush_returnRef();"
					else:
						print "	CEdgeData flux_adjacent_left_edge_data;"

					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", &local_left_edge_data);"
					print "	cKernelClass.op_edge_edge(local_left_edge_data, adjacent_left_edge_data, flux_local_left_edge_data, flux_adjacent_left_edge_data);"

			elif self.triangleFactory.even_odd == 'odd':
				if self.triangleFactory.edge_left == 'new' or self.triangleFactory.edge_left == 'b':
					print "	CEdgeData local_left_edge_data;"
					print "	CEdgeData flux_local_left_edge_data;"
					
					# for new edge, allocate storage on communication stack, otherwise a dummy value
					if self.triangleFactory.edge_left == 'new':
						print "	CEdgeData &flux_adjacent_left_edge_data = edge_data_comm_"+self.triangleFactory.edge_stack+"_edge_stack->fakePush_returnRef();"
					else:
						print "	CEdgeData flux_adjacent_left_edge_data;"

					print "	cKernelClass.op_cell_to_edge_left("+joined_kernel_hook_parameters_element_action+", &local_left_edge_data);"
					print "	cKernelClass.op_edge_edge(local_left_edge_data, adjacent_left_edge_data, flux_local_left_edge_data, flux_adjacent_left_edge_data);"

				if self.triangleFactory.edge_right == 'new' or self.triangleFactory.edge_right == 'b':
					print "	CEdgeData local_right_edge_data;"
					print "	CEdgeData flux_local_right_edge_data;"
					
					# for new edge, allocate storage on communication stack, otherwise a dummy value
					if self.triangleFactory.edge_right == 'new':
						print "	CEdgeData &flux_adjacent_right_edge_data = edge_data_comm_"+self.triangleFactory.edge_stack+"_edge_stack->fakePush_returnRef();"
					else:
						print "	CEdgeData flux_adjacent_right_edge_data;"

					print "	cKernelClass.op_cell_to_edge_right("+joined_kernel_hook_parameters_element_action+", &local_right_edge_data);"
					print "	cKernelClass.op_edge_edge(local_right_edge_data, adjacent_right_edge_data, flux_local_right_edge_data, flux_adjacent_right_edge_data);"

			kernel_hook_parameters_element_action.append('&flux_local_hyp_edge_data');
			kernel_hook_parameters_element_action.append('&flux_local_right_edge_data');
			kernel_hook_parameters_element_action.append('&flux_local_left_edge_data');
	
	
	
	###################################################
	# VERTEX COMM CODE
	###################################################
	def createVertexCommCode(
								self,
								kernel_hook_parameters_element_action,		# parameters used to call op_cell methods
								kernel_hook_parameters_edge_comm
							):
	
		joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
		joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)
		
		#
		# three different vertex types have to be distinguished:
		#
		#  > FIRST_TOUCH: vertices for which data has to be initially created
		#  > LAST_TOUCH: vertices which are not further modified
		#  > UPDATE: vertices which are updated by the data of another triangle
		#
		# the vertices are named in the following way:
		#
		#		  top
		#		   __
		#		 _/  \_
		#	   _/	  \_
		# left /__________\ right
		#
	
	
		def isFirstTouchVertexForward(edge_type_1, edge_type_2):
			if edge_type_1 == 'new' and edge_type_2 == 'new':
				return True
			# also create first touch policy for boundaries
			if edge_type_1 == 'b' and edge_type_2 == 'b':
				return True
			if edge_type_1 == 'b' and edge_type_2 == 'new':
				return True
			if edge_type_1 == 'new' and edge_type_2 == 'b':
				return True
			return False
	
	
		def isFirstTouchVertexBackward(edge_type_1, edge_type_2):
			if edge_type_1 == 'new' and edge_type_2 == 'new':
				return True
#			if edge_type_1 == 'b' and edge_type_2 == 'new':
#				return True
#			if edge_type_1 == 'new' and edge_type_2 == 'b':
#				return True
			return False


		def isMiddleTouchVertex(edge_type_1, edge_type_2):
			if edge_type_1 == 'old' and (edge_type_2 == 'new' or edge_type_2 == 'b'):
				return True
			if (edge_type_1 == 'new' or edge_type_1 == 'b') and edge_type_2 == 'old':
				return True
			if edge_type_1 == 'b' and edge_type_2 == 'new':
				return True
			if edge_type_1 == 'new' and edge_type_2 == 'b':
				return True

			return False


		def isLastTouchVertexForward(edge_type_1, edge_type_2):
			if edge_type_1 == 'old' and edge_type_2 == 'old':
				return True
#			if edge_type_1 == 'b' and edge_type_2 == 'old':
#				return True
#			if edge_type_1 == 'old' and edge_type_2 == 'b':
#				return True
			return False


		def isLastTouchVertexBackward(edge_type_1, edge_type_2):
			if edge_type_1 == 'old' and edge_type_2 == 'old':
				return True
			if edge_type_1 == 'b' and edge_type_2 == 'old':
				return True
			if edge_type_1 == 'old' and edge_type_2 == 'b':
				return True
			if edge_type_1 == 'b' and edge_type_2 == 'b':
				return True
			return False
	
		if self.triangleFactory.direction == 'forward':
	
			def createVertexDataCode(edge_type1, edge_type2, LeftRightOrHyp, kernel_parameters, edge_stack):
					print "	// "+edge_type1+" "+edge_type2
	
					if isFirstTouchVertexForward(edge_type1, edge_type2):
						print "	// first touch"
						# first touch -> create new vertex data
						print "	cKernelClass.actionFirstTouchVertexData_"+LeftRightOrHyp+"("+joined_kernel_hook_parameters_element_action+", vertex_data_comm_"+edge_stack+"_edge_stack->fakePush_returnPtr());"
	
					elif isMiddleTouchVertex(edge_type1, edge_type2):
						# middle touch -> middle vertex data
						print "	// middle touch"
						print "	cKernelClass.actionMiddleTouchVertexData_"+LeftRightOrHyp+"("+joined_kernel_hook_parameters_element_action+", vertex_data_comm_"+edge_stack+"_edge_stack->getTopElementPtr());"
	
					elif isLastTouchVertexForward(edge_type1, edge_type2):
						# last touch -> update vertex data and move to buffer
						print "	// last touch"
						print "	cKernelClass.actionLastTouchVertexData_"+LeftRightOrHyp+"("+joined_kernel_hook_parameters_element_action+", vertex_data_comm_"+edge_stack+"_edge_stack->getTopElementPtr());"
						print "	vertex_data_comm_buffer->push_PtrValue(vertex_data_comm_"+edge_stack+"_edge_stack->pop_returnPtr());"
	
					else:
						sys.stderr.write("INVALID EDGE COMBINATION (forward): "+edge_type1+" "+edge_type2+"\n")
	
			if self.triangleFactory.even_odd == 'even':
				leftOrRightStack = {
									'K':	{'left':'right', 'right':'left', 'top':'left'},
									'H':	{'left':'left', 'right':'right', 'top':'left'},
									'V':	{'left':'right', 'right':'right', 'top':'left'}
									}
	
				createVertexDataCode(self.triangleFactory.edge_left, self.triangleFactory.edge_hyp, 'Left', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['left'])
				createVertexDataCode(self.triangleFactory.edge_left, self.triangleFactory.edge_right, 'Top', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['top'])
				createVertexDataCode(self.triangleFactory.edge_hyp, self.triangleFactory.edge_right, 'Right', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['right'])
	
			elif self.triangleFactory.even_odd == 'odd':
	
				leftOrRightStack = {
									'K':	{'left':'right', 'right':'left', 'top':'right'},
									'H':	{'left':'left', 'right':'right', 'top':'right'},
									'V':	{'left':'left', 'right':'left', 'top':'right'}
									}
	
				createVertexDataCode(self.triangleFactory.edge_hyp, self.triangleFactory.edge_right, 'Right', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['right'])
				createVertexDataCode(self.triangleFactory.edge_left, self.triangleFactory.edge_right, 'Top', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['top'])
				createVertexDataCode(self.triangleFactory.edge_left, self.triangleFactory.edge_hyp, 'Left', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['left'])
	
			# no element action has to be called during forward traversal
			return False
	
	
		#####################################################################
		# using the backward traversal, previously stored edge communication is
		# read from the buffer stack as well as from the edge comm buffer
		# to start the computations for the element
		#
		elif self.triangleFactory.direction == 'backward':
	
			def createVertexAction(edge_type1, edge_type2, LeftRightOrHyp, kernel_parameters, edge_stack):
				# if this is the first and last touch vertex, the data can be immediately used for 
	
				if isFirstTouchVertexBackward(edge_type1, edge_type2):
					# first touch -> load vertex data from buffer and store to edge comm buffer
					print "	// isFirstTouchVertexBackward"
					print "	TVertexData &vertexData"+LeftRightOrHyp+" = vertex_data_comm_buffer->pop_returnRef();"
					print "	vertex_data_comm_"+edge_stack+"_edge_stack->push(vertexData"+LeftRightOrHyp+");"
	
				elif isLastTouchVertexBackward(edge_type1, edge_type2):
					# last touch -> pop & drop vertex data
					print "	// isLastTouchVertexForward"
					print "	TVertexData vertexData"+LeftRightOrHyp+" = vertex_data_comm_"+edge_stack+"_edge_stack->pop_returnRef();"
	
				elif isMiddleTouchVertex(edge_type1, edge_type2):
					# update touch -> only load pointer to top most stack-element
					print "	// isMiddleTouchVertex"
					print "	TVertexData vertexData"+LeftRightOrHyp+" = vertex_data_comm_"+edge_stack+"_edge_stack->getTopElementRef();"
	
				else:
					sys.stderr.write("INVALID EDGE COMBINATION (backward): "+edge_type1+" "+edge_type2+"\n")
	
			if self.triangleFactory.even_odd == 'even':
				leftOrRightStack = {
									'K':	{'left':'left', 'right':'right', 'top':'right'},
									'H':	{'left':'right', 'right':'left', 'top':'right'},
									'V':	{'left':'left', 'right':'left', 'top':'right'}
									}
	
				createVertexAction(self.triangleFactory.edge_hyp, self.triangleFactory.edge_right, 'Right', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['right'])
				createVertexAction(self.triangleFactory.edge_left, self.triangleFactory.edge_right, 'Top', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['top'])
				createVertexAction(self.triangleFactory.edge_hyp, self.triangleFactory.edge_left, 'Left', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['left'])
	
			elif self.triangleFactory.even_odd == 'odd':			
				leftOrRightStack = {
									'K':	{'left':'left', 'right':'right', 'top':'left'},
									'H':	{'left':'right', 'right':'left', 'top':'left'},
									'V':	{'left':'right', 'right':'right', 'top':'left'}
									}
	
				createVertexAction(self.triangleFactory.edge_left, self.triangleFactory.edge_hyp, 'Left', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['left'])
				createVertexAction(self.triangleFactory.edge_left, self.triangleFactory.edge_right, 'Top', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['top'])
				createVertexAction(self.triangleFactory.edge_hyp, self.triangleFactory.edge_right, 'Right', joined_kernel_hook_parameters_element_action, leftOrRightStack[self.triangleFactory.type_parent]['right'])
			else:
				assert(False)
	
			kernel_hook_parameters_element_action.append('&vertexDataLeft')
			kernel_hook_parameters_element_action.append('&vertexDataRight')
			kernel_hook_parameters_element_action.append('&vertexDataTop')
	
		return True
	
	
	
	def createLoadCellData(
							self,
							force_creation = False
						):
		if force_creation:
			print "	CCellData *element_data = "+self.config.varname_element_data_input_stream+".getNextDataPtr();"
			return
	
		# element data
		if self.config.kernelOpCellCall and self.config.kernelParams_cellData:
			print "	CCellData *element_data = "+self.config.varname_element_data_input_stream+".getNextDataPtr();"

		elif self.config.edgeComm:
			if self.triangleFactory.edge_right == 'new' or self.triangleFactory.edge_left == 'new' or self.triangleFactory.edge_hyp == 'new':
				print "	CCellData *element_data = "+self.config.varname_element_data_input_stream+".getNextDataPtr();"
			else:
				print "	"+self.config.varname_element_data_input_stream+".getNextDataPtr();"

		elif self.config.vertexDataComm:
#			if self.triangleFactory.edge_right == 'b' and self.triangleFactory.edge_left == 'b' and self.triangleFactory.edge_hyp == 'b':
#				print "	"+self.config.varname_element_data_input_stream+".getNextDataPtr();"
#			else:
			print "	CCellData *element_data = "+self.config.varname_element_data_input_stream+".getNextDataPtr();"

		elif self.config.fluxComm:
			if self.triangleFactory.edge_right == 'new' or self.triangleFactory.edge_left == 'new' or self.triangleFactory.edge_hyp == 'new':
				print "	CCellData *element_data = "+self.config.varname_element_data_input_stream+".getNextDataPtr();"
			else:
				print "	"+self.config.varname_element_data_input_stream+".getNextDataPtr();"
	
	
	
	def create_adaptivity_incomingEdgeStateTransfer(self):
		#
		# read incoming flags from left and right stacks
		#
	
		# setup incoming edge states
		print "	unsigned char incomingEdgeStateTransfer = 0;"
	
		if self.triangleFactory.edge_hyp == 'old':
			print	"	unsigned char hyp_trigger = comm_"+self.triangleFactory.hyp_stack+"_edge_stack->pop();"
			print	"	incomingEdgeStateTransfer |= (hyp_trigger&1) << 2;"
	
		if self.triangleFactory.x_format:
			if self.triangleFactory.edge_left == 'old':
				print	"	unsigned char edge_left_trigger = comm_"+self.triangleFactory.edge_stack+"_edge_stack->pop();"
				print	"	incomingEdgeStateTransfer |= (edge_left_trigger&1) << 0;"
	
			if self.triangleFactory.edge_right == 'old':
				print	"	unsigned char edge_right_trigger = comm_"+self.triangleFactory.edge_stack+"_edge_stack->pop();"
				print	"	incomingEdgeStateTransfer |= (edge_right_trigger&1) << 1;"
		else:
			if self.triangleFactory.edge_right == 'old':
				print	"	unsigned char edge_right_trigger = comm_"+self.triangleFactory.edge_stack+"_edge_stack->pop();"
				print	"	incomingEdgeStateTransfer |= (edge_right_trigger&1) << 1;"
	
			if self.triangleFactory.edge_left == 'old':
				print	"	unsigned char edge_left_trigger = comm_"+self.triangleFactory.edge_stack+"_edge_stack->pop();"
				print	"	incomingEdgeStateTransfer |= (edge_left_trigger&1) << 0;"
	
	print   ""
	
	
	
	def create_adaptivity_old_state_var(self):
		# for the first traversal, this triangle is in state '000'
		print "	unsigned char old_state = 0;"
		print ""


	def create_adaptivity_checkStateChangeForRefinement(self, parameters):
		x = 0
	
		if self.config.adaptivity_depthLimiterMax:
			print   "	if (depth < depth_limiter_max)"
			print   "	{"
			x += 1
	
		# if a refinement is desired, switch to state 1 (running hyp refinement)	
		print	"\t"*x+"	if (cKernelClass.should_refine("+(', '.join(parameters))+"))"
		print	"\t"*x+"		old_state = 3;"
	
		if self.config.adaptivity_depthLimiterMax:
			print   "	}"
			x -= 1
			
		if self.config.adaptivity_callShouldRefineForAllLeaves and self.config.adaptivity_depthLimiterMax:
			print	"\t"*x+"	 else"
			print	"\t"*x+"		cKernelClass.should_refine("+(', '.join(parameters))+");"	
			
		print ""
	
		
	
	def create_adaptivity_checkStateChangeForCoarsening(self, parameters):
		# don't allow coarsening if both catheti edges are at the border
		if self.triangleFactory.edge_right != 'b' or self.triangleFactory.edge_left != 'b':
			x = 0
	
			# only check coarsening, if the possibly existing edge trigger values allow a coarsening operation
			if self.triangleFactory.edge_right == 'old' or self.triangleFactory.edge_left == 'old':
				tests = []
				if self.triangleFactory.edge_left == 'old':
					tests.append("(edge_left_trigger == 2)")
				if self.triangleFactory.edge_right == 'old':
					tests.append("(edge_right_trigger == 2)")
				print   "\t"*x+"	if ("+(" && ".join(tests))+")"
				print   "\t"*x+"	{"
				x += 1
	
			if self.config.adaptivity_depthLimiterMin:
				print	"\t"*x+"	if (depth > depth_limiter_min)"
				print	"\t"*x+"	{"
				x += 1
	
			# only allow coarsening if the current state is 0 (no refinement)
			print	"\t"*x+"	if (old_state == 0)"
			print	"\t"*x+"	{"
	
			# if a coarsening is desired, switch to state 2
			print	"\t"*x+"		if (cKernelClass.should_coarsen("+(', '.join(parameters))+"))"
			print	"\t"*x+"		{"
			print	"\t"*x+"			old_state = 2;"
			print	"\t"*x+"		}"
			print	"\t"*x+"	}"
	
			if self.config.adaptivity_depthLimiterMax:
				x -= 1
				print	"\t"*x+"	}"
	
	
			if self.triangleFactory.edge_right == 'old' or self.triangleFactory.edge_left == 'old':
				x -= 1
				print	"\t"*x+"	}"
	
			print ""


	def create_adaptivity_loadOldStateAndEdgeTransferState(self):
		print	"	unsigned char old_state = adaptivity_state_flag_stack_in->pop();"
		print	"	unsigned char oldEdgeStateTransfer = old_state >> 4;"
		print	"	old_state &= 7;"
		print	""
		
		
	def create_adaptivity_updateOutgoingEdgeStateInformation(self):
		print	"	outgoingEdgeStateInformation |= oldEdgeStateTransfer;"


	def create_adaptivity_coarseningAdjacentAgreement(
								self,
								check_adjacent_coarsening
							):
	
		if self.triangleFactory.edge_left == 'old' or self.triangleFactory.edge_right == 'old' or self.triangleFactory.edge_hyp == 'old':
			#
			# new_state is not 2 if there was a refinement request from some adjacent triangle.
			# this adjacent triangle must not be equal to another adjacent triangle requesting a
			# coarsening. If such a 'requesting coarsening triangle' exists, we have to do
			# another traversal.
			#
	
			print   "	if (new_state != 2)"
			print   "	{"
	
			tests = []
			if self.triangleFactory.edge_left == 'old':
				tests.append("(edge_left_trigger == 2)")
	
			if self.triangleFactory.edge_right == 'old':
				tests.append("(edge_right_trigger == 2)")
	
			if self.triangleFactory.edge_hyp == 'old':
			   tests.append("(hyp_trigger == 2)")
	
			print   "		if ("+(" || ".join(tests))+")"
			print   "			repeat_traversal = true;"
	
			print   "	}"
	
			# if this triangle likes to trigger a coarsening, we have to check whether all other
			# adjacent triangles which can transmit some information to this triangle during the
			# current traversal, agree to the coarsening 
			print   "	else"
			print   "	{"
	
			# check valid adjacent coarsening
			if check_adjacent_coarsening:
				if self.triangleFactory.edge_left == 'old' or self.triangleFactory.edge_right == 'old':
					tests = []
					tests2 = []
					if self.triangleFactory.edge_left == 'old':
						tests.append("(edge_left_trigger != 2)")
						tests2.append("(edge_left_trigger == 2)")
		
					if self.triangleFactory.edge_right == 'old':
						tests.append("(edge_right_trigger != 2)")
						tests2.append("(edge_right_trigger == 2)")
		
					print   "		if ("+(" || ".join(tests))+")"
					print   "		{"
					print   "			new_state = 0;"
					
					if self.triangleFactory.edge_left == 'old' and self.triangleFactory.edge_right == 'old':
						print   "			if ("+(" || ".join(tests2))+")"
	
					print   "				repeat_traversal = true;"
					print   "		}"
		
					if self.triangleFactory.edge_hyp == 'old':
						print   "		else"
	
			# if an adjacent triangle likes to coarsen with our hyp,
			# the information about not allowed coarsening has to be transmitted
			# within the next coarsening pass  
			if self.triangleFactory.edge_hyp == 'old':
				print   "		if (hyp_trigger == 2)"
				print   "			repeat_traversal = true;"
	
			print   "	}"
			print   ""
	
	
	
	def create_adaptivity_lookupNewStateForStateTransfer(self):
			# load new state
			print	"	unsigned char new_state = automatonTable[old_state][incomingEdgeStateTransfer];"
			print	"	unsigned char outgoingEdgeStateInformation = new_state >> 4;"
			print	"	new_state = new_state & 7;"
			print	""
	
	
	
	def createOutgoingEdgeStateTransfer(self):
		print	"	// spread refine/coarsening information"
	
		mask = 7		# 111
	
		if self.triangleFactory.edge_hyp == 'new':
			print	"	comm_"+self.triangleFactory.hyp_stack+"_edge_stack->push((outgoingEdgeStateInformation >> 2) & 1);"
	
		if self.triangleFactory.edge_hyp != 'old':
			mask &= 3	# 011
	
		# setup communication data for coarsening
		if self.triangleFactory.edge_left == 'new' or self.triangleFactory.edge_right == 'new':
			print	"	// spread coarsening information"
			print   "	if (new_state == 2)"
			print   "	{"
	
			if self.triangleFactory.edge_left == 'new':
				print	"		comm_"+self.triangleFactory.edge_stack+"_edge_stack->push(2);"
			if self.triangleFactory.edge_right == 'new':
				print	"		comm_"+self.triangleFactory.edge_stack+"_edge_stack->push(2);"
	
			print   "	}"
			print   "	else"
			print   "	{"
	
			if self.triangleFactory.edge_left == 'new':
				print	"		comm_"+self.triangleFactory.edge_stack+"_edge_stack->push(0);"
			if self.triangleFactory.edge_right == 'new':
				print	"		comm_"+self.triangleFactory.edge_stack+"_edge_stack->push(0);"
	
			print   "	}"
	
		if self.triangleFactory.edge_right != 'old':
			mask &= 5	 # 101
		if self.triangleFactory.edge_left != 'old':
			mask &= 6	 # 110
	
		print   "	outgoingEdgeStateInformation &= "+str(mask)+";"
		print   ""
		
		# did we spread all the refinement information?
		print   "	if (outgoingEdgeStateInformation != 0)"
		print   "		repeat_traversal = true;"
		print   ""
	
		# create new state combined with further requested refinement informations"
		print   "	new_state |= (outgoingEdgeStateInformation << 4);"
		print   "	adaptivity_state_flag_stack_out->push(new_state);"
	
	
	
	#
	# output code to update maxLeafTraversalDepth
	#
	def output_updateMaxDepth(
							self,
							displacement,
						):
		if self.config.adaptivity_updateMaxDepth:
			print """
		if (maxLeafTraversalDepth < (depth+"""+str(displacement)+"""))
			maxLeafTraversalDepth = depth+"""+str(displacement)+""";
"""
	

	#
	# output the refinement information on the edge stacks
	#
	# TODO: some pop/push operations can be avoideself.triangleFactory.
	# e.g. popping an adaptive information from an unrefined edge and pushing a 0
	# to the same stack can be avoided
	#
	def create_output_edge_comm_adaptive_information(
											self,
											value_edge_hyp,		# values to be pushed to the stack for the corresponding edge
											value_edge_right,
											value_edge_left
										):

		if self.config.adaptivity_last_traversal_edge_comm_adaptive_information:
			if self.triangleFactory.edge_hyp == 'old':
				print """		comm_"""+self.triangleFactory.hyp_stack+"""_edge_stack->pop();"""
			elif self.triangleFactory.edge_hyp == 'new':
				print """		comm_"""+self.triangleFactory.hyp_stack+"""_edge_stack->push("""+str(value_edge_hyp)+""");"""

			if self.triangleFactory.x_format:
				if self.triangleFactory.edge_left == 'new':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				if self.triangleFactory.edge_left == 'old':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->pop();"""
	
				if self.triangleFactory.edge_right == 'new':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				if self.triangleFactory.edge_right == 'old':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->pop();"""
			else:
				if self.triangleFactory.edge_right == 'new':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				if self.triangleFactory.edge_right == 'old':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->pop();"""
	
				if self.triangleFactory.edge_left == 'new':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				if self.triangleFactory.edge_left == 'old':
					print """		comm_"""+self.triangleFactory.edge_stack+"""_edge_stack->pop();"""




	#
	# output the refinement information on the edge stacks
	#
	# TODO: some pop/push operations can be avoideself.triangleFactory.
	# e.g. popping an adaptive information from an unrefined edge and pushing a 0
	# to the same stack can be avoided
	#
	def create_output_edge_comm_adaptive_information_x_format(
											self,
											x_format,
											value_edge_hyp,		# values to be pushed to the stack for the corresponding edge
											value_edge_right,
											value_edge_left
										):

		if not x_format:
			hyp_stack = 'left'
			edge_stack = 'right'
		else:
			hyp_stack = 'right'
			edge_stack = 'left'

		if self.config.adaptivity_last_traversal_edge_comm_adaptive_information:
			print """		if (edge_type_hyp == 'o')"""
			print """			comm_"""+hyp_stack+"""_edge_stack->pop();"""
			print """		if (edge_type_hyp == 'n')"""
			print """			comm_"""+hyp_stack+"""_edge_stack->push("""+str(value_edge_hyp)+""");"""


			if not x_format:
				print """		if (edge_type_left == 'n')"""
				print """			comm_"""+edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				print """		if (edge_type_left == 'o')"""
				print """			comm_"""+edge_stack+"""_edge_stack->pop();"""
	
				print """		if (edge_type_right == 'n')"""
				print """			comm_"""+edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				print """		if (edge_type_right == 'o')"""
				print """			comm_"""+edge_stack+"""_edge_stack->pop();"""
			else:
				print """		if (edge_type_right == 'n')"""
				print """			comm_"""+edge_stack+"""_edge_stack->push("""+str(value_edge_right)+""");"""
				print """		if (edge_type_right == 'o')"""
				print """			comm_"""+edge_stack+"""_edge_stack->pop();"""
	
				print """		if (edge_type_left == 'n')"""
				print """			comm_"""+edge_stack+"""_edge_stack->push("""+str(value_edge_left)+""");"""
				print """		if (edge_type_left == 'o')"""
				print """			comm_"""+edge_stack+"""_edge_stack->pop();"""



	###################################
	# output recursion table
	###################################
	def outputRecursionTable(
							self,
							sfcMethodExists_Lambda,
							recursionTable
						):
		if self.config.kernelParams_depth:
			self.config.internal_traversatorMethodDepth = True
	
		rec_current_parameters = []
		
		# vertex information is created during traversal (e. g. for visualization) 
		if self.config.kernelParams_vertexCoords:
			rec_current_parameters.append("TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar")

		# normals
		if self.config.kernelParams_normals and self.config.internal_traversator_normal_as_int_parameter:
			rec_current_parameters.append("int")

		# if the depth information has to be created for traversators
		if self.config.internal_traversatorMethodDepth:
			rec_current_parameters.append("int")

		parameter_types = ", ".join(rec_current_parameters)
		
		recursions = len(recursionTable)
		for r in range(0, len(recursionTable)):
			recursionTable[r].idx = r

		print
		print "static const int number_of_recursion_functions = "+str(recursions)+";"
		print "typedef "+('char' if self.config.adaptivity_char_return_variable else 'void')+" (TThisClass::*TRecursiveMethod)("+parameter_types+");"
	
		#
		# NEW: OUTPUT RECURSION ARRAY
		#
	
		#
		# get pointer to recursion method
		#
	
		# determine array dimensions
		arrayDims = []
	
		# type
		if not self.config.assumeKVHToBeEqual:
			arrayDims.append(['K', 'H', 'V'])
	
		# even odd
		arrayDims.append(['e', 'o'])

		if self.config.internal_edgeTypes:
			edgeTypes = []
			for a in ['n', 'o', 'b']:
				for b in ['n', 'o', 'b']:
						for c in ['n', 'o', 'b']:
							edgeTypes.append(a+b+c)
	
			# new, old, border
			# hyp, right, left
			arrayDims.append(edgeTypes)
	
		# forward/backward
		arrayDims.append(['forward', 'backward'])
	
		# kernelParams_normals?
		if self.config.kernelParams_normals and not self.config.internal_traversator_normal_as_int_parameter:
			arrayDims.append(['normal_'+i for i in ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw']])
	
	
		def printMethodArray(remainingArrayDims, sig):
			if len(remainingArrayDims) == 0:
				if sfcMethodExists_Lambda(sig):
					print "		&"+self.config.recursionTableClass+"::"+sig,
				else:
					print "		nullptr",
				return
			
			print "	{"
	
			for r in range(0, len(remainingArrayDims[0])):
				i = remainingArrayDims[0][r]
				nsig = sig+i
				if len(remainingArrayDims) > 1:
					nsig += '_'
				printMethodArray(remainingArrayDims[1:], nsig)
				if r < len(remainingArrayDims[0])-1:
					print ","
				else:
					print ""
			print "	}",
	
	
		print "enum ESFCMethod{"
		print "	SFC_METHOD_DEFAULT = 0,"
		print "	SFC_METHOD_FORCE_FORWARD = (1 << 0),"
		print "	SFC_METHOD_FORCE_BACKWARD = (1 << 1),"
		print "	SFC_METHOD_NEW_TO_OLD_EDGE_TYPES = (1 << 2),"
		print "	SFC_METHOD_OLD_TO_NEW_EDGE_TYPES = (1 << 3),"
		print "	SFC_METHOD_LIMITER___ = (1 << 4)"
		print "};"
		print ""
		print "template <int specialMode = 0>"
		print "TRecursiveMethod getSFCMethod(CTriangle_Factory &i_cTriangleFactory)"
		print "{"
		print "	static TRecursiveMethod methodArray",
		for dim in arrayDims:
			print "["+str(len(dim))+"]",
		print "="
	
		if self.config.assumeKVHToBeEqual:
			printMethodArray(arrayDims, 'V_')
		else:
			printMethodArray(arrayDims, '')
	
		print "	;"
		
		lookup_indices = ""
		
		print ""
		
		# type
		if not self.config.assumeKVHToBeEqual:
			lookup_indices += "[i_cTriangleFactory.triangleType]"
		print "	assert(i_cTriangleFactory.triangleType != CTriangle_Enums::TRIANGLE_TYPE_INVALID);"
	
		# even odd
		lookup_indices += "[i_cTriangleFactory.evenOdd]"
		print "	assert(i_cTriangleFactory.evenOdd != CTriangle_Enums::EVEN_ODD_INVALID);"
		
		print ""
	
		# edge communication types
		if self.config.internal_edgeTypes:
			print "		assert(i_cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID);"
			print "		assert(i_cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID);"
			print "		assert(i_cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID);"
	
			print "		CTriangle_Enums::EEdgeType hyp, left, right;"
			print "		if (specialMode == 0)"
			print "		{"
			print "			hyp = i_cTriangleFactory.edgeTypes.hyp;"
			print "			right = i_cTriangleFactory.edgeTypes.right;"
			print "			left = i_cTriangleFactory.edgeTypes.left;"
			print "		}"
			print "		else if (specialMode & SFC_METHOD_NEW_TO_OLD_EDGE_TYPES)"
			print "		{"
			print "			hyp = (i_cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.hyp);"
			print "			right = (i_cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.right);"
			print "			left = (i_cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.left);"
			print "		}"
			print "		else if (specialMode & SFC_METHOD_OLD_TO_NEW_EDGE_TYPES)"
			print "		{"
			print "			hyp = (i_cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.hyp);"
			print "			right = (i_cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.right);"
			print "			left = (i_cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.left);"
			print "		}"
			print "	#if DEBUG"
			print "		else"
			print "		{"
			print "			assert(false);"
			print "		}"
			print "	#endif"
			lookup_indices += "[hyp*9+right*3+left]"
	
		# forward/backward
		print "	CTriangle_Enums::EDirection direction;"
		print "	if (specialMode & SFC_METHOD_FORCE_FORWARD)"
		print "	{"
		print "		direction = CTriangle_Enums::DIRECTION_FORWARD;"
		print "	}"
		print "	else if (specialMode & SFC_METHOD_FORCE_BACKWARD)"
		print "	{"
		print "		direction = CTriangle_Enums::DIRECTION_BACKWARD;"
		print "	}"
		print "	else"
		print "	{"
		print "		direction = i_cTriangleFactory.traversalDirection;"
		print "	}"
		print ""
	
		lookup_indices += "[direction]"
		print "		assert(i_cTriangleFactory.traversalDirection != CTriangle_Enums::DIRECTION_INVALID);"
	
		# kernelParams_normals?
		if self.config.kernelParams_normals and not self.config.internal_traversator_normal_as_int_parameter:
			lookup_indices += "[i_cTriangleFactory.hypNormal]"
			
		print "	assert(i_cTriangleFactory.hypNormal != CTriangle_Enums::NORMAL_INVALID);"
	
		print "#if DEBUG"
		print "	assert(methodArray"+lookup_indices+" != nullptr);"
		print "#endif"
		
		print "	return methodArray"+lookup_indices+";"
		print "}"
	
		
