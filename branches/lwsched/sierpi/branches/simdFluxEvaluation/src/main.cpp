/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include <stdlib.h>
#include <stdint.h>
#include <sstream>
#include "libxml/parser.h"

#if CONFIG_SIERPI_ENABLE_GUI
	#include "CMainGui.hpp"
#else
	#include "CMain.hpp"
#endif


/**
 * main() function
 */
int main(
		int argc,
		char *argv[]
)
{
#if CONFIG_SIERPI_ENABLE_GUI
	CMainGui cMain(argc, argv);
#else
	CMain cMain(argc, argv);
#endif

	// create the simulation
	cMain.simulation_create();

	// setup MPI
	cMain.mpi_setup();

	if (!cMain.setupProgramParameters())
		return -1;

	// setup threading tools
	cMain.threading_setup();

	// setup the simulation
	cMain.simulation_setup();

	// output some useful information
	cMain.outputVerboseInformation();

#if CONFIG_SIERPI_ENABLE_GUI

		// setup gui stuff AFTER setting up the simulation
		cMain.setupGui();

		// run the simulation loop
		cMain.run();

#else

		// simulation loop prefix
		cMain.simulation_loopPrefix();

		// run the simulation
		cMain.threading_simulationLoop();

		// simulation loop suffix
		cMain.simulation_loopSuffix();
#endif

	// shutdown the simulation
	cMain.simulation_shutdown();

	// shutdown threading tools
	cMain.threading_shutdown();

	// shutdown MPI
	cMain.mpi_shutdown();

	// XML stuff
	xmlCleanupParser();

	return 0;
}
