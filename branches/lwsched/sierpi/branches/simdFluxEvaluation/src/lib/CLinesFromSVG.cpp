/*
 * CLinesFromSVG.cpp
 *
 *  Created on: Dec 7, 2011
 *      Author: schreibm
 */

#include "CLinesFromSVG.hpp"
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <libxml/parser.h>
#include <cassert>
#include "libmath/CMath.hpp"



/**
 * setup a new triangle given by a string in SVG path format
 *
 * only 3 vertices are allowed to be given
 *
 * triangle string is given in form:
 * "m 0,1040.3622 0.70710678,0.7071 -1.41421358,0 z"
 */
bool CLinesFromSVG::newLineFromString(
		std::string &inputString
)
{
	size_t strpos = 0;

	float penx = transform_x;
	float peny = transform_y;

	float startpospenx = 0;
	float startpospeny = 0;

	while(true)
	{
		if (inputString.length() <= strpos)
			return true;

		char c = inputString.substr(strpos, 1)[0];
		switch(c)
		{
		case 'c':
			lineList.push_back(std::list<std::vector<float> >());
			strpos += 2;
			break;

		case 'l':
			lineList.push_back(std::list<std::vector<float> >());
			strpos += 2;
			break;

		case 'm':
			lineList.push_back(std::list<std::vector<float> >());
			strpos += 2;
			break;

		case 'z':
			std::vector<float> tmpFloat(2, 0.0);
			tmpFloat[0] = startpospenx;
			tmpFloat[1] = startpospeny;

			lineList.back().push_back(tmpFloat);
			strpos += 2;
			continue;
		}


		size_t strEndPos = inputString.find(" ", strpos+1);

		if (strEndPos == std::string::npos)
		{
			strEndPos = inputString.length();
			break;
		}

		std::string tmpString = inputString.substr(strpos, strEndPos - strpos);

		if (tmpString == "")
		{
			// end of string
			return true;
		}

		size_t commaPos = tmpString.find(',');
		if (commaPos == std::string::npos)
		{
			std::cerr << "invalid comma position at " << strpos << std::endl;
			std::cerr << inputString << std::endl;
			return false;
		}

		penx += atof(tmpString.substr(0, commaPos).c_str());
		peny -= atof(tmpString.substr(commaPos+1).c_str());

		std::vector<float> tmpFloat(2, 0.0);
		tmpFloat[0] = penx;
		tmpFloat[1] = peny;

		lineList.back().push_back(tmpFloat);

		if (c == 'm')
		{
			startpospenx = tmpFloat[0];
			startpospeny = tmpFloat[1];
		}

		strpos = strEndPos+1;
	}

	return true;
}

bool CLinesFromSVG::searchPath(xmlNode * a_node)
{
    static const std::string string_path("path");
    static const std::string string_d("d");


    for (xmlNode *cur_node = a_node; cur_node; cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
        	// if we are inside the drawable area, search for path
        	if (string_path == std::string((char*)cur_node->name))
        	{
        		// search for translation attribute
        	    for (xmlAttr *cur_attr = cur_node->properties; cur_attr; cur_attr = cur_attr->next)
        	    {
            		if (cur_attr->type == XML_ATTRIBUTE_NODE)
            		{
						if (string_d == std::string((char*)cur_attr->name))
						{
							std::string lineString((char*)cur_attr->children->content);
							if (!newLineFromString(lineString))
								return false;
						}
            		}
        	    }
        	}
        	else
        	{
        		if (cur_node->children != 0)
        			searchPath(cur_node->children);
        	}
        }
    }
    return true;
}


bool CLinesFromSVG::setupTransform(std::string &transformString)
{
	static std::string string_translate("translate(");
	static size_t len = string_translate.length();

	if (transformString.substr(0, len) != string_translate)
	{
		std::cerr << "invalid translation string at beginning" << std::endl;
		return false;
	}

	size_t commaPos = transformString.find(',', len+1);
	if (commaPos == std::string::npos)
	{
		std::cerr << "comma position not found in translation information" << std::endl;
		return false;
	}

	size_t endPos = transformString.find(')', commaPos+1);
	if (endPos == std::string::npos)
	{
		std::cerr << "end position not found in translation information" << std::endl;
		return false;
	}

	transform_x = atof(transformString.substr(len+1, commaPos).c_str());
	transform_y = -atof(transformString.substr(commaPos+1, endPos).c_str());

//	std::cout << "t: " << transform_x << " " << transform_y << std::endl;
	return true;
}

bool CLinesFromSVG::searchDrawingArea(xmlNode * a_node)
{
	static const std::string string_g("g");
	static const std::string string_transform("transform");

    for (xmlNode *cur_node = a_node; cur_node; cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
        	// if we are inside the drawable area, search for path
        	if (string_g == std::string((char*)cur_node->name))
        	{
        		// search for translation attribute
        	    for (xmlAttr *cur_attr = cur_node->properties; cur_attr; cur_attr = cur_attr->next)
        	    {
            		if (cur_attr->type == XML_ATTRIBUTE_NODE)
            		{
						if (string_transform == std::string((char*)cur_attr->name))
						{
							std::string transformString((char*)cur_attr->children->content);
							if (!setupTransform(transformString))
								return false;
						}
            		}
        	    }

        	    if (!searchPath(cur_node->children))
        	    	return false;
        	}
        	else
        	{
        		if (!searchDrawingArea(cur_node->children))
        			return false;
        	}
        }
    }
    return true;
}

bool CLinesFromSVG::loadSVGFile(const char *filename)
{
    xmlDoc *doc;
    xmlNode *root_element;

    doc = xmlReadFile(filename, NULL, 0);

    if (doc == NULL)
    {
    	std::cerr << "failed to parse file " << filename << std::endl;
        xmlCleanupParser();
    	return false;
    }

	transform_x = 0;
	transform_y = 0;

	lineList.empty();

    root_element = xmlDocGetRootElement(doc);
    bool valid = searchDrawingArea(root_element);
    xmlFreeDoc(doc);

    xmlCleanupParser();

#if 0
    std::cout << triangles.size() << " triangle(s) found" << std::endl;

    for (std::vector<CTriangle2D>::iterator iter = triangles.begin(); iter != triangles.end(); iter++)
   	{
    	for (int i = 0; i < 3; i++)
    		std::cout << iter->vertices[i][0] << " " << iter->vertices[i][1] << std::endl;
    	std::cout << std::endl;
   	}

#endif

    return valid;
}

