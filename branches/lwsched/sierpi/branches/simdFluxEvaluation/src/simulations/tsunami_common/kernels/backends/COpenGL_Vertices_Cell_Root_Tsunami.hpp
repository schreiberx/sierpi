/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_HPP_
#define COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlViewport.hpp"


#define COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_TRIANGLE_COUNT		(128)
#define COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_VERTEX_COUNT		(COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_TRIANGLE_COUNT*3)

#if COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT % 3 != 0
	#error "COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT has to be a multiple of 3"
#endif

class COpenGL_Vertices_Cell_Root_Tsunami
{
	typedef CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar	T;

public:
	CGlVertexArrayObject vao;
	CGlBuffer vertex_buffer;
	CGlBuffer normal_buffer;

public:
	CError error;

	COpenGL_Vertices_Cell_Root_Tsunami()
	{
		GLint glfloat = (sizeof(T) == 4 ? GL_FLOAT : GL_DOUBLE);

		vao.bind();
			vertex_buffer.bind();
			vertex_buffer.resize(COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_VERTEX_COUNT*sizeof(T)*3);

				// vertex coordinates
				glVertexAttribPointer(0, 3, glfloat, GL_FALSE, 3*sizeof(T), 0);
				glEnableVertexAttribArray(0);

			normal_buffer.bind();
			normal_buffer.resize(COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_VERTEX_COUNT*sizeof(T)*3);

				// normals
				glVertexAttribPointer(1, 3, glfloat, GL_FALSE, 3*sizeof(T), 0);
				glEnableVertexAttribArray(1);

		vao.unbind();
	}




	/**
	 * initialize rendering
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRendering()
	{
		/**
		 * !!! We have to do the setup right here since other threads are not allowed to access the context !!!
		 */
		vao.bind();

		CGlErrorCheck();
	}


	inline void render(
			T *i_vertex_buffer_data,		///< pointer to vertices
			T *i_normal_buffer_data,		///< pointer to normals
			size_t i_vertices_count			///< number of vertices
	)
	{
		for (size_t i = 0; i < i_vertices_count; i += COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_VERTEX_COUNT)
		{
			size_t block_size = CMath::min<size_t>(COPENGL_VERTICES_CELLDATA_ROOT_TSUNAMI_VERTEX_COUNT, i_vertices_count - i);

			vertex_buffer.bind();
			vertex_buffer.subData(0, block_size*(3*sizeof(T)), i_vertex_buffer_data);

			normal_buffer.bind();
			normal_buffer.subData(0, block_size*(3*sizeof(T)), i_normal_buffer_data);

			glDrawArrays(GL_TRIANGLES, 0, block_size);

			i_vertex_buffer_data += 3*block_size;
			i_normal_buffer_data += 3*block_size;
		}
	}



	inline void shutdownRendering()
	{
		vao.unbind();
		CGlErrorCheck();
	}

};


#endif
