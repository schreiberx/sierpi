#! /bin/bash

cd ../..
make clean

scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-asagi=on --tsunami-flux-solver=5 --fp-default-precision=double -j8 --tsunami-runge-kutta-order=1 --tsunami-adaptivity-mode=2
