###
### Sierpi
### Display dart data
### Contact: breuera@in.tum.de, schreibm@in.tum.de
###


## datasets to analyze
benchmark_output <- "tohoku_2012_06_06_dart_21413_ver2"
benchmark_measured_data <- "tohoku_dart_data"
benchmark_measured_data_single <- c(21413, "21413_5day-03142011_notide.txt")
benchmark_output_group_regexp <- "output_dart_d([0-9]*)_a([0-9]*)_cfl(.*)\\.txt"

limit_d <- -1
limit_d_even = TRUE
limit_a <- -1
limit_cfl <- 0.5

## path to benchmark directory
benchmark_dir <- "workspace/sierpi/benchmarks/field_benchmarks"

benchmark_output_dir <- paste(benchmark_dir, benchmark_output, sep="/")
benchmark_measured_data_dir <- paste(benchmark_dir, benchmark_measured_data, sep="/")

benchmark_measured_data_single_file <- paste(paste(benchmark_dir, benchmark_measured_data, sep="/"), benchmark_measured_data_single[2], sep="/")


# BENCHMARK OUTPUT FILES
benchmark_output_files <- list.files(path=benchmark_output_dir, pattern=".*txt")


# READ DART DATASET
benchmark_measured_data_single_file_dataset <- read.table(benchmark_measured_data_single_file)

tmp_benchmark_output_files <- c()

# use limitations to draw only a subset of images
for (benchmark_output_filename in benchmark_output_files)
{
  tmp <- unlist(strsplit(sub(benchmark_output_group_regexp, "\\1 \\2 \\3", benchmark_output_filename), split=" "))
  nd <- as.integer(tmp[1])
  na <- as.integer(tmp[2])
  ncfl <- tmp[3]
  
  if (limit_d != -1 && limit_d != nd)
    next

  if (limit_d_even)
    if ((nd %% 2) == 1)
      next

  if (limit_a != -1 && limit_a != na)
    next
  
  if (limit_cfl != -1 && limit_cfl != ncfl)
    next

  tmp_benchmark_output_files <- c(tmp_benchmark_output_files, benchmark_output_filename)
}

benchmark_output_files <- tmp_benchmark_output_files


rainbow_map <- c("black", rainbow(length(benchmark_output_files)))


# START a new PLOT
#plot_output_file <- "output"
#plot_output_file <- paste(paste(plot_output_file, "_d", sep=""), d, sep="")
#plot_output_file <- paste(paste(plot_output_file, "_a", sep=""), a, sep="")

plot(benchmark_measured_data_single_file_dataset, type="l", xlab="", ylab="", xlim=c(-1000,25000), ylim=c(-0.5, 1), col=rainbow_map[1])
title(main=paste("Dart station", benchmark_measured_data_single[1], sep=""), xlab="time since earthquake (seconds)", ylab="height relative to sealevel (meters)")

legend_data <- c("Measured data")

c <- 2
for (benchmark_output_filename in benchmark_output_files)
{
  tmp <- unlist(strsplit(sub(benchmark_output_group_regexp, "\\1 \\2 \\3", benchmark_output_filename), split=" "))
  nd <- tmp[1]
  na <- tmp[2]
  ncfl <- tmp[3]
  
  dataset_name <- ""
  dataset_name <- paste(paste(dataset_name, "d=", sep=""), nd, sep="")
  dataset_name <- paste(dataset_name, ", ", sep="")
  dataset_name <- paste(paste(dataset_name, "a=", sep=""), na, sep="")
  dataset_name <- paste(dataset_name, ", ", sep="")
  dataset_name <- paste(paste(dataset_name, "cfl=", sep=""), ncfl, sep="")

  # benchmark output filename
  benchmark_output_file <- paste(benchmark_output_dir, benchmark_output_filename, sep="/")
  benchmark_single_dataset <- read.table(benchmark_output_file, sep="\t")
  
  output_dataset_name <- sub(pattern=".txt", replace="", x=benchmark_output_filename)
  output_dataset_name <- sub(pattern="output_dart_", replace="", x=output_dataset_name)
  
  lines(benchmark_single_dataset$V1, benchmark_single_dataset$V2, type="l", col=rainbow_map[c])
  
  legend_data <- c(legend_data, dataset_name)
  c = c+1
}

legend("topright", legend_data, col=rainbow_map, lty=1:1)

#dev.off()