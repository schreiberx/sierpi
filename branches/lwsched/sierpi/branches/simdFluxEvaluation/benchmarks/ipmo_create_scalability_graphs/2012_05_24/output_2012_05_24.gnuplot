reset

set terminal pdf enhanced linewidth 1 font "Sans,5.5" dashed

# no legend
set nokey
#set key left top

set xlabel 'Number of threads'
#set logscale x
set xrange [1:40]

set ylabel 'Grid-cells'

set zlabel 'Mega triangles per second (MTPS)' rotate by 90
set zrange [0:50]

set style data line
#set style data linespoints


set output "output_2012_05_24_lines.pdf"
splot 'data2.dat' using 1:2:3 with lines

set pm3d at s hidden3d 100
set style line 100 linecolor rgb "black"
unset hidden3d
unset surf 


set output "output_2012_05_24.pdf"
splot 'data2.dat' using 1:2:3 with pm3d, 'data2.dat' using 1:2:3 with lines linecolor rgb "black"



