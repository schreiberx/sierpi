/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include "libmath/CMath.hpp"


#include "CPMO.hpp"

#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
	CPMO *cPmo;
	int max_cores;

public:

	CMainThreading()
	{
		max_cores = 512;
	}

	void threading_setup()
	{
		cPmo = new CPMO;

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): " << (int)omp_get_max_threads() << std::endl;

		threading_updateRessourceUtilization();
	}


	/**
	 * update the ressource utilization
	 */
	void threading_updateRessourceUtilization()
	{
		setNumberOfThreadsToUse(cPmo->num_running_threads);

		std::vector<float> v1(40, 0);
		float s = 1.0;

		float dFac_array[] = {
				0.1,		// depth == 0
				0.1,
				0.1,
				0.1,
				0.2,
				0.2,		// depth == 5
				0.2,
				0.2,
				0.2,
				0.2,
				0.25,		// depth == 10
				0.3,
				0.35,
				0.4,
				0.45,
				0.5,		// depth == 15
				0.55,
				0.6,
				0.65,
				0.7,
				0.75,		// depth == 20
				0.8,
				0.85,
				0.9,
				0.9,
				0.9,		// depth == 25
		};


		float dFac = 0;
		if (getGridInitialRecursionDepth() > 25)
			dFac = 1.0-0.9;
		else
			dFac = 1.0-dFac_array[getGridInitialRecursionDepth()];


		double v0 = 2.0-1.0/std::sqrt(dFac);
		double shift = 1.0-v0;

		for (size_t i = 0; i < v1.size(); i++)
		{
			double d = (double)i / (double)v1.size();
			d = 1.0-(d*d);

			v1[i] = 2.0-1.0/CMath::sqrt(((double)i+1.0)*dFac) + shift;
		}

		cPmo->invade(1, max_cores, v1);
	}


	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			// REINVADE
			cPmo->reinvade();

#pragma omp parallel
#pragma omp single nowait
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}


	bool threading_simulationLoopIteration()
	{
		// REINVADE
		cPmo->reinvade();

		bool retval;

#pragma omp parallel
#pragma omp single nowait
		retval = simulationLoopIteration();

		return retval;
	}


	void threading_shutdown()
	{
		cPmo->client_shutdown_hint = (double)getSimLoopSumNumberOfTriangles()*0.000001;
		cPmo->retreat();

		delete cPmo;
		cPmo = nullptr;
	}


	void threading_setNumThreads(int i)
	{
		max_cores = i;
		threading_updateRessourceUtilization();
	}


	virtual ~CMainThreading()
	{
		if (cPmo != nullptr)
			delete cPmo;
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
