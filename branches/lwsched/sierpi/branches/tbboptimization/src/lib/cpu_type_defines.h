/*
 * cpu_type_defines.h
 *
 *  Created on: Aug 10, 2011
 *      Author: schreibm
 */

#ifndef CPU_TYPE_DEFINES_H_
#define CPU_TYPE_DEFINES_H_



#include <limits.h>
#if (__WORDSIZE == 64)
#define BUILD_64	1
#else
#define BUILD_64	0
#endif


#endif /* CPU_TYPE_DEFINES_H_ */
