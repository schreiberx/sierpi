#ifndef MDEBUGCLASS_HPP__
#define MDEBUGCLASS_HPP__

#if 0
#ifdef DEBUG
	#include <typeinfo>
	#define DEBUG_FUN()			std::cout << __FUNCTION__ << std::endl;
	#define DEBUG_FUN_ARG(x)	std::cout << __FUNCTION__ << ": " << x << std::endl;
	#define DEBUG_METHOD()		std::cout << typeid(*this).name() << "::" << __FUNCTION__ << std::endl;
	#define DEBUG_METHOD_ARG(x)	std::cout << typeid(*this).name() << "::" << __FUNCTION__ << ": " << x << std::endl;
#endif
#endif

#ifndef DEBUG_FUN
	#define DEBUG_FUN()
	#define DEBUG_FUN_ARG(x)
	#define DEBUG_METHOD()
	#define DEBUG_METHOD_ARG(x)
#endif

#endif
