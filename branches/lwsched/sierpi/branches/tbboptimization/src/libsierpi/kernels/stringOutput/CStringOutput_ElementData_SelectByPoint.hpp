/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 12, 2011
 *      Author: schreibm
 */

#ifndef KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_HPP_
#define KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_HPP_

#include "libmath/CMath.hpp"

namespace sierpi
{
namespace kernels
{

template <typename p_TElementData>
class CStringOutput_ElementData_SelectByPoint
{
	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertex;

	TVertexScalar px, py;

public:
	typedef p_TElementData TElementData;

	typedef sierpi::travs::CTraversator_Vertices_ElementData<CStringOutput_ElementData_SelectByPoint<TElementData> > TRAV;


	inline void initBorderVertices(TVertexScalar v[4][2])
	{
		v[0][0] = -1;
		v[0][1] = 1;

		v[1][0] = 1;
		v[1][1] = 1;

		v[2][0] = 1;
		v[2][1] = -1;

		v[3][0] = -1;
		v[3][1] = -1;
	}

	CStringOutput_ElementData_SelectByPoint()
	{
	}

	inline bool elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			const TElementData *element_data
		)
	{

		if (!CPointInTriangleTest<TVertexScalar>::test(vx1, vy1, vx2, vy2, vx3, vy3, px, py))
			return false;

		std::cout << *element_data << std::endl;
		return true;
	}

	inline void setup(
				TVertexScalar p_px,
				TVertexScalar p_py
			)
	{
		px = p_px;
		py = p_py;
	}

	inline void setup_WithKernel(
			CStringOutput_ElementData_SelectByPoint<p_TElementData> *p
			)
	{
		px = p->px;
		py = p->py;
	}

	void pre_hook(CStack<char> &p_structure_stack)
	{
	}


	void post_hook(CStack<char> &p_structure_stack)
	{
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
