/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Nov 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de), Florian Klein (kleinfl@in.tum.de)
 *      
 *  Changelog:
 *    - 2012-02-18: Serial-regular implementation (Florian Klein)
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_REGULAR_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_REGULAR_HPP_


#include "../lib/CStopwatch.hpp"

#include "../tsunami_common/types/CTsunamiTypes.hpp"

#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

#if COMPILE_SIMULATION_WITH_GUI
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Tsunami.hpp"
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Root_Tsunami.hpp"

#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Tsunami.hpp"
#endif


#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_ElementData_Normal_SelectByPoint.hpp"


#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"
//#include "libsierpi/kernels/specialized_tsunami/CSetup_Column.hpp"

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "../tsunami_common/CSimulationCommon.hpp"
#include "lib/CStopwatch.hpp"


/*
 * additional classes for processing of regular patterns
 */
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/flux_solver/CFluxSolver.hpp"
#include "../tsunami_common/kernels/simulation/CFluxProjections.hpp"
#include "../tsunami_common/kernels/simulation/CEdgeComm_Tsunami.hpp"
#include "../../libsierpi/triangle/CTriangle_Normals.hpp"


#if SIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE
	#include "CSimulationTsunami_Serial_Regular_Traverse_Recursive.hpp"
#else
	#include "CSimulationTsunami_Serial_Regular_Traverse_Table.hpp"
#endif


class CSimulationTsunami	: public CSimulationCommon
{
public:
	CError error;

	/// traversator to setup the structure stack and element data stack by a given depth
	sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;

	/// to offer single-element modifications by coordinates, this traversal cares about it
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

	/// output element data at given point
	sierpi::kernels::CStringOutput_ElementData_Normal_SelectByPoint<CTsunamiSimulationStacks>::TRAV cStringOutput_ElementData_SelectByPoint;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	/// validation functions
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;
#endif

	/// exchange of edge communication data and timestep on fixed grid
#if SIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE
	sierpi::travs::CSimulationTsunami_Serial_Regular_Traverse_Recursive cTsunami_EdgeComm_Serial_Regular_Traverse_Recursive;
#else
	sierpi::travs::CSimulationTsunami_Serial_Regular_Traverse_Table cTsunami_EdgeComm_Serial_Regular_Traverse_Table;
#endif

	/// adaptive traversals to refine or coarsen grid cells without having hanging nodes
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element cTsunami_Adaptive;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Vertices_Element_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Element_Root_Tsunami;

	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,0>::TRAV cOpenGL_Vertices_Element_Tsunami_simple;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,3>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,4>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned;

	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;
	sierpi::kernels::COpenGL_Vertices_Wireframe_Tsunami<CTsunamiSimulationStacks, TTsunamiVertexScalar>::TRAV cOpenGL_Vertices_Wireframe_Tsunami;
#endif

	/// VTK visualization
	sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks>::TRAV cOutputVTK_Vertices_Element_Tsunami;

	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	sierpi::kernels::CSetup_TsunamiElementData_Validation::TRAV cSetup_TsunamiElementData_Validation;
#endif

public:
	CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *cStacks;

	float quadVertices[4][2];

	CTriangle_Factory template_rootTriangle;

	TTsunamiDataScalar cfl_timestep_condition;


#if COMPILE_SIMULATION_WITH_GUI
	/**
	 * set callback handler which has to be executed when parameters on gui have to be updated
	 */
	void(*callback_update_gui_func)(void *user_data);
	void *callback_update_gui_func_user_data;
#endif


public:
	CSimulationTsunami(char *p_initial_structure = NULL)	:
		cStacks(NULL)
#if COMPILE_SIMULATION_WITH_GUI
		,
		callback_update_gui_func(nullptr),
		callback_update_gui_func_user_data(nullptr)
#endif
	{
		/**
		 * setup depth for root triangle
		 */
		template_rootTriangle.recursionDepthFirstRecMethod = 0;
		template_rootTriangle.maxDepth = initial_recursion_depth + max_relative_recursion_depth;

		quadVertices[0][0] = -1;
		quadVertices[0][1] = 1;
		quadVertices[1][0] = 1;
		quadVertices[1][1] = 1;
		quadVertices[2][0] = 1;
		quadVertices[2][1] = -1;
		quadVertices[3][0] = -1;
		quadVertices[3][1] = -1;

		reset_Simulation();
	}


#if COMPILE_SIMULATION_WITH_GUI
	/**
	 * set callback handler which has to be executed when parameters on gui have to be updated
	 */
	void setCallbackParameterUpdate(
			void (*p_callback_update_gui_func)(void* user_data),
			void *p_callback_update_gui_func_user_data
	)
	{
		callback_update_gui_func = p_callback_update_gui_func;
		callback_update_gui_func_user_data = p_callback_update_gui_func_user_data;
	}


	inline void callbackParameterUpdate()
	{
		if (callback_update_gui_func != nullptr)
			callback_update_gui_func(callback_update_gui_func_user_data);
	}


	void render_surfaceDefault_simple(
			CGlProgram &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}



	void render_surface_aligned(
			CGlProgram &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_aligned.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_aligned(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Element_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();

		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Wireframe_Tsunami.action(cStacks);
		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
	}
#endif


	/**
	 * setup the stacks
	 */
	void setup_Stacks()
	{
		clean();

#if ADAPTIVE_SUBPARTITION_STACKS
		cStacks = new CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData>(
					(1 << initial_recursion_depth) + (ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING/sizeof(CTsunamiElementData)),
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#else
		cStacks = new CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData>(
					initial_recursion_depth+max_relative_recursion_depth,
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#endif

	}


	void setup_TraversatorsAndKernels()
	{
		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		template_rootTriangle.evenOdd = CTriangle_Enums::EVEN;
		template_rootTriangle.hypNormal = CTriangle_Enums::NORMAL_NE;
		template_rootTriangle.recursionDepthFirstRecMethod = 0;
		template_rootTriangle.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BORDER;
		template_rootTriangle.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BORDER;
		template_rootTriangle.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BORDER;

		template_rootTriangle.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		template_rootTriangle.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		template_rootTriangle.partitionTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		cTsunami_Adaptive.setup_sfcMethods(template_rootTriangle);
		//cTsunami_EdgeComm.setup_sfcMethods(template_rootTriangle);
		cSetup_Column.setup_sfcMethods(template_rootTriangle);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.setup_sfcMethods(template_rootTriangle);
#endif

		cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(template_rootTriangle);
		cStringOutput_ElementData_SelectByPoint.setup_sfcMethods(template_rootTriangle);

#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Vertices_Element_Tsunami_simple.setup_sfcMethods(template_rootTriangle);
		cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(0, (TTsunamiDataScalar)0.2, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(template_rootTriangle);
		cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(0, (TTsunamiDataScalar)0.2, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.setup_sfcMethods(template_rootTriangle);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.cKernelClass.setup(0, ((TTsunamiDataScalar)0.3)/surface_bathymetry_base_distance, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.setup_sfcMethods(template_rootTriangle);
		cOpenGL_Vertices_Element_Tsunami_Bathymetry_aligned.cKernelClass.setup(0, ((TTsunamiDataScalar)0.3)/surface_bathymetry_base_distance, &cOpenGL_Vertices_Element_Root_Tsunami);

		cOpenGL_Vertices_Wireframe_Tsunami.setup_sfcMethods(template_rootTriangle);
		cOpenGL_Vertices_Wireframe_Tsunami.cKernelClass.setup(0, (TTsunamiDataScalar)0.2, &cOpenGL_Vertices_Wireframe_Root_Tsunami);
#endif

		cModify_OneElementValue_SelectByPoint.setup_sfcMethods(template_rootTriangle);
	}

	void setup()
	{
		reset_Simulation();
	}

	void reset_Simulation()
	{
		cfl_timestep_condition = 0;

		setup_Stacks();
		setup_TraversatorsAndKernels();
		setup_Parameters();
		setup_InitialTriangulation();

		resetCommon();
	}


	void clean()
	{
		if (cStacks)
		{
			delete cStacks;
			cStacks = NULL;
		}
	}

	unsigned long long setup_ColumnAt2DPosition(
			float x = 0,
			float y = 0,
			float radius = 0.3
	)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
		cSetup_Column.setup_KernelClass(
				x,
				y,
				radius,
				&element_data_modifier,
				&element_data_modifier.dofs,
				&element_data_setup,
				&element_data_setup.dofs
		);
#else
		cSetup_Column.setup_KernelClass(
				x,
				y,
				radius,
				&element_data_modifier,
				&element_data_modifier.left_edge,
				&element_data_setup,
				&element_data_setup.left_edge
		);
#endif

		unsigned long long prev_number_of_triangles;
		do
		{
			prev_number_of_triangles = number_of_triangles;

			bool repeat_traversal = cSetup_Column.actionFirstTraversal(cStacks);

			while(repeat_traversal)
				repeat_traversal = cSetup_Column.actionMiddleTraversals_Serial(cStacks);

			cSetup_Column.actionLastTraversal_Serial(cStacks);

			number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
		} while (number_of_triangles != prev_number_of_triangles);

		number_of_triangles = prev_number_of_triangles;
		return number_of_triangles;
	}


	void setup_Parameters()
	{
		// compute timestep size automatically
		computeTimestepSize(-1.0);

#if SIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE
		cTsunami_EdgeComm_Serial_Regular_Traverse_Recursive.setup_WithParameters(timestep_size, domain_length, 9.81);
		cTsunami_EdgeComm_Serial_Regular_Traverse_Recursive.setup_WithParameters_KernelBoundaryDirichlet(&dirichlet_boundary_edge_data);
#else
		cTsunami_EdgeComm_Serial_Regular_Traverse_Table.setup_WithParameters(timestep_size, domain_length, 9.81);
		cTsunami_EdgeComm_Serial_Regular_Traverse_Table.setup_WithParameters_KernelBoundaryDirichlet(&dirichlet_boundary_edge_data);
#endif

		cTsunami_Adaptive.setup_KernelClass(
				domain_length,

				refine_height_threshold,
				coarsen_height_threshold,

				refine_slope_threshold,
				coarsen_slope_threshold
			);

		cTsunami_Adaptive.setup_RootTraversator(initial_recursion_depth+min_relative_recursion_depth, initial_recursion_depth+max_relative_recursion_depth);

		cSetup_Column.setup_RootTraversator(initial_recursion_depth+min_relative_recursion_depth, initial_recursion_depth+max_relative_recursion_depth);
	}


	void setup_InitialTriangulation()
	{
		number_of_triangles = cSetup_Structure_ElementData.setup(cStacks, initial_recursion_depth, &element_data_setup);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.action(cStacks);
#endif
	}


	void refineRandomized()
	{
	}


private:
	void p_adaptive_timestep()
	{
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		number_of_triangles = cStacks->element_data_stacks.getNumberOfElementsOnStack();
	}


private:
	/**
	 * timestep (without adaptivity)
	 */
	void p_edge_comm_timestep()
	{
		if (adaptive_timestep_size)
		{
			// adaptive timestep size

			timestep_size = cfl_timestep_condition*SIMULATION_TSUNAMI_CFL;

//			std::cout << "CFL: " << cfl_timestep_condition << std::endl;
			if (timestep_size < 0.000001)
				timestep_size = 0.000001;
		}

		if (max_relative_recursion_depth != 0 || min_relative_recursion_depth != 0)
		{
			std::cout << "min/max depth != 0 !!!" << std::endl;
			std::cout << "append '-a 0' to run regular tests" << std::endl;
			assert(false);
			exit(-1);
		}

		//std::cout << "cfl: " << cfl_timestep_condition << ", timestep size: " << timestep_size << std::endl;
#if SIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE
		cfl_timestep_condition = cTsunami_EdgeComm_Serial_Regular_Traverse_Recursive.traverse(cStacks, initial_recursion_depth, timestep_size);
#else
		cfl_timestep_condition = cTsunami_EdgeComm_Serial_Regular_Traverse_Table.traverse(cStacks, initial_recursion_depth, timestep_size);
#endif

	}


public:
	void refineAt2DPosition(float x, float y, float radius_not_used = 0)
	{
		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(domain_length, refine_height_threshold, -1, refine_slope_threshold, -999);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold);
	}


	void coarsenAt2DPosition(float x, float y)
	{
		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_setup);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(domain_length, 99999, coarsen_height_threshold, 999, coarsen_slope_threshold);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold);
	}

public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(domain_length, refine_height_threshold, -1, refine_slope_threshold, -999);
		p_adaptive_timestep();
		cTsunami_Adaptive.setup_KernelClass(domain_length, refine_height_threshold, coarsen_height_threshold, refine_slope_threshold, coarsen_slope_threshold);
	}


public:
	void runSingleTimestep()
	{
		p_edge_comm_timestep();
		p_adaptive_timestep();

		timestep_nr++;
		time_for_timestep += timestep_size;
	}


	CStopwatch cStopwatch;

public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_edge_comm_timestep();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_adaptive_timestep();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		timestep_nr++;
		time_for_timestep += timestep_size;
	}



public:
	void debugOutput(CVector<2,float> planePosition)
	{
		cStringOutput_ElementData_SelectByPoint.cKernelClass.setup(planePosition[0], planePosition[1]);
		cStringOutput_ElementData_SelectByPoint.action(cStacks->structure_stacks, cStacks->element_data_stacks);
	}


	void writeTrianglesToVTKFile(
			const char *p_filename,
			const char *additional_vtk_info_string = NULL
	)
	{
			std::ofstream vtkfile;
			vtkfile.open(p_filename);

			vtkfile << "# vtk DataFile Version 5.0" << std::endl;
			vtkfile << "Sierpi VTK File";
			if (additional_vtk_info_string != NULL)
				vtkfile << ": " << additional_vtk_info_string;
			vtkfile << std::endl;
			vtkfile << "ASCII" << std::endl;
			vtkfile << "DATASET POLYDATA" << std::endl;

			// output 3 x #triangles vertices
			vtkfile << "POINTS " << (number_of_triangles*3) << " float" << std::endl;

			// ACTION
			cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(&vtkfile);
//			cOutputVTK_Vertices_Element_Tsunami.setup_Stacks(&cStacks->structure_stacks, &cStacks->element_data_stacks);
			cOutputVTK_Vertices_Element_Tsunami.action(cStacks);

			// output 3 x #triangles vertices
			vtkfile << std::endl;
			vtkfile << "TRIANGLE_STRIPS " << (number_of_triangles) << " " << (number_of_triangles*4) << std::endl;
			for (unsigned long long i = 0; i < number_of_triangles; i++)
				vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

			vtkfile.close();
	}

	void keydown(int key)
	{
		switch(key)
		{
			case 'f':
				refineRandomized();
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_ColumnAt2DPosition(-0.5, 0.4, 0.2);
				break;

			case 't':
				initial_recursion_depth += 1;
				reset_Simulation();
				std::cout << "Setting initial recursion depth to " << initial_recursion_depth << std::endl;
				break;

			case 'T':
				max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'g':
				if (initial_recursion_depth > 0)
					initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << initial_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'G':
				if (max_relative_recursion_depth > 0)
					max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

		}
	}

	virtual ~CSimulationTsunami()
	{
		clean();
	}
};

#endif /* CTSUNAMI_HPP_ */
