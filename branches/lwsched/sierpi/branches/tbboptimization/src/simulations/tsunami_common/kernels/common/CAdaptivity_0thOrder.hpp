/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"



/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_0thOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiEdgeData TEdgeData;


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	TTsunamiDataScalar cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;


	/**
	 * constructor
	 */
	CAdaptivity_0thOrder()	:
		cathetus_side_length(-1),
		cSimulationDataSets(nullptr),
		cfl_domain_size_div_max_wave_speed(-1)
	{

	}


	void traversal_pre_hook()
	{
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}



	/**
	 * get center of weight for triangle
	 */
	inline void computeAdaptiveSamplingPoint(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiVertexScalar *o_mx, TTsunamiVertexScalar *o_my
	)
	{
#if 1
		// use midpoint on hypotenuse
		*o_mx = (vright_x)*(TTsunamiVertexScalar)(1.0/2.0) +
				(vleft_x)*(TTsunamiVertexScalar)(1.0/2.0);

		*o_my = (vright_y)*(TTsunamiVertexScalar)(1.0/2.0) +
				(vleft_y)*(TTsunamiVertexScalar)(1.0/2.0);

#else

		*o_mx = vtop_x +
				(vright_x - vtop_x)*(TTsunamiVertexScalar)(1.0/3.0) +
				(vleft_x - vtop_x)*(TTsunamiVertexScalar)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(TTsunamiVertexScalar)(1.0/3.0) +
				(vleft_y - vtop_y)*(TTsunamiVertexScalar)(1.0/3.0);

#endif
	}



	/**
	 * get center of weight for triangle for both childs
	 */
	inline void computeAdaptiveSamplingPointForLeftAndRightChild(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiVertexScalar *o_left_mx, TTsunamiVertexScalar *o_left_my,
			TTsunamiVertexScalar *o_right_mx, TTsunamiVertexScalar *o_right_my
	)
	{
#if 1
		// use midpoint on hypotenuse

		*o_left_mx = (vright_x)*(TTsunamiVertexScalar)(1.0/2.0) +
				(vleft_x)*(TTsunamiVertexScalar)(1.0/2.0);

		*o_left_my = (vright_y)*(TTsunamiVertexScalar)(1.0/2.0) +
				(vleft_y)*(TTsunamiVertexScalar)(1.0/2.0);

		*o_right_mx = *o_left_mx;
		*o_right_my = *o_left_my;

#else

		// midpoint on hypotenuse
		TTsunamiDataScalar mx = (vleft_x + vright_x)*(TTsunamiDataScalar)(1.0/2.0);
		TTsunamiDataScalar my = (vleft_y + vright_y)*(TTsunamiDataScalar)(1.0/2.0);

		TTsunamiDataScalar dx_left = (vleft_x - mx)*(TTsunamiDataScalar)(1.0/3.0);
		TTsunamiDataScalar dy_left = (vleft_y - my)*(TTsunamiDataScalar)(1.0/3.0);

		TTsunamiDataScalar dx_up = (vtop_x - mx)*(TTsunamiDataScalar)(1.0/3.0);
		TTsunamiDataScalar dy_up = (vtop_y - my)*(TTsunamiDataScalar)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;

#endif
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int i_depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		assert (cSimulationDataSets != nullptr);

		TTsunamiVertexScalar lmx, lmy, rmx, rmy;

		computeAdaptiveSamplingPointForLeftAndRightChild(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,

				&lmx, &lmy,
				&rmx, &rmy
			);

		TTsunamiDataScalar h_horizon = i_elementData->dofs.b + i_elementData->dofs.h;

		o_left_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(lmx, lmy, 0);
		o_left_elementData->dofs.h = -o_left_elementData->dofs.b + h_horizon;
		o_left_elementData->dofs.qx = i_elementData->dofs.qx;
		o_left_elementData->dofs.qy = i_elementData->dofs.qy;

		o_right_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(rmx, rmy, 0);
		o_right_elementData->dofs.h = -o_right_elementData->dofs.b + h_horizon;
		o_right_elementData->dofs.qx = i_elementData->dofs.qx;
		o_right_elementData->dofs.qy = i_elementData->dofs.qy;

		assert(cfl_domain_size_div_max_wave_speed != -1);

		TTsunamiDataScalar local_cfl_domain_size_div_max_wave_speed = i_elementData->cfl_domain_size_div_max_wave_speed*(TTsunamiDataScalar)(1.0/CMath::sqrt(2.0));

		o_left_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);



#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				i_depth,
				&o_left_elementData->validation,
				&o_right_elementData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int i_depth,

			CTsunamiElementData *o_elementData,
			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			TTsunamiVertexScalar mx = (vleft_x + vright_x)*(TTsunamiVertexScalar)0.5;
			TTsunamiVertexScalar my = (vleft_y + vright_y)*(TTsunamiVertexScalar)0.5;

			i_left_elementData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_elementData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif


		assert (cSimulationDataSets != nullptr);

		TTsunamiVertexScalar mx, my;

		computeAdaptiveSamplingPoint(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				&mx, &my
			);

		TTsunamiVertexScalar b = (i_left_elementData->dofs.b + i_right_elementData->dofs.b)*(TTsunamiDataScalar)0.5;

		o_elementData->dofs.b = cSimulationDataSets->getTerrainHeightByPosition(mx, my, 0);

// TODO: remove me
//			o_elementData->dofs.b = (i_left_elementData->dofs.b + i_right_elementData->dofs.b)*(TTsunamiDataScalar)0.5;
		o_elementData->dofs.h = (i_left_elementData->dofs.h + i_right_elementData->dofs.h)*(TTsunamiDataScalar)0.5 + (b - o_elementData->dofs.b);
		o_elementData->dofs.qx = (i_left_elementData->dofs.qx + i_right_elementData->dofs.qx)*(TTsunamiDataScalar)0.5;
		o_elementData->dofs.qy = (i_left_elementData->dofs.qy + i_right_elementData->dofs.qy)*(TTsunamiDataScalar)0.5;

		o_elementData->cfl_domain_size_div_max_wave_speed = (i_left_elementData->cfl_domain_size_div_max_wave_speed + i_right_elementData->cfl_domain_size_div_max_wave_speed)*(TTsunamiDataScalar)(0.5*CMath::sqrt(2.0));

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_coarsed_element->refine = false;
		o_coarsed_element->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_elementData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_elementData->validation,
				&i_right_elementData->validation
		);
#endif
	}
};


#endif /* CADAPTIVITY_0STORDER_HPP_ */
