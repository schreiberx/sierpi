/*
 * CEdgeComm_Tsunami_Config.hpp
 *
 *  Created on: Jan 24, 2012
 *      Author: schreibm
 */

#ifndef CEDGECOMM_TSUNAMI_PARAMETERS_HPP_
#define CEDGECOMM_TSUNAMI_PARAMETERS_HPP_


class CEdgeComm_TsunamiParameters
{
public:
	typedef TTsunamiDataScalar T;

	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar cathetus_real_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiEdgeData boundary_dirichlet;

	/**
	 * available boundary conditions
	 */
	enum EBoundaryCondition
	{
		BOUNDARY_CONDITION_DIRICHLET,
		BOUNDARY_CONDITION_VELOCITY_DAMPING,
		BOUNDARY_CONDITION_BOUNCE_BACK
	};


	/**
	 * set-up boundary condition
	 */
	EBoundaryCondition eBoundaryCondition;

	/**
	 * damping factor when velocity damping boundary condition is used
	 */
	TTsunamiDataScalar eBoundaryConditionVelocityDampingFactor;


	/******************************************************
	 * CFL condition stuff
	 *
	 * TODO: the CFL condition also has to be built-in into the
	 * adaptive traversals (due to refine operations)
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;

	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;


	/*
	 * inner radius for rectangular triangle:
	 *
	 * r = ab/(a+b+c)
	 *
	 * a = i
	 * b = i
	 * c = i sqrt(2)
	 *
	 * r = i^2 / (2i + i sqrt(2)) = i / (2 + sqrt(2))
	 */
	TTsunamiDataScalar incircle_unit_diameter;


	/**
	 * setup with default values
	 */
	CEdgeComm_TsunamiParameters()	:
		eBoundaryCondition(BOUNDARY_CONDITION_DIRICHLET),
		eBoundaryConditionVelocityDampingFactor(0.9)
	{
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
		timestep_size = -CMath::numeric_inf<T>();
		cathetus_real_length = -CMath::numeric_inf<T>();
		gravitational_constant = -CMath::numeric_inf<T>();

		incircle_unit_diameter = (TTsunamiDataScalar)(2.0/(2.0+CMath::sqrt(2.0)));
	}


	/**
	 * return the reduce value for the traversator
	 */
	inline void storeReduceValue(TTsunamiDataScalar *o_reduceValue)
	{
		*o_reduceValue = cfl_domain_size_div_max_wave_speed;
	}

	/**
	 * return the set-up timestep size
	 */
	inline TTsunamiDataScalar getTimestepSize()
	{
		return timestep_size;
	}




	/**
	 * \brief set the timestep size
	 */
	inline void setTimestepSize(T i_timestep_size)
	{
		timestep_size = i_timestep_size;
	}


	/**
	 * \brief set the gravitation
	 *
	 * WARNING: this value is not always used by the fluxes in the current state!
	 */
	inline void setGravitationalConstant(T i_gravitational_constant)
	{
		gravitational_constant = i_gravitational_constant;
	}



	/**
	 * \brief set the length of a catheti
	 */
	inline void setSquareSideLength(T i_square_side_length)
	{
		cathetus_real_length = i_square_side_length;
	}



	/**
	 * setup parameter for dirichlet boundary condition
	 */
	void setBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		boundary_dirichlet = *p_boundary_dirichlet;
	}



	/**
	 * \brief set all parameters for simulation
	 */
	inline void setParameters(
			T i_timestep_size,					///< timestep size
			T i_square_side_length,				///< length of a square (a single catheti)
			T i_gravitational_constant = 9.81	///< gravitational constant
	)
	{
		timestep_size = i_timestep_size;
		gravitational_constant = i_gravitational_constant;
		cathetus_real_length = i_square_side_length;
	}



	/**
	 * \brief compute CFL, update sub-partition local CFL and return local CFL
	 */
	inline void computeAndUpdateCFLCondition3(
			T i_max_wave_speed1,
			T i_max_wave_speed2,
			T i_max_wave_speed3,

			T i_depth,
			T *o_local_domain_size_div_max_wave_speed
	)
	{
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2, i_max_wave_speed3);

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;

		*o_local_domain_size_div_max_wave_speed = cat_length/max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(*o_local_domain_size_div_max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}

#if 0
	/**
	 * \brief update the CFL condition with 3 given max wavespeeds
	 */
	inline void updateCFLCondition3(
			T i_max_wave_speed1,
			T i_max_wave_speed2,
			T i_max_wave_speed3,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2, i_max_wave_speed3);

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}



	/**
	 * \brief update the CFL condition with 2 given max wavespeeds
	 */
	inline void updateCFLCondition2(
			T i_max_wave_speed1,
			T i_max_wave_speed2,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T max_wave_speed = CMath::max(i_max_wave_speed1, i_max_wave_speed2);

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/max_wave_speed, cfl_domain_size_div_max_wave_speed);
	}





	/**
	 * \brief update the CFL condition with 2 given max wavespeeds
	 */
	inline void updateCFLCondition1(
			T i_max_wave_speed1,

			T i_depth
	)
	{
		/**
		 * CFL condition handling
		 */
		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length*incircle_unit_diameter;
		cfl_domain_size_div_max_wave_speed = CMath::min(cat_length/i_max_wave_speed1, cfl_domain_size_div_max_wave_speed);
	}
#endif
};



#endif /* CEDGECOMM_TSUNAMI_CONFIG_HPP_ */
