/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 24, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CTSUNAMI_TYPES_VALIDATE_ELEMENT_DATA_HPP
#define CTSUNAMI_TYPES_VALIDATE_ELEMENT_DATA_HPP

#include <string.h>

#include "CValidation_EdgeData.hpp"

/*
 * validation data for element data
 */
class CValidation_ElementData
{
	TTsunamiDataScalar CR_EPSILON;

public:
	/*
	 * vertex coordinates for this triangle in normalized position.
	 *
	 * left(0) and right(1) vertex at triangle hypotenuse and top(2) vertex of triangle
	 */
	TTsunamiVertexScalar vertices[3][2];

	/*
	 * triangle recursive depth
	 */
	int depth;

	/*
	 * setup midpoint coordinates for hypotenuse edge comm data
	 */
	void setupHypEdgeCommData(
		CValidation_EdgeData *o_edgeData
	)	const
	{
		o_edgeData->midpoint_x = (vertices[0][0]+vertices[1][0])*(TTsunamiVertexScalar)0.5;
		o_edgeData->midpoint_y = (vertices[0][1]+vertices[1][1])*(TTsunamiVertexScalar)0.5;
	}


	/*
	 * setup midpoint coordinates for right edge comm data
	 */
	void setupRightEdgeCommData(
		CValidation_EdgeData *o_edgeData
	)	const
	{
		o_edgeData->midpoint_x = (vertices[1][0]+vertices[2][0])*(TTsunamiVertexScalar)0.5;
		o_edgeData->midpoint_y = (vertices[1][1]+vertices[2][1])*(TTsunamiVertexScalar)0.5;
	}


	/*
	 * setup midpoint coordinates for left edge comm data
	 */
	void setupLeftEdgeCommData(
		CValidation_EdgeData *o_edgeData
	)	const
	{
		o_edgeData->midpoint_x = (vertices[2][0]+vertices[0][0])*(TTsunamiVertexScalar)0.5;
		o_edgeData->midpoint_y = (vertices[2][1]+vertices[0][1])*(TTsunamiVertexScalar)0.5;
	}


	void set(
		TTsunamiVertexScalar vx0, TTsunamiVertexScalar vy0,
		TTsunamiVertexScalar vx1, TTsunamiVertexScalar vy1,
		TTsunamiVertexScalar vx2, TTsunamiVertexScalar vy2,
		int i_depth
	)
	{
		vertices[0][0] = vx0;
		vertices[0][1] = vy0;
		vertices[1][0] = vx1;
		vertices[1][1] = vy1;
		vertices[2][0] = vx2;
		vertices[2][1] = vy2;
		depth = i_depth;
	}


	void setupLeftElementFromParent(
			CValidation_ElementData *parentElement
		)
	{
		depth = parentElement->depth+1;

		/*
		 *          2
		 *         /|\
		 *       /  |  \
		 *     /    |    \
		 *   /      |      \
		 * /________|________\
		 * 0                 1
		 *
		 */
		assert(parentElement->vertices[0][0] != parentElement->vertices[1][0] || parentElement->vertices[0][1] != parentElement->vertices[1][1]);
		assert(parentElement->vertices[0][0] != parentElement->vertices[2][0] || parentElement->vertices[0][1] != parentElement->vertices[2][1]);

		TTsunamiVertexScalar mx = (parentElement->vertices[0][0]+parentElement->vertices[1][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar my = (parentElement->vertices[0][1]+parentElement->vertices[1][1])*(TTsunamiVertexScalar)0.5;

		vertices[0][0] = parentElement->vertices[2][0];
		vertices[0][1] = parentElement->vertices[2][1];
		vertices[1][0] = parentElement->vertices[0][0];
		vertices[1][1] = parentElement->vertices[0][1];
		vertices[2][0] = mx;
		vertices[2][1] = my;
	}


	void setupRightElementFromParent(
			CValidation_ElementData *parentElement
		)
	{
		depth = parentElement->depth+1;

		/*
		 *          2
		 *         /|\
		 *       /  |  \
		 *     /    |    \
		 *   /      |      \
		 * /________|________\
		 * 0                 1
		 *
		 */
		assert(parentElement->vertices[0][0] != parentElement->vertices[1][0] || parentElement->vertices[0][1] != parentElement->vertices[1][1]);
		assert(parentElement->vertices[0][0] != parentElement->vertices[2][0] || parentElement->vertices[0][1] != parentElement->vertices[2][1]);

		TTsunamiVertexScalar mx = (parentElement->vertices[0][0]+parentElement->vertices[1][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar my = (parentElement->vertices[0][1]+parentElement->vertices[1][1])*(TTsunamiVertexScalar)0.5;

		vertices[0][0] = parentElement->vertices[1][0];
		vertices[0][1] = parentElement->vertices[1][1];
		vertices[1][0] = parentElement->vertices[2][0];
		vertices[1][1] = parentElement->vertices[2][1];
		vertices[2][0] = mx;
		vertices[2][1] = my;
	}


	void setupRefineLeftAndRight(
		TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
		TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
		TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,
		int i_depth,
		CValidation_ElementData *left_element,
		CValidation_ElementData *right_element
	)
	{
		assert(depth == i_depth);
		left_element->depth = i_depth+1;
		right_element->depth = i_depth+1;

		/*
		 * test for correct normal
		 */
		TTsunamiDataScalar vecx;
		TTsunamiDataScalar vecy;
		TTsunamiDataScalar inv_length;
		TTsunamiDataScalar close_to_one;

		/**
		 * HYP
		 */
		vecx = vertices[0][0] - vertices[1][0];
		vecy = vertices[0][1] - vertices[1][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_hypy - vecy*normal_hypx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}


		/**
		 * right edge
		 */

		vecx = vertices[1][0] - vertices[2][0];
		vecy = vertices[1][1] - vertices[2][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_righty - vecy*normal_rightx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}


		/**
		 * left edge
		 */

		vecx = vertices[2][0] - vertices[0][0];
		vecy = vertices[2][1] - vertices[0][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_lefty - vecy*normal_leftx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}

		/*
		 *          2
		 *         /|\
		 *       /  |  \
		 *     /    |    \
		 *   /      |      \
		 * /________|________\
		 * 0                 1
		 *
		 */
		assert(vertices[0][0] != vertices[1][0] || vertices[0][1] != vertices[1][1]);
		assert(vertices[0][0] != vertices[2][0] || vertices[0][1] != vertices[2][1]);

		TTsunamiVertexScalar mx = (vertices[0][0]+vertices[1][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar my = (vertices[0][1]+vertices[1][1])*(TTsunamiVertexScalar)0.5;

		left_element->vertices[0][0] = vertices[2][0];
		left_element->vertices[0][1] = vertices[2][1];
		left_element->vertices[1][0] = vertices[0][0];
		left_element->vertices[1][1] = vertices[0][1];
		left_element->vertices[2][0] = mx;
		left_element->vertices[2][1] = my;

		right_element->vertices[0][0] = vertices[1][0];
		right_element->vertices[0][1] = vertices[1][1];
		right_element->vertices[1][0] = vertices[2][0];
		right_element->vertices[1][1] = vertices[2][1];
		right_element->vertices[2][0] = mx;
		right_element->vertices[2][1] = my;
	}



	void setupCoarsen(
			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int i_depth,

			CValidation_ElementData *left_element,
			CValidation_ElementData *right_element
	)
	{
		assert(left_element->depth == right_element->depth);
		depth = left_element->depth-1;
		assert(depth == i_depth);


		/*
		 *          2
		 *         /|\
		 *       /  |  \
		 *     /    |    \
		 *   /      |      \
		 * /________|________\
		 * 0                 1
		 *
		 */

		if (left_element->vertices[2][0] != right_element->vertices[2][0])
		{
			std::cout << "COARSEN DIFF MID X: " << left_element->vertices[2][0] << " " << right_element->vertices[2][0] << std::endl;
			assert(false);
		}

		if (left_element->vertices[2][1] != right_element->vertices[2][1])
		{
			std::cout << "COARSEN DIFF MID Y: " << left_element->vertices[2][1] << " " << right_element->vertices[2][1] << std::endl;
			assert(false);
		}

		if (left_element->vertices[0][0] != right_element->vertices[1][0])
		{
			std::cout << "COARSEN DIFF MID X: " << left_element->vertices[0][0] << " " << right_element->vertices[1][0] << std::endl;
			assert(false);
		}

		if (left_element->vertices[0][1] != right_element->vertices[1][1])
		{
			std::cout << "COARSEN DIFF MID Y: " << left_element->vertices[0][1] << " " << right_element->vertices[1][1] << std::endl;
			assert(false);
		}

		vertices[0][0] = left_element->vertices[1][0];
		vertices[0][1] = left_element->vertices[1][1];
		vertices[1][0] = right_element->vertices[0][0];
		vertices[1][1] = right_element->vertices[0][1];
		vertices[2][0] = right_element->vertices[1][0];
		vertices[2][1] = right_element->vertices[1][1];

		/*
		 * test for correct normal
		 */

		TTsunamiDataScalar vecx;
		TTsunamiDataScalar vecy;
		TTsunamiDataScalar inv_length;
		TTsunamiDataScalar close_to_one;

		/**
		 * HYP
		 */
		vecx = vertices[0][0] - vertices[1][0];
		vecy = vertices[0][1] - vertices[1][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_hypy - vecy*normal_hypx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}

		/**
		 * right edge
		 */
		vecx = vertices[1][0] - vertices[2][0];
		vecy = vertices[1][1] - vertices[2][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_righty - vecy*normal_rightx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}


		/**
		 * left edge
		 */
		vecx = vertices[2][0] - vertices[0][0];
		vecy = vertices[2][1] - vertices[0][1];

		inv_length = 1.0/CMath::sqrt(vecx*vecx + vecy*vecy);

		vecx *= inv_length;
		vecy *= inv_length;

		close_to_one = CMath::abs(vecx*normal_lefty - vecy*normal_leftx);
		if (close_to_one < CR_EPSILON)
		{
			std::cerr << "VALIDATION: NORMAL ERROR " << close_to_one << std::endl;
			assert(false);
		}
	}



	void testVertices(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y
	)
	{
		if (vertices[0][0] != vleft_x)
		{
			std::cout << "EA diff: " << vertices[0][0] << " " << vleft_x << std::endl;
			assert(false);
		}
		if (vertices[0][1] != vleft_y)
		{
			std::cout << "EA diff: " << vertices[0][1] << " " << vleft_y << std::endl;
			assert(false);
		}

		if (vertices[1][0] != vright_x)
		{
			std::cout << "EA diff: " << vertices[1][0] << " " << vright_x << std::endl;
			assert(false);
		}
		if (vertices[1][1] != vright_y)
		{
			std::cout << "EA diff: " << vertices[1][1] << " " << vright_y << std::endl;
			assert(false);
		}

		if (vertices[2][0] != vtop_x)
		{
			std::cout << "EA diff: " << vertices[2][0] << " " << vtop_x << std::endl;
			assert(false);
		}
		if (vertices[2][1] != vtop_y)
		{
			std::cout << "EA diff: " << vertices[2][1] << " " << vtop_y << std::endl;
			assert(false);
		}
	}


	void testDepth(int i_depth)
	{
		if (depth != i_depth)
			std::cout << "DEPTH ERROR: (element/parameter) " << depth << " " << i_depth << std::endl;
	}


	void testEdgeMidpoints(
			CValidation_EdgeData &i_hypEdgeData,
			CValidation_EdgeData &i_rightEdgeData,
			CValidation_EdgeData &i_leftEdgeData
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		TTsunamiVertexScalar hyp_midpoint_x = (vertices[0][0] + vertices[1][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_midpoint_y = (vertices[0][1] + vertices[1][1])*(TTsunamiVertexScalar)0.5;

		TTsunamiVertexScalar right_midpoint_x = (vertices[1][0] + vertices[2][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar right_midpoint_y = (vertices[1][1] + vertices[2][1])*(TTsunamiVertexScalar)0.5;

		TTsunamiVertexScalar left_midpoint_x = (vertices[2][0] + vertices[0][0])*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar left_midpoint_y = (vertices[2][1] + vertices[0][1])*(TTsunamiVertexScalar)0.5;

		if (	hyp_midpoint_x != i_hypEdgeData.midpoint_x ||
				hyp_midpoint_y != i_hypEdgeData.midpoint_y
		)
		{
			std::cout << "ERROR HYP   " << hyp_midpoint_x << " " << i_hypEdgeData.midpoint_x << std::endl;
			std::cout << "            " << hyp_midpoint_y << " " << i_hypEdgeData.midpoint_y << std::endl;
			assert(false);
		}

		if (	right_midpoint_x != i_rightEdgeData.midpoint_x ||
				right_midpoint_y != i_rightEdgeData.midpoint_y
		)
		{
			std::cout << "ERROR RIGHT   " << right_midpoint_x << " " << i_rightEdgeData.midpoint_x << std::endl;
			std::cout << "            " << right_midpoint_y << " " << i_rightEdgeData.midpoint_y << std::endl;
			assert(false);
		}

		if (	left_midpoint_x != i_leftEdgeData.midpoint_x ||
				left_midpoint_y != i_leftEdgeData.midpoint_y
		)
		{
			std::cout << "ERROR LEFT   " << left_midpoint_x << " " << i_leftEdgeData.midpoint_x << std::endl;
			std::cout << "            " << left_midpoint_y << " " << i_leftEdgeData.midpoint_y << std::endl;
			assert(false);
		}
#endif
	}


	CValidation_ElementData& operator=(const CValidation_ElementData &t)
	{
		if (t.depth != -666)
			depth = t.depth;

		if (t.vertices[0][0] != -666)
		{
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 2; j++)
					vertices[i][j] = t.vertices[i][j];
		}

		return *this;
	}


	CValidation_ElementData(const CValidation_ElementData &t)
	{
		if (t.depth != -666)
			depth = t.depth;

		if (t.vertices[0][0] != -666)
		{
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 2; j++)
					vertices[i][j] = t.vertices[i][j];
		}
	}

	CValidation_ElementData()	:
		CR_EPSILON(0.99999)
	{
		vertices[0][0] = -666;
		depth = -666;
	}



	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CValidation_ElementData &d)
	{
		return os << "(v: " << d.vertices[0][0] << ", " << d.vertices[0][1] << " | " << d.vertices[1][0] << ", " << d.vertices[1][1] << " | " << d.vertices[2][0] << ", " << d.vertices[2][1] << ")" << "(d: " << d.depth << ")";
	}
};




#endif
