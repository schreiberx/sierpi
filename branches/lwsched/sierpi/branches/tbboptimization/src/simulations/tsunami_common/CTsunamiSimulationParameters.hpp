/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationParameters.hpp
 *
 *  Created on: Jul 5, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_SIMULATION_PARAMETERS_HPP_
#define CTSUNAMI_SIMULATION_PARAMETERS_HPP_



#include <stdint.h>
#include "lib/CError.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "libgl/hud/CGlHudConfig.hpp"
	#include "mainvis/CGuiConfig.hpp"
#endif

/**
 * simulation parameters used by serial and parallel simulation
 */
class CTsunamiSimulationParameters
{
public:
	/**
	 * true, if the simulation should be driven
	 */
	bool run_simulation;

	/**
	 * reset simulation
	 */
	bool simulation_reset;

	/**
	 * operator() returns true if some error happened. Then no method should be executed
	 */
	CError error;


	/**
	 * timesteps which should be simulated per frame
	 */
	int timesteps_per_frame;

	/**
	 * initial recursion depth during setup
	 */
	int grid_initial_recursion_depth;

	/**
	 * minimum and maximum recursion depth during refinements / coarsenings
	 */
	int grid_min_relative_recursion_depth;
	int grid_max_relative_recursion_depth;

	/**
	 * thresholds for coarsening/refinement
	 */
	TTsunamiDataScalar coarsen_height_threshold;
	TTsunamiDataScalar refine_height_threshold;

	TTsunamiDataScalar coarsen_slope_threshold;
	TTsunamiDataScalar refine_slope_threshold;


	/**
	 * element data during setup
	 */
	CTsunamiElementData element_data_setup;

	/**
	 * Dirichlet boundary edge data
	 */
	CTsunamiEdgeData dirichlet_boundary_edge_data;

	/**
	 * adaptively set the timestep size by using CFL condition
	 */
	bool adaptive_timestep_size;

	/**
	 * domain length of unit square
	 */
	TTsunamiDataScalar simulation_parameter_domain_length;

	/**
	 * Root partitions per unit domain in the direction of the x axis.
	 *
	 * This is important to setup the domain length parameters for root triangles
	 * when more than 2 triangles are used to set-up the domain
	 */
	TTsunamiDataScalar simulation_parameter_root_partitions_per_x_axis_on_unit_domain;

	/**
	 * Gravitation for simulation
	 */
	TTsunamiDataScalar simulation_parameter_gravitation;

	/**
	 * CFL condition for adaptive timestep size
	 */
	TTsunamiDataScalar simulation_parameter_cfl;

	/**
	 * the time-step size
	 */
	TTsunamiDataScalar simulation_parameter_timestep_size;

	/**
	 * number of current timestep
	 */
	unsigned long long timestep_nr;

	/**
	 * time for current timestep
	 */
	TTsunamiDataScalar time_for_timestep;


	/**
	 * overall number of triangles in domain
	 */
	unsigned long long number_of_triangles;

	/**
	 * number of triangles directly after domain triangulation
	 */
	unsigned long long number_of_initial_triangles_after_domain_triangulation;


	/**
	 * number of simulation handler partitions
	 */
	unsigned int number_of_simulation_handler_partitions;

	/**
	 * number of threads to use for simulation
	 */
	int simulation_number_of_threads;


	/**
	 * number of triangles when to split the partition
	 */
	unsigned int splitting_partition_split_workload_size;


	/**
	 * number of overall triangles of 2 partitions when to join the partition
	 */
	unsigned int splitting_partition_join_workload_size;


	/**
	 * Set the number of triangles in a single partition when to split/join
	 */
	int partition_update_split_join_size_after_elapsed_timesteps;

	/**
	 * Scalar to use for non-table based computation of optimal split/join size
	 */
	int partition_update_split_join_size_after_elapsed_scalar;


	/**
	 * world scene (base triangulation)
	 */
	int simulation_world_scene_id;

	/**
	 * terrain scene id
	 */
	int simulation_terrain_scene_id;

	/**
	 * water surface scene id
	 */
	int simulation_water_surface_scene_id;


	/**
	 * emit random raindrops for testing purpose
	 */
	bool simulation_random_raindrops_activated;



	/*******************************************************
	 * DATASET RELATED PARAMETERS
	 */

	/**
	 * parameters for default setup of column
	 */
	TTsunamiDataScalar simulation_dataset_cylinder_posx;
	TTsunamiDataScalar simulation_dataset_cylinder_posy;
	TTsunamiDataScalar simulation_dataset_cylinder_radius;
	TTsunamiDataScalar simulation_dataset_cylinder_height;

	/**
	 * value to setup the fluid surface height
	 */
	TTsunamiDataScalar simulation_dataset_terrain_default_distance;

	/**
	 * value to setup the default water surface height(should be usually close to 0)
	 */
	TTsunamiDataScalar simulation_dataset_water_surface_default_displacement;


	/**
	 * Join/split all partitions during next step
	 */
	bool joinAllPartitionsInNextStep;
	bool splitAllPartitionsInNextStep;

#if COMPILE_SIMULATION_WITH_GUI
	TTsunamiVertexScalar visualization_water_surface_default_displacement;
	TTsunamiVertexScalar visualization_water_surface_scale_factor;
	TTsunamiVertexScalar visualization_terrain_default_displacement;
	TTsunamiVertexScalar visualization_terrain_scale_factor;
#endif

#if SIMULATION_TSUNAMI_PARALLEL
	virtual void updateSubpartitionParameters() = 0;
#endif

	virtual void reset_Simulation() = 0;


public:
	CTsunamiSimulationParameters()	:
		run_simulation(true),
		simulation_reset(false),
		grid_initial_recursion_depth(6),
		grid_min_relative_recursion_depth(0),
		grid_max_relative_recursion_depth(6),

		adaptive_timestep_size(true),

		simulation_parameter_domain_length(5000.0),
		simulation_parameter_root_partitions_per_x_axis_on_unit_domain(1),

		simulation_parameter_gravitation(9.81),
		simulation_parameter_cfl(SIMULATION_TSUNAMI_CFL),
		simulation_parameter_timestep_size(-1),


		number_of_triangles(0),
		number_of_initial_triangles_after_domain_triangulation(0),
		number_of_simulation_handler_partitions(0),

		simulation_number_of_threads(-1),

		splitting_partition_split_workload_size(512),
		splitting_partition_join_workload_size(splitting_partition_split_workload_size/2),

		partition_update_split_join_size_after_elapsed_timesteps(0),
		partition_update_split_join_size_after_elapsed_scalar(0.0),
		simulation_world_scene_id(3),
		simulation_terrain_scene_id(0),
		simulation_water_surface_scene_id(0),

		simulation_random_raindrops_activated(false),

		simulation_dataset_cylinder_posx(-0.5),
		simulation_dataset_cylinder_posy(0.4),
		simulation_dataset_cylinder_radius(0.2),
		simulation_dataset_cylinder_height(1.0),

		simulation_dataset_terrain_default_distance(300),
		simulation_dataset_water_surface_default_displacement(0),

		joinAllPartitionsInNextStep(false),
		splitAllPartitionsInNextStep(false)

#if COMPILE_SIMULATION_WITH_GUI
		,
		visualization_water_surface_default_displacement(0),
		visualization_water_surface_scale_factor((TTsunamiDataScalar)0.3),
		visualization_terrain_default_displacement(0),
		visualization_terrain_scale_factor((TTsunamiDataScalar)1.0/simulation_dataset_terrain_default_distance)
#endif
	{

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		element_data_setup.dofs.h = simulation_dataset_terrain_default_distance;
		element_data_setup.dofs.qx = 0;
		element_data_setup.dofs.qy = 0;
		element_data_setup.dofs.b = -simulation_dataset_terrain_default_distance;

		dirichlet_boundary_edge_data.h = simulation_dataset_terrain_default_distance;
		dirichlet_boundary_edge_data.qx = 0;
		dirichlet_boundary_edge_data.qy = 0;
		dirichlet_boundary_edge_data.b = -simulation_dataset_terrain_default_distance;

#else

		element_data_setup.hyp_edge.h = simulation_dataset_terrain_default_distance;
		element_data_setup.hyp_edge.qx = 0;
		element_data_setup.hyp_edge.qy = 0;
		element_data_setup.hyp_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_setup.left_edge.h = simulation_dataset_terrain_default_distance;
		element_data_setup.left_edge.qx = 0;
		element_data_setup.left_edge.qy = 0;
		element_data_setup.left_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_setup.right_edge.h = simulation_dataset_terrain_default_distance;
		element_data_setup.right_edge.qx = 0;
		element_data_setup.right_edge.qy = 0;
		element_data_setup.right_edge.b = -simulation_dataset_terrain_default_distance;

		dirichlet_boundary_edge_data.h = simulation_dataset_terrain_default_distance;
		dirichlet_boundary_edge_data.qx = 0;
		dirichlet_boundary_edge_data.qy = 0;
		dirichlet_boundary_edge_data.b = -simulation_dataset_terrain_default_distance;
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		element_data_setup.refine = false;
		element_data_setup.coarsen = false;
#endif

		/*
		 * adaptive stuff
		 */

		/*
		 * surface height
		 */
		coarsen_height_threshold = 0.01;
		refine_height_threshold = 0.02;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		coarsen_slope_threshold = -1;
		refine_slope_threshold = -1;
#else
		/*
		 * threshold for 1st order is given by surface slope
		 */
		TTsunamiDataScalar unit_slope = CMath::abs((TTsunamiDataScalar)1.0/simulation_dataset_water_surface_default_displacement);
		coarsen_slope_threshold	= 0.002*unit_slope;
		refine_slope_threshold	= 0.003*unit_slope;
#endif

		resetSimulationParameters();
	}


#if COMPILE_SIMULATION_WITH_GUI

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigSimulation(
		CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		/************************
		 * SIMULATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- SIMULATION CONTROL ---|"));
		io_cGlHudConfig.insert(o.setupBoolean("Run simulation", &run_simulation));
		io_cGlHudConfig.insert(o.setupBoolean("Reset simulation fluid", &simulation_reset));
		io_cGlHudConfig.insert(o.setupBoolean("Insert Random Raindrops", &simulation_random_raindrops_activated));

		/**
		 * Tsunami simulation parameters
		 */
		io_cGlHudConfig.insert(o.setupText("|--- Setup / Dataset parameters ---|"));
		io_cGlHudConfig.insert(o.setupInt("World scene", &simulation_world_scene_id, 1, -999, 999));
		io_cGlHudConfig.insert(o.setupInt("Terrain scene", &simulation_terrain_scene_id, 1, -100, 100));
		io_cGlHudConfig.insert(o.setupInt("Water height scene", &simulation_water_surface_scene_id, 1, -100, 100));

		io_cGlHudConfig.insert(o.setupText("|--- EdgeComm parameters ---|"));
		io_cGlHudConfig.insert(o.setupFloat("Gravitation length (TODO): ", &simulation_parameter_gravitation, 0.1, -100, 0));
		io_cGlHudConfig.insert(o.setupFloatHI("Timestep size: ", &simulation_parameter_timestep_size, 0.01));
		io_cGlHudConfig.insert(o.setupFloatHI("CFL: ", &simulation_parameter_cfl, 0.001, 0, 1));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain length: ", &simulation_parameter_domain_length, 0.01, 0, CMath::numeric_inf<float>()));



		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- DATASET PARAMETERS ---|"));
		io_cGlHudConfig.insert(o.setupText("Generic Parameters:"));
		io_cGlHudConfig.insert(o.setupFloatHI(" + Bathymetry default distance: ", &simulation_dataset_terrain_default_distance, 0.1, -CMath::numeric_max<float>(), CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Water surface default height: ", &simulation_dataset_water_surface_default_displacement, 0.01, -CMath::numeric_max<float>(), CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupText("Surface setup for column (1):"));
		io_cGlHudConfig.insert(o.setupFloat(" + X-Position: ", &simulation_dataset_cylinder_posx, 0.02, -CMath::numeric_max<float>(), CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Y-Position: ", &simulation_dataset_cylinder_posy, 0.02, -CMath::numeric_max<float>(), CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloatHI(" + Radius: ", &simulation_dataset_cylinder_radius, 0.02, 0.000001, CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloat(" + Height: ", &simulation_dataset_cylinder_height, 0.02, -CMath::numeric_max<float>(), CMath::numeric_max<float>()));

		/************************
		 * Coarsen/Refine
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- ADAPTIVE PARAMETERS ---|"));
		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen height threshold", &coarsen_height_threshold, 0.02, 0.0000001, CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine height threshold", &refine_height_threshold, 0.02, 0.0000001, CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen slope threshold (TODO)", &coarsen_slope_threshold, 0.02, 0.0000001, CMath::numeric_max<float>()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine slope threshold (TODO)", &refine_slope_threshold, 0.02, 0.0000001, CMath::numeric_max<float>()));



		/************************
		 * PARALLELIZATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- PARALLELIZATION ---|"));
		io_cGlHudConfig.insert(o.setupUInt("Split Elements", &splitting_partition_split_workload_size, 1, 2, CMath::numeric_max<int>()));
		io_cGlHudConfig.insert(o.setupUInt("Join Elements", &splitting_partition_join_workload_size, 1, 0, CMath::numeric_max<int>()));
		io_cGlHudConfig.insert(o.setupInt("Number of Threads to use", &simulation_number_of_threads, 1, 1, CMath::numeric_max<int>()));
	}

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigVisualization(
		CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- Water Vis. CONTROL ---|"));

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("GENERAL"));
		io_cGlHudConfig.insert(o.setupFloat("Water surface translation: ", &visualization_water_surface_default_displacement, 0.1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Water surface scale: ", &visualization_water_surface_scale_factor, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloat("Terrain surface translation: ", &visualization_terrain_default_displacement, 0.1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Terrain surface scale: ", &visualization_terrain_scale_factor, 0.01, -999999, 999999));
	}

	/**
	 * setup callback handlers
	 */
	void setupCGuiCallbackHandlers(
			CGuiConfig &cGuiConfig
	)
	{
#if SIMULATION_TSUNAMI_PARALLEL
		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.updateSubpartitionParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_domain_length);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_timestep_size);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_gravitation);
		}

		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.adaptive_timestep_size = false;
				c.updateSubpartitionParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_timestep_size);
		}
#endif

		{
			CGUICONFIG_CALLBACK_START(CTsunamiSimulationParameters);
				c.reset_Simulation();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_world_scene_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_terrain_scene_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_water_surface_scene_id);
		}

	}
#endif


public:
	inline void outputVerboseInformation()
	{
		std::cout << " + Flux solver: " << SIMULATION_TSUNAMI_FLUX_SOLVER_STRING << std::endl;
		std::cout << " + CFL: " << SIMULATION_TSUNAMI_CFL << std::endl;
		std::cout << " + RK ORDER: " << SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER << std::endl;
		std::cout << " + Order of basis functions: " << SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Adaptive conforming subpartition skipping: ";

#if ADAPTIVE_CONFORMING_SUBPARTITION_SKIPPING_ACTIVE
		std::cout << " [ACTIVE]" << std::endl;
#else
		std::cout << " [DEACTIVATED]" << std::endl;
#endif
	}


public:
	void resetSimulationParameters()
	{
		timestep_nr = 0;
		time_for_timestep = 0;
	}

};

#endif
