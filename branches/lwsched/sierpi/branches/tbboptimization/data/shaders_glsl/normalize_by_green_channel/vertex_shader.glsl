// use opengl 3.2 core version!
#version 150

in vec4 vertex_position;

void main(void)
{
	gl_Position = vertex_position;
}
