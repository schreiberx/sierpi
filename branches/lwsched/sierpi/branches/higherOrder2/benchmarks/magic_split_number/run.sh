#! /bin/bash

. inc_parameters.sh
. ../inc_environment_vars.sh

INITIAL_DEPTH=12

for t in $THREADS; do
	echo
	echo "Threads: $THREAD"
	echo "THREADS	MAGIC_NUMBER	MTPS	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"

	for m in `seq 1 26`; do
		PARAMS_="-d $INITIAL_DEPTH -n $t -u 10 -U $m $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT="`$EXEC_CMD`"

		MTPS=`getValueFromString "$OUTPUT" "MTPS"`
		AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
		AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`

		echo "$t	$m	$MTPS	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
	done
done
