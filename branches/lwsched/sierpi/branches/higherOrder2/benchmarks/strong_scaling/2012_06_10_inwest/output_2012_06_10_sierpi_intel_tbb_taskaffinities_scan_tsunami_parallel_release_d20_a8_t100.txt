Executable + params: ../../build/sierpi_intel_tbb_taskaffinities_scan_tsunami_parallel_release -o 32768 -A 1 -d 20 -a 8 -t 100
Problem size with different number of CPUs using function optimization
THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS
80	64.2331	100	0.000491318		1.9047e+07	1760.14
64	55.2973	100	0.000491318		1.9047e+07	1445.57
40	56.245	100	0.000491318		1.9047e+07	963.71
32	39.9956	100	0.000491318		1.9047e+07	773.72
16	22.1968	100	0.000491318		1.9047e+07	433.08
8	12.2631	100	0.000491318		1.9047e+07	236.11
4	8.7927	100	0.000491318		1.9047e+07	133.26
2	4.77741	100	0.000491318		1.9047e+07	75.89
1	2.43608	100	0.000491318		1.9047e+07	48
