#! /bin/bash

cd ../../

make clean

scons --compiler=intel --enable-gui=off --mode=release --threading=omp --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --enable-scan-split-and-join=on && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --enable-scan-split-and-join=on --enable-tbb-task-affinities=on && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --enable-scan-split-and-join=on --enable-tbb-task-affinities=on --skip-adaptive-conforming-sub-partitions=on && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --enable-scan-split-and-join=on --skip-adaptive-conforming-sub-partitions=on && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --skip-adaptive-conforming-sub-partitions=on && \
scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-bathymetry-kernels=on --tsunami-order-of-basis-functions=1 --enable-force-scan-traversal=on
