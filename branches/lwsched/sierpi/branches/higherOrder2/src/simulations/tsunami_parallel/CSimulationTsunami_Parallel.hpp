/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_TSUNAMI_PARALLEL_HPP_
#define CSIMULATION_TSUNAMI_PARALLEL_HPP_


#include "../tsunami_common/tsunami_config.h"

#include "CSimulationTsunami_Parallel_Cluster.hpp"
#include "CSimulationTsunami_DomainSetups.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"
#include "libmath/CVector.hpp"
#include "lib/CStopwatch.hpp"


#include "libsierpi/parallelization/CStackAccessorMethods.hpp"
#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelAdaptivityTraversals.hpp"

#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER==2
	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals_RK2.hpp"
#else
	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals.hpp"
#endif


#include "libsierpi/domain_triangulation/CBaseTriangulation_To_GenericTree.hpp"

#include "libsierpi/parallelization/CSplitJoinTuning.hpp"

#include "CSplitJoin_TsunamiTuningTable.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "libgl/shaders/shader_blinn/CShaderBlinn.hpp"
	#include "libgl/shaders/shader_height_color_blinn/CShaderHeightColorBlinn.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Element_Splats_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Root_Tsunami.hpp"

	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"
#endif

#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"
#include "../tsunami_common/kernels/backends/CGetDataSample_Vertices_Element_Tsunami.hpp"

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/types/CTsunamiTypes.hpp"
	#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif

#include "../tsunami_common/CTsunamiSimulationDataSets.hpp"

#include "../tsunami_common/kernels/modifiers/CSetup_ElementData.hpp"

#include "lib/CLittleBigEndian.hpp"

/**
 * \brief Main class for parallel Tsunami Simulation
 *
 * This class is the central point of a parallel Tsunami simulation.
 *
 * sets up the simulation.
 * It manages all sub-partitions, creates the initial domain triangulation and
 */
class CSimulationTsunami_Parallel	: public CTsunamiSimulationParameters
{
	/**
	 * Convenient typedefs
	 */
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_Cluster> CPartition_TreeNode_;

	/*
	 * Typedefs. among others used by partition handler
	 */
	typedef CGenericTreeNode<CPartition_TreeNode_> CGenericTreeNode_;

public:
	/**
	 * Base domain triangulation
	 */
	CDomain_BaseTriangulation<CSimulationTsunami_Parallel_Cluster> cDomain_BaseTriangulation;

	/**
	 * Pointer to root generic node
	 */
	CGenericTreeNode_ *rootGenericTreeNode;

	/**
	 * heper value to compute CFL
	 */
	TTsunamiDataScalar cfl_helper_reduce_value;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Element_Splats_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Element_Splats_Root_Tsunami;
	COpenGL_Vertices_Element_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Element_Root_Tsunami;
	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;

	CGlVertexArrayObject render_partitions_vao;
	CGlBuffer render_partitions_buffer;
#endif


	/**
	 * tuning for the split and join parameters
	 */
	CSplitJoinTuning<CSplitJoin_TsunamiTuningTable> cSplitJoinTuning;


	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CTsunamiSimulationDataSets cTsunamiSimulationDataSets;



	/**
	 * number of triangles stored during scan in sub-partition tree valid?
	 */
#if DEBUG && CONFIG_ENABLE_SCAN_DATA
	bool cluster_workload_scans_valid;
#endif

	/**
	 * constructor for parallel tsunami simulation
	 */
	CSimulationTsunami_Parallel(
			int i_verbosity_level
	)	:
		rootGenericTreeNode(nullptr),
		cTsunamiSimulationDataSets((CTsunamiSimulationParameters&)*this, i_verbosity_level)
	{
		verbosity_level = i_verbosity_level;
		number_of_triangles = 0;
		number_of_initial_triangles_after_domain_triangulation = 0;
#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}


	/**
	 * Deconstructor
	 */
	virtual ~CSimulationTsunami_Parallel()
	{
		// free generic tree
		freeGenericTreeNode();
	}


	/**
	 * free generic tree node data
	 */
	void freeGenericTreeNode()
	{
		/**
		 * free generic tree
		 */
		if (rootGenericTreeNode != nullptr)
		{
			rootGenericTreeNode->freeChildren();

			delete rootGenericTreeNode;
			rootGenericTreeNode = nullptr;
		}
	}


#if CONFIG_ENABLE_MPI

	inline bool mpi_globalLoopShouldContinue(bool i_should_continue_loop)
	{
		// request break statement
		char r_input = i_should_continue_loop;
		char r_output;
		MPI_Allreduce(&r_input, &r_output, 1, MPI_BYTE, MPI_BOR, MPI_COMM_WORLD);
		return r_output;
	}


	inline void mpi_debugBarrier()
	{
#if DEBUG
		MPI_Barrier(MPI_COMM_WORLD);
#endif
	}

#endif


	/**
	 * Reset the simulation
	 */
	void reset_Simulation()
	{
		number_of_triangles = 0;

		freeGenericTreeNode();

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): setupTsunamiSimulationDataSets" << std::endl;

		cTsunamiSimulationDataSets.setupTsunamiSimulationDataSets();

		/*
		 * reset the world: setup triangulation of "scene"
		 */

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): p_setup_World_PartitionTreeNodes_SimulationPartitionHandlers" << std::endl;

		p_setup_World_PartitionTreeNodes_SimulationPartitionHandlers(simulation_world_scene_id);

		simulation_parameter_timestep_size = simulation_parameter_minimum_timestep_size;

		cfl_helper_reduce_value = 0;

		/***************************************************************************************
		 * STACKS: setup the stacks (only the memory allocation) of the partitions
		 ***************************************************************************************/
		unsigned int stackInitializationFlags =
					CSimulationStacks_Enums::ELEMENT_STACKS							|
					CSimulationStacks_Enums::ADAPTIVE_STACKS						|
					CSimulationStacks_Enums::EDGE_COMM_STACKS						|
					CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS;

#if COMPILE_SIMULATION_WITH_GUI
		stackInitializationFlags |=
					CSimulationStacks_Enums::VERTEX_COMM_STACKS						|
					CSimulationStacks_Enums::VERTEX_COMM_PARALLEL_EXCHANGE_STACKS;
#endif

		/***************************************************************************************
		 * SETUP STRUCTURE STACK and ELEMENT DATA
		 ***************************************************************************************/

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): resetStacks / setupStacks" << std::endl;

		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_node)
			{
				// setup structure stack and element data to default values
				i_node->cPartition_TreeNode->resetStacks(stackInitializationFlags, grid_initial_recursion_depth);

				sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;
				cSetup_Structure_ElementData.setup(i_node->cPartition_TreeNode->cStacks, grid_initial_recursion_depth, &element_data_setup);
			}
		);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		/***************************************************************************************
		 * SETUP vertex coordinates in element data for debugging purposes
		 ***************************************************************************************/
		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): setup validation" << std::endl;

		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				i_cGenericTreeNode->cPartition_TreeNode->cCluster->cSetup_TsunamiElementData_Validation.action(i_cGenericTreeNode->cPartition_TreeNode->cStacks);
			}
		);
#endif

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): updateClusterParameters" << std::endl;

		updateClusterParameters();

#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Element_Splats_Root_Tsunami.setupTexture(512, 512);
		cOpenGL_Element_Splats_Root_Tsunami.setupRegion(cDomain_BaseTriangulation.region);

		if (cOpenGL_Element_Splats_Root_Tsunami.error())
		{
			std::cout << cOpenGL_Element_Splats_Root_Tsunami.error << std::endl;
		}

		render_partitions_vao.bind();
			render_partitions_buffer.bind();
			render_partitions_buffer.resize(6*3*sizeof(GLfloat));

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);
		render_partitions_vao.unbind();
#endif

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): reset_simulation_parameters" << std::endl;

		reset_simulation_parameters();

#if CONFIG_ENABLE_SCAN_DATA

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): setup_ScanDatasets" << std::endl;

		setup_ScanDatasets();

#else

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): p_simulation_cluster_split_and_join" << std::endl;

		p_simulation_cluster_split_and_join();

#endif

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (reset_Simulation): DONE" << std::endl;
	}


private:
	/**
	 * Setup:
	 *  - new world triangulation
	 *  - root partition tree nodes
	 *  - simulation partition handlers
	 */
	void p_setup_World_PartitionTreeNodes_SimulationPartitionHandlers(
			int world_id = 0	///< world id to set-up
	)
	{
		cDomain_BaseTriangulation.clear();

		bool valid_dm_triangulation = false;
		TTsunamiDataScalar tmp;

		switch(world_id)
		{
			case -5:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_3_triangles.svg", 1.0);
				valid_dm_triangulation = false;
				break;

			case -4:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_2DCube_PeriodicBoundaries(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case -3:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_small_even_and_odd.svg", 0.5);
				valid_dm_triangulation = false;
				break;

			case -2:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_Strip_PeriodicBoundaries(cDomain_BaseTriangulation, 10);
				valid_dm_triangulation = true;
				break;

			case -1:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_Quad_PeriodicBoundaries(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case 0:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_Triangle(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case 1:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles1(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case 2:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles2(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case 3:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_Quad(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;

			case 4:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_2EvenTriangles1(cDomain_BaseTriangulation);
				valid_dm_triangulation = false;
				break;

			case 5:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_2EvenTriangles1(cDomain_BaseTriangulation);
				valid_dm_triangulation = false;
				break;

			case 7:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_3Triangles(cDomain_BaseTriangulation);
				valid_dm_triangulation = false;
				break;

			case 8:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_QuadTiles(cDomain_BaseTriangulation, 1, 1);
				valid_dm_triangulation = false;
				break;

			case 10:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_small.svg", 0.5);
				valid_dm_triangulation = false;
				break;

			case 11:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_small_odd.svg", 0.5);
				valid_dm_triangulation = false;
				break;

			case 12:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain.svg");
				valid_dm_triangulation = false;
				break;

			case 13:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_triangle.svg", 2.0);
				valid_dm_triangulation = false;
				break;

			case 14:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_BaseTriangulation, DOMAIN_FILE_PATH"test_domain_cross.svg", 0.5);
				valid_dm_triangulation = false;
				break;

			default:
				tmp = CSimulationTsunami_DomainSetups::setupTriangulation_Triangle(cDomain_BaseTriangulation);
				valid_dm_triangulation = true;
				break;
		}

		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = tmp;

		if (verbosity_level >= 99)
			std::cout << "Valid DM triangulation: " << valid_dm_triangulation << std::endl;

		/*
		 * remove triangles which are available for domain triangulations with odd depth
		 */
#if !CONFIG_ENABLE_MPI

		if (grid_initial_recursion_depth & 1)
			cDomain_BaseTriangulation.removeInvalidCluster();

#else

		if (!valid_dm_triangulation)
		{
			std::cerr << "ERROR: Triangulation is not valid for MPI usage!!! Setting up alternative domain!!!" << std::endl;
			cDomain_BaseTriangulation.clear();
		}

		// always reduce base triangulation to consist either of even or odd base triangles
		// IMPORTANT: dont remove this method since it sets a flag
		cDomain_BaseTriangulation.removeInvalidCluster();

#endif


		/*
		 * setup the adjacency informations
		 */
		cDomain_BaseTriangulation.setup_AdjacencyInformation();


		/*
		 * TODO: FURTHER SPLITTING HERE
		 */

#if CONFIG_ENABLE_MPI

		/*
		 * setup mpi enumeration
		 */
		cDomain_BaseTriangulation.setup_MPINodeEnumeration(simulation_mpi_rank, simulation_mpi_size);

#endif

		/*
		 * convert to generic tree
		 */
		if (rootGenericTreeNode != nullptr)
			delete rootGenericTreeNode;

		CBaseTriangulation_To_GenericTree<CSimulationTsunami_Parallel_Cluster> cBaseTriangulation_To_GenericTree;

		number_of_triangles = cBaseTriangulation_To_GenericTree.setup_GenericTree_From_BaseTriangulation(
				cDomain_BaseTriangulation,				///< root triangles
				grid_initial_recursion_depth,			///< initial recursion depth
				grid_initial_recursion_depth-grid_min_relative_recursion_depth,
				grid_initial_recursion_depth+grid_max_relative_recursion_depth,
				simulation_mpi_rank,
				&rootGenericTreeNode						///< reference to rootNode handed back!
			);

		number_of_simulation_clusters = cBaseTriangulation_To_GenericTree.number_of_initial_root_partitions;
		number_of_initial_triangles_after_domain_triangulation = number_of_triangles;

#if CONFIG_ENABLE_MPI

		if (verbosity_level >= 5)
		{
			for (int i = 0; i < simulation_mpi_size; i++)
			{
				if (i == simulation_mpi_rank)
					output_genericTreeInformation();
				MPI_Barrier(MPI_COMM_WORLD);
			}
		}

#else
		if (verbosity_level >= 5)
			output_genericTreeInformation();
#endif

		assert(rootGenericTreeNode != nullptr);
	}




	/***************************************************************************************
	 * ADAPTIVITY
	 ***************************************************************************************/
private:
	/**
	 * run adaptive traversal
	 */
	void p_simulation_adaptive_traversals()
	{
#if CONFIG_ENABLE_MPI
		mpi_debugBarrier();
#endif


		CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationTsunami_Parallel_Cluster::cTsunami_Adaptive,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				&number_of_triangles,
				&cfl_helper_reduce_value
			);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}


public:
	/**
	 * setup the scan information by running one adaptivity traversal and a split&join traversal
	 */
	void setup_ScanDatasets(int use_number_of_threads = -1)
	{
		p_simulation_adaptive_traversal_create_initial_scan_data();
		p_simulation_cluster_split_and_join(use_number_of_threads);
	}

private:
	void p_simulation_adaptive_traversal_create_initial_scan_data()
	{
		CHelper_GenericParallelAdaptivityTraversals::action<true>(
				&CSimulationTsunami_Parallel_Cluster::cTsunami_Adaptive,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				&number_of_triangles,
				&cfl_helper_reduce_value
			);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}

	long long p_adaptive_traversal_setup_column(
			bool i_split_partitions = true
	)
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel_Scan(
			[=](CGenericTreeNode_ *i_genericTreeNode)
				{
					i_genericTreeNode->cPartition_TreeNode->cCluster->cSetup_Column.setup_KernelClass(
							simulation_dataset_cylinder_posx,
							simulation_dataset_cylinder_posy,
							simulation_dataset_cylinder_radius,

							2,	// REFINE ONLY
							&cTsunamiSimulationDataSets
						);
				}
			);

		long long prev_number_of_triangles;
		long long prev_number_of_simulation_clusters;

		do
		{
			prev_number_of_triangles = number_of_triangles;
			prev_number_of_simulation_clusters = number_of_simulation_clusters;

			CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationTsunami_Parallel_Cluster::cSetup_Column,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				&number_of_triangles,
				&cfl_helper_reduce_value
			);

			if (i_split_partitions)
				splitOrJoinPartitions();

			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_triangles << ", number_of_simulation_clusters: " << number_of_simulation_clusters << std::endl;

		}
		while (
#if !CONFIG_ENABLE_MPI
				prev_number_of_triangles != number_of_triangles	||
				prev_number_of_simulation_clusters != number_of_simulation_clusters
#else
				mpi_globalLoopShouldContinue(
						prev_number_of_triangles != number_of_triangles	||
						prev_number_of_simulation_clusters != number_of_simulation_clusters
				)
#endif
			);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif

		// always run splitOrJoinPartitions in the end to set cluster_workload_scans_valid
		p_simulation_cluster_split_and_join();

		/**
		 * finally setup element data values
		 */
		int backupSetupSurfaceMethod = simulation_water_surface_scene_id;
		simulation_water_surface_scene_id = CTsunamiSimulationDataSets::SIMULATION_WATER_HEIGHT_CYLINDER_OUTER_CYLINDER_MINUS_INF;

		p_setup_initial_grid_data(false);

		simulation_water_surface_scene_id = backupSetupSurfaceMethod;


		return number_of_triangles;
	}



	/**
	 * setup element data specified by simulation_terrain_scene_id and simulation_water_surface_scene_id
	 */
private:
	void p_setup_initial_grid_data(bool i_initial_grid_setup)
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel_Scan(
			[=](CGenericTreeNode_ *i_genericTreeNode)
				{
					// setup element data with respect to vertex positions
					sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
					cSetup_ElementData.setup_sfcMethods(i_genericTreeNode->cPartition_TreeNode->cTriangleFactory);
					cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets, i_initial_grid_setup);
					cSetup_ElementData.action(i_genericTreeNode->cPartition_TreeNode->cStacks);
				}
		);
	}


public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
		if (verbosity_level > 99)
			std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): START" << std::endl;

		long long prev_number_of_triangles;
		long long prev_number_of_simulation_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		TTsunamiDataScalar coarsen_threshold_backup = adaptive_coarsen_threshold;
		adaptive_coarsen_threshold = -9999999;
		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 20;

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement" << std::endl;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_triangles = number_of_triangles;
			prev_number_of_simulation_clusters = number_of_simulation_clusters;

			// setup grid data

			if (verbosity_level > 99)
				std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;
			p_setup_initial_grid_data(true);

			// run single timestep
			if (verbosity_level > 99)
				std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_edge_comm" << std::endl;
			p_simulation_edge_comm();

			// refine / coarsen grid
			if (verbosity_level > 99)
				std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_adaptive_traversals" << std::endl;
			p_simulation_adaptive_traversals();

			// split/join partitions
			if (verbosity_level > 99)
				std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_cluster_split_and_join" << std::endl;
			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_triangles << ", number_of_simulation_clusters: " << number_of_simulation_clusters << std::endl;

#if !CONFIG_ENABLE_MPI

			if (	prev_number_of_triangles == number_of_triangles	&&
					prev_number_of_simulation_clusters == number_of_simulation_clusters
			)
			{
				break;
			}

#else

			if (!mpi_globalLoopShouldContinue(
					prev_number_of_triangles != number_of_triangles	||
					prev_number_of_simulation_clusters != number_of_simulation_clusters)
			)
				break;
#endif
		}

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;

		p_setup_initial_grid_data(true);

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): updateClusterParameters" << std::endl;

		// update cluster parameters
		adaptive_coarsen_threshold = coarsen_threshold_backup;
		updateClusterParameters();


		/*****************************************************
		 * setup initial split of partitions
		 *
		 * initial splitting of partitions should be executed before
		 * setting up column to avoid any preprocessing adaptive effects
		 */
		long long t = 0;

		if (verbosity_level > 99)
			std::cout << "TSUNAMI (setup_GridDataWithAdaptiveSimulation): setup_SplitJoinPartitions" << std::endl;

		do
		{
			t = number_of_simulation_clusters;

#if SIMULATION_TSUNAMI_PARALLEL
			setup_SplitJoinPartitions();
#endif

			if (verbosity_level >= 5)
				std::cout << " + splitted to " << number_of_simulation_clusters << " partitions with " << number_of_triangles << " triangles" << std::endl;
		}
		while (
#if !CONFIG_ENABLE_MPI
				number_of_simulation_clusters != t
#else
				mpi_globalLoopShouldContinue(number_of_simulation_clusters != t)
#endif
		);

		p_adaptive_traversal_setup_column(false);
	}



	/***************************************************************************************
	 * EDGE COMM
	 ***************************************************************************************/
private:
	void p_simulation_edge_comm()
	{
#if CONFIG_ENABLE_MPI
		mpi_debugBarrier();
#endif

		if (simulation_parameter_adaptive_timestep_size_with_cfl)
		{
			// adaptive timestep size
			simulation_parameter_timestep_size = cfl_helper_reduce_value*simulation_parameter_cfl;

			if (simulation_parameter_timestep_size < simulation_parameter_minimum_timestep_size)
			{
				if (simulation_timestep_nr != 0)
				{
					std::cout << "EMERGENCY (simulation probably unstable): fixing time-step size from " << simulation_parameter_timestep_size << " to " << simulation_parameter_minimum_timestep_size << std::endl;
					std::cerr << "EMERGENCY (simulation probably unstable): fixing time-step size from " << simulation_parameter_timestep_size << " to " << simulation_parameter_minimum_timestep_size << std::endl;
				}

				simulation_parameter_timestep_size = simulation_parameter_minimum_timestep_size;
			}
		}

#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 2
		CHelper_GenericParallelFluxCommTraversals_RK2::action(
				&CSimulationTsunami_Parallel_Cluster::cTsunami_EdgeComm,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeFluxCommData_Tsunami,
				rootGenericTreeNode,
				simulation_parameter_timestep_size,
				&cfl_helper_reduce_value
			);

#else

		CHelper_GenericParallelFluxCommTraversals::action(
				&CSimulationTsunami_Parallel_Cluster::cTsunami_EdgeComm,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeFluxCommData_Tsunami,
				rootGenericTreeNode,
				simulation_parameter_timestep_size,
				&cfl_helper_reduce_value
			);
#endif
	}




	/***************************************************************************************
	 * MODIFY_SINGLE_ELEMENT
	 ***************************************************************************************/
	private:
		/**
		 * set element data at coordinate at (x,y) to *elementData
		 */
		void p_set_element_data_at_coordinate(
				TTsunamiVertexScalar x,				///< x-coordinate inside of specific triangle
				TTsunamiVertexScalar y,				///< y-coordinate inside of specific triangle
				CTsunamiElementData *i_elementData	///< set element data at (x,y) to this data
		)
		{
			rootGenericTreeNode->traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_genericTreeNode)
					{
						// We instantiate it right here to avoid any overhead due to split/join operations
						sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

						cModify_OneElementValue_SelectByPoint.setup_sfcMethods(i_genericTreeNode->cPartition_TreeNode->cTriangleFactory);
						cModify_OneElementValue_SelectByPoint.cKernelClass.setup(
								x,
								y,
								i_elementData
							);

						cModify_OneElementValue_SelectByPoint.action(i_genericTreeNode->cPartition_TreeNode->cStacks);
					}
				);
		}



	/***************************************************************************************
	 * UPDATE CLUSTER SIMULATION PARAMETERS
	 *
	 * This has to be executed whenever the simulation parameters are updated
	 ***************************************************************************************/

public:
	void updateClusterParameters()
	{
		/***************************************************************************************
		 * EDGE COMM: setup boundary parameters
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_EdgeComm.setBoundaryCondition((EBoundaryConditions)simulation_domain_boundary_condition);
					i_cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_EdgeComm.setBoundaryDirichlet(&simulation_domain_boundary_dirichlet_edge_data);
				}
			);


		/***************************************************************************************
		 * EDGE COMM: setup generic parameters
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_EdgeComm.setParameters(simulation_parameter_timestep_size, simulation_parameter_domain_length/simulation_parameter_root_partitions_per_x_axis_on_unit_domain, simulation_parameter_gravitation);

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
					i_cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_EdgeComm.setAdaptivityParameters(adaptive_refine_threshold, adaptive_coarsen_threshold);
#endif
				}
			);


		/***************************************************************************************
		 * ADAPTIVITY
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_Adaptive.setup_KernelClass(
							simulation_parameter_domain_length/simulation_parameter_root_partitions_per_x_axis_on_unit_domain,

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
							adaptive_refine_threshold,
							adaptive_coarsen_threshold,

							adaptive_refine_slope_threshold,
							adaptive_coarsen_slope_threshold,
#endif

							&cTsunamiSimulationDataSets
						);
				}
			);
	}




public:
	/**
	 * SPLIT/JOINS for SETUP
	 *
	 * This splits all sub-partitions into appropriate sizes for the initialization.
	 */
	inline void setup_SplitJoinPartitions()
	{
		long long prev_number_of_triangles = 0;

		do
		{
			p_simulation_adaptive_traversals();
			prev_number_of_triangles = number_of_triangles;
		}
		while (
#if !CONFIG_ENABLE_MPI
				prev_number_of_triangles != number_of_triangles
#else
				mpi_globalLoopShouldContinue(prev_number_of_triangles != number_of_triangles)
#endif
		);

		splitOrJoinPartitions();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


private:
	/**
	 * update splitting size when desired
	 */
	inline void p_update_split_join_sizes()
	{
		if (cluster_update_split_join_size_after_elapsed_timesteps != 0)
		{
			if (simulation_timestep_nr % cluster_update_split_join_size_after_elapsed_timesteps == 0)
			{
				if (cluster_update_split_join_size_after_elapsed_scalar != 0)
				{
					/**
					 * compute the split/join sizes by using `partition_update_split_join_size_after_elapsed_scalar`
					 */
					cluster_split_workload_size = std::sqrt((double)number_of_triangles)*cluster_update_split_join_size_after_elapsed_scalar;
					cluster_join_workload_size = cluster_split_workload_size / 2;
				}
				else
				{
					/**
					 * lookup the best split/join in a table
					 */
					cSplitJoinTuning.updateSplitJoin(
							cluster_split_workload_size,
							cluster_join_workload_size,
							number_of_triangles
						);
				}
			}
		}
	}



/***************************************************************************************
 * TIMESTEP
 ***************************************************************************************/

public:
	inline void runSingleTimestep()
	{
		// simulation timestep
		p_simulation_edge_comm();

		// adaptive timestep
		p_simulation_adaptive_traversals();

		if (simulation_timestep_nr % cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations
			p_simulation_cluster_split_and_join();
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_timestep_size;

		p_update_split_join_sizes();
	}


	CStopwatch cStopwatch;

	/***************************************************************************************
	 * run a single TIMESTEP and update the detailed benchmarks
	 ***************************************************************************************/
public:
	/**
	 * execute a single time-step
	 */
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,	///< add time taken for edge communication traversal to this value
			double *io_adaptiveTime,	///< add time taken for adaptive traversal to this value
			double *io_splitJoinTime	///< add time taken for split/joins to this value
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		if (simulation_timestep_nr % cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations
			cStopwatch.start();
			p_simulation_cluster_split_and_join();
			*io_splitJoinTime += cStopwatch.getTimeSinceStart();
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_timestep_size;

		p_update_split_join_sizes();
	}



/***************************************************************************************
 * REFINE / COARSEN
 ***************************************************************************************/

public:
	/**
	 * setup column at 2d position with given radius
	 */
	long long setup_ColumnAt2DPosition(
			TTsunamiVertexScalar i_x,			///< x-coordinate of center of column to set-up
			TTsunamiVertexScalar i_y,			///< y-coordinate of center of column to set-up
			TTsunamiVertexScalar i_radius,		///< radius of column to setup
			bool i_split_partitions = true		///< also split partitions during setup
	)
	{
		simulation_dataset_cylinder_posx = i_x;
		simulation_dataset_cylinder_posy = i_y;
		simulation_dataset_cylinder_radius = i_radius;

		return p_adaptive_traversal_setup_column(i_split_partitions);
	}



	long long setup_ColumnAt2DPosition()
	{
		return p_adaptive_traversal_setup_column(true);
	}



public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,		///< x-coordinate of center of column to set-up
			TTsunamiVertexScalar y,		///< y-coordinate of center of column to set-up
			TTsunamiVertexScalar radius = 0.3		///< radius of column to set-up
	)
	{
		CTsunamiElementData element_data_modifier;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		element_data_modifier.dofs.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.dofs.qx = 0;
		element_data_modifier.dofs.qy = 0;
		element_data_modifier.dofs.b = -simulation_dataset_terrain_default_distance;

#else
		element_data_modifier.hyp_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.hyp_edge.qx = 0;
		element_data_modifier.hyp_edge.qy = 0;
		element_data_modifier.hyp_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.left_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.left_edge.qx = 0;
		element_data_modifier.left_edge.qy = 0;
		element_data_modifier.left_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.right_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.right_edge.qx = 0;
		element_data_modifier.right_edge.qy = 0;
		element_data_modifier.right_edge.b = -simulation_dataset_terrain_default_distance;
#endif

		p_set_element_data_at_coordinate(x, y, &element_data_modifier);
	}



/***************************************************************************************
 * SAMPLE DATA
 ***************************************************************************************/
double getDataSample(
		double i_sample_pos_x,
		double i_sample_pos_y,
		const char *i_sample_information
)
{
	double result = -1.0;

	TTsunamiVertexScalar domain_origin_x = 0;
	TTsunamiVertexScalar domain_origin_y = 0;
	TTsunamiVertexScalar domain_unit_size_x = 0;
	TTsunamiVertexScalar domain_unit_size_y = 0;

	cTsunamiSimulationDataSets.getTerrainOriginAndSize(
			&domain_origin_x,
			&domain_origin_y,
			&domain_unit_size_x,
			&domain_unit_size_y
		);

	/*
	 * map to scale [-1;1]^2 to correct window size
	 */
	domain_unit_size_x *= 0.5;
	domain_origin_x += domain_unit_size_x;
	domain_unit_size_y *= 0.5;
	domain_origin_y += domain_unit_size_y;

	// TRAVERSAL
	rootGenericTreeNode->traverse_GenericTreeNode_Serial(
		[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						domain_origin_x + node->cTriangleFactory.vertices[0][0]*domain_unit_size_x, domain_origin_y + node->cTriangleFactory.vertices[0][1]*domain_unit_size_y,
						domain_origin_x + node->cTriangleFactory.vertices[1][0]*domain_unit_size_x, domain_origin_y + node->cTriangleFactory.vertices[1][1]*domain_unit_size_y,
						domain_origin_x + node->cTriangleFactory.vertices[2][0]*domain_unit_size_x, domain_origin_y + node->cTriangleFactory.vertices[2][1]*domain_unit_size_y,
						i_sample_pos_x,
						i_sample_pos_y
					))
						return;

				// We instantiate it right here to avoid any overhead due to split/join operations
				sierpi::kernels::CGetDataSample_Vertices_Element_Tsunami<CTsunamiSimulationStacks>::TRAV cGetDataSample_Vertices_Element_Tsunami;
				cGetDataSample_Vertices_Element_Tsunami.setup_sfcMethods(i_cGenericTreeNode->cPartition_TreeNode->cTriangleFactory);
				cGetDataSample_Vertices_Element_Tsunami.cKernelClass.setup(
						&result,

						i_sample_information,
						i_sample_pos_x,
						i_sample_pos_y,

						domain_origin_x,
						domain_origin_y,
						domain_unit_size_x,
						domain_unit_size_y
					);
				cGetDataSample_Vertices_Element_Tsunami.action(i_cGenericTreeNode->cPartition_TreeNode->cStacks);
			}
	);

	return result;
}



/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	/**
	 * output currently stored triangles to vtk file
	 */
	void writeTrianglesToVTKFile(
			const char *i_filename,		///< filename to write vtk files to
			bool i_output_binary = true	///< ascii format or binary
	)
	{
		/*
		 * little endian test -> vtk binary files expect data to be written in big-endian format
		 */
		int tmp1 = 1;
		unsigned char *tmp2 = (unsigned char *) &tmp1;
		bool swapLittleToBigEndian = (*tmp2 != 0);


		/**
		 * SURFACE + BATHYMETRY + MOMENTUM
		 */
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(i_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << grid_initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_clusters << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;

		vtkfile << (i_output_binary != true ? "ASCII" : "BINARY") << std::endl;

		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_triangles*3) << " " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;

		TTsunamiVertexScalar origin_x = 0;
		TTsunamiVertexScalar origin_y = 0;
		TTsunamiVertexScalar size_x = 0;
		TTsunamiVertexScalar size_y = 0;

		cTsunamiSimulationDataSets.getTerrainOriginAndSize(
				&origin_x,
				&origin_y,
				&size_x,
				&size_y
			);

		/*
		 * map to [-1;1]^2
		 */
		size_x *= 0.5;
		origin_x += size_x;
		size_y *= 0.5;
		origin_y += size_y;


		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 0>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(i_cGenericTreeNode->cPartition_TreeNode->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);

					cOutputVTK_Vertices_Element_Tsunami.action(i_cGenericTreeNode->cPartition_TreeNode->cStacks);
				}
		);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "POLYGONS " << number_of_triangles << " " << (number_of_triangles*4) << std::endl;

		if (i_output_binary)
		{
			int v[4];
			for (long long i = 0; i < number_of_triangles; i++)
			{
				v[0] = 3;
				CLittleBigEndian::swapBytes(v[0]);
				v[1] = (i*3+0);
				CLittleBigEndian::swapBytes(v[1]);
				v[2] = (i*3+1);
				CLittleBigEndian::swapBytes(v[2]);
				v[3] = (i*3+2);
				CLittleBigEndian::swapBytes(v[3]);

				vtkfile.write((char*)v, sizeof(v[0])*4);
			}
		}
		else
		{
			for (long long i = 0; i < number_of_triangles; i++)
				vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;
		}


		// output height field
		vtkfile << std::endl;
		vtkfile << "CELL_DATA " << number_of_triangles << std::endl;
		vtkfile << "SCALARS h " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 1>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(i_cGenericTreeNode->cPartition_TreeNode->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);

					cOutputVTK_Vertices_Element_Tsunami.action(i_cGenericTreeNode->cPartition_TreeNode->cStacks);
				}
		);


		vtkfile << "SCALARS b " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 4>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(node->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);
					cOutputVTK_Vertices_Element_Tsunami.action(node->cStacks);
				}
		);


#if 1
		vtkfile << "SCALARS hu " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 2>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(i_cGenericTreeNode->cPartition_TreeNode->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);
					cOutputVTK_Vertices_Element_Tsunami.action(i_cGenericTreeNode->cPartition_TreeNode->cStacks);
				}
		);


		vtkfile << "SCALARS hv " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 3>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(node->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);
					cOutputVTK_Vertices_Element_Tsunami.action(node->cStacks);
				}
		);

		vtkfile << "SCALARS cfl_value " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;
		vtkfile << "LOOKUP_TABLE default" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks, 5>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(node->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(
							&vtkfile,
							origin_x,
							origin_y,
							size_x,
							size_y,
							i_output_binary,
							swapLittleToBigEndian
						);
					cOutputVTK_Vertices_Element_Tsunami.action(node->cStacks);
				}
		);
#endif
		vtkfile.close();
	}



	/**
	 * output partitions to vtk file
	 */
	void writeClustersToVTKFile(
			const char *p_filename,	///< filename to write partitions to
			bool i_output_binary = true	///< ascii format or binary
	)
	{
		/*
		 * little endian test -> vtk binary files expect data to be written in big-endian format
		 */
		int tmp1 = 1;
		unsigned char *tmp2 = (unsigned char *) &tmp1;
		bool swapLittleToBigEndian = (*tmp2 != 0);

		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkostream;
		vtkostream.open(p_filename);

		vtkostream << "# vtk DataFile Version 5.0" << std::endl;
		vtkostream << "Sierpi VTK File, " << grid_initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_clusters << "partitions, " << simulation_timestamp_for_timestep << " time";
		if (i_additional_vtk_info_string != NULL)
			vtkostream << ": " << i_additional_vtk_info_string;
		vtkostream << std::endl;
		vtkostream << (i_output_binary != true ? "ASCII" : "BINARY") << std::endl;
		vtkostream << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkostream << "POINTS " << (number_of_simulation_clusters*3) << " " << (sizeof(TTsunamiVertexScalar) == 4 ? "float" : "double") << std::endl;

		TTsunamiVertexScalar origin_x = 0;
		TTsunamiVertexScalar origin_y = 0;
		TTsunamiVertexScalar size_x = 0;
		TTsunamiVertexScalar size_y = 0;

		cTsunamiSimulationDataSets.getTerrainOriginAndSize(
				&origin_x,
				&origin_y,
				&size_x,
				&size_y
			);

		/*
		 * map to [-1;1]^2
		 */
		size_x *= 0.5;
		origin_x += size_x;
		size_y *= 0.5;
		origin_y += size_y;

		if (i_output_binary)
		{
			// TRAVERSAL
			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

						TTsunamiVertexScalar v[3*6];
						v[0*3+0] = node->cTriangleFactory.vertices[0][0]*size_x+origin_x;
						v[0*3+1] = node->cTriangleFactory.vertices[0][1]*size_y+origin_y;
						v[0*3+2] = 0;

						v[1*3+0] = node->cTriangleFactory.vertices[1][0]*size_x+origin_x;
						v[1*3+1] = node->cTriangleFactory.vertices[1][1]*size_y+origin_y;
						v[1*3+2] = 0;

						v[2*3+0] = node->cTriangleFactory.vertices[2][0]*size_x+origin_x;
						v[2*3+1] = node->cTriangleFactory.vertices[2][1]*size_y+origin_y;
						v[2*3+2] = 0;

						if (swapLittleToBigEndian)
						{
							CLittleBigEndian::swapBytes(v[0*3+0]);
							CLittleBigEndian::swapBytes(v[0*3+1]);
							CLittleBigEndian::swapBytes(v[1*3+0]);
							CLittleBigEndian::swapBytes(v[1*3+1]);
							CLittleBigEndian::swapBytes(v[2*3+0]);
							CLittleBigEndian::swapBytes(v[2*3+1]);
						}

						vtkostream.write((char*)v, sizeof(v[0*3+0])*3*3);
					}
			);
		}
		else
		{
			// TRAVERSAL
			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

						vtkostream << (node->cTriangleFactory.vertices[0][0]*size_x+origin_x) << " " << (node->cTriangleFactory.vertices[0][1]*size_y+origin_y) << " " << 0 << std::endl;
						vtkostream << (node->cTriangleFactory.vertices[1][0]*size_x+origin_x) << " " << (node->cTriangleFactory.vertices[1][1]*size_y+origin_y) << " " << 0 << std::endl;
						vtkostream << (node->cTriangleFactory.vertices[2][0]*size_x+origin_x) << " " << (node->cTriangleFactory.vertices[2][1]*size_y+origin_y) << " " << 0 << std::endl;
					}
			);
		}


		// output 3 x #triangles vertices
		vtkostream << std::endl;
		vtkostream << "TRIANGLE_STRIPS " << (number_of_simulation_clusters) << " " << (number_of_simulation_clusters*4) << std::endl;

		if (i_output_binary)
		{
			int v[4];
			for (long long i = 0; i < number_of_simulation_clusters; i++)
			{
				v[0] = 3;
				CLittleBigEndian::swapBytes(v[0]);
				v[1] = (i*3+0);
				CLittleBigEndian::swapBytes(v[1]);
				v[2] = (i*3+1);
				CLittleBigEndian::swapBytes(v[2]);
				v[3] = (i*3+2);
				CLittleBigEndian::swapBytes(v[3]);

				vtkostream.write((char*)v, sizeof(v[0])*4);
			}
		}
		else
		{
			for (long long i = 0; i < number_of_simulation_clusters; i++)
				vtkostream << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;
		}

#if CONFIG_ENABLE_SCAN_DATA

		// output height field
		vtkostream << std::endl;
		vtkostream << "CELL_DATA " << number_of_simulation_clusters << std::endl;
		vtkostream << "SCALARS affinity integer" << std::endl;
		vtkostream << "LOOKUP_TABLE default" << std::endl;

		/*
		 * output affinities
		 */
		if (i_output_binary)
		{

			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						int tid = i_cGenericTreeNode->workload_thread_id;
						CLittleBigEndian::swapBytes(tid);

						vtkostream.write((char*)&tid, sizeof(tid));
					}
			);
		}
		else
		{
			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						vtkostream << i_cGenericTreeNode->workload_thread_id << std::endl;
					}
			);
		}
#endif

		vtkostream.close();
	}


public:
/*****************************************************************************************************************
 * DEBUG: Output
 *****************************************************************************************************************/
	void debugOutputEdgeCommunicationInformation(
			float x,	///< x-coordinate of triangle cell
			float y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "Partition ID: " << node->uniqueId << std::endl;
					std::cout << "Adjacent Communication Information" << std::endl;
					std::cout << node->cEdgeComm_InformationAdjacentClusters << std::endl;
					std::cout << std::endl;
				}
		);
	}


public:
/*****************************************************************************************************************
 * DEBUG: output element data
 *****************************************************************************************************************/
	void debugOutputElementData(
			double i_position_x,		///< x-coordinate of triangle cell
			double i_position_y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel_Scan(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_position_x, i_position_y
						))
							return;

					/// output element data at given point
					sierpi::kernels::CStringOutput_ElementData_Normal_SelectByPoint<CTsunamiSimulationStacks>::TRAV cStringOutput_ElementData_SelectByPoint;

					cStringOutput_ElementData_SelectByPoint.setup_sfcMethods(node->cTriangleFactory);
					cStringOutput_ElementData_SelectByPoint.cKernelClass.setup(i_position_x, i_position_y);
					cStringOutput_ElementData_SelectByPoint.action(node->cStacks);
				}
		);
	}

/***************************************************************************************
 * DEBUG: output information about triangle partition
 ***************************************************************************************/
public:
	void debugOutputPartition(
			float x,	///< x-coordinate of triangle cell
			float y		///< y-coordinate of triangle cell
	)
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << "* Partition information at " << x << ", " << y << ":" << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << std::endl;
					std::cout << *node << std::endl;
					std::cout << node->cPartition_SplitJoinInformation << std::endl;
					std::cout << std::endl;
					std::cout << "min CFL cell_size / max_speed after edge comm: " << node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm << std::endl;
					std::cout << "min CFL cell_size / max_speed after adaptivity: " << node->cCluster->cfl_domain_size_div_max_wave_speed_after_adaptivity << std::endl;
#if 0
					CSimulationTsunami_Parallel_Cluster *partitionHandler = node->cCluster;

					std::cout << std::endl;
					std::cout << "PartitionAndStackInformation:" << std::endl;
					std::cout << "  + Structure Stacks.direction = " << node->cStacks->structure_stacks.direction << std::endl;
					std::cout << "  + Structure Stacks.forward.getStackElementCounter() = " << node->cStacks->structure_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + Structure Stacks.backward.getStackElementCounter() = " << node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + ElementData Stacks.direction = " << node->cStacks->element_data_stacks.direction << std::endl;
					std::cout << "  + ElementData Stacks.forward.getStackElementCounter() = " << node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + ElementData Stacks.backward.getStackElementCounter() = " << node->cStacks->element_data_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + EdgeComm Stacks.left = " << (void*)node->cStacks->edge_data_comm_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm Stacks.right = " << (void*)node->cStacks->edge_data_comm_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.left = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.right = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + splitJoin.elements_in_first_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements << std::endl;
					std::cout << "  + splitJoin.elements_in_second_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements << std::endl;
					std::cout << std::endl;
#endif
				}
		);
	}


/*****************************************************************************************************************
 * SPLIT / JOIN
 *****************************************************************************************************************/


private:
	/**
	 * split or join sub-partitions
	 *
	 * this method may only be invoked after at least one adaptivity traversal to create the scan information
	 *
	 * \return number of sub-partitions
	 */
	inline size_t p_simulation_cluster_split_and_join(
			int i_number_of_threads = -1	///< if != -1, use this value for new number of threads
	)
	{
#if CONFIG_ENABLE_MPI
		return number_of_simulation_clusters;
#endif

		/***************************************************************************************
		 * ACTION: run the split or join operations and update the edge communication information
		 ***************************************************************************************/

		// split/join operations

		// access cTestVariables.allSplit directly within anonymous function does not work.
		// therefore we create a new storage to variables.

		assert(simulation_threading_number_of_threads > 0);

		if (i_number_of_threads != -1)
			simulation_threading_number_of_threads = i_number_of_threads;

		// signed variable necessary!

#if CONFIG_ENABLE_SCAN_DATA

		long long triangles_per_thread = number_of_triangles / simulation_threading_number_of_threads;
		if (triangles_per_thread*simulation_threading_number_of_threads != number_of_triangles)
			triangles_per_thread++;

#endif



		rootGenericTreeNode->traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			[&](CGenericTreeNode_ *cGenericTreeNode)
				{
#if CONFIG_ENABLE_SCAN_DATA
					/*
					 * update workload scan data and thread assignments
					 */
					assert(cGenericTreeNode->workload_in_subtree >= 0);
					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif

					cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::NO_OPERATION;


#if (!CONFIG_ENABLE_SCAN_DATA) || (!CONFIG_ENABLE_SCAN_SPLIT_AND_JOIN_DISTRIBUTION && CONFIG_ENABLE_SCAN_DATA)

					long long number_of_triangles = cGenericTreeNode->workload_in_subtree;

					if (number_of_triangles >= cluster_split_workload_size)
						cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
					else if (number_of_triangles < cluster_join_workload_size)
						cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;

					if (!cGenericTreeNode->cPartition_TreeNode->fun_splitAtLeaves())
						return;

#else

					assert(cGenericTreeNode->workload_scan_start_index >= 0);
					assert(cGenericTreeNode->workload_scan_end_index >= 1);
					assert(cGenericTreeNode->workload_thread_id_end >= cGenericTreeNode->workload_thread_id_start);
					assert(cGenericTreeNode->workload_thread_id_start < simulation_threading_number_of_threads);
					assert(cGenericTreeNode->workload_thread_id_end < simulation_threading_number_of_threads);

					long long number_of_triangles = cGenericTreeNode->workload_in_subtree;

					// distance to start and end of scan partition distribution
					long long dist_start = cGenericTreeNode->workload_scan_start_index % triangles_per_thread;
					long long dist_end = triangles_per_thread - (((cGenericTreeNode->workload_scan_end_index-1) % triangles_per_thread) + 1);

					// half workload in cluster
					long long half_workload = cGenericTreeNode->workload_in_subtree >> 1;
					long long quater_workload = half_workload >> 1;


					/*
					 * check whether the SFC cut is going through the current cluster
					 */

					/*
					 * SPLITS
					 */
#if 0
					if (number_of_triangles > cluster_split_workload_size)
					{
						cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
						goto done;
					}
#endif

//					if (number_of_triangles >= cluster_scan_split_min_workload_size)
					{
						if (cGenericTreeNode->workload_thread_id_start != cGenericTreeNode->workload_thread_id_end)
						{
							cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}

						/*
						 * pre-split to SFC-cut borders
						 */
						if (	quater_workload > dist_start ||
								quater_workload > dist_end
						)
						{
							cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}
					}

					if (number_of_triangles >= cluster_scan_join_max_workload_size)
						goto done;

#if 0
					if (number_of_triangles >= cluster_split_workload_size/2)
					{
						goto done;
					}
#endif

					/*
					 * JOIN
					 */
#if 0
					if (number_of_triangles < cluster_join_workload_size)
					{
						cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
						goto done;
					}
#endif

					if (	dist_start > cGenericTreeNode->workload_in_subtree &&
							dist_end > cGenericTreeNode->workload_in_subtree
					)
					{
						// distance to start and end of scan partition distribution
//						if (dist_start != 0 || dist_end != 0)
							cGenericTreeNode->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
					}


				done:
					if (!cGenericTreeNode->cPartition_TreeNode->fun_splitAtLeaves())
						return;
#endif


					/*
					 * update scan data when node was not split
					 */
#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->first_child_node->updateWorkloadThreadAssignment(triangles_per_thread);
					cGenericTreeNode->second_child_node->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadScanTopDown();

					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_TreeNode_::fun_testAndJoinAtMidNodes(cGenericTreeNode);

#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
			);


#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = true;
#endif

//		output_genericTreeInformation();

		rootGenericTreeNode->traverse_GenericTreeNode_Reduce_Parallel_Scan(
					[&](CGenericTreeNode_ *node, long long *i_reduceValue)
					{
						node->cPartition_TreeNode->cPartition_SplitJoinActions.pass2_updateAdjacentClusterInformation();
						*i_reduceValue = 1;	// one cluster
					},
					&CReduceOperators::ADD<long long>,
					&number_of_simulation_clusters
				);

		auto fun2 = [&](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_SplitJoinActions<CSimulationTsunami_Parallel_Cluster>::pass3_swapAndCleanAfterUpdatingEdgeComm(cGenericTreeNode);
				};

		rootGenericTreeNode->traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
				fun2,
				fun2
			);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA

		/*
		 * validate correct workload scans
		 */
		int scan_start_id = 0;
		int thread_start_id = 0;
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *node)
				{
					assert(node->workload_scan_start_index >= 0);
					assert(node->workload_scan_end_index >= 0);

					assert (node->workload_scan_start_index == scan_start_id);
					scan_start_id = node->workload_scan_end_index;


					if (	node->workload_thread_id_start != thread_start_id &&
							node->workload_thread_id_start != thread_start_id+1
							)
					{
						std::cout << node->workload_thread_id_start << std::endl;
						std::cout << node->workload_thread_id_start << std::endl;
						assert(false);
					}
					thread_start_id = node->workload_thread_id_end;
				}
			);

#endif

		return number_of_simulation_clusters;
	}


public:
	size_t splitOrJoinPartitions()
	{
		return p_simulation_cluster_split_and_join();
	}


public:
	void splitAndJoinRandomized()
	{
		if ((rand() & 1) == 0)
		{
			splitOrJoinPartitionRandomNTimes(10);
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}



	void markClusterForSplitting(CVector<2,float> pos)
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
						node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
						node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
						pos.data[0], pos.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
			}
		);
	}



	void splitPartition(
			const CVector<2,float> &planePosition
	)
	{
		markClusterForSplitting(planePosition);
		splitOrJoinPartitions();
	}



	void markClusterForJoining(
			const CVector<2,float> &planePosition
	)
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
						node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
						node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
						planePosition.data[0], planePosition.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
			}
		);
	}



	/**
	 * ACTION: random split and join partition at random positions
	 */
	void splitOrJoinPartitionRandomNTimes(
			const int i_count
	)
	{
		for (int i = 0; i < i_count; i++)
		{
			rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					int r = random() % 6;

					if (r == 0)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
					}
					else if (r < 6)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
					}
				}
			);
		}

		splitOrJoinPartitions();
	}


	/**
	 * output verbose information
	 */
	void output_bogusData()
	{
		if (dart_samplings_size > 0)
		{
			for (int i = 0; i < dart_samplings_size; i++)
			{
				double sampleValue = getDataSample(dart_sample_points[i].position_x, dart_sample_points[i].position_y, "h+b");

				if (dart_sample_points[i].output_file.empty())
				{
					std::cout << simulation_timestamp_for_timestep << ": " << sampleValue << std::endl;
				}
				else
				{
					std::ofstream s;

					if (simulation_timestep_nr == 0)
						s.open(dart_sample_points[i].output_file.c_str());
					else
						s.open(dart_sample_points[i].output_file.c_str(), std::ofstream::app);

					s << simulation_timestamp_for_timestep << "\t" << sampleValue << "\t" << number_of_triangles << std::endl;
				}
			}
		}
	}

	/**
	 * output information about the underlying tree
	 */
	void output_genericTreeInformation()
	{
		rootGenericTreeNode->traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					assert(cGenericTreeNode->first_child_node == nullptr);
					assert(cGenericTreeNode->second_child_node == nullptr);
					assert(cGenericTreeNode->cPartition_TreeNode != nullptr);
#endif
					// LEAF
					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;
					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "parent non existing";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first child" : "second child");

					std::cout << "-> LEAF  [" << first_or_second_child << "]: (" << depth;

#if CONFIG_ENABLE_SCAN_DATA
					std::cout << ", workload (start/size/end): " <<
								cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index <<
								" , thread(start/end): " << cGenericTreeNode->workload_thread_id_start << ", " << cGenericTreeNode->workload_thread_id_end <<
								" , thread_id: " << cGenericTreeNode->workload_thread_id;
#endif
					std::cout << ")";

					if (cGenericTreeNode->cPartition_TreeNode == nullptr)
					{
						std::cout << "Empty Leaf (ERROR!)" << std::endl;
						return;
					}

					std::cout << " UniqueId: " << cGenericTreeNode->cPartition_TreeNode->uniqueId;

#if DEBUG
					std::cout << "   timestep size: " << cGenericTreeNode->cPartition_TreeNode->cCluster->cTsunami_EdgeComm.getTimestepSize();
#endif
					std::cout << std::endl;


					std::cout << cGenericTreeNode->cPartition_TreeNode->cTriangleFactory.streamVertices(std::cout) << std::endl;
					std::cout << cGenericTreeNode->cPartition_TreeNode->cEdgeComm_InformationAdjacentClusters << std::endl;;

				},
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					if (cGenericTreeNode->first_child_node)
						assert(cGenericTreeNode->first_child_node->parent_node == cGenericTreeNode);

					if (cGenericTreeNode->second_child_node)
						assert(cGenericTreeNode->second_child_node->parent_node == cGenericTreeNode);
#endif

					// MIDDLE NODE

					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;

					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "parent non existing";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first" : "second");


					std::cout << "| MID NODE [parents " << first_or_second_child << " child]: (" << depth;
#if CONFIG_ENABLE_SCAN_DATA
					std::cout << ", workload (start/size/end): " << cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index;
#endif
					std::cout << ")";


#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
					assert(cGenericTreeNode->cPartition_TreeNode == nullptr);
#else
					std::cout << " UniqueId: " << cGenericTreeNode->cPartition_TreeNode->uniqueId << std::endl;
#endif
					std::cout << std::endl;
				}
			);
	}



/*****************************************************************************************************************
 * GUI STUFF & VISUALIZATION
 *****************************************************************************************************************/

#if COMPILE_SIMULATION_WITH_GUI

public:
	void keydown(int key)
	{
		switch(key)
		{
			case 'z':
				simulation_world_scene_id--;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << simulation_world_scene_id << std::endl;
				break;

			case 'x':
				simulation_world_scene_id++;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << simulation_world_scene_id << std::endl;
				break;

			case 'Z':
				simulation_terrain_scene_id--;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << simulation_terrain_scene_id << std::endl;
				break;

			case 'X':
				simulation_terrain_scene_id++;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << simulation_terrain_scene_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'P':
				output_genericTreeInformation();
				break;

			case 'c':
				p_adaptive_traversal_setup_column();
				break;

			case 'N':
				splitOrJoinPartitions();
				break;


#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			case 'I':
				splitAndJoinRandomized();
				break;


			case 'y':
				splitOrJoinPartitionRandomNTimes(1);
				break;

		}
	}


	void render_surfaceDefault_simple(
			CShaderBlinn &cShaderBlinn,
			int i_mode
	)
	{
		render_surfaceVerticesElement(cShaderBlinn, i_mode);
	}


	void render_surfaceVerticesElement(
			CShaderBlinn &cShaderBlinn,
			int i_mode = 0
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;

				cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(
						visualization_water_surface_default_displacement,
						visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
					);

				cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple;

				cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.cKernelClass.setup(
						visualization_terrain_default_displacement,
						visualization_terrain_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
					);

				cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_smooth(
			CShaderBlinn &cShaderBlinn
	)
	{
		return;
#if CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_Cluster,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
				&CSimulationTsunami_Parallel_Cluster::cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeVertexDataCommData_Terrain,
				rootGenericTreeNode,
				[&](CPartition_TreeNode_ *node)
				{
					cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.cKernelClass.setup(
							visualization_water_surface_default_displacement,
							visualization_water_surface_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);

#if 0
					sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth;

					cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.setup_sfcMethods(node->cTriangleFactory);

					cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.cKernelClass.setup(
							visualization_terrain_default_displacement,
							visualization_terrain_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);

					cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.cKernelClass.setup(
							visualization_terrain_default_displacement, visualization_terrain_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
				);
#endif
			}

		);


		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
#endif
	}


	void render_surfaceWithHeightColors(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;

				cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(
						visualization_terrain_default_displacement,
						visualization_terrain_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
					);

				cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}



	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV cOpenGL_Vertices_Element_Tsunami_simple;

				cOpenGL_Vertices_Element_Tsunami_simple.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(
						visualization_water_surface_default_displacement,
						visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
					);

				cOpenGL_Vertices_Element_Tsunami_simple.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}



	void render_surfaceWireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Wireframe_Tsunami<CTsunamiSimulationTypes>::TRAV cOpenGL_Vertices_Wireframe_Tsunami;

				cOpenGL_Vertices_Wireframe_Tsunami.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Wireframe_Tsunami.cKernelClass.setup(
						visualization_water_surface_default_displacement,
						visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Wireframe_Root_Tsunami
					);

				cOpenGL_Vertices_Wireframe_Tsunami.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}



	void render_surfaceSmooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
	#if CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_Cluster,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
			&CSimulationTsunami_Parallel_Cluster::cOpenGL_Vertices_Smooth_Element_Tsunami,
			&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeVertexDataCommData_WaterSurface,
			rootGenericTreeNode,
			[&](CPartition_TreeNode_ *node)
			{
#if 0
	// TODO
				node->cCluster->cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(
						visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
#endif
				);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	#endif

#else

		cShaderBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationTypes,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;

				cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(
						visualization_water_surface_default_displacement,
						visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Element_Root_Tsunami
					);

				cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();

#endif
	}


	void render_surfaceSmoothWithHeightColors(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
#if CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderColorHeightBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action
		<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_Cluster,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>
		(
				&CSimulationTsunami_Parallel_Cluster::cOpenGL_Vertices_Smooth_Element_Tsunami,
				&CSimulationTsunami_Parallel_Cluster::cPartition_ExchangeVertexDataCommData_WaterSurface,
				rootGenericTreeNode,
				[&](CPartition_TreeNode_ *node)
					{
						node->cCluster->cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(
								visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
								&cOpenGL_Vertices_Element_Root_Tsunami
					);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
#endif
	}



	void render_clusterScans(
			CShaderBlinn &cShaderBlinn
	)
	{
#if CONFIG_ENABLE_SCAN_DATA

		cShaderBlinn.use();

		static const GLfloat ct[7][3] = {
				{1.0, 0.0, 0.0},
				{0.0, 1.0, 0.0},
				{0.0, 0.0, 1.0},
				{1.0, 1.0, 0.0},
				{1.0, 0.0, 1.0},
				{0.0, 1.0, 1.0},
				{1.0, 1.0, 1.0},
		};

		GLSL::vec3 c(0.8, 0.1, 0.1);
		cShaderBlinn.material_ambient_color3_uniform.set(c);
		cShaderBlinn.material_diffuse_color3_uniform.set(c);
		cShaderBlinn.material_specular_color3_uniform.set(c);

		render_partitions_vao.bind();
			render_partitions_buffer.bind();

			CGlBuffer *tmp_render_partitions_buffer = &render_partitions_buffer;

			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;


					GLfloat vertex_buffer[3*3];


					int thread_id;
					if (i_cGenericTreeNode->workload_thread_id == -1)
						thread_id = 6;
					else
						thread_id = (i_cGenericTreeNode->workload_thread_id+1) % 6;

					GLSL::vec3 c(ct[thread_id][0], ct[thread_id][1], ct[thread_id][2]);

					cShaderBlinn.material_ambient_color3_uniform.set(c);
					cShaderBlinn.material_diffuse_color3_uniform.set(c);
					cShaderBlinn.material_specular_color3_uniform.set(c);


					vertex_buffer[0*3+0] = node->cTriangleFactory.vertices[0][0];
					vertex_buffer[0*3+1] = 0.001;
					vertex_buffer[0*3+2] = -node->cTriangleFactory.vertices[0][1];

					vertex_buffer[1*3+0] = node->cTriangleFactory.vertices[1][0];
					vertex_buffer[1*3+1] = 0.001;
					vertex_buffer[1*3+2] = -node->cTriangleFactory.vertices[1][1];

					vertex_buffer[2*3+0] = node->cTriangleFactory.vertices[2][0];
					vertex_buffer[2*3+1] = 0.001;
					vertex_buffer[2*3+2] = -node->cTriangleFactory.vertices[2][1];

					tmp_render_partitions_buffer->subData(0, sizeof(vertex_buffer), vertex_buffer);

					glDrawArrays(GL_TRIANGLES, 0, 3);
				}
			);

			CGlErrorCheck();

		render_partitions_vao.unbind();

		cShaderBlinn.disable();
#endif
	}



	void render_clusterBorders(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		GLSL::vec3 c(0.8, 0.1, 0.1);
		cShaderBlinn.material_ambient_color3_uniform.set(c);
		cShaderBlinn.material_diffuse_color3_uniform.set(c);
		cShaderBlinn.material_specular_color3_uniform.set(c);

		render_partitions_vao.bind();
			render_partitions_buffer.bind();

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

			CGlBuffer *tmp_render_partitions_buffer = &render_partitions_buffer;

			rootGenericTreeNode->traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					GLfloat vertex_buffer[6*3];

					vertex_buffer[0*3+0] = node->cTriangleFactory.vertices[0][0];
					vertex_buffer[0*3+1] = 0;
					vertex_buffer[0*3+2] = -node->cTriangleFactory.vertices[0][1];

					vertex_buffer[1*3+0] = node->cTriangleFactory.vertices[1][0];
					vertex_buffer[1*3+1] = 0;
					vertex_buffer[1*3+2] = -node->cTriangleFactory.vertices[1][1];

					vertex_buffer[2*3+0] = node->cTriangleFactory.vertices[1][0];
					vertex_buffer[2*3+1] = 0;
					vertex_buffer[2*3+2] = -node->cTriangleFactory.vertices[1][1];

					vertex_buffer[3*3+0] = node->cTriangleFactory.vertices[2][0];
					vertex_buffer[3*3+1] = 0;
					vertex_buffer[3*3+2] = -node->cTriangleFactory.vertices[2][1];

					vertex_buffer[4*3+0] = node->cTriangleFactory.vertices[2][0];
					vertex_buffer[4*3+1] = 0;
					vertex_buffer[4*3+2] = -node->cTriangleFactory.vertices[2][1];

					vertex_buffer[5*3+0] = node->cTriangleFactory.vertices[0][0];
					vertex_buffer[5*3+1] = 0;
					vertex_buffer[5*3+2] = -node->cTriangleFactory.vertices[0][1];

					tmp_render_partitions_buffer->subData(0, sizeof(vertex_buffer), vertex_buffer);

					glDrawArrays(GL_LINES, 0, 6);

				}
			);

			CGlErrorCheck();

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

		render_partitions_vao.unbind();

		cShaderBlinn.disable();
	}
#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

	void action_Validation_EdgeCommLength()
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				node->cCluster->cPartition_ExchangeEdgeCommData_Adaptivity.validateCommDataLength();
			}
		);
	}


	void action_Validation_EdgeCommMidpointsAndNormals()	const
	{
		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				CSimulationTsunami_Parallel_Cluster *worker = node->cCluster;

				/*
				 * setup fake validation element data stack
				 */
				if (node->cValidationStacks->element_data_stacks.forward.getNumberOfElementsOnStack() != node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack())
				{
					node->cValidationStacks->element_data_stacks.forward.clear();

					for (unsigned int i = 0; i < node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack(); i++)
					{
						node->cValidationStacks->element_data_stacks.forward.push(CValElementData());
					}
				}

				worker->cEdgeComm_ValidateComm.cKernelClass.noElementDataChecks = true;

				worker->cEdgeComm_ValidateComm.actionFirstPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->element_data_stacks,
						node->cValidationStacks->edge_data_comm_left_edge_stack,
						node->cValidationStacks->edge_data_comm_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);
			}
		);

#if CONFIG_ENABLE_MPI

		/*
		 * TODO: run this on one thread while running the the shared memory communications on another thread
		 */
		rootGenericTreeNode->traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				node->cCluster->cPartitionTree_Node_EdgeComm_Validation.exchangeEdgeCommData_DM_pass1();
			}
		);

#endif

		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				node->cCluster->cPartitionTree_Node_EdgeComm_Validation.exchangeEdgeCommData_SM();
			}
		);

#if CONFIG_ENABLE_MPI

		rootGenericTreeNode->traverse_GenericTreeNode_Serial_Reversed(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				node->cCluster->cPartitionTree_Node_EdgeComm_Validation.exchangeEdgeCommData_DM_pass2();
			}
		);

#endif

		rootGenericTreeNode->traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CPartition_TreeNode_ *node = i_cGenericTreeNode->cPartition_TreeNode;

				CSimulationTsunami_Parallel_Cluster *worker = node->cCluster;

				/*
				 * stacks are cleared here since there may be some data left from the last traversal
				 */
				node->cValidationStacks->edge_data_comm_left_edge_stack.clear();
				node->cValidationStacks->edge_data_comm_right_edge_stack.clear();

				/**
				 * second pass
				 */
				worker->cEdgeComm_ValidateComm.actionSecondPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->element_data_stacks,
						node->cValidationStacks->edge_data_comm_exchange_left_edge_stack,		/// !!! here we use the "exchange stacks"!
						node->cValidationStacks->edge_data_comm_exchange_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);
			}
		);
	}


	void action_Validation()
	{
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
	}

#endif

	void outputVerboseInformation()
	{
		((CTsunamiSimulationParameters&)(*this)).outputVerboseInformation();
		cTsunamiSimulationDataSets.outputVerboseInformation();
	}
};


#endif /* CSIMULATION_TSUNAMI_PARALLEL_HPP_ */
