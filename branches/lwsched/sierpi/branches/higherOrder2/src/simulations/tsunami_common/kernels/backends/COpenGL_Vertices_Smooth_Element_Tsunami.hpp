/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COPENGL_VERTICES_SMOOTH_ELEMENT_TSUNAMI_HPP_
#define COPENGL_VERTICES_SMOOTH_ELEMENT_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "COpenGL_Vertices_Element_Root_Tsunami.hpp"
#include "libsierpi/traversators/vertexData/CTraversator_VertexData_ElementData_Depth.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "../../types/CValidation_VertexData.hpp"
#include "../../types/CTsunamiTypes_VisualizationVertexData.hpp"


namespace sierpi
{
namespace kernels
{


template <
	typename p_CSimulationTypes,
	int p_visualizationType			///< type of visualization
									///< 0: surface
									///< 3: bathymetry
>
class COpenGL_Vertices_Smooth_Element_Tsunami
{
public:
	typedef typename p_CSimulationTypes::TSimulationElementData	TElementData;
	typedef typename p_CSimulationTypes::TVisualizationVertexScalar TVertexScalar;
	typedef typename p_CSimulationTypes::TVisualizationVertexData	TVisualizationVertexData;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexData_ElementData_Depth<COpenGL_Vertices_Smooth_Element_Tsunami<p_CSimulationTypes, p_visualizationType>, CTsunamiSimulationStacks> TRAV;



private:
	GLfloat *vertex_attrib_buffer;
	GLfloat *current_vertex_attrib;

	GLfloat *vertex_attrib_buffer_end;
	size_t max_vertices;

	TVertexScalar scale_min;
	TVertexScalar scale_factor;

	COpenGL_Vertices_Element_Root_Tsunami<TVertexScalar> *cOpenGL_Vertices_Element_Root_Tsunami;


public:
	inline COpenGL_Vertices_Smooth_Element_Tsunami()	:
			max_vertices(COPENGL_VERTICES_ELEMENT_ROOT_TSUNAMI_VERTEX_COUNT)
	{
		// allocate space for VERTICES AND NORMALS!!!
		vertex_attrib_buffer = new GLfloat[3*3*max_vertices*2];
		vertex_attrib_buffer_end = vertex_attrib_buffer+(3*max_vertices*2);
	}


	virtual inline ~COpenGL_Vertices_Smooth_Element_Tsunami()
	{
		delete[] vertex_attrib_buffer;
	}



private:
	inline void p_actionFirstTouchVertexData(int i_depth, TElementData *i_elementData, TVisualizationVertexData *o_vertexData)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		o_vertexData->normal[0] += 0*l;
		o_vertexData->normal[1] += 1*l;
		o_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			o_vertexData->height = (i_elementData->dofs.h+i_elementData->dofs.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = (i_elementData->dofs.b)*l;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = l;

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

		o_vertexData->normal[0] = 0;
		o_vertexData->normal[1] = 1;
		o_vertexData->normal[2] = 0;

		/*
		 * TODO: fix hyp_edge to care about left/right/hyp edge
		 */
		if (p_visualizationType == 0)
		{
			o_vertexData->height = i_elementData->hyp_edge.h+i_elementData->hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = i_elementData->hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;
#endif
	}



public:
	inline void actionFirstTouchVertexData_Left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}

	inline void actionFirstTouchVertexData_Right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}

	inline void actionFirstTouchVertexData_Top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}



private:
	inline void p_normalizeVertexData(
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->normal[0] *= inv_fac;
		io_vertexData->normal[1] *= inv_fac;
		io_vertexData->normal[2] *= inv_fac;

		io_vertexData->height *= inv_fac;
#else
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->normal[0] *= inv_fac;
		io_vertexData->normal[1] *= inv_fac;
		io_vertexData->normal[2] *= inv_fac;

		io_vertexData->height *= inv_fac;
#endif
	}



	/**
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void vertexCommLastJoin(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		std::cout << "vertexCommLastJoin()" << std::endl;
		std::cout << i_vertexData1->validation << std::endl;
//		std::cout << i_vertexData1->normal[0] << " " << i_vertexData1->normal[1] << " " << i_vertexData1->normal[2] << std::endl;
//		std::cout << i_vertexData1->normalization_factor << std::endl;
		std::cout << i_vertexData2->validation << std::endl;
//		std::cout << i_vertexData2->normal[0] << " " << i_vertexData2->normal[1] << " " << i_vertexData2->normal[2] << std::endl;
//		std::cout << i_vertexData2->normalization_factor << std::endl;
		std::cout << std::endl;

		o_vertexData->normal[0] = i_vertexData1->normal[0] + i_vertexData2->normal[0];
		o_vertexData->normal[1] = i_vertexData1->normal[1] + i_vertexData2->normal[1];
		o_vertexData->normal[2] = i_vertexData1->normal[2] + i_vertexData2->normal[2];

		o_vertexData->height = i_vertexData1->height + i_vertexData2->height;

		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeVertexData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}


	/**
	 * final touch
	 */
public:
	inline void vertexCommLastTouch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		std::cout << "LAST TOUCH: " << io_vertexData->validation << std::endl;

		p_normalizeVertexData(io_vertexData);
	}




private:
	inline void p_actionFirstAndLastTouchVertexData(int i_depth, TElementData *i_elementData, TVisualizationVertexData *o_vertexData)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		o_vertexData->normal[0] += 0;
		o_vertexData->normal[1] += 1;
		o_vertexData->normal[2] += 0;

		if (p_visualizationType == 0)
		{
			o_vertexData->height = (i_elementData->dofs.h+i_elementData->dofs.b);
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = (i_elementData->dofs.b);
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

		o_vertexData->normal[0] = 0;
		o_vertexData->normal[1] = 1;
		o_vertexData->normal[2] = 0;

		/*
		 * TODO: fix hyp_edge to care about left/right/hyp edge
		 */
		if (p_visualizationType == 0)
		{
			o_vertexData->height = i_elementData->hyp_edge.h+i_elementData->hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = i_elementData->hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;
#endif
	}



public:
	inline void actionFirstAndLastTouchVertexData_Left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}

	inline void actionFirstAndLastTouchVertexData_Right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}

	inline void actionFirstAndLastTouchVertexData_Top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}



private:
	inline void p_actionMiddleTouchVertexData(int i_depth, TElementData *i_elementData, TVisualizationVertexData *io_vertexData)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		io_vertexData->normal[0] += 0*l;
		io_vertexData->normal[1] += 1*l;
		io_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += (i_elementData->dofs.h+i_elementData->dofs.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_elementData->dofs.b*l;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += l;

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;


		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_elementData->hyp_edge.h+i_elementData->hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_elementData->hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif
	}



public:
	inline void actionMiddleTouchVertexData_Left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	inline void actionMiddleTouchVertexData_Right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	inline void actionMiddleTouchVertexData_Top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


private:
	inline void p_actionLastTouchVertexData(
			int i_depth,
			TElementData *i_elementData,
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		io_vertexData->normal[0] += 0*l;
		io_vertexData->normal[1] += 1*l;
		io_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += (i_elementData->dofs.h+i_elementData->dofs.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += (i_elementData->dofs.b)*l;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += l;

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_elementData->hyp_edge.h+i_elementData->hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_elementData->hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif

		p_normalizeVertexData(io_vertexData);
	}



public:
	inline void actionLastTouchVertexData_Left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	inline void actionLastTouchVertexData_Right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	inline void actionLastTouchVertexData_Top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			TElementData *i_elementData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_elementData, o_vertexData);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	inline void renderOpenGLVertexArray(size_t p_vertices_count)
	{
		assert(cOpenGL_Vertices_Element_Root_Tsunami != nullptr);

		cOpenGL_Vertices_Element_Root_Tsunami->renderOpenGLVertexArray(
				vertex_attrib_buffer,
				p_vertices_count
			);
	}

	inline TTsunamiVertexScalar fixHeight(TTsunamiVertexScalar h)
	{
		return (((h)-scale_min)*scale_factor);
	}

	inline void elementAction(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			int i_depth,
			TElementData *i_element,
			TVisualizationVertexData *i_leftVertexData,
			TVisualizationVertexData *i_rightVertexData,
			TVisualizationVertexData *i_topVertexData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_leftVertexData->validation.testVertex(v0x, v0y);
		i_rightVertexData->validation.testVertex(v1x, v1y);
		i_topVertexData->validation.testVertex(v2x, v2y);
#endif

		current_vertex_attrib[0*3+0] = v0x;
		current_vertex_attrib[0*3+1] = fixHeight(i_leftVertexData->height);
		current_vertex_attrib[0*3+2] = -v0y;

		current_vertex_attrib[1*3+0] = 0;
		current_vertex_attrib[1*3+1] = 1;
		current_vertex_attrib[1*3+2] = 0;

		current_vertex_attrib[2*3+0] = v1x;
		current_vertex_attrib[2*3+1] = fixHeight(i_rightVertexData->height);
		current_vertex_attrib[2*3+2] = -v1y;

		current_vertex_attrib[3*3+0] = 0;
		current_vertex_attrib[3*3+1] = 1;
		current_vertex_attrib[3*3+2] = 0;

		current_vertex_attrib[4*3+0] = v2x;
		current_vertex_attrib[4*3+1] = fixHeight(i_topVertexData->height);
		current_vertex_attrib[4*3+2] = -v2y;

		current_vertex_attrib[5*3+0] = 0;
		current_vertex_attrib[5*3+1] = 1;
		current_vertex_attrib[5*3+2] = 0;

		current_vertex_attrib += 3*3*2;


		assert(current_vertex_attrib <= vertex_attrib_buffer_end);
		if (current_vertex_attrib == vertex_attrib_buffer_end)
		{
			renderOpenGLVertexArray(max_vertices);
			current_vertex_attrib = vertex_attrib_buffer;
		}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_element->validation.testVertices(v0x, v0y, v1x, v1y, v2x, v2y);
#endif
	}

	inline void traversal_pre_hook()
	{
		current_vertex_attrib = vertex_attrib_buffer;
	}

	inline void traversal_post_hook()
	{
		if (current_vertex_attrib > vertex_attrib_buffer)
		{
			// involve stored vertices AND normals in primitive counting
			renderOpenGLVertexArray((size_t)(current_vertex_attrib-vertex_attrib_buffer)/(3*2));
			return;
		}
	}

	/**
	 * setup traversator based on parent triangle
	 */
	inline void setup_WithKernel(
			COpenGL_Vertices_Smooth_Element_Tsunami &parent_kernel
	)
	{
		scale_min = parent_kernel.scale_min;
		scale_factor = parent_kernel.scale_factor;
		cOpenGL_Vertices_Element_Root_Tsunami = parent_kernel.cOpenGL_Vertices_Element_Root_Tsunami;
	}


	void setup(
			TVertexScalar i_scale_min,
			TVertexScalar i_scale_factor,
			COpenGL_Vertices_Element_Root_Tsunami<TVertexScalar> *p_cOpenGL_Vertices_Element_Root_Tsunami
	)
	{
		scale_min = i_scale_min;
		scale_factor = i_scale_factor;
		cOpenGL_Vertices_Element_Root_Tsunami = p_cOpenGL_Vertices_Element_Root_Tsunami;
	}
};


}
}

#endif
