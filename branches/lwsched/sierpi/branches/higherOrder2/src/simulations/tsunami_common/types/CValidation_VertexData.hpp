/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 24, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CTSUNAMI_TYPES_VALIDATE_VERTEX_DATA_HPP
#define CTSUNAMI_TYPES_VALIDATE_VERTEX_DATA_HPP

#include <string.h>

/*
 * validation data for edge data
 */
class CValidation_VertexData
{

public:
	TTsunamiVertexScalar vertex_x;
	TTsunamiVertexScalar vertex_y;

	CValidation_VertexData()	:
		vertex_x(-666),
		vertex_y(-666)
	{
	}


	CValidation_VertexData(
			const CValidation_VertexData &d
	)
	{
		vertex_x = d.vertex_x;
		vertex_y = d.vertex_y;
		return;
	}


	CValidation_VertexData& operator=(
			const CValidation_VertexData &d
	)
	{
		if (d.vertex_x == -666)
			return *this;

		vertex_x = d.vertex_x;
		vertex_y = d.vertex_y;

		return *this;
	}


	void setupVertex(
			TTsunamiVertexScalar i_vertex_x,
			TTsunamiVertexScalar i_vertex_y
	)
	{
		vertex_x = i_vertex_x;
		vertex_y = i_vertex_y;
	}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	void testVertex(
			TTsunamiVertexScalar i_vertex_x,
			TTsunamiVertexScalar i_vertex_y
	)
	{
		assert(i_vertex_x != -666.0);
		assert(i_vertex_y != -666.0);

#if !CONFIG_PERIODIC_BOUNDARIES
		if (i_vertex_x != vertex_x || i_vertex_y != vertex_y)
		{
			std::cout << "Vertex mismatch!" << std::endl;
			std::cout << " +(" << vertex_x << ", " << vertex_y << ")" << std::endl;
			std::cout << " +(" << i_vertex_x << ", " << i_vertex_y << ")" << std::endl;
			assert(false);
		}
#endif
	}

	void testVertex(
			CValidation_VertexData &i_vertexData
	)
	{
		assert(i_vertexData.vertex_x != -666.0);
		assert(i_vertexData.vertex_y != -666.0);

#if !CONFIG_PERIODIC_BOUNDARIES
		if (i_vertexData.vertex_x != vertex_x || i_vertexData.vertex_y != vertex_y)
		{
			std::cout << "Vertex mismatch!" << std::endl;
			std::cout << " + local(" << vertex_x << ", " << vertex_y << ")" << std::endl;
			std::cout << " +   adj(" << i_vertexData.vertex_x << ", " << i_vertexData.vertex_y << ")" << std::endl;
			assert(false);
		}
#endif
	}
#endif

	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CValidation_VertexData &d)
	{
		return os << "(" << d.vertex_x << ", " << d.vertex_y << ")";
	}
};

#endif
