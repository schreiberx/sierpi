/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_1STORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_1STORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_1stOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiEdgeData TEdgeData;


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;

	/**
	 * constructor
	 */
	CAdaptivity_1stOrder()	:
		cathetus_side_length(-1),
		cSimulationDataSets(nullptr),
		cfl_domain_size_div_max_wave_speed(-1)
	{

	}


	void traversal_pre_hook()
	{
//		std::cout << "LKJASLKDFJ" << std::endl;
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}


	/**
	 * get center of weight for triangle
	 */
	inline void computeCenterOfWeight(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);
	}



	/**
	 * get center of weight for triangle for both children
	 */
	inline void computeCenterOfWeightForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif
		// left element
		o_left_elementData->hyp_edge = i_elementData->left_edge;
		o_left_elementData->right_edge = i_elementData->hyp_edge;
		o_left_elementData->left_edge.h = (i_elementData->left_edge.h+i_elementData->right_edge.h)*(T)0.5;
		o_left_elementData->left_edge.qx = (i_elementData->left_edge.qx+i_elementData->right_edge.qx)*(T)0.5;
		o_left_elementData->left_edge.qy = (i_elementData->left_edge.qy+i_elementData->right_edge.qy)*(T)0.5;
		o_left_elementData->left_edge.b = (i_elementData->left_edge.b+i_elementData->right_edge.b)*(T)0.5;

		assert(SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1);
		o_left_elementData->support_points[0] = i_elementData->support_points[2];
		o_left_elementData->support_points[1] = i_elementData->support_points[0];
		o_left_elementData->support_points[2].h = (i_elementData->support_points[1].h + i_elementData->support_points[2].h)*(T)0.5;
		o_left_elementData->support_points[2].qx = (i_elementData->support_points[1].qx + i_elementData->support_points[2].qx)*(T)0.5;
		o_left_elementData->support_points[2].qy = (i_elementData->support_points[1].qy + i_elementData->support_points[2].qy)*(T)0.5;
		o_left_elementData->support_points[2].b = (i_elementData->support_points[1].b + i_elementData->support_points[2].b)*(T)0.5;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_left_elementData->refine = false;
		o_left_elementData->coarsen = false;
#endif

		// right element
		o_right_elementData->hyp_edge = i_elementData->right_edge;
		o_right_elementData->right_edge = o_left_elementData->left_edge;
		o_right_elementData->left_edge = i_elementData->hyp_edge;

		assert(SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1);
		o_right_elementData->support_points[0] = i_elementData->support_points[1];
		o_right_elementData->support_points[1] = o_left_elementData->support_points[2];
		o_right_elementData->support_points[2] = i_elementData->support_points[0];

		assert(cfl_domain_size_div_max_wave_speed != -1);

		T local_cfl_domain_size_div_max_wave_speed = i_elementData->cfl_domain_size_div_max_wave_speed*(T)(1.0/CMath::sqrt(2.0));

		o_left_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_left_elementData->refine = false;
		o_left_elementData->coarsen = false;
		o_right_elementData->refine = false;
		o_right_elementData->coarsen = false;
#endif

		assert(cfl_domain_size_div_max_wave_speed != -1);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				i_depth,
				&o_left_elementData->validation,
				&o_right_elementData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiElementData *o_elementData,
			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			T mx = (vleft_x + vright_x)*(T)0.5;
			T my = (vleft_y + vright_y)*(T)0.5;

			i_left_elementData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_elementData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif

		o_elementData->left_edge = i_left_elementData->hyp_edge;
		o_elementData->right_edge = i_right_elementData->hyp_edge;
		o_elementData->hyp_edge.h = (i_left_elementData->right_edge.h+i_right_elementData->left_edge.h)*(T)0.5;
		o_elementData->hyp_edge.qx = (i_left_elementData->right_edge.qx+i_right_elementData->left_edge.qx)*(T)0.5;
		o_elementData->hyp_edge.qy = (i_left_elementData->right_edge.qy+i_right_elementData->left_edge.qy)*(T)0.5;
		o_elementData->hyp_edge.b = (i_left_elementData->right_edge.b+i_right_elementData->left_edge.b)*(T)0.5;

		assert(SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1);
		for (int i=0; i<3; ++i){
			o_elementData->support_points[i].h = (i_left_elementData->support_points[i].h + i_right_elementData->support_points[i].h)*(T)0.5;
			o_elementData->support_points[i].qx = (i_left_elementData->support_points[i].qx + i_right_elementData->support_points[i].qx)*(T)0.5;
			o_elementData->support_points[i].qy = (i_left_elementData->support_points[i].qy + i_right_elementData->support_points[i].qy)*(T)0.5;
			o_elementData->support_points[i].b = (i_left_elementData->support_points[i].b + i_right_elementData->support_points[i].b)*(T)0.5;
		}

		o_elementData->cfl_domain_size_div_max_wave_speed = (i_left_elementData->cfl_domain_size_div_max_wave_speed + i_right_elementData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*CMath::sqrt(2.0));

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_elementData->refine = false;
		o_elementData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_elementData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_elementData->validation,
				&i_right_elementData->validation
		);
#endif
	}
};


#endif /* CADAPTIVITY_0STORDER_HPP_ */
