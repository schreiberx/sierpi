/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_UniqueId.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_UNIQUEID_HPP_
#define CPARTITION_UNIQUEID_HPP_

#include "lib/intToBinString.hpp"

/**
 * \brief unique id for each partition
 *
 * uniqueIDs:
 * ==========
 *   - those are unique also for sub-triangles.
 *   - to get even unique IDs for the sub-triangles, they are bit-combined.
 *   - the maximum number of bits used for the unique root domain triangles IDs is given in by max_unique_id_depth
 *   - the root unique IDs are initialized with "(1 << max_unique_id_depth) + triangle_enum_id" in the class CDomain_BaseTriangulation
 *   - then during the split operation, the parent's uniqueID is shifted to the left by a single bit and the lowest bit
 *     is set to 0 or 1 to account for first/second child node.
 */
class CPartition_UniqueId
{
public:
	/**
	 * unique id of partition
	 */
	unsigned long long raw_unique_id;

	/**
	 * unique id depth of partition to split up further partitions with an unique id
	 *
	 * if the partition is split up into 2 distinct triangles, their new ids are
	 * created by using the old uniqueId and using a prefix of 0 or 1 of the
	 * uniqueIdDepth-th less significant parent uniqueId
	 */
	unsigned int unique_id_depth;

	/**
	 * constructor
	 */
public:
	CPartition_UniqueId()	:
		raw_unique_id(0),
		unique_id_depth(0)
	{
	}

public:
	void setup(
			unsigned long long i_rawUniqueId,
			unsigned long i_uniqueId_depth
	)
	{
		raw_unique_id = i_rawUniqueId;
		assert(raw_unique_id != 0);
		unique_id_depth = i_uniqueId_depth;
	}

	/**
	 * setup uniqueId by rawUniqueId
	 */
public:
	CPartition_UniqueId(unsigned long long i_rawUniqueId)
	{
		raw_unique_id = i_rawUniqueId;

		assert(raw_unique_id != 0);

		unique_id_depth = 0;
		for (int i = raw_unique_id; i > 0; i >>= 1)
			unique_id_depth++;
	}

	/**
	 * setup uniqueId by other uniqueId
	 */
public:
	CPartition_UniqueId(
			const CPartition_UniqueId &cPartition_UniqueId
	)
	{
		raw_unique_id = cPartition_UniqueId.raw_unique_id;
		unique_id_depth = cPartition_UniqueId.unique_id_depth;
	}

	/**
	 * return the parents raw unique id
	 */
public:
	unsigned int getParentRawUniqueId()	const
	{
		return raw_unique_id >> 1;
	}

	/**
	 * return true, if the uniqueId was initialized
	 */
	bool isValid()	const
	{
		return raw_unique_id != 0;
	}


	/**
	 * setup uniqueId for this parent assuming that i_parent_cPartition_UniqueId
	 * is a child
	 */
public:
	void setupParentFromChild(
			CPartition_UniqueId &i_child_cPartition_UniqueId
	)
	{
		raw_unique_id = (i_child_cPartition_UniqueId.raw_unique_id >> 1);
		unique_id_depth = i_child_cPartition_UniqueId.unique_id_depth-1;
	}



	/**
	 * setup uniqueId for this child assuming that this is the first child
	 * and the uniqueId given as a parameter is the parent's uniqueId
	 */
public:
	void setupFirstChildFromParent(
			CPartition_UniqueId &i_parent_cPartition_UniqueId
	)
	{
		raw_unique_id = i_parent_cPartition_UniqueId.raw_unique_id << 1;
		unique_id_depth = i_parent_cPartition_UniqueId.unique_id_depth+1;
	}



	/**
	 * setup uniqueId for this child assuming that this is the first child
	 * and the uniqueId given as a parameter is the parent's uniqueId
	 */
public:
	void setupSecondChildFromParent(
			CPartition_UniqueId &i_parent_cPartition_UniqueId
	)
	{
		raw_unique_id = (i_parent_cPartition_UniqueId.raw_unique_id << 1) | 1;
		unique_id_depth = i_parent_cPartition_UniqueId.unique_id_depth+1;
	}



	/**
	 * return true if this is the first child
	 */
public:
	bool isFirstChild()	const
	{
		assert(raw_unique_id != 0);

		return (raw_unique_id & 1) == 0;
	}


	/**
	 * return true if this is the second child
	 */
public:
	bool isSecondChild()	const
	{
		assert(raw_unique_id != 0);

		return (raw_unique_id & 1) == 1;
	}

	friend
	inline
	bool operator==(const CPartition_UniqueId &o1, const CPartition_UniqueId &o2)
	{
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);

		return (o1.raw_unique_id == o2.raw_unique_id);
	}


	friend
	inline
	bool operator!=(const CPartition_UniqueId &o1, const CPartition_UniqueId &o2)
	{
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);

		return (o1.raw_unique_id != o2.raw_unique_id);
	}


	friend inline bool operator<(const CPartition_UniqueId &o1, const CPartition_UniqueId &o2)
	{
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);

		unsigned int max_depth = CMath::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 < shifted_o2;
	}


	friend inline bool operator>(const CPartition_UniqueId &o1, const CPartition_UniqueId &o2)
	{
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);

		unsigned int max_depth = CMath::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 > shifted_o2;
	}


	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			const CPartition_UniqueId &p
	)
	{
//		co << "UniqueId: " << p.rawUniqueId << " (" << intToBinString(p.rawUniqueId) << ") | " << p.uniqueId_depth;
		co << "(" << intToBinString(p.raw_unique_id) << " | " << p.raw_unique_id << " | " << p.unique_id_depth << ")";
		return co;
	}
};

#endif /* CPARTITION_UNIQUEID_H_ */
