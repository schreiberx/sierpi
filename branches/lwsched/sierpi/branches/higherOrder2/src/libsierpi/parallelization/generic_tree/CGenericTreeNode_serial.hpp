/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_serial.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_SERIAL_HPP_
#define CGENERICTREENODE_SERIAL_HPP_

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGenericTreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

inline void specializedConstructorMethod()
{
}

#endif

/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE)
 */


template <typename CLambdaFun>
void traverse_GenericTreeNode_Serial(
		CLambdaFun p_lambda
)
{
	class CTraversalTask_GenericTreeNode_Serial
	{
public:
		static void traverse(CGenericTreeNode_ *this_node, CLambdaFun lambda)
		{
			if (this_node->isLeaf())
			{
				lambda(this_node);
				return;
			}

			if (this_node->first_child_node)
				traverse(this_node->first_child_node, lambda);

			if (this_node->second_child_node)
				traverse(this_node->second_child_node, lambda);
		}
	};


	CTraversalTask_GenericTreeNode_Serial::traverse(this, p_lambda);
}



#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	template <typename CLambdaFun>
	void traverse_GenericTreeNode_Parallel(
			CLambdaFun i_lambda
	)
	{
		traverse_GenericTreeNode_Serial<CLambdaFun>(i_lambda);
	}


	template <typename CLambdaFun>
	void traverse_GenericTreeNode_Parallel_Scan(
			CLambdaFun i_lambda
	)
	{
		traverse_GenericTreeNode_Serial<CLambdaFun>(i_lambda);
	}

#endif


/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) in reversed order
 */


template <typename CLambdaFun>
void traverse_GenericTreeNode_Serial_Reversed(
		CLambdaFun p_lambda
)
{
	class CTraversalTask_GenericTreeNode_Serial_Reversed
	{
public:
		static void traverse(CGenericTreeNode_ *this_node, CLambdaFun lambda)
		{
			if (this_node->isLeaf())
			{
				lambda(this_node);
				return;
			}

			if (this_node->second_child_node)
				traverse(this_node->second_child_node, lambda);

			if (this_node->first_child_node)
				traverse(this_node->first_child_node, lambda);
		}
	};

	CTraversalTask_GenericTreeNode_Serial_Reversed::traverse(this, p_lambda);
}



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */

template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial
	{
public:
	public:
		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes_preorder,
				CLambdaFun3 i_lambda_midnodes_postorder
		)
		{
			if (!i_this_node->isLeaf())
			{
				i_lambda_midnodes_preorder(i_this_node);
			}
			else
			{
				i_lambda_leaves(i_this_node);
				return;
			}

//			traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);

			if (i_this_node->first_child_node)
				traverse(i_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);

			if (i_this_node->second_child_node)
				traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);

			i_lambda_midnodes_postorder(i_this_node);

			return;
		}
	};



	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
}


#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	template <typename CLambdaFun1, typename CLambdaFun2, typename CLambdaFun3>
	void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)
	{
		traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Serial<CLambdaFun1, CLambdaFun2, CLambdaFun3>(i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder);
	}


#endif



/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in POSTORDER
 */

template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial
	{
	public:
		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes
		)
		{
			if (i_this_node->isLeaf())
			{
				i_lambda_leaves(i_this_node);
				return;
			}

			if (i_this_node->first_child_node)
				traverse(i_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes);

			if (i_this_node->second_child_node)
				traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes);

			if (i_this_node)
				i_lambda_midnodes(i_this_node);
		}
	};


	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Serial::traverse(this, i_lambda_leaves, i_lambda_midnodes);
}



#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	template <typename CLambdaFun1, typename CLambdaFun2>
	void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes
	)
	{
		traverse_GenericTreeNode_LeafAndPostorderMidNodes_Serial(i_lambda_leaves, i_lambda_midnodes);
	}


#endif


/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * serial
 */

/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial
	{
	public:
		static void traverse(
				CGenericTreeNode_ *this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes_preorder,
				int p_genericTreeDepth
		)
		{
			if (this_node->isLeaf())
			{
				i_lambda_leaves(this_node, p_genericTreeDepth);
				return;
			}
			else
			{
				i_lambda_midnodes_preorder(this_node, p_genericTreeDepth);
			}

			p_genericTreeDepth++;
			if (this_node->first_child_node)
				traverse(this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, p_genericTreeDepth);

			if (this_node->second_child_node)
				traverse(this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes_preorder, p_genericTreeDepth);
		}
	};


	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial::traverse(this, i_lambda_leaves, i_lambda_midnodes_preorder, 0);
}



#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	/*
	 * parallel
	 */
	template <
		typename CLambdaFun1,
		typename CLambdaFun2
	>
	void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder
	)
	{
		traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(i_lambda_leaves, i_lambda_midnodes_preorder);
	}


#endif





/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * SERIAL
 */


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial
	{
	public:
		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes,
				int i_generic_tree_depth
		)
		{
			if (i_this_node->isLeaf())
			{
				i_lambda_leaves(i_this_node, i_generic_tree_depth);
				return;
			}

			i_generic_tree_depth++;

			if (i_this_node->first_child_node)
				traverse(i_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, i_generic_tree_depth);

			if (i_this_node->second_child_node)
				traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, i_generic_tree_depth);

			i_lambda_midnodes(i_this_node, i_generic_tree_depth-1);
		}
	};


	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial::traverse(this, i_lambda_leaves, i_lambda_midnodes, 0);
}


#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	template <typename CLambdaFun1, typename CLambdaFun2>
	void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes
	)
	{
		traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Serial(i_lambda_leaves, i_lambda_midnodes);
	}


#endif



/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/

template <
	typename CLambdaFun,
	typename TReduceValue
>
void traverse_GenericTreeNode_Reduce_Serial(
		CLambdaFun i_lambdaFun,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	class CTraversalTask_GenericTreeNode_Reduce_Serial
	{
	public:

		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun i_lambda,
				void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
				TReduceValue *o_reduce_output
		)
		{
			/**
			 * LEAF COMPUTATION
			 */
			if (i_this_node->isLeaf())
			{
				i_lambda(i_this_node, o_reduce_output);
				return;
			}

			/**
			 * PARALLEL TRAVERSALS
			 */

			/**
			 * first and second child node exists
			 */
			if (i_this_node->first_child_node && i_this_node->second_child_node)
			{
				TReduceValue reduceValueFirst;
				TReduceValue reduceValueSecond;

				traverse(i_this_node->first_child_node, i_lambda, i_reduce_operator, &reduceValueFirst);
				traverse(i_this_node->second_child_node, i_lambda, i_reduce_operator, &reduceValueSecond);

				i_reduce_operator(reduceValueFirst, reduceValueSecond, o_reduce_output);

				return;
			}

			if (i_this_node->first_child_node)
			{
				traverse(i_this_node->first_child_node, i_lambda, i_reduce_operator, o_reduce_output);
				return;
			}

#if CONFIG_ENABLE_MPI
			if (i_this_node->second_child_node)
			{
				traverse(i_this_node->second_child_node, i_lambda, i_reduce_operator, o_reduce_output);
			}
#else
			assert(i_this_node->second_child_node != nullptr);
			traverse(i_this_node->second_child_node, i_lambda, i_reduce_operator, o_reduce_output);
#endif
			return;
		}
	};



	CTraversalTask_GenericTreeNode_Reduce_Serial::traverse(this, i_lambdaFun, i_reduceOperator, o_reduceOutput);
}


#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY

	template <
		typename CLambdaFun,
		typename TReduceValue
	>
	void traverse_GenericTreeNode_Reduce_Parallel(
			CLambdaFun i_lambdaFun,
			void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
			TReduceValue *o_reduceOutput
	)
	{
		traverse_GenericTreeNode_Reduce_Serial(i_lambdaFun, i_reduceOperator, o_reduceOutput);
	}


	template <
		typename CLambdaFun,
		typename TReduceValue
	>
	void traverse_GenericTreeNode_Reduce_Parallel_Scan(
			CLambdaFun i_lambdaFun,
			void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
			TReduceValue *o_reduceOutput
	)
	{
		traverse_GenericTreeNode_Reduce_Serial(i_lambdaFun, i_reduceOperator, o_reduceOutput);
	}

#endif


/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
		TReduceValue *o_reduceOutput
)
{
	class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce
	{
public:
		static void traverse(
				CGenericTreeNode_ *i_this_node,
				CLambdaFun1 i_lambda_leaves,
				CLambdaFun2 i_lambda_midnodes,
				void (*i_reduce_operator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduceOutput),
				TReduceValue *o_reduce_output
		)
		{
			/**
			 * LEAF COMPUTATION
			 */
			if (i_this_node->isLeaf())
			{
				i_lambda_leaves(i_this_node, o_reduce_output);
				return;
			}

			/**
			 * PARALLEL TRAVERSALS
			 */

			/**
			 * first and second child node exists
			 */
			if (i_this_node->first_child_node && i_this_node->second_child_node)
			{
				TReduceValue reduceValueFirst;
				TReduceValue reduceValueSecond;

				traverse(i_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, i_reduce_operator, &reduceValueFirst);
				traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, i_reduce_operator, &reduceValueSecond);

				i_reduce_operator(reduceValueFirst, reduceValueSecond, o_reduce_output);

				i_lambda_midnodes(i_this_node, o_reduce_output);

				return;
			}

			if (i_this_node->first_child_node)
			{
				traverse(i_this_node->first_child_node, i_lambda_leaves, i_lambda_midnodes, i_reduce_operator, o_reduce_output);

				i_lambda_midnodes(i_this_node, o_reduce_output);

				return;
			}

#if CONFIG_ENABLE_MPI

			if (i_this_node->second_child_node)
				traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, i_reduce_operator, o_reduce_output);

#else
			assert(i_this_node->second_child_node != nullptr);
			traverse(i_this_node->second_child_node, i_lambda_leaves, i_lambda_midnodes, i_reduce_operator, o_reduce_output);
#endif

			i_lambda_midnodes(i_this_node, o_reduce_output);

			return;
		}
	};



	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce::traverse(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduceOutput);

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}


#if !CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &i_a, const TReduceValue &i_b, TReduceValue *o_reduce_output),
		TReduceValue *o_reduce_output
)
{
	traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Serial(i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduce_output);
}



#endif


#endif
