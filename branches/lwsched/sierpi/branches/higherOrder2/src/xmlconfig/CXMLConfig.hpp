/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 19. Juni 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CXMLCONFIG_HPP
#define CXMLCONFIG_HPP

#include <list>


/**
 * this interface has to be implemented by another one which likes to
 * receive xml configuration parameters
 */
class CXMLConfigInterface
{
public:
	virtual bool setupXMLParameter(
			const char *i_scope,
			const char *i_name,
			const char *i_parameter
	) = 0;

	virtual ~CXMLConfigInterface()
	{
	}
};


/**
 * some helper macros for the following method:
 *
 *
	bool setupXMLParameter(
			const char *i_scope_name,
			const char *i_name,
			const char *i_value
	)
 */

#define XML_CONFIG_TEST_AND_SET_INT(xmlname, variable)			\
	if (strcmp(i_name, xmlname) == 0)			\
	{											\
		variable = atoi(i_value);				\
		return true;							\
	}

#define XML_CONFIG_TEST_AND_SET_BOOL(xmlname, variable)		\
	if (strcmp(i_name, xmlname) == 0)			\
	{											\
		if (strcmp(i_value, "off") == 0) {		\
			variable = false;					\
			return true;						\
		}										\
		if (strcmp(i_value, "on") == 0) {		\
			variable = true;					\
			return true;						\
		}										\
		std::cerr << "XML Parsing - invalid value " << i_value << " for parameter " << i_name << std::endl;	\
		exit(-1);								\
		return false;							\
	}

#define XML_CONFIG_TEST_AND_SET_FLOAT(xmlname, variable)		\
	if (strcmp(i_name, xmlname) == 0)			\
	{											\
		variable = atof(i_value);				\
		return true;							\
	}

#define XML_CONFIG_TEST_AND_SET_STRING(xmlname, variable)		\
	if (strcmp(i_name, xmlname) == 0)			\
	{											\
		variable = i_value;						\
		return true;							\
	}



class CXMLConfig
{
	std::list<std::pair<const char*, CXMLConfigInterface*> > configuration_interfaces;

	/**
	 * register a class to handle configuration parameters
	 */
public:
	void registerConfigClass(
			const char *i_config_scope_name,			///< name of xml scope to forward parameters (e. g. swe, threading or sierpi)
			CXMLConfigInterface *i_cXMLConfigInterface	///< pointer to xml interface handling the parameter
	);


	/**
	 * load configuration from xml file
	 */
public:
	bool loadConfig(
			const char *i_xml_file			///< load configuration from xml file
	);
};



#endif
