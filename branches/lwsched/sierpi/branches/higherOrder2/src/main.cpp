/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include <stdlib.h>
#include <stdint.h>
#include <sstream>

#if COMPILE_SIMULATION_WITH_GUI
	#include "CMainGui.hpp"
#else
	#include "CMain.hpp"
#endif


int main(int argc, char *argv[])
{
#if COMPILE_SIMULATION_WITH_GUI
	CMainGui<CSimulation> cMain(argc, argv);
#else
	CMain<CSimulation> cMain(argc, argv);
#endif

	// create the simulation
	cMain.simulation_create();

	// setup MPI
	cMain.mpi_setup();

	if (!cMain.setupProgramParameters())
		return -1;

	// setup threading tools
	cMain.threading_setup();

	// setup the simulation
	cMain.simulation_setup();

	// output some useful information
	cMain.outputVerboseInformation();

#if COMPILE_SIMULATION_WITH_GUI

		// setup gui stuff AFTER setting up the simulation
		cMain.setupGui();

		// run the simulation loop
		cMain.run();

#else

		// simulation loop prefix
		cMain.simulation_loopPrefix();

		// run the simulation
		cMain.threading_simulationLoop();

		// simulation loop suffix
		cMain.simulation_loopSuffix();
#endif

	// shutdown the simulation
	cMain.simulation_shutdown();

	// shutdown threading tools
	cMain.threading_shutdown();

	// shutdown MPI
	cMain.mpi_shutdown();

	return 0;
}
