
#include <stdint.h>
#include "CEdgeComm_Tsunami_1stOrder.hpp"
#include "CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "lib/CStopwatch.hpp"
#include "libmath/CMath.hpp"
#include <xmmintrin.h>



// convenient typedef
typedef TTsunamiDataScalar T;


/*******
 * TEST KERNEL
 */

float rand01()
{
	return ((float)std::rand()/(float)RAND_MAX);
}

sierpi::kernels::CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET> edgeComm_Tsunami_1stOrder;

void testrun(
		bool use_simd,
		int counter,
		bool output,
		int max_depth,
		CTsunamiElementData *elementData
)
{
	CStopwatch cStopwatch;

	edgeComm_Tsunami_1stOrder.use_simd = use_simd;

	CTsunamiEdgeData *hyp_edge = new CTsunamiEdgeData[counter];
	CTsunamiEdgeData *left_edge = new CTsunamiEdgeData[counter];
	CTsunamiEdgeData *right_edge = new CTsunamiEdgeData[counter];

	for (int i = 0; i < counter; i++)
	{
		hyp_edge[i].h = 50.1;
		hyp_edge[i].qx = 1;
		hyp_edge[i].qy = 2;

		left_edge[i].h = 50.1;
		left_edge[i].qx = 1;
		left_edge[i].qy = 2;

		right_edge[i].h = 51.0;
		right_edge[i].qx = 1;
		right_edge[i].qy = 2;
	}

	cStopwatch.start();

	int depth = 0;
	for (int i = 0; i < counter; i++)
	{
		edgeComm_Tsunami_1stOrder.elementAction_EEE(
				0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
				depth,
				&elementData[i],
				&hyp_edge[i], &left_edge[i], &right_edge[i]
			);

		depth = (depth+1) % max_depth;
	}

	cStopwatch.stop();


	if (output)
	{
		std::cout << "===============================================" << std::endl;

		if (use_simd)
			std::cout << "With SIMD (" << counter << " iterations)" << std::endl;
		else
			std::cout << "Without SIMD (" << counter << " iterations)" << std::endl;

		std::cout << "===============================================" << std::endl;
		std::cout << elementData[counter/2] << std::endl;;
		std::cout << "TIME: " << cStopwatch.time << std::endl;
		std::cout << "===============================================" << std::endl;
		std::cout << std::endl;
	}

	delete hyp_edge;
	delete right_edge;
	delete left_edge;
}

int main()
{
	CStopwatch cStopwatch;

	int counter = 4*10000000;

	CTsunamiElementData *elementData1 = new CTsunamiElementData[counter];
	CTsunamiElementData *elementData2 = new CTsunamiElementData[counter];

	int max_depth = 8;

	for (int i = 0; i < counter; i++)
	{
		elementData1[i].hyp_edge.h = 50.5;
		elementData1[i].hyp_edge.qx = 2+rand01()*10.5;
		elementData1[i].hyp_edge.qy = 1+rand01()*20.5;

		elementData1[i].left_edge.h = 50.5;
		elementData1[i].left_edge.qx = 2-rand01()*10.5;
		elementData1[i].left_edge.qy = 1+rand01()*20.5;

		elementData1[i].right_edge.h = 50.5-rand01()*10.5;
		elementData1[i].right_edge.qx = 2+rand01()*20.5;
		elementData1[i].right_edge.qy = 1-rand01()*10.5;

		elementData2[i] = elementData1[i];
	}


	std::cout << "======== NO SIMD =========" << std::endl;

	testrun(false, counter, true, max_depth, elementData1);

	std::cout << "======== SIMD =========" << std::endl;
	testrun(true, counter, true, max_depth, elementData2);


	std::cout << "======== Comparing =========" << std::endl;

	static T epsilon = 0.00001;
	for (int i = 0; i < counter; i++)
	{
		if (std::abs(elementData1[i].hyp_edge.h - elementData2[i].hyp_edge.h) > epsilon)
		{
			std::cout << "DIFF h " << i << std::endl;
			std::cout << elementData1[i];
			std::cout << elementData2[i];
		}

		if (std::abs(elementData1[i].right_edge.h - elementData2[i].right_edge.h) > epsilon)
		{
			std::cout << "DIFF r " << i << std::endl;
			std::cout << elementData1[i];
			std::cout << elementData2[i];
		}

		if (std::abs(elementData1[i].left_edge.h - elementData2[i].left_edge.h) > epsilon)
		{
			std::cout << "DIFF l " << i << std::endl;
			std::cout << elementData1[i];
			std::cout << elementData2[i];
		}
	}


	delete elementData1;
	delete elementData2;
}
