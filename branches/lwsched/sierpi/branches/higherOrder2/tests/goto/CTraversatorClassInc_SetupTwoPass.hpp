/*
 * CTraversator_Setup2Pass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: schreibm
 */

#include "CTraversatorClassInc_DefaultInclude.hpp"



/**
 * idx's of methods to execute for given triangleFactory
 */
int recursionMethodForwardPassId;
int recursionMethodBackwardPassId;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory triangleFactory;


/**
 * setup the initial partition traversal for the given factory
 */
void setup_RootPartition(
		CTriangle_Factory &p_triangleFactory
)
{
	triangleFactory = p_triangleFactory;

	setupRecursionTable();

	/*
	 * search for recursive function id for first pass
	 *
	 * all edges of type OLD  of this triangle have to be set to NEW
	 */
	recursionMethodForwardPassId = getRecursiveFunctionTableIndex_WithConvertedEdgeTypes(
			triangleFactory,
			CTriangle_Enums::EDGE_TYPE_OLD,
			CTriangle_Enums::EDGE_TYPE_NEW
			);

	/*
	 * search for recursive function id for second pass
	 *
	 * all edges of type NEW  of this triangle have to be set to OLD
	 */
	CTriangle_Factory backwardTriangleFactory = p_triangleFactory;
	backwardTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_BACKWARD;

	recursionMethodBackwardPassId = getRecursiveFunctionTableIndex_WithConvertedEdgeTypes(
			backwardTriangleFactory,
			CTriangle_Enums::EDGE_TYPE_NEW,
			CTriangle_Enums::EDGE_TYPE_OLD
			);
}



/**
 * setup the parameters with the one by the parent partition
 */
void setup_ChildPartition(
		TThisClass &p_parent,			///< information of parent traversator
		CTriangle_Factory &p_triangleFactory	///< triangle factory to find the first method
)
{
	// make sure that this is really a root node
	assert(triangleFactory.partitionTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	recursionTable = p_parent.recursionTable;

	triangleFactory = p_triangleFactory;

	if (triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_FIRST_CHILD)
	{
		recursionMethodForwardPassId = p_parent.recursionTable[p_parent.recursionMethodForwardPassId].first_child_method_type_to_k_and_old_to_new_idx;

		// use SECOND child due to backward traversal!!!
		recursionMethodBackwardPassId = p_parent.recursionTable[p_parent.recursionMethodBackwardPassId].second_child_method_type_to_k_and_new_to_old_idx;
	}
	else
	{
		assert(triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD);
		recursionMethodForwardPassId = p_parent.recursionTable[p_parent.recursionMethodForwardPassId].second_child_method_type_to_k_and_old_to_new_idx;

		// use SECOND child due to backward traversal!!!
		recursionMethodBackwardPassId = p_parent.recursionTable[p_parent.recursionMethodBackwardPassId].first_child_method_type_to_k_and_new_to_old_idx;
	}

	// setup kernel class
	kernelClass = p_parent.kernelClass;
}
