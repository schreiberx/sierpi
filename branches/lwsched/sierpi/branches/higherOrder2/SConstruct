#! /usr/bin/python

import os
import commands
import re
import sys

import CCompileXMLOptions



# specify iOMP location here
iomp_location=os.environ['PWD']+'/../iomp/'

# specify iPMO location here
ipmo_location=os.environ['PWD']+'/../ipmo/'


###################################################################
# configuration ends here - don't modify any line below this one
###################################################################

env = Environment()



###################################################################
# fix environment vars (not imported by default)
###################################################################

# import all environment variables (expecially PATH, LD_LIBRARY_PATH, LD_PATH)
env = Environment(ENV = os.environ)


# load options from environment

if os.environ.has_key('TBB_LIBDIR'):
	# LRZ
	env['tbb_inc'] = os.environ['TBB_INC']
	env['tbb_lib_path'] = os.environ['TBB_LIBDIR']

elif os.environ.has_key('TBBROOT'):
	env['tbb_inc'] = '-I'+os.environ['TBBROOT']+'/include'
	env['tbb_lib_path'] = os.environ['TBBROOT']+'/lib'


if os.environ.has_key('ASAGI_PATH'):
	env['asagi_path'] = os.environ['ASAGI_PATH']
else:
	env['asagi_path'] = os.environ['HOME']+'/local_src/ASAGI'


###################################################################
# Command line options
###################################################################

#
# boolean values should be set to 'on' or 'off' strings via the command line
# True and False values are then set to the configuration variable stored to the env[] array
#


def setupBoolOption(varname, default):
	global env

	env[varname] = GetOption(varname)

	if env[varname]==None:
		env[varname] = default
		return

	if env[varname] not in set(['on', 'off']):
		print 'Invalid option "'+env[varname]+'" for ' + varname + ' specified'
		Exit(1)

	if env[varname]=='on':
		env[varname] = True
	else:
		env[varname] = False


def setupStringOption(varname, constraints, default):
	global env

	env[varname] = GetOption(varname)

	if env[varname]==None:
		env[varname] = default
	elif env[varname] not in constraints:
		print 'Invalid option \'"'+env[varname]+'"\' for ' + varname + ' specified'
		Exit(1)



################################################################
# COMPILER STUFF
################################################################

#
# compiler (gnu/intel)
#
compiler_constraints = ['gnu', 'intel']
AddOption(	'--compiler',
		dest='compiler',
		type='string',
		nargs=1,
		action='store',
		help='specify compiler to use (gnu/intel), default: gnu')

setupStringOption('compiler', compiler_constraints, 'gnu')





#
# enable fortran (only valid for intel compiler)
#
AddOption(	'--enable-fortran-source',
		dest='enable_fortran_source',
		type='string',
		nargs=1,
		action='store',
		help='enable fortran compiler (on/off), default: off')

setupBoolOption('enable_fortran_source', False)


#
# enable MPI
#
AddOption(	'--enable-mpi',
		dest='enable_mpi',
		type='string',
		nargs=1,
		action='store',
		help='enable MPI (on/off), default: off')

setupBoolOption('enable_mpi', False)



#
# compile mode (debug/release)
#
mode_constraints = ['debug', 'release']
AddOption(	'--mode',
		dest='mode',
		type='string',
		nargs=1,
		action='store',
		help='specify release or debug mode (release/debug), default: release')

setupStringOption('mode', mode_constraints, 'debug')



#
# floating point precision
#
fp_default_precision_constraints = ['single', 'double']
AddOption(	'--fp-default-precision',
		dest='fp_default_precision',
		type='string',
		nargs=1,
		action='store',
		help='Floating point precision (single/double), default: single')

setupStringOption('fp_default_precision', fp_default_precision_constraints, 'single')



#
# create scan distribution data
#
AddOption(	'--enable-scan-data',
		dest='enable_scan_data',
		type='string',
		nargs=1,
		action='store',
		help='Enable creating scan data for clusters (on/off), default: off')

setupBoolOption('enable_scan_data', False)



#
# execute clusters only on threads specified by scans
#
AddOption(	'--enable-scan-threading',
		dest='enable_scan_threading',
		type='string',
		nargs=1,
		action='store',
		help='Enable executing tree nodes only on threads specified by scan data (on/off), default: off')

setupBoolOption('enable_scan_threading', False)



#
# scan distribution
#
AddOption(	'--enable-scan-split-and-join',
		dest='enable_scan_split_and_join',
		type='string',
		nargs=1,
		action='store',
		help='Enable split and join requests via equal scan distribution and run scan based blocked traversals (on/off), default: off')

setupBoolOption('enable_scan_split_and_join', False)



#
# force scan distribution
#
AddOption(	'--enable-scan-force-threading-traversal',
		dest='enable_scan_force_threading_traversal',
		type='string',
		nargs=1,
		action='store',
		help='Force using scan traversals by splitting non-scanable traversals by a scan and parallel traversal (on/off), default: off')

setupBoolOption('enable_scan_force_threading_traversal', False)
if env['enable_scan_force_threading_traversal']:
	env['enable_scan_data'] = True
	env['enable_scan_threading'] = True
#	env['enable_scan_split_and_join'] = True





################################################################
# GUI
################################################################

#
# GUI (true/false)
#
AddOption(	'--enable-gui',
		dest='enable_gui',
		type='string',
		action='store',
		help='Build with enabled OpenGL GUI Backend (off/on), default: off')

setupBoolOption('enable_gui', False)


################################################################
# SIMULATION
################################################################


############################
# GENERIC PARAMS FOR FRAMEWORK / TRAVERSALS
############################


#
# Threading
#
threading_constraints = ['off', 'omp', 'tbb', 'ipmo', 'ipmo_async', 'itbb', 'itbb_async', 'iomp']
AddOption(	'--threading',
		dest='threading',
		type='string',
		nargs=1,
		action='store',
		help='Threading to use '+' / '.join(threading_constraints)+', default: omp')

setupStringOption('threading', threading_constraints, 'off')


#
# OpenMP specific
#
AddOption(	'--enable-openmp-untied-tasks',
		dest='enable_openmp_untied_tasks',
		type='string',
		nargs=1,
		action='store',
		help='OpenMP: use untied tasks (on/off), default: on')

setupBoolOption('enable_openmp_untied_tasks', True)


AddOption(	'--enable-openmp-task-for-each-leaf',
		dest='enable_openmp_task_for_each_leaf',
		type='string',
		nargs=1,
		action='store',
		help='OpenMP: create a task for each leaf instead only the first one (on/off), default: on')

setupBoolOption('enable_openmp_task_for_each_leaf', True)

#
# FLUX or EDGE comm
#

AddOption(	'--enable-single-flux-evaluation-between-clusters',
		dest='enable_single_flux_evaluation_between_clusters',
		type='string',
		nargs=1,
		action='store',
		help='enable single flux cluster communication (on/off), default: on')

setupBoolOption('enable_single_flux_evaluation_between_clusters', True)


#
# TBB specific
#

AddOption(	'--enable-tbb-task-affinities',
		dest='enable_tbb_task_affinities',
		type='string',
		nargs=1,
		action='store',
		help='TBB: use task affinities (on/off), default: off [EXPERIMENTAL]')

setupBoolOption('enable_tbb_task_affinities', False)

#
# OpenCL
#
AddOption(	'--enable-opencl',
		dest='enable_opencl',
		type='string',
		nargs=1,
		action='store',
		help='Enable OpenCL accelleration (on/off), default: on [TODO/EXPERIMENTAL]')

setupBoolOption('enable_opencl', False)



#
# Periodic boundaries activated/deactivated
#
AddOption('--enable-domain-periodic-boundaries',
		dest='enable_domain_periodic_boundaries',
		type='string',
		nargs=1,
		action='store',
		help='activate periodic boundaries for domain (on/off), default: \'off\'')

setupBoolOption('enable_domain_periodic_boundaries', False)



#
# Adaptive Stack Size (true/false)
#
AddOption(	'--enable-adaptive-stack-size',
		dest='enable_adaptive_stack_size',
		type='string',
		action='store',
		help='build with enabled adaptiveStackSize (on/off), default: on')


setupBoolOption('enable_adaptive_stack_size', True)



#
# skip adaptive conforming sub-partitions
#
AddOption(	'--enable-skip-adaptive-conforming-sub-partitions',
		dest='enable_skip_adaptive_conforming_sub_partitions',
		type='string',
		nargs=1,
		action='store',
		help='Skip adaptive conforming sub-partitions (on/off), default: off [EXPERIMENTAL]')

setupBoolOption('enable_skip_adaptive_conforming_sub_partitions', False)



############################
# SIMULATION SPECIFIC
############################


#
# simulation (tsunami_parallel/tsunami_serial/tsunami_1d)
#
simulation_constraints = ['tsunami_parallel', 'tsunami_serial', 'tsunami_1d']
AddOption('--simulation',
		dest='simulation',
		type='string',
		nargs=1,
		action='store',
		help='simulation to compile ('+' / '.join(simulation_constraints)+'), default: tsunami_parallel')

setupStringOption('simulation', simulation_constraints, 'tsunami_parallel')


#
# use bathymetry
#
AddOption(	'--enable-bathymetry-kernels',
		dest='enable_bathymetry_kernels',
		type='string',
		action='store',
		help='Build with enabled Bathymetry handling (off/on), default: on')

setupBoolOption('enable_bathymetry_kernels', True)


#
# different flux solvers
#
tsunami_flux_solver_constraints = ['0', '1', '2', '3', '4', '5']
AddOption(	'--tsunami-flux-solver',
		dest='tsunami_flux_solver',
		type='string',
		nargs=1,
		action='store',
		help='''set flux solver to apply to riemann problems: (
			0: lax friedrich with constant numerical friction
			1: lax friedrich
			2: fwave
			3: augumented riemann
			4: hybrid
			5: augumented riemann (fortrancode from geoclaw)''')

setupStringOption('tsunami_flux_solver', tsunami_flux_solver_constraints, '1')



#
# tsunami runge-kutta order
#
tsunami_runge_kutta_order_constraints = ['1', '2']
AddOption(	'--tsunami-runge-kutta-order',
		dest='tsunami_runge_kutta_order',
		type='string',
		nargs=1,
		action='store',
		help='tsunami runge kutta order (1/2), default: 1')

setupStringOption('tsunami_runge_kutta_order', tsunami_runge_kutta_order_constraints, '1')



#
# Adaptivity 
#
tsunami_adaptivity_mode_constraints = ['1', '2']
AddOption(	'--tsunami-adaptivity-mode',
		dest='tsunami_adaptivity_mode',
		type='string',
		nargs=1,
		action='store',
		help='tsunami adaptivity mode, 1: height, 2: net-update based')

setupStringOption('tsunami_adaptivity_mode', tsunami_adaptivity_mode_constraints, '1')




#
# order of basis functions for tsunami simulation
#
tsunami_order_of_basis_functions_constraints = ['0', '1', '1b', '2']
AddOption(	'--tsunami-order-of-basis-functions',
		dest='tsunami_order_of_basis_functions',
		type='string',
		nargs=1,
		action='store',
		help='''order of basis-function (0 and 1 supported so far)''')

setupStringOption('tsunami_order_of_basis_functions', tsunami_order_of_basis_functions_constraints, '0')


#
# run simulation with local time-stepping
#
AddOption('--enable-tsunami-local-time-stepping',
		dest='enable_tsunami_local_time_stepping',
		type='string',
		nargs=1,
		action='store',
		help='Local time-stepping for sub-partitions (on/off), default: \'off\' [TODO/EXPERIMENTAL]')

setupBoolOption('enable_tsunami_local_time_stepping', True)


#
# pinning for serial simulation activated
#
AddOption('--enable-tsunami-serial-pinning',
		dest='enable_tsunami_serial_pinning',
		type='string',
		nargs=1,
		action='store',
		help='Serial Tsunami simulation only: pinning for simulation activated (on/off), default: \'off\'')

setupBoolOption('enable_tsunami_serial_pinning', False)





################################################################
# ADDITIONAL MODULES
################################################################


#
# compile with fancy ASAGI
#
AddOption(	'--enable-asagi',
		dest='enable_asagi',
		type='string',
		nargs=1,
		action='store',
		help='compiler with asagi (on/off), default: off')

setupBoolOption('enable_asagi', False)




################################################################
# COMPILER STUFF
################################################################

AddOption(	'--xml-config',
		dest='xml_config',
		type='string',
		nargs=1,
		action='store',
		help='xml configuration file with compile options, default: off')

env['xml_config'] = GetOption('xml_config')

if env['xml_config'] != None:
	env = CCompileXMLOptions.load(env['xml_config'], env)




###################################################################
# SETUP COMPILER AND LINK OPTIONS
###################################################################


###################################################################
# ASAGI
#
if env['enable_asagi']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_ASAGI=1 -I'+env['asagi_path']+'/include')
	
	# asagi should be available in default library directory
	env.Append(LIBS=['asagi'])
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_ASAGI=0')



###################################################################
# Default libs
#

#env.ParseConfig("pkg-config libxml-2.0 --cflags --libs")
env.ParseConfig("xml2-config --cflags --libs")


###################################################################
# Bathymetry
#
if env['enable_bathymetry_kernels']:
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS=1')
else:
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS=0')


###################################################################
# Tsunami flux solver to use
#
env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_FLUX_SOLVER='+str(env['tsunami_flux_solver']))

if env['tsunami_flux_solver'] == '5':

	# enable fortran compiler
	env['enable_fortran_source']='true'

	if env['fp_default_precision'] != 'double':
		print 'Flux Solver 5 only available for double floating point precision (use --fp-precision=double)!'
		Exit(-1)



###################################################################
# Tsunami runge kutta order
#
env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER='+str(env['tsunami_runge_kutta_order']))



###################################################################
# Tsunami adaptivity mode
#
env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_ADAPTIVITY_MODE='+str(env['tsunami_adaptivity_mode']))


###################################################################
# Tsunami flux solver to use
#
env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS='+str(env['tsunami_order_of_basis_functions']))



###################################################################
# Compiler stuff
#
if env['compiler'] == 'gnu':
	reqversion = [4,6,1]

	#
	# get gcc version using -v instead of -dumpversion since SUSE gnu compiler
	# returns only 2 instead of 3 digits with -dumpversion
	#
	gccv = commands.getoutput('g++ -v').splitlines()

	# updated to search for 'gcc version ' line prefix
	found = False
	search_string = 'gcc version '
	found_line = ''
	for l in gccv:
		if l[:len(search_string)] == search_string:
			found_line = l
			found = True
			break

	if not found:
		print search_string+" not found"
		sys.exit(-1)

	gccversion = found_line.split(' ')[2].split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'At least GCC Version 4.6.1 necessary.'
			Exit(1)

#	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
#	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate gnu C++ compiler

	# todo: fix me also for intel mpicxx compiler
	if env['enable_mpi']:
		env.Replace(CXX = 'mpicxx')
	else:
		env.Replace(CXX = 'g++')

	if env['enable_fortran_source']:
		print "GNU compiler not supported with fortran enabled"
		Exit(-1)


if env['compiler'] == 'open64':
	reqversion = [4,6,1]
	open64version = commands.getoutput('openCC -dumpversion').split('.')

	print "Open64 not supported so far..."
	Exit(-1)

#	for i in range(0, 3):
#		if (int(open64version[i]) < int(reqversion[i])):
#			print 'Open64 version ??!? necessary.'
#			Exit(1)

#	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
#	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
#	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate open64 C++ compiler
	env.Replace(CXX = 'openCC')

if env['compiler'] == 'intel':
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ICPC Version 12.1 necessary.'
			Exit(1)

#	env.Append(LINKFLAGS=' -static-intel')

	env.Append(LINKFLAGS=' -shared-intel')
	env.Append(LINKFLAGS=' -shared-libgcc')
	env.Append(LINKFLAGS=' -debug inline-debug-info')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# output more warnings
	env.Append(CXXFLAGS=' -w1')

	# compiler option which has to be appended for icpc 12.1 without update1
	env.Append(CXXFLAGS=' -U__GXX_EXPERIMENTAL_CXX0X__')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')

	# activate intel C++ compiler
	if env['enable_mpi']:
		env.Replace(CXX = 'mpicxx')
		env.Replace(CXXFLAGS='')
	else:
		env.Replace(CXX = 'icpc')

	if env['enable_fortran_source']:
		env.Replace(FORTRAN='ifort')
		env.Replace(F90='ifort')
		env.Replace(LINK='icpc')




if env['mode'] == 'debug':
	env.Append(CXXFLAGS=' -DDEBUG=1')

	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O0 -g3 -Wall')

	elif env['compiler'] == 'intel':
		env.Append(CXXFLAGS=' -O0 -g')
#		env.Append(CXXFLAGS=' -traceback')

	if env['enable_fortran_source']:
		env.Append(FORTRANFLAGS=' -O0')
		env.Append(F90FLAGS=' -O0')

elif env['mode'] == 'release':
	env.Append(CXXFLAGS=' -DNDEBUG=1')

	if env['compiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O3 -mtune=native')

	elif env['compiler'] == 'intel':
#		env.Append(CXXFLAGS=' -xHOST -O3 -fast -fno-alias')
		env.Append(CXXFLAGS=' -O3 -fast -fno-alias')
#		env.Append(CXXFLAGS=' -O2')

	if env['enable_fortran_source']:
		env.Append(FORTRANFLAGS=' -O3')
		env.Append(F90FLAGS=' -O3')

else:
	print 'ERROR: mode'
	Exit(1)


###################################################################
# Skipping for adaptive conforming cluster
#
	
if env['enable_skip_adaptive_conforming_sub_partitions']:
	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE=0')



###################################################################
# Flux communication instead of edge communication
#

if env['enable_single_flux_evaluation_between_clusters']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER=0')


###################################################################
# enable scan split and join distribution
#

if env['enable_mpi']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_MPI=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_MPI=0')
	


###################################################################
# enable creating scan data
#
if env['enable_scan_data']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_DATA=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_DATA=0')

###################################################################
# execute threads using scan thread id information
#
if env['enable_scan_threading']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_THREADING=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_THREADING=0')


###################################################################
# enable scan split and join distribution
#
if env['enable_scan_split_and_join']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_SPLIT_AND_JOIN_DISTRIBUTION=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_SPLIT_AND_JOIN_DISTRIBUTION=0')




###################################################################
# enable force scan split and join distribution
#

if env['enable_scan_force_threading_traversal']:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL=0')



###################################################################
# Additional Fortran options to create threadsafe code
#

if env['threading'] != 'off':
	if env['enable_fortran_source']:
		if env['compiler'] == 'intel':
			env.Append(FORTRANFLAGS=' -openmp')
			env.Append(F90FLAGS=' -openmp')


	if env['enable_openmp_untied_tasks']:
		env.Append(CXXFLAGS=' -DOPENMP_EXTRA_TASK_CLAUSE=untied')

	if env['enable_openmp_task_for_each_leaf']:
		env.Append(CXXFLAGS=' -DOPENMP_CREATE_TASK_FOR_SECOND_LEAF=1')

	if env['enable_tbb_task_affinities']:
		if env['threading'] != 'tbb':
			print 'ERROR: TBB task affinities only available for TBB!'
			sys.exit(-1)

		env.Append(CXXFLAGS=' -DCONFIG_TBB_TASK_AFFINITIES')

	if env['threading'] == 'omp':
		env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1')
		if env['compiler'] == 'gnu':
			env.Append(CXXFLAGS=' -fopenmp')
			env.Append(LINKFLAGS=' -fopenmp')
		elif env['compiler'] == 'intel':
			env.Append(CXXFLAGS=' -openmp')
	#		env.Append(LINKFLAGS=' -openmp -openmp-link=static')
	
			env.Append(LINKFLAGS=' -openmp')
	
			# for amplifier
			env.Append(LINKFLAGS=' -openmp-link dynamic')
	
	
	elif env['threading'] == 'iomp':
		env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1 -DCOMPILE_WITH_IOMP=1')
		if env['compiler'] == 'gnu':
			env.Append(CXXFLAGS=' -fopenmp')
			env.Append(LINKFLAGS=' -fopenmp')
		elif env['compiler'] == 'intel':
			env.Append(CXXFLAGS=' -openmp')
			env.Append(LINKFLAGS=' -openmp')
	
		# include directories for iOMP
		env.Append(CXXFLAGS=' -I'+iomp_location+'/iomp/include/')
		env.Append(LINKFLAGS=' -L'+iomp_location+'/build/iomp/ -liomp')
		env.Append(LINKFLAGS=' -L'+os.environ['HOME']+'/local/lib')
		env.Append(LIBS=['iomp', 'boost_serialization'])
		
		env.Append(LINKFLAGS=' -lboost_serialization')	
	
	
	elif env['threading'] == 'ipmo' or env['threading'] == 'ipmo_async':
		env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1 -DCOMPILE_WITH_IPMO=1')

		if env['threading'] == 'ipmo_async':
			env.Append(CXXFLAGS=' -DCOMPILE_WITH_IPMO_ASYNC=1')

		if env['compiler'] == 'gnu':
			env.Append(CXXFLAGS=' -fopenmp')
			env.Append(LINKFLAGS=' -fopenmp')
		elif env['compiler'] == 'intel':
			env.Append(CXXFLAGS=' -openmp')
			env.Append(LINKFLAGS=' -openmp')
	
		# include directories for iPMO
		env.Append(CXXFLAGS=' -I'+ipmo_location+'/include/')
	
	
	elif env['threading'] == 'itbb' or env['threading'] == 'itbb_async':
		env.Append(CXXFLAGS=' -DCOMPILE_WITH_TBB=1 -DCOMPILE_WITH_ITBB=1')

		if env['threading'] == 'itbb_async':
			env.Append(CXXFLAGS=' -DCOMPILE_WITH_ITBB_ASYNC=1')

		env.Append(CXXFLAGS=' '+env['tbb_inc'])
		env.Append(LIBPATH=[env['tbb_lib_path']])

		if os.environ['LD_LIBRARY_PATH'] != None:
			env.Append(LIBPATH=os.environ['LD_LIBRARY_PATH'].split(':'))
	
		if env['mode'] == 'debug':
			env.Append(LIBS=['tbb_debug'])
		else:
			env.Append(LIBS=['tbb'])

		env.Append(CXXFLAGS=' -I'+ipmo_location+'/include/')

	
	elif env['threading'] == 'tbb':
		env.Append(CXXFLAGS=' -DCOMPILE_WITH_TBB=1')
		env.Append(CXXFLAGS=' '+env['tbb_inc'])
	
		env.Append(LIBPATH=[env['tbb_lib_path']])

		if os.environ['LD_LIBRARY_PATH'] != None:
			env.Append(LIBPATH=os.environ['LD_LIBRARY_PATH'].split(':'))
	
		if env['mode'] == 'debug':
			env.Append(LIBS=['tbb_debug'])
		else:
			env.Append(LIBS=['tbb'])


###################################################################
# GUI
#

if env['enable_gui']:
	# compile flags
	env.Append(CXXFLAGS=' -I'+os.environ['HOME']+'/local/include')
	env.Append(CXXFLAGS=' -DCOMPILE_SIMULATION_WITH_GUI=1')

	# linker flags

	# add nvidia lib path when running on atsccs* workstation
	hostname = commands.getoutput('uname -n')
	if re.match("atsccs.*", hostname):
		env.Append(LIBPATH=['/usr/lib/nvidia-current/'])

	env.Append(LIBPATH=[os.environ['HOME']+'/local/lib'])
	env.Append(LIBS=['GL'])

	reqversion = [2,0,0]
	sdlversion = commands.getoutput('sdl2-config --version').split('.')

	for i in range(0, 3):
		if (int(sdlversion[i]) > int(reqversion[i])):
			break;
		if (int(sdlversion[i]) < int(reqversion[i])):
			print 'libSDL Version 2.0.0 necessary.'
			Exit(1)

	env.ParseConfig("sdl2-config --cflags --libs")
	env.ParseConfig("pkg-config freetype2 --cflags --libs")
else:
	env.Append(CXXFLAGS=' -DCOMPILE_SIMULATION_WITH_GUI=0')


###################################################################
# Simulation
#

env.Append(CXXFLAGS=' -DSIMULATION_DEFINED=1')

if env['simulation']=='tsunami_parallel':
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_PARALLEL=1')

elif env['simulation']=='tsunami_serial':
	if env['threading'] != 'off':
		print 'WARNING: Compiling serial version with threading activated is non-sense! => deactivating'
		env['threading'] = 'off'

	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL=1')

	if env['enable_tsunami_serial_pinning']:
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_PINNING=1')
		env.Append(LIBS=['pthread'])
	else:
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_PINNING=0')
		
		
elif env['simulation']=='tsunami_1d':
	if env['threading'] != 'off':
		print 'Compiling 1d version with threading activated is non-sense!'
		Exit(-1)
	env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_1D=1')

else:
	print 'Invalid simulation specified'
	Exit(-1)


###################################################################
# periodic boundaries
#
if env['enable_domain_periodic_boundaries']:
	env.Append(CXXFLAGS=' -DCONFIG_PERIODIC_BOUNDARIES=1')
else:
	env.Append(CXXFLAGS=' -DCONFIG_PERIODIC_BOUNDARIES=0')


###################################################################
# adaptive stacks
#

if env['enable_adaptive_stack_size']:
	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS=1')
#	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING='+str(env['enable_adaptive_stack_size']))
#	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING='+str(env['enable_adaptive_stack_size']))
else:
	env.Append(CXXFLAGS=' -DCONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS=0')


###################################################################
# Floating point precision
#

if env['fp_default_precision'] == None:
	pass
elif env['fp_default_precision'] == 'single':
	env.Append(CXXFLAGS=' -DCONFIG_DEFAULT_FLOATING_POINT_PRECISION_SINGLE=1')
elif env['fp_default_precision'] == 'double':
	env.Append(CXXFLAGS=' -DCONFIG_DEFAULT_FLOATING_POINT_PRECISION_DOUBLE=1')



###################################################################
# DEPENDENCIES
###################################################################

# search this paths for dependencies
env.Append(CPPPATH = ['/usr/local/include', '/usr/include'])
# also include the 'src' directory to search for dependencies
env.Append(CPPPATH = ['.', 'src/'])

if env['threading'] == 'ipmo':
	env.Append(CPPPATH = [ipmo_location+'/include/'])
	


######################
# INCLUDE PATH
######################

# ugly hack
for i in ['src/', 'src/include/']:
	env.Append(CXXFLAGS = ' -I'+os.environ['PWD']+'/'+i)



######################
# setup PROGRAM NAME base on parameters
######################
program_name = 'sierpi'

# compiler
program_name += '_'+env['compiler']

# MPI
if env['enable_mpi']:
	program_name += '_mpi'

# threading
if env['threading'] != 'off':
	program_name += '_'+env['threading']

	if not env['enable_openmp_untied_tasks']:
		program_name += '_nountied'

	if not env['enable_openmp_task_for_each_leaf']:
		program_name += '_nosecondleaftasks'

	if env['enable_tbb_task_affinities']:
		program_name += '_taskaffinities'

	if env['enable_scan_force_threading_traversal']:
		program_name += '_scan_force_threading'
	elif env['enable_scan_threading']:
		program_name += '_scan_threading'
	elif env['enable_scan_data']:
		program_name += '_scan_data'

	if env['enable_scan_split_and_join']:
		program_name += '_scan_saj'

# sacsp
if env['simulation'] == 'tsunami_parallel':
	if env['enable_skip_adaptive_conforming_sub_partitions']:
		program_name += '_sacsp'

# gui
if env['enable_gui']:
	program_name += '_gui'

# simulation
if env['simulation'] != None:
	program_name += '_'+env['simulation']

if env['enable_asagi']:
	program_name += '_asagi'

if env['tsunami_runge_kutta_order'] != '1':
	program_name += '_rk'+str(env['tsunami_runge_kutta_order'])


# mode
program_name += '_'+env['mode']



#
# build directory
#
build_dir='build/build_'+program_name


#
# fortran special handling
#
if env['enable_fortran_source']:
	env.Append(FORTRANFLAGS=' -module '+build_dir)
	env.Append(F90FLAGS=' -module '+build_dir)



######################
# get source code files
######################

env.src_files = []

Export('env')
SConscript('src/SConscript', variant_dir=build_dir, duplicate=0)
Import('env')

print
print 'Building program "'+program_name+'"'
print


env.Program('build/'+program_name, env.src_files)

