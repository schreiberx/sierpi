/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreading.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_HPP_
#define CMAINTHREADING_HPP_


// IPMO
#if COMPILE_WITH_IPMO
	#include "CMainThreadingIPMO.hpp"
	#include "CMainThreading_LockOMP.hpp"
#else

// IOMP
#if COMPILE_WITH_IOMP
	#include "CMainThreadingIOMP.hpp"
	#include "CMainThreading_LockOMP.hpp"
#else

// OMP
#if COMPILE_WITH_OMP
	#include "CMainThreadingOMP.hpp"
	#include "CMainThreading_LockOMP.hpp"
#else

// ITBB
#if COMPILE_WITH_ITBB
	#include "CMainThreadingITBB.hpp"
	#include "CMainThreading_LockTBB.hpp"
#else

// TBB
#if COMPILE_WITH_TBB
	#include "CMainThreadingTBB.hpp"
	#include "CMainThreading_LockTBB.hpp"
#else

// DUMMY
	#include "CMainThreadingDummy.hpp"
	#include "CMainThreading_LockDummy.hpp"
#endif
#endif
#endif
#endif
#endif


#endif /* CMAINTHREADING_HPP_ */
