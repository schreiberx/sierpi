/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTriangle_StringEnums.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRIANGLE_STRINGENUMS_HPP_
#define CTRIANGLE_STRINGENUMS_HPP_


/**
 * \brief methods to get a string representation of an enumeration state
 */
class CTriangle_StringEnums
{
public:
	static
	const char* getTriangleTypeString(	CTriangle_Enums::ETriangleTypes triangleType	)
	{
		switch(triangleType)
		{
			case CTriangle_Enums::TRIANGLE_TYPE_K:				return "K";
			case CTriangle_Enums::TRIANGLE_TYPE_H:				return "H";
			case CTriangle_Enums::TRIANGLE_TYPE_V:				return "V";
			default:											return "Invalid";
		}
		return "";
	}

	static
	const char* getTraversalDirectionString(	CTriangle_Enums::EDirection traversalDirection	)
	{
		switch(traversalDirection)
		{
			case CTriangle_Enums::DIRECTION_FORWARD:	return "FORWARD";
			case CTriangle_Enums::DIRECTION_BACKWARD:	return "BACKWARD";
			default:									return "Invalid";
		}
		return "";
	}

	static
	const char* getEdgeTypeString(	CTriangle_Enums::EEdgeType edgeType	)
	{
		switch(edgeType)
		{
			case CTriangle_Enums::EDGE_TYPE_NEW:		return "new";
			case CTriangle_Enums::EDGE_TYPE_OLD:		return "old";
			case CTriangle_Enums::EDGE_TYPE_BOUNDARY:		return "border";
			default:									return "Invalid";
		}
		return "";
	}


	static
	const char* getNormalString(	CTriangle_Enums::ENormals normal	)
	{
		switch(normal)
		{
			case CTriangle_Enums::NORMAL_N:			return "N";
			case CTriangle_Enums::NORMAL_NE:		return "NE";
			case CTriangle_Enums::NORMAL_E:			return "E";
			case CTriangle_Enums::NORMAL_SE:		return "SE";
			case CTriangle_Enums::NORMAL_S:			return "S";
			case CTriangle_Enums::NORMAL_SW:		return "SW";
			case CTriangle_Enums::NORMAL_W:			return "W";
			case CTriangle_Enums::NORMAL_NW:		return "NW";
			default:								return "Invalid";
		}
		return "";
	}


	static
	const char* getEvenOddString(	CTriangle_Enums::EEvenOdd evenOdd	)
	{
		switch(evenOdd)
		{
			case CTriangle_Enums::EVEN:				return "EVEN";
			case CTriangle_Enums::ODD:				return "ODD";
			default:								return "Invalid";
		}
		return "";
	}
};

#endif /* CTRIANGLE_STRINGENUMS_H_ */
