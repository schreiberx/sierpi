/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_
#define CSIMULATION_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "lib/CLittleBigEndian.hpp"
#include "../../types/CTsunamiTypes.hpp"

#include "../../CSimulationTsunami_GridDataArrays.hpp"

namespace sierpi {
namespace kernels {

class CSimulationTsunami_OutputGridDataArrays {
public:
	typedef typename CTsunamiSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiVertexScalar T;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<
			CSimulationTsunami_OutputGridDataArrays,
			CTsunamiSimulationStacksAndTypes> TRAV;

	CMainThreading_Lock lock;

private:
	CSimulationTsunami_GridDataArrays *cGridDataArrays;

	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing;

public:
	inline void op_cell(
			TVertexScalar i_vertex_coord_left_x,	TVertexScalar i_vertex_coord_left_y,
			TVertexScalar i_vertex_coord_right_x,	TVertexScalar i_vertex_coord_right_y,
			TVertexScalar i_vertex_coord_top_x,		TVertexScalar i_vertex_coord_top_y,

			TVertexScalar i_normal_hyp_x, TVertexScalar i_normal_hyp_y,
			TVertexScalar i_normal_right_x, TVertexScalar i_normal_right_y,
			TVertexScalar i_normal_left_x, TVertexScalar i_normal_left_y,

			CCellData *i_cCellData
	) {

		TTsunamiVertexScalar *v;
		if (output_flags & CSimulationTsunami_GridDataArrays::VERTICES)
		{
			v = &(cGridDataArrays->triangle_vertex_buffer[cell_write_index * 3 * 3]);

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			v[0 * 3 + 0] = i_vertex_coord_left_x;
			v[0 * 3 + 1] = i_vertex_coord_left_y;
			v[0 * 3 + 2] = 0;
			v[1 * 3 + 0] = i_vertex_coord_right_x;
			v[1 * 3 + 1] = i_vertex_coord_right_y;
			v[1 * 3 + 2] = 0;
			v[2 * 3 + 0] = i_vertex_coord_top_x;
			v[2 * 3 + 1] = i_vertex_coord_top_y;
			v[2 * 3 + 2] = 0;
#endif

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS>=1
			if (preprocessing == 0)
			{
				v[0 * 3 + 0] = i_vertex_coord_left_x;
				v[0 * 3 + 1] = i_vertex_coord_left_y;
				v[0 * 3 + 2] = 0;
				v[1 * 3 + 0] = i_vertex_coord_right_x;
				v[1 * 3 + 1] = i_vertex_coord_right_y;
				v[1 * 3 + 2] = 0;
				v[2 * 3 + 0] = i_vertex_coord_top_x;
				v[2 * 3 + 1] = i_vertex_coord_top_y;
				v[2 * 3 + 2] = 0;
			}
			else
			{
				TVertexScalar hyp_edge_x = (i_vertex_coord_left_x+i_vertex_coord_right_x)*(T)0.5;
				TVertexScalar hyp_edge_y = (i_vertex_coord_left_y+i_vertex_coord_right_y)*(T)0.5;

				TVertexScalar right_edge_x = (i_vertex_coord_right_x+i_vertex_coord_top_x)*(T)0.5;
				TVertexScalar right_edge_y = (i_vertex_coord_right_y+i_vertex_coord_top_y)*(T)0.5;

				TVertexScalar left_edge_x = (i_vertex_coord_top_x+i_vertex_coord_left_x)*(T)0.5;
				TVertexScalar left_edge_y = (i_vertex_coord_top_y+i_vertex_coord_left_y)*(T)0.5;

				TVertexScalar hyp_dx = right_edge_x - left_edge_x;
				TVertexScalar hyp_dy = right_edge_y - left_edge_y;

				TVertexScalar hyp_dhb = i_cCellData->dofs_right_edge.h+i_cCellData->dofs_right_edge.b - (i_cCellData->dofs_left_edge.h+i_cCellData->dofs_left_edge.b);

				TVertexScalar left_dx = right_edge_x - hyp_edge_x;
				TVertexScalar left_dy = right_edge_y - hyp_edge_y;
				T left_dhb = i_cCellData->dofs_right_edge.h+i_cCellData->dofs_right_edge.b - (i_cCellData->dofs_hyp_edge.h+i_cCellData->dofs_hyp_edge.b);


				v[0*3+0] = hyp_edge_x-hyp_dx;
				v[0*3+1] = (hyp_edge_y-hyp_dy);
				v[0*3+2] = (i_cCellData->dofs_hyp_edge.h+i_cCellData->dofs_hyp_edge.b) - hyp_dhb;

				v[1*3+0] = hyp_edge_x+hyp_dx;
				v[1*3+1] = (hyp_edge_y+hyp_dy);
				v[1*3+2] = (i_cCellData->dofs_hyp_edge.h+i_cCellData->dofs_hyp_edge.b) + hyp_dhb;

				v[2*3+0] = left_edge_x+left_dx;
				v[2*3+1] = (left_edge_y+left_dy);
				v[2*3+2] = (i_cCellData->dofs_left_edge.h+i_cCellData->dofs_left_edge.b)+left_dhb;
			}
#endif
		}

		if (output_flags & CSimulationTsunami_GridDataArrays::NORMALS)
		{
			TTsunamiVertexScalar *n = &(cGridDataArrays->triangle_normal_buffer[cell_write_index * 3 * 3]);

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			n[0*3+0] = 0;
			n[0*3+1] = 0;
			n[0*3+2] = 1;
			n[1*3+0] = 0;
			n[1*3+1] = 0;
			n[1*3+2] = 1;
			n[2*3+0] = 0;
			n[2*3+1] = 0;
			n[2*3+2] = 1;
#endif


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS >= 1
			if (preprocessing == 0)
			{
				n[0 * 3 + 0] = 0;
				n[0 * 3 + 1] = 0;
				n[0 * 3 + 2] = 1;
				n[1 * 3 + 0] = 0;
				n[1 * 3 + 1] = 0;
				n[1 * 3 + 2] = 1;
				n[2 * 3 + 0] = 0;
				n[2 * 3 + 1] = 0;
				n[2 * 3 + 2] = 1;
			}
			else
			{
				assert(output_flags & CSimulationTsunami_GridDataArrays::VERTICES);

				TVertexScalar bx = (v[0*3+0]-v[1*3+0]);
				TVertexScalar by = (v[0*3+1]-v[1*3+1]);
				TVertexScalar bz = (v[0*3+2]-v[1*3+2]);

				TVertexScalar cx = (v[0*3+0]-v[2*3+0]);
				TVertexScalar cy = (v[0*3+1]-v[2*3+1]);
				TVertexScalar cz = (v[0*3+2]-v[2*3+2]);

				TVertexScalar nx = by*cz-bz*cy;
				TVertexScalar ny = bz*cx-bx*cz;
				TVertexScalar nz = bx*cy-by*cx;

				TVertexScalar a = (T)1.0/CMath::sqrt<TVertexScalar>(nx*nx + ny*ny + nz*nz);

				nx *= a;
				ny *= a;
				nz *= a;

				n[0*3+0] = nx;
				n[0*3+1] = ny;
				n[0*3+2] = nz;

				n[1*3+0] = nx;
				n[1*3+1] = ny;
				n[1*3+2] = nz;

				n[2*3+0] = nx;
				n[2*3+1] = ny;
				n[2*3+2] = nz;
			}
#endif
		}

		/*
		 * H
		 */
		if (output_flags & CSimulationTsunami_GridDataArrays::H)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			TVertexScalar h = i_cCellData->dofs_center.h;
#else
			TVertexScalar h = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.h + i_cCellData->dofs_left_edge.h + i_cCellData->dofs_right_edge.h);
#endif

			cGridDataArrays->h[cell_write_index] = h;
		}

		/*
		 * MOMENTUM HU
		 * MOMENTUM HV
		 */
		if (output_flags & (CSimulationTsunami_GridDataArrays::HU | CSimulationTsunami_GridDataArrays::HV))
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			TVertexScalar hu = i_cCellData->dofs_center.hu;
			TVertexScalar hv = i_cCellData->dofs_center.hv;
#else
			TVertexScalar hu = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.hu + i_cCellData->dofs_left_edge.hu + i_cCellData->dofs_right_edge.hu);
			TVertexScalar hv = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.hv + i_cCellData->dofs_left_edge.hv + i_cCellData->dofs_right_edge.hv);
#endif

			CProjections::rotateToBasisWithXAxis(&hu, &hv, -i_normal_right_x, -i_normal_right_y);

			if (output_flags & CSimulationTsunami_GridDataArrays::HU)
				cGridDataArrays->hu[cell_write_index] = hu;

			if (output_flags & CSimulationTsunami_GridDataArrays::HV)
				cGridDataArrays->hv[cell_write_index] = hv;
		}

		/*
		 * BATHYMETRY
		 */
		if (output_flags & CSimulationTsunami_GridDataArrays::B)
		{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
			TVertexScalar b = i_cCellData->dofs_center.b;
#else
			TVertexScalar b = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.b + i_cCellData->dofs_left_edge.b + i_cCellData->dofs_right_edge.b);
#endif

			cGridDataArrays->b[cell_write_index] = b;
		}


		/*
		 * cfl_domain_size_div_max_wave_speed
		 */
		if (output_flags & CSimulationTsunami_GridDataArrays::CFL_VALUE_HINT)
		{
			TVertexScalar cfl_domain_size_div_max_wave_speed;

			if (i_cCellData->cfl_domain_size_div_max_wave_speed > 100000000000.0)
				cfl_domain_size_div_max_wave_speed = 0;
			else
				cfl_domain_size_div_max_wave_speed = i_cCellData->cfl_domain_size_div_max_wave_speed;

			cGridDataArrays->cfl_value_hint[cell_write_index] = cfl_domain_size_div_max_wave_speed;
		}

		cell_write_index++;
	}

	inline void traversal_pre_hook() {
	}

	inline void traversal_post_hook() {
	}

	inline void setup(
			CSimulationTsunami_GridDataArrays *o_cFileOutput_Block,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing = 0
	) {
		cGridDataArrays = o_cFileOutput_Block;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
		preprocessing = i_preprocessing;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			CSimulationTsunami_OutputGridDataArrays &parent_kernel) {
		assert(false);
	}
};

#undef VISUALIZATION_SIMPLE
#undef VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS
#undef VISUALIZATION_ALIGNED_TRIANGLES

}
}

#endif
;
