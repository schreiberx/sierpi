/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_2ND_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_2ND_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData_Depth.hpp"
#include "../common/CAdaptivity_2ndOrder.hpp"
#include "../../CTsunamiSimulationScenarios.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 2nd order
 */
template <typename t_CSimulationStacksAndTypes>
class CSetup_CellData_2ndOrder	: public CAdaptivity_2ndOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_CellData_Depth<CSetup_CellData_2ndOrder, t_CSimulationStacksAndTypes>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiDataScalar T;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CEdgeData CEdgeData;


	/*.
	 * callback for terrain data
	 */
	CTsunamiSimulationScenarios *cSimulationScenarios;

	bool initial_grid_setup;


	CSetup_CellData_2ndOrder()	:
		cSimulationScenarios(nullptr),
		initial_grid_setup(true)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	void updateDOF(
			TTsunamiVertexScalar mx, TTsunamiVertexScalar my,
			CTsunamiSimulationDOFs &io_dofsData,
			int i_depth
		)
	{
		io_dofsData.b = cSimulationScenarios->getTerrainHeightByPosition(mx, my, i_depth);


		if (cSimulationScenarios->getUseMaximumFunctionForSurfaceHeightSetup() && !initial_grid_setup)
		{
			// fix height with bathymetry displacement
			TTsunamiDataScalar oldb = io_dofsData.b;
			io_dofsData.h += (oldb - io_dofsData.b);

			io_dofsData.h = CMath::max(io_dofsData.h, -io_dofsData.b + cSimulationScenarios->getWaterSurfaceHeightByPosition(mx, my, i_depth));
		}
		else
		{
			io_dofsData.h = -io_dofsData.b + cSimulationScenarios->getWaterSurfaceHeightByPosition(mx, my, i_depth);
		}
	}


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			int i_depth,

			CTsunamiSimulationCellData *io_cellData
	)
	{
		TVertexScalar mx, my;

		// hyp edge
		mx = (TTsunamiVertexScalar)0.5*(vleft_x+vright_x);
		my = (TTsunamiVertexScalar)0.5*(vleft_y+vright_y);
		updateDOF(mx, my, io_cellData->dofs_hyp_edge, i_depth);
		updateDOF(mx, my, io_cellData->support_points[3], i_depth);

		// right edge
		mx = (TTsunamiVertexScalar)0.5*(vright_x+vtop_x);
		my = (TTsunamiVertexScalar)0.5*(vright_y+vtop_y);
		updateDOF(mx, my, io_cellData->dofs_right_edge, i_depth);
		updateDOF(mx, my, io_cellData->support_points[5], i_depth);

		// left edge
		mx = (TTsunamiVertexScalar)0.5*(vleft_x+vtop_x);
		my = (TTsunamiVertexScalar)0.5*(vleft_y+vtop_y);
		updateDOF(mx, my, io_cellData->dofs_left_edge, i_depth);
		updateDOF(mx, my, io_cellData->support_points[1], i_depth);

		// support points
		updateDOF(vtop_x, vtop_y, io_cellData->support_points[0], i_depth);
		updateDOF(vleft_x, vleft_y, io_cellData->support_points[2], i_depth);
		updateDOF(vright_x, vright_y, io_cellData->support_points[4], i_depth);
//		mx = (TTsunamiVertexScalar)(1/3.)*(vleft_x+vtop_x+vright_x);
//		my = (TTsunamiVertexScalar)(1/3.)*(vleft_y+vtop_y+vright_y);
//		for(int i=0; i<SIMULATION_TSUNAMI_NUMBER_OF_DOFS; i++) {
//			updateDOF(mx, my, io_cellData->support_points[i], i_depth);
//		}

		io_cellData->cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void setup_Parameters(
			CTsunamiSimulationScenarios *i_cSimulationScenarios,
			bool i_initial_grid_setup
	)
	{
		cSimulationScenarios = i_cSimulationScenarios;
		initial_grid_setup = i_initial_grid_setup;
	}



	void setup_WithKernel(
			CSetup_CellData_2ndOrder<t_CSimulationStacksAndTypes> &i_parent
	)
	{
		cSimulationScenarios = i_parent.cSimulationScenarios;
		initial_grid_setup = i_parent.initial_grid_setup;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
