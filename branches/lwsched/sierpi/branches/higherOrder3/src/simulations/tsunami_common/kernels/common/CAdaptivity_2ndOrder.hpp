/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_2NDORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_2NDORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "../../CProjections.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_2ndOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiSimulationEdgeData CEdgeData;


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationScenarios *cSimulationScenarios;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;

	/**
	 * constructor
	 */
	CAdaptivity_2ndOrder()	:
		cathetus_side_length(-1),
		cSimulationScenarios(nullptr),
		cfl_domain_size_div_max_wave_speed(-1)
	{

	}


	void traversal_pre_hook()
	{
//		std::cout << "LKJASLKDFJ" << std::endl;
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}


	/**
	 * get center of weight for triangle
	 */
	inline void computeCenterOfWeight(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);
	}



	/**
	 * get center of weight for triangle for both children
	 */
	inline void computeCenterOfWeightForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiSimulationCellData *i_cellData,
			CTsunamiSimulationCellData *o_left_cellData,
			CTsunamiSimulationCellData *o_right_cellData
	)
	{
		assert(false && "No refinement for 2nd order!");
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cellData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif
		// left element
		o_left_cellData->dofs_hyp_edge = i_cellData->dofs_left_edge;
		o_left_cellData->dofs_right_edge = i_cellData->dofs_hyp_edge;
		o_left_cellData->dofs_left_edge.h = (i_cellData->dofs_left_edge.h+i_cellData->dofs_right_edge.h)*(T)0.5;
		o_left_cellData->dofs_left_edge.hu = (i_cellData->dofs_left_edge.hu+i_cellData->dofs_right_edge.hu)*(T)0.5;
		o_left_cellData->dofs_left_edge.hv = (i_cellData->dofs_left_edge.hv+i_cellData->dofs_right_edge.hv)*(T)0.5;
		o_left_cellData->dofs_left_edge.b = (i_cellData->dofs_left_edge.b+i_cellData->dofs_right_edge.b)*(T)0.5;

		o_left_cellData->support_points[1] = i_cellData->support_points[0];
		o_left_cellData->support_points[2] = i_cellData->support_points[1];
		o_left_cellData->support_points[0].h = 0.5*(i_cellData->support_points[0].h+i_cellData->support_points[2].h);
		o_left_cellData->support_points[0].hu = 0.5*(i_cellData->support_points[0].hu+i_cellData->support_points[2].hu);
		o_left_cellData->support_points[0].hv = 0.5*(i_cellData->support_points[0].hv+i_cellData->support_points[2].hv);
		o_left_cellData->support_points[0].b = 0.5*(i_cellData->support_points[0].b+i_cellData->support_points[2].b);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_left_cellData->refine = false;
		o_left_cellData->coarsen = false;
#endif

		// right element
		o_right_cellData->dofs_hyp_edge = i_cellData->dofs_right_edge;
		o_right_cellData->dofs_right_edge = o_left_cellData->dofs_left_edge;
		o_right_cellData->dofs_left_edge = i_cellData->dofs_hyp_edge;

		o_right_cellData->support_points[1] = i_cellData->support_points[2];
		o_right_cellData->support_points[2] = i_cellData->support_points[0];
		o_right_cellData->support_points[0] = i_cellData->support_points[1];

		assert(cfl_domain_size_div_max_wave_speed != -1);

		T local_cfl_domain_size_div_max_wave_speed = i_cellData->cfl_domain_size_div_max_wave_speed*(T)(1.0/CMath::sqrt(2.0));

		o_left_cellData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_cellData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		o_left_cellData->refine = false;
		o_left_cellData->coarsen = false;
		o_right_cellData->refine = false;
		o_right_cellData->coarsen = false;
#endif

		assert(cfl_domain_size_div_max_wave_speed != -1);

		/*
		 * left cell data
		 */
		CProjections::changeBasisWithXAxis(
				&o_left_cellData->dofs_hyp_edge.hu,
				&o_left_cellData->dofs_hyp_edge.hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);

		CProjections::changeBasisWithXAxis(
				&o_left_cellData->dofs_right_edge.hu,
				&o_left_cellData->dofs_right_edge.hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);

		CProjections::changeBasisWithXAxis(
				&o_left_cellData->dofs_left_edge.hu,
				&o_left_cellData->dofs_left_edge.hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);


		/*
		 * right cell data
		 */
		CProjections::changeBasisWithXAxis(
				&o_right_cellData->dofs_hyp_edge.hu,
				&o_right_cellData->dofs_hyp_edge.hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		CProjections::changeBasisWithXAxis(
				&o_right_cellData->dofs_right_edge.hu,
				&o_right_cellData->dofs_right_edge.hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		CProjections::changeBasisWithXAxis(
				&o_right_cellData->dofs_left_edge.hu,
				&o_right_cellData->dofs_left_edge.hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cellData->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				i_depth,
				&o_left_cellData->validation,
				&o_right_cellData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CTsunamiSimulationCellData *o_cellData,
			CTsunamiSimulationCellData *i_left_cellData,
			CTsunamiSimulationCellData *i_right_cellData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			T mx = (vleft_x + vright_x)*(T)0.5;
			T my = (vleft_y + vright_y)*(T)0.5;

			i_left_cellData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_cellData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif
		assert(false && "No refinement for 2nd order!");
		o_cellData->dofs_left_edge = i_left_cellData->dofs_hyp_edge;
		CProjections::changeBasisWithXAxis(
				&o_cellData->dofs_left_edge.hu,
				&o_cellData->dofs_left_edge.hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);
		CProjections::changeBasisWithXAxis(
				&o_cellData->dofs_left_edge.hu,
				&o_cellData->dofs_left_edge.hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);

		o_cellData->dofs_right_edge = i_right_cellData->dofs_hyp_edge;
		CProjections::changeBasisWithXAxis(
				&o_cellData->dofs_right_edge.hu,
				&o_cellData->dofs_right_edge.hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		o_cellData->dofs_hyp_edge.h = (i_left_cellData->dofs_right_edge.h+i_right_cellData->dofs_left_edge.h)*(T)0.5;

		T shared_left_hu = i_left_cellData->dofs_right_edge.hu;
		T shared_left_hv = i_left_cellData->dofs_right_edge.hv;
		CProjections::changeBasisWithXAxis(
				&shared_left_hu,
				&shared_left_hv,
				-(T)M_SQRT1_2,
				-(T)M_SQRT1_2
			);

		T shared_right_hu = i_right_cellData->dofs_left_edge.hu;
		T shared_right_hv = i_right_cellData->dofs_left_edge.hv;
		CProjections::changeBasisWithXAxis(
				&shared_right_hu,
				&shared_right_hv,
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);


		o_cellData->dofs_hyp_edge.hu = (shared_left_hu+shared_right_hu)*(T)0.5;
		o_cellData->dofs_hyp_edge.hv = (shared_left_hv+shared_right_hv)*(T)0.5;
		o_cellData->dofs_hyp_edge.b = (i_left_cellData->dofs_right_edge.b+i_right_cellData->dofs_left_edge.b)*(T)0.5;

		o_cellData->cfl_domain_size_div_max_wave_speed = (i_left_cellData->cfl_domain_size_div_max_wave_speed + i_right_cellData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*CMath::sqrt(2.0));

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_cellData->refine = false;
		o_cellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_cellData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_cellData->validation,
				&i_right_cellData->validation
		);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
