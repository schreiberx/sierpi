/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CASAGI_HPP_
#define CASAGI_HPP_


#if !CONFIG_ENABLE_MPI
	#define ASAGI_NOMPI
#endif

#include "asagi.h"
#include "../../types/CTsunamiTypes.hpp"
#include "../CDataSet_Interface.hpp"


/**
 * dataset interface to asagi
 */
class CAsagi
/*
:
	public CDataSet_Interface
*/
{
private:
	typedef CTsunamiSimulationTypes::T T;

public:
	void getOriginAndSize(
			TTsunamiDataScalar	*o_origin_x,
			TTsunamiDataScalar	*o_origin_y,
			TTsunamiDataScalar	*o_size_x,
			TTsunamiDataScalar	*o_size_y
	);


	/**
	 * get nodal data (surface height, momentums and bathymetry) for given coordinate
	 */
	void getNodalData(
			TTsunamiDataScalar i_x,
			TTsunamiDataScalar i_y,
			int i_level_of_detail,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	/**
	 * get displacement data for given coordinate
	 */
	TTsunamiDataScalar getDisplacementData(
			TTsunamiDataScalar i_x,		///< x-coordinate in model-space
			TTsunamiDataScalar i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);


	/**
	 * get displacement data for world-space coordinate
	 */
	TTsunamiDataScalar getDisplacementDataWorldSpace(
			TTsunamiDataScalar i_x,		///< x-coordinate in world-space
			TTsunamiDataScalar i_y,		///< x-coordinate in world-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);



	/**
	 * get bathymetry data for given coordinate
	 */
	TTsunamiDataScalar getBathymetryData(
			TTsunamiDataScalar i_x,		///< x-coordinate in model-space
			TTsunamiDataScalar i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);

	TTsunamiDataScalar getWaterSufaceData(
			TTsunamiDataScalar i_x,		///< x-coordinate in model-space
			TTsunamiDataScalar i_y,		///< x-coordinate in model-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	);

	/**
	 * get boundary data at given position
	 */
	void getBoundaryData(
			TTsunamiDataScalar i_x,
			TTsunamiDataScalar i_y,
			int i_level_of_detail,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	/**
	 * get benchmark (analytical) nodal data
	 */
	void getBenchmarkNodalData(
			TTsunamiDataScalar i_x,
			TTsunamiDataScalar i_y,
			int i_level_of_detail,
			TTsunamiDataScalar i_timestamp,
			CTsunamiSimulationDOFs *o_nodal_data
	);


	bool isDatasetLoaded();

	void outputVerboseInformation();


	bool loadDataset(
			const char *i_filename_bathymetry,		///< filename for bathymetry data
			const char *i_filename_displacements	///< filename for displacement data
	);


	CAsagi();
	~CAsagi();


private:

//public:
	T bathymetry_size_x;
	T bathymetry_size_y;

	T bathymetry_min_x;
	T bathymetry_min_y;

	T bathymetry_max_x;
	T bathymetry_max_y;

	T bathymetry_center_x;
	T bathymetry_center_y;


	T displacements_size_x;
	T displacements_size_y;

	T displacements_min_x;
	T displacements_min_y;

	T displacements_max_x;
	T displacements_max_y;

	T displacements_center_x;
	T displacements_center_y;

	/**
	 * set to true when setup was successful
	 */
	bool is_dataset_loaded;

	/**
	 * singleton for bathymetry datasets
	 */
	asagi::Grid *bathymetry_grid;

	/**
	 * singleton for displacements
	 */
	asagi::Grid *displacements_grid;

};

#endif /* CASAGI_HPP_ */
