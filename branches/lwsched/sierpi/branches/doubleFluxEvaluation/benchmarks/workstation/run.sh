#! /bin/bash

. ../inc_environment_vars.sh

EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

PARAMS=""

# [-d initial recursion depth]
PARAMS="$PARAMS -d 8"

# [-v verbose]
#PARAMS="$PARAMS -v 1"

#[-n number of threads to use]
PARAMS="-n 4"

#[-t timesteps]
PARAMS="$PARAMS -t 6500"
#PARAMS="$PARAMS -t 100"

#[-s timestep size]
PARAMS="$PARAMS -s 20"

#[-f output .vtk files each #nth timestep]
#PARAMS="$PARAMS -f 200"

#[-w world scene]
PARAMS="$PARAMS -w 3"

#[-i min relative recursion depth]
PARAMS="$PARAMS -i 0"

#[-a max relative recursion depth]
PARAMS="$PARAMS -a 6"

seqPow2 () {
	S=$1
	while [ $S -le $2 ]; do
		OUTPUT="$OUTPUT $S"
		S=$(($S*2))
	done

	echo "$OUTPUT"
}

getValue () {
	echo "`cat \"$1\" | grep \" $2 \" | sed \"s/ $2 .*//\"`"
}

TESTCASE=2

case $TESTCASE in
1 )
	#
	# TEST SEVERAL PARTITION SPLIT/JOIN SIZES
	#

	PARTITION_SPLIT_SIZES=`seqPow2 4 $((4096*6))`	

	echo "SplitSize	\"Mega Triangles Per Second\"	\"Triangles Processed\"	\"Average Seconds per Timestep\"	\"Triangles Processed in Average per Simulation Timestep\""

	for PARTITION_SPLIT_SIZE in $PARTITION_SPLIT_SIZES; do
		_PARAMS="$PARAMS -o $PARTITION_SPLIT_SIZE"

		#[-l partition size when to request join (both childs have to request a join)]
		_PARAMS="$_PARAMS -l $(($PARTITION_SPLIT_SIZE/2))"

		# be a little bit verbose
		_PARAMS="$_PARAMS -v 1"

		# extend parameters
		_PARAMS="$_PARAMS $@"

		DIR="split_size_`printf \"%05d\" $PARTITION_SPLIT_SIZE`"
		mkdir -p "$DIR"

		# RUN SIMULATION
		cd "$DIR";
		../$EXEC_BIN $_PARAMS > "output.txt";
		cd ..;

		OUTPUTFILE="$DIR/output.txt"

		MTPS=`getValue "$OUTPUTFILE" MTPS`
		TP=`getValue "$OUTPUTFILE" TP`
		TPST=`getValue "$OUTPUTFILE" TPST`
		ASPT=`getValue "$OUTPUTFILE" ASPT`
		echo "$PARTITION_SPLIT_SIZE	$MTPS	$TP	$ASPT	$TPST"
	done
	;;
2 )
	#
	# TEST DIFFERENT DIFFERENT NUMBER OF THREADS
	#

	echo "Threads	\"Mega Triangles Per Second\"	\"Triangles Processed\"	\"Average Seconds per Timestep\"	\"Triangles Processed in Average per Simulation Timestep\""

	# TODO
	for NUMBER_OF_THREADS in 1 2 3 4; do
#	for NUMBER_OF_THREADS in 4; do
		# [-d initial recursion depth]
		PARAMS="$PARAMS -d 10"

		# TODO: fix me!!! timestep size
		PARAMS="$PARAMS -s 5"

		# TODO OUTPUT
#		PARAMS="$PARAMS -f 800"

		# TODO TIMESTEPS
		PARAMS="$PARAMS -t $((3*48000))"

		_PARAMS="$PARAMS -n $NUMBER_OF_THREADS"

		# be a little bit verbose
		_PARAMS="$_PARAMS -v 1"

		# extend parameters
		_PARAMS="$_PARAMS $@"

		DIR="number_of_threads_`printf \"%05d\" $NUMBER_OF_THREADS`"
		mkdir -p "$DIR"

		# INTEL AFFINITY
		export KMP_AFFINITY="proclist=[0,1,2,3],explicit"
#		export KMP_AFFINITY="proclist=[0,1,2,3],scatter"
#		export KMP_AFFINITY="verbose,proclist=[0,1,2,3],explicit"

		# RUN SIMULATION
		cd "$DIR";
		../$EXEC_BIN $_PARAMS > "output.txt";
		cd ..;

		OUTPUTFILE="$DIR/output.txt"

		MTPS=`getValue "$OUTPUTFILE" MTPS`
		TP=`getValue "$OUTPUTFILE" TP`
		TPST=`getValue "$OUTPUTFILE" TPST`
		ASPT=`getValue "$OUTPUTFILE" ASPT`
		echo "$NUMBER_OF_THREADS	$MTPS	$TP	$ASPT	$TPST"
	done
	;;


3 )
	#[-o partition size when to request split]
	PARTITION_SPLIT_SIZES=`seq 10 50 1000`

	STRING_MATCH="Mega Triangles per second"

	for PARTITION_SPLIT_SIZE in $PARTITION_SPLIT_SIZES; do
		_PARAMS="$PARAMS -o $PARTITION_SPLIT_SIZE"

		#[-l partition size when to request join (both childs have to request a join)]
		_PARAMS="$_PARAMS -l $(($PARTITION_SPLIT_SIZE/2))"

		# extend parameters
		_PARAMS="$_PARAMS $@"

		DIR="split_size_$PARTITION_SPLIT_SIZE"
		mkdir -p "$DIR"
		OUTPUT=`cd "$DIR"; $EXEC_BIN $_PARAMS`

		MTPS=`echo "$OUTPUT" | grep "$STRING_MATCH"`
		echo $PARTITION_SPLIT_SIZE	$MTPS
	done
	;;
esac
