/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationCommon.hpp
 *
 *  Created on: Jul 5, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include <stdint.h>
#include "lib/CError.hpp"

#ifndef CSIMULATIONCOMMON_HPP_
#define CSIMULATIONCOMMON_HPP_


/**
 * simulation parameters used by serial and parallel simulation
 */
class CSimulationCommon
{
public:
	/**
	 * true, if the simulation should be driven
	 */
	bool run_simulation;

	/**
	 * true, if some error happened. Then no method should be executed
	 */
	CError error;


	/**
	 * timesteps which should be simulated per frame
	 */
	int timesteps_per_frame;

	/**
	 * initial recursion depth during setup
	 */
	int initial_recursion_depth;

	/**
	 * minimum and maximum recursion depth during refinements / coarsenings
	 */
	int min_relative_recursion_depth;
	int max_relative_recursion_depth;

	/**
	 * thresholds for coarsening/refinement
	 */
	TTsunamiDataScalar coarsen_height_threshold;
	TTsunamiDataScalar refine_height_threshold;

	TTsunamiDataScalar coarsen_slope_threshold;
	TTsunamiDataScalar refine_slope_threshold;

	/**
	 * element data during setup
	 */
	CTsunamiElementData element_data_setup;

	/**
	 * Dirichlet boundary edge data
	 */
	CTsunamiEdgeData dirichlet_boundary_edge_data;

	/**
	 * element data to be modified by external event (e. g. user input)
	 */
	CTsunamiElementData element_data_modifier;

	/**
	 * the time-step size
	 */
	float timestep_size;

	/**
	 * adaptively set the timestep size by using CFL condition
	 */
	bool adaptive_timestep_size;


	/**
	 * number of current timestep
	 */
	unsigned long long timestep_nr;

	/**
	 * time for current timestep
	 */
	double time_for_timestep;

	/**
	 * if this variable is set to true, the timestep size is manually specified
	 */
	bool timestepSize_override;

	/**
	 * domain length of unit square
	 */
	TTsunamiDataScalar domain_length;

	/**
	 * overall number of triangles in domain
	 */
	unsigned long long number_of_triangles;


	/**
	 * number of simulation handler partitions
	 */
	unsigned int number_of_simulation_handler_partitions;


	/**
	 * number of triangles when to split the partition
	 */
	unsigned long long partition_split_workload_size;

	/**
	 * number of overall triangles of 2 partitions when to join the partition
	 */
	unsigned long long partition_join_workload_size;

	/**
	 * Set the number of triangles in a single partition when to split/join
	 */
	int partition_update_split_join_size_after_elapsed_timesteps;

	/**
	 * Scalar to use for non-table based computation of optimal split/join size
	 */
	int partition_update_split_join_size_after_elapsed_scalar;


	int world_scene_id;


	/**
	 * value to setup the fluid surface height
	 */
	TTsunamiDataScalar surface_base_height;

	/**
	 * value to setup the column height
	 */
	TTsunamiDataScalar surface_max_height;


	/**
	 * setup initial recursion depth
	 */
public:
	void setInitialRecursionDepth(int p_initial_recursion_depth)
	{
		initial_recursion_depth = p_initial_recursion_depth;
	}


	/**
	 * compute the optimum timestep size for given parameters
	 *
	 * TODO: this should be automatically set via the CFL condition
	 */
public:
	void computeTimestepSize(
			TTsunamiDataScalar p_timestep_size = -1.0
	)
	{
		if (p_timestep_size == -1.0)
		{
			adaptive_timestep_size = true;
		}
		else
		{
			adaptive_timestep_size = false;
		}

		timestep_size = p_timestep_size;

		timesteps_per_frame = (int)(1.0/(timestep_size*30.0));

		if (timesteps_per_frame <= 0)
			timesteps_per_frame = 1;
	}

	/**
	 * setup relative minimum recursion depth
	 */
public:
	void setMinRelativeRecursionDepth(int p_min_recursion_depth)
	{
		assert(min_relative_recursion_depth <= 0);
		min_relative_recursion_depth = p_min_recursion_depth;
	}


	/**
	 * setup relative maximum recursion depth
	 */
public:
	void setMaxRelativeRecursionDepth(int p_max_recursion_depth)
	{
		max_relative_recursion_depth = p_max_recursion_depth;
	}


	/**
	 * Set the scene id which has to be setup
	 */
public:
	void setWorldSceneId(int i_world_scene_id)
	{
		world_scene_id = i_world_scene_id;
	}


	/**
	 * Set the number of triangles in a single partition when to split/join
	 */
public:
	void setSplitJoinSizes(
			unsigned long long i_partition_split_workload_size,
			unsigned long long i_partition_join_workload_size
		)
	{
		partition_split_workload_size = i_partition_split_workload_size;
		partition_join_workload_size = i_partition_join_workload_size;
	}


	/**
	 * Set the number of elapsed timesteps to update the
	 */
public:
	void setElapsedTimestepsForAutomaticSplitJoinUpdates(
			int i_elapsed_timesteps,				///< update split/join sizes after this number of timesteps
			double i_elapsed_timesteps_scalar = 0	///< scalar to use for automatic (not table based) computation of best splitting size
		)
	{
		partition_update_split_join_size_after_elapsed_timesteps = i_elapsed_timesteps;
		partition_update_split_join_size_after_elapsed_scalar = i_elapsed_timesteps_scalar;
	}


	/**
	 * return random number between 0 and 1
	 */
public:
	double getRand01()
	{
		return ((double)::random())*(1.0/(double)RAND_MAX);
	}


public:
	inline void outputVerboseInformation()
	{
		std::cout << " + Flux solver: " << SIMULATION_TSUNAMI_FLUX_SOLVER_STRING << std::endl;
		std::cout << " + Order of basis functions: " << SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS << std::endl;
	}


public:
	void resetCommon()
	{
		timestep_nr = 0;
		time_for_timestep = 0;

		timestepSize_override = false;
	}


public:
	CSimulationCommon()	:
		run_simulation(false),
		initial_recursion_depth(6),
		min_relative_recursion_depth(0),
		max_relative_recursion_depth(6),
		timestep_size(-1),
		adaptive_timestep_size(true),
		domain_length(100.0),
		number_of_triangles(0),
		number_of_simulation_handler_partitions(0),
		partition_split_workload_size(1024*2),
		partition_join_workload_size(1024),
		partition_update_split_join_size_after_elapsed_timesteps(0),
		partition_update_split_join_size_after_elapsed_scalar(0.0),
		world_scene_id(3)
	{
		surface_base_height = 3000;
		surface_max_height = surface_base_height+(TTsunamiDataScalar)1.0;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		element_data_modifier.dofs.h = surface_max_height;
		element_data_modifier.dofs.qx = 0;
		element_data_modifier.dofs.qy = 0;
		element_data_modifier.dofs.b = -surface_base_height;
#else
		element_data_modifier.hyp_edge.h = surface_max_height;
		element_data_modifier.hyp_edge.qx = 0;
		element_data_modifier.hyp_edge.qy = 0;
		element_data_modifier.hyp_edge.b = -surface_base_height;

		element_data_modifier.left_edge.h = surface_max_height;
		element_data_modifier.left_edge.qx = 0;
		element_data_modifier.left_edge.qy = 0;
		element_data_modifier.left_edge.b = -surface_base_height;

		element_data_modifier.right_edge.h = surface_max_height;
		element_data_modifier.right_edge.qx = 0;
		element_data_modifier.right_edge.qy = 0;
		element_data_modifier.right_edge.b = -surface_base_height;
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		element_data_modifier.refine = false;
		element_data_modifier.coarsen = false;
#endif


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		element_data_setup.dofs.h = surface_base_height;
		element_data_setup.dofs.qx = 0;
		element_data_setup.dofs.qy = 0;
		element_data_setup.dofs.b = -surface_base_height;

		dirichlet_boundary_edge_data.h = surface_base_height;
		dirichlet_boundary_edge_data.qx = 0;
		dirichlet_boundary_edge_data.qy = 0;
		dirichlet_boundary_edge_data.b = -surface_base_height;

#else

		element_data_setup.hyp_edge.h = surface_base_height;
		element_data_setup.hyp_edge.qx = 0;
		element_data_setup.hyp_edge.qy = 0;
		element_data_setup.hyp_edge.b = -surface_base_height;

		element_data_setup.left_edge.h = surface_base_height;
		element_data_setup.left_edge.qx = 0;
		element_data_setup.left_edge.qy = 0;
		element_data_setup.left_edge.b = -surface_base_height;

		element_data_setup.right_edge.h = surface_base_height;
		element_data_setup.right_edge.qx = 0;
		element_data_setup.right_edge.qy = 0;
		element_data_setup.right_edge.b = -surface_base_height;

		dirichlet_boundary_edge_data.h = surface_base_height;
		dirichlet_boundary_edge_data.qx = 0;
		dirichlet_boundary_edge_data.qy = 0;
		dirichlet_boundary_edge_data.b = -surface_base_height;
#endif

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		element_data_setup.refine = false;
		element_data_setup.coarsen = false;
#endif

		/*
		 * adaptive stuff
		 */

		/*
		 * surface height
		 */
		coarsen_height_threshold = surface_base_height+0.01;
		refine_height_threshold = surface_base_height+0.02;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		coarsen_slope_threshold = -1;
		refine_slope_threshold = -1;
#else
		/*
		 * threshold for 1st order is given by surface slope
		 */
		TTsunamiDataScalar unit_slope = CMath::abs((element_data_modifier.hyp_edge.h-dirichlet_boundary_edge_data.h)/element_data_modifier.hyp_edge.h);
		coarsen_slope_threshold	= 0.002*unit_slope;
		refine_slope_threshold	= 0.003*unit_slope;
#endif

		resetCommon();
	}
};

#endif
