/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_TYPES_0TH_ORDER_HPP
#define CTSUNAMI_TYPES_0TH_ORDER_HPP

#include "CTsunamiTypes_Validate.hpp"


class CTsunamiEdgeData
{
public:
	TTsunamiDataScalar h;	// height
	TTsunamiDataScalar qx;	// x-component of velocity
	TTsunamiDataScalar qy;	// y-component of velocity
	TTsunamiDataScalar b;	// bathymetry

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif


	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiEdgeData &d)
	{
		return os << d.h << " " << d.qx << " " << d.qy

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
				<< d.validation
#endif
		;
	}
};



class CTsunamiElementData
{
public:
	CTsunamiEdgeData dofs;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
	#if 0
		unsigned refine:1;
		unsigned coarsen:1;
	#else
		bool refine;
		bool coarsen;
	#endif
#endif


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_ElementData validation;
#endif


	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CTsunamiElementData &d)
	{
		os << d.dofs << std::endl;
		return os;
	}
};


#endif
