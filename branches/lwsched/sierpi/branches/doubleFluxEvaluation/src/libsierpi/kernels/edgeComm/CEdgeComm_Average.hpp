/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 12, 2011
 *      Author: schreibm
 */

#ifndef CKERNEL_CEDGECOMM_AVERAGE_HPP_
#define CKERNEL_CEDGECOMM_AVERAGE_HPP_

#include "libmath/CMath.hpp"
#include "libsierpi/traversators/edgeComm/CEdgeComm.hpp"

namespace sierpi
{
namespace kernels
{

template <typename TElementData, typename TEdgeData>
class CEdgeComm_Average
{
public:
	typedef sierpi::travs::CEdgeComm<CEdgeComm_Average<TElementData, TEdgeData>, TElementData, TEdgeData> TRAV;

	void setup()
	{
	}

	CEdgeComm_Average()
	{
		setup();
	}

	inline void storeLeftEdgeCommData(
			TElementData *element_data,
			TEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = (TEdgeData)0.5 * *element_data;
	}

	inline void storeRightEdgeCommData(
			TElementData *element_data,
			TEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = (TEdgeData)0.5 * *element_data;
	}

	inline void storeHypCommData(
			TElementData *element_data,
			TEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = (TEdgeData)0.5 * *element_data;
	}


	void pre_hook(CStack<char> &p_structure_stack)
	{
	}


	void post_hook(CStack<char> &p_structure_stack)
	{
	}


	void elementAction(
			TElementData *element,
			TEdgeData *hyp_edge,
			TEdgeData *right_edge,
			TEdgeData *left_edge
	)
	{
		*element =	(TEdgeData)(1.0/(1.0+1.0+CMath::sqrt2<TEdgeData>()))*
					(
						(TEdgeData)(0.5*CMath::sqrt2<TEdgeData>()) * ((TEdgeData)*element + *hyp_edge) +
						(TEdgeData)0.5 * ((TEdgeData)*element + *left_edge) +
						(TEdgeData)0.5 * ((TEdgeData)*element + *right_edge)
					);
	}


	void elementAction_BEE(
			TElementData *element,
			TEdgeData *right_edge,
			TEdgeData *left_edge
	)
	{
		*element =	(TEdgeData)(1.0/2.0)*
					(
						(TEdgeData)0.5 * ((TEdgeData)*element + *left_edge) +
						(TEdgeData)0.5 * ((TEdgeData)*element + *right_edge)
					);
	}

	void elementAction_EBE(
			TElementData *element,
			TEdgeData *hyp_edge,
			TEdgeData *left_edge
	)
	{
		*element =	(TEdgeData)(1.0/(1.0+CMath::sqrt2<TEdgeData>()))*
					(
						(TEdgeData)(0.5*CMath::sqrt2<TEdgeData>()) * ((TEdgeData)*element + *hyp_edge) +
						(TEdgeData)0.5 * ((TEdgeData)*element + *left_edge)
					);
	}

	void elementAction_EEB(
			TElementData *element,
			TEdgeData *hyp_edge,
			TEdgeData *right_edge
	)
	{
		*element =	(TEdgeData)(1.0/(1.0+CMath::sqrt2<TEdgeData>()))*
					(
						(TEdgeData)(0.5*CMath::sqrt2<TEdgeData>()) * ((TEdgeData)*element + *hyp_edge) +
						(TEdgeData)0.5 * ((TEdgeData)*element + *right_edge)
					);
	}


	void elementAction_EBB(
			TElementData *element,
			TEdgeData *hyp_edge
	)
	{
		*element =	(TEdgeData)(1.0/CMath::sqrt2<TEdgeData>())*
					(
						(TEdgeData)(0.5*CMath::sqrt2<TEdgeData>()) * ((TEdgeData)*element + *hyp_edge)
					);
	}

};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
