/*
 * CSSEIntrinsicHelpers.h
 *
 *  Created on: Nov 9, 2011
 *      Author: schreibm
 */

#ifndef CSSEINTRINSICHELPERS_H_
#define CSSEINTRINSICHELPERS_H_



// gnu compiler
#define SSEALIGN16		__attribute__ ((aligned (16)))

// MS$!@#$ compiler
//#define SSEALIGN16	__declspec(align(16))




inline __m128 inv(__m128 vec)
{
	return _mm_xor_ps(vec, (__m128)_mm_set1_epi32(0x80000000));
}



#endif /* CSSEINTRINSICHELPERS_H_ */
