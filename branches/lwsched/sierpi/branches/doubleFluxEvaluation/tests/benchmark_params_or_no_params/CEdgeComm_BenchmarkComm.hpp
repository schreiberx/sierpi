/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: schreibm
 *
 *
 * Class to test the correct edge communication
 *
 * The adjacent edge center as well as the edge normal
 * is transmitted.
 */

#ifndef CEDGECOMM_BENCHMARK_COMM_HPP_
#define CEDGECOMM_BENCHMARK_COMM_HPP_


#include "CMath.hpp"
#include "CEdgeComm_Vertices_Normals.hpp"
#include "CValidateTypes.hpp"

namespace sierp
{
namespace kernels
{

class CEdgeComm_BenchmarkComm
{
public:
	// dummy data
	typedef sierp::travs::CEdgeComm_Vertices_Normals<CEdgeComm_BenchmarkComm, CValElementData, CValEdgeData> TRAV;

	typedef float TVertexScalar;

	void setup()
	{
	}

	CEdgeComm_BenchmarkComm()
	{
		setup();
	}


	inline void initBorderVertices(TVertexScalar v[4][2])
	{
		v[0][0] = -1;
		v[0][1] = 1;

		v[1][0] = 1;
		v[1][1] = 1;

		v[2][0] = 1;
		v[2][1] = -1;

		v[3][0] = -1;
		v[3][1] = -1;

	}

	inline void storeLeftEdgeCommData(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data
	)
	{
		dst_comm_data->nx = n2x;
		dst_comm_data->ny = n2y;

		dst_comm_data->mx = (v0x+v2x)*0.5;
		dst_comm_data->my = (v0y+v2y)*0.5;
	}

	inline void storeRightEdgeCommData(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n1x;
		dst_comm_data->ny = n1y;

		dst_comm_data->mx = (v1x+v2x)*0.5;
		dst_comm_data->my = (v1y+v2y)*0.5;
	}

	inline void storeHypCommData(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n0x;
		dst_comm_data->ny = n0y;

		dst_comm_data->mx = (v0x+v1x)*0.5;
		dst_comm_data->my = (v0y+v1y)*0.5;
	}


	void pre_hook(CStack<char> &p_structure_stack)
	{
	}


	void post_hook(CStack<char> &p_structure_stack)
	{
	}

	inline void checkNormal(
						TVertexScalar n0x, TVertexScalar n0y,
						CValEdgeData *e)
	{

		TVertexScalar a = n0x*e->ny - n0y*e->nx;
		if (CMath<TVertexScalar>::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: INVALID NORMAL" << std::endl;
	}

	inline void checkRightMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
		TVertexScalar mx = (v1x+v2x)*0.5;
		TVertexScalar my = (v1y+v2y)*0.5;

		TVertexScalar a = CMath<TVertexScalar>::abs(mx-e->mx) + CMath<TVertexScalar>::abs(my-e->my);

		if (CMath<TVertexScalar>::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: RIGHT EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);
//		std::cout << "Right: " << m << " " << ed->right << std::endl;

		if (CMath<TVertexScalar>::abs((m-ed->right).getLength()) > 0.0001)
		{
			std::cout << "EDGECOMM VALIDATION FAILED: RIGHT EDGE MIDPOINT compared with element data" << std::endl;
			std::cout << m << " " << ed->right << std::endl;
		}
	}


	inline void checkLeftMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
		TVertexScalar mx = (v0x+v2x)*0.5;
		TVertexScalar my = (v0y+v2y)*0.5;

		TVertexScalar a = CMath<TVertexScalar>::abs(mx-e->mx) + CMath<TVertexScalar>::abs(my-e->my);

		if (CMath<TVertexScalar>::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: LEFT EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);
//		std::cout << "Right: " << m << " " << ed->right << std::endl;

		if (CMath<TVertexScalar>::abs((m-ed->left).getLength()) > 0.0001)
		{
			std::cout << "EDGECOMM VALIDATION FAILED: LEFT EDGE MIDPOINT compared with element data" << std::endl;
			std::cout << m << " " << ed->left << std::endl;
		}
	}



	inline void checkHypMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
		TVertexScalar mx = (v0x+v1x)*0.5;
		TVertexScalar my = (v0y+v1y)*0.5;


		TVertexScalar a = CMath<TVertexScalar>::abs(mx-e->mx) + CMath<TVertexScalar>::abs(my-e->my);

		if (CMath<TVertexScalar>::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: HYP EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);
//		std::cout << "Hyp: " << m << " " << ed->hyp << std::endl;

		if (CMath<TVertexScalar>::abs((m-ed->hyp).getLength()) > 0.0001)
		{
			std::cout << "EDGECOMM VALIDATION FAILED: HYP EDGE MIDPOINT compared with element data" << std::endl;
			std::cout << m << " " << ed->hyp << std::endl;
		}
	}

	inline void elementAction_EEE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *right_edge,
			CValEdgeData *left_edge
	)
	{
		/*
		 * This is just to include the normals in the calculations
		 */
		element->hyp.data[0] += hyp_edge->mx*n0x;
		element->hyp.data[1] += hyp_edge->my*n0y;

		element->left.data[0] += left_edge->mx*n1x;
		element->left.data[1] += left_edge->my*n1y;

		element->right.data[0] += right_edge->mx*n2x;
		element->right.data[1] += right_edge->my*n2y;
/*
 * Don't do anything
 */
		//checkNormal(n0x, n0y, hyp_edge);
		//checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

		//checkNormal(n1x, n1y, right_edge);
		//checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);

		//checkNormal(n2x, n2y, left_edge);
		//checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}


	inline void elementAction_BEE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *right_edge,
			CValEdgeData *left_edge
	)
	{
		/*
		 * This is just to include the normals in the calculations
		 */
		*element->hyp.data=*element->hyp.data*n0x*n0y;
/*
 * Don't do anything
 */
//		checkNormal(n1x, n1y, right_edge);
//		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);

//		checkNormal(n2x, n2y, left_edge);
//		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}

	inline void elementAction_EBE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *left_edge
	)
	{
		/*
		 * This is just to include the normals in the calculations
		 */
		*element->hyp.data=*element->hyp.data*n0x*n0y;
		/*
		 * Don't do anything
		 */
//		checkNormal(n0x, n0y, hyp_edge);
//		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

//		checkNormal(n2x, n2y, left_edge);
//		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}

	inline void elementAction_EEB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *right_edge
	)
	{
		/*
		 * This is just to include the normals in the calculations
		 */
		*element->hyp.data=*element->hyp.data*n0x*n0y;
		/*
	     * Don't do anything
	 	 */
//		checkNormal(n0x, n0y, hyp_edge);
//		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

//		checkNormal(n1x, n1y, right_edge);
//		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);
	}


	inline void elementAction_EBB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge
	)
	{
		/*
		 * This is just to include the normals in the calculations
		 */
		*element->hyp.data=*element->hyp.data*n0x*n0y;
		/*
		 * Don't do anything
		 */
//		checkNormal(n0x, n0y, hyp_edge);
//		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);
	}

};

}
}


#endif /* CEDGECOMM_TESTNORMAL_HPP_ */
