/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CBENCHMARK_SINGLE_WAVE_ON_SINGLE_BEACH_HPP
#define CBENCHMARK_SINGLE_WAVE_ON_SINGLE_BEACH_HPP


template <typename T>
class CSingleWaveOnSingleBeach
{
	/**
	 * see
	 * "Standards, Criteria, and Procedures for NOAA Evaluation of Tsunami Numerical Models"
	 * page 26ff. for more details about this benchmark.
	 */
public:
	T Hd;		// dimensional: initial max wave height
	T beach_slope;	// beach slope
	T Xsd;		// dimensional: midpoint of initial wave

	T H;		// non-dimensional: initial max wave height
	T Xs;		// non-dimensional: wave midpoint
	T X0;		// non-dimensional: start position of beach

	T gamma_dimensionless;	// gamma value used for surface wave elevation

	T simulation_domain_length;		// length of simulation domain
	T dimensionless_scale_factor;	// scale factor for dimensionless space-parameters
	T simulation_domain_translate;	// displacement of simulation domain relative in real-world-space



	CSingleWaveOnSingleBeach()
	{
		setup();
	}


	void setup(
			T i_simulation_domain_length = 1.0					///< domain length to scale the height (x-coordinate is given normalized)
	)
	{
		// midpoint of wave
		Xs = 40.0;

		// height of wave
		H = 0.0185;

		// start of beach
		X0 = 19.85;

		// angle of beach
		// d = 1 (dimensionless)
		beach_slope = 1.0/X0;

		gamma_dimensionless = CMath::sqrt(0.25*3.0*H);


		simulation_domain_length = i_simulation_domain_length;
		dimensionless_scale_factor = i_simulation_domain_length/(Xs*0.8);
		simulation_domain_translate = simulation_domain_length*0.8;

		Xs *= dimensionless_scale_factor;
		X0 *= dimensionless_scale_factor;

		H *= dimensionless_scale_factor;

#if 0
		std::cout << std::endl;
		std::cout << "simulation_domain_length: " << simulation_domain_length << std::endl;
		std::cout << "dimensionless_scale_factor: " << dimensionless_scale_factor << std::endl;
		std::cout << "simulation_domain_translate: " << simulation_domain_translate << std::endl;

		std::cout << std::endl;
		std::cout << "beach_slope: " << beach_slope << std::endl;
		std::cout << "Xs (midpoint of initial wave): " << Xs << std::endl;
		std::cout << "X0 (start of beach slope): " << X0 << std::endl;
		std::cout << "H: " << H << std::endl;
		std::cout << "gamma_dimensionless: " << gamma_dimensionless << std::endl;

		std::cout << std::endl;
#endif
	}


	inline T getTerrainHeightByPosition(T i_x)
	{
		T x = i_x*simulation_domain_length + simulation_domain_translate;

		if (x > X0)
			return -dimensionless_scale_factor;
#if 0
		std::cout << (X0-x) << std::endl;
		std::cout << (X0-x)*beach_slope << std::endl;
		std::cout << std::endl;
#endif

		return (X0-x)*beach_slope-dimensionless_scale_factor;
	}

	inline T getWaterSurfaceHeightByPosition(T i_x)
	{
		T x = i_x*simulation_domain_length + simulation_domain_translate;

		// parameter for sech(x)
		T sech_x = gamma_dimensionless * ((x - Xs)/dimensionless_scale_factor);

		T sech_tanh_x = CMath::tanh(sech_x);

		T sech = 1.0 - sech_tanh_x*sech_tanh_x;

		T h = H * sech*sech;

		return h;
	}
};

#endif
