/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_TSUNAMI_PARALLEL_HPP_
#define CSIMULATION_TSUNAMI_PARALLEL_HPP_

#include "CSimulationTsunami_Parallel_SubPartitionHandler.hpp"
#include "CSimulationTsunami_DomainSetups.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"
#include "libmath/CVector.hpp"
#include "lib/CStopwatch.hpp"


#include "libsierpi/parallelization/CStackAccessorMethods.hpp"
#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelAdaptivityTraversals.hpp"
#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelEdgeCommTraversals.hpp"

#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER==2
	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals_RK2.hpp"
#else
	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals.hpp"
#endif

#include "libsierpi/domain_triangulation/CBaseTriangulation_To_GenericTree.hpp"

#include "libsierpi/parallelization/CSplitJoinTuning.hpp"

#include "CSplitJoin_TsunamiTuningTable.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "libgl/shaders/shader_blinn/CShaderBlinn.hpp"
	#include "libgl/shaders/shader_height_color_blinn/CShaderHeightColorBlinn.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Element_Splats_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Root_Tsunami.hpp"

	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/types/CTsunamiTypes.hpp"
	#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif

#include "../tsunami_common/CTsunamiSimulationDataSets.hpp"

#include "../tsunami_common/kernels/modifiers/CSetup_ElementData.hpp"


/**
 * \brief Main class for parallel Tsunami Simulation
 *
 * This class is the central point of a parallel Tsunami simulation.
 *
 * It manages all sub-partitions, creates the initial domain triangulation and
 * sets up the simulation.
 */
class CSimulationTsunami_Parallel	: public CTsunamiSimulationParameters
{
	/**
	 * Convenient typedefs
	 */
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_SubPartitionHandler> CPartition_TreeNode_;

	/*
	 * Typedefs. among others used by partition handler
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;
	typedef TTsunamiVertexScalar TVertexScalar;
	typedef CGenericTreeNode<CPartition_TreeNode_> CGenericTreeNode_;

public:
	/**
	 * Base domain triangulation
	 */
	CDomain_BaseTriangulation<CSimulationTsunami_Parallel_SubPartitionHandler> cDomain_RootTriangulation;


	/**
	 * Pointer to root generic node
	 */
	CGenericTreeNode_ *rootGenericTreeNode;


	TTsunamiDataScalar cfl_helper_reduce_value;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Element_Splats_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Element_Splats_Root_Tsunami;
	COpenGL_Vertices_Element_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Element_Root_Tsunami;
	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;

	CGlVertexArrayObject render_partitions_vao;
	CGlBuffer render_partitions_buffer;
#endif


	/**
	 * tuning for the split and join parameters
	 */
	CSplitJoinTuning<CSplitJoin_TsunamiTuningTable> cSplitJoinTuning;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CTsunamiSimulationDataSets cTsunamiSimulationDataSets;


	/**
	 * Start the parallel Tsunami simulation
	 */
	CSimulationTsunami_Parallel()	:
		 rootGenericTreeNode(nullptr),
		 cTsunamiSimulationDataSets((CTsunamiSimulationParameters&)*this)
	{
		 number_of_triangles = 0;
		 number_of_initial_triangles_after_domain_triangulation = 0;
	}


	/**
	 * Deconstructor
	 */
	virtual ~CSimulationTsunami_Parallel()
	{
		// free generic tree
		freeGenericTreeNode();
	}


	/**
	 * free generic tree node data
	 */
	void freeGenericTreeNode()
	{
		/**
		 * free generic tree
		 */
		if (rootGenericTreeNode != nullptr)
		{
			rootGenericTreeNode->freeChilds();

			delete rootGenericTreeNode;
			rootGenericTreeNode = nullptr;
		}
	}



	/**
	 * Reset the simulation
	 */
	void reset_Simulation()
	{
		number_of_triangles = 0;

		freeGenericTreeNode();

		/*
		 * reset the world: setup triangulation of "scene"
		 */
		p_setup_World_PartitionTreeNodes_SimulationPartitionHandlers(simulation_world_scene_id);

		cfl_helper_reduce_value = 0;

		/***************************************************************************************
		 * STACKS: setup the stacks (only the memory allocation) of the partitions
		 ***************************************************************************************/
		unsigned int stackInitializationFlags =
					CSimulationStacks_Enums::ELEMENT_STACKS							|
					CSimulationStacks_Enums::ADAPTIVE_STACKS						|
					CSimulationStacks_Enums::EDGE_COMM_STACKS						|
					CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS;

#if COMPILE_SIMULATION_WITH_GUI
		stackInitializationFlags |=
					CSimulationStacks_Enums::VERTEX_COMM_STACKS						|
					CSimulationStacks_Enums::VERTEX_COMM_PARALLEL_EXCHANGE_STACKS;
#endif

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->resetStacks(stackInitializationFlags, grid_initial_recursion_depth);
				}
		);


		/***************************************************************************************
		 * SETUP STRUCTURE STACK and ELEMENT DATA
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					// setup structure stack and element data to default values
					sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;
					node->resetStacks(stackInitializationFlags, grid_initial_recursion_depth);

					cSetup_Structure_ElementData.setup(node->cStacks, grid_initial_recursion_depth, &element_data_setup);

					// setup element data with respect to vertex positions
					sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
					cSetup_ElementData.setup_sfcMethods(node->cTriangleFactory);
					cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets);
					cSetup_ElementData.action(node->cStacks);
				}
		);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		/***************************************************************************************
		 * SETUP vertex coordinates in element data for debugging purposes
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cSetup_TsunamiElementData_Validation.action(node->cStacks);
				}
			);
#endif

		updateSubpartitionParameters();

#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Element_Splats_Root_Tsunami.setupTexture(512, 512);
		cOpenGL_Element_Splats_Root_Tsunami.setupRegion(cDomain_RootTriangulation.region);

		if (cOpenGL_Element_Splats_Root_Tsunami.error())
		{
			std::cout << cOpenGL_Element_Splats_Root_Tsunami.error << std::endl;
		}

		render_partitions_vao.bind();
			render_partitions_buffer.bind();
			render_partitions_buffer.resize(6*3*sizeof(GLfloat));

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);
		render_partitions_vao.unbind();
#endif
		resetSimulationParameters();
	}


private:
	/**
	 * Setup:
	 *  - new world triangulation
	 *  - root partition tree nodes
	 *  - simulation partition handlers
	 */
	void p_setup_World_PartitionTreeNodes_SimulationPartitionHandlers(
			int world_id = 0	///< world id to set-up
	)
	{
		cDomain_RootTriangulation.clear();

		switch(world_id)
		{
			case -4:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2DCube_PeriodicBoundaries(cDomain_RootTriangulation);			break;
			case -3:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small_even_and_odd.svg", 0.5);		break;
			case -2:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_Strip_PeriodicBoundaries(cDomain_RootTriangulation, 10);		break;
			case -1:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_QuadQuad_PeriodicBoundaries(cDomain_RootTriangulation);			break;

			case 0:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_Triangle(cDomain_RootTriangulation);			break;
			case 1:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles1(cDomain_RootTriangulation);		break;
			case 2:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles2(cDomain_RootTriangulation);		break;

			case 3:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_Quad(cDomain_RootTriangulation);				break;
			case 4:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2EvenTriangles1(cDomain_RootTriangulation);		break;
			case 5:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2EvenTriangles1(cDomain_RootTriangulation);		break;
			case 6:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_2EvenTriangles2(cDomain_RootTriangulation);		break;
			case 7:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_3Triangles(cDomain_RootTriangulation);			break;
			case 8:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_QuadTiles(cDomain_RootTriangulation, 1, 1);		break;
			case 9:		simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_TriangleStrip(cDomain_RootTriangulation);		break;
			case 10:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small.svg", 0.5);		break;
			case 11:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small_odd.svg", 0.5);	break;
			case 12:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain.svg");					break;
			case 13:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_triangle.svg", 2.0);			break;
			case 14:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_cross.svg", 0.5);		break;

			default:	simulation_parameter_root_partitions_per_x_axis_on_unit_domain = CSimulationTsunami_DomainSetups::setupTriangulation_QuadTiles(cDomain_RootTriangulation, 1, 1);		break;
		}


		/*
		 * remove triangles which are available for domain triangulations with odd depth
		 */
		if (grid_initial_recursion_depth & 1)
		{
			cDomain_RootTriangulation.fixForOddDepth();
		}


		/*
		 * setup the adjacency informations
		 */
		cDomain_RootTriangulation.setup_AdjacencyInformation();


		/*
		 * convert to generic tree
		 */
		CBaseTriangulation_To_GenericTree<CSimulationTsunami_Parallel_SubPartitionHandler> cBaseTriangulation_To_GenericTree;

		if (rootGenericTreeNode != nullptr)
			delete rootGenericTreeNode;

		number_of_triangles = cBaseTriangulation_To_GenericTree.setup_GenericTree_From_BaseTriangulation(
				cDomain_RootTriangulation,		///< root triangles
				grid_initial_recursion_depth,		///< initial recursion depth
				grid_initial_recursion_depth-grid_min_relative_recursion_depth,
				grid_initial_recursion_depth+grid_max_relative_recursion_depth,
				rootGenericTreeNode						///< reference to rootNode handed back!
			);

		number_of_initial_triangles_after_domain_triangulation = number_of_triangles;

		assert(rootGenericTreeNode != nullptr);
	}



private:
	/**
	 * reset the partition handlers to specific values.
	 *
	 * especially care about the initialization of the structure stacks!
	 */
	void p_reset_SubPartitionStacks()
	{
	}


/***************************************************************************************
 * ADAPTIVITY
 ***************************************************************************************/
private:
	/**
	 * run adaptive traversal
	 */
	void p_adaptive_traversal_simulation()
	{
		CHelper_GenericParallelAdaptivityTraversals::action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cTsunami_Adaptive,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				splitting_partition_split_workload_size,
				splitting_partition_join_workload_size,
				&number_of_triangles,
				&cfl_helper_reduce_value
			);
	}

	unsigned long long p_adaptive_traversal_setup_column()
	{
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cSetup_Column.setup_KernelClass(
							simulation_dataset_cylinder_posx,
							simulation_dataset_cylinder_posy,
							simulation_dataset_cylinder_radius,

							2,	// REFINE ONLY
							&cTsunamiSimulationDataSets
						);
				}
			);

		unsigned long long prev_number_of_triangles;

		do
		{
			prev_number_of_triangles = number_of_triangles;

			CHelper_GenericParallelAdaptivityTraversals::action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cSetup_Column,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				splitting_partition_split_workload_size,
				splitting_partition_join_workload_size,
				&number_of_triangles,
				&cfl_helper_reduce_value
			);

		} while (prev_number_of_triangles != number_of_triangles);

		/**
		 * finally setup element data values
		 */
		int backupSetupSurfaceMethod = simulation_water_surface_scene_id;
		simulation_water_surface_scene_id = CTsunamiSimulationDataSets::SIMULATION_WATER_HEIGHT_CYLINDER_OUTER_CYLINDER_MINUS_INF;

		setupElementData();

		simulation_water_surface_scene_id = backupSetupSurfaceMethod;

		return number_of_triangles;
	}


	/**
	 * setup element data specified by simulation_terrain_scene_id and simulation_water_surface_scene_id
	 */
public:
	void setupElementData()
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					// setup element data with respect to vertex positions
					sierpi::kernels::CSetup_ElementData::TRAV cSetup_ElementData;
					cSetup_ElementData.setup_sfcMethods(node->cTriangleFactory);
					cSetup_ElementData.cKernelClass.setup_Parameters(&cTsunamiSimulationDataSets, true);
					cSetup_ElementData.action(node->cStacks);
				}
		);
	}



/***************************************************************************************
 * MODIFY_SINGLE_ELEMENT
 ***************************************************************************************/
private:
	/**
	 * set element data at coordinate at (x,y) to *elementData
	 */
	void p_set_element_data_at_coordinate(
			TTsunamiVertexScalar x,				///< x-coordinate inside of specific triangle
			TTsunamiVertexScalar y,				///< y-coordinate inside of specific triangle
			CTsunamiElementData *i_elementData	///< set element data at (x,y) to this data
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

					cModify_OneElementValue_SelectByPoint.setup_sfcMethods(node->cTriangleFactory);
					cModify_OneElementValue_SelectByPoint.cKernelClass.setup(
							x,
							y,
							i_elementData
						);

					cModify_OneElementValue_SelectByPoint.action(node->cStacks);
				}
			);
	}



	/***************************************************************************************
	 * FLUX COMM
	 ***************************************************************************************/
private:
	void p_flux_comm_simulation()
	{
		if (adaptive_timestep_size)
		{
			// adaptive timestep size
			simulation_parameter_timestep_size = cfl_helper_reduce_value*simulation_parameter_cfl;

			if (simulation_parameter_timestep_size < 0.00000001)
				simulation_parameter_timestep_size = 0.00000001;
		}
#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 2
		CHelper_GenericParallelFluxCommTraversals_RK2::action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cTsunami_EdgeComm,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeFluxCommData_Tsunami,
				rootGenericTreeNode,
				simulation_parameter_timestep_size,
				&cfl_helper_reduce_value
			);
#else
		CHelper_GenericParallelFluxCommTraversals::action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cTsunami_EdgeComm,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeFluxCommData_Tsunami,
				rootGenericTreeNode,
				simulation_parameter_timestep_size,
				&cfl_helper_reduce_value
			);
#endif
	}



	/***************************************************************************************
	 * UPDATE SUBPARTITION SIMULATION PARAMETERS
	 *
	 * This has to be executed whenever the simulation parameters are updated
	 ***************************************************************************************/

public:
	void updateSubpartitionParameters()
	{
		/***************************************************************************************
		 * EDGE COMM: setup boundary parameters
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cTsunami_EdgeComm.setBoundaryDirichlet(&dirichlet_boundary_edge_data);
				}
			);


		/***************************************************************************************
		 * EDGE COMM: setup generic parameters
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cTsunami_EdgeComm.setParameters(simulation_parameter_timestep_size, simulation_parameter_domain_length/simulation_parameter_root_partitions_per_x_axis_on_unit_domain, simulation_parameter_gravitation);
				}
			);


		/***************************************************************************************
		 * ADAPTIVITY
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{

					worker->cTsunami_Adaptive.setup_KernelClass(
							simulation_parameter_domain_length/simulation_parameter_root_partitions_per_x_axis_on_unit_domain,

							refine_height_threshold,
							coarsen_height_threshold,

							refine_slope_threshold,
							coarsen_slope_threshold,
							&cTsunamiSimulationDataSets
						);
				}
			);

	}



/***************************************************************************************
 * SPLIT/JOINS for SETUP
 *
 * This splits all sub-partitions into appropriate sizes for the initialization.
 ***************************************************************************************/

public:
	void setup_SplitJoinPartitions()
	{
		unsigned long long prev_number_of_triangles = 0;

		while (prev_number_of_triangles != number_of_triangles)
		{
			p_adaptive_traversal_simulation();
			prev_number_of_triangles = number_of_triangles;
		}

		splitOrJoinPartitions();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


public:
	inline void updateSplitJoinSizes()
	{
		if (partition_update_split_join_size_after_elapsed_timesteps != 0)
		{
			if (timestep_nr % partition_update_split_join_size_after_elapsed_timesteps == 0)
			{
				if (partition_update_split_join_size_after_elapsed_scalar != 0)
				{
					/**
					 * compute the split/join sizes by using `partition_update_split_join_size_after_elapsed_scalar`
					 */
					splitting_partition_split_workload_size = std::sqrt((double)number_of_triangles)*partition_update_split_join_size_after_elapsed_scalar;
					splitting_partition_join_workload_size = splitting_partition_split_workload_size / 2;
				}
				else
				{
					/**
					 * lookup the best split/join in a table
					 */
					cSplitJoinTuning.updateSplitJoin(
							splitting_partition_split_workload_size,
							splitting_partition_join_workload_size,
							number_of_triangles
						);
				}
			}
		}

	}



/***************************************************************************************
 * TIMESTEP
 ***************************************************************************************/

public:
	inline void runSingleTimestep()
	{
		// simulation timestep
		p_flux_comm_simulation();

		// adaptive timestep
		p_adaptive_traversal_simulation();

		// split/join operations
		splitOrJoinPartitions();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		timestep_nr++;
		time_for_timestep += simulation_parameter_timestep_size;

		updateSplitJoinSizes();
	}


	CStopwatch cStopwatch;

	/***************************************************************************************
	 * run a single TIMESTEP and update the detailed benchmarks
	 ***************************************************************************************/
public:
	/**
	 * execute a single time-step
	 */
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,	///< add time taken for edge communication traversal to this value
			double *io_adaptiveTime,	///< add time taken for adaptive traversal to this value
			double *io_splitJoinTime	///< add time taken for split/joins to this value
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_flux_comm_simulation();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_adaptive_traversal_simulation();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		// split/join operations
		cStopwatch.start();
		splitOrJoinPartitions();
		*io_splitJoinTime += cStopwatch.getTimeSinceStart();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		timestep_nr++;
		time_for_timestep += simulation_parameter_timestep_size;

		updateSplitJoinSizes();
	}


/***************************************************************************************
 * REFINE / COARSEN
 ***************************************************************************************/

public:
	/**
	 * setup column at 2d position with given radius
	 */
	unsigned long long setup_ColumnAt2DPosition(
			TTsunamiVertexScalar i_x,			///< x-coordinate of center of column to set-up
			TTsunamiVertexScalar i_y,			///< y-coordinate of center of column to set-up
			TTsunamiVertexScalar i_radius		///< radius of column to setup
	)
	{
		simulation_dataset_cylinder_posx = i_x;
		simulation_dataset_cylinder_posy = i_y;
		simulation_dataset_cylinder_radius = i_radius;

		return p_adaptive_traversal_setup_column();
	}


	unsigned long long setup_ColumnAt2DPosition()
	{
		return p_adaptive_traversal_setup_column();
	}



public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,		///< x-coordinate of center of column to set-up
			TTsunamiVertexScalar y,		///< y-coordinate of center of column to set-up
			TTsunamiVertexScalar radius = 0.3		///< radius of column to set-up
	)
	{
		CTsunamiElementData element_data_modifier;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		element_data_modifier.dofs.h = simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance;
		element_data_modifier.dofs.qx = 0;
		element_data_modifier.dofs.qy = 0;
		element_data_modifier.dofs.b = -simulation_dataset_terrain_default_distance;

#else
		element_data_modifier.hyp_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.hyp_edge.qx = 0;
		element_data_modifier.hyp_edge.qy = 0;
		element_data_modifier.hyp_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.left_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.left_edge.qx = 0;
		element_data_modifier.left_edge.qy = 0;
		element_data_modifier.left_edge.b = -simulation_dataset_terrain_default_distance;

		element_data_modifier.right_edge.h = simulation_dataset_terrain_default_distance+simulation_dataset_water_surface_default_displacement;
		element_data_modifier.right_edge.qx = 0;
		element_data_modifier.right_edge.qy = 0;
		element_data_modifier.right_edge.b = -simulation_dataset_terrain_default_distance;
#endif

		p_set_element_data_at_coordinate(x, y, &element_data_modifier);
	}


/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	/**
	 * output currently stored triangles to vtk file
	 */
	void writeTrianglesToVTKFile(
			const char *p_filename	///< filename to write vtk files to
	)
	{
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(p_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << grid_initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_handler_partitions << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;
		vtkfile << "ASCII" << std::endl;
		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_triangles*3) << " float" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks>::TRAV cOutputVTK_Vertices_Element_Tsunami;
					cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(node->cTriangleFactory);
					cOutputVTK_Vertices_Element_Tsunami.cKernelClass.setup(&vtkfile);
					cOutputVTK_Vertices_Element_Tsunami.action(node->cStacks);
				}
		);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "TRIANGLE_STRIPS " << (number_of_triangles) << " " << (number_of_triangles*4) << std::endl;
		for (unsigned long long i = 0; i < number_of_triangles; i++)
			vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

		vtkfile.close();
	}



	/**
	 * output partitions to vtk file
	 */
	void writePartitionsToVTKFile(
			const char *p_filename		///< filename to write partitions to
	)
	{
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(p_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << grid_initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_handler_partitions << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;
		vtkfile << "ASCII" << std::endl;
		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_simulation_handler_partitions*3) << " float" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					vtkfile << node->cTriangleFactory.vertices[0][0] << " " << 0 << " " << -node->cTriangleFactory.vertices[0][1] << std::endl;
					vtkfile << node->cTriangleFactory.vertices[1][0] << " " << 0 << " " << -node->cTriangleFactory.vertices[1][1] << std::endl;
					vtkfile << node->cTriangleFactory.vertices[2][0] << " " << 0 << " " << -node->cTriangleFactory.vertices[2][1] << std::endl;
				}
		);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "TRIANGLE_STRIPS " << (number_of_simulation_handler_partitions) << " " << (number_of_simulation_handler_partitions*4) << std::endl;
		for (unsigned long long i = 0; i < number_of_simulation_handler_partitions; i++)
			vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

		vtkfile.close();
	}


public:
/*****************************************************************************************************************
 * DEBUG: Output
 *****************************************************************************************************************/
	void debugOutputEdgeCommunicationInformation(
			float x,	///< x-coordinate of triangle cell
			float y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "Partition ID: " << node->uniqueId << std::endl;
					std::cout << "Adjacent Communication Information" << std::endl;
					std::cout << node->cEdgeComm_InformationAdjacentPartitions << std::endl;
					std::cout << std::endl;
				}
		);
	}


public:
/*****************************************************************************************************************
 * DEBUG: output element data
 *****************************************************************************************************************/
	void debugOutputElementData(
			float x,	///< x-coordinate of triangle cell
			float y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					CSimulationTsunami_Parallel_SubPartitionHandler *partitionHandler = node->cSimulation_SubPartitionHandler;

					std::cout << "PartitionAndStackInformation:" << std::endl;
					std::cout << "  + Structure Stacks.direction = " << node->cStacks->structure_stacks.direction << std::endl;
					std::cout << "  + Structure Stacks.forward.getStackElementCounter() = " << node->cStacks->structure_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + Structure Stacks.backward.getStackElementCounter() = " << node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + ElementData Stacks.direction = " << node->cStacks->element_data_stacks.direction << std::endl;
					std::cout << "  + ElementData Stacks.forward.getStackElementCounter() = " << node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + ElementData Stacks.backward.getStackElementCounter() = " << node->cStacks->element_data_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + EdgeComm Stacks.left = " << (void*)node->cStacks->edge_data_comm_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm Stacks.right = " << (void*)node->cStacks->edge_data_comm_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.left = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.right = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + splitJoin.elements_in_first_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements << std::endl;
					std::cout << "  + splitJoin.elements_in_second_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements << std::endl;
					std::cout << std::endl;
				}
		);
	}

/***************************************************************************************
 * DEBUG: output information about triangle partition
 ***************************************************************************************/
public:
	void debugOutputPartition(
			float x,	///< x-coordinate of triangle cell
			float y		///< y-coordinate of triangle cell
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
				{
					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << "* Partition information at " << x << ", " << y << ":" << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << std::endl;
					std::cout << *node << std::endl;
					std::cout << node->cPartition_SplitJoinInformation << std::endl;
					std::cout << std::endl;
					std::cout << "min CFL cell_size / max_speed after edge comm: " << node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_edge_comm << std::endl;
					std::cout << "min CFL cell_size / max_speed after adaptivity: " << node->cSimulation_SubPartitionHandler->cfl_domain_size_div_max_wave_speed_after_adaptivity << std::endl;
				}
		);
	}


/*****************************************************************************************************************
 * SPLIT / JOIN
 *****************************************************************************************************************/


public:
	void splitAndJoinRandomized()
	{
		if ((rand() & 1) == 0)
		{
			splitOrJoinPartitionRandomNTimes(10);
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}



public:
	size_t splitOrJoinPartitions()
	{
		/***************************************************************************************
		 * ACTION: run the split or join operations and update the edge communication information
		 ***************************************************************************************/

		// access cTestVariables.allSplit directly within anonymous function does not work.
		// therefore we create a new storage to variables.
		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes<true>(
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					cGenericTreeNode->cPartition_TreeNode->fun_splitAtLeaves(splitAllPartitionsInNextStep);
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_TreeNode_::fun_testAndJoinAtMidNodes(cGenericTreeNode, joinAllPartitionsInNextStep);
				}
			);

		rootGenericTreeNode->traverse_PartitionTreeNode_Reduce<true>(
					[&](CPartition_TreeNode_ *node, unsigned int *o_reduceValue)
					{
						*o_reduceValue = node->cPartition_SplitJoinActions.pass2_updateAdjacentPartitionInformation();
					},
					&CReduceOperators::ADD<unsigned int>,
					&number_of_simulation_handler_partitions
				);

		static auto fun2 = [=](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_SplitJoinActions<CSimulationTsunami_Parallel_SubPartitionHandler>::pass3_swapAndCleanAfterUpdatingEdgeComm(cGenericTreeNode);
				};

		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes<true>(fun2, fun2);

		return number_of_simulation_handler_partitions;
	}


	void markPartitionForSplitting(CVector<2,float> pos)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
			{
				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
						node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
						node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
						pos.data[0], pos.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
			}
		);
	}



	void splitPartition(
			const CVector<2,float> &planePosition
	)
	{
		markPartitionForSplitting(planePosition);
		splitOrJoinPartitions();
	}



	void markPartitionForJoining(
			const CVector<2,float> &planePosition
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
			{
				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
						node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
						node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
						planePosition.data[0], planePosition.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
			}
		);
	}



	/**
	 * ACTION: random split and join partition at random positions
	 */
	void splitOrJoinPartitionRandomNTimes(
			const int count
	)
	{
		for (int i = 0; i < count; i++)
			rootGenericTreeNode->traverse_PartitionTreeNode<true>(
				[&](CPartition_TreeNode_ *node)
				{
					int r = random() % 6;

					if (r == 0)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
					}
					else if (r < 6)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
					}
				}
			);

		splitOrJoinPartitions();
	}


	/**
	 * output information about the underlying tree
	 */
	void outputGenericTreeInformation()
	{
		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes_Depth<true>(
				[=](CGenericTreeNode_ *genericTreeNode, int depth)
				{
					// LEAF

					for (int i = 0; i < depth; i++)
						std::cout << "        ";

					std::cout << "|------- (" << depth << ") ";

					if (genericTreeNode->cPartition_TreeNode == nullptr)
					{
						std::cout << "Empty Leaf (ERROR!)" << std::endl;
						return;
					}

					std::cout << "UniqueId: " << genericTreeNode->cPartition_TreeNode->uniqueId;

#if DEBUG
					std::cout << std::endl;
					for (int i = 0; i < depth+1; i++)
						std::cout <<  "        ";

					std::cout << "    timestep size: " << genericTreeNode->cPartition_TreeNode->cSimulation_SubPartitionHandler->cTsunami_EdgeComm.getTimestepSize() << std::endl;
#endif
				}
				,
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
					if (cGenericTreeNode->cPartition_TreeNode == nullptr)
						return;

					// MIDDLE NODE

					for (int i = 0; i < depth; i++)
						std::cout << "        ";

					std::cout << "|------- (" << depth << ") ";


					if (cGenericTreeNode->cPartition_TreeNode == nullptr)
					{
						std::cout << "Empty MidNode" << std::endl;
						return;
					}

					std::cout << "MidNode: " << cGenericTreeNode->cPartition_TreeNode->uniqueId << std::endl;
				}
			);
	}



/*****************************************************************************************************************
 * GUI STUFF & VISUALIZATION
 *****************************************************************************************************************/

#if COMPILE_SIMULATION_WITH_GUI

public:
	void keydown(int key)
	{
		switch(key)
		{
			case 'z':
				simulation_world_scene_id--;
				reset_Simulation();
				std::cout << "World ID: " << simulation_world_scene_id << std::endl;
				break;

			case 'x':
				simulation_world_scene_id++;
				reset_Simulation();
				std::cout << "World ID: " << simulation_world_scene_id << std::endl;
				break;

			case 'Z':
				simulation_terrain_scene_id--;
				reset_Simulation();
				std::cout << "Terrain ID: " << simulation_terrain_scene_id << std::endl;
				break;

			case 'X':
				simulation_terrain_scene_id++;
				reset_Simulation();
				std::cout << "Terrain ID: " << simulation_terrain_scene_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset_Simulation();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset_Simulation();
				break;

			case 'P':
				outputGenericTreeInformation();
				break;

			case 'c':
#if USE_ASAGI
				p_adaptive_traversal_setup_column();
#else
				p_adaptive_traversal_setup_column();
#endif
				break;

#if USE_ASAGI
			case 'k':
				p_adaptive_traversal_setup_terrain();
				break;
#endif

			case ',':
				splitAllPartitionsInNextStep = true;
				p_adaptive_traversal_simulation();
				splitOrJoinPartitions();
				splitAllPartitionsInNextStep = false;
				break;

			case '.':
				joinAllPartitionsInNextStep = true;
				p_adaptive_traversal_simulation();
				splitOrJoinPartitions();
				joinAllPartitionsInNextStep = false;
				break;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			case 'I':
				splitAndJoinRandomized();
				break;


			case 'y':
				splitOrJoinPartitionRandomNTimes(1);
				break;

		}
	}


	void render_surfaceDefault_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
		render_surfaceVerticesElement(cShaderBlinn);
	}


	void render_surfaceVerticesElement(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_aligned.cKernelClass.setup(
							visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);

					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_simple(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.cKernelClass.setup(
							visualization_terrain_default_displacement, visualization_terrain_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);

					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_Bathymetry_simple.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_smooth(
			CShaderBlinn &cShaderBlinn
	)
	{
		return;
#if COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_SubPartitionHandler,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeVertexDataCommData_Terrain,
				rootGenericTreeNode,
				[&](CPartition_TreeNode_ *node)
					{
						node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_Bathymetry_smooth.cKernelClass.setup(
								visualization_terrain_default_displacement, visualization_terrain_scale_factor,
								&cOpenGL_Vertices_Element_Root_Tsunami
					);
				}

			);


		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
#endif
	}


	void render_surfaceWithHeightColors(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_simple.cKernelClass.setup(
							visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);

					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_simple.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Wireframe_Tsunami.cKernelClass.setup(
							visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
							&cOpenGL_Vertices_Wireframe_Root_Tsunami
						);

					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Wireframe_Tsunami.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_surfaceSmooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_SubPartitionHandler,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cOpenGL_Vertices_Smooth_Element_Tsunami,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeVertexDataCommData_WaterSurface,
				rootGenericTreeNode,
				[&](CPartition_TreeNode_ *node)
					{
						node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(
								visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
								&cOpenGL_Vertices_Element_Root_Tsunami
					);
				}
			);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
#endif
	}


	void render_surfaceSmoothWithHeightColors(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
#if COMPILE_WITH_PARALLEL_VERTEX_COMM
		cShaderColorHeightBlinn.use();
		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action
		<
			CPartition_TreeNode_,
			CSimulationTsunami_Parallel_SubPartitionHandler,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>
		(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cOpenGL_Vertices_Smooth_Element_Tsunami,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeVertexDataCommData_WaterSurface,
				rootGenericTreeNode,
				[&](CPartition_TreeNode_ *node)
					{
						node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Smooth_Element_Tsunami.cKernelClass.setup(
								visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
								&cOpenGL_Vertices_Element_Root_Tsunami
					);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
#endif
	}



	void render_subPartitionBorders(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		GLSL::vec3 c(0.8, 0.1, 0.1);
		cShaderBlinn.material_ambient_color3_uniform.set(c);
		cShaderBlinn.material_diffuse_color3_uniform.set(c);
		cShaderBlinn.material_specular_color3_uniform.set(c);

		render_partitions_vao.bind();
			render_partitions_buffer.bind();

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

			CGlBuffer *tmp_render_partitions_buffer = &render_partitions_buffer;

			rootGenericTreeNode->traverse_PartitionTreeNode<false>(
				[&](CPartition_TreeNode_ *node)
				{
					GLfloat vertex_buffer[6*3];

					vertex_buffer[0*3+0] = node->cTriangleFactory.vertices[0][0];
					vertex_buffer[0*3+1] = 0;
					vertex_buffer[0*3+2] = -node->cTriangleFactory.vertices[0][1];

					vertex_buffer[1*3+0] = node->cTriangleFactory.vertices[1][0];
					vertex_buffer[1*3+1] = 0;
					vertex_buffer[1*3+2] = -node->cTriangleFactory.vertices[1][1];

					vertex_buffer[2*3+0] = node->cTriangleFactory.vertices[1][0];
					vertex_buffer[2*3+1] = 0;
					vertex_buffer[2*3+2] = -node->cTriangleFactory.vertices[1][1];

					vertex_buffer[3*3+0] = node->cTriangleFactory.vertices[2][0];
					vertex_buffer[3*3+1] = 0;
					vertex_buffer[3*3+2] = -node->cTriangleFactory.vertices[2][1];

					vertex_buffer[4*3+0] = node->cTriangleFactory.vertices[2][0];
					vertex_buffer[4*3+1] = 0;
					vertex_buffer[4*3+2] = -node->cTriangleFactory.vertices[2][1];

					vertex_buffer[5*3+0] = node->cTriangleFactory.vertices[0][0];
					vertex_buffer[5*3+1] = 0;
					vertex_buffer[5*3+2] = -node->cTriangleFactory.vertices[0][1];

					tmp_render_partitions_buffer->subData(0, sizeof(vertex_buffer), vertex_buffer);
					glDrawArrays(GL_LINES, 0, 6);
				}
			);

			CGlErrorCheck();

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

		render_partitions_vao.unbind();

		cShaderBlinn.disable();
	}
#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

	void action_Validation_EdgeCommLength()
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
			{
				node->cSimulation_SubPartitionHandler->cPartition_ExchangeEdgeCommData_Adaptivity.validateCommDataLength();
			}
		);
	}


	void action_Validation_EdgeCommMidpointsAndNormals()	const
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *worker = node->cSimulation_SubPartitionHandler;

					/*
					 * setup fake validation element data stack
					 */
					if (node->cValidationStacks->element_data_stacks.forward.getNumberOfElementsOnStack() != node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack())
					{
						node->cValidationStacks->element_data_stacks.forward.clear();

						for (unsigned int i = 0; i < node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack(); i++)
						{
							node->cValidationStacks->element_data_stacks.forward.push(CValElementData());
						}
					}

					worker->cEdgeComm_ValidateComm.cKernelClass.noElementDataChecks = true;

					worker->cEdgeComm_ValidateComm.actionFirstPass(
							node->cStacks->structure_stacks,
							node->cValidationStacks->element_data_stacks,
							node->cValidationStacks->edge_data_comm_left_edge_stack,
							node->cValidationStacks->edge_data_comm_right_edge_stack,
							node->cValidationStacks->edge_comm_buffer
						);
				}
		);


		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cPartitionTree_Node_EdgeComm_Validation.pullEdgeCommData();
				}
		);


		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *worker = node->cSimulation_SubPartitionHandler;

					/*
					 * stacks are cleared here since there may be some data left from the last traversal
					 */
					node->cValidationStacks->edge_data_comm_left_edge_stack.clear();
					node->cValidationStacks->edge_data_comm_right_edge_stack.clear();

					/**
					 * second pass
					 */
					worker->cEdgeComm_ValidateComm.actionSecondPass(
							node->cStacks->structure_stacks,
							node->cValidationStacks->element_data_stacks,
							node->cValidationStacks->edge_data_comm_exchange_left_edge_stack,		/// !!! here we use the "exchange stacks"!
							node->cValidationStacks->edge_data_comm_exchange_right_edge_stack,
							node->cValidationStacks->edge_comm_buffer
						);
				}
		);
	}


	void action_Validation()
	{
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
	}

#endif

};


#endif /* CSIMULATION_TSUNAMI_PARALLEL_HPP_ */
