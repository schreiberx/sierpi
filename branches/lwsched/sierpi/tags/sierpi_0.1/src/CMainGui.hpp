/*
 * CMainGui.hpp
 *
 *  Created on: Apr 18, 2012
 *      Author: schreibm
 */

#ifndef CMAINGUI_HPP_
#define CMAINGUI_HPP_


#include <stdlib.h>
#include <sstream>

#include "config.h"

#include "libmath/CMath.hpp"
#include "libgl/draw/CGlDrawSphere.hpp"
#include "libgl/core/CGlState.hpp"
#include "lib/CEyeBall.hpp"

#include "mainvis/CCommonShaderPrograms.hpp"
#include "mainvis/CRenderWindow.hpp"

#include "simulations/CSimulation.hpp"

#include "mainvis/CGuiConfig.hpp"

#include "libgl/engine/CTime.hpp"
#include "libgl/engine/camera/iCamera.hpp"
#include "libgl/engine/camera/CCamera1stPerson.hpp"
#include "libgl/engine/iInputState.hpp"

#include "libgl/hud/CGlFreeType.hpp"
#include "libgl/hud/CGlWindow.hpp"
#include "libgl/hud/CGlRenderOStream.hpp"
#include "libgl/hud/CGlHudConfig.hpp"

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "lib/CProcessMemoryInformation.hpp"

#include "lib/CLinesFromSVG.hpp"


#include "CMain.hpp"


template <typename CSimulation>
class CMainGui :
		public CRenderWindowEventCallbacks,
		public CMain<CSimulation>
{
	using CMain<CSimulation>::cSimulation;
	using CMain<CSimulation>::simulationLoopPrefix;
	using CMain<CSimulation>::simulationLoopIteration;
	using CMain<CSimulation>::simulationLoopSuffix;

	using CMain<CSimulation>::threading_simulationLoopIteration;


public:
	CError error;
	bool verbose;
	bool quit;

	CRenderWindow cRenderWindow;

	CGuiConfig cGuiConfig;
	CGlFreeType cGlFreeType;
	CGlRenderOStream cGlRenderOStream;

	CProcessMemoryInformation cProcessMemoryInformation;

	// parameter for model matrix
	CEyeBall<float> cModelEyeBall;

	// camera control
	CCamera1stPerson camera;
	CTime cTime;
	CVector<3,float> player_velocity;
	iInputState inputState;

	// commonly used shader programs
	CCommonShaderPrograms cCommonShaderPrograms;


	// some opengl parameters for perspective matrix
	float perspective_zoom;
	float zoom;
	float near_plane;
	float far_plane;

    /**
     * import lines from SVG file
     */
    CLinesFromSVG cLinesFromSVG;

	int screenshotEnumeration;

	/**
	 * Number of threads was updated during the last step.
	 *
	 * For TBB, the number of threads has to be set by the thread which created the initial threads.
	 */
	bool simulation_number_of_threads_updated;


	/**
	 * CONSTRUCTOR
	 */
	CMainGui(
			char *p_initial_structure = NULL,
			bool p_verbose = false
	)	:
		verbose(p_verbose),
		quit(false),
		cRenderWindow(*this, "Sierpi", 800, 600, false, 3, 3),
		cGlRenderOStream(cGlFreeType),
		cCommonShaderPrograms(p_verbose),
		screenshotEnumeration(0),
		simulation_number_of_threads_updated(false)
	{
	}


	void setupGui()
	{
		/**********************************************
		 * SETUP OpenGL stuff
		 */
		near_plane = 1.0;
		far_plane = 20.0;


		/**********************************************
		 * CONFIGURATION HUD
		 */
		cGlFreeType.setup(12);
		CError_AppendReturn(cGlFreeType);


		/**********************************************
		 * LOAD SIERPI SVG
		 */
		cLinesFromSVG.loadSVGFile("data/setupGraphics/sierpi.svg");

		/***********************************************
		 * HUD
		 */
		// then we add the other configuration parameters and setup the HUD itsetup
		cGuiConfig.setup();

		// first we add the configuration parameters of the simulation itself
		cSimulation->setupCGlHudConfigSimulation(cGuiConfig.cGlHudConfigMainRight);
		// first we add the configuration parameters of the simulation itself
		cSimulation->setupCGlHudConfigVisualization(cGuiConfig.cGlHudConfigVisualization);

		cGuiConfig.assembleGui(cGlFreeType, cGlRenderOStream);

		cSimulation->setupCGuiCallbackHandlers(cGuiConfig);


		// activate HUD
		cGuiConfig.setHudVisibility(true);



		/**********************************************
		 * COMMON SHADERS
		 */
		if (cCommonShaderPrograms.error())
		{
			error = cCommonShaderPrograms.error;
			return;
		}

		/**********************************************
		 * SIMULATION
		 */
		if (cSimulation->error())
		{
			error = cSimulation->error;
			return;
		}


		/**********************************************
		 * INSTALL CCONFIG CALLBACK HANDLERS
		 */

#if SIMULATION_TSUNAMI_PARALLEL

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.simulation_number_of_threads_updated = true;
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cSimulation->simulation_number_of_threads);
		}


		/***************
		 * VISUALIZATION CALLBACK HANDLERS
		 */
		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatTextureSize();
			CGUICONFIG_CALLBACK_END();
			cGuiConfig.setCallbackHandler(&cGuiConfig.splats_texture_height, CGUICONFIG_CALLBACK_HANDLER, this);
			cGuiConfig.setCallbackHandler(&cGuiConfig.splats_texture_width, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatTextureSize();
			CGUICONFIG_CALLBACK_END();
			cGuiConfig.setCallbackHandler(&cGuiConfig.splats_texture_height, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatSizeScalar();
			CGUICONFIG_CALLBACK_END();
			cGuiConfig.setCallbackHandler(&cGuiConfig.splat_size_scalar, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SurfaceHeightTranslateAndScale();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cGuiConfig.surface_texel_height_scale);
			CGUICONFIG_CALLBACK_INSTALL(cGuiConfig.surface_texel_height_translate);
		}


#endif


		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.callback_take_screenshot();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cGuiConfig.take_screenshot);
		}


		/**********************************************
		 * reset view
		 */
		resetView();
	}


	virtual ~CMainGui()
	{
	}


	void takeNextScreenshotImage()
	{
		char buffer[1024];
		sprintf(buffer, "screenshot_%08i.bmp", screenshotEnumeration);

		cRenderWindow.saveScreenshotWithThread(buffer);

		screenshotEnumeration++;
	}

	void callback_take_screenshot()
	{
		takeNextScreenshotImage();
		cGuiConfig.take_screenshot = false;
	}


	void resetView()
	{
		perspective_zoom = 1.0f;

		cModelEyeBall.reset();

		camera.reset();
		camera.moveRelative(CVector<3,float>(0, 1, 2));
		camera.rotate(M_PI*0.1, 0, 0);
		camera.computeMatrices();

		cGuiConfig.visualization_enabled = true;

#if SIMULATION_TSUNAMI_PARALLEL
		update_SplatTextureSize();
		update_SplatSizeScalar();
		update_SurfaceHeightTranslateAndScale();
#endif
	}

	/**
	 * reset the simulation
	 */
	void reset()
	{
		std::cout << "[ RESET ]" << std::endl;
		cProcessMemoryInformation.outputUsageInformation();

		// suffix first!
		simulationLoopSuffix();

		cSimulation->reset_Simulation();

		simulationLoopPrefix();

		cProcessMemoryInformation.outputUsageInformation();
	}


#if SIMULATION_TSUNAMI_PARALLEL

	void update_SplatTextureSize()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.resizeTexture(cGuiConfig.splats_texture_width, cGuiConfig.splats_texture_height);
	}

	void update_SplatSizeScalar()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.setSplatSizeScalar(cGuiConfig.splat_size_scalar);
	}

	void update_SurfaceHeightTranslateAndScale()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.setupTranslationAndScalingParameters(cGuiConfig.surface_texel_height_translate, cGuiConfig.surface_texel_height_scale);
	}
#endif




	/**
	 * callback method called whenever a key is pressed
	 */
	void callback_key_down(
			int key,
			int mod,
			int scancode,
			int unicode
	)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

		CVector<2,float> position;

		switch(key)
		{
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			cSimulation->simulation_number_of_threads = key-'0';
			simulation_number_of_threads_updated = true;
			break;

		case 'q':
			quit = true;
			break;

		case 'e':
			cSimulation->reset_Simulation();
			resetView();
			break;

		case 'r':
			reset();
			break;

		case 'v':
			cGuiConfig.visualization_enabled ^= true;
			break;

		case 'o':
			cGuiConfig.surface_visualization_method++;
			break;

		case 'O':
			cGuiConfig.surface_visualization_method--;
			break;


		case 'b':
			cGuiConfig.bathymetry_visualization_method++;
			break;

		case 'B':
			cGuiConfig.bathymetry_visualization_method--;
			break;

		case 'U':
		case 'u':
			cGuiConfig.visualization_render_wireframe ^= true;
			break;

		case 'p':
			cGuiConfig.visualization_render_subpartition_borders ^= true;
			break;

		case 'a':	player_velocity[0] = CMath::max(player_velocity[0]-1, -1.f);	break;
		case 'd':	player_velocity[0] = CMath::min(player_velocity[0]+1, +1.f);	break;
		case 'w':	player_velocity[2] = CMath::max(player_velocity[2]-1, -1.f);	break;
		case 's':	player_velocity[2] = CMath::min(player_velocity[2]+1, +1.f);	break;

#if SIMULATION_TSUNAMI_PARALLEL
		case 'S':
			{
				float scale = 0.5;

				for (auto i = cLinesFromSVG.lineList.begin(); i != cLinesFromSVG.lineList.end(); i++)
				{
					auto &list = *i;
					for (auto d = list.begin(); d != list.end(); d++)
					{
						cSimulation->setup_ElementDataAt2DPosition((*d)[0]*scale, (*d)[1]*scale);
					}
				}
			}
			break;
#endif

		case 'i':	cSimulation->simulation_random_raindrops_activated ^= true;
			break;

		case 'V':
			cProcessMemoryInformation.outputUsageInformation();
			break;

		case 'l':
			cSimulation->run_simulation ^= true;
			break;



#if SIMULATION_TSUNAMI_PARALLEL
		case 'M':
			position = computeMouseOnPlaneCoords();
			cSimulation->debugOutputElementData(position[0], position[1]);
			break;

		case 'm':
			position = computeMouseOnPlaneCoords();
			cSimulation->debugOutputEdgeCommunicationInformation(position[0], position[1]);
			break;

		case 'n':
			position = computeMouseOnPlaneCoords();
			cSimulation->debugOutputPartition(position[0], position[1]);
			break;

/*
 * FULLSCREEN
 */
		case 'F':
			cRenderWindow.setWindowFullscreenState(!cRenderWindow.fullscreen_active);
			break;
#if 0
/*
 * SPLIT
 */
		case 'b':
			cSimulation->splitPartition(computeMouseOnPlaneCoords());
			cSimulation->splitOrJoinPartitions();
			break;

		case 'B':
			cSimulation->markPartitionForSplitting(computeMouseOnPlaneCoords());
			break;

/*
 * JOIN
 */
		case 'h':
			cSimulation->markPartitionForJoining(computeMouseOnPlaneCoords());
			cSimulation->splitOrJoinPartitions();
			break;

		case 'H':
			cSimulation->markPartitionForJoining(computeMouseOnPlaneCoords());
			break;

/*
 * SPLIT/JOIN depending on flags
 */

		case 'N':
			cSimulation->splitOrJoinPartitions();
			break;
#endif	// #if 0

#endif	// #if tsunami_parallel

/*
 * show/hide GUI
 */
		case ' ':
			cGuiConfig.setHudVisibility(!cGuiConfig.hud_visible);
			break;


/*
 * record window
 */
		case 'R':
			cGuiConfig.take_screenshot_series = !cGuiConfig.take_screenshot_series;
			break;

		/*
		 * forward key to simulation
		 */
		default:
			cSimulation->keydown(key);
			break;

		}
	}


	void callback_key_up(int key, int mod, int scancode, int unicode)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

		switch(key)
		{
			case 'a':	player_velocity[0] += 1;	break;
			case 'd':	player_velocity[0] -= 1;	break;
			case 'w':	player_velocity[2] += 1;	break;
			case 's':	player_velocity[2] -= 1;	break;
		}
	}


	void callback_quit()
	{
		quit = true;
	}


	void callback_mouse_motion(int x, int y)
	{
		inputState.update((float)x*2.0f/(float)cRenderWindow.window_width-1.0f, (float)y*2.0f/(float)cRenderWindow.window_height-1.0f);


		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])			// middle mouse button
		{
			setupElementDataAtCurrentMousePosition();
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])		// right mouse button
		{
			setupColumnAtCurrentMousePosition();
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])			// left mouse button
		{
		}

		cGuiConfig.mouse_motion(x, cRenderWindow.window_height - y);
	}

	void callback_mouse_button_down(int button)
	{
		// if the button was pressed down within the config window, do nothing
		if (cGuiConfig.mouse_button_down(button))
			return;

		switch(button)
		{
			case CRenderWindow::MOUSE_BUTTON_RIGHT:
				setupColumnAtCurrentMousePosition();
				break;

			case CRenderWindow::MOUSE_BUTTON_MIDDLE:
				setupElementDataAtCurrentMousePosition();
				break;
		}

		if (button <= 3)
			inputState.mouse_buttons[button] = true;
	}

	void callback_mouse_button_up(int button)
	{
		// if the button was pressed down within the config window, do nothing
		cGuiConfig.mouse_button_up(button);

		if (button <= 3)
			inputState.mouse_buttons[button] = false;
	}


	void callback_mouse_wheel(int x, int y)
	{
		if (cGuiConfig.mouse_wheel(x, y))
			return;

		perspective_zoom += (float)y*(-0.1);
	}

	void callback_viewport_changed(int width, int height)
	{
	}


	GLSL::vec3 light_view_pos;

	void setupBlinnShader_Surface(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}



	void setupBlinnShader_Bathymetry(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}



	void setupHeightColorShader(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cHeightColorBlinn.use();
		cCommonShaderPrograms.cHeightColorBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cHeightColorBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cHeightColorBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cHeightColorBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		cCommonShaderPrograms.cHeightColorBlinn.setupColorScaleAndOffset(0.025, 10);
	}



	void renderWorld()
	{
		CGlErrorCheck();

		GLSL::mat4 local_model_matrix = cModelEyeBall.rotationMatrix;
		local_model_matrix.loadIdentity();

		GLSL::mat4 local_view_model_matrix = camera.view_matrix * local_model_matrix;
		GLSL::mat4 local_pvm_matrix = camera.projection_matrix*local_view_model_matrix;

		/*
		 * render simplified bathymetry field
		 */
		int bathymetry_max_visualizations = 3;
		cGuiConfig.bathymetry_visualization_method = (cGuiConfig.bathymetry_visualization_method+bathymetry_max_visualizations) % bathymetry_max_visualizations;

		switch(cGuiConfig.bathymetry_visualization_method)
		{
			case 0:
				// do not render bathymetry
				break;

			case 1:
				setupBlinnShader_Bathymetry(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_terrainBathymetry_smooth(cCommonShaderPrograms.cBlinn);
				break;

			case 2:
				setupBlinnShader_Bathymetry(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_terrainBathymetry_simple(cCommonShaderPrograms.cBlinn);
				break;
		}


		/*
		 * render water surface
		 */
		int surface_max_visualizations = 4;

		cGuiConfig.surface_visualization_method = (cGuiConfig.surface_visualization_method+surface_max_visualizations) % surface_max_visualizations;
		switch(cGuiConfig.surface_visualization_method)
		{
			case 0:
				// do not render any surface
				break;

			case 1:
				// smooth renderer
				setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceSmoothWithHeightColors(cCommonShaderPrograms.cHeightColorBlinn);
				break;

			case 2:
				// dg renderer
				setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceWithHeightColors_simple(cCommonShaderPrograms.cHeightColorBlinn);
				break;

			case 3:
				// simple aligned renderer
				setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceSmooth(cCommonShaderPrograms.cBlinn);
				break;

			default:
				setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceDefault_simple(cCommonShaderPrograms.cBlinn);
				break;
		}


		/*
		 * render wireframe
		 */
		if (cGuiConfig.visualization_render_wireframe)
		{
			// wireframe
			setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
			cSimulation->render_surfaceWireframe(cCommonShaderPrograms.cBlinn);
		}


		/*
		 * render subpartition borders
		 */
#if SIMULATION_TSUNAMI_PARALLEL
		if (cGuiConfig.visualization_render_subpartition_borders)
		{
			setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
			cSimulation->render_subPartitionBorders(cCommonShaderPrograms.cBlinn);
		}
#endif
	}


	CVector<2,float> computeMouseOnPlaneCoords()
	{
		GLSL::mat4 inv_projection_matrix = camera.projection_matrix.getInverse();

		// dirty hack: get view center from view matrix
		GLSL::vec3 ray_start = camera.getPosition();

		// end position is somewhere at the far plane
		GLSL::vec4 unproj_space_vector4 = inv_projection_matrix*vec4f(inputState.mouse_x, -inputState.mouse_y, -1, 1);
		GLSL::vec4 unproj_space_vector3 = unproj_space_vector4 / unproj_space_vector4[3];

		GLSL::vec3 ray = camera.view_matrix.getTranspose3x3() * unproj_space_vector3;

		// compute point on plane
		GLSL::vec3 pos = ray_start-ray*(ray_start[1]/ray[1]);
		return GLSL::vec2(pos[0], -pos[2]);
	}


	void refineAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->setup_ColumnAt2DPosition(point_on_plane[0], point_on_plane[1], cSimulation->simulation_dataset_cylinder_radius);
	}


	void setupElementDataAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->setup_ElementDataAt2DPosition(
				point_on_plane[0], point_on_plane[1]
			);
	}


	void setupColumnAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->setup_ColumnAt2DPosition(
				point_on_plane[0], point_on_plane[1], cSimulation->simulation_dataset_cylinder_radius
			);
	}


#if 0
	void coarsenAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->coarsenAt2DPosition(point_on_plane[0], point_on_plane[1]);
	}
#endif


	void simulationTimesteps()
	{
		if (cSimulation->run_simulation)
		{
			/*
			 * the number of available openmp threads are set only during initialization when not running
			 * in interactive mode (GUI)
			 */

			for (int i = 0; i < cGuiConfig.visualization_simulation_steps_per_frame; i++)
				threading_simulationLoopIteration();
		}
	}

	double getRand01()
	{
		return ((double)::random())*(1.0/(double)RAND_MAX);
	}

	void raindropsRefine(
			bool undelayed = false
	)
	{
		static double lastTicks = -1.0;
		static double nextRaindropAbsoluteTicks = 1.0;
		static double newRaindropRelativeTicks = 1.0;

		double recentTicks = cRenderWindow.getTicks();

		if (recentTicks > nextRaindropAbsoluteTicks || undelayed)
		{
			lastTicks = recentTicks;
			nextRaindropAbsoluteTicks = lastTicks+getRand01()*newRaindropRelativeTicks;

#if 1
 			cSimulation->simulation_dataset_cylinder_radius = CMath::max(CMath::min(0.1, 5000.f/(float)cSimulation->number_of_triangles*getRand01()), 0.01);

			cSimulation->setup_ColumnAt2DPosition(
					getRand01()*2.0-1.0,
					getRand01()*2.0-1.0,
					cSimulation->simulation_dataset_cylinder_radius
					);
#else
			cSimulation->setup_ElementDataAt2DPosition(
					getRand01()*2.0-1.0,
					getRand01()*2.0-1.0,
					cSimulation->simulation_dataset_cylinder_radius
					);
#endif
		}
	}

	void render()
	{
		camera.moveRelative(player_velocity*(float)cTime.frame_elapsed_seconds*5.0f);
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			camera.rotate(	-inputState.relative_mouse_y*cTime.frame_elapsed_seconds*300.0,
							-inputState.relative_mouse_x*cTime.frame_elapsed_seconds*300.0,
							0
							);

		zoom = CMath::exp(perspective_zoom)*0.2;
		camera.frustum(-zoom*cRenderWindow.aspect_ratio, zoom*cRenderWindow.aspect_ratio, -zoom, zoom, near_plane, far_plane);
		camera.computeMatrices();

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		light_view_pos = camera.view_matrix*GLSL::vec3(2, 4, -6);

		renderWorld();

		cGuiConfig.render();

		inputState.clearRelativeMovement();
	}

	void run()
	{
		cSimulation->outputVerboseInformation();

		simulationLoopPrefix();

		while (!quit)
		{
			cTime.update();

			if (simulation_number_of_threads_updated)
			{
				threading_setNumThreads(cSimulation->simulation_number_of_threads);

				simulation_number_of_threads_updated = false;
			}

			std::ostringstream buf;

			if (cTime.fpsUpdatedInLastFrame)
			{
#if !SIMULATION_TSUNAMI_PARALLEL
				buf << "Sierpinski Zeug running @ " << cTime.fps << " FPS      Triangles: " << cSimulation->number_of_triangles;

#ifndef NDEBUG
//				buf << "     Max. Triangles: " << cSimulation->cStacks->structure_stacks.getSize();
#endif

#else
				buf << "Sierpinski Zeug running @ " << cTime.fps << " FPS";
				buf << " | ";
				buf << cSimulation->number_of_triangles << " Triangles";
				buf << " | ";
				buf << cSimulation->number_of_simulation_handler_partitions << " Partitions";
				buf << " | ";
				buf << ((double)cSimulation->number_of_triangles*cGuiConfig.visualization_simulation_steps_per_frame*cTime.fps)*0.000001 << " Mega Triangles per second (inaccurate)";
#endif
			}

			if (cSimulation->simulation_random_raindrops_activated)
			{
				raindropsRefine();
			}

			if (cGuiConfig.visualization_enabled)
			{
				glClearColor(0,0,0,0);
				glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				/*
				 * SIMULATION TIMESTEP
				 */
				simulationTimesteps();

				/*
				 * UPDATE FRAME
				 */
				render();

				if (cTime.fpsUpdatedInLastFrame)
				{
					cRenderWindow.setWindowTitle(buf.str().c_str());
				}

				cRenderWindow.eventLoop();

				if (cGuiConfig.take_screenshot_series)
				{
					takeNextScreenshotImage();
				}

				cRenderWindow.swapBuffer();
			}
			else
			{
				cRenderWindow.eventLoop();

				simulationTimesteps();

				if (cTime.fpsUpdatedInLastFrame)
				{
					std::cout << buf.str() << std::endl;
				}
			}
		}

		simulationLoopSuffix();
	}
};




#endif /* CMAINGUI_HPP_ */
