/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: schreibm
 *
 *
 * Class to test the correct edge communication
 *
 * The adjacent edge center as well as the edge normal
 * is transmitted.
 */

#ifndef CEDGECOMM_VALIDATE_COMM_HPP_
#define CEDGECOMM_VALIDATE_COMM_HPP_


#include "libmath/CMath.hpp"
#include "libsierpi/traversators/edgeComm/CEdgeComm_VertexCoords_Normals.hpp"
#include "CValidateTypes.hpp"

namespace sierpi
{
namespace kernels
{

class CEdgeComm_ValidateComm
{
public:
	// dummy data
	typedef sierpi::travs::CEdgeComm_VertexCoords_Normals<CEdgeComm_ValidateComm, CValElementData, CValEdgeData> TRAV;

	typedef float TVertexScalar;

	bool noElementDataChecks;

	void setup()
	{
		noElementDataChecks = false;
	}

	CEdgeComm_ValidateComm()
	{
		setup();
	}


	void setup_RootPartition()
	{
	}

	void setup_WithKernel(
			CEdgeComm_ValidateComm &parent
	)
	{
	}


	inline void computeEdgeCommData_Hyp(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n0x;
		dst_comm_data->ny = n0y;

		dst_comm_data->mx = (v0x+v1x)*0.5;
		dst_comm_data->my = (v0y+v1y)*0.5;
	}

	inline void computeEdgeCommData_Right(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n1x;
		dst_comm_data->ny = n1y;

		dst_comm_data->mx = (v1x+v2x)*0.5;
		dst_comm_data->my = (v1y+v2y)*0.5;
	}

	inline void computeEdgeCommData_Left(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data
	)
	{
		dst_comm_data->nx = n2x;
		dst_comm_data->ny = n2y;

		dst_comm_data->mx = (v0x+v2x)*0.5;
		dst_comm_data->my = (v0y+v2y)*0.5;
	}



	inline void computeBoundaryCommData_Hyp(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n0x;
		dst_comm_data->ny = n0y;

		dst_comm_data->mx = (v0x+v1x)*0.5;
		dst_comm_data->my = (v0y+v1y)*0.5;
	}

	inline void computeBoundaryCommData_Right(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data)
	{
		dst_comm_data->nx = n1x;
		dst_comm_data->ny = n1y;

		dst_comm_data->mx = (v1x+v2x)*0.5;
		dst_comm_data->my = (v1y+v2y)*0.5;
	}

	inline void computeBoundaryCommData_Left(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element_data,
			CValEdgeData *dst_comm_data
	)
	{
		dst_comm_data->nx = n2x;
		dst_comm_data->ny = n2y;

		dst_comm_data->mx = (v0x+v2x)*0.5;
		dst_comm_data->my = (v0y+v2y)*0.5;
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}

	inline void checkNormal(
						TVertexScalar n0x, TVertexScalar n0y,
						CValEdgeData *e)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		TVertexScalar a = n0x*e->ny - n0y*e->nx;
		if (CMath::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: INVALID NORMAL" << std::endl;
#endif
	}


	inline void checkRightMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		TVertexScalar mx = (v1x+v2x)*0.5;
		TVertexScalar my = (v1y+v2y)*0.5;

		TVertexScalar a = CMath::abs(mx-e->mx) + CMath::abs(my-e->my);

		if (CMath::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: RIGHT EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);
//		std::cout << "Right: " << m << " " << ed->right << std::endl;


		if (!noElementDataChecks)
		{
			if (CMath::abs((m-ed->right).getLength()) > 0.0001)
			{
				std::cout << "EDGECOMM VALIDATION FAILED: RIGHT EDGE MIDPOINT compared with element data" << std::endl;
				std::cout << m << " " << ed->right << std::endl;
				ed->right = m;
			}
		}
#endif
	}


	inline void checkLeftMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		TVertexScalar mx = (v0x+v2x)*0.5;
		TVertexScalar my = (v0y+v2y)*0.5;

		TVertexScalar a = CMath::abs(mx-e->mx) + CMath::abs(my-e->my);

		if (CMath::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: LEFT EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);
//		std::cout << "Right: " << m << " " << ed->right << std::endl;

		if (!noElementDataChecks)
		{
			if (CMath::abs((m-ed->left).getLength()) > 0.0001)
			{
				std::cout << "EDGECOMM VALIDATION FAILED: LEFT EDGE MIDPOINT compared with element data" << std::endl;
				std::cout << m << " " << ed->left << std::endl;
				ed->left = m;
			}
		}
#endif
	}



	inline void checkHypMidpoint(
						TVertexScalar v0x, TVertexScalar v0y,
						TVertexScalar v1x, TVertexScalar v1y,
						TVertexScalar v2x, TVertexScalar v2y,
						CValEdgeData *e,
						CValElementData *ed
	)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		TVertexScalar mx = (v0x+v1x)*0.5;
		TVertexScalar my = (v0y+v1y)*0.5;


		TVertexScalar a = CMath::abs(mx-e->mx) + CMath::abs(my-e->my);

		if (CMath::abs(a) > 0.0001)
			std::cout << "EDGECOMM VALIDATION FAILED: HYP EDGE MIDPOINT" << std::endl;

		CVertex2d<float> m(mx,my);

		if (!noElementDataChecks)
		{
			if (CMath::abs((m-ed->hyp).getLength()) > 0.0001)
			{
				std::cout << "EDGECOMM VALIDATION FAILED: HYP EDGE MIDPOINT compared with element data" << std::endl;
				std::cout << m << " " << ed->hyp << std::endl;
				ed->hyp = m;
			}
		}
#endif
	}

	inline void elementAction(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *right_edge,
			CValEdgeData *left_edge
	)
	{
		checkNormal(n0x, n0y, hyp_edge);
		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

		checkNormal(n1x, n1y, right_edge);
		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);

		checkNormal(n2x, n2y, left_edge);
		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}


	inline void elementAction_BBB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element
	)
	{
	}

	inline void elementAction_BEE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *right_edge,
			CValEdgeData *left_edge
	)
	{

		checkNormal(n1x, n1y, right_edge);
		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);

		checkNormal(n2x, n2y, left_edge);
		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}

	inline void elementAction_BBE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *left_edge
	)
	{
		checkNormal(n2x, n2y, left_edge);
		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}

	inline void elementAction_BEB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *right_edge
	)
	{
		checkNormal(n1x, n1y, right_edge);
		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);
	}


	inline void elementAction_EBE(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *left_edge
	)
	{
		checkNormal(n0x, n0y, hyp_edge);
		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

		checkNormal(n2x, n2y, left_edge);
		checkLeftMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, left_edge, element);
	}

	inline void elementAction_EEB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge,
			CValEdgeData *right_edge
	)
	{
		checkNormal(n0x, n0y, hyp_edge);
		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);

		checkNormal(n1x, n1y, right_edge);
		checkRightMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, right_edge, element);
	}


	inline void elementAction_EBB(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,

			CValElementData *element,
			CValEdgeData *hyp_edge
	)
	{
		checkNormal(n0x, n0y, hyp_edge);
		checkHypMidpoint(v0x, v0y, v1x, v1y, v2x, v2y, hyp_edge, element);
	}

};

}
}


#endif /* CEDGECOMM_TESTNORMAL_HPP_ */
