/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef COPENGL_VERTICES_GRID_ZDEPTH_DISPLACEMENT_HPP_
#define COPENGL_VERTICES_GRID_ZDEPTH_DISPLACEMENT_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{


class COpenGL_VertexCoords_Wireframe
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords<sierpi::kernels::COpenGL_VertexCoords_Wireframe, CTsunamiSimulationStacks> TRAV;

	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

private:
	TVertexScalar *vertex_buffer;
	TVertexScalar *current_triangle;
	TVertexScalar *last_triangle;
	size_t max_triangles;

public:
	inline COpenGL_VertexCoords_Wireframe(
			size_t p_max_triangles = 16
	)	:
			max_triangles(p_max_triangles)
	{
		vertex_buffer = new TVertexScalar[3*3*2*max_triangles];
		last_triangle = vertex_buffer+3*3*2*(max_triangles-1);

		current_triangle = vertex_buffer;
	}

	inline ~COpenGL_VertexCoords_Wireframe()
	{
		delete vertex_buffer;
	}

	inline void renderOpenGLVertexArray(size_t triangles)
	{
		glDrawArrays(GL_LINES, 0, triangles*3*2);
	}

	inline void elementAction(
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			TVertexScalar v3x, TVertexScalar v3y)
	{
		/*
		 * TODO: drawing hyp line would be enough (except on the border)
		 * (but not for filled triangles...)
		 */

		current_triangle[0*3+0] = v1x;
		current_triangle[0*3+1] = 0;
		current_triangle[0*3+2] = -v1y;

		current_triangle[1*3+0] = v2x;
		current_triangle[1*3+1] = 0;
		current_triangle[1*3+2] = -v2y;


		current_triangle[2*3+0] = v2x;
		current_triangle[2*3+1] = 0;
		current_triangle[2*3+2] = -v2y;

		current_triangle[3*3+0] = v3x;
		current_triangle[3*3+1] = 0;
		current_triangle[3*3+2] = -v3y;


		current_triangle[4*3+0] = v3x;
		current_triangle[4*3+1] = 0;
		current_triangle[4*3+2] = -v3y;

		current_triangle[5*3+0] = v1x;
		current_triangle[5*3+1] = 0;
		current_triangle[5*3+2] = -v1y;

		assert(current_triangle <= last_triangle);
		if (current_triangle == last_triangle)
		{
			renderOpenGLVertexArray(max_triangles);
			current_triangle = vertex_buffer;
			return;
		}

		current_triangle += 6*3;
	}

	inline void traversal_pre_hook()
	{
		glVertexAttrib3f(1, 0, 1, 0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vertex_buffer);
		glEnableVertexAttribArray(0);
	}

	inline void traversal_post_hook()
	{
		if (current_triangle > vertex_buffer)
		{
			renderOpenGLVertexArray(
					(size_t)(current_triangle-vertex_buffer)/(3*3*2));
		}

		glDisableVertexAttribArray(0);
	}
};

}
}

#endif /* CSTRUCTURE_VERTEXHANDLER_OPENGL_H_ */
