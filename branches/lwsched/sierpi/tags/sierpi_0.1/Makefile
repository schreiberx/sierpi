SCONS_OPTS:=-Q -j4

all:	gnu	intel	serial	nosacsp


tests:	gnu intel iomp fancy fancy_intel gnu_serial_regular nosacsp

# make targets for pproc servers
pproc:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release

dpproc:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=debug

iomp:
	# version with iomp
	scons $(SCONS_OPTS) --threading=iomp --compiler=gnu --gui=off --mode=release

ipmo:
	# version with ipmo and omp
	scons $(SCONS_OPTS) --threading=ipmo --compiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release

ipmo_gnu:
	# version with ipmo and omp using gnu compiler
	scons $(SCONS_OPTS) --threading=ipmo --compiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=release


omp:
	# version with OpenMP
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=release

dfancy_rk2:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=debug --tsunami-runge-kutta-order=2

dfancy:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=debug

dfancy_intel:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=on --mode=debug
        

fancy:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=release

fancy_intel:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=on --mode=release

fancy_asagi:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=release --use-asagi=true --use-mpicompiler=true

nosacsp:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release --sacsp=off


gnu:
	scons $(SCONS_OPTS) --threading=off --compiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --compiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=on --mode=release

fortran:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release --enable-fortran=true

fortran_asagi:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release --enable-fortran=true --use-asagi=true  --use-mpicompiler=true


#
# SERIAL
#
serial:	gnu_serial	intel_serial

gnu_serial_gui:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --gui=on --mode=release

gnu_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --gui=on --mode=release

gnu_serial_debug:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --gui=off --mode=debug
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --gui=on --mode=debug


intel_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --gui=on --mode=release


#
# SERIAL REGULAR
#
serial_regular:	gnu_serial_regular_table intel_serial_regular_table gnu_serial_regular_recursive intel_serial_regular_recursive

gnu_serial_regular_table:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=off --mode=release --serial_regular_type=table
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=on --mode=release --serial_regular_type=table

gnu_serial_regular_recursive:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=off --mode=release --serial_regular_type=recursive
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=on --mode=release --serial_regular_type=recursive

intel_serial_regular_table:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=intel --gui=off --mode=release --serial_regular_type=table
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=intel --gui=on --mode=release --serial_regular_type=table

intel_serial_regular_recursive:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=intel --gui=off --mode=release --serial_regular_type=recursive
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=intel --gui=on --mode=release --serial_regular_type=recursive



#
# TEST
#
test_release:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=on --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=off --mode=release

test_debug:
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=on --mode=debug
	scons $(SCONS_OPTS) --simulation=tsunami_serial_regular --compiler=gnu --gui=off --mode=debug

gnu_r:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --gui=off --mode=release

intel_r:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release

intel:
	# OpenMP
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=release
#	scons $(SCONS_OPTS) --threading=off --compiler=intel --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --compiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --gui=on --mode=release

tbb:
	# TBB
	scons $(SCONS_OPTS) --threading=tbb --compiler=intel --gui=off --mode=release
	scons $(SCONS_OPTS) --threading=tbb --compiler=intel --gui=on --mode=release


serial_intel:
	# serial
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --gui=off --mode=release



clean_gnu:
	rm -rf build/build_sierpi_gnu_*

clean_intel:
	rm -rf build/build_sierpi_intel_*

clean: clean_gnu clean_intel
	rm -f build/sierpi*
