/*
 * CEdgeComm_Tsunami_1stOrder_BoundaryConditions.hpp
 *
 *  Created on: Nov 6, 2011
 *      Author: schreibm
 */

#ifndef CEDGECOMM_TSUNAMI_1STORDER_BOUNDARYCONDITIONS_HPP_
#define CEDGECOMM_TSUNAMI_1STORDER_BOUNDARYCONDITIONS_HPP_


/**
 * DIRICHLET BOUNDARY CONDITION
 */
template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;
	hyp_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BBB" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, &right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BEE" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EBE" << std::endl;
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif

	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EEB" << std::endl;
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EBB" << std::endl;
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}



template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData left_edge;

	hyp_edge = config.boundary_dirichlet;
	left_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BEB" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData right_edge;

	hyp_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BBE" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, &right_edge, left_edge);
}


/**
 * BOUNDARY CONDITION: VELOCITY_ZERO
 */

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge.h = element->hyp_edge.h;
	hyp_edge.qx = 0;
	hyp_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge.h = element->right_edge.h;
	right_edge.qx = 0;
	right_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = 0;
	left_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = 0;
	left_edge.qy = 0;

	right_edge.h = element->right_edge.h;
	right_edge.qx = 0;
	right_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}

/**
 * BOUNDARY CONDITION: BOUNCE_BACK
 */

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge.h = element->hyp_edge.h;
	hyp_edge.qx = -element->hyp_edge.qx;
	hyp_edge.qy = -element->hyp_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge.h = element->right_edge.h;
	right_edge.qx = -element->right_edge.qx;
	right_edge.qy = -element->right_edge.qy;

	#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = -element->left_edge.qx;
	left_edge.qy = -element->left_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = -element->left_edge.qx;
	left_edge.qy = -element->left_edge.qy;

	right_edge.h = element->right_edge.h;
	right_edge.qx = -element->right_edge.qx;
	right_edge.qy = -element->right_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif

	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}



#endif /* CEDGECOMM_TSUNAMI_1STORDER_BOUNDARYCONDITIONS_HPP_ */
