/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#define COMPILE_SIMULATION_WITH_GUI	0

#include <stdlib.h>
#include <stdint.h>
#include <sstream>

#if COMPILE_WITH_OMP
	#include <omp.h>
#endif

#if COMPILE_WITH_TBB
#include <tbb/task_scheduler_init.h>
#endif

#include "config.h"
#include "lib/iRef.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "lib/CProcessMemoryInformation.hpp"
#include "lib/CStopwatch.hpp"

#if SIMULATION_TSUNAMI_SERIAL
	#include "simulations/tsunami_serial/CSimulationTsunami_Serial.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami
#endif

#if SIMULATION_TSUNAMI_SERIAL_REGULAR
	#include "simulations/tsunami_serial_regular/CSimulationTsunami_Serial_Regular.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami
#endif

#if SIMULATION_TSUNAMI_PARALLEL
	#include "simulations/tsunami_parallel/CSimulationTsunami_Parallel.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami_Parallel
#endif

#if COMPILE_WITH_IOMP
	#include <OR.hpp>
	#include <AND.hpp>
	#include <Claim.hpp>
	#include <PEQuantity.hpp>
	#include <ScalabilityHint.hpp>
#endif


CProcessMemoryInformation cProcessMemoryInformation;


int timesteps = -1;
float timestepSize = -1;

int initialRecursionDepth = 6;
int minRelativeRecursionDepth = 0;
int maxRelativeRecursionDepth = 6;

int number_of_threads_to_use = -1;

bool divide_number_of_timesteps_by_timestep_size = false;

unsigned long long partition_split_workload_size = 512;
unsigned long long partition_join_workload_size = 0;

int world_scene_id = 3;

int verbose = 0;

int output_verbose_information_each_nth_timestep = -1;
int output_simulation_data_each_nth_timestep = -1;
double output_verbose_information_after_each_n_seconds = -1.0;

const char *output_simulation_data_filename = "frame_%08i.vtk";
bool output_partitions_to_vtk_file = false;

int elapsed_timesteps_to_update_split_and_join_parameters = 10;
double elapsed_timesteps_to_udpate_split_and_join_scalar = 5.5;


#if COMPILE_WITH_TBB
    tbb::task_scheduler_init *tbb_task_scheduler_init;
#endif


#if 0
extern long int random (void)	throw()
{
	std::cout << "RANDOM FUNCTION CALLED!" << std::endl;
	exit(-1);
}
#endif



void run()
{
	SIMULATION_CLASS cSimulation;

	if (verbose > 2)
	{
		cSimulation.outputVerboseInformation();
	}

	/**************************
	 * SETUP
	 */
#if SIMULATION_TSUNAMI_PARALLEL
	cSimulation.setWorldSceneId(world_scene_id);
#endif

	cSimulation.setInitialRecursionDepth(initialRecursionDepth);
	cSimulation.setMinRelativeRecursionDepth(minRelativeRecursionDepth);
	cSimulation.setMaxRelativeRecursionDepth(maxRelativeRecursionDepth);

#if SIMULATION_TSUNAMI_PARALLEL
	cSimulation.setSplitJoinSizes(partition_split_workload_size, partition_join_workload_size);
	cSimulation.setElapsedTimestepsForAutomaticSplitJoinUpdates(elapsed_timesteps_to_update_split_and_join_parameters, elapsed_timesteps_to_udpate_split_and_join_scalar);
#endif

	cSimulation.computeTimestepSize(timestepSize);
	cSimulation.reset_Simulation();


	if (divide_number_of_timesteps_by_timestep_size)
	{
		if (verbose > 2)
			std::cout << "Updating number of timesteps from " << timesteps << " to ";

		timesteps = (int)((double)timesteps/(double)cSimulation.timestep_size);

		if (verbose > 2)
			std::cout << timesteps << std::endl;

		if (verbose > 2)
			std::cout << "Timesteps: " << timesteps << std::endl;

		if (verbose > 2)
			std::cout << "Updating number of output of simulation data from " << output_simulation_data_each_nth_timestep << " to ";
			output_simulation_data_each_nth_timestep = (int)((double)output_simulation_data_each_nth_timestep/(double)cSimulation.timestep_size);

		if (verbose > 2)
			std::cout << "Output_simulation_data_each_nth_timestep: " << output_simulation_data_each_nth_timestep;

		std::cout << std::endl;
	}


	if (verbose > 2)
	{
		std::cout << " + Number of threads to use: " << number_of_threads_to_use << std::endl;
		std::cout << " + Initial recursion depth: " << initialRecursionDepth << std::endl;
		std::cout << " + Verbose: " << verbose << std::endl;
		std::cout << " + Timesteps: " << timesteps << std::endl;
		std::cout << " + Timestep size: " << cSimulation.timestep_size << std::endl;
		std::cout << " + Output .vtk files each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
		std::cout << " + Output simulation data filename: " << output_simulation_data_filename << std::endl;
		std::cout << " + InitialDepth: " << initialRecursionDepth << std::endl;
		std::cout << " + MinDepth: " << (initialRecursionDepth+minRelativeRecursionDepth) << std::endl;
		std::cout << " + MaxDepth: " << (initialRecursionDepth+maxRelativeRecursionDepth) << std::endl;
		std::cout << " + WorldSceneId: " << world_scene_id << std::endl;
		std::cout << " + Partition size when to request split: " << partition_split_workload_size << std::endl;
		std::cout << " + Partition size when to request join (both childs have to request a join): " << partition_join_workload_size << std::endl;

		if (elapsed_timesteps_to_update_split_and_join_parameters > 0)
		{
			std::cout << " + elapsed_timesteps_to_update_split_and_join_parameters: " << elapsed_timesteps_to_update_split_and_join_parameters << std::endl;
			std::cout << " + elapsed_timesteps_to_udpate_split_and_join_scalar: " << elapsed_timesteps_to_udpate_split_and_join_scalar << std::endl;
		}

		if (cSimulation.timestep_size == -1)
			std::cout << " + Timestep Size: adaptive (CFL)" << std::endl;
		else
			std::cout << " + Timestep Size: " << cSimulation.timestep_size << std::endl;

#if ADAPTIVE_SUBPARTITION_STACKS
		std::cout << " + ADAPTIVE_SUBPARTITION_STACKS enabled" << std::endl;
#endif
	}

	/**************************
	 * remember initial number of triangles
	 */
	unsigned long long initial_number_of_triangles = cSimulation.number_of_triangles;
	if (verbose > 2)
		std::cout << " + Initial number of triangles before setting up column: " << initial_number_of_triangles << std::endl;


	/**************************
	 * setup column
	 */
	if (verbose > 0)
		std::cout << "[ SETUP ]" << std::endl;

	if (verbose > 1)
		std::cout << " + initial number of triangles: " << cSimulation.number_of_triangles << std::endl;


	cSimulation.number_of_triangles = cSimulation.setup_ColumnAt2DPosition(-0.5, 0.4, 0.2);

	if (verbose > 1)
		std::cout << " + refined to " << cSimulation.number_of_triangles << " triangles" << std::endl;

	if (verbose > 0)
		std::cout << " + pre splitting partitions" << std::endl;


	/****************************
	 * setup initial split of partitions
	 */
	{
		size_t t = std::numeric_limits<size_t>::max();

		while (cSimulation.number_of_simulation_handler_partitions != t)
		{
			t = cSimulation.number_of_simulation_handler_partitions;

#if SIMULATION_TSUNAMI_PARALLEL
			cSimulation.setup_SplitJoinPartitions();
#endif

			if (verbose > 1)
				std::cout << " + splitted to " << cSimulation.number_of_simulation_handler_partitions << " partitions" << std::endl;
		}
	}

	CStopwatch cStopwatch;


	if (verbose > 1)
	{
		std::cout << "[ START ]" << std::endl;
	}

	cStopwatch.start();

	/**************************
	 * SIMULATION
	 */
	unsigned long long sum_number_of_triangles = 0;
	unsigned long long sum_number_of_partitions = 0;
	char char_buffer[1024];

	int outputVTKFrame = 0;
	int outputVerboseFrame = 0;
	int frameCounter = 0;

	/*
	 * stopwatches for edge/adaptive/split-join traversals
	 */
	double edgeCommTime = 0;
	double adaptiveTime = 0;
	double splitJoinTime = 0;

	/*
	 * output verbose information after output_verbose_information_after_each_n_seconds seconds
	 */
	double verbose_output_time = CMath::numeric_inf<double>();
	if (output_verbose_information_after_each_n_seconds > 0)
		verbose_output_time = 0;

	if (output_verbose_information_each_nth_timestep == -1 && verbose > 3)
		output_verbose_information_each_nth_timestep = 100;

	double simulation_time = 0;

//	if (output_verbose_information_each_nth_timestep == -1)
//		output_verbose_information_each_nth_timestep = 100;

	/**
	 * Simulation loop
	 */
	if (number_of_threads_to_use == -1)
	{
#if COMPILE_WITH_OMP

#pragma omp parallel
#pragma omp single
		number_of_threads_to_use = omp_get_max_threads();

#elif COMPILE_WITH_TBB
		number_of_threads_to_use = 1;
#else
		number_of_threads_to_use = 1;
#endif
	}

#if COMPILE_WITH_IOMP
	int max_num_threads = omp_get_max_threads();

	Claim claim;
#endif

	bool finishSimulation = false;
	int timestep;
	for (timestep = 0; timestep != timesteps && !finishSimulation; timestep++)
	{
#if COMPILE_WITH_IOMP
#if 0
		/*
		 * increase number of threads to maximum when exceeding twice the
		 * initial number of triangles
		 */
		int num_threads;
//		std::cout << initial_number_of_triangles*2 << " > " << cSimulation.number_of_triangles << std::endl;

		if (initial_number_of_triangles*2 > cSimulation.number_of_triangles)
			num_threads = 1;
		else
			num_threads = max_num_threads;
#else
		int num_threads = max_num_threads;
#endif
		float shTable[] = {
				1,
				0.957,
				0.9423,
				0.914,
				0.8926,
				0.8557,
				0.821,
				0.7868,
				0.7532,
				0.725,
				0.6886,
				0.6587,
		};

		ScalabilityHint sh;

		for (int i = 1; i <= sizeof(shTable)/sizeof(float); i++)
		{
			double scalability = shTable[i-1]*(double)i;
			std::cout << "iOMP: adding scalability hint " << i << ", " << scalability << std::endl;
			sh.insert(i, scalability);
		}


		AND a;
		a.add(new PEQuantity(1, num_threads));
		a.add(&sh);

		claim.invade(a);

	#if COMPILE_WITH_OMP
	#pragma omp parallel
	#pragma omp single nowait
	#endif
#endif
		{
			/**
			 * output some data?
			 */
			bool outputInfo = false;

			if (output_simulation_data_each_nth_timestep > 0)
			{
				if (outputVTKFrame == 0)
				{
					sprintf(char_buffer, output_simulation_data_filename, frameCounter);

					if (verbose > 3)
					{
						std::cout << "=========================================" << std::endl;
						std::cout << "   + writing file " << char_buffer << std::endl;
						outputInfo = true;
					}

					cSimulation.writeTrianglesToVTKFile(char_buffer);
					std::string partitionFile = "partitions_";
					partitionFile += char_buffer;
#if SIMULATION_TSUNAMI_PARALLEL
					if (output_partitions_to_vtk_file)
						cSimulation.writePartitionsToVTKFile(partitionFile.c_str());
#endif
					frameCounter++;
				}

				outputVTKFrame = (outputVTKFrame+1) % output_simulation_data_each_nth_timestep;
			}
			else if (
					verbose > 3 ||
					output_verbose_information_each_nth_timestep != -1
				)
			{
				if (outputVerboseFrame == 0)
				{
					std::cout << "=========================================" << std::endl;
					outputInfo = true;

					if (verbose > 6)
					{
						cProcessMemoryInformation.outputUsageInformation();
					}
				}

				outputVerboseFrame = (outputVerboseFrame+1) % output_verbose_information_each_nth_timestep;
			}
			else if (verbose_output_time <= simulation_time)
			{
				std::cout << "=========================================" << std::endl;
				outputInfo = true;

				if (verbose_output_time < simulation_time)
					verbose_output_time += output_verbose_information_after_each_n_seconds;
			}


			if (outputInfo)
			{
				if (timestep != 0)
					std::cout << "   + " << ((double)sum_number_of_triangles/cStopwatch.getTimeSinceStart())*0.000001 << " MTPS(Mega Triangles per second)" << std::endl;
				std::cout << "   + " << timestep << "\tTIMESTEP" << std::endl;
				std::cout << "   + " << simulation_time << "\tSIMULATION_TIME" << std::endl;
				std::cout << "   + " << cSimulation.number_of_triangles << "\tTRIANGLES" << std::endl;
				double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation.number_of_triangles)/(1024.0*1024.0);
				std::cout << "   + " << elementdata_megabyte_per_timestep << "\tElementData Megabyte per Timestep (RW)" << std::endl;
				std::cout << "   + " << cSimulation.timestep_size << "\tTIMESTEP SIZE" << std::endl;
				std::cout << "   + " << cSimulation.number_of_simulation_handler_partitions << "\tPARTITIONS" << std::endl;
			}


			/*
			 * single simulation timestep
			 */
			if (verbose > 2)
			{
				cSimulation.runSingleTimestepDetailedBenchmarks(
						&edgeCommTime,
						&adaptiveTime,
						&splitJoinTime
					);
			}
			else
			{
				cSimulation.runSingleTimestep();
			}

			simulation_time += cSimulation.timestep_size;

			/*
			 * increment counter of number of triangles processed so far
			 */
			sum_number_of_triangles += cSimulation.number_of_triangles;
			sum_number_of_partitions += cSimulation.number_of_simulation_handler_partitions;

			/*
			 * verbose points
			 */
			if (verbose > 5)
			{
				std::cout << "." << std::flush;
			}

			/*
			 * if timesteps are not set, we quit as soon as the initial number of triangles was reached
			 */
			if (timesteps == -1)
			{
				if (initial_number_of_triangles == cSimulation.number_of_triangles)
				{
					std::cout << std::endl;
					std::cout << " + Initial number of triangles reached -> EXIT" << std::endl;
					std::cout << " + Final timestep: " << timestep << std::endl;
					std::cout << std::endl;
					finishSimulation = true;
				}
			}
		}	// end parallel region iOMP
	}

	cStopwatch.stop();

	if (timesteps == -1)
	{
		timesteps = timestep;
	}

	if (verbose > 1)
	{
		std::cout << "[ END ]" << std::endl;
		std::cout << std::endl;
	}


	if (verbose > 2)
	{
		std::cout << std::endl;
		std::cout << "Timings for simulation phases:" << std::endl;
		std::cout << " + EdgeCommTime: " << edgeCommTime << std::endl;
		std::cout << " + AdaptiveTime: " << adaptiveTime << std::endl;
		std::cout << " + SplitJoinTime: " << splitJoinTime << std::endl;
		std::cout << std::endl;
	}


	double stoptime = cStopwatch();

	std::cout << timesteps << " TS (Timesteps)" << std::endl;
	std::cout << simulation_time << " ST (SIMULATION_TIME)" << std::endl;
	std::cout << cSimulation.timestep_size << " TSS (Timestep size)" << std::endl;
	std::cout << stoptime << " SFS (Overall seconds for Simulation)" << std::endl;
	std::cout << sum_number_of_triangles << " TP (Triangles processed)" << std::endl;
	std::cout << (double)stoptime/(double)timesteps << " ASPT (Averaged Seconds per Timestep)" << std::endl;
	std::cout << (double)sum_number_of_triangles/(double)timesteps << " TPST (Triangles Processed in Average per Simulation Timestep)" << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
	std::cout << (double)sum_number_of_partitions/(double)timesteps << " PPST (Partitions Processed in Average per Simulation Timestep)" << std::endl;
#endif

	std::cout << ((double)sum_number_of_triangles/cStopwatch())*0.000001 << " MTPS (Million Triangles per Second)" << std::endl;
	std::cout << ((double)sum_number_of_triangles/(cStopwatch()*(double)number_of_threads_to_use))*0.000001 << " MTPSPT (Million Triangles per Second per Thread)" << std::endl;

	double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)sum_number_of_triangles/(double)timesteps)/(1024.0*1024.0);
	std::cout << elementdata_megabyte_per_timestep << " EDMBPT (ElementData Megabyte per Timestep (RW))" << std::endl;

	double elementdata_megabyte_per_second = ((double)sizeof(CTsunamiElementData)*2.0)*((double)sum_number_of_triangles/(double)stoptime)/(1024.0*1024.0);
	std::cout << elementdata_megabyte_per_second << " EDMBPS (ElementData Megabyte per Second (RW))" << std::endl;
}



int main(int argc, char *argv[])
{
	char optchar;
	while ((optchar = getopt(argc, argv, "d:t:f:w:a:i:s:v:n:o:l:rpg:u:U:b:h:")) > 0)
	{
		switch(optchar)
		{
		case 'r':
			divide_number_of_timesteps_by_timestep_size = true;
			break;

		case 'p':
			output_partitions_to_vtk_file = true;
			break;

		case 'd':
			initialRecursionDepth = atoi(optarg);
			break;

		case 'v':
			verbose = atoi(optarg);
			break;

		case 't':
			timesteps = atoi(optarg);
			break;

		case 's':
			timestepSize = atof(optarg);
			break;

		case 'f':
			output_simulation_data_each_nth_timestep = atoi(optarg);
			if (output_verbose_information_each_nth_timestep == -1)
				output_verbose_information_each_nth_timestep = output_simulation_data_each_nth_timestep;
			break;

		case 'h':
			output_verbose_information_after_each_n_seconds = atof(optarg);
			break;

		case 'b':
			output_verbose_information_each_nth_timestep = atoi(optarg);
			break;

		case 'g':
			output_simulation_data_filename = optarg;
			break;

		case 'n':
			number_of_threads_to_use = atoi(optarg);
			break;

		case 'w':
			world_scene_id = atoi(optarg);
			break;

		case 'i':
			minRelativeRecursionDepth = atoi(optarg);
			break;

		case 'a':
			maxRelativeRecursionDepth = atoi(optarg);
			break;

		case 'o':
			partition_split_workload_size = atoi(optarg);

			// deactivate auto-mode
			elapsed_timesteps_to_update_split_and_join_parameters = 0;
			break;

		case 'l':
			partition_join_workload_size = atoi(optarg);

			// deactivate auto-mode
			elapsed_timesteps_to_update_split_and_join_parameters = 0;
			break;

		case 'u':
			elapsed_timesteps_to_update_split_and_join_parameters = atoi(optarg);
			break;

		case 'U':
			elapsed_timesteps_to_udpate_split_and_join_scalar = atof(optarg);
			break;

		default:
			goto parameter_error;
		}

	}

	goto parameter_error_ok;

	parameter_error:
	std::cout << "usage: " << argv[0] << std::endl;
	std::cout << "	[-d [int]: initial recursion depth]" << std::endl;
	std::cout << "	[-v [int]: verbose mode (0-5)]" << std::endl;
	std::cout << "	[-t [int]: timesteps]" << std::endl;
	std::cout << "	[-r : divide number of timesteps and frame output rate by timestep size]" << std::endl;
	std::cout << "	[-s [float]: timestep size]" << std::endl;
	std::cout << "	[-p : also write vtk files with partitions]" << std::endl;
	std::cout << "	[-f [int]: output .vtk files each #nth timestep]" << std::endl;
	std::cout << "	[-b [int]: output verbose information each #nth timestep]" << std::endl;
	std::cout << "  [-h [double]: output verbose information each n seconds]" << std::endl;
	std::cout << "	[-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;
	std::cout << "	[-i [int]: min relative recursion depth]" << std::endl;
	std::cout << "	[-a [int]: max relative recursion depth]" << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
	std::cout << "	[-n [int]: number of threads to use]" << std::endl;
	std::cout << "	[-w [int]: world scene]" << std::endl;
	std::cout << "	[-o [int]: partition size when to request split]" << std::endl;
	std::cout << "	[-l [int]: partition size when to request join (both childs have to request a join)]" << std::endl;
	std::cout << "	[-u [int]: elapsed timesteps when to update split and join parameters with table hints]" << std::endl;
	std::cout << "	[-U [double]: factor used for automatic computation of split/join parameter. default: 0 (disabled)]" << std::endl;
#endif

	return -1;

parameter_error_ok:
	if (verbose > 5)
	{
		cProcessMemoryInformation.outputUsageInformation();
	}
	srand(0);


#if COMPILE_WITH_TBB
	// setup scheduler
	tbb_task_scheduler_init = new tbb::task_scheduler_init(number_of_threads_to_use);

	if (verbose > 5)
		std::cout << "tbb::task_scheduler_init::default_num_threads(): " << tbb::task_scheduler_init::default_num_threads() << std::endl;
#endif


#if COMPILE_WITH_OMP
	if (number_of_threads_to_use == -1)
		number_of_threads_to_use = omp_get_num_threads();
	else
		omp_set_num_threads(number_of_threads_to_use);

	if (verbose > 5)
		std::cout << "omp_get_max_threads(): " << omp_get_max_threads() << std::endl;
#endif

	if (partition_split_workload_size > 0 && partition_split_workload_size < 2)
	{
		std::cout << "Invalid split size: " << partition_split_workload_size << "   setting split to 2" << std::endl;
		partition_split_workload_size = 2;
	}

	if (partition_join_workload_size == 0)
	{
		partition_join_workload_size = partition_split_workload_size/2;
	}
	else
	{
		if (partition_join_workload_size > partition_split_workload_size)
		{
			std::cerr << "ERROR: partition_join_workload_size > partition_split_workload_size" << std::endl;
			return -1;
		}
	}

	/*
	 * RUN THE SIMULATION WITH PARALLELIZATION
	 */
#if COMPILE_WITH_IOMP
	/*
	 * we have to move the parallel region into the simulation
	 * timestep loop to control the amount of CPUs which are used
	 */
#else
	#if COMPILE_WITH_OMP
	#pragma omp parallel
	#pragma omp single nowait
	#endif
#endif
	{
		run();
	}

#if COMPILE_WITH_TBB
		delete tbb_task_scheduler_init;
#endif


#ifdef DEBUG
	if (!debug_ibase_list.empty())
	{
		std::cout << "MEMORY LEAK: iBase class missing in action" << std::endl;
		std::cout << "  + number of classes: " << debug_ibase_list.size() << std::endl;
	}
#endif

	if (verbose > 5)
	{
		cProcessMemoryInformation.outputUsageInformation();
	}

	return 0;
}
