/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_


#include "config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
#include "libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// flux projections
#include "CFluxProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (elementAction)
 */
class CEdgeComm_Tsunami_1stOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::TReduceValue;
	using CEdgeComm_TsunamiParameters::getReduceValue;
	using CEdgeComm_TsunamiParameters::getTimestepSize;
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;

	using CEdgeComm_TsunamiParameters::updateCFLCondition3;
	using CEdgeComm_TsunamiParameters::updateCFLCondition2;
	using CEdgeComm_TsunamiParameters::updateCFLCondition1;


	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder> TRAV;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_1stOrder()
	{
		boundary_dirichlet.h = CMath::numeric_inf<T>();
		boundary_dirichlet.qx = CMath::numeric_inf<T>();
		boundary_dirichlet.qy = CMath::numeric_inf<T>();
	}



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_1stOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-partition
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-partition
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->hyp_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->right_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 */
	inline void computeEdgeCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->left_edge;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->hyp_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->hyp_edge.h;
			o_edge_comm_data->qx = -i_elementData->hyp_edge.qx;
			o_edge_comm_data->qy = -i_elementData->hyp_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_elementData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->right_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->right_edge.h;
			o_edge_comm_data->qx = -i_elementData->right_edge.qx;
			o_edge_comm_data->qy = -i_elementData->right_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_elementData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			*o_edge_comm_data = i_elementData->left_edge;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->left_edge.h;
			o_edge_comm_data->qx = -i_elementData->left_edge.qx;
			o_edge_comm_data->qy = -i_elementData->left_edge.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_elementData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void computeElementDataUpdate_WithFluxes(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			CTsunamiEdgeData &i_hyp_edge_flux,		///< incoming flux from hypotenuse
			CTsunamiEdgeData &i_right_edge_flux,	///< incoming flux from right edge
			CTsunamiEdgeData &i_left_edge_flux,		///< incoming flux from left edge

			CTsunamiElementData *io_elementData
	)
	{
		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(square_side_length > 0);

		if (CMath::isNan(io_elementData->hyp_edge.h + io_elementData->right_edge.h + io_elementData->left_edge.h))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*square_side_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*square_side_length;

#if 0
		TTsunamiDataScalar damping_per_second = (T)0.9999;
		TTsunamiDataScalar damping = (T)1.0-(((T)1.0-damping_per_second)*(timestep_size));

		damping = CMath::min(damping, (T)0.9999);
		damping = CMath::max(damping, (T)0.99);

		if (i != 0xfff)
		{
			std::cout << damping << std::endl;
			i = 0xfff;
		}

//		std::cout << damping << std::endl;

//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*delta_timestep/cat_length, (T)0.99);

//		TTsunamiDataScalar damping = (T)1.0-(delta_timestep/cat_length);
//		TTsunamiDataScalar damping = (T)1.0-(delta_timestep/cat_length);

		io_elementData->hyp_edge.qx *= damping;
		io_elementData->hyp_edge.qy *= damping;

		io_elementData->left_edge.qx *= damping;
		io_elementData->left_edge.qy *= damping;

		io_elementData->right_edge.qx *= damping;
		io_elementData->right_edge.qy *= damping;
#endif

		// rotate flux
		CFluxProjections::projectToNormalSpaceAndInvert(i_hyp_edge_flux.qx, i_hyp_edge_flux.qy, i_right_normal_x, i_right_normal_y);
		CFluxProjections::projectToNormalSpaceAndInvert(i_right_edge_flux.qx, i_right_edge_flux.qy, i_right_normal_x, i_right_normal_y);
		CFluxProjections::projectToNormalSpaceAndInvert(i_left_edge_flux.qx, i_left_edge_flux.qy, i_right_normal_x, i_right_normal_y);

		// rotate element data
		CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->hyp_edge.qx, io_elementData->hyp_edge.qy, i_right_normal_x, i_right_normal_y);
		CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->left_edge.qx, io_elementData->left_edge.qy, i_right_normal_x, i_right_normal_y);
		CFluxProjections::projectToNormalSpaceAndInvert(io_elementData->right_edge.qx, io_elementData->right_edge.qy, i_right_normal_x, i_right_normal_y);

//		T six_div_h2 = (T)6.0/(cat_length*cat_length);
//		T timestep_six_div_h2 = delta_timestep*six_div_h2;
		T timestep_six_div_h2 = (T)(6.0*timestep_size)/(cat_length*cat_length);
		T two_div_h = (T)2.0/cat_length;
		T h_div_3 = cat_length/(T)3.0;

// 4 ops
// sum: 224
		T inv_hyp_edge_h = (T)1.0/io_elementData->hyp_edge.h;
		T inv_right_edge_h = (T)1.0/io_elementData->right_edge.h;
		T inv_left_edge_h = (T)1.0/io_elementData->left_edge.h;

//		std::cout <<timestep_six_div_h2 << " " << two_div_h << " " << h_div_3 << " " << inv_hyp_edge_h << " " << inv_right_edge_h << " " << inv_left_edge_h << std::endl;

// 3 ops
// sum: 227

		T elementData_sum_edge_qx = io_elementData->left_edge.qx + io_elementData->hyp_edge.qx + io_elementData->right_edge.qx;
		T elementData_sum_edge_qy = io_elementData->left_edge.qy + io_elementData->hyp_edge.qy + io_elementData->right_edge.qy;
// 4 ops
// sum: 231

		T triangle_area = cat_length*cat_length*(T)0.5;
// 2 ops
// sum: 233

		T elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = io_elementData->hyp_edge.qx*io_elementData->hyp_edge.qx*inv_hyp_edge_h;
		T elementData_right_edge_qx2_MUL_inv_right_edge_h = io_elementData->right_edge.qx*io_elementData->right_edge.qx*inv_right_edge_h;
		T elementData_left_edge_qx2_MUL_inv_left_edge_h = io_elementData->left_edge.qx*io_elementData->left_edge.qx*inv_left_edge_h;
// 6 ops
// sum: 239

		T sum_elementData_edges_qx2_MUL_inv_edges_h = elementData_left_edge_qx2_MUL_inv_left_edge_h + elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h + elementData_right_edge_qx2_MUL_inv_right_edge_h;
// 2 ops
// sum: 241

		T elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = io_elementData->hyp_edge.qy*io_elementData->hyp_edge.qy*inv_hyp_edge_h;
		T elementData_right_edge_qy2_MUL_inv_right_edge_h = io_elementData->right_edge.qy*io_elementData->right_edge.qy*inv_right_edge_h;
		T elementData_left_edge_qy2_MUL_inv_left_edge_h = io_elementData->left_edge.qy*io_elementData->left_edge.qy*inv_left_edge_h;
// 6 ops
// sum: 247

		T sum_elementData_edges_qy2_MUL_inv_edges_h = elementData_left_edge_qy2_MUL_inv_left_edge_h + elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h + elementData_right_edge_qy2_MUL_inv_right_edge_h;
// 2 ops
// sum: 249

		T elementData_hyp_edge_h2 = io_elementData->hyp_edge.h*io_elementData->hyp_edge.h;
		T elementData_right_edge_h2 = io_elementData->right_edge.h*io_elementData->right_edge.h;
		T elementData_left_edge_h2 = io_elementData->left_edge.h*io_elementData->left_edge.h;
// 3 ops
// sum: 252

		T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;
// 2 ops
// sum: 254


		T elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = io_elementData->hyp_edge.qx*io_elementData->hyp_edge.qy*inv_hyp_edge_h;
		T elementData_right_edge_qx_qy_MUL_inv_right_edge_h = io_elementData->right_edge.qx*io_elementData->right_edge.qy*inv_right_edge_h;
		T elementData_left_edge_qx_qy_MUL_inv_left_edge_h = io_elementData->left_edge.qx*io_elementData->left_edge.qy*inv_left_edge_h;
// 6 ops
// sum: 260

		T sum_elementData_edges_qx_qy_MUL_inv_edges_h = elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h + elementData_right_edge_qx_qy_MUL_inv_right_edge_h + elementData_left_edge_qx_qy_MUL_inv_left_edge_h;
// 2 ops
// sum: 262

		T triangle_area_MUL_two_div_h_MUL_1_3 = (T)(1.0/3.0)*triangle_area*two_div_h;
// 2 ops
// sum: 264

		T gravity_potential = (T)0.5*gravitational_constant*sum_elementData_edges_h2;
// 2 ops
// sum: 266



		/*
		 * H
		 */
		T hyp_edge_h = timestep_six_div_h2*(
				h_div_3*(elementData_sum_edge_qx + elementData_sum_edge_qy)
				-hyp_length*i_hyp_edge_flux.h
				);

		T right_edge_h = timestep_six_div_h2*(
				-h_div_3*elementData_sum_edge_qx
				-cat_length*i_right_edge_flux.h
				);

		T left_edge_h = timestep_six_div_h2*(
				-h_div_3*elementData_sum_edge_qy
				-cat_length*i_left_edge_flux.h
				);
// 13 ops
// sum: 279

		/*
		 * QX
		 */
		T hyp_edge_qx =
				timestep_six_div_h2*
				(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*
						(
							sum_elementData_edges_qx2_MUL_inv_edges_h +
							gravity_potential +
							sum_elementData_edges_qx_qy_MUL_inv_edges_h
						)
						- hyp_length*i_hyp_edge_flux.qx
				);
// 6 ops
// sum: 285

		T right_edge_qx =
				timestep_six_div_h2*(
					(T)triangle_area_MUL_two_div_h_MUL_1_3*(
							- gravity_potential
							- sum_elementData_edges_qx2_MUL_inv_edges_h
						)
						- cat_length*i_right_edge_flux.qx
				);
// 6 ops
// sum: 291

		T left_edge_qx =
				+ timestep_six_div_h2*(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*(
							-sum_elementData_edges_qx_qy_MUL_inv_edges_h
						)
						- cat_length*i_left_edge_flux.qx
				);
// 5 ops
// sum: 296


		/*
		 * QY
		 */
		T hyp_edge_qy =
				timestep_six_div_h2*(
						triangle_area_MUL_two_div_h_MUL_1_3*(
							sum_elementData_edges_qx_qy_MUL_inv_edges_h +
							sum_elementData_edges_qy2_MUL_inv_edges_h +
							gravity_potential
						)
						- hyp_length*i_hyp_edge_flux.qy
				);
// 6 ops
// sum: 302

		T right_edge_qy =
				timestep_six_div_h2*(
						- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_qx_qy_MUL_inv_edges_h
						- cat_length*i_right_edge_flux.qy
				);
// 5 ops
// sum: 307


		T left_edge_qy =
				timestep_six_div_h2*(
						-triangle_area_MUL_two_div_h_MUL_1_3*(
								sum_elementData_edges_qy2_MUL_inv_edges_h +
								gravity_potential
						)
						- cat_length*i_left_edge_flux.qy
				);

// 6 ops
// sum: 313
		io_elementData->hyp_edge.h += hyp_edge_h;
		io_elementData->hyp_edge.qx += hyp_edge_qx;
		io_elementData->hyp_edge.qy += hyp_edge_qy;
		CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->hyp_edge.qx, io_elementData->hyp_edge.qy, i_right_normal_x, i_right_normal_y);

		io_elementData->right_edge.h += right_edge_h;
		io_elementData->right_edge.qx += right_edge_qx;
		io_elementData->right_edge.qy += right_edge_qy;
		CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->right_edge.qx, io_elementData->right_edge.qy, i_right_normal_x, i_right_normal_y);

		io_elementData->left_edge.h += left_edge_h;
		io_elementData->left_edge.qx += left_edge_qx;
		io_elementData->left_edge.qy += left_edge_qy;
		CFluxProjections::backprojectFromNormalSpaceAndInvert(io_elementData->left_edge.qx, io_elementData->left_edge.qy, i_right_normal_x, i_right_normal_y);
	}



	/**
	 * \brief CALLED by TRAVERSATOR: All outer edge data is available and this method
	 * is executed giving, among others, the outer edge data as parameters.
	 */
	inline void elementAction(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,

			int i_depth,								///< depth of recursion

			CTsunamiElementData *io_elementData,		///< pointer to element data

			CTsunamiEdgeData *i_hyp_edgeData_outer,		///< DOF on hyp edge from adjacent triangle
			CTsunamiEdgeData *i_right_edgeData_outer,	///< DOF on right edge from adjacent triangle
			CTsunamiEdgeData *i_left_edgeData_outer		///< DOF on left edge from adjacent triangle
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_hyp_edgeData_outer->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edgeData_outer->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edgeData_outer->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_elementData->validation.testEdgeMidpoints(
				i_hyp_edgeData_outer->validation,
				i_right_edgeData_outer->validation,
				i_left_edgeData_outer->validation
			);
#endif

		/*
		 * LOAD local flux DOFs
		 */
		T hyp_max_wave_speed, left_max_wave_speed, right_max_wave_speed;

		// HYPOTENUSE EDGE
		CTsunamiEdgeData edgeCommData_Hyp;
		computeEdgeCommData_Hyp(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &edgeCommData_Hyp
			);


		// RIGHT EDGE
		CTsunamiEdgeData right_edgeData_inner;
		computeEdgeCommData_Right(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &right_edgeData_inner
			);


		// LEFT EDGE
		CTsunamiEdgeData left_edgeData_inner;
		computeEdgeCommData_Left(
				i_hyp_normal_x, i_hyp_normal_y,	i_right_normal_x, i_right_normal_y,	i_left_normal_x, i_left_normal_y,
				i_depth, io_elementData, &left_edgeData_inner
			);

		/*
		 * compute fluxes
		 */
		CTsunamiEdgeData hyp_edge_flux_inner, left_edge_flux_inner, right_edge_flux_inner;
		CTsunamiEdgeData hyp_edge_flux_outer, left_edge_flux_outer, right_edge_flux_outer;


		fluxSolver.computeNetUpdates(
				edgeCommData_Hyp,		*i_hyp_edgeData_outer,
				hyp_edge_flux_inner,	hyp_edge_flux_outer,
				hyp_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(hyp_edge_flux_inner.qx, hyp_edge_flux_inner.qy, i_hyp_normal_x, i_hyp_normal_y);

		fluxSolver.computeNetUpdates(
				right_edgeData_inner,	*i_right_edgeData_outer,
				right_edge_flux_inner,	right_edge_flux_outer,
				right_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(right_edge_flux_inner.qx, right_edge_flux_inner.qy, i_right_normal_x, i_right_normal_y);

		fluxSolver.computeNetUpdates(
				left_edgeData_inner,	*i_left_edgeData_outer,
				left_edge_flux_inner,	left_edge_flux_outer,
				left_max_wave_speed,
				gravitational_constant
			);
		CFluxProjections::backprojectFromNormalSpace(left_edge_flux_inner.qx, left_edge_flux_inner.qy, i_left_normal_x, i_left_normal_y);

		/*
		 * update element data using computed fluxes
		 */
		computeElementDataUpdate_WithFluxes(
				i_hyp_normal_x, i_hyp_normal_y,
				i_right_normal_x, i_right_normal_y,
				i_left_normal_x, i_left_normal_y,
				i_depth,

				hyp_edge_flux_inner,
				right_edge_flux_inner,
				left_edge_flux_inner,

				io_elementData
			);

		/*
		 * update CFL condition
		 */
		updateCFLCondition3(hyp_max_wave_speed, right_max_wave_speed, left_max_wave_speed, i_depth);
	}
};

}
}



#endif /* CEDGECOMM_TESTNORMAL_HPP_ */
