/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXLAXFRIEDRICH_HPP_
#define CFLUXLAXFRIEDRICH_HPP_


template <typename T>
class CFluxSolver_LaxFriedrichConstantFriction
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	void computeNetUpdates(
			CTsunamiEdgeData &i_edgeData_left,		///< edge data on left (inner) edge
			CTsunamiEdgeData &i_edgeData_right,		///< edge data on right (outer) edge
			CTsunamiEdgeData &o_edgeFlux_left,		///< output for inner flux
			CTsunamiEdgeData &o_edgeFlux_right,		///< output for outer flux
			T &o_max_squared_wave_speed,					///< maximum wave speed
			T i_gravitational_constant				///< gravitational constant
	)
	{
		T left_vx = i_edgeData_left.qx/i_edgeData_left.h;
		T left_vy = i_edgeData_left.qy/i_edgeData_left.h;

		T right_vx = i_edgeData_right.qx/i_edgeData_right.h;
		T right_vy = i_edgeData_right.qy/i_edgeData_right.h;

		T lambda = SIMULATION_TSUNAMI_FLUX_SOLVER_LAX_FRIEDRICH_CONST_FRICTION_COEFF;

		o_edgeFlux_left.h =
				(T)0.5*
				(
					i_edgeData_left.qx + i_edgeData_right.qx
					+ (T)lambda*(i_edgeData_left.h - i_edgeData_right.h)
				);

		T inner_h2_plus_outer_h2 = i_edgeData_left.h*i_edgeData_left.h + i_edgeData_right.h*i_edgeData_right.h;

		o_edgeFlux_left.qx =
				(T)0.5*
				(
					left_vx*i_edgeData_left.qx +
					right_vx*i_edgeData_right.qx +
					(TTsunamiDataScalar)0.5*i_gravitational_constant*inner_h2_plus_outer_h2
					+
					(T)lambda*(
							i_edgeData_left.qx - i_edgeData_right.qx
					)
				);

		o_edgeFlux_left.qy =
				(T)0.5*
				(
					left_vx*i_edgeData_left.qx +
					right_vx*i_edgeData_right.qx +
					(
							left_vy*i_edgeData_left.qx +
							right_vy*i_edgeData_right.qx
					) +
					(T)lambda*(
							i_edgeData_left.qy - i_edgeData_right.qy
					)
				);

		o_edgeFlux_right.h = -o_edgeFlux_left.h;
		o_edgeFlux_right.qx = -o_edgeFlux_left.qx;
		o_edgeFlux_right.qy = -o_edgeFlux_left.qy;

		/**
		 * CFL condition
		 */
		o_max_wave_speed = lambda;
//		o_max_wave_speed = CMath::sqrt(o_edgeFlux_left.qx*o_edgeFlux_left.qx + o_edgeFlux_left.qy*o_edgeFlux_left.qy);
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
