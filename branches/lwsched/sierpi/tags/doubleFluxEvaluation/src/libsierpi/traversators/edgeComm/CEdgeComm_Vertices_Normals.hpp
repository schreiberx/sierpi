/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef TRAVERSATORS_CEDGE_COMM_VERTICES_NORMALS_HPP_
#define TRAVERSATORS_CEDGE_COMM_VERTICES_NORMALS_HPP_


#include "libsierpi/stacks/CFBStacks.hpp"

namespace sierpi
{
namespace travs
{


/**
 * adaptive refinement
 */
template <typename CKernelClass, typename TElementData, typename TEdgeData>
class CEdgeComm_Vertices_Normals
{
public:
	/**
	 * implementation of kernel class
	 */
	CKernelClass kernelClass;

	/**
	 * a few typedefs which could be utilized by the traversator to get more informations
	 */
	typedef typename CKernelClass::TVertexScalar TVertexScalar;
	typedef CEdgeComm_Vertices_Normals<CKernelClass, TElementData, TEdgeData> TThisClass;


	/**
	 * stack and lifo handlers used in this traversator
	 */
	CStack<char> *structure_stack_out;
	CStackReaderTopDown<char> structure_lifo_in;

	/**
	 * lifo and fifo iterator for element data
	 */
	CStackReaderTopDown<TElementData> element_data_lifo;
	CStackReaderBottomUp<TElementData> element_data_fifo;

	/**
	 * pointer to the corresponding communication stacks
	 */
	CStack<TEdgeData> *edge_data_comm_left_edge_stack;
	CStack<TEdgeData> *edge_data_comm_right_edge_stack;

	/**
	 * pointer to stack used for edge communication in forward order
	 */
	CStack<TEdgeData> *edge_comm_buffer;

#define SIERPI_PARALLEL_CODE	1
#include "auto/CTraversator_EdgeComm_Vertices_Normals_autogenerated.hpp"
#undef SIERPI_PARALLEL_CODE
#include "../CTraversatorClassInc_SetupTwoPass.hpp"


	void actionFirstPass(
			CFBStacks<char> &p_structure_stacks,
			CFBStacks<TElementData> &p_element_data_stacks,
			CStack<TEdgeData> &p_edge_data_comm_left_edge_stack,
			CStack<TEdgeData> &p_edge_data_comm_right_edge_stack,
			CStack<TEdgeData> &p_edge_comm_buffer
	)
	{
		assert(p_element_data_stacks.getNumberOfElementsOnStack() != 0);

		assert(p_structure_stacks.direction == p_element_data_stacks.direction);
		assert(p_structure_stacks.direction == CFBStacks<char>::FORWARD);

		structure_stack_out = &p_structure_stacks.backward;
		assert(structure_stack_out->isEmpty());

		structure_lifo_in.setup(p_structure_stacks.forward);

		edge_data_comm_left_edge_stack = &p_edge_data_comm_left_edge_stack;
		edge_data_comm_right_edge_stack = &p_edge_data_comm_right_edge_stack;

		edge_comm_buffer = &p_edge_comm_buffer;
		element_data_lifo.setup(p_element_data_stacks.forward);

		kernelClass.traversal_pre_hook();

		(this->*sfcRecursiveMethod_Forward)(
					triangleFactory.vertices[0][0], triangleFactory.vertices[0][1],
					triangleFactory.vertices[1][0], triangleFactory.vertices[1][1],
					triangleFactory.vertices[2][0], triangleFactory.vertices[2][1]
		);

		assert(structure_lifo_in.isEmpty());
		assert(element_data_lifo.isEmpty());
	}


	void actionSecondPass(
			CFBStacks<char> &p_structure_stacks,
			CFBStacks<TElementData> &p_element_data_stacks,
			CStack<TEdgeData> &p_edge_data_comm_left_edge_stack,
			CStack<TEdgeData> &p_edge_data_comm_right_edge_stack,
			CStack<TEdgeData> &p_edge_comm_buffer
	)
	{
		assert(p_element_data_stacks.getNumberOfElementsOnStack() != 0);

		edge_data_comm_left_edge_stack = &p_edge_data_comm_right_edge_stack;
		edge_data_comm_right_edge_stack = &p_edge_data_comm_left_edge_stack;

		edge_comm_buffer = &p_edge_comm_buffer;

		structure_lifo_in.setup(&p_structure_stacks.backward);
		element_data_fifo.setup(p_element_data_stacks.forward);

		(this->*sfcRecursiveMethod_Backward)(
					triangleFactory.vertices[0][0], triangleFactory.vertices[0][1],
					triangleFactory.vertices[1][0], triangleFactory.vertices[1][1],
					triangleFactory.vertices[2][0], triangleFactory.vertices[2][1]
		);
		
		kernelClass.traversal_post_hook();

		assert(edge_comm_buffer->isEmpty());
		assert(edge_data_comm_left_edge_stack->isEmpty());
		assert(edge_data_comm_right_edge_stack->isEmpty());
		assert(structure_lifo_in.isEmpty());
		assert(element_data_fifo.isEmpty());

		structure_stack_out->clear();
	}
};

}
}

#endif
