/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode_serial.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CGENERICTREENODE_SERIAL_HPP_
#define CGENERICTREENODE_SERIAL_HPP_



/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE)
 */
template <
	typename CLambdaFun
>
class CTraversalTask_PartitionTreeNode
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_PartitionTreeNode(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			assert(this_node->cPartition_TreeNode != nullptr);
			lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		CTraversalTask_PartitionTreeNode<CLambdaFun> first(this_node->first_child_node, lambda);
		CTraversalTask_PartitionTreeNode<CLambdaFun> second(this_node->second_child_node, lambda);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		return 0;
	}
};


template <bool parallelProcessing, typename CLambdaFun>
void traverse_PartitionTreeNode(
		CLambdaFun p_lambda
)
{
	CTraversalTask_PartitionTreeNode<CLambdaFun>(this, p_lambda).execute();
}


template <typename CLambdaFun>
void traverse_PartitionTreeNode_Parallel(
		CLambdaFun p_lambda
)
{
	CTraversalTask_PartitionTreeNode<CLambdaFun>(this, p_lambda).execute();
}


template <typename CLambdaFun>
void traverse_PartitionTreeNode_Serial(
		CLambdaFun p_lambda
)
{
	CTraversalTask_PartitionTreeNode<CLambdaFun>(this, p_lambda).execute();
}



/****************************************************************************
 * PARTITION TREE NODE (NO REDUCE) MID AND LEAF NODES
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_PartitionTreeNode_MidAndLeafNodes
{
	CGenericTreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun1 p_lambda_leaves,
			CLambdaFun2 p_lambda_midnodes
	)	:
		this_node(p_this_node),
		lambda_leaves(p_lambda_leaves),
		lambda_midnodes(p_lambda_midnodes)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node->cPartition_TreeNode);
			return 0;
		}

		CTraversalTask_PartitionTreeNode_MidAndLeafNodes<CLambdaFun1, CLambdaFun2> first(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		CTraversalTask_PartitionTreeNode_MidAndLeafNodes<CLambdaFun1, CLambdaFun2> second(this_node->second_child_node, lambda_leaves, lambda_midnodes);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		if (this_node->cPartition_TreeNode)
			lambda_midnodes(this_node->cPartition_TreeNode);

		return 0;
	}
};


template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_PartitionTreeNode_MidAndLeafNodes_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_PartitionTreeNode_MidAndLeafNodes<CLambdaFun1, CLambdaFun2>(this, p_lambda_leaves, p_lambda_midnodes).execute();
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES
 */


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return;
		}

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes);
		}

		lambda_midnodes(this_node);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}



/*
 * parallel/serial
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes);
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;

		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Parallel(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes,
		int p_genericTreeDepth
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}


/*
 * serial
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial
{
public:
	static void traverse(
			CGenericTreeNode_ *this_node,
			CLambdaFun1 lambda_leaves,
			CLambdaFun2 lambda_midnodes,
			int p_genericTreeDepth
	)
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, p_genericTreeDepth);
			return;
		}

		p_genericTreeDepth++;
		if (this_node->first_child_node)
		{
			traverse(this_node->first_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		if (this_node->second_child_node)
		{
			traverse(this_node->second_child_node, lambda_leaves, lambda_midnodes, p_genericTreeDepth);
		}

		lambda_midnodes(this_node, p_genericTreeDepth-1);
	}
};



template <typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth_Serial(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}



/*
 * parallel/serial
 */
template <bool parallelProcessing, typename CLambdaFun1, typename CLambdaFun2>
void traverse_GenericTreeNode_MidAndLeafNodes_Depth(
		CLambdaFun1 p_lambda_leaves,
		CLambdaFun2 p_lambda_midnodes
)
{
	CTraversalTask_GenericTreeNode_MidAndLeafNodes_Depth_Serial<CLambdaFun1, CLambdaFun2>::traverse(this, p_lambda_leaves, p_lambda_midnodes, 0);
}



/****************************************************************************
 * PARTITION TREE NODE (WITH REDUCE)
 ****************************************************************************/
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_PartitionTreeNode_Reduce
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;
	TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b);

public:
	TReduceValue reduceValue;

	CTraversalTask_PartitionTreeNode_Reduce(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda,
			TReduceValue (*p_reduceOperator)(const TReduceValue &a, const TReduceValue &b)
	)	:
		this_node(p_this_node),
		lambda(p_lambda),
		reduceOperator(p_reduceOperator),
		reduceValue(0)
	{
	}

	/**
	 * TASK
	 */
	void* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			reduceValue = lambda(this_node->cPartition_TreeNode);
			return 0;
		}

		/**
		 * PARALLEL TRAVERSALS
		 */
		CTraversalTask_PartitionTreeNode_Reduce<CLambdaFun, TReduceValue> first(this_node->first_child_node, lambda, reduceOperator);
		CTraversalTask_PartitionTreeNode_Reduce<CLambdaFun, TReduceValue> second(this_node->second_child_node, lambda, reduceOperator);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		/**
		 * REDUCTION
		 */
		if (this_node->first_child_node)
		{
			if (this_node->second_child_node)
			{
				reduceValue = reduceOperator(first.reduceValue, second.reduceValue);
			}
			else
			{
				reduceValue = first.reduceValue;
			}
		}
		else
		{
			if (this_node->second_child_node)
			{
				reduceValue = second.reduceValue;
			}
		}

		return 0;
	}
};


template <bool parallelProcessing, typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce(
		CLambdaFun lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	// start root task
	CTraversalTask_PartitionTreeNode_Reduce<CLambdaFun, TReduceValue> root(this, lambda, reduceOperator);
	root.execute();

	return root.reduceValue;
}

template <typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce_Serial(
		CLambdaFun lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	return traverse_PartitionTreeNode_Reduce<false, CLambdaFun, TReduceValue>(lambda, reduceOperator);
}

template <typename CLambdaFun, typename TReduceValue>
TReduceValue traverse_PartitionTreeNode_Reduce_Parallel(
		CLambdaFun lambda,
		TReduceValue (*reduceOperator)(const TReduceValue &a, const TReduceValue &b)
)
{
	return traverse_PartitionTreeNode_Reduce<true, CLambdaFun, TReduceValue>(lambda, reduceOperator);
}

/****************************************************************************
 * SIMULATION SUB-PARTITION HANDLER (NO REDUCE)
 ****************************************************************************/
template <
	typename CLambdaFun
>
class CTraversalTask_SimulationPartitionHandler
{
	CGenericTreeNode_ *this_node;
	CLambdaFun lambda;

public:
	CTraversalTask_SimulationPartitionHandler(
			CGenericTreeNode_ *p_this_node,
			CLambdaFun p_lambda
	)	:
		this_node(p_this_node),
		lambda(p_lambda)
	{
	}

	void* execute()
	{
		if (this_node->isLeaf())
		{
			lambda(this_node->cPartition_TreeNode->cSimulation_SubPartitionHandler);
			return 0;
		}

		CTraversalTask_SimulationPartitionHandler<CLambdaFun> first(this_node->first_child_node, lambda);
		CTraversalTask_SimulationPartitionHandler<CLambdaFun> second(this_node->second_child_node, lambda);

		if (this_node->first_child_node)
			first.execute();

		if (this_node->second_child_node)
			second.execute();

		return 0;
	}

};



template <bool parallelProcessing, class CLambdaFun>
void traverse_SimulationPartitionHandler(
		CLambdaFun lambda
)
{
	CTraversalTask_SimulationPartitionHandler<CLambdaFun>(this, lambda).execute();
}


template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Serial(
		CLambdaFun lambda
)
{
	CTraversalTask_SimulationPartitionHandler<CLambdaFun>(this, lambda).execute();
}

template <class CLambdaFun>
void traverse_SimulationPartitionHandler_Parallel(
		CLambdaFun lambda
)
{
	CTraversalTask_SimulationPartitionHandler<CLambdaFun>(this, lambda).execute();
}
#endif
