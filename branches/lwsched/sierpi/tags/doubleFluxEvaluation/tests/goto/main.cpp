
#include "lib/CStopwatch.hpp"
#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"
#include "CTraversator_Vertices_ElementData.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "simulations/tsunami_common/CTsunamiTypes.hpp"
#include <stdint.h>



/*******
 * TEST KERNEL
 */


class CTestKernel
{
public:
	typedef CTsunamiElementData TElementData;

	typedef sierpi::travs::CTraversator_Vertices_ElementData<CTestKernel> TRAV;

	typedef float TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

public:

	inline CTestKernel()
	{
	}


	inline ~CTestKernel()
	{
	}


	void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TElementData *element) __attribute__ ((noinline))
	{
		element->hyp_edge.h = (element->hyp_edge.h + (vx1 + vy1 + vx2 + vy2 + vx3 + vy3)*((TVertexScalar)1.0/(TVertexScalar)6.0))*(TVertexScalar)0.5;
	}

	void elementAction2(
			TVertexScalar v[3][2],
			TElementData *element) __attribute__ ((noinline))
	{
		element->hyp_edge.h = (element->hyp_edge.h + (v[0][0] + v[0][1] + v[1][0] + v[1][1] + v[2][0] + v[2][1])*((TVertexScalar)1.0/(TVertexScalar)6.0))*(TVertexScalar)0.5;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}
};

CTestKernel::TRAV testTrav;

void run()
{
	testTrav.action();
}

int main()
{
	sierpi::travs::CSetup_Structure_ElementData<CTsunamiElementData> cSetup_Structure_ElementData;

	CTriangle_Factory template_rootTriangle;
	template_rootTriangle.vertices[0][0] = 1.0;
	template_rootTriangle.vertices[0][1] = -1.0;
	template_rootTriangle.vertices[1][0] = -1.0;
	template_rootTriangle.vertices[1][1] = 1.0;
	template_rootTriangle.vertices[2][0] = -1.0;
	template_rootTriangle.vertices[2][1] = -1.0;

	template_rootTriangle.evenOdd = CTriangle_Enums::EVEN;
	template_rootTriangle.hypNormal = CTriangle_Enums::NORMAL_NE;
	template_rootTriangle.recursionDepth = 0;
	template_rootTriangle.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BORDER;
	template_rootTriangle.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BORDER;
	template_rootTriangle.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BORDER;

	template_rootTriangle.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
	template_rootTriangle.triangleType = CTriangle_Enums::TRIANGLE_TYPE_K;

	template_rootTriangle.partitionTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

	testTrav.setup_RootPartition(template_rootTriangle);



	CTsunamiElementData element_data_setup;
	element_data_setup.hyp_edge.h = 50;
	element_data_setup.hyp_edge.qx = 0;
	element_data_setup.hyp_edge.qy = 0;

	element_data_setup.left_edge.h = 50;
	element_data_setup.left_edge.qx = 0;
	element_data_setup.left_edge.qy = 0;

	element_data_setup.right_edge.h = 50;
	element_data_setup.right_edge.qx = 0;
	element_data_setup.right_edge.qy = 0;

	element_data_setup.refine = false;
	element_data_setup.coarsen = false;

	int initial_recursion_depth = 27;
	int max_relative_recursion_depth = 0;

	CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *cStacks;
	cStacks = new CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData>(
				initial_recursion_depth+max_relative_recursion_depth,
				CSimulationStacks_Enums::ELEMENT_STACKS			|
				CSimulationStacks_Enums::ADAPTIVE_STACKS		|
				CSimulationStacks_Enums::EDGE_COMM_STACKS
			);

	/**
	 * setup trav stacks
	 */
	testTrav.setup_RootPartition(template_rootTriangle);
	testTrav.setup_Stacks(&cStacks->structure_stacks, &cStacks->element_data_stacks);
	cSetup_Structure_ElementData.setup_Stacks(cStacks->structure_stacks, cStacks->element_data_stacks);

//	cSetup_Structure_ElementData.setup_RootPartition(template_rootTriangle);
	unsigned long long number_of_triangles = cSetup_Structure_ElementData.setup(initial_recursion_depth, &element_data_setup);

	std::cout << "number of triangles: " << number_of_triangles << std::endl;

	CStopwatch cStopwatch;

	testTrav.action();

	cStopwatch.start();
	run();
	cStopwatch.stop();

	std::cout << "TIME: " << cStopwatch.time << std::endl;
}
