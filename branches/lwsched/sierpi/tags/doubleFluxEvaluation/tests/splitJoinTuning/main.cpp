#include <iostream>
#include "../../src/libsierpi/parallelization/CSplitJoinTuning.hpp"


class CTuningTable
{
public:
	int tuning_table_size;

	unsigned long long tuning_table[10][2];

	CTuningTable()
	{
		tuning_table[0][0] = 2048;		tuning_table[0][1] = 512;
		tuning_table[1][0] = 4096;		tuning_table[1][1] = 1024;
		tuning_table[2][0] = 8192;		tuning_table[2][1] = 1024;
		tuning_table[3][0] = 16384;		tuning_table[3][1] = 1024;
		tuning_table[4][0] = 32768;		tuning_table[4][1] = 2048;

		tuning_table[5][0] = 65536;		tuning_table[5][1] = 2048;
		tuning_table[6][0] = 131072;	tuning_table[6][1] = 2048;
		tuning_table[7][0] = 262144;	tuning_table[7][1] = 4096;
		tuning_table[8][0] = 524288;	tuning_table[8][1] = 4096;
		tuning_table[9][0] = 1048576;	tuning_table[9][1] = 4096;

		tuning_table[10][0] = 2097152;	tuning_table[10][1] = 8192;
		tuning_table[11][0] = 4194304;	tuning_table[11][1] = 8192;
		tuning_table[12][0] = 8388608;	tuning_table[12][1] = 16384;
		tuning_table[13][0] = 16777216;	tuning_table[13][1] = 16384;
		tuning_table[14][0] = 33554432;	tuning_table[14][1] = 32768;

		tuning_table_size = sizeof(tuning_table)/(sizeof(unsigned long long)*2);
	}
};


int main(int argc, char *argv[])
{
	CSplitJoinTuning<CTuningTable> cSplitJoinTuning;

	unsigned long long s, j;

	cSplitJoinTuning.updateSplitJoin(s, j, 97);
	std::cout << s << " " << j << std::endl;
	
	
}

