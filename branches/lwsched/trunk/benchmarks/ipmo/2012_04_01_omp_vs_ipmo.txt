Ausfuehrung von 5 oder 10 Programmen.


OpenMP sequentielle Hintereinanderausfuehrung: 983 Sekunden
OpenMP parallele Hintereinanderausfuehrung (ohne pinning): 1289 Sekunden
iPMO sequentielle Hintereinanderausfuehrung: 1178 Sekunden
iPMO parallel: 549 Sekunden

Wie immer gilt, dass die verwendeten Simulationen geringe Problemgroesse hatten und damit allgemein schlecht skalieren.
