#! /bin/bash

. ../inc_environment_vars.sh

#
# usage information
#
# ./run [initial_depth] [timesteps] [set to arbitrary value to output .vtk files]
#



if [ -z "$1" ]; then
	DEPTH=12
else
	DEPTH=$1
fi

if [ -z "$2" ]; then
	TIMESTEPS=1200
else
	TIMESTEPS=$2
fi

if [ ! -z "$3" ]; then
	VTKFILES="true"
else
	VTKFILES=""
fi

OPTS="-d $DEPTH -a 0 -t $TIMESTEPS $ADD_OPT"


# triangles per timestep
TRIANGLES=`echo "2^$DEPTH" | bc`

# sizeof(float)=4, 9+1 DOFs, read+write
MB_PER_TIMESTEP=`echo "scale=4; $TRIANGLES*4*10*2/1024/1024" | bc`

echo "DEPTH: $DEPTH"
echo "TRIANGLES: $TRIANGLES"
echo "MB_PER_TIMESTEP: $MB_PER_TIMESTEP MB"
echo "TIMESTEPS: $TIMESTEPS"

#########################################################################
echo "GCC SERIAL RELEASE"
#########################################################################

if [ ! -z "$VTKFILES" ]; then
	ADD_OPTS=" -g frame_gnu_serial_%08i.vtk -f 100"
else
	ADD_OPTS=""
fi
../../build/sierpi_gnu_tsunami_serial_release $OPTS $ADD_OPTS | grep " MTPS "

#########################################################################
echo "GCC SERIAL REGULAR RELEASE"
#########################################################################

if [ ! -z "$VTKFILES" ]; then
	ADD_OPTS=" -g frame_gnu_serial_regular_%08i.vtk -f 100"
else
	ADD_OPTS=""
fi
../../build/sierpi_gnu_tsunami_serial_regular_release $OPTS $ADD_OPTS | grep " MTPS "



EXEC=../../build/sierpi_intel_tsunami_serial_release
if [ -e "$EXEC" ]; then
#########################################################################
	echo "ICC SERIAL RELEASE"
#########################################################################

if [ ! -z "$VTKFILES" ]; then
	ADD_OPTS=" -g frame_intel_serial_%08i.vtk -f 100"
else
	ADD_OPTS=""
fi
	$EXEC $OPTS $ADD_OPTS | grep " MTPS "
fi


EXEC=../../build/sierpi_intel_tsunami_serial_regular_release
if [ -e "$EXEC" ]; then
#########################################################################
	echo "ICC SERIAL REGULAR RELEASE"
#########################################################################

if [ ! -z "$VTKFILES" ]; then
	ADD_OPTS=" -g frame_intel_serial_regular_%08i.vtk -f 100"
else
	ADD_OPTS=""
fi
	$EXEC $OPTS $ADD_OPTS | grep " MTPS "
fi
