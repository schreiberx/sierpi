/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CTSUNAMI_TYPES_VALIDATE_EDGE_DATA_HPP
#define CTSUNAMI_TYPES_VALIDATE_EDGE_DATA_HPP

#include <string.h>


/**
 * validation data for edge data
 */
class CValidation_EdgeData
{

public:
	TTsunamiVertexScalar midpoint_x;
	TTsunamiVertexScalar midpoint_y;

	TTsunamiVertexScalar normal_x;
	TTsunamiVertexScalar normal_y;

	CValidation_EdgeData()	:
		midpoint_x(-666),
		midpoint_y(-666),

		normal_x(-666),
		normal_y(-666)
	{
	}


	CValidation_EdgeData(
			const CValidation_EdgeData &d
	)
	{
		midpoint_x = d.midpoint_x;
		midpoint_y = d.midpoint_y;

		normal_x = d.normal_x;
		normal_y = d.normal_y;
		return;
	}


	CValidation_EdgeData& operator=(
			const CValidation_EdgeData &d
	)
	{
		if (d.midpoint_x == -666)
			return *this;

		midpoint_x = d.midpoint_x;
		midpoint_y = d.midpoint_y;

		normal_x = d.normal_x;
		normal_y = d.normal_y;

		return *this;
	}


	void setupNormal(
			TTsunamiVertexScalar i_normal_x,
			TTsunamiVertexScalar i_normal_y
	)
	{
		normal_x = i_normal_x;
		normal_y = i_normal_y;
	}


	void testNormal(
			TTsunamiVertexScalar i_normal_x,
			TTsunamiVertexScalar i_normal_y
	)
	{
		assert(i_normal_x != -666.0);
		assert(i_normal_y != -666.0);

#if !CONFIG_PERIODIC_BOUNDARIES
		if (CMath::abs(i_normal_x) == 0)
			i_normal_x = 0;

		if (CMath::abs(i_normal_y) == 0)
			i_normal_y = 0;

		if (i_normal_x != normal_x || i_normal_y != normal_y)
		{
			std::cout << "Normal mismatch!" << std::endl;
			std::cout << " +(" << normal_x << ", " << normal_y << ")" << std::endl;
			std::cout << " +(" << i_normal_x << ", " << i_normal_y << ")" << std::endl;
			assert(false);
		}
#endif
	}


	void testInverseNormal(
			TTsunamiVertexScalar i_normal_x,
			TTsunamiVertexScalar i_normal_y
	)
	{
		testNormal(-i_normal_x, -i_normal_y);
	}

	friend
	::std::ostream&
	operator<<(::std::ostream& os, const CValidation_EdgeData &d)
	{
		return os << "(mid: " << d.midpoint_x << ", " << d.midpoint_y << "| normal: " << d.normal_x << ", " << d.normal_y << ")";
	}
};


#endif
