/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 4, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMI_PARALLEL_CLUSTER_HANDLER_HPP_
#define CTSUNAMI_PARALLEL_CLUSTER_HANDLER_HPP_

#include "../tsunami_common/tsunami_config.h"

// typedefs for types related to basic tsunami simulation
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"


// specialized traversators (to speed up compilation due to parallel processing)
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

// structure setup traversator
#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"

#include "libsierpi/parallelization/CCluster_TreeNode.hpp"
#include "libsierpi/parallelization/CCluster_ExchangeEdgeCommData.hpp"
#include "libsierpi/parallelization/CCluster_ExchangeFluxCommData.hpp"

#include "libsierpi/domain_triangulation/CDomain_BaseTriangulation.hpp"

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#include "libsierpi/parallelization/CCluster_ExchangeVertexDataCommData.hpp"
#endif

#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
	#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/kernels/CSetup_TsunamiCellData_Validation.hpp"
#endif

#include "../tsunami_common/kernels/backends/CSimulationTsunami_OutputGridDataArrays.hpp"

#include "libsierpi/parallelization/CStackAccessorMethods.hpp"


/**
 * \brief Tsunami Simulation Cluster Handler
 *
 * This class implements all kinds of sub-cluster related user-defined data storages.
 */
class CSimulationTsunami_Cluster
{
public:
	typedef sierpi::CCluster_TreeNode<CSimulationTsunami_Cluster> CCluster_TreeNode_;
	typedef sierpi::CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> CEdgeComm_InformationAdjacentCluster_;

	/**
	 * "exports" for CCluster_TreeNode
	 */
	typedef CTsunamiSimulationTypes CSimulationTypes;
	typedef CTsunamiVisualizationTypes CVisualizationTypes;
	typedef CTsunamiSimulationStacksAndTypes CSimulationStacks;

	typedef CSimulationTypes::CCellData		CCellData;
	typedef CSimulationTypes::CEdgeData		CEdgeData;

	/**
	 * pointer to handler of this cluster node
	 */
	CCluster_TreeNode_ *cCluster_TreeNode;

	/**
	 * TIMESTEP: EDGE COMM
	 */
	/// edge communication and simulation without adaptivity
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/**
	 * TSUNAMI ADAPTIVITY
	 */
	/// adaptivity - refine/coarsen
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData cTsunami_Adaptive;


	/**
	 * WATER COLUMN SETUP ADAPTIVITY
	 */
	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;


	/**
	 * EDGE COMM FOR ADAPTIVE TRAVERSALS
	 */
	/// handler for adaptivity information exchange with adjacent clusters
	sierpi::CCluster_ExchangeEdgeCommData<
			CCluster_TreeNode_,
			char,
			sierpi::CStackAccessorMethodsAdaptivityEdgeData<CCluster_TreeNode_>
		> cCluster_ExchangeEdgeCommData_Adaptivity;


	/**
	 * INTER-PARTITION-EDGE COMMUNICATION
	 */

	/// handler for flux communication with adjacent clusters
	sierpi::CCluster_ExchangeFluxCommData<
			CCluster_TreeNode_,
			CTsunamiSimulationEdgeData,
			sierpi::CStackAccessorMethodsSimulationEdgeData<CCluster_TreeNode_, CTsunamiSimulationEdgeData>,
			sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth::CKernelClass
		> cCluster_ExchangeFluxCommData_Tsunami;

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	/**
	 * INTER-PARTITION-VERTEX-DATA COMMUNICATION
	 */
	/// handler for vertex communication with adjacent clusters
	sierpi::CCluster_ExchangeVertexDataCommData<
			CCluster_TreeNode_,
			CTsunamiSimulationStacksAndTypes::CVisualizationTypes::CNodeData,
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CTsunamiSimulationStacksAndTypes::CVisualizationTypes::CNodeData>,
			sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV::CKernelClass
		> cCluster_ExchangeVertexDataCommData_WaterSurface;

	/// handler for vertex communication with adjacent clusters
	sierpi::CCluster_ExchangeVertexDataCommData<
			CCluster_TreeNode_,
			CTsunamiSimulationStacksAndTypes::CVisualizationTypes::CNodeData,
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CTsunamiSimulationStacksAndTypes::CVisualizationTypes::CNodeData>,
			sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<3>::TRAV::CKernelClass
		> cCluster_ExchangeVertexDataCommData_Terrain;
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	// validate edge communication
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;

	/// handler for edge communication with adjacent clusters
	sierpi::CCluster_ExchangeEdgeCommData<CCluster_TreeNode_, CValEdgeData, sierpi::CStackAccessorMethodsValidationEdgeData<CCluster_TreeNode_> > cCluster_ExchangeEdgeCommData_Validation;
#endif


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	// this traversal is used to setup the element data vertex coordinates during the initial setup
	sierpi::kernels::CSetup_TsunamiCellData_Validation::TRAV cSetup_TsunamiCellData_Validation;
#endif

#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 2
	// only computed explicit euler updates are stored to this element data sets
	CStack<CTsunamiSimulationCellData> cStackCellDataRK2_f_t0;

	// second euler updates
	CStack<CTsunamiSimulationCellData> cStackCellDataRK2_yapprox_t1_AND_f_t1;
#endif

	/*
	 * fast accessor for triangle factory
	 */
	CTriangle_Factory &cTriangleFactory;

	/**
	 * stored cfl condition values after edge communication!
	 *
	 * This is used in the ongoing adaptive step to finally update the cfl to it's accurate number
	 */
	TTsunamiDataScalar local_timestep_size;

	/**
	 * constructor for setup
	 *
	 * this constructor is called whenever a split or merge operation is executed.
	 */
	CSimulationTsunami_Cluster(
			CCluster_TreeNode_ *i_cCluster_TreeNode,				///< pointer to cluster tree node of this cluster
			CSimulationTsunami_Cluster *i_cluster_parameterSetup	///< cluster handler to setup parameters
		)	:
		cCluster_TreeNode(i_cCluster_TreeNode),
		cCluster_ExchangeEdgeCommData_Adaptivity(i_cCluster_TreeNode),
		cCluster_ExchangeFluxCommData_Tsunami(i_cCluster_TreeNode, cTsunami_EdgeComm.cKernelClass),

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cCluster_ExchangeVertexDataCommData_WaterSurface(i_cCluster_TreeNode, nullptr),
		cCluster_ExchangeVertexDataCommData_Terrain(i_cCluster_TreeNode, nullptr),
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cCluster_ExchangeEdgeCommData_Validation(i_cCluster_TreeNode),
#endif
		cTriangleFactory(i_cCluster_TreeNode->cTriangleFactory)
	{
		setup(i_cluster_parameterSetup);
	}



	/**
	 * setup method to initialize traversal methods and parameters
	 */
	void setup(
			CSimulationTsunami_Cluster *i_cluster_parameterSetup	///< cluster handler to setup parameters
		)
	{
		if (i_cluster_parameterSetup == nullptr)
		{
			/*
			 * this has to be a domain root sub-cluster and its parameters
			 * are set-up at another place.
			 */
			cSetup_Column.setup_sfcMethods(cTriangleFactory);

			cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
			cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			cEdgeComm_ValidateComm.setup_sfcMethods(cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			cSetup_TsunamiCellData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

			local_timestep_size = 0;

			return;
		}


		/**
		 * SETUP traversals and parameters based on another node
		 */
		cSetup_Column.setup_Cluster(i_cluster_parameterSetup->cSetup_Column, cTriangleFactory);

		cTsunami_EdgeComm.setup_Cluster(i_cluster_parameterSetup->cTsunami_EdgeComm, cTriangleFactory);

		cTsunami_Adaptive.setup_Cluster(i_cluster_parameterSetup->cTsunami_Adaptive, cTriangleFactory);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cEdgeComm_ValidateComm.setup_Cluster(i_cluster_parameterSetup->cEdgeComm_ValidateComm, cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiCellData_Validation.setup_Cluster(i_cluster_parameterSetup->cSetup_TsunamiCellData_Validation, cTriangleFactory);
#endif

		local_timestep_size = i_cluster_parameterSetup->local_timestep_size;
	}

};



#endif
