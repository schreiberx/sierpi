/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 */
/*
 * config.h
 *
 * Precompiler settings for compilation
 *
 *  Created on: Sep 27, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


/**
 * Resize stacks when the element data exceeds the stack size due to
 * adaptivity.
 *
 * The CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING gives the approximated
 * number of bytes to additionally extend the stack to avoid frequent
 * malloc/free operators.
 *
 * The CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING gives the approximated
 * number of bytes which the new max element data stack counter has to be fall
 * below the maximum to allow a shrinking of the stack.
 */
#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
#error "CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS not defined"
#endif

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
	#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING
		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING	(128*8)
	#endif

	#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_ELEMENTS(SizeOfElement)	(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING / SizeOfElement)

	#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING
		// subtract a little offset (16) to avoid flickering
		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING	(128*8-16)
	#endif

	#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING_ELEMENTS(SizeOfElement)	((CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING / SizeOfElement) + 32)
#endif



/**
 * during adaptive traversals a conforming grid should be gained.
 *
 * for sub-clusters already being in a conforming state and without any requests
 * from adjacent edge communication to refine, the adaptive traversal of such a
 * sub-cluster can be skipped.
 */
#ifndef CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE
#error "CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE not defined"
#endif



/**
 * avoid last adaptive traversal if no adaptive refinements have to be executed
 */
#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED 0

#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED
#error "CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED not defined"
#endif



/**
 * default floating point precision
 */
#ifdef CONFIG_DEFAULT_FLOATING_POINT_PRECISION_SINGLE
	#define CONFIG_DEFAULT_FLOATING_POINT_PRECISION		float
#else
	#ifdef CONFIG_DEFAULT_FLOATING_POINT_PRECISION_DOUBLE
		#define CONFIG_DEFAULT_FLOATING_POINT_PRECISION		double
	#else
		#define CONFIG_DEFAULT_FLOATING_POINT_PRECISION		float
	#endif
#endif

#define CONFIG_SIERPI_GUI_FLOATING_POINT_PRECISION	CONFIG_DEFAULT_FLOATING_POINT_PRECISION


/**
 * scalar type used in the triangle factory describing in which format the 3 triangle vertices are stored
 */
#define CONFIG_SIERPI_TRIANGLE_FACTORY_SCALAR_TYPE CONFIG_DEFAULT_FLOATING_POINT_PRECISION


/**
 * number of SIMD vector elements
 */
#define BUFFER_PADDING_FOR_SIMD_ELEMENTS	16



/**
 * create scan data
 */
#ifndef CONFIG_ENABLE_SCAN_DATA
#error "CONFIG_ENABLE_SCAN_DATA not defined"
#define CONFIG_ENABLE_SCAN_DATA		1
#endif


/**
 * assign clusters to single thread according to the scan workload information
 */
#ifndef CONFIG_ENABLE_SCAN_THREADING
#error "CONFIG_ENABLE_SCAN_THREADING not defined"
#define CONFIG_ENABLE_SCAN_THREADING	1
#endif


/**
 * enable scan split and join distribution
 */
#ifndef CONFIG_ENABLE_SCAN_SPLIT_AND_JOIN_DISTRIBUTION
#error "CONFIG_ENABLE_SCAN_SPLIT_AND_JOIN_DISTRIBUTION not defined"
#endif

/**
 * force scan traversal whenever it is possible
 */
#ifndef CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL
#error "CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL not defined"
#endif


/**
 * Enable compilation with vertex data communication information
 */
#ifndef CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	#error "CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA is not defined"
	#define CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA 1
#endif


/**
 * MPI
 */
//#undef CONFIG_ENABLE_MPI


#ifndef CONFIG_ENABLE_MPI
#error "CONFIG_ENABLE_MPI not defined"
#define CONFIG_ENABLE_MPI 1
#endif


/**
 * Enable gui by default
 */
#ifndef CONFIG_SIERPI_ENABLE_GUI
#error "CONFIG_SIERPI_ENABLE_GUI not defined"
#define CONFIG_SIERPI_ENABLE_GUI 1
#endif


/**
 * Reuse stacks from parent sub-cluster
 */
#if 1
	#define CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS	0
#else
	#define CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS	1
#endif



#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS && CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
	#error "adaptive sub-cluster stacks in combination with parent's stack reusage not implemented yet!"
#endif



/**
 * Enable/disable compilation with validation stacks and validation tests
 */
#if DEBUG
	#define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		1
#else
	#define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		0
#endif



/**
 * Enable/disable deletion of CCluster classes due to split operation
 */
#if 1
	#define CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION		1
#else
	#define CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION		0
#endif

#ifndef CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
	#define CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING	0
#endif


/**************************************
 * OPENMP SPECIFIC
 **************************************/
/**
 * when compiled with omp, use this extra clause appended to #pragma omp task
 */


// untie tasks to be executed on other cpu after being idle and thus interrupted
//#define OPENMP_EXTRA_TASK_CLAUSE
#ifndef OPENMP_EXTRA_TASK_CLAUSE
	#define OPENMP_EXTRA_TASK_CLAUSE	untied
#endif

/**
 * should a task be created for the second leaf of the generic tree or should
 * the computation be done by the currently running task
 *
 * TODO: default: deactivated since this boosts the performance. however, this may
 * change in the future!
 *
 * 2012-05-06: for many-core systems it seems to be faster
 */
#ifndef OPENMP_CREATE_TASK_FOR_SECOND_LEAF
	#define OPENMP_CREATE_TASK_FOR_SECOND_LEAF	1
#endif


/**************************************
 * SIMULATION SPECIFIC
 **************************************/
/**
 * allow periodic boundaries?
 * in this case, the validation for the edge comm normals and vertices cannot be applied anymore!
 */
#ifndef CONFIG_PERIODIC_BOUNDARIES
	#define CONFIG_PERIODIC_BOUNDARIES	1
#endif


/**
 * create code with normals as integer parameters for traversators
 *
 * see also configuration option config.internal_traversator_normal_as_int_parameter
 */
#define CONFIG_TRAVERSATORS_WITH_NORMALS_AS_INTS	1

#ifndef CONFIG_TBB_TASK_AFFINITIES
	#define CONFIG_TBB_TASK_AFFINITIES 0
#endif

#ifndef CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL
	#define CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL		0
#endif



#ifndef CONFIG_ENABLE_ASAGI
# error "CONFIG_ENABLE_ASAGI not defined"
# define CONFIG_ENABLE_ASAGI 1
#endif


#ifndef CONFIG_ENABLE_SIMPLE_NETCDF
# error "CONFIG_ENABLE_SIMPLE_NETCDF not defined"
# define CONFIG_ENABLE_SIMPLE_NETCDF 1
#endif

