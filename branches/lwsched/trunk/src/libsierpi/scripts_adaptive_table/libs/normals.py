###################################
# Convenient handler for normals
###################################

import math

#
# normal IDs:
# the directions are enumerated in clockwise order to handle rotations by simply using arithmetic operations
#
n, ne, e, se, s, sw, w, nw = range(0,8)

# get a string for the normal id
def getString(num):
    return ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'][num]

# return the normal belonging to the direction
def getNormal(num):
    a = "%.15f" % (1.0/math.sqrt(2.0))
    ma = "%.15f" % (-1.0/math.sqrt(2.0))
    o = str(0)
    l = str(1)
    ml = str(-1)
    return    [[o, l], [a,a], [l,o], [a,ma], [o,ml], [ma,ma], [ml,o], [ma,a]][num]

def getArrayNormal(num):
    return ["CTriangle_Normals::normal_table["+str(num)+"][0]", "CTriangle_Normals::normal_table["+str(num)+"][1]"]

# return the normal belonging to the inverted direction
def getInverseNormal(num):
    return getNormal((num+4)%8)

