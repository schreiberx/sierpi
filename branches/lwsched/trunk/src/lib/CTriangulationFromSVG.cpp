/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Apr 8, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#include "CTriangulationFromSVG.hpp"
#include <iostream>
#include <stdlib.h>
#if CONFIG_ENABLE_LIBXML
#	include <libxml/parser.h>
#endif
#include "libmath/CMath.hpp"


CTriangulationFromSVG::CTriangulationFromSVG()
{
}


static inline float crossProd(const float a[2], const float b[2])
{
	return a[0]*b[1] - a[1]*b[0];
}

static inline float dotProd(const float a[2], const float b[2])
{
	return a[0]*b[0] + a[1]*b[1];
}


static inline void normalize(float a[2])
{
	float l = 1.0f/CMath::sqrt<float>(a[0]*a[0]+a[1]*a[1]);
	a[0] *= l;
	a[1] *= l;
}


/**
 * setup a new triangle given by a string in SVG path format
 *
 * only 3 vertices are allowed to be given
 *
 * triangle string is given in form:
 * "m 0,1040.3622 0.70710678,0.7071 -1.41421358,0 z"
 */
bool CTriangulationFromSVG::newTriangleFromString(std::string triangleString)
{

	if (triangleString.substr(0, 2) != "m ")
	{
		std::cerr << "invalid triangle string at beginning" << std::endl;
		return false;
	}

	/*
	 * find the vertex positions by searching for ' ' characters
	 */
	size_t vertexPos[4];
	vertexPos[0] = 1;

	for (int i = 0; i < 3; i++)
	{
		vertexPos[i+1] = triangleString.find(" ", vertexPos[i]+1);

		if (vertexPos[i+1] == std::string::npos)
		{
			std::cerr << "vertex not found" << std::endl;
			std::cerr << "vertexPos " << i << std::endl;
			return false;
		}
	}

	/*
	 * 1) read out the vertex strings
	 * 2) split them up to their components by the separation comma ','
	 * 3) convert them to floating point value and store to vertices_raw
	 */
	std::string vertexString[3];
	float vertices_raw[3][2];

	for (int i = 0; i < 3; i++)
	{
		vertexString[i] =	triangleString.substr(vertexPos[i], vertexPos[i+1]-vertexPos[i]);

		if (vertexString[i] == "")
		{
			std::cerr << "invalid vertex format" << std::endl;
			std::cerr << "vertex " << i << ": " << vertexString[i] << std::endl;
			return false;
		}

		size_t commaPos = vertexString[i].find(',');
		if (commaPos == std::string::npos)
		{
			std::cerr << "invalid comma position" << std::endl;
			std::cerr << "vertex " << i << ": " << vertexString[i] << std::endl;
			return false;
		}

		vertices_raw[i][0] = atof(vertexString[i].substr(0, commaPos).c_str());
		vertices_raw[i][1] = -atof(vertexString[i].substr(commaPos+1).c_str());
	}

	/*
	 * convert path to absolute vertex coordinates
	 *
	 * also apply a possible transformation
	 */
	vertices_raw[0][0] += transform_x;
	vertices_raw[0][1] += transform_y;
	vertices_raw[1][0] += vertices_raw[0][0];
	vertices_raw[1][1] += vertices_raw[0][1];
	vertices_raw[2][0] += vertices_raw[1][0];
	vertices_raw[2][1] += vertices_raw[1][1];


#if 0
	std::cout << "raw vertices: " << std::endl;
	for (int i = 0; i < 3; i++)
		std::cout << vertices_raw[i][0] << " " << vertices_raw[i][1] << std::endl;
	std::cout << std::endl;
#endif

	/*
	 * search for the corner with a perpendicular angle
	 */
	size_t idx_perpendicular_corner;
	for (idx_perpendicular_corner = 0; idx_perpendicular_corner < 3; idx_perpendicular_corner++)
	{
		size_t ia = (idx_perpendicular_corner+0)%3;
		size_t ib = (idx_perpendicular_corner+1)%3;
		size_t ic = (idx_perpendicular_corner+2)%3;

		float va[2] = {
				vertices_raw[ib][0] - vertices_raw[ia][0],
				vertices_raw[ib][1] - vertices_raw[ia][1]
		};
		normalize(va);

		float vb[2] = {
				vertices_raw[ic][0] - vertices_raw[ia][0],
				vertices_raw[ic][1] - vertices_raw[ia][1]
		};
		normalize(vb);

		if (CMath::abs(dotProd(va, vb)) < 0.001f)
			break;
	}

	if (idx_perpendicular_corner == 3)
	{
		std::cerr << "perpendicular corner not found!" << std::endl;
		return false;
	}

	float vertices[3][2];
	for (int i = 0; i < 3; i++)
	{
		vertices[i][0] = vertices_raw[(i+idx_perpendicular_corner+1)%3][0];
		vertices[i][1] = vertices_raw[(i+idx_perpendicular_corner+1)%3][1];
	}

	float va[2] = {
			vertices[0][0] - vertices[2][0],
			vertices[0][1] - vertices[2][1]
	};
	normalize(va);

	float vb[2] = {
			vertices[1][0] - vertices[2][0],
			vertices[1][1] - vertices[2][1]
	};
	normalize(vb);

	if (crossProd(va, vb) < 0.001)
	{
		std::swap(vertices[0][0], vertices[1][0]);
		std::swap(vertices[0][1], vertices[1][1]);
	}

#if 0
	std::cout << "triangle found: " << triangleString << std::endl;
	for (int i = 0; i < 3; i++)
		std::cout << vertices[i][0] << " " << vertices[i][1] << std::endl;
	std::cout << std::endl;
#endif

	CTriangle2D t_tmp;
	triangles.push_back(t_tmp);
	CTriangle2D &t = triangles.back();

	for (int i = 0; i < 3; i++)
	{
		t.vertices[i][0] = vertices[i][0];
		t.vertices[i][1] = vertices[i][1];
	}

	return true;
}

bool CTriangulationFromSVG::searchPath(xmlNode * a_node)
{
#if CONFIG_ENABLE_LIBXML
    static const std::string string_path("path");
    static const std::string string_d("d");

    for (xmlNode *cur_node = a_node; cur_node; cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
        	// if we are inside the drawable area, search for path
        	if (string_path == std::string((char*)cur_node->name))
        	{
        		// search for translation attribute
        	    for (xmlAttr *cur_attr = cur_node->properties; cur_attr; cur_attr = cur_attr->next)
        	    {
            		if (cur_attr->type == XML_ATTRIBUTE_NODE)
            		{
						if (string_d == std::string((char*)cur_attr->name))
						{
							std::string triangleString((char*)cur_attr->children->content);
							if (!newTriangleFromString(triangleString))
								return false;
						}
            		}
        	    }
        	}
        }
    }
#endif
    return true;
}


bool CTriangulationFromSVG::setupTransform(std::string transformString)
{
	static std::string string_translate("translate(");
	static size_t len = string_translate.length();

	if (transformString.substr(0, len) != string_translate)
	{
		std::cerr << "invalid translation string at beginning" << std::endl;
		return false;
	}

	size_t commaPos = transformString.find(',', len+1);
	if (commaPos == std::string::npos)
	{
		std::cerr << "comma position not found in translation information" << std::endl;
		return false;
	}

	size_t endPos = transformString.find(')', commaPos+1);
	if (endPos == std::string::npos)
	{
		std::cerr << "end position not found in translation information" << std::endl;
		return false;
	}

	transform_x = atof(transformString.substr(len+1, commaPos).c_str());
	transform_y = -atof(transformString.substr(commaPos+1, endPos).c_str());

//	std::cout << "t: " << transform_x << " " << transform_y << std::endl;
	return true;
}

bool CTriangulationFromSVG::searchDrawingArea(xmlNode * a_node)
{
#if CONFIG_ENABLE_LIBXML
	static const std::string string_g("g");
	static const std::string string_transform("transform");

    for (xmlNode *cur_node = a_node; cur_node; cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE)
        {
//            std::cout << (char*)cur_node->name << std::endl;

        	// if we are inside the drawable area, search for path
        	if (string_g == std::string((char*)cur_node->name))
        	{
        		// search for translation attribute
        	    for (xmlAttr *cur_attr = cur_node->properties; cur_attr; cur_attr = cur_attr->next)
        	    {
            		if (cur_attr->type == XML_ATTRIBUTE_NODE)
            		{
						if (string_transform == std::string((char*)cur_attr->name))
						{
							std::string transformString((char*)cur_attr->children->content);
							if (!setupTransform(transformString))
								return false;
						}
            		}
        	    }

        	    if (!searchPath(cur_node->children))
        	    	return false;
        	}
        	else
        	{
        		if (!searchDrawingArea(cur_node->children))
        			return false;
        	}
        }
    }
#endif
    return true;
}

bool CTriangulationFromSVG::loadSVGFile(const char *filename)
{
    triangles.clear();

#if CONFIG_ENABLE_LIBXML
    xmlDoc *doc;
    xmlNode *root_element;

    doc = xmlReadFile(filename, NULL, 0);

    if (doc == NULL)
    {
    	std::cerr << "failed to parse file " << filename << std::endl;
        xmlCleanupParser();
    	return false;
    }

	transform_x = 0;
	transform_y = 0;

    root_element = xmlDocGetRootElement(doc);
    bool valid = searchDrawingArea(root_element);
    xmlFreeDoc(doc);

    xmlCleanupParser();

#if 0
    std::cout << triangles.size() << " triangle(s) found" << std::endl;

    for (std::vector<CTriangle2D>::iterator iter = triangles.begin(); iter != triangles.end(); iter++)
   	{
    	for (int i = 0; i < 3; i++)
    		std::cout << iter->vertices[i][0] << " " << iter->vertices[i][1] << std::endl;
    	std::cout << std::endl;
   	}

#endif

    return valid;
#else
    return true;
#endif
}
