/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */

#ifndef KERNEL_CADAPTIVE_ELEMENT_VALIDATE_HPP_
#define KERNEL_CADAPTIVE_ELEMENT_VALIDATE_HPP_

#include "libmath/CVector.hpp"
#include "CValidateTypes.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Cell.hpp"

namespace sierpi
{
namespace kernels
{

class CAdaptive_Cell_Normals_Validate
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Cell<CAdaptive_Cell_Normals_Validate, CValCellData>	TRAV;
	typedef float TVertexScalar;

	void setup()
	{
	}

	CAdaptive_Cell_Normals_Validate()
	{
		setup();
	}

	inline bool should_refine(
			int depth,
			CValCellData *element
	)
	{
		return rand()%10 == 1;

		return element->refine;
	}

	inline void refine_l_r(
			float normal_hypx, float normal_hypy,
			float normal_rightx, float normal_righty,
			float normal_leftx, float normal_lefty,
			int depth,
			CValCellData *element,
			CValCellData *left_element,
			CValCellData *right_element
	)
	{
		// left element
		left_element->hyp = element->left;
		CVertex2d<float> normal_hyp = CVertex2d<float>(normal_hypx, -normal_hypy);
		// abuse normal_hyp as vector
		normal_hyp = normal_hyp*(element->hyp-element->left).getDotProd(normal_hyp);

#if 1
		if (CMath::abs(normal_hyp[0]) < 1.49012e-05)
			normal_hyp[0] = 0;
		if (CMath::abs(normal_hyp[1]) < 1.49012e-05)
			normal_hyp[1] = 0;
#endif
		left_element->right = element->left+normal_hyp;
		left_element->left = (element->left+element->right)*0.5;
#if 1
		if (CMath::abs(left_element->left[0]) < 1.49012e-05)
			left_element->left[0] = 0;
		if (CMath::abs(left_element->left[1]) < 1.49012e-05)
			left_element->left[1] = 0;
#endif
		left_element->refine = false;

		// right element
		right_element->hyp = element->right;
//		right_element->left = element->hyp;
		right_element->left = element->right+normal_hyp;
		right_element->right = left_element->left;
		right_element->refine = false;
	}


	inline void refine_ll_r(
			float normal_hypx, float normal_hypy,
			float normal_rightx, float normal_righty,
			float normal_leftx, float normal_lefty,
			int depth,
			CValCellData *element,
			CValCellData *left_left_element,
			CValCellData *left_right_element,
			CValCellData *right_element)
	{
		refine_l_r(normal_hypx, normal_hypy, 0, 0, 0, 0, depth,
				element, left_right_element, right_element);

		refine_l_r(normal_leftx, normal_lefty, 0, 0, 0, 0, depth,
				left_right_element, left_left_element, left_right_element);
	}



	inline void refine_l_rr(
			float normal_hypx, float normal_hypy,
			float normal_rightx, float normal_righty,
			float normal_leftx, float normal_lefty,
			int depth,
			CValCellData *element,
			CValCellData *left_element,
			CValCellData *right_left_element,
			CValCellData *right_right_element)
	{
		refine_l_r(normal_hypx, normal_hypy, 0, 0, 0, 0, depth,
				element, left_element, right_right_element);

		refine_l_r(normal_rightx, normal_righty, 0, 0, 0, 0, depth,
				right_right_element, right_left_element, right_right_element);
	}


	inline void refine_ll_rr(
			float normal_hypx, float normal_hypy,
			float normal_rightx, float normal_righty,
			float normal_leftx, float normal_lefty,
			int depth,
			CValCellData *element,
			CValCellData *left_left_element,
			CValCellData *left_right_element,
			CValCellData *right_left_element,
			CValCellData *right_right_element
	)
	{
		refine_l_r(normal_hypx, normal_hypy, 0, 0, 0, 0, depth,
				element, left_right_element, right_right_element);

		refine_l_r(normal_leftx, normal_lefty, 0, 0, 0, 0, depth,
				left_right_element, left_left_element, left_right_element);

		refine_l_r(normal_rightx, normal_righty, 0, 0, 0, 0, depth,
				right_right_element, right_left_element, right_right_element);
	}

	inline bool should_coarsen(
			int depth,
			CValCellData *element
	)
	{
		return false;
	}

	inline void coarsen(
			float normal_hypx, float normal_hypy,
			float normal_rightx, float normal_righty,
			float normal_leftx, float normal_lefty,
			int depth,
			CValCellData *left_element,
			CValCellData *right_element,
			CValCellData *coarsed_element
	)
	{
		coarsed_element->left = left_element->hyp;
		coarsed_element->right = right_element->hyp;
		coarsed_element->hyp = (left_element->right+right_element->left)*0.5;
	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
