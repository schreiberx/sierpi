/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_HPP_

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
	#include <pthread.h>
#endif


#include "../lib/CStopwatch.hpp"

#include "../tsunami_common/types/CTsunamiTypes.hpp"

#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

#if CONFIG_SIERPI_ENABLE_GUI
#	include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
#	include "../tsunami_common/kernels/backends/COpenGL_Vertices_Cell_Root_Tsunami.hpp"
//#	include "../tsunami_common/kernels/backends/COpenGL_Vertices_Smooth_Cell_Tsunami.hpp"
#endif

#include "../tsunami_common/kernels/backends/CSimulationTsunami_OutputGridDataArrays.hpp"

#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"


//#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Cell_Tsunami.hpp"
//#include "libsierpi/kernels/specialized_tsunami/CSetup_Column.hpp"

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../tsunami_common/kernels/CSetup_TsunamiCellData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"
#include "lib/CStopwatch.hpp"

#include "../tsunami_common/CTsunamiSimulationScenarios.hpp"

#include "../tsunami_common/kernels/modifiers/CSetup_CellData.hpp"

#include "CSimulation_MainInterface.hpp"
#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulation_MainGuiInterface.hpp"
#endif
#include "CSimulation_MainInterface_FileOutput.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

class CSimulationTsunami_Serial	:
	public CTsunamiSimulationParameters,
	public CSimulation_MainInterface,
	public CSimulation_MainInterface_FileOutput
#if CONFIG_SIERPI_ENABLE_GUI
	,
	public CSimulation_MainGuiInterface
#endif
{
public:
	/// traversator to setup the structure stack and element data stack by a given depth
	sierpi::travs::CSetup_Structure_CellData<CTsunamiSimulationStacksAndTypes> cSetup_Structure_CellData;

	/// to offer single-element modifications by coordinates, this traversal cares about it
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacksAndTypes>::TRAV cModify_OneElementValue_SelectByPoint;

	/// output element data at given point
	sierpi::kernels::CStringOutput_CellData_Normal_SelectByPoint<CTsunamiSimulationStacksAndTypes>::TRAV cStringOutput_CellData_SelectByPoint;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	/// validation functions
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;
#endif

	/// exchange of edge communication data and timestep on fixed grid
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/// adaptive traversals to refine or coarsen grid cells without having hanging nodes
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData cTsunami_Adaptive;

#if CONFIG_SIERPI_ENABLE_GUI
	COpenGL_Vertices_Cell_Root_Tsunami cOpenGL_Vertices_Cell_Root_Tsunami;
	COpenGL_Vertices_Wireframe_Root_Tsunami cOpenGL_Vertices_Wireframe_Root_Tsunami;
#endif

	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	sierpi::kernels::CSetup_TsunamiCellData_Validation::TRAV cSetup_TsunamiCellData_Validation;
#endif

public:
	CSimulationStacks<CTsunamiSimulationTypes, CTsunamiVisualizationTypes> *cStacks;

	CTriangle_Factory cTriangleFactory;

	/*
	 * part of CFL condition returned by kernels
	 */
	TTsunamiDataScalar part_of_cfl_timestep_condition;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CTsunamiSimulationScenarios cTsunamiSimulationScenarios;

	/**
	 * handler to simulation parameters to use same code as for parallel version
	 */
	CTsunamiSimulationParameters &cTsunamiSimulationParameters;

public:
	/**
	 * constructor for serial tsunami simulation
	 */
	CSimulationTsunami_Serial(
			int i_verbosity_level
	)	:
		cStacks(nullptr),
		cTsunamiSimulationScenarios(*this, i_verbosity_level),
		cTsunamiSimulationParameters(*this)
	{
		verbosity_level = i_verbosity_level;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			testPinToCore(-2);
#endif

		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = grid_initial_recursion_depth + grid_max_relative_recursion_depth;

		reset();
	}


#if CONFIG_SIERPI_ENABLE_GUI

	/**
	 * render BATHYMETRY
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		switch(i_bathymetry_visualization_method % 3)
		{
			case 1:
				render_terrainBathymetry_smooth(cCommonShaderPrograms.cBlinn);
				return "smooth";

			case 2:
				// do not render bathymetry
				return "blank";

			default:
			{

				CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::B);

				TTsunamiDataScalar *v = cGridDataArrays.triangle_vertex_buffer;
				TTsunamiDataScalar *n = cGridDataArrays.triangle_normal_buffer;
				TTsunamiDataScalar t;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.b[i]*cTsunamiSimulationParameters.visualization_terrain_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Cell_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cTsunamiSimulationParameters.number_of_local_cells*3
					);

				cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}
			return "simple";
		}
	}


	/**
	 * render water surface
	 */
	const char *render_DOFs(
			int i_surface_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);



		const char *ret_str = nullptr;
		TTsunamiDataScalar *v;
		TTsunamiDataScalar *n;
		TTsunamiDataScalar t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::HU);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.hu[i]*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "x-momentum";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::HV);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.hv[i]*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "y-momentum";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::B);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.b[i]*cTsunamiSimulationParameters.visualization_terrain_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "bathymetry";
				break;


			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::H + CSimulationTsunami_GridDataArrays::B);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.h[i]+cGridDataArrays.b[i])*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "surface height";
				break;
		}

		cCommonShaderPrograms.cHeightColorBlinn.use();
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

		cOpenGL_Vertices_Cell_Root_Tsunami.render(
				cGridDataArrays.triangle_vertex_buffer,
				cGridDataArrays.triangle_normal_buffer,
				cTsunamiSimulationParameters.number_of_local_cells*3
			);

		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;
	}



	void p_render_Wireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);

		storeDOFsToGridDataArrays(&cGridDataArrays, 0);

		TTsunamiDataScalar *v, *l;
		TTsunamiDataScalar *lines = new TTsunamiDataScalar[cTsunamiSimulationParameters.number_of_local_cells*2*3*3];

		v = cGridDataArrays.triangle_vertex_buffer;
		l = lines;

		for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
		{
			for (int vn = 0; vn < 2; vn++)
			{
				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				l[0] = v[3+0];
				l[1] = v[3+2];
				l[2] = -v[3+1];
				l += 3;

				v += 3;
			}

			l[0] = v[0];
			l[1] = v[2];
			l[2] = -v[1];
			l += 3;

			v -= 6;

			l[0] = v[0];
			l[1] = v[2];
			l[2] = -v[1];
			l += 3;

			v += 9;
		}

		cShaderBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
				lines,
				cTsunamiSimulationParameters.number_of_local_cells*3*2
			);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();

		delete [] lines;
	}

	void render_Wireframe(
			int i_visualization_render_wireframe,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
			p_render_Wireframe(cCommonShaderPrograms.cBlinn);
	}


	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}

	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}



	void p_applyTranslateAndScaleToVertex(TTsunamiVertexScalar *io_vertex)
	{
		io_vertex[0] = (io_vertex[0] + cTsunamiSimulationParameters.visualization_domain_translate_x)*cTsunamiSimulationParameters.visualization_domain_scale_x;
		io_vertex[1] = (io_vertex[1] + cTsunamiSimulationParameters.visualization_domain_translate_y)*cTsunamiSimulationParameters.visualization_domain_scale_y;
		io_vertex[2] = (io_vertex[2] + cTsunamiSimulationParameters.visualization_domain_translate_z)*cTsunamiSimulationParameters.visualization_domain_scale_z;
	}


	void storeDOFsToGridDataArrays(
			CSimulationTsunami_GridDataArrays *io_cGridDataArrays,
			int i_flags
	)
	{
		i_flags |=
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::NORMALS;


		size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(number_of_local_cells);

		// We instantiate it right here to avoid any overhead due to split/join operations
		sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				io_cGridDataArrays,
				offset,
				i_flags
			);

		cOutputGridDataArrays.action(cStacks);


		assert(io_cGridDataArrays->number_of_triangle_cells == cTsunamiSimulationParameters.number_of_local_cells);

		/*
		 * postprocessing
		 */
		if (	cTsunamiSimulationParameters.visualization_domain_scale_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_z != 1.0 ||

				cTsunamiSimulationParameters.visualization_domain_translate_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_z != 1.0
		)
		{
			for (int i = 0; i < cTsunamiSimulationParameters.number_of_local_cells; i++)
			{
				TTsunamiVertexScalar *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

				for (int vn = 0; vn < 3; vn++)
				{
					p_applyTranslateAndScaleToVertex(v);
					v += 3;
				}

				io_cGridDataArrays->h[i] *= cTsunamiSimulationParameters.visualization_domain_scale_z;
				io_cGridDataArrays->b[i] *= cTsunamiSimulationParameters.visualization_domain_scale_z;
			}
		}
	}

	void render_surfaceDefault_simple(
			CGlProgram &cShaderBlinn
	)
	{
		cShaderBlinn.use();
#if 0
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Cell_Tsunami_simple.cKernelClass.setup(
				visualization_water_surface_default_displacement,
				visualization_water_surface_scale_factor,
				0,
				&cOpenGL_Vertices_Cell_Root_Tsunami
			);
		cOpenGL_Vertices_Cell_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
#endif
		cShaderBlinn.disable();
	}



	void render_surface_aligned(
			CGlProgram &cShaderBlinn
	)
	{
		cShaderBlinn.use();

#if 0
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Cell_Tsunami_aligned.cKernelClass.setup(
				visualization_water_surface_default_displacement,
				visualization_water_surface_scale_factor,
				0,
				&cOpenGL_Vertices_Cell_Root_Tsunami
			);
		cOpenGL_Vertices_Cell_Tsunami_aligned.action(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
#endif
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_aligned(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

#if 0
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Cell_Tsunami_Bathymetry_aligned.cKernelClass.setup(
				visualization_terrain_default_displacement,
				visualization_terrain_scale_factor,
				0,
				&cOpenGL_Vertices_Cell_Root_Tsunami
			);
		cOpenGL_Vertices_Cell_Tsunami_Bathymetry_aligned.action(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
#endif
		cShaderBlinn.disable();
	}


	void render_terrainBathymetry_smooth(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

#if 0
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Cell_Tsunami_Bathymetry_smooth.cKernelClass.setup(visualization_terrain_default_displacement, visualization_terrain_scale_factor, &cOpenGL_Vertices_Cell_Root_Tsunami);
		cOpenGL_Vertices_Cell_Tsunami_Bathymetry_smooth.action_FirstPass(cStacks);
		cOpenGL_Vertices_Cell_Tsunami_Bathymetry_smooth.action_SecondPass_Serial(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
#endif

		cShaderBlinn.disable();
	}

	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();
#if 0
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Cell_Tsunami_simple.cKernelClass.setup(
				visualization_water_surface_default_displacement,
				visualization_water_surface_scale_factor,
				0,
				&cOpenGL_Vertices_Cell_Root_Tsunami
			);
		cOpenGL_Vertices_Cell_Tsunami_simple.action(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
#endif
		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceSmooth(
			CGlProgram &cShaderBlinn
	)
	{
#if 0
		cShaderBlinn.use();

		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Cell_Root_Tsunami);
		cOpenGL_Vertices_Smooth_Cell_Tsunami.action_FirstPass(cStacks);
		cOpenGL_Vertices_Smooth_Cell_Tsunami.action_SecondPass_Serial(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
#endif
	}


	void render_surfaceSmoothWithHeightColors(
			CGlProgram &cShaderBlinn
	)
	{
#if 0
		cShaderBlinn.use();

		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();
		cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(visualization_water_surface_default_displacement, visualization_water_surface_scale_factor, &cOpenGL_Vertices_Cell_Root_Tsunami);
		cOpenGL_Vertices_Smooth_Cell_Tsunami.action_FirstPass(cStacks);
		cOpenGL_Vertices_Smooth_Cell_Tsunami.action_SecondPass_Parallel(cStacks);
		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();

		cShaderBlinn.disable();
#endif
	}


#endif


	/**
	 * setup the stacks
	 */
	void setup_Stacks()
	{
		clean();

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		cStacks = new CSimulationStacks<CTsunamiSimulationTypes, CTsunamiVisualizationTypes>(
					(1 << grid_initial_recursion_depth) + (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CTsunamiSimulationCellData)),
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS		|
					CSimulationStacks_Enums::VERTEX_COMM_STACKS
				);
#else
		cStacks = new CSimulationStacks<CTsunamiSimulationTypes, CTsunamiVisualizationTypes>(
				grid_initial_recursion_depth+grid_max_relative_recursion_depth,
					CSimulationStacks_Enums::ELEMENT_STACKS			|
					CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#endif

	}


	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 0.5;
		cTriangleFactory.vertices[0][1] = -0.5;
		cTriangleFactory.vertices[1][0] = -0.5;
		cTriangleFactory.vertices[1][1] = 0.5;
		cTriangleFactory.vertices[2][0] = -0.5;
		cTriangleFactory.vertices[2][1] = -0.5;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		for (int i = 0; i < 3; i++)
		{
			cTriangleFactory.vertices[i][0] = (cTriangleFactory.vertices[i][0]*simulation_parameter_domain_size_x) + simulation_parameter_domain_origin_x;
			cTriangleFactory.vertices[i][1] = (cTriangleFactory.vertices[i][1]*simulation_parameter_domain_size_y) + simulation_parameter_domain_origin_y;
		}

		cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
		cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
		cSetup_Column.setup_sfcMethods(cTriangleFactory);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiCellData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

		cStringOutput_CellData_SelectByPoint.setup_sfcMethods(cTriangleFactory);
		cModify_OneElementValue_SelectByPoint.setup_sfcMethods(cTriangleFactory);
	}

	void setup()
	{
		reset();
	}

	void reset()
	{
		cTsunamiSimulationScenarios.setup();

		part_of_cfl_timestep_condition = 0;

		setup_Stacks();
		setup_TraversatorsAndKernels();
		p_setup_Parameters();
		p_setup_InitialTriangulation();

		reset_simulation_parameters();
	}


	void clean()
	{
		if (cStacks)
		{
			delete cStacks;
			cStacks = NULL;
		}
	}

	void setup_RadialDamBreak(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius
	)
	{
		number_of_local_cells = p_adaptive_traversal_setup_column(x, y, radius);
	}


	long long p_adaptive_traversal_setup_column(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius
	)
	{
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0

		simulation_dataset_breaking_dam_posx = x;
		simulation_dataset_breaking_dam_posy = y;
		simulation_dataset_breaking_dam_radius = radius;

		cSetup_Column.setup_KernelClass(
				simulation_dataset_breaking_dam_posx,
				simulation_dataset_breaking_dam_posy,
				simulation_dataset_breaking_dam_radius,
				2,
				&cTsunamiSimulationScenarios
		);

#else

		cSetup_Column.setup_KernelClass(
				simulation_dataset_breaking_dam_posx,
				simulation_dataset_breaking_dam_posy,
				simulation_dataset_breaking_dam_radius,
				2,
				&cTsunamiSimulationScenarios
		);

#endif

		long long prev_number_of_local_cells;
		do
		{
			prev_number_of_local_cells = number_of_local_cells;

			bool repeat_traversal = cSetup_Column.actionFirstTraversal(cStacks);

			while(repeat_traversal)
				repeat_traversal = cSetup_Column.actionMiddleTraversals_Serial(cStacks);

			cSetup_Column.actionLastTraversal_Serial(cStacks);

			number_of_local_cells = cStacks->element_data_stacks.getNumberOfElementsOnStack();
		} while (number_of_local_cells != prev_number_of_local_cells);

		number_of_local_cells = prev_number_of_local_cells;

		int backupSetupSurfaceMethod = simulation_water_surface_scene_id;
		simulation_water_surface_scene_id = CTsunamiSimulationScenarios::SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF;


		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
		cSetup_CellData.setup_sfcMethods(cTriangleFactory);
		cSetup_CellData.cKernelClass.setup_Parameters(&cTsunamiSimulationScenarios, false);
		cSetup_CellData.action(cStacks);

		simulation_water_surface_scene_id = backupSetupSurfaceMethod;

		return number_of_local_cells;
	}

private:
	void p_setup_Parameters()
	{
		cTsunami_EdgeComm.setParameters(simulation_parameter_timestep_size, simulation_parameter_domain_size_x, simulation_parameter_gravitation);
		cTsunami_EdgeComm.setBoundaryCondition((EBoundaryConditions)simulation_domain_boundary_condition);
		cTsunami_EdgeComm.setBoundaryDirichlet(&simulation_domain_boundary_dirichlet_edge_data);

		cTsunami_Adaptive.setup_KernelClass(
				simulation_parameter_domain_size_x,

				adaptive_refine_threshold,
				adaptive_coarsen_threshold,

				adaptive_refine_slope_threshold,
				adaptive_coarsen_slope_threshold,

				&cTsunamiSimulationScenarios
			);

		cTsunami_Adaptive.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);

		cSetup_Column.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);
	}


private:
	void p_setup_InitialTriangulation()
	{
		number_of_local_cells = cSetup_Structure_CellData.setup(cStacks, grid_initial_recursion_depth, simulation_parameter_cell_data_setup);
		number_of_global_cells = number_of_local_cells;

		number_of_local_initial_cells_after_domain_triangulation = number_of_local_cells;
		number_of_global_initial_cells_after_domain_triangulation = number_of_local_initial_cells_after_domain_triangulation;

		p_setup_initial_grid_data();
	}


private:
	void p_setup_initial_grid_data()
	{
		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
		cSetup_CellData.setup_sfcMethods(cTriangleFactory);
		cSetup_CellData.cKernelClass.setup_Parameters(&cTsunamiSimulationScenarios, true);
		cSetup_CellData.action(cStacks);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiCellData_Validation.action(cStacks);
#endif
	}


private:
	void p_simulation_adaptive_traversals()
	{
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		TTsunamiDataScalar adaptCFL;
		cTsunami_Adaptive.storeReduceValue(&adaptCFL);

		if (adaptCFL != CMath::numeric_inf<TTsunamiDataScalar>())
		{
			part_of_cfl_timestep_condition = CMath::min(adaptCFL, part_of_cfl_timestep_condition);
		}

		number_of_local_cells = cStacks->element_data_stacks.getNumberOfElementsOnStack();
	}

public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
		long long prev_number_of_local_cells;
		long long prev_number_of_local_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		TTsunamiDataScalar coarsen_threshold_backup = adaptive_coarsen_threshold;
		adaptive_coarsen_threshold = -9999999;

//		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 10;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_local_cells = number_of_local_cells;
			prev_number_of_local_clusters = number_of_local_clusters;

			// setup grid data
			p_setup_initial_grid_data();

			// run single timestep
			p_simulation_edge_comm();

			// refine / coarsen grid
			p_simulation_adaptive_traversals();

			// split/join clusters
//			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_local_cells << ", number_of_local_clusters: " << number_of_local_clusters << std::endl;

			if (	prev_number_of_local_cells == number_of_local_cells	&&
					prev_number_of_local_clusters == number_of_local_clusters
			)
				break;
		}

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		// update cluster parameters
		adaptive_coarsen_threshold = coarsen_threshold_backup;
//		updateClusterParameters();
	}

private:
	/**
	 * timestep edge comm
	 */
	void p_simulation_edge_comm()
	{
		if (simulation_parameter_adaptive_timestep_size_with_cfl)
		{
			// adaptive timestep size

			simulation_parameter_timestep_size = part_of_cfl_timestep_condition*simulation_parameter_cfl;

			if (simulation_parameter_timestep_size < 0.000001)
				simulation_parameter_timestep_size = 0.000001;
		}

		cTsunami_EdgeComm.actionFirstPass(cStacks);
		cTsunami_EdgeComm.actionSecondPass_Serial(cStacks, simulation_parameter_timestep_size, &part_of_cfl_timestep_condition);
	}

public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_CellDataAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		CTsunamiSimulationCellData element_data_modifier;

		element_data_modifier.setup(simulation_dataset_water_surface_default_displacement-simulation_dataset_terrain_default_distance, -simulation_dataset_terrain_default_distance);

		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_size_x, adaptive_refine_threshold, -1, adaptive_refine_slope_threshold, -999, &cTsunamiSimulationScenarios);
		p_simulation_adaptive_traversals();
		cTsunami_Adaptive.setup_KernelClass(simulation_parameter_domain_size_x, adaptive_refine_threshold, adaptive_coarsen_threshold, adaptive_refine_slope_threshold, adaptive_coarsen_slope_threshold, &cTsunamiSimulationScenarios);
	}


#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
public:
	inline void testPinToCore(int pinning_core)
	{
		if (pinning_core == -1)
			return;


		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);

		// pin to first core for initialization to assure placement of allocated data
		if (pinning_core == -2)
			CPU_SET(0, &cpu_set);
		else
			CPU_SET(pinning_core, &cpu_set);

		std::cout << "Pinning application to core " << pinning_core << std::endl;

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		std::cout << std::endl;
		std::cout << "WARNING: Adaptive cluster stacks activated!!!" << std::endl;
		std::cout << std::endl;
#endif

		int err;
		err = pthread_setaffinity_np(
				pthread_self(),
				sizeof(cpu_set_t),
				&cpu_set
			);


		if (err != 0)
		{
			std::cout << err << std::endl;
			perror("pthread_setaffinity_np");
			assert(false);
			exit(-1);
		}

		if (pinning_core >= 0)
			threading_pin_to_core_nr_after_one_timestep = -1;
	}
#endif

public:
	void runSingleTimestep()
	{
		p_simulation_edge_comm();
		p_simulation_adaptive_traversals();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/
	double getDataSample(
			double i_sample_pos_x,
			double i_sample_pos_y,
			const char *i_sample_information
		)
	{
		return 0;
	}

public:
	void debugOutput(CVector<2,float> planePosition)
	{
		cStringOutput_CellData_SelectByPoint.cKernelClass.setup(planePosition[0], planePosition[1]);
		cStringOutput_CellData_SelectByPoint.action(cStacks);
	}


	void writeSimulationDataToFile(
			const char *i_filename,
			const char *i_information_string = nullptr
	)
	{
		CSimulationTsunami_GridDataArrays cGridDataArrays(number_of_local_cells);

		sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

		int flags =
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::H	|
				CSimulationTsunami_GridDataArrays::HU	|
				CSimulationTsunami_GridDataArrays::HV	|
				CSimulationTsunami_GridDataArrays::B	|
				CSimulationTsunami_GridDataArrays::CFL_VALUE_HINT;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				&cGridDataArrays,
				0,
				flags
			);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

		cVtkXMLTrianglePolyData.setup();
		cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
		cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.h);
		cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.hu);
		cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.hv);
		cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.b);
		cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.cfl_value_hint);

		cVtkXMLTrianglePolyData.write(i_filename);
	}


	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		std::cout << "writeSimulationClustersDataToFile() not implemented (meaningless for serial version)" << std::endl;
	}

#if CONFIG_SIERPI_ENABLE_GUI

	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	bool gui_key_down_event(
			int key
	)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_RadialDamBreak(simulation_dataset_breaking_dam_posx, simulation_dataset_breaking_dam_posy, simulation_dataset_breaking_dam_radius);
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			default:
				return false;
		}

		return true;
	}


	bool gui_mouse_motion_event(
		TTsunamiDataScalar i_mouse_coord_x,
		TTsunamiDataScalar i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	bool gui_mouse_button_down_event(
		TTsunamiDataScalar i_mouse_coord_x,
		TTsunamiDataScalar i_mouse_coord_y,
		int i_button
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}
#endif


	void outputVerboseInformation()
	{
		((CTsunamiSimulationParameters&)(*this)).outputVerboseInformation();
	}

	void output_simulationSpecificData()
	{
		std::cout << "output_simulationSpecificData() not implemented" << std::endl;
	}

	void debug_OutputCellData(
				TTsunamiDataScalar i_position_x,		///< x-coordinate of triangle cell
				TTsunamiDataScalar i_position_y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputCellData() not implemented" << std::endl;
	}

	void debug_OutputClusterInformation(
				TTsunamiDataScalar x,	///< x-coordinate of triangle cell
				TTsunamiDataScalar y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputClusterInformation() not implemented" << std::endl;
	}

	void debug_OutputEdgeCommunicationInformation(
				TTsunamiDataScalar x,	///< x-coordinate of triangle cell
				TTsunamiDataScalar y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputEdgeCommunicationInformation() not implemented" << std::endl;
	}

	void output_ClusterTreeInformation()
	{
	}

	void action_Validation()
	{

	}

	virtual ~CSimulationTsunami_Serial()
	{
		clean();
	}

	void updateScanDatasets(
			int use_number_of_threads = -1
	)
	{
	}
};

#endif /* CTSUNAMI_HPP_ */
