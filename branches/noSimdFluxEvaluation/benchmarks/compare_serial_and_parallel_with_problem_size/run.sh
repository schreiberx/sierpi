#! /bin/bash

. ../tools.sh

TIMESTEPS=$TIMESTEPS_COARSEST_LEVEL


EXEC_S=../../build/sierpi_intel_tsunami_serial_release
EXEC_P=../../build/sierpi_intel_omp_tsunami_parallel_release


##########################################
# GENERIC PARAMETERS
##########################################
#
PARAMS="-a 0 "


##########################################
# SERIAL PARAMETERS
##########################################
#
PARAMS_S=""


##########################################
# PARALLEL PARAMETERS
##########################################
#
# -w 0: load world 0
# -o 999...: set splitting size to 999... to avoid any splitting
# -n 1: run on only one task
#

# parallel version with single thread and with splitting deactivated
PARAMS_P1="-w 0 -n 1 -o 999999999999"
# parallel version with single thread and enabled automatic splitting
PARAMS_P2="-w 0 -n 1 -u 10 -U 10"
# parallel version with 2 threads running and enabled automatic splitting
PARAMS_P3="-w 0 -n 2 -u 10 -U 10"
# parallel version with 6 threads running and enabled automatic splitting
PARAMS_P4="-w 0 -n 6 -u 10 -U 10"
# parallel version with 12 threads running and enabled automatic splitting
PARAMS_P5="-w 0 -n 12 -u 10 -U 10"


PROBLEM_SIZES_IN_DEPTH=`seq 10 28`

echo 
echo "Simulation on single triangle."
echo
echo "Serial: simply the serial version"
echo "Parallel1: parallel version without any splits"
echo "Parallel2: parallel version with any splits and joins"
echo "Parallel3: parallel version with any splits and joins running on 2 threads"
echo 
echo "INITIAL_DEPTH	MTPS_SERIAL	MTPS_PARALLEL1	MTPS_PARALLEL2	MTPS_PARALLEL3	MTPS_PARALLEL4	MTPS_PARALLEL5	TIMESTEPS	AVERAGE_TRIANGLES_SERIAL	AVERAGE_TRIANGLES_PARALLEL1	AVERAGE_TRIANGLES_PARALLEL2	AVERAGE_TRIANGLES_PARALLEL3	AVERAGE_PARTITIONS_PARALLEL2"

for d in $PROBLEM_SIZES_IN_DEPTH; do

	REL_DEPTH=$((26-d))
	[ $REL_DEPTH -lt 0 ] && REL_DEPTH=0

	TIMESTEPS=$((2**REL_DEPTH))
	[ $TIMESTEPS -lt 4 ] && TIMESTEPS=4

	######################
	# SERIAL
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_S $@"

	EXEC_CMD="$EXEC_S $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_SERIAL=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_SERIAL=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_SERIAL=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL nosplit
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P1 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL1=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_PARALLEL1=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL1=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL split
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P2 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL2=`getValueFromString "$OUTPUT" "MTPS"`
	AVERAGE_TRIANGLES_PARALLEL2=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL2=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL 2 threads
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P3 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL3=`getValueFromString "$OUTPUT" "MTPSPT"`
	AVERAGE_TRIANGLES_PARALLEL3=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL3=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL 6 threads
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P4 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL4=`getValueFromString "$OUTPUT" "MTPSPT"`
	AVERAGE_TRIANGLES_PARALLEL4=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL4=`getValueFromString "$OUTPUT" "PPST"`

	######################
	# PARALLEL 12 threads
	######################
	PARAMS_="-t $TIMESTEPS -d $d $PARAMS $PARAMS_P5 $@"

	EXEC_CMD="$EXEC_P $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS_PARALLEL4=`getValueFromString "$OUTPUT" "MTPSPT"`
	AVERAGE_TRIANGLES_PARALLEL5=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS_PARALLEL5=`getValueFromString "$OUTPUT" "PPST"`

	echo "$d	$MTPS_SERIAL	$MTPS_PARALLEL1	$MTPS_PARALLEL2	$MTPS_PARALLEL3	$MTPS_PARALLEL4	$MTPS_PARALLEL5	$TIMESTEPS	$AVERAGE_TRIANGLES_SERIAL	$AVERAGE_TRIANGLES_PARALLEL1	$AVERAGE_TRIANGLES_PARALLEL2	$AVERAGE_TRIANGLES_PARALLEL3	$AVERAGE_PARTITIONS_PARALLEL2"
done
