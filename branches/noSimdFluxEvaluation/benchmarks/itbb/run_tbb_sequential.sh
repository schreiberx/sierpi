#!/bin/bash

# application is running for 100 seconds in average

. params.sh

killall sierpi_intel_tbb_tsunami_parallel_release

EXEC="$EXEC_TBB"
PARAMS="$PARAMS_DEFAULT -N 40 -n 40 -A 1 -S 0"

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}


date
STARTSECONDS=`date +%s`

echo "App 1: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_sequential_1.txt"

echo "App 2: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_sequential_2.txt"

echo "App 3: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_sequential_3.txt"

echo "App 4: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_tbb_sequential_4.txt"


ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS" > "output_""$DATE""_tbb_sequential.txt"

sleep 1
