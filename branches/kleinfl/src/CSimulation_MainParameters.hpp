/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CSimulation_MainParameters.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_MAINPARAMETERS_HPP_
#define CSIMULATION_MAINPARAMETERS_HPP_

#include "lib/xmlconfig/CXMLConfig.hpp"
#include "global_config.h"

/**
 * this class describes the parameters which have to be
 * provided by the simulation to the CMain class
 */
class CSimulation_MainParameters
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE	T;

public:
	/**
	 * verbosity level
	 */
	int verbosity_level;

	/**
	 * number of threads to use for simulation
	 */
	int simulation_threading_number_of_threads;

	/**
	 * the time-step size
	 */
	T simulation_parameter_global_timestep_size;

	/**
	 * number of local cells in domain computed on one (local) node
	 */
	long long number_of_local_cells;


	/**
	 * overall number of local cells in domain computed across all nodes
	 */
	long long number_of_global_cells;


	/**
	 * number of simulation clusters
	 */
	long long number_of_local_clusters;


	/**
	 * number of simulation clusters
	 */
	long long number_of_global_clusters;



	/**
	 * load_balacing_statistics_each_nth_timestep
	 */
	int load_balancing_statistics_each_nth_timestep;



	/**
	 * number of local cells sent by migration
	 */
	long long migration_sent_number_of_local_cells;



	/**
	 * number of local clusters sent by migration
	 */
	long long migration_sent_number_of_local_cluster;


	/**
	 * number of triangles directly after domain triangulation
	 */
	long long number_of_local_initial_cells_after_domain_triangulation;


	/**
	 * number of global triangles directly after domain triangulation
	 */
	long long number_of_global_initial_cells_after_domain_triangulation;


	/**
	 * MPI rank
	 */
	int parallelization_mpi_rank;

	/**
	 * MPI size
	 */
	int parallelization_mpi_size;


	/**
	 * when this value is not equal to -1, the thread is pinned to the given core
	 */
	int threading_pin_to_core_nr_after_one_timestep;

	/**
	 * timestamp for current timestep
	 */
	T simulation_timestamp_for_timestep;

	/**
	 * timestamp for current timestep
	 */
	T simulation_timestamp_for_initial_timestep;

	/**
	 * number of current timestep
	 */
	long long simulation_timestep_nr;




	/**
	 * scale simulation along x-axis for visualization
	 */
	T visualization_scale_x;

	/**
	 * scale simulation along y-axis for visualization
	 */
	T visualization_scale_y;

	/**
	 * scale simulation along z-axis for visualization
	 */
	T visualization_scale_z;



	/**
	 * translate simulation along x-axis for visualization
	 */
	T visualization_translate_x;

	/**
	 * translate simulation along x-axis for visualization
	 */
	T visualization_translate_y;

	/**
	 * translate simulation along x-axis for visualization
	 */
	T visualization_translate_z;


	T visualization_data0_scale_factor;
	T visualization_data1_scale_factor;

#if SIMULATION_MULTILAYER_ENABLED
	/**
	 * layer id of multi-layer simulation to visualize
	 */
	int visualization_multi_layer_id;
#endif

	/**
	 * default visualization methods for dofs
	 */
	int visualization_dof_method;

	/**
	 * default visualization methods for boundary
	 */
	int visualization_boundary_method;


	/**
	 * flops per cell for statistics
	 */
	int flops_cell_update;

	/**
	 * get argument strings for getopt() command line parsing
	 */
	virtual const char* config_command_line_getArgumentString() = 0;


	/**
	 * handle a command line argument
	 */
	virtual bool config_command_line_parseArgument(
			char i_command_line_argument_option,	///< incoming argument character (e. g. 'e' for command line argument '-e')
			const char* i_command_line_argument		///< command line argument when additional value for argument is allowed
		) = 0;


	/**
	 * validate and fix parameters
	 */
	virtual bool config_validateAndFixParameters() = 0;


	/**
	 * output some config line arguments help
	 */
	virtual void config_command_line_getArgumentsHelp() = 0;

	/**
	 * setup xml global_config.handling interface
	 */
	virtual void config_xml_file_setupInterface(
			CXMLConfig &i_cXMLConfig
	) = 0;



	/**
	 * constructor
	 */
	CSimulation_MainParameters()	:
			verbosity_level(0),

			simulation_threading_number_of_threads(-1),
			simulation_parameter_global_timestep_size(-1),

			load_balancing_statistics_each_nth_timestep(-1),

			number_of_local_initial_cells_after_domain_triangulation(0),
			number_of_global_initial_cells_after_domain_triangulation(0),

			parallelization_mpi_rank(0),
			parallelization_mpi_size(1),

			threading_pin_to_core_nr_after_one_timestep(-1),
			simulation_timestamp_for_timestep(0),
			simulation_timestep_nr(0),

			visualization_scale_x(DEFAULT_PARAMETER_VISUALIZATION_SCALE_X),
			visualization_scale_y(DEFAULT_PARAMETER_VISUALIZATION_SCALE_Y),
			visualization_scale_z(DEFAULT_PARAMETER_VISUALIZATION_SCALE_Z),

			visualization_translate_x(DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_X),
			visualization_translate_y(DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Y),
			visualization_translate_z(DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Z),

			visualization_data0_scale_factor(DEFAULT_PARAMETER_VISUALIZATION_DATA0_SCALE),
			visualization_data1_scale_factor(DEFAULT_PARAMETER_VISUALIZATION_DATA1_SCALE),

			visualization_dof_method(0),
			visualization_boundary_method(0),

#if SIMULATION_MULTILAYER_ENABLED
			visualization_multi_layer_id(0),
#endif

			flops_cell_update(-1)
	{
		reset();
	}


	void reset()
	{
		number_of_local_cells = 0;
		number_of_global_cells = 0;

		number_of_local_clusters = 0;
		number_of_global_clusters = 0;

		migration_sent_number_of_local_cells = 0;
		migration_sent_number_of_local_cluster = 0;
	}

	void resetAfterSetup()
	{
	}

	virtual ~CSimulation_MainParameters()
	{

	}
};


#endif /* CSIMULATION_MAINPARAMETERS_HPP_ */
