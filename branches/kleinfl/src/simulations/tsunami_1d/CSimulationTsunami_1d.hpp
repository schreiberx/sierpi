/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: August 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>, Breuer Alexander <breuera@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_1D_HPP_
#define CSIMULATIONTSUNAMI_1D_HPP_

#include <iostream>
#include <stdexcept>

#include "global_config.h"
#include "lib/CStopwatch.hpp"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "../hyperbolic_common/traversators/CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_EdgeComm_Normals_Depth.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_Setup_Column.hpp"

#if CONFIG_SIERPI_ENABLE_GUI
#	include "libgl/draw/CGlDrawWireframeFromVertexArray.hpp"
#	include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalArray.hpp"
#endif

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "lib/CStopwatch.hpp"

#include "../hyperbolic_common/CParameters.hpp"
#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_CellData.hpp"

#include "CSimulation_MainInterface.hpp"
#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulation_MainGuiInterface.hpp"
#endif
#include "CSimulation_MainInterface_FileOutput.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"


#include "COutputSimulationSpecificData.hpp"

#include "CGrid_1D.hpp"

#include "CSimulationTsunami_1d_FileOutput.hpp"


class CSimulationTsunami_1d:
	public CSimulation_MainInterface
#if CONFIG_SIERPI_ENABLE_GUI
	,
	public CSimulation_MainGuiInterface
#endif
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CGrid_1D<T> cGrid_1D;

#if CONFIG_SIERPI_ENABLE_GUI
	CGlDrawTrianglesWithVertexAndNormalArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesFromVertexAndNormalArray;
	CGlDrawWireframeFromVertexArray<CHyperbolicTypes::CVisualizationTypes::T> cOpenGL_Vertices_Wireframe_Root_Tsunami;
#endif

public:
	sierpi::CTriangle_Factory cTriangleFactory;

	/**
	 * simulation parameter
	 */
	class CParametersExtended	:
			public CParameters
	{
		CSimulationTsunami_1d &parent;

	public:
		CParametersExtended(
				CSimulationTsunami_1d &i_parent
		)	:
			parent(i_parent)
		{

		}

	public:
		void updateClusterParameters()
		{
//			parent.updateClusterParameters();
		}
	};

	CParametersExtended cParameters;


	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CDatasets cDatasets;

	/**
	 * output of simulation specific data
	 */
	COutputSimulationSpecificData cOutputSimulationSpecificData;

	/**
	 * file io for 1d simulation
	 */
	CSimulationTsunami_1d_FileOutput cFileOutput;

public:
	/**
	 * constructor for serial tsunami simulation
	 */
	CSimulationTsunami_1d(
			int i_verbosity_level
	)	:
		cParameters(*this),
		cDatasets(cParameters, i_verbosity_level),
		cOutputSimulationSpecificData(cParameters, cDatasets, cGrid_1D),
		cFileOutput(cParameters, cDatasets, cGrid_1D)
	{
		cParameters.verbosity_level = i_verbosity_level;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			testPinToCore(-2);
#endif

		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = cParameters.grid_initial_recursion_depth + cParameters.grid_max_relative_recursion_depth;

//		reset();
	}


#if CONFIG_SIERPI_ENABLE_GUI

	/**
	 * render BATHYMETRY
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		switch(i_bathymetry_visualization_method % 4)
		{
			case 2:
			{
				CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells, 2, 1, CGridDataArrays_Enums::ALL);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[5][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_cells*2
					);

				cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();

				return "benchmark data";
			}

			case 3:
				// do not render bathymetry
				return "blank";

			default:
			{
				CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells, 2, 1, CGridDataArrays_Enums::ALL);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_cells*2
					);

				cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}

			return "simple";
		}
	}


	/**
	 * render water surface
	 */
	const char *render_DOFs(
			int i_surface_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells, 2, 1, CGridDataArrays_Enums::ALL);


		const char *ret_str = nullptr;
		T *v;
		T *n;
		T t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE1);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[1][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "x-momentum";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE2);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[2][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "y-momentum";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "bathymetry";
				break;


			case 5:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[5][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "benchmark data";
				break;


			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE0 + CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[0][i]+cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "surface height";
				break;
		}

		cCommonShaderPrograms.cHeightColorBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
				cGridDataArrays.triangle_vertex_buffer,
				cParameters.number_of_local_cells*2
			);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;
	}



	void p_render_Wireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
	}

	void render_Wireframe(
			int i_visualization_render_wireframe,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
			p_render_Wireframe(cCommonShaderPrograms.cBlinn);
	}


	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}

	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}



	void p_applyTranslateAndScaleToVertex(
			CHyperbolicTypes::CVisualizationTypes::T *io_vertex
	)
	{
		io_vertex[0] = (io_vertex[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
		io_vertex[1] = (io_vertex[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;
		io_vertex[2] = (io_vertex[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_z;
	}


	void storeDOFsToGridDataArrays(
			CGridDataArrays<2,6> *io_cGridDataArrays,
			int i_flags
	)
	{
		i_flags |=
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::NORMALS;

//		size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(number_of_local_cells);

		CHyperbolicTypes::CVisualizationTypes::T *v = &(io_cGridDataArrays->triangle_vertex_buffer[0]);

		for (int i = 0; i < cParameters.number_of_local_cells; i++)
		{
			CCellData_1D &c = cGrid_1D.getCellData(i);

//			T d;

			if (i_flags & CGridDataArrays_Enums::VALUE0)
				io_cGridDataArrays->dof_element[0][i] = c.dofs.h[0];

			if (i_flags & CGridDataArrays_Enums::VALUE1)
				io_cGridDataArrays->dof_element[1][i] = c.dofs.hu[0];

			if (i_flags & CGridDataArrays_Enums::VALUE2)
				io_cGridDataArrays->dof_element[2][i] = c.dofs.hv[0];

			if (i_flags & CGridDataArrays_Enums::VALUE3)
				io_cGridDataArrays->dof_element[3][i] = c.dofs.b[0];

			if (i_flags & CGridDataArrays_Enums::VALUE5)
			{
				CTsunamiSimulationNodeData benchmark_data;
				cDatasets.getBenchmarkNodalData(c.getPositionX(), 0, 0, &benchmark_data);
//				std::cout << c.getPositionX() << std::endl;
				io_cGridDataArrays->dof_element[5][i] = benchmark_data.h;
			}

			v[0] = c.getPositionX() - c.getCellSizeX()*0.5;
			v[1] = 0;
			v[2] = 0;
			v+=3;

			v[0] = c.getPositionX() + c.getCellSizeX()*0.5;
			v[1] = 0;
			v[2] = 0;
			v+=3;
		}



		/*
		 * postprocessing
		 */
		if (	cParameters.visualization_scale_x != 1.0 ||
				cParameters.visualization_scale_y != 1.0 ||
				cParameters.visualization_scale_z != 1.0 ||

				cParameters.visualization_translate_x != 1.0 ||
				cParameters.visualization_translate_y != 1.0 ||
				cParameters.visualization_translate_z != 1.0
		)
		{
			CHyperbolicTypes::CVisualizationTypes::T *v = &(io_cGridDataArrays->triangle_vertex_buffer[0]);

			for (int i = 0; i < cParameters.number_of_local_cells; i++)
			{
				for (int vn = 0; vn < 2; vn++)
				{
					p_applyTranslateAndScaleToVertex(v);
					v += 3;
				}

				io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;
				io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;
			}
		}
	}

#endif

	/**
	 * setup main threading handler for iTBB & MPI
	 */
	void setupMainThreading(CMainThreading *i_cMainThreading)
	{
	}



	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 0.5;
		cTriangleFactory.vertices[0][1] = -0.5;
		cTriangleFactory.vertices[1][0] = -0.5;
		cTriangleFactory.vertices[1][1] = 0.5;
		cTriangleFactory.vertices[2][0] = -0.5;
		cTriangleFactory.vertices[2][1] = -0.5;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_K;

		cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		for (int i = 0; i < 3; i++)
		{
			cTriangleFactory.vertices[i][0] = (cTriangleFactory.vertices[i][0]*cParameters.simulation_dataset_default_domain_size_x) + cParameters.simulation_dataset_default_domain_translate_x;
			cTriangleFactory.vertices[i][1] = (cTriangleFactory.vertices[i][1]*cParameters.simulation_dataset_default_domain_size_y) + cParameters.simulation_dataset_default_domain_translate_y;
		}
	}

	void setup()
	{
		reset();
	}

	void reset()
	{
		cDatasets.loadDatasets();

		setup_TraversatorsAndKernels();
		p_setup_InitialTriangulation();

		cParameters.reset();

		if (cParameters.simulation_domain_strip_boundary_condition_dataset_right)
		{
			cGrid_1D.setBoundaryConditionsLeftAndRight(
					EBoundaryConditions::BOUNDARY_CONDITION_BOUNCE_BACK,
					EBoundaryConditions::BOUNDARY_CONDITION_DATASET
				);
		}
		else
		{
			cGrid_1D.setBoundaryConditionsLeftAndRight(
					EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW,
					EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW
				);
		}
	}


	void clean()
	{
	}

	void setup_RadialDamBreak(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius
	)
	{
	}


private:
	void p_setup_InitialTriangulation()
	{
		cParameters.number_of_local_cells = 1 << cParameters.grid_initial_recursion_depth;
//		number_of_local_cells = cSetup_Structure_CellData.setup(cStacks, grid_initial_recursion_depth, simulation_parameter_cell_data_setup);
		cParameters.number_of_global_cells = cParameters.number_of_local_cells;

		cParameters.number_of_local_initial_cells_after_domain_triangulation = cParameters.number_of_local_cells;
		cParameters.number_of_global_initial_cells_after_domain_triangulation = cParameters.number_of_local_initial_cells_after_domain_triangulation;

		p_setup_initial_grid_data();
	}


private:
	void p_setup_initial_grid_data()
	{
		cGrid_1D.setup(
				cParameters.number_of_local_cells,			///< i_numberOfCells number of the cells in the initial grid.
				&cDatasets,	///< tsunami simulation scenario
				&cParameters	///< parameters
			);
	}


private:
	void p_simulation_adaptive_traversals()
	{
	}

public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
	}

private:
	/**
	 * timestep edge comm
	 */
	void p_simulation_edge_comm()
	{
//		cGrid1D.printCellValues();

		cParameters.simulation_parameter_global_timestep_size = cGrid_1D.runSingleTimestep(cParameters.simulation_parameter_cfl);
	}


public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_CellDataAt2DPosition(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius = 0.3
	)
	{
	}


#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
public:
	inline void testPinToCore(int pinning_core)
	{
		if (pinning_core == -1)
			return;


		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);

		// pin to first core for initialization to assure placement of allocated data
		if (pinning_core == -2)
			CPU_SET(0, &cpu_set);
		else
			CPU_SET(pinning_core, &cpu_set);

		std::cout << "Pinning application to core " << pinning_core << std::endl;

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		std::cout << std::endl;
		std::cout << "WARNING: Adaptive cluster stacks activated!!!" << std::endl;
		std::cout << std::endl;
#endif

		int err;
		err = pthread_setaffinity_np(
				pthread_self(),
				sizeof(cpu_set_t),
				&cpu_set
			);


		if (err != 0)
		{
			std::cout << err << std::endl;
			perror("pthread_setaffinity_np");
			assert(false);
			exit(-1);
		}

		if (pinning_core >= 0)
			threading_pin_to_core_nr_after_one_timestep = -1;
	}
#endif

public:
	void runSingleTimestep()
	{
		cParameters.simulation_dataset_benchmark_input_timestamp = cParameters.simulation_timestamp_for_timestep;

		p_simulation_edge_comm();
		p_simulation_adaptive_traversals();

		cParameters.simulation_timestep_nr++;
		cParameters.simulation_timestamp_for_timestep += cParameters.simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		cParameters.simulation_dataset_benchmark_input_timestamp = cParameters.simulation_timestamp_for_timestep;

		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		cParameters.simulation_timestep_nr++;
		cParameters.simulation_timestamp_for_timestep += cParameters.simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/
	double getDataSample(
			double i_sample_pos_x,
			double i_sample_pos_y,
			const char *i_sample_information
		)
	{
		return 0;
	}

public:
	void debugOutput(CVector<2,float> &planePosition)
	{
	}



#if CONFIG_SIERPI_ENABLE_GUI

	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	bool gui_key_down_event(
			int key
	)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_RadialDamBreak(cParameters.simulation_dataset_breaking_dam_posx, cParameters.simulation_dataset_breaking_dam_posy, cParameters.simulation_dataset_breaking_dam_radius);
				break;

			case 't':
				cParameters.grid_initial_recursion_depth += 1;
				reset();
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				cParameters.grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			case 'g':
				if (cParameters.grid_initial_recursion_depth > 0)
					cParameters.grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				reset();
				break;

			case 'G':
				if (cParameters.grid_max_relative_recursion_depth > 0)
					cParameters.grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			default:
				return false;
		}

		return true;
	}


	bool gui_mouse_motion_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, cParameters.simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	bool gui_mouse_button_down_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}
#endif

	void exception_output()
	{
		outputVerboseInformation();
	}

	void outputVerboseInformation()
	{
		((CParameters&)(*this)).outputVerboseInformation();
	}

	void output_simulationSpecificData(
			const std::string &i_identifier,
			const std::string &i_parameter
	)	{
		cOutputSimulationSpecificData.output(i_identifier, i_parameter);
//		std::cout << "output_simulationSpecificData() not implemented" << std::endl;
	}

	void debug_OutputCellData(
				T i_position_x,		///< x-coordinate of triangle cell
				T i_position_y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputCellData() not implemented" << std::endl;
	}

	void debug_OutputClusterInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputClusterInformation() not implemented" << std::endl;
	}

	void debug_OutputEdgeCommunicationInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputEdgeCommunicationInformation() not implemented" << std::endl;
	}

	void output_ClusterTreeInformation()
	{
	}

	void action_Validation()
	{

	}

	virtual ~CSimulationTsunami_1d()
	{
		clean();
	}

	void updateClusterScanInformation(
			int use_number_of_threads = -1
	)
	{
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T	*o_origin_x,	///< origin of domain in world-space
			T	*o_origin_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	)
	{
		*o_origin_x = cParameters.simulation_dataset_default_domain_translate_x;
		*o_origin_y = cParameters.simulation_dataset_default_domain_translate_y;

		*o_size_x = cParameters.simulation_dataset_default_domain_size_x;
		*o_size_y = cParameters.simulation_dataset_default_domain_size_y;
	}

};

#endif /* CTSUNAMI_HPP_ */
