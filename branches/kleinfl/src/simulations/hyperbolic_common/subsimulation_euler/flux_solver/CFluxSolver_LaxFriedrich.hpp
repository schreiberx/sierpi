/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_LAXFRIEDRICH_HPP_
#define CFLUXSOLVER_LAXFRIEDRICH_HPP_

#include <cmath>
#include "../CConfig.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

template <typename T>
class CFluxSolver_LaxFriedrich
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	void op_edge_edge(
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_left,		///< output for left flux
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T left_u = i_edgeData_left.ru / i_edgeData_left.r;
		T right_u = i_edgeData_right.ru / i_edgeData_right.r;

#if CONFIG_EULER_GAS_MODEL==0

		T left_p = (i_edgeData_left.e-(T)0.5*i_edgeData_left.ru*left_u)*((T)CONFIG_EULER_GAS_GAMMA-(T)1.0);
		T right_p = (i_edgeData_right.e-(T)0.5*i_edgeData_right.ru*right_u)*((T)CONFIG_EULER_GAS_GAMMA-(T)1.0);

		T left_lambda = std::abs(left_u)+std::sqrt(left_p);
		T right_lambda = std::abs(right_u)+std::sqrt(right_p);

#elif CONFIG_EULER_GAS_MODEL==1

		T left_p = i_edgeData_left.r*(T)CONFIG_EULER_GAS_A_SQUARED;
		T right_p = i_edgeData_right.r*(T)CONFIG_EULER_GAS_A_SQUARED;

		T left_lambda = std::abs(left_u)+std::sqrt(CONFIG_EULER_GAS_A_SQUARED);
		T right_lambda = std::abs(right_u)+std::sqrt(CONFIG_EULER_GAS_A_SQUARED);

#else
#	error "unknown gas model"
#endif

		T lambda = std::max(left_lambda, right_lambda);

		o_edgeFlux_left->r = (T)0.5*(i_edgeData_left.ru + i_edgeData_right.ru);

		o_edgeFlux_left->ru =
				(T)0.5*
				(
					i_edgeData_left.ru*left_u + left_p +
					i_edgeData_right.ru*right_u + right_p
				);

		o_edgeFlux_left->rv = 0;

		o_edgeFlux_left->e =
				(T)0.5*
				(
					(i_edgeData_left.e + left_p)*left_u	+
					(i_edgeData_right.e + right_p)*right_u
				);

		o_edgeFlux_right->r = -o_edgeFlux_left->r;
		o_edgeFlux_right->ru = -o_edgeFlux_left->ru;
		o_edgeFlux_right->rv = 0;
		o_edgeFlux_right->e = -o_edgeFlux_left->e;

		/*
		 * add numerical diffusion for stability reason
		 */
		T diffusion_r = (T)0.5*(T)lambda*(i_edgeData_left.r - i_edgeData_right.r);
		T diffusion_ru = (T)0.5*(T)lambda*(i_edgeData_left.ru - i_edgeData_right.ru);
		T diffusion_e = (T)0.5*(T)lambda*(i_edgeData_left.e - i_edgeData_right.e);

		o_edgeFlux_right->r -= diffusion_r;
		o_edgeFlux_right->ru -= diffusion_ru;
		o_edgeFlux_right->e -= diffusion_e;

		o_edgeFlux_left->r += diffusion_r;
		o_edgeFlux_left->ru += diffusion_ru;
		o_edgeFlux_left->e += diffusion_e;

		/*
		 * CFL condition
		 */
		*o_max_wave_speed_left = lambda;
		*o_max_wave_speed_right = lambda;
	}



	template <int N>
	void op_edge_edge(
			CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			CSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T wave_speed = 0;

// SIMD
#if 0

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			T i_edgeData_right_r = i_edgeData_right.r[N-1-i];
			T i_edgeData_right_ru = -i_edgeData_right.ru[N-1-i];
			T i_edgeData_right_rv = -i_edgeData_right.rv[N-1-i];
			T i_edgeData_right_e = i_edgeData_right.e[N-1-i];

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			T left_u = i_edgeData_left.ru[i] / i_edgeData_left.r[i];
			T right_u = i_edgeData_right_ru / i_edgeData_right_r;

			T left_v = i_edgeData_left.rv[i] / i_edgeData_left.r[i];
			T right_v = i_edgeData_right_rv / i_edgeData_right_r;

			// ideal gas: pressure is directly related to density
			//T left_p = i_edgeData_left.r[i]*0.01;
			//T right_p = i_edgeData_right_r*0.01;

			T left_p = (i_edgeData_left.e[i]-(T)0.5*i_edgeData_left.ru[i]*left_u)*((T)CONFIG_EULER_MONATOMIC_GAS_GAMMA-(T)1);
			T right_p = (i_edgeData_right.e[i]-(T)0.5*i_edgeData_right.ru[i]*right_u)*((T)CONFIG_EULER_MONATOMIC_GAS_GAMMA-(T)1);

			T left_lambda = std::abs(left_u)+std::sqrt(left_p);
			T right_lambda = std::abs(right_u)+std::sqrt(right_p);

			T lambda = std::max(left_lambda, right_lambda);

			o_edgeFlux_left->r[i] = (T)0.5*(i_edgeData_left.ru[i] + i_edgeData_right_ru);

			o_edgeFlux_left->ru[i] =
					(T)0.5*
					(
						i_edgeData_left.ru[i]*left_u + left_p +
						i_edgeData_right_ru*right_u + right_p
					);

			o_edgeFlux_left->rv[i] =
					(T)0.5*
					(
						i_edgeData_left.ru[i]*left_v +
						i_edgeData_right_ru*right_v
					);

			o_edgeFlux_left->e[i] =
					(T)0.5*
					(
						(i_edgeData_left.e[i] + left_p)*left_u	+
						(i_edgeData_right_e + right_p)*right_u
					);

			o_edgeFlux_right->r[N-1-i] = -o_edgeFlux_left->r[i];
			o_edgeFlux_right->ru[N-1-i] = -o_edgeFlux_left->ru[i];
			o_edgeFlux_right->rv[N-1-i] = -o_edgeFlux_left->rv[i];
			o_edgeFlux_right->e[N-1-i] = -o_edgeFlux_left->e[i];

	#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==0
			// remove fluxes for 0th order basis functions

			o_edgeFlux_left->r[i] -= i_edgeData_left.ru[i];
			o_edgeFlux_left->ru[i] -= i_edgeData_left.ru[i]*left_u + left_p;
			o_edgeFlux_left->rv[i] -= i_edgeData_left.ru[i]*left_v;
			o_edgeFlux_left->e[i] -= (i_edgeData_left.e[i] + left_p)*left_u;

			o_edgeFlux_right->r[N-1-i] += i_edgeData_right_ru;
			o_edgeFlux_right->ru[N-1-i] += i_edgeData_right_ru*right_u + right_p;
			o_edgeFlux_right->rv[N-1-i] += i_edgeData_right_ru*right_v;
			o_edgeFlux_right->e[N-1-i] += (i_edgeData_right_e + right_p)*right_u;
	#endif

			/*
			 * add numerical diffusion for stability reason
			 */
			T diffusion_r = (T)0.5*(T)lambda*(i_edgeData_left.r[i] - i_edgeData_right_r);
			T diffusion_ru = (T)0.5*(T)lambda*(i_edgeData_left.ru[i] - i_edgeData_right_ru);
			T diffusion_rv = (T)0.5*(T)lambda*(i_edgeData_left.rv[i] - i_edgeData_right_rv);
			T diffusion_e = (T)0.5*(T)lambda*(i_edgeData_left.e[i] - i_edgeData_right_e);

			o_edgeFlux_right->r[N-1-i] -= diffusion_r;
			o_edgeFlux_right->ru[N-1-i] -= diffusion_ru;
			o_edgeFlux_right->rv[N-1-i] -= diffusion_rv;
			o_edgeFlux_right->e[N-1-i] -= diffusion_e;

			o_edgeFlux_left->r[i] += diffusion_r;
			o_edgeFlux_left->ru[i] += diffusion_ru;
			o_edgeFlux_left->rv[i] += diffusion_rv;
			o_edgeFlux_left->e[i] += diffusion_e;

			o_edgeFlux_right->ru[N-1-i] = -o_edgeFlux_right->ru[N-1-i];
			o_edgeFlux_right->rv[N-1-i] = -o_edgeFlux_right->rv[N-1-i];

			/*
			 * CFL condition
			 */
			*o_max_wave_speed_left = lambda;
			*o_max_wave_speed_right = lambda;

			wave_speed = std::max(wave_speed, lambda);
		}

#else

		for (int i = 0; i < N; i++)
		{
			T l_wave_speed_left = 0;
			T l_wave_speed_right = 0;

			CSimulationNodeData i_left, i_right;

			i_left.r = i_edgeData_left.r[i];
			i_left.ru = i_edgeData_left.ru[i];
			i_left.rv = i_edgeData_left.rv[i];
			i_left.e = i_edgeData_left.e[i];

			i_right.r = i_edgeData_right.r[N-1-i];
			i_right.ru = -i_edgeData_right.ru[N-1-i];
			i_right.rv = -i_edgeData_right.rv[N-1-i];
			i_right.e = i_edgeData_right.e[N-1-i];

			CSimulationNodeData o_left, o_right;

			op_edge_edge(
					i_left,					///< edge data on left (left) edge
					i_right,				///< edge data on right (outer) edge

					&o_left,				///< output for left flux
					&o_right,				///< output for outer flux

					&l_wave_speed_left,		///< maximum wave speed
					&l_wave_speed_right,	///< maximum wave speed

					i_gravitational_constant				///< gravitational constant
			);

			o_edgeFlux_left->r[i] = o_left.r;
			o_edgeFlux_left->ru[i] = o_left.ru;
			o_edgeFlux_left->rv[i] = o_left.rv;
			o_edgeFlux_left->e[i] = o_left.e;

			o_edgeFlux_right->r[N-1-i] = o_right.r;
			o_edgeFlux_right->ru[N-1-i] = -o_right.ru;
			o_edgeFlux_right->rv[N-1-i] = -o_right.rv;
			o_edgeFlux_right->e[N-1-i] = o_right.e;

			wave_speed = std::max(wave_speed, l_wave_speed_left);
			wave_speed = std::max(wave_speed, l_wave_speed_right);
		}
#endif


		*o_max_wave_speed_left = wave_speed;
		*o_max_wave_speed_right = wave_speed;
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
