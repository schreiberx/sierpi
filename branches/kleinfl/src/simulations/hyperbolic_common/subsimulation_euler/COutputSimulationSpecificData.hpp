/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: September 10, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTSIMULATIONSPECIFICDATA_HPP_
#define COUTPUTSIMULATIONSPECIFICDATA_HPP_


#include "../../hyperbolic_parallel/CSimulationHyperbolic_Cluster.hpp"
#include "../../../libsierpi/cluster/CDomainClusters.hpp"
#include "../../../libsierpi/parallelization/CGlobalComm.hpp"
#include "../subsimulation_generic/CConfig.hpp"
#include "../CParameters.hpp"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData_Depth.hpp"
#include "mainthreading/CMainThreading_Locks.hpp"

class COutputSimulationSpecificData
{
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;
	typedef CHyperbolicTypes::CSimulationTypes::T	T;

	CParameters &cParameters;
	CDatasets &cDatasets;
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;

	bool setup_done;


	class CDartSamplings
	{
public:
		T position_x;
		T position_y;
		std::string output_file;
	};


	CDartSamplings *sampling_points;
	int dart_samplings_size;

	std::ofstream dartfile_stream;

	const char *output_file_delimiter;



	/**
	 * setup dart sampling points
	 *
	 * Format:
	 *
	 * [double]/[double]/[outputfile],[double]/[double]/[outputfile],[double]/[double]/[outputfile],...
	 *
	 * [posx]/[posy]/[outputfile]
	 */
	bool setupDartSamplingPoints(
			const std::string &i_dart_sampling_points_string
	)
	{
		dart_samplings_size = 1;
		for (unsigned int i = 0; i < i_dart_sampling_points_string.size(); i++)
			if (i_dart_sampling_points_string[i] == ',')
				dart_samplings_size++;

		size_t delimiter_pos;
		size_t old_pos = 0;

		if (sampling_points != nullptr)
			delete [] sampling_points;

		sampling_points = new CDartSamplings[dart_samplings_size];

		for (int i = 0; i < dart_samplings_size; i++)
		{
			// search for next delimiter
			delimiter_pos = i_dart_sampling_points_string.find(',', old_pos);

			std::string sampling_point_string;

			// if not found, we reached the end of the point array
			if (delimiter_pos == std::string::npos)
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos);
			else
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos, delimiter_pos-old_pos);

			old_pos = delimiter_pos+1;


			/*
			 * split single point
			 */
			size_t pos = sampling_point_string.find('/');
			if (pos == std::string::npos)
			{
				std::cout << "Invalid format for -D " << std::endl;
				std::cout << "   use e. g. -D 834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt " << std::endl;
				exit(-1);
			}
			std::string l = sampling_point_string.substr(0, pos);
			std::string r = sampling_point_string.substr(pos+1);


			/*
			 * search for appended filename
			 */
			pos = r.find('/', 1);
			if (pos != std::string::npos)
			{
				sampling_points[i].output_file = r.substr(pos+1);
				r = r.substr(0, pos);
			}

			sampling_points[i].position_x = std::atof(l.c_str());
			sampling_points[i].position_y = std::atof(r.c_str());
		}

		return true;
	}





public:
	COutputSimulationSpecificData(
			CParameters &i_cParameters,
			CDatasets &i_cDatasets,
			sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &i_cDomainClusters

	)	:
		cParameters(i_cParameters),
		cDatasets(i_cDatasets),
		cDomainClusters(i_cDomainClusters),
		setup_done(false),
		sampling_points(nullptr),
		dart_samplings_size(0)
	{
//		output_file_delimiter = "\t;";
		output_file_delimiter = ",";
	}



public:
	virtual ~COutputSimulationSpecificData()
	{
		if (sampling_points)
		{
			delete []sampling_points;
			sampling_points = nullptr;
		}
	}


	/**
	 * \brief return a data sample at the given position
	 *
	 * this is useful e. g. to get bouy data
	 */
	void p_getDataSample(
			T i_sample_pos_x,
			T i_sample_pos_y,
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData

#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			,
			T *o_max_eigen_coefficient
#endif
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_sample_pos_x,
							i_sample_pos_y
						))
							return;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CGetNodeDataSample<CHyperbolicTypes>::TRAV cGetNodeDataSample;

					cGetNodeDataSample.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cGetNodeDataSample.cKernelClass.setup(
							i_sample_pos_x,
							i_sample_pos_y,
							o_cNodeData

#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
							,
							o_max_eigen_coefficient
#endif
						);

					cGetNodeDataSample.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
	}


	void setup(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		if (i_identifier == "dart")
		{
			if (!setupDartSamplingPoints(i_parameters))
			{
				throw(std::runtime_error("wrong format for dart sampling points"));
			}
		}
	}



	void outputDartStationData(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{

#if CONFIG_ENABLE_MPI
		int mpi_rank = sierpi::CGlobalComm::getCommRank();

		if (mpi_rank == 0)
#endif
		{
			if (cParameters.verbosity_level > 4)
				std::cout << "Writing sampling point data" << std::endl;
		}

		for (int i = 0; i < dart_samplings_size; i++)
		{
			/*
			 * SIMULATION DATA
			 */
			CHyperbolicTypes::CSimulationTypes::CNodeData cSimulationNodeData;

			cSimulationNodeData.r = std::numeric_limits<T>::infinity();
			cSimulationNodeData.ru = std::numeric_limits<T>::infinity();
			cSimulationNodeData.rv = std::numeric_limits<T>::infinity();
			cSimulationNodeData.e = std::numeric_limits<T>::infinity();

#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			T max_eigen_coefficient = std::numeric_limits<T>::infinity();
#endif
			p_getDataSample(
					sampling_points[i].position_x, sampling_points[i].position_y,
					&cSimulationNodeData
#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
					,
					&max_eigen_coefficient
#endif
			);

#if CONFIG_ENABLE_MPI

			// set reduce rank to -1 if no valid data is available
			int reduce_rank = (std::isinf(cSimulationNodeData.h) ? -1 : mpi_rank);

			int sampling_rank = sierpi::CGlobalComm::reduceMax(reduce_rank);

			if (sampling_rank == -1)
			{
				if (mpi_rank == 0)
					std::cout << "Warning: No valid sampling data created for sampling point (" << sampling_points[i].position_x << ", " << sampling_points[i].position_y << ")" << std::endl;
			}
			else if (mpi_rank == 0)	// root node?
			{
				if (sampling_rank != 0)
				{
					// receive MPI message
					MPI_Recv(&cSimulationNodeData, sizeof(CHyperbolicTypes::CSimulationTypes::CNodeData), MPI_BYTE, sampling_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				}
			}
			else if (sampling_rank == mpi_rank)
			{
				// we have to send the data
				MPI_Request mpi_request;
				MPI_Isend(&cSimulationNodeData, sizeof(CHyperbolicTypes::CSimulationTypes::CNodeData), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &mpi_request);
				MPI_Request_free(&mpi_request);
			}

			// only RANK 0 should output data
			if (mpi_rank == 0)
#endif
			{
				/*
				 * preprocess simulation node data
				 */
				std::vector<T> simulationData;
				cSimulationNodeData.getSimulationSpecificBenchmarkData(&simulationData);


				/********************
				 * OUTPUT
				 */
				std::stringstream specificDataStreamBuf;

				/*
				 * header information
				 */
				if (cParameters.simulation_timestamp_for_timestep == 0)
				{
					specificDataStreamBuf << "time" << output_file_delimiter << "surface_elevation" << output_file_delimiter;

	#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
					specificDataStreamBuf << "max_eigen_coefficient" << output_file_delimiter;
	#endif

					specificDataStreamBuf << "reference_data" << output_file_delimiter << "number_of_local_cells" << output_file_delimiter << "number_of_global_cells" << std::endl;
				}

				/*
				 * OUTPUT: timestamp
				 */
				specificDataStreamBuf << cParameters.simulation_timestamp_for_timestep << output_file_delimiter;


				/*
				 * OUTPUT: simulation nodal data
				 */
				for (size_t j = 0; j < simulationData.size(); j++)
					specificDataStreamBuf << simulationData[j] << output_file_delimiter;

	#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
				/*
				 * OUTPUT: Max Eigen Coefficient
				 */
				specificDataStreamBuf << max_eigen_coefficient << output_file_delimiter;
	#endif


				/*
				 * OUTPUT: REFERENCE DATA (if available)
				 */
				CHyperbolicTypes::CSimulationTypes::CNodeData cReferenceNodeData;

				if (	cDatasets.getBenchmarkNodalData(
							sampling_points[i].position_x,
							sampling_points[i].position_y,
							0,
							cParameters.simulation_timestamp_for_timestep,
							&cReferenceNodeData
					)
				)	{
					std::vector<T> referenceData;
					cReferenceNodeData.getSimulationSpecificBenchmarkData(&referenceData);

					for (size_t j = 0; j < referenceData.size(); j++)
						specificDataStreamBuf << referenceData[j] << output_file_delimiter;
				}
				else
				{
					specificDataStreamBuf << 0 << output_file_delimiter;
				}

				/*
				 * OUTPUT: number of local/global cells
				 */
				specificDataStreamBuf << cParameters.number_of_local_cells << output_file_delimiter;
				specificDataStreamBuf << cParameters.number_of_global_cells;	// put no output file delimiter here

				if (sampling_points[i].output_file.empty())
				{
					std::cout << specificDataStreamBuf.str() << std::endl;
				}
				else
				{
					std::ofstream s;

					if (cParameters.simulation_timestep_nr == 0)
						s.open(sampling_points[i].output_file.c_str());
					else
						s.open(sampling_points[i].output_file.c_str(), std::ofstream::app);

					s << specificDataStreamBuf.str() << std::endl;
				}
			}
		}
	}


	/**
	 * storage class for final benchmark value
	 */
	class CBenchmarkValue
	{
		CMainThreading_Lock lock;

public:
		T v0x, v0y;
		T v1x, v1y;
		T v2x, v2y;

		T timestamp;

		int depth;

		T max_horizon_value;

		CBenchmarkValue()	:
			max_horizon_value(-std::numeric_limits<T>::infinity())
		{
		}


		/**
		 * test and update maximum horizon water height
		 */
		inline void update(
				T i_v0x, T i_v0y,
				T i_v1x, T i_v1y,
				T i_v2x, T i_v2y,
				int i_depth,
				T i_max_horizon_value
			)
		{
			// TODO/INFO: this is expected to create a bunch of false sharing conflicts

			if (i_max_horizon_value <= max_horizon_value)
				return;

			lock.lock();

				if (i_max_horizon_value > max_horizon_value)
				{
					v0x = i_v0x;	v0y = i_v0y;
					v1x = i_v1x;	v1y = i_v1y;
					v2x = i_v2x;	v2y = i_v2y;

					depth = i_depth;

					max_horizon_value = i_max_horizon_value;
				}

			lock.unlock();
		}
	};


	// maximum benchmark value
	CBenchmarkValue cBenchmarkValue_max;


	void getMaxBenchmarkValue(
			CBenchmarkValue *io_cBenchmarkValue
	)	{

		/**
		 * SWOSB benchmark traversator and kernel to store the maximum water height and position
		 */
		class CSWOSBTraversatorAndKernel
		{
		public:
			typedef typename CHyperbolicTypes::CSimulationTypes::CCellData	CCellData_;
			typedef CHyperbolicTypes::CSimulationTypes::T T;

		private:
			typedef CVertex2d<T> TVertex;

		public:
			typedef sierpi::travs::CTraversator_VertexCoords_CellData_Depth<CSWOSBTraversatorAndKernel, CHyperbolicTypes> TRAV;

			CBenchmarkValue *cBenchmarkValue;

			CSWOSBTraversatorAndKernel()	:
				cBenchmarkValue(nullptr)
			{
			}



			inline void op_cell(
				T i_v0x, T i_v0y,
				T i_v1x, T i_v1y,
				T i_v2x, T i_v2y,

				int i_depth,

				const CCellData_ *i_cCellData
			)
			{
				assert(cBenchmarkValue != nullptr);

				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
				{
					T r = i_cCellData->dofs.r[i];
					cBenchmarkValue->update(i_v0x, i_v0y, i_v1x, i_v1y, i_v2x, i_v2y, i_depth, r);
				}
			}


			void traversal_pre_hook()
			{
			}


			void traversal_post_hook()
			{
			}


			inline void setup(CBenchmarkValue *i_cBenchmarkValue)
			{
				cBenchmarkValue = i_cBenchmarkValue;
			}
		};

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					/// output element data at given point
					CSWOSBTraversatorAndKernel::TRAV cSWOSBTraversatorAndKernel;

					cSWOSBTraversatorAndKernel.setup_sfcMethods(node->cTriangleFactory);
					cSWOSBTraversatorAndKernel.cKernelClass.setup(io_cBenchmarkValue);
					cSWOSBTraversatorAndKernel.action(node->cStacks);
				}
		);

		io_cBenchmarkValue->timestamp = cParameters.simulation_timestamp_for_timestep;
	}



	void outputSWOSBDataToFile(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		CBenchmarkValue cBenchmarkValue;

		getMaxBenchmarkValue(&cBenchmarkValue);

		std::ofstream s;
		if (cParameters.simulation_timestep_nr == 0)
			s.open(i_parameters.c_str());
		else
			s.open(i_parameters.c_str(), std::ofstream::app);

		T px = (cBenchmarkValue.v0x + cBenchmarkValue.v1x + cBenchmarkValue.v2x) * (1.0/3.0);
		T py = (cBenchmarkValue.v0y + cBenchmarkValue.v1y + cBenchmarkValue.v2y) * (1.0/3.0);

		if (cBenchmarkValue.max_horizon_value > cBenchmarkValue_max.max_horizon_value)
			cBenchmarkValue_max = cBenchmarkValue;

		s << cParameters.simulation_timestamp_for_timestep << output_file_delimiter << cBenchmarkValue.max_horizon_value << output_file_delimiter << px << output_file_delimiter << py << std::endl;
	}



	void outputConsoleMaxSWOSBData(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		CBenchmarkValue cBenchmarkValue;

		getMaxBenchmarkValue(&cBenchmarkValue);

		if (cBenchmarkValue.max_horizon_value > cBenchmarkValue_max.max_horizon_value)
			cBenchmarkValue_max = cBenchmarkValue;

		T px = (cBenchmarkValue_max.v0x + cBenchmarkValue_max.v1x + cBenchmarkValue_max.v2x) * (1.0/3.0);
		T py = (cBenchmarkValue_max.v0y + cBenchmarkValue_max.v1y + cBenchmarkValue_max.v2y) * (1.0/3.0);

		std::cout << "MaxRunup:\t" << cBenchmarkValue_max.timestamp << "\t" << cBenchmarkValue_max.max_horizon_value << "\t" << px << "\t" << py << "\t" << cBenchmarkValue_max.depth << std::endl;
	}



	void output(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		if (i_identifier.size() == 0)
			return;

		if (!setup_done)
		{
			setup(i_identifier, i_parameters);
			setup_done = true;
		}

		if (i_identifier == "dart")
		{
			outputDartStationData(i_identifier, i_parameters);
			return;
		}

		if (i_identifier == "swosb")
		{
			outputSWOSBDataToFile(i_identifier, i_parameters);
			return;
		}

		if (i_identifier == "swosb_max_to_cout")
		{
			outputConsoleMaxSWOSBData(i_identifier, i_parameters);
			return;
		}
	}
};


#endif /* COUTPUTSIMULATIONSPECIFICDATA_HPP_ */
