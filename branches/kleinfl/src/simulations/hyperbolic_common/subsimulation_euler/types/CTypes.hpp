/*
 * CTypes.hpp
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEULER_MULTILAYERTYPES_HPP_
#define CEULER_MULTILAYERTYPES_HPP_

#include "libsierpi/parallelization/CGlobalComm.hpp"
#include <vector>
#include <iostream>
#include <limits>

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"

#include "../../subsimulation_generic/types/CValidation_EdgeData.hpp"
#include "../../subsimulation_generic/types/CValidation_CellData.hpp"
#include "../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"
#include "../libsierpi/grid/CCube_To_Sphere_Projection.hpp"

/**
 * Degree of freedoms for one point in simulation
 */
class CSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T r;	// density
	T ru;	// x-component of momentum
	T rv;	// y-component of momentum

// no union since energy is involved in update!
//	union	{
		T e;					// energy
		T cfl1_timestep_size;	// helper variable to determine the maximum allowed timestep size
//	};


	/**
	 * return simulation specific benchmark data
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = r;
	}

	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeData &d
	 )	{
		os << "CSimulationNodeData:" << std::endl;
		os << " + r: " << d.r << std::endl;
		os << " + ru: " << d.ru << std::endl;
		os << " + rv: " << d.rv << std::endl;
		os << " + e: " << d.e << std::endl;
		os << " + cfl1_timestep_size: " << d.cfl1_timestep_size << std::endl;
		os << " + vx: " << d.ru/d.r << std::endl;
		os << " + vy: " << d.rv/d.r << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,		///< cout
			T x_axis_x,				///< x-axis of new basis (x-component)
			T x_axis_y				///< x-axis of new basis (y-component)
	) const
	{
		os << "Euler DOF Information:" << std::endl;
		os << " + r: " << r << std::endl;

		T tru = ru;
		T trv = rv;
		CTriangle_VectorProjections::referenceToWorld(&tru, &trv, x_axis_x, x_axis_y);

		os << " + ru: " << tru << std::endl;
		os << " + rv: " << trv << std::endl;

		os << " + e: " << e << std::endl;
		os << " + cfl1_timestep_size: " << cfl1_timestep_size << std::endl;

		os << " + vx: " << tru/r << std::endl;
		os << " + vy: " << trv/r << std::endl;

		os << std::endl;
	}
};





/**
 * Degree of freedoms for one point in simulation
 *
 * TODO [schreibm]
 */
template <int N>
class CSimulationNodeDataSOA
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T r[N];	// height
	T ru[N];	// x-component of momentum
	T rv[N];	// y-component of momentum

//	union	{
		T e[N];	// bathymetry
		T cfl1_timestep_size[N];	// max wave speed for flux computations
//	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = r[0];
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultCellValues(T default_values[4])
	{

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3

		for (int i = 0; i < N; i++)
		{
			r[i] = default_values[0];
			ru[i] = default_values[1];
			rv[i] = default_values[2];
			e[i] = default_values[3];
		}

#else

		r[0] = default_values[0];
		ru[0] = default_values[1];
		rv[0] = default_values[2];
		e[0] = default_values[3];

		for (int i = 1; i < N; i++)
		{
			r[i] = 0;
			ru[i] = 0;
			rv[i] = 0;
			e[i] = 0;
		}
#endif
	}

	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultEdgeValues(T default_values[4])
	{
		for (int i = 0; i < N; i++)
		{
			r[i] = default_values[0];
			ru[i] = default_values[1];
			rv[i] = default_values[2];
			e[i] = default_values[3];
		}
	}

	static inline void outputVector(
			std::ostream &os,					///< output stream
			const T *v,
			int i_size
	)
	{
		os << "[";
		for (int i = 0; i < i_size-1; i++)
			os << " " << v[i];
		os << " " << v[i_size-1] << " ]" << std::endl;
	}



	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeDataSOA &d
	 )	{
		os << "CSimulationNodeDataSOA:" << std::endl;
		os << " + r:";
		outputVector(os, d.r, N);
		os << " + ru:";
		outputVector(os, d.ru, N);
		os << " + rv:";
		outputVector(os, d.rv, N);

		os << " + e/max_wave_speed:";
		outputVector(os, d.e, N);

		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		os << "CSimulationNodeDataSOA:" << std::endl;
		os << " + r:";
		outputVector(os, r, N);

		T wru[N];
		T wrv[N];

		for (int i = 0; i < N; i++)
		{
			wru[i] = ru[i];
			wrv[i] = rv[i];
			CTriangle_VectorProjections::referenceToWorld(&wru[i], &wrv[i], x_axis_x, x_axis_y);
		}

		os << " + wru:";
		outputVector(os, wru, N);
		os << " + wrv:";
		outputVector(os, wrv, N);

		os << " + e/max_wave_speed:";
		outputVector(os, e, N);
	}
};


/**
 * Edge communication data during simulation
 */
class CSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS> dofs;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

#if CONFIG_EULER_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
	T max_eigen_coefficient;
#endif

	T CFL1_scalar;

	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[])
	{
		dofs.setupDefaultEdgeValues(i_default_values);
	}


	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationEdgeData &d
	 )	{
		os << "Edge Data [r, ru, rv, e, cfl1_timestep_size, vx, vy]" << std::endl;
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			std::cout << " + " << i << ": [" <<
					d.dofs.r[i] << ", " <<
					d.dofs.ru[i] << ", " <<
					d.dofs.rv[i] << ", " <<
					d.dofs.e[i] << ", " <<
					d.dofs.cfl1_timestep_size[i] << ", " <<
					d.dofs.ru[i]/d.dofs.r[i] << ", " <<
					d.dofs.rv[i]/d.dofs.r[i] << ", " << std::endl;
		}
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};




/**
 * cell data
 *
 * this data structure contains all explicitly needed information for cell data
 */
class CSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * weights for basis functions
	 */
	CSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS> dofs;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
	/**
	 * area of cell as projected on sphere
	 */
	T cellArea;


	/**
	 * cube face of the edge
	 */
	CCube_To_Sphere_Projection::EFaceType face;

	/**
	 * x,y-Coordinates of the 3 vertices on the cube face
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T vertices[3][2];

	/**
	 * x,y,z-Coordinates of the 3 vertices projected on the sphere
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T coordinates3D[3][3];

	/**
	 * edge lengths on sphere
	 *
	 *
	 * 	|\
	 * 1|  \0
	 * 	|    \
	 * 	-------
	 * 	   2
	 *
	 */
	T edgeLength[3];

	/**
	 * radius of the inscribed circle
	 *
	 * |\
	 * |  \
	 * |    \
	 * |      \
	 * |___.    \
	 * |   |      \
	 * |___|________\
	 *
	 */
	T incircleRadius;

	/*
	 * rotation matrix from normal to edge space for left edge
	 */
	T leftEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for right edge
	 */
	T rightEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for hyp edge
	 */
	T hypEdgeRotationMatrix[2][2];

	/**
	 * transformation matrix for shear and scaling
	 *
	 *  _                               |\_
	 *  \ - _                           |  \_     _
	 *   \    - _                       |    \_ -|2'
	 *    \       - _          --->    1|      \_
	 *     \__________-_                |        \_
	 *                                  |__________\
	 *                                       1
	 */
	T transformationMatrix[2][2];

	/**
	 * inverse transformation matrix for shear and scaling
	 *
	 *      |\                    _
	 *      |  \     _            \ - _
	 *      |    \ -|2'            \    - _
	 *     1|      \      --->      \       - _
	 *      |        \               \__________-_
	 *      |__________\
	 *                                       1
	 */
	T inverseTransformationMatrix[2][2];


	void setupDistortedGrid(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y		///< normals for left edge (necessary for back-rotation of element)
	) {
		T i_x = (i_vertex_left_x + i_vertex_right_x + i_vertex_top_x)*(T)(1.0/3.0);
		T i_y = (i_vertex_left_y + i_vertex_right_y + i_vertex_top_y)*(T)(1.0/3.0);

		// get cube face
		face = CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(i_x, i_y);

		// project to (-1,1)^2
		CCube_To_Sphere_Projection::project2DTo2DFace(
				face,
				&i_vertex_left_x,	&i_vertex_left_y,
				&i_vertex_right_x,	&i_vertex_right_y,
				&i_vertex_top_x,	&i_vertex_top_y
			);

		vertices[0][0] = i_vertex_left_x;
		vertices[0][1] = i_vertex_left_y;
		vertices[1][0] = i_vertex_right_x;
		vertices[1][1] = i_vertex_right_y;
		vertices[2][0] = i_vertex_top_x;
		vertices[2][1] = i_vertex_top_y;


		// set 3D coordinates on sphere
		for(int i = 0; i < 3; i++)
		{
			CCube_To_Sphere_Projection::project2DFaceTo3D(
					face,
					vertices[i][0],
					vertices[i][1],
					&coordinates3D[i][0],
					&coordinates3D[i][1],
					&coordinates3D[i][2]
			);
		}


		// set correct radius
		T radius = EARTH_RADIUS_IN_METERS;


		for(int i = 0; i < 3; i++)
		{
			// set edge length as on sphere
			edgeLength[i] = CTriangle_VectorProjections::getEdgeLengthOnSphere(
					vertices[i][0],
					vertices[i][1],
					vertices[(i+1)%3][0],
					vertices[(i+1)%3][1],
					radius
			);
		}

		// set rotation matrices for all edges
		CTriangle_VectorProjections::setHypEdgeRotationMatrix(
				hypEdgeRotationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		// always aligned at x-axis
		CTriangle_VectorProjections::setLeftEdgeRotationMatrix(
				leftEdgeRotationMatrix
			);

		CTriangle_VectorProjections::setRightEdgeRotationMatrix(
				rightEdgeRotationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		CTriangle_VectorProjections::setTransformationMatrices(
				transformationMatrix,
				inverseTransformationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		// approximation of the triangle area as half of the product of the two cathetus lengths
//		cellArea = (edgeLength[1] * edgeLength[2] ) / (T)2.0;

		cellArea = CTriangle_VectorProjections::getCellAreaOnSphere(
				edgeLength[0],
				edgeLength[1],
				edgeLength[2],
				radius
			);

		// set inscribed circle radius
		incircleRadius = CTriangle_VectorProjections::getIncircleRadius(
				edgeLength[0],
				edgeLength[1],
				edgeLength[2],
				radius
			);
	}

#endif


#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		T max_eigen_coefficient;
#endif



#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
	bool refine;
	bool coarsen;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(T default_values[])
	{
		dofs.setupDefaultCellValues(default_values);
	}



	friend
	::std::ostream&
	 operator<<	(
			 ::std::ostream& os,
			  const CSimulationCellData &d
	 )	{
		d.outputVerboseData(os, 1, 0);
		return os;
	}


	void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	)	const
	{
		dofs.outputVerboseData(os, x_axis_x, x_axis_y);

#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		os << "max_eigen_coefficient: " << max_eigen_coefficient << std::endl;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << validation << std::endl;
#endif
	}
};

#include "../../subsimulation_generic/types/CVisualizationVertexData.hpp"



/**
 * simulation types which
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CSimulationEdgeData CEdgeData;
	};



	/**
	 * This class implements all types which are used for visualization
	 */
	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};


	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;
};




#endif /* CEULER_MULTILAYERTYPES_HPP_ */
