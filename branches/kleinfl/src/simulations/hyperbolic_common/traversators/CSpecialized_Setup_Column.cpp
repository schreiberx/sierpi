/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "CSpecialized_Setup_Column.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_Column.hpp"
#include "../subsimulation_generic/CDatasets.hpp"

namespace sierpi
{
namespace travs
{
class CSpecialized_Setup_Column_Private	:
	public CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<
		sierpi::kernels::CSetup_Column<CHyperbolicTypes::CSimulationTypes::CCellData, CHyperbolicTypes::CSimulationTypes>,		// setup of column
		CHyperbolicTypes					// tsunami element data
	>
{
};


CSpecialized_Setup_Column::CSpecialized_Setup_Column()	:
		repeat_traversal(false)
{
	generic_traversator = new CSpecialized_Setup_Column_Private;
}


CSpecialized_Setup_Column::~CSpecialized_Setup_Column()
{
	delete generic_traversator;
}



#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

void CSpecialized_Setup_Column::migration_send(int i_dst_rank)
{
	generic_traversator->migration_send(i_dst_rank);
}

void CSpecialized_Setup_Column::migration_send_postprocessing(int i_dst_rank)
{
	generic_traversator->migration_send_postprocessing(i_dst_rank);
}

void CSpecialized_Setup_Column::migration_recv(int i_src_rank)
{
	generic_traversator->migration_recv(i_src_rank);
}

#endif



void CSpecialized_Setup_Column::setup_TraversatorParameters(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_TraversatorParameters(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}



void CSpecialized_Setup_Column::setup_KernelClass(
		T p_x,
		T p_y,
		T p_radius,

		int i_methodId,
		CDatasets *i_cDatasets
)
{
	generic_traversator->cKernelClass.setup_Parameters(
			p_x,
			p_y,
			p_radius,
			i_methodId,
			i_cDatasets
		);
}


bool CSpecialized_Setup_Column::actionFirstTraversal(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->firstTraversal.action(cStacks);
	return repeat_traversal;
}

bool CSpecialized_Setup_Column::actionMiddleTraversals_Parallel(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Parallel(cStacks, repeat_traversal);
	return repeat_traversal;
}



bool CSpecialized_Setup_Column::actionMiddleTraversals_Serial(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Serial(cStacks, repeat_traversal);
	return repeat_traversal;
}


void CSpecialized_Setup_Column::actionLastTraversal_Parallel(
		CHyperbolicTypes::CSimulationStacks *cStacks,
		CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_Setup_Column::actionLastTraversal_Serial(
		CHyperbolicTypes::CSimulationStacks *cStacks
)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_Setup_Column::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_RootCluster(p_triangleFactory);
}


/**
 * setup the initial cluster traversal for the given factory
 */
void CSpecialized_Setup_Column::setup_Cluster(
		CSpecialized_Setup_Column &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Cluster(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->cKernelClass.setup_WithKernel(
			parent.generic_traversator->cKernelClass
		);
}



}
}
