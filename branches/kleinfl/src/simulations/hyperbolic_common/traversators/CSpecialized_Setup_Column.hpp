/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_


#include "../../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/stacks/CFBStacks.hpp"
#include "libsierpi/cluster/CCluster_SplitJoinInformation.hpp"
#include "../subsimulation_generic/CDatasets.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Setup_Column
{
private:
	class CSpecialized_Setup_Column_Private *generic_traversator;

	bool repeat_traversal;

	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:
	CSpecialized_Setup_Column();

	virtual ~CSpecialized_Setup_Column();

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	void migration_send(int i_dst_rank);
	void migration_send_postprocessing(int i_dst_rank);
	void migration_recv(int i_src_rank);
#endif

	void setSetupMethod(int i);

	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CHyperbolicTypes::CSimulationStacks *cStacks);


	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CHyperbolicTypes::CSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CHyperbolicTypes::CSimulationStacks *cStacks);


	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CHyperbolicTypes::CSimulationStacks *cStacks);


	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CHyperbolicTypes::CSimulationStacks *cStacks,
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	);


	/**
	 * setup the initial root domain cluster
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);


	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_TraversatorParameters(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);


	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			T p_x,
			T p_y,
			T p_radius,

			int i_methodId,
			CDatasets *i_cDatasets
	);


	/**
	 * setup the child cluster based on it's parent traversator information and the child's factory
	 */
	void setup_Cluster(
			CSpecialized_Setup_Column &parent,
			CTriangle_Factory &i_triangleFactory
		);
};

}
}

#endif
