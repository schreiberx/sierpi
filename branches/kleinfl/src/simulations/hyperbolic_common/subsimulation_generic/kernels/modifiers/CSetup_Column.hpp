/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUP_COLUMN_HPP_
#define CSETUP_COLUMN_HPP_


#if CONFIG_SUB_SIMULATION_TSUNAMI

#	include "simulations/hyperbolic_common/subsimulation_tsunami/kernels/modifiers/CSetup_Column.hpp"

#elif CONFIG_SUB_SIMULATION_EULER

#	include "simulations/hyperbolic_common/subsimulation_euler/kernels/modifiers/CSetup_Column.hpp"

#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER

#	include "simulations/hyperbolic_common/subsimulation_euler_multilayer/kernels/modifiers/CSetup_Column.hpp"

#elif CONFIG_SUB_SIMULATION_BENCHMARK_SPHERE

#	include "simulations/hyperbolic_common/subsimulation_benchmark_sphere/kernels/modifiers/CSetup_Column.hpp"

#else
#	error "unknown sub-simulation"
#endif


#endif /* CSETUP_COLUMN_HPP_ */
