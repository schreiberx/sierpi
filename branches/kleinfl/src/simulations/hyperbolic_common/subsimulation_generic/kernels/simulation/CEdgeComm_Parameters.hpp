/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Jan 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_PARAMETERS_HPP_
#define CEDGECOMM_TSUNAMI_PARAMETERS_HPP_

#include <limits>

#include "libsierpi/grid/CBoundaryConditions.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "libsierpi/parallelization/CMigration.hpp"

/**
 * common parameters used for edge comm based hyperbolic simulation
 */
class CEdgeComm_Parameters	:
		public sierpi::CMigration_RawClass
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T timestep_size;

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
	T cathetus_real_length;
#endif

#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
	T domain_real_height;
	T cell_real_height;
#endif

	/// gravitational constant
	T gravitational_constant;

	/// pointer to dataset handlers
	CDatasets *cDatasets;

	/// dirichlet values for edge data
	CHyperbolicTypes::CSimulationTypes::CEdgeData boundary_dirichlet;

#if SIMULATION_MULTILAYER_ENABLED
	/// dirichlet values for top-most / bottom-most faces
	CHyperbolicTypes::CSimulationTypes::CVerticalFaceData boundary_dirichlet_vertical_top_face;
	CHyperbolicTypes::CSimulationTypes::CVerticalFaceData boundary_dirichlet_vertical_bottom_face;
#endif

	/**
	 * set-up boundary condition
	 */
	EBoundaryConditions eBoundaryCondition;

	/**
	 * damping factor when velocity damping boundary condition is used
	 */
	T eBoundaryConditionVelocityDampingFactor;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;


	/******************************************************
	 * CFL condition stuff
	 *
	 * TODO: the CFL condition also has to be built-in into the
	 * adaptive traversals (due to refine operations)
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;

	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
//	TReduceValue cfl_domain_size_div_max_wave_speed;

//	T cfl1_max_value;
	T cfl1_min_value;

	/**
	 * inner radius for rectangular triangle:
	 *
	 * see http://en.wikipedia.org/wiki/Right_triangle#Inradius_and_circumradius
	 *
	 * r = (a+b-c)/2.0 = (a*b)/(a+b+c)
	 *
	 * a = i
	 * b = i
	 * c = i sqrt(2.0)
	 *
	 * r = 0.5 * i * (2.0 - sqrt(2.0))
	 *
	 * r = i^2 / (2.0 * i + i * sqrt(2)) = i / (2.0 + sqrt(2.0))
	 */

	/**
	 * refine and coarsen thresholds
	 */
	T refine_threshold;
	T coarsen_threshold;

	/**
	 * setup with default values
	 */
	CEdgeComm_Parameters()	:
		cDatasets(nullptr),
		eBoundaryCondition(BOUNDARY_CONDITION_OUTFLOW),
		eBoundaryConditionVelocityDampingFactor(0.9),
		instabilityDetected(false)
	{
//		cfl1_max_value = std::numeric_limits<T>::infinity();
		cfl1_min_value = std::numeric_limits<T>::infinity();

		timestep_size = -std::numeric_limits<T>::infinity();
#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		cathetus_real_length = -std::numeric_limits<T>::infinity();
#endif
#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
		domain_real_height = -std::numeric_limits<T>::infinity();
#endif


		refine_threshold = 999999;
		coarsen_threshold = 0;
	}



	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-cluster
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl1_min_value = std::numeric_limits<T>::infinity();
//		cfl1_max_value = 0;
	}

	/**
	 * update the cluster timestep size data with net updates from adjacent triangles
	 */
	inline void updateTimestepSizeWithNetUpdates(
			sierpi::CStack<CHyperbolicTypes::CSimulationTypes::CEdgeData> &i_edge_data
	)
	{
		size_t s = i_edge_data.getNumberOfElementsOnStack();

		for (size_t i = 0; i < s; i++)
			updateCFL1Value(i_edge_data.getElementAtIndex(i).CFL1_scalar);
	}

	inline void updateCFL1Value(
			T i_cfl1_value
	)
	{
#if CONFIG_EXIT_ON_SMALL_TIMESTEP
		assert(i_cfl1_value > 0);
#endif

		cfl1_min_value = std::min(cfl1_min_value, i_cfl1_value);
	}

	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-cluster
	 */
	void traversal_post_hook()
	{
	}


	/**
	 * return the reduce value for the traversator
	 */
	inline void storeReduceValue(T *o_reduceValue)
	{
		*o_reduceValue = cfl1_min_value;
	}

	/**
	 * return the set-up timestep size
	 */
	inline T getCFL1TimestepSize()
	{
		return cfl1_min_value;
	}


	/**
	 * \brief set the timestep size
	 */
	inline void setTimestepSize(T i_timestep_size)
	{
		timestep_size = i_timestep_size;
	}

	/**
	 * \brief set the gravitation
	 *
	 * WARNING: this value is not always used by the fluxes in the current state!
	 */
	inline void setGravitationalConstant(T i_gravitational_constant)
	{
		gravitational_constant = i_gravitational_constant;
	}


	/**
	 * boundary condition
	 */
	void setBoundaryCondition(
			EBoundaryConditions i_eBoundaryCondition
	)
	{
		eBoundaryCondition = i_eBoundaryCondition;
	}


	/**
	 * setup parameter for dirichlet boundary condition
	 */
	void setBoundaryDirichlet(
			const CHyperbolicTypes::CSimulationTypes::CEdgeData *p_boundary_dirichlet
	)
	{
		boundary_dirichlet = *p_boundary_dirichlet;

#if SIMULATION_MULTILAYER_ENABLED
		T default_values[5];

		default_values[0] = boundary_dirichlet.dofs.layers[0].r[0];
		default_values[1] = 0;
		default_values[2] = 0;
		default_values[3] = 0;
		default_values[4] = boundary_dirichlet.dofs.layers[0].e[0];

		boundary_dirichlet_vertical_top_face.setupDefaultValues(default_values);
		boundary_dirichlet_vertical_bottom_face.setupDefaultValues(default_values);
#endif
	}


	/**
	 * \brief set all parameters for simulation
	 */
	inline void setParameters(
			T i_timestep_size,				///< timestep size
			T i_cathetus_real_length,		///< length of a square (a single catheti)
#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
			T i_domain_real_height,			///< height of the domain
#endif
			T i_gravitational_constant,

			T i_refine_parameter,
			T i_coarsen_parameter,

			CDatasets *i_cDatasets
	)
	{
		timestep_size = i_timestep_size;
		gravitational_constant = i_gravitational_constant;

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		cathetus_real_length = i_cathetus_real_length;
#endif

#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
		domain_real_height = i_domain_real_height;
		cell_real_height = i_domain_real_height/(T)SIMULATION_NUMBER_OF_LAYERS;
#endif

		refine_threshold = i_refine_parameter;
		coarsen_threshold = i_coarsen_parameter;

		cDatasets = i_cDatasets;
	}


	inline void setAdaptivityParameters(
			T i_refine_threshold,
			T i_coarsen_threshold
	)
	{
		refine_threshold = i_refine_threshold;
		coarsen_threshold = i_coarsen_threshold;
	}


// only available for non-distorted grids
#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
	/**
	 * \brief compute CFL, update sub-cluster local CFL and return local CFL
	 */
	inline T getCFLCellFactor(
			int i_depth
	)	const
	{
		static const T incircle_unit_diameter = ((T)2.0 - (T)std::sqrt((T)2.0));


		return sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*
					cathetus_real_length*incircle_unit_diameter;
	}
#endif

	/**
	 * \brief return special boundary CFL factor, update sub-cluster local CFL and return local CFL
	 */
	template <typename T>
	static inline T getBoundaryCFLCellFactor(
			int i_depth
	)
	{
		return std::numeric_limits<T>::max();
	}
};


#endif /* CEDGECOMM_CONFIG_HPP_ */
