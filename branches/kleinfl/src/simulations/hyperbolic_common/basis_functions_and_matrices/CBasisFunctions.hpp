/*
 * CBasisFunctions.hpp
 *
 *  Created on: Sep 13, 2012
 *      Author: schreibm
 */

#ifndef CBASISFUNCTIONS2D_HPP_
#define CBASISFUNCTIONS2D_HPP_


assert(SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DIMENSIONS == 2);

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3

	// nodal on the fly
#	include "../basis_functions_and_matrices/CBasisFunctions2D_Nodal.hpp"

#else

#	error "invalid SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE"

#endif




#endif /* CBASISFUNCTIONS2D_HPP_ */
