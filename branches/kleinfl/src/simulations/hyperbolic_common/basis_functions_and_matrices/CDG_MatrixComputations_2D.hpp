/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDG_MATRIXCOMPUTATIONS_2D_HPP_
#define CDG_MATRIXCOMPUTATIONS_2D_HPP_


#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0
#	include "CDG_MatrixComputations_2D_Nodal.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 1
#	include "CDG_MatrixComputations_2D_Modal.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 2
#	include "CDG_MatrixComputations_2D_Monomial.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
#	include "CDG_MatrixComputations_2D_Nodal_OnTheFly.hpp"
//#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
//#	include "CDG_MatrixComputations_2D_UserDefined.hpp"
#else
#	error "Unknown basis functino type specified by SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE"
#endif


#endif /* CComputation2D_Matrices_HPP_ */
