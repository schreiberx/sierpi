/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 24, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDG_MATRICES_2D_NODAL_ON_THE_FLY_DEFINES_HPP__
#define CDG_MATRICES_2D_NODAL_ON_THE_FLY_DEFINES_HPP__





/**
 * degree of the basis function
 */
#ifndef SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE
	// default: 0 (finite volumes)
	#error "SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE not set"
	#define SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE		0
#endif

/**
 * CFL factor
 */

/**
 * see
 *   "High Order ADER FV/DG Numerical Methods for Hyperbolic Equations",
 *   Cristóbal E. Castro, page 33
 */
#define SIMULATION_HYPERBOLIC_CFL	(0.5/(2*SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE+1))


/*
 * degree of edge is equal to max. degree of basis function
 */
#define SIMULATION_HYPERBOLIC_EDGE_DEGREE	(SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE)


/**
 * 2D number of DOFs per CELL Data
 */
#define SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS (((SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE+1)*(SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE+2))/2)


#define SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS (SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS)

/**
 * order of integration over edges
 *
 * this order describes the integration rule which has to be accurate for
 * polynomials with a degree of SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER or less
 */
#define SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER	(SIMULATION_HYPERBOLIC_EDGE_DEGREE*2)


/**
 * 2D number of edge communication data DOFs
 *
 * We use k+1 integration points to apply a Gaussian Quadrature
 * of edge flux with double order of basis functions.
 *
 * This is accurate for polynomials of degree 2k+1.
 *
 * See "an introduction to the discontinuous galerkin method for convection dominated problems",
 * page 44, 3.3.2.
 */
#define SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS ((SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER+2)/2)

/**
 * degree of integration over cell area
 */
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE	(SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE)
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER	(SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE*2)






#endif
