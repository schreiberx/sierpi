
/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 19, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION2D_MATRICES_CONST_HPP_
#define CCOMPUTATION2D_MATRICES_CONST_HPP_

#include <string.h>
#include <assert.h>

#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"

#define SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS (2)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS (6)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS (6)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS (3)
#define SIMULATION_HYPERBOLIC_CFL	(0.5/(T)(2*2+1))
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS)
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS*2)

class CDG_MatrixComputations_2D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;



public:
        static inline void mul_edge_comm_hyp_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,0>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_right_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,1>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_left_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,2>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }

public:
        static inline void mul_adaptivity_project_momentum_reference_to_left_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_left_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                        );
                }
        }



public:
        static inline void mul_adaptivity_project_momentum_reference_to_right_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_right_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                        );
                }
        }



#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

public:
	static inline void mul_edge_comm_transform_to_edge_space(
		const T i_rotationMatrix[2][2],
		const T i_inverseTransformationMatrix[2][2],
		T io_hu[6],
		T io_hv[6]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			/*
			 * scale and shear from reference space
			 */
			CTriangle_VectorProjections::matrixTransformation<T>(
				i_inverseTransformationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

			/*
			 * rotate edge comm data to edge space
			 */
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace<T>(
				i_rotationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

		}
	}
#endif


	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void mul_timestep_inv_mass(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{72.,-240.,180.,-240.,360.,180.},
			{-240.,1200.,-1080.,600.,-1440.,-360.},
			{180.,-1080.,1080.,-360.,1080.,180.},
			{-240.,600.,-360.,1200.,-1440.,-1080.},
			{360.,-1440.,1080.,-1440.,2880.,1080.},
			{180.,-360.,180.,-1080.,1080.,1080.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void madd_timestep_inv_mass(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{72.,-240.,180.,-240.,360.,180.},
			{-240.,1200.,-1080.,600.,-1440.,-360.},
			{180.,-1080.,1080.,-360.,1080.,180.},
			{-240.,600.,-360.,1200.,-1440.,-1080.},
			{360.,-1440.,1080.,-1440.,2880.,1080.},
			{180.,-360.,180.,-1080.,1080.,1080.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_u(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.8333333333e-2,.3333333333e-1,-.8333333333e-2,.6666666667e-1,.6666666667e-1,.1666666667e-1},
			{-.1666666667e-1,.1333333333,.3333333333e-1,.6666666667e-1,.1333333333,-.1666666667e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_u(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.8333333333e-2,.3333333333e-1,-.8333333333e-2,.6666666667e-1,.6666666667e-1,.1666666667e-1},
			{-.1666666667e-1,.1333333333,.3333333333e-1,.6666666667e-1,.1333333333,-.1666666667e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_v(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.1666666667e-1,.6666666667e-1,-.1666666667e-1,.1333333333,.1333333333,.3333333333e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.8333333333e-2,.6666666667e-1,.1666666667e-1,.3333333333e-1,.6666666667e-1,-.8333333333e-2},
			{0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_v(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.1666666667e-1,.6666666667e-1,-.1666666667e-1,.1333333333,.1333333333,.3333333333e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.8333333333e-2,.6666666667e-1,.1666666667e-1,.3333333333e-1,.6666666667e-1,-.8333333333e-2},
			{0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_dofs_to_nodes
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_dofs_to_nodes(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_dofs_to_nodes
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_dofs_to_nodes(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_hyp_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_hyp_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_left_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_left_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_right_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,1.,1.,0.,0.,0.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_right_face(
		const T i_input[6],
		T o_output[3]
	) {
		static const T matrix[3][6] = 
		{
			{1.,1.,1.,0.,0.,0.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_hyp_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.2357022604,-.9428090414,-.2357022604},
			{0.,-.4714045206,-.2357022604},
			{.2357022604e-1,-.2828427124,-.2121320343},
			{-.2357022604,-.4714045206,0.},
			{-.2357022604e-1,-.1885618082,-.2357022604e-1},
			{-.2121320343,-.2828427124,.2357022604e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_hyp_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.2357022604,-.9428090414,-.2357022604},
			{0.,-.4714045206,-.2357022604},
			{.2357022604e-1,-.2828427124,-.2121320343},
			{-.2357022604,-.4714045206,0.},
			{-.2357022604e-1,-.1885618082,-.2357022604e-1},
			{-.2121320343,-.2828427124,.2357022604e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_left_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,-.3333333333,-.1666666667},
			{0.,0.,0.},
			{.1666666667e-1,-.2000000000,-.1500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_left_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,-.3333333333,-.1666666667},
			{0.,0.,0.},
			{.1666666667e-1,-.2000000000,-.1500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_right_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{-.1666666667,-.3333333333,0.},
			{-.1500000000,-.2000000000,.1666666667e-1},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_right_fluxes_to_dofs(
		const T i_input[3],
		T o_output[6]
	) {
		static const T matrix[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{-.1666666667,-.3333333333,0.},
			{-.1500000000,-.2000000000,.1666666667e-1},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_left(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,-.5000000000,-.5000000000,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000},
			{0.,.5000000000,0.,-.5000000000,.5000000000,-1.},
			{0.,0.,-1.,0.,0.,1.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_left(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,-.5000000000,-.5000000000,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000},
			{0.,.5000000000,0.,-.5000000000,.5000000000,-1.},
			{0.,0.,-1.,0.,0.,1.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_right(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-.5000000000,-.5000000000,0.},
			{0.,0.,1.,0.,0.,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_right(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-.5000000000,-.5000000000,0.},
			{0.,0.,1.,0.,0.,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_left(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,.5000000000,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_left(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,.5000000000,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_right(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000},
			{0.,.5000000000,.5000000000,-.5000000000,0.,-.5000000000},
			{0.,0.,-.5000000000,0.,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_right(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000},
			{0.,.5000000000,.5000000000,-.5000000000,0.,-.5000000000},
			{0.,0.,-.5000000000,0.,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void mul_convert_poly_to_dofs(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void madd_convert_poly_to_dofs(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_poly(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_poly(
		const T i_input[6],
		T o_output[6]
	) {
		static const T matrix[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_0th_order(
		const T i_input[6],
		T o_output[1]
	) {
		static const T matrix[1][6] = 
		{
			{1.,.3333333333,.1666666667,.3333333333,.8333333333e-1,.1666666667}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_0th_order(
		const T i_input[6],
		T o_output[1]
	) {
		static const T matrix[1][6] = 
		{
			{1.,.3333333333,.1666666667,.3333333333,.8333333333e-1,.1666666667}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

public:
	static inline int getNumberOfFlops()
	{
		return 1260;
	}


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
public:
	static void setup(int i_verbosity_level)
	{
	}


public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 0" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
//		std::cout << " + Integration cell degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
//		std::cout << " + Integration edge degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;
		std::cout << std::endl;

		std::cout << std::endl;
		std::cout << "timestep_inv_mass:" << std::endl;
		static const T timestep_inv_mass[6][6] = 
		{
			{72.,-240.,180.,-240.,360.,180.},
			{-240.,1200.,-1080.,600.,-1440.,-360.},
			{180.,-1080.,1080.,-360.,1080.,180.},
			{-240.,600.,-360.,1200.,-1440.,-1080.},
			{360.,-1440.,1080.,-1440.,2880.,1080.},
			{180.,-360.,180.,-1080.,1080.,1080.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_inv_mass[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_inv_mass[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_u:" << std::endl;
		static const T timestep_stiffness_u[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.8333333333e-2,.3333333333e-1,-.8333333333e-2,.6666666667e-1,.6666666667e-1,.1666666667e-1},
			{-.1666666667e-1,.1333333333,.3333333333e-1,.6666666667e-1,.1333333333,-.1666666667e-1}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_stiffness_u[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_stiffness_u[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_v:" << std::endl;
		static const T timestep_stiffness_v[6][6] = 
		{
			{0.,0.,0.,0.,0.,0.},
			{0.,.1666666667,0.,.1666666667,.1666666667,0.},
			{-.1666666667e-1,.6666666667e-1,-.1666666667e-1,.1333333333,.1333333333,.3333333333e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.8333333333e-2,.6666666667e-1,.1666666667e-1,.3333333333e-1,.6666666667e-1,-.8333333333e-2},
			{0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_stiffness_v[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_stiffness_v[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_dofs_to_nodes:" << std::endl;
		static const T timestep_stiffness_dofs_to_nodes[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_stiffness_dofs_to_nodes[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_stiffness_dofs_to_nodes[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_hyp_face:" << std::endl;
		static const T timestep_dofs_to_hyp_face[3][6] = 
		{
			{1.,0.,0.,1.,0.,1.},
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{1.,1.,1.,0.,0.,0.}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_hyp_face[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_dofs_to_hyp_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_left_face:" << std::endl;
		static const T timestep_dofs_to_left_face[3][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{1.,0.,0.,.5000000000,0.,.2500000000},
			{1.,0.,0.,1.,0.,1.}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_left_face[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_dofs_to_left_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_right_face:" << std::endl;
		static const T timestep_dofs_to_right_face[3][6] = 
		{
			{1.,1.,1.,0.,0.,0.},
			{1.,.5000000000,.2500000000,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_right_face[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_dofs_to_right_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_hyp_fluxes_to_dofs:" << std::endl;
		static const T timestep_hyp_fluxes_to_dofs[6][3] = 
		{
			{-.2357022604,-.9428090414,-.2357022604},
			{0.,-.4714045206,-.2357022604},
			{.2357022604e-1,-.2828427124,-.2121320343},
			{-.2357022604,-.4714045206,0.},
			{-.2357022604e-1,-.1885618082,-.2357022604e-1},
			{-.2121320343,-.2828427124,.2357022604e-1}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_hyp_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_hyp_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_left_fluxes_to_dofs:" << std::endl;
		static const T timestep_left_fluxes_to_dofs[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,-.3333333333,-.1666666667},
			{0.,0.,0.},
			{.1666666667e-1,-.2000000000,-.1500000000}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_left_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_left_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_right_fluxes_to_dofs:" << std::endl;
		static const T timestep_right_fluxes_to_dofs[6][3] = 
		{
			{-.1666666667,-.6666666667,-.1666666667},
			{-.1666666667,-.3333333333,0.},
			{-.1500000000,-.2000000000,.1666666667e-1},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_right_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_right_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_left:" << std::endl;
		static const T adaptivity_coarsen_left[6][6] = 
		{
			{.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,-.5000000000,-.5000000000,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000},
			{0.,.5000000000,0.,-.5000000000,.5000000000,-1.},
			{0.,0.,-1.,0.,0.,1.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << adaptivity_coarsen_left[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << adaptivity_coarsen_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_right:" << std::endl;
		static const T adaptivity_coarsen_right[6][6] = 
		{
			{.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-.5000000000,-.5000000000,0.},
			{0.,0.,1.,0.,0.,-1.},
			{0.,0.,.5000000000,0.,.5000000000,.5000000000}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << adaptivity_coarsen_right[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << adaptivity_coarsen_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_left:" << std::endl;
		static const T adaptivity_refine_left[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,.5000000000,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << adaptivity_refine_left[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << adaptivity_refine_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_right:" << std::endl;
		static const T adaptivity_refine_right[6][6] = 
		{
			{1.,.5000000000,.2500000000,.5000000000,.2500000000,.2500000000},
			{0.,-.5000000000,-.5000000000,-.5000000000,-.5000000000,-.5000000000},
			{0.,0.,.2500000000,0.,.2500000000,.2500000000},
			{0.,.5000000000,.5000000000,-.5000000000,0.,-.5000000000},
			{0.,0.,-.5000000000,0.,0.,.5000000000},
			{0.,0.,.2500000000,0.,-.2500000000,.2500000000}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << adaptivity_refine_right[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << adaptivity_refine_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_poly_to_dofs:" << std::endl;
		static const T convert_poly_to_dofs[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << convert_poly_to_dofs[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << convert_poly_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_poly:" << std::endl;
		static const T convert_dofs_to_poly[6][6] = 
		{
			{1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << convert_dofs_to_poly[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << convert_dofs_to_poly[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_0th_order:" << std::endl;
		static const T convert_dofs_to_0th_order[1][6] = 
		{
			{1.,.3333333333,.1666666667,.3333333333,.8333333333e-1,.1666666667}
		};
		for (int j = 0; j < 1; j++)
		{
			std::cout << convert_dofs_to_0th_order[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << convert_dofs_to_0th_order[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif
