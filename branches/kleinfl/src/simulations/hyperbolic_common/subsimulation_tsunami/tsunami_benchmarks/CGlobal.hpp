/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 12, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOWX_HPP_
#define CDEFORMATIONAL_FLOWX_HPP_


//#include "CSimulation_MainParameters.hpp"
//#include "../../CParameters.hpp"
#include "../CDatasets.hpp"

//#include "CMain.hpp"

//extern CONFIG_DEFAULT_FLOATING_POINT_TYPE simulation_timestamp_for_timestep;



class CGlobal
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	enum EBenchmark_Type {
		E_2D = -1,
		E_DEF_FLOW_CASE1 = 1,
		E_DEF_FLOW_LON_ONLY = 2,
		E_SOLID_BODY_ROTATION = 5
	};

	T simulation_timestamp_for_timestep;
	EBenchmark_Type benchmark_type;
	T benchmark_body_rotation_alpha;
	T benchmark_body_rotation_roundtime;
	CDatasets *cDatasets;

	T error_l1_n = 0;
	T error_l1_d = 0;
	int errors_set = 0;

	int element_ctr = 0;

	void addErrorL1(T error_n, T error_d){
		if (simulation_timestamp_for_timestep >= 750000){
			error_l1_n += error_n;
			error_l1_d += error_d;
			element_ctr ++;
//			std::cout << error_l1_n << std::endl;
		}
	}

	void setDatasets(CDatasets *i_cDatasets){
		cDatasets = i_cDatasets;
	}
	CDatasets *getDatasets(){
		return cDatasets;
	}

	void setTS(T ts){
		simulation_timestamp_for_timestep = ts;
		if(errors_set == 1){
			T error_l1, area_sphere;
			error_l1 = error_l1_n / error_l1_d;
//			area_sphere = 4 * M_PI * EARTH_RADIUS_IN_METERS * EARTH_RADIUS_IN_METERS;
//			error_l1 = error_l1 / (area_sphere);
//			std::cout << "l1: " << error_l1 << std::endl;
//			std::cout << ts << ", " << element_ctr << std::endl;
//			errors_set = -1;
		}
		else if(errors_set == -1){
//			std::cout << element_ctr << std::endl;
		}
		if(ts >= 750000)// && errors_set == 0)
			errors_set = 1;
	}
	T getTS(){
		return simulation_timestamp_for_timestep;
	}

	void setBenchmarkType(int i_benchmark_type){
		benchmark_type = EBenchmark_Type(i_benchmark_type);
	}
	EBenchmark_Type getBenchmarkType(){
		return benchmark_type;
	}

	void setAlpha(T alpha){
		benchmark_body_rotation_alpha = alpha;
	}
	T getAlpha(){
		return benchmark_body_rotation_alpha;
	}

	void setRoundTime(T time){
		benchmark_body_rotation_roundtime = time;
	}
	T getRoundTime(){
		return benchmark_body_rotation_roundtime;
	}

    static CGlobal& getInstance()
    {
        static CGlobal    instance; // Guaranteed to be destroyed.
                              // Instantiated on first use.
        return instance;
    }
private:
    CGlobal() {};                   // Constructor? (the {} brackets) are needed here.
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singleton appearing.
    CGlobal(CGlobal const&);              // Don't Implement
    void operator=(CGlobal const&); // Don't implement
};

#endif



