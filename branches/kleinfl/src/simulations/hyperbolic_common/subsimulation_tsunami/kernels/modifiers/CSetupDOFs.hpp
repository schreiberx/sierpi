/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUP_DOFS_HPP_
#define CSETUP_DOFS_HPP_


#include "../../CDatasets.hpp"
#include "../../types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
#	include "../../../basis_functions_and_matrices/CBasisFunctions2D.hpp"
#endif
#include "../../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"

#include "../../tsunami_benchmarks/CGlobal.hpp"

class CSetupTsunamiDOFs
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:
	static inline void setup(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CSimulationCellData *io_cCellData,

			CDatasets *cDatasets
	)	{
#if CONFIG_BENCHMARK_SPHERE
		CGlobal& gx = CGlobal::getInstance();
		gx.setDatasets(cDatasets);
#endif

		T lod = sierpi::CTriangle_Tools::getLODFromDepth(i_depth);

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3

		/*
		 * for SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE==3, nodal points are available.
		 *
		 * we utilize this feature to setup the DOFs.
		 */

		T *nodal_coords = CDG_MatrixComputations_2D::cBasisFunctions2D.getNodalCoords();

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			CSimulationNodeData n;

			// initialize in case that getNodalData only updates the values
			n.h = io_cCellData->dofs.h[i];
			n.hu = io_cCellData->dofs.hu[i];
			n.hv = io_cCellData->dofs.hv[i];
			n.b = io_cCellData->dofs.b[i];


			T world_coords[2];

			sierpi::CTriangle_PointProjections::referenceToWorld(
					i_vertex_left_x,	i_vertex_left_y,
					i_vertex_right_x,	i_vertex_right_y,
					i_vertex_top_x,		i_vertex_top_y,


#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT
					(T)(0.5),			(T)(0.5),
#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER
					(T)(0.25),			(T)(0.25),
#else
					(T)(1.0/3.0),		(T)(1.0/3.0),
#endif

					&world_coords[0],	&world_coords[1]
			);


			cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

			io_cCellData->dofs.h[i] = n.h;
			io_cCellData->dofs.hu[i] = n.hu;
			io_cCellData->dofs.hv[i] = n.hv;
			io_cCellData->dofs.b[i] = n.b;

			if (io_cCellData->dofs.h[i] < 0)
			{
				assert(false);
				throw("negative depth detected");
			}

			if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
			{
				//momentum was updated -> project to reference space
				CTriangle_VectorProjections::worldToReference(
						&io_cCellData->dofs.hu[i],
						&io_cCellData->dofs.hv[i],
						-i_right_normal_x,
						-i_right_normal_y
				);
			}

			nodal_coords += 2;
		}

#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 1 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 2

		T world_coords[2];
		sierpi::CTriangle_PointProjections::referenceToWorld(
			i_vertex_left_x,	i_vertex_left_y,
			i_vertex_right_x,	i_vertex_right_y,
			i_vertex_top_x,		i_vertex_top_y,
#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT
			(T)(0.5),			(T)(0.5),
#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER
			(T)(0.25),			(T)(0.25),
#else
			(T)(1.0/3.0),		(T)(1.0/3.0),
#endif

			&world_coords[0],	&world_coords[1]
		);

		CSimulationNodeData n;

 		T poly_coefficients_h[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
 		T poly_coefficients_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
 		T poly_coefficients_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
 		T poly_coefficients_b[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

		// initialize in case that getNodalData only updates the values
 		CDG_MatrixComputations_2D::mul_convert_dofs_to_poly(io_cCellData->dofs.h, poly_coefficients_h);
		n.h = poly_coefficients_h[0];

 		CDG_MatrixComputations_2D::mul_convert_dofs_to_poly(io_cCellData->dofs.hu, poly_coefficients_hu);
		n.hu = poly_coefficients_hu[0];

 		CDG_MatrixComputations_2D::mul_convert_dofs_to_poly(io_cCellData->dofs.hv, poly_coefficients_hv);
		n.hv = poly_coefficients_hv[0];

 		CDG_MatrixComputations_2D::mul_convert_dofs_to_poly(io_cCellData->dofs.b, poly_coefficients_b);
 		n.b = poly_coefficients_b[0];

 		// load nodal data
		cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

		/*
		 * setup DOFs
		 */
		// reset poly coefficients. we later on need only the first one
		poly_coefficients_h[0] = n.h;
 		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly_coefficients_h, io_cCellData->dofs.h);

 		poly_coefficients_hu[0] = n.hu;
 		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly_coefficients_hu, io_cCellData->dofs.hu);

 		poly_coefficients_hv[0] = n.hv;
 		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly_coefficients_hv, io_cCellData->dofs.hv);

 		poly_coefficients_b[0] = n.b;
 		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly_coefficients_b, io_cCellData->dofs.b);
#endif


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		io_cCellData->setupDistortedGrid(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				i_hyp_normal_x,		i_hyp_normal_y,
				i_right_normal_x,	i_right_normal_y,
				i_left_normal_x,	i_left_normal_y
			);
#endif

#if DEBUG
		if (io_cCellData->dofs.h[0] < 0)
		{
			std::cout << "ERROR: negative depth detected" << std::endl;
			assert(false);
			exit(-1);
		}
#endif

		if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
		{
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				//momentum was updated -> project to reference space
				CTriangle_VectorProjections::worldToReference(
						&io_cCellData->dofs.hu[i],
						&io_cCellData->dofs.hv[i],
						-i_right_normal_x,
						-i_right_normal_y
					);
			}
		}
	}
};

#endif /* CSETUP_TSUNAMI_DOFS_NODAL_HPP_ */
