/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_COLUMN_ORDER_N_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_ORDER_N_TSUNAMI_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "../common/CCommon_Adaptivity.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/CDatasets.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename CCellData, typename TSimulationStacks>
class CSetup_Column	:
	public CCommon_Adaptivity,
	public CMigration_RawClass
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<CSetup_Column, CHyperbolicTypes>	TRAV;

	typedef T TVertexScalar;
	typedef CSimulationEdgeData CEdgeData;

	/// center of column
	T column_center_x, column_center_y;

	// radius of column
	T column_radius;
	T column_radius_squared;


	CSetup_Column()	:
		column_center_x(0),
		column_center_y(0),
		column_radius(0),
		column_radius_squared(0)
	{
	}


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	CMigrationClass<CSetup_Column> cMigrationClass;

	inline void migration_send(int i_dst_rank)
	{
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::sendAtomic(*this, i_dst_rank);
#else
		cMigrationClass.send(*this, i_dst_rank);
#endif
	}

	inline void migration_send_postprocessing(int i_dst_rank)
	{
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
#else
		cMigrationClass.wait();
#endif
	}

	inline void migration_recv(int i_src_rank)
	{
		sierpi::CMigration::recvRawClass(*this, i_src_rank);
	}

#endif



	/**
	 * check whether a point is inside the circle
	 */
	inline bool insideCircle(
			TVertexScalar i_px,
			TVertexScalar i_py
	)
	{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE

		T point_sphere_x, point_sphere_y, point_sphere_z;
		CCube_To_Sphere_Projection::project2DTo3D(
				i_px, i_py,
				point_sphere_x,
				point_sphere_y,
				point_sphere_z
			);

		T center_sphere_x, center_sphere_y, center_sphere_z;
		CCube_To_Sphere_Projection::project2DTo3D(
				column_center_x, column_center_y,
				center_sphere_x,
				center_sphere_y,
				center_sphere_z
			);

		T alpha = std::acos(point_sphere_x * center_sphere_x + point_sphere_y * center_sphere_y + point_sphere_z * center_sphere_z);

		T distance = EARTH_RADIUS_IN_METERS * alpha;

		return (distance < column_radius);

#else

		TVertexScalar x = column_center_x-i_px;
		TVertexScalar y = column_center_y-i_py;
		return x*x+y*y < column_radius_squared;

#endif
	}



	inline bool should_refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int depth,

			CSimulationCellData *o_cellData
	)
	{
		assert(cDatasets != nullptr);

		/*
		 * check whether the line on the circle intersects with the triangle
		 */
		int counter = 0;
		counter += (int)insideCircle(i_vertex_left_x,	i_vertex_left_y);
		counter += (int)insideCircle(i_vertex_right_x,	i_vertex_right_y);
		counter += (int)insideCircle(i_vertex_top_x,	i_vertex_top_y);

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1

		if (counter > 0)
			return true;


		if (CPointInTriangleTest<T>::test(
				i_vertex_left_x, i_vertex_left_y,
				i_vertex_right_x, i_vertex_right_y,
				i_vertex_top_x, i_vertex_top_y,
				column_center_x, column_center_y
		))
			return true;


		return false;

#elif SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2 || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 3

		if (counter == 1 || counter == 3)
			return true;

		if (CPointInTriangleTest<T>::test(
				i_vertex_left_x, i_vertex_left_y,
				i_vertex_right_x, i_vertex_right_y,
				i_vertex_top_x, i_vertex_top_y,
				column_center_x, column_center_y
		))
			return true;

		return false;

#else
#	error "unknown adaptivity mode"
#endif
	}


	inline bool should_coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int depth,
			
			CSimulationCellData *i_cCellData
	)
	{
		assert(cDatasets != nullptr);

		/**
		 * we use the coarsening callback to update the column inner elements data (even if i_cCellData data should not be accessed)
		 */
/*
		if (!insideCircle(i_vertex_left_x, i_vertex_left_y))
			return false;

		if (!insideCircle(i_vertex_right_x, i_vertex_right_y))
			return false;

		if (!insideCircle(i_vertex_top_x, i_vertex_top_y))
			return false;
*/
		return false;
	}


	void setup_Parameters(
			T i_column_center_x,
			T i_column_center_y,
			T i_column_radius,

			int i_setup_method_id,
			CDatasets *i_cDatasets
	)
	{
		column_center_x = i_column_center_x;
		column_center_y = i_column_center_y;
		column_radius = i_column_radius;
		column_radius_squared = column_radius*column_radius;

		cDatasets = i_cDatasets;
	}


	void setup_WithKernel(
			CSetup_Column<CCellData, TSimulationStacks> &i_parent
	)
	{
		column_center_x = i_parent.column_center_x;
		column_center_y = i_parent.column_center_y;
		column_radius = i_parent.column_radius;
		column_radius_squared = i_parent.column_radius_squared;

		cDatasets = i_parent.cDatasets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
