/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_ORDER_N_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_ORDER_N_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData_Depth.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "CSetupDOFs.hpp"
#include "../../CDatasets.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename t_CSimulationStacksAndTypes>
class CSetup_CellData
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData_Depth<CSetup_CellData, t_CSimulationStacksAndTypes>	TRAV;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CNodeData CNodeData;


	/*.
	 * callback for terrain data
	 */
	CDatasets *cDatasets;


	CSetup_CellData()	:
		cDatasets(nullptr)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CSimulationCellData *io_cCellData
	)	{
		assert(cDatasets != nullptr);

		CSetupTsunamiDOFs::setup(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				i_hyp_normal_x,		i_hyp_normal_y,		///< normals for hypotenuse edge
				i_right_normal_x,	i_right_normal_y,		///< normals for righedge
				i_left_normal_x,	i_left_normal_y,		///< normals for lefedge (necessary for back-rotation of element)

				i_depth,

				io_cCellData,

				cDatasets
		);
		
#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
		io_cCellData->refine = false;
		io_cCellData->coarsen = false;
#endif
	}


	void setup_Parameters(
			CDatasets *i_cSimulationScenarios
	)
	{
		cDatasets = i_cSimulationScenarios;
	}



	void setup_WithKernel(
			CSetup_CellData<t_CSimulationStacksAndTypes> &i_parent
	)
	{
		cDatasets = i_parent.cDatasets;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
