/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGETDATASAMPLE_VERTICES_ELEMENT_HPP_
#define CGETDATASAMPLE_VERTICES_ELEMENT_HPP_

#include "libmath/CPointInTriangleTest.hpp"
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libmath/CVertex2d.hpp"


#include "simulations/hyperbolic_common/subsimulation_generic/CDatasets.hpp"



namespace sierpi
{
namespace kernels
{



template <typename t_CSimulationStacks>
class CGetNodeDataSample	:
	public CHyperbolicTypes::CSimulationTypes
{
public:
	typedef T TVertexScalar;

	typedef sierpi::travs::CTraversator_VertexCoords_CellData<CGetNodeDataSample<t_CSimulationStacks>, t_CSimulationStacks > TRAV;

	CNodeData *cNodeData;

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
	T *eigenCoefficients;
#endif

	T sample_pos_x;
	T sample_pos_y;

public:

	CGetNodeDataSample()	:
		cNodeData(nullptr),
#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		eigenCoefficients(nullptr),
#endif
		sample_pos_x(0),
		sample_pos_y(0)
	{

	}

	inline void op_cell(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			CCellData *i_cCellData
	)	{
		if (!CPointInTriangleTest<double>::test(
				v0x, v0y,
				v1x, v1y,
				v2x, v2y,
				sample_pos_x, sample_pos_y
		))
			return;

 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.h, &cNodeData->h);
 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.hu, &cNodeData->hu);
 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.hv, &cNodeData->hv);
 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.b, &cNodeData->b);

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		*eigenCoefficients = i_cCellData->max_eigen_coefficient;
#endif
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(
			T i_sample_pos_x,
			T i_sample_pos_y,

			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData
#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			,
			T *o_eigenCoefficients
#endif
	)
	{
		sample_pos_x = i_sample_pos_x;
		sample_pos_y = i_sample_pos_y;

		cNodeData = o_cNodeData;

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		eigenCoefficients = o_eigenCoefficients;
#endif
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			CGetNodeDataSample &parent_kernel
	)
	{
		assert(false);
	}
};


}
}

#endif
