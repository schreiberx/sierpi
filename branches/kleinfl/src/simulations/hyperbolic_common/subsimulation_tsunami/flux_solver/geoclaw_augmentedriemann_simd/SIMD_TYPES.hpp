/***********************************************************************************//**
 *
 * \file SIMD_TYPES.hpp
 *
 * \brief Defines the length of the vectors and the corresponding functions
 *
 * \author Wolfgang Hölzl (hoelzlw), hoelzlw AT in.tum.de
 *
 * \version 0.1
 * \date 24.04.2013 15:14:14
 *
 **************************************************************************************/

#pragma once

#ifndef  SIMD_TYPES_H
#define  SIMD_TYPES_H

#ifdef  SIMD_DEFINITIONS_H
	#error "SIMD Definitions already included! Never include that file directly! Include it only via including SIMD_TYPES (this file!)"
#endif /* defined SIMD_DEFINITIONS_H */

#if not WAVE_PROPAGATION_SOLVER == 5
	#pragma message "SIMD macros included but non-vectorized solver specified"
#endif /* not WAVE_PROPAGATION_SOLVER == 5  */

#if defined FLOAT64
	#pragma message "Using double as type for real numbers"
	typedef double real;
	#pragma message "Using unsigned long as type for integer numbers"
	typedef unsigned long integer;

	#define SHIFT_SIGN_RIGHT(x) (1 << (64 - (x)))
#else /* not defined FLOAT64  */
	#pragma message "Using float as type for real numbers"
	typedef float real;
	#pragma message "Using unsigned int as type for integer numbers"
	typedef unsigned int integer;

	#define SHIFT_SIGN_RIGHT(x) (1 << (32 - (x)))
#endif /* not defined FLOAT64  */


#if  (defined __SSE4_1__ and not defined __AVX__) or (defined __AVX__ and defined AVX128)
	#pragma message "Using SSE4.1 for vectorization"
	#include <smmintrin.h>

	typedef __m128i integer_vector;
	#if  defined FLOAT64
		#define VECTOR_LENGTH 2
		#define VECTOR_SSE4_FLOAT64
		#define VECTOR_FULL_MASK 0x00000003

		typedef __m128d real_vector;
		#pragma message "Using vectors of 2 float64"
	#else /* not defined FLOAT64  */
		#define VECTOR_LENGTH 4
		#define VECTOR_SSE4_FLOAT32
		#define VECTOR_FULL_MASK 0x0000000F

		typedef __m128 real_vector;
		#pragma message "Using vectors of 4 float32"
	#endif /* not defined FLOAT64 */
#elif  defined __AVX__
	#pragma message "Using AVX for vectorization"
	#include <immintrin.h>
	
	typedef __m256i integer_vector;
	#if  defined FLOAT64
		#define VECTOR_LENGTH 4
		#define VECTOR_AVX_FLOAT64
		#define VECTOR_FULL_MASK 0x0000000F

		typedef __m256d real_vector;
		#pragma message "Using vectors of 4 float64"
	#else /* not defined FLOAT64  */
		#define VECTOR_LENGTH 8
		#define VECTOR_AVX_FLOAT32
		#define VECTOR_FULL_MASK 0x000000FF

		typedef __m256 real_vector;
		#pragma message "Using vectors of 8 float32"
	#endif /* not defined FLOAT64  */
#else /* not defined __SSE4__ and not defined __AVX__  */
	#pragma message "Using no vectorization at all"
	#define VECTOR_LENGTH 1
	#define VECTOR_NOVEC
#endif /* not defined __SSE4__ and not defined __AVX__ */

#include "SIMD_DEFINITIONS.hpp"
#if defined COUNTFLOPS
	#include "SIMD_COSTS.hpp"
#endif /* defined COUNTFLOPS  */

#endif /* #ifndef SIMD_TYPES_H */ 
