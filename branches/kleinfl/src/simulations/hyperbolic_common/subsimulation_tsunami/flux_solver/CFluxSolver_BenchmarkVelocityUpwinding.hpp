/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: 12 Feb, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 */

#ifndef CFLUXSOLVER_VELOCITYUPWINDING_HPP_
#define CFLUXSOLVER_VELOCITYUPWINDING_HPP_


template <typename T>
class CFluxSolver_BenchmarkVelocityUpwinding
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses an upwinding method based on the velocity component
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	inline void op_edge_edge(
			const CSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed
			T i_gravitational_constant				///< gravitational constant
	)
	{

#if 1
		T hu_moving, u_avg;
		u_avg = i_edgeData_left.hu;
//		assert(i_edgeData_left.hu == i_edgeData_right.hu);
#if CONFIG_BENCHMARK_SPHERE>0
//		if(std::abs(i_edgeData_left.hu - i_edgeData_right.hu) > 0.0001)
//			std::cout << "l " << i_edgeData_left.hu << ", r " << i_edgeData_right.hu << std::endl;
#endif
		u_avg = (
				i_edgeData_left.hu
				+ i_edgeData_right.hu
				) * (T)0.5;
		if (u_avg > 0){
			hu_moving = i_edgeData_left.h * u_avg;
		} else {
			hu_moving = i_edgeData_right.h * u_avg;
		}
//		if (i_edgeData_left.h != i_edgeData_left.h)
//		if (i_edgeData_left.hu != i_edgeData_left.hu || i_edgeData_left.h > 5100)
//		std::cout << "u: " << u_avg << "  l: " << i_edgeData_left.hu << "  r: " << i_edgeData_right.hu << "  h: " << hu_moving << std::endl;

		o_edgeFlux_left->h = hu_moving;
		o_edgeFlux_left->hu = u_avg; //(T)0;
		o_edgeFlux_left->hv = u_avg; //(T)0;

		o_edgeFlux_right->h = -hu_moving;
		o_edgeFlux_right->hu = -u_avg; //(T)0;
		o_edgeFlux_right->hv = -u_avg; //(T)0;

		/**
		 * CFL condition
		 */
//		*o_max_wave_speed_left = std::max(std::abs(i_edgeData_left.hu), std::abs(i_edgeData_right.hu));
#if CONFIG_BENCHMARK_SPHERE==0
		*o_max_wave_speed_left = std::abs(hu_moving);
#else
		*o_max_wave_speed_left = std::abs(u_avg);
#endif
		*o_max_wave_speed_right = *o_max_wave_speed_left;
#else
		T moving_h_left = i_edgeData_left.h;
		T moving_h_right = i_edgeData_right.h;

		T left_vx = i_edgeData_left.hu;
		T right_vx = i_edgeData_right.hu;
		T lambda = std::max(std::abs(left_vx), std::abs(right_vx));

		/*
		 * Handle as 1D Riemann problem
		 */
		o_edgeFlux_left->h = (T)0.5*(i_edgeData_left.hu*i_edgeData_left.h + i_edgeData_right.hu*i_edgeData_right.h);

		o_edgeFlux_left->hu = 0;
//				(T)0.5*
//				( left_vx*i_edgeData_left.hu*i_edgeData_left.h + right_vx*i_edgeData_right.hu*i_edgeData_right.h );

		o_edgeFlux_left->hv = 0;	// multiplied with outward pointing normal anyhow, thus set this to 0


		o_edgeFlux_right->h = -o_edgeFlux_left->h;
		o_edgeFlux_right->hu = -o_edgeFlux_left->hu;
		o_edgeFlux_right->hv = -o_edgeFlux_left->hv;

		/*
		 * add numerical diffusion for stability reason
		 */
		T diffusion_h = (T)0.5*(T)lambda*(moving_h_left - moving_h_right);
		T diffusion_hu = (T)0.5*(T)lambda*(i_edgeData_left.hu*i_edgeData_left.h - i_edgeData_right.hu*i_edgeData_right.h);

		o_edgeFlux_right->h -= diffusion_h;
		o_edgeFlux_right->hu -= diffusion_hu;

		o_edgeFlux_left->h += diffusion_h;
		o_edgeFlux_left->hu += diffusion_hu;


		/**
		 * CFL condition
		 */
		*o_max_wave_speed_left = lambda;
		*o_max_wave_speed_right = lambda;
#endif
	}


	template <int N>
	void op_edge_edge(
			const CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T wave_speed_left = 0;
		T wave_speed_right = 0;

		for (int i = 0; i < N; i++)
		{
			T l_wave_speed_left = 0;
			T l_wave_speed_right = 0;

			CSimulationNodeData i_left, i_right;

			i_left.h = i_edgeData_left.h[i];
			i_left.hu = i_edgeData_left.hu[i];
			i_left.hv = i_edgeData_left.hv[i];
			i_left.b = i_edgeData_left.b[i];

			i_right.h = i_edgeData_right.h[N-1-i];
			i_right.hu = -i_edgeData_right.hu[N-1-i];
			i_right.hv = -i_edgeData_right.hv[N-1-i];
			i_right.b = i_edgeData_right.b[N-1-i];

			CSimulationNodeData o_left, o_right;

			op_edge_edge(
					i_left,			///< edge data on left (left) edge
					i_right,		///< edge data on right (outer) edge

					&o_left,		///< output for left flux
					&o_right,		///< output for outer flux

					&l_wave_speed_left,				///< maximum wave speed
					&l_wave_speed_right,				///< maximum wave speed

					i_gravitational_constant				///< gravitational constant
			);

			o_edgeFlux_left->h[i] = o_left.h;
			o_edgeFlux_left->hu[i] = o_left.hu;
			o_edgeFlux_left->hv[i] = o_left.hv;
			o_edgeFlux_left->b[i] = o_left.b;

			o_edgeFlux_right->h[N-1-i] = o_right.h;
			o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
			o_edgeFlux_right->hv[N-1-i] = -o_right.hv;
			o_edgeFlux_right->b[N-1-i] = o_right.b;

//			std::cout << "vmax l: " << l_wave_speed_left << "  vmax r: " << l_wave_speed_right << std::endl;
			wave_speed_left = std::max(wave_speed_left, l_wave_speed_left);
			wave_speed_right = std::max(wave_speed_right, l_wave_speed_right);
		}

//		std::cout << "vmax l: " << wave_speed_left << "  vmax r: " << wave_speed_right << std::endl;
//		if (wave_speed_left == 0)
//			wave_speed_left = (T)200;
//		if (wave_speed_right == 0)
//			wave_speed_right = (T)200;
		*o_max_wave_speed_left = wave_speed_left;
		*o_max_wave_speed_right = wave_speed_right;
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
