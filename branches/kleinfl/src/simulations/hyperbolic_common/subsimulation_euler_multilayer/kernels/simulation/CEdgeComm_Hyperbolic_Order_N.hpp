/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 *				Stjepan Bakrac <bakrac@in.tum.de>
 *				Philipp Müller <philippausmuensing@googlemail.com>
 */

#ifndef CEDGECOMM_ORDER_N_HPP_
#define CEDGECOMM_ORDER_N_HPP_



#include <cmath>
#include <limits>

// Config
#include "../../CConfig.hpp"

// Generic types
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

// Traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth_ReturnAdaptivityRequests.hpp"

// Flux solvers
#include "../../../subsimulation_generic/flux_solver/CFluxSolver.hpp"

// Edge projections
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"

// Triangle side lengths
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

// Triangle tools
#include "libsierpi/triangle/CTriangle_Tools.hpp"

// PDE lab stuff
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"

// Enum for boundary conditions
#include "libsierpi/grid/CBoundaryConditions.hpp"

// Default parameters for edge comm
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Parameters.hpp"

#include "../../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"

// Check for set buoys
#include "../../../CParameters.hpp"

#include "global_config.h"



namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (op_cell)
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Hyperbolic_Order_N	:
	public CEdgeComm_Parameters		// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_Parameters::setTimestepSize;
	using CEdgeComm_Parameters::setGravitationalConstant;
	using CEdgeComm_Parameters::setBoundaryDirichlet;
	using CEdgeComm_Parameters::setParameters;


	/**
	 * typedefs for convenience
	 */
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth_ReturnAdaptivityRequests<CEdgeComm_Hyperbolic_Order_N<t_storeElementUpdatesOnly>, CHyperbolicTypes > TRAV;

	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,2> CGaussTriangleEdge;


	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	inline void migration_send(int i_dst_rank)
	{
		sierpi::CMigration::sendRawClass((CEdgeComm_Parameters&)(*this), i_dst_rank);
	}

	inline void migration_send_postprocessing(int i_dst_rank)
	{
	}

	inline void migration_recv(int i_src_rank)
	{
		sierpi::CMigration::recvRawClass((CEdgeComm_Parameters&)(*this), i_src_rank);
	}

#endif



	/*
	 * storage for vertical fluxes to compute timestep condition
	 */
	class CVerticalFluxes
	{
	public:
		CSimulationVerticalFaceData fromUpper[SIMULATION_NUMBER_OF_LAYERS+1];
		CSimulationVerticalFaceData fromLower[SIMULATION_NUMBER_OF_LAYERS+1];
	};

	CStack<CVerticalFluxes> cVerticalFluxes;

	/*
	 * traversal prehook override to setup vertical flux storage
	 */
	inline void traversal_pre_hook(
			size_t i_number_of_cells
	)
	{
		CEdgeComm_Parameters::traversal_pre_hook();

		if (cVerticalFluxes.getMaxNumberOfElements() != i_number_of_cells)
		{
			cVerticalFluxes.resize(i_number_of_cells);
		}

		cVerticalFluxes.clear();
	}


	inline void traversal_post_hook()
	{
		CEdgeComm_Parameters::traversal_post_hook();
	}



	/**
	 * compute the vertical fluxes
	 *
	 * \return the timestep size for CFL=1
	 */
	T p_compute_vertical_fluxes(
			const CCellData *i_cCellData				///< cell data
	)
	{
		CVerticalFluxes &output = cVerticalFluxes.fakePush_returnRef();

		// face data computed for upward oriented faces
		CSimulationVerticalFaceData cVerticalFaceData_input_up[SIMULATION_NUMBER_OF_LAYERS+1];

		// face data computed for downward oriented faces
		CSimulationVerticalFaceData cVerticalFaceData_input_down[SIMULATION_NUMBER_OF_LAYERS+1];


		/****************************************
		 * 3 layers
		 ****************************************
		 *
		 * UP
		 *    |     |
		 *    |-----|
		 *    |     | 0
		 *    |     |
		 *    |-----|
		 *    |     | 1
		 *    |     |
		 *    |-----|
		 *    |     | 2
		 *    |     |
		 *    |-----|
		 *    |     | 3 (boundary condition)
		 *
		 *
		 * DOWN
		 *    |     | 0 (boundary condition)
		 *    |-----|
		 *    |     |
		 *    |     | 1
		 *    |-----|
		 *    |     |
		 *    |     | 2
		 *    |-----|
		 *    |     |
		 *    |     | 3
		 *    |-----|
		 *    |     |
		 *
		 *
		 */

		/*
		 * setup boundaries
		 */
		cVerticalFaceData_input_up[SIMULATION_NUMBER_OF_LAYERS] = boundary_dirichlet_vertical_top_face;
		cVerticalFaceData_input_down[0] = boundary_dirichlet_vertical_bottom_face;

		/*
		 * compute face DOFs
		 */
		T max_wave_speed = 0;

		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_top_face(i_cCellData->dofs.layers[j].r, cVerticalFaceData_input_up[j].dofs.r);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_top_face(i_cCellData->dofs.layers[j].ru, cVerticalFaceData_input_up[j].dofs.ru);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_top_face(i_cCellData->dofs.layers[j].rv, cVerticalFaceData_input_up[j].dofs.rv);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_top_face(i_cCellData->dofs.layers[j].rw, cVerticalFaceData_input_up[j].dofs.rw);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_top_face(i_cCellData->dofs.layers[j].e, cVerticalFaceData_input_up[j].dofs.e);

			T tmp_max_wave_speed_left;
			T tmp_max_wave_speed_right;

			fluxSolver.op_vertical_edge_edge(
					cVerticalFaceData_input_up[j],
					cVerticalFaceData_input_down[j],
					&output.fromUpper[j],
					&output.fromLower[j],
					&tmp_max_wave_speed_left,
					&tmp_max_wave_speed_right,
					gravitational_constant
				);

			output.fromUpper[j].CFL1_scalar = tmp_max_wave_speed_left;
			output.fromLower[j].CFL1_scalar = tmp_max_wave_speed_right;

			max_wave_speed = std::max(max_wave_speed, tmp_max_wave_speed_left);
			max_wave_speed = std::max(max_wave_speed, tmp_max_wave_speed_right);

			CDG_MatrixComputations_3D::mul_timestep_dofs_to_bottom_face(i_cCellData->dofs.layers[j].r, cVerticalFaceData_input_down[j+1].dofs.r);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_bottom_face(i_cCellData->dofs.layers[j].ru, cVerticalFaceData_input_down[j+1].dofs.ru);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_bottom_face(i_cCellData->dofs.layers[j].rv, cVerticalFaceData_input_down[j+1].dofs.rv);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_bottom_face(i_cCellData->dofs.layers[j].rw, cVerticalFaceData_input_down[j+1].dofs.rw);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_bottom_face(i_cCellData->dofs.layers[j].e, cVerticalFaceData_input_down[j+1].dofs.e);
		}

		/*
		 * handle last layer
		 */
		{
			T tmp_max_wave_speed_left;
			T tmp_max_wave_speed_right;

			fluxSolver.op_vertical_edge_edge(
					cVerticalFaceData_input_up[SIMULATION_NUMBER_OF_LAYERS],
					cVerticalFaceData_input_down[SIMULATION_NUMBER_OF_LAYERS],
					&output.fromUpper[SIMULATION_NUMBER_OF_LAYERS],
					&output.fromLower[SIMULATION_NUMBER_OF_LAYERS],
					&tmp_max_wave_speed_left,
					&tmp_max_wave_speed_right,
					gravitational_constant
				);

			output.fromUpper[SIMULATION_NUMBER_OF_LAYERS].CFL1_scalar = tmp_max_wave_speed_left;
			output.fromLower[SIMULATION_NUMBER_OF_LAYERS].CFL1_scalar = tmp_max_wave_speed_right;

			max_wave_speed = std::max(max_wave_speed, tmp_max_wave_speed_left);
			max_wave_speed = std::max(max_wave_speed, tmp_max_wave_speed_right);
		}

		return max_wave_speed;
	}


	/**
	 * constructor
	 */
	CEdgeComm_Hyperbolic_Order_N()
	{
		T setup_values[5] = {std::numeric_limits<T>::infinity(), std::numeric_limits<T>::infinity(), std::numeric_limits<T>::infinity(), std::numeric_limits<T>::infinity(), std::numeric_limits<T>::infinity()};
		boundary_dirichlet.setupDefaultValues(setup_values);
	}



#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1

	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CCellData &i_f_t0,
			CCellData &i_f_slope_t0,
			CCellData *o_f_t1_approx,
			T i_timestep_size
	)	{
#error "not supported, copy from tsunami subsimulation"
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_f_t1_approx->dofs.r[i]	= i_f_t0.dofs.r[i]	+ i_timestep_size*i_f_slope_t0.dofs.r[i];
			o_f_t1_approx->dofs.ru[i]	= i_f_t0.dofs.ru[i]	+ i_timestep_size*i_f_slope_t0.dofs.ru[i];
			o_f_t1_approx->dofs.rv[i]	= i_f_t0.dofs.rv[i]	+ i_timestep_size*i_f_slope_t0.dofs.rv[i];
			o_f_t1_approx->dofs.rw[i]	= i_f_t0.dofs.rw[i]	+ i_timestep_size*i_f_slope_t0.dofs.rw[i];
			o_f_t1_approx->dofs.e[i]	= i_f_t0.dofs.e[i]	+ i_timestep_size*i_f_slope_t0.dofs.e[i];
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CCellData &i_f_slope_t0,
			CCellData &i_f_slope_t1,
			CCellData *io_f_t0_t1,
			T i_timestep_size
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			io_f_t0_t1->dofs.r[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.r[i] + i_f_slope_t1.dofs.r[i]);
			io_f_t0_t1->dofs.ru[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.ru[i] + i_f_slope_t1.dofs.ru[i]);
			io_f_t0_t1->dofs.rv[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.rv[i] + i_f_slope_t1.dofs.rv[i]);
			io_f_t0_t1->dofs.rw[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.rw[i] + i_f_slope_t1.dofs.rw[i]);
			io_f_t0_t1->dofs.e[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.e[i] + i_f_slope_t1.dofs.e[i]);
		}

		// TODO: which CFL to use?
//		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE != 1
#error "RK2 only with adaptivity mode 1 supported so far due to adaptivity restrictions of flux requirements!"
#endif

		// adaptivity is handled separately
	}
#endif



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Hyperbolic_Order_N &i_kernel
	)	{
		// copy configuration
		(CEdgeComm_Parameters&)(*this) = (CEdgeComm_Parameters&)(i_kernel);
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			// R
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.layers[j].r, o_cEdgeData->dofs.layers[j].r);

			// RU, RV
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].ru);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.layers[j].rv, o_cEdgeData->dofs.layers[j].rv);
	#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			CDG_MatrixComputations_3D::mul_edge_comm_transform_to_edge_space(i_cCellData->hypEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
	#else
			CDG_MatrixComputations_3D::mul_edge_comm_hyp_project_to_edge_space(o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
	#endif
			// RW
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.layers[j].rw, o_cEdgeData->dofs.layers[j].rw);

			// E
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.layers[j].e, o_cEdgeData->dofs.layers[j].e);
		}

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif


		/*
		 * VERTICAL FLUX COMPUTATIONS
		 */
		T max_vertical_wave_speed = p_compute_vertical_fluxes(i_cCellData);

		// update CFL
		T unit_domain_height_to_length_ratio = 0.01;

		cfl1_min_value = std::min(cfl1_min_value, o_cEdgeData->CFL1_scalar/max_vertical_wave_speed*unit_domain_height_to_length_ratio);
//		std::cout << cfl1_min_value << std::endl;



		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupHypEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			// R
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.layers[j].r, o_cEdgeData->dofs.layers[j].r);

			// RU, RV
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].ru);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.layers[j].rv, o_cEdgeData->dofs.layers[j].rv);
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			CDG_MatrixComputations_3D::mul_edge_comm_transform_to_edge_space(i_cCellData->rightEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
#else
			CDG_MatrixComputations_3D::mul_edge_comm_right_project_to_edge_space(o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
#endif
			// RW
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.layers[j].rw, o_cEdgeData->dofs.layers[j].rw);
			// E
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.layers[j].e, o_cEdgeData->dofs.layers[j].e);
		}

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRightEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			// R
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.layers[j].r, o_cEdgeData->dofs.layers[j].r);

			// RU, RV
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].ru);
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.layers[j].rv, o_cEdgeData->dofs.layers[j].rv);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1
			CDG_MatrixComputations_3D::mul_edge_comm_transform_to_edge_space(i_cCellData->leftEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
#else
			CDG_MatrixComputations_3D::mul_edge_comm_left_project_to_edge_space(o_cEdgeData->dofs.layers[j].ru, o_cEdgeData->dofs.layers[j].rv);
#endif
			// RW
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.layers[j].rw, o_cEdgeData->dofs.layers[j].rw);
			// E
			CDG_MatrixComputations_3D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.layers[j].e, o_cEdgeData->dofs.layers[j].e);
		}

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupLeftEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)
	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
				}
			}
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_hyp(
				i_hyp_normal_x,		i_hyp_normal_y,
				i_right_normal_x,	i_right_normal_y,
				i_left_normal_x,	i_left_normal_y,

				i_depth,
				i_cCellData,
				o_cEdgeData
			);

			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			/*
			 * apply bounce back boundary condition only on component perpendicular to boundary!
			 */
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
			break;


		case BOUNDARY_CONDITION_DATASET:
			CSimulationNodeData n;
			if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
			{
				for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				{
					for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					{
						o_cEdgeData->dofs.layers[j].r[i] = n.r;
						o_cEdgeData->dofs.layers[j].ru[i] = n.ru;
						o_cEdgeData->dofs.layers[j].rv[i] = n.rv;
						o_cEdgeData->dofs.layers[j].rw[i] = n.rw;
						o_cEdgeData->dofs.layers[j].e[i] = n.e;

						CTriangle_VectorProjections::worldToReference(
										&o_cEdgeData->dofs.layers[j].ru[i],
										&o_cEdgeData->dofs.layers[j].rv[i],
										-i_right_normal_x,
										-i_right_normal_y
								);

						CTriangle_VectorProjections::toEdgeSpace<T,0>(
							&(o_cEdgeData->dofs.layers[j].ru[i]),
							&(o_cEdgeData->dofs.layers[j].rv[i])
						);
					}
				}
			}
			else
			{
				// default: outflow boundary condition
				op_cell_to_edge_hyp(
						i_hyp_normal_x,		i_hyp_normal_y,
						i_right_normal_x,	i_right_normal_y,
						i_left_normal_x,	i_left_normal_y,

						i_depth,
						i_cCellData,
						o_cEdgeData
					);
			}
			break;

		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].r[i] = SIMULATION_DATASET_DEFAULT_VALUE_0;
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
					o_cEdgeData->dofs.layers[j].e[i] = SIMULATION_DATASET_DEFAULT_VALUE_4;
				}
			}
			break;
		}



		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
			{
				o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
				o_cEdgeData->dofs.layers[j].rv[i] = -o_cEdgeData->dofs.layers[j].rv[i];
				o_cEdgeData->dofs.layers[j].rv[i] = 0;
				o_cEdgeData->dofs.layers[j].rw[i] = 0;
			}
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cCellData->validation.setupHypEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
				}
			}
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
			break;

		case BOUNDARY_CONDITION_DATASET:
			CSimulationNodeData n;
			if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
			{
				for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				{
					for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					{
						o_cEdgeData->dofs.layers[j].r[i] = n.r;
						o_cEdgeData->dofs.layers[j].ru[i] = n.ru;
						o_cEdgeData->dofs.layers[j].rv[i] = n.rv;
						o_cEdgeData->dofs.layers[j].rw[i] = n.rw;
						o_cEdgeData->dofs.layers[j].e[i] = n.e;

						CTriangle_VectorProjections::worldToReference(
										&o_cEdgeData->dofs.layers[j].ru[i],
										&o_cEdgeData->dofs.layers[j].rv[i],
										-i_right_normal_x,
										-i_right_normal_y
								);

						CTriangle_VectorProjections::toEdgeSpace<T,1>(
							&(o_cEdgeData->dofs.layers[j].ru[i]),
							&(o_cEdgeData->dofs.layers[j].rv[i])
						);
					}
				}
			}
			else
			{
				// default: outflow boundary condition
				op_cell_to_edge_right(
						i_hyp_normal_x,		i_hyp_normal_y,
						i_right_normal_x,	i_right_normal_y,
						i_left_normal_x,	i_left_normal_y,

						i_depth,
						i_cCellData,
						o_cEdgeData
					);
			}
			break;


		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].r[i] = SIMULATION_DATASET_DEFAULT_VALUE_0;
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
					o_cEdgeData->dofs.layers[j].e[i] = SIMULATION_DATASET_DEFAULT_VALUE_4;
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
			{
				o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
				o_cEdgeData->dofs.layers[j].rv[i] = -o_cEdgeData->dofs.layers[j].rv[i];
				o_cEdgeData->dofs.layers[j].rv[i] = 0;
				o_cEdgeData->dofs.layers[j].rw[i] = 0;
			}
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cCellData->validation.setupRightEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
				}
			}
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
			break;

		case BOUNDARY_CONDITION_DATASET:

			CSimulationNodeData n;
			if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
			{
				for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				{
					for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
					{
						o_cEdgeData->dofs.layers[j].r[i] = n.r;
						o_cEdgeData->dofs.layers[j].ru[i] = n.ru;
						o_cEdgeData->dofs.layers[j].rv[i] = n.rv;
						o_cEdgeData->dofs.layers[j].rw[i] = n.rw;
						o_cEdgeData->dofs.layers[j].e[i] = n.e;

						CTriangle_VectorProjections::worldToReference(
										&o_cEdgeData->dofs.layers[j].ru[i],
										&o_cEdgeData->dofs.layers[j].rv[i],
										-i_right_normal_x,
										-i_right_normal_y
								);

						CTriangle_VectorProjections::toEdgeSpace<T,2>(
							&(o_cEdgeData->dofs.layers[j].ru[i]),
							&(o_cEdgeData->dofs.layers[j].rv[i])
						);
					}
				}
			}
			else
			{
				// default: outflow boundary condition
				op_cell_to_edge_left(
						i_hyp_normal_x,		i_hyp_normal_y,
						i_right_normal_x,	i_right_normal_y,
						i_left_normal_x,	i_left_normal_y,

						i_depth,
						i_cCellData,
						o_cEdgeData
					);
			}
			break;


		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
				{
					o_cEdgeData->dofs.layers[j].r[i] = SIMULATION_DATASET_DEFAULT_VALUE_0;
					o_cEdgeData->dofs.layers[j].ru[i] = 0;
					o_cEdgeData->dofs.layers[j].rv[i] = 0;
					o_cEdgeData->dofs.layers[j].rw[i] = 0;
					o_cEdgeData->dofs.layers[j].e[i] = SIMULATION_DATASET_DEFAULT_VALUE_4;
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
			{
				o_cEdgeData->dofs.layers[j].ru[i] = -o_cEdgeData->dofs.layers[j].ru[i];
				o_cEdgeData->dofs.layers[j].rv[i] = -o_cEdgeData->dofs.layers[j].rv[i];
				o_cEdgeData->dofs.layers[j].rv[i] = 0;
				o_cEdgeData->dofs.layers[j].rw[i] = 0;
			}
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cCellData->validation.setupLeftEdgeCommData(&o_cEdgeData->validation);
#endif
	}

	/**
	 * compute flux components
	 */
	template <int N>
	inline void p_compute_fluxes(
		const CSimulationNodeDataLayeredSOA<N> &i_node_data,	///< nodal data
		CSimulationNodeDataLayeredSOA<N> *o_flux_data_x,		///< flux x-component
		CSimulationNodeDataLayeredSOA<N> *o_flux_data_y,		///< flux y-component
		CSimulationNodeDataLayeredSOA<N> *o_flux_data_z		///< flux z-component
	)	{

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			typename CSimulationNodeDataLayeredSOA<N>::CSingleLayer const &layer_data = i_node_data.layers[j];
			typename CSimulationNodeDataLayeredSOA<N>::CSingleLayer &flux_data_x = o_flux_data_x->layers[j];
			typename CSimulationNodeDataLayeredSOA<N>::CSingleLayer &flux_data_y = o_flux_data_y->layers[j];
			typename CSimulationNodeDataLayeredSOA<N>::CSingleLayer &flux_data_z = o_flux_data_z->layers[j];

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
			{
				T u = layer_data.ru[i]/layer_data.r[i];
				T v = layer_data.rv[i]/layer_data.r[i];
				T w = layer_data.rw[i]/layer_data.r[i];

#if CONFIG_EULER_GAS_MODEL==0
				T p = (layer_data.e[i]-(T)0.5*layer_data.r[i]*std::sqrt(u*u+v*v+w*w))*((T)CONFIG_EULER_GAS_GAMMA-(T)1);
#elif CONFIG_EULER_GAS_MODEL==1
				T p = layer_data.r[i]*(T)CONFIG_EULER_GAS_A_SQUARED;
#else
#	error "unknown gas model"
#endif

				/**
				 * see leveque, FV methods, page 431
				 *
				 * EULER FLUX EQUATIONS
				 */
				flux_data_x.r[i]  =  layer_data.ru[i];
				flux_data_x.ru[i] = (layer_data.ru[i]*u)+p;
				flux_data_x.rv[i] =  layer_data.ru[i]*v;
				flux_data_x.rw[i] =  layer_data.ru[i]*w;
				flux_data_x.e[i]  = (layer_data.e[i]+p)*u;

				flux_data_y.r[i]  =  layer_data.rv[i];
				flux_data_y.ru[i] =  layer_data.rv[i]*u;
				flux_data_y.rv[i] = (layer_data.rv[i]*v)+p;
				flux_data_y.rw[i] =  layer_data.rv[i]*w;
				flux_data_y.e[i]  = (layer_data.e[i]+p)*v;

				flux_data_z.r[i]  =  layer_data.rw[i];
				flux_data_z.ru[i] =  layer_data.rw[i]*u;
				flux_data_z.rv[i] =  layer_data.rw[i]*v;
				flux_data_z.rw[i] = (layer_data.rw[i]*w)+p;
				flux_data_z.e[i]  = (layer_data.e[i]+p)*w;

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 1 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 2
				if (std::abs(layer_data.r[i]) <= SIMULATION_EULER_ZERO_THRESHOLD)
				{
					flux_data_x.r[i] = 0;
					flux_data_x.ru[i] = 0;
					flux_data_x.rv[i] = 0;
					flux_data_x.rw[i] = 0;
					flux_data_x.e[i] = 0;

					flux_data_y.r[i] = 0;
					flux_data_y.ru[i] = 0;
					flux_data_y.rv[i] = 0;
					flux_data_y.rw[i] = 0;
					flux_data_y.e[i] = 0;

					flux_data_z.r[i] = 0;
					flux_data_z.ru[i] = 0;
					flux_data_z.rv[i] = 0;
					flux_data_z.rw[i] = 0;
					flux_data_z.e[i] = 0;
				}
#endif
			}
		}
	}



	inline unsigned char p_computeAdaptivityRequest(
			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update,		///< incoming flux from left edge

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T hyp_edge_length,
			T right_edge_length,
			T left_edge_length
#else
			T cat_edge_length
#endif
	)
	{

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==1
		/**
		 * ADAPTIVITY CRITERIA: density
		 */

		T r = 0;
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			T lr;
			CDG_MatrixComputations_3D::mul_convert_dofs_to_0th_order(io_cCellData->dofs.layers[j].r, &lr);
			r = std::max(r, lr);
		}

		if (std::abs(r) >= refine_threshold)
			return TRAV::ADAPTIVITY_REFINE;

		if (std::abs(r) < coarsen_threshold)
			return TRAV::ADAPTIVITY_COARSEN;

		return TRAV::ADAPTIVITY_NOTHING;


#elif SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

#error "SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2 not available"
		/*
		 * this adaptivity criteria is based on the 1st component of the Net-Updates
		 */
		T edge_weighted_flux = 0;
		T h = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
		{
			T t = std::abs(i_hyp_edge_net_update->dofs.r[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*hyp_edge_length;
#else
			T te = t;
#endif
			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		edge_weighted_flux *= (T)M_SQRT2;
#endif

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
		{
			T t = std::abs(i_right_edge_net_update->dofs.r[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*right_edge_length;
#else
			T te = t;
#endif
			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
		{
			T t = std::abs(i_left_edge_net_update->dofs.r[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*left_edge_length;
#else
			T te = t;
#endif

			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		edge_weighted_flux *= cat_edge_length;
#endif

		T inv_h;
		if (h > SIMULATION_EULER_DRY_THRESHOLD)
			inv_h = (T)1.0/h;
		else
			inv_h = 1.0/SIMULATION_EULER_DRY_THRESHOLD;

		assert(edge_weighted_flux*inv_h >= 0);

		if (edge_weighted_flux*inv_h > refine_threshold)
			return TRAV::ADAPTIVITY_REFINE;

		if (edge_weighted_flux*inv_h <= coarsen_threshold)
			return TRAV::ADAPTIVITY_COARSEN;

#else

#	error "unhandled adaptivity mode"

#endif

		return 0;
	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline unsigned char op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			int i_depth,

			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update		///< incoming flux from left edge
	)	{

#if DEBUG
#	if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
				assert(io_cCellData->dofs.layers[j].r[i] >= 0);
#	else
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			assert(io_cCellData->dofs.layers[j].r[0] >= 0);
#	endif
#endif



#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION

		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cCellData->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
			);
#endif


		static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		/**
		 * DOF updates to apply to the cell dof's
		 */
		CSimulationNodeDataLayeredSOA<N> cDofUpdatesSOA;

		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			for (int i = 0; i < N; i++){
				cDofUpdatesSOA.layers[j].r[i] = 0;
				cDofUpdatesSOA.layers[j].ru[i] = 0;
				cDofUpdatesSOA.layers[j].rv[i] = 0;
				cDofUpdatesSOA.layers[j].rw[i] = 0;
				cDofUpdatesSOA.layers[j].e[i] = 0;
			}
		}



		/*
		 * compute vertical fluxes
		 *
		 * => This is moved to the hypotenuse edge computations
		 */
//		p_compute_vertical_fluxes(io_cCellData);

		/*
		 * return output
		 */
		CVerticalFluxes &output = cVerticalFluxes.pop_returnRef();


		/*
		 * top/bottom face fluxes to dofs
		 */
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			CDG_MatrixComputations_3D::madd_timestep_top_fluxes_to_dofs(output.fromUpper[j].dofs.r, cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_top_fluxes_to_dofs(output.fromUpper[j].dofs.ru, cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_top_fluxes_to_dofs(output.fromUpper[j].dofs.rv, cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_top_fluxes_to_dofs(output.fromUpper[j].dofs.rw, cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_top_fluxes_to_dofs(output.fromUpper[j].dofs.e, cDofUpdatesSOA.layers[j].e);

			CDG_MatrixComputations_3D::madd_timestep_bottom_fluxes_to_dofs(output.fromLower[j+1].dofs.r, cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_bottom_fluxes_to_dofs(output.fromLower[j+1].dofs.ru, cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_bottom_fluxes_to_dofs(output.fromLower[j+1].dofs.rv, cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_bottom_fluxes_to_dofs(output.fromLower[j+1].dofs.rw, cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_bottom_fluxes_to_dofs(output.fromLower[j+1].dofs.e, cDofUpdatesSOA.layers[j].e);
		}



		/**
		 * INCOMING FLUX from edge face to reference element rotation
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
			{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

				// --- hyp edge transformation ---
				CTriangle_VectorProjections::fromEdgeSpace(
						io_cCellData->hypEdgeRotationMatrix,
						&i_hyp_edge_net_update->dofs.layers[j].ru[i],
						&i_hyp_edge_net_update->dofs.layers[j].rv[i]
				);

				CTriangle_VectorProjections::matrixTransformation(
						io_cCellData->transformationMatrix,
						&i_hyp_edge_net_update->dofs.layers[j].ru[i],
						&i_hyp_edge_net_update->dofs.layers[j].rv[i]
				);


				// --- right edge transformation ---
				CTriangle_VectorProjections::fromEdgeSpace(
						io_cCellData->rightEdgeRotationMatrix,
						&i_right_edge_net_update->dofs.layers[j].ru[i],
						&i_right_edge_net_update->dofs.layers[j].rv[i]
				);

				CTriangle_VectorProjections::matrixTransformation(
						io_cCellData->transformationMatrix,
						&i_right_edge_net_update->dofs.layers[j].ru[i],
						&i_right_edge_net_update->dofs.layers[j].rv[i]
				);


				// --- left edge transformation ---
				CTriangle_VectorProjections::fromEdgeSpace(
						io_cCellData->leftEdgeRotationMatrix,
						&i_left_edge_net_update->dofs.layers[j].ru[i],
						&i_left_edge_net_update->dofs.layers[j].rv[i]
				);

				CTriangle_VectorProjections::matrixTransformation(
						io_cCellData->transformationMatrix,
						&i_left_edge_net_update->dofs.layers[j].ru[i],
						&i_left_edge_net_update->dofs.layers[j].rv[i]
				);

#else

				CTriangle_VectorProjections::fromHypEdgeSpace(
						&i_hyp_edge_net_update->dofs.layers[j].ru[i],
						&i_hyp_edge_net_update->dofs.layers[j].rv[i]
					);

				CTriangle_VectorProjections::fromRightEdgeSpace(
						&i_right_edge_net_update->dofs.layers[j].ru[i],
						&i_right_edge_net_update->dofs.layers[j].rv[i]
					);

				CTriangle_VectorProjections::fromLeftEdgeSpace(
						&i_left_edge_net_update->dofs.layers[j].ru[i],
						&i_left_edge_net_update->dofs.layers[j].rv[i]
					);

#endif
			}
		}


		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		assert(cathetus_real_length > 0);
#endif


#if DEBUG
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				if (std::isnan(io_cCellData->dofs.layers[j].r[i]))
				{
					instabilityDetected = true;
					if (!instabilityDetected)
						std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
					return 0;
				}
			}
		}
#endif


		/*
		 * edge face FLUXES to DOF updates
		 *
		 * 0: hyp
		 * 1: right
		 * 2: left
		 */
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1

		T triangle_area = io_cCellData->cellArea;

		T hyp_edge_length = io_cCellData->edgeLength[0];
		T right_edge_length = io_cCellData->edgeLength[1];
		T left_edge_length = io_cCellData->edgeLength[2];

		assert(N=1);
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			// multiply with edge lengths
			for (int i = 0; i < N; i++)
			{
				cDofUpdatesSOA.layers[j].h[i] -= i_hyp_edge_net_update->dofs.layers[j].r[i] * hyp_edge_length;
				cDofUpdatesSOA.layers[j].hu[i] -= i_hyp_edge_net_update->dofs.layers[j].ru[i] * hyp_edge_length;
				cDofUpdatesSOA.layers[j].hv[i] -= i_hyp_edge_net_update->dofs.layers[j].rv[i] * hyp_edge_length;
				cDofUpdatesSOA.layers[j].hw[i] -= i_hyp_edge_net_update->dofs.layers[j].rw[i] * hyp_edge_length;
				cDofUpdatesSOA.layers[j].e[i] -= i_hyp_edge_net_update->dofs.layers[j].e[i] * hyp_edge_length;

				cDofUpdatesSOA.layers[j].h[i] -= i_right_edge_net_update->dofs.layers[j].r[i] * right_edge_length;
				cDofUpdatesSOA.layers[j].hu[i] -= i_right_edge_net_update->dofs.layers[j].ru[i] * right_edge_length;
				cDofUpdatesSOA.layers[j].hv[i] -= i_right_edge_net_update->dofs.layers[j].rv[i] * right_edge_length;
				cDofUpdatesSOA.layers[j].hw[i] -= i_right_edge_net_update->dofs.layers[j].rw[i] * right_edge_length;
				cDofUpdatesSOA.layers[j].e[i] -= i_right_edge_net_update->dofs.layers[j].e[i] * right_edge_length;

				cDofUpdatesSOA.layers[j].h[i] -= i_left_edge_net_update->dofs.layers[j].r[i] * left_edge_length;
				cDofUpdatesSOA.layers[j].hu[i] -= i_left_edge_net_update->dofs.layers[j].ru[i] * left_edge_length;
				cDofUpdatesSOA.layers[j].hv[i] -= i_left_edge_net_update->dofs.layers[j].rv[i] * left_edge_length;
				cDofUpdatesSOA.layers[j].hw[i] -= i_left_edge_net_update->dofs.layers[j].rw[i] * left_edge_length;
				cDofUpdatesSOA.layers[j].e[i] -= i_left_edge_net_update->dofs.layers[j].e[i] * left_edge_length;
			}
		}

#else

		T cat_edge_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
//		T height_edge_length = cell_real_height;


		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
			CDG_MatrixComputations_3D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.layers[j].r, cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.layers[j].ru, cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.layers[j].rv, cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.layers[j].rw, cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.layers[j].e, cDofUpdatesSOA.layers[j].e);

			// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
			CDG_MatrixComputations_3D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.layers[j].r, cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.layers[j].ru, cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.layers[j].rv, cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.layers[j].rw, cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.layers[j].e, cDofUpdatesSOA.layers[j].e);

			// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
			CDG_MatrixComputations_3D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.layers[j].r, cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.layers[j].ru, cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.layers[j].rv, cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.layers[j].rw, cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.layers[j].e, cDofUpdatesSOA.layers[j].e);
		}

#endif

#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS > 1

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
#	error "Distorted grids not yet supported for higher order methods"
#endif

		/*
		 * compute flux
		 */
		CSimulationNodeDataLayeredSOA<N> flux_x;
		CSimulationNodeDataLayeredSOA<N> flux_y;
		CSimulationNodeDataLayeredSOA<N> flux_z;

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE==0

		p_compute_fluxes(io_cCellData->dofs, &flux_x, &flux_y, &flux_z);

#else

		/*
		 * for non-nodal basis, we
		 *
		 * 1) first convert the modal to nodal basis,
		 *
		 * 2) compute fluxes and finally
		 *
		 * 3) convert back from nodal to modal basis.
		 */

		CSimulationNodeDataLayeredSOA<N> dofs_in_nodal_basis;
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			CDG_MatrixComputations_3D::mul_timestep_stiffness_dofs_to_nodes(io_cCellData->dofs.layers[j].r,		dofs_in_nodal_basis.layers[j].r);
			CDG_MatrixComputations_3D::mul_timestep_stiffness_dofs_to_nodes(io_cCellData->dofs.layers[j].ru,	dofs_in_nodal_basis.layers[j].ru);
			CDG_MatrixComputations_3D::mul_timestep_stiffness_dofs_to_nodes(io_cCellData->dofs.layers[j].rv,	dofs_in_nodal_basis.layers[j].rv);
			CDG_MatrixComputations_3D::mul_timestep_stiffness_dofs_to_nodes(io_cCellData->dofs.layers[j].rw,	dofs_in_nodal_basis.layers[j].rw);
			CDG_MatrixComputations_3D::mul_timestep_stiffness_dofs_to_nodes(io_cCellData->dofs.layers[j].e,	dofs_in_nodal_basis.layers[j].e);
		}

		p_compute_fluxes(dofs_in_nodal_basis, &flux_x, &flux_y, &flux_z);

#endif


		/*
		 * compute stiffness terms
		 */
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			CDG_MatrixComputations_3D::madd_timestep_stiffness_u(flux_x.layers[j].r,	cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_u(flux_x.layers[j].ru,	cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_u(flux_x.layers[j].rv,	cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_u(flux_x.layers[j].rw,	cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_u(flux_x.layers[j].e,	cDofUpdatesSOA.layers[j].e);

			CDG_MatrixComputations_3D::madd_timestep_stiffness_v(flux_y.layers[j].r,	cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_v(flux_y.layers[j].ru,	cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_v(flux_y.layers[j].rv,	cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_v(flux_y.layers[j].rw,	cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_v(flux_y.layers[j].e,	cDofUpdatesSOA.layers[j].e);

			CDG_MatrixComputations_3D::madd_timestep_stiffness_w(flux_z.layers[j].r,	cDofUpdatesSOA.layers[j].r);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_w(flux_z.layers[j].ru,	cDofUpdatesSOA.layers[j].ru);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_w(flux_z.layers[j].rv,	cDofUpdatesSOA.layers[j].rv);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_w(flux_z.layers[j].rw,	cDofUpdatesSOA.layers[j].rw);
			CDG_MatrixComputations_3D::madd_timestep_stiffness_w(flux_z.layers[j].e,	cDofUpdatesSOA.layers[j].e);
		}

#endif


#if DEBUG
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			if (	std::isnan(cDofUpdatesSOA.layers[j].ru[0])	||
					std::isnan(cDofUpdatesSOA.layers[j].rv[0])	||
					std::isnan(cDofUpdatesSOA.layers[j].rw[0])
			)	{
				std::cerr << "op_cell update instability" << std::endl;
				std::cerr << *io_cCellData << std::endl;
				std::cerr << cDofUpdatesSOA << std::endl;

				std::cerr << "net updates:" << std::endl;
				std::cerr << *i_hyp_edge_net_update << std::endl;
				std::cerr << *i_right_edge_net_update << std::endl;
				std::cerr << *i_left_edge_net_update << std::endl;
				throw(std::runtime_error("op_cell update instability"));
			}
		}
#endif

		/*
		 * compute timestep update
		 */
		if (t_storeElementUpdatesOnly)
		{
#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1
			std::cerr << "RK > 1 not supported yet for higher order methods" << std::endl;
			assert(false);
#endif

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			/*
			 * Runge Kutta
			 */
			// dimensional update
			T alpha = 1.0/cat_edge_length;

			std::cerr << "RK not tested and accurately implement for this simulation" << std::endl;
			assert(false);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].r[i] *= alpha;
				CDG_MatrixComputations_3D::mul_timestep_inv_mass(cDofUpdatesSOA.layers[j].r, io_cCellData->dofs.layers[j].r);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].ru[i] *= alpha;
				CDG_MatrixComputations_3D::mul_timestep_inv_mass(cDofUpdatesSOA.layers[j].ru, io_cCellData->dofs.layers[j].ru);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].rv[i] *= alpha;
				CDG_MatrixComputations_3D::mul_timestep_inv_mass(cDofUpdatesSOA.layers[j].rv, io_cCellData->dofs.layers[j].rv);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].rw[i] *= alpha;
				CDG_MatrixComputations_3D::mul_timestep_inv_mass(cDofUpdatesSOA.layers[j].rw, io_cCellData->dofs.layers[j].rw);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].e[i] *= alpha;
				CDG_MatrixComputations_3D::mul_timestep_inv_mass(cDofUpdatesSOA.layers[j].e, io_cCellData->dofs.layers[j].e);
			}
#endif
		}
		else
		{
			/*
			 * Explicit Euler
			 */

			// dimensional update
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T alpha = timestep_size/triangle_area;
#else
			T alpha = timestep_size/cat_edge_length;
//			T beta = timestep_size/cell_real_height;
#endif


#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].r[i] *= alpha;

				CDG_MatrixComputations_3D::madd_timestep_inv_mass(cDofUpdatesSOA.layers[j].r, io_cCellData->dofs.layers[j].r);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].ru[i] *= alpha;

				CDG_MatrixComputations_3D::madd_timestep_inv_mass(cDofUpdatesSOA.layers[j].ru, io_cCellData->dofs.layers[j].ru);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].rv[i] *= alpha;
				CDG_MatrixComputations_3D::madd_timestep_inv_mass(cDofUpdatesSOA.layers[j].rv, io_cCellData->dofs.layers[j].rv);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].rw[i] *= alpha;
				CDG_MatrixComputations_3D::madd_timestep_inv_mass(cDofUpdatesSOA.layers[j].rw, io_cCellData->dofs.layers[j].rw);
			}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
					cDofUpdatesSOA.layers[j].e[i] *= alpha;
				CDG_MatrixComputations_3D::madd_timestep_inv_mass(cDofUpdatesSOA.layers[j].e, io_cCellData->dofs.layers[j].e);
			}

#if SIMULATION_LINEAR_VELOCITY_DAMPING

			// artificial velocity damping
			T f = std::pow(SIMULATION_LINEAR_VELOCITY_DAMPING_FACTOR, timestep_size);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				for (int i = 0; i < N; i++)
				{
					io_cCellData->dofs.layers[j].ru[i] *= f;
					io_cCellData->dofs.layers[j].rv[i] *= f;
					io_cCellData->dofs.layers[j].rw[i] *= f;
				}
			}
#endif

#if DEBUG
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
			{
				if (	std::isnan(io_cCellData->dofs.layers[j].ru[0])			||
						std::isnan(io_cCellData->dofs.layers[j].rv[0])			||
						std::isnan(io_cCellData->dofs.layers[j].rw[0])
				)	{
					std::cerr << "op_cell instability" << std::endl;
					std::cerr << *io_cCellData << std::endl;
					assert(false);
					exit(-1);
				}
			}

#	if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
			for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
				for (int i = 0; i < N; i++)
					assert(io_cCellData->dofs.layers[j].r[i] >= 0);
#	endif

#endif
		}

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1

		// adaptivity is handled if rk2 values are updated (see method above)
		return 0;

#else


		return p_computeAdaptivityRequest(
				io_cCellData,				///< cell data

				i_hyp_edge_net_update,		///< incoming flux from hypotenuse
				i_right_edge_net_update,	///< incoming flux from right edge
				i_left_edge_net_update,		///< incoming flux from left edge

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
				hyp_edge_length,
				right_edge_length,
				left_edge_length
#else
				cat_edge_length
#endif
			);

#endif
	}



	/**
	 * Computes the fluxes for the given edge data.
	 *
	 * To use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			CEdgeData &i_edgeDOFs_left,			///< edge data on left edge
			CEdgeData &i_edgeDOFs_right,		///< edge data on right edge

			CEdgeData &o_edgeFlux_left,			///< output for left flux
			CEdgeData &o_edgeFlux_right			///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		T max_wave_speed_left;
		T max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeDOFs_left.dofs,
				i_edgeDOFs_right.dofs,

				&o_edgeFlux_left.dofs,
				&o_edgeFlux_right.dofs,

				&max_wave_speed_left,
				&max_wave_speed_right,

				gravitational_constant
			);

		/*
		 * compute the CFL
		 */
		assert(i_edgeDOFs_left.CFL1_scalar > 0);
		assert(i_edgeDOFs_right.CFL1_scalar > 0);
		assert(max_wave_speed_left >= 0);
		assert(max_wave_speed_right >= 0);

		// CELL_SIZE / MAX WAVE SPEED
		o_edgeFlux_left.CFL1_scalar = i_edgeDOFs_left.CFL1_scalar / max_wave_speed_left;
		o_edgeFlux_right.CFL1_scalar = i_edgeDOFs_right.CFL1_scalar / max_wave_speed_right;

		assert(o_edgeFlux_left.CFL1_scalar >= 0);
		assert(o_edgeFlux_right.CFL1_scalar >= 0);

#if DEBUG
		if (	!(o_edgeFlux_left.CFL1_scalar > 0) ||
				!(o_edgeFlux_right.CFL1_scalar > 0)
		)
		{
			std::cerr << "CFL1 scalar > 0" << std::endl;
			std::cerr << o_edgeFlux_left.CFL1_scalar << std::endl;
			std::cerr << o_edgeFlux_right.CFL1_scalar << std::endl;

			std::cerr << i_edgeDOFs_left << std::endl;
			std::cerr << i_edgeDOFs_right << std::endl;
		}
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edgeFlux_left.validation = i_edgeDOFs_left.validation;
		o_edgeFlux_right.validation = i_edgeDOFs_right.validation;
#endif
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CEdgeData> *io_edge_comm_buffer
	)	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_stack(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_stack(
		CEdgeData *io_edge_data_array,
		size_t i_edge_comm_elements
	)	{
		for (size_t i = 0; i < i_edge_comm_elements; i+=2)
		{
			// copy edge data to avoid aliasing
			CEdgeData ed0 = io_edge_data_array[i];
			CEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}

	static void setupMatrices(int i_verbosity_level)
	{
		CDG_MatrixComputations_3D::setup(i_verbosity_level);

		if (i_verbosity_level > 10)
		{
			CDG_MatrixComputations_3D::debugOutput(i_verbosity_level);
		}
	}
};

}
}



#endif
