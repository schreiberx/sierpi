/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 12, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOW_HPP_
#define CDEFORMATIONAL_FLOW_HPP_


#include "libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "libmath/CMatrix.hpp"
#include "libmath/CVector.hpp"
#include "../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"
#include "../flux_solver/CFluxSolver_BenchmarkVelocityUpwinding.hpp"

#include "CGlobal.hpp"
#include "../CDatasets.hpp"

extern CONFIG_DEFAULT_FLOATING_POINT_TYPE *simulation_timestamp_for_timestep;



class CDeformationalFlow
{
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	static const int N = SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE;

public:

	enum EEdge_Type {
		E_ERROR = -1,
		E_HYP = 0,
		E_RIGHT = 1,
		E_LEFT = 2
	};


	/*
	 * partial derivative of lat/lon to 3D function with respect to longitude
	 */
	template <typename T>
	inline static void latlon_derivative_lon(
			T i_lat,
			T i_lon,
			T *o_x, T *o_y, T *o_z
	){
		*o_x = -cos(i_lat) * sin(i_lon);
		*o_y = cos(i_lat) * cos(i_lon);
		*o_z = (T)0;
	}

	/*
	 * partial derivative of lat/lon to 3D function with respect to latitude
	 */
	template <typename T>
	inline static void latlon_derivative_lat(
			T i_lat,
			T i_lon,
			T *o_x, T *o_y, T *o_z
	){
		*o_x = -sin(i_lat) * cos(i_lon);
		*o_y = -sin(i_lat) * sin(i_lon);
		*o_z = cos(i_lat);
	}

	/**
	 * computes the velocity in lat/lon coordinates as defined by the
	 * deformational flow benchmark paper
	 */
	template <typename T>
	inline static void compute_latlonVelocity(
			T i_s_lat,
			T i_s_lon,
			T *o_u_lat,
			T *o_u_lon
	){
		T k, t_max, t;
		t_max = 5;
		// get current timestep
		t = 0;
		CGlobal& g = CGlobal::getInstance();
		t = g.getTS();
		T u_lon, v_lat;
		switch(g.getBenchmarkType()){
		case CGlobal::E_DEF_FLOW_CASE1:
			// deformational flow case-1 (Nair 2010)
			i_s_lon = (T)M_PI + i_s_lon;
			k = (T)2.4;
			u_lon = k * sin(i_s_lon * (T)0.5) * sin(i_s_lon * (T)0.5) * sin(i_s_lat * (T)2) * cos(M_PI * t / t_max);
			v_lat = k * (T)0.5 * sin(i_s_lon) * cos(i_s_lat) * cos(M_PI * t / t_max);
			*o_u_lon = u_lon / cos(i_s_lat);
			*o_u_lat = v_lat;
//			std::cout << u_lon << ", " << v_lat << std::endl;
			break;
		case CGlobal::E_DEF_FLOW_LON_ONLY:
			// solid body rotation only along latitude circles
			k = 1;
			*o_u_lon = k * cos(i_s_lat);
			*o_u_lon = k;
			*o_u_lat = 0;
			break;
		case CGlobal::E_SOLID_BODY_ROTATION:
			// solid body rotation (Williamson 1988)
			T roundtime, u_0, alpha;
			roundtime = g.getRoundTime();
			alpha = CCube_To_Sphere_Projection::angleDegToRad(g.getAlpha());
			u_0 = (T)2 * M_PI / roundtime;
			u_lon = u_0 * (cos(i_s_lat) * cos(alpha) + sin(i_s_lat) * cos(i_s_lon) * sin(alpha));
			v_lat = -u_0 * sin(i_s_lon) * sin(alpha);
			*o_u_lon = u_lon / cos(i_s_lat);
			*o_u_lat = v_lat;
			break;
		}
	}


	/*
	 * edge center in 3D given the edge endpoints
	 */
	template <typename T>
	inline static void edge_center(
			const T i_point1[3], const T i_point2[3],
			T *o_x, T *o_y, T *o_z
	){
		*o_x = (i_point1[0] + i_point2[0]) / (T)2;
		*o_y = (i_point1[1] + i_point2[1]) / (T)2;
		*o_z = (i_point1[2] + i_point2[2]) / (T)2;
	}


	/*
	 * triangle centroid in 3D given the three corner vertices
	 */
	template <typename T>
	inline static void element_centroid(
			const T i_coordinates3D[3][3],
			T *o_x, T *o_y, T *o_z
	){
		*o_x = (i_coordinates3D[0][0] + i_coordinates3D[1][0] + i_coordinates3D[2][0]) / (T)3;
		*o_y = (i_coordinates3D[0][1] + i_coordinates3D[1][1] + i_coordinates3D[2][1]) / (T)3;
		*o_z = (i_coordinates3D[0][2] + i_coordinates3D[1][2] + i_coordinates3D[2][2]) / (T)3;
	}


	template <typename T>
	inline static void vectorlength(T x, T y, T z, T *len){
		*len = std::sqrt(x*x + y*y + z*z);
	}


	/**
	 * computes the benchmark velocity components to be stored in the
	 * triangle's edge data
	 */
	template <typename T>
	inline static void compute_worldspaceVelocity(
			T i_center_x, T i_center_y, T i_center_z,
			T *o_velocity_x,
			T *o_velocity_y,
			bool print,
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			const CCellData *i_cellData,
			EEdge_Type i_edge_type
	){
		T s_lat, s_lon;
		T s_x = i_center_x; T s_y = i_center_y; T s_z = i_center_z;

		// edge center in lat/lon
		CCube_To_Sphere_Projection::project3DToLL(s_x, s_y, s_z,
				&s_lat, &s_lon);
#if 1
		// project edge midpoint from straight edge to sphere surface
		CCube_To_Sphere_Projection::projectLLTo3D(s_lat, s_lon, &s_x, &s_y, &s_z);
#endif

		// get lat/lon angular velocity for edge center
		T u_lat, u_lon;
		compute_latlonVelocity(s_lat, s_lon, &u_lat, &u_lon);

		// endpoint of velocity vector in lat/lon
		T u_vertex_lat, u_vertex_lon;
		u_vertex_lat = s_lat + u_lat;
		u_vertex_lon = s_lon + u_lon;

		/*
		 * old attempt: get 3D velocity endpoint
		 */
		// endpoint of velocity vector in 3D
		T u_x, u_y, u_z;
		CCube_To_Sphere_Projection::projectLLTo3D(u_vertex_lat, u_vertex_lon, &u_x, &u_y, &u_z);

		CVector<3, T> us_vector = CVector<3, T>(u_x-s_x, u_y-s_y, u_z-s_z);

		/*
		 * new attempt: get partial derivatives of lat/lon to 3D transformation
		 * and multiply these with angular velocities
		 */
		// partial derivatives
		T x_dlat, y_dlat, z_dlat, x_dlon, y_dlon, z_dlon;
		latlon_derivative_lat(s_lat, s_lon, &x_dlat, &y_dlat, &z_dlat);
		latlon_derivative_lon(s_lat, s_lon, &x_dlon, &y_dlon, &z_dlon);

		// multiply with angular velocities and add up
		T u_x_derive, u_y_derive, u_z_derive;
		u_x_derive = x_dlat * u_lat + x_dlon * u_lon;
		u_y_derive = y_dlat * u_lat + y_dlon * u_lon;
		u_z_derive = z_dlat * u_lat + z_dlon * u_lon;
#if CONFIG_BENCHMARK_SPHERE_DERIVATIVES
		us_vector = CVector<3, T>(u_x_derive, u_y_derive, u_z_derive);
#endif
		// --- new --- //

		/*
		 * old rotation: rotate to triangle plane and rescale
		 */
		// rotate 3d velocity vector to triangle plane -> left edge on x-axis, left edge normal on y-axis
		CMatrix3<T> M_inv = CMatrix3<T>(
				i_cellData->projectionMatrix3DTo2D[0][0], i_cellData->projectionMatrix3DTo2D[0][1], i_cellData->projectionMatrix3DTo2D[0][2],
				i_cellData->projectionMatrix3DTo2D[1][0], i_cellData->projectionMatrix3DTo2D[1][1], i_cellData->projectionMatrix3DTo2D[1][2],
				i_cellData->projectionMatrix3DTo2D[2][0], i_cellData->projectionMatrix3DTo2D[2][1], i_cellData->projectionMatrix3DTo2D[2][2]);
		CVector<3, T> flat_us_vector = M_inv * us_vector;

		// vector in 2D
		T us_xx, us_yy;
		us_xx = flat_us_vector[0];
		us_yy = flat_us_vector[1];

		// rescale to 3D length
		T us_3d_length, us_3d_length_rotated;
		vectorlength(flat_us_vector[0], flat_us_vector[1], flat_us_vector[2], &us_3d_length_rotated);
		vectorlength(us_vector[0], us_vector[1], us_vector[2], &us_3d_length);
		T us_2d_length = std::sqrt(us_xx * us_xx + us_yy * us_yy);
		if (us_2d_length != 0){
			us_xx = us_xx * us_3d_length / us_2d_length;
			us_yy = us_yy * us_3d_length / us_2d_length;
		}
#if CONFIG_ELEMENT_REFERENCE_SPACE
		CTriangle_VectorProjections::matrixTransformation(i_cellData->transformationMatrix, &us_xx, &us_yy);
#endif
		// set output, scale to earth radius
		*o_velocity_x = us_xx * EARTH_RADIUS_IN_METERS;
		*o_velocity_y = us_yy * EARTH_RADIUS_IN_METERS;


#if 0
		// compute for element centroid instead of edge midpoint.. obsolete..
		T cs_x, cs_y, cs_z, cs_lat, cs_lon, uc_lat, uc_lon, uc_vertex_lat, uc_vertex_lon, uc_x, uc_y, uc_z;
		element_centroid(i_cellData->coordinates3D, &cs_x, &cs_y, &cs_z);
		CCube_To_Sphere_Projection::project3DToLL(cs_x, cs_y, cs_z,
				&cs_lat, &cs_lon);
		CCube_To_Sphere_Projection::projectLLTo3D(cs_lat, cs_lon, &cs_x, &cs_y, &cs_z);
		CVector<3, T> vertex_cs = CVector<3, T>(cs_x, cs_y, cs_z);
		compute_latlonVelocity(cs_lat, cs_lon, &uc_lat, &uc_lon);
		uc_vertex_lat = cs_lat + uc_lat;
		uc_vertex_lon = cs_lon + uc_lon;
		CCube_To_Sphere_Projection::projectLLTo3D(uc_vertex_lat, uc_vertex_lon, &uc_x, &uc_y, &uc_z);
		CVector<3, T> ucs_vector = CVector<3, T>(uc_x-cs_x, uc_y-cs_y, uc_z-cs_z);
#endif

		/*
		 * new rotation: directly rotate to edge space
		 */
		// vertex vectors
		CVector<3, T> vertex_left = CVector<3, T>(i_cellData->coordinates3D[0][0], i_cellData->coordinates3D[0][1], i_cellData->coordinates3D[0][2]);
		CVector<3, T> vertex_right = CVector<3, T>(i_cellData->coordinates3D[1][0], i_cellData->coordinates3D[1][1], i_cellData->coordinates3D[1][2]);
		CVector<3, T> vertex_top = CVector<3, T>(i_cellData->coordinates3D[2][0], i_cellData->coordinates3D[2][1], i_cellData->coordinates3D[2][2]);
		// edge tangent vectors
		CVector<3, T> edge_right = vertex_top - vertex_right;
		CVector<3, T> edge_left = vertex_left - vertex_top;
		CVector<3, T> edge_hyp = vertex_right - vertex_left;

		// choose appropriate edge for rotation
		CVector<3, T> edge;
		switch(i_edge_type){
		case E_HYP:
			edge = edge_hyp;
			break;
		case E_RIGHT:
			edge = edge_right;
			break;
		case E_LEFT:
			edge = edge_left;
			break;
		case E_ERROR:
			return;
		}
		// get normalized basis vectors
		CVector<3, T> s_vector = CVector<3, T>(s_x, s_y, s_z);
		CVector<3, T> normal_edge = ( edge % s_vector );
		normal_edge.normalize();
		edge.normalize();
		s_vector.normalize();

		// rotation matrix
		CMatrix3<T> M = CMatrix3<T>(
				normal_edge[0], edge[0], s_vector[0],
				normal_edge[1], edge[1], s_vector[1],
				normal_edge[2], edge[2], s_vector[2]
				);
		CMatrix3<T> M_edge = M.getInverse();
		CVector<3, T> edge_us_vector = M_edge * us_vector;

		// project to 2D and rescale
		T edge_xx, edge_yy, us_edge_3d_length;
		vectorlength(edge_us_vector[0], edge_us_vector[1], edge_us_vector[2], &us_edge_3d_length);
		edge_xx = edge_us_vector[0];
		edge_yy = edge_us_vector[1];
//		std::cout << edge_xx << ", " << edge_yy << std::endl;
		T us_edge_2d_length = std::sqrt(edge_xx * edge_xx + edge_yy * edge_yy);
		// avoid division by 0 !!
		if (us_edge_2d_length != 0){
			edge_xx = edge_xx * us_edge_3d_length / us_edge_2d_length;
			edge_yy = edge_yy * us_edge_3d_length / us_edge_2d_length;
		}
#if CONFIG_BENCHMARK_SPHERE_DIRECT_EDGESPACE
		// set output, scale to earth radius
		*o_velocity_x = edge_xx * EARTH_RADIUS_IN_METERS;
		*o_velocity_y = edge_yy * EARTH_RADIUS_IN_METERS;
#endif
//		std::cout << *o_velocity_x << ", " << *o_velocity_y << std::endl;
		/*
		 * print debug info...
		 */
		bool forceprint = false;

		T latdeg = u_vertex_lat * (T)180 / M_PI;
		T londeg = u_vertex_lon * (T)180 / M_PI;
		T slatdeg = s_lat * (T)180 / M_PI;
		T slondeg = s_lon * (T)180 / M_PI;
		T ulatdeg = u_lat * (T)180 / M_PI;
		T ulondeg = u_lon * (T)180 / M_PI;
//		forceprint = slatdeg >= 43.28909936837 && slatdeg <= 43.28909936839 && slondeg >= 42.616242384714 && slondeg <= 42.616242384716;
		if(print || forceprint){
//			std::cout << std::endl;
			std::cout << "s lat/lon deg: " << slatdeg << ", " << slondeg << std::endl;
			std::cout << "u lat/lon deg: " << latdeg << ", " << londeg << std::endl;
			std::cout << "diff lat/lon deg: " << ulatdeg << ", " << ulondeg << std::endl;

			std::cout << "d/dlat s x/y/z: " << x_dlat << ", " << y_dlat << ", " << z_dlat << std::endl;
			std::cout << "d/dlon s x/y/z: " << x_dlon << ", " << y_dlon << ", " << z_dlon << std::endl;
			std::cout << "u vector x/y/z: " << u_x_derive << ", " << u_y_derive << ", " << u_z_derive << std::endl;
//			std::cout << "s lat/lon: " << s_lat << ", " << s_lon << std::endl;
//			std::cout << "u lat/lon: " << u_vertex_lat << ", " << u_vertex_lon << std::endl;
//			std::cout << "diff lat/lon: " << u_lat << ", " << u_lon << std::endl;

//			std::cout << "u2 lat/lon: " << u2_lat << ", " << u2_lon << std::endl;
//			std::cout << "s2 x/y/z: " << s2_x << ", " << s2_y << ", " << s2_z << std::endl;
			std::cout << std::endl;
			std::cout << "s x/y/z: " << s_x << ", " << s_y << ", " << s_z << std::endl;
			std::cout << "u x/y/z: " << u_x << ", " << u_y << ", " << u_z << std::endl;
			std::cout << "diff x/y/z: " << u_x-s_x << ", " << u_y-s_y << ", " << u_z-s_z << std::endl;
			std::cout << std::endl;

//			std::cout << us_vector << std::endl;
//			std::cout << flat_us_vector << std::endl;
//			std::cout << "u x/y/z: " << u2x << ", " << u2y << ", " << u2z << std::endl;
			std::cout << "u-s triangle plane x/y/z: " << flat_us_vector[0] << ", " << flat_us_vector[1] << ", " << flat_us_vector[2] << std::endl;
			std::cout << "u-s edge x/y/z: " << edge_us_vector[0] << ", " << edge_us_vector[1] << ", " << edge_us_vector[2] << std::endl;
			std::cout << "u-s edge x/y scaled: " << edge_xx << ", " << edge_yy << std::endl;

//			std::cout << "s vector x/y/z: " << s_vector[0] << ", " << s_vector[1] << ", " << s_vector[2] << std::endl;
//			std::cout << "cs vector x/y/z: " << vertex_cs[0] << ", " << vertex_cs[1] << ", " << vertex_cs[2] << std::endl;
//			std::cout << "u-cs vector x/y/z: " << ucs_vector[0] << ", " << ucs_vector[1] << ", " << ucs_vector[2] << std::endl;
//			std::cout << "u-cs edge x/y/z: " << edge_ucs_vector[0] << ", " << edge_ucs_vector[1] << ", " << edge_ucs_vector[2] << std::endl;

//			std::cout << "s triangle plane x/y/z: " << flat_s_vector[0] << ", " << flat_s_vector[1] << ", " << flat_s_vector[2] << std::endl;
//			std::cout << "trans matrix: (" << i_cellData->transformationMatrix[0][0] << ", " << i_cellData->transformationMatrix[0][1] <<
//					"), (" << i_cellData->transformationMatrix[1][0] << ", " << i_cellData->transformationMatrix[1][1] << ")" << std::endl;
//			std::cout << "M_inv: " << M_inv << std::endl;
//			CMatrix3<T> M = M_inv.getInverse();
//			std::cout << "M: " << M << std::endl;
//			std::cout << "u-s 2D x/y: " << *o_velocity_x << ", " << *o_velocity_y << std::endl;
//			std::cout << "scaled u-s 2D x/y: " << us_xx << ", " << us_yy << std::endl;
//			std::cout << "3d len " << us_3d_length << ", 2d len " << us_2d_length << ", diff " << us_3d_length - us_2d_length << std::endl;
//			std::cout << "rotated: 3d len " << us_3d_length_rotated << ", 2d len " << us_2d_length << ", diff " << us_3d_length_rotated - us_2d_length << std::endl;
//			std::cout << "s len 3d " << s_3dlen << ", 3d rotated " << s_3dlen_rot << std::endl;
			std::cout << "u-s len 3d " << us_3d_length << ", 3d rotated " << us_3d_length_rotated << std::endl;

//			std::cout << std::endl;
//			T dummy;
//			outputFluxData(i_cellData, i_edge_type, dummy);
			outputFluxData(i_cellData, i_edge_type, us_xx, us_yy);
		}
	}


	/*
	 * mimic edge space rotation for debugging..
	 */
	template <typename T>
	inline static void outputFluxData(
			const CCellData *i_cellData,
			EEdge_Type i_edge_type,
			T u_x, T u_y
	){
		CEdgeData o_edge;
		o_edge.dofs.hu[0] = u_x;
		o_edge.dofs.hv[0] = u_y;

		switch(i_edge_type){
		case E_ERROR:
			break;
		case E_HYP:
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->hypEdgeRotationMatrix, o_edge.dofs.hu, o_edge.dofs.hv);
			std::cout << "u edge hyp: " << o_edge.dofs.hu[0] << ", " << o_edge.dofs.hv[0] << std::endl;
//			std::cout << "len: " << std::sqrt(o_edge.dofs.hu[0]*o_edge.dofs.hu[0]+o_edge.dofs.hv[0]*o_edge.dofs.hv[0]) << std::endl;
			break;
		case E_LEFT:
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->leftEdgeRotationMatrix, o_edge.dofs.hu, o_edge.dofs.hv);
			std::cout << "u edge left: " << o_edge.dofs.hu[0] << ", " << o_edge.dofs.hv[0] << std::endl;
			break;
		case E_RIGHT:
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->rightEdgeRotationMatrix, o_edge.dofs.hu, o_edge.dofs.hv);
			std::cout << "u edge right: " << o_edge.dofs.hu[0] << ", " << o_edge.dofs.hv[0] << std::endl;
			break;
		}
	}


	/**
	 * reproduces the rotations to edge space (and flux computations)
	 * to print out and compare edge space vectors
	 *
	 * ### obsolete ###
	 */
	template <typename T>
	inline static void outputFluxData(
			const CCellData *i_cellData,
			EEdge_Type i_edge_type,
			T dummy
	){

		CEdgeData o_left, o_right, o_hyp;
		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.h, o_hyp.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.hu, o_hyp.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.hv, o_hyp.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->hypEdgeRotationMatrix, o_hyp.dofs.hu, o_hyp.dofs.hv);

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.h, o_right.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.hu, o_right.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.hv, o_right.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->rightEdgeRotationMatrix, o_right.dofs.hu, o_right.dofs.hv);

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.h, o_left.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.hu, o_left.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.hv, o_left.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->leftEdgeRotationMatrix, o_left.dofs.hu, o_left.dofs.hv);

		CSimulationNodeDataSOA<1> hyp_flux_l, hyp_flux_r, right_flux_l, right_flux_r, left_flux_l, left_flux_r;
		T dummy1, dummy2, dummy3;
		CSimulationNodeDataSOA<1> hyp_next, left_next, right_next;
		hyp_next.h[0] = o_hyp.dofs.h[0];
		hyp_next.hu[0] = -o_hyp.dofs.hu[0];
		hyp_next.hv[0] = -o_hyp.dofs.hv[0];
		left_next.h[0] = o_left.dofs.h[0];
		left_next.hu[0] = -o_left.dofs.hu[0];
		left_next.hv[0] = -o_left.dofs.hv[0];
		right_next.h[0] = o_right.dofs.h[0];
		right_next.hu[0] = -o_right.dofs.hu[0];
		right_next.hv[0] = -o_right.dofs.hv[0];
		bool printflux = false;
		if (i_cellData->face == 0 && i_cellData->vertices[1][0] == -1 && i_cellData->vertices[1][1] == -1){
			hyp_next.hu[0] = -0.0158240670528181;//-o_hyp.dofs.hu[0];
			hyp_next.hv[0] = 0.00696202910753179;//-o_hyp.dofs.hv[0];
			left_next.hu[0] = 0.0102267034562117;//-o_left.dofs.hu[0];
			left_next.hv[0] = -0.0140221695980423;//-o_left.dofs.hv[0];
			right_next.hu[0] = 0.0139478204848079;//-o_right.dofs.hu[0];
			right_next.hv[0] = 0.010289588321936;//-o_right.dofs.hv[0];
			printflux = true;
		}
		CFluxSolver_BenchmarkVelocityUpwinding<T> fluxsolver;
		fluxsolver.op_edge_edge<1>(
				o_hyp.dofs, hyp_next,
				&hyp_flux_l, &hyp_flux_r,
				&dummy1, &dummy2, dummy3
				);
		fluxsolver.op_edge_edge<1>(
				o_left.dofs, left_next,
				&left_flux_l, &left_flux_r,
				&dummy1, &dummy2, dummy3
				);
		fluxsolver.op_edge_edge<1>(
				o_right.dofs, right_next,
				&right_flux_l, &right_flux_r,
				&dummy1, &dummy2, dummy3
				);

		T ehu_left = left_flux_l.h[0] * i_cellData->edgeLength[2];
		T ehu_right = right_flux_l.h[0] * i_cellData->edgeLength[1];
		T ehu_hyp = hyp_flux_l.h[0] * i_cellData->edgeLength[0];
		T ehu_sum = ehu_left + ehu_right + ehu_hyp;

		switch(i_edge_type){
		case E_ERROR:
			std::cout << "u edge left: " << o_left.dofs.hu[0] << ", " << o_left.dofs.hv[0] << std::endl;
			std::cout << "u edge right: " << o_right.dofs.hu[0] << ", " << o_right.dofs.hv[0] << std::endl;
			std::cout << "u edge hyp: " << o_hyp.dofs.hu[0] << ", " << o_hyp.dofs.hv[0] << std::endl;
			break;
		case E_HYP:
			std::cout << "u edge hyp: " << o_hyp.dofs.hu[0] << ", " << o_hyp.dofs.hv[0] << std::endl;
			break;
		case E_LEFT:
			std::cout << "u edge left: " << o_left.dofs.hu[0] << ", " << o_left.dofs.hv[0] << std::endl;
			break;
		case E_RIGHT:
			std::cout << "u edge right: " << o_right.dofs.hu[0] << ", " << o_right.dofs.hv[0] << std::endl;
			break;
		}
		if(printflux){
			std::cout << "flux left: h " << left_flux_l.h[0] << ", hu " << left_flux_l.hu[0] <<
					", ehu " << ehu_left << std::endl;
			std::cout << "flux right: h " << right_flux_l.h[0] << ", hu " << right_flux_l.hu[0] <<
					", ehu " << ehu_right << std::endl;
			std::cout << "flux hyp: h " << hyp_flux_l.h[0] << ", hu " << hyp_flux_l.hu[0] <<
					", ehu " << ehu_hyp << std::endl;
			std::cout << "flux sum: ehu " << ehu_sum << ", ehu/area " << ehu_sum/i_cellData->cellArea << std::endl;
		}
	}


	/**
	 * overwrite dofs with values following the
	 * deformational flow benchmark formulas
	 *
	 * ### obsolete, we use overwrite_dofs_edge instead ###
	 */
	template <typename T>
	inline static void overwrite_dofs_element(
			T io_u[N],
			T io_v[N],
			CCellData *i_cellData,
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y	///< normals for left edge (necessary for back-rotation of element)
	){
		T velocity_x, velocity_y;
#if CONFIG_BENCHMARK_SPHERE>0
		T s_x, s_y, s_z;
		element_centroid(i_cellData->coordinates3D, &s_x, &s_y, &s_z);

		compute_worldspaceVelocity(s_x, s_y, s_z, &velocity_x, &velocity_y, false, i_right_normal_x, i_right_normal_y, i_cellData, E_ERROR);
#endif

#if CONFIG_BENCHMARK_SPHERE==-1
		velocity_x = 0.1;
		velocity_y = 0.08;
//		T x = i_cellData->validation.vertices[0][0] + i_cellData->validation.vertices[(0+1)%3][0];;
//		T y = i_cellData->validation.vertices[0][1] + i_cellData->validation.vertices[(0+1)%3][1];;
//		x = 0.5*x + 0.5;
//		y = 0.5*y + 0.5;
//		velocity_x = sin(M_PI * x) * sin(M_PI * x) * sin(2*M_PI*y);
//		velocity_y = -sin(M_PI*y) * sin(M_PI*y) * sin(2*M_PI*x);
		CTriangle_VectorProjections::worldToReference(&velocity_x, &velocity_y, -i_right_normal_x, -i_right_normal_y);
//		CTriangle_VectorProjections::toEdgeSpace<i_edge_type>(velocity_x, velocity_y);
#endif

		for (int i=0; i<=N; i++){
			io_u[i] = velocity_x;
			io_v[i] = velocity_y;
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID!=10
			io_u[i] = velocity_x * (i_cellData->dofs.h[i]);
			io_v[i] = velocity_y * (i_cellData->dofs.h[i]);
#endif
		}
	}


	/**
	 * overwrite dofs with values following the
	 * deformational flow benchmark formulas
	 */
	template <typename T>
	inline static void overwrite_dofs_edge(
			T io_u[N],
			T io_v[N],
			const CCellData *i_cellData,
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			EEdge_Type i_edge_type	// 0: hyp, 1: right, 2:left
	){
		T velocity_x, velocity_y;
#if CONFIG_BENCHMARK_SPHERE>0
		// benchmarks on sphere (solid body rotation, deformational flow)

		// get edge center
		T s_x, s_y, s_z;
		edge_center(i_cellData->coordinates3D[i_edge_type], i_cellData->coordinates3D[(i_edge_type+1)%3], &s_x, &s_y, &s_z);

		// compute 2D velocity vectors at edge center
		compute_worldspaceVelocity(s_x, s_y, s_z, &velocity_x, &velocity_y, false, i_right_normal_x, i_right_normal_y, i_cellData, i_edge_type);
//		std::cout << velocity_x << ", " << velocity_y << std::endl;
#endif

#if CONFIG_BENCHMARK_SPHERE<0
		// 2D test
#if CONFIG_BENCHMARK_SPHERE==-2
		// simple movement/rotation
		velocity_x = 0.1;
		velocity_y = 0.08;
#endif
#if CONFIG_BENCHMARK_SPHERE==-1
		def_flow_velocities_2d(i_cellData, i_edge_type, &velocity_x, &velocity_y, false);
#endif
		CTriangle_VectorProjections::worldToReference(&velocity_x, &velocity_y, -i_right_normal_x, -i_right_normal_y);
#endif

		for (int i=0; i<=N; i++){
			io_u[i] = velocity_x;
			io_v[i] = velocity_y;
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID!=10
			io_u[i] = velocity_x * (i_cellData->dofs.h[i]);
			io_v[i] = velocity_y * (i_cellData->dofs.h[i]);
#endif
		}
	}

	template <typename T>
	inline static void def_flow_velocities_2d(
			const CCellData *i_cellData,
			EEdge_Type i_edge_type,
			T *o_velocity_x, T *o_velocity_y,
			bool print
	){
		T velocity_x, velocity_y;
		// deformational flow
		T x, y, v1_x, v1_y, v2_x, v2_y;
		v1_x = i_cellData->validation.vertices[i_edge_type][0];
		v2_x = i_cellData->validation.vertices[(i_edge_type+1)%3][0];
		v1_y = i_cellData->validation.vertices[i_edge_type][1];
		v2_y = i_cellData->validation.vertices[(i_edge_type+1)%3][1];
		x = v1_x + v2_x + 1;
		y = v1_y + v2_y + 1;
		x = 0.5*x;
		y = 0.5*y;
		CGlobal& g = CGlobal::getInstance();
		// get current timestep
		T t = g.getTS();
		T t_max = 15000;
		T k = 0.0001;
		velocity_x = sin(M_PI * x) * sin(M_PI * x) * sin(2*M_PI*y) * cos(M_PI * t / t_max) * k;
		velocity_y = -sin(M_PI*y) * sin(M_PI*y) * sin(2*M_PI*x) * cos(M_PI * t / t_max) * k;
		if(print) std::cout << "mid " << velocity_x << ", " << velocity_y << std::endl;

		T int_vx_1, int_vy_1, int_vx_2, int_vy_2;
		velocity_integrals(v1_x, v1_y, t, t_max, &int_vx_1, &int_vy_1);
		velocity_integrals(v2_x, v2_y, t, t_max, &int_vx_2, &int_vy_2);
		if(std::sqrt(v2_x*v2_x + v2_y*v2_y) < std::sqrt(v1_x*v1_x + v2_y*v2_y)){
			velocity_x = int_vx_2 - int_vx_1;
			velocity_y = int_vy_2 - int_vy_1;
		} else {
			velocity_x = int_vx_1 - int_vx_2;
			velocity_y = int_vy_1 - int_vy_2;
		}

		T e = std::sqrt((v2_x-v1_x)*(v2_x-v1_x) + (v2_y-v1_y)*(v2_y-v1_y));
		velocity_x *= k/e;
		velocity_y *= k/e;
//		std::cout << e << std::endl;
		if(print) std::cout << "int " << velocity_x << ", " << velocity_y << std::endl;
		*o_velocity_x = velocity_x;
		*o_velocity_y = velocity_y;
	}

	template <typename T>
	inline static void velocity_integrals(
			T i_x, T i_y,
			T i_t, T i_t_max,
			T *o_int_vx, T *o_int_vy
	){
		*o_int_vx = - (T)0.5 *
				( cos(M_PI*i_t/i_t_max)
						* (-(T)0.5 * cos(M_PI*i_x) * sin(M_PI*i_x) + (T)0.5*M_PI*i_x)
						* cos((T)2*M_PI*i_y))
						/ (M_PI*M_PI);
		*o_int_vy = (T)0.5 *
				( cos(M_PI*i_t/i_t_max)
						* (-(T)0.5 * cos(M_PI*i_y) * sin(M_PI*i_y) + (T)0.5*M_PI*i_y)
						* cos((T)2*M_PI*i_x))
						/ (M_PI*M_PI);
	}


	template <typename T>
	inline static void compute_error_measures(
			const CCellData *i_cellData,
			T *l1_n, T *l1_d
//			T *v,
//			T h
	){
#if CONFIG_BENCHMARK_SPHERE>0
		CGlobal& g = CGlobal::getInstance();
		CDatasets *cDatasets = g.getDatasets();

		T world_coords[2];
		sierpi::CTriangle_PointProjections::referenceToWorld(
//				v[0], v[1],
//				v[3], v[4],
//				v[6], v[7],
//				v[0], v[1], v[2],
//				v[3], v[4], v[5],
//				v[6], v[7], v[8],
				i_cellData->vertices[0][0],	i_cellData->vertices[0][1],
				i_cellData->vertices[1][0],	i_cellData->vertices[1][1],
				i_cellData->vertices[2][0],	i_cellData->vertices[2][1],
				(T)(1.0/3.0),		(T)(1.0/3.0),
				&world_coords[0],	&world_coords[1]
		);

//		T v1_x, v1_y, v1_z, v2_x, v2_y, v2_z, v3_x, v3_y, v3_z;
//		CCube_To_Sphere_Projection::project2DTo3D(v[0], v[1], v1_x, v1_y, v1_z);
//		CCube_To_Sphere_Projection::project2DTo3D(v[3], v[4], v2_x, v2_y, v2_z);
//		CCube_To_Sphere_Projection::project2DTo3D(v[6], v[7], v3_x, v3_y, v3_z);
//
//		T e_0, e_1, e_2, area;
//		e_0 = CTriangle_VectorProjections::getEdgeLengthOnSphere(
//				v1_x, v1_y, v1_z,
//				v2_x, v2_y, v2_z, (T)1);
//		e_1 = CTriangle_VectorProjections::getEdgeLengthOnSphere(
//				v1_x, v1_y, v1_z,
//				v3_x, v3_y, v3_z, (T)1);
//		e_2 = CTriangle_VectorProjections::getEdgeLengthOnSphere(
//				v3_x, v3_y, v3_z,
//				v2_x, v2_y, v2_z, (T)1);
//		area = CTriangle_VectorProjections::getCellAreaOnSphere(e_0, e_1, e_2);
//		std::cout << v1_x << ", " << e_0 << ", " << area << std::endl;

		T h_T, h_N, I_d, I_n;
		h_T = cDatasets->getDatasetValue(1, world_coords[0], world_coords[1], 0);
		h_N = i_cellData->dofs.h[0];
//		h_N = h;

//		std::cout << h_N << ", " << h_T << std::endl;
		I_n = std::abs(h_N - h_T);// * i_cellData->cellArea;
		I_d = std::abs(h_T);// * i_cellData->cellArea;
//		std::cout << I_n << ", " << I_d << std::endl;
		g.addErrorL1(I_n, I_d);

		*l1_d = h_T;
		*l1_n = h_N;
#endif
	}

};

#endif



