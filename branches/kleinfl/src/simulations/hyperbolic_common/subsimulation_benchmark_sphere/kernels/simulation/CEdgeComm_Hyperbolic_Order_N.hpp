/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 *		Stjepan Bakrac <bakrac@in.tum.de>
 *		Philipp Müller <philippausmuensing@googlemail.com>
 *
 *		Mueller Michael <muellmic@in.tum.de>
 */

#ifndef CEDGECOMM_HYPERBOLIC_ORDER_N_HPP_
#define CEDGECOMM_HYPERBOLIC_ORDER_N_HPP_


#include <cmath>
#include <limits>

// Config
#include "../../CConfig.hpp"

// Generic types
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

// Traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth_ReturnAdaptivityRequests.hpp"

// Flux solvers
#include "../../../subsimulation_generic/flux_solver/CFluxSolver.hpp"

// Edge projections
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"

// Triangle side lengths
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

// Triangle tools
#include "libsierpi/triangle/CTriangle_Tools.hpp"

// PDE lab stuff
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"

// Enum for boundary conditions
#include "libsierpi/grid/CBoundaryConditions.hpp"

// Default parameters for tsunami edge comm
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Parameters.hpp"

#include "../../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"

// Check for set buoys
#include "../../../CParameters.hpp"

#include "global_config.h"

#include "simulations/hyperbolic_common/subsimulation_tsunami/tsunami_benchmarks/CDeformationalFlow.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (op_cell)
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Hyperbolic_Order_N	:
	public CEdgeComm_Parameters		// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_Parameters::setTimestepSize;
	using CEdgeComm_Parameters::setGravitationalConstant;
	using CEdgeComm_Parameters::setBoundaryDirichlet;
	using CEdgeComm_Parameters::setParameters;


	/**
	 * typedefs for convenience
	 */
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth_ReturnAdaptivityRequests<CEdgeComm_Hyperbolic_Order_N<t_storeElementUpdatesOnly>, CHyperbolicTypes > TRAV;

	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,2> CGaussTriangleEdge;


	/**
	 * accessor to flux solver
	 */
private:
	CFluxSolver<T> fluxSolver;
public:

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	CMigrationClass<CEdgeComm_Parameters> cMigrationClass;

	inline void migration_send(int i_dst_rank)
	{
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::sendAtomic((CEdgeComm_Parameters&)(*this), i_dst_rank);
#else
		cMigrationClass.send((CEdgeComm_Parameters&)(*this), i_dst_rank);
#endif
	}

	inline void migration_send_postprocessing(int i_dst_rank)
	{
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		cMigrationClass.wait();
#endif
	}

	inline void migration_recv(int i_src_rank)
	{
		sierpi::CMigration::recvRawClass((CEdgeComm_Parameters&)(*this), i_src_rank);
	}

#endif


	/**
	 * constructor
	 */
	CEdgeComm_Hyperbolic_Order_N()
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			boundary_dirichlet.dofs.h[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.hu[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.hv[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.b[i] = std::numeric_limits<T>::infinity();
		}
	}


#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1

	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk_cell_update(
			CCellData *io_V_0,
			CCellData &i_D_i,
			T i_scalar
	)	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			io_V_0->dofs.h[i]	+= i_scalar*i_D_i.dofs.h[i];
			io_V_0->dofs.hu[i]	+= i_scalar*i_D_i.dofs.hu[i];
			io_V_0->dofs.hv[i]	+= i_scalar*i_D_i.dofs.hv[i];
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_V_0->validation = i_D_i.validation;
#endif
	}

#endif





	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Hyperbolic_Order_N &i_kernel
	)	{
		// copy configuration
		(CEdgeComm_Parameters&)(*this) = (CEdgeComm_Parameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);

#if CONFIG_BENCHMARK_SPHERE
#if 1
		CDeformationalFlow::overwrite_dofs_edge(
				o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv, //i_cCellData->coordinates3D,
				i_cCellData,
				i_hyp_normal_x, i_hyp_normal_y,	///< normals for hypotenuse edge
				i_right_normal_x, i_right_normal_y,	///< normals for right edge
				i_left_normal_x, i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
				CDeformationalFlow::E_HYP
				);
#endif
#endif
//		std::cout << o_cEdgeData->dofs.hu[0] << ", " << o_cEdgeData->dofs.hv[0] << std::endl;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
#if !CONFIG_BENCHMARK_SPHERE_DIRECT_EDGESPACE
#if CONFIG_ELEMENT_REFERENCE_SPACE
		CDG_MatrixComputations_2D::mul_edge_comm_transform_to_edge_space(i_cCellData->hypEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cCellData->hypEdgeRotationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
#endif
#else
		CDG_MatrixComputations_2D::mul_edge_comm_hyp_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
		// B
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cCellData->dofs.b, o_cEdgeData->dofs.b);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius * (T)2.0;
		*(i_cCellData->fluxes[0]) = o_cEdgeData->dofs.hu[0];
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupHypEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);

#if CONFIG_BENCHMARK_SPHERE
#if 1
		CDeformationalFlow::overwrite_dofs_edge(
				o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv, //i_cCellData->coordinates3D,
				i_cCellData,
				i_hyp_normal_x, i_hyp_normal_y,	///< normals for hypotenuse edge
				i_right_normal_x, i_right_normal_y,	///< normals for right edge
				i_left_normal_x, i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
				CDeformationalFlow::E_RIGHT
				);
#endif
#endif

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
#if !CONFIG_BENCHMARK_SPHERE_DIRECT_EDGESPACE
#if CONFIG_ELEMENT_REFERENCE_SPACE
		CDG_MatrixComputations_2D::mul_edge_comm_transform_to_edge_space(i_cCellData->rightEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cCellData->rightEdgeRotationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
#endif
#else
		CDG_MatrixComputations_2D::mul_edge_comm_right_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
		// B
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cCellData->dofs.b, o_cEdgeData->dofs.b);


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius * (T)2.0;	// using diameter
		*(i_cCellData->fluxes[1]) = o_cEdgeData->dofs.hu[0];
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRightEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);

#if CONFIG_BENCHMARK_SPHERE
#if 1
		CDeformationalFlow::overwrite_dofs_edge(
				o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv, //i_cCellData->coordinates3D,
				i_cCellData,
				i_hyp_normal_x, i_hyp_normal_y,	///< normals for hypotenuse edge
				i_right_normal_x, i_right_normal_y,	///< normals for right edge
				i_left_normal_x, i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
				CDeformationalFlow::E_LEFT
				);
#endif
#endif


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1
#if !CONFIG_BENCHMARK_SPHERE_DIRECT_EDGESPACE
#if CONFIG_ELEMENT_REFERENCE_SPACE
		CDG_MatrixComputations_2D::mul_edge_comm_transform_to_edge_space(i_cCellData->leftEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cCellData->leftEdgeRotationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
#endif
#else
		CDG_MatrixComputations_2D::mul_edge_comm_left_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif

		// B
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cCellData->dofs.b, o_cEdgeData->dofs.b);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius * (T)2.0;	// using diameter
		*(i_cCellData->fluxes[2]) = o_cEdgeData->dofs.hu[0];
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupLeftEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_hyp(
				i_hyp_normal_x,		i_hyp_normal_y,
				i_right_normal_x,	i_right_normal_y,
				i_left_normal_x,	i_left_normal_y,

				i_depth,
				i_cCellData,
				o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				if (o_cEdgeData->dofs.hu[i] < 0)
				{
					o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
					o_cEdgeData->dofs.hu[i] = 0;
				}
			}
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			/*
			 * apply bounce back boundary condition only on component perpendicular to boundary!
			 */
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];

			break;


		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
							&o_cEdgeData->dofs.hu[i],
							&o_cEdgeData->dofs.hv[i],
							-i_right_normal_x,
							-i_right_normal_y
						);

					CTriangle_VectorProjections::toEdgeSpace<T,0>(
						&(o_cEdgeData->dofs.hu[i]),
						&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					op_cell_to_edge_hyp(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
						);

					break;
				}
			}
			break;


		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}
			break;

		}



		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
//			o_cEdgeData->dofs.hv[i] = 0;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cCellData->validation.setupHypEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				if (o_cEdgeData->dofs.hu[i] < 0)
				{
					o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
					o_cEdgeData->dofs.hu[i] = 0;
				}
			}
			break;


		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] += -o_cEdgeData->dofs.hu[i];
			break;

		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
									&o_cEdgeData->dofs.hu[i],
									&o_cEdgeData->dofs.hv[i],
									-i_right_normal_x,
									-i_right_normal_y
							);

					CTriangle_VectorProjections::toEdgeSpace<T,1>(
						&(o_cEdgeData->dofs.hu[i]),
						&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					// default: outflow boundary condition
					op_cell_to_edge_right(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
						);
					break;
				}
			}
			break;


		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
//			o_cEdgeData->dofs.hv[i] = 0;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cCellData->validation.setupRightEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// safety check - in case that this you really have 2^64 cells, remove this assertion
		assert(i_depth < 65);

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;


		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}

			break;


		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			break;


		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				if (o_cEdgeData->dofs.hu[i] < 0)
				{
					o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
					o_cEdgeData->dofs.hu[i] = 0;
				}
			}
			break;


		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
				);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			break;


		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
									&o_cEdgeData->dofs.hu[i],
									&o_cEdgeData->dofs.hv[i],
									-i_right_normal_x,
									-i_right_normal_y
							);

					CTriangle_VectorProjections::toEdgeSpace<T,2>(
						&(o_cEdgeData->dofs.hu[i]),
						&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					// default: outflow boundary condition
					op_cell_to_edge_left(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
						);
					break;
				}
			}
			break;


		case BOUNDARY_CONDITION_DEFAULT_VALUES_WITH_VELOCITY_ZERO:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				o_cEdgeData->dofs.h[i] = -o_cEdgeData->dofs.b[i];
				o_cEdgeData->dofs.hu[i] = 0;
				o_cEdgeData->dofs.hv[i] = 0;
			}
			break;

		}


		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
//			o_cEdgeData->dofs.hv[i] = 0;
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cCellData->validation.setupLeftEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * compute flux components
	 */
	template <int N>
	inline void p_compute_fluxes(
		const CSimulationNodeDataSOA<N> &i_node_data,	///< nodal data
		CSimulationNodeDataSOA<N> *o_flux_data_x,		///< flux x-component
		CSimulationNodeDataSOA<N> *o_flux_data_y			///< flux y-component
	)	{

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_flux_data_x->h[i] = i_node_data.hu[i];
			o_flux_data_x->hu[i] = (i_node_data.hu[i]*i_node_data.hu[i])/i_node_data.h[i] + (T)0.5*gravitational_constant*i_node_data.h[i]*i_node_data.h[i];
			o_flux_data_x->hv[i] = (i_node_data.hu[i]*i_node_data.hv[i])/i_node_data.h[i];

			o_flux_data_y->h[i] = i_node_data.hv[i];
			o_flux_data_y->hu[i] = (i_node_data.hu[i]*i_node_data.hv[i])/i_node_data.h[i];
			o_flux_data_y->hv[i] = (i_node_data.hv[i]*i_node_data.hv[i])/i_node_data.h[i] + (T)0.5*gravitational_constant*i_node_data.h[i]*i_node_data.h[i];

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 1 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 2
			if (std::abs(i_node_data.h[i]) <= 0.00001)
			{
				o_flux_data_x->h[i] = 0;
				o_flux_data_x->hu[i] = 0;
				o_flux_data_x->hv[i] = 0;

				o_flux_data_y->h[i] = 0;
				o_flux_data_y->hu[i] = 0;
				o_flux_data_y->hv[i] = 0;
			}
#endif
		}

#if 0

#if SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS && (SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1)
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			if (i_node_data.h[i] <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
			{
				o_flux_data_x->h[i] = 0;
				o_flux_data_x->hu[i] = 0;
				o_flux_data_x->hv[i] = 0;

				o_flux_data_y->h[i] = 0;
				o_flux_data_y->hu[i] = 0;
				o_flux_data_y->hv[i] = 0;
			}
		}
#endif

#endif

	}


	inline unsigned char p_computeAdaptivityRequest(
			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update,		///< incoming flux from left edge

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T hyp_edge_length,
			T right_edge_length,
			T left_edge_length
#else
			T cat_edge_length
#endif

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			, T max_eigen_coefficient
#endif
	)
	{

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==1
		/**
		 * ADAPTIVITY CRITERIA: HEIGHT
		 */

 		T poly_coefficients_hb[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(io_cCellData->dofs.h, poly_coefficients_hb);
 		CDG_MatrixComputations_2D::madd_convert_dofs_to_0th_order(io_cCellData->dofs.b, poly_coefficients_hb);
		T hb = poly_coefficients_hb[0];

		if (std::abs(hb) >= refine_threshold)
			return TRAV::ADAPTIVITY_REFINE;

		if (std::abs(hb) < coarsen_threshold)
			return TRAV::ADAPTIVITY_COARSEN;

		return TRAV::ADAPTIVITY_NOTHING;


#elif SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2


#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS == 1

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
                T d_mass = i_right_edge_net_update->dofs.h[0]*right_edge_length;
                d_mass += i_left_edge_net_update->dofs.h[0]*left_edge_length;
                d_mass += i_hyp_edge_net_update->dofs.h[0]*hyp_edge_length;

                d_mass = std::abs(d_mass);
#else
                T d_mass = i_right_edge_net_update->dofs.h[0];
                d_mass += i_left_edge_net_update->dofs.h[0];
                d_mass += std::sqrt(2.0f)*i_hyp_edge_net_update->dofs.h[0];

                d_mass = std::abs(d_mass)*cat_edge_length;
#endif

                if (d_mass >= refine_threshold)
                        return TRAV::ADAPTIVITY_REFINE;

                if (d_mass < coarsen_threshold)
                        return TRAV::ADAPTIVITY_COARSEN;

                return TRAV::ADAPTIVITY_NOTHING;

#endif
		/*
		 * this adaptivity criteria is based on the 1st component of the Net-Updates
		 */
		T edge_weighted_flux = 0;
		T h = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			T t = std::abs(i_hyp_edge_net_update->dofs.h[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*hyp_edge_length;
#else
			T te = t;
#endif
			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		edge_weighted_flux *= (T)M_SQRT2;
#endif

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			T t = std::abs(i_right_edge_net_update->dofs.h[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*right_edge_length;
#else
			T te = t;
#endif
			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			T t = std::abs(i_left_edge_net_update->dofs.h[i]);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T te = t*left_edge_length;
#else
			T te = t;
#endif

			if (t > edge_weighted_flux)
			{
				edge_weighted_flux = te;
				h = t;
			}
		}

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		edge_weighted_flux *= cat_edge_length;
#endif

		T inv_h;
		if (h > SIMULATION_TSUNAMI_DRY_THRESHOLD)
			inv_h = (T)1.0/h;
		else
			inv_h = 1.0/SIMULATION_TSUNAMI_DRY_THRESHOLD;

		assert(edge_weighted_flux*inv_h >= 0);

		if (edge_weighted_flux*inv_h > refine_threshold)
			return TRAV::ADAPTIVITY_REFINE;

		if (edge_weighted_flux*inv_h <= coarsen_threshold)
			return TRAV::ADAPTIVITY_COARSEN;

#elif SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==3

#if !CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
#	error "CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS"
#endif

		if (max_eigen_coefficient > refine_threshold)
			return TRAV::ADAPTIVITY_REFINE;

		if (max_eigen_coefficient <= coarsen_threshold)
			return TRAV::ADAPTIVITY_COARSEN;

#else

#	error "unhandled adaptivity mode"

#endif

		return 0;
	}


	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline unsigned char op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			int i_depth,

			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update		///< incoming flux from left edge
	)
	{

#if DEBUG

#	if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			assert(io_cCellData->dofs.h[i] >= 0);
#	else
		assert(io_cCellData->dofs.h[0] >= 0);
#	endif

#endif

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

//			T i_hu = i_hyp_edge_net_update->dofs.hu[i];
//			T i_hv = i_hyp_edge_net_update->dofs.hv[i];
//			T o_hu = i_hu;
//			T o_hv = i_hv;
//
//			CTriangle_VectorProjections::fromHypEdgeSpace(
//					io_cCellData->hypEdgeRotationMatrix,
////					&i_hyp_edge_net_update->dofs.hu[i],
////					&i_hyp_edge_net_update->dofs.hv[i],
//					&o_hu,
//					&o_hv,
//					io_cCellData->edgeLength[0]
//			);

//			if (i_hu > 0){
//				std::cout << "I  " << i_hu << ", " << i_hv << std::endl;
//				std::cout << "J  " << o_hu << ", " << o_hv << std::endl;
//			}
//			if (i_hyp_edge_net_update->dofs.hu[i] > 0){
//				std::cout << "O0  " << i_hyp_edge_net_update->dofs.hu[i] << ", " << i_hyp_edge_net_update->dofs.hv[i] << std::endl;
//			}

			// --- hyp edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->hypEdgeRotationMatrix,
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
			);
//			if (i_hyp_edge_net_update->dofs.hu[i] > 0){
//				std::cout << "O1  " << i_hyp_edge_net_update->dofs.hu[i] << ", " << i_hyp_edge_net_update->dofs.hv[i] << std::endl;
//			}

#if ELEMENT_REFERENCE_SPACE
			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
			);
#endif

//			if (i_hyp_edge_net_update->dofs.hu[i] > 0){
//				std::cout << "O2  " << i_hyp_edge_net_update->dofs.hu[i] << ", " << i_hyp_edge_net_update->dofs.hv[i] << std::endl << std::endl;
//			}

			// --- right edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->rightEdgeRotationMatrix,
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
			);

#if ELEMENT_REFERENCE_SPACE
			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
			);
#endif


			// --- left edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->leftEdgeRotationMatrix,
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
			);

#if ELEMENT_REFERENCE_SPACE
			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
			);
#endif

#else

			CTriangle_VectorProjections::fromHypEdgeSpace(
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
				);

			CTriangle_VectorProjections::fromRightEdgeSpace(
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
				);

			CTriangle_VectorProjections::fromLeftEdgeSpace(
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
				);

#endif
		}


		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		assert(cathetus_real_length > 0);
#endif


#if DEBUG
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			if (std::isnan(io_cCellData->dofs.h[i]))
			{
				instabilityDetected = true;
				if (!instabilityDetected)
					std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
				return 0;
			}
		}
#endif

		/*
		 * Conservation law:
		 *
		 *   U_t + U_x = 0	(1)
		 *
		 * Explicit Euler timestep:
		 *
		 *   U^{t+1} = U^{t} + \Delta t (- U_x)		(2)
		 *
		 *
		 * Update (See High Order ADER FV/DG Numerical Methods for Hyperbolic Equations, Cristobal Castro, page 42):
		 *
		 *   U^{t+1} = U^{t} + \Delta t * M^{-1} [ S_x F(U^{t}) + S_y G(U^{t}) - \sum_e (E_e X(U^{t})) ]		(2)
		 *
		 *
		 * We reformulate this formula by extracting the cathetus length c from the matrices
		 * to get the dimensionless matrices M, S_x, S_y and E_e
		 *
		 *   U^{t+1} = U^{t} + 1/{c*c} \Delta t * M^{-1} [ c * S_x F(U^{t}) + c * S_y G(U^{t}) - c * \sum_e (E_e X(U^{t})) ] 	(3)
		 *
		 *
		 * By factoring c out of the brackets, we get
		 *
		 *   U^{t+1} = U^{t} + \alpha * M^{-1} [ S_x * F(U^{t}) + S_y * G(U^{t}) - \sum_e (E_e X_e) ]		(4.a)
		 *
		 *   with \alpha = 1/{c} \Delta t																	(4.b)
		 *
		 * To use different cathetus and hypothenuses length we skip the last step and use 
		 * different equations based on right angled triangles or not.
		 *
		 */

		static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		/*
		 * dof updates to apply to the cell dof's
		 */
		CSimulationNodeDataSOA<N> cDofUpdatesSOA;
		for (int i = 0; i < N; i++){
			cDofUpdatesSOA.h[i] = 0;
			cDofUpdatesSOA.hu[i] = 0;
			cDofUpdatesSOA.hv[i] = 0;
		}

		/*
		 * flux
		 *
		 * 0: hyp
		 * 1: right
		 * 2: left
		 */
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1

		T triangle_area = io_cCellData->cellArea;

		T hyp_edge_length = io_cCellData->edgeLength[0];
		T right_edge_length = io_cCellData->edgeLength[1];
		T left_edge_length = io_cCellData->edgeLength[2];

		assert(N==1);
		// multiply with edge lengths
		for (int i = 0; i < N; i++)
		{
			cDofUpdatesSOA.h[i] -= i_hyp_edge_net_update->dofs.h[i] * hyp_edge_length;
			cDofUpdatesSOA.hu[i] -= i_hyp_edge_net_update->dofs.hu[i] * hyp_edge_length;
			cDofUpdatesSOA.hv[i] -= i_hyp_edge_net_update->dofs.hv[i] * hyp_edge_length;

			cDofUpdatesSOA.h[i] -= i_right_edge_net_update->dofs.h[i] * right_edge_length;
			cDofUpdatesSOA.hu[i] -= i_right_edge_net_update->dofs.hu[i] * right_edge_length;
			cDofUpdatesSOA.hv[i] -= i_right_edge_net_update->dofs.hv[i] * right_edge_length;

			cDofUpdatesSOA.h[i] -= i_left_edge_net_update->dofs.h[i] * left_edge_length;
			cDofUpdatesSOA.hu[i] -= i_left_edge_net_update->dofs.hu[i] * left_edge_length;
			cDofUpdatesSOA.hv[i] -= i_left_edge_net_update->dofs.hv[i] * left_edge_length;
		}

#else

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CDG_MatrixComputations_2D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.h, cDofUpdatesSOA.h);
		CDG_MatrixComputations_2D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.hu, cDofUpdatesSOA.hu);
		CDG_MatrixComputations_2D::madd_timestep_hyp_fluxes_to_dofs(i_hyp_edge_net_update->dofs.hv, cDofUpdatesSOA.hv);

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CDG_MatrixComputations_2D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.h, cDofUpdatesSOA.h);
		CDG_MatrixComputations_2D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.hu, cDofUpdatesSOA.hu);
		CDG_MatrixComputations_2D::madd_timestep_right_fluxes_to_dofs(i_right_edge_net_update->dofs.hv, cDofUpdatesSOA.hv);

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CDG_MatrixComputations_2D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.h, cDofUpdatesSOA.h);
		CDG_MatrixComputations_2D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.hu, cDofUpdatesSOA.hu);
		CDG_MatrixComputations_2D::madd_timestep_left_fluxes_to_dofs(i_left_edge_net_update->dofs.hv, cDofUpdatesSOA.hv);
#endif



#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		T cat_edge_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
#endif


		/*
		 * compute timestep update
		 */
		if (t_storeElementUpdatesOnly)
		{
#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1
			std::cerr << "RK > 1 not supported yet for higher order methods" << std::endl;
			assert(false);
#endif

#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			/*
			 * Runge Kutta
			 */
			// dimensional update
			T alpha = 1.0/cat_edge_length;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.h[i] *= alpha;
			CDG_MatrixComputations_2D::mul_timestep_inv_mass(cDofUpdatesSOA.h, io_cCellData->dofs.h);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.hu[i] *= alpha;
			CDG_MatrixComputations_2D::mul_timestep_inv_mass(cDofUpdatesSOA.hu, io_cCellData->dofs.hu);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.hv[i] *= alpha;
			CDG_MatrixComputations_2D::mul_timestep_inv_mass(cDofUpdatesSOA.hv, io_cCellData->dofs.hv);
#endif
		}
		else
		{
			/*
			 * Explicit Euler
			 */

			// dimensional update
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
			T alpha = timestep_size/(triangle_area*(T)2.0);
#else
			T alpha = timestep_size/cat_edge_length;
#endif


#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.h[i] *= alpha;

			CDG_MatrixComputations_2D::madd_timestep_inv_mass(cDofUpdatesSOA.h, io_cCellData->dofs.h);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.hu[i] *= alpha;

			CDG_MatrixComputations_2D::madd_timestep_inv_mass(cDofUpdatesSOA.hu, io_cCellData->dofs.hu);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				cDofUpdatesSOA.hv[i] *= alpha;
			CDG_MatrixComputations_2D::madd_timestep_inv_mass(cDofUpdatesSOA.hv, io_cCellData->dofs.hv);


#if SIMULATION_LINEAR_VELOCITY_DAMPING

			// artificial velocity damping
			T f = std::pow(SIMULATION_LINEAR_VELOCITY_DAMPING_FACTOR, timestep_size);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
			{
				io_cCellData->dofs.hu[i] *= f;
				io_cCellData->dofs.hv[i] *= f;
			}
#endif


#if SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS && SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE==0

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
			{
				if (io_cCellData->dofs.h[i] < SIMULATION_TSUNAMI_DRY_THRESHOLD)
				{
					/*
					 * assure positivity of height
					 */
					if (io_cCellData->dofs.h[i] < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
						io_cCellData->dofs.h[i] = 0;

					/* 
					 * !IMPORTANT!
					 * remove momentum to assure avoiding artificial momentum once this cell gets wet again
					 * !IMPORTANT!
					 */
					io_cCellData->dofs.hu[i] = 0;
					io_cCellData->dofs.hv[i] = 0;
				}
			}
#endif


#if DEBUG
			if (	std::isnan(io_cCellData->dofs.hu[0])			||
					std::isnan(io_cCellData->dofs.hv[0])
			)	{
				std::cerr << "op_cell instability" << std::endl;
				std::cerr << *io_cCellData << std::endl;
				assert(false);
				exit(-1);
			}

#	if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 0 || SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
			for (int i = 0; i < N; i++)
				assert(io_cCellData->dofs.h[i] >= 0);
#	endif
#endif
		}

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		T max_eigen_coefficient = std::max(std::max(i_hyp_edge_net_update->max_eigen_coefficient, i_right_edge_net_update->max_eigen_coefficient), i_left_edge_net_update->max_eigen_coefficient);
#endif

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		io_cCellData->max_eigen_coefficient = max_eigen_coefficient;
#endif

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1

		// adaptivity is handled if rk2 values are updated (see method above)
		return 0;

#else

#if CONFIG_BENCHMARK_SPHERE
#if 0
		CDeformationalFlow::overwrite_dofs_element(
				io_cCellData->dofs.hu, io_cCellData->dofs.hv, //io_cCellData->coordinates3D,
				io_cCellData,
				i_hyp_normal_x, i_hyp_normal_y,	///< normals for hypotenuse edge
				i_right_normal_x, i_right_normal_y,	///< normals for right edge
				i_left_normal_x, i_left_normal_y	///< normals for left edge (necessary for back-rotation of element)
				);
#endif
#endif

		return p_computeAdaptivityRequest(
				io_cCellData,				///< cell data

				i_hyp_edge_net_update,		///< incoming flux from hypotenuse
				i_right_edge_net_update,	///< incoming flux from right edge
				i_left_edge_net_update,		///< incoming flux from left edge

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
				hyp_edge_length,
				right_edge_length,
				left_edge_length
#else
				cat_edge_length
#endif

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
				, max_eigen_coefficient
#endif
			);

#endif
	}



	/**
	 * Computes the fluxes for the given edge data.
	 *
	 * To use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			CEdgeData &i_edgeDOFs_left,			///< edge data on left edge
			CEdgeData &i_edgeDOFs_right,		///< edge data on right edge
			CEdgeData &o_edgeFlux_left,			///< output for left flux
			CEdgeData &o_edgeFlux_right			///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		T max_wave_speed_left;
		T max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeDOFs_left.dofs,
				i_edgeDOFs_right.dofs,

				&o_edgeFlux_left.dofs,
				&o_edgeFlux_right.dofs,

				&max_wave_speed_left,
				&max_wave_speed_right,

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
				&o_edgeFlux_left.max_eigen_coefficient,
				&o_edgeFlux_right.max_eigen_coefficient,
#endif

				gravitational_constant
			);

		/*
		 * compute the CFL
		 */
		assert(i_edgeDOFs_left.CFL1_scalar > 0);
		assert(i_edgeDOFs_right.CFL1_scalar > 0);
		assert(max_wave_speed_left >= 0);
		assert(max_wave_speed_right >= 0);

		// incircle stored in i_edgeDOFs_left.CFL1_scalar / MAX WAVE SPEED
		o_edgeFlux_left.CFL1_scalar = i_edgeDOFs_left.CFL1_scalar / max_wave_speed_left;
		o_edgeFlux_right.CFL1_scalar = i_edgeDOFs_right.CFL1_scalar / max_wave_speed_right;
//		std::cout << "o, i, v: " << o_edgeFlux_left.CFL1_scalar << ", " << i_edgeDOFs_left.CFL1_scalar << ", " << max_wave_speed_left << std::endl;

		assert(o_edgeFlux_left.CFL1_scalar >= 0);
		assert(o_edgeFlux_right.CFL1_scalar >= 0);

#if DEBUG
		if (	!(o_edgeFlux_left.CFL1_scalar > 0) ||
				!(o_edgeFlux_right.CFL1_scalar > 0)
		)
		{
			std::cerr << "CFL1 scalar > 0" << std::endl;
			std::cerr << o_edgeFlux_left.CFL1_scalar << std::endl;
			std::cerr << o_edgeFlux_right.CFL1_scalar << std::endl;

			std::cerr << i_edgeDOFs_left << std::endl;
			std::cerr << i_edgeDOFs_right << std::endl;
		}
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edgeFlux_left.validation = i_edgeDOFs_left.validation;
		o_edgeFlux_right.validation = i_edgeDOFs_right.validation;
#endif
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CEdgeData> *io_edge_comm_buffer
	)	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_stack(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_stack(
		CEdgeData *io_edge_data_array,
		const size_t i_edge_comm_elements
	)
	{
		size_t i = 0;

#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==6 && SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS == 1

		assert((i_edge_comm_elements&1) == 0);

#if FLUX_SIMD_COMPONENTS == 2
		const size_t non_simdizable_edge_comm_elements = i_edge_comm_elements & (1<<1);
#elif FLUX_SIMD_COMPONENTS == 4
		const size_t non_simdizable_edge_comm_elements = i_edge_comm_elements & (3<<1);
#elif FLUX_SIMD_COMPONENTS == 8
		const size_t non_simdizable_edge_comm_elements = i_edge_comm_elements & (7<<1);
#else
#		error "This vaule of FLUX_SIMD_COMPONENTS is not supported!"
#endif

		for (; i < i_edge_comm_elements-non_simdizable_edge_comm_elements; i+=(FLUX_SIMD_COMPONENTS*2))
		{
			T left_h[FLUX_SIMD_COMPONENTS];
			T left_hu[FLUX_SIMD_COMPONENTS];
			T left_b[FLUX_SIMD_COMPONENTS];

			T right_h[FLUX_SIMD_COMPONENTS];
			T right_hu[FLUX_SIMD_COMPONENTS];
			T right_b[FLUX_SIMD_COMPONENTS];

			T update_left_h[FLUX_SIMD_COMPONENTS];
			T update_left_hu[FLUX_SIMD_COMPONENTS];

			T update_right_h[FLUX_SIMD_COMPONENTS];
			T update_right_hu[FLUX_SIMD_COMPONENTS];

			T min_CFL1_value = std::numeric_limits<T>::infinity();

			// pack data
			for (size_t k = 0; k < FLUX_SIMD_COMPONENTS; k++)
			{
				left_h[k] = io_edge_data_array[i+k*2].dofs.h[0];
				left_hu[k] = io_edge_data_array[i+k*2].dofs.hu[0];
				left_b[k] = io_edge_data_array[i+k*2].dofs.b[0];

				right_h[k] = io_edge_data_array[i+k*2+1].dofs.h[0];
				right_hu[k] = -io_edge_data_array[i+k*2+1].dofs.hu[0];
				right_b[k] = io_edge_data_array[i+k*2+1].dofs.b[0];

				min_CFL1_value = std::min(io_edge_data_array[i+k*2].CFL1_scalar, io_edge_data_array[i+k*2+1].CFL1_scalar);
			}

			T max_wave_speed;
			fluxSolver.op_edge_edge_simd(
					left_h,		right_h,
					left_hu,	right_hu,
					left_b,		right_b,

					update_left_h,	update_right_h,
					update_left_hu,	update_right_hu,

					max_wave_speed
				);

			// incircle / max_wave_speed
			updateCFL1Value(min_CFL1_value/max_wave_speed);

			// unpack data
			for (size_t k = 0; k < FLUX_SIMD_COMPONENTS; k++)
			{
				io_edge_data_array[i+k*2].dofs.h[0] = update_left_h[k];
				io_edge_data_array[i+k*2].dofs.hu[0] = update_left_hu[k];
				io_edge_data_array[i+k*2].dofs.hv[0] = 0;

				io_edge_data_array[i+k*2+1].dofs.h[0] = update_right_h[k];
				io_edge_data_array[i+k*2+1].dofs.hu[0] = -update_right_hu[k];
				io_edge_data_array[i+k*2+1].dofs.hv[0] = 0;
			}
		}

		assert(i == i_edge_comm_elements-non_simdizable_edge_comm_elements);

#endif

		for (; i < i_edge_comm_elements; i+=2)
		{
			// copy edge data to avoid aliasing
			CEdgeData ed0 = io_edge_data_array[i];
			CEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			// CFL1_scalar here contains incircle / max_wave_speed
			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}


#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==6 && SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS == 1

	inline void op_edge_edge_simd(
			T *i_left_h,	T *i_right_h,
			T *i_left_hu,	T *i_right_hu,
			T *i_left_b,	T *i_right_b,

			T *o_left_h,	T *o_right_h,
			T *o_left_hu,	T *o_right_hu,

			T &o_wave_speed
	)
	{
		fluxSolver.op_edge_edge_simd(
				i_left_h,	i_right_h,
				i_left_hu,	i_right_hu,
				i_left_b,	i_right_b,

				o_left_h,	o_right_h,
				o_left_hu,	o_right_hu,

				o_wave_speed
		);
	}
#endif

	static void setupMatrices(int i_verbosity_level)
	{
		CDG_MatrixComputations_2D::setup(i_verbosity_level);

		if (i_verbosity_level > 10)
		{
			CDG_MatrixComputations_2D::debugOutput(i_verbosity_level);
		}
	}
};

}
}



#endif
