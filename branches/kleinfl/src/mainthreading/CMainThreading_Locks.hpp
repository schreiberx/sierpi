/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CMainThreading_Locks.hpp
 *
 *  Created on: Jan 23, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_LOCKS_HPP_
#define CMAINTHREADING_LOCKS_HPP_


#if COMPILE_WITH_IPMO || COMPILE_WITH_IOMP || CONFIG_THREADING_OMP

	// OMP
	#include "CMainThreading_LockOMP.hpp"

#else

#if COMPILE_WITH_ITBB || CONFIG_THREADING_TBB

	// TBB
	#include "CMainThreading_LockTBB.hpp"

#else

	// DUMMY
	#include "CMainThreading_LockDummy.hpp"

#endif
#endif


#endif /* CMAINTHREADING_HPP_ */
