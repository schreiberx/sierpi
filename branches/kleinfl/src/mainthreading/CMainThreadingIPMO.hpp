/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CMainThreadingIPMO.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_IPMO_HPP_
#define CMAINTHREADING_IOMP_HPP_

#include <omp.h>
#include <iostream>
#include <cmath>

#include <CPMO_OMP.hpp>

#include "../simulations/hyperbolic_parallel/CSimulationHyperbolic_ScalabilityGraph.hpp"

#include "CMainThreading_Interface.hpp"

#include "libsierpi/parallelization/CGlobalComm.hpp"

#define DELAYED_LEAVE_OMP_PARALLEL_REGION	0

/**
 * Threading support for iPMO
 */
class CMainThreading :
	public CMainThreading_Interface
{
	/**
	 * PMO handler
	 */
	CPMO_OMP *cPmo;

	/**
	 * initial maximum number of cores
	 */
	int initial_max_cores;

	/**
	 * previous id used for invade scalability data
	 */
	int prev_unique_invade_id;

#if COMPILE_WITH_IPMO_ASYNC_MPI
	/**
	 * previous simulation workload
	 */
	unsigned long long prev_invade_simulation_workload;
#endif

	/**
	 * change in workload when to execute next invade
	 */
	int prev_workload_delta_to_update;



public:
	CMainThreading() :
			cPmo(nullptr),
			initial_max_cores(1024),
			prev_unique_invade_id(-1),
			prev_workload_delta_to_update(0)
#if COMPILE_WITH_IPMO_ASYNC_MPI
		,
		prev_invade_simulation_workload(99999999999)
#endif
	{
	}



	/**
	 * threading setup
	 */
	void threading_setup()
	{
	
#if COMPILE_WITH_IPMO_ASYNC_MPI
		// disable waiting for an ack
		cPmo = new CPMO_OMP(-1, 0, false);

		cPmo->setup();

		int commSize = sierpi::CGlobalComm::getCommSize();

		/*
		 * wait until each MPI node was assigned at least one core
		 */
		int reduceValue = 0;
		do
		{
			cPmo->reinvade_nonblocking();
			reduceValue = sierpi::CGlobalComm::reduceIntSum(cPmo->getNumberOfThreads());
		} while(commSize != reduceValue);

		// WARNING: in case that more processes are started than there are cores available on the system, this deadlocks
		sierpi::CGlobalComm::barrier();

#else

#	if DELAYED_LEAVE_OMP_PARALLEL_REGION
		cPmo = new CPMO_OMP(-1, true);
#	else
		cPmo = new CPMO_OMP(-1, false);
#	endif

		cPmo->setup();

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): "
					<< (int) omp_get_max_threads() << std::endl;
#endif

		// get at least 1 core (assured by blocking invade)
//		cPmo->invade_blocking(1, 1);

		threading_updateResourceUtilization();
	}



	/**
	 * update the resource utilization
	 */
	bool threading_updateResourceUtilization()
	{
		unsigned long long local_workload = getSimulationLocalWorkload();

		bool resources_updated;

#if COMPILE_WITH_IPMO_ASYNC_MPI

		long long diff_workload = std::abs((long long)prev_invade_simulation_workload - (long long)local_workload);

		float inbalance = (float)diff_workload/(float)local_workload;

		if (inbalance > COMPILE_WITH_ITBB_ASYNC_MPI_INVADE_THRESHOLD)
		{
			// request resource update with new workload
			cPmo->invade_nonblocking(1, 1024, 0, nullptr, (float)local_workload);

			prev_invade_simulation_workload = local_workload;
		}

		// is some message about optimizations available?
		// TODO: 'true' is not returned in case of optimization :-/
//		if (cPmo->reinvade_nonblocking())
		cPmo->reinvade_nonblocking();
		{
//			std::cout << " B " << sierpi::CGlobalComm::getCommRank() << ": " << cPmo->num_running_threads << ", " << local_workload << std::endl;
			setValueNumberOfThreadsToUse(cPmo->getNumberOfThreads());
			return true;
		}

		return false;

#else

		int scalability_graph_id = getThreadIPMOScalabilityGraphId();

		if (scalability_graph_id == -1)
		{
			/*
			 * workload based core-balancing
			 */

//			static unsigned long long prev_invade_workload = 0;

//			if (std::abs(local_workload - prev_invade_workload) >= prev_workload_delta_to_update)
			{
#	if COMPILE_WITH_IPMO_ASYNC

#if DEBUG
				std::cout << "invade_nonblocking with local_workload: " << local_workload << std::endl;
#endif
				cPmo->invade_nonblocking(
						1, 99999999,
						0, nullptr,
						local_workload
					);

				resources_updated = cPmo->reinvade_nonblocking();

#	else

				resources_updated = cPmo->invade_blocking(
						1, 99999999,
						0, nullptr,
						local_workload
					);

#	endif
//				prev_invade_workload = local_workload;

				// update when workload difference exceeds 5 percent of current workload
				prev_workload_delta_to_update = local_workload/20;
			}
		}
		else
		{
			int max_cores;
			int scalability_graph_size;
			float *scalability_graph;
			const char *o_information_string;

			int id =
					CSimulationHyperbolic_ScalabilityGraph::getInvadeHintsAndConstraints(
							local_workload, scalability_graph_id, &max_cores,
							&scalability_graph_size, &scalability_graph,
							&o_information_string);

#	if COMPILE_WITH_IPMO_ASYNC

			if (id != prev_unique_invade_id)
			{
				prev_unique_invade_id = id;
				cPmo->invade_nonblocking(1, max_cores, scalability_graph_size, scalability_graph);
			}

			resources_updated = cPmo->reinvade_nonblocking();

#	else

			if (id != prev_unique_invade_id) {
				prev_unique_invade_id = id;
				resources_updated = cPmo->invade_blocking(1, max_cores,
						scalability_graph_size, scalability_graph);
			} else {
				resources_updated = cPmo->reinvade_blocking();
			}

#	endif

		}

		setValueNumberOfThreadsToUse(cPmo->num_computing_threads);

		return resources_updated;
#endif
	}

	/**
	 * run the simulation
	 */
	void threading_simulationLoop() {

#if DELAYED_LEAVE_OMP_PARALLEL_REGION

		bool continue_simulation = true;

		// REINVADE
		threading_updateResourceUtilization();

		do
		{
			// manually update number of threads since this update cannot be executed during a parallel region
			cPmo->delayedUpdateNumberOfThreads();

			/*
			 * this flag specified, whether the following loop should be quit or not
			 */
			bool resources_updated = false;

			do
			{
				continue_simulation = simulation_loopIteration();
				resources_updated = threading_updateResourceUtilization();
			}
			while (!resources_updated && continue_simulation);

		} while(continue_simulation);
#else

		/**
		 * OLD, SLOWER VERSION
		 */

		bool continue_simulation = true;

		do {
			// REINVADE
			threading_updateResourceUtilization();

			continue_simulation = simulation_loopIteration();

		} while (continue_simulation);

#endif
	}

	bool threading_simulationLoopIteration() {
		// REINVADE
		cPmo->reinvade_blocking();

		bool continue_simulation;

		continue_simulation = simulation_loopIteration();

		return continue_simulation;
	}

	void threading_shutdown() {
		cPmo->client_shutdown_hint = ((double) getSimulationSumWorkload())
				* 0.000001;
		cPmo->retreat();

		delete cPmo;
		cPmo = nullptr;
	}

	void threading_setNumThreads(int i) {
		initial_max_cores = i;
		threading_updateResourceUtilization();
	}

	virtual ~CMainThreading() {
		if (cPmo != nullptr)
			delete cPmo;
	}
};

#endif /* CMAINTHREADINGOMP_HPP_ */
