/*
 * CMainGui.hpp
 *
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at
 * http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 18, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINGUI_HPP_
#define CMAINGUI_HPP_


#include <stdlib.h>
#include <sstream>
#include <cmath>

#include "global_config.h"

#include "libgl/draw/CGlDrawSphere.hpp"
#include "libgl/core/CGlState.hpp"
#include "lib/CEyeBall.hpp"

#include "mainvis/CCommonShaderPrograms.hpp"
#include "mainvis/CRenderWindow.hpp"

#include "libgl/hud/CGlFreeType.hpp"
#include "libgl/hud/CGlWindow.hpp"
#include "libgl/hud/CGlRenderOStream.hpp"
#include "libgl/hud/CGlHudConfig.hpp"

#include "simulations/CSimulation.hpp"

#include "mainvis/CGuiConfig.hpp"

#include "libgl/engine/CTime.hpp"
#include "libgl/engine/camera/iCamera.hpp"
#include "libgl/engine/camera/CCamera1stPerson.hpp"
#include "libgl/engine/iInputState.hpp"


#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "lib/CProcessMemoryInformation.hpp"

#include "lib/CLinesFromSVG.hpp"


#include "CMain.hpp"

#include "CSimulation_MainGuiParameters.hpp"
#include "CSimulation_MainGuiInterface.hpp"


class CMainGui :
		public CRenderWindowEventCallbacks,
		public CMain
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	using CMain::cSimulation;
	using CMain::cSimulation_MainParameters;
	using CMain::cSimulation_MainInterface;

	using CMain::simulation_loopPrefix;
	using CMain::simulation_loopSuffix;

	using CMain::threading_simulationLoopIteration;
	using CMain::threading_setNumThreads;

	CSimulation_MainGuiParameters *cSimulation_MainGuiParameters;
	CSimulation_MainGuiInterface *cSimulation_MainGuiInterface;

	typedef float TVertexScalar;

public:
	CError error;
	bool quit;

	CRenderWindow cRenderWindow;

	CGuiConfig cGuiConfig;
	CGlFreeType cGlFreeType;
	CGlRenderOStream cGlRenderOStream;

	CProcessMemoryInformation cProcessMemoryInformation;

	// parameter for model matrix
	CEyeBall<TVertexScalar> cModelEyeBall;

	// camera control
	CCamera1stPerson<TVertexScalar> camera;
	CTime cTime;
	CVector<3,TVertexScalar> player_velocity;
	iInputState inputState;

	// commonly used shader programs
	CCommonShaderPrograms cCommonShaderPrograms;


	// some opengl parameters for perspective matrix
	TVertexScalar perspective_zoom;
	TVertexScalar zoom;

	TVertexScalar near_plane;
	TVertexScalar far_plane;

	TVertexScalar camera_sphere_view_rotation_alpha;
	TVertexScalar camera_sphere_view_rotation_beta;

	/**
	 * size of domain for simulation after scaling
	 */
	TVertexScalar domain_visualization_size;


    /**
     * import lines from SVG file
     */
    CLinesFromSVG cLinesFromSVG;

    /**
     * enumeration value for screenshots
     */
	int screenshotEnumeration;

	/**
	 * Number of threads was updated during the last step.
	 *
	 * For TBB, the number of threads has to be set by the thread which created the initial threads.
	 */
	int simulation_update_number_of_threads_updated;


	/**
	 * name for the surface visualization description
	 */
	const char *dofs_vizualization_description;


	/**
	 * name for the bathymetry visualization description
	 */
	const char *boundary_vizualization_description;


#if CONFIG_ENABLE_MPI
	class CMPISharedEventData
	{
	public:
		int mpi_shared_event_type;	// 0: key down, 1: key up, 2: mouse
		int mpi_shared_event_id;
		TVertexScalar mpi_shared_mouse_point_on_plane_x, mpi_shared_mouse_point_on_plane_y;
		int mpi_shared_mouse_button;

		CMPISharedEventData(
				int i_mpi_shared_event_type,
				int i_mpi_shared_event_id = 0,
				TVertexScalar i_mpi_shared_mouse_point_on_plane_x = 0,
				TVertexScalar i_mpi_shared_mouse_point_on_plane_y = 0,
				int i_mpi_shared_mouse_button = 0
		)	:
			mpi_shared_event_type(i_mpi_shared_event_type),
			mpi_shared_event_id(i_mpi_shared_event_id),
			mpi_shared_mouse_point_on_plane_x(i_mpi_shared_mouse_point_on_plane_x),
			mpi_shared_mouse_point_on_plane_y(i_mpi_shared_mouse_point_on_plane_y),
			mpi_shared_mouse_button(i_mpi_shared_mouse_button)
		{
		}


		CMPISharedEventData() :
			mpi_shared_event_type(-1),
			mpi_shared_event_id(0),
			mpi_shared_mouse_point_on_plane_x(0),
			mpi_shared_mouse_point_on_plane_y(0),
			mpi_shared_mouse_button(0)
		{
		}
	};

	std::list<CMPISharedEventData> cMPISharedEventDataList;

#endif

	/**
	 * CONSTRUCTOR
	 */
	CMainGui(
			int i_argc,			///< number of specified arguments
			char *i_argv[]		///< array with arguments
	)	:
		CMain(i_argc, i_argv),
		quit(false),
		cRenderWindow(*this, "Sierpinski Framework", 800, 600, false, 3, 3),
		cGlRenderOStream(cGlFreeType),
		cCommonShaderPrograms(0),
		screenshotEnumeration(0),
		simulation_update_number_of_threads_updated(-1)
	{
	}



	void setupGui()
	{
		/**********************************************
		 * SETUP Simulation GUI related interfaces
		 */
		cSimulation_MainGuiParameters = &static_cast<CSimulation_MainGuiParameters&>(cSimulation->cParameters);
		cSimulation_MainGuiInterface = &static_cast<CSimulation_MainGuiInterface&>(*cSimulation);


		/**********************************************
		 * CONFIGURATION HUD
		 */
		cGlFreeType.loadFont(11);
		if (cGlFreeType.error())
		{
			std::cerr << cGlFreeType.error << std::endl;
			exit(-1);
		}
		//CError_AppendReturn(cGlFreeType);


		/**********************************************
		 * LOAD SIERPI SVG
		 */
		cLinesFromSVG.loadSVGFile("data/setupGraphics/sierpi.svg");


		/***********************************************
		 * HUD
		 */
		// then we add the other configuration parameters and setup the HUD itsetup
		cGuiConfig.setup();

		// first we add the configuration parameters of the simulation itself
		cSimulation_MainGuiParameters->setupCGlHudConfigSimulation(cGuiConfig.cGlHudConfigMainRight);

		// first we add the configuration parameters of the simulation itself
		cSimulation_MainGuiParameters->setupCGlHudConfigVisualization(cGuiConfig.cGlHudConfigVisualization);

		cGuiConfig.dofs_visualization_method = cSimulation_MainParameters->visualization_dof_method;
		cGuiConfig.boundary_visualization_method = cSimulation_MainParameters->visualization_boundary_method;

		cGuiConfig.assembleGui(cGlFreeType, cGlRenderOStream);

		cSimulation_MainGuiParameters->setupCGuiCallbackHandlers(cGuiConfig);

		// activate HUD
		cGuiConfig.setHudVisibility(true);


		/**********************************************
		 * COMMON SHADERS
		 */
		if (cCommonShaderPrograms.error())
		{
			std::cerr << cCommonShaderPrograms.error << std::endl;
			return;
		}


		/**********************************************
		 * INSTALL CCONFIG CALLBACK HANDLERS
		 */
		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.simulation_update_number_of_threads_updated = c.cSimulation_MainParameters->simulation_threading_number_of_threads;
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cSimulation_MainParameters->simulation_threading_number_of_threads);
		}


		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.callback_take_screenshot();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cGuiConfig.take_screenshot);
		}
	}



	virtual ~CMainGui()
	{
	}



	void takeNextScreenshotImage()
	{
		char buffer[1024];
		sprintf(buffer, "screenshot_%08i.bmp", screenshotEnumeration);

		cRenderWindow.saveScreenshotWithThread(buffer);

		screenshotEnumeration++;
	}



	void callback_take_screenshot()
	{
		takeNextScreenshotImage();
		cGuiConfig.take_screenshot = false;
	}



	/**
	 * reset view (rotation + position)
	 */
	void resetView()
	{
#if CONFIG_ENABLE_MPI
		if (sierpi::CGlobalComm::getCommRank() == 0)
#endif
			std::cout << "[ RESET View ]" << std::endl;


		/*
		 * update scale & translate parameters for visualization
		 */

		T domain_origin_x, domain_origin_y;
		T domain_size_x, domain_size_y;
		cSimulation->getOriginAndSize(&domain_origin_x, &domain_origin_y, &domain_size_x, &domain_size_y);

		TVertexScalar max_size = std::max(domain_size_x, domain_size_y);

		cSimulation_MainParameters->visualization_scale_x = (TVertexScalar)1.0/max_size;
		cSimulation_MainParameters->visualization_scale_y = (TVertexScalar)1.0/max_size;

#if CONFIG_SUB_SIMULATION_TSUNAMI || CONFIG_SUB_SIMULATION_BENCHMARK_SPHERE
		// scale bathymetry in z direction
		if (max_size > 10000)
		{
			cSimulation_MainParameters->visualization_scale_z = 0.5;
			cSimulation_MainParameters->visualization_data0_scale_factor = 0.2; //0.2;
			cSimulation_MainParameters->visualization_data1_scale_factor = 1/5000.0;
		}
		cSimulation_MainParameters->visualization_data0_scale_factor = 0.2; //0.2;
#endif

		cSimulation_MainParameters->visualization_translate_x = -domain_origin_x;
		cSimulation_MainParameters->visualization_translate_y = -domain_origin_y;

		cModelEyeBall.reset();
		camera.reset();

		domain_visualization_size =
			std::max(
				domain_size_x*cSimulation_MainParameters->visualization_scale_x,
				domain_size_y*cSimulation_MainParameters->visualization_scale_y
			);

		camera.setPosition(
				CVector<3,TVertexScalar>(
						0,
						domain_visualization_size*0.5,
						domain_visualization_size*1.1
				)
			);

		perspective_zoom = 1.0;

		far_plane = domain_visualization_size * (TVertexScalar)10.0;
		near_plane = domain_visualization_size * (TVertexScalar)0.01;

		camera_sphere_view_rotation_alpha = 0;
		camera_sphere_view_rotation_beta = 0;

		camera.rotate(M_PI*0.1, 0, 0);

//		camera.computeMatrices();

		cGuiConfig.visualization_enabled = true;
	}



	/**
	 * reset the simulation
	 */
	void reset()
	{
		std::cout << "[ RESET Simulation ]" << std::endl;

		// suffix first to output some interesting data
		simulation_loopSuffix();

		cSimulation_MainInterface->reset();

		/*****************************************************
		 * setup adaptive simulation
		 */
		cSimulation_MainInterface->setup_GridDataWithAdaptiveSimulation();

		simulation_loopPrefix();

		cProcessMemoryInformation.outputUsageInformation();
	}


	bool gui_key_down(int key)
	{
		TVertexScalar px, py;

		switch(key)
		{
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			simulation_update_number_of_threads_updated = key-'0';
			break;

		case 'q':
			quit = true;
			break;

		case 'e':
			resetView();
			break;

		case 'r':
			reset();
			break;

		case 'v':
			cGuiConfig.visualization_enabled ^= true;
			break;

		case ';':
			cGuiConfig.dofs_visualization_method = 0;
			cGuiConfig.boundary_visualization_method = 0;
			resetView();
			break;

		case '\'':
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
			cGuiConfig.dofs_visualization_method = 7;
			cGuiConfig.boundary_visualization_method = 7;
#else
			cGuiConfig.dofs_visualization_method = 8;
			cGuiConfig.boundary_visualization_method = 8;
#endif
			resetView();
			break;

		case 'o':
			cGuiConfig.dofs_visualization_method++;
			break;

		case 'O':
			cGuiConfig.dofs_visualization_method--;
			break;


		case 'b':
			cGuiConfig.boundary_visualization_method++;
			break;

		case 'B':
			cGuiConfig.boundary_visualization_method--;
			break;

		case 'u':
			cGuiConfig.visualization_render_wireframe++;
			break;

		case 'U':
			cGuiConfig.visualization_render_wireframe--;
			break;

		case 'p':
			cGuiConfig.visualization_render_cluster_borders++;
			break;

		case 'a':
			player_velocity[0] = std::max(player_velocity[0]-(TVertexScalar)1, (TVertexScalar)-1);	break;

		case 'd':
			player_velocity[0] = std::min(player_velocity[0]+(TVertexScalar)1, (TVertexScalar)+1);	break;

		case 'w':
			player_velocity[2] = std::max(player_velocity[2]-(TVertexScalar)1, (TVertexScalar)-1);	break;

		case 's':
			player_velocity[2] = std::min(player_velocity[2]+(TVertexScalar)1, (TVertexScalar)+1);	break;



#if 0
		/*
		 * deactivated (drawing sierpi logo)
		 */
		case 'S':
			{
				TVertexScalar scale = 0.5;

				for (auto i = cLinesFromSVG.lineList.begin(); i != cLinesFromSVG.lineList.end(); i++)
				{
					auto &list = *i;
					for (auto d = list.begin(); d != list.end(); d++)
					{
						cSimulation->setup_CellDataAt2DPosition((*d)[0]*scale, (*d)[1]*scale);
					}
				}
			}
			break;
#endif

		case 'V':
			cProcessMemoryInformation.outputUsageInformation();
			break;

		case 'l':
			cGuiConfig.run_simulation_timesteps ^= true;
			break;

		case 'm':
			computeSimulationPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputCellData(px, py);
			break;

		case 'M':
			computeSimulationPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputEdgeCommunicationInformation(px, py);
			break;

		case 'n':
			computeSimulationPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputClusterInformation(px, py);
			break;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		case '/':
#if 0
			cSimulation_MainGuiInterface->update_ClustersForMigrationByNumberOfClusters(true, 1);
#endif
			break;

		case '.':
#if 0
			cSimulation_MainGuiInterface->update_ClustersForMigrationByNumberOfClusters(false, 1);
#endif
			break;
#endif


/*
 * FULLSCREEN
 */
		case 'F':
			cRenderWindow.setWindowFullscreenState(!cRenderWindow.fullscreen_active);
			break;


/*
 * show/hide GUI
 */
		case ' ':
			cGuiConfig.setHudVisibility(!cGuiConfig.hud_visible);
			break;


/*
 * record window
 */
		case 'R':
			cGuiConfig.take_screenshot_series = !cGuiConfig.take_screenshot_series;
			break;

		default:
			return false;
		}

		return true;
	}


#if CONFIG_ENABLE_MPI
	/**
	 * synchronize keystrokes via MPI allreduce
	 */
	void mpi_test_and_sync_keystroke()
	{
		CMPISharedEventData event;

		while (true)
		{
			int mpi_event_node = -666;
			int mpi_event_node_output = 0;

			// if we have some data to send, request sending data by setting mpi_event_node to 0 or positive value
			if (cMPISharedEventDataList.size() > 0)
				mpi_event_node = mpi_rank;

			MPI_Allreduce(&mpi_event_node, &mpi_event_node_output, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD);

			// nothing to do?
			if (mpi_event_node_output < 0)
				break;

			if (mpi_event_node_output == mpi_rank)
			{
				event = cMPISharedEventDataList.front();
				cMPISharedEventDataList.pop_front();
			}


			/*
			 * Broadcast message
			 */
			MPI_Bcast(&event, sizeof(event), MPI_BYTE, mpi_event_node_output, MPI_COMM_WORLD);

			/*
			 * handle event
			 */
			assert(event.mpi_shared_event_type != -1);

			switch(event.mpi_shared_event_type)
			{
			case 0:
				if (!gui_key_down(event.mpi_shared_event_id))
					cSimulation_MainGuiInterface->gui_key_down_event(event.mpi_shared_event_id);
				break;

			case 1:
				if (!gui_key_up(event.mpi_shared_event_id))
					cSimulation_MainGuiInterface->gui_key_up_event(event.mpi_shared_event_id);
				break;

			case 2:
				switch(event.mpi_shared_event_id)
				{
				case -1:
					callback_mouse_button_up_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;

				case -2:
					callback_mouse_button_down_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;

				case -3:
					callback_mouse_motion_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;
				}
				break;
			}
		}
	}
#endif


	/**
	 * callback method called whenever a key is pressed
	 */
	void callback_key_down(
			int key,
			int mod,
			int scancode,
			int unicode
	)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

#if !CONFIG_ENABLE_MPI

		if (!gui_key_down(key))
			cSimulation_MainGuiInterface->gui_key_down_event(key);

#else

		cMPISharedEventDataList.push_back(CMPISharedEventData(0, key));

#endif
	}



	bool gui_key_up(int key)
	{
		switch(key)
		{
			case 'a':	player_velocity[0] += 1;	break;
			case 'd':	player_velocity[0] -= 1;	break;
			case 'w':	player_velocity[2] += 1;	break;
			case 's':	player_velocity[2] -= 1;	break;
			default:
				return false;
		}

		return true;
	}

	void callback_key_up(int key, int mod, int scancode, int unicode)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}


#if !CONFIG_ENABLE_MPI

		if (!gui_key_up(key))
			cSimulation_MainGuiInterface->gui_key_up_event(key);

#else

		cMPISharedEventDataList.push_back(CMPISharedEventData(1, key));

#endif
	}


	void callback_quit()
	{
		quit = true;
	}


	void callback_mouse_motion_simulation(TVertexScalar px, TVertexScalar py, int button)
	{
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_LEFT);
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_RIGHT);
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_MIDDLE);

			perspective_zoom += (TVertexScalar)(inputState.relative_mouse_y);
		}
	}


	void callback_mouse_motion(int x, int y)
	{
		inputState.update((TVertexScalar)x*2.0/(TVertexScalar)cRenderWindow.window_width-1.0, (TVertexScalar)y*2.0/(TVertexScalar)cRenderWindow.window_height-1.0);

		TVertexScalar px, py;
		computeSimulationPlaneCoords(&px, &py);

		int button = -1;

		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			button = CRenderWindow::MOUSE_BUTTON_LEFT;
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])
			button = CRenderWindow::MOUSE_BUTTON_RIGHT;
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])
			button = CRenderWindow::MOUSE_BUTTON_MIDDLE;


#if CONFIG_ENABLE_MPI

		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -3, px, py, button));

#else
		callback_mouse_motion_simulation(px, py, button);
#endif

		cGuiConfig.mouse_motion(x, cRenderWindow.window_height - y);
	}



	void callback_mouse_button_down_simulation(
		TVertexScalar i_plane_x,
		TVertexScalar i_plane_y,
		int i_button_nr
	)
	{
		cSimulation_MainGuiInterface->gui_mouse_button_down_event(i_plane_x, i_plane_y, i_button_nr);

		if (i_button_nr <= 3)
			inputState.mouse_buttons[i_button_nr] = true;
	}



	bool computeSimulationPlaneCoords(
		TVertexScalar *o_px,
		TVertexScalar *o_py
	)
	{
		if (cGuiConfig.dofs_visualization_method != 6 && cGuiConfig.dofs_visualization_method != 7)
		{
			// default visualization method
			computeMouseOnPlaneCoords(o_px, o_py);
			return true;
		}

		// spherical projection
		return computeMouseSphereToPlaneCoords(o_px, o_py);
	}



	void callback_mouse_button_down(
		int button
	)
	{
		// if the button was pressed down within the config window, do nothing
		if (cGuiConfig.mouse_button_down(button))
			return;

		TVertexScalar px, py;
		if (!computeSimulationPlaneCoords(&px, &py))
			return;

#if CONFIG_ENABLE_MPI
		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -2, px, py, button));
#else
		callback_mouse_button_down_simulation(px, py, button);
#endif
	}



	void callback_mouse_button_up_simulation(
		TVertexScalar px,
		TVertexScalar py,
		int button
	)
	{
		if (button <= 3)
			inputState.mouse_buttons[button] = false;
	}



	void callback_mouse_button_up(int button)
	{
		// if the button was pressed down within the config window, do nothing
		cGuiConfig.mouse_button_up(button);

#if CONFIG_ENABLE_MPI
		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -1, 0, 0, button));
#else
		callback_mouse_button_up_simulation(0, 0, button);
#endif
	}



	void callback_mouse_wheel(int x, int y)
	{
		if (cGuiConfig.mouse_wheel(x, y))
			return;

		perspective_zoom += (TVertexScalar)y*(-0.1);
	}



	void callback_viewport_changed(int width, int height)
	{
	}



	GLSL::vec3 light_view_pos;

	void setupBlinnShader_Surface(
		CMatrix4<TVertexScalar> &local_view_model_matrix,
		CMatrix4<TVertexScalar> &local_pvm_matrix
	)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}



	void setupBlinnShader_Boundaries(
		CMatrix4<TVertexScalar> &local_view_model_matrix,
		CMatrix4<TVertexScalar> &local_pvm_matrix
	)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.2,0.2,0.5));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.2,0.2,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();



		cCommonShaderPrograms.cBathymetry.use();
		cCommonShaderPrograms.cBathymetry.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBathymetry.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBathymetry.light0_ambient_color3_uniform.set(GLSL::vec3(0.7,0.7,0.7));
		cCommonShaderPrograms.cBathymetry.light0_diffuse_color3_uniform.set(GLSL::vec3(0.7,0.7,0.7));
		cCommonShaderPrograms.cBathymetry.light0_specular_color3_uniform.set(GLSL::vec3(0.7,0.7,0.7));
		cCommonShaderPrograms.cBathymetry.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBathymetry.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBathymetry.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBathymetry.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBathymetry.material_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBathymetry.material_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBathymetry.material_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBathymetry.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBathymetry.disable();
	}



	void setupHeightColorShader(
			CMatrix4<TVertexScalar> &local_view_model_matrix,
			CMatrix4<TVertexScalar> &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cHeightColorBlinn.use();
		cCommonShaderPrograms.cHeightColorBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cHeightColorBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cHeightColorBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cHeightColorBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		cCommonShaderPrograms.cHeightColorBlinn.setupColorScaleAndOffset(0.025, 10);
	}


	void setupSphereDistanceColorShader(
			CMatrix4<TVertexScalar> &local_view_model_matrix,
			CMatrix4<TVertexScalar> &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cSphereDistanceColorBlinn.use();
		cCommonShaderPrograms.cSphereDistanceColorBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cSphereDistanceColorBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cSphereDistanceColorBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cSphereDistanceColorBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cSphereDistanceColorBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cSphereDistanceColorBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cSphereDistanceColorBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cSphereDistanceColorBlinn.disable();

		cCommonShaderPrograms.cSphereDistanceColorBlinn.setupColorScaleAndOffset(1, 5.0);
	}



	/**
	 * render world scene
	 */
private:
	void p_renderWorld()
	{
		CGlErrorCheck();

		CMatrix4<TVertexScalar> local_model_matrix = cModelEyeBall.rotationMatrix;
		local_model_matrix.loadIdentity();

		CMatrix4<TVertexScalar> local_view_model_matrix = camera.view_matrix * local_model_matrix;
		CMatrix4<TVertexScalar> local_pvm_matrix = camera.projection_matrix*local_view_model_matrix;

		// DOF1 (BATHYMETRY)
		setupBlinnShader_Boundaries(local_view_model_matrix, local_pvm_matrix);
		boundary_vizualization_description = cSimulation_MainGuiInterface->render_boundaries(cGuiConfig.boundary_visualization_method, cCommonShaderPrograms);

		// DOF2 (WATER SURFACE)
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
		setupSphereDistanceColorShader(local_view_model_matrix, local_pvm_matrix);

#if CONFIG_SUB_SIMULATION_TSUNAMI || CONFIG_SUB_SIMULATION_BENCHMARK_SPHERE
		glEnable(GL_BLEND);

		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
		glBlendFuncSeparate(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA, GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
		glBlendColor(cGuiConfig.water_alpha, cGuiConfig.water_alpha, cGuiConfig.water_alpha, cGuiConfig.water_alpha);
#endif

		dofs_vizualization_description = cSimulation_MainGuiInterface->render_DOFs(cGuiConfig.dofs_visualization_method, cCommonShaderPrograms);

#if CONFIG_SUB_SIMULATION_TSUNAMI || CONFIG_SUB_SIMULATION_BENCHMARK_SPHERE
		glDisable(GL_BLEND);
#endif


		// CLUSTER SCANS
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		cSimulation_MainGuiInterface->render_ClusterScans(cGuiConfig.visualization_render_cluster_borders, cCommonShaderPrograms);

		// WIREFRAME
		cSimulation_MainGuiInterface->render_Wireframe(cGuiConfig.visualization_render_wireframe, cCommonShaderPrograms);

		// CLUSTERS
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		cSimulation_MainGuiInterface->render_ClusterBorders(cGuiConfig.visualization_render_cluster_borders, cCommonShaderPrograms);


		if (cGuiConfig.hud_visible)
		{
			cGlFreeType.viewportChanged(CVector<2,int>(cRenderWindow.window_width, cRenderWindow.window_height));
			cGlFreeType.setColor(GLSL::vec3(1,1,1));
			cGlFreeType.setPosition(CVector<2,int>(10, 30));
			cGlRenderOStream << "Rendering of DOFs: " << dofs_vizualization_description << std::endl;
			cGlFreeType.setPosition(CVector<2,int>(10, 10));
			cGlRenderOStream << "Rendering of boundary: " << boundary_vizualization_description << std::endl;
		}
	}

public:
	void computeMouseOnPlaneCoords(
			TVertexScalar *o_px,
			TVertexScalar *o_py
	)
	{
		CMatrix4<TVertexScalar> inv_projection_matrix = camera.projection_matrix.getInverse();

		// dirty hack: get view center from view matrix
		CVector<3,TVertexScalar> ray_start = camera.getPosition();

		// end position is somewhere at the far plane
		CVector<4,TVertexScalar> unproj_space_vector4 = inv_projection_matrix*vec4f(inputState.mouse_x, -inputState.mouse_y, -1, 1);
		CVector<3,TVertexScalar> unproj_space_vector3 = unproj_space_vector4 / unproj_space_vector4[3];

		CVector<3,TVertexScalar> ray_dir = camera.view_matrix.getTranspose3x3() * unproj_space_vector3;

		// compute point on plane
		CVector<3,TVertexScalar> pos = ray_start-ray_dir*(ray_start[1]/ray_dir[1]);
		*o_px = pos[0]/cSimulation_MainParameters->visualization_scale_x;
		*o_py = -pos[2]/cSimulation_MainParameters->visualization_scale_y;

		*o_px -= cSimulation_MainParameters->visualization_translate_x;
		*o_py -= cSimulation_MainParameters->visualization_translate_y;
	}



	bool computeMouseSphereToPlaneCoords(
			TVertexScalar *o_px,
			TVertexScalar *o_py
	)
	{
		CMatrix4<TVertexScalar> inv_projection_matrix = camera.projection_matrix.getInverse();

		if (std::isnan(inv_projection_matrix[0][0]))
			return false;

		// dirty hack: get view center from view matrix
		CVector<3,TVertexScalar> ray_o = camera.getPosition();

		// end position is somewhere at the far plane
		CVector<4,TVertexScalar> unproj_space_vector4 = inv_projection_matrix*vec4f(inputState.mouse_x, -inputState.mouse_y, -1, 1);
		CVector<3,TVertexScalar> unproj_space_vector3 = unproj_space_vector4 / unproj_space_vector4[3];

		CVector<3,TVertexScalar> ray_d = camera.view_matrix.getTranspose3x3() * unproj_space_vector3;

//		ray_start[2] = -ray_start[2];
//		ray_dir[2] = -ray_dir[2];


		/*
		 * [BEGIN]
		 * The following piece of code is based on the following URL!!!
		 *
		 * http://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection
		 */
		TVertexScalar r = 1.0;

		TVertexScalar a = ray_d.dotProd(ray_d);
		TVertexScalar b = (TVertexScalar)2.0*(ray_d.dotProd(ray_o));
		TVertexScalar c = ray_o.dotProd(ray_o)-(TVertexScalar)(r*r);

		TVertexScalar dist = b*b-(TVertexScalar)4.0*a*c;

		if (dist < (TVertexScalar)0)
		{
			*o_px = std::numeric_limits<TVertexScalar>::infinity();
			*o_py = std::numeric_limits<TVertexScalar>::infinity();
			return false;
		}

		TVertexScalar distSqrt = std::sqrt(dist);

		TVertexScalar q;
	    if (b < 0)
	        q = (-b - distSqrt)/(TVertexScalar)2.0;
	    else
	        q = (-b + distSqrt)/(TVertexScalar)2.0;

	    // compute t0 and t1
	    TVertexScalar t0 = q / a;
	    TVertexScalar t1 = c / q;

	    // make sure t0 is smaller than t1
	    if (t0 > t1)
	    	std::swap(t0, t1);

	    if (t1 < 0)
	        return false;


	    TVertexScalar t;

	    if (t0 < 0)
	        t = t1;		// if t0 is less than zero, the intersection point is at t1
	    else
	        t = t0;		// else the intersection point is at t0

	    /*
	     * [END]
	     *
	     */

		// compute point sphere
		CVector<3,TVertexScalar> pos = ray_o+ray_d*t;

		TVertexScalar x = pos[2];
		TVertexScalar y = pos[0];
		TVertexScalar z = pos[1];
		pos[0] = x;
		pos[1] = y;
		pos[2] = z;

		CCube_To_Sphere_Projection::project3DTo2D(pos[0], pos[1], pos[2], o_px, o_py);

		return true;
	}



	bool simulationTimesteps()
	{
		if (cGuiConfig.run_simulation_timesteps)
		{
			/*
			 * the number of available openmp threads are set only during initialization when not running
			 * in interactive mode (GUI)
			 */
			for (int i = 0; i < cGuiConfig.visualization_simulation_steps_per_frame; i++)
				threading_simulationLoopIteration();
		}

		return true;
	}



	/**
	 * render simulation data to screen
	 */
	void render()
	{
		if (	cGuiConfig.dofs_visualization_method == 6 || cGuiConfig.dofs_visualization_method == 7	||
				cGuiConfig.boundary_visualization_method == 6 || cGuiConfig.boundary_visualization_method == 7
		)
		{
			camera_sphere_view_rotation_alpha += (-player_velocity[0]*(TVertexScalar)cTime.frame_elapsed_seconds*30.0);
			camera_sphere_view_rotation_beta += (-player_velocity[2]*(TVertexScalar)cTime.frame_elapsed_seconds*30.0);

			if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			{
				camera_sphere_view_rotation_alpha += (-inputState.relative_mouse_x*cTime.frame_elapsed_seconds*zoom*cRenderWindow.window_width*12.0);
				camera_sphere_view_rotation_beta += (-inputState.relative_mouse_y*cTime.frame_elapsed_seconds*zoom*cRenderWindow.window_width*12.0);
			}

			camera.view_matrix =
					GLSL::translate<TVertexScalar>(0, 0, -3)*
					GLSL::rotate<TVertexScalar>(camera_sphere_view_rotation_beta, 1, 0, 0)*
					GLSL::rotate<TVertexScalar>(camera_sphere_view_rotation_alpha, 0, 1, 0);

			camera.position = camera.view_matrix.getInverse()*GLSL::vec4(0,0,0,1);
		}
		else
		{
			camera.moveRelative(player_velocity*(TVertexScalar)cTime.frame_elapsed_seconds*domain_visualization_size*0.5);

			if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			{
				camera.rotate(	-inputState.relative_mouse_y*cTime.frame_elapsed_seconds*zoom*600.0,
								-inputState.relative_mouse_x*cTime.frame_elapsed_seconds*zoom*600.0,
								0
							);
			}

			camera.computeMatrices();
		}

		zoom = std::exp(perspective_zoom)*0.2;

		TVertexScalar fzoom = zoom*near_plane;

		camera.frustum(-fzoom*cRenderWindow.aspect_ratio, fzoom*cRenderWindow.aspect_ratio, -fzoom, fzoom, near_plane, far_plane);

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);


		if (
				cGuiConfig.dofs_visualization_method == 6 || cGuiConfig.dofs_visualization_method == 7	||
				cGuiConfig.boundary_visualization_method == 6 || cGuiConfig.boundary_visualization_method == 7
		)
			light_view_pos = camera.view_matrix*GLSL::vec3(2.0*domain_visualization_size*0.3, 4.0*domain_visualization_size*0.3, 6.0f*domain_visualization_size*0.3);
		else
			light_view_pos = camera.view_matrix*GLSL::vec3(2.0*domain_visualization_size*0.3, 4.0*domain_visualization_size*0.3, -6.0f*domain_visualization_size*0.3);

		p_renderWorld();

		cGuiConfig.render();

		inputState.clearRelativeMovement();
	}


	/**
	 * run the simulation loop
	 */
	void run()
	{
		// update view using simulation parameters
		resetView();

		simulation_loopPrefix();

		while (!quit)
		{
			cTime.update();

			if (simulation_update_number_of_threads_updated != -1)
			{
#if CONFIG_ENABLE_SCAN_DATA
				if (cSimulation->cParameters.simulation_threading_number_of_threads > simulation_update_number_of_threads_updated)
				{
					// shrink
					cSimulation->updateClusterScanInformation(simulation_update_number_of_threads_updated);

					threading_setNumThreads(simulation_update_number_of_threads_updated);
				}
				else
				{
					// increase
					threading_setNumThreads(simulation_update_number_of_threads_updated);

					cSimulation->updateClusterScanInformation(simulation_update_number_of_threads_updated);
				}
#else
				threading_setNumThreads(simulation_update_number_of_threads_updated);
#endif

				simulation_update_number_of_threads_updated = -1;
			}

			if (simulation_run_for_fixed_timesteps != -1)
			{
				if (simloop_timestep_nr >= simulation_run_for_fixed_timesteps)
					quit = true;
			}

			if (simulation_run_for_fixed_simulation_time != -1)
			{
				if (cSimulation_MainParameters->simulation_timestamp_for_timestep > simulation_run_for_fixed_simulation_time)
					quit = true;
			}

			std::ostringstream buf;

			if (cTime.fpsUpdatedInLastFrame)
			{
				buf << "Sierpinski running with " << cSimulation->cParameters.simulation_threading_number_of_threads << " threads @ " << cTime.fps << " FPS";
				buf << " | ";
#if CONFIG_ENABLE_MPI
				buf << "MPI rank: " << cSimulation->cParameters.parallelization_mpi_rank;
#endif
				buf << " | (" << cSimulation->cParameters.number_of_local_cells << "/" << cSimulation->cParameters.number_of_global_cells << ") Cells";
				buf << " | (" << cSimulation->cParameters.number_of_local_clusters << "/" << cSimulation->cParameters.number_of_global_clusters << ") Clusters";
				buf << " | ";
				buf << ((TVertexScalar)cSimulation->cParameters.number_of_local_cells*cGuiConfig.visualization_simulation_steps_per_frame*cTime.fps)*0.000001 << " Mega Triangles per second (inaccurate)";
			}

			if (cSimulation->cParameters.simulation_random_raindrops_activated)
			{
				raindropsRefine();
			}

			if (cGuiConfig.visualization_enabled)
			{
				glClearColor(0,0,0,0);
				glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				/*
				 * SIMULATION TIMESTEP
				 */
				simulationTimesteps();

				/*
				 * UPDATE FRAME
				 */
				render();

				if (cTime.fpsUpdatedInLastFrame)
				{
					cRenderWindow.setWindowTitle(buf.str().c_str());
				}

				cRenderWindow.eventLoop();

				if (cGuiConfig.take_screenshot_series)
				{
					takeNextScreenshotImage();
				}

				cRenderWindow.swapBuffer();
			}
			else
			{
				cRenderWindow.eventLoop();

				if (cTime.fpsUpdatedInLastFrame)
					std::cout << buf.str() << std::endl;

				simulationTimesteps();
			}

#if CONFIG_ENABLE_MPI
			mpi_test_and_sync_keystroke();
#endif
		}

		simulation_loopSuffix();
	}
};




#endif /* CMAINGUI_HPP_ */
