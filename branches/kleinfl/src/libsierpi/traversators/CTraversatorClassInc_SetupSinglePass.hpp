/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CTraversator_Setup2Pass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


/**
 * recursive method for forward traversal
 */
TRecursiveMethod sfcRecursiveMethod_Forward;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory cTriangleFactory;


/**
 * setup the initial cluster traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &i_cTriangleFactory	///< triangle factory to find the first method
)
{
	cTriangleFactory = i_cTriangleFactory;

	sfcRecursiveMethod_Forward = getSFCMethod(cTriangleFactory);
}


/**
 * setup the parameters with the one by the parent cluster
 */
void setup_Cluster(
		TThisClass &p_parent,					///< information of parent traversator
		CTriangle_Factory &i_cTriangleFactory	///< triangle factory to find the first method
)
{
	// make sure that this is really a root node
	assert(i_cTriangleFactory.clusterTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	setup_sfcMethods(i_cTriangleFactory);

	cKernelClass.setup_WithKernel(p_parent.cKernelClass);
}
