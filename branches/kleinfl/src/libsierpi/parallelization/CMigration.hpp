/*
 * CMigration.hpp
 *
 *  Created on: Mar 1, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMIGRATION_HPP_
#define CMIGRATION_HPP_

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
#	include "libsierpi/parallelization/CGlobalComm.hpp"
#	include <vector>
#endif

#include <global_config.h>
#include <iostream>
#include <stdlib.h>

namespace sierpi
{


/**
 * this class has to be inherited to classes making the migration available by a raw copy
 */
class CMigration_RawClass
{
};

#if DEBUG

	/*
	 * MPI migration request with MPI_REQUEST_NULL tests
	 */
#	define MIGRATION_REQUEST_SKELETON(name)							\
	MPI_Request mpi_request;										\
	public:															\
	name() : mpi_request(MPI_REQUEST_NULL) {}						\
	~name() { /*assert(mpi_request == MPI_REQUEST_NULL);*/ }			\
	inline void wait()												\
	{																\
		assert(mpi_request != MPI_REQUEST_NULL);					\
		MPI_Wait(&mpi_request, MPI_STATUS_IGNORE);					\
		mpi_request = MPI_REQUEST_NULL;							\
	}

#else

	/*
	 * MPI migration request skeleton without debugging
	 */
#	define MIGRATION_REQUEST_SKELETON(name)							\
	MPI_Request mpi_request;										\
	public:															\
	inline void wait()												\
	{																\
		MPI_Wait(&mpi_request, MPI_STATUS_IGNORE);					\
	}

#endif



#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION



template <typename T>
class CMigrationClass
{
	MIGRATION_REQUEST_SKELETON(CMigrationClass)


	/**
	 * direct transfer for objects inheriting the class CMigration_RawCopy
	 */
	inline
	void send(
		T &i_object,
		int i_dst_rank
	)
	{
#if DEBUG
		// test for raw copy object
		(CMigration_RawClass)i_object;
//		assert(mpi_request == MPI_REQUEST_NULL);
#endif
		MPI_Isend(&i_object, sizeof(T), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
	}



	/**
	 * direct transfer for objects inheriting the class CMigration_RawCopy
	 */
	static
	inline
	void recv(
		T &i_object,
		int i_src_rank
	)
	{
#if DEBUG
		// test whether this object really supports being raw copied
		(CMigration_RawClass)i_object;
#endif

#if DEBUG
		MPI_Status mpi_status;
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T))
		{
			std::cerr << "MPI raw RECV ERROR:" << count << " != " << sizeof(T) << std::endl;
			assert(false);
			exit(-1);
		}

#else
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	}
};




template <typename T>
class CMigrationAtomic
{
	MIGRATION_REQUEST_SKELETON(CMigrationAtomic)

	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	inline
	void send(
		T &i_object,
		int i_dst_rank
	)
	{
		assert(mpi_request == MPI_REQUEST_NULL);
		MPI_Isend(&i_object, sizeof(T), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
	}


	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	inline
	void recv(
		T &i_object,
		int i_src_rank
	)
	{
#if DEBUG
		MPI_Status mpi_status;

		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T))
		{
			std::cerr << "count != i_size" << std::endl;
			assert(false);
			exit(-1);
		}
#else
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	}
};



template <typename T>
class CMigrationAtomicArray
{
	MPI_Request mpi_request;

#if DEBUG
	MPI_Request mpi_request_size;
	size_t size;
#endif

public:
	CMigrationAtomicArray() :
		mpi_request(MPI_REQUEST_NULL)
#if DEBUG
		,
		mpi_request_size(MPI_REQUEST_NULL)
#endif
	{}



	~CMigrationAtomicArray()
	{
		assert(mpi_request == MPI_REQUEST_NULL);
	}



	inline void wait()
	{
#if DEBUG
		MPI_Wait(&mpi_request_size, MPI_STATUS_IGNORE);
#endif
		MPI_Wait(&mpi_request, MPI_STATUS_IGNORE);
	}



	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	inline
	void send(
		T *i_array,
		size_t i_size,
		int i_dst_rank
	) {
#if DEBUG
		size = i_size;
		assert(mpi_request_size == MPI_REQUEST_NULL);
		MPI_Isend(&size, sizeof(size_t), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request_size);
#endif

		assert(mpi_request == MPI_REQUEST_NULL);
		assert(i_size > 0);
		MPI_Isend(i_array, sizeof(T)*i_size, MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);

	}



	/**
	 * Special direct transfer for arrays with atomic types (int, char, float, etc.)
	 *
	 * return number of array elements
	 */
	inline
	int recv(
		T *i_array,			// destination pointer
		size_t i_max_size,	// maximum array size
		int i_src_rank
	)
	{
		MPI_Status mpi_status;
		int count;

#if DEBUG
		MPI_Recv(&size, sizeof(size), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(size_t))
		{
			std::cerr << "count != i_size" << std::endl;
			assert(false);
			exit(-1);
		}
#endif

		MPI_Recv(i_array, sizeof(T)*i_max_size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

#if DEBUG
//		assert(size == count/sizeof(T));
#endif

		// optimized by precompiler
		if (sizeof(T) == 1)
			return count;
		if (sizeof(T) == 2)
			return count >> 1;
		if (sizeof(T) == 4)
			return count >> 2;
		if (sizeof(T) == 8)
			return count >> 3;
		if (sizeof(T) == 16)
			return count >> 4;

		return count/sizeof(T);
	}


	/**
	 * Special direct transfer for arrays with atomic types (int, char, float, etc.)]
	 *
	 * The size of the buffers have to match
	 */
#if 0 // not used
	inline
	void recvMatchingSize(
		T *i_array,			///< destination pointer
		size_t i_size,		///< elements in array
		int i_src_rank		///< source rank
	)
	{
		MPI_Status mpi_status;

#if DEBUG
		int count;

		MPI_Recv(&size, sizeof(size), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(size_t))
		{
			std::cerr << "count != sizeof_size_t)" << std::endl;
			assert(false);
			exit(-1);
		}
#endif

		MPI_Recv(i_array, sizeof(T)*i_size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

#if DEBUG
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T)*i_size)
		{
			std::cerr << "count != i_size" << std::endl;
			assert(false);
			exit(-1);
		}
#endif
	}
#endif
};



template <typename T>
class CMigrationVector
{
	MPI_Request mpi_request;

	// temporary storage for vector size since size is only implicitly stored in std::vector
	size_t vector_size;

	CMigrationAtomic<size_t> migration_vector_size;

public:

#if DEBUG
	CMigrationVector() :
		mpi_request(MPI_REQUEST_NULL)
	{}

	~CMigrationVector()
	{
		assert(mpi_request == MPI_REQUEST_NULL);
	}
#endif



public:
	/**
	 * receive a std::vector
	 */
	inline
	void send(
		std::vector<T> &i_vector,
		int i_dst_rank
	)
	{
		/*
		 * store size to i_vector_migration_data.size to avoid blocking communication
		 */
		vector_size = i_vector.size();
		migration_vector_size.send(vector_size, i_dst_rank);

		assert(mpi_request == MPI_REQUEST_NULL);
		if (vector_size > 0)
			MPI_Isend(i_vector.data(), sizeof(T)*vector_size, MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
	}


	inline void wait()
	{
		migration_vector_size.wait();
		if (vector_size > 0)
			MPI_Wait(&mpi_request, MPI_STATUS_IGNORE);
	}


	/**
	 * receive a std::vector
	 */
	inline
	void recv(
		std::vector<T> &i_vector,
		int i_src_rank
	)
	{
		/**
		 * store size to i_vector_migration_data.size to avoid blocking communication
		 */
		migration_vector_size.recv(vector_size, i_src_rank);

		if (vector_size > 0)
		{
			i_vector.resize(vector_size);
			MPI_Recv(i_vector.data(), sizeof(T)*vector_size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	}
};


#endif



class CMigration
{
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

public:
	class vector_migration_data
	{
	public:
		size_t size;
	};


	/**
	 * direct transfer for objects inheriting the class CMigration_RawCopy
	 */
	template <typename T>
	static
	inline
	void sendRawClass(
		T &i_object,
		int i_dst_rank
	)
	{
#if DEBUG
		// test for raw copy object
		(CMigration_RawClass)i_object;
#endif

		MPI_Request mpi_request;
		MPI_Isend(&i_object, sizeof(T), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
		MPI_Request_free(&mpi_request);
	}



	/**
	 * direct transfer for objects inheriting the class CMigration_RawCopy
	 */
	template <typename T>
	static
	inline
	void recvRawClass(
		T &i_object,
		int i_src_rank
	)
	{
#if DEBUG
		// test whether this object really supports being raw copied
		(CMigration_RawClass)i_object;
#endif

#if DEBUG
		MPI_Status mpi_status;
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T))
		{
			std::cerr << "MPI raw RECV ERROR:" << count << " != " << sizeof(T) << std::endl;
			assert(false);
			exit(-1);
		}

#else
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif

	}



public:
	/**
	 * receive a std::vector
	 */
	template <typename T>
	static
	inline
	void sendVector(
		std::vector<T> &i_vector,
		vector_migration_data &i_vector_migration_data,
		int i_dst_rank
	)
	{
		/**
		 * store size to i_vector_migration_data.size to avoid blocking communication
		 */
		i_vector_migration_data.size = i_vector.size();

		sendAtomic(i_vector_migration_data.size, i_dst_rank);

		MPI_Request mpi_request;
		MPI_Isend(i_vector.data(), sizeof(T)*i_vector_migration_data.size, MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
		MPI_Request_free(&mpi_request);
	}


	/**
	 * receive a std::vector
	 */
	template <typename T>
	static
	inline
	void recvVector(
		std::vector<T> &i_vector,
		int i_src_rank
	)
	{
		/**
		 * store size to i_vector_migration_data.size to avoid blocking communication
		 */
		size_t size;
		recvAtomic(size, i_src_rank);

		i_vector.resize(size);

		MPI_Recv(i_vector.data(), sizeof(T)*size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}



	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	template <typename T>
	static
	inline
	void sendAtomic(
			T &i_object,
			int i_dst_rank
	)
	{
		MPI_Request mpi_request;
		MPI_Isend(&i_object, sizeof(T), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
		MPI_Request_free(&mpi_request);
	}



	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 *
	 * store mpi request to mpi_request
	 */
	template <typename T>
	static
	inline
	void sendAtomic(
			T &i_object,
			int i_dst_rank,
			MPI_Request *mpi_request
	)
	{
		MPI_Isend(&i_object, sizeof(T), MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, mpi_request);
	}



	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	template <typename T>
	static
	inline
	void recvAtomic(
			T &i_object,
			int i_src_rank
	)
	{
#if DEBUG
		MPI_Status mpi_status;

		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T))
		{
			std::cerr << "count != i_size" << std::endl;
			assert(false);
			exit(-1);
		}
#else
		MPI_Recv(&i_object, sizeof(T), MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	}



	/**
	 * Special direct transfer for atomic types (int, char, float, etc.)
	 */
	template <typename T>
	static
	inline
	void sendAtomicArray(
			T *i_array,
			size_t i_size,
			int i_dst_rank
	)
	{
		MPI_Request mpi_request;
		MPI_Isend(i_array, sizeof(T)*i_size, MPI_BYTE, i_dst_rank, 0, MPI_COMM_WORLD, &mpi_request);
		MPI_Request_free(&mpi_request);
	}



	/**
	 * Special direct transfer for arrays with atomic types (int, char, float, etc.)
	 *
	 * return number of array elements
	 */
	template <typename T>
	static
	inline
	int recvAtomicArray(
			T *i_array,			// destination pointer
			size_t i_max_size,	// maximum array size
			int i_src_rank
	)
	{
		MPI_Status mpi_status;
		MPI_Recv(i_array, sizeof(T)*i_max_size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		// optimized by precompiler
		if (sizeof(T) == 1)
			return count;
		if (sizeof(T) == 2)
			return count >> 1;
		if (sizeof(T) == 4)
			return count >> 2;
		if (sizeof(T) == 8)
			return count >> 3;
		if (sizeof(T) == 16)
			return count >> 4;

		return count/sizeof(T);
	}



	/**
	 * Special direct transfer for arrays with atomic types (int, char, float, etc.)]
	 *
	 * The size of the buffers have to match
	 */
	template <typename T>
	static
	inline
	void recvAtomicArrayMatchingSize(
			T *i_array,			// destination pointer
			size_t i_size,		// elements in array
			int i_src_rank
	)
	{
		MPI_Status mpi_status;

		MPI_Recv(i_array, sizeof(T)*i_size, MPI_BYTE, i_src_rank, 0, MPI_COMM_WORLD, &mpi_status);

		int count;
		MPI_Get_count(&mpi_status, MPI_BYTE, &count);

		if (count != sizeof(T)*i_size)
		{
			std::cerr << "count != i_size" << std::endl;
			assert(false);
			exit(-1);
		}
	}

#endif

};


}


#endif
