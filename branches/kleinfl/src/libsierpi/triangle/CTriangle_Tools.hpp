/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRIANGLE_TOOLS_HPP_
#define CTRIANGLE_TOOLS_HPP_

namespace sierpi
{


class CTriangle_Tools
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:

	/**
	 * return level of detail
	 */
	static inline T getLODFromDepth(
			int i_depth
	)	{
		return ((T)i_depth + (T)1.0)*(T)0.5;
	}

	/**
	 * get center of weight for triangle
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPoint(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	) {
#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT

		// use midpoint on hypotenuse
		*o_mx = (vright_x)*(T)(0.5) +
				(vleft_x)*(T)(0.5);

		*o_my = (vright_y)*(T)(0.5) +
				(vleft_y)*(T)(0.5);

#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER

		// use midpoint on quarter
		*o_mx = (vright_x)*(T)(0.25) +
				(vleft_x)*(T)(0.25);

		*o_my = (vright_y)*(T)(0.25) +
				(vleft_y)*(T)(0.25);

		*o_mx = 0;
		*o_my = 0;

#else

		// use midpoint at center of mass
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);

#endif
	}



	/**
	 * get center of weight for triangle for both children
	 *
	 *
	 * quarter sampling points:
	 *
	 * |\
	 * |  \
	 * |    \
	 * |   a  \
	 * |      / \
	 * |    /  b  \
	 * |  /         \
	 * |/_____________\
	 *
	 * a = (1/4, 1/2)
	 * b = (1/2, 1/4)
	 */
	template <typename T>
	inline static void computeAdaptiveSamplingPointForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT

		// use midpoint on hypotenuse
		*o_left_mx = (vright_x)*(T)(1.0/2.0) +
				(vleft_x)*(T)(1.0/2.0);

		*o_left_my = (vright_y)*(T)(1.0/2.0) +
				(vleft_y)*(T)(1.0/2.0);

		*o_right_mx = *o_left_mx;
		*o_right_my = *o_left_my;

#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER

		std::cout << "TODO: verify this stuff" << std::endl;
		T dleft_dx = vleft_x - vtop_x;
		T dleft_dy = vleft_y - vtop_y;

		T dright_dx = vright_x - vtop_x;
		T dright_dy = vright_y - vtop_y;

		*o_left_mx = vtop_x + (T)0.5*dleft_dx;
		*o_left_my = vtop_y + (T)0.5*dleft_dy;

		*o_right_mx = vtop_x + (T)0.5*dright_dx;
		*o_right_my = vtop_y + (T)0.5*dright_dy;

#else

		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;

#endif
	}
};


}

#endif
