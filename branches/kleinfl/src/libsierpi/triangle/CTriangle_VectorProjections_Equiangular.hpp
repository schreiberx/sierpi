/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Dec 10, 2013
 *      Author: Florian Klein <kleinfl@in.tum.de>
 */

#ifndef XCPROJECTIONS_HPP_
#define XCPROJECTIONS_HPP_

#include <cmath>
#include <cassert>

/**
 * This class contains helper methods to project 2D problems stored in
 * world-space to a 2D problem stored in an edge-space.
 *
 * All projects are taken from the viewpoint of the reference element.
 */
class CTriangle_VectorProjections_Equiangular
{
public:

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

	/**
	 * Changes face-based cube coordinates to face-based coordinates according
	 * to equiangular projection.
	 * 1. Coordinates in [-1; 1] are scaled to central angles in [-pi/4; pi/4]
	 * 		alpha = x * pi / 4
	 * 		beta = y * pi / 4
	 * 2. New coordinates in [-1; 1] corresponding to the central angles are computed
	 * 		x* = 1 * tan(alpha)
	 * 		y* = 1 * tan(beta)
	 * 		Note: Cube has side length 2
	 */
	template <typename T>
	inline static void changeCubeCoordinatesToEquiangular(
			T *io_vertex_x,
			T *io_vertex_y
			)
	{
		*io_vertex_x = (T)1.0 * tan( *io_vertex_x * M_PI / (T)4.0 );
		*io_vertex_y = (T)1.0 * tan( *io_vertex_y * M_PI / (T)4.0 );
	}

	template <typename T>
	inline static void changeCubeCoordinateEquiangularToStandard(
			T *io_vertex_x
			)
	{
		T alpha = atan( *io_vertex_x );
		*io_vertex_x = alpha * (T)4.0 / M_PI;
	}

	template <typename T>
	inline static void changeCubeCoordinatesEquiangularToStandard(
			T *io_vertex_x,
			T *io_vertex_y
			)
	{
		T alpha = atan( *io_vertex_x );
		T beta = atan( *io_vertex_y );
		*io_vertex_x = alpha * (T)4.0 / M_PI;
		*io_vertex_y = beta * (T)4.0 / M_PI;
	}
#endif
};


#endif /* CFLUXPROJECTIONS_HPP_ */
