/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Dec 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CPROJECTIONS_HPP_
#define CPROJECTIONS_HPP_

#include <cmath>
#include <cassert>

#include "libsierpi/triangle/CTriangle_VectorProjections_Equidistant.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections_Equiangular.hpp"
#include "libmath/CMatrix.hpp"
#include "libmath/CVector.hpp"

/**
 * This class contains helper methods to project 2D problems stored in
 * world-space to a 2D problem stored in an edge-space.
 *
 * All projects are taken from the viewpoint of the reference element.
 */
class CTriangle_VectorProjections
{
public:
	/************************************************
	 * Project from CELL space to another space by giving X-asis of new space
	 ************************************************/

	/**
	 * Project from CELL space to another space by giving X-asis of CELL reference space
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void referenceToWorld(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis reference vector in world-space (x component)
			T i_by		///< x-axis reference vector in world-space (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(-i_by);
		*io_hv =	(*io_hu)	*	(i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Project from WORLD space to REFERENCE space.
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void worldToReference(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Represent Vector from Reference space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementToBasisWithXAxis(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}



	/**
	 * represent Vector from CELL space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementWithXAxisToWorldSpace(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}




	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T, int edge_id>
	inline static void toEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		/*
		 * hypotenuse edge
		 */
		if (edge_id == 0)
		{
			T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
			*io_hv = -*io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
			*io_hu = tmp;
			return;
		}

		/*
		 * right edge
		 */
		if (edge_id == 1)
		{
			T tmp = -*io_hu;
			*io_hv = -*io_hv;
			*io_hu = tmp;
			return;
		}


		/*
		 * left edge
		 */
		if (edge_id == 2)
		{
			T tmp = -*io_hv;
			*io_hv = *io_hu;
			*io_hu = tmp;
			return;
		}

		assert(false);
	}



	/************************************************
	 * HYPOTENUSE STUFF
	 ************************************************/

	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = -*io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hu = -tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from hyp edge space
	 */
	template <typename T>
	inline static void fromHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}



	/************************************************
	 * RIGHT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hu;
		*io_hv = -*io_hv;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		// nothing to do
	}



	/**
	 * compute backprojected components of (hu,hv) from right edge space
	 */
	template <typename T>
	inline static void fromRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		*io_hu = -*io_hu;
		*io_hv = -*io_hv;
	}


	/************************************************
	 * LEFT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hv;
		*io_hv = *io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from left edge space
	 */
	template <typename T>
	inline static void fromLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1


//	template <typename T>
//	inline static void setRotationMatrixToDistorted(
//			const T i_coordinates3D[3][3],
//			CMatrix3<T> *rotation_matrix,
//			const CCellData *i_cellData
//	){
//
//	}

	template <typename T>
	inline static void matrixTransformation(
			const T i_transformationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = (T)i_transformationMatrix[0][0] * *io_hu + (T)i_transformationMatrix[0][1] * *io_hv;
		*io_hv = (T)i_transformationMatrix[1][0] * *io_hu + (T)i_transformationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
	}

	template <typename T>
	inline static void fromAnyEdgeToEdgeSpace(
			const T i_rotationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = (T)i_rotationMatrix[0][0] * *io_hu + (T)i_rotationMatrix[0][1] * *io_hv;
		*io_hv = (T)i_rotationMatrix[1][0] * *io_hu + (T)i_rotationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
	}

	template <typename T>
	inline static void fromEdgeSpace(
			const T i_rotationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
//		T det = i_rotationMatrix[0][0] * i_rotationMatrix[1][1] - (T)i_rotationMatrix[0][1] * (T)i_rotationMatrix[1][0];
//		T idet = (T)1 / det;
//		T tmp = idet * (i_rotationMatrix[1][1] * *io_hu - i_rotationMatrix[0][1] * *io_hv);
//		*io_hv = idet * (i_rotationMatrix[0][0] * *io_hu - i_rotationMatrix[1][0] * *io_hv);
//		*io_hu = tmp;
//		std::cout << det << std::endl;
//		std::cout << *io_hu << ", " << *io_hv << std::endl;
		T tmp = (T)i_rotationMatrix[0][0] * *io_hu - (T)i_rotationMatrix[0][1] * *io_hv;
		*io_hv = -(T)i_rotationMatrix[1][0] * *io_hu + (T)i_rotationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
//		std::cout << "xx " << *io_hu << ", " << *io_hv << std::endl;
	}

	template <typename T>
	inline static void fromHypEdgeSpace(
			const T i_rotationMatrix[2][2],
			T *io_hu,
			T *io_hv,
			T i_hypEdgeLength
	)
	{
		T tmp = (T)i_rotationMatrix[1][1] * *io_hu - (T)i_rotationMatrix[0][1] * *io_hv;
		*io_hv = -(T)i_rotationMatrix[1][0] * *io_hu + (T)i_rotationMatrix[0][0] * *io_hv;
		*io_hu = tmp;
	}

	/*
	 * This methods computes the radius r_f of incircle of the flat distorted triangle.
	 * Afterwards the law of cosines is used, in order to compute the angle alpha between
	 * the two legs of the isoscales triangle. Which is needed for computation of the
	 * spherical incircle radius r_i.
	 *
	 *
         * a² = b² + c² - 2bc * cos(alpha)		//law of cosines
         *
	 * a = 2 * r_f
	 * b = r_s
	 * c = r_s
	 *
	 * (2 * r_f)² = 2 * r_s² - 2 * r_s² * cos(alpha)
	 *
	 * cos(alpha) = (2 * r_s² - 4 * r_f²) / (2 * r_s²)
	 *            = 1 - 2 * r_f² / r_s²
	 *
	 * r_i = (alpha/2) * r_s
	 *
	 *
	 *            ,  ´|`.
	 *    r_s , ´     |  \ r_s
	 *    . ´      r_f|   \
	 * ,´a`,__________|    )__
	 * ` .´           |    )
	 *     ` .     r_f|   /
	 *     r_s ` .    |  / r_s
	 *             ` .|,´
	 *
	 *
	 * a = alpha
	 * ref:
	 * - http://en.wikipedia.org/wiki/Incircle_and_excircles_of_a_triangle
	 * - http://en.wikipedia.org/wiki/Law_of_cosines
	 */
	template <typename T>
	inline static T getIncircleRadius(
			T i_hypEdgeLength, 		// length of the hyp edge
			T i_rightEdgeLength,	// length of the right edge
			T i_leftEdgeLength,		// length of the left edge
			T i_cellArea
//			T i_sphereRadius 		// radius of sphere
	){

#if true
		T h = i_hypEdgeLength;
		T r = i_rightEdgeLength;
		T l = i_leftEdgeLength;
		T s = (h + l + r) * (T)0.5;
//		T flatRadius = std::sqrt((s*s*s - s*s*h - s*s*r - s*s*l + s*h*r + s*h*l + s*r*l - h*r*l)/s);
//		flatRadius = std::sqrt( ( (s - h - r - l) * s + h*r + h*l + r*l ) - h*r*l/s );
//		flatRadius = std::sqrt( ( -s * s + h*r + h*l + r*l ) - h*r*l/s );
		T flatRadius = i_cellArea / s;

		return flatRadius;

#else
		T alpha = acos((T)1.0-(T)2.0*flatRadius*flatRadius/(i_sphereRadius * i_sphereRadius));

		return alpha * i_sphereRadius / (T) 2.0;
#endif
	}


	/*
	 * Sets the rotation matrix needed to compute
	 * an (edge length conserving) 2D projection of a triangle
	 * given its 3D coordinates.
	 *
	 * This matrix is used to compute the edge rotation matrices below.
	 *
	 */
	template <typename T>
	inline static void set2DProjectionMatrix(
			T o_projectionMatrix[3][3],

			CVector<3, T> i_vertex_left,
			CVector<3, T> i_vertex_right,
			CVector<3, T> i_vertex_top
	){
		// edge tangent vectors
		CVector<3, T> edge_right = i_vertex_right - i_vertex_top;
		CVector<3, T> edge_left = i_vertex_left - i_vertex_top;

		// left edge normal vector
		CVector<3, T> normal_left = ( edge_left % edge_right ) % edge_left;
		CVector<3, T> normal_top = ( edge_left % edge_right );

		// normalize vectors
		normal_left.normalize();
		edge_left.normalize();
		normal_top.normalize();
//		std::cout << normal_left.getLength() << ", " << edge_left.getLength() << ", " << i_vertex_top.getLength() << std::endl;
//		CVector<3, T> edge_left_normalized = edge_left.normal();

		// 2D to 3D projection matrix
		CMatrix3<T> M = CMatrix3<T>(
				edge_left[0], normal_left[0], normal_top[0],// i_vertex_top[0],
				edge_left[1], normal_left[1], normal_top[1],// i_vertex_top[1],
				edge_left[2], normal_left[2], normal_top[2]// i_vertex_top[2]
				);

		// inverse projection matrix: 3D to 2D
		CMatrix3<T> M_inv = M.getInverse();

		// store matrix
		o_projectionMatrix[0][0] = M_inv[0][0];
		o_projectionMatrix[0][1] = M_inv[0][1];
		o_projectionMatrix[0][2] = M_inv[0][2];
		o_projectionMatrix[1][0] = M_inv[1][0];
		o_projectionMatrix[1][1] = M_inv[1][1];
		o_projectionMatrix[1][2] = M_inv[1][2];
		o_projectionMatrix[2][0] = M_inv[2][0];
		o_projectionMatrix[2][1] = M_inv[2][1];
		o_projectionMatrix[2][2] = M_inv[2][2];
	}



	/*
	 * left edge is located on the x-axis
	 * therefore: turn by 90° to edge space
	 *
	 */
	template <typename T>
	inline static void setLeftEdgeRotationMatrix(
			T o_rotationMatrix[2][2]
	){
		// set rotation matrix
		o_rotationMatrix[0][0] = (T)0;
		o_rotationMatrix[0][1] = (T)-1;
		o_rotationMatrix[1][0] = (T)1;
		o_rotationMatrix[1][1] = (T)0;
	}


	/**
	 * right edge is located per construction over (or on) the x-axis
	 * therefore: turn by -alpha to left edge and another -90° to edge space
	 *
	 *
	 *          /\_
	 *         /   \_
	 *        /      \_
	 * right /         \_hyp
	 *      /-.         .\_
	 *     /_a_ì_______í_b_\
	 *          left
	 *
	 *  a = alpha
	 *  b = beta
	 */
	template <typename T>
	inline static void setRightEdgeRotationMatrix(
			T o_rotationMatrix[2][2],

			T i_2DProjectionMatrix[3][3],

//			T i_vertex_left_x, T i_vertex_left_y, T i_vertex_left_z,
//			T i_vertex_right_x, T i_vertex_right_y, T i_vertex_right_z,
//			T i_vertex_top_x, T i_vertex_top_y, T i_vertex_top_z,
			CVector<3, T> i_vertex_left,
			CVector<3, T> i_vertex_right,
			CVector<3, T> i_vertex_top

//			T i_hypEdgeLength, 		// length of hyp edge
//			T i_rightEdgeLength,	// length of right edge
//			T i_leftEdgeLength 		// length of left edge

	){

#if true
		// edge tangent vector in 3D
		CVector<3, T> edge_right = i_vertex_right - i_vertex_top;
//		CVector<3, T> edge_hyp = i_vertex_right - i_vertex_left;
//		CVector<3, T> edge_left = i_vertex_left - i_vertex_top;

		CMatrix3<T> M_inv = CMatrix3<T>(
				i_2DProjectionMatrix[0][0], i_2DProjectionMatrix[0][1], i_2DProjectionMatrix[0][2],
				i_2DProjectionMatrix[1][0], i_2DProjectionMatrix[1][1], i_2DProjectionMatrix[1][2],
				i_2DProjectionMatrix[2][0], i_2DProjectionMatrix[2][1], i_2DProjectionMatrix[2][2]);

//		CVector<3, T> flat_right = M_inv * i_vertex_right;
//		CVector<3, T> flat_top = M_inv * i_vertex_top;
//		CVector<3, T> flat_left = M_inv * i_vertex_left;

		// edge tangent vector on flat triangle (quasi-2D)
		CVector<3, T> flat_edge_right = M_inv * edge_right;
//		CVector<3, T> flat_edge_left = M_inv * edge_left;
//		CVector<3, T> flat_edge_hyp = M_inv * edge_hyp;

		// normalize edge vector
		CVector<3, T> flat_edge_right_normalized = flat_edge_right.normal();
//		CVector<3, T> flat_edge_right_normalized = flat_edge_right
//				/ std::sqrt(flat_edge_right[0] * flat_edge_right[0] + flat_edge_right[1] * flat_edge_right[1]);

		// set rotation matrix using tangent/normal vector
		o_rotationMatrix[0][0] = -flat_edge_right_normalized[1];
		o_rotationMatrix[0][1] = flat_edge_right_normalized[0];
		o_rotationMatrix[1][0] = -flat_edge_right_normalized[0];
		o_rotationMatrix[1][1] = -flat_edge_right_normalized[1];

//		CMatrix2<T> rotationMatrix = CMatrix2<T>(
//				-flat_edge_right_normalized[1], flat_edge_right_normalized[0],
//				-flat_edge_right_normalized[0], -flat_edge_right_normalized[1]);
//
//		// store rotation matrix
//		o_rotationMatrix[0][0] = rotationMatrix[0][0];
//		o_rotationMatrix[0][1] = rotationMatrix[0][1];
//		o_rotationMatrix[1][0] = rotationMatrix[1][0];
//		o_rotationMatrix[1][1] = rotationMatrix[1][1];

//		std::cout << "test." << std::endl;
////		std::cout << "normal_left = " << std::endl
////				<< "  ( edge_left x edge_right ) x edge_left = " << std::endl
////				<< "  ( " << edge_left << " x " << edge_right << " ) x " << edge_left << " = " << std::endl
////				<< "  " << normal_left << std::endl << std::endl;
//		std::cout << "|r|:" << edge_right.getLength() << " ;  " << flat_edge_right.getLength() << std::endl;
////		std::cout << "lengths: r " << i_rightEdgeLength << "  l " << i_leftEdgeLength << "  h " << i_hypEdgeLength << std::endl;
//		std::cout << "lengths: r " << flat_edge_right.getLength() << "  l " << flat_edge_left.getLength() << "  h " << flat_edge_hyp.getLength() << std::endl;
////		std::cout << "M: " << std::endl << M << std::endl;
////		std::cout << "right: " << right << std::endl << std::endl;
//		std::cout << "M_inv: " << std::endl << M_inv << std::endl;
////		std::cout << "flat_top: " << flat_top << std::endl;
//		std::cout << "i_right " << i_vertex_right << std::endl;
//		std::cout << "flat_right: " << flat_right << std::endl;
//		std::cout << "edge_right: " << flat_edge_right << std::endl << std::endl;

#else
		// compute alpha using law of cosine
		T alpha = (T)acos(
				(i_rightEdgeLength * i_rightEdgeLength + i_leftEdgeLength * i_leftEdgeLength - i_hypEdgeLength * i_hypEdgeLength)
				/ (T)2.0 / i_rightEdgeLength / i_leftEdgeLength
		);
		T angleToY = -(T)M_PI/2.0 - alpha;
		std::cout << "alpha: " << alpha * (T)180.0 / M_PI << std::endl;

		// set rotation matrix
		o_rotationMatrix[0][0] = (T)cos(angleToY);
		o_rotationMatrix[0][1] = (T)-sin(angleToY);
		o_rotationMatrix[1][0] = (T)sin(angleToY);
		o_rotationMatrix[1][1] = (T)cos(angleToY);
#endif
//
//		CMatrix2<T> rotmat = CMatrix2<T>(o_rotationMatrix[0][0], o_rotationMatrix[0][1],
//				o_rotationMatrix[1][0], o_rotationMatrix[1][1]);
//
//		std::cout << "rotmat" << std::endl << rotmat << std::endl;
//		std::cout << "newrot" << std::endl << newrot << std::endl;
//
//		T alpha_f = (T)acos(
//				(i_leftEdgeLength * i_leftEdgeLength + i_hypEdgeLength * i_hypEdgeLength - i_rightEdgeLength * i_rightEdgeLength)
//				/ ( (T)2.0 * i_hypEdgeLength * i_leftEdgeLength )
//				);
//
//		T h_left = i_hypEdgeLength * sin(alpha_f);
//
//		T vertex1_x = (T)0.0;
//		T vertex1_y = (T)0.0;
//		T vertex2_x = i_leftEdgeLength;
//		T vertex2_y = (T)0.0;
//		T vertex3_x = i_leftEdgeLength - std::sqrt(i_hypEdgeLength * i_hypEdgeLength - h_left * h_left);
//		T vertex3_y = h_left;
//		std::cout << "V  " << vertex1_x << ", " << vertex1_y << ";  " << vertex2_x << ", " << vertex2_y << ";  " << vertex3_x << ", " << vertex3_y << std::endl;
//		std::cout << "V  " << flat_top[0] << ", " << flat_top[1] << ";  "
//				<< flat_left[0] << ", " << flat_left[1] << ";  "
//				<< flat_right[0] << ", " << flat_right[1] << std::endl;
	}


	/**
	 * hyp edge is located per construction over (or on) the x-axis
	 * therefore: turn by beta to left edge and another -90° back to edge space
	 *
	 *
	 *
	 *          /\_
	 *         /   \_
	 *        /      \_
	 * right /         \_hyp
	 *      /-.         .\_
	 *     /_a_ì_______í_b_\
	 *          left
	 *
	 *  a = alpha
	 *  b = beta
	 */
	template <typename T>
	inline static void setHypEdgeRotationMatrix(
			T o_rotationMatrix [2][2],

			T i_2DProjectionMatrix[3][3],

			CVector<3, T> i_vertex_left,
			CVector<3, T> i_vertex_right,
			CVector<3, T> i_vertex_top
//			T i_hypEdgeLength, 		// length of hyp edge
//			T i_rightEdgeLength,	// length of right edge
//			T i_leftEdgeLength 		// length of left edge
	){
#if true
		// edge tangent vector in 3D
		CVector<3, T> edge_hyp = i_vertex_left - i_vertex_right;

		CMatrix3<T> M_inv = CMatrix3<T>(
				i_2DProjectionMatrix[0][0], i_2DProjectionMatrix[0][1], i_2DProjectionMatrix[0][2],
				i_2DProjectionMatrix[1][0], i_2DProjectionMatrix[1][1], i_2DProjectionMatrix[1][2],
				i_2DProjectionMatrix[2][0], i_2DProjectionMatrix[2][1], i_2DProjectionMatrix[2][2]);

		// edge tangent vector on flat triangle (quasi-2D)
//		edge_hyp = edge_hyp * (T)10;
//		edge_hyp = edge_hyp - i_vertex_right;
		CVector<3, T> flat_edge_hyp = M_inv * edge_hyp;
//		std::cout << i_vertex_left << std::endl;
//		std::cout << i_vertex_right << std::endl;
//		std::cout << edge_hyp << std::endl;
//		std::cout << flat_edge_hyp << std::endl << std::endl;

		// normalize edge vector
		CVector<3, T> flat_edge_hyp_normalized = flat_edge_hyp.normal();
//		CVector<3, T> flat_edge_hyp_normalized = flat_edge_hyp
//				/ std::sqrt(flat_edge_hyp[0] * flat_edge_hyp[0] + flat_edge_hyp[1] * flat_edge_hyp[1]);

		// set rotation matrix using tangent/normal vector
		o_rotationMatrix[0][0] = -flat_edge_hyp_normalized[1];
		o_rotationMatrix[0][1] = flat_edge_hyp_normalized[0];
		o_rotationMatrix[1][0] = -flat_edge_hyp_normalized[0];
		o_rotationMatrix[1][1] = -flat_edge_hyp_normalized[1];

#else
		// compute beta using law of cosine
		T beta = (T)acos(
				(i_leftEdgeLength * i_leftEdgeLength + i_hypEdgeLength * i_hypEdgeLength - i_rightEdgeLength * i_rightEdgeLength)
				/ (T)2.0 / i_leftEdgeLength / i_hypEdgeLength);
		T angleToY = -(T)M_PI/2.0 + beta;
//		std::cout << "A  " << (T)cos(angleToY) << ", " << (T)-sin(angleToY) << ";  " << (T)sin(angleToY) << ", " << (T)cos(angleToY) << std::endl << std::endl;

		// set rotation matrix
		o_rotationMatrix[0][0] = (T)cos(angleToY);
		o_rotationMatrix[0][1] = (T)-sin(angleToY);
		o_rotationMatrix[1][0] = (T)sin(angleToY);
		o_rotationMatrix[1][1] = (T)cos(angleToY);
#endif
	}

	/**
	 * This method sets the transformation matrices for scaling and shearing of the distorted triangle
	 * to the reference triangle and vice versa. The shearing is dependent on the angle alpha between
	 * left and right cathetus which is computed from the given edge lengths using the law of cosines.
	 *
	 *          /\_
	 *         /   \_
	 *        /      \_
	 * right /         \_hyp
	 *      /-.          \_
	 *     /_a_ì___________\
	 *          left
	 *
	 *  a = alpha
	 */
	template <typename T>
	inline static void setTransformationMatrices(
			T o_transvectionMatrix [2][2],			// matrix for scaling and shearing to reference space
			T o_inverseTransvectionMatrix [2][2],	// inverse matrix for scaling and shearing from reference space
//			T i_hypEdgeLength, 	// length of hyp edge
//			T i_rightEdgeLength,	// length of right edge
//			T i_leftEdgeLength, 	// length of left edge
			T i_2DProjectionMatrix[3][3],
			CVector<3, T> i_vertex_left,
			CVector<3, T> i_vertex_right,
			CVector<3, T> i_vertex_top,
			T i_radius		// real radius of sphere (e.g. earth)
	){
#if true
//		T alpha_f = (T)acos(
//				(i_leftEdgeLength * i_leftEdgeLength + i_hypEdgeLength * i_hypEdgeLength - i_rightEdgeLength * i_rightEdgeLength)
//				/ ( (T)2.0 * i_hypEdgeLength * i_leftEdgeLength )
//				);
//
//		T h_left = i_hypEdgeLength * sin(alpha_f);
//
//		T vertex1_x = (T)0.0;
//		T vertex1_y = (T)0.0;
//		T vertex2_x = i_leftEdgeLength;
//		T vertex2_y = (T)0.0;
//		T vertex3_x = i_leftEdgeLength - std::sqrt(i_hypEdgeLength * i_hypEdgeLength - h_left * h_left); // sqrt(6 * 6 - 2.6663 * 2.6663) = 5.3750
//		T vertex3_y = h_left;
//
//		o_inverseTransvectionMatrix[0][0] = vertex2_x - vertex1_x;
//		o_inverseTransvectionMatrix[0][1] = vertex2_y - vertex1_y;
//		o_inverseTransvectionMatrix[1][0] = vertex3_x - vertex1_x;
//		o_inverseTransvectionMatrix[1][1] = vertex3_y - vertex1_y;
//
//		T area = ( vertex2_x * ( vertex3_y - vertex1_y ) ) * (T)0.5;
//		T area_inv = (T)1.0 / ((T)2.0 * area);
//
//		o_transvectionMatrix[0][0] = area_inv * ( vertex3_y - vertex1_y );
//		o_transvectionMatrix[0][1] = area_inv * ( vertex1_y - vertex2_y );
//		o_transvectionMatrix[1][0] = area_inv * ( vertex1_x - vertex3_x );
//		o_transvectionMatrix[1][1] = area_inv * ( vertex2_x - vertex1_x );

		// edge tangent vector in 3D
		CVector<3, T> edge_right = (i_vertex_right - i_vertex_top) * i_radius;
		CVector<3, T> edge_left = (i_vertex_left - i_vertex_top) * i_radius;

		CMatrix3<T> M_inv = CMatrix3<T>(
				i_2DProjectionMatrix[0][0], i_2DProjectionMatrix[0][1], i_2DProjectionMatrix[0][2],
				i_2DProjectionMatrix[1][0], i_2DProjectionMatrix[1][1], i_2DProjectionMatrix[1][2],
				i_2DProjectionMatrix[2][0], i_2DProjectionMatrix[2][1], i_2DProjectionMatrix[2][2]);

		// edge tangent vector on flat triangle (quasi-2D)
		CVector<3, T> flat_edge_right = M_inv * edge_right;
		CVector<3, T> flat_edge_left = M_inv * edge_left;

		// normalize edge vector
//		flat_edge_right.normalize();
//		flat_edge_left.normalize();

		o_inverseTransvectionMatrix[0][0] = flat_edge_left[0];
		o_inverseTransvectionMatrix[0][1] = flat_edge_right[0];
		o_inverseTransvectionMatrix[1][0] = flat_edge_left[1];
		o_inverseTransvectionMatrix[1][1] = flat_edge_right[1];

		T det = flat_edge_left[0] * flat_edge_right[1] - flat_edge_right[0] * flat_edge_left[1];
		T det_inv = (T)1.0 / det;

		o_transvectionMatrix[0][0] = det_inv * ( flat_edge_right[1] );
		o_transvectionMatrix[0][1] = det_inv * ( -flat_edge_right[0] );
		o_transvectionMatrix[1][0] = det_inv * ( -flat_edge_left[1] );
		o_transvectionMatrix[1][1] = det_inv * ( flat_edge_left[0] );


//		T alpha = (T)acos(
//				(i_rightEdgeLength * i_rightEdgeLength + i_leftEdgeLength * i_leftEdgeLength - i_hypEdgeLength * i_hypEdgeLength)
//				/ (T)2.0 / i_rightEdgeLength / i_leftEdgeLength
//		);
//		o_transvectionMatrix[0][0] = (T)1.0 / ( vertex2_x - vertex1_x );
//		o_transvectionMatrix[0][1] = (T)0.0;
//		o_transvectionMatrix[1][0] = - ( vertex3_x - vertex1_x ) / ( ( vertex2_x - vertex1_x ) * ( vertex3_y - vertex1_y ) );
//		o_transvectionMatrix[1][1] = (T)1.0 / ( vertex3_y - vertex1_y );
//
//		std::cout << o_transvectionMatrix[0][0] << ", " << (T)1.0 / i_leftEdgeLength << std::endl;
//		std::cout << o_transvectionMatrix[0][1] << ", " << (T)-cos(alpha) / sin(alpha) / i_leftEdgeLength << std::endl;
//		std::cout << o_transvectionMatrix[1][0] << ", " << (T)0.0 << std::endl;
//		std::cout << o_transvectionMatrix[1][1] << ", " << (T)1.0 / sin(alpha) / i_rightEdgeLength << std::endl << std::endl;
//
//		std::cout << o_inverseTransvectionMatrix[0][0] << ", " << (T)i_leftEdgeLength << std::endl;
//		std::cout << o_inverseTransvectionMatrix[0][1] << ", " << (T)cos(alpha) * i_rightEdgeLength << std::endl;
//		std::cout << o_inverseTransvectionMatrix[1][0] << ", " << (T)0.0 << std::endl;
//		std::cout << o_inverseTransvectionMatrix[1][1] << ", " << (T)sin(alpha) * i_rightEdgeLength << std::endl << std::endl;
#else
		// compute alpha using law of cosines
		T alpha = (T)acos(
				(i_rightEdgeLength * i_rightEdgeLength + i_leftEdgeLength * i_leftEdgeLength - i_hypEdgeLength * i_hypEdgeLength)
				/ (T)2.0 / i_rightEdgeLength / i_leftEdgeLength
		);

		// set matrix for shearing and scaling of the distorted triangle to reference triangle
		o_transvectionMatrix[0][0] = (T)1.0 / i_leftEdgeLength;
		o_transvectionMatrix[0][1] = (T)-cos(alpha) / sin(alpha) / i_leftEdgeLength;
		o_transvectionMatrix[1][0] = (T)0.0;
		o_transvectionMatrix[1][1] = (T)1.0 / sin(alpha) / i_rightEdgeLength;

		// set inverse matrix for back transformation of the reference triangle to the distorted one
		o_inverseTransvectionMatrix[0][0] = (T)i_leftEdgeLength;
		o_inverseTransvectionMatrix[0][1] = (T)cos(alpha) * i_rightEdgeLength;
		o_inverseTransvectionMatrix[1][0] = (T)0.0;
		o_inverseTransvectionMatrix[1][1] = (T)sin(alpha) * i_rightEdgeLength;
#endif
	}


	/**
	 * This method computes the spherical distance of to spherical points,
	 * given by the face based cube coordinates (2D). It uses the dot product,
	 * in order to compute the angle between the position vectors.
	 *
	 * Notice: This methods is only applicable to two points on the same
	 * corresponding cube face. For other scenarios than computing triangle
	 * edge length (which are always on the same face), this method is not
	 * suitable.
	 */
	template <typename T>
	inline static T getEdgeLengthOnSphere(
//			T i_vertex1_x, 	// face based x-coordinate of vertex 1
//			T i_vertex1_y, 	// face based y-coordinate of vertex 1
//			T i_vertex2_x,	// face based x-coordinate of vertex 2
//			T i_vertex2_y,	// face based y-coordinate of vertex 2
			T i_vertex1_x, T i_vertex1_y, T i_vertex1_z,
			T i_vertex2_x, T i_vertex2_y, T i_vertex2_z,
			T i_radius		// real radius of sphere (e.g. earth)
	)
	{
		T edge_length = std::sqrt( (i_vertex2_x - i_vertex1_x) * (i_vertex2_x - i_vertex1_x)
				+ (i_vertex2_y - i_vertex1_y) * (i_vertex2_y - i_vertex1_y)
				+ (i_vertex2_z - i_vertex1_z) * (i_vertex2_z - i_vertex1_z) );
		return edge_length * i_radius;
	}


	/**
	 * This method computes the area of a triangle on the sphere using
	 * the spherical excess of angular sum of spherical triangles and
	 * the law of cosines for spherical triangles.
	 *
	 *
	 * ref: A. Filler, Euklidische und nichteuklidische Geometrie (1993), p. 18ff.
	 * http://www.mathematik.hu-berlin.de/~filler/publikat/filler_eukl-ne-geom.pdf
	 */
	template <typename T>
	inline static T getCellAreaOnSphere(
			T i_hypEdgeLength, 		// length of the hyp edge
			T i_rightEdgeLength,	// length of the right edge
			T i_leftEdgeLength		// length of the left edge
	){
		T s = (i_hypEdgeLength + i_rightEdgeLength + i_leftEdgeLength) / 2.0;
		T area = std::sqrt( s * (s - i_hypEdgeLength) * (s - i_rightEdgeLength) * (s - i_leftEdgeLength) );
		return area;
	}
	template <typename T>
	inline static T getCellAreaOnSphere(
//			T i_hypEdgeLength, 		// length of the hyp edge
//			T i_rightEdgeLength,	// length of the right edge
//			T i_leftEdgeLength,		// length of the left edge
//			T radius,				// real radius of sphere (e.g. earth)
			T i_inverseTransvectionMatrix[2][2]
	){
//		T s = (i_hypEdgeLength + i_rightEdgeLength + i_leftEdgeLength) / 2.0;
//		T area = std::sqrt( s * (s - i_hypEdgeLength) * (s - i_rightEdgeLength) * (s - i_leftEdgeLength) );
		T area = (T)0.5 * i_inverseTransvectionMatrix[0][0] * i_inverseTransvectionMatrix[1][1] - i_inverseTransvectionMatrix[0][1] * i_inverseTransvectionMatrix[1][0];
		return area;
//#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
//		return CTriangle_VectorProjections_Equiangular::getCellAreaOnSphere(i_hypEdgeLength, i_rightEdgeLength, i_leftEdgeLength, radius);
//#else
//		// normalizing edge lengths to unit sphere
//		T h = i_hypEdgeLength/radius;
//		T r = i_rightEdgeLength/radius;
//		T l = i_leftEdgeLength/radius;
//
//		// helper functions
//		T coshyp = cos(h);
//		T cosr = cos(r);
//		T cosl = cos(l);
//		T sinhyp = sin(h);
//		T sinr = sin(r);
//		T sinl = sin(l);
//
//		// law of cosines for spherical triangles, transformed to alpha, beta and gamma
//		T alpha = acos((coshyp-cosr*cosl)/(sinr*sinl));
//		T beta = acos((cosr-coshyp*cosl)/(sinhyp*sinl));
//		T gamma = acos((cosl-cosr*coshyp)/(sinr*sinhyp));
//
//		// spherical excess
//		return (T)(alpha + beta + gamma - M_PI) * radius * radius;
//#endif
	}
#endif
};


#endif /* CFLUXPROJECTIONS_HPP_ */
