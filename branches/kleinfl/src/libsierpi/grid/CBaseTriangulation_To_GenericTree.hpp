/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CBaseTriangulation_To_GenericTree.hpp
 *
 *  Created on: Oct 12, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CBASETRIANGULATION_TO_GENERICTREE_HPP_
#define CBASETRIANGULATION_TO_GENERICTREE_HPP_

#include "CDomain_BaseTriangulation.hpp"
#include "../cluster/CDomainClusters.hpp"
#include "global_config.h"


namespace sierpi
{

/**
 * \brief converts a base triangulation to a generic tree structure
 *
 * base triangulation is given via CDomain_BaseTriangulation.
 */
template <typename CSimulation_ClusterHandler>
class CBaseTriangulation_To_GenericTree
{
	typedef CDomain_BaseTriangle<CSimulation_ClusterHandler> CDomain_BaseTriangle_;
	typedef CDomain_BaseTriangulation<CSimulation_ClusterHandler> CDomain_BaseTriangulation_;

	typedef CCluster_TreeNode<CSimulation_ClusterHandler> CCluster_TreeNode_;
	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CEdgeComm_InformationAdjacentCluster_;
	typedef CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;


private:
	/**
	 * maximum allowed unique id depth for root triangles
	 */
	int base_triangulation_max_unique_id_depth;

	/**
	 * unique id counter for initialization
	 */
	int unique_id_counter;

	/**
	 * edge comm elements along a single cathetus
	 */
	int cat_edge_comm_elements;

	/**
	 * edge comm elements along a hypotenuse
	 */
	int hyp_edge_comm_elements;

	/**
	 * initial depth and thus number of triangles to setup stacks
	 */
	int initial_refinement_depth;

	/**
	 * minimum relative refinement depth
	 */
	int min_relative_depth;

	/**
	 * maximum relative refinement depth
	 */
	int max_relative_depth;

	/**
	 * initial splits of cluster
	 */
	int initial_cluster_splits;

	/**
	 * pointer to domain root triangulation
	 */
	CDomain_BaseTriangulation_ *cDomain_BaseTriangulation;


	/**
	 * mpi rank
	 */
	int mpi_rank;

public:
	/**
	 * number of base triangles
	 */
	unsigned long long number_of_initial_local_base_clusters;
	unsigned long long number_of_initial_global_base_clusters;

	/**
	 * constructor
	 */
	CBaseTriangulation_To_GenericTree()	:
		cat_edge_comm_elements(-1),
		hyp_edge_comm_elements(-1)
	{
	}


private:
	void _setupLeafCluster(
		CDomain_BaseTriangle_ &cDomain_BaseTriangle,
		CGeneric_TreeNode_ *i_ptr_parent_cGeneric_TreeNode,
		CGeneric_TreeNode_ *o_cGeneric_TreeNode
	)
	{
		/*
		 * setup unique id and increment unique id counter
		 */
		cDomain_BaseTriangle.cCluster_UniqueId.setup(unique_id_counter + (1 << base_triangulation_max_unique_id_depth), base_triangulation_max_unique_id_depth);

#if CONFIG_ENABLE_MPI
		if (mpi_rank == cDomain_BaseTriangle.mpi_rank)
		{
#endif

			// then allocate the cluster tree node
			CCluster_TreeNode_ *cClusterTreeNode = new CCluster_TreeNode_(
					cDomain_BaseTriangle.cTriangleFactory	// triangle factory
				);

			// setup pointers
			o_cGeneric_TreeNode->cCluster_TreeNode = cClusterTreeNode;
			cClusterTreeNode->cGeneric_TreeNode = o_cGeneric_TreeNode;

			/*
			 * update cluster node's adaptive depth values!
			 */
			assert(cClusterTreeNode->cTriangleFactory.recursionDepthFirstRecMethod == 0);
			cClusterTreeNode->cTriangleFactory.minDepth = min_relative_depth;
			cClusterTreeNode->cTriangleFactory.maxDepth = max_relative_depth;

			/*
			 * setup unique id and increment unique id counter
			 */
			cClusterTreeNode->cCluster_UniqueId = cDomain_BaseTriangle.cCluster_UniqueId;

			// setup pointer to allocated cluster tree node
			cDomain_BaseTriangle.cCluster_user_ptr = cClusterTreeNode;

			// setup genericTreeNode's variable to the parent node
			o_cGeneric_TreeNode->parent_node = i_ptr_parent_cGeneric_TreeNode;

			/*
			 * convert edges of type OLD to type NEW due to parallelization
			 */
			if (cClusterTreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cClusterTreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

			if (cClusterTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD)
				cClusterTreeNode->cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

			number_of_initial_local_base_clusters++;

#if CONFIG_ENABLE_MPI
		}
#endif
	}

private:
	void _setup_GenericTreeParentAndClusters(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGeneric_TreeNode_ *io_parent_GenericTreeNode,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		assert(current_depth <= base_triangulation_max_unique_id_depth);

		// left traversal
		io_parent_GenericTreeNode->first_child_node =
			_setup_GenericTreeChildAndClusters(
				iter,		// reference to iterator
				io_parent_GenericTreeNode,	// this (parent) node
				current_depth
			);

		if (iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end())
		{
			// right traversal
			io_parent_GenericTreeNode->second_child_node =
				_setup_GenericTreeChildAndClusters(
					iter,		// reference to iterator
					io_parent_GenericTreeNode,	// this (parent) node
					current_depth
				);
		}
	}

private:
	/**
	 * recursive method to insert the new generic tree-node given by `iter` below the given parent_node
	 */
	CGeneric_TreeNode_ *_setup_GenericTreeChildAndClusters(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGeneric_TreeNode_ *i_ptr_parent_GenericTreeNode,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;
		assert(current_depth <= base_triangulation_max_unique_id_depth);

		// leaf reached => create cluster
		if (current_depth == base_triangulation_max_unique_id_depth)
		{
			/****************************************************************
			 * allocate new cluster node
			 ****************************************************************/

			// first allocate the generic tree node
			CGeneric_TreeNode_ *cGeneric_TreeNode = new CGeneric_TreeNode_(i_ptr_parent_GenericTreeNode, current_depth <= base_triangulation_max_unique_id_depth-initial_cluster_splits);

			_setupLeafCluster(cDomain_BaseTriangle, i_ptr_parent_GenericTreeNode, cGeneric_TreeNode);

			unique_id_counter++;

			// increment the iterator
			iter++;

			// return pointer to this node
			return cGeneric_TreeNode;
		}


		// create a new tree node with attached leaves
		/****************************************************************
		 * allocate new generic tree node
		 ****************************************************************/
		CGeneric_TreeNode_ *new_node = new CGeneric_TreeNode_(i_ptr_parent_GenericTreeNode, current_depth <= base_triangulation_max_unique_id_depth-initial_cluster_splits);

		current_depth++;
		_setup_GenericTreeParentAndClusters(iter, new_node, current_depth);

		return new_node;
	}


	/**
	 * setup a root sub-cluster tree node for a given cluster
	 *
	 * the setup includes the information about the RLE encoded communication between the clusters
	 */
private:
	void _setup_ClusterEdgeCommunicationInformation(
			CCluster_TreeNode_ *io_cCluster_TreeNode,
			CDomain_BaseTriangle_ &i_cDomain_BaseTriangle
	)
	{
		/*
		 * setup hypotenuse adjacency information stack
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge != nullptr)
		{
			io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ &adjacentTriangleInformation = io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation.cCluster_TreeNode = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.cCluster_TreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.cCluster_UniqueId = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cCluster_UniqueId;
			adjacentTriangleInformation.edge_comm_elements = hyp_edge_comm_elements;
		}
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		else
		{
			io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ &adjacentTriangleInformation = io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back();

			adjacentTriangleInformation.cCluster_TreeNode = nullptr;
			adjacentTriangleInformation.edge_comm_elements = 0;
			adjacentTriangleInformation.cCluster_UniqueId.raw_unique_id = 0;
		}
#endif


		/*
		 * setup catheti adjacency information stack for left edge
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge != nullptr)
		{
			io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ &adjacentTriangleInformation = io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation.cCluster_TreeNode = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.cCluster_TreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.cCluster_UniqueId = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cCluster_UniqueId;
			adjacentTriangleInformation.edge_comm_elements = cat_edge_comm_elements;
		}
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		else
		{
			io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
			CEdgeComm_InformationAdjacentCluster_ * adjacentTriangleInformation = &io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back();

			adjacentTriangleInformation->cCluster_TreeNode = nullptr;
			adjacentTriangleInformation->edge_comm_elements = 0;
			adjacentTriangleInformation->cCluster_UniqueId.raw_unique_id = 0;
		}
#endif


		/*
		 * setup catheti adjacency information stack for right edge
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge)
		{
			CEdgeComm_InformationAdjacentCluster_ *adjacentTriangleInformation;

			/**
			 * for odd traversal type, the adjacent cluster comm information has to be inserted in reversed order
			 */
			if (io_cCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
			{
				io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.insert(
						io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin(),
						CEdgeComm_InformationAdjacentCluster_()
					);
				adjacentTriangleInformation = &(io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.front());
			}
			else
			{
				io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
						CEdgeComm_InformationAdjacentCluster_()
					);
				adjacentTriangleInformation = &(io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back());
			}

			if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cCluster_user_ptr)
			{
				adjacentTriangleInformation->cCluster_TreeNode = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cCluster_user_ptr;
			}
			else
			{
				adjacentTriangleInformation->cCluster_TreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation->mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation->cCluster_UniqueId = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cCluster_UniqueId;
			adjacentTriangleInformation->edge_comm_elements = cat_edge_comm_elements;
		}
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		else
		{
			CEdgeComm_InformationAdjacentCluster_ *adjacentTriangleInformation;
			if (io_cCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
			{
				io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(CEdgeComm_InformationAdjacentCluster_());
				adjacentTriangleInformation = &io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back();
			}
			else
			{
				io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.insert(
						io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin(),
						CEdgeComm_InformationAdjacentCluster_()
					);
				adjacentTriangleInformation = &(io_cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.front());
			}

			adjacentTriangleInformation->cCluster_TreeNode = nullptr;
			adjacentTriangleInformation->edge_comm_elements = 0;
			adjacentTriangleInformation->cCluster_UniqueId.raw_unique_id = 0;
		}
#endif
	}



#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	/**
	 * search for the adjacent matching vertices and store them
	 *
	 * the search is based on edge comm information!!!
	 */
	bool _setupVertexCommInformation(
			std::list<CEdgeComm_InformationAdjacentCluster_> &io_vertexInformationAdjacentClusters,	///< list to fill with information on adjacent clusters
			CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_baseTriangle,						///< base triangulation
			const CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_originBaseTriangle,			///< origin base triangle of this vertex search
			const CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_prevBaseTriangle,				///< base triangle triggering the execution of this function to find the matching edge and to avoid running backwards

			bool i_adjacentEdgeInClockwiseOrder						///< select vertex by taking the 1st vertex in clock order from the adjacent triangles point of view
	)
	{
		if (i_baseTriangle == nullptr)
			return false;

		assert(io_vertexInformationAdjacentClusters.size() < 8);

		/*
		 * stop setting up the tree when we are back at the base triangle where we started the vertex comm search
		 */
		if (i_baseTriangle == i_originBaseTriangle)
		{
			/*
			 * remove topmost element!
			 */
			io_vertexInformationAdjacentClusters.pop_back();
			return true;
		}

#if DEBUG
		for(auto iter = io_vertexInformationAdjacentClusters.begin(); iter != io_vertexInformationAdjacentClusters.end(); iter++)
			assert((*iter).cCluster_UniqueId != i_baseTriangle->cCluster_user_ptr->cCluster_UniqueId);
#endif

		if (i_prevBaseTriangle != i_originBaseTriangle)
		{
			/*
			 * add vertex comm information
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				io_vertexInformationAdjacentClusters.push_back(
						CEdgeComm_InformationAdjacentCluster_(i_baseTriangle->cCluster_user_ptr, i_baseTriangle->cCluster_user_ptr->cCluster_UniqueId, 0)
					);
			}
			else
			{
				io_vertexInformationAdjacentClusters.push_front(
						CEdgeComm_InformationAdjacentCluster_(i_baseTriangle->cCluster_user_ptr, i_baseTriangle->cCluster_user_ptr->cCluster_UniqueId, 0)
					);
			}
		}


		if (i_baseTriangle->adjacent_triangles.hyp_edge == i_prevBaseTriangle)
		{
			/*
			 * search on hypotenuse edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.right_edge == i_prevBaseTriangle)
		{
			/*
			 * search on right edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.left_edge == i_prevBaseTriangle)
		{
			/*
			 * search on left edge for matching source edge
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentClusters,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		std::cerr << "INVALID STATE REACHED WHEN SEARCHING FOR VERTICES" << std::endl;
		assert(false);

		return false;
	}


	/*
	 * setup a root sub-cluster tree node for a given cluster after edge comm information was set-up !!!
	 *
	 * this setup inserts 0-length encoded edge communication information to account for vertex data
	 */
private:
	void _setup_ClusterVertexCommunicationInformation(
			CDomain_BaseTriangle<CSimulation_ClusterHandler> *i_baseTriangle
	)
	{
		/*
		 * here we start setting up the 0-length encoded vertex data.
		 *
		 * CONVENTION:
		 *
		 * For the vertex shared by the left and hyp edge,
		 * the vertex has to be stored to the hyp RLE data.
		 *
		 * For the vertex shared by the right and hyp edge,
		 * the vertex has to be stored to the cat RLE data.
		 */

		/*
		 * storage containers for left, right and top vertices
		 */
		std::list<CEdgeComm_InformationAdjacentCluster_>
				leftVertexInformationAdjacentClusters,
				rightVertexInformationAdjacentClusters,
				topVertexInformationAdjacentClusters;

		// search on left edge for top vertices
		// RETURN: true, if clockwise search ended up at base triangle
		bool allLeftVerticesFound = _setupVertexCommInformation(
				leftVertexInformationAdjacentClusters,			// OUTPUT: left adjacent vertices
				i_baseTriangle->adjacent_triangles.left_edge,	// BASE TRIANGLE
				i_baseTriangle,									// ORIGIN
				i_baseTriangle,									// PREVIOUSLY VISITED TRIANGLE
				true	// clockwise
			);

		if (!allLeftVerticesFound)
		{
			// search on hyp edge for more left vertices
			_setupVertexCommInformation(
				leftVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// go anti-clockwise
			);
		}

		// search on right edge for top vertices
		bool allTopVerticesFound = _setupVertexCommInformation(
				topVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
			);


		if (!allTopVerticesFound)
		{
			// search on left edge for more top vertices
			_setupVertexCommInformation(
				topVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.left_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}

		// search on hyp edge for right vertices
		bool allRightVerticesFound = _setupVertexCommInformation(
				rightVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
		);

		if (!allRightVerticesFound)
		{
			// search on right edge for more right vertices
			_setupVertexCommInformation(
				rightVertexInformationAdjacentClusters,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}




#if 0
		std::cout << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << " TRIANGLE: " << i_baseTriangle->cCluster_user_ptr->cCluster_UniqueId << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;

		if (leftVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " left vertex adjacent clusters:" << std::endl;

			for (auto iter = leftVertexInformationAdjacentClusters.begin();
					iter != leftVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).cCluster_UniqueId << std::endl;
			}
		}

		if (rightVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " right vertex adjacent clusters:" << std::endl;

			for (auto iter = rightVertexInformationAdjacentClusters.begin();
					iter != rightVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).cCluster_UniqueId << std::endl;
			}
		}

		if (topVertexInformationAdjacentClusters.size() > 0)
		{
			std::cout << " top vertex adjacent clusters:" << std::endl;

			for (auto iter = topVertexInformationAdjacentClusters.begin();
					iter != topVertexInformationAdjacentClusters.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).cCluster_UniqueId << std::endl;
			}
		}
#endif

		/*
		 * insert data into RLE edge comm information
		 */
		CCluster_TreeNode_ *cCluster_TreeNode = i_baseTriangle->cCluster_user_ptr;

//		std::cout << cCluster_TreeNode->cTriangleFactory << std::endl;


		/*
		 * Extend RLE edge comm information for hypotenuse.
		 *
		 * The vertices shared by both catheti and the hypotenuse have to be handled by either the RLE
		 * stored for both catheti or for the hypotenuse.
		 *
		 * We do this since the root sub-cluster triangle type is always set to 'V' to store the vertices
		 * close to the entrance and exit curve on the 'hypotenuse' stack.
		 *
		 * Another reason is also to have a unique storage thus increasing searches for adjacent
		 * communication vertex data.
		 */
		std::vector<CEdgeComm_InformationAdjacentCluster_> &hyp_adjacent_clusters = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters;
		std::vector<CEdgeComm_InformationAdjacentCluster_> &cat_adjacent_clusters = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters;


		/*
		 * TOP VERTEX -> cat middle
		 */
		auto catInsertIter = cat_adjacent_clusters.begin();
		catInsertIter++;

		for (auto iter = topVertexInformationAdjacentClusters.begin();
				iter != topVertexInformationAdjacentClusters.end();
				iter++
		)
		{
			catInsertIter = cat_adjacent_clusters.insert(catInsertIter, *iter);
		}


		if (cCluster_TreeNode->cTriangleFactory.isEven())
		{
			/*
			 * LEFT VERTEX -> front of hyp
			 */
			for (auto iter = leftVertexInformationAdjacentClusters.rbegin();
					iter != leftVertexInformationAdjacentClusters.rend();
					iter++
			)
			{
				// push_front not available
				hyp_adjacent_clusters.insert(hyp_adjacent_clusters.begin(), *iter);
			}

			/*
			 * RIGHT VERTEX -> back of cat
			 */
			for (auto iter = rightVertexInformationAdjacentClusters.rbegin();
					iter != rightVertexInformationAdjacentClusters.rend();
					iter++
			)
			{
				cat_adjacent_clusters.push_back(*iter);
			}
		}
		else
		{
			/*
			 * LEFT VERTEX -> back of cat
			 */
			for (auto iter = leftVertexInformationAdjacentClusters.rbegin();
					iter != leftVertexInformationAdjacentClusters.rend();
					iter++
			)
			{
				cat_adjacent_clusters.push_back(*iter);
			}


			/*
			 * RIGHT VERTEX -> front of hyp
			 */
			for (auto iter = rightVertexInformationAdjacentClusters.rbegin();
					iter != rightVertexInformationAdjacentClusters.rend();
					iter++
			)
			{
				// push_front not available
				hyp_adjacent_clusters.insert(hyp_adjacent_clusters.begin(), *iter);
			}
		}
#if 0
		std::cout << "hyp comm data" << std::endl;
		for (auto i = hyp_adjacent_clusters.begin(); i != hyp_adjacent_clusters.end(); i++)
			std::cout << *i << std::endl;

		std::cout << "cat comm data" << std::endl;
		for (auto i = cat_adjacent_clusters.begin(); i != cat_adjacent_clusters.end(); i++)
			std::cout << *i << std::endl;
#endif

	}
#endif



public:
	bool _dm_cleanupUnusedNodes(CGeneric_TreeNode_ *node)
	{
		if (node->first_child_node)
		{
			if (_dm_cleanupUnusedNodes(node->first_child_node))
			{
				delete node->first_child_node;
				node->first_child_node = nullptr;
			}
		}

		if (node->second_child_node)
		{
			if (_dm_cleanupUnusedNodes(node->second_child_node))
			{
				delete node->second_child_node;
				node->second_child_node = nullptr;
			}
		}

		return (	node->first_child_node == nullptr &&
					node->second_child_node == nullptr &&
					node->cCluster_TreeNode == nullptr
		);
	}


public:
	/**
	 * \brief setup a generic cluster tree given by a base triangulation.
	 *
	 * this also initialized the RLE edge communication information.
	 *
	 * \return return number of local cells
	 */
	unsigned long long setup_GenericTree_And_Clusters_From_BaseTriangulation(
			CDomain_BaseTriangulation_ &io_cDomain_BaseTriangulation,	///< root triangulation

			int i_initial_refinement_depth,				///< initial depth of refinement
			int i_minimum_relative_refinement_depth,	///< minimum relative refinement depth to setup triangle factory
			int i_maximum_relative_refinement_depth,	///< maximum relative refinement depth to setup triangle factory

			int i_mpi_rank,								///< mpi rank for base triangles which should be setup

			int i_initial_cluster_splits,				///< initial cluster split

			CDomainClusters<CSimulation_ClusterHandler> *o_cDomainClusters		///< reference output of domain clusters
	)
	{
		o_cDomainClusters->freeChildren();

		// store mpi rank
		mpi_rank = i_mpi_rank;

		cDomain_BaseTriangulation = &io_cDomain_BaseTriangulation;

		number_of_initial_global_base_clusters = cDomain_BaseTriangulation->cDomainBase_Triangle_List.size();

		number_of_initial_local_base_clusters = 0;

		initial_refinement_depth = i_initial_refinement_depth;
		min_relative_depth = i_minimum_relative_refinement_depth;
		max_relative_depth = i_maximum_relative_refinement_depth;

		initial_cluster_splits = i_initial_cluster_splits;


		/**
		 * check for valid initial depth
		 */
		assert(i_initial_refinement_depth >= 0);
#if 0
		if ((i_initial_refinement_depth & 1) == 1)
		{
			if (!cDomain_BaseTriangulation->oddInitialDepthFixApplied)
			{
				std::cerr << "An odd initial depth value is not allowed due to restrictions of hanging nodes along catheti and hypotenuse" << std::endl;
				assert(false);
			}
		}
#endif

		/**
		 * compute edge communication elements for initial triangulation
		 */
		cat_edge_comm_elements = 1 << (i_initial_refinement_depth/2);
		hyp_edge_comm_elements = 1 << ((i_initial_refinement_depth+1)/2);


		/**
		 * determine max_unique_id_depth for initial base triangulation
		 */
		unsigned long long d = number_of_initial_global_base_clusters-1;
		base_triangulation_max_unique_id_depth = 0;
		for (; d > 0; d >>= 1)
			base_triangulation_max_unique_id_depth++;

		unique_id_counter = 0;

		if (number_of_initial_global_base_clusters == 0)
		{
			/*
			 * nothing to do for empty domain size
			 */
			o_cDomainClusters->setupDummyRoot();

			o_cDomainClusters->setBaseTriangulationDepth(-1);
			return 0;
		}

		{
			auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;
			CCluster_TreeNode_ *cCluster_TreeNode = cDomain_BaseTriangle.cCluster_user_ptr;

			o_cDomainClusters->setBaseTriangulationDepth(base_triangulation_max_unique_id_depth);

			if (number_of_initial_global_base_clusters == 1)
			{
				/*
				 * if there's only one initial root cluster, directly set it up in cCluster
				 */
	#if CONFIG_ENABLE_MPI
				if (cDomain_BaseTriangle.mpi_rank == mpi_rank)
	#endif
				{
					_setupLeafCluster(cDomain_BaseTriangle, nullptr, o_cDomainClusters->getGenericTreeRootNodePtr());
				}

			}
			else
			{
				/*
				 * assemble tree by allocating cGenericTreeNodes and cCluster_TreeNode
				 */
				assert(cCluster_TreeNode == nullptr);

				/*
				 * setup the cluster tree nodes for the root triangulation.
				 *
				 * also setup the simulation cluster handler and create the unique IDs.
				 *
				 * the method _setup_GenericTreeAndClusters recursively sets up the whole tree!
				 */
				_setup_GenericTreeParentAndClusters(iter, o_cDomainClusters, 1);
			}

		}


		/**
		 * setup clusters
		 */
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
		) {
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			CCluster_TreeNode_ *cCluster_TreeNode = cDomain_BaseTriangle.cCluster_user_ptr;

#if CONFIG_ENABLE_MPI
			if (cDomain_BaseTriangle.mpi_rank != mpi_rank)
			{
				assert(cCluster_TreeNode == nullptr);
				continue;
			}
#else
			assert(cCluster_TreeNode != nullptr);
#endif

			_setup_ClusterEdgeCommunicationInformation(
					cCluster_TreeNode,
					cDomain_BaseTriangle
			);

			cCluster_TreeNode->setup_SimulationClusterHandler(nullptr);
		}


#if CONFIG_ENABLE_MPI
		/*
		 * cleanup tree by removing unused nodes with both childs being a nullptr
		 */

		if (_dm_cleanupUnusedNodes(o_cDomainClusters) == true)
		{
			std::cout << "Warning from RANK " << mpi_rank << ": more nodes than initial clusters available!" << std::endl;
		}
#endif

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		/**
		 * extend edge communication information for vertex data comm
		 */
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ *cDomain_BaseTriangle = &(*iter);

			_setup_ClusterVertexCommunicationInformation(cDomain_BaseTriangle);
		}
#endif

		_validate();

		cDomain_BaseTriangulation = nullptr;

		// return number of local cells
		return (unsigned long long)(1 << initial_refinement_depth)*number_of_initial_local_base_clusters;
	}








	/**
	 * start some validation process based on the properties stored in the triangleWithCluster classes
	 *
	 * return true, if the validation failed
	 */
private:
	bool _validate()
	{
		bool valid = true;

#if DEBUG
		for (	auto iter = cDomain_BaseTriangulation->cDomainBase_Triangle_List.begin();
				iter != cDomain_BaseTriangulation->cDomainBase_Triangle_List.end();
				iter++
			)
		{
			CDomain_BaseTriangle_ &t = *iter;

			/**
			 * hypotenuse
			 */
			if (!t.adjacent_triangles.hyp_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle_ &a = *t.adjacent_triangles.hyp_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * left edge
			 */
			if (!t.adjacent_triangles.left_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (LEFT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_ClusterHandler> &a = *t.adjacent_triangles.left_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * right edge
			 */
			if (!t.adjacent_triangles.right_edge)
			{
				if (	t.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (RIGHT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_ClusterHandler> &a = *t.adjacent_triangles.right_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cCluster_user_ptr == t.cCluster_user_ptr)
					{
						if (a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cout << "triangle factory for local triangle: " << std::endl;
					std::cout << t.cTriangleFactory << std::endl;
					std::cout << "triangle factory for adjacent triangle: " << std::endl;
					std::cout << a.cTriangleFactory << std::endl;
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}

			if (!valid)
			{
				std::cout << "TriangleWithCluster id: " << t.cCluster_user_ptr->cCluster_UniqueId << std::endl;
				break;
			}
		}
#endif

		return valid;
	}

};

}

#endif /* CBASETRIANGULATION_TO_GENERICTREE_HPP_ */
