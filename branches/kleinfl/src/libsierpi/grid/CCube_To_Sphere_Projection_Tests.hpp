/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: 12. January 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 */

#ifndef CCUBE_TO_SPHERE_PROJECTIONS_TEST_HPP_
#define CCUBE_TO_SPHERE_PROJECTIONS_TEST_HPP_

#include <cmath>
#include <cstdlib>
#include <cstdio>


#include "CCube_To_Sphere_Projection.hpp"


class CCube_To_Sphere_Projection_Tests
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	static const T error_tolerance = 0.0000000001;
	static const int test_runs = 10000;

	/**
	 *
	 */
	template <typename T>
	static inline T randomNumber(
			T min,
			T max
	) {
		return min + ((T) rand() / RAND_MAX) * (max - min);
	}

public:
	/**
	 * Unit Tests.
	 */
	template <typename T>
	static void testProjections(T dummy) {
		return;
		testBoundaries(dummy);
//		testProjectionLLTo3D(dummy);
//		testProjectionLLTo2D(dummy);
//		testProjection2DTo3D(dummy);
	}

	template <typename T>
	static bool testBoundaries(T dummy) {
		std::cout << "Testing boundaries Lat/Lon to 2D/3D Conversion" << std::endl;
		T i_lat = (T)44.99999;
		T i_lon = (T)90.0001;
		T x, y, z, xx, yy;
		CCube_To_Sphere_Projection::projectLLDegTo3D(i_lat, i_lon, &x, &y, &z);
		CCube_To_Sphere_Projection::projectLLDegTo2D(i_lat, i_lon, &xx, &yy);
		printf("%4.10f, %4.10f -> %10.10f, %10.10f, %10.10f -> %4.10f, %4.10f \n\n",
				i_lat, i_lon, x, y, z, xx, yy);
		return true;
	}


	template <typename T>
	static bool testProjectionLLTo3D(T dummy) {

		std::cout << "Testing Lat/Lon to 3D Conversion" << std::endl;
		bool alert = false;
		for(int i=0; i<test_runs; i++){
			T i_lat = randomNumber((T) -90.0, (T) 90.0);
			T i_lon = randomNumber((T) -180.0, (T) 180.0);
			T x, y, z, o_lat, o_lon;
			CCube_To_Sphere_Projection::projectLLDegTo3D(i_lat, i_lon, &x, &y, &z);
			CCube_To_Sphere_Projection::project3DToLL(x, y, z, &o_lat, &o_lon);

			T error_lat = (i_lat - o_lat) / (T) 180.0;
			T error_lon = (i_lon - o_lon) / (T) 360.0;

			if (error_lat > error_tolerance || error_lon > error_tolerance) {
				printf("%4.4f, %4.4f -> %10.4f, %10.4f, %10.4f -> %4.4f, %4.4f \n   Error: %2.15f, %2.15f \n",
						i_lat, i_lon, x, y, z, o_lat, o_lon, error_lat, error_lon);
				alert = true;
			}
		}
		if (alert)
			std::cout << "Test failed." << std::endl;
		else
			std::cout << "Test passed." << std::endl;
		return alert;
	}


	template <typename T>
	static bool testProjectionLLTo2D(T dummy) {

		std::cout << "Testing Lat/Lon to 2D Conversion" << std::endl;
		bool alert = false;
		for(int i=0; i<test_runs; i++){
			T i_lat = randomNumber((T) -90.0, (T) 90.0);
			T i_lon = randomNumber((T) -180.0, (T) 180.0);
			T x, y, o_lat, o_lon;
			CCube_To_Sphere_Projection::projectLLDegTo2D(i_lat, i_lon, &x, &y);
			CCube_To_Sphere_Projection::project2DToLL(x, y, &o_lat, &o_lon);

			T error_lat = (i_lat - o_lat) / (T) 180.0;
			T error_lon = (i_lon - o_lon) / (T) 360.0;

			if (error_lat > error_tolerance || error_lon > error_tolerance) {
				printf("%4.4f, %4.4f -> %10.4f, %10.4f -> %4.4f, %4.4f \n   Error: %2.15f, %2.15f \n",
						i_lat, i_lon, x, y, o_lat, o_lon, error_lat, error_lon);
				alert = true;
			}
		}
		if (alert)
			std::cout << "Test failed." << std::endl;
		else
			std::cout << "Test passed." << std::endl;
		return alert;
	}


	template <typename T>
	static bool testProjection2DTo3D(T dummy) {

		std::cout << "Testing 2D to 3D Conversion" << std::endl;
		bool alert = false;
		for(int i=0; i<test_runs; i++){
			T i_x = randomNumber((T) -4.0, (T) 4.0);
			T i_y;
			if (i_x < 0 && i_x > -2){
				i_y = randomNumber((T) -3.0, (T) 3.0);
			} else{
				i_y = randomNumber((T) -1.0, (T) 1.0);
			}
			T x, y, z, o_x, o_y;
			CCube_To_Sphere_Projection::project2DTo3D(i_x, i_y, x, y, z);
			CCube_To_Sphere_Projection::project3DTo2D(x, y, z, &o_x, &o_y);

			T error_x = (i_x - o_x) / (T) 8.0;
			T error_y = (i_y - o_y) / (T) 2.0;

			if (error_x > error_tolerance || error_y > error_tolerance) {
				printf("%4.4f, %4.4f -> %10.4f, %10.4f, %10.4f -> %4.4f, %4.4f \n   Error: %2.15f, %2.15f \n",
						i_x, i_y, x, y, z, o_x, o_y, error_x, error_y);
				alert = true;
			}
		}
		if (alert)
			std::cout << "Test failed." << std::endl;
		else
			std::cout << "Test passed." << std::endl;
		return alert;
	}

};

#endif
