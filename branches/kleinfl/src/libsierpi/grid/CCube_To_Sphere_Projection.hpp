/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: 31. October 2012
 *      Author: Michael Müller <muellmic@in.tum.de>
 *      Author: Florian Klein <kleinfl@in.tum.de>
 */

#ifndef CCUBE_TO_SPHERE_PROJECTIONS_HPP_
#define CCUBE_TO_SPHERE_PROJECTIONS_HPP_

#include <cmath>

#include "libsierpi/triangle/CTriangle_VectorProjections_Equiangular.hpp"


/**
 * This class contains helper methods to project
 * 2D world coordinates to 2D coordinates on a cube face
 * and 3D coordinates on a unit sphere.
 */
class CCube_To_Sphere_Projection
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:

	/**
	 * Faces of the cube which is projected to the cubed sphere
	 */
	enum EFaceType {
		E_ERROR = -1,
		E_FACE_FRONT = 0,
		E_FACE_RIGHT = 1,
		E_FACE_BACK = 2,
		E_FACE_LEFT = 3,
		E_FACE_TOP = 4,
		E_FACE_BOTTOM = 5
	};



public:
	/**
	 * This method identifies the cube face of a triangle from its 2D position,
	 * assuming that the cube map lies in a 8x8 world, linearly scaled in x and
	 * y direction and centered at (0,0).
	 *
	 *
	 *
	 *           (-2,3)      (0,3)
	 *              +----------+
	 *              |          |
	 *              |          |
	 *              |   TOP    |
	 *(-4,1)        |          |        (2,1)      (4,1)
	 *   +----------+----------+----------+----------+
	 *   |          |          |          |          |
	 *   |          |          |          |          |
	 *   |   LEFT   |  FRONT   |  RIGHT   |   BACK   |
	 *   |          |          |          |          |
	 *   +----------+----------+----------+----------+
	 *(-4,-1)       |          |        (2,-1)     (4,-1)
	 *              |          |
	 *              |  BOTTOM  |
	 *              |          |
	 *              +----------+
	 *           (-2,-3)     (0,-3)
	 *
	 */
	static inline EFaceType getFaceIdOf2DCubeCoordinate(
		T i_x,	T i_y
	) {
		// compare sum of x- or y-coordinates to 3 times the interval boundaries

		// horizontal band
		if(	(i_y >= (T)-1.0) && (i_y <= (T)1.0)	)		// y between -1 and 1
		{
			if(i_x < (T)-4.0){		// x < -4	ERROR
				return E_ERROR;
			} else if(i_x < (T)-2.0) {	// x between -4 and -2
				return E_FACE_LEFT;
			} else if(i_x < (T)0.0) {	// x between -2 and 0
				return E_FACE_FRONT;
			} else if(i_x < (T)2.0) {	// x between 0 and 2
				return E_FACE_RIGHT;
			} else if(i_x <= (T)4.0) {	// x between 2 and 4
				return E_FACE_BACK;
			}
		}

		// vertical band
		if(	(i_x >= (T)-2.0) && (i_x <= (T)0.0)	)		// x between -2 and 0
		{
			if(	i_y < (T)-3.0) {		// y < -3	ERROR
				return E_ERROR;
			} else if(i_y < (T)-1.0) {	// y between -3 and -1
				return E_FACE_BOTTOM;
			} else if(i_y < (T)1.0) {	// y between -1 and 1
				return E_FACE_FRONT;
			} else if(i_y <= (T)3.0) {	// y between 1 and 3
				return E_FACE_TOP;
			}
		}

#if DEBUG
		std::cerr << "WARNING: Invalid vertex in method getFaceIdOf2DCubeCoordinate (" << i_x << " " << i_y << ")" << std::endl;
#endif
		return E_ERROR;
	}




	/**
	 * This method projects the 2D world coordinates of a triangle to the
	 * 2D coordinates of the corresponding cube face, assuming that the
	 * cube map lies in a 8x8 world, linearly scaled in x and y direction
	 * and centered at (0,0).
	 *
	 * INPUT REFERENCE SYSTEM:
	 *
	 *           (-2,3)      (0,3)
	 *              +----------+
	 *              |          |
	 *              |          |
	 *              |   TOP    |
	 *(-4,1)        |          |        (2,1)      (4,1)
	 *   +----------+----------+----------+----------+
	 *   |          |          |          |          |
	 *   |          |          |          |          |
	 *   |   LEFT   |  FRONT   |  RIGHT   |   BACK   |
	 *   |          |          |          |          |
	 *   +----------+----------+----------+----------+
	 *(-4,-1)       |          |        (2,-1)     (4,-1)
	 *              |          |
	 *              |  BOTTOM  |
	 *              |          |
	 *              +----------+
	 *           (-2,-3)     (0,-3)
	 *
	 *
	 *
	 * OUTPUT REFERENCE SYSTEM:
	 *
	 * (-1,1)      (1,1)
	 *    +----------+
	 *    |          |
	 *    |          |
	 *    |   FACE   |
	 *    |          |
	 *    +----------+
	 * (-1,-1)     (1,-1)
	 */
	template <typename T>
	inline static void project2DTo2DFace(
			CCube_To_Sphere_Projection::EFaceType i_face,
			T *io_vertex_x,	T *io_vertex_y
	)	{

		// translation to face
		switch(i_face){ // no change in y for LEFT, FRONT, RIGHT and BACK face
		case CCube_To_Sphere_Projection::E_FACE_LEFT:
			*io_vertex_x = *io_vertex_x + (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_FRONT:
			*io_vertex_x = *io_vertex_x + (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_RIGHT:
			*io_vertex_x = *io_vertex_x - (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BACK:
			*io_vertex_x = *io_vertex_x - (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_TOP:
			*io_vertex_x = *io_vertex_x + (T)1.0;
			*io_vertex_y = *io_vertex_y - (T)2.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BOTTOM:
			*io_vertex_x = *io_vertex_x + (T)1.0;
			*io_vertex_y = *io_vertex_y + (T)2.0;
			break;

		default:
		case CCube_To_Sphere_Projection::E_ERROR:
			std::cerr << "project2DTo2DFace: invalid face" << std::endl;
//			assert(false);
			break;
		}
	}


	template <typename T>
	inline static void project2DTo2DFace(
			CCube_To_Sphere_Projection::EFaceType i_face,
			T *io_vertex_left_x,	T *io_vertex_left_y,
			T *io_vertex_right_x,	T *io_vertex_right_y,
			T *io_vertex_top_x,		T *io_vertex_top_y
	)	{

		// translation to face
		switch(i_face)
		{ // no change in y for LEFT, FRONT, RIGHT and BACK face
		case CCube_To_Sphere_Projection::E_FACE_LEFT:
			*io_vertex_left_x = *io_vertex_left_x + (T)3.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)3.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_FRONT:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_RIGHT:
			*io_vertex_left_x = *io_vertex_left_x - (T)1.0;
			*io_vertex_right_x = *io_vertex_right_x - (T)1.0;
			*io_vertex_top_x = *io_vertex_top_x - (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BACK:
			*io_vertex_left_x = *io_vertex_left_x - (T)3.0;
			*io_vertex_right_x = *io_vertex_right_x - (T)3.0;
			*io_vertex_top_x = *io_vertex_top_x - (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_TOP:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_left_y = *io_vertex_left_y - (T)2.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_right_y = *io_vertex_right_y - (T)2.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			*io_vertex_top_y = *io_vertex_top_y - (T)2.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BOTTOM:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_left_y = *io_vertex_left_y + (T)2.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_right_y = *io_vertex_right_y + (T)2.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			*io_vertex_top_y = *io_vertex_top_y + (T)2.0;
			break;

		default:
		case CCube_To_Sphere_Projection::E_ERROR:
			std::cerr << "project2DTo2DFace: invalid face id " << i_face << std::endl;
//			assert(false);
			break;
		}
	}


	/**
	 * This method projects face based coordinates of a unit-sphere-bounding cube onto
	 * the unit sphere using central projection.
	 * The target coordinates are 3D sphere coordinates.
	 *
	 *
	 *
	 * INPUT REFERENCE SYSTEM:
	 *
	 * (-1,1)      (1,1)
	 *    +----------+
	 *    |          |
	 *    |          |
	 *    |   FACE   |
	 *    |          |
	 *    +----------+
	 * (-1,-1)     (1,-1)
	 *
	 *
	 * OUTPUT COODINATE SYSTEM:
	 *
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 */

	template <typename T>
	inline static void project2DFaceTo3D(
			EFaceType face,
			T i_vertex_x, 	// face based x-coordinate
			T i_vertex_y, 	// face based y-coordinate,

			T *o_sphere_x,	// 3D x-coordinate on sphere
			T *o_sphere_y,	// 3D y-coordinate on sphere
			T *o_sphere_z	// 3D z-coordinate on sphere
	)
	{

#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinatesToEquiangular(&i_vertex_x, &i_vertex_y);
#endif
		T x,y,z;
		// transform local 2D coordinates to global 3D coordinates
		switch(face){
		case E_FACE_LEFT:
			x = i_vertex_x;
			y = (T)-1.0;
			z = i_vertex_y;
			break;

		case E_FACE_FRONT:
			x = (T)1.0;
			y = i_vertex_x;
			z = i_vertex_y;
			break;

		case E_FACE_RIGHT:
			x = -i_vertex_x;
			y = (T)1.0;
			z = i_vertex_y;
			break;

		case E_FACE_BACK:
			x = (T)-1.0;
			y = -i_vertex_x;
			z = i_vertex_y;
			break;

		case E_FACE_TOP:
			x = -i_vertex_y;
			y = i_vertex_x;
			z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			x = i_vertex_y;
			y = i_vertex_x;
			z = (T)-1.0;
			break;

		default:
			x=y=z=(T)-1;
			break;
		}

		// get vector length
		T d;
		d = std::sqrt(y*y + z*z + x*x);

		// normalize vector to the size of 1 (unit sphere)
		*o_sphere_x = (T)x / d;
		*o_sphere_y = (T)y / d;
		*o_sphere_z = (T)z / d;

	}

	/**
	 * This method projects 3D sphere coordinates to a bounding cube using central projection.
	 * The target coordinates are local coordinates on the corresponding cube face.
	 *
	 * INPUT COODINATE SYSTEM:
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 *
	 *
	 * OUTPUT REFERENCE SYSTEM:
	 *
	 * (-1,1)      (1,1)
	 *    +----------+
	 *    |          |
	 *    |          |
	 *    |   FACE   |
	 *    |          |
	 *    +----------+
	 * (-1,-1)     (1,-1)
	 */
	template <typename T>
	inline static void project3DTo2DFace(
			T *o_vertex_x, 	// face based x-coordinate
			T *o_vertex_y, 	// face based y-coordinate,
			EFaceType *o_vertex_face,

			T i_sphere_x,	// 3D x-coordinate on sphere
			T i_sphere_y,	// 3D y-coordinate on sphere
			T i_sphere_z	// 3D z-coordinate on sphere
	)
	{
		project3DTo2DFace(
				o_vertex_x,
				o_vertex_y,
				i_sphere_x,
				i_sphere_y,
				i_sphere_z
				);

		EFaceType face;

		/*
		 * Get the face of the cube and largest component of the position vector.
		 *
		 * The given spherical coordinate will be mapped onto a face of the bounding cube.
		 * The face is defined by the largest component of the spherical coordinate.
		 * The sign of the largest component decides between two opposed faces.
		 */
		if(std::abs(i_sphere_x) >= std::abs(i_sphere_y) && std::abs(i_sphere_x) >= std::abs(i_sphere_z)){
			if(i_sphere_x >= 0 ){
				face = E_FACE_FRONT;
			}else{
				face = E_FACE_BACK;
			}
		}else if(std::abs(i_sphere_y) >= std::abs(i_sphere_x) && std::abs(i_sphere_y) >= std::abs(i_sphere_z)){
			if(i_sphere_y >= 0){
				face = E_FACE_RIGHT;
			}else{
				face = E_FACE_LEFT;
			}
		}else if(std::abs(i_sphere_z) >= std::abs(i_sphere_x) && std::abs(i_sphere_z) >= std::abs(i_sphere_y)){
			if(i_sphere_z > 0){
				face = E_FACE_TOP;
			}else {
				face = E_FACE_BOTTOM;
			}
		}else{
			assert(false);
		}

		*o_vertex_face = face;
//		*o_vertex_face = getFaceIdOf2DCubeCoordinate(*o_vertex_x, *o_vertex_y);
	}

	template <typename T>
	inline static void project3DTo2DFace(
			T *o_vertex_x, 	// face based x-coordinate
			T *o_vertex_y, 	// face based y-coordinate,

			T i_sphere_x,	// 3D x-coordinate on sphere
			T i_sphere_y,	// 3D y-coordinate on sphere
			T i_sphere_z	// 3D z-coordinate on sphere
	)
	{

		T x,y,z;

		T d;
		EFaceType face;

		/*
		 * Get the face of the cube and largest component of the position vector.
		 *
		 * The given spherical coordinate will be mapped onto a face of the bounding cube.
		 * The face is defined by the largest component of the spherical coordinate.
		 * The sign of the largest component decides between two opposed faces.
		 */
		if(std::abs(i_sphere_x) >= std::abs(i_sphere_y) && std::abs(i_sphere_x) >= std::abs(i_sphere_z)){
			if(i_sphere_x >= 0 ){
				d = i_sphere_x;
				face = E_FACE_FRONT;
			}else{
				d = -i_sphere_x;
				face = E_FACE_BACK;
			}
		}else if(std::abs(i_sphere_y) >= std::abs(i_sphere_x) && std::abs(i_sphere_y) >= std::abs(i_sphere_z)){
			if(i_sphere_y >= 0){
				d = i_sphere_y;
				face = E_FACE_RIGHT;
			}else{
				d = -i_sphere_y;
				face = E_FACE_LEFT;
			}
		}else if(std::abs(i_sphere_z) >= std::abs(i_sphere_x) && std::abs(i_sphere_z) >= std::abs(i_sphere_y)){
			if(i_sphere_z > 0){
				d = i_sphere_z;
				face = E_FACE_TOP;
			}else {
				d = -i_sphere_z;
				face = E_FACE_BOTTOM;
			}
		}else{
			assert(false);
		}

		// normalize x,y and z, so that the largest component is 1
		x = (T)i_sphere_x / d;
		y = (T)i_sphere_y / d;
		z = (T)i_sphere_z / d;
//		std::cout << "3dto2dface in " << i_sphere_x << ", " << i_sphere_y << ", " << i_sphere_z << std::endl;
//		std::cout << "3dto2dface abs " << std::abs(i_sphere_x) << ", " << std::abs(i_sphere_y) << ", " << std::abs(i_sphere_z) << std::endl;
//std::cout << "3dto2dface out " << x << ", " << y << ", " << z << ";  " << d << std::endl;
		// turn coordinate system to match with the local coordinates
		switch(face){
		case E_FACE_LEFT:
			*o_vertex_x = x;
			//y = (T)-1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_FRONT:
			//x = (T)1.0;
			*o_vertex_x = y;
			*o_vertex_y = z;
			break;

		case E_FACE_RIGHT:
			*o_vertex_x = -x;
			//y = (T)1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_BACK:
			//x = (T)-1.0;
			*o_vertex_x = -y;
			*o_vertex_y = z;
			break;

		case E_FACE_TOP:
			*o_vertex_y = -x;
			*o_vertex_x = y;
			//z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			*o_vertex_y = x;
			*o_vertex_x = y;
			//z = (T)-1.0;
			break;

		default:
			assert(false);
			x=y=z=(T)-1;
			break;
		}

#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinatesEquiangularToStandard(o_vertex_x, o_vertex_y);
#endif

	}

	template <typename T>
	inline static void project3DToFixed2DFace(
			T i_sphere_x,	// 3D x-coordinate on sphere
			T i_sphere_y,	// 3D y-coordinate on sphere
			T i_sphere_z,	// 3D z-coordinate on sphere
			EFaceType i_face,

			T *o_vertex_x, 	// face based x-coordinate
			T *o_vertex_y 	// face based y-coordinate,
	)
	{

		T x,y,z;
		T d;

		/*
		 * Get the face of the cube and largest component of the position vector.
		 *
		 * The given spherical coordinate will be mapped onto a face of the bounding cube.
		 * The face is defined by the largest component of the spherical coordinate.
		 * The sign of the largest component decides between two opposed faces.
		 */
		switch(i_face){
		case E_FACE_LEFT:
			d = -i_sphere_y;
			break;
		case E_FACE_FRONT:
			d = i_sphere_x;
			break;
		case E_FACE_RIGHT:
			d = i_sphere_y;
			break;
		case E_FACE_BACK:
			d = -i_sphere_x;
			break;
		case E_FACE_TOP:
			d = i_sphere_z;
			break;
		case E_FACE_BOTTOM:
			d = -i_sphere_z;
			break;
		}

		// normalize x,y and z
		x = (T)i_sphere_x / d;
		y = (T)i_sphere_y / d;
		z = (T)i_sphere_z / d;

		// turn coordinate system to match with the local coordinates
		switch(i_face){
		case E_FACE_LEFT:
			*o_vertex_x = x;
			//y = (T)-1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_FRONT:
			//x = (T)1.0;
			*o_vertex_x = y;
			*o_vertex_y = z;
			break;

		case E_FACE_RIGHT:
			*o_vertex_x = -x;
			//y = (T)1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_BACK:
			//x = (T)-1.0;
			*o_vertex_x = -y;
			*o_vertex_y = z;
			break;

		case E_FACE_TOP:
			*o_vertex_y = -x;
			*o_vertex_x = y;
			//z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			*o_vertex_y = x;
			*o_vertex_x = y;
			//z = (T)-1.0;
			break;

		default:
			assert(false);
			x=y=z=(T)-1;
			break;
		}

#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinatesEquiangularToStandard(o_vertex_x, o_vertex_y);
#endif

	}

	template <typename T>
	inline static void project3DTo2DWithFixedFace(
			T i_sphere_x,	// 3D x-coordinate on sphere
			T i_sphere_y,	// 3D y-coordinate on sphere
			T i_sphere_z,	// 3D z-coordinate on sphere
			EFaceType i_face,

			T *o_vertex_x, 	// face based x-coordinate
			T *o_vertex_y 	// face based y-coordinate,
	)
	{

		T x,y,z;
		T d;

		/*
		 * Get the face of the cube and largest component of the position vector.
		 *
		 * The given spherical coordinate will be mapped onto a face of the bounding cube.
		 * The face is defined by the largest component of the spherical coordinate.
		 * The sign of the largest component decides between two opposed faces.
		 */
		switch(i_face){
		case E_FACE_LEFT:
			d = -i_sphere_y;
			break;
		case E_FACE_FRONT:
			d = i_sphere_x;
			break;
		case E_FACE_RIGHT:
			d = i_sphere_y;
			break;
		case E_FACE_BACK:
			d = -i_sphere_x;
			break;
		case E_FACE_TOP:
			d = i_sphere_z;
			break;
		case E_FACE_BOTTOM:
			d = -i_sphere_z;
			break;
		}

		// normalize x,y and z
		x = (T)i_sphere_x / d;
		y = (T)i_sphere_y / d;
		z = (T)i_sphere_z / d;

		// turn coordinate system to match with the local coordinates
		switch(i_face)
		{
		case E_FACE_LEFT:
			*o_vertex_x = x-3;
			//y = (T)-1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_FRONT:
			//x = (T)1.0;
			*o_vertex_x = y-1;
			*o_vertex_y = z;
			break;

		case E_FACE_RIGHT:
			*o_vertex_x = 1-x;
			//y = (T)1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_BACK:
			//x = (T)-1.0;
			*o_vertex_x = 3-y;
			*o_vertex_y = z;
			break;

		case E_FACE_TOP:
			*o_vertex_y = 2-x;
			*o_vertex_x = y-1;
			//z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			*o_vertex_y = x-2;
			*o_vertex_x = y-1;
			//z = (T)-1.0;
			break;

		default:
			assert(false);
			x=y=z=(T)-1;
			break;
		}

#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinatesEquiangularToStandard(o_vertex_x, o_vertex_y);
#endif

	}


	/**
	 * This method projects 3D sphere coordinates to the 2D map of the bounding cube
	 * using central projection. It is assumed that the cube map lies in an 8x8 world,
	 * linearly scaled in x and y direction and centered at (0,0).
	 *
	 * INPUT COODINATE SYSTEM:
	 *
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 *
	 *
	 * OUTPUT REFERENCE SYSTEM:
	 *
	 *           (-2,3)      (0,3)
	 *              +----------+
	 *              |          |
	 *              |          |
	 *              |   TOP    |
	 *(-4,1)        |          |        (2,1)      (4,1)
	 *   +----------+----------+----------+----------+
	 *   |          |          |          |          |
	 *   |          |          |          |          |
	 *   |   LEFT   |  FRONT   |  RIGHT   |   BACK   |
	 *   |          |          |          |          |
	 *   +----------+----------+----------+----------+
	 *(-4,-1)       |          |        (2,-1)     (4,-1)
	 *              |          |
	 *              |  BOTTOM  |
	 *              |          |
	 *              +----------+
	 *           (-2,-3)     (0,-3)
	 *
	 */
	template <typename T>
	inline static void project3DTo2D(
		T i_sphere_x,	// 3D x-coordinate on sphere
		T i_sphere_y,	// 3D y-coordinate on sphere
		T i_sphere_z,	// 3D z-coordinate on sphere

		T *o_vertex_x, 	// world space x-coordinate
		T *o_vertex_y 	// world space y-coordinate
	)
	{
		T x,y,z;

		T d;
		EFaceType face;

		/*
		 * Get the face of the cube and largest component of the position vector.
		 *
		 * The given spherical coordinate will be mapped onto a face of the bounding cube.
		 * The face is defined by the largest component of the spherical coordinate.
		 * The sign of the largest component decides between two opposed faces.
		 */
		if(std::abs(i_sphere_x) >= std::abs(i_sphere_y) && std::abs(i_sphere_x) >= std::abs(i_sphere_z))
		{
			if (i_sphere_x >= 0 )
			{
				d = i_sphere_x;
				face = E_FACE_FRONT;
			}
			else
			{
				d = -i_sphere_x;
				face = E_FACE_BACK;
			}
		}
		else if (std::abs(i_sphere_y) >= std::abs(i_sphere_x) && std::abs(i_sphere_y) >= std::abs(i_sphere_z))
		{
			if(i_sphere_y >= 0)
			{
				d = i_sphere_y;
				face = E_FACE_RIGHT;
			}
			else
			{
				d = -i_sphere_y;
				face = E_FACE_LEFT;
			}
		}
		else if (std::abs(i_sphere_z) >= std::abs(i_sphere_x) && std::abs(i_sphere_z) >= std::abs(i_sphere_y))
		{
			if(i_sphere_z > 0)
			{
				d = i_sphere_z;
				face = E_FACE_TOP;
			}
			else
			{
				d = -i_sphere_z;
				face = E_FACE_BOTTOM;
			}
		}
		else
		{
			assert(false);
		}

		// normalize x,y and z, so that the largest component is 1
		x = (T)i_sphere_x / d;
		y = (T)i_sphere_y / d;
		z = (T)i_sphere_z / d;

#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinateEquiangularToStandard(&x);
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinateEquiangularToStandard(&y);
		CTriangle_VectorProjections_Equiangular::changeCubeCoordinateEquiangularToStandard(&z);
#endif

		// turn and translate coordinate system to match with the world space coordinates
		switch(face)
		{
		case E_FACE_LEFT:
			*o_vertex_x = x-3;
			//y = (T)-1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_FRONT:
			//x = (T)1.0;
			*o_vertex_x = y-1;
			*o_vertex_y = z;
			break;

		case E_FACE_RIGHT:
			*o_vertex_x = 1-x;
			//y = (T)1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_BACK:
			//x = (T)-1.0;
			*o_vertex_x = 3-y;
			*o_vertex_y = z;
			break;

		case E_FACE_TOP:
			*o_vertex_y = 2-x;
			*o_vertex_x = y-1;
			//z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			*o_vertex_y = x-2;
			*o_vertex_x = y-1;
			//z = (T)-1.0;
			break;

		default:
			assert(false);
			x=y=z=(T)-1;
			break;
		}

//#if CONFIG_SPHERICAL_EQUIANGULAR_MODE==1
//		CTriangle_VectorProjections_Equiangular::changeCubeCoordinatesEquiangularToStandard(o_vertex_x, o_vertex_y);
//#endif
	}


	template <typename T>
	static inline bool isValidCoordinate(
			T i_x,
			T i_y
	) {

		// horizontal band
		if(	(i_y >= (T)-1.0) && (i_y <= (T)1.0)	)		// y between -1 and 1
		{
			if(i_x < (T)-4.0){		// x < -4	ERROR
				return false;
			} else if(i_x < (T)-2.0) {	// x between -4 and -2
				return true;
			} else if(i_x < (T)0.0) {	// x between -2 and 0
				return true;
			} else if(i_x < (T)2.0) {	// x between 0 and 2
				return true;
			} else if(i_x <= (T)4.0) {	// x between 2 and 4
				return true;
			}
		}

		// vertical band
		if(	(i_x >= (T)-2.0) && (i_x <= (T)0.0)	)		// x between -2 and 0
		{
			if(	i_y < (T)-3.0) {		// y < -3	ERROR
				return false;
			} else if(i_y < (T)-1.0) {	// y between -3 and -1
				return true;
			} else if(i_y < (T)1.0) {	// y between -1 and 1
				return true;
			} else if(i_y <= (T)3.0) {	// y between 1 and 3
				return true;
			}
		}

		return false;
	}


	/**
	 * compute the 3D sphere coordinate based on 2D coordinate
	 */
	template <typename T>
	static inline bool project2DTo3D(
			T i_x,
			T i_y,

			T &o_x,
			T &o_y,
			T &o_z
	) {
		CCube_To_Sphere_Projection::EFaceType face_point = CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(i_x, i_y);

		if (face_point == E_ERROR)
			return false;

		CCube_To_Sphere_Projection::project2DTo2DFace(
				face_point,
				&i_x,&i_y
			);

		CCube_To_Sphere_Projection::project2DFaceTo3D(
				face_point,
				i_x,
				i_y,
				&o_x,
				&o_y,
				&o_z
			);

		return true;
	}


	/**
	 * Computes 3D sphere coordinates
	 * from latitude/longitude coordinates given in degrees.
	 */
	template <typename T>
	static inline void projectLLDegTo3D(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y,
			T *o_vertex_z
	) {
		i_vertex_lat = ((T)90.0 - i_vertex_lat) * M_PI / (T)180.0;
		i_vertex_lon = i_vertex_lon * M_PI / (T)180.0;

		projectLLTo3D(i_vertex_lat, i_vertex_lon, o_vertex_x, o_vertex_y, o_vertex_z);
	}


	/**
	 * Projects latitude/longitude coordinates given in degrees
	 * to 2D world space coordinates.
	 */
	template <typename T>
	static inline void projectLLDegTo2D(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y
	) {
		i_vertex_lat = ((T)90.0 - i_vertex_lat) * M_PI / (T)180.0;
		i_vertex_lon = i_vertex_lon * M_PI / (T)180.0;

		T x, y, z;
		projectLLTo3D(i_vertex_lat, i_vertex_lon, &x, &y, &z);

		CCube_To_Sphere_Projection::project3DTo2D(x, y, z, o_vertex_x, o_vertex_y);
	}


	/**
	 * Computes latitude/longitude coordinates in degrees
	 * from 3D sphere coordinates.
	 */
	template <typename T>
	static inline void project3DToLLDeg(
			T i_vertex_x,
			T i_vertex_y,
			T i_vertex_z,

			T *o_vertex_lat,
			T *o_vertex_lon
	) {
		*o_vertex_lat = (T)90.0 - (acos(i_vertex_z) * (T)180.0 / M_PI);
		*o_vertex_lon = (atan2(i_vertex_y, i_vertex_x) * (T)180.0 / M_PI);
	}


	/**
	 * Computes latitude/longitude coordinates in degrees
	 * from 2D world space coordinates.
	 */
	template <typename T>
	static inline void project2DToLLDeg(
			T i_vertex_x,
			T i_vertex_y,

			T *o_vertex_lat,
			T *o_vertex_lon
	) {
		T x, y, z;
		CCube_To_Sphere_Projection::project2DTo3D(
				i_vertex_x,
				i_vertex_y,
				x,
				y,
				z
				);
		CCube_To_Sphere_Projection::project3DToLLDeg(
				x,
				y,
				z,
				o_vertex_lat,
				o_vertex_lon);
	}


	/**
	 * Computes latitude/longitude coordinates in radians
	 * from 3D sphere coordinates.
	 */
	template <typename T>
	static inline void project3DToLL(
			T i_vertex_x,
			T i_vertex_y,
			T i_vertex_z,

			T *o_vertex_lat,
			T *o_vertex_lon
	) {
		normalizeVector(&i_vertex_x, &i_vertex_y, &i_vertex_z);
		*o_vertex_lat = asin(i_vertex_z);
		*o_vertex_lon = atan2(i_vertex_y, i_vertex_x);
	}


	/**
	 * Computes latitude/longitude coordinates in radians
	 * from 2D world space coordinates.
	 */
	template <typename T>
	static inline void project2DToLL(
			T i_vertex_x,
			T i_vertex_y,

			T *o_vertex_lat,
			T *o_vertex_lon
	) {
		T x, y, z;
		CCube_To_Sphere_Projection::project2DTo3D(
				i_vertex_x,
				i_vertex_y,
				x,
				y,
				z
				);
		CCube_To_Sphere_Projection::project3DToLL(
				x,
				y,
				z,
				o_vertex_lat,
				o_vertex_lon);
	}


	/**
	 * Computes 3D sphere coordinates
	 * from latitude/longitude coordinates given in radians.
	 */
	template <typename T>
	static inline void projectLLTo3D(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y,
			T *o_vertex_z
	) {
//		i_vertex_lat = (T)0.5 * M_PI - i_vertex_lat;
//
//		*o_vertex_x = sin(i_vertex_lat) * cos(i_vertex_lon);
//		*o_vertex_y = sin(i_vertex_lat) * sin(i_vertex_lon);
//		*o_vertex_z = cos(i_vertex_lat);
		*o_vertex_x = cos(i_vertex_lat) * cos(i_vertex_lon);
		*o_vertex_y = cos(i_vertex_lat) * sin(i_vertex_lon);
		*o_vertex_z = sin(i_vertex_lat);

//		std::cout << i_vertex_lat << ", " << i_vertex_lon << std::endl;
//		std::cout << "-> " << *o_vertex_x << ", " << *o_vertex_y << ", " << *o_vertex_z << std::endl;
	}


	/**
	 * Projects latitude/longitude coordinates given in radians
	 * to 2D world space coordinates.
	 */
	template <typename T>
	static inline void projectLLTo2D(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y
	) {
		i_vertex_lat = (T)0.5 * M_PI - i_vertex_lat;

		T x = sin(i_vertex_lat) * cos(i_vertex_lon);
		T y = sin(i_vertex_lat) * sin(i_vertex_lon);
		T z = cos(i_vertex_lat);

		CCube_To_Sphere_Projection::project3DTo2D(x, y, z, o_vertex_x, o_vertex_y);
	}


	/**
	 * Projects latitude/longitude coordinates given in radians
	 * to 2D world space coordinates.
	 */
	template <typename T>
	static inline void projectLLTo2DFace(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y
	) {
		i_vertex_lat = (T)0.5 * M_PI - i_vertex_lat;

		T x = sin(i_vertex_lat) * cos(i_vertex_lon);
		T y = sin(i_vertex_lat) * sin(i_vertex_lon);
		T z = cos(i_vertex_lat);

		CCube_To_Sphere_Projection::project3DTo2DFace(o_vertex_x, o_vertex_y, x, y, z);
	}


	/**
	 * Projects latitude/longitude coordinates given in radians
	 * to 2D world space coordinates.
	 */
	template <typename T>
	static inline void projectLLTo2DFace(
			T i_vertex_lat,
			T i_vertex_lon,

			T *o_vertex_x,
			T *o_vertex_y,
			EFaceType *o_face
	) {
		i_vertex_lat = (T)0.5 * M_PI - i_vertex_lat;

		T x = sin(i_vertex_lat) * cos(i_vertex_lon);
		T y = sin(i_vertex_lat) * sin(i_vertex_lon);
		T z = cos(i_vertex_lat);
//		std::cout << "xyz " << x << ", " << y << ", " << z << std::endl;
		CCube_To_Sphere_Projection::project3DTo2DFace(o_vertex_x, o_vertex_y, o_face, x, y, z);
	}

	/*
	 * vector length of (x, y, z)
	 */
	template <typename T>
	static inline T vectorLength(
			T i_x, T i_y, T i_z
	) {
		return std::sqrt(i_x*i_x + i_y*i_y + i_z*i_z);
	}

	/*
	 * normalize vector (x, y, z)
	 */
	template <typename T>
	static inline void normalizeVector(
			T *io_x, T *io_y, T *io_z
	) {
		T length = vectorLength(*io_x, *io_y, *io_z);
//		std::cout << *io_x << ", " << *io_y << ", " << *io_z << " -> " << length << std::endl;
		*io_x = *io_x / length;
		*io_y = *io_y / length;
		*io_z = *io_z / length;
	}


	/*
	 * conversion from angle in degrees to angle in radians
	 */
	template <typename T>
	static inline T angleDegToRad(
			T i_alpha
	) {
		return i_alpha * (T)M_PI / (T)180;
	}


	/*
	 * conversion from angle in radians to angle in degrees
	 */
	template <typename T>
	static inline T angleRadToDeg(
			T i_alpha
	) {
		return i_alpha * (T)180 / (T)M_PI;
	}

};

#endif
