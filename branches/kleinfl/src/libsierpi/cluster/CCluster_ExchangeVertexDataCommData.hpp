/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CCluster_ExchangeFluxCommData.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_EXCHANGE_VERTEXDATA_COMM_HPP
#define CCLUSTER_EXCHANGE_VERTEXDATA_COMM_HPP

#include "global_config.h"
#include "libsierpi/grid/CDomain_BaseTriangle.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"
#include <string.h>
#include <iostream>



namespace sierpi
{

/**
 * this class handles the communication with the adjacent edges based on the adjacency
 * information given in CClusterTree_Node.
 */
template <	typename CCluster_TreeNode,			///< cluster tree node
			typename CNodeData,					///< type of single node element
			typename CStackAccessors_,			///< access to stacks
			typename CVertexDataCommKernel		///< kernel type offering 'op_edge_edge'
		>
class CCluster_ExchangeVertexDataCommData	: public CStackAccessors_
{
private:
	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode> CEdgeComm_InformationAdjacentCluster_;

private:
	CStack<CNodeData> *localHypExchangeEdgeStack;
	CStack<CNodeData> *localCatExchangeEdgeStack;

	CStack<CNodeData> *localHypEdgeStack;
	CStack<CNodeData> *localCatEdgeStack;

	/**
	 * Handler to root clusterTreeNode
	 */
	CCluster_TreeNode *cCluster_TreeNode;


	/**
	 * Pointer to kernel offering method 'op_edge_edge' to
	 * compute the flux updates
	 */
	CVertexDataCommKernel *cVertexDataCommKernel;



	/**
	 * Constructor
	 */
public:
	CCluster_ExchangeVertexDataCommData(
			CCluster_TreeNode *io_cCluster_TreeNode,
			CVertexDataCommKernel *i_cVertexDataCommKernel
	)	:
		localHypExchangeEdgeStack(nullptr),
		localCatExchangeEdgeStack(nullptr),
		localHypEdgeStack(nullptr),
		localCatEdgeStack(nullptr)
	{
		cCluster_TreeNode = io_cCluster_TreeNode;
		cVertexDataCommKernel = i_cVertexDataCommKernel;
	}



	/**
	 * this method is executed for every adjacent cluster simulation to
	 * pull the edge communication data from an adjacent cluster.
	 */
private:
	inline void p_joinVertexCommDataFromAdjacentCluster(
		CNodeData *io_cLocalFirstNodeData,			///< first node data for RLE
		CNodeData *io_cLocalMidNodesDataReversed,	///< middle node data with RLE
		CNodeData *io_cLocalLastNodeData,			///< last node data for RLE

		const CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster	///< information about adjacent clusters
	)
	{
		CCluster_TreeNode *cAdjacentCluster_TreeNode = i_informationAdjacentCluster.cCluster_TreeNode;
		int edge_comm_pos;

		CStack<CNodeData> *adjHypStack, *adjCatStack;
		if (cAdjacentCluster_TreeNode->cTriangleFactory.isHypotenuseDataOnLeftStack())
		{
			adjHypStack = this->leftStackAccessor(cAdjacentCluster_TreeNode);
			adjCatStack = this->rightStackAccessor(cAdjacentCluster_TreeNode);
		}
		else
		{
			adjHypStack = this->rightStackAccessor(cAdjacentCluster_TreeNode);
			adjCatStack = this->leftStackAccessor(cAdjacentCluster_TreeNode);
		}

#if DEBUG && 0
		std::cout << "adjHypStack: " << adjHypStack->getNumberOfElementsOnStack() << std::endl;
		for (size_t i = 0; i < adjHypStack->getNumberOfElementsOnStack(); i++)
			std::cout << adjHypStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;

		std::cout << "adjCatStack: " << adjCatStack->getNumberOfElementsOnStack() << std::endl;
		for (size_t i = 0; i < adjCatStack->getNumberOfElementsOnStack(); i++)
			std::cout << adjCatStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;
#endif

		/*
		 * search on hypotenuse:
		 *
		 * first of all, we search on the hyp communication information to find the appropriate subset
		 */
		edge_comm_pos = 0;	// position of iterator
		auto &hyp_adjacent_clusters = cAdjacentCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters;

		int elements_on_hyp_stack = adjHypStack->getNumberOfElementsOnStack();

//		std::cout << "join search HYP COMM" << std::endl;
		for (auto iter = hyp_adjacent_clusters.begin(); iter != hyp_adjacent_clusters.end(); iter++)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

//			std::cout << adjacent_info << std::endl;

			/*
			 * handle boundary
			 */
			if (adjacent_info.cCluster_UniqueId.raw_unique_id == 0)
			{
				// boundary dummy vertex data
				assert(adjacent_info.edge_comm_elements == 0);
				assert(cAdjacentCluster_TreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_BOUNDARY);

				// nothing to do since we already process the correct position
				edge_comm_pos = adjHypStack->getNumberOfElementsOnStack()-1;
				continue;
			}

			/*
			 * matching adjacent cluster?
			 */
			if (adjacent_info.cCluster_UniqueId != cCluster_TreeNode->cCluster_UniqueId)
			{
				edge_comm_pos += adjacent_info.edge_comm_elements;
				continue;
			}
			assert(i_informationAdjacentCluster.edge_comm_elements == adjacent_info.edge_comm_elements);

			// load pointer to first node data
			CNodeData *cNodeData = adjHypStack->getElementPtrAtIndex(edge_comm_pos);

			/*
			 * FIRST node
			 */
			cVertexDataCommKernel->op_node_node_join_middle_touch(
					cNodeData,
					io_cLocalLastNodeData
				);

			if (adjacent_info.edge_comm_elements != 0)
			{
				cNodeData++;

				/*
				 * MID nodes
				 */
				io_cLocalMidNodesDataReversed += i_informationAdjacentCluster.edge_comm_elements-2;

				for (int i = 0; i < adjacent_info.edge_comm_elements-1; i++)
				{
					// communication partner found
					cVertexDataCommKernel->op_node_node_join_middle_touch(
								cNodeData,
								io_cLocalMidNodesDataReversed
							);

					cNodeData++;
					io_cLocalMidNodesDataReversed--;
				}

				/*
				 * LAST node
				 */
				assert(edge_comm_pos+adjacent_info.edge_comm_elements <= elements_on_hyp_stack);

				if (edge_comm_pos+adjacent_info.edge_comm_elements == elements_on_hyp_stack)
					cNodeData = &adjCatStack->getBackElement();
				else
					cNodeData = &adjHypStack->getElementAtIndex(edge_comm_pos+adjacent_info.edge_comm_elements);

				cVertexDataCommKernel->op_node_node_join_middle_touch(
							cNodeData,
							io_cLocalFirstNodeData
						);
			}
			return;
		}



		/*
		 * search on catheti:
		 *
		 * we continue searching on the cat communication information to find the appropriate subset
		 *
		 * the catheti search has to be split:
		 *
		 * the first and last RLE has to be handled in a special
		 * way due to storing of node data to the hypotenuse
		 */

		/*
		 * load pointer to adjacent hyp/cat stacks
		 */
		std::vector<CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode> > &cat_adjacent_clusters = cAdjacentCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters;

		/*
		 * position on catheti stack: set to -1
		 */
		edge_comm_pos = -1;
		int processed_boundary_RLE = 0;

		for (auto iter = cat_adjacent_clusters.begin(); iter != cat_adjacent_clusters.end(); iter++)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

			if (adjacent_info.cCluster_UniqueId.raw_unique_id == 0)
			{
				// boundary dummy vertex data
				assert(adjacent_info.edge_comm_elements == 0);
				assert(processed_boundary_RLE <= 1);

				if (processed_boundary_RLE == 0)
				{
					// FIRST boundary dummy vertex data match
					// the first match can be on the left or right triangle leg!!!

					// nothing to do since we already process the correct position
					if (	(	cAdjacentCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::ODD &&
								cAdjacentCluster_TreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY
							)
							||
							(	cAdjacentCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::EVEN &&
								cAdjacentCluster_TreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY
							)
					)
					{
						// first edge = boundary
						//   => skip edges
						edge_comm_pos = adjCatStack->getNumberOfElementsOnStack()-1;
						for (auto iter = cat_adjacent_clusters.begin(); iter != cat_adjacent_clusters.end(); iter++)
							edge_comm_pos -= (*iter).edge_comm_elements;
					}
					else
					{
						// go to last data
						edge_comm_pos = adjCatStack->getNumberOfElementsOnStack()-1;
					}

					processed_boundary_RLE++;
					continue;
				}


				// SECOND boundary dummy vertex data match
				// BOTH edges are of type BOUNDARY!!!

				assert(cAdjacentCluster_TreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
				assert(cAdjacentCluster_TreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY);

				// nothing to do since we already process the correct position
				edge_comm_pos = adjCatStack->getNumberOfElementsOnStack()-1;

#if DEBUG
				processed_boundary_RLE++;
#endif
				continue;
			}

			if (adjacent_info.cCluster_UniqueId != cCluster_TreeNode->cCluster_UniqueId)
			{
				edge_comm_pos += adjacent_info.edge_comm_elements;
				continue;
			}
			assert(i_informationAdjacentCluster.edge_comm_elements == adjacent_info.edge_comm_elements);

			CNodeData *cNodeData;

			/*
			 * FIRST node
			 */
			if (edge_comm_pos == -1)
				cNodeData = &adjHypStack->getFrontElement();		// first on hyp
			else
				cNodeData = &adjCatStack->getElementAtIndex(edge_comm_pos);

			cVertexDataCommKernel->op_node_node_join_middle_touch(
					cNodeData,
					io_cLocalLastNodeData
				);

			if (adjacent_info.edge_comm_elements != 0)
			{
				/*
				 * MID nodes
				 */
				io_cLocalMidNodesDataReversed += i_informationAdjacentCluster.edge_comm_elements-2;
				cNodeData = &adjCatStack->getElementAtIndex(edge_comm_pos+1);

				for (int i = 0; i < i_informationAdjacentCluster.edge_comm_elements-1; i++)
				{
					// communication partner found
					cVertexDataCommKernel->op_node_node_join_middle_touch(
								cNodeData,
								io_cLocalMidNodesDataReversed
							);

					cNodeData++;
					io_cLocalMidNodesDataReversed--;
				}

				cVertexDataCommKernel->op_node_node_join_middle_touch(
							cNodeData,
							io_cLocalFirstNodeData
						);
			}
			return;
		}

		std::cerr << "RLE communication partner " << cCluster_TreeNode->cCluster_UniqueId << " at cluster " << i_informationAdjacentCluster.cCluster_UniqueId << " not found!!!" << std::endl;
		assert(false);

	}



	/**
	 * this method is executed by the simulation running on the cluster after global synch to
	 * read the communication stack elements from the adjacent clusters
	 *
	 * loads all adjacent vertex data elements
	 */
public:
	void pullAndComputeVertexCommData()
	{
		/*
		 * setup stacks to store/load data to/from
		 */
		if (cCluster_TreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cCluster_TreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cCluster_TreeNode);

			localHypEdgeStack = this->leftStackAccessor(cCluster_TreeNode);
			localCatEdgeStack = this->rightStackAccessor(cCluster_TreeNode);
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cCluster_TreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cCluster_TreeNode);

			localHypEdgeStack = this->rightStackAccessor(cCluster_TreeNode);
			localCatEdgeStack = this->leftStackAccessor(cCluster_TreeNode);
		}

		assert(localHypExchangeEdgeStack->isEmpty());
		assert(localCatExchangeEdgeStack->isEmpty());

		// TODO: optimize with memcpy
		// update stack elements counters on exchange stacks and initialize exchange stacks
		localHypExchangeEdgeStack->setStackElementCounter(localHypEdgeStack->getNumberOfElementsOnStack());
		for (size_t i = 0; i < localHypEdgeStack->getNumberOfElementsOnStack(); i++)
			localHypExchangeEdgeStack->setElementAtIndex(i, localHypEdgeStack->getElementAtIndex(i));

		// update stack elements counters on exchange stacks and initialize exchange stacks
		localCatExchangeEdgeStack->setStackElementCounter(localCatEdgeStack->getNumberOfElementsOnStack());
		for (size_t i = 0; i < localCatEdgeStack->getNumberOfElementsOnStack(); i++)
			localCatExchangeEdgeStack->setElementAtIndex(i, localCatEdgeStack->getElementAtIndex(i));

#if 0
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << "+ Cluster ID: " << cCluster_TreeNode->cCluster_UniqueId << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
#endif

		/// handler for edge communication with adjacent clusters
#if 0
		std::cout << "localHypExchangeEdgeStack: " << localHypExchangeEdgeStack->getNumberOfElementsOnStack() << std::endl;
		for (size_t i = 0; i < localHypExchangeEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localHypExchangeEdgeStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;

		std::cout << "localCatExchangeEdgeStack: " << localCatExchangeEdgeStack->getNumberOfElementsOnStack() << std::endl;
		for (size_t i = 0; i < localCatExchangeEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localCatExchangeEdgeStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;
#endif


		// pointers to first, middle and last nodes
		CNodeData *cLocalFirstNodeData;
		CNodeData *cLocalMidNodesData;
		CNodeData *cLocalLastNodeData;

		/*
		 * start with processing nodes on hypotenuse.
		 *
		 * H: data stored on hypotenuse stack
		 *
		 * C
		 *  |\_
		 *  |  \_
		 *  |    \H
		 *  |      \_
		 *  |        \H
		 *  |__________\
		 *               H
		 */
//		if (cCluster_TreeNode->cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			int elements_on_hyp_stack = localHypExchangeEdgeStack->getNumberOfElementsOnStack();
			int edge_comm_pos = 0;

			for (	auto iter = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					iter != cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

				// boundary RLE information?
				if (adjacent_info.cCluster_UniqueId.raw_unique_id == 0)
				{
					// boundary dummy vertex data
					assert(adjacent_info.edge_comm_elements == 0);

					assert(cCluster_TreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_BOUNDARY);

					// nothing to do since we already process the correct position
					edge_comm_pos = localHypExchangeEdgeStack->getNumberOfElementsOnStack()-1;
					continue;
				}

				if (adjacent_info.edge_comm_elements == 0)
				{
					/*
					 * single node related communication information
					 */
					cLocalLastNodeData = localHypExchangeEdgeStack->getElementPtrAtIndex(edge_comm_pos);

#if DEBUG
					cLocalMidNodesData = nullptr;
					cLocalFirstNodeData = nullptr;
#endif
				}
				else
				{
					/*
					 * multi-node related communication information
					 */
					cLocalFirstNodeData = localHypExchangeEdgeStack->getElementPtrAtIndex(edge_comm_pos);
					cLocalMidNodesData = cLocalFirstNodeData+1;

					assert(edge_comm_pos+adjacent_info.edge_comm_elements <= elements_on_hyp_stack);

					if (edge_comm_pos+adjacent_info.edge_comm_elements == elements_on_hyp_stack)
						cLocalLastNodeData = &localCatExchangeEdgeStack->getBackElement();		// first on hyp
					else
						cLocalLastNodeData = cLocalFirstNodeData+adjacent_info.edge_comm_elements;

#if DEBUG
					if (adjacent_info.edge_comm_elements == 1)
						cLocalMidNodesData = nullptr;	// assure no access to mid node data
#endif
				}

//				std::cout << "local: " << cCluster_TreeNode->cCluster_UniqueId << " ||||| adjacent: " << adjacent_info << std::endl;

				p_joinVertexCommDataFromAdjacentCluster(
						cLocalFirstNodeData,
						cLocalMidNodesData,
						cLocalLastNodeData,
						adjacent_info
					);

				edge_comm_pos += adjacent_info.edge_comm_elements;
			}
		}



		/*
		 * start processing data stored on triangle legs:
		 *
		 * H: data stored on hypotenuse stack
		 * C: data stored on catheti
		 *
		 *	 C
		 *	  |\
		 *	 C|  \
		 *	  |    \
		 *	 C|      \
		 *	  |        \
		 *	 C|          \
		 *	  |____________\
		 *	 C  C  C  C  C    H
		 *
		 * shared nodes at the beginning of the corner are stored at the hyp RLE
		 */
		{
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode> > &cat_adjacent_clusters = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters;

			int edge_comm_pos = -1;
			int processed_boundary_RLE = 0;

			for (auto iter = cat_adjacent_clusters.begin(); iter != cat_adjacent_clusters.end(); iter++)
			{
				CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

				// boundary RLE information?
				if (adjacent_info.cCluster_UniqueId.raw_unique_id == 0)
				{
					// boundary dummy vertex data
					assert(adjacent_info.edge_comm_elements == 0);
					assert(processed_boundary_RLE <= 1);

					if (processed_boundary_RLE == 0)
					{
						// FIRST boundary dummy vertex data match
						// the first match can be on the left or right triangle leg!!!

						// nothing to do since we already process the correct position
						if (	(	cCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::ODD &&
									cCluster_TreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY
								)
								||
								(	cCluster_TreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::EVEN &&
									cCluster_TreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY
								)
						)
						{
							// first edge = boundary
							//   => skip edges
							edge_comm_pos = localCatExchangeEdgeStack->getNumberOfElementsOnStack()-1;
							for (auto iter = cat_adjacent_clusters.begin(); iter != cat_adjacent_clusters.end(); iter++)
								edge_comm_pos -= (*iter).edge_comm_elements;
						}
						else
						{
							// go to last data
							edge_comm_pos = localCatExchangeEdgeStack->getNumberOfElementsOnStack()-1;
						}

						processed_boundary_RLE++;
						continue;
					}


					// SECOND boundary dummy vertex data match
					// BOTH edges are of type BOUNDARY!!!

					assert(cCluster_TreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
					assert(cCluster_TreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY);

					// nothing to do since we already process the correct position
					edge_comm_pos = localCatExchangeEdgeStack->getNumberOfElementsOnStack()-1;

#if DEBUG
					processed_boundary_RLE++;
#endif
					continue;
				}

				/*
				 * FIRST node
				 */
				if (edge_comm_pos == -1)
					cLocalFirstNodeData = &localHypExchangeEdgeStack->getFrontElement();		// first on hyp
				else
					cLocalFirstNodeData = &localCatExchangeEdgeStack->getElementAtIndex(edge_comm_pos);

				if (adjacent_info.edge_comm_elements == 0)
				{
					// first element is stored on the hyp stack
					assert(localHypExchangeEdgeStack->getNumberOfElementsOnStack());

					cLocalLastNodeData = cLocalFirstNodeData;
#if DEBUG
					cLocalMidNodesData = nullptr;
					cLocalFirstNodeData = nullptr;
#endif
				}
				else
				{
					/*
					 * multi-node related communication information
					 */
					cLocalMidNodesData = &localCatExchangeEdgeStack->getElementAtIndex(edge_comm_pos+1);

					cLocalLastNodeData = cLocalMidNodesData+adjacent_info.edge_comm_elements-1;

#if DEBUG
					if (adjacent_info.edge_comm_elements == 1)
						cLocalMidNodesData = nullptr;
#endif
				}

				p_joinVertexCommDataFromAdjacentCluster(
						cLocalFirstNodeData,
						cLocalMidNodesData,
						cLocalLastNodeData,
						adjacent_info
					);

				edge_comm_pos += adjacent_info.edge_comm_elements;
			}
		}
	}
};

}

#endif
