/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSTACK_HPP_
#define CSTACK_HPP_

#include <iostream>
#include <cassert>
#include <string.h>

namespace sierpi
{
template <typename T>
class CStack;
}


#include "CStackReaderBottomUp.hpp"
#include "CStackReaderTopDown.hpp"
#include "CStackWriterTopDown.hpp"

namespace sierpi
{

/**
 * \brief generic stack with pop/push accessors
 */
template <typename T>
class CStack
{
	friend class CStackReaderBottomUp<T>;
	friend class CStackReaderTopDown<T>;
	friend class CStackWriterTopDown<T>;

private:

	/**
	 * maximum number of elements which can be stores on the stack
	 */
	size_t size;

	/**
	 * pointer to first (bottom most) element of the stack
	 */
	T *stack_start_ptr;

	/**
	 * pointer to the next element on the stack (e. g. the element which is written in the next push() operation)
	 */
	T *stack_ptr;


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
	CMigrationAtomic<size_t> migration_size;
	CMigrationAtomic<size_t> migration_elements;
	CMigrationAtomicArray<T> migration_stack_start_ptr;
#endif

	size_t migration_elements_tmp;



public:
	inline void migration_send(
			int i_dst_rank
	)
	{
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::sendAtomic(size, i_dst_rank);

		migration_elements_tmp = getNumberOfElementsOnStack();
		sierpi::CMigration::sendAtomic(migration_elements_tmp, i_dst_rank);

		if (migration_elements_tmp > 0)
			sierpi::CMigration::sendAtomicArray(stack_start_ptr, migration_elements_tmp, i_dst_rank);

#else
		migration_elements_tmp = getNumberOfElementsOnStack();

		migration_size.send(size, i_dst_rank);
		migration_elements.send(migration_elements_tmp, i_dst_rank);

		if (migration_elements_tmp > 0)
			migration_stack_start_ptr.send(stack_start_ptr, getNumberOfElementsOnStack(), i_dst_rank);

#endif
	}


	inline void migration_send_postprocessing(
			int i_dst_rank
	) {
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION

		migration_size.wait();
		migration_elements.wait();

		if (migration_elements_tmp > 0)
			migration_stack_start_ptr.wait();
#endif
	}


	inline void migration_recv(
			int i_src_rank
	) {
		size_t s;
		int elements;

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION

		// max stack size
		sierpi::CMigration::recvAtomic(s, i_src_rank);
		resize(s);

		// elements to transfer
		sierpi::CMigration::recvAtomic(s, i_src_rank);

		if (s == 0)
			elements = 0;
		else
			elements = CMigration::recvAtomicArray<T>(stack_start_ptr, size, i_src_rank);

#else

		// max stack size
		migration_size.recv(s, i_src_rank);
		resize(s);

		// elements to transfer
		migration_size.recv(s, i_src_rank);

		if (s == 0)
			elements = 0;
		else
			elements = migration_stack_start_ptr.recv(stack_start_ptr, size, i_src_rank);

#endif

		stack_ptr = stack_start_ptr + elements;
	}

#endif

	/**
	 * constructor
	 */
public:
	inline CStack()
	{
		stack_start_ptr = nullptr;
		stack_ptr = nullptr;
		size = 0;
	}


	/**
	 * deconstructor
	 */
public:
	virtual ~CStack()
	{
		if (stack_start_ptr != nullptr)
			delete[] stack_start_ptr;
	}


	/**
	 * setup a stack with the given size
	 */
public:
	inline CStack(size_t i_initial_size)
	{
		stack_start_ptr = new T[i_initial_size];
		stack_ptr = stack_start_ptr;
		size = i_initial_size;
	}

	/**
	 * returns true if the stack is initialized - otherwise false
	 */
public:
	inline bool isInitialized()
	{
		return (stack_start_ptr != NULL);
	}


	/**
	 * delete the existing elements on the stack and resize it
	 */
public:
	inline void resize(size_t p_size)
	{
		if (stack_start_ptr != nullptr)
				delete[] stack_start_ptr;

		stack_start_ptr = new T[p_size];
		stack_ptr = stack_start_ptr;
		size = p_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		memset(stack_start_ptr, 0, sizeof(T)*size);
#endif
	}


	/**
	 * return the number of bytes allocated by the stack element storage
	 */
public:
	inline size_t getMemSize()	const
	{
		return getMaxNumberOfElements()*sizeof(T);
	}


	/**
	 * return the number of elements currently stored on the stack
	 */
public:
	inline size_t getNumberOfElementsOnStack()	const
	{
		return (size_t)(stack_ptr - stack_start_ptr);
	}



	/**
	 * return the maximum number of elements which can be stored on the stack
	 */
public:
	size_t getMaxNumberOfElements()	const
	{
		return size;
	}


	/**
	 * return the current stack pointer
	 */


public:
	inline T *getStackPtr() const
	{
		return stack_ptr;
	}
	/**
	 * swap with another stack.
	 *
	 * this is done by swapping data pointers with those of the other stack
	 */
public:
	void swap(CStack &stack)
	{
		std::swap(stack_start_ptr, stack.stack_start_ptr);
		std::swap(stack_ptr, stack.stack_ptr);
		std::swap(size, stack.size);
	}

	/**
	 * set the stack element counter and thus the pointer to point to element `i_elementCounter`
	 */
public:
	inline void setStackElementCounter(size_t i_elementCounter)
	{
		stack_ptr = stack_start_ptr+i_elementCounter;
	}



	/**
	 * increment the elementCounter of the stack relatively
	 */
public:
	inline void incStackElementCounter(size_t elementCounter)
	{
		stack_ptr += elementCounter;
	}

	/**
	 * store the element at position `i_pos` to `o_cellData` with a copy operation (avoid this when possible)
	 */
public:
	inline void getElementAtIndex(size_t i_pos, T &o_CellData)	const
	{
		o_CellData = stack_start_ptr[i_pos];
		assert(i_pos < size);
	}

	/**
	 * store the element at position `i_pos` to `o_cellData`
	 */
public:
	inline void setElementAtIndex(size_t i_pos, const T &i_CellData)	const
	{
		stack_start_ptr[i_pos] = i_CellData;
	}

	/**
	 * set all elements to value i_cellData
	 */
public:
	inline void setAllElementsToValue(const T &i_CellData)	const
	{
		for (T *ptr = stack_start_ptr; ptr != stack_ptr; ptr++)
			*ptr = i_CellData;
	}


	/**
	 * set all elements to value i_setCellData when the element data is equal to i_testCellData
	 */
public:
	inline void setAllElementsEqualToValueToValue(
			const T &i_testCellData,	/// element data to test
			const T &i_setCellData	/// element data to set when equal to i_testCellData
	)
	{
		for (T *ptr = stack_start_ptr; ptr != stack_ptr; ptr++)
			if (*ptr == i_testCellData)
				*ptr = i_setCellData;
	}


	/**
	 * return a reference to the element at index i_pos
	 */
public:
	inline T &getElementAtIndex(size_t i_pos)	const
	{
		return stack_start_ptr[i_pos];
	}


	/**
	 * return reference to the first element
	 */
public:
	inline T &getFrontElement()	const
	{
		return *stack_start_ptr;
	}

	/**
	 * return reference the last element
	 */
public:
	inline T &getBackElement()
	{
		return *(stack_ptr-1);
	}

	/**
	 * return a reference to the element at index i_pos
	 */
public:
	inline T &operator[](size_t i_pos) const
	{
		return stack_start_ptr[i_pos];
	}

	/**
	 * return a pointer to the element at index i_pos
	 */
public:
	inline T *getElementPtrAtIndex(size_t i_pos)	const
	{
		return &(stack_start_ptr[i_pos]);
	}


	/**
	 * get pointer to element on the top of the stack without modifying the stack
	 */
public:
	inline T *getTopElementPtr()	const
	{
		return (stack_ptr-1);
	}

	/**
	 * get the start pointer of the stack
	 */
	inline T *getStartPtr()	const
	{
		return stack_start_ptr;
	}

	/**
	 * get reference to element on the top of the stack without modifying the stack
	 */
public:
	inline T &getTopElementRef()	const
	{
		return *(stack_ptr-1);
	}



	/**
	 * return the number of elements for which are equal to the given comparison value `i_value`
	 */
public:
	inline size_t getStackElementCounterEquals(const T &i_value)	const
	{
		size_t ret_val = 0;

		for (T *ptr = stack_start_ptr; ptr != stack_ptr; ptr++)
			if (i_value == *ptr)
				ret_val++;

		return ret_val;
	}


	/**
	 * clear all stack elements by moving the stack pointer to the very beginning
	 */
public:
	inline void clear()
	{
		stack_ptr = stack_start_ptr;
	}


	/**
	 * return true, if the stack is empty
	 */
public:
	inline bool isEmpty()	const
	{
		return stack_ptr == stack_start_ptr;
	}



	/*************************************************************************************
	 * PUSH OPERATIONS
	 *************************************************************************************/
#if DEBUG
	#define ASSERT_AFTER_PUSH()	{	assert(stack_ptr != stack_start_ptr+size+1);	}
#else
	#define ASSERT_AFTER_PUSH()
#endif

	/**
	 * push the value given by v to the stack
	 */
	inline void push(const T &v)
	{
		*stack_ptr = v;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push a value to the stack which is to by 'v'
	 */
	inline void push_PtrValue(T *v)
	{
		*stack_ptr = *v;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * act like pushing some data on the stack but don't do any push operation.
	 * \return	pointer to new stack element
	 */
	inline T *fakePush_returnPtr()
	{
#if DEBUG
		stack_ptr++;
		ASSERT_AFTER_PUSH();
		stack_ptr--;
#endif
		return stack_ptr++;
	}


	/**
	 * act like pushing some data on the stack but don't do any push operation.
	 * \return	reference to new stack element
	 */
	inline T &fakePush_returnRef()
	{
#if DEBUG
		stack_ptr++;
		ASSERT_AFTER_PUSH();
		stack_ptr--;
#endif
		return *(stack_ptr++);
	}


	/**
	 * push 2 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push 3 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push 4 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push 5 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push 6 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4, T v5)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}


	/**
	 * push 7 values to the stack in the order left-right
	 */
	inline void push(T v0, T v1, T v2, T v3, T v4, T v5, T v6)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;
		*stack_ptr = v6;
		stack_ptr++;

		ASSERT_AFTER_PUSH();
	}
#undef ASSERT_AFTER_PUSH



	/*************************************************************************************
	 * POP OPERATIONS
	 *************************************************************************************/
#define ASSERT_AFTER_POP()	{assert(stack_ptr >= stack_start_ptr);}
	inline T &pop()
	{
		stack_ptr--;

		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T &pop2()
	{
		stack_ptr -= 2;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T &pop3()
	{
		stack_ptr -= 3;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T &pop4()
	{
		stack_ptr -= 4;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T &pop5()
	{
		stack_ptr -= 5;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	inline T &pop6()
	{
		stack_ptr -= 6;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}

	/*
	 * do a pop operation on stack and return the pointer instead
	 * of the stack element
	 */
	inline T* pop_returnPtr()
	{
		stack_ptr--;
		ASSERT_AFTER_POP();
		return stack_ptr;
	}


	inline T& pop_returnRef()
	{
		stack_ptr--;
		ASSERT_AFTER_POP();
		return *stack_ptr;
	}
#undef ASSERT_AFTER_POP


/*********************************************************************************
 * QUERIES
 *********************************************************************************/


	/**
	 * return true, whenever any element is not equal to the given value
	 */
	inline bool query_anyElementNotEqualTo_Value(T v)	const
	{
		size_t c = getNumberOfElementsOnStack();
		for (size_t i = 0; i < c; i++)
			if (stack_start_ptr[i] != v)
				return true;
		return false;
	}

	/**
	 * return true, whenever any element is not equal to the given value
	 */
	inline bool query_anyElementEqualTo_Value(T v)	const
	{
		size_t c = getNumberOfElementsOnStack();
		for (size_t i = 0; i < c; i++)
			if (stack_start_ptr[i] == v)
				return true;
		return false;
	}


/*********************************************************************************
 * COPY
 *********************************************************************************/


	/**
	 * copy element data to other stack
	 */
	inline void copyStackElements(
			CStack<T> *o_dst_stack			///< destination stack
	)	const
	{
		assert(o_dst_stack->getMaxNumberOfElements() >= getNumberOfElementsOnStack());

		memcpy(o_dst_stack->stack_start_ptr, stack_start_ptr, sizeof(T)*getNumberOfElementsOnStack());

		// update counter
		o_dst_stack->setStackElementCounter(getNumberOfElementsOnStack());
	}




/*********************************************************************************
 * STRUCTURE
 *********************************************************************************/
	/**
	 * structure specific method
	 *
	 * return the number of leaf elements assuming that the structure stack is stored for a quad
	 */
	inline bool structure_isValidQuad()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = stack_start_ptr; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+2 == zeros;
	}


	/**
	 * structure specific method
	 *
	 * return the number of leaf elements assuming that the structure stack is stored for a triangle
	 */
	inline bool structure_isValidTriangle()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = stack_start_ptr; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+1 == zeros;
	}

	/**
	 * return the number of triangles on the leafes.
	 *
	 * This can be computed by the total number of stored bits in the structure stack:
	 *
	 * When a triangle is refined, a '0' is replaced by '100'.
	 * Assuming that the domains border is 'triangle-like', we get the following series:
	 *   # triangles: 1 2 3
	 *       # zeros: 2 3 4
	 *        # ones: 1 2 3
	 * sizeof(stack): 3 5 7
	 *
	 * for this situation, the number of leaf triangles (zeros) would be:
	 *   (sizeof(stack)+1)/2
	 *
	 * for 2 triangles setting up a rectangular domain, we get:
	 *   (sizeof(stack)+2)/2
	 *
	 * since the first refinement of the rectangle isn't stored explicitly
	 * (as it doesn't make sense), a further fix has to be done:
	 *   #leaf triangles = (sizeof(stack)+3)/2
	 */
	inline unsigned int structure_getNumberOfTrianglesInQuad()	const
	{
		return (getNumberOfElementsOnStack()+3) / 2;
	}

	inline unsigned int structure_getNumberOfTrianglesInTriangle()	const
	{
		return (getNumberOfElementsOnStack()+1) / 2;
	}


	/**
	 * raw push a chunk of another stack to this stack
	 *
	 * the order of the elements on the stack is not reversed!
	 *
	 * _____ ____ max elements of stack
	 * |   |
	 * |   |
	 * |   |
	 * |   | ____ stack_ptr -> pointing to next, empty stack element
	 * | d |
	 * | w |
	 * | d |
	 * |_a_| ____ data -> pointing to bottom of stack
	 */
	void pushChunksFrom(
			const CStack<T> &i_src_stack,			///< source stack to copy data from
			size_t i_src_start,						///< start element in the source stack
			size_t i_number_of_elements_to_copy		///< number of elements to be copied
	)
	{
		assert((signed long long)i_src_stack.getNumberOfElementsOnStack() - ((signed long long)i_number_of_elements_to_copy + (signed long long)i_src_start) >= 0);
		assert(getNumberOfElementsOnStack() + i_number_of_elements_to_copy <= getMaxNumberOfElements());

#if 0

		T *src = i_src_stack.stack_ptr - i_number_of_elements_to_copy - i_src_start;

		for (size_t i = 0; i < i_number_of_elements_to_copy; i++)
		{
			*stack_ptr = *src;
			stack_ptr++;
			src++;
		}

#else

		memcpy(	stack_ptr,						// dst data
				i_src_stack.stack_ptr - i_number_of_elements_to_copy - i_src_start,	// src pointer
				i_number_of_elements_to_copy*sizeof(T));			// size of chunk

		stack_ptr += i_number_of_elements_to_copy;

#endif
	}

#undef ASSERT_AFTER_PUSH
};


inline
::std::ostream&
operator<<(::std::ostream &co, sierpi::CStack<char> &stack)
{
	for (int i = stack.getNumberOfElementsOnStack()-1; i >= 0 ; i--)
		co << (int)stack.getElementAtIndex(i);
	co << "|";

	return co;
}

inline
::std::ostream&
operator<<(::std::ostream &co, sierpi::CStack<unsigned char> &stack)
{
	for (int i = stack.getNumberOfElementsOnStack()-1; i >= 0 ; i--)
		co << (int)stack.getElementAtIndex(i);
	co << "|";

	return co;
}

}

template <class T>
inline
::std::ostream&
operator<<(::std::ostream &co, sierpi::CStack<T> &stack)
{
	for (int i = stack.getNumberOfElementsOnStack()-1; i >= 0 ; i--)
		co << stack.getElementAtIndex(i) << ", ";
	co << "|";

	return co;
}




#endif /* CSTACK_H_ */
