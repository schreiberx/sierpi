/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CStackWriterTopDown.hpp
 *
 *  Created on: Mar 29, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CSTACK_WRITER_TOP_DOWN_HPP_
#define CSTACK_WRITER_TOP_DOWN_HPP_

namespace sierpi
{

template <typename T>
class CStackWriterTopDown;

}

#include "CStack.hpp"

namespace sierpi
{

/**
 * \brief stack writer for reversed stack writing:
 * top-down with a particular starting stack position
 */
template <typename T>
class CStackWriterTopDown
{
	friend class CStack<T>;

public:
	CStack<T> *stack;
	T *stack_ptr;

#if DEBUG
	#define ASSERT_AFTER_PUSH()	{	assert(stack_ptr >= stack->stack_start_ptr);	}
#else
	#define ASSERT_AFTER_PUSH()
#endif


	/**
	 * setup top-down writer
	 */
	inline void setup(
		CStack<T> *p_stack,		///< stack to apply top-down handling for
		size_t ptr_position		///< position of stack pointer
	)
	{
		stack = p_stack;
		stack_ptr = stack->stack_start_ptr+ptr_position;

		assert(stack->size >= ptr_position);
	}



	/**
	 * push the value given by v to the stack
	 */
	inline void push(const T &i_data)
	{
		stack_ptr--;
		*stack_ptr = i_data;

		ASSERT_AFTER_PUSH();
	}



	/**
	 * return true if access pointer is at bottom of stack
	 */
	inline bool isAtBottom()
	{
		return (stack_ptr == stack->stack_start_ptr);
	}


#undef ASSERT_AFTER_PUSH
};

}

#endif
