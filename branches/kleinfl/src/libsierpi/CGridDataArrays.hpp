/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CGridDataArrays.hpp
 *
 *  Created on: Jul 6, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGRIDDATAARRAYS_HPP_
#define CGRIDDATAARRAYS_HPP_


#include "mainthreading/CMainThreading_Locks.hpp"


class CGridDataArrays_Enums
{
public:
	enum EFLAGS
	{
		VERTICES	= (1 << 0),
		NORMALS		= (1 << 1),

		VALUE0	= (1 << 2),
		VALUE1	= (1 << 3),
		VALUE2	= (1 << 4),
		VALUE3	= (1 << 5),
		VALUE4 	= (1 << 6),
		VALUE5 	= (1 << 7),
		VALUE6 	= (1 << 8),
		VALUE7 	= (1 << 9),
		VALUE8 	= (1 << 10),

		ALL			= 0xffffff
	};
};



template <
	int t_vertices_per_cell,	///< vertices to be stored per cell (3 for 2D, 2 for 1D)
	int t_max_dof_variables		///< maximum degree of freedom variables
>
class CGridDataArrays
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * buffer for vertices
	 */
	T *triangle_vertex_buffer;

	/**
	 * buffer for normals
	 */
	T *triangle_normal_buffer;

	/**
	 * buffer to store degree of freedom arrays
	 */
	T *dof_element[t_max_dof_variables];

	/**
	 * number of overall triangle cells
	 */
	long long number_of_triangle_cells;

	/**
	 * next free cell id to be used during next request
	 */
	size_t next_free_cell_id;

	/**
	 * lock for mutual exclusive access
	 */
	CMainThreading_Lock lock;

	/**
	 * variables per dof
	 */
	size_t dof_variables_multiplier;


	/**
	 * constructor for simulation data storage
	 */
	CGridDataArrays(
		size_t i_number_of_triangle_cells = 1,		///< number of triangle cells to write data for
		size_t i_vertex_multiplier = 1,				///< multiplier for vertex data. 2 for 1D (lines), 3 for 2D (triangles), 3 for 3D (triangles=3)
		size_t i_dof_variables_multiplier = 1,		///< variables per DOF
		int i_flags = 0								///< flags for field to allocate
	)	{
		triangle_vertex_buffer = nullptr;
		triangle_normal_buffer = nullptr;

		for (int i = 0; i < t_max_dof_variables; i++)
			dof_element[i] = nullptr;

		reset(i_number_of_triangle_cells, i_vertex_multiplier, i_dof_variables_multiplier, i_flags);

	}



	/**
	 * resize buffers to given size
	 */
	void reset(
			size_t i_number_of_triangle_cells,			///< number of triangle cells to write data for
			size_t i_vertex_multiplier = 1,				///< multiplier for vertex data. 2 for 1D (lines), 3 for 2D (triangles), 3 for 3D (triangles=3)
			size_t i_dof_variables_multiplier = 1,		///< variables per DOF
			int i_flags = CGridDataArrays_Enums::ALL	///< flags for field to allocate
	)	{
		number_of_triangle_cells = i_number_of_triangle_cells;
		next_free_cell_id = 0;
		dof_variables_multiplier = i_dof_variables_multiplier;

		// clear buffers
		clear();

		if (i_flags & CGridDataArrays_Enums::VERTICES)
			triangle_vertex_buffer = new T[number_of_triangle_cells*t_vertices_per_cell*3*i_vertex_multiplier];

		if (i_flags & CGridDataArrays_Enums::NORMALS)
			triangle_normal_buffer = new T[number_of_triangle_cells*t_vertices_per_cell*3*i_vertex_multiplier];


		int f = CGridDataArrays_Enums::VALUE0;

		for (int i = 0; i < t_max_dof_variables; i++)
		{
			if (i_flags & f)
				dof_element[i] = new T[number_of_triangle_cells*i_dof_variables_multiplier];
			else
				dof_element[i] = nullptr;

			f <<= 1;
		}
	}



	/**
	 * get next cell start index for given number of grid-cells
	 */
	size_t getNextTriangleCellStartId(
			size_t i_number_of_cells
	)
	{
		lock.lock();

		size_t n = next_free_cell_id;
		next_free_cell_id += i_number_of_cells;

		lock.unlock();

		assert(next_free_cell_id <= (size_t)number_of_triangle_cells);

		return n;
	}



	/**
	 * cleanup
	 */
	void clear()
	{
		if (triangle_vertex_buffer != nullptr)
		{
			delete [] triangle_vertex_buffer;
			triangle_vertex_buffer = nullptr;
		}

		if (triangle_normal_buffer != nullptr)
		{
			delete [] triangle_normal_buffer;
			triangle_normal_buffer = nullptr;
		}

		for (int i = 0; i < t_max_dof_variables; i++)
		{
			if (dof_element[i])
			{
				delete [] dof_element[i];
				dof_element[i] = nullptr;
			}
		}
	}


	/**
	 * deconstructor
	 */
	virtual ~CGridDataArrays()
	{
		clear();
	}
};


#endif /* CSIMULATIONTSUNAMI_PARALLEL_FILEOUTPUT_BLOCK_HPP_ */
