/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CGeneric_TreeNode_tbb.hpp
 *
 *  Created on: Sep 25, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CGENERICTREENODE_TBB_HPP_
#define CGENERICTREENODE_TBB_HPP_

#define CGENERICTREENODE_SERIAL_SERIAL_CODE_ONLY	1
#include "CGeneric_TreeNode_serial.hpp"





#if CONFIG_ENABLE_SCAN_DATA

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! assertions for scan workload distributions !!!!!!!!!!!!!!!!!!!!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
inline void scan_omp_asserts()
{
#if DEBUG
	assert(workload_thread_id_start != -1);
	assert(workload_thread_id_start == 0);

	assert(workload_thread_id_end != -1);
	assert(workload_thread_id_end >= 0);
#endif
}

#endif



/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! THIS FILE IS INCLUDED DIRECTLY TO THE CGeneric_TreeNode Class !!!
 * !!! Therefore the following variables are members of the class   !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

inline void specializedConstructorMethod()
{
}


/****************************************************************************
 * GENERIC TREE NODE TRAVERSAL
 */

/*
 * parallel
 */
template <
	typename CLambdaFun
>
class CTraversalTask_GenericTreeNode_Parallel
	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun lambda_leaves;

public:
	CTraversalTask_GenericTreeNode_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun i_lambda_leaves
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		// thread_id becomes 0 when not initialized
		// for TBB this means, that there's no affinity set.
		// this is the case during initialization only.
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->second_child_node, lambda_leaves);
			spawn(t2);

			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->first_child_node, lambda_leaves);
			spawn_and_wait_for_all(t1);

			return nullptr;
		}


		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->first_child_node, lambda_leaves);
			t1.execute();
			return nullptr;
		}


#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this_node->second_child_node, lambda_leaves);
			t2.execute();
		}
		return nullptr;
	}
};



template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel(
		CLambdaFun p_lambda_leaves
)
{
	tbb::task::spawn_root_and_wait((*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Parallel<CLambdaFun>(this, p_lambda_leaves)));
}





#if CONFIG_ENABLE_SCAN_THREADING

/*
 * parallel scan
 */
template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda_leaves
)
{
	class CTraversalTask_GenericTreeNode_Parallel_Scan
	{
	public:
		static void traverse(
				int i_thread_id,
				CGeneric_TreeNode_ *i_this_node,
				CLambdaFun i_lambda_leaf
		)
		{
			assert(i_this_node->workload_thread_id_start >= 0 && i_this_node->workload_thread_id_end >= 0);

			if (i_this_node->workload_thread_id_start > i_thread_id || i_this_node->workload_thread_id_end < i_thread_id)
				return;

			if (i_this_node->isLeaf())
			{
				if (i_thread_id != i_this_node->workload_thread_id)
					return;

				i_lambda_leaf(i_this_node);
				return;
			}


			if (i_this_node->first_child_node)
			{
				traverse(i_thread_id, i_this_node->first_child_node, i_lambda_leaf);
			}

			if (i_this_node->second_child_node)
			{
				traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaf);
			}
		}
	};

	scan_omp_asserts();


	tbb::parallel_for(
		0, workload_thread_id_end+1, 1,
		[this,&i_lambda_leaves](int i)
		{
			CTraversalTask_GenericTreeNode_Parallel_Scan::traverse(i, this, i_lambda_leaves);
		}
	);
}


#else


template <typename CLambdaFun>
inline void traverse_GenericTreeNode_Parallel_Scan(
		CLambdaFun i_lambda
)
{
	traverse_GenericTreeNode_Parallel(i_lambda);
}


#endif





/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES, MID nodes in PRE- and POSTORDER
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
>
class CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel
	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes_preorder;
	CLambdaFun3 lambda_midnodes_postorder;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes_preorder,
			CLambdaFun3 i_lambda_midnodes_postorder
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes_preorder(i_lambda_midnodes_preorder),
		lambda_midnodes_postorder(i_lambda_midnodes_postorder)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	tbb::task* execute()
	{
		if (!this_node->isLeaf())
		{
			lambda_midnodes_preorder(this_node);
		}
		else
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			spawn_and_wait_for_all(t1);

			lambda_midnodes_postorder(this_node);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->first_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			t1.execute();

			lambda_midnodes_postorder(this_node);

			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this_node->second_child_node, lambda_leaves, lambda_midnodes_preorder, lambda_midnodes_postorder);
			t2.execute();

			lambda_midnodes_postorder(this_node);

			return nullptr;
		}
		return nullptr;
	}
};



template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename CLambdaFun3
	>
inline void traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes_preorder,
		CLambdaFun3 i_lambda_midnodes_postorder
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2, CLambdaFun3>(this, i_lambda_leaves, i_lambda_midnodes_preorder, i_lambda_midnodes_postorder));
}




/****************************************************************************
 * GENERIC TREE NODE (NO REDUCE) MID AND LEAF NODES
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel
	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node);
			return 0;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes);
			spawn_and_wait_for_all(t1);

			lambda_midnodes(this_node);

			return nullptr;
		}

		if (this_node->first_child_node != nullptr)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes);
			t1.execute();

			lambda_midnodes(this_node);

			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes);
			t2.execute();

			lambda_midnodes(this_node);

			return nullptr;
		}
		return nullptr;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes));
}



/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH (PREORDER)
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel
	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int p_genericTreeDepth
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(p_genericTreeDepth)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_midnodes(this_node, genericTreeDepth);
		}
		else
		{
			lambda_leaves(this_node, genericTreeDepth);
			return 0;
		}

		genericTreeDepth++;

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn_and_wait_for_all(t1);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t1.execute();

			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t2.execute();

			return nullptr;
		}
		return nullptr;
	}
};


template <typename CLambdaFun1, typename CLambdaFun2>
inline void traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));
}




/****************************************************************************
 * GENERIC TREE NODE AND DEPTH FOR ALL MID AND LEAF NODES + DEPTH
 */

/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel
	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	int genericTreeDepth;

public:
	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			int i_generic_tree_depth
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		genericTreeDepth(i_generic_tree_depth)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	tbb::task* execute()
	{
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, genericTreeDepth);
			return 0;
		}

		genericTreeDepth++;

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			spawn_and_wait_for_all(t1);

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->first_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t1.execute();

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this_node->second_child_node, lambda_leaves, lambda_midnodes, genericTreeDepth);
			t2.execute();

			lambda_midnodes(this_node, genericTreeDepth-1);
			return nullptr;
		}

		return nullptr;
	}
};


template <
	typename CLambdaFun1,
	typename CLambdaFun2
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Depth_Parallel<CLambdaFun1, CLambdaFun2>(this, i_lambda_leaves, i_lambda_midnodes, 0));

	if (!this->isLeaf())
		i_lambda_midnodes(this, 0);
}






/****************************************************************************
 * GENERIC TREE NODE (WITH REDUCE)
 ****************************************************************************/
 
/*
 * parallel
 */
template <
	typename CLambdaFun,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_Reduce_Parallel	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun lambda;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_Reduce_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun i_lambda,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_rootReduceValue
	)	:
		this_node(i_this_node),
		lambda(i_lambda),
		reduceOperator(i_reduceOperator),
		rootReduceValue(o_rootReduceValue)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda(this_node, rootReduceValue);

			return nullptr;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, &reduceValueSecond);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, &reduceValueFirst);
			spawn_and_wait_for_all(t1);

			reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);
			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->first_child_node, lambda, reduceOperator, rootReduceValue);
			t1.execute();

			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this_node->second_child_node, lambda, reduceOperator, rootReduceValue);
			t2.execute();

			return nullptr;
		}
		return nullptr;
	}
};


template <
	typename CLambdaFun,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_Reduce_Parallel(
		CLambdaFun i_lambda,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_Reduce_Parallel<CLambdaFun, TReduceValue>(this, i_lambda, i_reduceOperator, o_reduceOutput));
}



#if CONFIG_ENABLE_SCAN_THREADING

template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda_leaves,
		void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduce_output
)
{
	class CTraversalTask_GenericTreeNode_Reduce_Parallel_Scan
	{
	public:
		static bool traverse(
				int i_thread_id,
				CGeneric_TreeNode_ *i_this_node,
				CLambdaFun i_lambda_leaf,
				void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
				TReduceValue *o_reduce_output
		) {
			assert(i_this_node->workload_thread_id_start >= 0 && i_this_node->workload_thread_id_end >= 0);

			if (i_this_node->workload_thread_id_start > i_thread_id || i_this_node->workload_thread_id_end < i_thread_id)
				return false;


			/*
			 * LEAF COMPUTATION
			 */
			if (i_this_node->isLeaf())
			{
				if (i_thread_id != i_this_node->workload_thread_id)
					return false;

				i_lambda_leaf(i_this_node, o_reduce_output);
				return true;
			}


			TReduceValue firstValue;
			TReduceValue secondValue;

			/*
			 * run traversal on first child
			 */
			if (i_this_node->first_child_node)
			{
				if (!traverse(i_thread_id, i_this_node->first_child_node, i_lambda_leaf, i_reduce_operator, &firstValue))
				{
					// return value of 1st child is not valid -> only use return value of 2nd child

					if (!i_this_node->second_child_node)
						return false;

					return traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaf, i_reduce_operator, o_reduce_output);
				}

#if CONFIG_ENABLE_MPI
				if (i_this_node->second_child_node)
				{
#endif
					// 1st child was not successful => try to traverse 2nd child
					assert(i_this_node->second_child_node);

					if (!traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaf, i_reduce_operator, &secondValue))
					{
						// 2nd child with invalid data => use data of first child

						*o_reduce_output = firstValue;
						return true;
					}

					// both reduce values have been written
					i_reduce_operator(firstValue, secondValue, o_reduce_output);
					return true;
#if CONFIG_ENABLE_MPI
				}
#endif
				// only store first value
				*o_reduce_output = firstValue;

				return true;
			}


			/*
			 * first child does not exist => only run traversal on 2nd child
			 */
#if CONFIG_ENABLE_MPI
			if (i_this_node->second_child_node)
			{
#endif
				assert(i_this_node->second_child_node);
				return traverse(i_thread_id, i_this_node->second_child_node, i_lambda_leaf, i_reduce_operator, o_reduce_output);

#if CONFIG_ENABLE_MPI
			}
#endif
			return false;

		}
	};

	scan_omp_asserts();

	TReduceValue reduce_values[workload_thread_id_end+1];
	TReduceValue *r = reduce_values;
	TReduceValue initial_reduce_output = *o_reduce_output;

	tbb::parallel_for(
		0, workload_thread_id_end+1, 1,
		[this,&i_lambda_leaves,&i_reduce_operator,&r,&initial_reduce_output](int i)
		{
			r[i] = initial_reduce_output;
			CTraversalTask_GenericTreeNode_Reduce_Parallel_Scan::traverse(i, this, i_lambda_leaves, i_reduce_operator, &(r[i]));
		}
	);

	TReduceValue reduce_values_output[workload_thread_id_end+1];

	reduce_values_output[0] = reduce_values[0];
	for (int i = 1; i <= workload_thread_id_end; i++)
	{
		i_reduce_operator(reduce_values_output[i-1], reduce_values[i], &reduce_values_output[i]);
	}

	*o_reduce_output = reduce_values_output[workload_thread_id_end];
}

#else


template <typename CLambdaFun, typename TReduceValue>
inline void traverse_GenericTreeNode_Reduce_Parallel_Scan(
		CLambdaFun i_lambda_leaves,
		void (*i_reduce_operator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduce_output
)
{
	traverse_GenericTreeNode_Reduce_Parallel(i_lambda_leaves, i_reduce_operator, o_reduce_output);
}

#endif



/****************************************************************************
 * GENERIC TREE NODE LeafAndPostorderMidNodes (WITH REDUCE)
 ****************************************************************************/
/*
 * parallel
 */
template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
class CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel	: public tbb::task
{
	CGeneric_TreeNode_ *this_node;
	CLambdaFun1 lambda_leaves;
	CLambdaFun2 lambda_midnodes;
	void (*reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o);
	TReduceValue *rootReduceValue;

public:
	TReduceValue reduceValueFirst;
	TReduceValue reduceValueSecond;

	CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
			CGeneric_TreeNode_ *i_this_node,
			CLambdaFun1 i_lambda_leaves,
			CLambdaFun2 i_lambda_midnodes,
			void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
			TReduceValue *o_rootReduceValue
	)	:
		this_node(i_this_node),
		lambda_leaves(i_lambda_leaves),
		lambda_midnodes(i_lambda_midnodes),
		reduceOperator(i_reduceOperator),
		rootReduceValue(o_rootReduceValue)
	{
#if CONFIG_ENABLE_SCAN_THREADING && CONFIG_TBB_TASK_AFFINITIES
//		assert(i_this_node->workload_thread_id != -1);
		assert(i_this_node->workload_thread_id < tbb::task_scheduler_init::default_num_threads());
		set_affinity(i_this_node->workload_thread_id+1);
#endif
	}


	/**
	 * TASK
	 */
	task* execute()
	{
		/**
		 * LEAF COMPUTATION
		 */
		if (this_node->isLeaf())
		{
			lambda_leaves(this_node, rootReduceValue);
			return nullptr;
		}

		if (this_node->first_child_node != nullptr && this_node->second_child_node != nullptr)
		{
			set_ref_count(3);

			tbb::task &t2 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueSecond);
			spawn(t2);

			tbb::task &t1 = *new( allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator, &reduceValueFirst);
			spawn_and_wait_for_all(t1);

			reduceOperator(reduceValueFirst, reduceValueSecond, rootReduceValue);

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
		}

		if (this_node->first_child_node)
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t1 = *new(allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->first_child_node, lambda_leaves, lambda_midnodes, reduceOperator, rootReduceValue);
			t1.execute();

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
		}

#if !CONFIG_ENABLE_MPI
		assert(this_node->second_child_node);
#else
		if (this_node->second_child_node)
#endif
		{
			// !!! do the tree-traversal using allocate_child to inherit the tbb tasking information to the class
			// the allocated child seems to be automagically freed
			tbb::task &t2 = *new(allocate_child()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this_node->second_child_node, lambda_leaves, lambda_midnodes, reduceOperator, rootReduceValue);
			t2.execute();

			lambda_midnodes(this_node, rootReduceValue);

			return nullptr;
		}

		return nullptr;
	}
};


template <
	typename CLambdaFun1,
	typename CLambdaFun2,
	typename TReduceValue
>
inline void traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
		CLambdaFun1 i_lambda_leaves,
		CLambdaFun2 i_lambda_midnodes,
		void (*i_reduceOperator)(const TReduceValue &a, const TReduceValue &b, TReduceValue *o),
		TReduceValue *o_reduceOutput
)
{
	tbb::task::spawn_root_and_wait(*new(tbb::task::allocate_root()) CTraversalTask_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel<CLambdaFun1, CLambdaFun2, TReduceValue>(this, i_lambda_leaves, i_lambda_midnodes, i_reduceOperator, o_reduceOutput));

	if (!this->isLeaf())
		i_lambda_midnodes(this, o_reduceOutput);
}




#endif
