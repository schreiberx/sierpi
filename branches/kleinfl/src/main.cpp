/*
 * Copyright (C) 2010 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#if CONFIG_ENABLE_MPI
	// mpi.h has to be included before stdlib
	#include <mpi.h>
#endif

#include <stdlib.h>
#include <stdint.h>
#include <sstream>
#include <iostream>
#include <string>

//#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "lib/CProcessMemoryInformation.hpp"



#if CONFIG_SIERPI_ENABLE_GUI
	#include "CMainGui.hpp"
#else
	#include "CMain.hpp"
#endif


#if CONFIG_ENABLE_LIBXML
	#include <libxml/parser.h>
#endif


int run(
		int argc,
		char *argv[]
) {
	std::string exit_status_string = "unexpected exit";

//	bool shutdown_in_progress = false;

#if CONFIG_SIERPI_ENABLE_GUI
	CMainGui cMain(argc, argv);
#else
	CMain cMain(argc, argv);
#endif

	try
	{
		// setup MPI
		cMain.mpi_setup();

		// create the simulation
		cMain.simulation_create();

		if (!cMain.setupProgramParameters())
			return -1;

		// setup threading tools
		cMain.threading_setup();

		// setup the simulation
		cMain.simulation_setup();

		// output some useful information
		cMain.outputVerboseInformation();

#if CONFIG_SIERPI_ENABLE_GUI

		// setup gui stuff AFTER setting up the simulation
		cMain.setupGui();

		// run the simulation loop
		cMain.run();

#else

		// simulation loop prefix
		cMain.simulation_loopPrefix();

		// run the simulation
		cMain.threading_simulationLoop();

		// simulation loop suffix
		cMain.simulation_loopSuffix();
#endif

//		shutdown_in_progress = true;

		// shutdown the simulation
		cMain.simulation_shutdown();

		// shutdown threading tools
		cMain.threading_shutdown();

		// shutdown MPI
		cMain.mpi_shutdown();

		exit_status_string = "OK";
	}
	catch(std::exception &e)
	{
		exit_status_string = e.what();
		std::cout << "EXIT STATUS: " << exit_status_string << std::endl;

//		if (!shutdown_in_progress)
//			cMain.simulation_shutdown();

		cMain.simulation_exception_output();

		CProcessMemoryInformation cProcessMemoryInformation;
		cProcessMemoryInformation.outputUsageInformation();

		std::cout << std::endl;
		return -1;
	}

#if !CONFIG_ENABLE_MPI
	std::cout << "EXIT STATUS: " << exit_status_string << std::endl;
#endif

	return 0;
}


/**
 * main() function
 */
int main(
		int argc,
		char *argv[]
)
{
#if CONFIG_ENABLE_LIBXML
	xmlInitParser();
#endif

	int retval = run(argc, argv);

#if CONFIG_ENABLE_LIBXML
	xmlCleanupParser();
#endif

	return retval;
}
