#! /bin/bash
cd /home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/../../

source ./inc_vars.sh

export OMP_NUM_THREADS=4

export KMP_AFFINITIES=compact

# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

echo "./build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c ./scenarios/solitary_wave_on_composite_beach_1d_bouy_sampling.xml -d 12 -a 0 -r 0/0 -n 4 -o 1024 -A 1"
./build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c ./scenarios/solitary_wave_on_composite_beach_1d_bouy_sampling.xml -d 12 -a 0 -r 0/0 -n 4 -o 1024 -A 1 > /home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0.txt

mv "g5_0_0.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g5_0_0.csv"
mv "g6_2_18.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g6_2_18.csv"
mv "g7_4_36.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g7_4_36.csv"
mv "g8_5_82.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g8_5_82.csv"
mv "g9_7_29.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g9_7_29.csv"
mv "g10_7_76.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swocb_1d/2013_02_15//sierpi_intel_omp_tsunami_1d_netcdf_b0_release_d12_a00_s001024_t004_r0-0_g10_7_76.csv"
