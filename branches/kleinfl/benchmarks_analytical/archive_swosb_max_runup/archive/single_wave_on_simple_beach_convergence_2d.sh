#! /bin/bash

cd ..

scons --xml-config=./scenarios/single_wave_on_simple_beach_2d_strip.xml -j 8 || exit 1

for d in `seq 0 20`; do
	MAX=`./build/sierpi_intel_omp_hyperbolic_parallel_tsunami_netcdf_b0_release -c scenarios/single_wave_on_simple_beach_2d_strip.xml -d $d -a 0 | grep "MaxRunup:" | tail -n 1`
	echo "$d	$MAX"
done
