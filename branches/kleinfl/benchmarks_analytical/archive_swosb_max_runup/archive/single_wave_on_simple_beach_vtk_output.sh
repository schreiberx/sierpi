#! /bin/bash

cd ..

rm -f paraview/output_swosb_*

#
# the 2D simulation is initialized with 8-times domain size
# this means, that each simulation has the 4-fold simulation discretization
# => multiply 1D discretization depth with 4
#
# THIS DEPTH IS AUTOMATICALLY COMPUTED IN THE 1D SIMULATION BASED ON THE 2D SIMULATION DEPTH!!!
#

PARAMS="-v 3"
#PARAMS="$PARAMS -d 10"

scons --xml-config=./scenarios/single_wave_on_simple_beach_2d_strip.xml
scons --xml-config=./scenarios/single_wave_on_simple_beach_1d.xml


./build/sierpi_intel_omp_hyperbolic_parallel_tsunami_netcdf_b0_release -c scenarios/single_wave_on_simple_beach_2d_strip.xml $PARAMS
./build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c scenarios/single_wave_on_simple_beach_1d.xml $PARAMS
