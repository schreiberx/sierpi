#! /bin/bash

. inc_vars.sh

cd ../..

scons --xml-config=./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml --hyperbolic-adaptivity-mode=1 -j 4 $@ || exit -1
scons --xml-config=./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml --hyperbolic-adaptivity-mode=2 -j 4 $@ || exit -1
scons --xml-config=./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml --hyperbolic-adaptivity-mode=3 --enable-augumented-riemann-eigen-coefficients=on -j 4 $@ || exit -1
