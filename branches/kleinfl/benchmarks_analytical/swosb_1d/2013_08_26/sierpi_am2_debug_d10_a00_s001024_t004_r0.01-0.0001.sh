#! /bin/bash
cd /home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d

source ./inc_vars.sh

export OMP_NUM_THREADS=4

export KMP_AFFINITIES=compact

# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

echo "../../build/sierpi_intel_omp_tsunami_1d_netcdf_type0_degree0_am2_debug -c ../../scenarios/single_wave_on_simple_beach_1d_bouy_sampling.xml -d 10 -a 0 -r 0.01/0.0001 -n 4 -o 1024 -A 1"
../../build/sierpi_intel_omp_tsunami_1d_netcdf_type0_degree0_am2_debug -c ../../scenarios/single_wave_on_simple_beach_1d_bouy_sampling.xml -d 10 -a 0 -r 0.01/0.0001 -n 4 -o 1024 -A 1 > /home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d/2013_08_26//sierpi_am2_debug_d10_a00_s001024_t004_r0.01-0.0001.txt

mv "0_25.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d/2013_08_26//sierpi_am2_debug_d10_a00_s001024_t004_r0.01-0.0001_0.25.csv"
mv "9_95.csv" "/home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d/2013_08_26//sierpi_am2_debug_d10_a00_s001024_t004_r0.01-0.0001_9.95.csv"

echo "depth	10
adaptive_depth	0
splitting_size	1024
num_threads	4
adaptivity_parameters	0.01/0.0001
bouy	0.25" > /home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d/2013_08_26//sierpi_am2_debug_d10_a00_s001024_t004_r0.01-0.0001_0.25.info

echo "depth	10
adaptive_depth	0
splitting_size	1024
num_threads	4
adaptivity_parameters	0.01/0.0001
bouy	9.95" > /home/schreibm/workspace/sierpi/benchmarks_analytical/swosb_1d/2013_08_26//sierpi_am2_debug_d10_a00_s001024_t004_r0.01-0.0001_9.95.info

