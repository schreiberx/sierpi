36823 TS (Timesteps)
120.003 ST (SIMULATION_TIME)
0.00325891 TSS (Timestep size)
41.5487 RT (REAL_TIME)
37706752 CP (Cells processed)
0.00112833 ASPT (Averaged Seconds per Timestep)
1024 CPST (Cells Processed in Average per Simulation Timestep)
0 ACPST (Averaged number of clusters per Simulation Timestep)
0.907532 MCPS (Million Cells per Second) (local)
0.226883 MCPSPT (Million Clusters per Second per Thread)
0.1875 EDMBPT (CellData Megabyte per Timestep (RW))
166.174 EDMBPS (CellData Megabyte per Second (RW))
EXIT STATUS: OK
