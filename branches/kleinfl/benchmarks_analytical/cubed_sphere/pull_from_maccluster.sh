#! /bin/bash

TARBALL=benchmark_tmp.tar.bz2
LXHALLE_TMP=/u/halle/kleinfl/home_at/tmp
LOCAL_TMP=../cubed_sphere_results/$1
#MACDIR=/home/hpc/pr63so/di68zip/workspace/sierpi_2014_01_28/cubed_sphere/test6

#echo " + creating tarball..."
#ssh lxhalle tar cjf $LXHALLE_TMP/$TARBALL -C $LXHALLE_TMP output*
#ssh lxhalle 'ssh di68zip@mac-login-amd.tum-mac.cos.lrz.de 'cd $MACDIR && tar cjf $TARBALL output*''

#echo " + copying to lxhalle..."
#ssh lxhalle scp di68zip@mac-login-amd.tum-mac.cos.lrz.de:$MACDIR/output* $LXHALLE_TMP
#ssh lxhalle scp di68zip@mac-login-amd.tum-mac.cos.lrz.de:$MACDIR/$TARBALL $LXHALLE_TMP
#ssh lxhalle 'cd $LXHALLE_TMP && ssh di68zip@mac-login-amd.tum-mac.cos.lrz.de 'cd $MACDIR && tar czf - output*' | tar xzf -'

#echo " + creating tarball..."
#ssh lxhalle tar cjf $LXHALLE_TMP/$TARBALL -C $LXHALLE_TMP output*
#ssh lxhalle "cd $LXHALLE_TMP && tar cjf $TARBALL output*"


#ssh lxhalle 'bash -s' < pull_to_lxhalle.sh
scp pull_to_lxhalle.sh lxhalle:$LXHALLE_TMP
ssh lxhalle bash $LXHALLE_TMP/pull_to_lxhalle.sh $1


echo " + copying to localhost..."
mkdir $LOCAL_TMP
scp lxhalle:$LXHALLE_TMP/$TARBALL $LOCAL_TMP

echo " + extracting tarball..."
cd $LOCAL_TMP
tar xf $TARBALL
#tar xf $LOCAL_TMP/$TARBALL $LOCAL_TMP/

echo " + cleaning up lxhalle..."
#ssh lxhalle rm "~/tmp/output*"
ssh lxhalle rm "~/tmp/$TARBALL"


echo "Done!"


#tar czf - $LXHALLE_TMP | ssh lxhalle tar xzf - -C $LOCAL_TMP

#ssh lxhalle "ssh di68zip@mac-login-amd.tum-mac.cos.lrz.de "tar cjf -C $MACDIR - output*"" | tar xf -C $LOCAL_TMP
