#! /usr/bin/python

#
# python buoyPositionByLatitude.py 45 35.2644 16 0
#
# Es werden 16 Bojen um den gedachten Aequator (0 degree) relativ zum
# Nordpol bei (35.2644 degree N, 45 degree E) gelegt.
#


from numpy import *
import sys, getopt
import math

def rotateFromNorthPoleTo(positionVector, latitudeInRadians, longitudeInRadians):
	# create rotation matrix for rotation around x-axis
	X = matrix( [[1,0,0],[0,cos(-latitudeInRadians),sin(-latitudeInRadians)],[0,-sin(-latitudeInRadians),cos(-latitudeInRadians)]])
	# create rotation matrix for rotation around z-axis
	Z = matrix( [[cos(math.pi/2-longitudeInRadians),sin(math.pi/2-longitudeInRadians),0],[-sin(math.pi/2-longitudeInRadians),cos(math.pi/2-longitudeInRadians),0],[0,0,1] ])
	#return rotated position vector	
	return Z * X * positionVector;



if __name__=='__main__':
		
	if len(sys.argv) != 3:
		print "Incorrect number of args! Usage: " + sys.argv[0] + " <longitude of north pole in degree > <latitude of north pole in degree > <number_of_buoys> <latitude of buoys in degree >"
		sys.exit()

	
	# compute positions of buoys equally distributed at the given latitude
	coord = []
#	northPoleLon = float(sys.argv[1]) / 180.0 * math.pi	# north pole latitude in radians
#	northPoleLat = float(sys.argv[2]) / 180.0 * math.pi	# north pole latitude in radians
	k = int(sys.argv[1])					# number of buoys
	lat = float(sys.argv[2])
#	delta = 2.0 * math.pi / float(k)			# distance between buoys
	delta = 360.0 / k

	# iterate of buoys and place them at the given latitude with each two neighboring having distance "delta" in degrees
	for i in range(-k/2,k/2):
		lon = i*delta
		coord.append(("a{:.6f}".format(lat), "a{:.6f}".format(lon),lon))
#		print str((lat,lon)) + " --> " + str((x,y,z)) + " --> " + str(get2DCoordinates(float(x),float(y),float(z))	)
#		print str((x,y,z)) + " --> " + str(get2DCoordinates(float(x),float(y),float(z))	)

	# print output string to copy into scenario xml-file
	coordparams = []
	for (x,y,lon) in coord:
		coordparams.append("{x}/{y}/lat{lat:.1f}lon{lon:.1f}.csv".format(x=x, y=y, lat=lat, lon=lon))
	output = '<param name="output-simulation-specific-data-parameters" value="{}" />'.format(",".join(coordparams))
	
	print output
