#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 11.03.2013

@author: Flo
'''


import matplotlib.pyplot as pyplot
import matplotlib.dates as mdates
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import os, re, sys

# import matplotlib as mpl
# mpl.rcParams['text.usetex']=True
# mpl.rcParams['text.latex.unicode']=True

# pyplot.rc('text', usetex=True)
# pyplot.rc('font', family='serif')

folder = "test9"

basepath = "/home/flo/workspace/sierpi_kleinfl/benchmarks_field/cubed_sphere/test"
basepath = "../cubed_sphere_results"

mindepth = 10
maxdepth = 23

filefmt = "pdf"


class Plot():
	
	def __init__(self):
		self.figure = pyplot.figure("test")
		
	def savetofile(self, filename, extension, prefix=""):
		name = "{}_{}".format(prefix, filename) if prefix else filename
		pyplot.savefig(os.path.join(basepath, "{}.{}".format(name, extension)))
	
	def getdata(self, filename):
		with open(os.path.join(basepath, filename)) as f:
			data = f.readlines()[1:]
			data = [elem.split(",") for elem in data]
			xdata = [float(elem[0]) for elem in data]
			ydata = [float(elem[1]) for elem in data]
			return xdata, ydata
		
	def plotTS(self, depths, proj):
		xdata = {}
		ydata = {}
		filename = "sierpi_intel_mpi_release_bdz_{proj}_d{depth}_a00_cfl0.25_rk1_refine50_coarsen10_t064_n008.txt"
		for p in proj:
			xdata[p] = []
			ydata[p] = []
			for depth in depths:
				strdepth = "d{}".format(depth)
				ts = 0
				for filename in [elem for elem in os.listdir(basepath) if elem.startswith("sierpi") and p in elem and strdepth in elem]:
					with open(os.path.join(basepath, filename)) as f:
						text = f.readlines()
						for line in text:
							match = re.match("(.*) TS \(Timesteps\)", line.strip())
							if match:
								ts = match.groups()[0]
				ts = int(ts)
				xdata[p].append(depth)
				ydata[p].append(ts)
		bwidth = 0.25
		x = np.arange(len(depths))
		fig, ax = pyplot.subplots()
		ax.bar(x, ydata["equiangular"], bwidth, label="equiangular", color="r")
		ax.bar(x+bwidth, ydata["equidistant"], bwidth, label="equidistant", color="b")
		ax.set_xticks(x+bwidth)
		ax.set_xticklabels(depths)
#		for p in proj:
#			pyplot.bar(xdata[p], ydata[p], label=p)
# 		pyplot.title("Simulation timesteps required for simulation time 80000")
		ax.set_ylabel("Simulation Timesteps")
		ax.set_xlabel("Refinement Depth")
		pyplot.legend(prop={'size':6})
		self.savetofile("ts", filefmt, prefix=folder)
		pyplot.clf()
		
		ydata_diff = []
		for ctr in range(len(depths)):
			diff = float(ydata["equiangular"][ctr]) / ydata["equidistant"][ctr] - 1
			diff *= -100
			ydata_diff.append(diff)
		fig, ax = pyplot.subplots()
		bwidth = 0.45
		x = np.arange(len(depths))
		ax.bar(x, ydata_diff, bwidth, label="ea/ed", color="g")
		ax.set_ylabel("Timesteps saved by EA relative to ED timesteps [%]")
		ax.set_xlabel("Refinement Depth")
		ax.set_xticks(x+(bwidth/2))
		ax.set_xticklabels(depths)
		self.savetofile("ts_diff", filefmt, prefix=folder)
		pyplot.clf()
	
	def colorSet(self):
		result = []
#		for i in range(4, num + 4):
#			cdec = 256 * 256 * 256 / (num + 8) * i
#			chex = "#" + hex(cdec)[2:].zfill(6)
#			result.append(chex)
		for i in [255, 128, 192, 160, 224, 96]: #[255, 128, 64, 192, 32, 96, 160, 224]
			for t in [(i, 0, 0), (0, i, 0), (i, 0, 0), (0, i, i), (i, 0, i), (i, i, 0)]:
				newcol = "#" + hex(t[0])[2:].zfill(2) + hex(t[1])[2:].zfill(2) + hex(t[2])[2:].zfill(2)
				result.append(newcol)
		return result
		
	def action(self):
		proj = {"equiangular": "ea", "equidistant": "ed"}
		depths = range(mindepth, maxdepth + 1)
		self.plotTS(depths, proj)
		colorset = self.colorSet()
		for p in proj:
			pp = PdfPages(os.path.join(basepath, '{}_{}.pdf'.format(p, folder)))
			maxdatalist45 = []
			maxdatalist0 = []
			for depth in depths:
				dint = depth
				depth = "d{}".format(depth)
				print((p, depth))
	#			self.plot(depth)
				xdata = None
				ydatalist = []
				fulldatalist = []
				cctr = 0
				filelist = [elem for elem in os.listdir(basepath) if "output" in elem and depth in elem and p in elem]
				filelist.sort()
				colorset = self.colorSet()
				for filename in filelist:
					xdata, ydata = self.getdata(filename)
					lat, lon = re.match(".*dart_lat(.*)lon(.*)\.csv", filename).groups()
					label = filename.split("dart_")[1].replace(".csv", "")
					linestyle = "-"
					dashes = []
					color = colorset[cctr]
					if float(lat) == 0:
						linestyle = "--"
						dashes = (2, 1.2)
					pyplot.plot(xdata, ydata, label=label, linestyle=linestyle, color=color, linewidth=0.5, dashes=dashes)
					ydatalist.append((ydata, label))
					fulldatalist.append(([[xdata[ctr], ydata[ctr]] for ctr in range(len(xdata))], label, float(lon), float(lat)))
					cctr += 1
				pyplot.title(u"Surface elevation for darts at 0°N/45°N [{}]".format(depth))
				pyplot.ylabel("Surface elevation")
				pyplot.xlabel("Simulation time")
				pyplot.legend(prop={'size':6})
				pyplot.ylim([-0.6, 0.8])
#				self.savetofile("test", filefmt, prefix=p+depth)
				pp.savefig()
				pyplot.clf()
				
#				baseline = ydatalist[0][0]
#				for ydata in ydatalist[1:]:
#					err = [ydata[0][ctr] - baseline[ctr] for ctr in range(len(baseline))]
#					pyplot.plot(xdata, err, label=ydata[1])
#				pyplot.title(u"Surface elevation differences for darts at 20°N")
#				pyplot.ylabel("Surface elevation")
#				pyplot.xlabel("Simulation time")
#				pyplot.legend(prop={'size':6})
#				self.savetofile("errors", "svg", prefix=p+depth)
#				pyplot.clf()
				
				if (dint < 13):
					continue
				maxdata45 = [[elem[2], sorted(elem[0], key=lambda x: x[1])[-1][0]] for elem in fulldatalist if elem[3] == 45]
				maxdata45 = sorted(maxdata45, key=lambda x: x[0])
				maxdatalist45.append((maxdata45, depth))
				maxdata0 = [[elem[2], sorted(elem[0], key=lambda x: x[1])[-1][0]] for elem in fulldatalist if elem[3] == 0]
				maxdata0 = sorted(maxdata0, key=lambda x: x[0])
				maxdatalist0.append((maxdata0, depth))
#				if depth == "d10": print(fulldatalist)
			pp.close()
				
#			print(maxdatalist0)
			cctr = 0
			for maxdata, label in maxdatalist0:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			pyplot.title(u"Time of maximum elevation for darts at 0°")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Simulation time")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([43500, 44400])
			self.savetofile("0max", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
				
			cctr = 0
			for maxdata, label in maxdatalist45:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			pyplot.title(u"Time of maximum elevation for darts at 45°N")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Simulation time")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([20800, 21600])
			self.savetofile("45max", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
	

if __name__ == "__main__":
	if len(sys.argv) > 1:
		folder = sys.argv[1]
	basepath = os.path.join(basepath, folder)
	if not os.path.exists(basepath):
		os.makedirs(basepath)
	plot = Plot()
	plot.action()
