7445 TS (Timesteps)
120.007 ST (SIMULATION_TIME)
0.016088 TSS (Timestep size)
16.8276 RT (REAL_TIME)
121978880 CP (Cells processed)
0.00226025 ASPT (Averaged Seconds per Timestep)
16384 CPST (Cells Processed in Average per Simulation Timestep)
512 ACPST (Averaged number of clusters per Simulation Timestep)
7.24875 MCPS (Million Cells per Second) (local)
0.0906093 MCPSPT (Million Clusters per Second per Thread)
1.5 EDMBPT (CellData Megabyte per Timestep (RW))
663.643 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.391432 GFLOPS
EXIT STATUS: OK
