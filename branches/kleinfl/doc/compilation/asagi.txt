
evtl. immer CC=icc bei ./configure angeben!




* ASAGI:
	URL: https://github.com/TUM-I5/ASAGI

	git clone https://github.com/TUM-I5/ASAGI
	cd ASAGI
	for i in build_nompi build_mpi; do
		mkdir -p $i
		cd $i
		export CMAKE_PREFIX_PATH=$HOME/local/netcdf-4.1.3
		CC=icc CXX=icpc cmake -DNOMPI=ON -DCMAKE_INSTALL_PREFIX=$HOME/local/asagi-0.3 -DNETCDF_LIBRARIES=$HOME/local/netcdf-4.1.3/lib -DNETCDF_INCLUDES=$HOME/local/netcdf-4.1.3/include ..
		make -j 8 install || exit
		cd ..
	done
