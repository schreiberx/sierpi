#! /usr/bin/python

#
# python buoyPositionByLatitude.py 45 35.2644 16 0
#
# Es werden 16 Bojen um den gedachten Aequator (0 degree) relativ zum
# Nordpol bei (35.2644 degree N, 45 degree E) gelegt.
#


from numpy import *
import sys, getopt
import math

def rotateFromNorthPoleTo(positionVector, latitudeInRadians, longitudeInRadians):
	# create rotation matrix for rotation around x-axis
	X = matrix( [[1,0,0],[0,cos(-latitudeInRadians),sin(-latitudeInRadians)],[0,-sin(-latitudeInRadians),cos(-latitudeInRadians)]])
	# create rotation matrix for rotation around z-axis
	Z = matrix( [[cos(math.pi/2-longitudeInRadians),sin(math.pi/2-longitudeInRadians),0],[-sin(math.pi/2-longitudeInRadians),cos(math.pi/2-longitudeInRadians),0],[0,0,1] ])
	#return rotated position vector	
	return Z * X * positionVector;



def getFaceID(face): 
	if face == 'E_ERROR':
		return 0
	elif face == 'E_FACE_TOP':
		return 1
	elif face == 'E_FACE_BOTTOM':
		return 2
	elif face == 'E_FACE_FRONT':
		return 3
	elif face == 'E_FACE_BACK':
		return 4
	elif face == 'E_FACE_LEFT':
		return 5
	elif face == 'E_FACE_RIGHT':
		return 6
	else:
		return 0

def get2DCoordinates(i_sphere_x, i_sphere_y, i_sphere_z):

	#projection from sphere to cube using maximum norm
	if abs(i_sphere_x) >= abs(i_sphere_y) and abs(i_sphere_x) >= abs(i_sphere_z):
		if i_sphere_x >= 0:
			d = i_sphere_x
			face = 'E_FACE_FRONT'
		else:
			d = -i_sphere_x
			face = 'E_FACE_BACK'
	
	elif abs(i_sphere_y) >= abs(i_sphere_x) and abs(i_sphere_y) >= abs(i_sphere_z):
		if i_sphere_y >= 0:
			d = i_sphere_y
			face = 'E_FACE_RIGHT'
		else:
			d = -i_sphere_y
			face = 'E_FACE_LEFT'
	elif abs(i_sphere_z) >= abs(i_sphere_x) and abs(i_sphere_z) >= abs(i_sphere_y):
		if i_sphere_z >= 0:
			d = i_sphere_z
			face = 'E_FACE_TOP'
		else:
			d = -i_sphere_z
			face = 'E_FACE_BOTTOM'
	else:
		print "coordinates incorrect"		
		return

	x = float(i_sphere_x) / float(d)
	y = float(i_sphere_y) / float(d)
	z = float(i_sphere_z) / float(d)

	# projection from 3D cube to 2D cube map
	if face == 'E_FACE_LEFT':
		o_vertex_x = x-3
		o_vertex_y = z

	elif face == 'E_FACE_FRONT':
		o_vertex_x = y-1
		o_vertex_y = z

	elif face == 'E_FACE_RIGHT':
		o_vertex_x = 1-x
		o_vertex_y = z

	elif face == 'E_FACE_BACK':
		o_vertex_x = 3-y
		o_vertex_y = z

	elif face == 'E_FACE_TOP':
		o_vertex_y = 2-x
		o_vertex_x = y-1

	elif face == 'E_FACE_BOTTOM':
		o_vertex_y = x-2
		o_vertex_x = y-1

	else:
		print "face not correct"		
		return

	# shift position slightly to circumvent locations on triangle edges
	if o_vertex_x >= 0:
		x = round(o_vertex_x * 1000000-2) / 1000000.0
	else:
		x = round(o_vertex_x * 1000000+2) / 1000000.0
	if o_vertex_y >= 0:
		y = round(o_vertex_y * 1000000-1) / 1000000.0
	else:
		y = round(o_vertex_y * 1000000+1) / 1000000.0

	return (x, y) 

if __name__=='__main__':
		
	if len(sys.argv) != 5:
		print "Incorrect number of args! Usage: " + sys.argv[0] + " <longitude of north pole in degree > <latitude of north pole in degree > <number_of_buoys> <latitude of buoys in degree >"
		sys.exit()

	
	# compute positions of buoys equally distributed at the given latitude
	coord = []
	northPoleLon = float(sys.argv[1]) / 180.0 * math.pi	# north pole latitude in radians
	northPoleLat = float(sys.argv[2]) / 180.0 * math.pi	# north pole latitude in radians
	k = int(sys.argv[3])					# number of buoys
	lat = (90.0-float(sys.argv[4])) / 180.0 * math.pi	# buoy latitude in radians
	delta = 2.0 * math.pi / float(k)			# distance between buoys

	# iterate of buoys and place them at the given latitude with each two neighboring having distance "delta" in degrees
	for i in range(-k/2,k/2):
		lon = i*delta
		x = round(sin(lat) * cos(lon) * 1000000) / 1000000.0
		z = round(sin(lat) * sin(lon) * 1000000) / 1000000.0
		y = round(cos(lat) * 1000000) / 1000000.0
		
		# rotate position vector to according given position of the north pole
		positionVector = matrix( [[x],[y],[z]] )
		[[x],[y],[z]] = rotateFromNorthPoleTo(positionVector, northPoleLat, northPoleLon)
		
		# project spherical position to cube map
		coord.append((get2DCoordinates(float(x),float(y),float(z)),lon))
		print str((lat,lon)) + " --> " + str((x,y,z)) + " --> " + str(get2DCoordinates(float(x),float(y),float(z))	)
#		print str((x,y,z)) + " --> " + str(get2DCoordinates(float(x),float(y),float(z))	)

	# print output string to copy into scenario xml-file
	output = '<param name="output-simulation-specific-data-parameters" value="'
	first = True
	for ((x,y),lon)in coord:
		if first:
			output += "%.6f" % x + '/' + "%.6f" % y + '/lat' + str(int(sys.argv[4])) + 'lon' + str(int(lon/math.pi * 180)) + '.csv'
			first = False
		else:
			output += ',' +"%.6f" % x + '/' + "%.6f" % y + '/lat' + str(int(sys.argv[4])) + 'lon' + str(int(lon/math.pi * 180)) + '.csv'
	output += '" />'
	
	print output
