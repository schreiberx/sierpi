#!/bin/bash

# application is running for 100 seconds in average

. params.sh

killall sierpi_intel_itbb_tsunami_parallel_release

EXEC="$EXEC_ITBB"
PARAMS="$PARAMS_DEFAULT"

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}

date
STARTSECONDS=`date +%s`

killall iPMO_server_release

echo "Starting ipmo server"
$EXEC_IPMO -n 40 -v 2 > "output_""$DATE""_itbb_parallel_server.txt" &

echo "App 1: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_parallel_1.txt" &
PA1=$!

echo "App 2: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_parallel_2.txt" &
PA2=$!

echo "App 3: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_parallel_3.txt" &
PA3=$!

echo "App 4: $PROGRAM_START"
$PROGRAM_START > "output_""$DATE""_itbb_parallel_4.txt" &
PA4=$!


wait $PA1
wait $PA2
wait $PA3
wait $PA4



ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS" > "output_""$DATE""_itbb_parallel.txt"

sleep 1
