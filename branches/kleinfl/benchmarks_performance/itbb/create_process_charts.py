#! /usr/bin/python


import sys
import subprocess
import os
import time
import commands
import datetime


tracked_programs = "sierpi_intel_itbb_tsunami_parallel_release,sierpi_intel_tbb_tsunami_parallel_release,sierpi_intel_omp_tsunami_parallel_release,sierpi_gnu_ipmo_async_tsunami_parallel_debug"

max_cores = 80

ignore_utilization_threshold = 50

if len(sys.argv) > 1:
	max_cores = int(sys.argv[1])

print "\t".join(list(str(i) for i in range(0, max_cores)))

next_id = 1
pid_dict = {}


start_time = time.time();

while True:
#	print str(time.time() - start_time)+":\t"

	pid_on_core_list = list(0 for i in range(0, max_cores))

	ps_cmd = "ps -C "+tracked_programs+" -L -F"
#	print ps_cmd
	lines = commands.getoutput(ps_cmd).splitlines()[1:]

	for l in lines:
		l = l.replace('  ', ' ')
		l = l.replace('  ', ' ')
		l = l.replace('  ', ' ')
		l = l.replace('  ', ' ')
		l = l.replace('  ', ' ')
		l = l.replace('  ', ' ')
		cols = l.split(' ')

		pid=int(cols[1])
		core_utilization=int(cols[4])
		core=int(cols[8])

		# ignore core utilizations of less than given threshold
		if core_utilization < ignore_utilization_threshold:
			continue

		if pid_dict.has_key(pid):
			prog_nr = pid_dict[pid]
		else:
			prog_nr = next_id
			next_id += 1
			pid_dict[pid] = prog_nr

		if core >= max_cores:
			#print "WARNING: maximum number of cores exceeded"
			pass
		else:
			pid_on_core_list[int(core)] = prog_nr

	print "\t".join([str(x) for x in pid_on_core_list])

#	sys.stdout.write(pid+"\t"+core_utilization+"\t"+core)

	time.sleep(1)
