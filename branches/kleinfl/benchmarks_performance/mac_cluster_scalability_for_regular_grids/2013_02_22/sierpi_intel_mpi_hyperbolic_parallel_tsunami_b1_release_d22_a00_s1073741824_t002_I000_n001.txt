NetCDF not compiled in!!!

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.220155 ST (SIMULATION_TIME)
0.0218639 TSS (Timestep size)
28.98 RT (REAL_TIME)
83886080 CP (Cells processed)
2.898 ASPT (Averaged Seconds per Timestep)
8.38861e+06 CPST (Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
2.89462 MCPS (Million Cells per Second) (local)
2.89462 MCPSPT (Million Clusters per Second per Thread)
832 EDMBPT (CellData Megabyte per Timestep (RW))
287.095 EDMBPS (CellData Megabyte per Second (RW))
378 Flops per cell update (Matrix Multiplication)
1.09417 GFLOPS

++++++++++ SUMMARY ++++++++++
2.89462 GMCPS (Global Million Cells per Second) (global)
2.89462 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
