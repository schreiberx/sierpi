Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
26.9842 RT (REAL_TIME)
105205259 CP (Cells processed)
2.69842 ASPT (Averaged Seconds per Timestep)
1.05205e+07 CPST (Cells Processed in Average per Simulation Timestep)
5125 ACPST (Averaged number of clusters per Simulation Timestep)
3.89877 MCPS (Million Cells per Second) (local)
3.89877 MCPSPT (Million Clusters per Second per Thread)
321.061 EDMBPT (CellData Megabyte per Timestep (RW))
118.981 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.210533 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
26.9842 RT (REAL_TIME)
105211904 CP (Cells processed)
2.69842 ASPT (Averaged Seconds per Timestep)
1.05212e+07 CPST (Cells Processed in Average per Simulation Timestep)
5137.3 ACPST (Averaged number of clusters per Simulation Timestep)
3.89901 MCPS (Million Cells per Second) (local)
3.89901 MCPSPT (Million Clusters per Second per Thread)
321.081 EDMBPT (CellData Megabyte per Timestep (RW))
118.988 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.210547 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
26.9839 RT (REAL_TIME)
105199750 CP (Cells processed)
2.69839 ASPT (Averaged Seconds per Timestep)
1.052e+07 CPST (Cells Processed in Average per Simulation Timestep)
5132.3 ACPST (Averaged number of clusters per Simulation Timestep)
3.89861 MCPS (Million Cells per Second) (local)
3.89861 MCPSPT (Million Clusters per Second per Thread)
321.044 EDMBPT (CellData Megabyte per Timestep (RW))
118.976 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.210525 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
26.9839 RT (REAL_TIME)
105204275 CP (Cells processed)
2.69839 ASPT (Averaged Seconds per Timestep)
1.05204e+07 CPST (Cells Processed in Average per Simulation Timestep)
5129.5 ACPST (Averaged number of clusters per Simulation Timestep)
3.89878 MCPS (Million Cells per Second) (local)
3.89878 MCPSPT (Million Clusters per Second per Thread)
321.058 EDMBPT (CellData Megabyte per Timestep (RW))
118.981 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.210534 GFLOPS


++++++++++ SUMMARY ++++++++++
15.5952 GMCPS (Global Million Cells per Second) (global)
15.5952 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
