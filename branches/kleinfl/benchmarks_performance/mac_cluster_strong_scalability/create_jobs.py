#! /usr/bin/python

import sys
import time
import commands
import os
import math

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''


#
# Sierpi parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set=[
		'sierpi_intel_mpi_nothreading_hyperbolic_parallel_tsunami_b0_release',
		'sierpi_intel_mpi_nothreading_scan_data_scan_based_split_and_join_hyperbolic_parallel_tsunami_b0_release',

		'sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_b0_release',
		'sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_hyperbolic_parallel_tsunami_b0_release',

		'sierpi_intel_mpi_tbb_sacsp_hyperbolic_parallel_tsunami_b0_release',
		'sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_sacsp_hyperbolic_parallel_tsunami_b0_release'
	]

	sierpi_skip_split_and_join_parameter_set=[1,30,100]

	# DEPTH SET
	sierpi_sim_depth_set=[22]
#	sierpi_sim_depth_set=[20]

	# ADAPTIVE DEPTH SET
	sierpi_sim_adaptive_depth_set=[8]
#	sierpi_sim_adaptive_depth_set=[6]

	# SPLITTING SIZE SET
#	sierpi_sim_splitting_size_set=[1024*1, 1024*2, 1024*4, 1024*8, 1024*16, 1024*32, 1024*64, 1024*128, 1024*256, 1024*512, 1024*1024]

	# THREADS SET
	# Use 2 threads since cluster is runned in hyperthreading mode!
	#
	sierpi_sim_threads_set=[1,2,4,8,16]

	# MPI NODE SET
	sierpi_sim_mpi_nodes_set=[1, 4, 16, 64, 128, 256]

	sierpi_max_cores = 256

	# splitting sizes
#	sierpi_sim_splitting_size_set=[1024*8, 1024*16]
	sierpi_sim_splitting_size_set=[1024*4]
#	sierpi_sim_splitting_size_set=[1024]

	sierpi_sim_fixedtime=-1
	sierpi_sim_fixedtimesteps=100
	sierpi_sim_initial_split=0

	sierpi_adaptivity_parameters='0.01/0.005'

	sierpi_extra_params=''

	estimated_runtime_scalar=1.0




# bin set
for bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			if bin.find('scan_based_split') > 0:
				sierpi_sim_splitting_size_set_ = [1024]
			else:
				sierpi_sim_splitting_size_set_ = sierpi_sim_splitting_size_set

			# splitting sizes
			for splitting_size in sierpi_sim_splitting_size_set_:

				if bin.find('nothreading') > 0:
					sierpi_sim_threads_set_ = [1]
				else:
					sierpi_sim_threads_set_ = sierpi_sim_threads_set

				# threads set
				for num_threads in sierpi_sim_threads_set_:

					for skip_split_and_join in sierpi_skip_split_and_join_parameter_set:

						# threads set
						for num_mpi_nodes in sierpi_sim_mpi_nodes_set:

							if sierpi_max_cores < num_mpi_nodes*num_threads:
								continue

							# estimate number of cells
							max_cells=2**(depth+adaptive_depth)

							def get_estimated_runtime(max_cells, splitting_size):

								# estimate clusters
								clusters=max_cells/splitting_size
								clusters+=1

								# clusters per thread
								clusters_per_thread=clusters/num_threads
								clusters_per_thread+=1

								# cells per thread
								cells_per_thread=clusters_per_thread*splitting_size

								return cells_per_thread*0.0001

							if splitting_size == 0:
								estimated_runtime=get_estimated_runtime(max_cells, 16*1024)
							else:
								estimated_runtime=get_estimated_runtime(max_cells, splitting_size)

							estimated_runtime*=estimated_runtime_scalar

							estimated_runtime_string=time.strftime("00:30:00", time.gmtime(estimated_runtime))

							initial_domain_splits=int(math.log(num_mpi_nodes, 2))

							sierpi_run=bin
							sierpi_run+=' -d '+str(depth)
							sierpi_run+=' -a '+str(adaptive_depth)
							sierpi_run+=' -t '+str(sierpi_sim_fixedtime)
							sierpi_run+=' -L '+str(sierpi_sim_fixedtimesteps)
							sierpi_run+=' -k '+str(skip_split_and_join)
	#						sierpi_run+=' -I '+str(initial_domain_splits)
							sierpi_run+=' -r '+sierpi_adaptivity_parameters
							sierpi_run+=' -n '+str(num_threads)
							sierpi_run+=' -N '+str(num_threads)
							sierpi_run+=' -o '+str(splitting_size)

							if sierpi_extra_params != '':
								sierpi_run+=' '+sierpi_extra_params

							job_description_string = bin+'_d'+str(depth).zfill(2)+'_a'+str(adaptive_depth).zfill(2)+'_s'+str(splitting_size).zfill(6)+'_t'+str(num_threads).zfill(3)
	#						job_description_string += '_I'+str(initial_domain_splits).zfill(3)
							job_description_string += '_k'+str(skip_split_and_join).zfill(3)
							job_description_string += '_n'+str(num_mpi_nodes).zfill(3)
							job_filepath = jobfile_directory+'/'+job_description_string+'.cmd'
							output_filepath = jobfile_directory+'/'+job_description_string+'.txt'

							# assume 63 available GB
							mem_per_cpu=(63*1024)/num_threads+1

							job_file_content="""#! /bin/bash

# output
#SBATCH -o """+output_filepath+"""
# working directory
#SBATCH -D """+working_directory+"""
# job description
#SBATCH -J """+job_description_string+"""
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks="""+str(num_mpi_nodes)+"""
#SBATCH --cpus-per-task="""+str(num_threads*2)+"""
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time="""+estimated_runtime_string+"""

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd """+working_directory+"""

mpiexec.hydra -genv OMP_NUM_THREADS """+str(num_threads)+""" -envall -ppn """+str(16/num_threads)+""" -n """+str(num_mpi_nodes)+""" ../../build/"""+sierpi_run+"""
"""

#							print job_file_content

							print "Writing jobfile '"+job_filepath+"'"
							f=open(job_filepath, 'w')
							f.write(job_file_content)
							f.close()
