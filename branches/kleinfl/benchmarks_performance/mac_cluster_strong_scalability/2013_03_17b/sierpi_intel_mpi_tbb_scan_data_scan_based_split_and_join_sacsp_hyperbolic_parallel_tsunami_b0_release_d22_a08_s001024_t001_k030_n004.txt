Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.489 RT (REAL_TIME)
1062586535 CP (Cells processed)
2.20489 ASPT (Averaged Seconds per Timestep)
1.06259e+07 CPST (Cells Processed in Average per Simulation Timestep)
37.3 ACPST (Averaged number of clusters per Simulation Timestep)
4.81922 MCPS (Million Cells per Second) (local)
4.81922 MCPSPT (Million Clusters per Second per Thread)
324.276 EDMBPT (CellData Megabyte per Timestep (RW))
147.071 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.260238 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.489 RT (REAL_TIME)
1056542300 CP (Cells processed)
2.20489 ASPT (Averaged Seconds per Timestep)
1.05654e+07 CPST (Cells Processed in Average per Simulation Timestep)
58.8 ACPST (Averaged number of clusters per Simulation Timestep)
4.7918 MCPS (Million Cells per Second) (local)
4.7918 MCPSPT (Million Clusters per Second per Thread)
322.431 EDMBPT (CellData Megabyte per Timestep (RW))
146.234 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.258757 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.489 RT (REAL_TIME)
1060366855 CP (Cells processed)
2.20489 ASPT (Averaged Seconds per Timestep)
1.06037e+07 CPST (Cells Processed in Average per Simulation Timestep)
24 ACPST (Averaged number of clusters per Simulation Timestep)
4.80915 MCPS (Million Cells per Second) (local)
4.80915 MCPSPT (Million Clusters per Second per Thread)
323.598 EDMBPT (CellData Megabyte per Timestep (RW))
146.764 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259694 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.489 RT (REAL_TIME)
1059052128 CP (Cells processed)
2.20489 ASPT (Averaged Seconds per Timestep)
1.05905e+07 CPST (Cells Processed in Average per Simulation Timestep)
56.7 ACPST (Averaged number of clusters per Simulation Timestep)
4.80319 MCPS (Million Cells per Second) (local)
4.80319 MCPSPT (Million Clusters per Second per Thread)
323.197 EDMBPT (CellData Megabyte per Timestep (RW))
146.582 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259372 GFLOPS


++++++++++ SUMMARY ++++++++++
19.2234 GMCPS (Global Million Cells per Second) (global)
19.2234 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
