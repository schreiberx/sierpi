Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
64.6964 RT (REAL_TIME)
4238547816 CP (Cells processed)
0.646964 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20608.5 ACPST (Averaged number of clusters per Simulation Timestep)
65.5144 MCPS (Million Cells per Second) (local)
4.09465 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
1999.34 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.53778 GFLOPS

++++++++++ SUMMARY ++++++++++
65.5144 GMCPS (Global Million Cells per Second) (global)
4.09465 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
