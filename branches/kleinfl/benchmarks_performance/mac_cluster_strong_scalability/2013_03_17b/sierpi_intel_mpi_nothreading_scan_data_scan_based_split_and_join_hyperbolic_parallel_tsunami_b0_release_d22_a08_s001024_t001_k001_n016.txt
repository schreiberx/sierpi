Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!

Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264906240 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.64906e+06 CPST (Cells Processed in Average per Simulation Timestep)
38.89 ACPST (Averaged number of clusters per Simulation Timestep)
4.03149 MCPS (Million Cells per Second) (local)
4.03149 MCPSPT (Million Clusters per Second per Thread)
80.843 EDMBPT (CellData Megabyte per Timestep (RW))
123.031 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.2177 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264912127 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
28.3 ACPST (Averaged number of clusters per Simulation Timestep)
4.03158 MCPS (Million Cells per Second) (local)
4.03158 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
123.034 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217705 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264910769 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
32.4 ACPST (Averaged number of clusters per Simulation Timestep)
4.03156 MCPS (Million Cells per Second) (local)
4.03156 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
123.033 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217704 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264910807 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
26.63 ACPST (Averaged number of clusters per Simulation Timestep)
4.03156 MCPS (Million Cells per Second) (local)
4.03156 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
123.034 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217704 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264929280 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.64929e+06 CPST (Cells Processed in Average per Simulation Timestep)
24.26 ACPST (Averaged number of clusters per Simulation Timestep)
4.03184 MCPS (Million Cells per Second) (local)
4.03184 MCPSPT (Million Clusters per Second per Thread)
80.85 EDMBPT (CellData Megabyte per Timestep (RW))
123.042 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217719 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264890368 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.6489e+06 CPST (Cells Processed in Average per Simulation Timestep)
26.21 ACPST (Averaged number of clusters per Simulation Timestep)
4.03125 MCPS (Million Cells per Second) (local)
4.03125 MCPSPT (Million Clusters per Second per Thread)
80.8381 EDMBPT (CellData Megabyte per Timestep (RW))
123.024 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217687 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7093 RT (REAL_TIME)
264911616 CP (Cells processed)
0.657093 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
26.53 ACPST (Averaged number of clusters per Simulation Timestep)
4.03157 MCPS (Million Cells per Second) (local)
4.03157 MCPSPT (Million Clusters per Second per Thread)
80.8446 EDMBPT (CellData Megabyte per Timestep (RW))
123.034 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217705 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264900768 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.33 ACPST (Averaged number of clusters per Simulation Timestep)
4.03141 MCPS (Million Cells per Second) (local)
4.03141 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
123.029 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217696 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7091 RT (REAL_TIME)
264889440 CP (Cells processed)
0.657091 ASPT (Averaged Seconds per Timestep)
2.64889e+06 CPST (Cells Processed in Average per Simulation Timestep)
29.39 ACPST (Averaged number of clusters per Simulation Timestep)
4.03124 MCPS (Million Cells per Second) (local)
4.03124 MCPSPT (Million Clusters per Second per Thread)
80.8378 EDMBPT (CellData Megabyte per Timestep (RW))
123.024 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217687 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264918771 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64919e+06 CPST (Cells Processed in Average per Simulation Timestep)
27.82 ACPST (Averaged number of clusters per Simulation Timestep)
4.03168 MCPS (Million Cells per Second) (local)
4.03168 MCPSPT (Million Clusters per Second per Thread)
80.8468 EDMBPT (CellData Megabyte per Timestep (RW))
123.037 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217711 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264922381 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64922e+06 CPST (Cells Processed in Average per Simulation Timestep)
18.02 ACPST (Averaged number of clusters per Simulation Timestep)
4.03174 MCPS (Million Cells per Second) (local)
4.03174 MCPSPT (Million Clusters per Second per Thread)
80.8479 EDMBPT (CellData Megabyte per Timestep (RW))
123.039 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217714 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264914579 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
26.72 ACPST (Averaged number of clusters per Simulation Timestep)
4.03162 MCPS (Million Cells per Second) (local)
4.03162 MCPSPT (Million Clusters per Second per Thread)
80.8455 EDMBPT (CellData Megabyte per Timestep (RW))
123.035 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217707 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264901440 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.59 ACPST (Averaged number of clusters per Simulation Timestep)
4.03142 MCPS (Million Cells per Second) (local)
4.03142 MCPSPT (Million Clusters per Second per Thread)
80.8415 EDMBPT (CellData Megabyte per Timestep (RW))
123.029 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217697 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264902080 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64902e+06 CPST (Cells Processed in Average per Simulation Timestep)
30.28 ACPST (Averaged number of clusters per Simulation Timestep)
4.03143 MCPS (Million Cells per Second) (local)
4.03143 MCPSPT (Million Clusters per Second per Thread)
80.8417 EDMBPT (CellData Megabyte per Timestep (RW))
123.029 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217697 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264922448 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64922e+06 CPST (Cells Processed in Average per Simulation Timestep)
20.91 ACPST (Averaged number of clusters per Simulation Timestep)
4.03174 MCPS (Million Cells per Second) (local)
4.03174 MCPSPT (Million Clusters per Second per Thread)
80.8479 EDMBPT (CellData Megabyte per Timestep (RW))
123.039 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217714 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.7092 RT (REAL_TIME)
264904704 CP (Cells processed)
0.657092 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
38.95 ACPST (Averaged number of clusters per Simulation Timestep)
4.03147 MCPS (Million Cells per Second) (local)
4.03147 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
123.031 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217699 GFLOPS


++++++++++ SUMMARY ++++++++++
64.5046 GMCPS (Global Million Cells per Second) (global)
64.5046 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: EXIT STATUS: OK
OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
