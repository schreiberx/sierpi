Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 8: more nodes than initial clusters available!Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!

Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
10: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263097696 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63098e+06 CPST (Cells Processed in Average per Simulation Timestep)
41.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.93839 MCPS (Million Cells per Second) (local)
3.93839 MCPSPT (Million Clusters per Second per Thread)
80.291 EDMBPT (CellData Megabyte per Timestep (RW))
120.19 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212673 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
266758279 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.66758e+06 CPST (Cells Processed in Average per Simulation Timestep)
40 ACPST (Averaged number of clusters per Simulation Timestep)
3.99319 MCPS (Million Cells per Second) (local)
3.99319 MCPSPT (Million Clusters per Second per Thread)
81.4082 EDMBPT (CellData Megabyte per Timestep (RW))
121.862 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215632 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
267993569 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.67994e+06 CPST (Cells Processed in Average per Simulation Timestep)
43.2 ACPST (Averaged number of clusters per Simulation Timestep)
4.01168 MCPS (Million Cells per Second) (local)
4.01168 MCPSPT (Million Clusters per Second per Thread)
81.7851 EDMBPT (CellData Megabyte per Timestep (RW))
122.427 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.216631 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
268368959 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.68369e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.84 ACPST (Averaged number of clusters per Simulation Timestep)
4.0173 MCPS (Million Cells per Second) (local)
4.0173 MCPSPT (Million Clusters per Second per Thread)
81.8997 EDMBPT (CellData Megabyte per Timestep (RW))
122.598 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.216934 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
262573576 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.62574e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.84 ACPST (Averaged number of clusters per Simulation Timestep)
3.93055 MCPS (Million Cells per Second) (local)
3.93055 MCPSPT (Million Clusters per Second per Thread)
80.1311 EDMBPT (CellData Megabyte per Timestep (RW))
119.951 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21225 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263622152 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63622e+06 CPST (Cells Processed in Average per Simulation Timestep)
39 ACPST (Averaged number of clusters per Simulation Timestep)
3.94624 MCPS (Million Cells per Second) (local)
3.94624 MCPSPT (Million Clusters per Second per Thread)
80.4511 EDMBPT (CellData Megabyte per Timestep (RW))
120.43 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.213097 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263359840 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.6336e+06 CPST (Cells Processed in Average per Simulation Timestep)
37.64 ACPST (Averaged number of clusters per Simulation Timestep)
3.94232 MCPS (Million Cells per Second) (local)
3.94232 MCPSPT (Million Clusters per Second per Thread)
80.371 EDMBPT (CellData Megabyte per Timestep (RW))
120.31 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212885 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263163232 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63163e+06 CPST (Cells Processed in Average per Simulation Timestep)
43.16 ACPST (Averaged number of clusters per Simulation Timestep)
3.93937 MCPS (Million Cells per Second) (local)
3.93937 MCPSPT (Million Clusters per Second per Thread)
80.311 EDMBPT (CellData Megabyte per Timestep (RW))
120.22 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212726 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263294472 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63294e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.48 ACPST (Averaged number of clusters per Simulation Timestep)
3.94134 MCPS (Million Cells per Second) (local)
3.94134 MCPSPT (Million Clusters per Second per Thread)
80.3511 EDMBPT (CellData Megabyte per Timestep (RW))
120.28 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212832 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
264281334 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.64281e+06 CPST (Cells Processed in Average per Simulation Timestep)
40 ACPST (Averaged number of clusters per Simulation Timestep)
3.95611 MCPS (Million Cells per Second) (local)
3.95611 MCPSPT (Million Clusters per Second per Thread)
80.6523 EDMBPT (CellData Megabyte per Timestep (RW))
120.731 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21363 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
270428193 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.70428e+06 CPST (Cells Processed in Average per Simulation Timestep)
36.88 ACPST (Averaged number of clusters per Simulation Timestep)
4.04813 MCPS (Million Cells per Second) (local)
4.04813 MCPSPT (Million Clusters per Second per Thread)
82.5281 EDMBPT (CellData Megabyte per Timestep (RW))
123.539 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.218599 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263357284 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63357e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.16 ACPST (Averaged number of clusters per Simulation Timestep)
3.94228 MCPS (Million Cells per Second) (local)
3.94228 MCPSPT (Million Clusters per Second per Thread)
80.3703 EDMBPT (CellData Megabyte per Timestep (RW))
120.309 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212883 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263228768 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63229e+06 CPST (Cells Processed in Average per Simulation Timestep)
42.32 ACPST (Averaged number of clusters per Simulation Timestep)
3.94036 MCPS (Million Cells per Second) (local)
3.94036 MCPSPT (Million Clusters per Second per Thread)
80.331 EDMBPT (CellData Megabyte per Timestep (RW))
120.25 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212779 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263228768 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63229e+06 CPST (Cells Processed in Average per Simulation Timestep)
38.64 ACPST (Averaged number of clusters per Simulation Timestep)
3.94036 MCPS (Million Cells per Second) (local)
3.94036 MCPSPT (Million Clusters per Second per Thread)
80.331 EDMBPT (CellData Megabyte per Timestep (RW))
120.25 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212779 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
268694000 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.68694e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.04 ACPST (Averaged number of clusters per Simulation Timestep)
4.02217 MCPS (Million Cells per Second) (local)
4.02217 MCPSPT (Million Clusters per Second per Thread)
81.9989 EDMBPT (CellData Megabyte per Timestep (RW))
122.747 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.217197 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
66.8033 RT (REAL_TIME)
263097696 CP (Cells processed)
0.668033 ASPT (Averaged Seconds per Timestep)
2.63098e+06 CPST (Cells Processed in Average per Simulation Timestep)
41.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.93839 MCPS (Million Cells per Second) (local)
3.93839 MCPSPT (Million Clusters per Second per Thread)
80.291 EDMBPT (CellData Megabyte per Timestep (RW))
120.19 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212673 GFLOPS


++++++++++ SUMMARY ++++++++++
63.4482 GMCPS (Global Million Cells per Second) (global)
63.4482 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
