Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264359936 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
9.58521 MCPS (Million Cells per Second) (local)
4.79261 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
292.517 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517601 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
265457989 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.65458e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.13 ACPST (Averaged number of clusters per Simulation Timestep)
9.62502 MCPS (Million Cells per Second) (local)
4.81251 MCPSPT (Million Clusters per Second per Thread)
81.0113 EDMBPT (CellData Megabyte per Timestep (RW))
293.732 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.519751 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
265839977 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.6584e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.99 ACPST (Averaged number of clusters per Simulation Timestep)
9.63887 MCPS (Million Cells per Second) (local)
4.81944 MCPSPT (Million Clusters per Second per Thread)
81.1279 EDMBPT (CellData Megabyte per Timestep (RW))
294.155 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.520499 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
266020823 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.66021e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.29 ACPST (Averaged number of clusters per Simulation Timestep)
9.64543 MCPS (Million Cells per Second) (local)
4.82272 MCPSPT (Million Clusters per Second per Thread)
81.1831 EDMBPT (CellData Megabyte per Timestep (RW))
294.355 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.520853 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264364032 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64364e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.84 ACPST (Averaged number of clusters per Simulation Timestep)
9.58536 MCPS (Million Cells per Second) (local)
4.79268 MCPSPT (Million Clusters per Second per Thread)
80.6775 EDMBPT (CellData Megabyte per Timestep (RW))
292.522 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517609 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264359936 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
9.58521 MCPS (Million Cells per Second) (local)
4.79261 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
292.517 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517601 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264402944 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
9.58677 MCPS (Million Cells per Second) (local)
4.79338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
292.565 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517686 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264321024 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
9.5838 MCPS (Million Cells per Second) (local)
4.7919 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
292.474 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517525 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264402944 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
9.58677 MCPS (Million Cells per Second) (local)
4.79338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
292.565 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517686 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264804969 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64805e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.1 ACPST (Averaged number of clusters per Simulation Timestep)
9.60135 MCPS (Million Cells per Second) (local)
4.80067 MCPSPT (Million Clusters per Second per Thread)
80.8121 EDMBPT (CellData Megabyte per Timestep (RW))
293.01 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.518473 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
266659796 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.6666e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.49 ACPST (Averaged number of clusters per Simulation Timestep)
9.6686 MCPS (Million Cells per Second) (local)
4.8343 MCPSPT (Million Clusters per Second per Thread)
81.3781 EDMBPT (CellData Megabyte per Timestep (RW))
295.062 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.522104 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264381270 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64381e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.05 ACPST (Averaged number of clusters per Simulation Timestep)
9.58598 MCPS (Million Cells per Second) (local)
4.79299 MCPSPT (Million Clusters per Second per Thread)
80.6828 EDMBPT (CellData Megabyte per Timestep (RW))
292.541 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517643 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264321024 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
9.5838 MCPS (Million Cells per Second) (local)
4.7919 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
292.474 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517525 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264402944 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
9.58677 MCPS (Million Cells per Second) (local)
4.79338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
292.565 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517686 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
266088272 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.66088e+06 CPST (Cells Processed in Average per Simulation Timestep)
1280.76 ACPST (Averaged number of clusters per Simulation Timestep)
9.64788 MCPS (Million Cells per Second) (local)
4.82394 MCPSPT (Million Clusters per Second per Thread)
81.2037 EDMBPT (CellData Megabyte per Timestep (RW))
294.43 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.520985 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.58 RT (REAL_TIME)
264359936 CP (Cells processed)
0.2758 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
9.58521 MCPS (Million Cells per Second) (local)
4.79261 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
292.517 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.517601 GFLOPS


++++++++++ SUMMARY ++++++++++
153.682 GMCPS (Global Million Cells per Second) (global)
76.841 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
