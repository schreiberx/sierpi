Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 2: more nodes than initial clusters available!
3: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264910848 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.51 ACPST (Averaged number of clusters per Simulation Timestep)
7.1912 MCPS (Million Cells per Second) (local)
3.5956 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
219.458 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388325 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264900933 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.12 ACPST (Averaged number of clusters per Simulation Timestep)
7.19093 MCPS (Million Cells per Second) (local)
3.59547 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
219.45 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.38831 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264918377 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64918e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.31 ACPST (Averaged number of clusters per Simulation Timestep)
7.1914 MCPS (Million Cells per Second) (local)
3.5957 MCPSPT (Million Clusters per Second per Thread)
80.8467 EDMBPT (CellData Megabyte per Timestep (RW))
219.464 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388335 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8385 RT (REAL_TIME)
264906711 CP (Cells processed)
0.368385 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1282.32 ACPST (Averaged number of clusters per Simulation Timestep)
7.19104 MCPS (Million Cells per Second) (local)
3.59552 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
219.453 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388316 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8385 RT (REAL_TIME)
264904704 CP (Cells processed)
0.368385 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.48 ACPST (Averaged number of clusters per Simulation Timestep)
7.19098 MCPS (Million Cells per Second) (local)
3.59549 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
219.451 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388313 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8385 RT (REAL_TIME)
264912896 CP (Cells processed)
0.368385 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
7.1912 MCPS (Million Cells per Second) (local)
3.5956 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
219.458 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388325 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8384 RT (REAL_TIME)
264908800 CP (Cells processed)
0.368384 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
7.1911 MCPS (Million Cells per Second) (local)
3.59555 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
219.455 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.38832 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264914944 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.53 ACPST (Averaged number of clusters per Simulation Timestep)
7.19131 MCPS (Million Cells per Second) (local)
3.59566 MCPSPT (Million Clusters per Second per Thread)
80.8456 EDMBPT (CellData Megabyte per Timestep (RW))
219.461 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388331 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264900608 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.46 ACPST (Averaged number of clusters per Simulation Timestep)
7.19092 MCPS (Million Cells per Second) (local)
3.59546 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
219.45 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.38831 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264923229 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64923e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.35 ACPST (Averaged number of clusters per Simulation Timestep)
7.19154 MCPS (Million Cells per Second) (local)
3.59577 MCPSPT (Million Clusters per Second per Thread)
80.8482 EDMBPT (CellData Megabyte per Timestep (RW))
219.468 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388343 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264896343 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.05 ACPST (Averaged number of clusters per Simulation Timestep)
7.19081 MCPS (Million Cells per Second) (local)
3.5954 MCPSPT (Million Clusters per Second per Thread)
80.8399 EDMBPT (CellData Megabyte per Timestep (RW))
219.446 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388304 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264912351 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
1292.56 ACPST (Averaged number of clusters per Simulation Timestep)
7.19124 MCPS (Million Cells per Second) (local)
3.59562 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
219.459 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388327 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264906752 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.49 ACPST (Averaged number of clusters per Simulation Timestep)
7.19109 MCPS (Million Cells per Second) (local)
3.59555 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
219.455 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388319 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264912896 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
7.19126 MCPS (Million Cells per Second) (local)
3.59563 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
219.46 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388328 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264908624 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1287.58 ACPST (Averaged number of clusters per Simulation Timestep)
7.19114 MCPS (Million Cells per Second) (local)
3.59557 MCPSPT (Million Clusters per Second per Thread)
80.8437 EDMBPT (CellData Megabyte per Timestep (RW))
219.456 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388322 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
36.8382 RT (REAL_TIME)
264908800 CP (Cells processed)
0.368382 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
7.19115 MCPS (Million Cells per Second) (local)
3.59557 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
219.456 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.388322 GFLOPS


++++++++++ SUMMARY ++++++++++
115.058 GMCPS (Global Million Cells per Second) (global)
57.5292 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
