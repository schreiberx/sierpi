Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
556.39 RT (REAL_TIME)
4238547816 CP (Cells processed)
5.5639 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20608.5 ACPST (Averaged number of clusters per Simulation Timestep)
7.61794 MCPS (Million Cells per Second) (local)
3.80897 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
232.481 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.411369 GFLOPS

++++++++++ SUMMARY ++++++++++
7.61794 GMCPS (Global Million Cells per Second) (global)
3.80897 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
