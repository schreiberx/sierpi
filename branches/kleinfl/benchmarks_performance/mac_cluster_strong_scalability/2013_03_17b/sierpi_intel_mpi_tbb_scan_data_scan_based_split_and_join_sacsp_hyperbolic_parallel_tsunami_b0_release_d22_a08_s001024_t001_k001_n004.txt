Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.615 RT (REAL_TIME)
1059639943 CP (Cells processed)
2.20615 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
37.56 ACPST (Averaged number of clusters per Simulation Timestep)
4.80312 MCPS (Million Cells per Second) (local)
4.80312 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
146.58 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259369 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.615 RT (REAL_TIME)
1059631520 CP (Cells processed)
2.20615 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
40.47 ACPST (Averaged number of clusters per Simulation Timestep)
4.80308 MCPS (Million Cells per Second) (local)
4.80308 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
146.579 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259367 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.615 RT (REAL_TIME)
1059645683 CP (Cells processed)
2.20615 ASPT (Averaged Seconds per Timestep)
1.05965e+07 CPST (Cells Processed in Average per Simulation Timestep)
25.23 ACPST (Averaged number of clusters per Simulation Timestep)
4.80315 MCPS (Million Cells per Second) (local)
4.80315 MCPSPT (Million Clusters per Second per Thread)
323.378 EDMBPT (CellData Megabyte per Timestep (RW))
146.58 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.25937 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
220.615 RT (REAL_TIME)
1059630672 CP (Cells processed)
2.20615 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
47.05 ACPST (Averaged number of clusters per Simulation Timestep)
4.80308 MCPS (Million Cells per Second) (local)
4.80308 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
146.578 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259366 GFLOPS


++++++++++ SUMMARY ++++++++++
19.2124 GMCPS (Global Million Cells per Second) (global)
19.2124 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
