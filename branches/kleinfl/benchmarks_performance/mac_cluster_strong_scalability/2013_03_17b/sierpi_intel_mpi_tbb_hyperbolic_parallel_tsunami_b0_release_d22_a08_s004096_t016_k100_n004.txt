Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
21.7468 RT (REAL_TIME)
1066792581 CP (Cells processed)
0.217468 ASPT (Averaged Seconds per Timestep)
1.06679e+07 CPST (Cells Processed in Average per Simulation Timestep)
5121.44 ACPST (Averaged number of clusters per Simulation Timestep)
49.0552 MCPS (Million Cells per Second) (local)
3.06595 MCPSPT (Million Clusters per Second per Thread)
325.559 EDMBPT (CellData Megabyte per Timestep (RW))
1497.05 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.64898 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
21.7468 RT (REAL_TIME)
1052024832 CP (Cells processed)
0.217468 ASPT (Averaged Seconds per Timestep)
1.05202e+07 CPST (Cells Processed in Average per Simulation Timestep)
5136.84 ACPST (Averaged number of clusters per Simulation Timestep)
48.3761 MCPS (Million Cells per Second) (local)
3.02351 MCPSPT (Million Clusters per Second per Thread)
321.053 EDMBPT (CellData Megabyte per Timestep (RW))
1476.32 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.61231 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
21.7468 RT (REAL_TIME)
1061729683 CP (Cells processed)
0.217468 ASPT (Averaged Seconds per Timestep)
1.06173e+07 CPST (Cells Processed in Average per Simulation Timestep)
5135.14 ACPST (Averaged number of clusters per Simulation Timestep)
48.8224 MCPS (Million Cells per Second) (local)
3.0514 MCPSPT (Million Clusters per Second per Thread)
324.014 EDMBPT (CellData Megabyte per Timestep (RW))
1489.94 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.63641 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
21.7468 RT (REAL_TIME)
1058000720 CP (Cells processed)
0.217468 ASPT (Averaged Seconds per Timestep)
1.058e+07 CPST (Cells Processed in Average per Simulation Timestep)
5128.59 ACPST (Averaged number of clusters per Simulation Timestep)
48.6509 MCPS (Million Cells per Second) (local)
3.04068 MCPSPT (Million Clusters per Second per Thread)
322.876 EDMBPT (CellData Megabyte per Timestep (RW))
1484.71 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.62715 GFLOPS


++++++++++ SUMMARY ++++++++++
194.905 GMCPS (Global Million Cells per Second) (global)
12.1815 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
