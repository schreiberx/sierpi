Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
269.225 RT (REAL_TIME)
1066792581 CP (Cells processed)
2.69225 ASPT (Averaged Seconds per Timestep)
1.06679e+07 CPST (Cells Processed in Average per Simulation Timestep)
5121.44 ACPST (Averaged number of clusters per Simulation Timestep)
3.96246 MCPS (Million Cells per Second) (local)
3.96246 MCPSPT (Million Clusters per Second per Thread)
325.559 EDMBPT (CellData Megabyte per Timestep (RW))
120.925 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.213973 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
269.225 RT (REAL_TIME)
1052024832 CP (Cells processed)
2.69225 ASPT (Averaged Seconds per Timestep)
1.05202e+07 CPST (Cells Processed in Average per Simulation Timestep)
5136.84 ACPST (Averaged number of clusters per Simulation Timestep)
3.90761 MCPS (Million Cells per Second) (local)
3.90761 MCPSPT (Million Clusters per Second per Thread)
321.053 EDMBPT (CellData Megabyte per Timestep (RW))
119.251 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.211011 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
269.225 RT (REAL_TIME)
1061729683 CP (Cells processed)
2.69225 ASPT (Averaged Seconds per Timestep)
1.06173e+07 CPST (Cells Processed in Average per Simulation Timestep)
5135.14 ACPST (Averaged number of clusters per Simulation Timestep)
3.94365 MCPS (Million Cells per Second) (local)
3.94365 MCPSPT (Million Clusters per Second per Thread)
324.014 EDMBPT (CellData Megabyte per Timestep (RW))
120.351 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212957 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
269.225 RT (REAL_TIME)
1058000720 CP (Cells processed)
2.69225 ASPT (Averaged Seconds per Timestep)
1.058e+07 CPST (Cells Processed in Average per Simulation Timestep)
5128.59 ACPST (Averaged number of clusters per Simulation Timestep)
3.9298 MCPS (Million Cells per Second) (local)
3.9298 MCPSPT (Million Clusters per Second per Thread)
322.876 EDMBPT (CellData Megabyte per Timestep (RW))
119.928 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212209 GFLOPS


++++++++++ SUMMARY ++++++++++
15.7435 GMCPS (Global Million Cells per Second) (global)
15.7435 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
