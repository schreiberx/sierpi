Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK Warning from RANK 13: more nodes than initial clusters available!
2: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264910848 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.51 ACPST (Averaged number of clusters per Simulation Timestep)
4.90641 MCPS (Million Cells per Second) (local)
4.90641 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
149.732 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264946 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264900933 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.12 ACPST (Averaged number of clusters per Simulation Timestep)
4.90622 MCPS (Million Cells per Second) (local)
4.90622 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
149.726 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264936 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264918377 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64918e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.31 ACPST (Averaged number of clusters per Simulation Timestep)
4.90654 MCPS (Million Cells per Second) (local)
4.90654 MCPSPT (Million Clusters per Second per Thread)
80.8467 EDMBPT (CellData Megabyte per Timestep (RW))
149.736 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264953 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9932 RT (REAL_TIME)
264906711 CP (Cells processed)
0.539932 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1282.32 ACPST (Averaged number of clusters per Simulation Timestep)
4.9063 MCPS (Million Cells per Second) (local)
4.9063 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
149.728 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.26494 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9932 RT (REAL_TIME)
264904704 CP (Cells processed)
0.539932 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.48 ACPST (Averaged number of clusters per Simulation Timestep)
4.90626 MCPS (Million Cells per Second) (local)
4.90626 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
149.727 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264938 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9932 RT (REAL_TIME)
264912896 CP (Cells processed)
0.539932 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.90641 MCPS (Million Cells per Second) (local)
4.90641 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
149.732 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264946 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9932 RT (REAL_TIME)
264908800 CP (Cells processed)
0.539932 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
4.90634 MCPS (Million Cells per Second) (local)
4.90634 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
149.73 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264942 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264914944 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.53 ACPST (Averaged number of clusters per Simulation Timestep)
4.90648 MCPS (Million Cells per Second) (local)
4.90648 MCPSPT (Million Clusters per Second per Thread)
80.8456 EDMBPT (CellData Megabyte per Timestep (RW))
149.734 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.26495 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264900608 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.46 ACPST (Averaged number of clusters per Simulation Timestep)
4.90622 MCPS (Million Cells per Second) (local)
4.90622 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
149.726 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264936 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264923229 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64923e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.35 ACPST (Averaged number of clusters per Simulation Timestep)
4.90663 MCPS (Million Cells per Second) (local)
4.90663 MCPSPT (Million Clusters per Second per Thread)
80.8482 EDMBPT (CellData Megabyte per Timestep (RW))
149.739 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264958 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264896343 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.05 ACPST (Averaged number of clusters per Simulation Timestep)
4.90614 MCPS (Million Cells per Second) (local)
4.90614 MCPSPT (Million Clusters per Second per Thread)
80.8399 EDMBPT (CellData Megabyte per Timestep (RW))
149.723 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264931 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264912351 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
1292.56 ACPST (Averaged number of clusters per Simulation Timestep)
4.90643 MCPS (Million Cells per Second) (local)
4.90643 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
149.732 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264947 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264906752 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.49 ACPST (Averaged number of clusters per Simulation Timestep)
4.90633 MCPS (Million Cells per Second) (local)
4.90633 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
149.729 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264942 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264912896 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.90644 MCPS (Million Cells per Second) (local)
4.90644 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
149.733 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264948 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264908624 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1287.58 ACPST (Averaged number of clusters per Simulation Timestep)
4.90636 MCPS (Million Cells per Second) (local)
4.90636 MCPSPT (Million Clusters per Second per Thread)
80.8437 EDMBPT (CellData Megabyte per Timestep (RW))
149.73 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264944 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.9929 RT (REAL_TIME)
264908800 CP (Cells processed)
0.539929 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
4.90637 MCPS (Million Cells per Second) (local)
4.90637 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
149.73 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.264944 GFLOPS


++++++++++ SUMMARY ++++++++++
78.5019 GMCPS (Global Million Cells per Second) (global)
78.5019 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
