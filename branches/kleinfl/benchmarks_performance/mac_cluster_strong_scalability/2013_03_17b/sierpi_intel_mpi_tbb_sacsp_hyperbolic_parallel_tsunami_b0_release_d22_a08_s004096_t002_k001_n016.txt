Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 7Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264910848 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.51 ACPST (Averaged number of clusters per Simulation Timestep)
9.54903 MCPS (Million Cells per Second) (local)
4.77451 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
291.413 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515647 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264900933 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.12 ACPST (Averaged number of clusters per Simulation Timestep)
9.54867 MCPS (Million Cells per Second) (local)
4.77433 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
291.402 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515628 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7423 RT (REAL_TIME)
264918377 CP (Cells processed)
0.277423 ASPT (Averaged Seconds per Timestep)
2.64918e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.31 ACPST (Averaged number of clusters per Simulation Timestep)
9.54927 MCPS (Million Cells per Second) (local)
4.77463 MCPSPT (Million Clusters per Second per Thread)
80.8467 EDMBPT (CellData Megabyte per Timestep (RW))
291.421 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515661 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7424 RT (REAL_TIME)
264906711 CP (Cells processed)
0.277424 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1282.32 ACPST (Averaged number of clusters per Simulation Timestep)
9.5488 MCPS (Million Cells per Second) (local)
4.7744 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
291.406 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515635 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7424 RT (REAL_TIME)
264904704 CP (Cells processed)
0.277424 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.48 ACPST (Averaged number of clusters per Simulation Timestep)
9.54872 MCPS (Million Cells per Second) (local)
4.77436 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
291.404 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515631 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7424 RT (REAL_TIME)
264912896 CP (Cells processed)
0.277424 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
9.54901 MCPS (Million Cells per Second) (local)
4.77451 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
291.413 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515647 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7424 RT (REAL_TIME)
264908800 CP (Cells processed)
0.277424 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
9.54887 MCPS (Million Cells per Second) (local)
4.77443 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
291.408 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515639 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264914944 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.53 ACPST (Averaged number of clusters per Simulation Timestep)
9.54917 MCPS (Million Cells per Second) (local)
4.77459 MCPSPT (Million Clusters per Second per Thread)
80.8456 EDMBPT (CellData Megabyte per Timestep (RW))
291.418 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515655 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264900608 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.46 ACPST (Averaged number of clusters per Simulation Timestep)
9.54865 MCPS (Million Cells per Second) (local)
4.77433 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
291.402 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515627 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264923229 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64923e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.35 ACPST (Averaged number of clusters per Simulation Timestep)
9.54947 MCPS (Million Cells per Second) (local)
4.77474 MCPSPT (Million Clusters per Second per Thread)
80.8482 EDMBPT (CellData Megabyte per Timestep (RW))
291.427 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515671 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264896343 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.05 ACPST (Averaged number of clusters per Simulation Timestep)
9.5485 MCPS (Million Cells per Second) (local)
4.77425 MCPSPT (Million Clusters per Second per Thread)
80.8399 EDMBPT (CellData Megabyte per Timestep (RW))
291.397 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515619 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264912351 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
1292.56 ACPST (Averaged number of clusters per Simulation Timestep)
9.54908 MCPS (Million Cells per Second) (local)
4.77454 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
291.415 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.51565 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264906752 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.49 ACPST (Averaged number of clusters per Simulation Timestep)
9.54888 MCPS (Million Cells per Second) (local)
4.77444 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
291.409 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515639 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264912896 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
9.5491 MCPS (Million Cells per Second) (local)
4.77455 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
291.415 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515651 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264908624 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1287.58 ACPST (Averaged number of clusters per Simulation Timestep)
9.54894 MCPS (Million Cells per Second) (local)
4.77447 MCPSPT (Million Clusters per Second per Thread)
80.8437 EDMBPT (CellData Megabyte per Timestep (RW))
291.411 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515643 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7422 RT (REAL_TIME)
264908800 CP (Cells processed)
0.277422 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
9.54895 MCPS (Million Cells per Second) (local)
4.77448 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
291.411 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.515643 GFLOPS


++++++++++ SUMMARY ++++++++++
152.783 GMCPS (Global Million Cells per Second) (global)
76.3916 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
