Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!

Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264419216 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64419e+06 CPST (Cells Processed in Average per Simulation Timestep)
48.32 ACPST (Averaged number of clusters per Simulation Timestep)
4.05869 MCPS (Million Cells per Second) (local)
4.05869 MCPSPT (Million Clusters per Second per Thread)
80.6943 EDMBPT (CellData Megabyte per Timestep (RW))
123.861 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219169 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
265434783 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.65435e+06 CPST (Cells Processed in Average per Simulation Timestep)
26.62 ACPST (Averaged number of clusters per Simulation Timestep)
4.07428 MCPS (Million Cells per Second) (local)
4.07428 MCPSPT (Million Clusters per Second per Thread)
81.0043 EDMBPT (CellData Megabyte per Timestep (RW))
124.337 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220011 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
266124805 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.66125e+06 CPST (Cells Processed in Average per Simulation Timestep)
35.74 ACPST (Averaged number of clusters per Simulation Timestep)
4.08487 MCPS (Million Cells per Second) (local)
4.08487 MCPSPT (Million Clusters per Second per Thread)
81.2148 EDMBPT (CellData Megabyte per Timestep (RW))
124.66 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220583 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
266483859 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.66484e+06 CPST (Cells Processed in Average per Simulation Timestep)
34.44 ACPST (Averaged number of clusters per Simulation Timestep)
4.09038 MCPS (Million Cells per Second) (local)
4.09038 MCPSPT (Million Clusters per Second per Thread)
81.3244 EDMBPT (CellData Megabyte per Timestep (RW))
124.829 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220881 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
263919532 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.6392e+06 CPST (Cells Processed in Average per Simulation Timestep)
36.94 ACPST (Averaged number of clusters per Simulation Timestep)
4.05102 MCPS (Million Cells per Second) (local)
4.05102 MCPSPT (Million Clusters per Second per Thread)
80.5418 EDMBPT (CellData Megabyte per Timestep (RW))
123.627 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.218755 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
263935916 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.63936e+06 CPST (Cells Processed in Average per Simulation Timestep)
34.94 ACPST (Averaged number of clusters per Simulation Timestep)
4.05127 MCPS (Million Cells per Second) (local)
4.05127 MCPSPT (Million Clusters per Second per Thread)
80.5468 EDMBPT (CellData Megabyte per Timestep (RW))
123.635 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.218769 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264476560 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64477e+06 CPST (Cells Processed in Average per Simulation Timestep)
36 ACPST (Averaged number of clusters per Simulation Timestep)
4.05957 MCPS (Million Cells per Second) (local)
4.05957 MCPSPT (Million Clusters per Second per Thread)
80.7118 EDMBPT (CellData Megabyte per Timestep (RW))
123.888 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219217 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264341392 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64341e+06 CPST (Cells Processed in Average per Simulation Timestep)
54.64 ACPST (Averaged number of clusters per Simulation Timestep)
4.0575 MCPS (Million Cells per Second) (local)
4.0575 MCPSPT (Million Clusters per Second per Thread)
80.6706 EDMBPT (CellData Megabyte per Timestep (RW))
123.825 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219105 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264677292 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64677e+06 CPST (Cells Processed in Average per Simulation Timestep)
38.3 ACPST (Averaged number of clusters per Simulation Timestep)
4.06265 MCPS (Million Cells per Second) (local)
4.06265 MCPSPT (Million Clusters per Second per Thread)
80.7731 EDMBPT (CellData Megabyte per Timestep (RW))
123.982 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219383 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264350319 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.6435e+06 CPST (Cells Processed in Average per Simulation Timestep)
37.02 ACPST (Averaged number of clusters per Simulation Timestep)
4.05763 MCPS (Million Cells per Second) (local)
4.05763 MCPSPT (Million Clusters per Second per Thread)
80.6733 EDMBPT (CellData Megabyte per Timestep (RW))
123.829 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219112 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
266475373 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.66475e+06 CPST (Cells Processed in Average per Simulation Timestep)
16.84 ACPST (Averaged number of clusters per Simulation Timestep)
4.09025 MCPS (Million Cells per Second) (local)
4.09025 MCPSPT (Million Clusters per Second per Thread)
81.3218 EDMBPT (CellData Megabyte per Timestep (RW))
124.825 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220874 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264828979 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64829e+06 CPST (Cells Processed in Average per Simulation Timestep)
36.54 ACPST (Averaged number of clusters per Simulation Timestep)
4.06498 MCPS (Million Cells per Second) (local)
4.06498 MCPSPT (Million Clusters per Second per Thread)
80.8194 EDMBPT (CellData Megabyte per Timestep (RW))
124.053 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219509 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264294288 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64294e+06 CPST (Cells Processed in Average per Simulation Timestep)
55.58 ACPST (Averaged number of clusters per Simulation Timestep)
4.05677 MCPS (Million Cells per Second) (local)
4.05677 MCPSPT (Million Clusters per Second per Thread)
80.6562 EDMBPT (CellData Megabyte per Timestep (RW))
123.803 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219066 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264601488 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64601e+06 CPST (Cells Processed in Average per Simulation Timestep)
40.1 ACPST (Averaged number of clusters per Simulation Timestep)
4.06149 MCPS (Million Cells per Second) (local)
4.06149 MCPSPT (Million Clusters per Second per Thread)
80.75 EDMBPT (CellData Megabyte per Timestep (RW))
123.947 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21932 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
265764800 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.65765e+06 CPST (Cells Processed in Average per Simulation Timestep)
19.1 ACPST (Averaged number of clusters per Simulation Timestep)
4.07934 MCPS (Million Cells per Second) (local)
4.07934 MCPSPT (Million Clusters per Second per Thread)
81.105 EDMBPT (CellData Megabyte per Timestep (RW))
124.492 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220285 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.1489 RT (REAL_TIME)
264419216 CP (Cells processed)
0.651489 ASPT (Averaged Seconds per Timestep)
2.64419e+06 CPST (Cells Processed in Average per Simulation Timestep)
48.32 ACPST (Averaged number of clusters per Simulation Timestep)
4.05869 MCPS (Million Cells per Second) (local)
4.05869 MCPSPT (Million Clusters per Second per Thread)
80.6943 EDMBPT (CellData Megabyte per Timestep (RW))
123.861 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219169 GFLOPS


++++++++++ SUMMARY ++++++++++
65.0594 GMCPS (Global Million Cells per Second) (global)
65.0594 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
