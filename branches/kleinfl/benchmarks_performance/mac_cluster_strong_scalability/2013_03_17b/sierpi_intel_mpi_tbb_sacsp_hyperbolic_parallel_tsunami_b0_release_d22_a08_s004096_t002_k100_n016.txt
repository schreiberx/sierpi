Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!

Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
262922240 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
9.46913 MCPS (Million Cells per Second) (local)
4.73457 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
288.975 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511333 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
266895685 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.66896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.45 ACPST (Averaged number of clusters per Simulation Timestep)
9.61224 MCPS (Million Cells per Second) (local)
4.80612 MCPSPT (Million Clusters per Second per Thread)
81.4501 EDMBPT (CellData Megabyte per Timestep (RW))
293.342 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.519061 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
268119401 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.68119e+06 CPST (Cells Processed in Average per Simulation Timestep)
1278.28 ACPST (Averaged number of clusters per Simulation Timestep)
9.65631 MCPS (Million Cells per Second) (local)
4.82815 MCPSPT (Million Clusters per Second per Thread)
81.8235 EDMBPT (CellData Megabyte per Timestep (RW))
294.687 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.521441 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
268855255 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.68855e+06 CPST (Cells Processed in Average per Simulation Timestep)
1272.91 ACPST (Averaged number of clusters per Simulation Timestep)
9.68281 MCPS (Million Cells per Second) (local)
4.84141 MCPSPT (Million Clusters per Second per Thread)
82.0481 EDMBPT (CellData Megabyte per Timestep (RW))
295.496 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.522872 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
263090176 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
9.47518 MCPS (Million Cells per Second) (local)
4.73759 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
289.16 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.51166 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
262922240 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
9.46913 MCPS (Million Cells per Second) (local)
4.73457 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
288.975 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511333 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
263090176 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
9.47518 MCPS (Million Cells per Second) (local)
4.73759 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
289.16 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.51166 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
262922240 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
9.46913 MCPS (Million Cells per Second) (local)
4.73457 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
288.975 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511333 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
263090176 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
9.47518 MCPS (Million Cells per Second) (local)
4.73759 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
289.16 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.51166 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
264471532 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.64472e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.72 ACPST (Averaged number of clusters per Simulation Timestep)
9.52493 MCPS (Million Cells per Second) (local)
4.76247 MCPSPT (Million Clusters per Second per Thread)
80.7103 EDMBPT (CellData Megabyte per Timestep (RW))
290.678 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.514346 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
271078985 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.71079e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.36 ACPST (Averaged number of clusters per Simulation Timestep)
9.7629 MCPS (Million Cells per Second) (local)
4.88145 MCPSPT (Million Clusters per Second per Thread)
82.7267 EDMBPT (CellData Megabyte per Timestep (RW))
297.94 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.527197 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
263088990 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.63089e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.44 ACPST (Averaged number of clusters per Simulation Timestep)
9.47514 MCPS (Million Cells per Second) (local)
4.73757 MCPSPT (Million Clusters per Second per Thread)
80.2884 EDMBPT (CellData Megabyte per Timestep (RW))
289.158 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511658 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
262922240 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
9.46913 MCPS (Million Cells per Second) (local)
4.73457 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
288.975 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511333 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
263090176 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
9.47518 MCPS (Million Cells per Second) (local)
4.73759 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
289.16 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.51166 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
269066064 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.69066e+06 CPST (Cells Processed in Average per Simulation Timestep)
1276.37 ACPST (Averaged number of clusters per Simulation Timestep)
9.6904 MCPS (Million Cells per Second) (local)
4.8452 MCPSPT (Million Clusters per Second per Thread)
82.1124 EDMBPT (CellData Megabyte per Timestep (RW))
295.728 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.523282 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
27.7662 RT (REAL_TIME)
262922240 CP (Cells processed)
0.277662 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
9.46913 MCPS (Million Cells per Second) (local)
4.73457 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
288.975 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.511333 GFLOPS


++++++++++ SUMMARY ++++++++++
152.651 GMCPS (Global Million Cells per Second) (global)
76.3256 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
