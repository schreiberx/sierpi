Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!Warning from RANK 13: more nodes than initial clusters available!

Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264910848 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.51 ACPST (Averaged number of clusters per Simulation Timestep)
17.385 MCPS (Million Cells per Second) (local)
4.34625 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
530.548 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.93879 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264900933 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.12 ACPST (Averaged number of clusters per Simulation Timestep)
17.3843 MCPS (Million Cells per Second) (local)
4.34609 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
530.528 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938754 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264918377 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64918e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.31 ACPST (Averaged number of clusters per Simulation Timestep)
17.3854 MCPS (Million Cells per Second) (local)
4.34636 MCPSPT (Million Clusters per Second per Thread)
80.8467 EDMBPT (CellData Megabyte per Timestep (RW))
530.562 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938814 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2383 RT (REAL_TIME)
264906711 CP (Cells processed)
0.152383 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1282.32 ACPST (Averaged number of clusters per Simulation Timestep)
17.3842 MCPS (Million Cells per Second) (local)
4.34606 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
530.525 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938748 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2383 RT (REAL_TIME)
264904704 CP (Cells processed)
0.152383 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.48 ACPST (Averaged number of clusters per Simulation Timestep)
17.3841 MCPS (Million Cells per Second) (local)
4.34603 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
530.521 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938742 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2381 RT (REAL_TIME)
264912896 CP (Cells processed)
0.152381 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
17.3849 MCPS (Million Cells per Second) (local)
4.34623 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
530.545 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938785 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2381 RT (REAL_TIME)
264908800 CP (Cells processed)
0.152381 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
17.3847 MCPS (Million Cells per Second) (local)
4.34616 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
530.538 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938771 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264914944 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.53 ACPST (Averaged number of clusters per Simulation Timestep)
17.3853 MCPS (Million Cells per Second) (local)
4.34632 MCPSPT (Million Clusters per Second per Thread)
80.8456 EDMBPT (CellData Megabyte per Timestep (RW))
530.556 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938804 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264900608 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.46 ACPST (Averaged number of clusters per Simulation Timestep)
17.3843 MCPS (Million Cells per Second) (local)
4.34608 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
530.528 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938754 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264923229 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64923e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.35 ACPST (Averaged number of clusters per Simulation Timestep)
17.3858 MCPS (Million Cells per Second) (local)
4.34646 MCPSPT (Million Clusters per Second per Thread)
80.8482 EDMBPT (CellData Megabyte per Timestep (RW))
530.573 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938835 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264896343 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.05 ACPST (Averaged number of clusters per Simulation Timestep)
17.3841 MCPS (Million Cells per Second) (local)
4.34602 MCPSPT (Million Clusters per Second per Thread)
80.8399 EDMBPT (CellData Megabyte per Timestep (RW))
530.519 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938739 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264912351 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
1292.56 ACPST (Averaged number of clusters per Simulation Timestep)
17.3851 MCPS (Million Cells per Second) (local)
4.34628 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
530.552 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938796 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264906752 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.49 ACPST (Averaged number of clusters per Simulation Timestep)
17.3847 MCPS (Million Cells per Second) (local)
4.34618 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
530.54 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938776 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264912896 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
17.3851 MCPS (Million Cells per Second) (local)
4.34628 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
530.552 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938797 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264908624 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1287.58 ACPST (Averaged number of clusters per Simulation Timestep)
17.3849 MCPS (Million Cells per Second) (local)
4.34621 MCPSPT (Million Clusters per Second per Thread)
80.8437 EDMBPT (CellData Megabyte per Timestep (RW))
530.544 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938782 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
15.2379 RT (REAL_TIME)
264908800 CP (Cells processed)
0.152379 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
17.3849 MCPS (Million Cells per Second) (local)
4.34622 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
530.544 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.938783 GFLOPS


++++++++++ SUMMARY ++++++++++
278.157 GMCPS (Global Million Cells per Second) (global)
69.5392 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
