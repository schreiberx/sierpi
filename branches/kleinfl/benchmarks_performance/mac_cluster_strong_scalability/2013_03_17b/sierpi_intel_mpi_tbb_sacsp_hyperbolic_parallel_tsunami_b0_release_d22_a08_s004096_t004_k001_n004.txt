Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
54.7298 RT (REAL_TIME)
1059636869 CP (Cells processed)
0.547298 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5158.26 ACPST (Averaged number of clusters per Simulation Timestep)
19.3612 MCPS (Million Cells per Second) (local)
4.84031 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
590.858 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.04551 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
54.7298 RT (REAL_TIME)
1059641344 CP (Cells processed)
0.547298 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5174.03 ACPST (Averaged number of clusters per Simulation Timestep)
19.3613 MCPS (Million Cells per Second) (local)
4.84033 MCPSPT (Million Clusters per Second per Thread)
323.377 EDMBPT (CellData Megabyte per Timestep (RW))
590.861 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.04551 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
54.7294 RT (REAL_TIME)
1059632531 CP (Cells processed)
0.547294 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
5165.42 ACPST (Averaged number of clusters per Simulation Timestep)
19.3613 MCPS (Million Cells per Second) (local)
4.84033 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
590.861 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.04551 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
54.7294 RT (REAL_TIME)
1059637072 CP (Cells processed)
0.547294 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5168.09 ACPST (Averaged number of clusters per Simulation Timestep)
19.3614 MCPS (Million Cells per Second) (local)
4.84035 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
590.863 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.04552 GFLOPS


++++++++++ SUMMARY ++++++++++
77.4453 GMCPS (Global Million Cells per Second) (global)
19.3613 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
