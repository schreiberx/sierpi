Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.1688 RT (REAL_TIME)
1066792581 CP (Cells processed)
0.281688 ASPT (Averaged Seconds per Timestep)
1.06679e+07 CPST (Cells Processed in Average per Simulation Timestep)
5121.44 ACPST (Averaged number of clusters per Simulation Timestep)
37.8714 MCPS (Million Cells per Second) (local)
4.73393 MCPSPT (Million Clusters per Second per Thread)
325.559 EDMBPT (CellData Megabyte per Timestep (RW))
1155.74 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.04506 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.1688 RT (REAL_TIME)
1052024832 CP (Cells processed)
0.281688 ASPT (Averaged Seconds per Timestep)
1.05202e+07 CPST (Cells Processed in Average per Simulation Timestep)
5136.84 ACPST (Averaged number of clusters per Simulation Timestep)
37.3472 MCPS (Million Cells per Second) (local)
4.6684 MCPSPT (Million Clusters per Second per Thread)
321.053 EDMBPT (CellData Megabyte per Timestep (RW))
1139.75 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.01675 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.1688 RT (REAL_TIME)
1061729683 CP (Cells processed)
0.281688 ASPT (Averaged Seconds per Timestep)
1.06173e+07 CPST (Cells Processed in Average per Simulation Timestep)
5135.14 ACPST (Averaged number of clusters per Simulation Timestep)
37.6917 MCPS (Million Cells per Second) (local)
4.71146 MCPSPT (Million Clusters per Second per Thread)
324.014 EDMBPT (CellData Megabyte per Timestep (RW))
1150.26 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.03535 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.1688 RT (REAL_TIME)
1058000720 CP (Cells processed)
0.281688 ASPT (Averaged Seconds per Timestep)
1.058e+07 CPST (Cells Processed in Average per Simulation Timestep)
5128.59 ACPST (Averaged number of clusters per Simulation Timestep)
37.5593 MCPS (Million Cells per Second) (local)
4.69491 MCPSPT (Million Clusters per Second per Thread)
322.876 EDMBPT (CellData Megabyte per Timestep (RW))
1146.22 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.0282 GFLOPS


++++++++++ SUMMARY ++++++++++
150.47 GMCPS (Global Million Cells per Second) (global)
18.8087 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
