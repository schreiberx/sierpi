Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.2231 RT (REAL_TIME)
1059636869 CP (Cells processed)
0.222231 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5158.26 ACPST (Averaged number of clusters per Simulation Timestep)
47.6817 MCPS (Million Cells per Second) (local)
2.98011 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1455.13 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.57481 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.2231 RT (REAL_TIME)
1059641344 CP (Cells processed)
0.222231 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5174.03 ACPST (Averaged number of clusters per Simulation Timestep)
47.6821 MCPS (Million Cells per Second) (local)
2.98013 MCPSPT (Million Clusters per Second per Thread)
323.377 EDMBPT (CellData Megabyte per Timestep (RW))
1455.14 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.57483 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.2226 RT (REAL_TIME)
1059632531 CP (Cells processed)
0.222226 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
5165.42 ACPST (Averaged number of clusters per Simulation Timestep)
47.6827 MCPS (Million Cells per Second) (local)
2.98017 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
1455.16 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.57486 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.2226 RT (REAL_TIME)
1059637072 CP (Cells processed)
0.222226 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5168.09 ACPST (Averaged number of clusters per Simulation Timestep)
47.6829 MCPS (Million Cells per Second) (local)
2.98018 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1455.17 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.57487 GFLOPS


++++++++++ SUMMARY ++++++++++
190.729 GMCPS (Global Million Cells per Second) (global)
11.9206 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
