#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability/2013_03_17c//sierpi_intel_mpi_tbb_sacsp_hyperbolic_parallel_tsunami_b0_release_d22_a08_s004096_t002_k100_n001.txt
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability
# job description
#SBATCH -J sierpi_intel_mpi_tbb_sacsp_hyperbolic_parallel_tsunami_b0_release_d22_a08_s004096_t002_k100_n001
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=00:30:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability

mpiexec.hydra -genv OMP_NUM_THREADS 2 -envall -ppn 8 -n 1 ../../build/sierpi_intel_mpi_tbb_sacsp_hyperbolic_parallel_tsunami_b0_release -d 22 -a 8 -t -1 -L 100 -k 100 -r 0.01/0.005 -n 2 -N 2 -o 4096
