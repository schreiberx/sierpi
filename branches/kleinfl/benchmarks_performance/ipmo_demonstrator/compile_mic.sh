#! /bin/bash

cd ../../

export IPMO_ROOT="/home/schreibm/workspace/ipmo_2012_10_16"

scons --enable-libxml=off --enable-mic=on --compiler=intel --simulation=hyperbolic_parallel --mode=release --enable-bathymetry=off --threading=omp -j 8
scons --enable-libxml=off --enable-mic=on --compiler=intel --simulation=hyperbolic_parallel --mode=release --enable-bathymetry=off --threading=ipmo_async -j 8
