#! /bin/bash


DEPTH=12

MIN_ADAPT_DEPTH=0
MAX_ADAPT_DEPTH=8


if true; then
	for REL_DEPTH in `seq $MIN_ADAPT_DEPTH $MAX_ADAPT_DEPTH`; do
		echo "Non-invasive (DEPTH/REL_DEPTH: $DEPTH/$REL_DEPTH)"
		./run_sierpi_non_invasive_affinity_dispatcher.sh $DEPTH $REL_DEPTH 2>&1 >/dev/null

		echo "Invasive (DEPTH/REL_DEPTH: $DEPTH/$REL_DEPTH)"
		./run_sierpi_itbb_invasive_affinity_dispatcher.sh $DEPTH $REL_DEPTH 2>&1 >/dev/null
	done

	echo ""
fi

