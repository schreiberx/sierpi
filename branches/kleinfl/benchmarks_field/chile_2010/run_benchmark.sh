#! /bin/bash


EXEC="../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_b0_release"

XML_CONFIG_FILE="../../scenarios/chile_netcdf_intel_release.xml"
SIMULATION_TIME=43200

D="14 16 18 20 22 24 26"
A="0 2 4 6 8 10"

#D="16"
#A="4"

#A="0"
CFL="0.5"

USE_NCORES=64

MAX_DEPTH=30

REFINE="1"
COARSEN="0.1"

EXT_APPEND="_rk1_refine${REFINE}_coarsen${COARSEN}"

PARAMS="-c $XML_CONFIG_FILE -A 1 -t $SIMULATION_TIME -r $REFINE/$COARSEN"

PARAMS=" $PARAMS -v 3 "

SPLIT_SIZE=$((16*1024))
#SPLIT_SIZE=$((4*1024))
#PARAMS="$PARAMS -o $SPLIT_SIZE"

PARAMS="$PARAMS -u 6 -U 6"

# output data (e. g. dart stations) after n simulation seconds
PARAMS="$PARAMS -b 10"

DART_STATIONS="-2183402.5888/4415122.01759/32411.txt,-1457114.85459/1906574.05927/32412.txt,-4203300.98871/5355459.18906/43412.txt,-5835503.42943/1754842.92493/51406.txt,-5741803.9464/7041915.84576/46412.txt,-10128447.2326/3574249.06494/51407.txt"


for d in $D; do
	for a in $A; do
		[ $((d+a)) -gt $MAX_DEPTH ] && continue

		for cfl in $CFL; do

			OUTPUTFILE_EXTENSION="d""$d""_a""$a""_cfl""$cfl""$EXT_APPEND"

			DART_STATIONS_F=`echo "$DART_STATIONS" | sed "s/\([^/]*\)\.txt/output_$OUTPUTFILE_EXTENSION""_dart""\\1.txt/g"`
			echo $DART_STATIONS_F

			# dart station values output
			_PARAMS=" $PARAMS -D $DART_STATIONS_F"

			# adaptivity
			_PARAMS=" $_PARAMS -a $a -d $d"
			
			# cfl
			_PARAMS=" $_PARAMS -C $cfl"

			E="$EXEC $_PARAMS"
			echo "$E"

			$E
#			srun --nice=10000 -c $USE_NCORES $E > "output_""$OUTPUTFILE_EXTENSION""_EXEC.txt" &
			#$E
		done
	done
done
