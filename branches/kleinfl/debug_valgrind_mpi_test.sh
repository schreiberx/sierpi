#! /bin/sh


scons --enable-mpi=on --enable-gui=off -j 4 || exit -1

#./run_mpi_xterm 4 ./run_valgrind ./build/sierpi_gnu_mpi_nothreading_hyperbolic_parallel_tsunami_b0_debug  -o 4 -c scenarios/raindrops_activated.xml -c scenarios/set_debug_level_10.xml $@
mpirun -n 4 ./run_valgrind ./build/sierpi_gnu_mpi_nothreading_hyperbolic_parallel_tsunami_b0_debug  -o 4 -c scenarios/raindrops_activated.xml -c scenarios/set_debug_level_10.xml $@
