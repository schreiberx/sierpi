/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Feb 14, 2011
 *      Author: schreibm
 */

#ifndef CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_

#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_PACKED_FLUX	1

#include <xmmintrin.h>
#include "libmath/CMath.hpp"
#include "../../sierpi/src/libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"
#include "../../sierpi/src/simulations/tsunami_common/CTsunamiTypes.hpp"
#include "../../sierpi/src/simulations/tsunami_common/EBoundaryConditions.hpp"
#include "../../sierpi/src/libmath/CVertex2d.hpp"
#include "../../sierpi/src/lib/CDebugSetupDone.hpp"
#include "../../sierpi/src/libsierpi/parallelization/CTriangle_Factory.hpp"
#include "../../sierpi/src/config.h"


// gnu compiler
#define ALIGN16		__attribute__ ((aligned (16)))

// MS$!@#$ compiler
//#define ALIGN16	__declspec(align(16))



namespace sierpi
{
namespace kernels
{

class CEdgeComm_Tsunami_1stOrder_config
{
public:

	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar square_side_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiEdgeData boundary_dirichlet;
};

template <int EBoundaryConditionValue>
class CEdgeComm_Tsunami_1stOrder
{
	DEBUG_SETUP_DONE_VAR

public:
	typedef TTsunamiDataScalar TVertexScalar;
	typedef CVertex2d<TTsunamiDataScalar> TVertexType;
	typedef CTsunamiElementData TElementData;
	typedef CTsunamiEdgeData TEdgeData;

	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> > TRAV;
	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> > TRAV_PARALLEL;

	// convenient typedef
	typedef TTsunamiDataScalar T;

	CEdgeComm_Tsunami_1stOrder_config config;


#if CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_PACKED_FLUX
	struct CPackedFlux128ps
	{
		ALIGN16 float i_inner_h[4];
		ALIGN16 float i_inner_qx[4];
		ALIGN16 float i_inner_qy[4];

		ALIGN16 float i_outer_h[4];
		ALIGN16 float i_outer_qx[4];
		ALIGN16 float i_outer_qy[4];

		ALIGN16 float i_edge_normal_x[4];
		ALIGN16 float i_edge_normal_y[4];

		CTsunamiEdgeData *o_flux[4];
	};

	int packedFluxCounter;

	CPackedFlux128ps packedFlux128ps;

	bool use_simd;
#endif

	CEdgeComm_Tsunami_1stOrder()
	{
		DEBUG_SETUP_DONE_CONSTRUCTOR

		config.boundary_dirichlet.h = CMath::numeric_inf<T>();
		config.boundary_dirichlet.qx = CMath::numeric_inf<T>();
		config.boundary_dirichlet.qy = CMath::numeric_inf<T>();

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		config.boundary_dirichlet.edge_midpoint_x = -666;
		config.boundary_dirichlet.edge_midpoint_y = -666;
#endif

#if CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_PACKED_FLUX
		packedFluxCounter = 0;
		use_simd = true;
#endif
	}


	void setup_RootPartition(
			CEdgeComm_Tsunami_1stOrder_config &i_config
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config = i_config;
	}


	void setup_RootPartition(
			T i_delta_timestep,
			T i_square_side_length,
			T i_gravity = 9.81
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config.timestep_size = i_delta_timestep;
		config.gravitational_constant = i_gravity;
		config.square_side_length = i_square_side_length;

	}

	void setup_ChildPartition(
			CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> &parent
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config = parent.config;
	}

	void setup_RootPartition_KernelBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		config.boundary_dirichlet = *p_boundary_dirichlet;
	}


	inline void storeLeftEdgeCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->left_edge;
	}

	inline void storeRightEdgeCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->right_edge;
	}

	inline void storeHypCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->hyp_edge;
	}


#if 1
	void dumpm128(__m128 m)
	{
		float v[4];
		_mm_storeu_ps(v, m);
		std::cout << v[0] << " " << v[1] << " " << v[2] << std::endl;
	}

#define MAX_FLUXES	3

	void fluxComputationQueue(
			const CTsunamiEdgeData *i_inner_,
			const CTsunamiEdgeData *i_outer_,
			const T i_edge_normal_x_,
			const T i_edge_normal_y_,
			CTsunamiEdgeData *o_flux_
	)
	{
		packedFlux128ps.i_inner_h[packedFluxCounter] = i_inner_->h;
		packedFlux128ps.i_inner_qx[packedFluxCounter] = i_inner_->qx;
		packedFlux128ps.i_inner_qy[packedFluxCounter] = i_inner_->qy;

		packedFlux128ps.i_outer_h[packedFluxCounter] = i_outer_->h;
		packedFlux128ps.i_outer_qx[packedFluxCounter] = i_outer_->qx;
		packedFlux128ps.i_outer_qy[packedFluxCounter] = i_outer_->qy;

		packedFlux128ps.i_edge_normal_x[packedFluxCounter] = i_edge_normal_x_;
		packedFlux128ps.i_edge_normal_y[packedFluxCounter] = i_edge_normal_y_;

		packedFlux128ps.o_flux[packedFluxCounter] = o_flux_;

		packedFluxCounter++;
		if (packedFluxCounter < MAX_FLUXES)
			return;

//		std::cout << "SIMD COMP" << std::endl;

		__m128 i_inner_h = _mm_load_ps(packedFlux128ps.i_inner_h);
		__m128 i_inner_qx = _mm_load_ps(packedFlux128ps.i_inner_qx);
		__m128 i_inner_qy = _mm_load_ps(packedFlux128ps.i_inner_qy);

		__m128 i_outer_h = _mm_load_ps(packedFlux128ps.i_outer_h);
		__m128 i_outer_qx = _mm_load_ps(packedFlux128ps.i_outer_qx);
		__m128 i_outer_qy = _mm_load_ps(packedFlux128ps.i_outer_qy);

		__m128 i_edge_normal_x = _mm_load_ps(packedFlux128ps.i_edge_normal_x);
		__m128 i_edge_normal_y = _mm_load_ps(packedFlux128ps.i_edge_normal_y);

		__m128 gravity = _mm_load1_ps(&config.gravitational_constant);

/*
 * SERIAL CODE:
		T inner_vx = i_inner.qx/i_inner.h;
		T inner_vy = i_inner.qy/i_inner.h;

		T outer_vx = i_outer.qx/i_outer.h;
		T outer_vy = i_outer.qy/i_outer.h;
*/

		// TODO: use reciprocal
		__m128 inner_vx = _mm_div_ps(i_inner_qx, i_inner_h);
		__m128 inner_vy = _mm_div_ps(i_inner_qy, i_inner_h);

		// TODO: use reciprocal
		__m128 outer_vx = _mm_div_ps(i_outer_qx, i_outer_h);
		__m128 outer_vy = _mm_div_ps(i_outer_qy, i_outer_h);


#if 1
/*
 * SERIAL CODE:
		T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravity*i_inner.h);
*/
		__m128 inner_lambda =
				_mm_add_ps(
					_mm_sqrt_ps(
						_mm_add_ps(
								_mm_mul_ps(inner_vx, inner_vx),
								_mm_mul_ps(inner_vy, inner_vy)
								)
					),
					_mm_sqrt_ps(_mm_mul_ps(gravity, i_inner_h))
				);

/*
 * SERIAL CODE:
		T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravity*i_outer.h);
*/
		__m128 outer_lambda =
				_mm_add_ps(
					_mm_sqrt_ps(
						_mm_add_ps(
								_mm_mul_ps(outer_vx, outer_vx),
								_mm_mul_ps(outer_vy, outer_vy)
								)
					),
					_mm_sqrt_ps(_mm_mul_ps(gravity, i_outer_h))
				);

/*
 * SERIAL CODE:
		T lambda = CMath::max(inner_lambda, outer_lambda)
*/
		__m128 lambda = _mm_max_ps(inner_lambda, outer_lambda);


/*
 * load 0.5 to vector
 */
		static const float half_scalar = 0.5f;
		__m128 half = _mm_load1_ps(&half_scalar);

/*
 * SERIAL CODE:
		o_flux.h =
				(T)0.5*
				((
						(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
						(i_edge_normal_y*(i_inner.qy + i_outer.qy))
				) +
				(T)lambda*(
					i_inner.h - i_outer.h
				));
*/
		__m128 o_flux_h =
					// (T)0.5*...
					_mm_mul_ps(
						half,
						_mm_add_ps(
							// +
							_mm_add_ps(
								// (i_edge_normal_x*(i_inner.qx + i_outer.qx))
								_mm_mul_ps(
										i_edge_normal_x,
										_mm_add_ps(i_inner_qx, i_outer_qx)
										),
								// (i_edge_normal_y*(i_inner.qy + i_outer.qy))
								_mm_mul_ps(
										i_edge_normal_y,
										_mm_add_ps(i_inner_qy, i_outer_qy)
										)
							),
							// (T)lambda*(i_inner.h - i_outer.h))
							_mm_mul_ps(
								lambda,
								_mm_sub_ps(i_inner_h, i_outer_h)
							)
						)
					);

/*
 * SERIAL CODE:
		T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;
*/
		__m128 inner_h2_plus_outer_h2 =
				_mm_add_ps(
						_mm_mul_ps(i_inner_h, i_inner_h),
						_mm_mul_ps(i_outer_h, i_outer_h)
					);

/*
 * SERIAL CODE:
			o_flux.qx =
					(T)0.5*
					((
						i_edge_normal_x*	(	inner_vx*i_inner.qx +
												outer_vx*i_outer.qx +
												(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_y*	(	inner_vx*i_inner.qy +
												outer_vx*i_outer.qy
											)
					) +
					(T)lambda*(
							i_inner.qx - i_outer.qx
					));
*/

		__m128 o_flux_qx =
					// (T)0.5*...
					_mm_mul_ps(
						half,
						_mm_add_ps(
							_mm_add_ps(
								/*
								i_edge_normal_x*	(	inner_vx*i_inner.qx +
														outer_vx*i_outer.qx +
														(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
								 */
								_mm_mul_ps(
									i_edge_normal_x,
									_mm_add_ps(
										_mm_add_ps(
												_mm_mul_ps(inner_vx, i_inner_qx),
												_mm_mul_ps(outer_vx, i_outer_qx)
										),
										_mm_mul_ps(
											_mm_mul_ps(
													half,
													gravity
											),
											inner_h2_plus_outer_h2
										)
									)
								),
								/*
								i_edge_normal_y*	(	inner_vx*i_inner.qy +
														outer_vx*i_outer.qy
								 */
								_mm_mul_ps(
									i_edge_normal_y,
									_mm_add_ps(
											_mm_mul_ps(inner_vx, i_inner_qy),
											_mm_mul_ps(outer_vx, i_outer_qy)
									)
								)
							), // _mm_add_
							/*
							 * (T)lambda*(
									i_inner.qx - i_outer.qx
							*/
							_mm_mul_ps(
								lambda,
								_mm_sub_ps(i_inner_qx, i_outer_qx)
							)
						)
					);

/*
 * SERIAL CODE:

		o_flux.qy =
				(T)0.5*
				((
						i_edge_normal_y*	(	inner_vy*i_inner.qy +
												outer_vy*i_outer.qy +
												(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_x*	(	inner_vy*i_inner.qx +
												outer_vy*i_outer.qx
											)
				) +
				(T)lambda*(
						i_inner.qy - i_outer.qy
				));
*/

	__m128 o_flux_qy =
				// (T)0.5*...
				_mm_mul_ps(
					half,
					_mm_add_ps(
						_mm_add_ps(
							/*
							i_edge_normal_y*	(	inner_vy*i_inner.qy +
													outer_vy*i_outer.qy +
													(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
											)
							 */
							_mm_mul_ps(
								i_edge_normal_y,
								_mm_add_ps(
									_mm_add_ps(
											_mm_mul_ps(inner_vy, i_inner_qy),
											_mm_mul_ps(outer_vy, i_outer_qy)
									),
									_mm_mul_ps(
										_mm_mul_ps(
												half,
												gravity
										),
										inner_h2_plus_outer_h2
									)
								)
							),
							/*
								i_edge_normal_x*	(	inner_vy*i_inner.qx +
														outer_vy*i_outer.qx
											)
							 */
							_mm_mul_ps(
								i_edge_normal_x,
								_mm_add_ps(
										_mm_mul_ps(inner_vy, i_inner_qx),
										_mm_mul_ps(outer_vy, i_outer_qx)
								)
							)
						), // _mm_add_
						/*
						(T)lambda*(
							i_inner.qy - i_outer.qy
						*/
						_mm_mul_ps(
							lambda,
							_mm_sub_ps(i_inner_qy, i_outer_qy)
						)
					)
				);
#else

	/*
	 * SERIAL CODE:
			T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravity*i_inner.h);
	*/
	/*
			__m128 inner_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(inner_vx, inner_vx),
									_mm_mul_ps(inner_vy, inner_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_inner_h))
					);
	*/
			__m128 mul_a_1 = _mm_mul_ps(inner_vx, inner_vx);
			__m128 mul_a_2 = _mm_mul_ps(inner_vy, inner_vy);
			__m128 add_a_1 = _mm_add_ps(mul_a_1, mul_a_2);
			__m128 sqrt_a_1 = _mm_sqrt_ps(add_a_1);

			__m128 mul_a_3 = _mm_mul_ps(gravitational_constant, i_inner_h);
			__m128 sqrt_a_2 = _mm_sqrt_ps(mul_a_3);

			__m128 inner_lambda = _mm_add_ps(sqrt_a_1, sqrt_a_2);
/*
			__m128 inner_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(inner_vx, inner_vx),
									_mm_mul_ps(inner_vy, inner_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_inner_h))
					);
*/
	/*
	 * SERIAL CODE:
			T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravity*i_outer.h);
	*/
			__m128 mul_b_1 = _mm_mul_ps(outer_vx, outer_vx);
			__m128 mul_b_2 = _mm_mul_ps(outer_vy, outer_vy);
			__m128 add_b_1 = _mm_add_ps(mul_b_1, mul_b_2);
			__m128 sqrt_b_1 = _mm_sqrt_ps(add_b_1);

			__m128 mul_b_3 = _mm_mul_ps(gravitational_constant, i_outer_h);
			__m128 sqrt_b_2 = _mm_sqrt_ps(mul_b_3);

			__m128 outer_lambda = _mm_add_ps(sqrt_b_1, sqrt_b_2);
/*
			__m128 outer_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(outer_vx, outer_vx),
									_mm_mul_ps(outer_vy, outer_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_outer_h))
					);
*/
	/*
	 * SERIAL CODE:
			T lambda = CMath::max(inner_lambda, outer_lambda)
	*/
			__m128 lambda = _mm_max_ps(inner_lambda, outer_lambda);


	/*
	 * load 0.5 to vector
	 */
			static const float half_scalar = 0.5f;
			__m128 half = _mm_load1_ps(&half_scalar);

	/*
	 * SERIAL CODE:
			o_flux.h =
					(T)0.5*
					((
							(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
							(i_edge_normal_y*(i_inner.qy + i_outer.qy))
					) +
					(T)lambda*(
						i_inner.h - i_outer.h
					));
	*/
			__m128 add_c_1 = _mm_add_ps(i_inner_qx, i_outer_qx);
			__m128 mul_c_1 = _mm_mul_ps(i_edge_normal_x, add_c_1);

			__m128 add_c_2 = _mm_add_ps(i_inner_qy, i_outer_qy);
			__m128 mul_c_2 = _mm_mul_ps(i_edge_normal_y, add_c_2);

			__m128 add_c_3 = _mm_add_ps(mul_c_1, mul_c_2);

			__m128 sub_c_4 = _mm_sub_ps(i_inner_h, i_outer_h);
			__m128 mul_c_4 = _mm_mul_ps(lambda, sub_c_4);

			__m128 add_c_5 = _mm_add_ps(add_c_3, mul_c_4);

			__m128 o_flux_h = _mm_mul_ps(half, add_c_5);

/*
			__m128 o_flux_h =
						// (T)0.5*...
						_mm_mul_ps(
							half,
							_mm_add_ps(
								// +
								_mm_add_ps(
									// (i_edge_normal_x*(i_inner.qx + i_outer.qx))
									_mm_mul_ps(
											i_edge_normal_x,
											_mm_add_ps(i_inner_qx, i_outer_qx)
											),
									// (i_edge_normal_y*(i_inner.qy + i_outer.qy))
									_mm_mul_ps(
											i_edge_normal_y,
											_mm_add_ps(i_inner_qy, i_outer_qy)
											)
								),
								// (T)lambda*(i_inner.h - i_outer.h))
								_mm_mul_ps(
									lambda,
									_mm_sub_ps(i_inner_h, i_outer_h)
								)
							)
						);
*/
	/*
	 * SERIAL CODE:
			T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;
	*/

			__m128 inner_h2_plus_outer_h2 =
					_mm_add_ps(
							_mm_mul_ps(i_inner_h, i_inner_h),
							_mm_mul_ps(i_outer_h, i_outer_h)
						);

	/*
	 * SERIAL CODE:
				o_flux.qx =
						(T)0.5*
						((
							i_edge_normal_x*	(	inner_vx*i_inner.qx +
													outer_vx*i_outer.qx +
													(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
							+
							i_edge_normal_y*	(	inner_vx*i_inner.qy +
													outer_vx*i_outer.qy
												)
						) +
						(T)lambda*(
								i_inner.qx - i_outer.qx
						));
	*/

			__m128 o_flux_qx =
						// (T)0.5*...
						_mm_mul_ps(
							half,
							_mm_add_ps(
								_mm_add_ps(
									/*
									i_edge_normal_x*	(	inner_vx*i_inner.qx +
															outer_vx*i_outer.qx +
															(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
									 */
									_mm_mul_ps(
										i_edge_normal_x,
										_mm_add_ps(
											_mm_add_ps(
													_mm_mul_ps(inner_vx, i_inner_qx),
													_mm_mul_ps(outer_vx, i_outer_qx)
											),
											_mm_mul_ps(
												_mm_mul_ps(
														half,
														gravitational_constant
												),
												inner_h2_plus_outer_h2
											)
										)
									),
									/*
									i_edge_normal_y*	(	inner_vx*i_inner.qy +
															outer_vx*i_outer.qy
									 */
									_mm_mul_ps(
										i_edge_normal_y,
										_mm_add_ps(
												_mm_mul_ps(inner_vx, i_inner_qy),
												_mm_mul_ps(outer_vx, i_outer_qy)
										)
									)
								), // _mm_add_
								/*
								 * (T)lambda*(
										i_inner.qx - i_outer.qx
								*/
								_mm_mul_ps(
									lambda,
									_mm_sub_ps(i_inner_qx, i_outer_qx)
								)
							)
						);

	/*
	 * SERIAL CODE:

			o_flux.qy =
					(T)0.5*
					((
							i_edge_normal_y*	(	inner_vy*i_inner.qy +
													outer_vy*i_outer.qy +
													(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
							+
							i_edge_normal_x*	(	inner_vy*i_inner.qx +
													outer_vy*i_outer.qx
												)
					) +
					(T)lambda*(
							i_inner.qy - i_outer.qy
					));
	*/

		__m128 o_flux_qy =
					// (T)0.5*...
					_mm_mul_ps(
						half,
						_mm_add_ps(
							_mm_add_ps(
								/*
								i_edge_normal_y*	(	inner_vy*i_inner.qy +
														outer_vy*i_outer.qy +
														(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
								 */
								_mm_mul_ps(
									i_edge_normal_y,
									_mm_add_ps(
										_mm_add_ps(
												_mm_mul_ps(inner_vy, i_inner_qy),
												_mm_mul_ps(outer_vy, i_outer_qy)
										),
										_mm_mul_ps(
											_mm_mul_ps(
													half,
													gravitational_constant
											),
											inner_h2_plus_outer_h2
										)
									)
								),
								/*
									i_edge_normal_x*	(	inner_vy*i_inner.qx +
															outer_vy*i_outer.qx
												)
								 */
								_mm_mul_ps(
									i_edge_normal_x,
									_mm_add_ps(
											_mm_mul_ps(inner_vy, i_inner_qx),
											_mm_mul_ps(outer_vy, i_outer_qx)
									)
								)
							), // _mm_add_
							/*
							(T)lambda*(
								i_inner.qy - i_outer.qy
							*/
							_mm_mul_ps(
								lambda,
								_mm_sub_ps(i_inner_qy, i_outer_qy)
							)
						)
					);
#endif
		ALIGN16 T flux[3][4];	// h, qx, qy
		_mm_store_ps(&(flux[0][0]), o_flux_h);
		_mm_store_ps(&(flux[1][0]), o_flux_qx);
		_mm_store_ps(&(flux[2][0]), o_flux_qy);

		for (int i = 0; i < packedFluxCounter; i++)
		{
			CTsunamiEdgeData &e = *(packedFlux128ps.o_flux[i]);
			e.h = flux[0][i];
			e.qx = flux[1][i];
			e.qy = flux[2][i];
		}

		packedFluxCounter = 0;
	}

	void traversal_pre_hook()
	{
		packedFluxCounter = 0;
	}


	void traversal_post_hook()
	{
	}


	/**
	 * see diplomathesis, hoechstetter (2009), page 43
	 *
	 * ~60 floating point operations + 4x sqrt
	 */
	inline void computeFlux(
			const CTsunamiEdgeData &i_inner,
			const CTsunamiEdgeData &i_outer,
			const T i_edge_normal_x, const T i_edge_normal_y,
			CTsunamiEdgeData &o_flux
	)
	{
		T inner_vx = i_inner.qx/i_inner.h;
		T inner_vy = i_inner.qy/i_inner.h;

		T outer_vx = i_outer.qx/i_outer.h;
		T outer_vy = i_outer.qy/i_outer.h;

#if 1
		T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravitational_constant*i_inner.h);
		T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravitational_constant*i_outer.h);
		T lambda = CMath::max(inner_lambda, outer_lambda);
#else
		T i1 = inner_vx*inner_vx+inner_vy*inner_vy;
		T i2 = config.gravitational_constant*i_inner.h;

		T o1 = outer_vx*outer_vx+outer_vy*outer_vy;
		T o2 = config.gravitational_constant*i_outer.h;

		T lambda;
		if (i1 <= o1 && i2 <= o2)
		{
			lambda = CMath::sqrt<TTsunamiDataScalar>(o1)+CMath::sqrt<TTsunamiDataScalar>(o2);
		}
		else if (i1 > o1 && i2 > o2)
		{
			lambda = CMath::sqrt<TTsunamiDataScalar>(i1)+CMath::sqrt<TTsunamiDataScalar>(i2);
		}
		else
		{
			T inner_lambda = CMath::sqrt<T>(i1) + CMath::sqrt<T>(i2);
			T outer_lambda = CMath::sqrt<T>(o1) + CMath::sqrt<T>(o2);
			lambda = CMath::max(inner_lambda, outer_lambda);
		}
#endif

		o_flux.h =
				(T)0.5*
				((
						(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
						(i_edge_normal_y*(i_inner.qy + i_outer.qy))
				) +
				(T)lambda*(
					i_inner.h - i_outer.h
				));

		T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;

		o_flux.qx =
				(T)0.5*
				((
						i_edge_normal_x*	(	inner_vx*i_inner.qx +
												outer_vx*i_outer.qx +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_y*	(	inner_vx*i_inner.qy +
												outer_vx*i_outer.qy
											)
				) +
				(T)lambda*(
						i_inner.qx - i_outer.qx
				));

		o_flux.qy =
				(T)0.5*
				((
						i_edge_normal_y*	(	inner_vy*i_inner.qy +
												outer_vy*i_outer.qy +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_x*	(	inner_vy*i_inner.qx +
												outer_vy*i_outer.qx
											)
				) +
				(T)lambda*(
						i_inner.qy - i_outer.qy
				));
	}

	inline void elementAction_EEE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,			///< normal at hypotenuse
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,		///< normal at right edge
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,		///< normal at left edge
			int depth,														///< recursion depth

			CTsunamiElementData *element,									///< element data

			CTsunamiEdgeData *hyp_edge,										///< data from adjacent element next to hypotenuse
			CTsunamiEdgeData *right_edge,									///< data from adjacent element next to right edge
			CTsunamiEdgeData *left_edge										///< data from adjacent element next to left edge
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		if (element->depth != depth)
			std::cout << "DEPTH ERROR: (element/parameter) " << element->depth << " " << depth << std::endl;

		if (hyp_edge->edge_midpoint_x != element->hyp_edge.edge_midpoint_x || hyp_edge->edge_midpoint_y != element->hyp_edge.edge_midpoint_y)
		{
			std::cout << "ERROR HYP   " << hyp_edge->edge_midpoint_x << " " << element->hyp_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << hyp_edge->edge_midpoint_y << " " << element->hyp_edge.edge_midpoint_y << std::endl;
		}

		if (left_edge->edge_midpoint_x != element->left_edge.edge_midpoint_x || left_edge->edge_midpoint_y != element->left_edge.edge_midpoint_y)
		{
			std::cout << "ERROR LEFT  " << left_edge->edge_midpoint_x << " " << element->left_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << left_edge->edge_midpoint_y << " " << element->left_edge.edge_midpoint_y << std::endl;
		}

		if (right_edge->edge_midpoint_x != element->right_edge.edge_midpoint_x || right_edge->edge_midpoint_y != element->right_edge.edge_midpoint_y)
		{
			std::cout << "ERROR RIGHT " << right_edge->edge_midpoint_x << " " << element->right_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << right_edge->edge_midpoint_y << " " << element->right_edge.edge_midpoint_y << std::endl;
		}
#endif


		if (CMath::isNan(hyp_edge->h))
		{
			std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			exit(-1);
		}


		T tmp;
#define rotate(qx, qy)											\
		tmp = (-left_normal_y)*(qx) + (left_normal_x)*(qy);		\
		qy = (-left_normal_x)*(qx) + (-left_normal_y)*(qy);		\
		qx = tmp;

		rotate(hyp_edge->qx, hyp_edge->qy)
		rotate(left_edge->qx, left_edge->qy)
		rotate(right_edge->qx, right_edge->qy)

		rotate(element->hyp_edge.qx, element->hyp_edge.qy)
		rotate(element->left_edge.qx, element->left_edge.qy)
		rotate(element->right_edge.qx, element->right_edge.qy)
#undef rotate

		CTsunamiEdgeData flux_left, flux_hyp, flux_right;

		if (!use_simd)
		{
			computeFlux(element->left_edge, *left_edge, 0, -1, flux_left);
			computeFlux(element->hyp_edge, *hyp_edge, CMath::sqrt1_2<T>(), CMath::sqrt1_2<T>(), flux_hyp);
			computeFlux(element->right_edge, *right_edge, -1, 0, flux_right);
		}
		else
		{
			fluxComputationQueue(&element->left_edge, left_edge, 0, -1, &flux_left);
			fluxComputationQueue(&element->hyp_edge, hyp_edge, CMath::sqrt1_2<T>(), CMath::sqrt1_2<T>(), &flux_hyp);
			fluxComputationQueue(&element->right_edge, right_edge, -1, 0, &flux_right);
		}

#if 0
		T cathetus_length = getUnitCathetusLengthForDepth(depth)*config.square_side_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(depth)*config.square_side_length;
		const T h = cathetus_length;

#if 1
//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config.delta_timestep/cathetus_length, (T)0.99);
		TTsunamiDataScalar damping = (T)1.0-(config.timestep_size/cathetus_length);

		element->hyp_edge.qx *= damping;
		element->hyp_edge.qy *= damping;
		element->left_edge.qx *= damping;
		element->left_edge.qy *= damping;
		element->right_edge.qx *= damping;
		element->right_edge.qy *= damping;
#endif

		T six_div_h2 = (T)6.0/(h*h);
		T timestep_six_div_h2 = config.timestep_size*six_div_h2;
		T two_div_h = (T)2.0/h;
		T minus_two_div_h = -two_div_h;
		T h_div_3 = h/(T)3.0;
		T minus_h_div_3 = -h_div_3;

		T inv_left_edge_h = (T)1.0/element->left_edge.h;
		T inv_right_edge_h = (T)1.0/element->right_edge.h;
		T inv_hyp_edge_h = (T)1.0/element->hyp_edge.h;

		T left_edge_h = timestep_six_div_h2*(
				/*(0)*(h/(T)3.0)*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+*/
				minus_h_div_3*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)
				- cathetus_length*flux_left.h);

		T hyp_edge_h = timestep_six_div_h2*(
				h_div_3*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+
				h_div_3*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)
				- hyp_length*flux_hyp.h);

		T right_edge_h = timestep_six_div_h2*(
				minus_h_div_3*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+
				/*((T)0)*(h/3.0)*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)*/
				- cathetus_length*flux_right.h);

		T triangle_area = cathetus_length*cathetus_length*(T)0.5;

		T left_edge_qx =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qx*inv_left_edge_h)*minus_two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qx*inv_hyp_edge_h)*minus_two_div_h +
							(element->right_edge.qy*element->right_edge.qx*inv_right_edge_h)*minus_two_div_h
						))
				- timestep_six_div_h2*cathetus_length*flux_left.qx;

		T hyp_edge_qx =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qx*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qx*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*two_div_h +
							(element->right_edge.qx*element->right_edge.qx*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*two_div_h
						)) +
						timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qx*inv_left_edge_h)*two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qx*inv_hyp_edge_h)*two_div_h +
							(element->right_edge.qy*element->right_edge.qx*inv_right_edge_h)*two_div_h
						))
				- timestep_six_div_h2*hyp_length*flux_hyp.qx;

		T right_edge_qx =
				timestep_six_div_h2*(
					(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qx*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*minus_two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qx*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*minus_two_div_h +
							(element->right_edge.qx*element->right_edge.qx*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*minus_two_div_h
						))
				- timestep_six_div_h2*cathetus_length*flux_right.qx;




		T left_edge_qy =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qy*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*minus_two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qy*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*minus_two_div_h +
							(element->right_edge.qy*element->right_edge.qy*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*minus_two_div_h
						))
				- timestep_six_div_h2*cathetus_length*flux_left.qy;

		T hyp_edge_qy =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qy*inv_left_edge_h)*(2.0/h) +
							(element->hyp_edge.qx*element->hyp_edge.qy*inv_hyp_edge_h)*(2.0/h) +
							(element->right_edge.qx*element->right_edge.qy*inv_right_edge_h)*(2.0/h)
						)) +
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qy*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qy*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*two_div_h +
							(element->right_edge.qy*element->right_edge.qy*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*two_div_h
						))
				- timestep_six_div_h2*hyp_length*flux_hyp.qy;

		T right_edge_qy =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qy*inv_left_edge_h)*minus_two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qy*inv_hyp_edge_h)*minus_two_div_h +
							(element->right_edge.qx*element->right_edge.qy*inv_right_edge_h)*minus_two_div_h
						))
				- config.timestep_size*6.0/(h*h)*cathetus_length*flux_right.qy;

		element->left_edge.h += left_edge_h;
		element->left_edge.qx += left_edge_qx;
		element->left_edge.qy += left_edge_qy;
		element->hyp_edge.h += hyp_edge_h;
		element->hyp_edge.qx += hyp_edge_qx;
		element->hyp_edge.qy += hyp_edge_qy;
		element->right_edge.h += right_edge_h;
		element->right_edge.qx += right_edge_qx;
		element->right_edge.qy += right_edge_qy;
#else
		element->left_edge.h += flux_left.h;
		element->left_edge.qx += flux_left.qx;
		element->left_edge.qy += flux_left.qy;
		element->hyp_edge.h += flux_hyp.h;
		element->hyp_edge.qx += flux_hyp.qx;
		element->hyp_edge.qy += flux_hyp.qy;
		element->right_edge.h += flux_right.h;
		element->right_edge.qx += flux_right.qx;
		element->right_edge.qy += flux_right.qy;
#endif

#define unrotate(qx, qy)									\
		tmp = (-left_normal_y)*qx + (-left_normal_x)*qy;	\
		qy = (left_normal_x)*qx + (-left_normal_y)*qy;		\
		qx = tmp;

		unrotate(element->hyp_edge.qx, element->hyp_edge.qy)
		unrotate(element->left_edge.qx, element->left_edge.qy)
		unrotate(element->right_edge.qx, element->right_edge.qy)
#undef unrotate

	}

#else

	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}

	/**
	 * see diplomathesis, hoechstetter (2009), page 43
	 *
	 * ~60 floating point operations + 4x sqrt
	 */
	inline void computeFlux(
			const CTsunamiEdgeData &inner,
			const CTsunamiEdgeData &outer,
			const T edge_normal_x, const T edge_normal_y,
			CTsunamiEdgeData &flux
	)
	{
		T inner_vx = inner.qx/inner.h;
		T inner_vy = inner.qy/inner.h;

		T outer_vx = outer.qx/outer.h;
		T outer_vy = outer.qy/outer.h;

#if 1
		T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravitational_constant*inner.h);
		T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravitational_constant*outer.h);
		T lambda = CMath::max(inner_lambda, outer_lambda);
#else
		T i1 = inner_vx*inner_vx+inner_vy*inner_vy;
		T i2 = config.gravitational_constant*inner.h;

		T o1 = outer_vx*outer_vx+outer_vy*outer_vy;
		T o2 = config.gravitational_constant*outer.h;

		T lambda;
		if (i1 <= o1 && i2 <= o2)
		{
			lambda = CMath::sqrt<TTsunamiDataScalar>(o1)+CMath::sqrt<TTsunamiDataScalar>(o2);
		}
		else if (i1 > o1 && i2 > o2)
		{
			lambda = CMath::sqrt<TTsunamiDataScalar>(i1)+CMath::sqrt<TTsunamiDataScalar>(i2);
		}
		else
		{
			T inner_lambda = CMath::sqrt<T>(i1) + CMath::sqrt<T>(i2);
			T outer_lambda = CMath::sqrt<T>(o1) + CMath::sqrt<T>(o2);
			lambda = CMath::max(inner_lambda, outer_lambda);
		}
#endif

		flux.h =
				(T)0.5*(
					(edge_normal_x*inner.qx + edge_normal_y*inner.qy) +
					(edge_normal_x*outer.qx + edge_normal_y*outer.qy)
				) +
				(T)0.5*lambda*(
					inner.h - outer.h
				);

		flux.qx =
				(T)0.5*(
						(edge_normal_x*(inner_vx*inner.qx + (TTsunamiDataScalar)0.5*config.gravitational_constant*inner.h*inner.h)) +
						(edge_normal_y*(inner_vx*inner.qy)) +
						(edge_normal_x*(outer_vx*outer.qx + (TTsunamiDataScalar)0.5*config.gravitational_constant*outer.h*outer.h)) +
						(edge_normal_y*(outer_vx*outer.qy))
				) +
				(T)0.5*lambda*(
						inner.qx - outer.qx
				);

		flux.qy =
				(T)0.5*(
						(edge_normal_x*(inner_vy*inner.qx)) +
						(edge_normal_y*(inner_vy*inner.qy + (TTsunamiDataScalar)0.5*config.gravitational_constant*inner.h*inner.h)) +
						(edge_normal_x*(outer_vy*outer.qx)) +
						(edge_normal_y*(outer_vy*outer.qy + (TTsunamiDataScalar)0.5*config.gravitational_constant*outer.h*outer.h))
				) +
				(T)0.5*lambda*(
						inner.qy - outer.qy
				);
	}

	inline void elementAction_EEE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,

			CTsunamiElementData *element,

			CTsunamiEdgeData *hyp_edge,
			CTsunamiEdgeData *right_edge,
			CTsunamiEdgeData *left_edge
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		if (element->depth != depth)
			std::cout << "DEPTH ERROR: (element/parameter) " << element->depth << " " << depth << std::endl;

		if (hyp_edge->edge_midpoint_x != element->hyp_edge.edge_midpoint_x || hyp_edge->edge_midpoint_y != element->hyp_edge.edge_midpoint_y)
		{
			std::cout << "ERROR HYP   " << hyp_edge->edge_midpoint_x << " " << element->hyp_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << hyp_edge->edge_midpoint_y << " " << element->hyp_edge.edge_midpoint_y << std::endl;
		}

		if (left_edge->edge_midpoint_x != element->left_edge.edge_midpoint_x || left_edge->edge_midpoint_y != element->left_edge.edge_midpoint_y)
		{
			std::cout << "ERROR LEFT  " << left_edge->edge_midpoint_x << " " << element->left_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << left_edge->edge_midpoint_y << " " << element->left_edge.edge_midpoint_y << std::endl;
		}

		if (right_edge->edge_midpoint_x != element->right_edge.edge_midpoint_x || right_edge->edge_midpoint_y != element->right_edge.edge_midpoint_y)
		{
			std::cout << "ERROR RIGHT " << right_edge->edge_midpoint_x << " " << element->right_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << right_edge->edge_midpoint_y << " " << element->right_edge.edge_midpoint_y << std::endl;
		}
#endif
		T cathetus_length = getUnitCathetusLengthForDepth(depth)*config.square_side_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(depth)*config.square_side_length;
		const T h = cathetus_length;

#if 1
//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config.delta_timestep/cathetus_length, (T)0.99);
		TTsunamiDataScalar damping = (T)1.0-(config.timestep_size/cathetus_length);

		element->hyp_edge.qx *= damping;
		element->hyp_edge.qy *= damping;
		element->left_edge.qx *= damping;
		element->left_edge.qy *= damping;
		element->right_edge.qx *= damping;
		element->right_edge.qy *= damping;
#endif

		if (CMath::isNan(hyp_edge->h))
		{
			std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			exit(-1);
		}


		T tmp;
#define rotate(qx, qy)											\
		tmp = (-left_normal_y)*(qx) + (left_normal_x)*(qy);		\
		qy = (-left_normal_x)*(qx) + (-left_normal_y)*(qy);		\
		qx = tmp;

		rotate(hyp_edge->qx, hyp_edge->qy)
		rotate(left_edge->qx, left_edge->qy)
		rotate(right_edge->qx, right_edge->qy)

		rotate(element->hyp_edge.qx, element->hyp_edge.qy)
		rotate(element->left_edge.qx, element->left_edge.qy)
		rotate(element->right_edge.qx, element->right_edge.qy)
#undef rotate

		CTsunamiEdgeData flux_left, flux_hyp, flux_right;
		computeFlux(element->left_edge, *left_edge, 0, -1, flux_left);
		computeFlux(element->hyp_edge, *hyp_edge, CMath::sqrt1_2<T>(), CMath::sqrt1_2<T>(), flux_hyp);
		computeFlux(element->right_edge, *right_edge, -1, 0, flux_right);

		T six_div_h2 = (T)6.0/(h*h);
		T timestep_six_div_h2 = config.timestep_size*six_div_h2;
		T two_div_h = (T)2.0/h;
		T minus_two_div_h = -two_div_h;
		T h_div_3 = h/(T)3.0;
		T minus_h_div_3 = -h_div_3;

		T inv_left_edge_h = (T)1.0/element->left_edge.h;
		T inv_right_edge_h = (T)1.0/element->right_edge.h;
		T inv_hyp_edge_h = (T)1.0/element->hyp_edge.h;

		left_edge->h = timestep_six_div_h2*(
				/*(0)*(h/(T)3.0)*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+*/
				minus_h_div_3*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)
				- cathetus_length*flux_left.h);

		hyp_edge->h = timestep_six_div_h2*(
				h_div_3*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+
				h_div_3*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)
				- hyp_length*flux_hyp.h);

		right_edge->h = timestep_six_div_h2*(
				minus_h_div_3*(element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx)	+
				/*((T)0)*(h/3.0)*(element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy)*/
				- cathetus_length*flux_right.h);

		T triangle_area = cathetus_length*cathetus_length*(T)0.5;

		left_edge->qx =
				/*// multiplying with 0 does not work due to NaN restrictions
				config.delta_timestep*6.0/(h*h)*(
					(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qx*inv_left_edge_h + 0.5*config.gravity*element->left_edge.h*element->left_edge.h)*(0) +
							(element->hyp_edge.qx*element->hyp_edge.qx*inv_hyp_edge_h + 0.5*config.gravity*element->hyp_edge.h*element->hyp_edge.h)*(0) +
							(element->right_edge.qx*element->right_edge.qx*inv_right_edge_h + 0.5*config.gravity*element->right_edge.h*element->right_edge.h)*(0)
						))
				*/
				+ timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qx*inv_left_edge_h)*minus_two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qx*inv_hyp_edge_h)*minus_two_div_h +
							(element->right_edge.qy*element->right_edge.qx*inv_right_edge_h)*minus_two_div_h
						))
				- timestep_six_div_h2*cathetus_length*flux_left.qx;

		hyp_edge->qx =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qx*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qx*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*two_div_h +
							(element->right_edge.qx*element->right_edge.qx*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*two_div_h
						)) +
						timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qx*inv_left_edge_h)*two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qx*inv_hyp_edge_h)*two_div_h +
							(element->right_edge.qy*element->right_edge.qx*inv_right_edge_h)*two_div_h
						))
				- timestep_six_div_h2*hyp_length*flux_hyp.qx;

		right_edge->qx =
				timestep_six_div_h2*(
					(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qx*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*minus_two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qx*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*minus_two_div_h +
							(element->right_edge.qx*element->right_edge.qx*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*minus_two_div_h
						))
				/*// multiplying with 0 does not work due to NaN restrictions
				+ config.delta_timestep*6.0/(h*h)*(
					(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qx*inv_left_edge_h)*(0) +
							(element->hyp_edge.qy*element->hyp_edge.qx*inv_hyp_edge_h)*(0) +
							(element->right_edge.qy*element->right_edge.qx*inv_right_edge_h)*(0)
						))
				*/
				- timestep_six_div_h2*cathetus_length*flux_right.qx;




		left_edge->qy =
				/*// multiplying with 0 does not work due to NaN restrictions
				config.delta_timestep*6.0/(h*h)*(
					(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qy*inv_left_edge_h)*(0) +
							(element->hyp_edge.qx*element->hyp_edge.qy*inv_hyp_edge_h)*(0) +
							(element->right_edge.qx*element->right_edge.qy*inv_right_edge_h)*(0)
						)) +
				*/
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qy*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*minus_two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qy*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*minus_two_div_h +
							(element->right_edge.qy*element->right_edge.qy*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*minus_two_div_h
						))
				- timestep_six_div_h2*cathetus_length*flux_left.qy;

		hyp_edge->qy =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qy*inv_left_edge_h)*(2.0/h) +
							(element->hyp_edge.qx*element->hyp_edge.qy*inv_hyp_edge_h)*(2.0/h) +
							(element->right_edge.qx*element->right_edge.qy*inv_right_edge_h)*(2.0/h)
						)) +
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qy*inv_left_edge_h + (T)0.5*config.gravitational_constant*element->left_edge.h*element->left_edge.h)*two_div_h +
							(element->hyp_edge.qy*element->hyp_edge.qy*inv_hyp_edge_h + (T)0.5*config.gravitational_constant*element->hyp_edge.h*element->hyp_edge.h)*two_div_h +
							(element->right_edge.qy*element->right_edge.qy*inv_right_edge_h + (T)0.5*config.gravitational_constant*element->right_edge.h*element->right_edge.h)*two_div_h
						))
				- timestep_six_div_h2*hyp_length*flux_hyp.qy;

		right_edge->qy =
				timestep_six_div_h2*(
						(T)(1.0/3.0)*triangle_area*(
							(element->left_edge.qx*element->left_edge.qy*inv_left_edge_h)*minus_two_div_h +
							(element->hyp_edge.qx*element->hyp_edge.qy*inv_hyp_edge_h)*minus_two_div_h +
							(element->right_edge.qx*element->right_edge.qy*inv_right_edge_h)*minus_two_div_h
						))
				/*// multiplying with 0 does not work due to NaN restrictions
				+ config.delta_timestep*6.0/(h*h)*(
					(1.0/3.0)*triangle_area*(
							(element->left_edge.qy*element->left_edge.qy*inv_left_edge_h + 0.5*config.gravity*element->left_edge.h*element->left_edge.h)*(0) +
							(element->hyp_edge.qy*element->hyp_edge.qy*inv_hyp_edge_h + 0.5*config.gravity*element->hyp_edge.h*element->hyp_edge.h)*(0) +
							(element->right_edge.qy*element->right_edge.qy*inv_right_edge_h + 0.5*config.gravity*element->right_edge.h*element->right_edge.h)*(0)
						))
				*/
				- config.timestep_size*6.0/(h*h)*cathetus_length*flux_right.qy;


		element->left_edge.h += left_edge->h;
		element->left_edge.qx += left_edge->qx;
		element->left_edge.qy += left_edge->qy;
		element->hyp_edge.h += hyp_edge->h;
		element->hyp_edge.qx += hyp_edge->qx;
		element->hyp_edge.qy += hyp_edge->qy;
		element->right_edge.h += right_edge->h;
		element->right_edge.qx += right_edge->qx;
		element->right_edge.qy += right_edge->qy;

#define unrotate(qx, qy)									\
		tmp = (-left_normal_y)*qx + (-left_normal_x)*qy;	\
		qy = (left_normal_x)*qx + (-left_normal_y)*qy;		\
		qx = tmp;

		unrotate(element->hyp_edge.qx, element->hyp_edge.qy)
		unrotate(element->left_edge.qx, element->left_edge.qy)
		unrotate(element->right_edge.qx, element->right_edge.qy)
#undef unrotate

	}
#endif

	inline void elementAction_BEE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *right_edge,
			CTsunamiEdgeData *left_edge
	);

	inline void elementAction_BBB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element
	);


	inline void elementAction_EEB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge,
			CTsunamiEdgeData *right_edge
	);

	inline void elementAction_EBE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge,
			CTsunamiEdgeData *left_edge
	);


	inline void elementAction_EBB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge
	);


	inline void elementAction_BBE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *left_edge
	);


	inline void elementAction_BEB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *right_edge
	);

	inline void postTraversatorSetupPartition(CTriangle_Factory &triangleFactory)
	{
	}
};


/**
 * DIRICHLET BOUNDARY CONDITION
 */
template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;
	hyp_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BBB" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, &right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BEE" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EBE" << std::endl;
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif

	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EEB" << std::endl;
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "EBB" << std::endl;
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}



template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData left_edge;

	hyp_edge = config.boundary_dirichlet;
	left_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BEB" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_DIRICHLET>::elementAction_BBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;
	CTsunamiEdgeData right_edge;

	hyp_edge = config.boundary_dirichlet;
	right_edge = config.boundary_dirichlet;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
//	std::cout << "BBE" << std::endl;
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						&hyp_edge, &right_edge, left_edge);
}


/**
 * BOUNDARY CONDITION: VELOCITY_ZERO
 */

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge.h = element->hyp_edge.h;
	hyp_edge.qx = 0;
	hyp_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge.h = element->right_edge.h;
	right_edge.qx = 0;
	right_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = 0;
	left_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_VELOCITY_ZERO>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = 0;
	left_edge.qy = 0;

	right_edge.h = element->right_edge.h;
	right_edge.qx = 0;
	right_edge.qy = 0;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}

/**
 * BOUNDARY CONDITION: BOUNCE_BACK
 */

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_BEE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *right_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData hyp_edge;

	hyp_edge.h = element->hyp_edge.h;
	hyp_edge.qx = -element->hyp_edge.qx;
	hyp_edge.qy = -element->hyp_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	hyp_edge.edge_midpoint_x = element->hyp_edge.edge_midpoint_x;
	hyp_edge.edge_midpoint_y = element->hyp_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			&hyp_edge, right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EBE(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *left_edge
)
{
	CTsunamiEdgeData right_edge;

	right_edge.h = element->right_edge.h;
	right_edge.qx = -element->right_edge.qx;
	right_edge.qy = -element->right_edge.qy;

	#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, &right_edge, left_edge);
}

template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EEB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge,
		CTsunamiEdgeData *right_edge
)
{
	CTsunamiEdgeData left_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = -element->left_edge.qx;
	left_edge.qy = -element->left_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;
#endif


	elementAction_EEE(
			hyp_normal_x, hyp_normal_y,
			right_normal_x, right_normal_y,
			left_normal_x, left_normal_y,
			depth,
			element,
			hyp_edge, right_edge, &left_edge);
}


template <>
inline void CEdgeComm_Tsunami_1stOrder<BOUNDARY_CONDITION_BOUNCE_BACK>::elementAction_EBB(
		TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
		TVertexScalar right_normal_x, TVertexScalar right_normal_y,
		TVertexScalar left_normal_x, TVertexScalar left_normal_y,
		int depth,
		CTsunamiElementData *element,
		CTsunamiEdgeData *hyp_edge
)
{
	CTsunamiEdgeData left_edge;
	CTsunamiEdgeData right_edge;

	left_edge.h = element->left_edge.h;
	left_edge.qx = -element->left_edge.qx;
	left_edge.qy = -element->left_edge.qy;

	right_edge.h = element->right_edge.h;
	right_edge.qx = -element->right_edge.qx;
	right_edge.qy = -element->right_edge.qy;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	left_edge.edge_midpoint_x = element->left_edge.edge_midpoint_x;
	left_edge.edge_midpoint_y = element->left_edge.edge_midpoint_y;

	right_edge.edge_midpoint_x = element->right_edge.edge_midpoint_x;
	right_edge.edge_midpoint_y = element->right_edge.edge_midpoint_y;
#endif

	elementAction_EEE(	hyp_normal_x, hyp_normal_y,
						right_normal_x, right_normal_y,
						left_normal_x, left_normal_y,
						depth,
						element,
						hyp_edge, &right_edge, &left_edge);
}

}
}



#endif /* CEDGECOMM_TESTNORMAL_HPP_ */
