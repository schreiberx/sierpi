/*
 * !!!WARNING!!!
 *
 * this program represents only a rough estimation about the bandwidth
 *
 * !!!WARNING!!!
 */


#include <cmath>
#include <sstream>
#include <iostream>
#include <string.h>
#include <assert.h>
#include "../../src/lib/CStopwatch.hpp"


extern "C"
{
	#include "kernels.h"
}


void init(float *dst, float *src, size_t size)
{
	for (int i = 0; i < size/4; i++)
	{
		src[i] = i;
		dst[i] = i;
	}
}


double validate(float *dst, float *src, size_t size)
{
	double a = 0.0;
	for (int i = 0; i < size/4; i++)
	{
		a += src[i] + dst[i];
	}
	return a;
}


std::string sizeToConvenientFormat(size_t p_size)
{
	double size = p_size;

	std::stringstream ss;

	if (size > 1024.0*8.0)
	{
		ss << round(size/1024.0);
		ss << "KB";
		return ss.str();
	}

	if (size > 1024.0*1024.0*8.0)
	{
		ss << round(size/(1024.0*1024.0));
		ss << "MB";
		return ss.str();
	}

	if (size > 1024.0*1024.0*1024.0*8.0)
	{
		ss << round(size/(1024.0*1024.0*1024.0));
		ss << "GB";
		return ss.str();
	}

	ss << size;
	ss << "B";
	return ss.str();
}


int main()
{
	CStopwatch cStopwatch;

	int iterations = 5000;

	int max_block_size = 1024*1024*1024;

	float *src_data = new float[max_block_size/4];
	assert(src_data != 0);

	float *dst_data = new float[max_block_size/4];
	assert(dst_data != 0);

	/*
	 * initialize data
	 */
	std::cout << std::endl;
	std::cout << "max block size: " << (double)max_block_size/(1024.0*1024.0) << " MB" << std::endl;
	std::cout << std::endl;
	std::cout << "Starting benchmark..." << std::endl;
	std::cout << std::endl;
	std::cout << "SIZE	GB/s(memCpy)	GB/s(typeFloatMul)	GB/s(typeFloat)	GB/s(typeVec4F)" << std::endl;

	double sum = 0.0;

	double timeDiff;
	for (size_t size = (1<<4); size < max_block_size/4; size <<= 1)
	{
		/*
		 * MEMCPY
		 */
		init(dst_data, src_data, size);
		cStopwatch.start();
		memCpyBlock(dst_data, src_data, size);
		for (int i = 0; i < iterations; i++)
			memCpyBlock(dst_data, src_data, size);

		timeDiff = cStopwatch.getTimeSinceStart();
		double gb_per_second_memcpy = ((double)size*(double)iterations*2.0)/(1024.0*1024.0*1024.0*timeDiff);

		/*
		 * FLOAT MUL
		 */
		init(dst_data, src_data, size);
		cStopwatch.start();
		typeFloatMulBlock(dst_data, src_data, size);
		for (int i = 0; i < iterations; i++)
			typeFloatMulBlock(dst_data, src_data, size);

		timeDiff = cStopwatch.getTimeSinceStart();
		double gb_per_second_typefloatmul = ((double)size*(double)iterations*2.0)/(1024.0*1024.0*1024.0*timeDiff);

		/*
		 * FLOAT COPY
		 */
		init(dst_data, src_data, size);
		cStopwatch.start();
		typeFloatBlock(dst_data, src_data, size);
		for (int i = 0; i < iterations; i++)
			typeFloatBlock(dst_data, src_data, size);

		timeDiff = cStopwatch.getTimeSinceStart();
		double gb_per_second_typefloat = ((double)size*(double)iterations*2.0)/(1024.0*1024.0*1024.0*timeDiff);


		/*
		 * VEC4 FLOAT COPY
		 */
		init(dst_data, src_data, size);
		cStopwatch.start();
		typeVec4SIMDBlock(dst_data, src_data, size);
		for (int i = 0; i < iterations; i++)
			typeVec4SIMDBlock(dst_data, src_data, size);

		timeDiff = cStopwatch.getTimeSinceStart();
		double gb_per_second_typevec4float = ((double)size*(double)iterations*2.0)/(1024.0*1024.0*1024.0*timeDiff);



		/*
		 * FLOAT BLOCK AS INT
		 */
		init(dst_data, src_data, size);
		cStopwatch.start();
		floatBlockAsInt4(dst_data, src_data, size);
		for (int i = 0; i < iterations; i++)
			floatBlockAsInt4(dst_data, src_data, size);

		timeDiff = cStopwatch.getTimeSinceStart();
		double gb_per_second_floatBlockAsInt = ((double)size*(double)iterations*2.0)/(1024.0*1024.0*1024.0*timeDiff);

		std::cout << sizeToConvenientFormat(size)
				<< "	" << gb_per_second_memcpy
				<< "	" << gb_per_second_typefloatmul
				<< "	" << gb_per_second_typefloat
				<< "	" << gb_per_second_typevec4float
				<< "	" << gb_per_second_floatBlockAsInt
				<< std::endl;

		if (iterations > 2)
			iterations >>= 1;
	}
}
