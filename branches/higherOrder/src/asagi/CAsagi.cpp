/*
 * CAsagi.cpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#include "CAsagi.hpp"
#include <cassert>

/**
 * global singleton for asagi
 */
CAsagi *cAsagi;



CAsagi::CAsagi(
		const char *i_filename_bathymetry,
		const char *i_filename_displacements
)
{
	/*
	 * BATHYMETRY
	 */
	singleton_bathymetry = asagi::Grid::create(asagi::Grid::FLOAT);

	if (singleton_bathymetry->open(i_filename_bathymetry) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open bathymetry file '" << i_filename_bathymetry << std::endl;
		exit(-1);
	}

	bathymetry_min_x = CAsagi::singleton_bathymetry->getXMin();
	bathymetry_min_y = CAsagi::singleton_bathymetry->getYMin();

	bathymetry_max_x = CAsagi::singleton_bathymetry->getXMax();
	bathymetry_max_y = CAsagi::singleton_bathymetry->getYMax();

	bathymetry_size_x = bathymetry_max_x - bathymetry_min_x;
	bathymetry_size_y = bathymetry_max_y - bathymetry_min_y;

	/*
	 * domain size reduced to quad
	 */
	double min_size = CMath::min(bathymetry_size_x, bathymetry_size_y);

	double padding = min_size*0.001;
	double new_size = min_size - 2.0*padding;	// apply padding on both sides

	bathymetry_size_x = new_size;
	bathymetry_size_y = new_size;

	bathymetry_min_x += padding;
	bathymetry_min_y += padding;

	bathymetry_max_x = bathymetry_min_x + bathymetry_size_x;
	bathymetry_max_y = bathymetry_min_y + bathymetry_size_y;


	std::cout << "Bathymetry start: (" << bathymetry_min_x << ", " << bathymetry_min_y << ")" << std::endl;
	std::cout << "Bathymetry size: (" << bathymetry_size_x << ", " << bathymetry_size_y << ")" << std::endl;

	/**
	 * DISPLACEMENTS
	 */
	singleton_displacements = asagi::Grid::create(asagi::Grid::FLOAT);

	if (singleton_displacements->open(i_filename_displacements) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open file '" << i_filename_displacements << std::endl;
		exit(-1);
	}


	displacements_min_x = CAsagi::singleton_displacements->getXMin();
	displacements_min_y = CAsagi::singleton_displacements->getYMin();

	displacements_max_x = CAsagi::singleton_displacements->getXMax();
	displacements_max_y = CAsagi::singleton_displacements->getYMax();

	displacements_size_x = displacements_max_x - displacements_min_x;
	displacements_size_y = displacements_max_y - displacements_min_y;


	std::cout << "Displacement window: (" << CAsagi::singleton_displacements->getXMin() << ", " << CAsagi::singleton_displacements->getYMin() <<
			") x (" << CAsagi::singleton_displacements->getXMax() << ", " << CAsagi::singleton_displacements->getYMax() << ");" << std::endl;

	std::cout << "Displacement size: ("
			<< (CAsagi::singleton_displacements->getXMax() - CAsagi::singleton_displacements->getXMin()) <<
			", "
			<< (CAsagi::singleton_displacements->getYMax() - CAsagi::singleton_displacements->getYMin())<< ");" << std::endl;

	std::cout << "ASAGI !" << std::endl;
}

CAsagi::~CAsagi()
{
	delete singleton_bathymetry;
	delete singleton_displacements;
}


asagi::Grid *CAsagi::singleton_bathymetry;
asagi::Grid *CAsagi::singleton_displacements;


CTsunamiSimulationTypes::TVertexScalar getAsagiElementBathymetryMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_depth
	)
{
	typedef CTsunamiSimulationTypes::TVisualizationVertexScalar T;

	T mx = (i_mx+1.0)*0.5;
	T my = (i_my+1.0)*0.5;

	return CAsagi::singleton_bathymetry->getFloat2D(
			mx * cAsagi->bathymetry_max_x - cAsagi->bathymetry_min_x*(mx - 1.0),
			my * cAsagi->bathymetry_max_y - cAsagi->bathymetry_min_y*(my - 1.0),
			0);
}



CTsunamiSimulationTypes::TVertexScalar getAsagiElementDisplacementMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_size
	)
{
	typedef CTsunamiSimulationTypes::TVisualizationVertexScalar T;

	T mx = (i_mx+1.0)*0.5;
	T my = (i_my+1.0)*0.5;

	mx = mx * cAsagi->bathymetry_max_x - cAsagi->bathymetry_min_x*(mx - 1.0);
	my = my * cAsagi->bathymetry_max_y - cAsagi->bathymetry_min_y*(my - 1.0);

	if (mx < cAsagi->displacements_min_x)
		return 0;

	if (mx > cAsagi->displacements_max_x)
		return 0;

	if (my < cAsagi->displacements_min_y)
		return 0;

	if (my > cAsagi->displacements_max_y)
		return 0;

	return	CAsagi::singleton_displacements->getFloat2D(mx, my, 0);
}
