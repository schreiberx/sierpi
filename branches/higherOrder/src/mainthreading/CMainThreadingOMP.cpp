/*
 * CMainThreadingOMP.cpp
 *
 *  Created on: May 8, 2012
 *      Author: schreibm
 */

#include "CMainThreadingOMP.hpp"


#include <sys/types.h>
#include <unistd.h>
#include <linux/unistd.h>
#include <errno.h>
#include <pthread.h>
#include <cassert>
#include <signal.h>
#include <pthread.h>

#include <sstream>
#include <ostream>

#include "CAffinityMap.hpp"

pid_t gettid()
{
	return (pid_t)syscall(__NR_gettid);
}



bool CMainThreadingOMP_setAffinities(
	int i_num_threads,
	int i_max_cores,
	int i_pinning_distance,
	int i_pinning_start_id,
	int i_verbose_level
)
{
	if (i_pinning_start_id < 0)
		i_pinning_start_id = 0;

	assert(i_num_threads < 1024);
	assert(i_num_threads > 0);

	assert(i_max_cores < 1024);
	assert(i_max_cores > 0);

	int affinityMap[1024];
	if (!CAffinityMap::setup(i_num_threads, i_max_cores, i_pinning_distance, i_pinning_start_id, i_verbose_level, affinityMap))
		return true;

	cpu_set_t cpu_set;
	CPU_ZERO(&cpu_set);
	CPU_SET(affinityMap[0], &cpu_set);

	// pin the first pid to the first cpu
	int retval = sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpu_set);
	assert(retval == 0);

#pragma omp parallel for shared(affinityMap)  schedule(static,1)
	for (int i = 0; i < i_num_threads; i++)
	{
		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);
		CPU_SET(affinityMap[i], &cpu_set);

		int retval = sched_setaffinity(gettid(), sizeof(cpu_set_t), &cpu_set);
		assert(retval == 0);
	}


	return true;
}
