/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 4, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_PARALLEL_CLUSTER_HANDLER_HPP_
#define CTSUNAMI_PARALLEL_CLUSTER_HANDLER_HPP_

#include "../tsunami_common/tsunami_config.h"

// typedefs for types related to basic tsunami simulation
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Element_Splats_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Smooth_Element_Tsunami.hpp"
#endif

// specialized traversators (to speed up compilation due to parallel processing)
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Element.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

// structure setup traversator
#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"

#include "libsierpi/parallelization/CPartition_TreeNode.hpp"
#include "libsierpi/domain_triangulation/CDomain_BaseTriangulation.hpp"
#include "libsierpi/parallelization/CPartition_ExchangeEdgeCommData.hpp"

#include "libsierpi/parallelization/CPartition_ExchangeFluxCommData.hpp"

#if COMPILE_SIMULATION_WITH_GUI && CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
#include "libsierpi/parallelization/CPartition_ExchangeVertexDataCommData.hpp"
#endif

#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_ElementData_Normal_SelectByPoint.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
	#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif

#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"

#include "libsierpi/parallelization/CStackAccessorMethods.hpp"


/**
 * \brief Tsunami Simulation Cluster Handler
 *
 * This class implements all kinds of sub-partition related user-defined data storages.
 */
class CSimulationTsunami_Parallel_Cluster
{
public:
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_Cluster> CPartition_TreeNode_;
	typedef CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartition_;

	typedef CTsunamiElementData	CElementData;
	typedef CTsunamiEdgeData	CEdgeData;

	typedef CSimulationStacks<CTsunamiSimulationTypes> CTsunamiSimulationStacks;
	typedef CTsunamiSimulationTypes CSimulationTypes;

	/**
	 * pointer to handler of this partition node
	 */
	CPartition_TreeNode_ *cPartition_TreeNode;

	/**
	 * TIMESTEP: EDGE COMM
	 */
	/// edge communication and simulation without adaptivity
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/**
	 * TSUNAMI ADAPTIVITY
	 */
	/// adaptivity - refine/coarsen
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData cTsunami_Adaptive;


	/**
	 * WATER COLUMN SETUP ADAPTIVITY
	 */
	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;


	/**
	 * EDGE COMM FOR ADAPTIVE TRAVERSALS
	 */
	/// handler for adaptivity information exchange with adjacent partitions
	CPartition_ExchangeEdgeCommData<
			CPartition_TreeNode_,
			char,
			CStackAccessorMethodsAdaptivityEdgeData<CPartition_TreeNode_>
		> cPartition_ExchangeEdgeCommData_Adaptivity;


	/**
	 * INTER-PARTITION-EDGE COMMUNICATION
	 */

	/// handler for flux communication with adjacent partitions
	CPartition_ExchangeFluxCommData<
			CPartition_TreeNode_,
			CTsunamiEdgeData,
			CStackAccessorMethodsTsunamiEdgeData<CPartition_TreeNode_, CTsunamiEdgeData>,
			sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth::CKernelClass
		> cPartition_ExchangeFluxCommData_Tsunami;

#if COMPILE_SIMULATION_WITH_GUI && CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
	/**
	 * INTER-PARTITION-VERTEX-DATA COMMUNICATION
	 */
	/// handler for vertex communication with adjacent partitions
	CPartition_ExchangeVertexDataCommData<
			CPartition_TreeNode_,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,0>::TRAV::CKernelClass
		> cPartition_ExchangeVertexDataCommData_WaterSurface;

	/// handler for vertex communication with adjacent partitions
	CPartition_ExchangeVertexDataCommData<
			CPartition_TreeNode_,
			CTsunamiSimulationTypes::TVisualizationVertexData,
			CStackAccessorMethodsTsunamiVertexData<CPartition_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>,
			sierpi::kernels::COpenGL_Vertices_Smooth_Element_Tsunami<CTsunamiSimulationTypes,3>::TRAV::CKernelClass
		> cPartition_ExchangeVertexDataCommData_Terrain;
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	// validate edge communication
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;

	/// handler for edge communication with adjacent partitions
	CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, CValEdgeData, CStackAccessorMethodsValidationEdgeData<CPartition_TreeNode_> > cPartitionTree_Node_EdgeComm_Validation;
#endif


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	// this traversal is used to setup the element data vertex coordinates during the initial setup
	sierpi::kernels::CSetup_TsunamiElementData_Validation::TRAV cSetup_TsunamiElementData_Validation;
#endif

#if SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER == 2
	// only computed explicit euler updates are stored to this element data sets
	CStack<CTsunamiElementData> cStackElementDataRK2_f_t0;

	// second euler updates
	CStack<CTsunamiElementData> cStackElementDataRK2_yapprox_t1_AND_f_t1;
#endif

	/*
	 * fast accessor for triangle factory
	 */
	CTriangle_Factory &cTriangleFactory;


	/**
	 * number of triangles in this sub-partition
	 */
//	unsigned long long number_of_triangles;


	/**
	 * stored cfl condition values after edge communication!
	 *
	 * This is used in the ongoing adaptive step to finally update the cfl to it's accurate number
	 */
	TTsunamiDataScalar cfl_domain_size_div_max_wave_speed_after_edge_comm;

	/**
	 * stored cfl condition values after adaptivity communication
	 */
	TTsunamiDataScalar cfl_domain_size_div_max_wave_speed_after_adaptivity;


	/**
	 * constructor for setup
	 *
	 * this constructor is called whenever a split or merge operation is executed.
	 */
	CSimulationTsunami_Parallel_Cluster(
			CPartition_TreeNode_ *i_cPartition_TreeNode,					///< pointer to partition tree node of this partition
			CSimulationTsunami_Parallel_Cluster *i_cluster_parameterSetup	///< partition handler to setup parameters
		)	:
		cPartition_TreeNode(i_cPartition_TreeNode),
		cPartition_ExchangeEdgeCommData_Adaptivity(i_cPartition_TreeNode),
		cPartition_ExchangeFluxCommData_Tsunami(i_cPartition_TreeNode, cTsunami_EdgeComm.cKernelClass),

#if COMPILE_SIMULATION_WITH_GUI && CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
		cPartition_ExchangeVertexDataCommData_WaterSurface(i_cPartition_TreeNode, nullptr),
		cPartition_ExchangeVertexDataCommData_Terrain(i_cPartition_TreeNode, nullptr),
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cPartitionTree_Node_EdgeComm_Validation(i_cPartition_TreeNode),
#endif
		cTriangleFactory(i_cPartition_TreeNode->cTriangleFactory)
	{
		setup(i_cluster_parameterSetup);
	}



	/**
	 * setup method to initialize traversal methods and parameters
	 */
	void setup(
			CSimulationTsunami_Parallel_Cluster *i_cluster_parameterSetup	///< partition handler to setup parameters
		)
	{
		if (i_cluster_parameterSetup == nullptr)
		{
			/*
			 * this has to be a domain root sub-partition and its parameters
			 * are set-up at another place.
			 */
			cSetup_Column.setup_sfcMethods(cTriangleFactory);

			cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
			cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			cEdgeComm_ValidateComm.setup_sfcMethods(cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			cSetup_TsunamiElementData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

			cfl_domain_size_div_max_wave_speed_after_edge_comm = 0;
			cfl_domain_size_div_max_wave_speed_after_adaptivity = 0;

			return;
		}


		/**
		 * SETUP traversals and parameters based on another node
		 */
		cSetup_Column.setup_Partition(i_cluster_parameterSetup->cSetup_Column, cTriangleFactory);

		cTsunami_EdgeComm.setup_Partition(i_cluster_parameterSetup->cTsunami_EdgeComm, cTriangleFactory);

		cTsunami_Adaptive.setup_Partition(i_cluster_parameterSetup->cTsunami_Adaptive, cTriangleFactory);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cEdgeComm_ValidateComm.setup_Partition(i_cluster_parameterSetup->cEdgeComm_ValidateComm, cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.setup_Partition(i_cluster_parameterSetup->cSetup_TsunamiElementData_Validation, cTriangleFactory);
#endif

		cfl_domain_size_div_max_wave_speed_after_edge_comm = i_cluster_parameterSetup->cfl_domain_size_div_max_wave_speed_after_edge_comm;
		cfl_domain_size_div_max_wave_speed_after_adaptivity = i_cluster_parameterSetup->cfl_domain_size_div_max_wave_speed_after_adaptivity;
	}

};



#endif
