/*
 * Grid.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: breuera
 */

#ifndef GRID_HPP_
#define GRID_HPP_

#include <cassert>
#include <cmath>
#include <vector>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Cell.hpp"
#include "Edge.hpp"
#include "../tsunami_common/flux_solver/CFluxSolver_AugumentedRiemann.hpp"
#include "../tsunami_common/benchmarks/CSingleWaveOnSingleBeach.hpp"

// define a zero tolerance if not defined before
#ifndef ZEROTOL
#define ZEROTOL
const static double zeroTol = 0.0000001;
#endif

namespace oneDimensional {
  template <typename T> class Grid;
}

/**
 * Representation of the discrete one dimensional domain.
 */
template <typename T> class oneDimensional::Grid {
  //private:

  //! left domain boundary
  const T xMin;

  //! right domain boundary
  const T xMax;

  //! vector, which holds the cells
  std::vector< Cell<T> > cells;

  //! vector, which holds the edges
  std::vector< Edge<T> > edges;

  //! available boundary conditions
  enum BoundaryCondition {
    Outflow, Wall
  };

  //! boundary condition for the left boundary
  BoundaryCondition leftBoundaryCondition;

  //! boundary condition for the right boundary
  BoundaryCondition rightBoundaryCondition;

  /**
   * Compte the midpoint of a given cell.
   *
   * @param i_cellId cell id.
   * @return mid-point of the cell.
   */
  T computeMidPoint( const int i_cellId) {
    T midPoint = (edges[i_cellId].getPosition() + edges[i_cellId+1].getPosition())/(T)2.;
    return midPoint;
  }

  public:
    /**
     * Constructor of the grid.
     * Sets up a regular grid with grid spacing (i_xMax - i_xMin)/i_numberOfCells
     *
     * @param i_numberOfCells number of the cells in the initial grid.
     * @param i_xMin left domain boundary.
     * @param i_xMax right domain boundary
     */
    Grid( const int i_numberOfCells,
          const T i_xMin,
          const T i_xMax,
          const BoundaryCondition i_leftBoundaryCondition = Outflow,
          const BoundaryCondition i_rightBoundaryCondition = Outflow ): xMin(i_xMin), xMax(i_xMax) {

      // assert a proper defined domain
      assert( std::abs(i_xMax - i_xMin) > zeroTol );
      assert( i_numberOfCells > 0);

      // compute cell size
      T l_deltaX = (i_xMax - i_xMin)/i_numberOfCells;

      T l_leftEdgePos = i_xMin;
      // generate the grid
      for(int i = 0; i < i_numberOfCells; i++) {
        cells.push_back( oneDimensional::Cell<T>(l_deltaX) );

        edges.push_back( oneDimensional::Edge<T>(l_leftEdgePos) );
        l_leftEdgePos += l_deltaX;
      }
      // edge at the right domain boundary
      edges.push_back( oneDimensional::Edge<T>(l_leftEdgePos) );

      // assert that there are enough edges for the cells
      assert(edges.size() - cells.size() == 1);

      // assert boundary conditions = outflow as nothing else is implemented so far
      assert(i_leftBoundaryCondition == Outflow);
      assert(i_rightBoundaryCondition == Outflow);
      leftBoundaryCondition = i_leftBoundaryCondition;
      rightBoundaryCondition = i_rightBoundaryCondition;
    }
    virtual ~Grid() {
      // TODO Auto-generated destructor stub
    }

    /**
     * Set initial cell values according to the specified scenario.
     */
    void setInitialCellValues() {
      //TODO: hard coded example
      CSingleWaveOnSingleBeach<T> singleWaveOnASimpleBeach;
      for(unsigned int i = 0; i < cells.size(); i++) {
        //compute the midpoint of the cell
        T l_midPoint = computeMidPoint(i);
        cells[i].getCellData().h = singleWaveOnASimpleBeach.getWaterHeight(l_midPoint);
        cells[i].getCellData().qx = singleWaveOnASimpleBeach.getMomentum(l_midPoint);
        cells[i].getCellData().b = singleWaveOnASimpleBeach.getBathymetry(l_midPoint);

//        if(cells[i].getCellData().b < (T)0)
//          cells[i].getCellData().h -= cells[i].getCellData().b;
      }
    }

    /**
     * Set the bathymetry for a number of connected cells.
     * @param i_firstCellId Id of the first cell.
     * @param i_lastCellId Id of the last cell.
     */
    void setBathymetry( const unsigned int i_firstCellId,
                        const unsigned int i_lastCellId ) {
      //assert valid IDs
      assert(i_firstCellId <= i_lastCellId);
      assert(i_lastCellId < cells.size());

      CSingleWaveOnSingleBeach<T> singleWaveOnASimpleBeach;
      //set the bathymetry
      for( unsigned int i = i_firstCellId; i <= i_lastCellId; i++) {
        //compute the midpoint of the cell
        T l_midPoint = computeMidPoint(i);
        cells[i].getCellData().b = singleWaveOnASimpleBeach.getBathymetry(l_midPoint);
      }
    }

    /**
     * Copy the cell values to the corresponding edges.
     */
    void copyCellValuesToEdges() {
      for(unsigned int i = 0; i < cells.size(); i++) {
        //edge on the left side of the cell
        edges[i].getRightCellData() = cells[i].getCellData();
        //edge on the right side of the cell
        edges[i+1].getLeftCellData() = cells[i].getCellData();
      }
    }

    /**
     * Set the ghost cells according to the specified boundary conditions.
     */
    void setGhostValues() {
      //left boundary
      if(leftBoundaryCondition == Outflow)
        edges[0].getLeftCellData() = edges[0].getRightCellData();
      else
        assert(false); // TODO: assert boundary conditions = outflow as nothing else is implemented so far

      //right boundary
      if(rightBoundaryCondition == Outflow)
        edges[edges.size()-1].getRightCellData() = edges[edges.size()-1].getLeftCellData();
      else
        assert(false); // TODO: sassert boundary conditions = outflow as nothing else is implemented so far
    }

    /**
     * Compute net-updates from edge data and store them there.
     */
    void computeNetUpdates() {
      //TODO: hard coded tolerances
      CFluxSolver_AugumentedRiemann<T> augRieSolver((T)0.00001, (T)1., (T)0.000000001, 10, (T)0.00000000001);

      for(unsigned int i = 0; i < edges.size(); i++) {
        augRieSolver.computeNetUpdates( edges[i].getLeftCellData(),           edges[i].getRightCellData(),
                                        edges[i].getLeftCellData(),           edges[i].getRightCellData(),
                                        edges[i].getLeftCellData().waveSpeed, edges[i].getRightCellData().waveSpeed,
                                        9.81 );
      }
    }

    /**
     * Update all cell values (Global time step) with the specified with.
     *
     * @param i_timeStepWith width of the timestep.
     */
    void updateCellValues( const T i_timeStepWidth ) {
      for(unsigned int i = 0; i < cells.size(); i++) {
        assert(cells[i].getCellWidth() > zeroTol);
        T l_updateSize = i_timeStepWidth / cells[i].getCellWidth();

        //update with left net update
        cells[i].getCellData().h  -= l_updateSize * edges[i].getRightCellData().h;
        cells[i].getCellData().qx -= l_updateSize * edges[i].getRightCellData().qx;

        //update with right net update
        cells[i].getCellData().h  -= l_updateSize * edges[i+1].getLeftCellData().h;
        cells[i].getCellData().qx -= l_updateSize * edges[i+1].getLeftCellData().qx;

        //assert positivity preserving
        assert(cells[i].getCellData().h >= (T)0);
      }
    }

    /**
     * Sets the maximum allowed local time step width for each cell with respect to the given CFL number.
     * @param i_cflNumber CFL number
     */
    void setLocalTimeStepWidths( const T i_cflNumber ) {
      for(unsigned int i = 0; i < cells.size(); i++) {
        //compute maximum speed of the waves propgating inside the cell
        T l_maximumWaveSpeed = edges[i].getRightCellData().waveSpeed;
        l_maximumWaveSpeed = std::max( l_maximumWaveSpeed, std::abs(edges[i+1].getLeftCellData().waveSpeed) );

        //update the global time step width
        if(l_maximumWaveSpeed > zeroTol)
          cells[i].setTimeStepWidth( cells[i].getCellWidth()/l_maximumWaveSpeed );
        else
          cells[i].setTimeStepWidth( std::numeric_limits<T>::max() );
      }
    }

    /**
     * Compute the maximum allowed global time step with respect to the given CFL-number
     *
     * @param i_cflNumber CFL number.
     * @return maximum allowed time step width.
     */
    T computeMaximumGlobalTimeStepWidth( const T i_cflNumber ) {
      T l_globalTimeStepWidth = std::numeric_limits<T>::max();
//      for(unsigned int i = 0; i < cells.size(); i++) {
//        //compute maximum speed of the waves propgating inside the cell
//        T l_maximumWaveSpeed = edges[i].getRightCellData().waveSpeed;
//        l_maximumWaveSpeed = std::max( l_maximumWaveSpeed, std::abs(edges[i+1].getLeftCellData().waveSpeed) );
//
//        //update the global time step width
//        if(l_maximumWaveSpeed > zeroTol)
//          l_globalTimeStepWidth = std::min(l_globalTimeStepWidth, cells[i].getCellWidth()/l_maximumWaveSpeed);
//      }

      setLocalTimeStepWidths( i_cflNumber );

      for(unsigned int i = 0; i < cells.size(); i++) {
        l_globalTimeStepWidth = std::min( l_globalTimeStepWidth, cells[i].getTimeStepWidth() );
      }

      assert( l_globalTimeStepWidth > zeroTol );
      assert( l_globalTimeStepWidth < std::numeric_limits<T>::max() );

      return l_globalTimeStepWidth;
    }

    /**
     * Get the number of grid cells.
     *
     * @return number of grid cells.
     */
    int const getNumberOfCells() {
      return cells.size();
    }

    /**
     * Get the number of edges.
     *
     * @return number of edges.
     */
    int const getNumberOfEdges() {
      return edges.size();
    }

    /**
     * Print the values of the cells in the grid
     */
    void const printCellValues() {
      for(unsigned int i = 0; i < cells.size(); i++) {
        std::cout << " cell " << i << ": " << cells[i].getCellData()
                                   << std::endl;
      }
    }

    /**
     * Print the values of the edges in the grid
     */
    void const printEdgeValues() {
      for(unsigned int i = 0; i < edges.size(); i++) {
        std::cout << " edge " << i << " - left: " << edges[i].getLeftCellData()
                                   << " - right: " << edges[i].getRightCellData()
                                                  << std::endl;
      }
    }

    /**
     * Call the verification routine for a specified time.
     *
     * @param i_simulationTime simulation time.
     */
    void callVerifactionForGlobalTimeStep( const T i_simulationTime ) {
      std::cout << "callVerifactionForGlobalTimeStep(...) called" << std::endl;
      for( unsigned int i = 0; i < cells.size(); i++ ) {
        T l_midPoint = computeMidPoint(i);
        //define something
        //call your verification routine:
        // routine(i_simulationTime, l_midPoint, cells[i].getWaterHeight(), cells[i].getMomentum);
      }
    }

    //***********************************************************************************************
    //******************************Adaptivity routines**********************************************
    //***********************************************************************************************
    void refineCell( const unsigned int i_cellId,
                     const int i_numberOfCells ) {
      //assert valid input data
      assert(i_cellId < cells.size());
      assert(i_numberOfCells > 0);
      //TODO
      assert(false);
    }

    /**
     * Coarsen connected cells to one cell. This cell will hold the average values of the original cells.
     *
     * @param i_idFirstCell ID of the first cell.
     * @param i_idLastCell ID of the last cell.
     */
    void coarsenCells( const unsigned int i_idFirstCell, const unsigned int i_idLastCell ) {
      // assert valid ids
//      assert(i_idFirstCell > 0); //unsigned..
      assert(i_idLastCell < cells.size());
      assert(i_idFirstCell < i_idLastCell);

      // cell variables of the new coarsened cell
      T l_totalLength = (T)0.;
      T l_midWaterHeight = (T)0.;
      T l_midMomentum = (T)0.;
      T l_midBathymetry = (T)0.;

      // compute quantities
      for(unsigned int i = i_idFirstCell; i <= i_idLastCell; i++) {
        l_totalLength += cells[i].getCellWidth();
        l_midWaterHeight += cells[i].getWaterHeight() * cells[i].getCellWidth();
        l_midMomentum += cells[i].getMomentum() *  cells[i].getCellWidth();
        l_midBathymetry += cells[i].getBathymetry() * cells[i].getCellWidth();
      }
      l_midWaterHeight /= l_totalLength;
      l_midMomentum /= l_totalLength;
      l_midBathymetry /= l_totalLength;

      //remove the cells (except for one, which is the coarsened cell
      cells.erase( cells.begin()+i_idFirstCell, cells.begin()+i_idLastCell );
      //and their edges
      edges.erase( edges.begin()+i_idFirstCell+1, edges.begin()+i_idLastCell+1 );

      //assign new values to the coarsened cell
      cells[i_idFirstCell].getCellData().h = l_midWaterHeight;
      cells[i_idFirstCell].getCellData().qx = l_midMomentum;
      cells[i_idFirstCell].getCellData().b = l_midBathymetry;
      cells[i_idFirstCell].setCellWidth(l_totalLength);

      //update the edge positions
      edges[i_idFirstCell+1].setPosition( edges[i_idFirstCell].getPosition() + l_totalLength );

      assert( cells.size() + 1 == edges.size());
    }

    //***********************************************************************************************
    //**************************************IO routines**********************************************
    //***********************************************************************************************
    /**
     * Write the current grid data to a VTK file.
     *
     * @param i_fileNumber number of the file.
     */
    void writeVtkFile(const std::string i_path, const int i_fileNumber) {
      std::stringstream completeFileName;
      completeFileName << i_path << i_fileNumber << ".vtk";

      std::ofstream vtkFile;
      vtkFile.open(completeFileName.str().c_str());
      // version identifier
      vtkFile <<"# vtk DataFile Version 3.0"<<std::endl;
      // description
      vtkFile << "Results from the 1D test code: Alexander Breuer, Martin Schreiber"<<std::endl;
      //file format
      vtkFile << "ASCII" << std::endl;

      // geometry description
      vtkFile << "DATASET RECTILINEAR_GRID" << std::endl;
      vtkFile << "DIMENSIONS " << getNumberOfEdges() << " 1 1" << std::endl;

      vtkFile << "X_COORDINATES " << getNumberOfEdges() << " float" << std::endl;
      for(unsigned int i = 0; i < edges.size(); i++)
        vtkFile << edges[i].getPosition() << std::endl;

      vtkFile << "Y_COORDINATES " << 1 << " float" << std::endl;
      vtkFile << "0" << std::endl;

      vtkFile << "Z_COORDINATES " << 1 << " float" << std::endl;
      vtkFile << "0" << std::endl;

      vtkFile << "CELL_DATA " << getNumberOfCells() << std::endl;
      vtkFile << "SCALARS h float 1" << std::endl;
      vtkFile << "LOOKUP_TABLE default" << std::endl;
      for(unsigned int i = 0; i < cells.size(); i++)
        vtkFile << cells[i].getWaterHeight() << std::endl;

      vtkFile << "SCALARS hu float 1" << std::endl;
      vtkFile << "LOOKUP_TABLE default" << std::endl;
      for(unsigned int i = 0; i < cells.size(); i++)
        vtkFile << cells[i].getMomentum() << std::endl;

      vtkFile << "SCALARS b float 1" << std::endl;
      vtkFile << "LOOKUP_TABLE default" << std::endl;
      for(unsigned int i = 0; i < cells.size(); i++)
        vtkFile << cells[i].getBathymetry() << std::endl;

      vtkFile.close();

    }
};

#endif /* GRID_HPP_ */
