/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "CSpecialized_Tsunami_Setup_Column.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData.hpp"
#include "../kernels/modifiers/CSetup_Column.hpp"

#include "../CTsunamiSimulationDataSets.hpp"

namespace sierpi
{
namespace travs
{
class CSpecialized_Tsunami_Setup_Column_Private	:
	public CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData<
		sierpi::kernels::CSetup_Column,	// setup of column
		CTsunamiSimulationStacks		// tsunami element data
	>
{
};


CSpecialized_Tsunami_Setup_Column::CSpecialized_Tsunami_Setup_Column()
{
	generic_traversator = new CSpecialized_Tsunami_Setup_Column_Private;
}


CSpecialized_Tsunami_Setup_Column::~CSpecialized_Tsunami_Setup_Column()
{
	delete generic_traversator;
}



void CSpecialized_Tsunami_Setup_Column::setup_RootTraversator(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_RootTraversator(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}



void CSpecialized_Tsunami_Setup_Column::setup_KernelClass(
		TTsunamiDataScalar p_x,
		TTsunamiDataScalar p_y,
		TTsunamiDataScalar p_radius,

		int i_methodId,
		CTsunamiSimulationDataSets *i_cSimulationDataSets
)
{
	generic_traversator->cKernelClass.setup_Parameters(
			p_x,
			p_y,
			p_radius,
			i_methodId,
			i_cSimulationDataSets
		);
}


bool CSpecialized_Tsunami_Setup_Column::actionFirstTraversal(CTsunamiSimulationStacks *cStacks)
{
	repeat_traversal = generic_traversator->firstTraversal.action(cStacks);
	return repeat_traversal;
}

bool CSpecialized_Tsunami_Setup_Column::actionMiddleTraversals_Parallel(CTsunamiSimulationStacks *cStacks)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Parallel(cStacks, repeat_traversal);
	return repeat_traversal;
}



bool CSpecialized_Tsunami_Setup_Column::actionMiddleTraversals_Serial(CTsunamiSimulationStacks *cStacks)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Serial(cStacks, repeat_traversal);
	return repeat_traversal;
}


void CSpecialized_Tsunami_Setup_Column::actionLastTraversal_Parallel(
		CTsunamiSimulationStacks *cStacks,
		CPartition_SplitJoin_EdgeComm_Information &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_Tsunami_Setup_Column::actionLastTraversal_Serial(
		CTsunamiSimulationStacks *cStacks
)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_Tsunami_Setup_Column::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_RootPartition(p_triangleFactory);
}


/**
 * setup the initial partition traversal for the given factory
 */
void CSpecialized_Tsunami_Setup_Column::setup_Partition(
		CSpecialized_Tsunami_Setup_Column &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Partition(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->cKernelClass.setup_WithKernel(
			parent.generic_traversator->cKernelClass
		);
}



void CSpecialized_Tsunami_Setup_Column::storeReduceValue(
		TTsunamiDataScalar *o_cflReduceValue
	)
{
	generic_traversator->cKernelClass.storeReduceValue(o_cflReduceValue);
}

}
}
