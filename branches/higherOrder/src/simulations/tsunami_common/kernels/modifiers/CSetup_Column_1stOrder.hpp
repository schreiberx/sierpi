/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_SETUP_COLUMN_1ST_ORDER_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_1ST_ORDER_TSUNAMI_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

//#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData.hpp"
#include "../common/CAdaptivity_1stOrder.hpp"
#include "simulations/tsunami_common/CTsunamiSimulationDataSets.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename TElementData, typename TSimulationStacks>
class CSetup_Column_1stOrder	: public CAdaptivity_1stOrder
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData<CSetup_Column_1stOrder, TSimulationStacks>	TRAV;

	typedef TTsunamiVertexScalar TVertexScalar;
	typedef TTsunamiVertexScalar T;
	typedef typename TSimulationStacks::TEdgeData TEdgeData;

	/*
	 * refinement/coarsening parameters
	 */
	TTsunamiDataScalar refine_threshold;
	TTsunamiDataScalar coarsen_threshold;

	// center of column
	TTsunamiVertexScalar columnCenterX, columnCenterY;

	// radius of column
	TTsunamiVertexScalar columnRadius;
	TTsunamiVertexScalar columnRadiusSquared;


	/**
	 * specifies how to setup domain
	 *
	 * 0: reset all to outer_elementData
	 * 1: terrain only
	 * 2: column refinement (preprocessing for step 3)
	 * 3: column setup
	 */
	int setupMethodId;


	/*
	 * callback for terrain data
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;


	CSetup_Column_1stOrder()	:
		columnCenterX(0),
		columnCenterY(0),
		columnRadius(0),
		setupMethodId(0),
		cSimulationDataSets(nullptr)
	{
	}

	inline TTsunamiVertexScalar squaredDistToCenter(
			TVertexScalar px, TVertexScalar py
	)
	{
		TVertexScalar x = columnCenterX-px;
		TVertexScalar y = columnCenterY-py;
		return x*x+y*y;
	}


	inline bool insideColumn(
			TVertexScalar px, TVertexScalar py
	)
	{
		return squaredDistToCenter(px, py) < columnRadiusSquared;
	}


	inline bool should_refine(
			TVertexScalar vleft_x, TVertexScalar vleft_y,
			TVertexScalar vright_x, TVertexScalar vright_y,
			TVertexScalar vtop_x, TVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *o_elementData
	)
	{

		/*
		 * check whether the line on the circle intersects with the triangle
		 */
		int counter = 0;
		counter += (int)insideColumn(vleft_x, vleft_y);
		counter += (int)insideColumn(vright_x, vright_y);
		counter += (int)insideColumn(vtop_x, vtop_y);

		if (counter == 0)
		{
			/*
			 * completely outside circle
			 */
			// nothing to to & no refinement request
			return false;
		}

		return true;
/*
		// circle intersects triangle
		if (counter == 1 || counter == 2)
			return true;

		// nothing to do & no refinement request
		return false;
*/
	}


	inline bool should_coarsen(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,
			
			CTsunamiElementData *element
	)
	{
		/**
		 * we use the coarsening callback to update the column inner elements data (even if element data should not be accessed)
		 */

		if (!insideColumn(vx1, vy1))
			return false;

		if (!insideColumn(vx2, vy2))
			return false;

		if (!insideColumn(vx3, vy3))
			return false;

		return false;
	}


	void traversal_pre_hook()
	{
	}

	void traversal_post_hook()
	{
	}


	void setup_Parameters(
			TTsunamiVertexScalar p_columnCenterX,
			TTsunamiVertexScalar p_columnCenterY,
			TTsunamiVertexScalar p_columnRadius,

			int i_setupMethodId,
			CTsunamiSimulationDataSets *i_cSimulationDataSets
	)
	{
		columnCenterX = p_columnCenterX;
		columnCenterY = p_columnCenterY;
		columnRadius = p_columnRadius;
		columnRadiusSquared = columnRadius*columnRadius;

		setupMethodId = i_setupMethodId;
		cSimulationDataSets = i_cSimulationDataSets;
	}


	void setup_WithKernel(
			CSetup_Column_1stOrder<TElementData, TSimulationStacks> &parent
	)
	{
		columnCenterX = parent.columnCenterX;
		columnCenterY = parent.columnCenterY;
		columnRadius = parent.columnRadius;
		columnRadiusSquared = parent.columnRadiusSquared;

		cSimulationDataSets = parent.cSimulationDataSets;
	}



	/**
	 * COARSEN / REFINE
	 */
	inline void refine_l_r(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *element,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element
	)
	{
		// left element
		left_element->hyp_edge = element->left_edge;
		left_element->right_edge = element->hyp_edge;
		left_element->left_edge.h = (element->left_edge.h+element->right_edge.h)*(TVertexScalar)0.5;
		left_element->left_edge.qx = (element->left_edge.qx+element->right_edge.qx)*(TVertexScalar)0.5;
		left_element->left_edge.qy = (element->left_edge.qy+element->right_edge.qy)*(TVertexScalar)0.5;
		left_element->left_edge.b = (element->left_edge.b+element->right_edge.b)*(TVertexScalar)0.5;
		left_element->cfl_domain_size_div_max_wave_speed = element->cfl_domain_size_div_max_wave_speed;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		left_element->refine = false;
		left_element->coarsen = false;
#endif

		// right element
		right_element->hyp_edge = element->right_edge;
		right_element->left_edge = left_element->right_edge;
		right_element->right_edge = left_element->left_edge;
		right_element->cfl_domain_size_div_max_wave_speed = element->cfl_domain_size_div_max_wave_speed;

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		right_element->refine = false;
		right_element->coarsen = false;
#endif
/*
		TVertexScalar hyp_mid_edge_x = (vx1+vx2)*(TVertexScalar)0.5;
		TVertexScalar hyp_mid_edge_y = (vy1+vy2)*(TVertexScalar)0.5;
*/

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		left_element->validation.setupLeftElementFromParent(&element->validation);
		right_element->validation.setupRightElementFromParent(&element->validation);
#endif
	}




	inline void refine_ll_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_element
				);

		// LEFT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,

					depth+1,

					left_right_element, left_left_element, left_right_element
				);
	}



	inline void refine_l_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,

				depth,

				i_elementData, left_element, right_right_element
			);

		// RIGHT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,

					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void refine_ll_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,
			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// RIGHT + LEFT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_right_element);

		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
	
					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
			
					depth+1,

					left_right_element, left_left_element, left_right_element);


		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
					
					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					
					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void coarsen(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *coarsed_element,

			CTsunamiElementData *left_element,
			CTsunamiElementData *right_element
	)
	{
		// there was never an agreement to coarsening
		assert(false);
	}


	void storeReduceValue(
			TTsunamiDataScalar *o_reduceValue
	)
	{
		*o_reduceValue = 0;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
