/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_CADAPTIVE_TSUNAMI_0THORDER_HPP_
#define KERNEL_CADAPTIVE_TSUNAMI_0THORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

//#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData.hpp"
#include "../common/CAdaptivity_0thOrder.hpp"

#include "lib/CDebugSetupDone.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 0th order
 */
class CAdaptive_Tsunami_0thOrder	: public CAdaptivity_0thOrder
{
	DEBUG_SETUP_DONE_VAR
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData<CAdaptive_Tsunami_0thOrder, CTsunamiElementData>	TRAV;
	typedef TTsunamiVertexScalar TVertexScalar;

	/*
	 * refinement/coarsening parameters
	 */
#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
	TTsunamiDataScalar refine_height_threshold;
	TTsunamiDataScalar coarsen_height_threshold;

	TTsunamiDataScalar refine_slope_threshold;
	TTsunamiDataScalar coarsen_slope_threshold;
#endif



	CAdaptive_Tsunami_0thOrder()

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
	:
		refine_height_threshold(-1),
		coarsen_height_threshold(-1),
		refine_slope_threshold(-1),
		coarsen_slope_threshold(-1)
#endif
	{
		DEBUG_SETUP_DONE_CONSTRUCTOR;
	}



	inline bool should_refine(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int i_depth,
			CTsunamiElementData *i_element_data
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
	#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (i_element_data->refine)
		{
			i_element_data->refine = false;
			return true;
		}
	#endif

	if (i_element_data->dofs.h <= 0)
		return false;

	return (i_element_data->dofs.h+i_element_data->dofs.b > refine_height_threshold);
#endif

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		return i_element_data->refine;
#endif

	}



	inline bool should_coarsen(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar i_normal_hypx,		TTsunamiDataScalar i_normal_hypy,
			TTsunamiDataScalar i_normal_rightx,	TTsunamiDataScalar i_normal_righty,
			TTsunamiDataScalar i_normal_leftx,	TTsunamiDataScalar i_normal_lefty,

			int i_depth,
			CTsunamiElementData *i_element_data
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
	#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (i_element_data->coarsen)
		{
			i_element_data->coarsen = false;
			return true;
		}
	#endif

	return (i_element_data->dofs.h+i_element_data->dofs.b < coarsen_height_threshold);
#endif

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
		return i_element_data->coarsen;
#endif
	}

	void storeReduceValue(
			TReduceValue *o_reduceValue
	)
	{
		*o_reduceValue = cfl_domain_size_div_max_wave_speed;
	}


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void elementAction(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *io_elementData
	)
	{
	}

public:
	inline void refine_l_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		setupRefinedElements(
			vleft_x,	vleft_y,
			vright_x,	vright_y,
			vtop_x,		vtop_y,

			normal_hypx,	normal_hypy,
			normal_rightx,	normal_righty,
			normal_leftx,	normal_lefty,

			depth,

			i_elementData,
			o_left_elementData,
			o_right_elementData
		);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_coarsed_element->refine = false;
		o_coarsed_element->coarsen = false;
#endif
/*
 NOTE! VALIDATION SETUP IS DONE IN setupRefinedElements.
 VALIDATION SETUP IS NOT ALLOOWED HERE DUE TO SPECIFIC ORDER TO SETUP REFINED ELEMENTS

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_left_elementData->validation.setupLeftElementFromParent(&i_elementData->validation);
		o_right_elementData->validation.setupRightElementFromParent(&i_elementData->validation);
#endif
*/
	}



	inline void refine_ll_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_element
				);

		// LEFT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,

					depth+1,

					left_right_element, left_left_element, left_right_element
				);
	}



	inline void refine_l_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				depth,
				i_elementData, left_element, right_right_element
			);

		// RIGHT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,

					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void refine_ll_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// RIGHT + LEFT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,
					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,
					depth,
					i_elementData, left_right_element, right_right_element
				);

		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
	
					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
			
					depth+1,
					left_right_element, left_left_element, left_right_element
				);

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
					
					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					
					depth+1,

					right_right_element, right_left_element, right_right_element
				);
	}


	inline void coarsen(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *o_coarsed_elementData,

			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
		setupCoarsendElements(
			vleft_x,	vleft_y,
			vright_x,	vright_y,
			vtop_x,	vtop_y,

			normal_hypx,	normal_hypy,
			normal_rightx,	normal_righty,
			normal_leftx,	normal_lefty,

			depth,

			o_coarsed_elementData,
			i_left_elementData,
			i_right_elementData
		);
	}


	void setup_WithParameters(
			TTsunamiDataScalar p_cathetus_side_length,

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
			TTsunamiDataScalar i_refine_height_threshold,
			TTsunamiDataScalar i_coarsen_height_threshold,

			TTsunamiDataScalar i_refine_slope_threshold,
			TTsunamiDataScalar i_coarsen_slope_threshold,
#endif

			CTsunamiSimulationDataSets *i_cSimulationDataSets
	)
	{
		DEBUG_SETUP_DONE_SETUP;

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
		refine_height_threshold = i_refine_height_threshold;
		coarsen_height_threshold = i_coarsen_height_threshold;

		refine_slope_threshold = i_refine_slope_threshold;
		coarsen_slope_threshold = i_coarsen_slope_threshold;
#endif

		cathetus_side_length = p_cathetus_side_length;

		cSimulationDataSets = i_cSimulationDataSets;
	}


	void setup_WithKernel(const CAdaptive_Tsunami_0thOrder &parent)
	{
		DEBUG_SETUP_DONE_SETUP;

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
		refine_height_threshold = parent.refine_height_threshold;
		coarsen_height_threshold = parent.coarsen_height_threshold;

		refine_slope_threshold = parent.refine_slope_threshold;
		coarsen_slope_threshold = parent.coarsen_slope_threshold;
#endif

		cathetus_side_length = parent.cathetus_side_length;

		cSimulationDataSets = parent.cSimulationDataSets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
