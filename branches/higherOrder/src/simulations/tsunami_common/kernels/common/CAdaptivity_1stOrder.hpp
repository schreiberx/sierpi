/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_1STORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_1STORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"



/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CAdaptivity_1stOrder
{
public:
	typedef TTsunamiVertexScalar T;
	typedef CTsunamiEdgeData TEdgeData;

	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	TTsunamiDataScalar cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CTsunamiSimulationDataSets *cSimulationDataSets;

	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef TTsunamiDataScalar TReduceValue;


	/**
	 * The maximum computed squared domain size divided by the
	 * maximum squared speed is stored right here
	 */
	TReduceValue cfl_domain_size_div_max_wave_speed;

	/**
	 * constructor
	 */
	CAdaptivity_1stOrder()	:
		cathetus_side_length(-1),
		cSimulationDataSets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
		/**
		 * update CFL number to infinity to show that no change to the cfl was done so far
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	void traversal_post_hook()
	{
	}


	/**
	 * get center of weight for triangle
	 */
	inline void computeCenterOfWeight(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiVertexScalar *o_mx, TTsunamiVertexScalar *o_my
	)
	{
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(TTsunamiVertexScalar)(1.0/3.0) +
				(vleft_x - vtop_x)*(TTsunamiVertexScalar)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(TTsunamiVertexScalar)(1.0/3.0) +
				(vleft_y - vtop_y)*(TTsunamiVertexScalar)(1.0/3.0);
	}



	/**
	 * get center of weight for triangle for both children
	 */
	inline void computeCenterOfWeightForLeftAndRightChild(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiVertexScalar *o_left_mx, TTsunamiVertexScalar *o_left_my,
			TTsunamiVertexScalar *o_right_mx, TTsunamiVertexScalar *o_right_my
	)
	{
		// midpoint on hypotenuse
		TTsunamiDataScalar mx = (vleft_x + vright_x)*(TTsunamiDataScalar)(1.0/2.0);
		TTsunamiDataScalar my = (vleft_y + vright_y)*(TTsunamiDataScalar)(1.0/2.0);

		TTsunamiDataScalar dx_left = (vleft_x - mx)*(TTsunamiDataScalar)(1.0/3.0);
		TTsunamiDataScalar dy_left = (vleft_y - my)*(TTsunamiDataScalar)(1.0/3.0);

		TTsunamiDataScalar dx_up = (vtop_x - mx)*(TTsunamiDataScalar)(1.0/3.0);
		TTsunamiDataScalar dy_up = (vtop_y - my)*(TTsunamiDataScalar)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;
	}


	/**
	 * setup both refined elements
	 */
	inline void setupRefinedElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif
		// left element
		o_left_elementData->hyp_edge = i_elementData->left_edge;
		o_left_elementData->right_edge = i_elementData->hyp_edge;
		o_left_elementData->left_edge.h = (i_elementData->left_edge.h+i_elementData->right_edge.h)*(TTsunamiDataScalar)0.5;
		o_left_elementData->left_edge.qx = (i_elementData->left_edge.qx+i_elementData->right_edge.qx)*(TTsunamiDataScalar)0.5;
		o_left_elementData->left_edge.qy = (i_elementData->left_edge.qy+i_elementData->right_edge.qy)*(TTsunamiDataScalar)0.5;
		o_left_elementData->left_edge.b = (i_elementData->left_edge.b+i_elementData->right_edge.b)*(TTsunamiDataScalar)0.5;
#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_left_elementData->refine = false;
		o_left_elementData->coarsen = false;
#endif

		// right element
		o_right_elementData->hyp_edge = i_elementData->right_edge;
		o_right_elementData->right_edge = o_left_elementData->left_edge;
		o_right_elementData->left_edge = i_elementData->hyp_edge;


		TTsunamiDataScalar local_cfl_domain_size_div_max_wave_speed = i_elementData->cfl_domain_size_div_max_wave_speed*(TTsunamiDataScalar)(1.0/CMath::sqrt(2.0));

		o_left_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		o_right_elementData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;

		cfl_domain_size_div_max_wave_speed = CMath::min(cfl_domain_size_div_max_wave_speed, local_cfl_domain_size_div_max_wave_speed);



#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRefineLeftAndRight(
				normal_hypx,	normal_hypy,
				normal_rightx,	normal_righty,
				normal_leftx,	normal_lefty,
				depth,
				&o_left_elementData->validation,
				&o_right_elementData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void setupCoarsendElements(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int i_depth,

			CTsunamiElementData *o_elementData,
			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		{
			TTsunamiVertexScalar mx = (vleft_x + vright_x)*(TTsunamiVertexScalar)0.5;
			TTsunamiVertexScalar my = (vleft_y + vright_y)*(TTsunamiVertexScalar)0.5;

			i_left_elementData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_elementData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif

		o_elementData->left_edge = i_left_elementData->hyp_edge;
		o_elementData->right_edge = i_right_elementData->hyp_edge;
		o_elementData->hyp_edge.h = (i_left_elementData->right_edge.h+i_right_elementData->left_edge.h)*(TTsunamiDataScalar)0.5;
		o_elementData->hyp_edge.qx = (i_left_elementData->right_edge.qx+i_right_elementData->left_edge.qx)*(TTsunamiDataScalar)0.5;
		o_elementData->hyp_edge.qy = (i_left_elementData->right_edge.qy+i_right_elementData->left_edge.qy)*(TTsunamiDataScalar)0.5;
		o_elementData->hyp_edge.b = (i_left_elementData->right_edge.b+i_right_elementData->left_edge.b)*(TTsunamiDataScalar)0.5;

		o_elementData->cfl_domain_size_div_max_wave_speed = (i_left_elementData->cfl_domain_size_div_max_wave_speed+i_right_elementData->cfl_domain_size_div_max_wave_speed)*(TTsunamiDataScalar)(0.5*CMath::sqrt(2.0));

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_elementData->refine = false;
		o_elementData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_elementData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_elementData->validation,
				&i_right_elementData->validation
		);
#endif
	}
};


#endif /* CADAPTIVITY_0STORDER_HPP_ */
