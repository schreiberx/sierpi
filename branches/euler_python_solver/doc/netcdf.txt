
* hdf5-1.8.9:
	URL: http://www.hdfgroup.org/HDF5/

	CC=icc CXX=icpc ./configure --prefix=$HOME/local


* netcdf-4.1.3:
	URL: http://www.unidata.ucar.edu/software/netcdf/

	CPPFLAGS=-I$HOME/local/include LDFLAGS=-L$HOME/local/lib CC=icc CXX=icpc F90=ifort ./configure --enable-shared --enable-cxx-4 --prefix=$HOME/local

