Problem size with different number of CPUs using function optimization
Depth: 20
Timesteps: 
Cache: L1-dcache-load-misses
THREADS	DEPTH	MTPS	CACHE_MISSES	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS
4,568,407,129 L1-dcache-load-misses (scaled from 23.53%)
1	20	5.64238	     4,568,407,129 L1-dcache-load-misses     (scaled from 23.53%)		500	0.0346964	185.839	2.09715e+06	303.872
4,613,988,671 L1-dcache-load-misses (scaled from 23.54%)
2	20	10.9789	     4,613,988,671 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	95.5083	2.09715e+06	303.872
4,644,308,126 L1-dcache-load-misses (scaled from 23.54%)
3	20	15.9286	     4,644,308,126 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	65.8299	2.09715e+06	303.872
4,652,040,262 L1-dcache-load-misses (scaled from 23.53%)
4	20	20.4734	     4,652,040,262 L1-dcache-load-misses     (scaled from 23.53%)		500	0.0346964	51.2166	2.09715e+06	303.872
4,695,053,720 L1-dcache-load-misses (scaled from 23.54%)
5	20	24.2119	     4,695,053,720 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	43.3083	2.09715e+06	303.872
4,790,839,131 L1-dcache-load-misses (scaled from 23.53%)
6	20	27.3497	     4,790,839,131 L1-dcache-load-misses     (scaled from 23.53%)		500	0.0346964	38.3396	2.09715e+06	303.872
4,783,815,191 L1-dcache-load-misses (scaled from 23.53%)
7	20	31.3667	     4,783,815,191 L1-dcache-load-misses     (scaled from 23.53%)		500	0.0346964	33.4296	2.09715e+06	303.872
4,742,486,159 L1-dcache-load-misses (scaled from 23.54%)
8	20	35.8744	     4,742,486,159 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	29.2291	2.09715e+06	303.872
4,856,438,652 L1-dcache-load-misses (scaled from 23.54%)
9	20	38.3706	     4,856,438,652 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	27.3276	2.09715e+06	303.872
4,767,616,707 L1-dcache-load-misses (scaled from 23.54%)
10	20	42.2892	     4,767,616,707 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	24.7953	2.09715e+06	303.872
4,781,661,030 L1-dcache-load-misses (scaled from 23.54%)
11	20	44.7456	     4,781,661,030 L1-dcache-load-misses     (scaled from 23.54%)		500	0.0346964	23.4342	2.09715e+06	303.872
4,840,008,697 L1-dcache-load-misses (scaled from 23.56%)
12	20	47.1188	     4,840,008,697 L1-dcache-load-misses     (scaled from 23.56%)		500	0.0346964	22.2539	2.09715e+06	303.872
