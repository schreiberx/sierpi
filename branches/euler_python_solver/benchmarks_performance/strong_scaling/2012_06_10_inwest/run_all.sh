#! /bin/bash


DATE=2012_06_10


EXECS=../../build/sierpi_intel_*

PARAMS="-o $((32*1024))"


for e in $EXECS; do
	ENAME=${e/..\/..\/build\//}
	FILENAME=`echo "$e" | sed "s/.*\///"`
	./run_benchmark.sh $e $PARAMS -A 1  > "output_"$DATE"_"$ENAME"_d20_a8_t100.txt"
done
