/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTraversatorClassInc_SetupAdaptiveFirstPass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


/**********************************************************
 * IMPORTANT INFORMATION!!!
 *
 * This file is included inside a class scope.
 **********************************************************
 */

/**
 * method for forward traversals
 */
TRecursiveMethod sfcRecursiveMethod_Forward;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory cTriangleFactory;


/**
 * minimum depth limiters to restrict coarsening
 */
int depth_limiter_min;

/**
 * maximum depth limiters to restrict refinement
 */
int depth_limiter_max;


/**
 * setup the initial cluster traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &i_cTriangleFactory	///< triangle factory to find the first method
)
{
	cTriangleFactory = i_cTriangleFactory;

	sfcRecursiveMethod_Forward = getSFCMethod<SFC_METHOD_OLD_TO_NEW_EDGE_TYPES>(cTriangleFactory);

	setup_TraversatorParameters(cTriangleFactory.minDepth, cTriangleFactory.maxDepth);
}


/**
 * setup the parameters with the one by the parent cluster
 */
void setup_Cluster(
		TThisClass &i_parentTraversator,		///< information of parent traversator
		CTriangle_Factory &i_cTriangleFactory	///< triangle factory to find the first method
)
{
	// make sure that this is really a root node
	assert(i_cTriangleFactory.clusterTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	setup_sfcMethods(i_cTriangleFactory);

	cKernelClass.setup_WithKernel(i_parentTraversator.cKernelClass);
}

