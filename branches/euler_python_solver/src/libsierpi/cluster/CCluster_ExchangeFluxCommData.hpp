/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CCluster_ExchangeFluxCommData.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CPARTITIONTREE_NODE_FLUXCOMM_HPP
#define CPARTITIONTREE_NODE_FLUXCOMM_HPP

#include "config.h"
#include "libsierpi/grid/CDomain_BaseTriangle.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"
#include <string.h>

#if CONFIG_ENABLE_MPI
#include <mpi.h>
#endif


namespace sierpi
{

/**
 * this class handles the communication with the adjacent edges based on the adjacency
 * information given in CClusterTree_Node.
 */
template <	typename CCluster_TreeNode,			///< cluster tree node
			typename TEdgeElement,					///< type of single edge element
			typename CStackAccessors,				///< access to stacks
			typename CKernelWith_op_edge_edge	///< kernel type offering 'op_edge_edge'
		>
class CCluster_ExchangeFluxCommData	: public CStackAccessors
{
private:
	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode> CEdgeComm_InformationAdjacentCluster_;

private:
	CStack<TEdgeElement> *localHypExchangeEdgeStack;
	CStack<TEdgeElement> *localCatExchangeEdgeStack;

	CStack<TEdgeElement> *localHypEdgeStack;
	CStack<TEdgeElement> *localCatEdgeStack;

	/**
	 * Handler to root clusterTreeNode
	 */
	CCluster_TreeNode *cClusterTreeNode;


	/**
	 * Pointer to kernel offering method 'op_edge_edge' to
	 * compute the flux updates
	 */
	CKernelWith_op_edge_edge *cKernel_withComputeNetUpdates;


	/**
	 * Constructor
	 */
public:
	CCluster_ExchangeFluxCommData(
			CCluster_TreeNode *p_cClusterTreeNode,
			CKernelWith_op_edge_edge *p_cKernelWith_op_edge_edge
	)
	{
		cClusterTreeNode = p_cClusterTreeNode;
		cKernel_withComputeNetUpdates = p_cKernelWith_op_edge_edge;
	}



	/**
	 * return the information about where to find the communication data at the adjacent triangle stack
	 */
private:
	void _getEdgeCommDataPointersFromAdjacentCluster(
			CCluster_UniqueId &i_thisUniqueId,				///< unique id of cluster to write data to
			CCluster_TreeNode *i_adjacentClusterTreeNode,	///< pointer to adjacent cluster
			TEdgeElement **o_src_edgeElements,					///< output: pointer to starting point of data to be exchanged
			int *o_edgeElementCount,							///< output: number of elements stored on the stack
			bool *o_clockwise									///< output: true, if edge elements are stored with clockwise traversal
	)	const
	{
		int pos;

		/*
		 * first of all, we search on the hyp communication information to find the appropriate subset
		 */
		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
					i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			if (adjacent_info.comm_elements == 0)
				continue;
#endif

			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjHypStack;

				if (i_adjacentClusterTreeNode->cTriangleFactory.isHypotenuseDataOnLeftStack())
				{
					/*
					 * clockwise stack is always the left one
					 */
					adjHypStack = this->leftStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjHypStack = this->rightStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.comm_elements;
				*o_src_edgeElements = adjHypStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.comm_elements;
		}

		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
					i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;
			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjCatStack;
				if (i_adjacentClusterTreeNode->cTriangleFactory.isCathetusDataOnLeftStack())
				{
					adjCatStack = this->leftStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjCatStack = this->rightStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.comm_elements;
				*o_src_edgeElements = adjCatStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.comm_elements;
		}

		std::cout << "FATAL ERROR: Communication partner not found! 2" << std::endl;
		assert(false);
	}



	/**
	 * this method is executed for every adjacent cluster simulation to
	 * pull the edge communication data from an adjacent cluster.
	 */
private:
	void _pullEdgeCommDataFromSharedMemoryCluster(
			CStack<TEdgeElement> *io_cLocalExchangeStack,	///< stack to store edge element data to
			bool i_clockwise,								///< order in which to store data
			const CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
			unsigned long local_edge_comm_stack_index		///< index to local edge comm stack to start copying data
	)
	{
		TEdgeElement *src_edgeElements = nullptr;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		/*
		 * first we search for the adjacent cluster
		 */
		_getEdgeCommDataPointersFromAdjacentCluster(
					cClusterTreeNode->uniqueId,
					i_informationAdjacentCluster.clusterTreeNode,
					&src_edgeElements,
					&edgeElementCount,
					&adj_clockwise
				);

		if (i_informationAdjacentCluster.comm_elements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + cluster id: " << cClusterTreeNode->uniqueId << std::endl;
			assert(false);
		}

		if (i_clockwise != adj_clockwise)
		{
			/*
			 * directly copy stack elements
			 */

			/*
			 * this does not work anymore since there may be gaps within the edge comm stack due to
			 * partially exchanged data
			 */
			//memcpy(i_cStack->getStackPtr(), src_edgeElements, sizeof(TEdgeElement)*edgeElementCount);

			memcpy(io_cLocalExchangeStack->getElementPtrAtIndex(local_edge_comm_stack_index), src_edgeElements, sizeof(TEdgeElement)*edgeElementCount);
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			size_t adj_i = edgeElementCount-1;

			for (int i = 0; i < edgeElementCount; i++)
			{
				memcpy(io_cLocalExchangeStack->getElementPtrAtIndex(local_edge_comm_stack_index+i), &(src_edgeElements[adj_i]), sizeof(TEdgeElement));
				adj_i--;
			}
		}
	}



	/**
	 * this method is executed for every adjacent cluster to
	 * pull the edge communication data from an adjacent cluster.
	 */
private:
	template <bool updateAdjacentExchangeEdgeData>
	void _pullEdgeCommDataFromAdjacentCluster_AND_computeFlux(
			CStack<TEdgeElement> *io_cLocalExchangeStack,		///< stack to store edge element data to
			const CStack<TEdgeElement> *io_cLocalStack,		///< stack with previously written data needed to compute the flux
			bool i_clockwise,									///< order in which to store data
			const CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
			unsigned long local_edge_comm_stack_index			///< index to local edge comm stack to start copying data
	)
	{
		TEdgeElement *adjacent_edgeCommElements = nullptr;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		/*
		 * first we search for the adjacent cluster
		 */
		_getEdgeCommDataPointersFromAdjacentCluster(
					cClusterTreeNode->uniqueId,	///< unique id
					i_informationAdjacentCluster.clusterTreeNode,	///< pointer to cluster tree node
					&adjacent_edgeCommElements,	///< pointer pointing into stack
					&edgeElementCount,			///< number of elements store don stack
					&adj_clockwise				///< adjacent data stored in clockwise order?
				);

#if DEBUG
		if (i_informationAdjacentCluster.comm_elements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + cluster id: " << cClusterTreeNode->uniqueId << std::endl;
			assert(i_informationAdjacentCluster.comm_elements != edgeElementCount);
		}
#endif

		TEdgeElement tmpEdgeElement;

		if (i_clockwise != adj_clockwise)
		{
			TEdgeElement *local_edgeCommStackElement = io_cLocalStack->getElementPtrAtIndex(local_edge_comm_stack_index);
			TEdgeElement *local_exchangeEdgeCommStackElement = io_cLocalExchangeStack->getElementPtrAtIndex(local_edge_comm_stack_index);

			for (int i = 0; i < edgeElementCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernel_withComputeNetUpdates->op_edge_edge(
						*local_edgeCommStackElement,					/// left edge comm element
						adjacent_edgeCommElements[i],					/// adjacent right edge comm element

						*local_exchangeEdgeCommStackElement,			/// output: net updates for local cell
						tmpEdgeElement									/// output: net updates for adjacent cell
					);

				/*
				 * update adjacent exchange edge data only for single-flux-evaluation
				 */
				if (updateAdjacentExchangeEdgeData)
				{
					memcpy(local_edgeCommStackElement, &tmpEdgeElement, sizeof(TEdgeElement));
				}

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
			}
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			size_t adj_i = edgeElementCount-1;

			TEdgeElement *local_edgeCommStackElement = io_cLocalStack->getElementPtrAtIndex(local_edge_comm_stack_index);
			TEdgeElement *local_exchangeEdgeCommStackElement = io_cLocalExchangeStack->getElementPtrAtIndex(local_edge_comm_stack_index);

			for (int i = 0; i < edgeElementCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernel_withComputeNetUpdates->op_edge_edge(
						*local_edgeCommStackElement,					/// left edge comm element
						adjacent_edgeCommElements[adj_i],				/// adjacent right edge comm element

						*local_exchangeEdgeCommStackElement,			/// output: local left flux
						tmpEdgeElement									/// output: adjacent right flux
					);

				if (updateAdjacentExchangeEdgeData)
				{
					// copy to local edge comm stack since this element is read by the adjacent sub-cluster during the 2nd pass
					memcpy(local_edgeCommStackElement, &tmpEdgeElement, sizeof(TEdgeElement));
				}

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
				adj_i--;
			}
		}
	}


#if CONFIG_ENABLE_MPI

	/**
	 * this method is executed to exchange MPI communication data
	 */
private:
	inline void _sendEdgeCommDataToDistributedMemoryCluster_pass1(
			CStack<TEdgeElement> *o_cStack,				///< stack to store edge element data to
			CStack<TEdgeElement> *i_cStack,				///< stack to load edge element data from
			CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
			unsigned long local_edge_comm_stack_index,		///< index to local edge comm stack to start copying data
			bool clockwise_edge
	)
	{

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		if (i_informationAdjacentCluster.comm_elements == 0)
			return;
#endif

		/**
		 * directly send and receive stack elements
		 */
		i_informationAdjacentCluster.mpi_last_send_data_ptr = i_cStack->getElementPtrAtIndex(local_edge_comm_stack_index);

		MPI_Isend(
				i_informationAdjacentCluster.mpi_last_send_data_ptr,
				sizeof(TEdgeElement)*i_informationAdjacentCluster.comm_elements,	///< size
				MPI_BYTE,										///< data type
				i_informationAdjacentCluster.mpi_rank,			///< destination rank
				clockwise_edge,									///< use information of clockwise edge traversal direction
																///< for periodic boundary conditions
				MPI_COMM_WORLD,									///< communicator
				&(i_informationAdjacentCluster.mpi_requests[0])
			);

		i_informationAdjacentCluster.mpi_last_recv_data_ptr = o_cStack->getElementPtrAtIndex(local_edge_comm_stack_index);
	}

	/**
	 * swap e1 and e2
	 */
	inline void swapEdgeElement(TEdgeElement *e1, TEdgeElement *e2)
	{
		TEdgeElement tmp;
		memcpy(&tmp, e1, sizeof(TEdgeElement));
		memcpy(e1, e2, sizeof(TEdgeElement));
		memcpy(e2, &tmp, sizeof(TEdgeElement));
	}



	/**
	 * this method is executed to trigger receive commands for the MPI requests
	 */
private:
	void _receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
			CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
			bool clockwise_edge			///< use information of clockwise edge traversal direction
										///< for periodic boundary conditions
	)
	{
		MPI_Recv(
				i_informationAdjacentCluster.mpi_last_recv_data_ptr,					///< destination pointer
				sizeof(TEdgeElement)*i_informationAdjacentCluster.comm_elements,
				MPI_BYTE,
				i_informationAdjacentCluster.mpi_rank,
				clockwise_edge,								///< tag
				MPI_COMM_WORLD,
				0
			);

		// assume that the adjacent cluster stack direction forces a reversal of the communication data
		TEdgeElement *local_edge_element, *adjacent_edge_element;

		/*
		 * reverse elements
		 */
		adjacent_edge_element = (TEdgeElement*)i_informationAdjacentCluster.mpi_last_recv_data_ptr;

		TEdgeElement tmpElement;
		size_t switch_id = i_informationAdjacentCluster.comm_elements-1;
		for (int i = 0; i < i_informationAdjacentCluster.comm_elements/2; i++)
		{
			swapEdgeElement(&(adjacent_edge_element[i]), &(adjacent_edge_element[switch_id]));
			switch_id--;
		}


		/*
		 * compute fluxes
		 */
		local_edge_element = (TEdgeElement*)i_informationAdjacentCluster.mpi_last_send_data_ptr;
		adjacent_edge_element = (TEdgeElement*)i_informationAdjacentCluster.mpi_last_recv_data_ptr;

		TEdgeElement local_netUpdate, adjacent_netUpdate;
		assert(cKernel_withComputeNetUpdates != nullptr);

		for (int i = 0; i < i_informationAdjacentCluster.comm_elements; i++)
		{
			// compute net update and store to tmpEdgeElement to avoid race conditions
			cKernel_withComputeNetUpdates->op_edge_edge(
					*local_edge_element,		/// left edge comm element
					*adjacent_edge_element,	/// adjacent right edge comm element

					local_netUpdate,		/// output: local left flux
					adjacent_netUpdate		/// output: adjacent right flux
				);

			memcpy(adjacent_edge_element, &local_netUpdate, sizeof(TEdgeElement));

			local_edge_element++;
			adjacent_edge_element++;
		}

		MPI_Wait(&(i_informationAdjacentCluster.mpi_requests[0]), 0);
	}

#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

private:
	void validateEdgeCommDataLengthFromCluster(
			CStack<TEdgeElement> *stack,			///< stack to load edge element data from
			bool clockwise,							///< order in which to store data
			CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster	///< information about adjacent clusters
	)	const
	{
		TEdgeElement *src_edgeElements = NULL;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		if (i_informationAdjacentCluster.comm_elements == 0)
			return;
#endif

		if (i_informationAdjacentCluster.clusterTreeNode != nullptr)
		{
			_getEdgeCommDataPointersFromAdjacentCluster(
						cClusterTreeNode->uniqueId,
						i_informationAdjacentCluster.clusterTreeNode,
						&src_edgeElements,
						&edgeElementCount,
						&adj_clockwise
					);

#if DEBUG
			if (i_informationAdjacentCluster.comm_elements != edgeElementCount)
			{
				std::cerr << "EDGE COMM FAILURE 1: comm Elements mismatch" << std::endl;
				std::cerr << " + cluster id: " << cClusterTreeNode->uniqueId << std::endl;
				assert(i_informationAdjacentCluster.comm_elements != edgeElementCount);
			}
#endif
		}
	}
#endif


	/**
	 * this method is executed by the simulation running on the cluster after global synch to
	 * read the communication stack elements from the adjacent clusters.
	 *
	 * !!! only load adjacent edge comm data when localUniqueId < adjacentUniqueId !!!
	 *
	 * for distributed memory systems (MPI), additional:
	 *   - send operation has to be executed for adjacent clusters stored
	 *     on a different node and a
	 *   - receive operation to read the adjacent data previously send by an adjacent node.
	 */
public:
	void pullEdgeCommData_SM_pass1()
	{
		/**
		 * true, when traversal along hyp/cat is clockwise
		 */
		bool hypClockwise, catClockwise;
	
		/**
		 * setup stacks to store data to
		 */
		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			if (cClusterTreeNode->uniqueId < cEdgeComm_InformationAdjacentCluster.uniqueId)
			{
#if CONFIG_ENABLE_MPI
				if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
#endif
				{
					_pullEdgeCommDataFromAdjacentCluster_AND_computeFlux<true>(
							localHypExchangeEdgeStack,
							localHypEdgeStack,
							hypClockwise,
							cEdgeComm_InformationAdjacentCluster,
							local_edge_comm_stack_index
						);
				}
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
		// fix exchange stack size
		localHypExchangeEdgeStack->setStackElementCounter(localHypEdgeStack->getNumberOfElementsOnStack());


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			// only load adjacent edge comm data when localUniqueId < adjacentUniqueId
			if (cClusterTreeNode->uniqueId < cEdgeComm_InformationAdjacentCluster.uniqueId)
			{
#if CONFIG_ENABLE_MPI
				if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
#endif
				{
					_pullEdgeCommDataFromAdjacentCluster_AND_computeFlux<true>(
							localCatExchangeEdgeStack,
							localCatEdgeStack,
							catClockwise,
							cEdgeComm_InformationAdjacentCluster,
							local_edge_comm_stack_index
						);
				}
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
		// fix exchange stack size
		localCatExchangeEdgeStack->setStackElementCounter(localCatEdgeStack->getNumberOfElementsOnStack());
	}



public:
	void pullEdgeCommData_DM_pass1()
	{
		/**
		 * true, when traversal along hyp/cat is clockwise
		 */

		bool hypClockwise, catClockwise;

		/**
		 * setup stacks to store data to
		 */
		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_sendEdgeCommDataToDistributedMemoryCluster_pass1(
						localHypExchangeEdgeStack,	///< receive stack
						localHypEdgeStack,	///< send stack
						cEdgeComm_InformationAdjacentCluster,
						local_edge_comm_stack_index,
						hypClockwise
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_sendEdgeCommDataToDistributedMemoryCluster_pass1(
						localCatExchangeEdgeStack,		///< receive stack
						localCatEdgeStack,				///< send stack
						cEdgeComm_InformationAdjacentCluster,
						local_edge_comm_stack_index,
						catClockwise
					);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
	}



	/**
	 * this method is executed by the simulation running on the cluster after global synch to
	 * read the communication stack elements from the adjacent clusters
	 *
	 * !!! only load adjacent edge comm data when localUniqueId > adjacentUniqueId !!!
	 */
public:
	void pullEdgeCommData_SM_pass2()
	{
		/**
		 * true, when traversal along hyp/cat is clockwise
		 */
		bool hypClockwise, catClockwise;

		/**
		 * TODO: remove me - this should be already setup
		 */
		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}

		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

#if !CONFIG_ENABLE_MPI

			// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
			if (cClusterTreeNode->uniqueId > cEdgeComm_InformationAdjacentCluster.uniqueId)
			{
				_pullEdgeCommDataFromSharedMemoryCluster(
						localHypExchangeEdgeStack,
						hypClockwise,
						cEdgeComm_InformationAdjacentCluster,
						local_edge_comm_stack_index
					);
			}

#else

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
			{
				// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
				if (cClusterTreeNode->uniqueId > cEdgeComm_InformationAdjacentCluster.uniqueId)
				{
					_pullEdgeCommDataFromSharedMemoryCluster(
							localHypExchangeEdgeStack,
							hypClockwise,
							cEdgeComm_InformationAdjacentCluster,
							local_edge_comm_stack_index
						);
				}
			}

#endif

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

#if !CONFIG_ENABLE_MPI

			// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
			if (cClusterTreeNode->uniqueId > cEdgeComm_InformationAdjacentCluster.uniqueId)
			{
				_pullEdgeCommDataFromSharedMemoryCluster(
						localCatExchangeEdgeStack,
						catClockwise,
						cEdgeComm_InformationAdjacentCluster,
						local_edge_comm_stack_index
					);
			}

#else

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
			{
				// Only load adjacent edge comm data when localUniqueId > adjacentUniqueId
				if (cClusterTreeNode->uniqueId > cEdgeComm_InformationAdjacentCluster.uniqueId)
				{
					_pullEdgeCommDataFromSharedMemoryCluster(
							localCatExchangeEdgeStack,
							catClockwise,
							cEdgeComm_InformationAdjacentCluster,
							local_edge_comm_stack_index
						);
				}
			}

#endif

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
	}



	/**
	 * this method is executed by the simulation running on the cluster after global synch to
	 * read the communication stack elements from the adjacent clusters
	 *
	 * !!! only load adjacent edge comm data when localUniqueId > adjacentUniqueId !!!
	 */
public:
	void pullEdgeCommData_DM_pass2()
	{
		/**
		 * true, when traversal along hyp/cat is clockwise
		 */

		bool hypClockwise, catClockwise;

		/**
		 * TODO: remove me - this should be already setup
		 */
		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}

		int hyp_comm_data_size = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size();

		for (int i = hyp_comm_data_size-1; i >= 0; i--)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters[i];

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
						cEdgeComm_InformationAdjacentCluster,
						hypClockwise
					);
			}
		}

		int cat_comm_data_size = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();

		for (int i = cat_comm_data_size-1; i >= 0; i--)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters[i];

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
						cEdgeComm_InformationAdjacentCluster,
						catClockwise
					);
			}
		}
	}



	/**
	 * this method is executed by the simulation running on a cluster reading the adjacent
	 * edge communication.
	 *
	 * in this way the fluxes for the shared sub-cluster borders are double evaluated.
	 * 
	 * For MPI processes, those fluxes are double evaluated.
	 */
public:
	void pullEdgeCommData_doubleFluxEvaluationSharedMemory()
	{
		/**
		 * true, when traversal along hyp/cat is clockwise
		 */
		bool hypClockwise, catClockwise;


		/**
		 * setup stacks to store data to
		 */
		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->leftStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			localCatExchangeEdgeStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			localHypEdgeStack = this->rightStackAccessor(cClusterTreeNode);
			localCatEdgeStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}


		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

//			std::cout << "HYP COMM (double): " << cEdgeComm_InformationAdjacentCluster.comm_elements << " " << cEdgeComm_InformationAdjacentCluster.mpi_rank << std::endl;

			// only load adjacent edge comm data when localUniqueId < adjacentUniqueId
			_pullEdgeCommDataFromAdjacentCluster_AND_computeFlux<false>(
					localHypExchangeEdgeStack,	// local destination
					localHypEdgeStack,			// local source
					hypClockwise,
					cEdgeComm_InformationAdjacentCluster,
					local_edge_comm_stack_index
				);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}

		assert(local_edge_comm_stack_index == localHypEdgeStack->getNumberOfElementsOnStack());

		localHypExchangeEdgeStack->setStackElementCounter(localHypEdgeStack->getNumberOfElementsOnStack());


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
						cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

//			std::cout << "CAT COMM (double): " << cEdgeComm_InformationAdjacentCluster.comm_elements << " " << cEdgeComm_InformationAdjacentCluster.mpi_rank << std::endl;

			_pullEdgeCommDataFromAdjacentCluster_AND_computeFlux<false>(
					localCatExchangeEdgeStack,	// local destination
					localCatEdgeStack,			// local source
					catClockwise,
					cEdgeComm_InformationAdjacentCluster,
					local_edge_comm_stack_index
				);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}

		assert(local_edge_comm_stack_index == localCatEdgeStack->getNumberOfElementsOnStack());

		// fix exchange stack size
		localCatExchangeEdgeStack->setStackElementCounter(localCatEdgeStack->getNumberOfElementsOnStack());
	}



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

public:
	void validateCommDataLength()	const
	{
		bool hypClockwise = cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			// hypEdgeStack was not initialized yet -> ignore
			if (localHypExchangeEdgeStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromCluster(localHypExchangeEdgeStack, hypClockwise, *iter);
		}


		bool catClockwise = cClusterTreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			// catEdgeStack was not initialized yet -> ignore
			if (localCatExchangeEdgeStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromCluster(localCatExchangeEdgeStack, catClockwise, *iter);
		}
	}
#endif

	/**
	 * update the sizes of the edge pieces which have to be exchanged with the adjacent clusters
	 *
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * ! this method is intended to update the edge communication information for adaptivity !
	 * ! AND                                                                                 !
	 * ! for the split/join information which is given as a parameter                        !
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 *
	 *
	 * stack elements have to be of type 'char'.
	 * 	-1: coarsening done
	 * 	 0: nothing
	 * 	 1: refinement done
	 */
public:
	void updateEdgeCommSizeAndSplitJoinInformation(
			CStack<char>* p_leftEdgeStack,
			CStack<char>* p_rightEdgeStack,
			CCluster_SplitJoin_EdgeComm_Information &p_adaptive_splitJoinInformation,
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	)
	{
		CStack<char> *catEdgeStack, *hypEdgeStack;

		bool even = (p_adaptive_splitJoinInformation.triangleFactory.evenOdd == CTriangle_Enums::EVEN);

		if (even)
		{
			catEdgeStack = p_leftEdgeStack;
			hypEdgeStack = p_rightEdgeStack;
		}
		else
		{
			catEdgeStack = p_rightEdgeStack;
			hypEdgeStack = p_leftEdgeStack;
		}

		CStackReaderTopDown<char> readerTopDown;

		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);

		/*******************************************************
		 * COPY-COPY-COPY
		 *******************************************************/

		// copy fixed variables
		p_splitJoinInformation.splitting_permitted = p_adaptive_splitJoinInformation.splitting_permitted;
		p_splitJoinInformation.joining_permitted = p_adaptive_splitJoinInformation.joining_permitted;

		// splitting and joining requests can be either triggered by the adaptive process (p_adaptive_splitJoinInformation) or by the user (p_splitJoinInformation)
		if (p_splitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::NO_OPERATION)
			p_splitJoinInformation.splitJoinRequests = p_adaptive_splitJoinInformation.splitJoinRequests;


		/*******************************************************
		 * HYPOTENUSE
		 *******************************************************/
		/*
		 * setup the adaptive edge comm updates for the hyp
		 */
		readerTopDown.setup(*hypEdgeStack);

		if (p_splitJoinInformation.splitting_permitted || p_splitJoinInformation.joining_permitted)
		{
			p_splitJoinInformation.number_of_shared_edge_comm_elements = p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

			// number of edge comm elements which have to be handled before switching to the second sub-triangle
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}


			if (cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0)
			{
#if DEBUG
				bool secondTriangleActive = false;
#endif
				/*
				 * iterate over the adjacency information about clusters next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
							cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
						iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentCluster_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_cluster_edge_comm_size = 0;

					/**
					 * variable to determine whether the previous edge comm element was
					 * one of a triangle which should be coarsened
					 */
					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent cluster
					 */
					for (int c = 0; c < i.comm_elements; c++)
					{
						if (edgeCommRemainingCounter == 0)
						{
#if DEBUG
							assert(secondTriangleActive == false);
							// switch to second triangle
#endif
							/*
							 * compute the remaining edge elements which have to be considered for this edge.
							 * for the second triangle, this is used as a variable to check whether all edge information
							 * is popped from the adjacency stacks.
							 */
							if (even)
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
							}
							else
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
							}

							assert(edgeCommRemainingCounter != 0);
#if DEBUG
							secondTriangleActive = true;
#endif
						}

						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// one edge element less which we have to consider
						edgeCommRemainingCounter--;

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_cluster_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_cluster_edge_comm_size += 2;
							// comm elements are increased by one edgeComm element due to refinement
							*splitJoinInformationElementsOnStackPtr += 1;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_cluster_edge_comm_size += 1;

						/*
						 * comm elements are reduced by one edgeComm element
						 */
						*splitJoinInformationElementsOnStackPtr -= 1;
					}

					/**
					 * setup new edge communication size for this edge
					 */
					i.comm_elements = new_cluster_edge_comm_size;

					assert(within_coarsening == false);
				}

				assert(edgeCommRemainingCounter == 0);
			}
		}
		else
		{
			readerTopDown.setup(*hypEdgeStack);

			/*
			 * no further splitting possible
			 */

			if (cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0)
			{
				/*
				 * iterate over the adjacency information about clusters next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
							cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
						iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentCluster_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_cluster_edge_comm_size = 0;

					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent cluster
					 */
					for (int c = 0; c < i.comm_elements; c++)
					{
						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_cluster_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_cluster_edge_comm_size += 2;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_cluster_edge_comm_size += 1;
					}

					assert(within_coarsening == false);

					/**
					 * setup new edge communication size for this edge
					 */
					i.comm_elements = new_cluster_edge_comm_size;
				}
			}
		}



		/*******************************************************
		 * CATHETUS
		 *******************************************************/
		if (p_splitJoinInformation.splitting_permitted)
		{
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}

			readerTopDown.setup(*catEdgeStack);
#if DEBUG
			bool secondTriangleActive = false;
#endif
			/*
			 * iterate over the adjacency information about clusters next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentCluster_ &i = *iter;

				// compute new cluster communication size
				unsigned int new_cluster_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.comm_elements; c++)
				{
					if (edgeCommRemainingCounter == 0)
					{
#if DEBUG
						assert(secondTriangleActive == false);
						// switch to second triangle
#endif
						/*
						 * compute the remaining edge elements which have to be considered for this edge.
						 * for the second triangle, this is used as a variable to check whether all edge information
						 * is popped from the adjacency stacks.
						 */
						if (even)
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
						}
						else
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
						}

						assert(edgeCommRemainingCounter != 0);
//						if (edgeCommRemainingCounter == 0)
//							break;
#if DEBUG
						secondTriangleActive = true;
#endif
					}

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// one edge element less which we have to consider
					edgeCommRemainingCounter--;

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_cluster_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_cluster_comm_size += 2;
						*splitJoinInformationElementsOnStackPtr += 1;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_cluster_comm_size += 1;

					*splitJoinInformationElementsOnStackPtr -= 1;
				}

				i.comm_elements = new_cluster_comm_size;

				assert(within_coarsening == false);
			}

			p_splitJoinInformation.first_triangle.number_of_elements = p_adaptive_splitJoinInformation.first_triangle.number_of_elements;
			p_splitJoinInformation.second_triangle.number_of_elements = p_adaptive_splitJoinInformation.second_triangle.number_of_elements;

			assert(edgeCommRemainingCounter == 0);
		}
		else
		{
			readerTopDown.setup(*catEdgeStack);

			/*
			 * iterate over the adjacency information about clusters next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentCluster_ &i = *iter;

				// compute new cluster communication size
				unsigned int new_cluster_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.comm_elements; c++)
				{

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_cluster_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_cluster_comm_size += 2;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_cluster_comm_size += 1;
				}

				i.comm_elements = new_cluster_comm_size;
			}
		}
	}
};

}

#endif
