/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulation_MainGuiParameters.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_MAINGUIPARAMETERS_HPP_
#define CSIMULATION_MAINGUIPARAMETERS_HPP_

#include "libgl/hud/CGlHudConfig.hpp"
#include "mainvis/CGuiConfig.hpp"

/**
 * this class describes the methods to setup gui parameters which have to be
 * provided by the simulation to the CMainGui class
 */
class CSimulation_MainGuiParameters
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;


public:
	virtual void setupCGlHudConfigSimulation(
			CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
		) = 0;


	virtual void setupCGlHudConfigVisualization(
			CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
		) = 0;


	virtual void setupCGuiCallbackHandlers(
			CGuiConfig &io_cGuiConfig
		) = 0;


	virtual ~CSimulation_MainGuiParameters()
	{

	}
};


#endif /* CSIMULATION_MAINPARAMETERS_HPP_ */
