/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_HPP_

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
	#include <pthread.h>
#endif

#include <iostream>
#include <stdexcept>

#include "config.h"
#include "lib/CStopwatch.hpp"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "../hyperbolic_common/traversators/CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_EdgeComm_Normals_Depth.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_Setup_Column.hpp"

#if CONFIG_SIERPI_ENABLE_GUI
#	include "libgl/draw/CGlDrawWireframeFromVertexArray.hpp"
#	include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalArray.hpp"
#endif

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
#	include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#	include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif


#include "../hyperbolic_common/CParameters.hpp"

#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_CellData.hpp"

#include "CSimulation_MainInterface.hpp"
#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulation_MainGuiInterface.hpp"
#endif
#include "CSimulation_MainInterface_FileOutput.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

#include "libsierpi/CGridDataArrays.hpp"


class CSimulationTsunami_Serial	:
	public CParameters,
	public CSimulation_MainInterface,
	public CSimulation_MainInterface_FileOutput
#if CONFIG_SIERPI_ENABLE_GUI
	,
	public CSimulation_MainGuiInterface
#endif
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/// traversator to setup the structure stack and element data stack by a given depth
	sierpi::travs::CSetup_Structure_CellData<CHyperbolicTypes> cSetup_Structure_CellData;

	/// to offer single-element modifications by coordinates, this traversal cares about it
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CHyperbolicTypes>::TRAV cModify_OneElementValue_SelectByPoint;

	/// output element data at given point
	sierpi::kernels::CStringOutput_CellData_Normal_SelectByPoint<CHyperbolicTypes>::TRAV cStringOutput_CellData_SelectByPoint;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	/// validation functions
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;
#endif

	/// exchange of edge communication data and timestep on fixed grid
	sierpi::travs::CSpecialized_EdgeComm_Normals_Depth cTsunami_EdgeComm;

	/// adaptive traversals to refine or coarsen grid cells without having hanging nodes
	sierpi::travs::CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData cTsunami_Adaptive;

#if CONFIG_SIERPI_ENABLE_GUI
	CGlDrawTrianglesWithVertexAndNormalArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesFromVertexAndNormalArray;
	CGlDrawWireframeFromVertexArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawWireframeFromVertexArray;
#endif

	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Setup_Column cSetup_Column;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	sierpi::kernels::CSetup_CellData_Validation::TRAV cSetup_CellData_Validation;
#endif

public:
	/**
	 * pointer to simulatino stacks
	 */
	CHyperbolicTypes::CSimulationStacks *cStacks;

	/**
	 * triangle factory for domain triangle
	 */
	CTriangle_Factory cTriangleFactory;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CDatasets cDatasets;

	/**
	 * handler to simulation parameters to use same code as for parallel version
	 */
	CParameters &cParameters;

public:
	/**
	 * constructor for serial tsunami simulation
	 */
	CSimulationTsunami_Serial(
			int i_verbosity_level
	)	:
		cStacks(nullptr),
		cDatasets(*this, i_verbosity_level),
		cParameters(*this)
	{
		verbosity_level = i_verbosity_level;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			testPinToCore(-2);
#endif

		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = grid_initial_recursion_depth + grid_max_relative_recursion_depth;

		reset();
	}


#if CONFIG_SIERPI_ENABLE_GUI

	/**
	 * render BATHYMETRY
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		switch(i_bathymetry_visualization_method % 2)
		{
			case 1:
				// do not render bathymetry
				return "blank";

			default:
			{

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesFromVertexAndNormalArray.initRendering();

				cGlDrawTrianglesFromVertexAndNormalArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesFromVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}
			return "simple";
		}
	}


	/**
	 * render water surface
	 */
	const char *render_DOFs(
			int i_surface_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);



		const char *ret_str = nullptr;
		T *v;
		T *n;
		T t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE1);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[1][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "x-momentum";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE2);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[2][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "y-momentum";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "bathymetry";
				break;


			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE0 + CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[0][i]+cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "surface height";
				break;
		}

		cCommonShaderPrograms.cHeightColorBlinn.use();
		cGlDrawTrianglesFromVertexAndNormalArray.initRendering();

		cGlDrawTrianglesFromVertexAndNormalArray.render(
				cGridDataArrays.triangle_vertex_buffer,
				cGridDataArrays.triangle_normal_buffer,
				cParameters.number_of_local_cells*3
			);

		cGlDrawTrianglesFromVertexAndNormalArray.shutdownRendering();
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;
	}



	void p_render_Wireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

		storeDOFsToGridDataArrays(&cGridDataArrays, 0);

		T *v, *l;
		T *lines = new T[cParameters.number_of_local_cells*2*3*3];

		v = cGridDataArrays.triangle_vertex_buffer;
		l = lines;

		for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
		{
			for (int vn = 0; vn < 2; vn++)
			{
				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				l[0] = v[3+0];
				l[1] = v[3+2];
				l[2] = -v[3+1];
				l += 3;

				v += 3;
			}

			l[0] = v[0];
			l[1] = v[2];
			l[2] = -v[1];
			l += 3;

			v -= 6;

			l[0] = v[0];
			l[1] = v[2];
			l[2] = -v[1];
			l += 3;

			v += 9;
		}

		cShaderBlinn.use();
		cGlDrawWireframeFromVertexArray.initRendering();

		cGlDrawWireframeFromVertexArray.render(
				lines,
				cParameters.number_of_local_cells*3*2
			);

		cGlDrawWireframeFromVertexArray.shutdownRendering();
		cShaderBlinn.disable();

		delete [] lines;
	}

	void render_Wireframe(
			int i_visualization_render_wireframe,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
			p_render_Wireframe(cCommonShaderPrograms.cBlinn);
	}


	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}

	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}



	void p_applyTranslateAndScaleToVertex(CHyperbolicTypes::CVisualizationTypes::T *io_vertex)
	{
		io_vertex[0] = (io_vertex[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
		io_vertex[1] = (io_vertex[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;
		io_vertex[2] = (io_vertex[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_z;
	}


	void storeDOFsToGridDataArrays(
			CGridDataArrays<3,6> *io_cGridDataArrays,
			int i_flags
	)
	{
		i_flags |=
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::NORMALS;


		size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(number_of_local_cells);

		// We instantiate it right here to avoid any overhead due to split/join operations
		sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				io_cGridDataArrays,
				&cDatasets,
				offset,
				i_flags,
				0
			);

		cOutputGridDataArrays.action(cStacks);


		assert(io_cGridDataArrays->number_of_triangle_cells == cParameters.number_of_local_cells);

		/*
		 * postprocessing
		 */
		if (	cParameters.visualization_scale_x != 1.0 ||
				cParameters.visualization_scale_y != 1.0 ||
				cParameters.visualization_scale_z != 1.0 ||

				cParameters.visualization_translate_x != 1.0 ||
				cParameters.visualization_translate_y != 1.0 ||
				cParameters.visualization_translate_z != 1.0
		)
		{
			for (int i = 0; i < cParameters.number_of_local_cells; i++)
			{
				CHyperbolicTypes::CVisualizationTypes::T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

				for (int vn = 0; vn < 3; vn++)
				{
					p_applyTranslateAndScaleToVertex(v);
					v += 3;
				}

				io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;
				io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;
			}
		}
	}

#endif


	/**
	 * setup the stacks
	 */
	void setup_Stacks()
	{
		clean();

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		cStacks = new CHyperbolicTypes::CSimulationStacks(
					(1 << grid_initial_recursion_depth) + (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CTsunamiSimulationCellData)),
					sierpi::CSimulationStacks_Enums::ELEMENT_STACKS			|
					sierpi::CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					sierpi::CSimulationStacks_Enums::EDGE_COMM_STACKS		|
					sierpi::CSimulationStacks_Enums::VERTEX_COMM_STACKS
				);
#else
		cStacks = new CHyperbolicTypes::CSimulationStacks(
				grid_initial_recursion_depth+grid_max_relative_recursion_depth,
					sierpi::CSimulationStacks_Enums::ELEMENT_STACKS			|
					sierpi::CSimulationStacks_Enums::ADAPTIVE_STACKS		|
					sierpi::CSimulationStacks_Enums::EDGE_COMM_STACKS
				);
#endif

	}


	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 0.5;
		cTriangleFactory.vertices[0][1] = -0.5;
		cTriangleFactory.vertices[1][0] = -0.5;
		cTriangleFactory.vertices[1][1] = 0.5;
		cTriangleFactory.vertices[2][0] = -0.5;
		cTriangleFactory.vertices[2][1] = -0.5;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		for (int i = 0; i < 3; i++)
		{
			cTriangleFactory.vertices[i][0] = (cTriangleFactory.vertices[i][0]*simulation_dataset_default_domain_size_x) + simulation_dataset_default_domain_translate_x;
			cTriangleFactory.vertices[i][1] = (cTriangleFactory.vertices[i][1]*simulation_dataset_default_domain_size_y) + simulation_dataset_default_domain_translate_y;
		}

		cTsunami_Adaptive.setup_sfcMethods(cTriangleFactory);
		cTsunami_EdgeComm.setup_sfcMethods(cTriangleFactory);
		cSetup_Column.setup_sfcMethods(cTriangleFactory);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		cSetup_CellData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

		cStringOutput_CellData_SelectByPoint.setup_sfcMethods(cTriangleFactory);
		cModify_OneElementValue_SelectByPoint.setup_sfcMethods(cTriangleFactory);
	}

	void setup()
	{
		reset();
	}

	void reset()
	{
		sierpi::travs::CSpecialized_EdgeComm_Normals_Depth::setupMatrices(verbosity_level);

		cDatasets.loadDatasets();

		setup_Stacks();
		setup_TraversatorsAndKernels();
		p_setup_Parameters();
		p_setup_InitialTriangulation();

		reset_simulation_parameters();
	}


	void clean()
	{
		if (cStacks)
		{
			delete cStacks;
			cStacks = NULL;
		}
	}

	void setup_RadialDamBreak(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius
	)
	{
		number_of_local_cells = p_adaptive_traversal_setup_column(x, y, radius);
	}


	long long p_adaptive_traversal_setup_column(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius
	)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0

		simulation_dataset_breaking_dam_posx = x;
		simulation_dataset_breaking_dam_posy = y;
		simulation_dataset_breaking_dam_radius = radius;

		cSetup_Column.setup_KernelClass(
				simulation_dataset_breaking_dam_posx,
				simulation_dataset_breaking_dam_posy,
				simulation_dataset_breaking_dam_radius,
				2,
				&cDatasets
		);

#else

		cSetup_Column.setup_KernelClass(
				simulation_dataset_breaking_dam_posx,
				simulation_dataset_breaking_dam_posy,
				simulation_dataset_breaking_dam_radius,
				2,
				&cDatasets
		);

#endif

		long long prev_number_of_local_cells;
		do
		{
			prev_number_of_local_cells = number_of_local_cells;

			bool repeat_traversal = cSetup_Column.actionFirstTraversal(cStacks);

			while(repeat_traversal)
				repeat_traversal = cSetup_Column.actionMiddleTraversals_Serial(cStacks);

			cSetup_Column.actionLastTraversal_Serial(cStacks);

			number_of_local_cells = cStacks->element_data_stacks.getNumberOfElementsOnStack();
		} while (number_of_local_cells != prev_number_of_local_cells);

		number_of_local_cells = prev_number_of_local_cells;

		int backupSetupSurfaceMethod = simulation_dataset_1_id;
		simulation_dataset_1_id = CDatasets::SIMULATION_INTERACTIVE_UPDATE;

		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
		cSetup_CellData.setup_sfcMethods(cTriangleFactory);
		cSetup_CellData.cKernelClass.setup_Parameters(&cDatasets, false);
		cSetup_CellData.action(cStacks);

		simulation_dataset_1_id = backupSetupSurfaceMethod;

		return number_of_local_cells;
	}

private:
	void p_setup_Parameters()
	{
		cTsunami_EdgeComm.setParameters(
				simulation_parameter_global_timestep_size,
				simulation_dataset_default_domain_size_x,
				simulation_parameter_gravitation,
				&cDatasets
			);
		cTsunami_EdgeComm.setBoundaryCondition((EBoundaryConditions)simulation_domain_boundary_condition);
		cTsunami_EdgeComm.setBoundaryDirichlet(&simulation_domain_boundary_dirichlet_edge_data);

		cTsunami_Adaptive.setup_KernelClass(
				simulation_dataset_default_domain_size_x,

				adaptive_refine_parameter_0,
				adaptive_coarsen_parameter_0,

				adaptive_refine_parameter_1,
				adaptive_coarsen_parameter_1,

				&cDatasets
			);

		cTsunami_Adaptive.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);

		cSetup_Column.setup_RootTraversator(grid_initial_recursion_depth+grid_min_relative_recursion_depth, grid_initial_recursion_depth+grid_max_relative_recursion_depth);
	}


private:
	void p_setup_InitialTriangulation()
	{
		number_of_local_cells = cSetup_Structure_CellData.setup(cStacks, grid_initial_recursion_depth, simulation_parameter_cell_data_setup);
		number_of_global_cells = number_of_local_cells;

		number_of_local_initial_cells_after_domain_triangulation = number_of_local_cells;
		number_of_global_initial_cells_after_domain_triangulation = number_of_local_initial_cells_after_domain_triangulation;

		p_setup_initial_cell_data(true);
	}


private:
	void p_setup_initial_cell_data(
			bool i_initial_grid_setup
	)
	{
		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
		cSetup_CellData.setup_sfcMethods(cTriangleFactory);
		cSetup_CellData.cKernelClass.setup_Parameters(&cDatasets, i_initial_grid_setup);
		cSetup_CellData.action(cStacks);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		cSetup_CellData_Validation.action(cStacks);
#endif
	}


private:
	void p_simulation_adaptive_traversals()
	{
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		number_of_local_cells = cStacks->element_data_stacks.getNumberOfElementsOnStack();
	}

public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
		long long prev_number_of_local_cells;
		long long prev_number_of_local_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		T coarsen_threshold_backup = adaptive_coarsen_parameter_0;
		adaptive_coarsen_parameter_0 = -9999999;

//		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 10;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_local_cells = number_of_local_cells;
			prev_number_of_local_clusters = number_of_local_clusters;

			// setup grid data
			p_setup_initial_cell_data(true);

			// run single timestep
			p_simulation_edge_comm();

			// refine / coarsen grid
			p_simulation_adaptive_traversals();

			// split/join clusters
//			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_local_cells << ", number_of_local_clusters: " << number_of_local_clusters << std::endl;

			if (	prev_number_of_local_cells == number_of_local_cells	&&
					prev_number_of_local_clusters == number_of_local_clusters
			)
				break;
		}

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		p_setup_initial_cell_data(true);

		// update cluster parameters
		adaptive_coarsen_parameter_0 = coarsen_threshold_backup;
//		updateClusterParameters();
	}

private:
	/**
	 * timestep edge comm
	 */
	void p_simulation_edge_comm()
	{
		cTsunami_EdgeComm.actionFirstPass(cStacks);

		// adaptive timestep size
		T cfl1_value;
		cTsunami_EdgeComm.actionMiddlePass_computeClusterBorderCFL(cStacks, &cfl1_value);

		simulation_parameter_global_timestep_size = cfl1_value * simulation_parameter_cfl;

		cTsunami_EdgeComm.actionSecondPass_Serial(cStacks, simulation_parameter_global_timestep_size);
	}


#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
public:
	inline void testPinToCore(int pinning_core)
	{
		if (pinning_core == -1)
			return;


		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);

		// pin to first core for initialization to assure placement of allocated data
		if (pinning_core == -2)
			CPU_SET(0, &cpu_set);
		else
			CPU_SET(pinning_core, &cpu_set);

		std::cout << "Pinning application to core " << pinning_core << std::endl;

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		std::cout << std::endl;
		std::cout << "WARNING: Adaptive cluster stacks activated!!!" << std::endl;
		std::cout << std::endl;
#endif

		int err;
		err = pthread_setaffinity_np(
				pthread_self(),
				sizeof(cpu_set_t),
				&cpu_set
			);


		if (err != 0)
		{
			std::cout << err << std::endl;
			perror("pthread_setaffinity_np");
			assert(false);
			exit(-1);
		}

		if (pinning_core >= 0)
			threading_pin_to_core_nr_after_one_timestep = -1;
	}
#endif

public:
	void runSingleTimestep()
	{
		p_simulation_edge_comm();
		p_simulation_adaptive_traversals();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/
	double getDataSample(
			double i_sample_pos_x,
			double i_sample_pos_y,
			const char *i_sample_information
		)
	{
		return 0;
	}

public:
	/*
	void debugOutput(CVector<2,float> planePosition)
	{
		cStringOutput_CellData_SelectByPoint.cKernelClass.setup(planePosition[0], planePosition[1]);
		cStringOutput_CellData_SelectByPoint.action(cStacks);
	}
	*/


	void writeSimulationDataToFile(
			const char *i_filename,
			const char *i_information_string = nullptr
	)
	{
		CGridDataArrays<3,6> cGridDataArrays(number_of_local_cells);

		sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

		int flags =
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::VALUE0	|
				CGridDataArrays_Enums::VALUE1	|
				CGridDataArrays_Enums::VALUE2	|
				CGridDataArrays_Enums::VALUE3	|
				CGridDataArrays_Enums::VALUE4;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				&cGridDataArrays,
				&cDatasets,
				0,
				flags,
				0
			);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

		cVtkXMLTrianglePolyData.setup();
		cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
		cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.dof_element[0]);
		cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.dof_element[1]);
		cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.dof_element[2]);
		cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.dof_element[3]);
		cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.dof_element[4]);

		cVtkXMLTrianglePolyData.write(i_filename);
	}


	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		std::cout << "writeSimulationClustersDataToFile() not implemented (meaningless for serial version)" << std::endl;
	}

#if CONFIG_SIERPI_ENABLE_GUI

	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	bool gui_key_down_event(
			int key
	)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_RadialDamBreak(simulation_dataset_breaking_dam_posx, simulation_dataset_breaking_dam_posy, simulation_dataset_breaking_dam_radius);
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			default:
				return false;
		}

		return true;
	}


	bool gui_mouse_motion_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	bool gui_mouse_button_down_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}
#endif


	void outputVerboseInformation()
	{
		((CParameters&)(*this)).outputVerboseInformation();
	}

	void output_simulationSpecificData()
	{
		std::cout << "output_simulationSpecificData() not implemented" << std::endl;
	}

	void debug_OutputCellData(
				T i_coord_x,		///< x-coordinate of triangle cell
				T i_coord_y		///< y-coordinate of triangle cell
		)
	{
		/// output element data at given point
		sierpi::kernels::CStringOutput_CellData_Normal_SelectByPoint<CHyperbolicTypes>::TRAV cStringOutput_CellData_SelectByPoint;

		cStringOutput_CellData_SelectByPoint.setup_sfcMethods(cTriangleFactory);
		cStringOutput_CellData_SelectByPoint.cKernelClass.setup(i_coord_x, i_coord_y);
		cStringOutput_CellData_SelectByPoint.action(cStacks);
	}

	void debug_OutputClusterInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputClusterInformation() not implemented" << std::endl;
	}

	void debug_OutputEdgeCommunicationInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputEdgeCommunicationInformation() not implemented" << std::endl;
	}

	void output_ClusterTreeInformation()
	{
	}

	void action_Validation()
	{

	}

	virtual ~CSimulationTsunami_Serial()
	{
		clean();
	}

	void updateScanDatasets(
			int use_number_of_threads = -1
	)
	{
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T	*o_translate_x,	///< origin of domain in world-space
			T	*o_translate_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	)
	{
		*o_translate_x = cParameters.simulation_dataset_default_domain_translate_x;
		*o_translate_y = cParameters.simulation_dataset_default_domain_translate_y;

		*o_size_x = cParameters.simulation_dataset_default_domain_size_x;
		*o_size_y = cParameters.simulation_dataset_default_domain_size_y;
	}

};

#endif /* CTSUNAMI_HPP_ */
