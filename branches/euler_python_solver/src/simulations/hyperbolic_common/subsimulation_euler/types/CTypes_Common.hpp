/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 27, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CEULERTYPES_COMMON_HPP_
#define CEULERTYPES_COMMON_HPP_

#include <iostream>
#include <vector>
#include <limits>

// load typedef for CONFIG_DEFAULT_FLOATING_POINT_TYPE
#include "../CConfig.hpp"

#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"



/**
 * Degree of freedoms for one point in tsunami simulation
 */
class CEulerSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T r;	// density
	T ru;	// density momentum in x-direction
	T rv;	// density momentum in y-direction
	T e;	// Energy

	T cfl1_timestep_size;	// max wave speed for flux computations

	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(4);
		(*o_data)[0] = r;
		(*o_data)[1] = ru;
		(*o_data)[2] = rv;
		(*o_data)[3] = e;
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T default_values[4])
	{
		r = default_values[0];
		ru = default_values[1];
		rv = default_values[2];
		e = default_values[3];
	}


	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			const CEulerSimulationNodeData &d
	)	{
		os << "CEulerSimulationNodeData:" << std::endl;
		os << " + r: " << d.r << std::endl;
		os << " + ru: " << d.ru << std::endl;
		os << " + rv: " << d.rv << std::endl;
		os << " + e: " << d.e << std::endl;
		os << " + cfl: " << d.cfl1_timestep_size << std::endl;
		os << " + vx: " << d.ru/d.r << std::endl;
		os << " + vy: " << d.rv/d.r << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,	///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		os << "Euler DOF Information:" << std::endl;
		os << " + rho: " << r << std::endl;

		T tru = ru;
		T trv = rv;
		CTriangle_VectorProjections::referenceToWorld(&tru, &trv, x_axis_x, x_axis_y);

		os << " + ru: " << tru << std::endl;
		os << " + rv: " << trv << std::endl;

		os << " + e: " << e << std::endl;

		os << " + cfl: " << cfl1_timestep_size << std::endl;

		os << " + vx: " << tru/r << std::endl;
		os << " + vy: " << trv/r << std::endl;

		os << std::endl;
	}
};



#endif /* CTSUNAMITYPES_COMMON_HPP_ */
