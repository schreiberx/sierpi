/*
 * CEulerTypes.hpp
 *
 *  Created on: Aug 27, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMITYPES_HPP_
#define CTSUNAMITYPES_HPP_

#include "libsierpi/stacks/CSimulationStacks.hpp"


#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
#	include "CTypes_0thOrder.hpp"
#else
#	error "only 0-th order supported"
#endif


#include "simulations/hyperbolic_common/subsimulation_generic/types/CVisualizationVertexData.hpp"





/**
 * simulation types which
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CEulerSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CEulerSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CEulerSimulationEdgeData CEdgeData;
	};



	/**
	 * This class implements all types which are used for visualization
	 */
	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CEulerSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};


	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;
};




#endif /* CTSUNAMITYPES_HPP_ */
