/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 26, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHYPERBOLIC_CONFIG_EULER_HPP_
#define CHYPERBOLIC_CONFIG_EULER_HPP_


#define SIMULATION_DATASET_DEFAULT_VALUE_0	(1)
#define SIMULATION_DATASET_DEFAULT_VALUE_1	(0)
#define SIMULATION_DATASET_DEFAULT_VALUE_2	(0)
#define SIMULATION_DATASET_DEFAULT_VALUE_3	(1)


#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_X		(0)
#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Y		(0)
#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Z		(-2)


#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1

#	define SIMULATION_REFINE_PARAMETER_0	1.1
#	define SIMULATION_REFINE_PARAMETER_1	1.1

#	define SIMULATION_COARSEN_PARAMETER_0	1.01
#	define SIMULATION_COARSEN_PARAMETER_1	1.01

#endif


#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2

#	define SIMULATION_REFINE_PARAMETER_0	1.1
#	define SIMULATION_REFINE_PARAMETER_1	1.1

#	define SIMULATION_COARSEN_PARAMETER_0	1.01
#	define SIMULATION_COARSEN_PARAMETER_1	1.01

#endif


#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==1
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_STRING	"local lax friedrich"
#endif

#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==2
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_STRING	"local euler python geoclaw"
#endif


#endif /* CCONFIGTSUNAMI_HPP_ */
