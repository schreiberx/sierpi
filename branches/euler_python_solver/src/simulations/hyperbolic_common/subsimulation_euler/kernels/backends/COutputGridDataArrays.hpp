/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice aT rttp://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_
#define CSIMULATION_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "../../../subsimulation_generic/types/CTypes.hpp"

#include "libsierpi/CGridDataArrays.hpp"

namespace sierpi {
namespace kernels {

template <
	int t_vertices_per_cell		///< vertices to be stored per cell (3 for 2D, 2 for 1D)
>
class COutputGridDataArrays {
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<
			COutputGridDataArrays,
			CHyperbolicTypes
		> TRAV;

	CMainThreading_Lock lock;

private:
	CGridDataArrays<t_vertices_per_cell,6> *cGridDataArrays;

	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing;

public:
	inline void op_cell(
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,

			T i_normal_hyp_x, T i_normal_hyp_y,
			T i_normal_right_x, T i_normal_right_y,
			T i_normal_left_x, T i_normal_left_y,

			CCellData *i_cCellData
	) {
		T *v;

		if (output_flags & CGridDataArrays_Enums::VERTICES)
		{
			v = &(cGridDataArrays->triangle_vertex_buffer[cell_write_index * 3 * 3]);

#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
			v[0 * 3 + 0] = i_vertex_coord_left_x;
			v[0 * 3 + 1] = i_vertex_coord_left_y;
			v[0 * 3 + 2] = 0;
			v[1 * 3 + 0] = i_vertex_coord_right_x;
			v[1 * 3 + 1] = i_vertex_coord_right_y;
			v[1 * 3 + 2] = 0;
			v[2 * 3 + 0] = i_vertex_coord_top_x;
			v[2 * 3 + 1] = i_vertex_coord_top_y;
			v[2 * 3 + 2] = 0;
#endif

#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==1
			if (preprocessing == 0)
			{
				v[0 * 3 + 0] = i_vertex_coord_left_x;
				v[0 * 3 + 1] = i_vertex_coord_left_y;
				v[0 * 3 + 2] = 0;
				v[1 * 3 + 0] = i_vertex_coord_right_x;
				v[1 * 3 + 1] = i_vertex_coord_right_y;
				v[1 * 3 + 2] = 0;
				v[2 * 3 + 0] = i_vertex_coord_top_x;
				v[2 * 3 + 1] = i_vertex_coord_top_y;
				v[2 * 3 + 2] = 0;
			}
			else
			{
				T ryp_edge_x = (i_vertex_coord_left_x+i_vertex_coord_right_x)*(T)0.5;
				T ryp_edge_y = (i_vertex_coord_left_y+i_vertex_coord_right_y)*(T)0.5;

				T right_edge_x = (i_vertex_coord_right_x+i_vertex_coord_top_x)*(T)0.5;
				T right_edge_y = (i_vertex_coord_right_y+i_vertex_coord_top_y)*(T)0.5;

				T left_edge_x = (i_vertex_coord_top_x+i_vertex_coord_left_x)*(T)0.5;
				T left_edge_y = (i_vertex_coord_top_y+i_vertex_coord_left_y)*(T)0.5;

				T ryp_dx = right_edge_x - left_edge_x;
				T ryp_dy = right_edge_y - left_edge_y;

				T ryp_dhb = i_cCellData->dofs_right_edge.r+i_cCellData->dofs_right_edge.b - (i_cCellData->dofs_left_edge.r+i_cCellData->dofs_left_edge.b);

				T left_dx = right_edge_x - hyp_edge_x;
				T left_dy = right_edge_y - hyp_edge_y;
				T left_dhb = i_cCellData->dofs_right_edge.r+i_cCellData->dofs_right_edge.b - (i_cCellData->dofs_hyp_edge.r+i_cCellData->dofs_hyp_edge.b);


				v[0*3+0] = hyp_edge_x-hyp_dx;
				v[0*3+1] = (hyp_edge_y-hyp_dy);
				v[0*3+2] = (i_cCellData->dofs_hyp_edge.r+i_cCellData->dofs_hyp_edge.b) - hyp_dhb;

				v[1*3+0] = hyp_edge_x+hyp_dx;
				v[1*3+1] = (hyp_edge_y+hyp_dy);
				v[1*3+2] = (i_cCellData->dofs_hyp_edge.r+i_cCellData->dofs_hyp_edge.b) + hyp_dhb;

				v[2*3+0] = left_edge_x+left_dx;
				v[2*3+1] = (left_edge_y+left_dy);
				v[2*3+2] = (i_cCellData->dofs_left_edge.r+i_cCellData->dofs_left_edge.b)+left_dhb;
			}
#endif
		}

		if (output_flags & CGridDataArrays_Enums::NORMALS)
		{
			T *n = &(cGridDataArrays->triangle_normal_buffer[cell_write_index * 3 * 3]);

#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
			n[0*3+0] = 0;
			n[0*3+1] = 0;
			n[0*3+2] = 1;
			n[1*3+0] = 0;
			n[1*3+1] = 0;
			n[1*3+2] = 1;
			n[2*3+0] = 0;
			n[2*3+1] = 0;
			n[2*3+2] = 1;
#endif


#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1
			if (preprocessing == 0)
			{
				n[0 * 3 + 0] = 0;
				n[0 * 3 + 1] = 0;
				n[0 * 3 + 2] = 1;
				n[1 * 3 + 0] = 0;
				n[1 * 3 + 1] = 0;
				n[1 * 3 + 2] = 1;
				n[2 * 3 + 0] = 0;
				n[2 * 3 + 1] = 0;
				n[2 * 3 + 2] = 1;
			}
			else
			{
				assert(output_flags & CGridDataArrays_Enums::VERTICES);

				T bx = (v[0*3+0]-v[1*3+0]);
				T by = (v[0*3+1]-v[1*3+1]);
				T bz = (v[0*3+2]-v[1*3+2]);

				T cx = (v[0*3+0]-v[2*3+0]);
				T cy = (v[0*3+1]-v[2*3+1]);
				T cz = (v[0*3+2]-v[2*3+2]);

				T nx = by*cz-bz*cy;
				T ny = bz*cx-bx*cz;
				T nz = bx*cy-by*cx;

				T a = (T)1.0/std::sqrt<T>(nx*nx + ny*ny + nz*nz);

				nx *= a;
				ny *= a;
				nz *= a;

				n[0*3+0] = nx;
				n[0*3+1] = ny;
				n[0*3+2] = nz;

				n[1*3+0] = nx;
				n[1*3+1] = ny;
				n[1*3+2] = nz;

				n[2*3+0] = nx;
				n[2*3+1] = ny;
				n[2*3+2] = nz;
			}
#endif
		}

		/*
		 * H
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
			T r = i_cCellData->dofs[0].r;
#else
			T r = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.r + i_cCellData->dofs_left_edge.r + i_cCellData->dofs_right_edge.r);
#endif

			cGridDataArrays->dof_element[0][cell_write_index] = r;
		}

		/*
		 * MOMENTUM HU
		 * MOMENTUM HV
		 */
		if (output_flags & (CGridDataArrays_Enums::VALUE1 | CGridDataArrays_Enums::VALUE2))
		{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
			T ru = i_cCellData->dofs[0].ru;
			T rv = i_cCellData->dofs[0].rv;
#else
			T ru = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.ru + i_cCellData->dofs_left_edge.ru + i_cCellData->dofs_right_edge.ru);
			T rv = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.rv + i_cCellData->dofs_left_edge.rv + i_cCellData->dofs_right_edge.rv);
#endif

			CTriangle_VectorProjections::referenceToWorld(&ru, &rv, -i_normal_right_x, -i_normal_right_y);

			if (output_flags & CGridDataArrays_Enums::VALUE1)
				cGridDataArrays->dof_element[1][cell_write_index] = ru;

			if (output_flags & CGridDataArrays_Enums::VALUE2)
				cGridDataArrays->dof_element[2][cell_write_index] = rv;
		}

		/*
		 * BATHYMETRY
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE3)
		{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
			T e = i_cCellData->dofs[0].e;
#else
			T e = (1.0/3.0)*(i_cCellData->dofs_hyp_edge.e + i_cCellData->dofs_left_edge.e + i_cCellData->dofs_right_edge.e);
#endif

			cGridDataArrays->dof_element[3][cell_write_index] = e;
		}


		/*
		 * cfl_domain_size_div_max_wave_speed
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE4)
		{
			T cfl_domain_size_div_max_wave_speed;

			if (i_cCellData->cfl_domain_size_div_max_wave_speed > 100000000000.0)
				cfl_domain_size_div_max_wave_speed = 0;
			else
				cfl_domain_size_div_max_wave_speed = i_cCellData->cfl_domain_size_div_max_wave_speed;

			cGridDataArrays->dof_element[4][cell_write_index] = cfl_domain_size_div_max_wave_speed;
		}

		cell_write_index++;
	}

	inline void traversal_pre_hook() {
	}

	inline void traversal_post_hook() {
	}

	inline void setup(
			CGridDataArrays<t_vertices_per_cell,6> *o_GridDataArrays,
			CDatasets *i_cDatasets,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing = 0
	) {
		cGridDataArrays = o_GridDataArrays;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
		preprocessing = i_preprocessing;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputGridDataArrays &parent_kernel) {
		assert(false);
	}
};

#undef VISUALIZATION_SIMPLE
#undef VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS
#undef VISUALIZATION_ALIGNED_TRIANGLES

}
}

#endif
