/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_LAXFRIEDRICH_HPP_
#define CFLUXSOLVER_LAXFRIEDRICH_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

template <typename T>
class CFluxSolver_LaxFriedrich
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	void op_edge_edge(
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_left,		///< output for left flux
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T left_u = i_edgeData_left.ru / i_edgeData_left.r;
		T right_u = i_edgeData_right.ru / i_edgeData_right.r;

		T left_v = i_edgeData_left.rv / i_edgeData_left.r;
		T right_v = i_edgeData_right.rv / i_edgeData_right.r;

		T left_p = i_edgeData_left.r*0.01;
		T right_p = i_edgeData_right.r*0.01;

		T left_lambda = std::abs(left_u)+std::sqrt(left_p);
		T right_lambda = std::abs(right_u)+std::sqrt(right_p);

		T lambda = std::max(left_lambda, right_lambda);

		o_edgeFlux_left->r = (T)0.5*(i_edgeData_left.ru + i_edgeData_right.ru);

		o_edgeFlux_left->ru =
				(T)0.5*
				(
					i_edgeData_left.ru*left_u + left_p +
					i_edgeData_right.ru*right_u + right_p
				);

		o_edgeFlux_left->rv =
				(T)0.5*
				(
					i_edgeData_left.ru*left_v +
					i_edgeData_right.ru*right_v
				);

		o_edgeFlux_left->e =
				(T)0.5*
				(
					(i_edgeData_left.e + left_p)*left_u	+
					(i_edgeData_right.e + right_p)*right_u
				);


		o_edgeFlux_right->r = -o_edgeFlux_left->r;
		o_edgeFlux_right->ru = -o_edgeFlux_left->ru;
		o_edgeFlux_right->rv = -o_edgeFlux_left->rv;
		o_edgeFlux_right->e = -o_edgeFlux_left->e;

#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==0
		// remove fluxes for 0th order basis functions

		o_edgeFlux_left->r -= i_edgeData_left.ru;
		o_edgeFlux_left->ru -= i_edgeData_left.ru*left_u + left_p;
		o_edgeFlux_left->rv -= i_edgeData_left.ru*left_v;
		o_edgeFlux_left->e -= (i_edgeData_left.e + left_p)*left_u;

		o_edgeFlux_right->r += i_edgeData_right.ru;
		o_edgeFlux_right->ru += i_edgeData_right.ru*right_u + right_p;
		o_edgeFlux_right->rv += i_edgeData_right.ru*right_v;
		o_edgeFlux_right->e += (i_edgeData_right.e + right_p)*right_u;
#endif

		/**
		 * add numerical diffusion for stability reason
		 */

		T diffusion_r = (T)0.5*(T)lambda*(i_edgeData_left.r - i_edgeData_right.r);
		T diffusion_ru = (T)0.5*(T)lambda*(i_edgeData_left.ru - i_edgeData_right.ru);
		T diffusion_rv = (T)0.5*(T)lambda*(i_edgeData_left.rv - i_edgeData_right.rv);
		T diffusion_e = (T)0.5*(T)lambda*(i_edgeData_left.e - i_edgeData_right.e);

		o_edgeFlux_right->r -= diffusion_r;
		o_edgeFlux_right->ru -= diffusion_ru;
		o_edgeFlux_right->rv -= diffusion_rv;
		o_edgeFlux_right->e -= diffusion_e;

		o_edgeFlux_left->r += diffusion_r;
		o_edgeFlux_left->ru += diffusion_ru;
		o_edgeFlux_left->rv += diffusion_rv;
		o_edgeFlux_left->e += diffusion_e;

		/**
		 * CFL condition
		 */
		*o_max_wave_speed_left = lambda;
		*o_max_wave_speed_right = lambda;
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
