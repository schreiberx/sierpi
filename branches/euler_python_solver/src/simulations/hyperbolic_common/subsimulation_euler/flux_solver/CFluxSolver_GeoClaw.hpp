/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_GEOCLAW_HPP_
#define CFLUXSOLVER_GEOCLAW_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "Python.h"
#include "numpy/core/include/numpy/arrayobject.h"
#include <iostream>

// !!!! IMPORTANT !!!! Don't forget to set the python path
template <typename T>
class CFluxSolver_GeoClaw
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	int op_edge_edge(
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CHyperbolicTypes::CSimulationTypes::CNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_left,		///< output for left flux
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
	    PyObject *pName, *pModule, *pDict, *pFunc, *pReturn, *pArgs;

	    
	    // Initialize the Python Interpreter
	    Py_Initialize();    

	    // !!!!!!! If this is forgotten Py_SimpleNewFromData will segfault
	    // Also, if the function is void, this line will create an error
	    // claiming that I'm trying to return from a void function
	    import_array1(-1);

	    // Build the name object
	    pName = PyString_FromString("rp_euler");

	    // Load the module object
	    pModule = PyImport_Import(pName);

	    // pDict is a borrowed reference 
	    pDict = PyModule_GetDict(pModule);

	    // pFunc is also a borrowed reference 
	    pFunc = PyDict_GetItemString(pDict, "rp_euler_roe_1d");
	   
		

	    if (PyCallable_Check(pFunc)) {
	    // Prepare the argument list for the call
		  
		PyObject *vars, *gamma, *gamma1, *efix, *mat, *ql_python, *qr_python, *ql_aux_python, *qr_aux_python;
		PyArrayObject *wave_python, *wave_speed_python, *flux_left_python, *flux_right_python;
		
		// parameters for the euler solver
		gamma = PyFloat_FromDouble(0.3);
		gamma1 = PyFloat_FromDouble(0.7);
		efix = PyBool_FromLong(0);
		vars = PyDict_New();
		PyDict_SetItemString( vars, "gamma", gamma);
		PyDict_SetItemString( vars, "gamma1", gamma1);
		PyDict_SetItemString( vars, "efix", efix);
		
		// pressure
		T p_left = i_edgeData_left.r * 0.01;
		T p_right = i_edgeData_right.r * 0.01; 
		
		// create matrix for ql
		int qdim[2] = {1, 3};
		T ** ql = new T*[3];
		ql[0] = new T[3];
		ql[0][0] = i_edgeData_left.r;
		ql[0][1] = i_edgeData_left.ru;
		ql[0][2] = i_edgeData_left.e;
		std::cout<<"ql: "<<ql[0][0]<<" "<<ql[0][1]<<" "<<ql[0][2]<<std::endl;
		ql_python = PyArray_SimpleNewFromData( 2, qdim, NPY_FLOAT, ql[0] );
		
		
		// create matrix for qr
		
		// the matrix has to be allocated like this (one large array), otherwise it won't be 
		// turned into the numpy array properly
		T ** qr = new T*[3];
		qr[0] = new T[3];
		qr[0][0] = i_edgeData_right.r;
		qr[0][1] = i_edgeData_right.ru;
		qr[0][2] = i_edgeData_right.e;
		
		qr_python = PyArray_SimpleNewFromData( 2, qdim, NPY_FLOAT, qr[0] );
		
		
		
		// auxiliary array for ql, not sure what goes here
		T ** ql_aux = new T*[3];
		ql_aux[0] = new T[3];
		ql_aux[0][0] = i_edgeData_left.ru;
		ql_aux[0][1] = i_edgeData_left.ru * i_edgeData_left.ru / i_edgeData_left.r + p_left;
		ql_aux[0][2] = i_edgeData_left.ru * (i_edgeData_left.e + p_left) / i_edgeData_left.r;
		
		ql_aux_python = PyArray_SimpleNewFromData( 2, qdim, NPY_FLOAT, ql_aux[0] );
		
		
		// auxiliary array for qr, not sure what goes here
		T ** qr_aux = new T*[3];
		qr_aux[0] = new T[3];
		qr_aux[0][0] = i_edgeData_right.ru;
		qr_aux[0][1] = i_edgeData_right.ru * i_edgeData_right.ru / i_edgeData_right.r + p_right;
		qr_aux[0][2] = i_edgeData_right.ru * (i_edgeData_right.e + p_right) / i_edgeData_right.r;
		
		qr_aux_python = PyArray_SimpleNewFromData( 2, qdim, NPY_FLOAT, qr_aux[0] );
		
		
		// argument list must be a tuple, so here is a tuple
		pArgs = PyTuple_New(5);
		
		PyTuple_SetItem(pArgs, 0, ql_python);
		PyTuple_SetItem(pArgs, 1, qr_python);
		PyTuple_SetItem(pArgs, 2, ql_aux_python);
		PyTuple_SetItem(pArgs, 3, qr_aux_python);
		PyTuple_SetItem(pArgs, 4, vars);
		
		// call the function
		pReturn = PyObject_CallObject(pFunc, pArgs);
		
		    

		if (pReturn != NULL) 
		{
		
			int dim[2] = {1, 3};
			
			// parse the returned python object as tuple to get array objects
			if (!PyArg_ParseTuple(pReturn, "O!O!O!O!",
         			&PyArray_Type, &wave_python, &PyArray_Type, &wave_speed_python, &PyArray_Type, &flux_left_python, &PyArray_Type, 						&flux_right_python)) {
         			std::cout<<"parsing tuple failed"<<std::endl;
         			return NULL;
         		}
         		// important!!! python gives back double
         		int dims[3] = {0,0};
         		
         		// arrays that pyton returned
         		double** wave = pymatrix_to_Carrayptrs(wave_python);
         		double** wave_speed = pymatrix_to_Carrayptrs(wave_speed_python);
			double** flux_left = pymatrix_to_Carrayptrs(flux_left_python);
			double** flux_right = pymatrix_to_Carrayptrs(flux_right_python);
			
			// assign to return values
			*o_max_wave_speed_left = (T) wave_speed[0][0];
			*o_max_wave_speed_right = (T) wave_speed[0][2];
			
			o_edgeFlux_right->r = (T) flux_right[0][0];
			o_edgeFlux_right->ru = (T) flux_right[0][1];
			o_edgeFlux_right->rv = (T) flux_right[0][1];
			o_edgeFlux_right->e = (T) flux_right[0][2];
			
			o_edgeFlux_left->r = (T) flux_left[0][0];
			o_edgeFlux_left->ru = (T) flux_left[0][1];
			o_edgeFlux_left->rv = (T) flux_left[0][1];
			o_edgeFlux_left->e = (T) flux_left[0][2];
			
			//std::cout<<"wave: "<<wave[0][0]<<" "<<wave[0][1]<<" "<<wave[0][2]<<std::endl;
			//std::cout<<"wave speed: "<<wave_speed[0][0]<<" "<<wave_speed[0][1]<<" "<<wave_speed[0][2]<<std::endl;
			//std::cout<<"flux left: "<<flux_left[0][0]<<" "<<flux_left[0][1]<<" "<<flux_left[0][2]<<std::endl;
			//std::cout<<"flux right: "<<flux_right[0][0]<<" "<<flux_right[0][1]<<" "<<flux_right[0][2]<<std::endl;
			Py_DECREF(pReturn);
			
		}
		else 
		{
			std::cout<<"python returned null"<<std::endl;
			PyErr_Print();
		}
	    
	    // some code omitted...
	    } else {
	    	printf("Callable check failed");
	    }

	    // Clean up
	    Py_DECREF(pModule);
	    Py_DECREF(pName);

	    // !! IMPORTANT !! 
	    // Do not call Py_Finalize() here
	   
	   	/* set lambda and output to something so it doesn't crash for now */

		
		/**
		 * add numerical diffusion for stability reason
		 */

		
		return 1;
	  
    }
    
  private:
  double **pymatrix_to_Carrayptrs(PyArrayObject *arrayin)  {
      double **c, *a;
      int i,n,m;
      
      n=arrayin->dimensions[0];
      m=arrayin->dimensions[1];
      c=ptrvector(n);
      a=(double *) arrayin->data;  /* pointer to arrayin data as double */
      for ( i=0; i<n; i++)  {
          c[i]=a+i*m;  }
      return c;
  }
  
  double **ptrvector(long n)  {
     double **v;
     v=(double **)malloc((size_t) (n*sizeof(double)));
     if (!v)   {
         printf("In **ptrvector. Allocation of memory for double array failed.");
         exit(0);  }
     return v;
  }
};



#endif /* CFLUXGEOCLAW_HPP_ */
