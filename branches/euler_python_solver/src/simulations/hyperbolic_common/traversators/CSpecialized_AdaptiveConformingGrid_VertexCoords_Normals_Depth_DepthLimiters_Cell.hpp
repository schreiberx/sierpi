/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_


#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/cluster/CCluster_SplitJoinInformation.hpp"
#include "../subsimulation_generic/CDatasets.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData
{
private:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	class cSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData_Private *generic_traversator;

	bool repeat_traversal;

public:
	CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData();

	virtual ~CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData();

	void setSetupMethod(int i);

	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CHyperbolicTypes::CSimulationStacks *cStacks);

	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CHyperbolicTypes::CSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CHyperbolicTypes::CSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CHyperbolicTypes::CSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CHyperbolicTypes::CSimulationStacks *cStacks,
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	);

	/**
	 * setup the initial root domain cluster
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);

	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_RootTraversator(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);

	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			T i_square_side_length,

			T i_refine_parameter_0,
			T i_coarsen_parameter_0,

			T i_refine_parameter_1,
			T i_coarsen_parameter_1,

			CDatasets *i_cSimulationScenarios
		);

	/**
	 * setup the child cluster based on it's parent traversator information and the child's factory
	 */
	void setup_Cluster(
			CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData &parent,
			CTriangle_Factory &i_child_triangleFactory
		);
};

}
}

#endif
