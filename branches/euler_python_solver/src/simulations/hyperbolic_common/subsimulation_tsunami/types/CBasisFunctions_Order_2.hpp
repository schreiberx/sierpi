/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *      		Stjepan Bakrac <bakrac@in.tum.de>
 *      		Philipp Müller <philippausmuensing@googlemail.com>
 */

#ifndef CBASISFUNCTIONS_ORDER_2_HPP_
#define CBASISFUNCTIONS_ORDER_2_HPP_


/*
 * The ordering of the nodal points is as follows:
 *
 * 4
 * | \
 * |   \
 * |     \
 * 5       3
 * |         \
 * |           \
 * |             \
 * 0------1--------2
 *
 * That is, we start at the origin, and do it counter-clockwise.
 *
 */

class CTsunamiSimulationBasisFunctions
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:

	typedef T (*FBasisFunctions) (T x, T y);


	static inline T phi_0(T x, T y){ return 4 * x * y; }
	static inline T phi_0_dx(T x, T y){ return 4 * y; }
	static inline T phi_0_dy(T x, T y){ return 4 * x; }

	static inline T phi_1(T x, T y){ return -y * (1 - 2 * y); }
	static inline T phi_1_dx(T x, T y){ return 0; }
	static inline T phi_1_dy(T x, T y){ return -1 + 4 * y; }

	static inline T phi_2(T x, T y){ return 4 * y * (1 - x - y); }
	static inline T phi_2_dx(T x, T y){ return -4 * y; }
	static inline T phi_2_dy(T x, T y){ return 4 - 4 * x - 8 * y; }

	static inline T phi_3(T x, T y){ return 1 - 3 * x - 3 * y + 2 * x * x + 4 * x * y + 2 * y * y; }
	static inline T phi_3_dx(T x, T y){ return -3 + 4 * x + 4 * y; }
	static inline T phi_3_dy(T x, T y){ return -3 + 4 * x + 4 * y; }

	static inline T phi_4(T x, T y){ return 4 * x * (1 - x - y); }
	static inline T phi_4_dx(T x, T y){ return 4 - 8 * x - 4 * y; }
	static inline T phi_4_dy(T x, T y){ return -4 * x; }

	static inline T phi_5(T x, T y){ return -x * (1 - 2 * x); }
	static inline T phi_5_dx(T x, T y){ return -1 + 4 * x; }
	static inline T phi_5_dy(T x, T y){ return 0; }

	static T eval(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0,
				phi_1,
				phi_2,
				phi_3,
				phi_4,
				phi_5
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dx(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dx,
				phi_1_dx,
				phi_2_dx,
				phi_3_dx,
				phi_4_dx,
				phi_5_dx
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T eval_dy(int id, T x, T y)
	{
		FBasisFunctions fBasisFunctions[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] =
		{
				phi_0_dy,
				phi_1_dy,
				phi_2_dy,
				phi_3_dy,
				phi_4_dy,
				phi_5_dy
		};
		assert(id < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);
		return fBasisFunctions[id](x, y);
	}

	static T* getNodalCoords()
	{
		static T nodal_coords[] = {
				0.5 , 0.5 ,
				0, 1,
				0, 0.5,
				0, 0,
				0.5, 0,
				1, 0,
		};

		return nodal_coords;
	}

	static int getNumberOfFunctions()
	{
		return 6;
	}
};



#endif /* CBASISFUNCTIONS_ORDER_2_HPP_ */
