/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *      		Stjepan Bakrac <bakrac@in.tum.de>
 *      		Philipp Müller <philippausmuensing@googlemail.com>
 */

#include "CBasisFunctions_Nodal_Order_N.hpp"




CONFIG_DEFAULT_FLOATING_POINT_TYPE CTsunamiSimulationBasisFunctions::exponent_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CONFIG_DEFAULT_FLOATING_POINT_TYPE CTsunamiSimulationBasisFunctions::exponent_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CONFIG_DEFAULT_FLOATING_POINT_TYPE CTsunamiSimulationBasisFunctions::alpha[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

CONFIG_DEFAULT_FLOATING_POINT_TYPE CTsunamiSimulationBasisFunctions::nodalCoords[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*2];
