/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors_HPP_
#define CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors_HPP_

#include <string.h>

#include "../../../subsimulation_generic/CConfig.hpp"
#include "../../../subsimulation_generic/types/CTypes.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature2D_TriangleArea.hpp"
#include "libmath/CMatrixOperations.hpp"

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"

class CHyperbolic_Order_N_Matrices_Vectors
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

public:
	static T mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T stiffness_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T stiffness_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	static T edge_comm_data_nodal_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T edge_flux_quadrature_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS];

	static T adaptivity_refine_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T adaptivity_refine_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	static T adaptivity_coarsen_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T adaptivity_coarsen_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	typedef sierpi::pdelab::CGaussQuadrature2D_TriangleArea<T,SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER> CGaussQuadratureTriangleArea;
	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER> CGaussQuadratureTriangleEdge;
	typedef CHyperbolicTypes::CBasisFunctions CBasisFunctions;


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
	static void setup(int i_verbosity_level)
	{
		/*
		 * setup nodal basis functions by using gauss quadrature points
		 */
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1

	#if 0
			static T coords[3][2] =
			{
					{0, 0},
					{1, 0},
					{0, 1}
			};
	#else
			static T coords[3][2] =
			{
					{0.5, 0.5},
					{0, 0.5},
					{0.5, 0.0}
			};
	#endif

		CBasisFunctions::setup(&coords[0][0], i_verbosity_level);

#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 2

	#if 1
			static T coords[6][2] =
			{
					{0, 1},
					{0, 0.5},
					{0, 0},
					{0.5, 0},
					{1, 0},
					{0.5, 0.5}
			};

	#else

			const T a = 0.57735026918962576451e0*0.5;

			static T coords[6][2] =
			{
					{0, 0.5+a},
					{0, 0.5-a},
					{0.5-a, 0},
					{0.5+a, 0},
					{0.5-a, 0.5+a},
					{0.5+a, 0.5-a}
			};

	#endif

		CBasisFunctions::setup(&coords[0][0], i_verbosity_level);
		//CBasisFunctions::setup(coords, i_verbosity_level);

#else

		CBasisFunctions::setup(CGaussQuadratureTriangleArea::getCoords(), i_verbosity_level);

#endif

#if 0
		for (int i = 0; i < 100000; i++)
		{
			std::cout << "." << std::flush;
			static const int N = 10;

			T im[N][N];

			for (int i = 0; i < N; i++)
				for (int j = 0; j < N; j++)
					im[i][j] = ((T)rand()/(T)RAND_MAX);

			T om[N][N];

			T tm[N][N];

			T d = CMatrixOperations::getInverse<T,N>(im, om);

			CMatrixOperations::multiply<T,N>(im, om, tm);

			T tol = 0.0001/std::abs(d);

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (i == j)
					{
						if (std::abs(tm[i][i] - 1.0) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
					else
					{
						if (std::abs(tm[i][j]) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
				}
			}

			continue;

	error:
			std::cout << "TOL: " << tol << std::endl;

			std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << "Determinant: " << d << std::endl;
			std::cout << std::endl;

			CMatrixOperations::print<T,N>(im);
			CMatrixOperations::print<T,N>(om);
			CMatrixOperations::print<T,N>(tm);


			goto ok;
		}
	ok:
		exit(-1);
#endif

		T *gauss_2d_weights = CGaussQuadratureTriangleArea::getWeights();
		T *gauss_2d_coords = CGaussQuadratureTriangleArea::getCoords();

//		assert(CGaussQuadratureTriangleArea::getNumCoords() == SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);


		/*
		 * MASS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				mass_matrix[i][j] = 0;

				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					mass_matrix[i][j] += gauss_2d_weights[k] * 0.5 * (
							CBasisFunctions::eval(i, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1]) *
							CBasisFunctions::eval(j, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1])
						);
				}
			}
		}


		T determinant = CMatrixOperations::getInverse<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(mass_matrix, inv_mass_matrix);

		if (determinant < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
		{
			std::cout << "Determinant " << determinant << " detected for mass matrix" << std::endl;
			std::cout << std::endl;
			std::cout << "Mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(mass_matrix);
			std::cout << "Inverse mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(inv_mass_matrix);
#if 0
			std::cout << "Emergency exit" << std::endl;
			exit(-1);
#endif
		}

#if 0
		/*
		 * cleanup matrices
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				if (std::abs(inv_mass_matrix[i][j]) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = 0;
				if (std::abs(inv_mass_matrix[i][j]-1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = 1;
				if (std::abs(inv_mass_matrix[i][j]+1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					inv_mass_matrix[i][j] = -1;
			}
		}
#endif

//		assert(CGaussQuadratureTriangleArea::getNumCoords() == SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);

		/*
		 * STIFFNESS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				stiffness_x[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					stiffness_x[i][j] += gauss_2d_weights[k] * 0.5 * (
							CBasisFunctions::eval_dx(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							CBasisFunctions::eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
						);
				}


				stiffness_y[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					stiffness_y[i][j] += gauss_2d_weights[k] * 0.5 * (
							CBasisFunctions::eval_dy(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							CBasisFunctions::eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
						);
				}
			}
		}

		assert(CGaussQuadratureTriangleEdge::getNumPoints() == SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS);

		/*
		 * Edge comm data nodal weights
		 */
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),	edge_comm_data_nodal_weight_matrix[0]);
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	edge_comm_data_nodal_weight_matrix[1]);
		computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),	edge_comm_data_nodal_weight_matrix[2]);

		/*
		 * flux matrices
		 */
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),		M_SQRT2,	edge_flux_quadrature_weight_matrix[0]);
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	1.0,		edge_flux_quadrature_weight_matrix[1]);
		computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),		1.0,		edge_flux_quadrature_weight_matrix[2]);


		T *cell_data_nodal_points = CHyperbolicTypes::CBasisFunctions::getNodalCoords();

		/*
		 * Adaptivity: left restriction matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T child_nodal_points[2];
				CTriangle_PointProjections::leftChildToReference(&cell_data_nodal_points[i*2], child_nodal_points);
				adaptivity_refine_left_child_matrix[i][j] = CTsunamiSimulationBasisFunctions::eval(j, child_nodal_points[0], child_nodal_points[1]);

				assert(child_nodal_points[0] >= 0.0);
				assert(child_nodal_points[1] >= 0.0);
				assert(child_nodal_points[0] + child_nodal_points[1] <= 1.0);	// maybe this failes due to machine accuracy
				assert(child_nodal_points[0] >= child_nodal_points[1]);
			}
		}

		/*
		 * Adaptivity: right restriction matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T child_nodal_points[2];
				CTriangle_PointProjections::rightChildToReference(&cell_data_nodal_points[i*2], child_nodal_points);
				adaptivity_refine_right_child_matrix[i][j] = CTsunamiSimulationBasisFunctions::eval(j, child_nodal_points[0], child_nodal_points[1]);

				assert(child_nodal_points[0] >= 0.0);
				assert(child_nodal_points[1] >= 0.0);
				assert(child_nodal_points[0] + child_nodal_points[1] <= 1.0);	// maybe this failes due to machine accuracy
				assert(child_nodal_points[0] <= child_nodal_points[1]);
			}
		}


		T *ref_coord = CHyperbolicTypes::CBasisFunctions::getNodalCoords();

		/*
		 * Adaptivity: left coarsen matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			assert(ref_coord[0] >= 0.0);
			assert(ref_coord[1] >= 0.0);
			assert(ref_coord[0] + ref_coord[1] <= 1.0);	// maybe this failes due to machine accuracy

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T scalar;

				// inside left triangle
				if (ref_coord[0] > ref_coord[1])
					scalar = 1.0;
				// inside right triangle
				if (ref_coord[0] < ref_coord[1])
					scalar = 0.0;
				// on borderline
				else if (ref_coord[0] == ref_coord[1])
					scalar = 0.5;


				T child_nodal_points[2];
				CTriangle_PointProjections::referenceToLeftChild(&ref_coord[i*2], child_nodal_points);

				adaptivity_coarsen_left_child_matrix[i][j] = CTsunamiSimulationBasisFunctions::eval(j, child_nodal_points[0], child_nodal_points[1])*scalar;
			}
		}

		/*
		 * Adaptivity: right coarsen matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			assert(ref_coord[0] >= 0.0);
			assert(ref_coord[1] >= 0.0);
			assert(ref_coord[0] + ref_coord[1] <= 1.0);	// maybe this failes due to machine accuracy

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T scalar;

				// inside left triangle
				if (ref_coord[0] < ref_coord[1])
					scalar = 1.0;
				// inside right triangle
				if (ref_coord[0] > ref_coord[1])
					scalar = 0.0;
				// on borderline
				else if (ref_coord[0] == ref_coord[1])
					scalar = 0.5;


				T child_nodal_points[2];
				CTriangle_PointProjections::referenceToRightChild(&ref_coord[i*2], child_nodal_points);

				adaptivity_coarsen_right_child_matrix[i][j] = CTsunamiSimulationBasisFunctions::eval(j, child_nodal_points[0], child_nodal_points[1])*scalar;
			}
		}


	}


	static void computeEdgeCommDataNodalWeights(
			const T *edge_sampling_coords,
			T o_edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS]
	)
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				o_edge_comm_data_nodal_weight_matrix[i][j] = CBasisFunctions::eval(j, edge_sampling_coords[0], edge_sampling_coords[1]);
			}
			edge_sampling_coords += 2;
		}
	}


	static void computeEdgeFluxQuardatureWeights(
			const T *i_edge_sampling_coords,
			T i_edge_length,
			T o_edge_flux_quadrature_weights[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
	)	{
		const T *gauss_1d_weights = CGaussQuadratureTriangleEdge::getWeights();

		// for each DOF on the edge
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			const T *edge_sampling_coords = i_edge_sampling_coords;

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; j++)
			{
				T weight = CBasisFunctions::eval(i, edge_sampling_coords[0], edge_sampling_coords[1]);

				// multiply weight with gauss quadrature coefficients
				weight *= gauss_1d_weights[j];

				// multiply weight with 0.5 since integration interval is not [-1,1] but [0,1]
				weight *= 0.5;

				// multiply with edge length
				weight *= i_edge_length;

				o_edge_flux_quadrature_weights[i][j] = weight;

				edge_sampling_coords += 2;
			}
		}
	}


	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Integration cell order: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
		std::cout << " + Integration edge order: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;

		std::cout << "Mass matrix:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(mass_matrix);

		std::cout << "Inv. mass matrix:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(inv_mass_matrix);

		std::cout << "Stiffness matrix X:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(stiffness_x);

		std::cout << "Stiffness matrix Y:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(stiffness_x);

		const char *edge_string[] = {"hyp", "right", "left"};

		for (int e = 0; e < 3; e++)
		{
			std::cout << " > EDGE " << edge_string[e] << std::endl;
			std::cout << "edge_comm_data_nodal_weight_matrix:" << std::endl;
			CMatrixOperations::printNxM<T,SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&edge_comm_data_nodal_weight_matrix[e][0][0]);

			std::cout << "edge_flux_quadrature_weight_matrix:" << std::endl;
			CMatrixOperations::printNxM<T,SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&edge_flux_quadrature_weight_matrix[e][0][0]);
		}
	}
};


#endif /* CEDGECOMM_HYPERBOLIC_ORDER_1_MATRICES_HPP_ */
