/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COPENGL_VERTICES_SMOOTH_ELEMENT_TSUNAMI_HPP_
#define COPENGL_VERTICES_SMOOTH_ELEMENT_TSUNAMI_HPP_

#include "libgl/incgl3.h"

#include "libsierpi/traversators/vertexData/CTraversator_VertexData_CellData_Depth.hpp"
#include "simulations/hyperbolic_common/types/CTypes.hpp"

#include "../../types/CValidation_NodeData.hpp"
#include "../../types/CTsunamiTypes_VisualizationVertexData.hpp"

#include "libsierpi/COutputGridDataArrays.hpp"

namespace sierpi
{
namespace kernels
{


template <
	int p_visualizationType	///< type of visualization
								///< 0: surface
								///< 3: bathymetry
>
class COpenGL_Vertices_Smooth_Cell_Tsunami
{
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::T 			TVertexScalar;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	TVisualizationVertexData;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexData_CellData_Depth<COpenGL_Vertices_Smooth_Cell_Tsunami<p_visualizationType>, CHyperbolicTypes> TRAV;



private:
	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	COutputGridDataArrays<3,6> *cGridDataArrays;


private:
	inline void p_actionFirstTouchVertexData(int i_depth, CCellData *i_cellData, TVisualizationVertexData *o_vertexData)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		o_vertexData->normal[0] += 0*l;
		o_vertexData->normal[1] += 1*l;
		o_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			o_vertexData->height = (i_cellData->dofs_center.h+i_cellData->dofs_center.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = (i_cellData->dofs_center.b)*l;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = l;

#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1

		o_vertexData->normal[0] = 0;
		o_vertexData->normal[1] = 1;
		o_vertexData->normal[2] = 0;

		/*
		 * TODO: fix hyp_edge to care about left/right/hyp edge
		 */
		if (p_visualizationType == 0)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.h+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;
#endif
	}



public:
	inline void op_node_first_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}

	inline void op_node_first_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}

	inline void op_node_first_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}



private:
	inline void p_normalizeVertexData(
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->normal[0] *= inv_fac;
		io_vertexData->normal[1] *= inv_fac;
		io_vertexData->normal[2] *= inv_fac;

		io_vertexData->height *= inv_fac;
#else
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->normal[0] *= inv_fac;
		io_vertexData->normal[1] *= inv_fac;
		io_vertexData->normal[2] *= inv_fac;

		io_vertexData->height *= inv_fac;
#endif
	}



	/**
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void op_node_node_join_middle_touch(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		o_vertexData->normal[0] = i_vertexData1->normal[0] + i_vertexData2->normal[0];
		o_vertexData->normal[1] = i_vertexData1->normal[1] + i_vertexData2->normal[1];
		o_vertexData->normal[2] = i_vertexData1->normal[2] + i_vertexData2->normal[2];

		o_vertexData->height = i_vertexData1->height + i_vertexData2->height;

		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeVertexData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}


	/**
	 * final touch
	 */
public:
	inline void op_node_last_touch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		p_normalizeVertexData(io_vertexData);
	}




private:
	inline void p_actionFirstAndLastTouchVertexData(int i_depth, CCellData *i_cellData, TVisualizationVertexData *o_vertexData)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0

		o_vertexData->normal[0] += 0;
		o_vertexData->normal[1] += 1;
		o_vertexData->normal[2] += 0;

		if (p_visualizationType == 0)
		{
			o_vertexData->height = (i_cellData->dofs_center.h+i_cellData->dofs_center.b);
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = (i_cellData->dofs_center.b);
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;

#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1

		o_vertexData->normal[0] = 0;
		o_vertexData->normal[1] = 1;
		o_vertexData->normal[2] = 0;

		/*
		 * TODO: fix hyp_edge to care about left/right/hyp edge
		 */
		if (p_visualizationType == 0)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.h+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;
#endif
	}



public:
	inline void op_node_first_and_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}

	inline void op_node_first_and_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}

	inline void op_node_first_and_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstAndLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}



private:
	inline void p_actionMiddleTouchVertexData(int i_depth, CCellData *i_cellData, TVisualizationVertexData *io_vertexData)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		io_vertexData->normal[0] += 0*l;
		io_vertexData->normal[1] += 1*l;
		io_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += (i_cellData->dofs_center.h+i_cellData->dofs_center.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_cellData->dofs_center.b*l;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += l;

#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;


		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.h+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif
	}



public:
	inline void op_node_middle_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	inline void op_node_middle_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	inline void op_node_middle_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


private:
	inline void p_actionLastTouchVertexData(
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 0

		T l = getUnitCathetusLengthForDepth(i_depth);
		l = (T)1.0/(l*l);

		io_vertexData->normal[0] += 0*l;
		io_vertexData->normal[1] += 1*l;
		io_vertexData->normal[2] += 0*l;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += (i_cellData->dofs_center.h+i_cellData->dofs_center.b)*l;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += (i_cellData->dofs_center.b)*l;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += l;

#elif SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.h+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif

		p_normalizeVertexData(io_vertexData);
	}



public:
	inline void op_node_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	inline void op_node_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	inline void op_node_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}

	inline void op_cell(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,
			int i_depth,
			CCellData *i_element,
			TVisualizationVertexData *i_leftVertexData,
			TVisualizationVertexData *i_rightVertexData,
			TVisualizationVertexData *i_topVertexData
	)
	{
#if 0
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_leftVertexData->validation.testVertex(v0x, v0y);
		i_rightVertexData->validation.testVertex(v1x, v1y);
		i_topVertexData->validation.testVertex(v2x, v2y);
#endif

		current_vertex_attrib[0*3+0] = v0x;
		current_vertex_attrib[0*3+1] = fixHeight(i_leftVertexData->height);
		current_vertex_attrib[0*3+2] = -v0y;

		current_vertex_attrib[1*3+0] = 0;
		current_vertex_attrib[1*3+1] = 1;
		current_vertex_attrib[1*3+2] = 0;

		current_vertex_attrib[2*3+0] = v1x;
		current_vertex_attrib[2*3+1] = fixHeight(i_rightVertexData->height);
		current_vertex_attrib[2*3+2] = -v1y;

		current_vertex_attrib[3*3+0] = 0;
		current_vertex_attrib[3*3+1] = 1;
		current_vertex_attrib[3*3+2] = 0;

		current_vertex_attrib[4*3+0] = v2x;
		current_vertex_attrib[4*3+1] = fixHeight(i_topVertexData->height);
		current_vertex_attrib[4*3+2] = -v2y;

		current_vertex_attrib[5*3+0] = 0;
		current_vertex_attrib[5*3+1] = 1;
		current_vertex_attrib[5*3+2] = 0;

		current_vertex_attrib += 3*3*2;


		assert(current_vertex_attrib <= vertex_attrib_buffer_end);
		if (current_vertex_attrib == vertex_attrib_buffer_end)
		{
			renderOpenGLVertexArray(max_vertices);
			current_vertex_attrib = vertex_attrib_buffer;
		}

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_element->validation.testVertices(v0x, v0y, v1x, v1y, v2x, v2y);
#endif
#endif
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(
			COutputGridDataArrays<3,6> *o_GridDataArrays,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing = 0
	) {
		cGridDataArrays = o_GridDataArrays;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COpenGL_Vertices_Smooth_Cell_Tsunami<p_visualizationType> &parent_kernel) {
		assert(false);
	}
};


}
}

#endif
