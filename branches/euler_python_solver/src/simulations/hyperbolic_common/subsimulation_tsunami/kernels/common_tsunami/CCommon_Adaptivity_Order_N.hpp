/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/kernels/simulation/CHyperbolic_Order_N_Matrices_Vectors.hpp"
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CCommon_Adaptivity_Order_N
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	// typedef for CAdaptivity_default_handlers (see include at end of this file)
	typedef CCommon_Adaptivity_Order_N CAdaptivity_Common;
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/common/CAdaptivity_default_handlers.hpp"

	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CDatasets *cDatasets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;

	/**
	 * constructor
	 */
	CCommon_Adaptivity_Order_N()	:
		cathetus_side_length(-1),
		cDatasets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}


	/**
	 * get center of weight for triangle
	 */
	inline void computeCenterOfWeight(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_mx, T *o_my
	)
	{
		*o_mx = vtop_x +
				(vright_x - vtop_x)*(T)(1.0/3.0) +
				(vleft_x - vtop_x)*(T)(1.0/3.0);

		*o_my = vtop_y +
				(vright_y - vtop_y)*(T)(1.0/3.0) +
				(vleft_y - vtop_y)*(T)(1.0/3.0);
	}



	/**
	 * get center of weight for triangle for both children
	 */
	inline void computeCenterOfWeightForLeftAndRightChild(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T *o_left_mx, T *o_left_my,
			T *o_right_mx, T *o_right_my
	)
	{
		// midpoint on hypotenuse
		T mx = (vleft_x + vright_x)*(T)(1.0/2.0);
		T my = (vleft_y + vright_y)*(T)(1.0/2.0);

		T dx_left = (vleft_x - mx)*(T)(1.0/3.0);
		T dy_left = (vleft_y - my)*(T)(1.0/3.0);

		T dx_up = (vtop_x - mx)*(T)(1.0/3.0);
		T dy_up = (vtop_y - my)*(T)(1.0/3.0);

		// center of mass in left triangle
		*o_left_mx = mx + dx_left + dx_up;
		*o_left_my = my + dy_left + dy_up;

		// center of mass in right triangle
		*o_right_mx = mx - dx_left + dx_up;
		*o_right_my = my - dy_left + dy_up;
	}


	/**
	 * setup both refined elements
	 */
	inline void refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			const CTsunamiSimulationCellData *i_cCellData,
			CTsunamiSimulationCellData *o_left_cCellData,
			CTsunamiSimulationCellData *o_right_cCellData
	)
	{
		assert(i_cCellData != o_right_cCellData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

#if 0

		// left element
		o_left_cCellData->dofs.h[0]		= i_cCellData->dofs.h[2];
		o_left_cCellData->dofs.hu[0]	= i_cCellData->dofs.hu[2];
		o_left_cCellData->dofs.hv[0]	= i_cCellData->dofs.hv[2];
		o_left_cCellData->dofs.b[0]		= i_cCellData->dofs.b[2];

		o_left_cCellData->dofs.h[1]		= i_cCellData->dofs.h[0];
		o_left_cCellData->dofs.hu[1]	= i_cCellData->dofs.hu[0];
		o_left_cCellData->dofs.hv[1]	= i_cCellData->dofs.hv[0];
		o_left_cCellData->dofs.b[1]		= i_cCellData->dofs.b[0];

		o_left_cCellData->dofs.h[2]		= (i_cCellData->dofs.h[2]+i_cCellData->dofs.h[1])*(T)0.5;
		o_left_cCellData->dofs.hu[2]	= (i_cCellData->dofs.hu[2]+i_cCellData->dofs.hu[1])*(T)0.5;
		o_left_cCellData->dofs.hv[2]	= (i_cCellData->dofs.hv[2]+i_cCellData->dofs.hv[1])*(T)0.5;
		o_left_cCellData->dofs.b[2]		= (i_cCellData->dofs.b[2]+i_cCellData->dofs.b[1])*(T)0.5;


		// right element
		o_right_cCellData->dofs.h[0]	= i_cCellData->dofs.h[1];
		o_right_cCellData->dofs.hu[0]	= i_cCellData->dofs.hu[1];
		o_right_cCellData->dofs.hv[0]	= i_cCellData->dofs.hv[1];
		o_right_cCellData->dofs.b[0]	= i_cCellData->dofs.b[1];

		o_right_cCellData->dofs.h[1]	= o_left_cCellData->dofs.h[2];
		o_right_cCellData->dofs.hu[1]	= o_left_cCellData->dofs.hu[2];
		o_right_cCellData->dofs.hv[1]	= o_left_cCellData->dofs.hv[2];
		o_right_cCellData->dofs.b[1]	= o_left_cCellData->dofs.b[2];

		o_right_cCellData->dofs.h[2]	= i_cCellData->dofs.h[0];
		o_right_cCellData->dofs.hu[2]	= i_cCellData->dofs.hu[0];
		o_right_cCellData->dofs.hv[2]	= i_cCellData->dofs.hv[0];
		o_right_cCellData->dofs.b[2]	= i_cCellData->dofs.b[0];



		/*
		 * right cell data
		 */
		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&o_right_cCellData->dofs.hu[0],
				&o_right_cCellData->dofs.hv[0],
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&o_right_cCellData->dofs.hu[1],
				&o_right_cCellData->dofs.hv[1],
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);

		CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
				&o_right_cCellData->dofs.hu[2],
				&o_right_cCellData->dofs.hv[2],
				-(T)M_SQRT1_2,
				(T)M_SQRT1_2
			);
#endif

		/*
		 * nodal coords are given in reference coordinate system
		 *
		 * |\
		 * |  \
		 * |    \
		 * | R  / \
		 * |  /  L  \
		 * |/_________\
		 *
		 */


		/*
		 * LEFT
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_left_cCellData->dofs.h[i] = 0;
			o_left_cCellData->dofs.hu[i] = 0;
			o_left_cCellData->dofs.hv[i] = 0;
			o_left_cCellData->dofs.b[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				o_left_cCellData->dofs.h[i]		+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_left_child_matrix[i][j] * i_cCellData->dofs.h[j];
				o_left_cCellData->dofs.hu[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_left_child_matrix[i][j] * i_cCellData->dofs.hu[j];
				o_left_cCellData->dofs.hv[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_left_child_matrix[i][j] * i_cCellData->dofs.hv[j];

				/**
				 * NOTE ON BATHYMETRY DATA:
				 *
				 * This data may not be interpolated since constant functions are
				 * only available in modal (frequency) basis representations.
				 */
				o_left_cCellData->dofs.b[i]		+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_left_child_matrix[i][j] * i_cCellData->dofs.b[j];
			}

			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&o_left_cCellData->dofs.hu[i],
					&o_left_cCellData->dofs.hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);
		}




		/*
		 * RIGHT
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_right_cCellData->dofs.h[i] = 0;
			o_right_cCellData->dofs.hu[i] = 0;
			o_right_cCellData->dofs.hv[i] = 0;
			o_right_cCellData->dofs.b[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				o_right_cCellData->dofs.h[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_right_child_matrix[i][j] * i_cCellData->dofs.h[j];
				o_right_cCellData->dofs.hu[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_right_child_matrix[i][j] * i_cCellData->dofs.hu[j];
				o_right_cCellData->dofs.hv[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_right_child_matrix[i][j] * i_cCellData->dofs.hv[j];
				o_right_cCellData->dofs.b[i]	+= CHyperbolic_Order_N_Matrices_Vectors::adaptivity_refine_right_child_matrix[i][j] * i_cCellData->dofs.b[j];

				CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
						&o_right_cCellData->dofs.hu[i],
						&o_right_cCellData->dofs.hv[i],
						-(T)M_SQRT1_2,
						(T)M_SQRT1_2
					);
			}
		}


		//		T local_cfl_domain_size_div_max_wave_speed = i_cCellData->cfl_domain_size_div_max_wave_speed*(T)(1.0/std::sqrt(2.0));

		//		o_left_cCellData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;
		//		o_right_cCellData->cfl_domain_size_div_max_wave_speed = local_cfl_domain_size_div_max_wave_speed;


#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_left_cCellData->refine = false;
		o_left_cCellData->coarsen = false;
		o_right_cCellData->refine = false;
		o_right_cCellData->coarsen = false;
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRefineLeftAndRight(
				i_normal_hyp_x,	i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_depth,
				&o_left_cCellData->validation,
				&o_right_cCellData->validation
			);
#endif
	}


	/**
	 * setup coarsed elements
	 */
	inline void coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CTsunamiSimulationCellData *o_cCellData,
			CTsunamiSimulationCellData *i_left_cCellData,
			CTsunamiSimulationCellData *i_right_cCellData
	)	{

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		{
			T mx = (i_vertex_left_x + i_vertex_right_x)*(T)0.5;
			T my = (i_vertex_left_y + i_vertex_right_y)*(T)0.5;

			i_left_cCellData->validation.testVertices(i_vertex_top_x, i_vertex_top_y, i_vertex_left_x, i_vertex_left_y, mx, my);
			i_right_cCellData->validation.testVertices(i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y, mx, my);
		}
#endif

		T *ref_coord = CHyperbolicTypes::CBasisFunctions::getNodalCoords();

		/*
		 * nodal coords are given in reference coordinate system
		 *
		 * |\
		 * |  \
		 * |    \
		 * | R  / \
		 * |  /  L  \
		 * |/_________\
		 *
		 */
		T left_ref_coord[2];
		T right_ref_coord[2];

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			assert(ref_coord[0] >= 0.0);
			assert(ref_coord[1] >= 0.0);
			assert(ref_coord[0] + ref_coord[1] <= 1.0);	// maybe this failes due to machine accuracy

			if (ref_coord[0] > ref_coord[1])
			{
				/*
				 * left triangle
				 */
				CTriangle_PointProjections::referenceToLeftChild(ref_coord, left_ref_coord);
				CTsunamiSimulationNodeData n;
				i_left_cCellData->computeNodeData(left_ref_coord[0], left_ref_coord[1], &n);

				o_cCellData->dofs.h[i] = n.h;
				o_cCellData->dofs.hu[i] = n.hu;
				o_cCellData->dofs.hv[i] = n.hv;
				o_cCellData->dofs.b[i] = n.b;

				CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
						&o_cCellData->dofs.hu[i],
						&o_cCellData->dofs.hv[i],
						-(T)M_SQRT1_2,
						-(T)M_SQRT1_2
					);
			}
			else if (ref_coord[0] < ref_coord[1])
			{
				/*
				 * right triangle
				 */
				CTriangle_PointProjections::referenceToRightChild(ref_coord, right_ref_coord);
				CTsunamiSimulationNodeData n;
				i_right_cCellData->computeNodeData(right_ref_coord[0], right_ref_coord[1], &n);

				o_cCellData->dofs.h[i] = n.h;
				o_cCellData->dofs.hu[i] = n.hu;
				o_cCellData->dofs.hv[i] = n.hv;
				o_cCellData->dofs.b[i] = n.b;

				CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
						&o_cCellData->dofs.hu[i],
						&o_cCellData->dofs.hv[i],
						-(T)M_SQRT1_2,
						(T)M_SQRT1_2
					);
			}
			else
			{
				/*
				 * average between both triangles
				 */
				CNodeData left_cNodeData;
				CTriangle_PointProjections::referenceToLeftChild(ref_coord, left_ref_coord);
				i_left_cCellData->computeNodeData(left_ref_coord[0], left_ref_coord[1], &left_cNodeData);

				CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
						&left_cNodeData.hu,
						&left_cNodeData.hv,
						-(T)M_SQRT1_2,
						-(T)M_SQRT1_2
					);

				CNodeData right_cNodeData;
				CTriangle_PointProjections::referenceToRightChild(ref_coord, right_ref_coord);
				i_right_cCellData->computeNodeData(right_ref_coord[0], right_ref_coord[1], &right_cNodeData);

				CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
						&right_cNodeData.hu,
						&right_cNodeData.hv,
						-(T)M_SQRT1_2,
						(T)M_SQRT1_2
					);

				o_cCellData->dofs.h[i]	=	(	left_cNodeData.h	+ right_cNodeData.h		) * (T)0.5;
				o_cCellData->dofs.hu[i]	=	(	left_cNodeData.hu	+ right_cNodeData.hu	) * (T)0.5;
				o_cCellData->dofs.hv[i]	=	(	left_cNodeData.hv	+ right_cNodeData.hv	) * (T)0.5;
				o_cCellData->dofs.b[i]	=	(	left_cNodeData.b	+ right_cNodeData.b		) * (T)0.5;
			}

			ref_coord += 2;
		}

//		o_cCellData->cfl_domain_size_div_max_wave_speed = (i_left_cCellData->cfl_domain_size_div_max_wave_speed + i_right_cCellData->cfl_domain_size_div_max_wave_speed)*(T)(0.5*std::sqrt(2.0));

#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_cCellData->refine = false;
		o_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cCellData->validation.setupCoarsen(
				i_normal_hyp_x, i_normal_hyp_y,
				i_normal_right_x, i_normal_right_y,
				i_normal_left_x, i_normal_left_y,
				i_depth,
				&i_left_cCellData->validation,
				&i_right_cCellData->validation
		);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
