/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_HPP_


#if SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS==-1

	#include "CCommon_Adaptivity_Order_0.hpp"
	namespace sierpi
	{
		namespace kernels
		{
			typedef CCommon_Adaptivity_Order_0 CCommon_Adaptivity;
		}
	}

#else

	#include "CCommon_Adaptivity_Order_N.hpp"
	namespace sierpi
	{
		namespace kernels
		{
			typedef CCommon_Adaptivity_Order_N CCommon_Adaptivity;
		}
	}

#endif

#endif /* CADAPTIVITY_0STORDER_HPP_ */
