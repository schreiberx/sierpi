/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_ELEMENT_DATA_ORDER_N_HPP_
#define KERNEL_SETUP_ELEMENT_DATA_ORDER_N_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData_Depth.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "../../CDatasets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
template <typename t_CSimulationStacksAndTypes>
class CSetup_CellData_Order_N
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData_Depth<CSetup_CellData_Order_N, t_CSimulationStacksAndTypes>	TRAV;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CNodeData CNodeData;


	/*.
	 * callback for terrain data
	 */
	CDatasets *cDatasets;

	bool initial_grid_setup;


	CSetup_CellData_Order_N()	:
		cDatasets(nullptr),
		initial_grid_setup(true)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiSimulationCellData *io_cCellData
	)	{
		T lod = CTriangle_Tools::getLODFromDepth(i_depth);

		T *nodal_coords = CHyperbolicTypes::CBasisFunctions::getNodalCoords();

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T world_coords[2];

			CTriangle_PointProjections::referenceToWorld(
					i_vertex_left_x,	i_vertex_left_y,
					i_vertex_right_x,	i_vertex_right_y,
					i_vertex_top_x,		i_vertex_top_y,

					nodal_coords[0],	nodal_coords[1],

					&world_coords[0],	&world_coords[1]
				);

			CTsunamiSimulationNodeData n;

			// initialize in case that getNodalData only updates the values
			n.h = io_cCellData->dofs.h[i];
			n.hu = io_cCellData->dofs.hu[i];
			n.hv = io_cCellData->dofs.hv[i];
			n.b = io_cCellData->dofs.b[i];

			cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

			io_cCellData->dofs.h[i] = n.h;
			io_cCellData->dofs.hu[i] = n.hu;
			io_cCellData->dofs.hv[i] = n.hv;
			io_cCellData->dofs.b[i] = n.b;

			if (io_cCellData->dofs.h[i] < 0)
			{
				assert(false);
				throw("negative depth detected");
			}

			if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
			{
				//momentum was updated -> project to reference space
				CTriangle_VectorProjections::worldToReference(
						&io_cCellData->dofs.hu[i],
						&io_cCellData->dofs.hv[i],
						-i_right_normal_x,
						-i_right_normal_y
					);
			}

			std::cout << io_cCellData->dofs.h[i] << std::endl;

			nodal_coords += 2;
		}

//		io_cCellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();


#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2
		io_cCellData->refine = false;
		io_cCellData->coarsen = false;
#endif
	}


	void setup_Parameters(
			CDatasets *i_cSimulationScenarios,
			bool i_initial_grid_setup
	)
	{
		cDatasets = i_cSimulationScenarios;
		initial_grid_setup = i_initial_grid_setup;
	}



	void setup_WithKernel(
			CSetup_CellData_Order_N<t_CSimulationStacksAndTypes> &i_parent
	)
	{
		cDatasets = i_parent.cDatasets;
		initial_grid_setup = i_parent.initial_grid_setup;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
