/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_HYBRID_HPP_
#define CFLUXSOLVER_HYBRID_HPP_

#include <cmath>
#include <iostream>
#include <cassert>

#include "tsunami_solver/Hybrid.hpp"

/**
 * \brief Hybrid flux solver
 */
template <typename T>
class CFluxSolver_Hybrid	: public solver::Hybrid<T>
{

public:
	CFluxSolver_Hybrid()	:
		solver::Hybrid<T>(
				SIMULATION_TSUNAMI_DRY_THRESHOLD,
				SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION,
				0.000001,
				10,
				SIMULATION_TSUNAMI_ZERO_THRESHOLD
			)
	{
	}

	/**
	 * This method is executed by the cellData computing kernel method
	 * to get the net updates for given fluxes
	 */
public:
	inline void op_edge_edge(
			const CTsunamiSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CTsunamiSimulationNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CTsunamiSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CTsunamiSimulationNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		solver::Hybrid<T>::computeNetUpdates(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.hu, i_edgeData_right.hu,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left->h, o_edgeFlux_right->h,
				o_edgeFlux_left->hu, o_edgeFlux_right->hu,
				*o_max_wave_speed_left
		);

		*o_max_wave_speed_right = *o_max_wave_speed_left;

		o_edgeFlux_left->hv = 0;
		o_edgeFlux_right->hv = 0;
	}



	template <int N>
	void op_edge_edge(
			const CTsunamiSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			const CTsunamiSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CTsunamiSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CTsunamiSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T wave_speed_left = 0;
		T wave_speed_right = 0;

#ifdef __INTEL_COMPILER
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			T l_wave_speed_left = 0;
			T l_wave_speed_right = 0;

			/*
			 * TODO: SIMD
			 */

			CTsunamiSimulationNodeData i_left, i_right;

			i_left.h = i_edgeData_left.h[i];
			i_left.hu = i_edgeData_left.hu[i];
			i_left.hv = i_edgeData_left.hv[i];
			i_left.b = i_edgeData_left.b[i];

			i_right.h = i_edgeData_right.h[N-1-i];
			i_right.hu = -i_edgeData_right.hu[N-1-i];
			i_right.hv = -i_edgeData_right.hv[N-1-i];
			i_right.b = i_edgeData_right.b[N-1-i];

			CTsunamiSimulationNodeData o_left, o_right;

			op_edge_edge(
					i_left,			///< edge data on left (left) edge
					i_right,		///< edge data on right (outer) edge

					&o_left,		///< output for left flux
					&o_right,		///< output for outer flux

					&l_wave_speed_left,				///< maximum wave speed
					&l_wave_speed_right,				///< maximum wave speed

					i_gravitational_constant				///< gravitational constant
			);

			o_edgeFlux_left->h[i] = o_left.h;
			o_edgeFlux_left->hu[i] = o_left.hu;
			o_edgeFlux_left->hv[i] = o_left.hv;
			o_edgeFlux_left->b[i] = o_left.b;

			o_edgeFlux_right->h[N-1-i] = o_right.h;
			o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
			o_edgeFlux_right->hv[N-1-i] = -o_right.hv;
			o_edgeFlux_right->b[N-1-i] = o_right.b;

			wave_speed_left = std::max(wave_speed_left, l_wave_speed_left);
			wave_speed_right = std::max(wave_speed_right, l_wave_speed_right);
		}

		*o_max_wave_speed_left = wave_speed_left;
		*o_max_wave_speed_right = wave_speed_right;
	}
};



#endif
