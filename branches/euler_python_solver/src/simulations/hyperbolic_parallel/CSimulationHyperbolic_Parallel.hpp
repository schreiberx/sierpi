/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_HPP_

#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>

#include "config.h"
#include "libsierpi/parallelization/CSplitJoinTuning.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "libsierpi/grid/CDomain_BaseTriangulationSetup.hpp"
#include "libsierpi/grid/CBaseTriangulation_To_GenericTree.hpp"

#include "lib/CStopwatch.hpp"

#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../hyperbolic_common/CParameters.hpp"
#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_CellData.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/backends/CGetNodeDataSample.hpp"


#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelAdaptivityTraversals.hpp"
#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==2
	#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals_RK2.hpp"
#else
	#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals.hpp"
#endif

#include "CSimulationHyperbolic_Cluster.hpp"
#include "CSplitJoin_TuningTable.hpp"

#include "libmath/CVector.hpp"


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	#include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif

#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulationHyperbolic_Parallel_Gui.hpp"
#endif

#include "CSimulationHyperbolic_Parallel_FileOutput.hpp"

#include "CSimulation_MainInterface.hpp"

/**
 * \brief Main class for parallel hyperbolic Simulation
 *
 * This class is the central point of a parallelized hyperbolic
 * simulation models based on edge-oriented models.
 *
 * Sets up the simulation.
 * It manages all sub-clusters, creates the initial domain triangulation and
 */
class CSimulationHyperbolic_Parallel	:
	public CParameters,
#if CONFIG_SIERPI_ENABLE_GUI
	public CSimulationHyperbolic_Parallel_Gui,			///< GUI?
#else
	public CSimulation_MainInterface,					///< simulation control interface
#endif
	public CSimulationHyperbolic_Parallel_FileOutput	///< file backend
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

	typedef CHyperbolicTypes::CSimulationTypes::T	T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;


public:
	/**
	 * datasets to get bathymetry, water surface parameters, benchmark values, etc.
	 */
	CDatasets cDatasets;


	/**
	 * access to domain clusters
	 */
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> cDomainClusters;


	/**
	 * tuning for the split and join parameters
	 */
	sierpi::CSplitJoinTuning<CSplitJoin_TuningTable> cSplitJoinTuning;


	/**
	 * number of triangles stored during scan in sub-cluster tree valid?
	 */
#if DEBUG && CONFIG_ENABLE_SCAN_DATA
	bool cluster_workload_scans_valid;
#endif

	/**
	 * constructor for parallel hyperbolic simulations
	 */
	CSimulationHyperbolic_Parallel(
			int i_verbosity_level
	)	:
#if CONFIG_SIERPI_ENABLE_GUI
		CSimulationHyperbolic_Parallel_Gui((CParameters&)*this, cDomainClusters),
#endif
		CSimulationHyperbolic_Parallel_FileOutput((CParameters&)*this, cDomainClusters, cDatasets),
		cDatasets((CParameters&)*this, i_verbosity_level)
	{
		verbosity_level = i_verbosity_level;

		number_of_local_cells = 0;
		number_of_local_initial_cells_after_domain_triangulation = 0;

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}


	/**
	 * Deconstructor
	 */
	virtual ~CSimulationHyperbolic_Parallel()
	{
	}

	/**
	 * Reset the simulation
	 */
	void reset()
	{
		sierpi::travs::CSpecialized_EdgeComm_Normals_Depth::setupMatrices(verbosity_level);

		config_validateAndFixParameters();

		number_of_local_cells = 0;

		/*
		 * setup scenarios
		 */
		cDatasets.loadDatasets();

		/*
		 * reset the world: setup triangulation of "scene"
		 */
		p_setup_DomainClusters(simulation_world_scene_id);

		simulation_parameter_global_timestep_size = simulation_parameter_minimum_timestep_size;

		/***************************************************************************************
		 * STACKS: setup the stacks (only the memory allocation) of the clusters
		 ***************************************************************************************/
		unsigned int stackInitializationFlags =
					sierpi::CSimulationStacks_Enums::ELEMENT_STACKS							|
					sierpi::CSimulationStacks_Enums::ADAPTIVE_STACKS						|
					sierpi::CSimulationStacks_Enums::EDGE_COMM_STACKS						|
					sierpi::CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS;

#if CONFIG_SIERPI_ENABLE_GUI
		stackInitializationFlags |=
				sierpi::CSimulationStacks_Enums::VERTEX_COMM_STACKS						|
				sierpi::CSimulationStacks_Enums::VERTEX_COMM_PARALLEL_EXCHANGE_STACKS;
#endif

		/***************************************************************************************
		 * SETUP STRUCTURE STACK and ELEMENT DATA
		 ***************************************************************************************/

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): resetStacks / setupStacks" << std::endl;

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_node)
			{
				// setup structure stack and element data to default values
				i_node->cCluster_TreeNode->resetStacks(stackInitializationFlags, grid_initial_recursion_depth_with_initial_cluster_splits);

				sierpi::travs::CSetup_Structure_CellData<CHyperbolicTypes> cSetup_Structure_CellData;
				cSetup_Structure_CellData.setup(i_node->cCluster_TreeNode->cStacks, grid_initial_recursion_depth_with_initial_cluster_splits, simulation_parameter_cell_data_setup);
			}
		);


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		/***************************************************************************************
		 * SETUP vertex coordinates in element data for debugging purposes
		 ***************************************************************************************/
		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): setup validation" << std::endl;

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				i_cGenericTreeNode->cCluster_TreeNode->cCluster->cSetup_CellData_Validation.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
			}
		);
#endif

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): updateClusterParameters" << std::endl;

		updateClusterParameters();


		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): reset_simulation_parameters" << std::endl;

		reset_simulation_parameters();

#if CONFIG_ENABLE_SCAN_DATA

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): setup_ScanDatasets" << std::endl;

		updateScanDatasets();

#else

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): p_simulation_cluster_split_and_join" << std::endl;

		p_simulation_cluster_split_and_join();

#endif

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): DONE" << std::endl;
	}


private:
	/**
	 * Setup domain cluster:
	 *  - new world triangulation
	 *  - root cluster tree nodes
	 *  - simulation cluster handlers
	 */
	void p_setup_DomainClusters(
			int i_world_scene_id = 0		///< base triangulation to set-up
	)
	{
		sierpi::CDomain_BaseTriangulation<CSimulationHyperbolic_Cluster> cDomain_BaseTriangulation;

		sierpi::CDomain_BaseTriangulationSetup::setupBaseTriangulation(
			i_world_scene_id,
			&cDomain_BaseTriangulation,
			verbosity_level,
			grid_initial_cluster_splits,
			simulation_mpi_rank,
			simulation_mpi_size
		);

		grid_initial_recursion_depth_with_initial_cluster_splits = grid_initial_recursion_depth-grid_initial_cluster_splits;

		simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain = cDomain_BaseTriangulation.length_of_catheti_for_unit_domain;

		cDatasets.getOriginAndSize(
				&simulation_dataset_default_domain_translate_x,
				&simulation_dataset_default_domain_translate_y,
				&simulation_dataset_default_domain_size_x,
				&simulation_dataset_default_domain_size_y
		);


		/*
		 * scale & translate base triangulation to match world-scene
		 */
		cDomain_BaseTriangulation.scaleAndTranslate(
			simulation_dataset_default_domain_translate_x,
			simulation_dataset_default_domain_translate_y,
			simulation_dataset_default_domain_size_x,
			simulation_dataset_default_domain_size_y
		);


		/*
		 * convert to generic tree
		 */
		sierpi::CBaseTriangulation_To_GenericTree<CSimulationHyperbolic_Cluster> cBaseTriangulation_To_GenericTree;

		number_of_local_cells = cBaseTriangulation_To_GenericTree.setup_GenericTree_From_BaseTriangulation(
			cDomain_BaseTriangulation,				///< root triangles
			grid_initial_recursion_depth_with_initial_cluster_splits,			///< initial recursion depth
			grid_initial_recursion_depth_with_initial_cluster_splits-grid_min_relative_recursion_depth,
			grid_initial_recursion_depth_with_initial_cluster_splits+grid_max_relative_recursion_depth,
			simulation_mpi_rank,
			&cDomainClusters						///< domain clusters
		);

		number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_cells);

		number_of_local_clusters = cBaseTriangulation_To_GenericTree.number_of_initial_local_base_clusters;
		number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_clusters);

		number_of_local_initial_cells_after_domain_triangulation = number_of_local_cells;
		number_of_global_initial_cells_after_domain_triangulation = sierpi::CGlobalComm::reduceLongLongSum(number_of_global_clusters);

		if (verbosity_level >= 20)
		{
			for (int i = 0; i < simulation_mpi_size; i++)
			{
				if (i == simulation_mpi_rank)
				{
					std::cout << "GENERIC TREE INFORMATION FOR RANK " << simulation_mpi_rank << std::endl;
					output_ClusterTreeInformation();
				}
				sierpi::CGlobalComm::barrier();
			}
		}
	}


	/***************************************************************************************
	 * ADAPTIVITY
	 ***************************************************************************************/
private:
	/**
	 * run adaptive traversal
	 */
	void p_simulation_adaptive_traversals()
	{
		CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationHyperbolic_Cluster::cAdaptiveTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeEdgeCommData_Adaptivity,
				cDomainClusters,
				&number_of_local_cells
			);

		number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_cells);


#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}


public:
	/**
	 * setup the scan information by running one adaptivity traversal and a split&join traversal
	 */
	void updateScanDatasets(
			int use_number_of_threads = -1
	)
	{
		p_simulation_adaptive_traversal_create_initial_scan_data();
		p_simulation_cluster_split_and_join(use_number_of_threads);
	}


private:
	void p_simulation_adaptive_traversal_create_initial_scan_data()
	{
		CHelper_GenericParallelAdaptivityTraversals::action<true>(
				&CSimulationHyperbolic_Cluster::cAdaptiveTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeEdgeCommData_Adaptivity,
				cDomainClusters,
				&number_of_local_cells
			);

		number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_cells);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif
	}


	long long p_adaptive_traversal_setup_radial_dam_break(
			bool i_split_clusters = true,
			bool i_use_inf_setup_criteria = true
	)
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGenericTreeNode_ *i_genericTreeNode)
				{
					i_genericTreeNode->cCluster_TreeNode->cCluster->cSetupColumnTraversal.setup_KernelClass(
							simulation_dataset_breaking_dam_posx,
							simulation_dataset_breaking_dam_posy,
							simulation_dataset_breaking_dam_radius,

							2,	// REFINE ONLY
							&cDatasets
						);
				}
			);

		long long prev_number_of_local_cells;
		long long prev_number_of_local_simulation_clusters;

		do
		{
			prev_number_of_local_cells = number_of_local_cells;
			prev_number_of_local_simulation_clusters = number_of_local_clusters;

			CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationHyperbolic_Cluster::cSetupColumnTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeEdgeCommData_Adaptivity,
				cDomainClusters,
				&number_of_local_cells
			);

			number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_cells);

			if (i_split_clusters)
				splitOrJoinClusters();

			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_local_cells << ", number_of_simulation_clusters: " << number_of_local_clusters << std::endl;
		}
		while (
			sierpi::CGlobalComm::reduceBooleanOr(
					prev_number_of_local_cells != number_of_local_cells	||
					prev_number_of_local_simulation_clusters != number_of_local_clusters
					)
		);

		number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_cells);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = false;
#endif

		// always run splitOrJoinClusters in the end to set cluster_workload_scans_valid
		p_simulation_cluster_split_and_join();

		/**
		 * finally setup element data values
		 */
		int backupSetupSurfaceMethod = simulation_dataset_1_id;

		if (i_use_inf_setup_criteria)
			simulation_dataset_1_id = CDatasets::SIMULATION_INTERACTIVE_UPDATE;

		p_setup_initial_cell_data(false);

		simulation_dataset_1_id = backupSetupSurfaceMethod;


		return number_of_local_cells;
	}



	/**
	 * setup element data specified by simulation_terrain_scene_id and simulation_water_surface_scene_id
	 */
private:
	void p_setup_initial_cell_data(
			bool i_initial_grid_setup
	)
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGenericTreeNode_ *i_genericTreeNode)
				{
					// setup element data with respect to vertex positions
					sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
					cSetup_CellData.setup_sfcMethods(i_genericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cSetup_CellData.cKernelClass.setup_Parameters(&cDatasets, i_initial_grid_setup);
					cSetup_CellData.action(i_genericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
	}


public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): START" << std::endl;

		long long prev_number_of_local_cells;
		long long prev_number_of_simulation_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		T coarsen_threshold_backup = adaptive_coarsen_parameter_0;
		adaptive_coarsen_parameter_0 = -9999999;
		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 20;

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement" << std::endl;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_local_cells = number_of_local_cells;
			prev_number_of_simulation_clusters = number_of_local_clusters;

			// setup grid data

			if (verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;
			p_setup_initial_cell_data(true);

			// run single timestep
			if (verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_edge_comm" << std::endl;
			p_simulation_edge_comm();

			// refine / coarsen grid
			if (verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_adaptive_traversals" << std::endl;
			p_simulation_adaptive_traversals();

			// split/join clusters
			if (verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_cluster_split_and_join" << std::endl;
			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_local_cells << ", number_of_simulation_clusters: " << number_of_local_clusters << std::endl;

			if (	!sierpi::CGlobalComm::reduceBooleanOr(
						prev_number_of_local_cells != number_of_local_cells	||
						prev_number_of_simulation_clusters != number_of_local_clusters
					)
			)
				break;
		}

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;

		p_setup_initial_cell_data(true);

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): updateClusterParameters" << std::endl;

		// update cluster parameters
		adaptive_coarsen_parameter_0 = coarsen_threshold_backup;
		updateClusterParameters();


		/*****************************************************
		 * setup initial split of clusters
		 *
		 * initial splitting of clusters should be executed before
		 * setting up column to avoid any preprocessing adaptive effects
		 */
		long long t = 0;

		if (verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): setup_SplitJoinClusters" << std::endl;

		do
		{
			t = number_of_local_clusters;

#if SIMULATION_HYPERBOLIC_PARALLEL
			setup_SplitJoinClusters();
#endif

			if (verbosity_level >= 5)
				std::cout << " + splitted to " << number_of_local_clusters << " clusters with " << number_of_local_cells << " triangles" << std::endl;
		}
		while (sierpi::CGlobalComm::reduceBooleanOr(number_of_local_clusters != t));
	}



	/***************************************************************************************
	 * EDGE COMM
	 ***************************************************************************************/
private:
	void p_simulation_edge_comm()
	{
		T local_timestep_size;

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER == 2
		CHelper_GenericParallelFluxCommTraversals_RK2::action<CHyperbolicTypes::CSimulationTypes::CCellData>(
				&CSimulationHyperbolic_Cluster::cEdgeCommTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeFluxCommData,
				cDomainClusters,

				simulation_parameter_cfl,
				&local_timestep_size
			);

#else

		CHelper_GenericParallelFluxCommTraversals::action(
				&CSimulationHyperbolic_Cluster::cEdgeCommTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeFluxCommData,
				cDomainClusters,

				simulation_parameter_cfl,
				&local_timestep_size
			);
#endif

		simulation_parameter_global_timestep_size = sierpi::CGlobalComm::reduceMin(local_timestep_size);

		if (simulation_parameter_global_timestep_size < simulation_parameter_minimum_timestep_size)
		{
			if (simulation_timestep_nr != 0)
			{
				std::ostringstream s;
				s << "Simulation stopped due to small timestep " << simulation_parameter_global_timestep_size << std::endl;

#if CONFIG_EXIT_ON_SMALL_TIMESTEP
				throw(std::runtime_error(s.str()));
#else
				std::cerr << "EMERGENCY (simulation probably unstable): fixing time-step size from " << simulation_parameter_global_timestep_size << " to " << simulation_parameter_minimum_timestep_size << std::endl;
#endif
			}

			simulation_parameter_global_timestep_size = simulation_parameter_minimum_timestep_size;
		}

	}




	/***************************************************************************************
	 * MODIFY_SINGLE_ELEMENT
	 ***************************************************************************************/
	private:
		/**
		 * set element data at coordinate at (x,y) to *cellData
		 */
		void p_set_element_data_at_coordinate(
				T x,				///< x-coordinate inside of specific triangle
				T y,				///< y-coordinate inside of specific triangle
				CHyperbolicTypes::CSimulationTypes::CCellData *i_cellData	///< set element data at (x,y) to this data
		)
		{
			cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_genericTreeNode)
					{
						// We instantiate it right here to avoid any overhead due to split/join operations
						sierpi::kernels::CModify_OneElementValue_SelectByPoint<CHyperbolicTypes>::TRAV cModify_OneElementValue_SelectByPoint;

						cModify_OneElementValue_SelectByPoint.setup_sfcMethods(i_genericTreeNode->cCluster_TreeNode->cTriangleFactory);
						cModify_OneElementValue_SelectByPoint.cKernelClass.setup(
								x,
								y,
								i_cellData
							);

						cModify_OneElementValue_SelectByPoint.action(i_genericTreeNode->cCluster_TreeNode->cStacks);
					}
				);
		}



	/***************************************************************************************
	 * UPDATE CLUSTER SIMULATION PARAMETERS
	 *
	 * This has to be executed whenever the simulation parameters are updated
	 ***************************************************************************************/

public:
	void updateClusterParameters()
	{
		/***************************************************************************************
		 * EDGE COMM: setup boundary parameters
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					EBoundaryConditions boundary_condition = (EBoundaryConditions)simulation_domain_boundary_condition;

					i_cGenericTreeNode->cCluster_TreeNode->cCluster->cEdgeCommTraversal.setBoundaryDirichlet(&simulation_domain_boundary_dirichlet_edge_data);

					/**
					 * check
					 */
					if (simulation_domain_strip_boundary_condition_dataset_right)
					{
						CTriangle_Factory &cTriangle_Factory = i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory;

						// vertical hypotenuse
						if (	(cTriangle_Factory.vertices[0][0] - cTriangle_Factory.vertices[1][0]) == 0	&&
								(cTriangle_Factory.vertices[0][0] == (simulation_dataset_default_domain_size_x*(T)0.5 + simulation_dataset_default_domain_translate_x))
						)
						{
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
						else
						if (	(cTriangle_Factory.vertices[1][0] - cTriangle_Factory.vertices[2][0]) == 0	&&
								(cTriangle_Factory.vertices[1][0] == (simulation_dataset_default_domain_size_x*(T)0.5 + simulation_dataset_default_domain_translate_x))
						)
						{
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
						else
						if (	(cTriangle_Factory.vertices[2][0] - cTriangle_Factory.vertices[0][0]) == 0	&&
								(cTriangle_Factory.vertices[2][0] == (simulation_dataset_default_domain_size_x*(T)0.5 + simulation_dataset_default_domain_translate_x))
						)
						{
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
					}

					i_cGenericTreeNode->cCluster_TreeNode->cCluster->cEdgeCommTraversal.setBoundaryCondition(boundary_condition);
				}
			);


		/***************************************************************************************
		 * EDGE COMM: setup generic parameters
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cCluster_TreeNode->cCluster->cEdgeCommTraversal.setParameters(
							simulation_parameter_global_timestep_size,
							simulation_dataset_default_domain_size_x*simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,
							simulation_parameter_gravitation,
							&cDatasets
						);

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
					i_cGenericTreeNode->cCluster_TreeNode->cCluster->cEdgeCommTraversal.setAdaptivityParameters(adaptive_refine_parameter_0, adaptive_coarsen_parameter_0);
#endif
				}
			);


		/***************************************************************************************
		 * ADAPTIVITY
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cCluster_TreeNode->cCluster->cAdaptiveTraversal.setup_KernelClass(
							simulation_dataset_default_domain_size_x*simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,

							adaptive_refine_parameter_0,
							adaptive_coarsen_parameter_0,

							adaptive_refine_parameter_1,
							adaptive_coarsen_parameter_1,

							&cDatasets
						);
				}
			);
	}




public:
	/**
	 * SPLIT/JOINS for SETUP
	 *
	 * This splits all sub-clusters into appropriate sizes for the initialization.
	 */
	inline void setup_SplitJoinClusters()
	{
		long long prev_number_of_local_cells = 0;

		do
		{
			p_simulation_adaptive_traversals();
			p_simulation_cluster_split_and_join();

			prev_number_of_local_cells = number_of_local_cells;
		}
		while (sierpi::CGlobalComm::reduceBooleanOr(prev_number_of_local_cells != number_of_local_cells));

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


private:
	/**
	 * \brief update split and join size
	 *
	 * this method is intended to optimize the split and/or join size
	 * related to the current workload size
	 */
	inline void p_update_split_join_sizes()
	{
		if (cluster_update_split_join_size_after_elapsed_timesteps != 0)
		{
			if (simulation_timestep_nr % cluster_update_split_join_size_after_elapsed_timesteps == 0)
			{
				if (cluster_update_split_join_size_after_elapsed_scalar != 0)
				{
					/**
					 * compute the split/join sizes by using `cluster_update_split_join_size_after_elapsed_scalar`
					 */
					cluster_split_workload_size = std::sqrt((double)number_of_local_cells)*cluster_update_split_join_size_after_elapsed_scalar;
					cluster_join_workload_size = cluster_split_workload_size / 2;
				}
				else
				{
					/**
					 * lookup the best split/join in a table
					 */
					cSplitJoinTuning.updateSplitJoin(
							cluster_split_workload_size,
							cluster_join_workload_size,
							number_of_local_cells
						);
				}
			}
		}
	}



/***************************************************************************************
 * TIMESTEP
 ***************************************************************************************/

public:
	inline void runSingleTimestep()
	{
		simulation_dataset_benchmark_input_timestamp = simulation_timestamp_for_timestep;

		// simulation timestep
		p_simulation_edge_comm();

		// adaptive timestep
		p_simulation_adaptive_traversals();

		if (simulation_timestep_nr % cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations
			p_simulation_cluster_split_and_join();
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

		p_update_split_join_sizes();
	}


	CStopwatch cStopwatch;

	/***************************************************************************************
	 * run a single TIMESTEP and update the detailed benchmarks
	 ***************************************************************************************/
public:
	/**
	 * execute a single time-step
	 */
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,	///< add time taken for edge communication traversal to this value
			double *io_adaptiveTime,	///< add time taken for adaptive traversal to this value
			double *io_splitJoinTime	///< add time taken for split/joins to this value
	)
	{
		simulation_dataset_benchmark_input_timestamp = simulation_timestamp_for_timestep;

		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		if (simulation_timestep_nr % cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations
			cStopwatch.start();
			p_simulation_cluster_split_and_join();
			*io_splitJoinTime += cStopwatch.getTimeSinceStart();
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

		p_update_split_join_sizes();
	}



	/***************************************************************************************
	 * Radial dam break
	 ***************************************************************************************/

public:
	/**
	 * setup column at 2d position with given radius
	 */
	void setup_RadialDamBreak(
			T i_position_x,			///< x-coordinate of center of column to set-up
			T i_position_y,			///< y-coordinate of center of column to set-up
			T i_radius		///< radius of column to setup
	)
	{
		simulation_dataset_breaking_dam_posx = i_position_x;
		simulation_dataset_breaking_dam_posy = i_position_y;
		simulation_dataset_breaking_dam_radius = i_radius;

		number_of_local_cells = p_adaptive_traversal_setup_radial_dam_break(true);
	}




	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/


	/**
	 * \brief return a data sample at the given position
	 *
	 * this is useful e. g. to get bouy data
	 */
	void p_getDataSample(
			T i_sample_pos_x,
			T i_sample_pos_y,
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_sample_pos_x,
							i_sample_pos_y
						))
							return;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CGetNodeDataSample<CHyperbolicTypes>::TRAV cGetNodeDataSample;

					cGetNodeDataSample.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cGetNodeDataSample.cKernelClass.setup(
							i_sample_pos_x,
							i_sample_pos_y,
							o_cNodeData
						);

					cGetNodeDataSample.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
	}



public:
/*****************************************************************************************************************
 * DEBUG: Output
 *****************************************************************************************************************/
	void debug_OutputEdgeCommunicationInformation(
			T x,	///< x-coordinate of triangle cell
			T y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "Cluster ID: " << node->uniqueId << std::endl;
					std::cout << "Adjacent Communication Information" << std::endl;
					std::cout << node->cCluster_EdgeComm_InformationAdjacentClusters << std::endl;
					std::cout << std::endl;
				}
		);
	}


public:
/*****************************************************************************************************************
 * DEBUG: output element data
 *****************************************************************************************************************/
	void debug_OutputCellData(
			T i_coord_x,		///< x-coordinate of triangle cell
			T i_coord_y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_coord_x, i_coord_y
						))
							return;

					/// output element data at given point
					sierpi::kernels::CStringOutput_CellData_Normal_SelectByPoint<CHyperbolicTypes>::TRAV cStringOutput_CellData_SelectByPoint;

					cStringOutput_CellData_SelectByPoint.setup_sfcMethods(node->cTriangleFactory);
					cStringOutput_CellData_SelectByPoint.cKernelClass.setup(i_coord_x, i_coord_y);
					cStringOutput_CellData_SelectByPoint.action(node->cStacks);
				}
		);
	}

/***************************************************************************************
 * DEBUG: output information about triangle cluster
 ***************************************************************************************/
public:
	void debug_OutputClusterInformation(
			T x,	///< x-coordinate of triangle cell
			T y		///< y-coordinate of triangle cell
	)
	{
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << "* Cluster information at " << x << ", " << y << ":" << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << std::endl;
					std::cout << *node << std::endl;
//					std::cout << node->cCluster_SplitJoinInformation << std::endl;
//					std::cout << std::endl;
					std::cout << "min CFL cell_size / max_speed after edge comm: " << node->cCluster->local_timestep_size << std::endl;
//					std::cout << "min CFL cell_size / max_speed after adaptivity: " << node->cCluster->cfl_domain_size_div_max_wave_speed_after_adaptivity << std::endl;

#if 0
					CSimulationHyperbolic_Cluster *clusterHandler = node->cCluster;

					std::cout << std::endl;
					std::cout << "ClusterAndStackInformation:" << std::endl;
					std::cout << "  + Structure Stacks.direction = " << node->cStacks->structure_stacks.direction << std::endl;
					std::cout << "  + Structure Stacks.forward.getStackElementCounter() = " << node->cStacks->structure_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + Structure Stacks.backward.getStackElementCounter() = " << node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + CellData Stacks.direction = " << node->cStacks->element_data_stacks.direction << std::endl;
					std::cout << "  + CellData Stacks.forward.getStackElementCounter() = " << node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + CellData Stacks.backward.getStackElementCounter() = " << node->cStacks->element_data_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + EdgeComm Stacks.left = " << (void*)node->cStacks->edge_data_comm_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm Stacks.right = " << (void*)node->cStacks->edge_data_comm_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.left = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.right = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + splitJoin.elements_in_first_triangle" << clusterHandler->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements << std::endl;
					std::cout << "  + splitJoin.elements_in_second_triangle" << clusterHandler->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements << std::endl;
					std::cout << std::endl;
#endif
				}
		);
	}


/*****************************************************************************************************************
 * SPLIT / JOIN
 *****************************************************************************************************************/


private:
	/**
	 * split or join sub-clusters
	 *
	 * this method may only be invoked after at least one adaptivity traversal to create the scan information
	 *
	 * \return number of sub-clusters
	 */
	inline size_t p_simulation_cluster_split_and_join(
			int i_number_of_threads = -1	///< if != -1, use this value for new number of threads
	)
	{
#if CONFIG_ENABLE_MPI
		// TODO: deactivated when using MPI
		return number_of_local_clusters;
#endif

		/***************************************************************************************
		 * ACTION: run the split or join operations and update the edge communication information
		 ***************************************************************************************/

		// split/join operations

		// access cTestVariables.allSplit directly within anonymous function does not work.
		// therefore we create a new storage to variables.

		assert(simulation_threading_number_of_threads > 0);

		if (i_number_of_threads != -1)
			simulation_threading_number_of_threads = i_number_of_threads;

		// signed variable necessary!

#if CONFIG_ENABLE_SCAN_DATA

		long long triangles_per_thread = number_of_local_cells / simulation_threading_number_of_threads;
		if (triangles_per_thread*simulation_threading_number_of_threads != number_of_local_cells)
			triangles_per_thread++;

#endif



		cDomainClusters.traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			[&](CGenericTreeNode_ *cGenericTreeNode)
				{
#if CONFIG_ENABLE_SCAN_DATA
					/*
					 * update workload scan data and thread assignments
					 */
					assert(cGenericTreeNode->workload_in_subtree >= 0);
					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif

					cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;


#if (!CONFIG_ENABLE_SCAN_DATA) || (!CONFIG_ENABLE_SCAN_BASED_SPLIT_AND_JOIN && CONFIG_ENABLE_SCAN_DATA)

					long long number_of_local_cells = cGenericTreeNode->workload_in_subtree;

					if (number_of_local_cells >= cluster_split_workload_size)
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
					else if (number_of_local_cells < cluster_join_workload_size)
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::JOIN;

					if (!cGenericTreeNode->cCluster_TreeNode->fun_splitAtLeaves())
						return;

#else

					assert(cGenericTreeNode->workload_scan_start_index >= 0);
					assert(cGenericTreeNode->workload_scan_end_index >= 1);
					assert(cGenericTreeNode->workload_thread_id_end >= cGenericTreeNode->workload_thread_id_start);
					assert(cGenericTreeNode->workload_thread_id_start < simulation_threading_number_of_threads);
					assert(cGenericTreeNode->workload_thread_id_end < simulation_threading_number_of_threads);

					long long number_of_local_cells = cGenericTreeNode->workload_in_subtree;

					// distance to start and end of scan cluster distribution
					long long dist_start = cGenericTreeNode->workload_scan_start_index % triangles_per_thread;
					long long dist_end = triangles_per_thread - (((cGenericTreeNode->workload_scan_end_index-1) % triangles_per_thread) + 1);

					// half workload in cluster
					long long half_workload = cGenericTreeNode->workload_in_subtree >> 1;
					long long quater_workload = half_workload >> 1;


					/*
					 * check whether the SFC cut is going through the current cluster
					 */

					/*
					 * SPLITS
					 */
#if 0
					if (number_of_local_cells > cluster_split_workload_size)
					{
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
						goto done;
					}
#endif

//					if (number_of_local_cells >= cluster_scan_split_min_workload_size)
					{
						if (cGenericTreeNode->workload_thread_id_start != cGenericTreeNode->workload_thread_id_end)
						{
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}

						/*
						 * pre-split to SFC-cut borders
						 */
						if (	quater_workload > dist_start ||
								quater_workload > dist_end
						)
						{
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}
					}

					if (number_of_local_cells >= cluster_scan_join_max_workload_size)
						goto done;

#if 0
					if (number_of_local_cells >= cluster_split_workload_size/2)
					{
						goto done;
					}
#endif

					/*
					 * JOIN
					 */
#if 0
					if (number_of_local_cells < cluster_join_workload_size)
					{
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::JOIN;
						goto done;
					}
#endif

					if (	dist_start > cGenericTreeNode->workload_in_subtree &&
							dist_end > cGenericTreeNode->workload_in_subtree
					)
					{
						// distance to start and end of scan cluster distribution
//						if (dist_start != 0 || dist_end != 0)
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::JOIN;
					}


				done:
					if (!cGenericTreeNode->cCluster_TreeNode->fun_splitAtLeaves())
						return;
#endif


					/*
					 * update scan data when node was not split
					 */
#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->first_child_node->updateWorkloadThreadAssignment(triangles_per_thread);
					cGenericTreeNode->second_child_node->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadScanTopDown();

					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					CCluster_TreeNode_::fun_testAndJoinAtMidNodes(cGenericTreeNode);

#if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadThreadAssignment(triangles_per_thread);
#endif
				}
			);


#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = true;
#endif

		number_of_local_clusters = 0;

		cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
					[&](CGenericTreeNode_ *node, long long *i_reduceValue)
					{
						node->cCluster_TreeNode->cCluster_SplitJoinActions.pass2_updateAdjacentClusterInformation();
						*i_reduceValue = 1;	// one cluster
					},
					&sierpi::CReduceOperators::ADD<long long>,
					&number_of_local_clusters
				);

		auto fun2 = [&](CGenericTreeNode_ *cGenericTreeNode)
				{
					sierpi::CCluster_SplitJoinActions<CSimulationHyperbolic_Cluster>::pass3_swapAndCleanAfterUpdatingEdgeComm(cGenericTreeNode);
				};

		cDomainClusters.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
				fun2,
				fun2
			);


#if DEBUG && CONFIG_ENABLE_SCAN_DATA

		/*
		 * validate correct workload scans
		 */
		int scan_start_id = 0;
		int thread_start_id = 0;
		cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *node)
				{
					assert(node->workload_scan_start_index >= 0);
					assert(node->workload_scan_end_index >= 0);

					assert (node->workload_scan_start_index == scan_start_id);
					scan_start_id = node->workload_scan_end_index;


					if (	node->workload_thread_id_start != thread_start_id &&
							node->workload_thread_id_start != thread_start_id+1
							)
					{
						std::cout << node->workload_thread_id_start << std::endl;
						std::cout << node->workload_thread_id_start << std::endl;
						assert(false);
					}
					thread_start_id = node->workload_thread_id_end;
				}
			);

#endif

		number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(number_of_local_clusters);

		return number_of_local_clusters;
	}


public:
	size_t splitOrJoinClusters()
	{
		return p_simulation_cluster_split_and_join();
	}


	/**
	 * output simulation specific data - e. g. dart smpling points
	 */
	void output_simulationSpecificData()
	{
		if (dart_samplings_size > 0)
		{
			if (verbosity_level > 4)
				std::cout << "Writing DART data" << std::endl;

			for (int i = 0; i < dart_samplings_size; i++)
			{
				// output string buffer
				std::stringstream specificDataStreamBuf;
				specificDataStreamBuf << simulation_timestamp_for_timestep << "\t";

				/*
				 * SIMULATION DATA
				 */
				CHyperbolicTypes::CSimulationTypes::CNodeData cSimulationNodeData;
				p_getDataSample(dart_sample_points[i].position_x, dart_sample_points[i].position_y, &cSimulationNodeData);

				std::vector<T> simulationData;
				cSimulationNodeData.getSimulationSpecificBenchmarkData(&simulationData);

				for (size_t j = 0; j < simulationData.size(); j++)
					specificDataStreamBuf << simulationData[j] << "\t";


				/*
				 * REFERENCE DATA (if available)
				 */
				CHyperbolicTypes::CSimulationTypes::CNodeData cReferenceNodeData;

				if (	cDatasets.getBenchmarkNodalData(
							dart_sample_points[i].position_x,
							dart_sample_points[i].position_y,
							0,
							simulation_timestamp_for_timestep,
							&cReferenceNodeData
					)
				)	{
					std::vector<T> referenceData;
					cReferenceNodeData.getSimulationSpecificBenchmarkData(&referenceData);

					for (size_t j = 0; j < referenceData.size(); j++)
						specificDataStreamBuf << referenceData[j] << "\t";
				}

				specificDataStreamBuf << number_of_local_cells << "\t";
				specificDataStreamBuf << number_of_global_cells << "\t";

				if (dart_sample_points[i].output_file.empty())
				{
					std::cout << specificDataStreamBuf.str() << std::endl;
				}
				else
				{
					std::ofstream s;

					if (simulation_timestep_nr == 0)
						s.open(dart_sample_points[i].output_file.c_str());
					else
						s.open(dart_sample_points[i].output_file.c_str(), std::ofstream::app);

					s << specificDataStreamBuf.str() << std::endl;
				}
			}
		}
	}

	/**
	 * output information about the underlying tree
	 */
	void output_ClusterTreeInformation()
	{
		cDomainClusters.traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					assert(cGenericTreeNode->first_child_node == nullptr);
					assert(cGenericTreeNode->second_child_node == nullptr);
					assert(cGenericTreeNode->cCluster_TreeNode != nullptr);
#endif
					// LEAF
					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;
					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "parent non existing";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first child" : "second child");

					std::cout << "-> LEAF  [" << first_or_second_child << "]: (" << depth;

#if CONFIG_ENABLE_SCAN_DATA
					std::cout << ", workload (start/size/end): " <<
								cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index <<
								" , thread(start/end): " << cGenericTreeNode->workload_thread_id_start << ", " << cGenericTreeNode->workload_thread_id_end <<
								" , thread_id: " << cGenericTreeNode->workload_thread_id;
#endif
					std::cout << ")";

					if (cGenericTreeNode->cCluster_TreeNode == nullptr)
					{
						std::cout << "Empty Leaf (ERROR!)" << std::endl;
						return;
					}

					std::cout << " UniqueId: " << cGenericTreeNode->cCluster_TreeNode->uniqueId;

#if DEBUG
					std::cout << "   timestep size: " << cGenericTreeNode->cCluster_TreeNode->cCluster->cEdgeCommTraversal.getTimestepSize();
#endif
					std::cout << std::endl;


					std::cout << cGenericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters << std::endl;;

				},
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					if (cGenericTreeNode->first_child_node)
						assert(cGenericTreeNode->first_child_node->parent_node == cGenericTreeNode);

					if (cGenericTreeNode->second_child_node)
						assert(cGenericTreeNode->second_child_node->parent_node == cGenericTreeNode);
#endif

					// MIDDLE NODE
					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;

					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "parent non existing";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first" : "second");


					std::cout << "| MID NODE [parents " << first_or_second_child << " child]: (" << depth;
#if CONFIG_ENABLE_SCAN_DATA
					std::cout << ", workload (start/size/end): " << cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index;
#endif
					std::cout << ")";


#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
					assert(cGenericTreeNode->cCluster_TreeNode == nullptr);
#else
					std::cout << " UniqueId: " << cGenericTreeNode->cCluster_TreeNode->uniqueId << std::endl;
#endif
					std::cout << std::endl;
				}
			);
	}




#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

	void action_Validation_EdgeCommLength()
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cCluster->cCluster_ExchangeEdgeCommData_Adaptivity.validateCommDataLength();
			}
		);
	}


	void action_Validation_EdgeCommMidpointsAndNormals()
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				CSimulationHyperbolic_Cluster *worker = node->cCluster;

				/*
				 * setup fake validation element data stack
				 */
				if (node->cValidationStacks->element_data_stacks.forward.getNumberOfElementsOnStack() != node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack())
				{
					node->cValidationStacks->element_data_stacks.forward.clear();

					for (unsigned int i = 0; i < node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack(); i++)
					{
						node->cValidationStacks->element_data_stacks.forward.push(CValCellData());
					}
				}

				worker->cEdgeComm_ValidateComm.cKernelClass.noCellDataChecks = true;

				worker->cEdgeComm_ValidateComm.actionFirstPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->element_data_stacks,
						node->cValidationStacks->edge_data_comm_left_edge_stack,
						node->cValidationStacks->edge_data_comm_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);
			}
		);

#if CONFIG_ENABLE_MPI

		/*
		 * TODO: run this on one thread while running the the shared memory communications on another thread
		 */
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cCluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_DM_pass1();
			}
		);

#endif

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				i_cGenericTreeNode->cCluster_TreeNode->cCluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_SM();
			}
		);

#if CONFIG_ENABLE_MPI

		cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cCluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_DM_pass2();
			}
		);

#endif

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				CSimulationHyperbolic_Cluster *worker = node->cCluster;

				/*
				 * stacks are cleared here since there may be some data left from the last traversal
				 */
				node->cValidationStacks->edge_data_comm_left_edge_stack.clear();
				node->cValidationStacks->edge_data_comm_right_edge_stack.clear();

				/**
				 * second pass
				 */
				worker->cEdgeComm_ValidateComm.actionSecondPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->element_data_stacks,
						node->cValidationStacks->edge_data_comm_exchange_left_edge_stack,		/// !!! here we use the "exchange stacks"!
						node->cValidationStacks->edge_data_comm_exchange_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);
			}
		);
	}

#endif

	void action_Validation()
	{
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


	void outputVerboseInformation()
	{
		((CParameters&)(*this)).outputVerboseInformation();
		cDatasets.outputVerboseInformation();
	}


	/**
	 * dataset accessor for visualization of benchmark data
	 */
	CDatasets *getDatasets()
	{
		return &cDatasets;
	}


};


#endif /* CSIMULATION_HYPERBOLIC_PARALLEL_HPP_ */
