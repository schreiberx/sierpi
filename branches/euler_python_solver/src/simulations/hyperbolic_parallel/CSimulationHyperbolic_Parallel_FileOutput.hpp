/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_FILE_OUTPUT_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_FILE_OUTPUT_HPP_


#include "config.h"

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/backends/CGetNodeDataSample.hpp"

#include "CSimulationHyperbolic_Cluster.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

#include "CSimulation_MainInterface_FileOutput.hpp"
#include "libsierpi/CGridDataArrays.hpp"
#include "lib/CVtkXMLTrianglePolyData.hpp"


class CSimulationHyperbolic_Parallel_FileOutput	:
	public CSimulation_MainInterface_FileOutput
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

	/**
	 * typedefs for domain clusters
	 */
	typedef sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> CDomainClusters_;


public:
	CParameters &cParameters;
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;
	CDatasets &cDatasets;

	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationHyperbolic_Parallel_FileOutput(
			CParameters &i_cParameters,
			CDomainClusters_ &i_cDomainClusters,
			CDatasets &i_cDatasets
	)	:
			cParameters(i_cParameters),
			cDomainClusters(i_cDomainClusters),
			cDatasets(i_cDatasets)
	{
	}

/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	/**
	 * write simulation data to file
	 *
	 * if some information can be included in the file, also write i_information_string to the file
	 */
	void writeSimulationDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		int flags =
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::VALUE0		|
				CGridDataArrays_Enums::VALUE1		|
				CGridDataArrays_Enums::VALUE2		|
				CGridDataArrays_Enums::VALUE3		|
				CGridDataArrays_Enums::VALUE4;

		CGridDataArrays<3,6> cGridDataArrays(
				cParameters.number_of_local_cells,
				1,
				1,
				flags
			);

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = cGridDataArrays.getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

					cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cOutputGridDataArrays.cKernelClass.setup(
							&cGridDataArrays,
							nullptr,
							offset,
							flags
						);

					cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);

		assert(cGridDataArrays.number_of_triangle_cells == cParameters.number_of_local_cells);

		/*
		 * postprocessing
		 */
		if (	cParameters.visualization_scale_x != 1.0 ||
				cParameters.visualization_scale_y != 1.0 ||
				cParameters.visualization_scale_z != 1.0 ||

				cParameters.visualization_translate_x != 1.0 ||
				cParameters.visualization_translate_y != 1.0 ||
				cParameters.visualization_translate_z != 1.0
		)
		{
			for (int i = 0; i < cParameters.number_of_local_cells; i++)
			{
				CONFIG_DEFAULT_FLOATING_POINT_TYPE *v = &(cGridDataArrays.triangle_vertex_buffer[3*3*i]);

				for (int vn = 0; vn < 3; vn++)
				{
					v[3*vn+0] = (v[3*vn+0]-cParameters.simulation_dataset_default_domain_translate_x + cParameters.visualization_translate_x)*cParameters.visualization_scale_x + cParameters.simulation_dataset_default_domain_translate_x;
					v[3*vn+1] = (v[3*vn+1]-cParameters.simulation_dataset_default_domain_translate_y + cParameters.visualization_translate_y)*cParameters.visualization_scale_y + cParameters.simulation_dataset_default_domain_translate_y;
					v[3*vn+2] = (v[3*vn+2] + cParameters.visualization_translate_y)*cParameters.visualization_scale_z;
				}
			}
		}

		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;
			cVtkXMLTrianglePolyData.setup();
			cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
			cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.dof_element[0]);
			cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.dof_element[1]);
			cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.dof_element[2]);
			cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.dof_element[3]);
			cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.dof_element[4]);
		cVtkXMLTrianglePolyData.write(i_filename);
	}



	/**
	 * output clusters to vtk file
	 */

	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		CONFIG_DEFAULT_FLOATING_POINT_TYPE *cluster_vertex_buffer = new CONFIG_DEFAULT_FLOATING_POINT_TYPE[cParameters.number_of_local_clusters*3*3];
		size_t local_cluster_offset = 0;

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t offset = local_cluster_offset;
					local_cluster_offset++;

					CONFIG_DEFAULT_FLOATING_POINT_TYPE *v = &(cluster_vertex_buffer[offset*3*3]);

					v[0*3+0] = node->cTriangleFactory.vertices[0][0];
					v[0*3+1] = node->cTriangleFactory.vertices[0][1];
					v[0*3+2] = 0;

					v[1*3+0] = node->cTriangleFactory.vertices[1][0];
					v[1*3+1] = node->cTriangleFactory.vertices[1][1];
					v[1*3+2] = 0;

					v[2*3+0] = node->cTriangleFactory.vertices[2][0];
					v[2*3+1] = node->cTriangleFactory.vertices[2][1];
					v[2*3+2] = 0;
				}
		);


		assert(local_cluster_offset == (size_t)cParameters.number_of_local_clusters);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;
			cVtkXMLTrianglePolyData.setup();
			cVtkXMLTrianglePolyData.setTriangleVertexCoords(cluster_vertex_buffer, cParameters.number_of_local_clusters);
			delete cluster_vertex_buffer;

#if CONFIG_ENABLE_SCAN_DATA
			int *cluster_id_buffer = new int[cParameters.number_of_local_clusters];
			size_t local_cluster_info_offset;

			cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = local_cluster_info_offset;
					local_cluster_info_offset++;

					cluster_id_buffer[offset] = i_cGenericTreeNode->workload_thread_id;
				}
			);

			assert(local_cluster_info_offset == (size_t)cParameters.number_of_local_clusters);

			cVtkXMLTrianglePolyData.setCellDataInt("cluster_id", cluster_id_buffer);
			delete cluster_id_buffer;
#endif


		cVtkXMLTrianglePolyData.write(i_filename);
	}
};


#endif
