/*
 * CVtkXMLTrianglePolyData.hpp
 *
 *  Created on: Jul 6, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CVTK_XML_TRIANGLE_POLY_DATA
#define CVTK_XML_TRIANGLE_POLY_DATA

#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>

#if CONFIG_ENABLE_LIBXML
	#include <libxml/parser.h>
	#include <libxml/tree.h>
	#include <libxml/xmlwriter.h>
#endif

#include "lib/CLittleBigEndian.hpp"


/**
 *
 * http://www.vtk.org/Wiki/VTK_XML_Formats
 */
class CVtkXMLTrianglePolyData
{
#if CONFIG_ENABLE_LIBXML
	xmlNodePtr root_node;
	xmlNodePtr doc_node;
	xmlNodePtr node_piece;
	xmlNodePtr node_celldata;

	int vtk_raw_header_value;

	/*
	 * number of cells to write data
	 */
	size_t number_of_triangles;

	/*
	 * data buffer
	 */
	char *raw_data_buffer;
	size_t raw_data_buffer_size;
	size_t raw_data_buffer_offset;

	bool setup_valid;
#endif
public:
	CVtkXMLTrianglePolyData()
	{
#if CONFIG_ENABLE_LIBXML
		LIBXML_TEST_VERSION;

		root_node = NULL;
		doc_node = NULL;
		node_piece = NULL;
		number_of_triangles = 0;

		raw_data_buffer = NULL;
		raw_data_buffer_size = 0;
		raw_data_buffer_offset = 0;

		setup_valid = false;
#endif
	}


	void setup()
	{
#if CONFIG_ENABLE_LIBXML
		assert(setup_valid == false);
		setup_valid = true;

		// create VTKFile root node
		root_node = xmlNewNode(NULL, BAD_CAST "VTKFile");

		// setup root node properties
	    xmlNewProp(root_node, BAD_CAST "type", BAD_CAST "PolyData");
	    xmlNewProp(root_node, BAD_CAST "version", BAD_CAST "0.1");
	    xmlNewProp(root_node, BAD_CAST "byte_order", BAD_CAST (CLittleBigEndian::isLittleEndian() ? "LittleEndian" : "BigEndian"));

	    /*
	     * we like to output polygonal data
	     */
		xmlNodePtr node_polydata = xmlNewChild(root_node, NULL, BAD_CAST "PolyData", NULL);


		/*
		 * General information about how many datasets there are
		 */
		node_piece = xmlNewChild(node_polydata, NULL, BAD_CAST "Piece", NULL);
#endif
	}




private:
	/**
	 * append to buffer and return offset
	 */
	template <typename U>
	inline size_t p_appendToBuffer(
		size_t i_data_length,
		U *i_data
	)
	{
		// remember old size to return offset
		size_t n = raw_data_buffer_size;

		// reallocate when buffer was allocated before
		if (raw_data_buffer != NULL)
		{
			char *new_raw_data_buffer = new char[raw_data_buffer_size + i_data_length*sizeof(U)+sizeof(vtk_raw_header_value)];
			memcpy(new_raw_data_buffer, raw_data_buffer, raw_data_buffer_size);

			delete [] raw_data_buffer;
			raw_data_buffer = new_raw_data_buffer;
		}
		else
		{
			assert(raw_data_buffer_size == 0);
			raw_data_buffer = new char[i_data_length*sizeof(U)+sizeof(vtk_raw_header_value)];
		}

		// size of raw data to write to buffer
		vtk_raw_header_value = i_data_length*sizeof(U);

		// copy next buffer block size to buffer
		memcpy(raw_data_buffer+raw_data_buffer_size, (char*)&vtk_raw_header_value, sizeof(vtk_raw_header_value));

		// copy data to buffer
		memcpy(raw_data_buffer+raw_data_buffer_size+sizeof(vtk_raw_header_value), (char*)i_data, i_data_length*sizeof(U));

		// increment with corresponding size
		raw_data_buffer_offset += i_data_length*sizeof(U);
		raw_data_buffer_size += i_data_length*sizeof(U)+sizeof(vtk_raw_header_value);

		return n;
	}


public:
	template <typename T>
	void setTriangleVertexCoords(
			T *i_vertices,				///< filename of created xml file
			int i_number_of_triangles
	)
	{
#if CONFIG_ENABLE_LIBXML
		assert(setup_valid);

		number_of_triangles += i_number_of_triangles;

		xmlNodePtr node_data_array;
		size_t raw_offset;
		char str_buf[1024];

		/*
		 * Create Points XML Scope
		 */
		xmlNodePtr node_points = xmlNewChild(node_piece, NULL, BAD_CAST "Points", NULL);

		node_data_array = xmlNewChild(node_points, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST (sizeof(T) == 4 ? "Float32" : "Float64"));
		xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST "Points");
		xmlNewProp(node_data_array, BAD_CAST "NumberOfComponents", BAD_CAST "3");
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		// copy vertices to buffer
		raw_offset = p_appendToBuffer(number_of_triangles*3*3, i_vertices);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);


		/*
		 * Cells
		 */
		xmlNodePtr node_cells = xmlNewChild(node_piece, NULL, BAD_CAST "Polys", NULL);


		/*
		 * connectivity
		 */
		node_data_array = xmlNewChild(node_cells, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST "Int32");
		xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST "connectivity");
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		int *connectivity = new int[number_of_triangles*3];
		for (size_t i = 0; i < (size_t)number_of_triangles*3; i++)
			connectivity[i] = i;

		// copy connectivity to buffer
		raw_offset = p_appendToBuffer(number_of_triangles*3, connectivity);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);

		delete [] connectivity;


		/*
		 * offsets
		 */
		node_data_array = xmlNewChild(node_cells, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST "Int32");
		xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST "offsets");
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		int *offsets = new int[number_of_triangles];
		for (size_t i = 0; i < (size_t)number_of_triangles; i++)
			offsets[i] = (i+1)*3;

		raw_offset = p_appendToBuffer(number_of_triangles, offsets);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);

		delete [] offsets;

		/*
		 * types
		 */
		node_data_array = xmlNewChild(node_cells, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST "Int32");
		xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST "types");
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		int *types = new int[number_of_triangles];
		for (size_t i = 0; i < (size_t)number_of_triangles; i++)
			types[i] = 5;

		raw_offset = p_appendToBuffer(number_of_triangles, types);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);

		delete [] types;


		/*
		 * open scope for celldata
		 */
		node_celldata = xmlNewChild(node_piece, NULL, BAD_CAST "CellData", NULL);
	    xmlNewProp(node_celldata, BAD_CAST "Scalars", BAD_CAST "scalars");
#endif
	}


	template <typename U>
	void setCellDataFloat(
			const char *i_field_name,
			U *i_data
	)
	{
#if CONFIG_ENABLE_LIBXML
		assert(setup_valid);

		xmlNodePtr node_data_array;
		char str_buf[1024];

	    /*
	     * Cell Data
	     */

		node_data_array = xmlNewChild(node_celldata, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST ((sizeof(U) == 4) ? "Float32" : "Float64"));
	    xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST i_field_name);
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		size_t raw_offset = p_appendToBuffer(number_of_triangles, i_data);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);
#endif
	}




	template <typename U>
	void setCellDataInt(
			const char *i_field_name,
			U *i_data
	)
	{
#if CONFIG_ENABLE_LIBXML
		assert(setup_valid);

		xmlNodePtr node_data_array;
		char str_buf[1024];

	    /*
	     * Cell Data
	     */

		node_data_array = xmlNewChild(node_celldata, NULL, BAD_CAST "DataArray", NULL);
		xmlNewProp(node_data_array, BAD_CAST "type", BAD_CAST ((sizeof(U) == 4) ? "Int32" : "Int64"));
	    xmlNewProp(node_data_array, BAD_CAST "Name", BAD_CAST i_field_name);
		xmlNewProp(node_data_array, BAD_CAST "Format", BAD_CAST "appended");

		size_t raw_offset = p_appendToBuffer(number_of_triangles, i_data);
		sprintf(str_buf, "%lu", (unsigned long)raw_offset);
		xmlNewProp(node_data_array, BAD_CAST "offset", BAD_CAST str_buf);
#endif
	}




	/**
	 * open file to write xml data to
	 */
	void write(
		const char *i_filename
	)
	{
#if CONFIG_ENABLE_LIBXML
		assert(setup_valid);

		// create document
		xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");

			// set root node for document
			doc_node = xmlDocSetRootElement(doc, root_node);

			char buf[1024];

			/*
			 * append information about number of triangles to node "piece"
			 */
			sprintf(buf, "%lu", (unsigned long)number_of_triangles*3);
			xmlNewProp(node_piece, BAD_CAST "NumberOfPoints", BAD_CAST buf);
			xmlNewProp(node_piece, BAD_CAST "NumberOfVerts", BAD_CAST "0");
			xmlNewProp(node_piece, BAD_CAST "NumberOfLines", BAD_CAST "0");
			xmlNewProp(node_piece, BAD_CAST "NumberOfStrips", BAD_CAST "0");
			sprintf(buf, "%lu", (unsigned long)number_of_triangles);
			xmlNewProp(node_piece, BAD_CAST "NumberOfPolys", BAD_CAST buf);

			/*
			 * <AppendedData encoding="raw">
			 *
			 * setup dummy append_data_tag since writing raw data to vtp files is not allowed!
			 */
			const char *append_data_tag = "[APPEND%DATA]";

			xmlNodePtr node_rawdata = xmlNewChild(root_node, NULL, BAD_CAST "AppendedData", BAD_CAST append_data_tag);
			xmlNewProp(node_rawdata, BAD_CAST "encoding", BAD_CAST "raw");

			xmlBufferPtr buffer = xmlBufferCreate();
			xmlOutputBufferPtr output_buffer = xmlOutputBufferCreateBuffer(buffer, NULL);

				xmlSaveFormatFileTo(output_buffer, doc, "UTF-8", 1);

				char *b = (char*)buffer->content;

				std::ofstream ofile;
				ofile.open(i_filename);

				// search for start of tag
				char *tag_start = strstr(b, append_data_tag);
				size_t tag_start_len = tag_start - b;

				// write data until start tag
				ofile.write(b, tag_start - b);

				// write "_" to show start of raw data
				ofile.write("\n", 1);
				ofile.write("   _", 4);

				// write raw data
				ofile.write(raw_data_buffer, raw_data_buffer_size);

				ofile.write("\n", 1);

				// write remaining data of xml file
				size_t tag_length = strlen(append_data_tag);
				ofile.write(tag_start+tag_length, buffer->use - (tag_start_len + tag_length));

				ofile.close();

			xmlBufferFree(buffer);

	    xmlFreeDoc(doc);

	    cleanup();
#endif
	}



	void cleanup()
	{
#if CONFIG_ENABLE_LIBXML

		if (doc_node != NULL)
		{
			xmlFreeNode(doc_node);
			doc_node = NULL;
		}

		if (raw_data_buffer)
		{
			delete [] raw_data_buffer;
			raw_data_buffer = NULL;
			raw_data_buffer_size = 0;
			raw_data_buffer_offset = 0;
		}

		number_of_triangles = 0;

		setup_valid = false;
#endif
	}

	~CVtkXMLTrianglePolyData()
	{
		cleanup();
	}
};

#endif
