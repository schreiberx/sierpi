#! /bin/bash

EXEC_ITBB="../../build/sierpi_intel_itbb_tsunami_parallel_release"
EXEC_TBB="../../build/sierpi_intel_tbb_tsunami_parallel_release"
EXEC_OMP="../../build/sierpi_intel_omp_tsunami_parallel_release"

killall $EXEC_TBB
killall $EXEC_ITBB
killall $EXEC_OMP

EXEC_IPMO="../../../ipmo_2012_05_28/build/iPMO_server_release"

PARAMS_DEFAULT=" -d 4 -o 8192 -B 1 -a 16 -v -99"
