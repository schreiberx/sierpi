#!/bin/bash

# application is running for 100 seconds in average

. params.sh

killall sierpi_intel_tbb_tsunami_parallel_release

EXEC="$EXEC_TBB"
PARAMS="$PARAMS_DEFAULT -N 10 -n 10"

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}


date
STARTSECONDS=`date +%s`

echo "App 1: $PROGRAM_START"
$PROGRAM_START -A 1 -S 0 > "output_""$DATE""_tbb_pinned_parallel_1.txt" &
PA1=$!

echo "App 2: $PROGRAM_START"
$PROGRAM_START -A 1 -S 10 > "output_""$DATE""_tbb_pinned_parallel_2.txt" &
PA2=$!

echo "App 3: $PROGRAM_START"
$PROGRAM_START -A 1 -S 20 > "output_""$DATE""_tbb_pinned_parallel_3.txt" &
PA3=$!

echo "App 4: $PROGRAM_START"
$PROGRAM_START -A 1 -S 30 > "output_""$DATE""_tbb_pinned_parallel_4.txt" &
PA4=$!


wait $PA1
wait $PA2
wait $PA3
wait $PA4


ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS" > "output_""$DATE""_tbb_pinned_parallel.txt"

sleep 1
