Executable + params: ../../build/sierpi_intel_tbb_taskaffinities_scan_tsunami_parallel_release -o 32768 -A 1 -d 20 -a 8 -t 100
Problem size with different number of CPUs using function optimization
THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS
40	49.9495	100	0.000491318		1.9047e+07	963.71
32	49.5516	100	0.000491318		1.9047e+07	773.72
30	40.599	100	0.000491318		1.9047e+07	756.66
20	24.4507	100	0.000491318		1.9047e+07	533.26
16	21.1311	100	0.000491318		1.9047e+07	433.08
10	13.3644	100	0.000491318		1.9047e+07	289.05
8	14.5348	100	0.000491318		1.9047e+07	236.11
4	8.6905	100	0.000491318		1.9047e+07	133.26
2	4.59263	100	0.000491318		1.9047e+07	75.89
1	2.39257	100	0.000491318		1.9047e+07	48
