/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingIOMP.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include "libmath/CMath.hpp"


#include <OR.hpp>
#include <AND.hpp>
#include <Claim.hpp>
#include <PEQuantity.hpp>
#include <ScalabilityHint.hpp>


#include "CMainThreading_Interface.hpp"

/**
 * Threading support for invasive OpenMP
 */
class CMainThreading	:
	public CMainThreading_Interface
{
public:
	int max_cores;
	Claim claim;

	CMainThreading()
	{
		max_cores = 512;
	}

	void threading_setup()
	{
		int n = getNumberOfThreadsToUse();

		if (n == -1)
		{
			n = omp_get_max_threads();
			setValueNumberOfThreadsToUse(n);
		}
		else
		{
			omp_set_num_threads(n);
		}

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): " << (int)omp_get_max_threads() << std::endl;

		threading_updateResourceUtilization();
	}


	/**
	 * update the ressource utilization
	 */
	void threading_updateResourceUtilization()
	{
		claim.invade(PEQuantity(1, max_cores));
	}


	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			threading_updateResourceUtilization();
			continue_simulation = threading_simulationLoopIteration();
		} while(continue_simulation);
	}


	/**
	 * FOR GUI ONLY
	 */
	bool threading_simulationLoopIteration()
	{
		return simulationLoopIteration();
	}


	void threading_shutdown()
	{
	}


	void threading_setNumThreads(int i)
	{
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
