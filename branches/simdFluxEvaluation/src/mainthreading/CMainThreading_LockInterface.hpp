/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreading_MutexOMP.hpp
 *
 *  Created on: Jul 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_LOCK_INTERFACE_HPP_
#define CMAINTHREADING_LOCK_INTERFACE_HPP_

/**
 * Mutex inteface
 */
class CMainThreading_LockInterface
{
public:
	virtual void lock() = 0;
	virtual void unlock() = 0;

	virtual ~CMainThreading_LockInterface() {};
};


#endif /* CMAINTHREADING_MUTEXOMP_HPP_ */
