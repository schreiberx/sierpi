/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreading_MutexOMP.hpp
 *
 *  Created on: Jul 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_MUTEX_OMP_HPP_
#define CMAINTHREADING_LOCK_OMP_HPP_

#include "CMainThreading_LockInterface.hpp"
/**
 * Threading support for dummy threading
 */
class CMainThreading_Lock	:
	public CMainThreading_LockInterface
{

public:
	CMainThreading_Lock()
	{
	}

	void lock()
	{
	}

	void unlock()
	{
	}

	~CMainThreading_Lock()
	{
	}
};


#endif /* CMAINTHREADING_LOCK_OMP_HPP_ */
