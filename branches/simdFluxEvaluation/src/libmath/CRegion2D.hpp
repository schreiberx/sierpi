/*
 * CRegion2D.hpp
 *
 *  Created on: Sep 5, 2011
 *
 *      Author: Martin Schreiber (schreiberx@gmail.com)
 */

#ifndef CREGION2D_HPP_
#define CREGION2D_HPP_



/**
 * Implementation of a 2D region description with some handy features
 */
template <typename T>
class CRegion2D
{
public:
	/// regions minimum limit along x axis
	T x_min;

	/// regions maximum limit along x axis
	T x_max;

	/// regions minimum limit along y axis
	T y_min;

	/// regions maximum limit along y axis
	T y_max;


	/**
	 * constructor
	 */
	CRegion2D()
	{
		reset();
	}

	/**
	 * compute and return the region size in X direction
	 */
	T getSizeX()
	{
		return x_max - x_min;
	}

	/**
	 * compute and return the region size in Y direction
	 */
	T getSizeY()
	{
		return y_max - y_min;
	}

	/**
	 * reset (set region to invalid values)
	 */
	void reset()
	{
		x_min = CMath::numeric_inf<T>();
		x_max = -CMath::numeric_inf<T>();
		y_min = CMath::numeric_inf<T>();
		y_max = -CMath::numeric_inf<T>();
	}

	/**
	 * extend region if necessary to fit given coordinate into region
	 */
	void extend(
			const T i_pos_x,	///< x coordinate
			const T i_pos_y		///< y coordinate
	)
	{
		x_min = CMath::min(i_pos_x, x_min);
		x_max = CMath::max(i_pos_x, x_max);

		y_min = CMath::min(i_pos_y, y_min);
		y_max = CMath::max(i_pos_y, y_max);
	}

	/**
	 * scale the region
	 */
	void scale(
			const T i_scalar	///< scalar to scale up region
	)
	{
		x_min *= i_scalar;
		x_max *= i_scalar;

		y_min *= i_scalar;
		y_max *= i_scalar;
	}
};



/**
 * output region information
 */
template <class T>
inline
::std::ostream&
operator<<(
		::std::ostream &co,
		 const CRegion2D<T> &r
)
{
	return co << "[" << r.x_min << ", " << r.x_max << "]x[" << r.y_min << ", " << r.y_max << "]";
}

#endif /* CREGION2D_HPP_ */
