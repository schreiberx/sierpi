/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_

#include "config.h"

#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CDomainClusters.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CCluster_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals
{
public:

	/**
	 * run the edge comm traversals
	 */
	template<
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue				/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

		/*
		 * first EDGE COMM TRAVERSAL pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					(node->cCluster->*(i_simulationSubClass)).actionFirstPass(node->cStacks);
				}
		);


#if CONFIG_ENABLE_MPI
	#if !CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER
		#error "Single flux evaluation not available with MPI! For single flux evaluation, fluxes are still double evaluated across MPI nodes!"
	#endif
#endif

//#if CONFIG_ENABLE_MPI
		*o_cfl_reduce_value = CMath::numeric_inf<TReduceValue>();
//#endif

#if CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER || CONFIG_ENABLE_MPI

#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass1();
			}
		);
#endif

		/*
		 * Compute fluxes using uniqueIDs to avoid double flux evaluation:
		 *
		 * The sub-cluster with the relation 'uniqueID < adjacentUniqueID' is responsible to compute the fluxes
		 * also in a writing manner for the adjacent one.
		 *
		 * In FLUX COMM PASS:
		 * 1) The responsible sub-cluster first fetches the data from the adjacent cluster
		 *    to the exchange edge comm data stacks.
		 *
		 * 2) The fluxes are computed for all fetched edge communication data by the responsible sub-cluster.
		 *
		 * In SECOND PASS:
		 * 3) Storing the fluxes to the local_edge_comm_data_stack and exchange_edge_comm_data_stack, pulling
		 *    the edge communication data from the sub-clusters with 'uniqueID > adjacentUniqueID' fetches the
		 *    already computed fluxes.
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// pull edge data only in one direction using uniqueIDs as relation and compute the flux
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_SM_pass1();
				}
		);
#endif

#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
			}
		);
#endif

		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode, TReduceValue *o_reduceValue)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

#if CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER
					// firstly, pull edge communication data from adjacent sub-clusters
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_SM_pass2();
#else
					// firstly, pull edge communication data from adjacent sub-clusters and compute fluxes
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluationSharedMemory();
#endif
					// run computation based on newly set-up stacks
					(node->cCluster->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, i_timestep_size, &node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm);

					*o_reduceValue = node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm;
				},
				&(sierpi::CReduceOperators::MIN<TReduceValue>),	// use minimum since the minimum timestep has to be selected
				o_cfl_reduce_value
			);
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
