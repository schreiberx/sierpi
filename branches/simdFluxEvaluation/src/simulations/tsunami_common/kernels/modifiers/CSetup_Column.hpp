/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef KERNEL_SETUP_COLUMN_TSUNAMI_HPP_
#define KERNEL_SETUP_COLUMN_TSUNAMI_HPP_


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CSetup_Column_0thOrder.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_Column_0thOrder<CTsunamiSimulationCellData, CTsunamiSimulationStacksAndTypes> CSetup_Column;
	}
}

#else

#include "CSetup_Column_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_Column_1stOrder<CTsunamiSimulationCellData, CTsunamiSimulationStacksAndTypes> CSetup_Column;
	}
}

#endif

#endif
