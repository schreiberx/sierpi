#! /bin/bash

. ../tools.sh

START_DEPTH=4
MAX_REFINEMENT_DEPTH=16

#export
#exit

NUM_CORES=40

# set splitting size to be fixed
PARAMS="-N $NUM_CORES -d $START_DEPTH -o $((1024*8)) -A 1 -v -99 -B 1 -a $MAX_REFINEMENT_DEPTH"
#

EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_release"


THREAD_LIST=`seq $NUM_CORES -1 1`

echo "THREADS	MTPS	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
for THREADS in $THREAD_LIST; do
	echo "Using $THREADS threads"

	PARAMS_=" -n $THREADS $PARAMS $@"

	EXEC_CMD="$EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	$EXEC_CMD
	echo

	echo "Sleeping for 61 seconds" 2>&1
	sleep 61
done
