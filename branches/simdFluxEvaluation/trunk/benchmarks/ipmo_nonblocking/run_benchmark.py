#! /usr/bin/python

import subprocess, sys, os, os.path
from datetime import date

date = str(date.today()).replace("-", "_")

n_iter = 5

default_params=" -G 1 -t 10000 -d 6 -a 0 -A 1"

execs = ['sierpi_gnu_ipmo_async_tsunami_parallel_b0_release', 'sierpi_gnu_ipmo_tsunami_parallel_b0_release', 'sierpi_gnu_itbb_async_tsunami_parallel_b0_release', 'sierpi_gnu_itbb_tsunami_parallel_b0_release', 'sierpi_gnu_omp_tsunami_parallel_b0_release', 'sierpi_gnu_tbb_tsunami_parallel_b0_release']
exec_prefix="../../build/"


# HEADER

sys.stdout.write("application\taveraged_runtime")
for i in range(0, n_iter):
	sys.stdout.write("\t"+str(i))
sys.stdout.write("\n")

# RUN SIMULATION

for e in execs:
	runtimes = []

	runtime_sum = 0
	for j in range(0, n_iter):
		lines = subprocess.Popen(exec_prefix+e+" "+default_params, stdout=subprocess.PIPE, shell=True).communicate()[0].splitlines()

		runtime = -1
		for line in lines:
			# get simulation real time
			if " RT " in line:
				runtime = float(line.split(" RT ")[0])
				runtime_sum += runtime

		runtimes.append(runtime)

	averaged_runtime = runtime_sum / n_iter

	sys.stdout.write(e+"\t")
	sys.stdout.write(str(averaged_runtime))

	for j in range(0, n_iter):
		sys.stdout.write("\t"+str(runtimes[j]))
	sys.stdout.write("\n")
		
