Running benchmarks with default parameters:  -G 1 -d 8 -a 8 -A 1

application	averaged_runtime	0	1	2	3	4	5	6	7	8	9
sierpi_intel_ipmo_async_tsunami_parallel_b0_release	1.63814	1.6422	1.6488	1.64037	1.63881	1.63379	1.6385	1.63506	1.63295	1.63537	1.63555
sierpi_intel_ipmo_tsunami_parallel_b0_release	1.684886	1.6837	1.6826	1.68052	1.68302	1.68403	1.68832	1.67918	1.68707	1.69673	1.68369
sierpi_intel_itbb_async_tsunami_parallel_b0_release	1.372234	1.36811	1.37333	1.37187	1.3708	1.36942	1.373	1.37678	1.36888	1.37046	1.37969
sierpi_intel_itbb_tsunami_parallel_b0_release	1.416585	1.42134	1.41011	1.41396	1.42332	1.42603	1.4154	1.4147	1.41262	1.41217	1.4162
sierpi_intel_omp_tsunami_parallel_b0_release	1.484454	1.47793	1.4986	1.47982	1.47952	1.4798	1.48741	1.49142	1.48319	1.48213	1.48472
sierpi_intel_tbb_tsunami_parallel_b0_release	1.225459	1.22032	1.22325	1.22261	1.22199	1.22869	1.22451	1.219	1.22188	1.22179	1.25055
