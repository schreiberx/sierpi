/*
 * CGlNormalizeByGreenChannel.hpp
 *
 *  Created on: Sep 7, 2011
 *      Author: schreibm
 */

#ifndef CGLNORMALIZEBYALPHACHANNEL_HPP_
#define CGLNORMALIZEBYALPHACHANNEL_HPP_

#include "libgl/incgl3.h"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CGlSlMath.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlProgram.hpp"
#include "libgl/core/CGlViewport.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "libgl/core/CGlBuffer.hpp"
#include "libgl/core/CGlVertexArrayObject.hpp"
#include "lib/CError.hpp"

#include "libgl/core/CGlFbo.hpp"

class CGlNormalizeByGreenChannel
{
	/**
	 * helper stuff
	 */
	CGlDrawTexturedQuad cGlDrawTexturedQuad;

	/*
	 * variables related to rendering splats
	 */
	/// FBO to render the splats to a texture
	CGlFbo cGlFbo;

	/// viewport for FBO
	CGlViewport cGlViewport;

	/// program for rendering splats
	CGlProgram cGlProgram;


	CGlBuffer buffer;
	CGlVertexArrayObject vao;


public:
	CError error;

	/**
	 * texture storing the rendered splats
	 */
	CGlTexture cGlTexture;



	CGlNormalizeByGreenChannel()
	{
		setupTexture(16, 16);

		cGlProgram.initVertFragShadersFromDirectory("normalize_by_green_channel");
		CError_AppendReturn(cGlProgram);

		cGlProgram.link();
		if (cGlProgram.error())
		{
			error << "info Log: linking: " << cGlProgram.error.getString() << std::endl;
			return;
		}

		cGlFbo.bind();
		cGlFbo.bindTexture(cGlTexture);
		cGlFbo.unbind();

		CGlErrorCheck();

		vao.bind();
			buffer.bind();

			GLfloat vertices[4][4] = {
						{-1.0, -1.0, 0.0, 1.0},
						{ 1.0, -1.0, 0.0, 1.0},
						{-1.0,  1.0, 0.0, 1.0},
						{ 1.0,  1.0, 0.0, 1.0},
					};


			buffer.resize(sizeof(vertices));
			buffer.subData(0, sizeof(vertices), vertices);

			// index, size, type, normalized, stride, pointer
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);

		vao.unbind();
	}


	/**
	 * resize the texture for normalization output
	 */
	void resizeTexture(GLsizei p_width, GLsizei p_height)
	{
		cGlTexture.bind();
		cGlTexture.resize(p_width, p_height);
		cGlTexture.unbind();
	}


	/**
	 * setup the texture size
	 */
	void setupTexture(
			GLint p_width,
			GLint p_height
	)
	{
		cGlTexture.bind();
		cGlTexture.setTextureParameters(GL_TEXTURE_2D, GL_R16F, GL_RGBA, GL_FLOAT);
		cGlTexture.resize(p_width, p_height);
		cGlTexture.unbind();

		CGlErrorCheck();
	}


	inline void normalizeByGreenChannel(
			CGlTexture &p_cGlTexture		///< texture to be normalized
	)
	{
		cGlFbo.bind();

			cGlViewport.saveState();
			cGlViewport.setSize(cGlTexture.width, cGlTexture.height);

				cGlProgram.use();

				vao.bind();

					glDisable(GL_CULL_FACE);
					glDisable(GL_DEPTH_TEST);

						p_cGlTexture.bind();

						glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

					glEnable(GL_DEPTH_TEST);
					glEnable(GL_CULL_FACE);

				vao.unbind();
				cGlProgram.disable();

			cGlViewport.restoreState();

		cGlFbo.unbind();

		CGlErrorCheck();
	}

};

#endif /* CGLNORMALIZEBYALPHACHANNEL_HPP_ */
