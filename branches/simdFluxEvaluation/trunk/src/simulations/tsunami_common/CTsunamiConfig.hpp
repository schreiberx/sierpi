/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CTSUNAMI_CONFIG_HPP
#define CTSUNAMI_CONFIG_HPP

/**
 * configuration for tsunami simulation
 */


/**
 * floating point type which should be used for element computations
 */
typedef CONFIG_DEFAULT_FLOATING_POINT_PRECISION		TTsunamiDataScalar;

/**
 * floating point type which should be used for vertex based computations
 */
typedef CONFIG_DEFAULT_FLOATING_POINT_PRECISION		TTsunamiVertexScalar;


/**
 * set the order of the basis function
 */
#ifndef SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS
	// default: 0 (finite volumes)
	#define SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS		0
#endif


/**
 * flux solver to use
 *
 * 0: lax friedrich with constant numerical friction
 * 1: lax friedrich
 * 2: fwave
 * 3: augumented riemann
 * 4: hybrid
 * 5: augumented riemann (fortrancode from geoclaw)
 */

#ifndef SIMULATION_TSUNAMI_FLUX_SOLVER
	// default: 4 (hybrid)
	#define SIMULATION_TSUNAMI_FLUX_SOLVER	4
#endif

#if SIMULATION_TSUNAMI_FLUX_SOLVER>1 && SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
//	#error "fwave and 1st order not yet possible"
#endif

#if SIMULATION_TSUNAMI_FLUX_SOLVER<2 && SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
//	#error "hybrid solver necessary for implemented 0th order simulation"
#endif



/**
 * CFL factor
 */
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

	#if SIMULATION_TSUNAMI_FLUX_SOLVER== 5
		#define SIMULATION_TSUNAMI_CFL	0.45
	#else
		#define SIMULATION_TSUNAMI_CFL	0.3
	#endif

#else

	#define SIMULATION_TSUNAMI_CFL	0.15

#endif



/**
 * RungeKutta
 *
 * use 2nd order by default
 */
#ifndef SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER
	#define SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER 2
#endif

/**
 * store refine and/or coarsen flags in cellData.
 *
 * this becomes handy when forcing refinements for a cell.
 */
#define SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA	0


/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * AUTOMATICALLY SET PRECOMPILER DEFINES - do not modify anything below this line
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */


/**
 * constant friction coefficient for lax friedrich solver with constant numerical friction
 */
#if SIMULATION_TSUNAMI_FLUX_SOLVER == 0
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_LAX_FRIEDRICH_CONST_FRICTION_COEFF	1000.0
#endif


#if SIMULATION_TSUNAMI_FLUX_SOLVER==0
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"lax friedrich with constant numerical friction"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==1
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"local lax friedrich"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==2
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"fwave"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==3
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"augumented riemann"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==4
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"hybrid"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==5
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"augumented riemann (geoclaw)"
#endif
#if SIMULATION_TSUNAMI_FLUX_SOLVER==6
	#define SIMULATION_TSUNAMI_FLUX_SOLVER_STRING	"velocity upwinding"
#endif



#endif
