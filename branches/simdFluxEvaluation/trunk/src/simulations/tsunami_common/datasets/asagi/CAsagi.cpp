/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */



#include "CAsagi.hpp"
#include <cassert>


CAsagi::CAsagi()	:
	is_dataset_loaded(false)
{
}


bool CAsagi::loadDataset(
		const char *i_filename_bathymetry,
		const char *i_filename_displacements
)
{
	is_dataset_loaded = false;

	/*
	 * BATHYMETRY
	 */
	bathymetry_grid = asagi::Grid::create(asagi::Grid::FLOAT);

	if (bathymetry_grid->open(i_filename_bathymetry) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open bathymetry file '" << i_filename_bathymetry << std::endl;
		exit(-1);
	}

	bathymetry_min_x = bathymetry_grid->getXMin();
	bathymetry_min_y = bathymetry_grid->getYMin();

	bathymetry_max_x = bathymetry_grid->getXMax();
	bathymetry_max_y = bathymetry_grid->getYMax();

	bathymetry_size_x = bathymetry_max_x - bathymetry_min_x;
	bathymetry_size_y = bathymetry_max_y - bathymetry_min_y;

	/*
	 * domain size reduced to quad
	 */
	double min_size = CMath::min(bathymetry_size_x, bathymetry_size_y);

	double padding = min_size*0.001;
	double new_size = min_size - 2.0*padding;	// apply padding on both sides

	bathymetry_size_x = new_size;
	bathymetry_size_y = new_size;

	bathymetry_min_x += padding;
	bathymetry_min_y += padding;

	bathymetry_max_x = bathymetry_min_x + bathymetry_size_x;
	bathymetry_max_y = bathymetry_min_y + bathymetry_size_y;

	bathymetry_center_x = ((T)0.5)*(bathymetry_min_x+bathymetry_max_x);
	bathymetry_center_y = ((T)0.5)*(bathymetry_min_y+bathymetry_max_y);

	/**
	 * DISPLACEMENTS
	 */
	displacements_grid = asagi::Grid::create(asagi::Grid::FLOAT);

	if (displacements_grid->open(i_filename_displacements) != asagi::Grid::SUCCESS)
	{
		std::cerr << "NO ASAGI :-( !" << std::endl;
		std::cerr << "Failed to open file '" << i_filename_displacements << std::endl;
		is_dataset_loaded = false;
		return false;
	}

	displacements_min_x = displacements_grid->getXMin();
	displacements_min_y = displacements_grid->getYMin();

	displacements_max_x = displacements_grid->getXMax();
	displacements_max_y = displacements_grid->getYMax();

	displacements_size_x = displacements_max_x - displacements_min_x;
	displacements_size_y = displacements_max_y - displacements_min_y;

	displacements_center_x = ((T)0.5)*(displacements_min_x+displacements_max_x);
	displacements_center_y = ((T)0.5)*(displacements_min_y+displacements_max_y);

	is_dataset_loaded = true;
	return true;
}



CAsagi::~CAsagi()
{
	delete bathymetry_grid;
	delete displacements_grid;
}


void CAsagi::outputVerboseInformation()
{
	std::cout << "ASAGI:" << std::endl;
	std::cout << " + Bathymetry data-window: (" << bathymetry_grid->getXMin() << ", " << bathymetry_grid->getYMin() << 	") x (" << bathymetry_grid->getXMax() << ", " << bathymetry_grid->getYMax() << ")" << std::endl;
	std::cout << " + Bathymetry data-size: (" << (bathymetry_grid->getXMax() - bathymetry_grid->getXMin()) << ", " 	<< (bathymetry_grid->getYMax() - bathymetry_grid->getYMin())<< ")" << std::endl;

	std::cout << " + Bathymetry start: (" << bathymetry_min_x << ", " << bathymetry_min_y << ")" << std::endl;
	std::cout << " + Bathymetry size: (" << bathymetry_size_x << ", " << bathymetry_size_y << ")" << std::endl;

	std::cout << " + Displacement window: (" << displacements_grid->getXMin() << ", " << displacements_grid->getYMin() << 	") x (" << displacements_grid->getXMax() << ", " << displacements_grid->getYMax() << ")" << std::endl;
	std::cout << " + Displacement size: (" << (displacements_grid->getXMax() - displacements_grid->getXMin()) << ", " 	<< (displacements_grid->getYMax() - displacements_grid->getYMin())<< ")" << std::endl;
}


void CAsagi::getOriginAndSize(
		TTsunamiDataScalar	*o_origin_x,
		TTsunamiDataScalar	*o_origin_y,
		TTsunamiDataScalar	*o_size_x,
		TTsunamiDataScalar	*o_size_y
)
{
	*o_origin_x = bathymetry_min_x;
	*o_origin_y = bathymetry_min_y;

	*o_size_x = bathymetry_size_x;
	*o_size_y = bathymetry_size_y;
}




TTsunamiDataScalar CAsagi::getDisplacementData(
		TTsunamiDataScalar i_x,		///< x-coordinate in world-space
		TTsunamiDataScalar i_y,		///< x-coordinate in world-space
		int i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	i_x = i_x*bathymetry_size_x + bathymetry_center_x;
	i_y = i_y*bathymetry_size_y + bathymetry_center_y;

	return getDisplacementDataWorldSpace(i_x, i_y, i_level_of_detail);
}


TTsunamiDataScalar CAsagi::getDisplacementDataWorldSpace(
		TTsunamiDataScalar i_x,		///< x-coordinate in world-space
		TTsunamiDataScalar i_y,		///< x-coordinate in world-space
		int i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	i_level_of_detail = 0;
	if (i_x < displacements_min_x)
		return 0;

	if (i_x > displacements_max_x)
		return 0;

	if (i_y < displacements_min_y)
		return 0;

	if (i_y > displacements_max_y)
		return 0;

	return displacements_grid->getFloat2D(i_x, i_y, i_level_of_detail);
}



/**
 * get nodal data for given coordinate
 */
void CAsagi::getNodalData(
		TTsunamiDataScalar i_x,
		TTsunamiDataScalar i_y,
		int i_level_of_detail,
		CTsunamiSimulationDOFs *o_nodal_data
)
{
	i_x = i_x*bathymetry_size_x + bathymetry_center_x;
	i_y = i_y*bathymetry_size_y + bathymetry_center_y;

	i_level_of_detail = 0;

	o_nodal_data->b = bathymetry_grid->getFloat2D(i_x, i_y, i_level_of_detail);
	o_nodal_data->qx = 0;
	o_nodal_data->qy = 0;

	o_nodal_data->h = -o_nodal_data->b + getDisplacementDataWorldSpace(i_x, i_y, i_level_of_detail);
}


/**
 * get nodal data for given coordinate
 */
TTsunamiDataScalar CAsagi::getBathymetryData(
		TTsunamiDataScalar i_x,		///< x-coordinate in model-space
		TTsunamiDataScalar i_y,		///< x-coordinate in model-space
		int i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	i_x = i_x*bathymetry_size_x + bathymetry_center_x;
	i_y = i_y*bathymetry_size_y + bathymetry_center_y;

	i_level_of_detail = 0;

	return bathymetry_grid->getFloat2D(i_x, i_y, i_level_of_detail) + getDisplacementDataWorldSpace(i_x, i_y, i_level_of_detail);
}



/**
 * get nodal data for given coordinate
 */
TTsunamiDataScalar CAsagi::getWaterSufaceData(
		TTsunamiDataScalar i_x,		///< x-coordinate in model-space
		TTsunamiDataScalar i_y,		///< x-coordinate in model-space
		int i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	i_level_of_detail = 0;
	return getDisplacementData(i_x, i_y, i_level_of_detail);
}



/**
 * get boundary data at given position
 */
void CAsagi::getBoundaryData(
		TTsunamiDataScalar i_x,
		TTsunamiDataScalar i_y,
		int i_level_of_detail,
		CTsunamiSimulationDOFs *o_nodal_data
)
{
	std::cerr << "NOT IMPLEMENTED YET" << std::endl;
	assert(false);
}



bool CAsagi::isDatasetLoaded()
{
	return is_dataset_loaded;
}
