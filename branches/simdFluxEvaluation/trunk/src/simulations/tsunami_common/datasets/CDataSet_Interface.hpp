/*
 * CDataSet.hpp
 *
 *  Created on: Jul 3, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDATASET_INTERFACE_HPP_
#define CDATASET_INTERFACE_HPP_

#include "../CTsunamiConfig.hpp"
#include "../types/CTsunamiTypes.hpp"

/**
 * The following interfaces have to be provided by the different data sets
 */
class CDataSet_Interface
{
public:
	virtual ~CDataSet_Interface()	{}

	/**
	 * load the specified bathymetry and displacement datasets
	 */
	virtual bool loadDataset(
			const char *i_filename_bathymetry,		///< filename for bathymetry data
			const char *i_filename_displacements	///< filename for displacement data
	) = 0;


	/**
	 * return true if the dataset was successfully loaded
	 */
	virtual bool isDatasetLoaded() = 0;


	/**
	 * load terrain origin coordinate and size
	 */
	virtual void getOriginAndSize(
			TTsunamiDataScalar	*o_origin_x,	///< origin of domain in world-space
			TTsunamiDataScalar	*o_origin_y,	///< origin of domain in world-space
			TTsunamiDataScalar	*o_size_x,		///< size of domain in world-space
			TTsunamiDataScalar	*o_size_y		///< size of domain in world-space
	) = 0;


	/**
	 * get nodal data (surface height, momentums and bathymetry) for given coordinate
	 */
	virtual void getNodalData(
			TTsunamiDataScalar i_x,		///< x-coordinate in world-space
			TTsunamiDataScalar i_y,		///< x-coordinate in world-space
			int i_level_of_detail,		///< level of detail (0 = coarsest level)
			CTsunamiSimulationDOFs *o_nodal_data
	) = 0;


	/**
	 * get bathymetry height for given coordinate
	 */
	virtual TTsunamiDataScalar getBathymetryData(
			TTsunamiDataScalar i_x,		///< x-coordinate in world-space
			TTsunamiDataScalar i_y,		///< x-coordinate in world-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	) = 0;


	/**
	 * get water surface height for given coordinate
	 */
	virtual TTsunamiDataScalar getWaterSufaceData(
			TTsunamiDataScalar i_x,		///< x-coordinate in world-space
			TTsunamiDataScalar i_y,		///< x-coordinate in world-space
			int i_level_of_detail		///< level of detail (0 = coarsest level)
	) = 0;


	/**
	 * get boundary data at given position
	 */
	virtual void getBoundaryData(
			TTsunamiDataScalar i_x,
			TTsunamiDataScalar i_y,
			int i_level_of_detail,
			CTsunamiSimulationDOFs *o_nodal_data
	) = 0;


	/**
	 * get benchmark (analytical) nodal data
	 */
	virtual void getBenchmarkNodalData(
			TTsunamiDataScalar i_x,
			TTsunamiDataScalar i_y,
			int i_level_of_detail,
			TTsunamiDataScalar i_timestamp,
			CTsunamiSimulationDOFs *o_nodal_data
	) = 0;


	/**
	 * output some verbose information about this dataset
	 */
	virtual void outputVerboseInformation() = 0;
};


#endif /* CDATASET_HPP_ */
