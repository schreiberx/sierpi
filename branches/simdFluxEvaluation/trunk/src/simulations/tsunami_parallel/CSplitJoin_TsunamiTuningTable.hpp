/*
 * CSplitJoinTsunamiTuning.hpp
 *
 *  Created on: Jan 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSPLITJOIN_TSUNAMITUNINGTABLE_HPP_
#define CSPLITJOIN_TSUNAMITUNINGTABLE_HPP_


/**
 * tuning table for split/join parameters
 */
class CSplitJoin_TsunamiTuningTable
{
public:
	/**
	 * size of tunign table
	 */
	int tuning_table_size;

	/**
	 * allocation of tuning table
	 */
	unsigned long long tuning_table[15][2];

	/**
	 * constructor with initialization
	 */
	CSplitJoin_TsunamiTuningTable()
	{
		tuning_table[0][0] = 2048;		tuning_table[0][1] = 512;
		tuning_table[1][0] = 4096;		tuning_table[1][1] = 1024;
		tuning_table[2][0] = 8192;		tuning_table[2][1] = 1024;
		tuning_table[3][0] = 16384;		tuning_table[3][1] = 1024;
		tuning_table[4][0] = 32768;		tuning_table[4][1] = 2048;

		tuning_table[5][0] = 65536;		tuning_table[5][1] = 2048;
		tuning_table[6][0] = 131072;	tuning_table[6][1] = 2048;
		tuning_table[7][0] = 262144;	tuning_table[7][1] = 4096;
		tuning_table[8][0] = 524288;	tuning_table[8][1] = 4096;
		tuning_table[9][0] = 1048576;	tuning_table[9][1] = 4096;

		tuning_table[10][0] = 2097152;	tuning_table[10][1] = 8192;
		tuning_table[11][0] = 4194304;	tuning_table[11][1] = 8192;
		tuning_table[12][0] = 8388608;	tuning_table[12][1] = 16384;
		tuning_table[13][0] = 16777216;	tuning_table[13][1] = 16384;
		tuning_table[14][0] = 33554432;	tuning_table[14][1] = 32768;

		tuning_table_size = sizeof(tuning_table)/(sizeof(unsigned long long)*2);
	}
};




#endif
