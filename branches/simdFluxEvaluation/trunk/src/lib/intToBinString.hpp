#ifndef CINTTOBINSTRING_HPP
#define CINTTOBINSTRING_HPP

#include <string>


std::string intToBinString(unsigned int i)
{
	if (i == 0)
		return std::string("0b0");

	std::string s;
	for (; i > 0; i>>=1)
		s = (i&1 ? '1' : '0')+s;
	s = "0b"+s;
	return s;
}

#endif
