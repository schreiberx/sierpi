/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CCluster_EdgeComm_InformationAdjacentClusters.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTERS_HPP_
#define CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTERS_HPP_

#include <vector>
#include "CCluster_EdgeComm_InformationAdjacentCluster.hpp"
#include "CCluster_TreeNode.hpp"
#include "CCluster_UniqueId.hpp"


namespace sierpi
{

/**
 * informations about adjacent clusters encoded in a RLE format
 */
template <typename t_CCluster_TreeNode>
class CCluster_EdgeComm_InformationAdjacentClusters
{
	typedef CCluster_EdgeComm_InformationAdjacentCluster<t_CCluster_TreeNode> CEdgeComm_InformationAdjacentCluster_;

public:
	/***********************************************************************
	 * HYPOTENUSE
	 ***********************************************************************/

	/**
	 * current edge comm information of clusters aligned at the hypotenuse.
	 */
	std::vector<CEdgeComm_InformationAdjacentCluster_> hyp_adjacent_clusters;

	/**
	 * additional storage for edge comm information for clusters adjacent to the
	 * hypotenuse.
	 *
	 * this buffer is used to avoid race conditions when new items are inserted.
	 */
	std::vector<CEdgeComm_InformationAdjacentCluster_> new_hyp_adjacent_clusters;

	/**
	 * this variable is set to true whenever the information about the adjacent clusters
	 * was modified and thus new_hyp_adjacent_clusters is the storage of the new informations.
	 */
	bool hyp_swapOrNotToBeSwapped;

	/***********************************************************************
	 * CATHETI
	 ***********************************************************************/

	/**
	 * edge comm information of both catheti
	 */
	std::vector<CEdgeComm_InformationAdjacentCluster_> cat_adjacent_clusters;

	/**
	 * updated edge of catheti to avoid race conditions when new items are inserted
	 */
	std::vector<CEdgeComm_InformationAdjacentCluster_> new_cat_adjacent_clusters;

	/**
	 * swap catheti edge comm information
	 */
	bool cat_swapOrNotToBeSwapped;



	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentClusters()	:
		hyp_swapOrNotToBeSwapped(false),
		cat_swapOrNotToBeSwapped(false)
	{
		// preallocate larger vector to avoid resizing
		static const int CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE = 32;

		hyp_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
		cat_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);

		new_hyp_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
		new_cat_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
	}



	/**
	 * After the edge communication was updated, maybe the edge information stacks have to be swapped.
	 *
	 * !!! This method has to be called after _all_ edge comms are finished with their updates !!!
	 */
	inline void swapAndCleanAfterUpdatingEdgeComm()
	{
		if (hyp_swapOrNotToBeSwapped)
		{
			// TODO: this swap operation is done on the std::vector instead of it's pointer

			hyp_adjacent_clusters.swap(new_hyp_adjacent_clusters);
			// clear new stacks to prepare them for further update edge communication calls
			new_hyp_adjacent_clusters.clear();
			hyp_swapOrNotToBeSwapped = false;
		}

		if (cat_swapOrNotToBeSwapped)
		{
			cat_adjacent_clusters.swap(new_cat_adjacent_clusters);
			// clear new stacks to prepare them for further update edge communication calls
			new_cat_adjacent_clusters.clear();
			cat_swapOrNotToBeSwapped = false;
		}
	}


	/**
	 * return the number of edge communicatino elements for the catheti
	 */
	inline unsigned int getSumOfEdgeCommElementsOnCatheti()
	{
		unsigned int sum = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cat_adjacent_clusters.begin();
				iter != cat_adjacent_clusters.end();
				iter++
		)
		{
			sum += (*iter).comm_elements;
		}

		return sum;
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, CCluster_EdgeComm_InformationAdjacentClusters &m)
	{
		co << " Adjacent information: " << std::endl;

		co << " + adj information hyp:" << std::endl;

		if (m.hyp_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = m.new_hyp_adjacent_clusters.begin();
					iter != m.new_hyp_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = m.hyp_adjacent_clusters.begin();
					iter != m.hyp_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}

		}
		co << std::endl;


		co << " + adj information cat:" << std::endl;

		if (m.cat_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = m.new_cat_adjacent_clusters.begin();
					iter != m.new_cat_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = m.cat_adjacent_clusters.begin();
					iter != m.cat_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		co << std::endl;

		return co;
	}
};

}

#endif /* CADJACENTPARTITIONS_H_ */
