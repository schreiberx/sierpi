/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGTBB_HPP_
#define CMAINTHREADINGTBB_HPP_

#include <tbb/task_scheduler_init.h>
#include <iostream>
#include <stdlib.h>
#include <cassert>

#include "CMainThreading_Interface.hpp"


bool CMainThreadingTBB_setAffinities(
		int i_num_threads,
		int i_max_cores,
		int i_distance,
		int i_start_id,
		int i_verbose_level
);


/**
 * Threading support for TBB
 */
class CMainThreading	:
	public CMainThreading_Interface
{
    tbb::task_scheduler_init *tbb_task_scheduler_init;

public:
	void threading_setup()
	{
		/**
		 * assume that default_num_threads also returns the maximum number of threads and thus logical cores
		 */
		if (getMaxNumberOfThreads() == -1)
			setMaxNumberOfThreads(tbb::task_scheduler_init::default_num_threads());

		if (getMaxNumberOfThreads() == -1)
		{
			std::cout << "Unable to determine maximum number of threads!" << std::endl;
			exit(-1);
		}

		// setup scheduler
		if (getNumberOfThreadsToUse() == -1)
			setValueNumberOfThreadsToUse(getMaxNumberOfThreads());

		if (getMaxNumberOfThreads() < getNumberOfThreadsToUse())
		{
			std::cerr << "ASSERT[ max_number_of_threads(" << getMaxNumberOfThreads() << ") > num_threads (" << getNumberOfThreadsToUse() << ") ] failed" << std::endl;
			exit(-1);
		}

		tbb_task_scheduler_init = new tbb::task_scheduler_init(getNumberOfThreadsToUse());

//		if (getVerboseLevel() >= 5)
//			std::cout << "tbb::task_scheduler_init::default_num_threads(): " << getMaxNumberOfThreads() << std::endl;

		if (getThreadAffinityPadding() < 0)
			return;

		if (getVerboseLevel() > 0)
			std::cerr << "WARNING: Setting Affinities in TBB is experimental since additional threads are sometimes created" << std::endl;

		CMainThreadingTBB_setAffinities(
				getNumberOfThreadsToUse(),
				getMaxNumberOfThreads(),
				getThreadAffinityPadding(),
				getThreadAffinityStartId(),
				getVerboseLevel()
			);

	}


	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulation_loopIteration();

		} while(continue_simulation);
	}


	bool threading_simulationLoopIteration()
	{
		return simulation_loopIteration();
	}


	void threading_shutdown()
	{
		if (tbb_task_scheduler_init)
		{
			delete tbb_task_scheduler_init;
			tbb_task_scheduler_init = nullptr;
		}
	}


	void threading_setNumThreads(int i)
	{
		delete tbb_task_scheduler_init;

		setValueNumberOfThreadsToUse(i);

		tbb_task_scheduler_init = new tbb::task_scheduler_init(i);
	}


	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGTBB_HPP_ */
