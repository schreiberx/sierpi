/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreading_MutexOMP.hpp
 *
 *  Created on: Jul 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADING_MUTEX_OMP_HPP_
#define CMAINTHREADING_LOCK_OMP_HPP_

#include <omp.h>
#include "CMainThreading_LockInterface.hpp"

/**
 * Threading support for invasive OpenMP
 */
class CMainThreading_Lock	:
	public CMainThreading_LockInterface
{
	omp_lock_t omp_lock;

public:
	CMainThreading_Lock()
	{
		omp_init_lock(&omp_lock);
	}

	void lock()
	{
		omp_set_lock(&omp_lock);
	}

	void unlock()
	{
		omp_unset_lock(&omp_lock);
	}

	~CMainThreading_Lock()
	{
		omp_destroy_lock(&omp_lock);
	}
};


#endif /* CMAINTHREADING_LOCK_OMP_HPP_ */
