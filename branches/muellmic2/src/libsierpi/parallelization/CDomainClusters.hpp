/*
 * CDomainClusters.h
 *
 *  Created on: Jul 4, 2012
 *      Author: schreibm
 */

#ifndef CSIERPI_DOMAINCLUSTERS_HPP_
#define CSIERPI_DOMAINCLUSTERS_HPP_

#include "generic_tree/CGenericTreeNode.hpp"
#include "CCluster_TreeNode.hpp"


namespace sierpi
{

/**
 * provide better naming for root generic tree node element
 */
template <typename t_CSimulation_Cluster>
class CDomainClusters	:
	public CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >
{
public:
	CDomainClusters()	:
		CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >::CGenericTreeNode(nullptr, true)
	{
	}


	~CDomainClusters()
	{
	}

	/**
	 * return reference to generic tree node
	 */
	CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >& getGenericTreeNodeRef()
	{
		return (CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >&)(*this);
	}

	/**
	 * return pointer to generic tree node
	 */
	CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >* getGenericTreeNodePtr()
	{
		return &((CGenericTreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >&)(*this));
	}
};

}

#endif /* CDOMAINCLUSTERS_H_ */
