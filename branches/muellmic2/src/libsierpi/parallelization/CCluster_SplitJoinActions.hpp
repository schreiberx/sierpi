/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CCluster_SplitJoinAction.hpp
 *
 *  Created on: Jul 4, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_SPLITJOINACTIONS_HPP_
#define CCLUSTER_SPLITJOINACTIONS_HPP_

#include "config.h"
#include "generic_tree/CGenericTreeNode.hpp"


namespace sierpi
{

template <typename t_CSimulation_Cluster> class CCluster_TreeNode;


/**
 * class to store the enumerator for the transfer states
 */
class CCluster_SplitJoinActions_Enums
{
public:
	/**
	 * these states account for the currently running transformation.
	 *
	 * after each timestep, the state of each sub-cluster has to be NO_TRANSFER
	 */
	enum EPARTITION_TRANSFER_STATE
	{
		NO_TRANSFER = 0,
		SPLITTED_PARENT = 1,
		SPLITTED_CHILD = 2,
		JOINED_PARENT = 3,
		JOINED_CHILD = 4
	};
};



/**
 * \brief split and join actions which have to be executed in a particular order
 *
 * this class offers methods to split and join cluster tree nodes.
 */
template <typename t_CSimulation_Cluster>
class CCluster_SplitJoinActions	: public CCluster_SplitJoinActions_Enums
{
	typedef CCluster_TreeNode<t_CSimulation_Cluster> CCluster_TreeNode_;
	typedef CGenericTreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CCluster_EdgeComm_InformationAdjacentCluster_;
	typedef CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> CCluster_EdgeComm_InformationAdjacentClusters_;

	typedef typename t_CSimulation_Cluster::CCellData CCellData_;


	/**
	 * triangle factory
	 */
	CTriangle_Factory &triangleFactory;

	/**
	 * reference to cluster tree node
	 */
	CCluster_TreeNode_ &cCluster_TreeNode;

	/**
	 * split/join information which should be used for data exchange
	 */
	CCluster_SplitJoin_EdgeComm_Information &cCluster_SplitJoinInformation;

	/**
	 * this is the point where the adjacency information to other paritions is stored
	 */
	CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> &cCluster_EdgeComm_InformationAdjacentClusters;

	/**
	 * cluster's unique ID
	 */
	CCluster_UniqueId &uniqueId;

	/**
	 * triangle child factories already set-up
	 */
	bool triangleChildrenFactorySetupDone;


public:
	/**
	 * one of the most important parts: the transfer state describes the current
	 * state of the cluster tree node to run further actions during later processing.
	 */
	EPARTITION_TRANSFER_STATE transferState;


public:
	/**
	 * Constructor
	 */
	CCluster_SplitJoinActions(
			CCluster_TreeNode_ &p_cClusterTreeNode	///< handler to cluster tree node (e. g. to access stacks)
	)	:
		triangleFactory(p_cClusterTreeNode.cTriangleFactory),
		cCluster_TreeNode(p_cClusterTreeNode),
		cCluster_SplitJoinInformation(p_cClusterTreeNode.cCluster_SplitJoinInformation),
		cCluster_EdgeComm_InformationAdjacentClusters(p_cClusterTreeNode.cCluster_EdgeComm_InformationAdjacentClusters),
		uniqueId(p_cClusterTreeNode.uniqueId),

		triangleChildrenFactorySetupDone(false),
		transferState(NO_TRANSFER)
	{
	}



	/**
	 * split this sub-cluster.
	 */
public:
	void pass1_splitCluster()
	{
		CGeneric_TreeNode_ *cGeneric_TreeNode = cCluster_TreeNode.cGeneric_TreeNode;

		assert(transferState == NO_TRANSFER);
		assert(cCluster_SplitJoinInformation.splitting_permitted);
		assert(cGeneric_TreeNode->isLeaf());


		/*
		 * STEP 1)
		 *
		 * allocate & create new child nodes
		 */

		if (!triangleChildrenFactorySetupDone)
		{
			triangleFactory.setupChildFactories(
					cCluster_SplitJoinInformation.first_triangle.triangleFactory,
					cCluster_SplitJoinInformation.second_triangle.triangleFactory
				);
		}

		/**
		 * override type to reduce possible recursive methods without changing any traversals
		 */
		cCluster_SplitJoinInformation.first_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;
		cCluster_SplitJoinInformation.second_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		// mark triangle child setup to be finished
		triangleChildrenFactorySetupDone = true;


		// setup children

		// FIRST child
		CGeneric_TreeNode_ *firstGenericTreeNode = new CGeneric_TreeNode_(cCluster_TreeNode.cGeneric_TreeNode, false);
		CCluster_TreeNode_ *firstClusterTreeNode = new CCluster_TreeNode_(cCluster_SplitJoinInformation.first_triangle.triangleFactory);

		firstClusterTreeNode->cGeneric_TreeNode = firstGenericTreeNode;
		firstGenericTreeNode->cCluster_TreeNode = firstClusterTreeNode;

		cCluster_TreeNode.cGeneric_TreeNode->first_child_node = firstGenericTreeNode;
		firstClusterTreeNode->uniqueId.setupFirstChildFromParent(uniqueId);
		firstClusterTreeNode->cCluster_SplitJoinActions.transferState = SPLITTED_CHILD;


		// SECOND child
		CGeneric_TreeNode_ *secondGenericTreeNode = new CGeneric_TreeNode_(cCluster_TreeNode.cGeneric_TreeNode, false);
		CCluster_TreeNode_ *secondClusterTreeNode = new CCluster_TreeNode_(cCluster_SplitJoinInformation.second_triangle.triangleFactory);

		secondClusterTreeNode->cGeneric_TreeNode = secondGenericTreeNode;
		secondGenericTreeNode->cCluster_TreeNode = secondClusterTreeNode;

		cCluster_TreeNode.cGeneric_TreeNode->second_child_node = secondGenericTreeNode;
		secondClusterTreeNode->uniqueId.setupSecondChildFromParent(uniqueId);
		secondClusterTreeNode->cCluster_SplitJoinActions.transferState = SPLITTED_CHILD;


		/*
		 * STEP 2)
		 *
		 * setup new simulation cluster handlers
		 */

		/*
		 * FIRST CHILD
		 */
		// setup simulation handler
		firstClusterTreeNode->setup_SimulationClusterHandler(
				CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS,
				cCluster_TreeNode.cCluster
			);

		assert(cCluster_SplitJoinInformation.first_triangle.number_of_elements != -1);

		// setup stacks
		firstClusterTreeNode->setup_SplittedChildrenStacks(
				cCluster_SplitJoinInformation,		/// split information
				false
		);

		firstGenericTreeNode->workload_in_subtree = cCluster_SplitJoinInformation.first_triangle.number_of_elements;

#if CONFIG_ENABLE_SCAN_DATA
		firstGenericTreeNode->workload_scan_start_index = cGeneric_TreeNode->workload_scan_start_index;
		firstGenericTreeNode->workload_scan_end_index = cGeneric_TreeNode->workload_scan_start_index + firstGenericTreeNode->workload_in_subtree;
#endif
		/*
		 * SECOND CHILD
		 */
		/*
		 * reuse the stacks of this node since they are of no further use.
		 * this is done by handing over the current cCluster as a parameter.
		 */
		secondClusterTreeNode->setup_SimulationClusterHandler(
				CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS,
				cCluster_TreeNode.cCluster
			);

		assert(cCluster_SplitJoinInformation.second_triangle.number_of_elements != -1);

		// setup stacks
		secondClusterTreeNode->setup_SplittedChildrenStacks(
				cCluster_SplitJoinInformation,		/// split information
				CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		);

		secondGenericTreeNode->workload_in_subtree = cCluster_SplitJoinInformation.second_triangle.number_of_elements;

#if CONFIG_ENABLE_SCAN_DATA
		secondGenericTreeNode->workload_scan_start_index = firstGenericTreeNode->workload_scan_end_index;
		secondGenericTreeNode->workload_scan_end_index = secondGenericTreeNode->workload_scan_start_index + secondGenericTreeNode->workload_in_subtree;

		assert(cCluster_TreeNode.cGeneric_TreeNode->workload_in_subtree == firstGenericTreeNode->workload_in_subtree + secondGenericTreeNode->workload_in_subtree);
#endif

		/*
		 * STEP 3)
		 */
		/*
		 * SETUP ADJACENT EDGE COMMUNICATION
		 *
		 * since the edge communication data is setup during the initialization by the root simulation handler,
		 * this has now to be right here - based on the split/join information and the information about the adjacent clusters
		 */

		_pass1_setupAdjacentClusterInformationAfterSplit(
				cCluster_SplitJoinInformation,
				cCluster_EdgeComm_InformationAdjacentClusters,
				cCluster_TreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode,
				cCluster_TreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode
			);

		// finally set joining and splitting permission to false to avoid any further conflicts
		cCluster_SplitJoinInformation.joining_permitted = false;
		cCluster_SplitJoinInformation.splitting_permitted = false;

#if !CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		cCluster_TreeNode.freeStacks();
#endif

		this->transferState = SPLITTED_PARENT;
	}



	/**
	 * \brief join both child nodes
	 *
	 * this method is declared as static since it is possible that clusters do not exist
	 * when CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION is activated.
	 */
	static void pass1_joinChildClusters(CGeneric_TreeNode_ *cGeneric_TreeNode)
	{
#if !CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		assert(cGeneric_TreeNode->cCluster_TreeNode != nullptr);
		assert(cGeneric_TreeNode->cCluster_TreeNode->cCluster_SplitJoinActions.transferState == NO_TRANSFER);
#endif

		CCluster_TreeNode_* first_child_node = cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
		CCluster_TreeNode_* second_child_node = cGeneric_TreeNode->second_child_node->cCluster_TreeNode;

		assert(first_child_node != NULL);
		assert(second_child_node != NULL);

#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		/*
		 * the cluster tree node is assumed to be deleted
		 */
		assert (cGeneric_TreeNode->cCluster_TreeNode == nullptr);

		CTriangle_Factory parentTriangleFactory;

		parentTriangleFactory.setupParentFromChildFatories(first_child_node->cTriangleFactory, second_child_node->cTriangleFactory);
		cGeneric_TreeNode->cCluster_TreeNode = new CCluster_TreeNode_(parentTriangleFactory);

		CCluster_TreeNode_ &cCluster_TreeNode = *(cGeneric_TreeNode->cCluster_TreeNode);

		cCluster_TreeNode.cGeneric_TreeNode = cGeneric_TreeNode;

		cCluster_TreeNode.setup_SimulationClusterHandler(
				false,
				cGeneric_TreeNode->cCluster_TreeNode->cCluster
			);

		cCluster_TreeNode.uniqueId.setupParentFromChild(first_child_node->uniqueId);

#else
		CCluster_TreeNode_ &cCluster_TreeNode = *(cGeneric_TreeNode->cCluster_TreeNode);
#endif



		CCluster_UniqueId &first_child_unique_id = first_child_node->uniqueId;
		CCluster_UniqueId &second_child_unique_id = second_child_node->uniqueId;

		assert(cGeneric_TreeNode == cCluster_TreeNode.cGeneric_TreeNode);
		assert(&cCluster_TreeNode == cCluster_TreeNode.cGeneric_TreeNode->cCluster_TreeNode);

		assert(first_child_node->cCluster_SplitJoinInformation.joining_permitted);
		assert(second_child_node->cCluster_SplitJoinInformation.joining_permitted);

		assert(first_child_node->cGeneric_TreeNode->isLeaf());
		assert(second_child_node->cGeneric_TreeNode->isLeaf());
		assert(first_child_node->cTriangleFactory.evenOdd == second_child_node->cTriangleFactory.evenOdd);

		CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> &cCluster_EdgeComm_InformationAdjacentClusters = cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters;

		/*
		 * prepare parent's adjacency information
		 */
		cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.clear();
		cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.clear();


		{
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter_first, iter_second;

			size_t number_of_edge_comm_informations = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();
			assert(number_of_edge_comm_informations > 0);

			if (number_of_edge_comm_informations == 1)
			{
				/**
				 * the only edge comm information which exists is shared by both sub-triangles => nothing to do
				 */
			}
			else
			{
				/*************************************
				 * setup new EDGE COMM INFORMATION
				 *************************************/

				/*
				 * FIRST CHILD - CATHETUS
				 */
				iter_first = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();

				/*
				 * iterate to the next-to-last edge comm information
				 */
				for (	size_t i = 0;
						i < number_of_edge_comm_informations-2;
						iter_first++, i++
				)
				{
					CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_first;

					// another way would be simply to drop the last adjacency information
					if (informationAdjCluster.uniqueId == second_child_unique_id)
						continue;

					cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(informationAdjCluster);
				}


				/*
				 * SECOND CHILD - CATHETUS
				 */
				number_of_edge_comm_informations = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();

				if (number_of_edge_comm_informations == 1)
				{
					/*
					 * only a single edge comm information is available which is shared by both child-triangles
					 */
					cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back((*iter_first));
#if DEBUG
					iter_second = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					assert((*iter_second).uniqueId == first_child_node->uniqueId);
					iter_first++;
					assert((*iter_first).uniqueId == second_child_node->uniqueId);
#endif
				}
				else
				{
					/*
					 * check whether it's possible to join the first edge comm information with the last one
					 */

					// setup second iterator
					iter_second = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					assert((*iter_second).uniqueId == first_child_node->uniqueId);
					iter_second++;	// skip first edge comm (shared by)

					/*
					 * join adj information
					 */
					if ((*iter_second).uniqueId == (*iter_first).uniqueId)
					{
						/*
						 * the information about the edge communication data has to be joined since there's only one
						 * adjacent triangle
						 */
						CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_first;

						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(
								CCluster_EdgeComm_InformationAdjacentCluster_(
										informationAdjCluster.clusterTreeNode,
										informationAdjCluster.uniqueId,
										informationAdjCluster.comm_elements+(*iter_second).comm_elements
#if CONFIG_ENABLE_MPI
										,
										informationAdjCluster.mpi_rank
#endif
									)
								);
					}
					else
					{
						/*
						 * in case of a still splitted edge comm dataset, just push the edge comm information
						 */
						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(*iter_first);
						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(*iter_second);
					}

					iter_second++;

					/**
					 * SECOND CHILD - CATHETUS
					 */
					for (	;
							iter_second != second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
							iter_second++
					)
					{
						CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_second;

						if (informationAdjCluster.uniqueId == first_child_unique_id)
							continue;

						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(informationAdjCluster);
					}
				}
			}
		}


		{
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter;

			/**
			 * FIRST CHILD - HYPOTENUSE
			 */
			for (	iter = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					iter != first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					iter++)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter;

				// another way would be simply to drop the last adjacency information
				if (informationAdjCluster.uniqueId == second_child_unique_id)
					continue;

				cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(informationAdjCluster);
			}

			/**
			 * SECOND CHILD - HYPOTENUSE
			 */
			for (	iter = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					iter != second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					iter++)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter;

				if (informationAdjCluster.uniqueId == first_child_unique_id)
					continue;

				cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(informationAdjCluster);
			}
		}


		/***********************************************
		 * Reassemble stacks
		 ***********************************************/

#if !CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS && !CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		assert(cCluster_TreeNode.cStacks == NULL);
#endif

		cCluster_TreeNode.setup_JoinedStacks(
				cCluster_TreeNode.cCluster_SplitJoinInformation,
				CONFIG_SIERPI_PARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS				//< reuse child's stack?
			);

#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		cCluster_TreeNode.cCluster->setup(
				first_child_node->cCluster
			);
#endif

		/**
		 * set child nodes of being joined during the last traversal
		 */
		first_child_node->cCluster_SplitJoinActions.transferState = JOINED_CHILD;
		second_child_node->cCluster_SplitJoinActions.transferState = JOINED_CHILD;


		cCluster_TreeNode.cCluster_SplitJoinActions.transferState = JOINED_PARENT;

		// finally set joining and splitting permission to false to avoid any further conflicts
		cCluster_TreeNode.cCluster_SplitJoinInformation.joining_permitted = false;
		cCluster_TreeNode.cCluster_SplitJoinInformation.splitting_permitted = false;

		cGeneric_TreeNode->setDelayedChildDeletion();
	}



	/**
	 * update the information about the adjacent information after a split operation
	 */
public:
	void _pass1_setupAdjacentClusterInformationAfterSplit(
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation,	///< split/join information
			CCluster_EdgeComm_InformationAdjacentClusters_ &p_parentAdjacentClusters,		///< handler to parent adjacent information
			CCluster_TreeNode_ *p_first_child_node,					///< first child node
			CCluster_TreeNode_ *p_second_child_node					///< second child node
	)
	{
		/**********************
		 * CATHETI
		 *
		 * splitting on the catheti should be quite easy since the information about the adjacent clusters only
		 * has to be split and no adjacent cluster information has to be split up
		 *
		 * EVEN:
		 * |\
		 * |  \
		 * | 2  \
		 * |    / \
		 * |  /  1  \
		 * |/____ ____\
		 *
		 *
		 * ODD:
		 *     /|\
		 *   /2 | 1\
		 * /____|____\
		 *
		 */
		{
			// current child node which is set-up
			CCluster_TreeNode_*current_child_node = p_first_child_node;

			// remaining communication elements on right stack until switching to the second child node
			int counter_remaining_edge_comm_elements;

			if (triangleFactory.isCathetusDataOnLeftStack())
				counter_remaining_edge_comm_elements = p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			else
				counter_remaining_edge_comm_elements = p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

			for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter =
						p_parentAdjacentClusters.cat_adjacent_clusters.begin();
					iter != p_parentAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				/*
				 * switch to second sub triangle if updating the adj information of the first one is finished
				 */
				if (counter_remaining_edge_comm_elements == 0)
				{
					assert(current_child_node != p_second_child_node);

					current_child_node = p_second_child_node;

					if (triangleFactory.evenOdd == CTriangle_Enums::EVEN)
						counter_remaining_edge_comm_elements = p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
					else
						counter_remaining_edge_comm_elements = p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
				}

				// create & insert the information about the adjacent cluster into the communication information
				current_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(i);

				counter_remaining_edge_comm_elements -= i.comm_elements;

				/*
				 * since splitting along the catheti does not split an adjacent cluster information,
				 * the counter can never be below 0
				 */
				assert(counter_remaining_edge_comm_elements >= 0);
			}

			assert(counter_remaining_edge_comm_elements == 0);
		}



		/**********************
		 * HYPOTENUSE
		 *
		 * the splitting on the hypotenuse has to consider the new edge comm elements between both subtriangles
		 */
		{
			int counter_remaining_edge_comm_elements;

			/*
			 * setup remaining edge communication elements to consider for the first triangle
			 */
			if (triangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements =
					p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements =
					p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}

			/*
			 * iterate over the first adjacent cluster information of the first sub-triangle
			 */
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter =
									p_parentAdjacentClusters.hyp_adjacent_clusters.begin();

			for (	;
					iter != p_parentAdjacentClusters.hyp_adjacent_clusters.end();
					iter++
			)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.comm_elements;

				/*
				 * either this is the last adjacent information or we have to split this
				 * adjacency information
				 */
				if (counter_remaining_edge_comm_elements <= 0)
					break;

				/*
				 * insert adj information
				 */
				p_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
			}

			assert(counter_remaining_edge_comm_elements <= 0);


			/*
			 * Now we are at the point where an adjacent Information has to be splitted to both sub-triangles
			 *
			 * The edge comm data marked with \\ or // is reconstructed during the next 4 steps
			 * |\
			 * |  \[D]
			 * | 2  \\
			 * |    //
			 * |  //[C]
			 * |//        // \\ [A]
			 *       [B]//  1  \
			 *        //____ ____\
			 */

			CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

			/*
			 * [A]
			 *
			 * add remaining adjacent edge comm information
			 */
			if (iter != p_parentAdjacentClusters.hyp_adjacent_clusters.end())
			{
				// first sub-triangle: new adj information
				p_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
						CCluster_EdgeComm_InformationAdjacentCluster_(
								i.clusterTreeNode,
								i.uniqueId,
								// counter_remaining_edge_comm_elements <= 0
								i.comm_elements + counter_remaining_edge_comm_elements
#if CONFIG_ENABLE_MPI
								,
								i.mpi_rank
#endif
							)
						);
			}

			/*
			 * [B]
			 */
			// first sub-triangle: shared adj information
			p_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
					CCluster_EdgeComm_InformationAdjacentCluster_(
							p_second_child_node,
							p_second_child_node->uniqueId,
							p_splitJoinInformation.number_of_shared_edge_comm_elements
#if CONFIG_ENABLE_MPI
								,
								i.mpi_rank
#endif
						)
					);

			/*
			 * [C]
			 */
			// second sub-triangle: shared adj information
			p_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
					CCluster_EdgeComm_InformationAdjacentCluster_(
							p_first_child_node,
							p_first_child_node->uniqueId,
							p_splitJoinInformation.number_of_shared_edge_comm_elements
#if CONFIG_ENABLE_MPI
								,
								i.mpi_rank
#endif
						)
					);

			/*
			 * [D]
			 */
			if (counter_remaining_edge_comm_elements != 0)
			{
				// second sub-triangle: new adj information to adj cluster
				p_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
						CCluster_EdgeComm_InformationAdjacentCluster_(
								i.clusterTreeNode,
								i.uniqueId,
								-counter_remaining_edge_comm_elements
#if CONFIG_ENABLE_MPI
								,
								i.mpi_rank
#endif
							)
						);
			}



			/*
			 * second sub-triangle
			 */

			/*
			 * setup remaining edge communication elements for second triangle
			 *
			 * since we already handled some edge comm data elements of the second sub-triangle,
			 * we modify `counter_remaining_edge_comm_elements` relatively to it's current value!
			 */
			if (triangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements +=
					p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements +=
					p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
					- p_splitJoinInformation.number_of_shared_edge_comm_elements;
			}


			if (iter != p_parentAdjacentClusters.hyp_adjacent_clusters.end())
			{
				/*
				 * INCREMENT THE ITERATOR RIGHT HERE TO GO TO THE NEXT COMM ELEMENT!
				 */
				iter++;
			}

			for (	;
					iter != p_parentAdjacentClusters.hyp_adjacent_clusters.end();
					iter++
			)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.comm_elements;

				/*
				 * insert adj information
				 */
				p_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
			}

			assert (counter_remaining_edge_comm_elements == 0);
		}
	}



public:
	/**
	 * update information about adjacent clusters whenever a split or join operation was done
	 */
	void pass2_updateAdjacentClusterInformation()
	{
		assert(cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped == false);
		assert(cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped == false);

		/*
		 * HYPOTENUSE
		 */
		bool hypDataOnClockwiseStack = triangleFactory.isHypotenuseDataOnAClockwiseStack();

		if (_pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters,
				hypDataOnClockwiseStack
		))
			cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped = true;

		/*
		 * CATHETI
		 */
		bool catDataOnClockwiseStack = triangleFactory.isCathetusDataOnAClockwiseStack();

		if (_pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters,
				catDataOnClockwiseStack
				))
			cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped = true;

		return;
	}


	/**
	 * iterate over local adjacent clusters for hyp or cat
	 *
	 * the idea is to avoid modification of the local information about adjacent parts
	 */
private:
	bool _pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_new_localInformationAdjacentClusters,	///< new cat or hyp adjacency information
			bool localStackClockwise
	)
	{
		/*
		 * iterate over all local adjacency informations
		 */
		int counter = 0;
		int remainingCommElements = 0;

		// access first and second child in reversed order?
		bool first_second_reversed = false;

		typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator local_iter = p_localInformationAdjacentClusters.begin();

		for (	;
				local_iter != p_localInformationAdjacentClusters.end();
				local_iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

#if CONFIG_ENABLE_MPI
			if (local_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				// this is an MPI node => ignore so far.
				// later on we have to send split/join information to adjacent MPI nodes
				counter++;
				continue;
			}
#endif

			/*
			 * if the adjacent cluster tree node is not a leaf, the cluster was split up
			 * during the last traversal.
			 *
			 *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 * maybe we have to modify the edge comm data and do a break here to avoid modifications of
			 * the edge comm data which could be accessed by other adjacent clusters.
			 * modifying this edge comm data can confuse parallel running programs.
			 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 */
			EPARTITION_TRANSFER_STATE transferState = local_InformationAdjacentCluster.clusterTreeNode->cCluster_SplitJoinActions.transferState;

			// splitted_child is possible due to split operation
			assert(transferState != JOINED_PARENT);

			if (	transferState == SPLITTED_PARENT ||
					transferState == JOINED_CHILD
			)
			{
				/*
				 * we call a break here to work on a copy of the adjacent information to avoid further race conditions
				 */
				goto splittedOrJoinedAdjacentCluster;
			}

			counter++;
		}
		return false;

splittedOrJoinedAdjacentCluster:
		/*
		 * this new vector is swapped with the old one (hopefully) after all (also the adjacent ones)
		 * edge communication information was updated
		 */

		// COPY adjacent informations!!!
		p_new_localInformationAdjacentClusters = p_localInformationAdjacentClusters;

		local_iter = p_new_localInformationAdjacentClusters.begin();
		for (int i = 0; i < counter; i++)
				local_iter++;

		for (	;
				local_iter != p_new_localInformationAdjacentClusters.end();
				local_iter++
		)
		{
for_loop_start:
			CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

#if CONFIG_ENABLE_MPI
			if (local_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				// this is an MPI node => ignore so far.
				// later on we have to send split/join information to adjacent MPI nodes
				continue;
			}
#endif


			// this can happen for 2 splitted child nodes
			assert(local_InformationAdjacentCluster.clusterTreeNode->cCluster_SplitJoinActions.transferState != JOINED_PARENT);

			remainingCommElements = local_InformationAdjacentCluster.comm_elements;

			if (local_InformationAdjacentCluster.clusterTreeNode->cCluster_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   SPLITTED PARENT
				 ***********************************************************************************
				 ***********************************************************************************/

				// store parent cluster tree node since this value could be modified
				//CClusterTreeNode_ *parentClusterTreeNode = local_InformationAdjacentCluster.clusterTreeNode;
				CCluster_TreeNode_ &adjacent_parentClusterTreeNode = *(local_InformationAdjacentCluster.clusterTreeNode->cGeneric_TreeNode->cCluster_TreeNode);

				/**
				 * in case that the local triangle was splitted, we have to consider the correct order of edge comms.
				 *
				 * since we know that the adjacent cluster can be split only along the hypotenuse, this simplifies our assumptions.
				 */
				first_second_reversed = (adjacent_parentClusterTreeNode.cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == localStackClockwise);

				/**
				 * check wether the cluster as well as the adjacent cluster was splitted and the current splitted tree is the second child
				 */
				CCluster_TreeNode_ *firstChildToTraverse, *secondChildToTraverse;

				if (triangleFactory.isSecondChild())
					first_second_reversed = !first_second_reversed;

				if (first_second_reversed)
				{
					firstChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode;
					secondChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
				}
				else
				{
					firstChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
					secondChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode;
				}

				if (transferState == SPLITTED_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLITTED CHILD
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						if (remainingCommElements > 0)
						{
							/**
							 * search for remaining edge comm data
							 */

							// a new edge comm information element has to be inserted
							local_iter++;
							local_iter = p_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

							/**
							 * if an additional edge comm element was created, maybe those have to be reversed
							 */
							if (triangleFactory.isSecondChild())
							{
								typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
								*local_iter = *prev_iter;

								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
										*prev_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							else
							{
								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
										*local_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							continue;
						}

//						if (remainingCommElements < 0)
						{
							/**
							 * here we handle the special case when the adjacent edge comm data was larger than our local one.
							 * in this case, the local one was splitted without a split at the adjacent edge comm information.
							 *
							 * however, we are allowed to simply ignore this case by assuming that the adjacent cluster
							 * updates it's edge information data to match to our splitted local edge comm information.
							 */
							continue;
						}
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode : adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements <= 0);

					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						assert(remainingCommElements > 0);

						/**
						 * search for remaining edge comm data
						 */

						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = p_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

						/**
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (triangleFactory.isSecondChild())
						{
							typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode : adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements <= 0);

					continue;
				}

				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   SPLITTED PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						assert(remainingCommElements >= 0);

						// perfect match
						if (remainingCommElements == 0)
							continue;

						/**
						 * search for remaining edge comm data
						 */

						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = p_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

						/**
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (triangleFactory.isSecondChild())
						{
							typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}

						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode : adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements == 0);

					continue;
				}
			}

			if (local_InformationAdjacentCluster.clusterTreeNode->cCluster_SplitJoinActions.transferState == JOINED_CHILD)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   JOINED CHILD
				 ***********************************************************************************
				 ***********************************************************************************/

				if (transferState == SPLITTED_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLITTED CHILD
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_clusterTreeNode = local_InformationAdjacentCluster.clusterTreeNode;

					remainingCommElements = local_InformationAdjacentCluster.comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_clusterTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).clusterTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentClusterTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.uniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.comm_elements += (*next_local_iter).comm_elements;

						local_iter = p_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_clusterTreeNode = local_InformationAdjacentCluster.clusterTreeNode;

					remainingCommElements = local_InformationAdjacentCluster.comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_clusterTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).clusterTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentClusterTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.uniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.comm_elements += (*next_local_iter).comm_elements;

						local_iter = p_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}


				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_clusterTreeNode = local_InformationAdjacentCluster.clusterTreeNode;

					remainingCommElements = local_InformationAdjacentCluster.comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_clusterTreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == p_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).clusterTreeNode;

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */

					if (next_adjacent_parentClusterTreeNode->uniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.uniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.comm_elements += (*next_local_iter).comm_elements;

						local_iter = p_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == p_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 1
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
				}

				continue;
			}
		}
		return true;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent cluster
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
	)
	{
		if (_pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentCluster,
				p_adjacentClusterTreeNode->cGeneric_TreeNode->parent_node->cCluster_TreeNode,
				p_remainingCommElements,
				p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters)
		)
			return true;

#if DEBUG
		bool retval =
#endif
		_pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentCluster,
				p_adjacentClusterTreeNode->cGeneric_TreeNode->parent_node->cCluster_TreeNode,
				p_remainingCommElements,
				p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters
				);

		assert(retval);

		// true has to be always returned since some elements are still left
		return true;
	}


private:
	/**
	 * JOIN
	 *
	 * search for adjacency information on the adjacent edge comm information `p_edgeComm_InformationAdjacentCluster`
	 *
	 * return true, when some p_remainingCommElements is undershot and the local edge comm information has to be update once again
	 */
	bool _pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,	///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentParentClusterTreeNode,					///< adjacent parent cluster tree node to update adjacency information
			int &p_remainingCommElements,												///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_edgeComm_ChildInformationAdjacentCluster
	)
	{
		/**
		 * iterate over all edge comm data of the adjacently lying joined child triangle
		 */
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = p_edgeComm_ChildInformationAdjacentCluster.begin();
				iter != p_edgeComm_ChildInformationAdjacentCluster.end();
				iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local cluster
			 */

			/*
			 * test with currently local uniqueId but also with the child's uniqueId
			 */
			if (transferState == SPLITTED_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != (uniqueId.raw_unique_id >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentCluster.uniqueId.raw_unique_id >> 1) != uniqueId.raw_unique_id)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != uniqueId.raw_unique_id)
					continue;
			}


			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentCluster.uniqueId = p_adjacentParentClusterTreeNode->uniqueId;
			p_localInformationAdjacentCluster.clusterTreeNode = p_adjacentParentClusterTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentCluster.comm_elements)
			{
				p_localInformationAdjacentCluster.comm_elements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentCluster.comm_elements)
			{
				p_localInformationAdjacentCluster.comm_elements = adjacent_informationAdjacentCluster.comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;


				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_ChildInformationAdjacentCluster.end())
					return true;
				CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

				if (transferState == SPLITTED_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != (uniqueId.raw_unique_id >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentCluster.uniqueId.raw_unique_id >> 1) != uniqueId.raw_unique_id)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != uniqueId.raw_unique_id)
						return true;
				}

				p_localInformationAdjacentCluster.comm_elements += adjacent_informationAdjacentCluster.comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other sibling triangle cares about getting the right data
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentCluster.comm_elements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentCluster.comm_elements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;
				return true;
			}

			assert(false);
		}

		return false;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent cluster
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
	)
	{
		if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentCluster,
					p_adjacentClusterTreeNode,
					p_remainingCommElements,
					p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters
		))
			return true;

		return _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentCluster,
					p_adjacentClusterTreeNode,
					p_remainingCommElements,
					p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters
				);
	}



private:
	/**
	 * update the local adjacency information along the cat or hyp
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements,														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_edgeComm_InformationAdjacentCluster
	)
	{
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = p_edgeComm_InformationAdjacentCluster.begin();
				iter != p_edgeComm_InformationAdjacentCluster.end();
				iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local cluster
			 */
			if (transferState == SPLITTED_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != (uniqueId.raw_unique_id >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentCluster.uniqueId.raw_unique_id >> 1) != uniqueId.raw_unique_id)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != uniqueId.raw_unique_id)
					continue;
			}

			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentCluster.uniqueId = p_adjacentClusterTreeNode->uniqueId;
			p_localInformationAdjacentCluster.clusterTreeNode = p_adjacentClusterTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentCluster.comm_elements)
			{
				p_localInformationAdjacentCluster.comm_elements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentCluster.comm_elements)
			{
				p_localInformationAdjacentCluster.comm_elements = adjacent_informationAdjacentCluster.comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;

				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_InformationAdjacentCluster.end())
					return true;
				CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;


				/*
				 * test whether the adjacent id is equal to the id of the local cluster
				 */
				if (transferState == SPLITTED_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != (uniqueId.raw_unique_id >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentCluster.uniqueId.raw_unique_id >> 1) != uniqueId.raw_unique_id)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentCluster.uniqueId.raw_unique_id != uniqueId.raw_unique_id)
						return true;
				}

				p_localInformationAdjacentCluster.comm_elements += adjacent_informationAdjacentCluster.comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other
			 * sibling triangle cares about getting the right data.
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentCluster.comm_elements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentCluster.comm_elements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.comm_elements;
				return true;
			}

			assert(false);
		}

		return false;
	}



public:
	/**
	 * this method has to be executed for all nodes!
	 */
	static void pass3_swapAndCleanAfterUpdatingEdgeComm(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode
	)
	{
		if (i_cGeneric_TreeNode->cCluster_TreeNode == nullptr)
			return;

		CCluster_TreeNode_ *cCluster_TreeNode = i_cGeneric_TreeNode->cCluster_TreeNode;

		/*
		 * free child nodes due to a join operation
		 */
		if (i_cGeneric_TreeNode->delayed_child_deletion)
		{
			assert(cCluster_TreeNode->cCluster_SplitJoinActions.transferState == JOINED_PARENT);
			i_cGeneric_TreeNode->freeChildren();
		}
		else
		{
#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
			if (cCluster_TreeNode->cCluster_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				delete cCluster_TreeNode;
				i_cGeneric_TreeNode->cCluster_TreeNode = nullptr;
				return;
			}
#endif
		}

		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.swapAndCleanAfterUpdatingEdgeComm();

		cCluster_TreeNode->cCluster_SplitJoinActions.transferState = NO_TRANSFER;
	}



public:
	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CCluster_SplitJoinActions &p
	)
	{
		std::cout << " + transferState: " << p.transferState << std::endl;
		return co;
	}
};

}

#endif /* CCLUSTER_SPLITJOINACTION_HPP_ */
