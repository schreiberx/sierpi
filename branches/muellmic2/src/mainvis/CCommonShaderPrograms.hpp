/*
 * Copyright 2010 Martin Schreiber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * CShaders.hpp
 *
 *  Created on: Mar 22, 2010
 *      Author: Martin Schreiber (schreiberx@gmail.com)
 */

#ifndef CCOMMON_SHADERPROGRAMS_HPP_
#define CCOMMON_SHADERPROGRAMS_HPP_

#include "lib/CError.hpp"
#include "libgl/shaders/shader_blinn/CShaderBlinn.hpp"
#include "libgl/shaders/shader_blinn_shadow_map/CShaderBlinnShadowMap.hpp"
#include "libgl/shaders/shader_cube_map_mirror/CShaderCubeMapMirror.hpp"
#include "libgl/shaders/shader_texturize/CShaderTexturize.hpp"
#include "libgl/shaders/shader_color/CShaderColor.hpp"
#include "libgl/shaders/shader_height_color_blinn/CShaderHeightColorBlinn.hpp"

/**
 * \brief class to handle different shaders
 */
class CCommonShaderPrograms
{
	bool verbose;

public:
	CError error;	///< error handler

	CShaderCubeMapMirror	cCubeMapMirror;		///< shader to render cubemap
	CShaderTexturize		cTexturize;			///< shader for simple texturization
	CShaderBlinn			cBlinn;				///< blinn shader
	CShaderBlinnShadowMap	cBlinnShadowMap;	///< blinn shader with shadow map
	CShaderColor			cColor;				///< color shader
	CShaderHeightColorBlinn	cHeightColorBlinn;	///< blinn shader with height dependent height

	CCommonShaderPrograms(bool p_verbose = true)	:
			verbose(p_verbose)
	{
		CError_Append(cCubeMapMirror);
		CError_Append(cTexturize);
		CError_Append(cBlinn);
		CError_Append(cBlinnShadowMap);
		CError_Append(cColor);
		CError_Append(cHeightColorBlinn);
	}
};

#endif
