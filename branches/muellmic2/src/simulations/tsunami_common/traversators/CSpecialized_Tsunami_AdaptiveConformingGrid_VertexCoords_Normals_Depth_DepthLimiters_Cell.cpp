/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData.hpp"
#include "../kernels/simulation/CAdaptive_Tsunami.hpp"

namespace sierpi
{
namespace travs
{
	class CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData_Private	:
		public CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<
			sierpi::kernels::CAdaptive_Tsunami,
			CTsunamiSimulationStacksAndTypes
		>
	{
	};


CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData()
{
	generic_traversator = new CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData_Private;
}


CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::~CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData()
{
	delete generic_traversator;
}



void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_RootTraversator(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_RootTraversator(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_KernelClass(
		TTsunamiDataScalar i_square_side_length,

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
		TTsunamiDataScalar i_refine_height_threshold,
		TTsunamiDataScalar i_coarsen_height_threshold,

		TTsunamiDataScalar i_refine_slope_threshold,
		TTsunamiDataScalar i_coarsen_slope_threshold,
#endif

		CTsunamiSimulationScenarios *i_cSimulationDataSets
)
{
	generic_traversator->cKernelClass.setup_WithParameters(
			i_square_side_length,

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 1
			i_refine_height_threshold,
			i_coarsen_height_threshold,

			i_refine_slope_threshold,
			i_coarsen_slope_threshold,
#endif

			i_cSimulationDataSets
		);
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionFirstTraversal(
		CTsunamiSimulationStacksAndTypes *cStacks
)
{
	repeat_traversal = generic_traversator->firstTraversal.action(cStacks);
	return repeat_traversal;
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionMiddleTraversals_Serial(
		CTsunamiSimulationStacksAndTypes *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Serial(cStacks, repeat_traversal);
	return repeat_traversal;
}


bool CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionMiddleTraversals_Parallel(
		CTsunamiSimulationStacksAndTypes *cStacks
)
{
	repeat_traversal = generic_traversator->middleTraversals.action_Parallel(cStacks, repeat_traversal);
	return repeat_traversal;
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionLastTraversal_Parallel(
		CTsunamiSimulationStacksAndTypes *cStacks,
		CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
)
{
	generic_traversator->lastTraversal.action_Parallel(
			cStacks,
			p_splitJoinInformation
		);
}


void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::actionLastTraversal_Serial(
		CTsunamiSimulationStacksAndTypes *cStacks
)
{
	generic_traversator->lastTraversal.action_Serial(cStacks);
}

void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_RootCluster(p_triangleFactory);
}


/**
 * setup the initial cluster traversal for the given factory
 */
void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::setup_Cluster(
		CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData &parent,
		CTriangle_Factory &p_triangleFactory
)
{
	generic_traversator->setup_Cluster(
			*(parent.generic_traversator),
			p_triangleFactory
		);

	generic_traversator->cKernelClass.setup_WithKernel(
			parent.generic_traversator->cKernelClass
		);
}



void CSpecialized_Tsunami_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData::storeReduceValue(
		TTsunamiDataScalar *o_cflReduceValue
	)
{
	generic_traversator->cKernelClass.storeReduceValue(o_cflReduceValue);
}


}
}
