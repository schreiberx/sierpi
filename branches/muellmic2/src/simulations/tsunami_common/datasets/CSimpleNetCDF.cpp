/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: July 19, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "../../tsunami_common/types/CTsunamiTypes.hpp"

#include "CDataSamplingSet_SimpleNetCDF.hpp"
#include "CSimpleNetCDF.hpp"
#include <cassert>



CSimpleNetCDF::CSimpleNetCDF()	:
	is_dataset_loaded(false),
	cSimpleNetCDF_TerrainData_private(nullptr),
	cSimpleNetCDF_SurfaceDisplacementData_private(nullptr)
{
}


bool CSimpleNetCDF::loadDataset(
	const char *i_filename_bathymetry,
	const char *i_filename_displacements
)
{
	is_dataset_loaded = false;

	cSimpleNetCDF_TerrainData_private = new CDataSamplingSet_SimpleNetCDF<TTsunamiDataScalar>();
	if (cSimpleNetCDF_TerrainData_private->loadDataset(i_filename_bathymetry) == false)
	{
		delete cSimpleNetCDF_TerrainData_private;
		cSimpleNetCDF_TerrainData_private = nullptr;
		return false;
	}


	cSimpleNetCDF_SurfaceDisplacementData_private = new CDataSamplingSet_SimpleNetCDF<TTsunamiDataScalar>();
	if (cSimpleNetCDF_SurfaceDisplacementData_private->loadDataset(i_filename_displacements) == false)
	{
		delete cSimpleNetCDF_TerrainData_private;
		cSimpleNetCDF_TerrainData_private = nullptr;
		return false;
	}

	domain_min_x = cSimpleNetCDF_TerrainData_private->getXMin();
	domain_max_x = cSimpleNetCDF_TerrainData_private->getXMax();

	domain_min_y = cSimpleNetCDF_TerrainData_private->getYMin();
	domain_max_y = cSimpleNetCDF_TerrainData_private->getYMax();

	domain_length_x = domain_max_x - domain_min_x;
	domain_length_y = domain_max_y - domain_min_y;

	domain_center_x = ((T)0.5)*(domain_min_x+domain_max_x);
	domain_center_y = ((T)0.5)*(domain_min_y+domain_max_y);


    if (domain_length_x < domain_length_y)
    {
    	domain_max_y = domain_min_y + domain_length_x;
    	domain_length_y = domain_length_x;
    }
    else
    {
    	domain_max_x = domain_min_x + domain_length_y;
    	domain_length_x = domain_length_y;
    }




	displacements_min_x = cSimpleNetCDF_SurfaceDisplacementData_private->getXMin();
	displacements_max_x = cSimpleNetCDF_SurfaceDisplacementData_private->getXMax();

	displacements_min_y = cSimpleNetCDF_SurfaceDisplacementData_private->getYMin();
	displacements_max_y = cSimpleNetCDF_SurfaceDisplacementData_private->getYMax();

	displacements_size_x = displacements_max_x - displacements_min_x;
	displacements_size_y = displacements_max_y - displacements_min_y;

	displacements_center_x = ((T)0.5)*(displacements_min_x+displacements_max_x);
	displacements_center_y = ((T)0.5)*(displacements_min_y+displacements_max_y);

	is_dataset_loaded = true;

	outputVerboseInformation();

	return true;
}



CSimpleNetCDF::~CSimpleNetCDF()
{
	delete cSimpleNetCDF_TerrainData_private;
	delete cSimpleNetCDF_SurfaceDisplacementData_private;
}



void CSimpleNetCDF::outputVerboseInformation()
{
	std::cout << "Simple NetCDF:" << std::endl;

	std::cout << " + Bathymetry data-window: (" << cSimpleNetCDF_TerrainData_private->getXMin() << ", " << cSimpleNetCDF_TerrainData_private->getYMin() << 	") x (" << cSimpleNetCDF_TerrainData_private->getXMax() << ", " << cSimpleNetCDF_TerrainData_private->getYMax() << ")" << std::endl;
	std::cout << " + Bathymetry data-size: (" << (cSimpleNetCDF_TerrainData_private->getXMax() - cSimpleNetCDF_TerrainData_private->getXMin()) << ", " 	<< (cSimpleNetCDF_TerrainData_private->getYMax() - cSimpleNetCDF_TerrainData_private->getYMin())<< ")" << std::endl;

	std::cout << " + Used bathymetry start: (" << domain_min_x << ", " << domain_min_y << ")" << std::endl;
	std::cout << " + Used bathymetry size: (" << domain_length_x << ", " << domain_length_y << ")" << std::endl;
	std::cout << " + Used bathymetry end: (" << domain_max_x << ", " << domain_max_y << ")" << std::endl;

	std::cout << " + Displacement window: (" << cSimpleNetCDF_SurfaceDisplacementData_private->getXMin() << ", " << cSimpleNetCDF_SurfaceDisplacementData_private->getYMin() << 	") x (" << cSimpleNetCDF_SurfaceDisplacementData_private->getXMax() << ", " << cSimpleNetCDF_SurfaceDisplacementData_private->getYMax() << ")" << std::endl;
	std::cout << " + Displacement size: (" << (cSimpleNetCDF_SurfaceDisplacementData_private->getXMax() - cSimpleNetCDF_SurfaceDisplacementData_private->getXMin()) << ", " 	<< (cSimpleNetCDF_SurfaceDisplacementData_private->getYMax() - cSimpleNetCDF_SurfaceDisplacementData_private->getYMin())<< ")" << std::endl;
}



void CSimpleNetCDF::getOriginAndSize(
		TTsunamiDataScalar	*o_origin_x,
		TTsunamiDataScalar	*o_origin_y,
		TTsunamiDataScalar	*o_size_x,
		TTsunamiDataScalar	*o_size_y
)
{
	*o_origin_x = domain_min_x+domain_length_x*(TTsunamiDataScalar)0.5;
	*o_origin_y = domain_min_y+domain_length_y*(TTsunamiDataScalar)0.5;

	*o_size_x = domain_length_x;
	*o_size_y = domain_length_y;
}



TTsunamiDataScalar CSimpleNetCDF::getDisplacementData(
		TTsunamiDataScalar i_x,		///< x-coordinate in world-space
		TTsunamiDataScalar i_y,		///< x-coordinate in world-space
		TTsunamiDataScalar i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	return cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D_BorderValue(i_x, i_y, i_level_of_detail);
}



/**
 * get nodal data for given coordinate
 */
void CSimpleNetCDF::getNodalData(
		TTsunamiDataScalar i_x,
		TTsunamiDataScalar i_y,
		TTsunamiDataScalar i_level_of_detail,
		CTsunamiSimulationDOFs *o_nodal_data
)
{
	o_nodal_data->b = cSimpleNetCDF_TerrainData_private->getFloat2D(i_x, i_y, i_level_of_detail);
	o_nodal_data->hu = 0;
	o_nodal_data->hv = 0;
	o_nodal_data->h = -o_nodal_data->b + cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D(i_x, i_y, i_level_of_detail);
}


/**
 * get nodal data for given coordinate
 */
TTsunamiDataScalar CSimpleNetCDF::getBathymetryData(
		TTsunamiDataScalar i_x,		///< x-coordinate in model-space
		TTsunamiDataScalar i_y,		///< x-coordinate in model-space
		TTsunamiDataScalar i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
//	std::cout << i_level_of_detail << "\t";

	return cSimpleNetCDF_TerrainData_private->getFloat2D(i_x, i_y, i_level_of_detail) +
			cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D(i_x, i_y, i_level_of_detail);
}



/**
 * get nodal data for given coordinate
 */
TTsunamiDataScalar CSimpleNetCDF::getWaterSufaceData(
		TTsunamiDataScalar i_x,		///< x-coordinate in model-space
		TTsunamiDataScalar i_y,		///< x-coordinate in model-space
		TTsunamiDataScalar i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	return cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D(i_x, i_y, i_level_of_detail);
}



/**
 * get nodal data for benchmark
 */
void CSimpleNetCDF::getBenchmarkNodalData(
		TTsunamiDataScalar i_x,
		TTsunamiDataScalar i_y,
		TTsunamiDataScalar i_level_of_detail,
		TTsunamiDataScalar i_timestamp,
		CTsunamiSimulationDOFs *o_nodal_data
)
{
	/**
	 * TODO
	 */
}


/**
 * get boundary data at given position
 */
void CSimpleNetCDF::getBoundaryData(
		TTsunamiDataScalar i_x,
		TTsunamiDataScalar i_y,
		TTsunamiDataScalar i_level_of_detail,
		CTsunamiSimulationDOFs *o_nodal_data
)
{
	std::cerr << "NOT IMPLEMENTED YET" << std::endl;
	assert(false);
}



bool CSimpleNetCDF::isDatasetLoaded()
{
	return is_dataset_loaded;
}
