/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationTsunami_Parallel_FileOutput_Block.hpp
 *
 *  Created on: Jul 6, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_STOREGRIDDATATOARRAY_HPP_
#define CSIMULATIONTSUNAMI_STOREGRIDDATATOARRAY_HPP_

#include "../../mainthreading/CMainThreading.hpp"


class CSimulationTsunami_GridDataArrays
{
public:
	TTsunamiVertexScalar *triangle_vertex_buffer;
	TTsunamiVertexScalar *triangle_normal_buffer;

	TTsunamiDataScalar *h;
	TTsunamiDataScalar *hu;
	TTsunamiDataScalar *hv;
	TTsunamiDataScalar *b;

	TTsunamiDataScalar *cfl_value_hint;

	long long number_of_triangle_cells;
	size_t next_free_cell_id;

	enum EFLAGS
	{
		VERTICES	= (1 << 0),
		NORMALS		= (1 << 1),
		H			= (1 << 2),
		HU			= (1 << 3),
		HV			= (1 << 4),
		B			= (1 << 5),
		CFL_VALUE_HINT = (1 << 6),
		ALL			= 0xffffff
	};


	CMainThreading_Lock lock;

	CSimulationTsunami_GridDataArrays(
		size_t i_number_of_triangle_cells,
		size_t i_vertex_multiplier = 1,
		size_t i_dof_multiplier = 1,
		int i_flags = EFLAGS::ALL
	)	:
		triangle_vertex_buffer(nullptr),
		triangle_normal_buffer(nullptr),

		h(nullptr),
		hu(nullptr),
		hv(nullptr),
		b(nullptr),

		number_of_triangle_cells(i_number_of_triangle_cells),
		next_free_cell_id(0)
	{

		if (i_flags & EFLAGS::VERTICES)
			triangle_vertex_buffer = new TTsunamiVertexScalar[number_of_triangle_cells*3*3*i_vertex_multiplier];

		if (i_flags & EFLAGS::NORMALS)
			triangle_normal_buffer = new TTsunamiVertexScalar[number_of_triangle_cells*3*3*i_vertex_multiplier];

		if (i_flags & EFLAGS::H)
			h = new TTsunamiDataScalar[number_of_triangle_cells*i_dof_multiplier];

		if (i_flags & EFLAGS::HU)
			hu = new TTsunamiDataScalar[number_of_triangle_cells*i_dof_multiplier];

		if (i_flags & EFLAGS::HV)
			hv = new TTsunamiDataScalar[number_of_triangle_cells*i_dof_multiplier];

		if (i_flags & EFLAGS::B)
			b = new TTsunamiDataScalar[number_of_triangle_cells*i_dof_multiplier];

		if (i_flags & EFLAGS::CFL_VALUE_HINT)
			cfl_value_hint = new TTsunamiDataScalar[number_of_triangle_cells*i_dof_multiplier];

	}



	size_t getNextTriangleCellStartId(
			size_t i_number_of_cells
	)
	{
		lock.lock();

		size_t n = next_free_cell_id;
		next_free_cell_id += i_number_of_cells;

		lock.unlock();

		assert(next_free_cell_id <= (size_t)number_of_triangle_cells);

		return n;
	}


	~CSimulationTsunami_GridDataArrays()
	{
		if (triangle_vertex_buffer != nullptr)
			delete [] triangle_vertex_buffer;

		if (triangle_normal_buffer != nullptr)
			delete [] triangle_normal_buffer;

		if (h != nullptr)
			delete [] h;

		if (hu != nullptr)
			delete [] hu;

		if (hv != nullptr)
			delete [] hv;

		if (b != nullptr)
			delete [] b;

		if (cfl_value_hint != nullptr)
			delete [] cfl_value_hint;
	}
};


#endif /* CSIMULATIONTSUNAMI_PARALLEL_FILEOUTPUT_BLOCK_HPP_ */
