/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK2_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK2_HPP_

#include "config.h"

#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CDomainClusters.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CCluster_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals_RK2
{
public:


	template<
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T,						/// T
		int i_rungeKuttaPass
	>
	static void singleAction(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size
	)
	{
		typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;

					CSimulation_Cluster *cCluster = i_node->cCluster;

					if (i_rungeKuttaPass == 1)
					{
						// allocate element stacks
						if (i_node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() > cCluster->cStackCellDataRK2_f_t0.getMaxNumberOfElements())
						{
							// resize cStackCellDataRK2 if all element data stack elements don't fit into it
							cCluster->cStackCellDataRK2_f_t0.resize(i_node->cStacks->element_data_stacks.forward.getMaxNumberOfElements());
							cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1.resize(i_node->cStacks->element_data_stacks.forward.getMaxNumberOfElements());

							// TODO: should we also shrink the stacks?!?
							// then this stacks should be moved to the simulation stacks class
						}

						// backup element data
						i_node->cStacks->element_data_stacks.forward.copyStackElements(&(cCluster->cStackCellDataRK2_f_t0));

						// store results to cStackCellData1stEulerUpdatesRK2
						i_node->cStacks->element_data_stacks.forward.swap(cCluster->cStackCellDataRK2_f_t0);


						/*
						 * y~ = y0 + h f(t0, y0)
						 *
						 * f(t0, y0) is computed and stored in cStackCellDataRK2_1stEulerUpdates
						 */
					}

					if (i_rungeKuttaPass == 2)
					{
						// store results to cStackCellData1stEulerUpdatesRK2
						i_node->cStacks->element_data_stacks.forward.swap(cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1);
					}

					// store updates (slope) only
					(cCluster->*(i_simulationSubClass)).actionFirstPass(i_node->cStacks);
				}
		);



#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass1();
			}
		);
#endif


		T cfl1_reduce_value = CMath::numeric_inf<T>();

#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
			}
		);
#endif

		/*
		 * middle pass: pull edge comm data & compute global timestep size
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode, T *o_reduceValue)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// firstly, pull edge communication data from adjacent sub-clusters and compute fluxes
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluationSharedMemory();

					// run computation based on newly set-up stacks
					(node->cCluster->*(i_simulationSubClass)).actionMiddlePass_computeCFL(node->cStacks, o_reduceValue);
				},
				&(sierpi::CReduceOperators::MIN<T>),	// use minimum since the minimum timestep has to be selected
				&cfl1_reduce_value
			);

		TTsunamiDataScalar timestep_size = i_cfl_value * cfl1_reduce_value;
		*o_timestep_size = timestep_size;


		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		if (i_rungeKuttaPass == 1)
		{
			i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;
					CSimulation_Cluster *cCluster = i_node->cCluster;

					// run computation based on newly set-up stacks
					(cCluster->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, timestep_size);

					// restore swapped stacks
					i_node->cStacks->element_data_stacks.forward.swap(cCluster->cStackCellDataRK2_f_t0);

					/*
					 * y~ = y0 + h * f(t0, y0)
					 *
					 * compute y~:
					 */

					size_t elementsOnStack = cCluster->cStackCellDataRK2_f_t0.getNumberOfElementsOnStack();

					cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1.setStackElementCounter(elementsOnStack);

					for (size_t i = 0; i < elementsOnStack; i++)
					{
						TFluxCommTraversator::CKernelClass::rk2_step1_cell_update_with_edges(
								i_node->cStacks->element_data_stacks.forward.getElementAtIndex(i),		// y_0
								cCluster->cStackCellDataRK2_f_t0.getElementAtIndex(i),	// f(t0, y0)
								cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1.getElementPtrAtIndex(i), // y~ = y0 + h f(t0, y0)
								timestep_size
							);
					}
				}
			);
		}

		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		if (i_rungeKuttaPass == 2)
		{
			i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;
					CSimulation_Cluster *cCluster = i_node->cCluster;

					// run computation based on newly set-up stacks
					(cCluster->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, timestep_size);


					// restore swapped stacks
					i_node->cStacks->element_data_stacks.forward.swap(cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1);


					/*
					 * apply update
					 */
					size_t elementsOnStack = cCluster->cStackCellDataRK2_f_t0.getNumberOfElementsOnStack();

					TTsunamiDataScalar cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<TTsunamiDataScalar>();

					for (size_t i = 0; i < elementsOnStack; i++)
					{
						CTsunamiSimulationCellData *e = i_node->cStacks->element_data_stacks.forward.getElementPtrAtIndex(i);

						TFluxCommTraversator::CKernelClass::rk2_step2_cell_update_with_edges(
								cCluster->cStackCellDataRK2_f_t0.getElementAtIndex(i),
								cCluster->cStackCellDataRK2_yapprox_t1_AND_f_t1.getElementAtIndex(i),
								e,
								timestep_size
							);

						cfl_domain_size_div_max_wave_speed = CMath::min(
								cfl_domain_size_div_max_wave_speed,
								e->cfl_domain_size_div_max_wave_speed
								);

					}
				}
			);
		}
	}

	/**
	 * run the edge comm traversals
	 */
	template<
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T							/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size
	)
	{
		singleAction
			<
				CCluster_TreeNode_,
				CSimulation_Cluster,
				TFluxCommTraversator,	/// Traversator including kernel
				CEdgeData,				/// type of edge communication data
				CStackAccessors,		/// accessors to adjacent stacks
				T,						/// ...
				1						// first pass: compute updates for explicit euler
			>(i_simulationSubClass, i_simulationFluxCommSubClass, i_cDomainClusters, i_cfl_value, o_timestep_size);


		singleAction
			<
				CCluster_TreeNode_,
				CSimulation_Cluster,
				TFluxCommTraversator,	/// Traversator including kernel
				CEdgeData,				/// type of edge communication data
				CStackAccessors,		/// accessors to adjacent stacks
				T,						/// ...
				2						// second pass: compute updates using explicit euler at (t_{i+1}, y_{i+1})
			>(i_simulationSubClass, i_simulationFluxCommSubClass, i_cDomainClusters, i_cfl_value, o_timestep_size);
	}
};

#endif
