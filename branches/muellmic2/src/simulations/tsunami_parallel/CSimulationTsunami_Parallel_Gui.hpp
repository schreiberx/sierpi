/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_TSUNAMI_PARALLEL_GUI_HPP_
#define CSIMULATION_TSUNAMI_PARALLEL_GUI_HPP_


#include "config.h"
#include "../tsunami_common/tsunami_config.h"
#include "../tsunami_common/types/CTsunamiTypes.hpp"
#include "../tsunami_common/CTsunamiSimulationParameters.hpp"

#include "libsierpi/parallelization/CDomainClusters.hpp"
#include "CSimulationTsunami_Cluster.hpp"


/*
 * Write data stored in recursive SFC format to 1D arrays data structure:
 *
 * CSimulationTsunami_GridDataArrays represents the container.
 * CSimulationTsunami_OutputGridDataArrays implements the kernel which writes the data.
 */
#include "../tsunami_common/CSimulationTsunami_GridDataArrays.hpp"
#include "../tsunami_common/kernels/backends/CSimulationTsunami_OutputGridDataArrays.hpp"

/*
 * root renderer:
 *
 * setup buffers,
 * render specific primitives stored in buffer
 */
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Cell_Root_Tsunami.hpp"
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Cluster_Root_Tsunami.hpp"


#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	#include "../tsunami_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"
#endif



/*
 * interfaces which have to be implemented
 */
#include "CSimulation_MainInterface.hpp"
#include "CSimulation_MainGuiInterface.hpp"

/**
 * \brief Main class for parallel Tsunami Simulation
 *
 * This class is the central point of a parallel Tsunami simulation.
 *
 * sets up the simulation.
 * It manages all sub-clusters, creates the initial domain triangulation and
 */
class CSimulationTsunami_Parallel_Gui	:
	public CSimulation_MainGuiInterface,
	public CSimulation_MainInterface
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationTsunami_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;


	/**
	 * root renderer
	 */
	COpenGL_Vertices_Cell_Root_Tsunami cOpenGL_Vertices_Cell_Root_Tsunami;
	COpenGL_Vertices_Wireframe_Root_Tsunami cOpenGL_Vertices_Wireframe_Root_Tsunami;
	COpenGL_Vertices_Cluster_Root_Tsunami cOpenGL_Vertices_Cluster_Root_Tsunami;

	/**
	 * reference to simulation parameters
	 */
	CTsunamiSimulationParameters &cTsunamiSimulationParameters;

	/**
	 * reference to clusters to start traversals
	 */
	sierpi::CDomainClusters<CSimulationTsunami_Cluster> &cDomainClusters;

public:
	/**
	 * constructor for parallel tsunami simulation
	 */
	CSimulationTsunami_Parallel_Gui(
			CTsunamiSimulationParameters &i_cTsunamiSimulationParameters,
			sierpi::CDomainClusters<CSimulationTsunami_Cluster> &i_cDomainClusters
	)	:
			cTsunamiSimulationParameters(i_cTsunamiSimulationParameters),
			cDomainClusters(i_cDomainClusters)
	{
	}


	/**
	 * Deconstructor
	 */
	virtual ~CSimulationTsunami_Parallel_Gui()
	{
	}


public:

	/**
	 * ignore key-up events
	 */
	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	/**
	 * handle key-down event
	 */
	bool gui_key_down_event(
			int i_key	///< pressed key (ascii)
	)
	{
		switch(i_key)
		{
			case 'i':
				cTsunamiSimulationParameters.simulation_random_raindrops_activated ^= true;
				break;

			case 'z':
				cTsunamiSimulationParameters.simulation_world_scene_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cTsunamiSimulationParameters.simulation_world_scene_id << std::endl;
				break;

			case 'x':
				cTsunamiSimulationParameters.simulation_world_scene_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cTsunamiSimulationParameters.simulation_world_scene_id << std::endl;
				break;

			case 'Z':
				cTsunamiSimulationParameters.simulation_terrain_scene_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cTsunamiSimulationParameters.simulation_terrain_scene_id << std::endl;
				break;

			case 'X':
				cTsunamiSimulationParameters.simulation_terrain_scene_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cTsunamiSimulationParameters.simulation_terrain_scene_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 'y':
				cTsunamiSimulationParameters.grid_initial_cluster_splits += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cTsunamiSimulationParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 'h':
				if (cTsunamiSimulationParameters.grid_initial_cluster_splits > 0)
					cTsunamiSimulationParameters.grid_initial_cluster_splits -= 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cTsunamiSimulationParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 't':
				cTsunamiSimulationParameters.grid_initial_recursion_depth += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial recursion depth to " << cTsunamiSimulationParameters.grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				cTsunamiSimulationParameters.grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cTsunamiSimulationParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'g':
				if (cTsunamiSimulationParameters.grid_initial_recursion_depth > 0)
					cTsunamiSimulationParameters.grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << cTsunamiSimulationParameters.grid_initial_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'G':
				if (cTsunamiSimulationParameters.grid_max_relative_recursion_depth > 0)
					cTsunamiSimulationParameters.grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cTsunamiSimulationParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'P':
				output_ClusterTreeInformation();
				break;

			case 'c':
				setup_RadialDamBreak(
						cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posx,
						cTsunamiSimulationParameters.simulation_dataset_breaking_dam_posy,
						cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius
					);
				break;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			default:
				return false;
		}

		return true;
	}


	/**
	 * handle gui mouse motion event
	 */
	bool gui_mouse_motion_event(
		TTsunamiDataScalar i_mouse_coord_x,
		TTsunamiDataScalar i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, cTsunamiSimulationParameters.simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	/**
	 * handle mouse button event
	 */
	bool gui_mouse_button_down_event(
		TTsunamiDataScalar i_mouse_coord_x,	///< x-coordinate of mouse cursor
		TTsunamiDataScalar i_mouse_coord_y,	///< y-coordinate of mouse cursor
		int i_button			///< pressed button id
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}


	/**
	 * render boundaries. For SWE, this are the bathymetries
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		switch(i_bathymetry_visualization_method % 3)
		{
			case 1:
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

				cCommonShaderPrograms.cBlinn.use();
				cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();


				CHelper_GenericParallelVertexDataCommTraversals::action<
					sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV,
					sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CTsunamiVisualizationTypes::T>
				>(
					&CSimulationTsunami_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
					cDomainClusters,
					[&](sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV *i_traversator)
					{
						i_traversator->cKernelClass.setup(
								cTsunamiSimulationParameters.visualization_water_surface_default_displacement,
								cTsunamiSimulationParameters.visualization_water_surface_scale_factor,
								&cOpenGL_Vertices_Cell_Root_Tsunami
							);
					}
				);

				cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
#endif
				return "smooth";


			case 2:
				// do not render bathymetry
				return "blank";


			default:
			{
				CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::B);

				TTsunamiDataScalar *v = cGridDataArrays.triangle_vertex_buffer;
				TTsunamiDataScalar *n = cGridDataArrays.triangle_normal_buffer;
				TTsunamiDataScalar t;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.b[i]*cTsunamiSimulationParameters.visualization_terrain_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Cell_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cTsunamiSimulationParameters.number_of_local_cells*3
					);

				cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}
				return "simple";
		}

		return "";
	}


	/**
	 * translate and scale vertex with specific visualization parameters
	 */
	inline void p_applyTranslateAndScaleToVertex(
			TTsunamiVertexScalar *io_vertex
	)
	{
		io_vertex[0] = (io_vertex[0] + cTsunamiSimulationParameters.visualization_domain_translate_x)*cTsunamiSimulationParameters.visualization_domain_scale_x;
		io_vertex[1] = (io_vertex[1] + cTsunamiSimulationParameters.visualization_domain_translate_y)*cTsunamiSimulationParameters.visualization_domain_scale_y;
		io_vertex[2] = (io_vertex[2] + cTsunamiSimulationParameters.visualization_domain_translate_z)*cTsunamiSimulationParameters.visualization_domain_scale_z;
	}



	/**
	 * store the DOFs and grid data to an array
	 */
	void storeDOFsToGridDataArrays(
			CSimulationTsunami_GridDataArrays *io_cGridDataArrays,
			int i_flags,
			int i_preprocessing_mode = 0
	)
	{
		i_flags |=
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::NORMALS;

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

				// We instantiate it right here to avoid any overhead due to split/join operations
				sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

				cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
				cOutputGridDataArrays.cKernelClass.setup(
						io_cGridDataArrays,
						offset,
						i_flags,
						i_preprocessing_mode
					);

				cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
			}
		);

		assert(io_cGridDataArrays->number_of_triangle_cells == cTsunamiSimulationParameters.number_of_local_cells);

		/*
		 * postprocessing
		 */
		if (	cTsunamiSimulationParameters.visualization_domain_scale_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_scale_z != 1.0 ||

				cTsunamiSimulationParameters.visualization_domain_translate_x != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_y != 1.0 ||
				cTsunamiSimulationParameters.visualization_domain_translate_z != 1.0
		)
		{
			for (int i = 0; i < cTsunamiSimulationParameters.number_of_local_cells; i++)
			{
				TTsunamiVertexScalar *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

				for (int vn = 0; vn < 3; vn++)
				{
					p_applyTranslateAndScaleToVertex(v);
					v += 3;
				}

				if (i_flags & CSimulationTsunami_GridDataArrays::H)
					io_cGridDataArrays->h[i] *= cTsunamiSimulationParameters.visualization_domain_scale_z;

				if (i_flags & CSimulationTsunami_GridDataArrays::B)
					io_cGridDataArrays->b[i] *= cTsunamiSimulationParameters.visualization_domain_scale_z;
			}
		}
	}


	/**
	 * render water surface
	 */
	const char *render_DOFs(
			int i_surface_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);

		const char *ret_str = nullptr;
		TTsunamiDataScalar *v;
		TTsunamiDataScalar *n;
		TTsunamiDataScalar t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::HU);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.hu[i]*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "x-momentum";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::HV);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.hv[i]*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "y-momentum";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::B);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.b[i]*cTsunamiSimulationParameters.visualization_terrain_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "bathymetry";
				break;


#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1
			case 4:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::H + CSimulationTsunami_GridDataArrays::B, 1);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] *= cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "aligned surface";
				break;
#endif

			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CSimulationTsunami_GridDataArrays::H + CSimulationTsunami_GridDataArrays::B);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						if (cGridDataArrays.h[i] > 0)
							v[1] = (cGridDataArrays.h[i]+cGridDataArrays.b[i])*cTsunamiSimulationParameters.visualization_water_surface_scale_factor*cTsunamiSimulationParameters.visualization_domain_scale_z;
						else
							v[1] = std::numeric_limits<TTsunamiDataScalar>::infinity();

						v += 3;
						n += 3;
					}
				}

				ret_str = "surface height";
				break;
		}

		cCommonShaderPrograms.cHeightColorBlinn.use();
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

		cOpenGL_Vertices_Cell_Root_Tsunami.render(
				cGridDataArrays.triangle_vertex_buffer,
				cGridDataArrays.triangle_normal_buffer,
				cTsunamiSimulationParameters.number_of_local_cells*3
			);

		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;

#if 0
			case 5:
				// smooth renderer
#if 0

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
				cCommonShaderPrograms.cHeightColorBlinn.use();
				cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();


				CHelper_GenericParallelVertexDataCommTraversals::action
				<
					CCluster_TreeNode_,
					CSimulationTsunami_Cluster,
					sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
					CTsunamiSimulationTypes::TVisualizationVertexData,
					CStackAccessorMethodsTsunamiVertexData<CCluster_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
				>
				(
						&CSimulationTsunami_Cluster::cOpenGL_Vertices_Smooth_Cell_Tsunami,
						&CSimulationTsunami_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
						rootGenericTreeNode,
						[&](CCluster_TreeNode_ *node)
						{
							node->cCluster->cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(
									cTsunamiSimulationParameters.visualization_water_surface_default_displacement,
									cTsunamiSimulationParameters.visualization_water_surface_scale_factor,
									&cOpenGL_Vertices_Cell_Root_Tsunami
							);
						}
				);

				cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cHeightColorBlinn.disable();
#endif

#endif
				return "smooth surface renderer with height color shader";

			case 6:
				// simple aligned renderer
				p_render_surfaceSmooth(cCommonShaderPrograms.cBlinn);
				return "smooth surface";
		}
#endif
		return "[none]";
	}


	/**
	 * render a wireframe
	 */
	void render_Wireframe(
			int i_visualization_render_wireframe,			///< submode
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader programs
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
		{
			CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_cells);

			storeDOFsToGridDataArrays(&cGridDataArrays, 0);

			TTsunamiDataScalar *v, *l;
			TTsunamiDataScalar *lines = new TTsunamiDataScalar[cTsunamiSimulationParameters.number_of_local_cells*2*3*3];

			v = cGridDataArrays.triangle_vertex_buffer;
			l = lines;

			for (size_t i = 0; i < (size_t)cTsunamiSimulationParameters.number_of_local_cells; i++)
			{
				for (int vn = 0; vn < 2; vn++)
				{
					l[0] = v[0];
					l[1] = v[2];
					l[2] = -v[1];
					l += 3;

					l[0] = v[3+0];
					l[1] = v[3+2];
					l[2] = -v[3+1];
					l += 3;

					v += 3;
				}

				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				v -= 6;

				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				v += 9;
			}

			cCommonShaderPrograms.cBlinn.use();
			cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0,0,0.5));
			cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0,0,0));
			cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(0.1,0.1,0.1));


			cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

			cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
					lines,
					cTsunamiSimulationParameters.number_of_local_cells*3*2
				);

			cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
			cCommonShaderPrograms.cBlinn.disable();

			delete [] lines;
		}
	}


	/**
	 * highlight the cluster borders
	 */
	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,		///< submode
			CCommonShaderPrograms &cCommonShaderPrograms	///< shaders
	)
	{
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 1 || i_visualization_render_cluster_borders % 3 == 2)
		{
			CSimulationTsunami_GridDataArrays cGridDataArrays(cTsunamiSimulationParameters.number_of_local_clusters*2);

			cDomainClusters.traverse_GenericTreeNode_Parallel(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t id = cGridDataArrays.getNextTriangleCellStartId(2);

					TTsunamiVertexScalar *v = &(cGridDataArrays.triangle_vertex_buffer[id*3*3]);

					v[0] = node->cTriangleFactory.vertices[0][0];
					v[1] = -node->cTriangleFactory.vertices[0][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v += 3;

					v[0] = node->cTriangleFactory.vertices[1][0];
					v[1] = -node->cTriangleFactory.vertices[1][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v += 3;

					v[0] = node->cTriangleFactory.vertices[1][0];
					v[1] = -node->cTriangleFactory.vertices[1][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v += 3;

					v[0] = node->cTriangleFactory.vertices[2][0];
					v[1] = -node->cTriangleFactory.vertices[2][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v += 3;

					v[0] = node->cTriangleFactory.vertices[2][0];
					v[1] = -node->cTriangleFactory.vertices[2][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v += 3;

					v[0] = node->cTriangleFactory.vertices[0][0];
					v[1] = -node->cTriangleFactory.vertices[0][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
				}
			);

			GLSL::vec3 c(0.8, 0.1, 0.1);
			cCommonShaderPrograms.cBlinn.use();
			cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
			cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
			cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);

				glDisable(GL_DEPTH_TEST);
				glDisable(GL_CULL_FACE);

				cOpenGL_Vertices_Cluster_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Cluster_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cTsunamiSimulationParameters.number_of_local_clusters*3*2
					);

				cOpenGL_Vertices_Cluster_Root_Tsunami.shutdownRendering();

				glEnable(GL_DEPTH_TEST);
				glEnable(GL_CULL_FACE);

			cCommonShaderPrograms.cBlinn.disable();
		}
	}



	/**
	 * render the cluster scan information
	 */
	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
#if CONFIG_ENABLE_SCAN_DATA
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 2)
		{
			cCommonShaderPrograms.cBlinn.use();

			cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

				cDomainClusters.traverse_GenericTreeNode_Serial(
					[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

						CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar vertex_buffer[3*3];

						int thread_mod_id;
						if (i_cGenericTreeNode->workload_thread_id == -1)
							thread_mod_id = 6;
						else
							thread_mod_id = (i_cGenericTreeNode->workload_thread_id+1) % 6;

						static const CTsunamiSimulationStacksAndTypes::CVisualizationTypes::TVisualizationVertexScalar ct[7][3] = {
								{1.0, 0.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 0.0, 1.0},
								{1.0, 1.0, 0.0},
								{1.0, 0.0, 1.0},
								{0.0, 1.0, 1.0},
								{1.0, 1.0, 1.0},
						};

						GLSL::vec3 c(ct[thread_mod_id][0], ct[thread_mod_id][1], ct[thread_mod_id][2]);

						cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);


						TTsunamiVertexScalar *v = vertex_buffer;

						v[0] = node->cTriangleFactory.vertices[0][0];
						v[1] = -node->cTriangleFactory.vertices[0][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);
						v += 3;

						v[0] = node->cTriangleFactory.vertices[1][0];
						v[1] = -node->cTriangleFactory.vertices[1][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);
						v += 3;

						v[0] = node->cTriangleFactory.vertices[2][0];
						v[1] = -node->cTriangleFactory.vertices[2][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);


						static const TTsunamiVertexScalar normals[3][3] = {
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0}
						};

						cOpenGL_Vertices_Cell_Root_Tsunami.render(vertex_buffer, &(normals[0][0]), 3);
					}
				);

				CGlErrorCheck();

			cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();

			cCommonShaderPrograms.cBlinn.disable();
		}
#endif
	}



	/**
	 * render a smoothed surface using node based parallelization
	 */
	void p_render_surfaceSmooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if 0

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
	#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cShaderBlinn.use();
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CCluster_TreeNode_,
			CSimulationTsunami_Cluster,
			sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<CTsunamiSimulationStacksAndTypes,0>::TRAV,
			CTsunamiCTsunamiSimulationStacksAndTypes::CVisualizationTypes::CVertexData,
			CStackAccessorMethodsTsunamiVertexData<CCluster_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
			&CSimulationTsunami_Cluster::cOpenGL_Vertices_Smooth_Cell_Tsunami,
			&CSimulationTsunami_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
			rootGenericTreeNode,
			[&](CCluster_TreeNode_ *node)
			{
#if 0
	// TODO
			node->cCluster->cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(
					visualization_water_surface_default_displacement, visualization_water_surface_scale_factor,
					&cOpenGL_Vertices_Cell_Root_Tsunami
			);
#endif
			}
		);

		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	#endif

#else

		cShaderBlinn.use();
		cOpenGL_Vertices_Cell_Root_Tsunami.initRendering();

		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Cell_Tsunami<2>::TRAV cOpenGL_Vertices_Cell_Tsunami_aligned;

				cOpenGL_Vertices_Cell_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Cell_Tsunami_aligned.cKernelClass.setup(
						cTsunamiSimulationParameters.visualization_water_surface_default_displacement,
						cTsunamiSimulationParameters.visualization_water_surface_scale_factor,
						&cOpenGL_Vertices_Cell_Root_Tsunami
					);

				cOpenGL_Vertices_Cell_Tsunami_aligned.action(node->cStacks);
			}
		);

		cOpenGL_Vertices_Cell_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();

#endif

#endif
	}

};


#endif /* CSIMULATION_TSUNAMI_PARALLEL_HPP_ */
