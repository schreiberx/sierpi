#! /bin/bash

#EXEC="../../build/sierpi_intel_tbb_taskaffinities_tsunami_parallel_asagi_release"
#EXT_APPEND=""

EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_asagi_release"
EXT_APPEND="_rk1"

#EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_asagi_rk2_release"
#EXT_APPEND="_rk2"


D="22"
#A="2 4 6 8 10"
A="2"
CFL="0.5"

REFINE="1"
COARSEN="0.1"

EXT_APPEND="$EXT_APPEND""_refine${REFINE}_coarsen${COARSEN}"
echo $EXT_APPEND

PARAMS="-A 1 -y -1 -z -1 -L 20000 -v 5 -r $REFINE/$COARSEN -o $((16*1024))"

# output data after n simulation seconds
PARAMS="$PARAMS -B 10"

#PARAMS="$PARAMS -F 100"
PARAMS="$PARAMS"

DART_STATIONS="834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt"

#DART_STATIONS="834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt"


for d in $D; do

	for a in $A; do

		for cfl in $CFL; do

			OUTPUTFILE_EXTENSION="d""$d""_a""$a""_cfl""$cfl""$EXT_APPEND"
			DART_STATIONS_F=`echo "$DART_STATIONS" | sed "s/\([^/]*\)\.txt/output_$OUTPUTFILE_EXTENSION""_dart""\\1.txt/g"`

			# dart output
			_PARAMS=" $PARAMS -D $DART_STATIONS_F "

			# adaptivity
			_PARAMS=" $_PARAMS -a $a -d $d"
			
			# cfl
			_PARAMS=" $_PARAMS -C $cfl"

			echo "$EXEC $_PARAMS"
			$EXEC $_PARAMS
		done
	done
done
