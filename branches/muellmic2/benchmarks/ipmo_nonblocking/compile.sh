#! /bin/bash


DEFAULT_PARAMS=" --mode=release"

cd ../../

scons --threading=ipmo $DEFAULT_PARAMS
scons --threading=ipmo_async $DEFAULT_PARAMS
scons --threading=itbb $DEFAULT_PARAMS
scons --threading=itbb_async $DEFAULT_PARAMS
scons --threading=omp $DEFAULT_PARAMS
scons --threading=tbb $DEFAULT_PARAMS
