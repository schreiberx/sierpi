#! /bin/bash

. ../tools.sh
. ../inc_environment_vars.sh

#THREADS=`seq 1 4`
THREADS=12
PROBLEM_SIZES_IN_DEPTH_START=8
PROBLEM_SIZES_IN_DEPTH=`seq $PROBLEM_SIZES_IN_DEPTH_START 2 16`

EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

# no adaptive refinement
PARAMS="-a 0"

CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`

CPULIST="0"
for i in `seq 1 $((CPUS-1))`; do
	CPULIST+=" $i"
done

echo "CPUS: $CPULIST"

PROCLIST=$(echo $CPULIST | sed "y/ /,/")


for p in $PROBLEM_SIZES_IN_DEPTH; do
	echo
	echo "THREADS	INITIAL_DEPTH	MTPS	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
	for THREADS in 1 2 4 6 8 10 12; do
		PARAMS_="-r -d $p -n $THREADS $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		OUTPUT="`$EXEC_CMD`"

		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo "\$$VARNAME")
		SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

		AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
		AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
		TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
		TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`

		echo "$THREADS	$p	$MTPS	$SPEEDUP_PER	$TIMESTEPS	$TIMESTEP_SIZE	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
	done
done
