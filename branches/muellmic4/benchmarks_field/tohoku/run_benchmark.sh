#! /bin/bash

#
XEC="../../build/sierpi_intel_tbb_taskaffinities_tsunami_parallel_asagi_release"
#EXT_APPEND=""

#EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_asagi_release"
EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_simple_netcdf_b0_release"
#EXEC="../../build/sierpi_intel_tbb_scan_tsunami_parallel_asagi_release"

XML_CONFIG_FILE="../../scenarios/netcdf_intel_release.xml"
SIMULATION_TIME=20000

D="14 16 18 20 22 24 26"
A="0 2 4 6 8 10"

D="16"
A="4"

#A="0"
CFL="0.5"

USE_NCORES=64

MAX_DEPTH=26

REFINE="1"
COARSEN="0.1"

EXT_APPEND="_rk1_refine${REFINE}_coarsen${COARSEN}"


PARAMS="-c $XML_CONFIG_FILE -A 1 -t $SIMULATION_TIME -r $REFINE/$COARSEN"

PARAMS=" $PARAMS -v 3 "

SPLIT_SIZE=$((16*1024))
#SPLIT_SIZE=$((4*1024))
PARAMS="$PARAMS -o $SPLIT_SIZE"

PARAMS="$PARAMS -u 6 -U 6"

# output data (e. g. dart stations) after n simulation seconds
PARAMS="$PARAMS -b 10"


DART_STATIONS="834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt"

#DART_STATIONS="834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt"


for d in $D; do
	for a in $A; do
		[ $((d+a)) -gt $MAX_DEPTH ] && continue

		for cfl in $CFL; do

			OUTPUTFILE_EXTENSION="d""$d""_a""$a""_cfl""$cfl""$EXT_APPEND"
			DART_STATIONS_F=`echo "$DART_STATIONS" | sed "s/\([^/]*\)\.txt/output_$OUTPUTFILE_EXTENSION""_dart""\\1.txt/g"`
			echo $DART_STATIONS_F

			# dart station values output
			_PARAMS=" $PARAMS -D $DART_STATIONS_F"

			# adaptivity
			_PARAMS=" $_PARAMS -a $a -d $d"
			
			# cfl
			_PARAMS=" $_PARAMS -C $cfl"

			E="$EXEC $_PARAMS"
			echo "$E"

			$E
#			srun --nice=10000 -c $USE_NCORES $E > "output_""$OUTPUTFILE_EXTENSION""_EXEC.txt" &
			#$E
		done
	done
done
