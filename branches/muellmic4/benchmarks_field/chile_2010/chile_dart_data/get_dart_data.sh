#!/bin/bash
#
# @file
# This file is part of the tsunami repository.
#
# @author Alexander Breuer (breuera AT in.tum.de, http://www5.in.tum.de/wiki/index.php/Dipl.-Math._Alexander_Breuer)
#
# @section LICENSE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# @section DESCRIPTION
#
# Downloads specified dart station data.

year=2010
dart_stations=(32411 32412 43412 51406 46412 51407)

echo "Number of defined dart stations: ${#dart_stations[*]}"

for dart_station in ${dart_stations[*]}
do
  wget "http://www.ndbc.noaa.gov/data/historical/dart/"$dart_station"t"$year".txt.gz"
  gzip -d $dart_station"t"$year".txt.gz"
done
