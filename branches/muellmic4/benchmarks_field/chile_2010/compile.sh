#! /bin/bash

cd ../..
make clean

scons --xml-config=./scenarios/chile_netcdf_intel_release.xml --enable-exit-on-instability-thresholds=on -j 8
