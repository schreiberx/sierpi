/*
 * CMatrixOperations.hpp
 *
 *  Created on: Sep 1, 2012
 *      Author: schreibm
 */

#ifndef CMATRIX_OPERATIONS_HPP_
#define CMATRIX_OPERATIONS_HPP_



class CMatrixOperations
{
public:
	/**
	 * compute the inverse of the matrix and return the determinant
	 */
	template <typename T, int N>
	static T getInverse(
			const T i_matrix[N][N],	///< input matrix
			T o_matrix[N][N]			///< inverse of input matrix
	)	{
		T determinant = 1.0;

		// allocate on HEAP to allow inversion of large matrices as well
		T *m = new T[N*N];

		// copy input matrix to matrix space for gauss elimination
		memcpy(m, i_matrix, sizeof(T)*N*N);

		/*
		 * setup identity matrix
		 */
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
				o_matrix[i][j] = 0.0;
			o_matrix[i][i] = 1.0;
		}

		for (int current_row = 0; current_row < N; current_row++)
		{
			/*
			 * search for pivoting row
			 */
			int pivot_row = current_row;

			for (int i = pivot_row+1; i < N; i++)
			{
				if (std::abs(m[i*N+current_row]) > std::abs(m[pivot_row*N+current_row]))
					pivot_row = i;
			}

			/*
			 * exchange rows when pivoting should be used
			 */
			if (pivot_row != current_row)
			{
				for (int j = current_row; j < N; j++)
					std::swap(m[current_row*N+j], m[pivot_row*N+j]);

				for (int j = 0; j < N; j++)
					std::swap(o_matrix[current_row][j], o_matrix[pivot_row][j]);
			}

			T pivot = m[current_row*N+current_row];

			determinant *= pivot;

			T inv_pivot = (T)1.0 / pivot;

			/*
			 * divide current line by pivot
			 */

			// removed, since it's unused data
			//m[current_row*N+current_row] = (T)1.0;

			for (int j = current_row+1; j < N; j++)
				m[current_row*N+j] *= inv_pivot;

			for (int j = 0; j < N; j++)
				o_matrix[current_row][j] *= inv_pivot;


			/*
			 * divide rows except current row by
			 */
			for (int i = 0; i < N; i++)
			{
				if (i == current_row)
					continue;

				// load row base value
				T base_value = m[i*N+current_row];

				// removed, since it's unused data
				//m[i*N+current_row] = 0.0;

				for (int j = current_row+1; j < N; j++)
					m[i*N+j] -= base_value * m[current_row*N+j];


				for (int j = 0; j < N; j++)
					o_matrix[i][j] -= base_value * o_matrix[current_row][j];
			}
		}

		delete[] m;
		return determinant;
	}


	/**
	 * matrix-matrix multiplication
	 */
	template <typename T, int N>
	static void multiply(
			T i_m1[N][N],
			T i_m2[N][N],
			T o_m[N][N]
	)	{
		multiply<T,N>(&i_m1[0][0], &i_m2[0][0], &o_m[0][0]);
	}

	/**
	 * matrix-matrix multiplication
	 */
	template <typename T, int N>
	static void multiply(
			T *i_m1,
			T *i_m2,
			T *o_m
	)	{
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
			{
				o_m[i*N+j] = 0.0;

				for (int k = 0; k < N; k++)
					o_m[i*N+j] += i_m1[i*N+k]*i_m2[k*N+j];
			}
		}
	}


	/**
	 * output matrix
	 */
	template <typename T, int N>
	static void print(
			T i_m[N][N]
	)	{
		print<T,N>(&i_m[0][0]);
	}


	/**
	 * output matrix
	 */
	template <typename T, int N>
	static void print(
			T *i_m
	)	{
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < N; j++)
				std::cout << i_m[i*N+j] << "\t";
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}


	/**
	 * output matrix
	 */
	template <typename T, int N, int M>
	static void printNxM(
			T *i_m
	)	{
		for (int i = 0; i < N; i++)
		{
			for (int j = 0; j < M; j++)
				std::cout << i_m[i*M+j] << "\t";
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
};

#endif /* CMATRIX_OPS_HPP_ */
