/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CKERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_HPP_
#define CKERNEL_SETUP_ELEMENT_DATA_0TH_ORDER_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_euler/types/CTypes.hpp"

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData_Depth.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "../../CDatasets.hpp"


namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 1st order
 */
class CSetup_CellData_0thOrder
{
public:
	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData_Depth<CSetup_CellData_0thOrder, CHyperbolicTypes>	TRAV;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef typename CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;


	/*
	 * dataset accessor
	 */
	CDatasets *cDatasets;



	CSetup_CellData_0thOrder()	:
		cDatasets(nullptr)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void op_cell(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CHyperbolicTypes::CSimulationTypes::CCellData *io_cellData
	)
	{
		TVertexScalar mx, my;

		CTriangle_Tools::computeAdaptiveSamplingPoint(
			i_vertex_left_x, i_vertex_left_y,
			i_vertex_right_x, i_vertex_right_y,
			i_vertex_top_x, i_vertex_top_y,
			&mx, &my
		);

		T lod = CTriangle_Tools::getLODFromDepth(i_depth);

		cDatasets->getNodalData(mx, my, lod, &(io_cellData->dofs[0]));

		io_cellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
	}


	void setup_Parameters(
			CDatasets *i_cSimulationScenarios
	)
	{
		cDatasets = i_cSimulationScenarios;
	}



	void setup_WithKernel(
			CSetup_CellData_0thOrder &parent
	)
	{
		cDatasets = parent.cDatasets;
	}


};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */

