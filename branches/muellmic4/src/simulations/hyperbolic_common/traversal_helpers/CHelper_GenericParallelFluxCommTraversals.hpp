/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_

#include "config.h"
#include "libsierpi/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/cluster/CCluster_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals
{
public:

	/**
	 * run the edge comm traversals
	 */
	template<
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T							/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size
	)
	{
		typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

		/*
		 * first EDGE COMM TRAVERSAL pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// A) push conserved variables to stacks
					// B) compute local net updates
					(node->cCluster->*(i_simulationSubClass)).actionFirstPass(node->cStacks);
				}
		);

		T cfl_reduce_value = std::numeric_limits<T>::infinity();

#if CONFIG_ENABLE_MPI

#error "TODO: IMPLEMENT CFL FIX"
		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass1();
			}
		);

		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[=](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using uniqueIDs as relation and compute the flux
				(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
			}
		);
#endif


		/*
		 * middle pass: pull edge comm data & compute global timestep size
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode, T *o_reduceValue)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// pull edge communication data from adjacent sub-clusters and compute fluxes
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluationSharedMemory();

					T tmp;

					// run computation based on newly set-up stacks
					(node->cCluster->*(i_simulationSubClass)).actionMiddlePass_computeClusterBorderCFL(node->cStacks, &tmp);

					// store local timestep to cluster
					tmp *= i_cfl_value;
					node->cCluster->local_timestep_size = tmp;

					// return local timestep size
					*o_reduceValue = tmp;
				},
				&(sierpi::CReduceOperators::MIN<T>),	// use minimum since the minimum timestep has to be selected
				&cfl_reduce_value
			);

		CONFIG_DEFAULT_FLOATING_POINT_TYPE t = cfl_reduce_value;
		*o_timestep_size = t;



		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// run computation based on newly set-up stacks
					(node->cCluster->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, t);
				}
			);
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
