/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_0THORDER_HPP_

#include <limits>
#include <stdexcept>

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"






namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CCommon_Adaptivity_Order_0
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	// typedef for CAdaptivity_default_handlers (see include at end of this file)
	typedef CCommon_Adaptivity_Order_0 CAdaptivity_Common;
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/common/CAdaptivity_default_handlers.hpp"


	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CDatasets *cDatasets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;


	/**
	 * constructor
	 */
	CCommon_Adaptivity_Order_0()	:
		cathetus_side_length(-1),
		cDatasets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	inline void updateAfterInterpolation(
			CNodeData *io_cNodeData
	)	{

	}


	/**
	 * setup both refined elements
	 */
	inline void refine(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T i_normal_hypx,	T i_normal_hypy,
			T i_normal_rightx,	T i_normal_righty,
			T i_normal_leftx,	T i_normal_lefty,

			int i_parent_depth,

			CCellData *i_cell_data,
			CCellData *o_left_cell_data,
			CCellData *o_right_cell_data
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cell_data->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		assert (cDatasets != nullptr);

		T lmx, lmy, rmx, rmy;

		CTriangle_Tools::computeAdaptiveSamplingPointForLeftAndRightChild(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,

				&lmx, &lmy,
				&rmx, &rmy
			);

		T lod = CTriangle_Tools::getLODFromDepth(i_parent_depth+1);

		if (i_cell_data->dofs.h[0] <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
		{
			if (i_cell_data->dofs.h[0] < 0)
				std::cerr << "Negative h: " << i_cell_data->dofs.h[0] << std::endl;

			o_left_cell_data->dofs.b[0] = cDatasets->getDatasetValue(0, lmx, lmy, lod);
			o_left_cell_data->dofs.h[0] = std::max((T)0.0,  -o_left_cell_data->dofs.b[0]);
			o_left_cell_data->dofs.hu[0] = 0;
			o_left_cell_data->dofs.hv[0] = 0;

			o_right_cell_data->dofs.b[0] = cDatasets->getDatasetValue(0, rmx, rmy, lod);
			o_right_cell_data->dofs.h[0] = std::max((T)0.0,  -o_right_cell_data->dofs.b[0]);
			o_right_cell_data->dofs.hu[0] = 0;
			o_right_cell_data->dofs.hv[0] = 0;
		}
		else
		{
			T h_horizon = i_cell_data->dofs.b[0] + i_cell_data->dofs.h[0];

			o_left_cell_data->dofs.b[0] = cDatasets->getDatasetValue(0, lmx, lmy, lod);
			o_left_cell_data->dofs.h[0] = std::max((T)0.0,  -o_left_cell_data->dofs.b[0] + h_horizon);

#if 0

			/*
			 * conserve momentum
			 */
			o_left_cell_data->dofs.hu[0] = i_cell_data->dofs.hu[0];
			o_left_cell_data->dofs.hv[0] = i_cell_data->dofs.hv[0];

			o_right_cell_data->dofs.hu[0] = i_cell_data->dofs.hu[0];
			o_right_cell_data->dofs.hv[0] = i_cell_data->dofs.hv[0];

#else

			/*
			 * conserve velocity
			 */
			if (o_left_cell_data->dofs.h[0] <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
			{
				o_left_cell_data->dofs.hu[0] = 0;
				o_left_cell_data->dofs.hv[0] = 0;
			}
			else
			{
				T t = o_left_cell_data->dofs.h[0]/i_cell_data->dofs.h[0];
				o_left_cell_data->dofs.hu[0] = i_cell_data->dofs.hu[0]*t;
				o_left_cell_data->dofs.hv[0] = i_cell_data->dofs.hv[0]*t;
			}

			o_right_cell_data->dofs.b[0] = cDatasets->getDatasetValue(0, rmx, rmy, lod);
			o_right_cell_data->dofs.h[0] = std::max((T)0.0,  -o_right_cell_data->dofs.b[0] + h_horizon);


			if (o_right_cell_data->dofs.h[0] <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
			{
				o_right_cell_data->dofs.hu[0] = 0;
				o_right_cell_data->dofs.hv[0] = 0;
			}
			else
			{
				T t = o_right_cell_data->dofs.h[0]/i_cell_data->dofs.h[0];
				o_right_cell_data->dofs.hu[0] = i_cell_data->dofs.hu[0]*t;
				o_right_cell_data->dofs.hv[0] = i_cell_data->dofs.hv[0]*t;
			}
#endif


			/*
			 * |\_
			 * |  \_
			 * |    \_
			 * |    / \_
			 * |  /     \_
			 * |/_________\
			 *
			 * use X-Axis for projection
			 */
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&o_left_cell_data->dofs.hu[0],
					&o_left_cell_data->dofs.hv[0],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);

			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&o_right_cell_data->dofs.hu[0],
					&o_right_cell_data->dofs.hv[0],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);
		}

//		o_left_cell_data->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
//		o_right_cell_data->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();

#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
		if (	std::abs(o_left_cell_data->dofs.hu[0]) > o_left_cell_data->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(o_left_cell_data->dofs.hv[0]) > o_left_cell_data->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(o_right_cell_data->dofs.hu[0]) > o_right_cell_data->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(o_right_cell_data->dofs.hv[0]) > o_right_cell_data->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY
		)
		{
			std::cerr << "setupRefinedElement: Velocity exceeded threshold value " << CONFIG_EXIT_THRESHOLD_MAX_VELOCITY << std::endl;

			std::cerr << "Parent cell data: " << std::endl;
			std::cerr << *i_cell_data << std::endl;

			std::cerr << "Left cell data: " << std::endl;
			std::cerr << *o_left_cell_data << std::endl;

			std::cerr << "Right cell data: " << std::endl;
			std::cerr << *o_right_cell_data << std::endl;

			throw(std::runtime_error("setupRefinedElement: Velocity exceeded threshold value"));
		}
#endif

#if DEBUG
		if (	std::isnan(o_left_cell_data->dofs.hu[0])	||
				std::isnan(o_left_cell_data->dofs.hv[0])	||

				std::isnan(o_right_cell_data->dofs.hu[0])	||
				std::isnan(o_right_cell_data->dofs.hv[0])	||

				std::isnan(i_cell_data->dofs.hu[0])			||
				std::isnan(i_cell_data->dofs.hv[0])
		)	{
			std::cerr << "refine instability" << std::endl;
			std::cerr << *i_cell_data << std::endl;
			std::cerr << *o_left_cell_data << std::endl;
			std::cerr << *o_right_cell_data << std::endl;
			exit(-1);
		}
#endif


#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_left_cell_data->refine = false;
		o_left_cell_data->coarsen = false;

		o_right_cell_data->refine = false;
		o_right_cell_data->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cell_data->validation.setupRefineLeftAndRight(
				i_normal_hypx,		i_normal_hypy,
				i_normal_rightx,	i_normal_righty,
				i_normal_leftx,		i_normal_lefty,
				i_parent_depth,
				&o_left_cell_data->validation,
				&o_right_cell_data->validation
			);
#endif

	}


	/**
	 * setup coarsed elements
	 */
	inline void coarsen(
			T vleft_x, T vleft_y,
			T vright_x, T vright_y,
			T vtop_x, T vtop_y,

			T normal_hypx,		T normal_hypy,
			T normal_rightx,	T normal_righty,
			T normal_leftx,	T normal_lefty,

			int i_depth,

			CCellData *o_cellData,
			CCellData *i_left_cellData,
			CCellData *i_right_cellData
	)
	{
		assert(o_cellData != i_left_cellData);
		assert(o_cellData != i_right_cellData);
		assert(i_left_cellData != i_right_cellData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		{
			T mx = (vleft_x + vright_x)*(T)0.5;
			T my = (vleft_y + vright_y)*(T)0.5;

			i_left_cellData->validation.testVertices(vtop_x, vtop_y, vleft_x, vleft_y, mx, my);
			i_right_cellData->validation.testVertices(vright_x, vright_y, vtop_x, vtop_y, mx, my);
		}
#endif

		assert (cDatasets != nullptr);

		T mx, my;
		CTriangle_Tools::computeAdaptiveSamplingPoint(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				&mx, &my
			);

#if 0
		// add 0.5 to match sampling rate
		T lod = getLODFromDepth(i_depth);

		T b = cDatasets->getDatasetData(mx, my, lod);

//		assert(b >= i_left_cellData->dofs.b[0] || b >= i_right_cellData->dofs.b[0]);
//		assert(b <= i_left_cellData->dofs.b[0] || b <= i_right_cellData->dofs.b[0]);

#else

		T b = (i_left_cellData->dofs.b[0] + i_right_cellData->dofs.b[0])*(T)0.5;

#endif




		o_cellData->dofs.b[0] = b;

		T sum_horizon = 0;

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
		T sum_hu = 0;
		T sum_hv = 0;
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
		T sum_u = 0;
		T sum_v = 0;
#endif

		T sum_div = 0;

		if (i_left_cellData->dofs.h[0] >= SIMULATION_TSUNAMI_DRY_THRESHOLD)
		{
			T tmp_hu = i_left_cellData->dofs.hu[0];
			T tmp_hv = i_left_cellData->dofs.hv[0];

			/*
			 * project from left child to coarse cell basis
			 *
			 *
			 * |\
			 * |  \
			 * | L  \
			 * |------
			 * |    /
			 * |  /
			 * |/
			 */
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&tmp_hu,
					&tmp_hv,
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);

			sum_horizon = i_left_cellData->dofs.h[0] + i_left_cellData->dofs.b[0];

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
			sum_hu = tmp_hu;
			sum_hv = tmp_hv;
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
			T inv_h = (T)1.0/i_left_cellData->dofs.h[0];
			sum_u = tmp_hu * inv_h;
			sum_v = tmp_hv * inv_h;
#endif

			sum_div++;
		}

		if (i_right_cellData->dofs.h[0] >= SIMULATION_TSUNAMI_DRY_THRESHOLD)
		{
			T tmp_hu = i_right_cellData->dofs.hu[0];
			T tmp_hv = i_right_cellData->dofs.hv[0];

			/*
			 * project from right child to coarse cell basis
			 *
			 *      /|\
			 *    /  |  \
			 *  /    | R  \
			 * ------|------
			 */
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&tmp_hu,
					&tmp_hv,
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);

			sum_horizon += i_right_cellData->dofs.h[0] + i_right_cellData->dofs.b[0];

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
			sum_hu += tmp_hu;
			sum_hv += tmp_hv;
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
			T inv_h = (T)1.0/i_right_cellData->dofs.h[0];
			sum_u += tmp_hu * inv_h;
			sum_v += tmp_hv * inv_h;
#endif
			sum_div++;
		}

		if (sum_div == 0)
		{
			o_cellData->dofs.h[0] = 0;
			o_cellData->dofs.hu[0] = 0;
			o_cellData->dofs.hv[0] = 0;
		}
		else
		{
			o_cellData->dofs.h[0] = sum_horizon/sum_div - o_cellData->dofs.b[0];

			if (o_cellData->dofs.h[0] <= 0)
			{
				o_cellData->dofs.h[0] = 0;
				o_cellData->dofs.hu[0] = 0;
				o_cellData->dofs.hv[0] = 0;
			}
			else
			{
#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
				o_cellData->dofs.hu[0] = sum_hu/sum_div;
				o_cellData->dofs.hv[0] = sum_hv/sum_div;
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
				o_cellData->dofs.hu[0] = sum_u/sum_div*o_cellData->dofs.h[0];
				o_cellData->dofs.hv[0] = sum_v/sum_div*o_cellData->dofs.h[0];
#endif
			}
		}
/*
		o_cellData->dofs.hu[0] = 0;
		o_cellData->dofs.hv[0] = 0;
*/
//		o_cellData->cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();


#if CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
		if (	std::abs(o_cellData->dofs.hu[0]) > o_cellData->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY	||
				std::abs(o_cellData->dofs.hv[0]) > o_cellData->dofs.h[0]*CONFIG_EXIT_THRESHOLD_MAX_VELOCITY
		)
		{
			std::cerr << "setupCoarsenedElement: Velocity exceeded threshold value" << CONFIG_EXIT_THRESHOLD_MAX_VELOCITY << std::endl;

			std::cerr << "Parent cell data: " << std::endl;
			std::cerr << *o_cellData << std::endl;

			std::cerr << "Left cell data: " << std::endl;
			std::cerr << *i_left_cellData << std::endl;

			std::cerr << "Right cell data: " << std::endl;
			std::cerr << *i_right_cellData << std::endl;

			throw(std::runtime_error("setupCoarsenedElement: Velocity exceeded threshold value"));
		}
#endif



#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
		o_cellData->refine = false;
		o_cellData->coarsen = false;
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cellData->validation.setupCoarsen(
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				i_depth,
				&i_left_cellData->validation,
				&i_right_cellData->validation
		);
#endif
	}

};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
