/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 *      		Stjepan Bakrac <bakrac@in.tum.de>
 *      		Philipp Müller <philippausmuensing@googlemail.com>
 */

#ifndef CEDGECOMM_TSUNAMI_ORDER_N_HPP_
#define CEDGECOMM_TSUNAMI_ORDER_N_HPP_


#include <cmath>
#include <limits>

// Generic types
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

// Traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// Flux solvers
#include "../../../subsimulation_tsunami/flux_solver/CFluxSolver.hpp"

// Edge projections
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"

// Triangle side lengths
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

// Triangle side lengths
#include "libsierpi/triangle/CTriangle_Tools.hpp"

// PDE lab stuff
#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"

// Enum for boundary conditions
#include "libsierpi/grid/CBoundaryConditions.hpp"

// Default parameters for tsunami edge comm
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/simulation/CEdgeComm_Parameters.hpp"

#include "../../../basis_functions_and_matrices/CComputation_Matrices.hpp"

// Check for set buoys
#include "../../../CParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (op_cell)
 */

template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Hyperbolic_Order_N	:
public CEdgeComm_Parameters	// generic configurations (gravitational constant, timestep size, ...)
{

public:
	using CEdgeComm_Parameters::setTimestepSize;
	using CEdgeComm_Parameters::setGravitationalConstant;
	using CEdgeComm_Parameters::setBoundaryDirichlet;
	using CEdgeComm_Parameters::setParameters;


	/**
	 * typedefs for convenience
	 */
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Hyperbolic_Order_N<t_storeElementUpdatesOnly>, CHyperbolicTypes > TRAV;

	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,2> CGaussTriangleEdge;
	//	typedef sierpi::pdelab::CGaussQuadrature2D_TriangleArea<T,2> CGaussTriangleArea;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Hyperbolic_Order_N()
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			boundary_dirichlet.dofs.h[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.hu[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.hv[i] = std::numeric_limits<T>::infinity();
			boundary_dirichlet.dofs.b[i] = std::numeric_limits<T>::infinity();
		}
	}


	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CCellData &i_f_t0,
			CCellData &i_f_slope_t0,
			CCellData *o_f_t1_approx,
			T i_timestep_size
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_f_t1_approx->dofs.h[i]	= i_f_t0.dofs.h[i]	+ i_timestep_size*i_f_slope_t0.dofs.h[i];
			o_f_t1_approx->dofs.hu[i]	= i_f_t0.dofs.hu[i]	+ i_timestep_size*i_f_slope_t0.dofs.hu[i];
			o_f_t1_approx->dofs.hv[i]	= i_f_t0.dofs.hv[i]	+ i_timestep_size*i_f_slope_t0.dofs.hv[i];
			o_f_t1_approx->dofs.b[i]	= i_f_t0.dofs.b[i];
		}

		//		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CCellData &i_f_slope_t0,
			CCellData &i_f_slope_t1,
			CCellData *io_f_t0_t1,
			T i_timestep_size
	)	{

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			io_f_t0_t1->dofs.h[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.h[i] + i_f_slope_t1.dofs.h[i]);
			io_f_t0_t1->dofs.hu[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.hu[i] + i_f_slope_t1.dofs.hu[i]);
			io_f_t0_t1->dofs.hv[i]	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.hv[i] + i_f_slope_t1.dofs.hv[i]);
		}

		// TODO: which CFL to use?
		//		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Hyperbolic_Order_N &i_kernel
	)	{
		// copy configuration
		(CEdgeComm_Parameters&)(*this) = (CEdgeComm_Parameters&)(i_kernel);
	}

#if 0
	/**
	 * compute nodal data for edge based communication on hypotenuse
	 */
	template <int edge_id>
	static inline void compute_edge_comm_nodal_data(
			const CCellData *i_cCellData,
			CEdgeData *o_cEdgeData
	)	{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * compute DOF values at sampling points by computing the sum over all basis functions
			 */
			o_cEdgeData->dofs.h[i] = 0;
			o_cEdgeData->dofs.hu[i] = 0;
			o_cEdgeData->dofs.hv[i] = 0;
			o_cEdgeData->dofs.b[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T weight = CComputation_Matrices::edge_comm_data_nodal_weight_matrix[edge_id][i][j];

				o_cEdgeData->dofs.h[i]	+= weight * i_cCellData->dofs.h[j];
				o_cEdgeData->dofs.hu[i]	+= weight * i_cCellData->dofs.hu[j];
				o_cEdgeData->dofs.hv[i]	+= weight * i_cCellData->dofs.hv[j];
				o_cEdgeData->dofs.b[i]	+= weight * i_cCellData->dofs.b[j];
			}
		}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,edge_id>(
					&(o_cEdgeData->dofs.hu[i]),
					&(o_cEdgeData->dofs.hv[i])
			);
		}
	}
#endif


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// H
		CComputation_Matrices::mul_edge_comm_hyp_data_nodal_weight_matrix(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CComputation_Matrices::mul_edge_comm_hyp_data_nodal_weight_matrix(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CComputation_Matrices::mul_edge_comm_hyp_data_nodal_weight_matrix(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);
#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		CComputation_Matrices::mul_edge_comm_transform_to_edge_space(i_cCellData->hypEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CComputation_Matrices::mul_edge_comm_hyp_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif
		// B
		CComputation_Matrices::mul_edge_comm_hyp_data_nodal_weight_matrix(i_cCellData->dofs.b, o_cEdgeData->dofs.b);

#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupHypEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// H
		CComputation_Matrices::mul_edge_comm_right_data_nodal_weight_matrix(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CComputation_Matrices::mul_edge_comm_right_data_nodal_weight_matrix(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CComputation_Matrices::mul_edge_comm_right_data_nodal_weight_matrix(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);

#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		CComputation_Matrices::mul_edge_comm_transform_to_edge_space(i_cCellData->rightEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CComputation_Matrices::mul_edge_comm_right_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif

		// B
		CComputation_Matrices::mul_edge_comm_right_data_nodal_weight_matrix(i_cCellData->dofs.b, o_cEdgeData->dofs.b);

#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRightEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		// H
		CComputation_Matrices::mul_edge_comm_left_data_nodal_weight_matrix(i_cCellData->dofs.h, o_cEdgeData->dofs.h);

		// HU, HV
		CComputation_Matrices::mul_edge_comm_left_data_nodal_weight_matrix(i_cCellData->dofs.hu, o_cEdgeData->dofs.hu);
		CComputation_Matrices::mul_edge_comm_left_data_nodal_weight_matrix(i_cCellData->dofs.hv, o_cEdgeData->dofs.hv);

#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		CComputation_Matrices::mul_edge_comm_transform_to_edge_space(i_cCellData->leftEdgeRotationMatrix, i_cCellData->inverseTransformationMatrix, o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#else
		CComputation_Matrices::mul_edge_comm_left_project_to_edge_space(o_cEdgeData->dofs.hu, o_cEdgeData->dofs.hv);
#endif

		// B
		CComputation_Matrices::mul_edge_comm_left_data_nodal_weight_matrix(i_cCellData->dofs.b, o_cEdgeData->dofs.b);

#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
		o_cEdgeData->CFL1_scalar = i_cCellData->incircleRadius;
#else
		o_cEdgeData->CFL1_scalar = getCFLCellFactor(i_depth);
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupLeftEdgeCommData(&(o_cEdgeData->validation));
		o_cEdgeData->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_hyp(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);

			/*
			 * apply bounce back boundary condition only on component perpendicular to boundary!
			 */
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			break;


		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CTsunamiSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
							&o_cEdgeData->dofs.hu[i],
							&o_cEdgeData->dofs.hv[i],
							-i_right_normal_x,
							-i_right_normal_y
					);


					CTriangle_VectorProjections::toEdgeSpace<T,0>(
							&(o_cEdgeData->dofs.hu[i]),
							&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					op_cell_to_edge_hyp(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
					);

					break;
				}
			}
			break;
		}



		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			o_cEdgeData->dofs.hv[i] = -o_cEdgeData->dofs.hv[i];
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cCellData->validation.setupHypEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			// TODO
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_right(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			break;

		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CTsunamiSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
							&o_cEdgeData->dofs.hu[i],
							&o_cEdgeData->dofs.hv[i],
							-i_right_normal_x,
							-i_right_normal_y
					);

					CTriangle_VectorProjections::toEdgeSpace<T,1>(// TODO adapt to distortion mode?
							&(o_cEdgeData->dofs.hu[i]),
							&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					// default: outflow boundary condition
					op_cell_to_edge_right(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
					);
					break;
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			o_cEdgeData->dofs.hv[i] = -o_cEdgeData->dofs.hv[i];
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cCellData->validation.setupRightEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x,	T i_hyp_normal_y,
			T i_right_normal_x,	T i_right_normal_y,
			T i_left_normal_x,	T i_left_normal_y,

			int i_depth,
			const CCellData *i_cCellData,

			CEdgeData *o_cEdgeData
	)	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_cEdgeData = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			op_cell_to_edge_left(
					i_hyp_normal_x,		i_hyp_normal_y,
					i_right_normal_x,	i_right_normal_y,
					i_left_normal_x,	i_left_normal_y,

					i_depth,
					i_cCellData,
					o_cEdgeData
			);
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
				o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			break;

		case BOUNDARY_CONDITION_DATASET:
			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
			{
				CTsunamiSimulationNodeData n;
				if (cDatasets->getBoundaryData(0, 0, CTriangle_Tools::getLODFromDepth(i_depth), &n))
				{
					o_cEdgeData->dofs.h[i] = n.h;
					o_cEdgeData->dofs.hu[i] = n.hu;
					o_cEdgeData->dofs.hv[i] = n.hv;
					o_cEdgeData->dofs.b[i] = n.b;

					CTriangle_VectorProjections::worldToReference(
							&o_cEdgeData->dofs.hu[i],
							&o_cEdgeData->dofs.hv[i],
							-i_right_normal_x,
							-i_right_normal_y
					);

					CTriangle_VectorProjections::toEdgeSpace<T,2>(// TODO adapt to distortion mode?
							&(o_cEdgeData->dofs.hu[i]),
							&(o_cEdgeData->dofs.hv[i])
					);
				}
				else
				{
					// default: outflow boundary condition
					op_cell_to_edge_left(
							i_hyp_normal_x,		i_hyp_normal_y,
							i_right_normal_x,	i_right_normal_y,
							i_left_normal_x,	i_left_normal_y,

							i_depth,
							i_cCellData,
							o_cEdgeData
					);
					break;
				}
			}
			break;
		}

		/*
		 * invert direction since this edge comm data is assumed to be streamed from the adjacent cell
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_cEdgeData->dofs.hu[i] = -o_cEdgeData->dofs.hu[i];
			o_cEdgeData->dofs.hv[i] = -o_cEdgeData->dofs.hv[i];
		}

		o_cEdgeData->CFL1_scalar = getBoundaryCFLCellFactor<T>(i_depth);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cEdgeData->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cCellData->validation.setupLeftEdgeCommData(&o_cEdgeData->validation);
#endif
	}



	/**
	 * compute flux components
	 */
	template <int N>
	inline void p_compute_fluxes(
			const CTsunamiSimulationNodeDataSOA<N> &i_node_data,	///< nodal data
			CTsunamiSimulationNodeDataSOA<N> *o_flux_data_x,		///< flux x-component
			CTsunamiSimulationNodeDataSOA<N> *o_flux_data_y			///< flux y-component
	)	{

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_flux_data_x->h[i] = i_node_data.hu[i];
			o_flux_data_x->hu[i] = (i_node_data.hu[i]*i_node_data.hu[i])/i_node_data.h[i] + (T)0.5*gravitational_constant*i_node_data.h[i]*i_node_data.h[i];
			o_flux_data_x->hv[i] = (i_node_data.hu[i]*i_node_data.hv[i])/i_node_data.h[i];

			o_flux_data_y->h[i] = i_node_data.hv[i];
			o_flux_data_y->hu[i] = (i_node_data.hu[i]*i_node_data.hv[i])/i_node_data.h[i];
			o_flux_data_y->hv[i] = (i_node_data.hv[i]*i_node_data.hv[i])/i_node_data.h[i] + (T)0.5*gravitational_constant*i_node_data.h[i]*i_node_data.h[i];
		}

#if SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			if (i_node_data.h[i] <= SIMULATION_TSUNAMI_DRY_THRESHOLD)
			{
				o_flux_data_x->h[i] = 0;
				o_flux_data_x->hu[i] = 0;
				o_flux_data_x->hv[i] = 0;

				o_flux_data_y->h[i] = 0;
				o_flux_data_y->hu[i] = 0;
				o_flux_data_y->hv[i] = 0;
			}
		}
#endif

	}



	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			int i_depth,

			CCellData *io_cCellData,				///< cell data

			CEdgeData *i_hyp_edge_net_update,		///< incoming flux from hypotenuse
			CEdgeData *i_right_edge_net_update,		///< incoming flux from right edge
			CEdgeData *i_left_edge_net_update		///< incoming flux from left edge
	)	{

#if 0
		std::cout << std::endl;
		std::cout << "++++++ CELL DATA +++++++" << std::endl;
		std::cout << *io_cCellData << std::endl;

		std::cout << std::endl;
		std::cout << *i_hyp_edge_net_update << std::endl;
		std::cout << *i_right_edge_net_update << std::endl;
		std::cout << *i_left_edge_net_update << std::endl;
#endif



#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION

		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cCellData->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
		);
#endif

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1

			// --- hyp edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->hypEdgeRotationMatrix,
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
			);

			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
			);


			// --- right edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->rightEdgeRotationMatrix,
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
			);

			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
			);


			// --- left edge transformation ---
			CTriangle_VectorProjections::fromEdgeSpace(
					io_cCellData->leftEdgeRotationMatrix,
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
			);

			CTriangle_VectorProjections::matrixTransformation(
					io_cCellData->transformationMatrix,
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
			);

#else
			CTriangle_VectorProjections::fromHypEdgeSpace(
					&i_hyp_edge_net_update->dofs.hu[i],
					&i_hyp_edge_net_update->dofs.hv[i]
			);

			CTriangle_VectorProjections::fromRightEdgeSpace(
					&i_right_edge_net_update->dofs.hu[i],
					&i_right_edge_net_update->dofs.hv[i]
			);

			CTriangle_VectorProjections::fromLeftEdgeSpace(
					&i_left_edge_net_update->dofs.hu[i],
					&i_left_edge_net_update->dofs.hv[i]
			);

#endif
		}
		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);

#if DEBUG
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			if (std::isnan(io_cCellData->dofs.h[i]))
			{
				instabilityDetected = true;
				if (!instabilityDetected)
					std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
				return;
			}
		}
#endif

		/*
		 * Conservation law:
		 *
		 *   U_t + U_x = 0	(1)
		 *
		 * Explicit Euler timestep:
		 *
		 *   U^{t+1} = U^{t} + \Delta t (- U_x)		(2)
		 *
		 *
		 * Update (See High Order ADER FV/DG Numerical Methods for Hyperbolic Equations, Cristobal Castro, page 42):
		 *
		 *   U^{t+1} = U^{t} + \Delta t * M^{-1} [ S_x F(U^{t}) + S_y G(U^{t}) - \sum_e (E_e X(U^{t})) ]		(2)
		 *
		 *
		 * We reformulate this formula by extracting the cathetus length c from the matrices
		 * to get the dimensionless matrices M, S_x, S_y and E_e
		 *
		 *   U^{t+1} = U^{t} + 1/{c*c} \Delta t * M^{-1} [ c * S_x F(U^{t}) + c * S_y G(U^{t}) - c * \sum_e (E_e X(U^{t})) ] 	(3)
		 *
		 *
		 * By factoring c out of the brackets, we get
		 *
		 *   U^{t+1} = U^{t} + \alpha * M^{-1} [ S_x * F(U^{t}) + S_y * G(U^{t}) - \sum_e (E_e X_e) ]		(4.a)
		 *
		 *   with \alpha = 1/{c} \Delta t																	(4.b)
		 *
		 * To use different cathetus and hypothenuses length we skip the last step and use the equation (3)
		 *
		 */

		static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

		/*
		 * setup updates (fill with 0)
		 */
		CTsunamiSimulationNodeDataSOA<N> dofUpdates;

#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS == 1

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			dofUpdates.h[i] = 0;
			dofUpdates.hu[i] = 0;
			dofUpdates.hv[i] = 0;
		}

#else

#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 1
#	error "Distorted grids not yet supported for higher order methods"
#endif

		/*
		 * compute flux
		 */
		CTsunamiSimulationNodeDataSOA<N> flux_x;
		CTsunamiSimulationNodeDataSOA<N> flux_y;
		p_compute_fluxes(io_cCellData->dofs, &flux_x, &flux_y);


		/*
		 * compute stiffness terms
		 */
		CComputation_Matrices::mul_stiffness_x(flux_x.h,	dofUpdates.h);
		CComputation_Matrices::mul_stiffness_x(flux_x.hu,	dofUpdates.hu);
		CComputation_Matrices::mul_stiffness_x(flux_x.hv,	dofUpdates.hv);

		CComputation_Matrices::madd_stiffness_y(flux_y.h,	dofUpdates.h);
		CComputation_Matrices::madd_stiffness_y(flux_y.hu,	dofUpdates.hu);
		CComputation_Matrices::madd_stiffness_y(flux_y.hv,	dofUpdates.hv);

#endif

		/*
		 * flux
		 *
		 * 0: hyp
		 * 1: right
		 * 2: left
		 */


#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 1
		T triangle_area = io_cCellData->cellArea;

		T hyp_length = io_cCellData->edgeLength[0];
		T right_cat_length = io_cCellData->edgeLength[1];
		T left_cat_length = io_cCellData->edgeLength[2];

		// multiply with edge lengths
		for (int i = 0; i < N; i++){
			dofUpdates.h[i] -= i_hyp_edge_net_update->dofs.h[i] * hyp_length;
			dofUpdates.hu[i] -= i_hyp_edge_net_update->dofs.hu[i] * hyp_length;
			dofUpdates.hv[i] -= i_hyp_edge_net_update->dofs.hv[i] * hyp_length;

			dofUpdates.h[i] -= i_right_edge_net_update->dofs.h[i] * right_cat_length;
			dofUpdates.hu[i] -= i_right_edge_net_update->dofs.hu[i] * right_cat_length;
			dofUpdates.hv[i] -= i_right_edge_net_update->dofs.hv[i] * right_cat_length;

			dofUpdates.h[i] -= i_left_edge_net_update->dofs.h[i] * left_cat_length;
			dofUpdates.hu[i] -= i_left_edge_net_update->dofs.hu[i] * left_cat_length;
			dofUpdates.hv[i] -= i_left_edge_net_update->dofs.hv[i] * left_cat_length;
		}

#else

		T cat_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CComputation_Matrices::madd_edge_hyp_flux_quadrature_weight_matrix(i_hyp_edge_net_update->dofs.h, dofUpdates.h);
		CComputation_Matrices::madd_edge_hyp_flux_quadrature_weight_matrix(i_hyp_edge_net_update->dofs.hu, dofUpdates.hu);
		CComputation_Matrices::madd_edge_hyp_flux_quadrature_weight_matrix(i_hyp_edge_net_update->dofs.hv, dofUpdates.hv);

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CComputation_Matrices::madd_edge_right_flux_quadrature_weight_matrix(i_right_edge_net_update->dofs.h, dofUpdates.h);
		CComputation_Matrices::madd_edge_right_flux_quadrature_weight_matrix(i_right_edge_net_update->dofs.hu, dofUpdates.hu);
		CComputation_Matrices::madd_edge_right_flux_quadrature_weight_matrix(i_right_edge_net_update->dofs.hv, dofUpdates.hv);

		// we are allowed to use madd operations since the coefficients in the quadrature matrix are already inverted
		CComputation_Matrices::madd_edge_left_flux_quadrature_weight_matrix(i_left_edge_net_update->dofs.h, dofUpdates.h);
		CComputation_Matrices::madd_edge_left_flux_quadrature_weight_matrix(i_left_edge_net_update->dofs.hu, dofUpdates.hu);
		CComputation_Matrices::madd_edge_left_flux_quadrature_weight_matrix(i_left_edge_net_update->dofs.hv, dofUpdates.hv);


#endif


#if DEBUG
		if (	std::isnan(dofUpdates.hu[0])	||
				std::isnan(dofUpdates.hv[0])
		)	{
			std::cerr << "op_cell update instability" << std::endl;
			std::cerr << *io_cCellData << std::endl;
			std::cerr << dofUpdates << std::endl;

			std::cerr << "net updates:" << std::endl;
			std::cerr << *i_hyp_edge_net_update << std::endl;
			std::cerr << *i_right_edge_net_update << std::endl;
			std::cerr << *i_left_edge_net_update << std::endl;
			throw(std::runtime_error("op_cell update instability"));
		}
#endif



		//T cat_length = sierpi::CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;

		/*
		 * compute timestep update
		 */
		if (t_storeElementUpdatesOnly)
		{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS != 0
			std::cerr << "RK > 1 not supported yet for higher order methods" << std::endl;
			assert(false);
#endif

#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 0
			/*
			 * Runge Kutta
			 */
			// dimensional update
			T alpha = 1.0/cat_length;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.h[i] *= alpha;
			CComputation_Matrices::mul_inv_mass(dofUpdates.h, io_cCellData->dofs.h);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.hu[i] *= alpha;
			CComputation_Matrices::mul_inv_mass(dofUpdates.hu, io_cCellData->dofs.hu);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.hv[i] *= alpha;
			CComputation_Matrices::mul_inv_mass(dofUpdates.hv, io_cCellData->dofs.hv);
#endif
		}
		else
		{
			/*
			 * Explicit Euler
			 */

			// dimensional update
#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 1
			T alpha = timestep_size/triangle_area;
#else
			T alpha = timestep_size/cat_length;
#endif

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.h[i] *= alpha;
			CComputation_Matrices::madd_inv_mass(dofUpdates.h, io_cCellData->dofs.h);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.hu[i] *= alpha;
			CComputation_Matrices::madd_inv_mass(dofUpdates.hu, io_cCellData->dofs.hu);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
				dofUpdates.hv[i] *= alpha;
			CComputation_Matrices::madd_inv_mass(dofUpdates.hv, io_cCellData->dofs.hv);

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < N; i++)
			{
				if (io_cCellData->dofs.h[i] < SIMULATION_TSUNAMI_DRY_THRESHOLD)
				{
					io_cCellData->dofs.h[i] = 0;
					io_cCellData->dofs.hu[i] = 0;
					io_cCellData->dofs.hv[i] = 0;
				}
			}

#if DEBUG
			if (
					std::isnan(io_cCellData->dofs.hu[0])			||
					std::isnan(io_cCellData->dofs.hv[0])
			)	{
				std::cerr << "op_cell instability" << std::endl;
				std::cerr << *io_cCellData << std::endl;
				exit(-1);
			}
#endif
		}

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE==2

		/*
		 * this adaptivity criteria is based on the 1st component of the Net-Updates
		 */
		T m = 0;
		T h = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			if (i_hyp_edge_net_update->dofs.h[i] > m)
			{
				m = i_hyp_edge_net_update->dofs.h[i];
				h = m;
			}
		}
		m *= (T)M_SQRT2;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			if (i_right_edge_net_update->dofs.h[i] > m)
			{
				m = i_right_edge_net_update->dofs.h[i];
				h = m;
			}
		}

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			if (i_left_edge_net_update->dofs.h[i] > m)
			{
				m = i_left_edge_net_update->dofs.h[i];
				h = m;
			}
		}

		m *= cat_length;

		T inv_h;
		if (h > SIMULATION_TSUNAMI_DRY_THRESHOLD)
			inv_h = (T)1.0/h;
		else
			inv_h = SIMULATION_TSUNAMI_DRY_THRESHOLD;

		// rescale with height to get update for unit area over unit timestep
		io_cCellData->refine = (m*inv_h > refine_threshold);
		io_cCellData->coarsen = (m*inv_h < coarsen_threshold);
#endif

	}



	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			CEdgeData &i_edgeFlux_left,		///< edge data on left edge
			CEdgeData &i_edgeFlux_right,		///< edge data on right edge
			CEdgeData &o_edgeNetUpdates_left,		///< output for left flux
			CEdgeData &o_edgeNetUpdates_right		///< output for outer flux
	)	{
		/*
		 * fix edge normal space for right flux
		 */
		T max_wave_speed_left;
		T max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeFlux_left.dofs,
				i_edgeFlux_right.dofs,
				&o_edgeNetUpdates_left.dofs,
				&o_edgeNetUpdates_right.dofs,

				&max_wave_speed_left,
				&max_wave_speed_right,

				gravitational_constant
		);

		o_edgeNetUpdates_left.CFL1_scalar = i_edgeFlux_left.CFL1_scalar / max_wave_speed_left;
		o_edgeNetUpdates_right.CFL1_scalar = i_edgeFlux_right.CFL1_scalar / max_wave_speed_right;

#if DEBUG
		if (	!(o_edgeNetUpdates_left.CFL1_scalar > 0) ||
				!(o_edgeNetUpdates_right.CFL1_scalar > 0)
		)
		{
			std::cerr << "CFL1 scalar > 0" << std::endl;
			std::cerr << o_edgeNetUpdates_left.CFL1_scalar << std::endl;
			std::cerr << o_edgeNetUpdates_right.CFL1_scalar << std::endl;

			std::cerr << i_edgeFlux_left << std::endl;
			std::cerr << i_edgeFlux_right << std::endl;
		}
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_edgeNetUpdates_left.validation = i_edgeFlux_left.validation;
		o_edgeNetUpdates_right.validation = i_edgeFlux_right.validation;
#endif
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CEdgeData> *io_edge_comm_buffer
	)	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_stack(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_stack(
			CEdgeData *io_edge_data_array,
			size_t i_edge_comm_elements
	)	{
		for (size_t i = 0; i < i_edge_comm_elements; i+=2)
		{
			CEdgeData ed0 = io_edge_data_array[i];
			CEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}

	static void setupMatrices(int i_verbosity_level)
	{
		CComputation_Matrices::setup(i_verbosity_level);

		if (i_verbosity_level > 10)
		{
			CComputation_Matrices::debugOutput(i_verbosity_level);
		}
	}
};

}
}



#endif
