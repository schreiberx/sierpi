/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "../../../subsimulation_generic/CConfig.hpp"

#include "CEdgeComm_Hyperbolic_Order_N.hpp"

namespace sierpi
{
	namespace kernels
	{
#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER == 1
		// directly apply explicit euler
		typedef CEdgeComm_Hyperbolic_Order_N<false> CEdgeComm_Hyperbolic;
#else
		// store updates only
		typedef CEdgeComm_Hyperbolic_Order_N<true> CEdgeComm_Hyperbolic;
#endif
	}
}
