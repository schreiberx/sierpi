/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */



#include "CSetup_CellData_Order_N.hpp"

namespace sierpi
{
	namespace kernels
	{
		typedef CSetup_CellData_Order_N<CHyperbolicTypes> CSetup_CellData;
	}
}
