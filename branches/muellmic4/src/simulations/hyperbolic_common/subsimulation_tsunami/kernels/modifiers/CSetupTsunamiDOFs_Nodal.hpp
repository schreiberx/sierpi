/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUPTSUNAMI_DOFS_NODAL_HPP_
#define CSETUPTSUNAMI_DOFS_NODAL_HPP_


#include "../../CDatasets.hpp"
#include "../../types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "../../../subsimulation_generic/CConfig.hpp"

#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 1
#	include "../../../../../libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#endif


class CSetupTsunamiDOFs
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:
	static inline void setup(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiSimulationCellData *io_cCellData,

			CDatasets *cDatasets
	)	{

		T lod = sierpi::CTriangle_Tools::getLODFromDepth(i_depth);

		T *nodal_coords = CBasisFunctions::getNodalCoords();

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T world_coords[2];

			sierpi::CTriangle_PointProjections::referenceToWorld(
					i_vertex_left_x,	i_vertex_left_y,
					i_vertex_right_x,	i_vertex_right_y,
					i_vertex_top_x,		i_vertex_top_y,

					nodal_coords[0],	nodal_coords[1],

					&world_coords[0],	&world_coords[1]
			);

			CTsunamiSimulationNodeData n;

			// initialize in case that getNodalData only updates the values
			n.h = io_cCellData->dofs.h[i];
			n.hu = io_cCellData->dofs.hu[i];
			n.hv = io_cCellData->dofs.hv[i];
			n.b = io_cCellData->dofs.b[i];

			cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

			io_cCellData->dofs.h[i] = n.h;
			io_cCellData->dofs.hu[i] = n.hu;
			io_cCellData->dofs.hv[i] = n.hv;
			io_cCellData->dofs.b[i] = n.b;

			if (io_cCellData->dofs.h[i] < 0)
			{
				assert(false);
				throw("negative depth detected");
			}

			if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
			{
				//momentum was updated -> project to reference space
				CTriangle_VectorProjections::worldToReference(
						&io_cCellData->dofs.hu[i],
						&io_cCellData->dofs.hv[i],
						-i_right_normal_x,
						-i_right_normal_y
				);
			}

			nodal_coords += 2;
		}



#if CONFIG_SIERPI_DISTORTED_GRID_MODE == 1

		io_cCellData->face = CCube_To_Sphere_Projection::getFaceOfCube(
				i_vertex_left_x, i_vertex_left_y,
				i_vertex_right_x, i_vertex_right_y,
				i_vertex_top_x, i_vertex_top_y);

		CCube_To_Sphere_Projection::project2DToCubeFace(
				io_cCellData->face,
				&i_vertex_left_x, &i_vertex_left_y,
				&i_vertex_right_x, &i_vertex_right_y,
				&i_vertex_top_x, &i_vertex_top_y
		);

		io_cCellData->vertices[0][0] = i_vertex_left_x;
		io_cCellData->vertices[0][1] = i_vertex_left_y;
		io_cCellData->vertices[1][0] = i_vertex_right_x;
		io_cCellData->vertices[1][1] = i_vertex_right_y;
		io_cCellData->vertices[2][0] = i_vertex_top_x;
		io_cCellData->vertices[2][1] = i_vertex_top_y;

		// set correct radius
		T radius = EARTH_RADIUS_IN_METERS;


		for(int i = 0; i < 3; i++){
			// set edge length as on sphere
			CTriangle_VectorProjections::setEdgeLengthOnSphere(
					&io_cCellData->edgeLength[i],
					&io_cCellData->vertices[i][0],
					&io_cCellData->vertices[i][1],
					&io_cCellData->vertices[(i+1)%3][0],
					&io_cCellData->vertices[(i+1)%3][1],
					radius
			);
		}

		// set rotation matrices for all edges
		CTriangle_VectorProjections::setHypEdgeRotationMatrix(
				io_cCellData->hypEdgeRotationMatrix,
				io_cCellData->edgeLength[0],
				io_cCellData->edgeLength[1],
				io_cCellData->edgeLength[2]);
		CTriangle_VectorProjections::setLeftEdgeRotationMatrix(
				io_cCellData->leftEdgeRotationMatrix);
		CTriangle_VectorProjections::setRightEdgeRotationMatrix(
				io_cCellData->rightEdgeRotationMatrix,
				io_cCellData->edgeLength[0],
				io_cCellData->edgeLength[1],
				io_cCellData->edgeLength[2]);

		CTriangle_VectorProjections::setTransformationMatrices(
				io_cCellData->transformationMatrix,
				io_cCellData->inverseTransformationMatrix,
				io_cCellData->edgeLength[0],
				io_cCellData->edgeLength[1],
				io_cCellData->edgeLength[2]);

		// approximation of the triangle area as half of the product of the two cathetus lengths
//		io_cCellData->cellArea = (io_cCellData->edgeLength[1] * io_cCellData->edgeLength[2] ) / (T)2.0;

		CTriangle_VectorProjections::setCellAreaOnSphere(
				&io_cCellData->cellArea,
				io_cCellData->edgeLength[0],
				io_cCellData->edgeLength[1],
				io_cCellData->edgeLength[2],
				radius);

		// set inscribed circle radius
		CTriangle_VectorProjections::setIncircleRadius(
				&io_cCellData->incircleRadius,
				io_cCellData->edgeLength[0],
				io_cCellData->edgeLength[1],
				io_cCellData->edgeLength[2]);


		// set 3D coordinates on sphere
		for(int i = 0; i < 3; i++){
			CCube_To_Sphere_Projection::projectToSphere(
					io_cCellData->face,
					io_cCellData->vertices[i][0],
					io_cCellData->vertices[i][1],
					&io_cCellData->coordinates3D[i][0],
					&io_cCellData->coordinates3D[i][1],
					&io_cCellData->coordinates3D[i][2]
			);
		}
#endif

	}



};

#endif /* CSETUPTSUNAMI_DOFS_NODAL_HPP_ */
