/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CComputation_Matrices_Nodal.hpp"


CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::stiffness_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::stiffness_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::edge_comm_data_nodal_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::edge_flux_quadrature_weight_matrix[3][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS];

CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::adaptivity_refine_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::adaptivity_refine_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::adaptivity_coarsen_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CHyperbolicTypes::CSimulationTypes::T CComputation_Matrices::adaptivity_coarsen_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

//CHyperbolicTypes::CSimulationTypes::T CEdgeComm_Hyperbolic_Order_N_Matrices_Vectors::edge_weights[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS];
