/*
 * CBasisFunctions.hpp
 *
 *  Created on: Sep 13, 2012
 *      Author: schreibm
 */

#ifndef CBASISFUNCTIONS_HPP_
#define CBASISFUNCTIONS_HPP_


#if SIMULATION_HYPERBOLIC_CONST_MATRICES_ID == -1

	#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==0
		#include "../basis_functions_and_matrices/CBasisFunctions_Order_0.hpp"
	#else
		#include "../basis_functions_and_matrices/CBasisFunctions_Nodal_Order_N.hpp"
	#endif

#else

//	#include "../basis_functions_and_matrices/CBasisFunctions_Modal_Order_N.hpp"

#endif



#endif /* CBASISFUNCTIONS_HPP_ */
