/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION_MATRICES_CONST_HPP_
#define CCOMPUTATION_MATRICES_CONST_HPP_

#include <string.h>

#include "CComputation_Matrices_Const_Config.hpp"
#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"


class CComputation_Matrices
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;

public:

	static inline int getNumberOfFlops()
	{
		int single_quantity_flops =
				SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS	+	/// mass matrix
				SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS +	/// stiffness
				SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS +	/// stiffness
				SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS*SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*3		+	// edge comm
				SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS*3		+	// flux quadrature
				0;

		// multiply with 3 since we have 3 conserved quantities
		single_quantity_flops *= 3;

		// multiply with 2 since we have an add operation per multiplication
		single_quantity_flops *= 2;

		return single_quantity_flops;
	}



	/*********************************************************
	 * INV MASS
	 *********************************************************/
	static inline void mul_inv_mass(
			const T i_input[],
			T o_output[]
	) {
		static const T inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{2}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += inv_mass_matrix[i][j]*i_input[j];
		}
	}


	static inline void madd_inv_mass(
			const T i_input[],
			T o_output[]
	) {
		static const T inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{2}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += inv_mass_matrix[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS X
	 *********************************************************/
	static inline void mul_stiffness_x(
			const T i_input[],
			T o_output[]
	) {
		static const T stiffness_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{0}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += stiffness_x[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS Y
	 *********************************************************/
	static inline void madd_stiffness_y(
			const T i_input[],
			T o_output[]
	) {
		static const T stiffness_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{0}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += stiffness_y[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * FLUX QUADRATURE
	 *********************************************************/
	static inline void madd_edge_hyp_flux_quadrature_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T edge_flux_quadrature_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS] = {{-M_SQRT2}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}


	static inline void madd_edge_right_flux_quadrature_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T edge_flux_quadrature_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS] = {{-1}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}


	static inline void madd_edge_left_flux_quadrature_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T edge_flux_quadrature_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS] = {{-1}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}



	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX HYP
	 *********************************************************/

public:
	static inline void mul_edge_comm_hyp_data_nodal_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{1}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_hyp_project_to_edge_space(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,0>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}

public:
	static inline void mul_edge_comm_rotate_to_edge_space(
			const T i_rotationMatrix[2][2],
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * rotate edge comm data to edge space
			 */
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace<T>(
					i_rotationMatrix,
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}




	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX RIGHT
	 *********************************************************/

public:
	static inline void mul_edge_comm_right_data_nodal_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{1.0}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_right_project_to_edge_space(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,1>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}



	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX LEFT
	 *********************************************************/

public:
	static inline void mul_edge_comm_left_data_nodal_weight_matrix(
			const T i_input[],
			T o_output[]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{1.0}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_left_project_to_edge_space(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,2>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}



	/*********************************************************
	 * ADAPTIVITY REFINEMENT
	 *********************************************************/

public:
	static inline void mul_adaptivity_refine_left_child_matrix(
			const T i_input[],
			T o_output[]
	) {
		// adaptive
		static const T adaptivity_refine_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{1.0}};


#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= adaptivity_refine_left_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_left_child(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
			);
		}
	}



public:
	static inline void mul_adaptivity_refine_right_child_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T adaptivity_refine_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{1.0}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= adaptivity_refine_right_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_right_child(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
			);
		}
	}



	/*********************************************************
	 * ADAPTIVITY COARSENING
	 *********************************************************/

public:
	static inline void mul_adaptivity_coarsen_left_child_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T adaptivity_coarsen_left_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{0.5}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= adaptivity_coarsen_left_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_left_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
			);
		}
	}



public:
	static inline void mul_adaptivity_coarsen_right_child_matrix(
			const T i_input[],
			T o_output[]
	) {
		static const T adaptivity_coarsen_right_child_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS] = {{0.5}};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
				o_output[i]		+= adaptivity_coarsen_right_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_right_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
			);
		}
	}



	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
	static void setup(int i_verbosity_level)
	{
	}



public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 0" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Integration cell order: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
		std::cout << " + Integration edge order: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;

		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif /* CCOMPUTATION_MATRICES_CONST_HPP */
