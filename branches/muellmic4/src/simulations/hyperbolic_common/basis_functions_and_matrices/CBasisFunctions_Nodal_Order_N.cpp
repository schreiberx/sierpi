/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CBasisFunctions_Nodal_Order_N.hpp"
#include "CComputation_Matrices_Nodal_Config.hpp"




CONFIG_DEFAULT_FLOATING_POINT_TYPE CBasisFunctions::exponent_x[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CONFIG_DEFAULT_FLOATING_POINT_TYPE CBasisFunctions::exponent_y[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
CONFIG_DEFAULT_FLOATING_POINT_TYPE CBasisFunctions::alpha[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

CONFIG_DEFAULT_FLOATING_POINT_TYPE CBasisFunctions::nodalCoords[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS*2];
