/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 19. Juni 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CXMLConfig.hpp"

#include <iostream>
#if CONFIG_ENABLE_LIBXML
#include <libxml/parser.h>
#include <libxml/tree.h>
#endif
#include <string.h>


void CXMLConfig::registerConfigClass(
			const char *i_config_scope_name,			///< name of xml scope to forward parameters (e. g. swe, threading or sierpi)
			CXMLConfigInterface *i_cXMLConfigInterface	///< pointer to xml interface handling the parameter
)
{
	configuration_interfaces.push_back(
		std::pair<const char*, CXMLConfigInterface*>(i_config_scope_name, i_cXMLConfigInterface)
	);
}



bool CXMLConfig::loadConfig(
			const char *i_xml_file			///< load configuration from xml file
)
{
#if CONFIG_ENABLE_LIBXML
    LIBXML_TEST_VERSION

    xmlDoc* doc = xmlReadFile(i_xml_file, nullptr, 0);

    if (doc == nullptr)
    {
    	std::cerr << "failed to parse xml configuration file" << std::endl;
    	exit(-1);
    }

    // load root element
	xmlNode* root_element = xmlDocGetRootElement(doc);

	bool config_read_successfully = true;

	if (root_element == nullptr)
	{
		std::cout << "XML ERROR: No root element found" << std::endl;
		config_read_successfully = false;
		goto done;
	}

	if (root_element->type != XML_ELEMENT_NODE)
	{
		std::cout << "XML ERROR: Root node not of type element node!" << std::endl;
		config_read_successfully = false;
		goto done;
	}


	/*
	 * first level traversal over scopes (threading, swe, sierpi, ...)
	 */
    for (xmlNode *cur_node = root_element->children; cur_node; cur_node = cur_node->next)
    {
    	if (cur_node->type == XML_ELEMENT_NODE)
    	{
    		const unsigned char *scope_name = cur_node->name;

    		/*
    		 * search config interface
    		 */
    		CXMLConfigInterface *cXMLConfigInterface = nullptr;

    		for (auto iter = configuration_interfaces.begin(); iter != configuration_interfaces.end(); iter++)
    		{
    			std::pair<const char*, CXMLConfigInterface*> &pair = *iter;

    			if (strcmp(pair.first, (const char*)scope_name) == 0)
    			{
    				cXMLConfigInterface = pair.second;
    				break;
    			}
    		}

    		if (cXMLConfigInterface == nullptr)
    		{
    			if (strcmp((char*)scope_name, "compiler") != 0)
    				std::cout << "CXMLConfig: WARNING - configuration interface for element '" << scope_name << "' not found!" << std::endl;
    			continue;
    		}

    	    for (xmlNode *cur_param_node = cur_node->children; cur_param_node; cur_param_node = cur_param_node->next)
    	    {
    	    	if (strcmp((char*)cur_param_node->name, "param") == 0)
    	    	{
        			char *name = (char*)xmlGetProp(cur_param_node, (xmlChar*)"name");

        			if (name == nullptr)
        			{
        				std::cerr << "CXMLConfig: 'name' attribute missing in line " << cur_param_node->line << std::endl;
        				continue;
        			}

        			char *value = (char*)xmlGetProp(cur_param_node, (xmlChar*)"value");

        			if (value == nullptr)
        			{
            			xmlFree(name);
        				std::cerr << "CXMLConfig: 'value' attribute missing in line " << cur_param_node->line << std::endl;
        				continue;
        			}

        			/*
        			 * only use xml configuration values when default values are not expected
        			 */
        			if (strcmp(value, "*") != 0)
        			{
        				if (!cXMLConfigInterface->setupXMLParameter((const char*)scope_name, (const char*)name, value))
        				{
        					std::cout << "WARNING: In scope '" << scope_name << "', parameter '" << name << "' not handled with value '" << value << "'" << std::endl;
        				}
        			}

        			xmlFree(name);
        			xmlFree(value);
    	    	}
    	    }
    	}
    }

done:
    xmlFreeDoc(doc);
    return config_read_successfully;

#else

    return false;

#endif
}

