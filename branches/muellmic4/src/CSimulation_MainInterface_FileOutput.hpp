/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulation_MainInterface.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_MAININTERFACE_FILE_OUTPUT_HPP_
#define CSIMULATION_MAININTERFACE_FILE_OUTPUT_HPP_


/**
 * interface descriptions which have to be offered by the simulation class
 */
class CSimulation_MainInterface_FileOutput
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:

	/**
	 * output some information about simulation data
	 */
	virtual void writeSimulationDataToFile(
			const char *i_filename,						///< filename to write simulation data to
			const char *i_information_string = nullptr		///< information string about simulation data to be printed into simulation data file
	) = 0;


	/**
	 * output some information about simulation data
	 */
	virtual void writeSimulationClustersDataToFile(
			const char *i_filename,						///< filename to write simulation data to
			const char *i_information_string = nullptr		///< information string about simulation data to be printed into simulation data file
	) = 0;


	virtual ~CSimulation_MainInterface_FileOutput()
	{
	}
};


#endif /* CSIMULATION_MAININTERFACE_HPP_ */
