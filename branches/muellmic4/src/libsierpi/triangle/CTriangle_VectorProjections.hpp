/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CPROJECTIONS_HPP_
#define CPROJECTIONS_HPP_

#include <cmath>
#include <cassert>

/**
 * This class contains helper methods to project 2D problems stored in
 * world-space to a 2D problem stored in an edge-space.
 *
 * All projects are taken from the viewpoint of the reference element.
 */
class CTriangle_VectorProjections
{
public:
	/************************************************
	 * Project from CELL space to another space by giving X-asis of new space
	 ************************************************/

	/**
	 * Project from CELL space to another space by giving X-asis of CELL reference space
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void referenceToWorld(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis reference vector in world-space (x component)
			T i_by		///< x-axis reference vector in world-space (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(-i_by);
		*io_hv =	(*io_hu)	*	(i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Project from WORLD space to REFERENCE space.
	 *
	 * The axis is the x-axis of the reference space in world space
	 */
	template <typename T>
	inline static void worldToReference(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}


	/**
	 * Represent Vector from Reference space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementToBasisWithXAxis(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}



	/**
	 * represent Vector from CELL space in another basis
	 */
	template <typename T>
	inline static void changeFromReferenceElementWithXAxisToWorldSpace(
			T *io_hu,
			T *io_hv,
			T i_bx,		///< x-axis basis vector (x component)
			T i_by		///< x-axis basis vector (y component)
	)
	{
		T tmp =		(*io_hu)	*	(i_bx)	 +	(*io_hv)	*	(i_by);
		*io_hv =	(*io_hu)	*	(-i_by)	 +	(*io_hv)	*	(i_bx);
		*io_hu = tmp;
	}




	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T, int edge_id>
	inline static void toEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		/*
		 * hypotenuse edge
		 */
		if (edge_id == 0)
		{
			T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
			*io_hv = -*io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
			*io_hu = tmp;
			return;
		}

		/*
		 * right edge
		 */
		if (edge_id == 1)
		{
			T tmp = -*io_hu;
			*io_hv = -*io_hv;
			*io_hu = tmp;
			return;
		}


		/*
		 * left edge
		 */
		if (edge_id == 2)
		{
			T tmp = -*io_hv;
			*io_hv = *io_hu;
			*io_hu = tmp;
			return;
		}

		assert(false);
	}



	/************************************************
	 * HYPOTENUSE STUFF
	 ************************************************/

	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = -*io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to hyp edge space
	 */
	template <typename T>
	inline static void toHypEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hu = -tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from hyp edge space
	 */
	template <typename T>
	inline static void fromHypEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hu*(T)M_SQRT1_2 - *io_hv*(T)M_SQRT1_2;
		*io_hv = *io_hu*(T)M_SQRT1_2 + *io_hv*(T)M_SQRT1_2;
		*io_hu = tmp;
	}



	/************************************************
	 * RIGHT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hu;
		*io_hv = -*io_hv;
		*io_hu = tmp;
	}


	/**
	 * compute projected components of (hu,hv) to right edge space
	 */
	template <typename T>
	inline static void toRightEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		// nothing to do
	}



	/**
	 * compute backprojected components of (hu,hv) from right edge space
	 */
	template <typename T>
	inline static void fromRightEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hu;
		*io_hv = -*io_hv;
		*io_hu = tmp;
	}


	/************************************************
	 * LEFT EDGE STUFF
	 ************************************************/


	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = -*io_hv;
		*io_hv = *io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute projected components of (hu,hv) to left edge space
	 */
	template <typename T>
	inline static void toLeftEdgeSpaceAndInvert(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}



	/**
	 * compute backprojected components of (hu,hv) from left edge space
	 */
	template <typename T>
	inline static void fromLeftEdgeSpace(
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = *io_hv;
		*io_hv = -*io_hu;
		*io_hu = tmp;
	}


	//#if CONFIG_SIERPI_DISTORTED_GRID_MODE==1
	template <typename T>
	inline static void matrixTransformation(
			const T i_transformationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = (T)i_transformationMatrix[0][0] * *io_hu + (T)i_transformationMatrix[0][1] * *io_hv;
		*io_hv = (T)i_transformationMatrix[1][0] * *io_hu + (T)i_transformationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
	}

	template <typename T>
	inline static void fromAnyEdgeToEdgeSpace(
			const T i_rotationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
		//		std::cout << i_rotationMatrix[0][0] << std::endl;
		T tmp = (T)i_rotationMatrix[0][0] * *io_hu + (T)i_rotationMatrix[0][1] * *io_hv;
		*io_hv = (T)i_rotationMatrix[1][0] * *io_hu + (T)i_rotationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
	}

	template <typename T>
	inline static void fromEdgeSpace(
			const T i_rotationMatrix[2][2],
			T *io_hu,
			T *io_hv
	)
	{
		T tmp = (T)i_rotationMatrix[0][0] * *io_hu - (T)i_rotationMatrix[0][1] * *io_hv;
		*io_hv = -(T)i_rotationMatrix[1][0] * *io_hu + (T)i_rotationMatrix[1][1] * *io_hv;;
		*io_hu = tmp;
	}

	template <typename T>
	inline static void setIncircleRadius(
			T *o_incircleRadius,

			T i_hypEdgeLength, 		// length of the hyp edge
			T i_rightEdgeLength,	// length of the right edge
			T i_leftEdgeLength		// length of the left edge
	){

		T h = i_hypEdgeLength;
		T r = i_rightEdgeLength ;
		T l = i_leftEdgeLength;
		T s = (h + l + r) / (T)2.0;
		*o_incircleRadius = (T)sqrt((s*s*s - s*s*h - s*s*r - s*s*l + s*h*r + s*h*l + s*r*l - h*r*l)/s);

	}

	/*
	 * left edge is located on the x-axis
	 * therefore: turn by 90° to edge space
	 *
	 */
	template <typename T>
	inline static void setLeftEdgeRotationMatrix(
			T o_rotationMatrix[2][2]
	){
		o_rotationMatrix[0][0] = (T)0;
		o_rotationMatrix[0][1] = (T)-1;
		o_rotationMatrix[1][0] = (T)1;
		o_rotationMatrix[1][1] = (T)0;
	}


	/**
	 * right edge is located per construction over (or on) the x-axis
	 * therefore: turn by -alpha to left edge and another -90° to edge space
	 *
	 *
	 *          /\_
	 *         /   \_
	 *        /      \_
	 * right /         \_hyp
	 *      /-.         .\_
	 *     /_a_ì_______í_b_\
	 *          left
	 *
	 *  a = alpha
	 *  b = beta
	 */
	template <typename T>
	inline static void setRightEdgeRotationMatrix(
			T o_rotationMatrix[2][2],

			T i_hypEdgeLength, 		// length of hyp edge
			T i_rightEdgeLength,	// length of right edge
			T i_leftEdgeLength 		// length of left edge

	){
		T alpha = (T)acos((i_rightEdgeLength * i_rightEdgeLength + i_leftEdgeLength * i_leftEdgeLength - i_hypEdgeLength * i_hypEdgeLength) / (T)2.0 / i_rightEdgeLength / i_leftEdgeLength);
		T angleToY = -(T)M_PI/2.0 - alpha;

		o_rotationMatrix[0][0] = (T)cos(angleToY);
		o_rotationMatrix[0][1] = (T)-sin(angleToY);
		o_rotationMatrix[1][0] = (T)sin(angleToY);
		o_rotationMatrix[1][1] = (T)cos(angleToY);
	}

	/**
	 * hyp edge is located per construction over (or on) the x-axis
	 * therefore: turn by beta to left edge and another -90° back to edge space
	 *
	 *
	 *
	 *          /\_
	 *         /   \_
	 *        /      \_
	 * right /         \_hyp
	 *      /-.         .\_
	 *     /_a_ì_______í_b_\
	 *          left
	 *
	 *  a = alpha
	 *  b = beta
	 */
	template <typename T>
	inline static void setHypEdgeRotationMatrix(
			T o_rotationMatrix [2][2],
			T i_hypEdgeLength, 		// length of hyp edge
			T i_rightEdgeLength,	// length of right edge
			T i_leftEdgeLength 		// length of left edge
	){
		T beta = (T)acos((i_leftEdgeLength * i_leftEdgeLength + i_hypEdgeLength * i_hypEdgeLength - i_rightEdgeLength * i_rightEdgeLength) / (T)2.0 / i_leftEdgeLength / i_hypEdgeLength);
		T angleToY = -(T)M_PI/2.0 + beta;

		o_rotationMatrix[0][0] = (T)cos(angleToY);
		o_rotationMatrix[0][1] = (T)-sin(angleToY);
		o_rotationMatrix[1][0] = (T)sin(angleToY);
		o_rotationMatrix[1][1] = (T)cos(angleToY);
	}

	template <typename T>
	inline static void setTransformationMatrices(
			T o_transvectionMatrix [2][2],			// matrix for scaling and shearing to reference space
			T o_inverseTransvectionMatrix [2][2],	// inverse matrix for scaling and shearing from reference space
			T i_hypEdgeLength, 	// length of hyp edge
			T i_rightEdgeLength,	// length of right edge
			T i_leftEdgeLength 	// length of left edge
	){

		T alpha = (T)acos((i_rightEdgeLength * i_rightEdgeLength + i_leftEdgeLength * i_leftEdgeLength - i_hypEdgeLength * i_hypEdgeLength) / (T)2.0 / i_rightEdgeLength / i_leftEdgeLength);

		o_transvectionMatrix[0][0] = (T)1.0 / i_leftEdgeLength;
		o_transvectionMatrix[0][1] = (T)-cos(alpha) / sin(alpha) / i_leftEdgeLength;
		o_transvectionMatrix[1][0] = (T)0.0;
		o_transvectionMatrix[1][1] = (T)1.0 / sin(alpha) / i_rightEdgeLength;

		o_inverseTransvectionMatrix[0][0] = (T)i_leftEdgeLength;
		o_inverseTransvectionMatrix[0][1] = (T)cos(alpha) * i_rightEdgeLength;
		o_inverseTransvectionMatrix[1][0] = (T)0.0;
		o_inverseTransvectionMatrix[1][1] = (T)sin(alpha) * i_rightEdgeLength;
	}

	template <typename T>
	inline static void setEdgeLengthOnSphere(
			T *edgeLength,

			T *vertex1_x, 	// face based x-coordinate of vertex 1
			T *vertex1_y, 	// face based y-coordinate of vertex 1
			T *vertex2_x,	// face based x-coordinate of vertex 2
			T *vertex2_y,	// face based y-coordinate of vertex 2
			T radius		// real radius of sphere (e.g. earth)
	)
	{

		T x1 = *vertex1_x;
		T y1 = *vertex1_y;
		T x2 = *vertex2_x;
		T y2 = *vertex2_y;

		T alpha = acos((x1 * x2 + y1 * y2 + 1) /
				(sqrt(x1 * x1 + y1 * y1 + 1)
						* sqrt(x2 * x2 + y2 * y2 + 1)));

		*edgeLength = radius * alpha;
	}

	template <typename T>
	inline static void setCellAreaOnSphere(
			T *cellArea,

			T i_hypEdgeLength, 		// length of the hyp edge
			T i_rightEdgeLength,	// length of the right edge
			T i_leftEdgeLength,		// length of the left edge
			T radius				// real radius of sphere (e.g. earth)
	){

		T h = i_hypEdgeLength/radius;
		T r = i_rightEdgeLength/radius;
		T l = i_leftEdgeLength/radius;

		T coshyp = cos(h);
		T cosr = cos(r);
		T cosl = cos(l);
		T sinhyp = sin(h);
		T sinr = sin(r);
		T sinl = sin(l);

		T alpha = acos((coshyp-cosr*cosl)/(sinr*sinl));
		T beta = acos((cosr-coshyp*cosl)/(sinhyp*sinl));
		T gamma = acos((cosl-cosr*coshyp)/(sinr*sinhyp));


		*cellArea = (T)(alpha + beta + gamma - M_PI) * radius * radius;
		//		T s = (h + r + l) / (T)2.0;
		//		*cellArea = (T)sqrt((s*s*s - s*s*h - s*s*r - s*s*l + s*h*r + s*h*l + s*r*l - h*r*l)*s);

	}
	//#endif
};


#endif /* CFLUXPROJECTIONS_HPP_ */
