/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CCluster_TreeNode.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_TREENODE_HPP_
#define CCLUSTER_TREENODE_HPP_

#include "config.h"
#include "CCluster_SplitJoinInformation.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"
#include "CCluster_SplitJoinActions.hpp"
#include "CCluster_UniqueId.hpp"
#include "../stacks/CSimulationStacks.hpp"
#include "../generic_tree/CGenericTreeNode.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
#endif


namespace sierpi
{

/**
 * \brief Container for all data and methods which is needed to execute the computations on this cluster
 * in parallel without requesting any data from the adjacent cells.
 *
 * All important features for parallel processing are included in this cluster.
 * Among others, this basic features are:
 *  - cCluster_SplitJoinActions				- to run split- and join-operations
 *  - cCluster_SplitJoinInformation			- information which is necessary to do fast split/joins.
 *  											  this information is created during the last backward adaptive traversal.
 *  - cCluster_AdaptiveSplitJoinInformation	- split/join informations after adaptive step
 *  - cCluster_EdgeComm_InformationAdjacentClusters		- information about adjacent clusters to communicate with
 *
 * A pointer to the Simulation_ClusterHandler is also stored which implements the application developer
 * handlers.
 *
 * Methods of GenericTreeNode are inherited directly to this class to get a node of the domain triangulation tree!
 */
template <typename CSimulation_Cluster>
class CCluster_TreeNode
{
public:
	/*
	 * typedefs inherited from CCluster !!!
	 */
	typedef typename CSimulation_Cluster::CSimulationTypes CSimulationTypes_;
	typedef typename CSimulation_Cluster::CVisualizationTypes CVisualizationTypes_;

	typedef typename CSimulationTypes_::CEdgeData CEdgeData_;
	typedef typename CSimulationTypes_::CCellData CCellData_;

	typedef CGenericTreeNode<CCluster_TreeNode<CSimulation_Cluster> > CGeneric_TreeNode_;

	/*
	 * more typedefs for convenience
	 */
	typedef CCluster_TreeNode<CSimulation_Cluster> CCluster_TreeNode_;

	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CEdgeComm_InformationAdjacentCluster_;
	typedef CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> CEdgeComm_InformationAdjacentClusters_;

	typedef CSimulationStacks<CSimulationTypes_,CVisualizationTypes_> CSimulationStacks_;


public:
	/**
	 * user's simulation handler which actually runs all the computations
	 */
	CSimulation_Cluster *cCluster;

	/**
	 * generic tree node
	 */
	CGeneric_TreeNode_ *cGeneric_TreeNode;

	/**
	 * cluster's unique ID
	 */
	CCluster_UniqueId uniqueId;

	/**
	 * all stacks we need during the simulation
	 */
	CSimulationStacks_ *cStacks;


	/**
	 * this flag is set to true, if the stacks are reused from a different node (the parent node)
	 * and should not be neither allocated nor deallocated
	 */
	bool reuse_parent_stacks;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	/**
	 * validation stacks needed?
	 */
	CValidationStacks *cValidationStacks;
#endif



	/**
	 * the instance of the triangle factory for this cluster
	 *
	 * IMPORTANT: this is no reference!
	 */
	CTriangle_Factory cTriangleFactory;

	/**
	 * split and join actions
	 */
	CCluster_SplitJoinActions<CSimulation_Cluster> cCluster_SplitJoinActions;

	/**
	 * split/join information which should be used for data exchange
	 */
	CCluster_SplitJoin_EdgeComm_Information cCluster_SplitJoinInformation;

	/**
	 * split/join information right after the adaptive step
	 */
	CCluster_SplitJoin_EdgeComm_Information cCluster_AdaptiveSplitJoinInformation;

	/**
	 * here the adjacency information to other clusters is stored
	 */
	CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> cCluster_EdgeComm_InformationAdjacentClusters;


	/**
	 * constructor
	 *
	 * only initialize classes. other things like setting up the stack has to be handled by execution of different methods.
	 */
	CCluster_TreeNode(
			CTriangle_Factory &i_triangleFactory	///< the triangle factory for this triangle
	)	:
		cCluster(nullptr),
		cGeneric_TreeNode(nullptr),
		cStacks(nullptr),
		reuse_parent_stacks(false),
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks(nullptr),
#endif
		cTriangleFactory(i_triangleFactory),
		cCluster_SplitJoinActions(*this),
		cCluster_SplitJoinInformation(cTriangleFactory),
		cCluster_AdaptiveSplitJoinInformation(cTriangleFactory)
	{
	}


	/**
	 * set the unique id of this cluster
	 */
	void setUniqueId(
			CCluster_UniqueId &i_uniqueId		///< new unique id
	)
	{
		uniqueId = i_uniqueId;
	}



	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * !!! allocate and setup new SIMULATION PARTITION HANDLER !!!
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	void setup_SimulationClusterHandler(
			bool initialSetup,						///< run the initial setup (allocate simulation cluster handler and call constructor)
			CSimulation_Cluster *i_simulation_cluster_parameterSetup	///< cluster handler to setup parameters
	)
	{
		if (cCluster != nullptr)
		{
			cCluster->setup(i_simulation_cluster_parameterSetup);
			return;
		}

		cCluster = new CSimulation_Cluster(this, i_simulation_cluster_parameterSetup);
	}



	/**
	 * deconstructor
	 */
	virtual ~CCluster_TreeNode()
	{
		freeStacks();

		if (cCluster != nullptr)
		{
			delete cCluster;
		}
	}



	/**
	 * this method cares about the memory allocation for the stacks
	 */
	void setup_EmptyStacks(
			size_t i_max_depth_OR_elements_on_stack,	///< maximum number of elements stored on the stack
			unsigned int i_flags						///< flags to setup the stacks
	)
	{
		assert(cStacks == nullptr);

		cStacks = new CSimulationStacks_(
					i_max_depth_OR_elements_on_stack,
					i_flags
				);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		assert(cValidationStacks == nullptr);

		cValidationStacks = new CValidationStacks(	cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod	);
#endif
	}



	/**
	 * setup stacks during SPLIT operation!
	 */
	void setup_SplittedChildrenStacks(
			CCluster_SplitJoin_EdgeComm_Information &splitJoinInformation,
			bool i_reuse_parent_stacks
	)
	{
		reuse_parent_stacks = i_reuse_parent_stacks;

// never reuse parent's stacks for validation data
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks = new CValidationStacks(cTriangleFactory.maxDepth - cTriangleFactory.recursionDepthFirstRecMethod);
#endif

		unsigned int i_simulationStackFlags = cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->flags;

		/*
		 * allocate stacks
		 *
		 * only the simulation knows which stacks to use
		 */
		if (reuse_parent_stacks)
		{
			assert(cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD);

			cStacks = cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks;

			// set parent's stack to be reused by the child node
			cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks = nullptr;

			// second sub-triangle
			cStacks->structure_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements*2-1
				);

			cStacks->element_data_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements
				);
		}
		else
		{
			assert(cStacks == nullptr);

#if !CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
			cStacks = new CSimulationStacks_(
						// new maximum depth
						cTriangleFactory.maxDepth - cTriangleFactory.recursionDepthFirstRecMethod,
						i_simulationStackFlags
					);
#endif

			if (cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_FIRST_CHILD)
			{
#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.first_triangle.number_of_elements+(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CCellData_)),
							i_simulationStackFlags
						);
#endif
				// first sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->structure_stacks.forward,
						1,
						splitJoinInformation.first_triangle.number_of_elements*2-1
					);

				cStacks->element_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->element_data_stacks.forward,
						0,
						splitJoinInformation.first_triangle.number_of_elements
					);
			}
			else if (cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD)
			{
#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.second_triangle.number_of_elements+(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CCellData_)),
							i_simulationStackFlags
						);
#endif

				// second sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->structure_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements*2,
						splitJoinInformation.second_triangle.number_of_elements*2-1
					);

				cStacks->element_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->element_data_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements,
						splitJoinInformation.second_triangle.number_of_elements
					);
			}
			else
			{
				assert(false);
			}
		}

	}



	/**
	 * setup stacks during JOIN operation!
	 */
	void setup_JoinedStacks(
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation,
			bool p_reuse_second_child_stacks
	)
	{
		CSimulationStacks_* cFirstChildrenStacks = cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cStacks;
		CSimulationStacks_* cSecondChildrenStacks = cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cStacks;

		assert(cStacks == nullptr);

		if (p_reuse_second_child_stacks)
		{
			size_t first_triangle_number_of_elements = cFirstChildrenStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG
			size_t second_triangle_number_of_elements = cSecondChildrenStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

			if (cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

			/*
			 * SECOND SUB-TRIANGLE
			 */
			/*
			 * the stack of the second child was reused => steal it
			 */
			cStacks = cSecondChildrenStacks;

			// set second child node stack to be not allocated
			cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cStacks = nullptr;


			/*
			 * FIRST SUB-TRIANGLE
			 */
			/*
			 * structure data
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);


			cStacks->structure_stacks.forward.push(1);


			/*
			 * element data
			 */
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->element_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);
		}
		else
		{
			size_t first_triangle_number_of_elements = cFirstChildrenStacks->element_data_stacks.forward.getNumberOfElementsOnStack();
			size_t second_triangle_number_of_elements = cSecondChildrenStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG

			if (cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

#if !CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
			setup_EmptyStacks(cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod, cFirstChildrenStacks->flags);
#else
			setup_EmptyStacks(first_triangle_number_of_elements+second_triangle_number_of_elements+(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CCellData_)), cFirstChildrenStacks->flags);
#endif
			/*
			 * SECOND SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cSecondChildrenStacks->structure_stacks.forward,
					0,
					second_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cSecondChildrenStacks->element_data_stacks.forward,
					0,
					second_triangle_number_of_elements
				);

			/*
			 * FIRST SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->element_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);

			/*
			 * parent split element
			 */
			cStacks->structure_stacks.forward.push(1);
		}
	}



	/**
	 * this function is called for each leaf nodes
	 *
	 * if a splitting is requested, the cluster is split
	 */
	bool fun_splitAtLeaves()
	{
		// is a splitting permitted?
		if (!cCluster_SplitJoinInformation.splitting_permitted)
			return false;

		if (!(cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::SPLIT))
			return false;

		cCluster_SplitJoinActions.pass1_splitCluster();

		// reset split request flag
		cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;

		return true;
	}



	/**
	 * this function is executed for each mid-node of the tree
	 *
	 * it tests, whether both children are leaf nodes and requestes a join operation in this case.
	 */
	static void fun_testAndJoinAtMidNodes(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode
	)
	{
#if CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		if (i_cGeneric_TreeNode->base_triangulation_node)
			return;
#else
		if (i_cGeneric_TreeNode->cCluster_TreeNode == nullptr)
			return;
#endif

		/*
		 * 1) check whether both child nodes really exist.
		 *
		 * a single sub-cluster cannot be joined
		 */
		CCluster_TreeNode_ *first_node = i_cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
		CCluster_TreeNode_ *second_node = i_cGeneric_TreeNode->second_child_node->cCluster_TreeNode;

		if (first_node == nullptr)
			return;

		if (second_node == nullptr)
			return;


		/*
		 * 2) check if both child nodes are leaves
		 */
		if (!first_node->cGeneric_TreeNode->isLeaf())
			return;

		if (!second_node->cGeneric_TreeNode->isLeaf())
			return;

#if !CONFIG_SIERPI_DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		CCluster_SplitJoinActions<CCluster> &cCluster_SplitJoinActions = i_cGeneric_TreeNode->cCluster_TreeNode->cCluster_SplitJoinActions;
		assert(cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::NO_TRANSFER);
#endif

		/*
		 * check if there was a split operation at the children
		 */
		if (first_node->cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;

		if (second_node->cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;


		/*
		 * 3) check if both child nodes are permitted to join and also requested a join operation
		 */
		if (!first_node->cCluster_SplitJoinInformation.joining_permitted)
			return;

		if (!(first_node->cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::JOIN))
			return;

		if (!second_node->cCluster_SplitJoinInformation.joining_permitted)
			return;

		if (!(second_node->cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::JOIN))
			return;

		/*
		 * this method is executed by a static method since this class
		 * (CParititon_TreeNode) may be deleted due to a split operation
		 */
		CCluster_SplitJoinActions<CSimulation_Cluster>::pass1_joinChildClusters(i_cGeneric_TreeNode);
	}



	/**
	 * free stacks and reallocate them with empty stacks
	 */
	void resetStacks(
			unsigned int i_flags,		///< stack flags
			size_t initial_recursion_depth		///< initial recursion depth
	)
	{
		freeStacks();

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		setup_EmptyStacks((1 << initial_recursion_depth) + (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING/sizeof(CCellData_)), i_flags);
#else
		setup_EmptyStacks(cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod, i_flags);
#endif
	}



	/**
	 * free the stacks
	 */
	void freeStacks()
	{
		if (cStacks != nullptr)
		{
			delete cStacks;
			cStacks = nullptr;
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		if (cValidationStacks != nullptr)
		{
			delete cValidationStacks;
			cValidationStacks = nullptr;
		}
#endif
	}




	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CCluster_TreeNode &p
	)
	{
		co << "UniqueId: " << p.uniqueId << std::endl;
		co << p.cTriangleFactory << std::endl;
		co << "splitJoinActions:" << std::endl;
		co << p.cCluster_SplitJoinActions << std::endl;
		co << std::endl;

		co << p.cCluster_EdgeComm_InformationAdjacentClusters << std::endl;

		co << "***************************************************************" << std::endl;
		co << std::endl;

		return co;
	}

};

}

#endif
