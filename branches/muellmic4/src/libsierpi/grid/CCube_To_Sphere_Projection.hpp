/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 31. October 2012
 *      Author: Michael Müller <muellmic@in.tum.de>
 */

#ifndef CCUBE_TO_SPHERE_PROJECTIONS_HPP_
#define CCUBE_TO_SPHERE_PROJECTIONS_HPP_


/**
 * This class contains helper methods to project
 * 2D world coordinates to 2D coordinates on a cube face
 * and 3D coordinates on a unit sphere.
 */
class CCube_To_Sphere_Projection
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
public:

	/**
	 * Faces of the cube which is projected to the cubed sphere
	 */
	enum EFaceType {
		E_ERROR = 0,
		E_FACE_TOP,
		E_FACE_BOTTOM,
		E_FACE_FRONT,
		E_FACE_BACK,
		E_FACE_LEFT,
		E_FACE_RIGHT
	};



public:

	/**
	 * This method identifies the cube face of a triangle from its 2D position,
	 * assuming that the cube map lies in a 8x8 world, linearly scaled in x and
	 * y direction and centered at (0,0).
	 *
	 *
	 *
	 *           (-2,3)      (0,3)
	 *              +----------+
	 *              |          |
	 *              |          |
	 *              |   TOP    |
	 *(-4,1)        |          |        (2,1)      (4,1)
	 *   +----------+----------+----------+----------+
	 *   |          |          |          |          |
	 *   |          |          |          |          |
	 *   |   LEFT   |  FRONT   |  RIGHT   |   BACK   |
	 *   |          |          |          |          |
	 *   +----------+----------+----------+----------+
	 *(-4,-1)       |          |        (2,-1)     (4,-1)
	 *              |          |
	 *              |  BOTTOM  |
	 *              |          |
	 *              +----------+
	 *           (-2,-3)     (0,-3)
	 *
	 */
	static inline EFaceType getFaceOfCube(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x, T i_vertex_top_y
	){

		// compare sum of x- or y-coordinates to 3 times the interval boundaries

		// horizontal band
		if((i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) > (T)-3.0
				&& (i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) < (T)3.0){		// y between -1 and 1

			if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)-12.0){		// x < -4	ERROR
				return E_ERROR;

			} else if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)-6.0){	// x between -4 and -2
				return E_FACE_LEFT;

			} else if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)0.0){	// x between -2 and 0
				return E_FACE_FRONT;

			} else if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)6.0){	// x between 0 and 2
				return E_FACE_RIGHT;

			} else if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)12.0){	// x between 2 and 4
				return E_FACE_BACK;
			}


		}

		// vertical band
		if((i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) > (T)-6.0
				&& (i_vertex_left_x + i_vertex_right_x + i_vertex_top_x) < (T)0.0){		// x between -2 and 0

			if((i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) < (T)-9.0){		// y < -3	ERROR
				return E_ERROR;

			} else if((i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) < (T)-3.0){	// y between -3 and -1
				return E_FACE_BOTTOM;

			} else if((i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) < (T)3.0){	// y between -1 and 1
				return E_FACE_FRONT;

			} else if((i_vertex_left_y + i_vertex_right_y + i_vertex_top_y) < (T)9.0){	// y between 1 and 3
				return E_FACE_TOP;

			}
		}

		assert(false);
		return E_ERROR;
	}


	/**
	 * This method projects the 2D world coordinates of a triangle to the
	 * 2D coordinates of the corresponding cube face, assuming that the
	 * cube map lies in a 8x8 world, linearly scaled in x and y direction
	 * and centered at (0,0).
	 *
	 *
	 * INPUT REFERENCE SYSTEM:
	 *
	 *           (-2,3)      (0,3)
	 *              +----------+
	 *              |          |
	 *              |          |
	 *              |   TOP    |
	 *(-4,1)        |          |        (2,1)      (4,1)
	 *   +----------+----------+----------+----------+
	 *   |          |          |          |          |
	 *   |          |          |          |          |
	 *   |   LEFT   |  FRONT   |  RIGHT   |   BACK   |
	 *   |          |          |          |          |
	 *   +----------+----------+----------+----------+
	 *(-4,-1)       |          |        (2,-1)     (4,-1)
	 *              |          |
	 *              |  BOTTOM  |
	 *              |          |
	 *              +----------+
	 *           (-2,-3)     (0,-3)
	 *
	 *
	 *
	 * OUTPUT REFERENCE SYSTEM:
	 *
	 * (-1,1)      (1,1)
	 *    +----------+
	 *    |          |
	 *    |          |
	 *    |   FACE   |
	 *    |          |
	 *    +----------+
	 * (-1,-1)     (1,-1)
	 *
	 */
	template <typename T>
	inline static void project2DToCubeFace(
			CCube_To_Sphere_Projection::EFaceType i_face,
			T *io_vertex_left_x,	T *io_vertex_left_y,
			T *io_vertex_right_x,	T *io_vertex_right_y,
			T *io_vertex_top_x,		T *io_vertex_top_y
	)	{

		// translation to face
		switch(i_face){ // no change in y for LEFT, FRONT, RIGHT and BACK face
		case CCube_To_Sphere_Projection::E_FACE_LEFT:
			*io_vertex_left_x = *io_vertex_left_x + (T)3.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)3.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_FRONT:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_RIGHT:
			*io_vertex_left_x = *io_vertex_left_x - (T)1.0;
			*io_vertex_right_x = *io_vertex_right_x - (T)1.0;
			*io_vertex_top_x = *io_vertex_top_x - (T)1.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BACK:
			*io_vertex_left_x = *io_vertex_left_x - (T)3.0;
			*io_vertex_right_x = *io_vertex_right_x - (T)3.0;
			*io_vertex_top_x = *io_vertex_top_x - (T)3.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_TOP:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_left_y = *io_vertex_left_y - (T)2.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_right_y = *io_vertex_right_y - (T)2.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			*io_vertex_top_y = *io_vertex_top_y - (T)2.0;
			break;

		case CCube_To_Sphere_Projection::E_FACE_BOTTOM:
			*io_vertex_left_x = *io_vertex_left_x + (T)1.0;
			*io_vertex_left_y = *io_vertex_left_y + (T)2.0;
			*io_vertex_right_x = *io_vertex_right_x + (T)1.0;
			*io_vertex_right_y = *io_vertex_right_y + (T)2.0;
			*io_vertex_top_x = *io_vertex_top_x + (T)1.0;
			*io_vertex_top_y = *io_vertex_top_y + (T)2.0;
			break;

		default:
		case CCube_To_Sphere_Projection::E_ERROR:
			assert(false);
			break;
		}
	}


	/**
	 * coordinate system:
	 *
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 */
	template <typename T>
	inline static void projectToSphere(
			EFaceType face,
			T i_vertex_x, 	// face based x-coordinate
			T i_vertex_y, 	// face based y-coordinate,

			T *o_sphere_x,	// 3D x-coordinate on sphere
			T *o_sphere_y,	// 3D y-coordinate on sphere
			T *o_sphere_z	// 3D z-coordinate on sphere
	)
	{

		T x,y,z;

		switch(face){
		case E_FACE_LEFT:
			x = i_vertex_x;
			y = (T)-1.0;
			z = i_vertex_y;
			break;

		case E_FACE_FRONT:
			x = (T)1.0;
			y = i_vertex_x;
			z = i_vertex_y;
			break;

		case E_FACE_RIGHT:
			x = -i_vertex_x;
			y = (T)1.0;
			z = i_vertex_y;
			break;

		case E_FACE_BACK:
			x = (T)-1.0;
			y = -i_vertex_x;
			z = i_vertex_y;
			break;

		case E_FACE_TOP:
			x = -i_vertex_y;
			y = i_vertex_x;
			z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			x = i_vertex_y;
			y = i_vertex_x;
			z = (T)-1.0;
			break;

		default:
			x=y=z=(T)-1;
			break;
		}

		T d;
		d = std::sqrt((T)y * y + z * z + x * x);

		*o_sphere_x = (T)x / d;
		*o_sphere_y = (T)y / d;
		*o_sphere_z = (T)z / d;

	}

	/**
	 * coordinate system:
	 *
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 */
	template <typename T>
	inline static void projectFromSphereToCube(
			T *o_vertex_x, 	// face based x-coordinate
			T *o_vertex_y, 	// face based y-coordinate,

			T i_sphere_x,	// 3D x-coordinate on sphere
			T i_sphere_y,	// 3D y-coordinate on sphere
			T i_sphere_z	// 3D z-coordinate on sphere
	)
	{

		T x,y,z;

		T d;
		EFaceType face;


		if(i_sphere_x > 0 && i_sphere_x >= i_sphere_y && i_sphere_x >= i_sphere_z){
			d = i_sphere_x;
			face = E_FACE_FRONT;
		}else if(i_sphere_x < 0 && i_sphere_x <= i_sphere_y && i_sphere_x <= i_sphere_z){
			d = -i_sphere_x;
			face = E_FACE_BACK;
		}else if(i_sphere_y > 0 && i_sphere_y >= i_sphere_x && i_sphere_y >= i_sphere_z){
			d = i_sphere_y;
			face = E_FACE_RIGHT;
		}else if(i_sphere_y < 0 && i_sphere_y <= i_sphere_x && i_sphere_y <= i_sphere_z){
			d = -i_sphere_y;
			face = E_FACE_LEFT;
		}else if(i_sphere_z > 0 && i_sphere_z >= i_sphere_x && i_sphere_z >= i_sphere_y){
			d = i_sphere_z;
			face = E_FACE_TOP;
		}else if(i_sphere_z < 0 && i_sphere_z <= i_sphere_x && i_sphere_z <= i_sphere_y){
			d = -i_sphere_z;
			face = E_FACE_BOTTOM;
		}else{
			assert(false);
		}

		x = (T)i_sphere_x / d;
		y = (T)i_sphere_y / d;
		z = (T)i_sphere_z / d;

		switch(face){
		case E_FACE_LEFT:
			*o_vertex_x = x;
			//y = (T)-1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_FRONT:
			//x = (T)1.0;
			*o_vertex_x = y;
			*o_vertex_y = z;
			break;

		case E_FACE_RIGHT:
			*o_vertex_x = -x;
			//y = (T)1.0;
			*o_vertex_y = z;
			break;

		case E_FACE_BACK:
			//x = (T)-1.0;
			*o_vertex_x = -y;
			*o_vertex_y = z;
			break;

		case E_FACE_TOP:
			*o_vertex_y = -x;
			*o_vertex_x = y;
			//z = (T)1.0;
			break;

		case E_FACE_BOTTOM:
			*o_vertex_y = x;
			*o_vertex_x = y;
			//z = (T)-1.0;
			break;

		default:
			assert(false);
			x=y=z=(T)-1;
			break;
		}


	}
};

#endif
