/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingIPMO.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include <cmath>


#include "CPMO_TBB.hpp"

#include "../simulations/tsunami_parallel/CSimulationTsunami_ScalabilityGraph.hpp"

#include "CMainThreading_Interface.hpp"

/**
 * Threading support for invasive TBB
 */
class CMainThreading	:
	public CMainThreading_Interface
{
	/**
	 * PMO handler
	 */
	CPMO_TBB *cPmo;

	/**
	 * initial maximum number of cores
	 */
	int initial_max_cores;

	/**
	 * previous id used for invade scalability data
	 */
	int prev_unique_invade_id;


public:
	CMainThreading()	:
		initial_max_cores(512),
		prev_unique_invade_id(-1)
	{
	}

	void threading_setup()
	{
		if (getVerboseLevel() > 0)
		{
			std::cerr << "WARNING: Setting Affinities in TBB is experimental since additional threads are sometimes created" << std::endl;
			std::cerr << "Use `taskset` to assure execution of the program only on the given cores." << std::endl;
		}

		cPmo = new CPMO_TBB;

		if (getVerboseLevel() >= 5)
			std::cout << "max threads: " << (int)tbb::task_scheduler_init::default_num_threads() << std::endl;

		threading_updateResourceUtilization();
	}


	/**
	 * update the ressource utilization
	 */
	void threading_updateResourceUtilization()
	{
		unsigned long long workload = getSimulationWorkload();
		int scalability_graph_id = getThreadIPMOScalabilityGraphId();

		int max_cores;
		int scalability_graph_size;
		float *scalability_graph;
		const char *o_information_string;

		int id = CSimulationTsunami_ScalabilityGraph::getInvadeHintsAndConstraints(
				workload,
				scalability_graph_id,
				&max_cores,
				&scalability_graph_size,
				&scalability_graph,
				&o_information_string
		);

#if COMPILE_WITH_ITBB_ASYNC
		if (id != prev_unique_invade_id)
		{
			prev_unique_invade_id = id;
			cPmo->invade_nonblocking(1, max_cores, scalability_graph_size, scalability_graph);
		}

		cPmo->reinvade_nonblocking();

#else
		bool resources_updated;

		if (id != prev_unique_invade_id)
		{
			prev_unique_invade_id = id;
			resources_updated = cPmo->invade(1, max_cores, scalability_graph_size, scalability_graph);
		}
		else
		{
			resources_updated = cPmo->reinvade();
		}
#endif

		setValueNumberOfThreadsToUse(cPmo->num_running_threads);
	}



	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			// REINVADE
			threading_updateResourceUtilization();

			continue_simulation = simulation_loopIteration();

		} while(continue_simulation);
	}



	bool threading_simulationLoopIteration()
	{
		// REINVADE
		cPmo->reinvade();

		return simulation_loopIteration();
	}



	void threading_shutdown()
	{
		cPmo->client_shutdown_hint = ((double)getSimulationSumWorkload())*0.000001;
		cPmo->retreat();

		delete cPmo;
		cPmo = nullptr;
	}



	void threading_setNumThreads(int i)
	{
		initial_max_cores = i;
		threading_updateResourceUtilization();
	}



	virtual ~CMainThreading()
	{
		if (cPmo != nullptr)
			delete cPmo;
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
