#! /usr/bin/python

import sys
import subprocess
import os
import time
import commands

base_depth = 10

parallel_compilation_processes = 4

parallel_simulation_processes = -1

default_params=" -A 1 -n "+str(parallel_simulation_processes)

def testGCC():
	#
	# TEST GCC VERSION
	#
	reqversion = [4,6,1]

	#
	# get gcc version using -v instead of -dumpversion since SUSE gnu compiler
	# returns only 2 instead of 3 digits with -dumpversion
	#
	gccv = commands.getoutput('g++ -v').splitlines()
	gccversion = gccv[-1].split(' ')[2].split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ERROR: At least GCC Version 4.6.1 necessary.'
			sys.exit(-1)


def testIntelCompiler():
	#
	# TEST INTEL VERSION
	#
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'At least ICPC Version 12.1 necessary.'
			Exit(1)



mainmode = None
if len(sys.argv) > 1:
	mainmode = sys.argv[1]

subtests = None
if len(sys.argv) > 2:
	subtests = int(sys.argv[2])


prev_compiler_cmd=''

#
# executed the unit tests given by i
#
def unitTest(i, subtests):
	global prev_compiler_cmd
	CRED = '\033[91m'
	CGREEN = '\033[92m'
	CDEFAULT = '\033[0m'

	def print_err(s):
		print CRED+s+CDEFAULT

	def print_ok(s):
		print CGREEN+s+CDEFAULT

	print "Running subtest "+str(subtests)

	compiler_cmd = i[0]+' -j '+str(parallel_compilation_processes)
	exec_cmd = i[1]

	if compiler_cmd != prev_compiler_cmd:
		print "COMPILING: "+compiler_cmd
		p = subprocess.Popen(['make clean 2>&1 > /dev/null'], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO MAKE CLEAN!")
			return

		prev_compiler_cmd = compiler_cmd

		p = subprocess.Popen([compiler_cmd+' 2>&1 > /dev/null'], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO COMPILE! ("+compiler_cmd+")")
			sys.exit(-1)
	else:
		print "Using previously compiled program"

	print "EXECUTING: "+exec_cmd

	startTime = time.time()

	p = subprocess.Popen([exec_cmd+' 2>&1 > /dev/null'], shell=True)
	p.wait()
	if p.returncode != 0:
		print_err(" > TEST FAILED")
	else:
		print_ok(" > TEST OK ("+str(time.time() - startTime)+" Seconds)")

	print "*"*60


#
# GNU Compiler tests
#
if mainmode == None or mainmode=='gnu_compiler':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: gnu_compiler"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=off --simulation=tsunami_serial --mode=debug', './build/sierpi_gnu_tsunami_serial_debug -d '+str(base_depth-4)],
		['scons --compiler=gnu --threading=off --simulation=tsunami_parallel --mode=debug', './build/sierpi_gnu_tsunami_parallel_debug -d '+str(base_depth-3)],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=debug', './build/sierpi_gnu_omp_tsunami_parallel_debug -d '+str(base_depth-4)+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release', './build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-6)+' -o 4 '+default_params+' -a 6'],

	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# EVEN/ODD Triangle tests
#
if mainmode == None or mainmode=='even_odd':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: even_odd"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=debug', './build/sierpi_gnu_omp_tsunami_parallel_debug -d '+str(base_depth-4)+' -w 3'+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=debug', './build/sierpi_gnu_omp_tsunami_parallel_debug -d '+str(base_depth-4)+' -w 7'+default_params]
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# RK2 tests
#
if mainmode == None or mainmode=='rk2':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: rk2"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=debug --tsunami-runge-kutta-order=2', './build/sierpi_gnu_omp_tsunami_parallel_rk2_debug -d '+str(base_depth-4)+' -w 3'+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=debug --tsunami-runge-kutta-order=2', './build/sierpi_gnu_omp_tsunami_parallel_rk2_debug -d '+str(base_depth-4)+' -w 7'+default_params]
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# Special compile options
#
if mainmode == None or mainmode=='sco':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: sco"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-data=on', './build/sierpi_intel_omp_scan_data_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on', './build/sierpi_intel_omp_scan_threading_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-split-and-join=on', './build/sierpi_intel_omp_scan_threading_scan_saj_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-data=on --enable-scan-split-and-join=on --enable-scan-force-threading-traversal=on', './build/sierpi_intel_omp_scan_force_threading_scan_saj_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-split-and-join=on --enable-scan-force-threading-traversal=on', './build/sierpi_intel_omp_scan_force_threading_scan_saj_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-scan-force-threading-traversal=on', './build/sierpi_intel_omp_scan_force_threading_tsunami_parallel_debug -d '+str(base_depth-3)+' -T 100 '+default_params],

		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-skip-adaptive-conforming-sub-partitions=on', './build/sierpi_intel_omp_sacsp_tsunami_parallel_debug -d '+str(base_depth-5)+' '+default_params],

		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-tsunami-local-time-stepping=on', './build/sierpi_intel_omp_tsunami_parallel_debug -d '+str(base_depth-3)+' '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-opencl=on', './build/sierpi_intel_omp_tsunami_parallel_debug -d '+str(base_depth-3)+' '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug --enable-single-flux-evaluation-between-clusters=off', './build/sierpi_intel_omp_tsunami_parallel_debug -d '+str(base_depth-3)+' '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=debug  --tsunami-adaptivity-mode=2', './build/sierpi_intel_omp_tsunami_parallel_debug -d '+str(base_depth-3)+' '+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=release --enable-skip-adaptive-conforming-sub-partitions=on', './build/sierpi_intel_omp_sacsp_tsunami_parallel_release -d '+str(base_depth-3)+' -o 4098 '+default_params+' -a 6'],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# FLUX TESTS
#
if mainmode == None or mainmode=='fluxes':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: fluxes"
	print "*"*60

	tests = [
		# 1st order with fixed number of timesteps
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=0 --tsunami-order-of-basis-functions=1',
				'./build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-4)+' -T 2000'+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=1 --tsunami-order-of-basis-functions=1',
				'./build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-1)+' -T 2000'+default_params],
		# 0th order
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=2 --tsunami-order-of-basis-functions=0',
				'./build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=3 --tsunami-order-of-basis-functions=0',
				'./build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
		['scons --compiler=gnu --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=4 --tsunami-order-of-basis-functions=0',
				'./build/sierpi_gnu_omp_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=release --tsunami-flux-solver=5 --tsunami-order-of-basis-functions=0 --fp-default-precision=double',
				'./build/sierpi_intel_omp_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
	]


	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# Intel Compiler tests
#
if mainmode == None or mainmode=='intel_compiler':
	testIntelCompiler()

	print
	print "*"*60
	print "* MAIN TESTS: intel_compiler"
	print "*"*60

	tests = [
#		['scons --compiler=intel --threading=off --simulation=tsunami_serial --mode=release', './build/sierpi_intel_tsunami_serial_release -d '+str(base_depth+2)],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=release', './build/sierpi_intel_omp_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
		['scons --compiler=intel --threading=omp --simulation=tsunami_parallel --mode=release', './build/sierpi_intel_omp_tsunami_parallel_release -d '+str(base_depth-4)+' -o 4'+default_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# TBB Tests
#
if mainmode == None or mainmode=='tbb':
	print
	print "*"*60
	print "* MAIN TESTS: tbb"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=tbb --simulation=tsunami_parallel --mode=release', './build/sierpi_intel_tbb_tsunami_parallel_release -d '+str(base_depth-2)+default_params],
		['scons --compiler=intel --threading=tbb --simulation=tsunami_parallel --mode=release', './build/sierpi_intel_tbb_tsunami_parallel_release -d '+str(base_depth-4)+' -o 4'+default_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)
