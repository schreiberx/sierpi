/*
 * CAsagi.hpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#ifndef CASAGI_HPP_
#define CASAGI_HPP_

#define ASAGI_NOMPI
#include "asagi.h"
#include "../simulations/tsunami_common/types/CTsunamiTypes.hpp"



class CAsagi
{
	typedef CTsunamiSimulationTypes::TVisualizationVertexScalar T;

public:
	double bathymetry_size_x;
	double bathymetry_size_y;

	double bathymetry_min_x;
	double bathymetry_min_y;

	double bathymetry_max_x;
	double bathymetry_max_y;


	double displacements_size_x;
	double displacements_size_y;

	double displacements_min_x;
	double displacements_min_y;

	double displacements_max_x;
	double displacements_max_y;


	/**
	 * set to true when setup was successful
	 */
	bool setup_ok;

	/**
	 * singleton for bathymetry datasets
	 */
	asagi::Grid *bathymetry_grid;

	/**
	 * singleton for displacements
	 */
	asagi::Grid *displacements_grid;

public:
	CAsagi(
			const char *i_filename_bathymetry,	///< filename for bathymetry data
			const char *i_filename_displacements	///< filename for displacement data
		);

	void outputVerboseInformation();

	CTsunamiSimulationTypes::TVertexScalar getBathymetry(
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_depth
		);


	CTsunamiSimulationTypes::TVertexScalar getDisplacement(
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
			CTsunamiSimulationTypes::TVisualizationVertexScalar i_depth
		);


	virtual ~CAsagi();
};

#endif /* CASAGI_HPP_ */
