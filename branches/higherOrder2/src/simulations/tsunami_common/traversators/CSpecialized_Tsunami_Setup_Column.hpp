/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_VERTICES_DEPTHLIMITER_ELEMENT_DATA_HPP_


#include "../../tsunami_common/types/CTsunamiTypes.hpp"
#include "libsierpi/stacks/CFBStacks.hpp"
#include "libsierpi/parallelization/CPartition_SplitJoinInformation.hpp"
#include "../CTsunamiSimulationDataSets.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Tsunami_Setup_Column
{
private:
	class CSpecialized_Tsunami_Setup_Column_Private *generic_traversator;

	bool repeat_traversal;

public:
	CSpecialized_Tsunami_Setup_Column();

	virtual ~CSpecialized_Tsunami_Setup_Column();

	void setSetupMethod(int i);

	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CTsunamiSimulationStacks *cStacks);

	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CTsunamiSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CTsunamiSimulationStacks *cStacks,
			CPartition_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	);

	/**
	 * setup the initial root domain partition
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);


	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_RootTraversator(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);


	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			TTsunamiDataScalar p_x,
			TTsunamiDataScalar p_y,
			TTsunamiDataScalar p_radius,

			int i_methodId,
			CTsunamiSimulationDataSets *i_cSimulationDataSets
	);



	/**
	 * setup the child partition based on it's parent traversator information and the child's factory
	 */
	void setup_Partition(
			CSpecialized_Tsunami_Setup_Column &parent,
			CTriangle_Factory &i_triangleFactory
		);

	void storeReduceValue(
			TTsunamiDataScalar *o_cflReduceValue
		);
};

}
}

#endif
