/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * tsunami_config.h
 *
 * Precompiler settings for tsunami simulation
 *
 *  Created on: June 21, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#include "../../config.h"


/**
 * ASAGI
 */
#ifndef CONFIG_ENABLE_ASAGI
	#define CONFIG_ENABLE_ASAGI		1
#endif


#ifndef SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER
#error "SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER not defined!"
#define SIMULATION_TSUNAMI_RUNGE_KUTTA_ORDER 1
#endif

#ifndef SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS
#error "SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS not defined"
#define SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS 1
#endif

#ifndef SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS
#error "SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS not defined"
#define SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS 0
#endif

#define SIMULATION_TSUNAMI_NUMBER_OF_DOFS (((SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS+1)*(SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS+2))/2)

#ifndef SIMULATION_TSUNAMI_ADAPTIVITY_MODE
#error "SIMULATION_TSUNAMI_ADAPTIVITY_MODE not defined"
#define SIMULATION_TSUNAMI_ADAPTIVITY_MODE 1
#endif



/**
 * enable/disable compilation with tests for tsunami vertex coordinates
 */
#if DEBUG
	#define COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION		1
#else
	#define COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION		0
#endif

