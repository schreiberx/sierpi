/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jul 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_CADAPTIVE_TSUNAMI_2NDORDER_HPP_
#define KERNEL_CADAPTIVE_TSUNAMI_2NDORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

//#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData.hpp"
#include "../common/CAdaptivity_2ndOrder.hpp"

#include "lib/CDebugSetupDone.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 2nd order
 */
class CAdaptive_Tsunami_2ndOrder	: public CAdaptivity_2ndOrder
{
	DEBUG_SETUP_DONE_VAR
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_ElementData<CAdaptive_Tsunami_2ndOrder, CTsunamiElementData>	TRAV;
	typedef TTsunamiVertexScalar TVertexScalar;

	/*
	 * refinement/coarsening parameters
	 */
	TTsunamiDataScalar refine_height_threshold;
	TTsunamiDataScalar coarsen_height_threshold;

	TTsunamiDataScalar refine_slope_threshold;
	TTsunamiDataScalar coarsen_slope_threshold;


	CAdaptive_Tsunami_2ndOrder()	:
		refine_height_threshold(-1),
		coarsen_height_threshold(-1),
		refine_slope_threshold(-1),
		coarsen_slope_threshold(-1)
	{
		DEBUG_SETUP_DONE_CONSTRUCTOR;
	}


	inline bool should_refine(
			TTsunamiVertexScalar vx1, TTsunamiVertexScalar vy1,
			TTsunamiVertexScalar vx2, TTsunamiVertexScalar vy2,
			TTsunamiVertexScalar vx3, TTsunamiVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,
			CTsunamiElementData *element
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (element->refine)
		{
			element->refine = false;
			return true;
		}
#endif

		/**
		 * adaptive refinement triggered via height
		 */

		TTsunamiDataScalar h = (TTsunamiDataScalar)(1.0/3.0)*(
					element->hyp_edge.h+element->hyp_edge.b+
					element->right_edge.h+element->right_edge.b+
					element->left_edge.h+element->left_edge.b
				);
		if (h > refine_height_threshold)
			return true;

#if 0
		/**
		 * adaptive refinement triggered via slope
		 */
		TTsunamiDataScalar d1 = CMath::abs(element->hyp_edge.h - element->left_edge.h);
		TTsunamiDataScalar d2 = CMath::abs(element->right_edge.h - element->left_edge.h)*CMath::sqrt2<TTsunamiDataScalar>();
		TTsunamiDataScalar d3 = CMath::abs(element->hyp_edge.h - element->right_edge.h);

		// TODO: do this computation only once
		TTsunamiDataScalar m = CMath::max(d1, CMath::max(d2, d3))/(getUnitCathetusLengthForDepth(depth)*cathetus_side_length);

		if (m > refine_slope_threshold)
			return true;
#endif

		return false;
	}


	inline bool should_coarsen(
			TTsunamiVertexScalar vx1, TTsunamiVertexScalar vy1,
			TTsunamiVertexScalar vx2, TTsunamiVertexScalar vy2,
			TTsunamiVertexScalar vx3, TTsunamiVertexScalar vy3,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,
			CTsunamiElementData *element
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (element->coarsen)
		{
			element->coarsen = false;
			return true;
		}
#endif

		/**
		 * adaptive coarsening triggered via height
		 */
//		TTsunamiDataScalar h = CMath::max(element->hyp_edge.h, CMath::max(element->right_edge.h, element->left_edge.h));

		TTsunamiDataScalar h = (TTsunamiDataScalar)(1.0/3.0)*(
					element->hyp_edge.h+element->hyp_edge.b+
					element->right_edge.h+element->right_edge.b+
					element->left_edge.h+element->left_edge.b
				);

		if (h > coarsen_height_threshold)
			return false;


#if 0
		/**
		 * adaptive refinement triggered via slope
		 */
		TTsunamiDataScalar d1 = CMath::abs(element->hyp_edge.h - element->left_edge.h);
		TTsunamiDataScalar d2 = CMath::abs(element->right_edge.h - element->left_edge.h)*CMath::sqrt2<TTsunamiDataScalar>();
		TTsunamiDataScalar d3 = CMath::abs(element->hyp_edge.h - element->right_edge.h);

		// TODO: do this computation only once
		// we can reuse this value assuming that should_refine is always called before should_coarsen - TODO: is this true?!?
		TTsunamiDataScalar m = CMath::max(d1, CMath::max(d2, d3))/(getUnitCathetusLengthForDepth(depth)*cathetus_side_length);

		if (m > coarsen_slope_threshold)
			return false;
#endif
		return true;
	}

	void storeReduceValue(
			TReduceValue *o_reduceValue
	)
	{
		*o_reduceValue = cfl_domain_size_div_max_wave_speed;
	}
	


public:
	/**
	 * element action call executed for unmodified element data
	 */
	inline void elementAction(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *io_elementData
	)
	{
	}

public:
	inline void refine_l_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *o_left_elementData,
			CTsunamiElementData *o_right_elementData
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif

		setupRefinedElements(
			vleft_x,	vleft_y,
			vright_x,	vright_y,
			vtop_x,		vtop_y,

			normal_hypx,	normal_hypy,
			normal_rightx,	normal_righty,
			normal_leftx,	normal_lefty,

			depth,

			i_elementData,
			o_left_elementData,
			o_right_elementData
		);

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		o_coarsed_element->refine = false;
		o_coarsed_element->coarsen = false;
#endif
/*
 NOTE! VALIDATION SETUP IS DONE IN setupRefinedElements.
 VALIDATION SETUP IS NOT ALLOOWED HERE DUE TO SPECIFIC ORDER TO SETUP REFINED ELEMENTS

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_left_elementData->validation.setupLeftElementFromParent(&i_elementData->validation);
		o_right_elementData->validation.setupRightElementFromParent(&i_elementData->validation);
#endif
*/
	}



	inline void refine_ll_r(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx,		TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx,	TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx,	TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,

					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,

					depth,

					i_elementData, left_right_element, right_element
				);

		// LEFT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,

					depth+1,

					left_right_element, left_left_element, left_right_element
				);
	}



	inline void refine_l_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// LEFT + RIGHT
		refine_l_r(
				vleft_x, vleft_y,
				vright_x, vright_y,
				vtop_x, vtop_y,
				normal_hypx, normal_hypy,
				normal_rightx, normal_righty,
				normal_leftx, normal_lefty,
				depth,
				i_elementData, left_element, right_right_element
			);

		// RIGHT children (LEFT + RIGHT)
		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,

					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,

					depth+1,

					right_right_element, right_left_element, right_right_element);
	}



	inline void refine_ll_rr(
			TTsunamiVertexScalar vleft_x,	TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x,	TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x,	TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *i_elementData,
			CTsunamiElementData *left_left_element,
			CTsunamiElementData *left_right_element,
			CTsunamiElementData *right_left_element,
			CTsunamiElementData *right_right_element
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.testVertices(vleft_x, vleft_y, vright_x, vright_y, vtop_x, vtop_y);
#endif


		// RIGHT + LEFT
		refine_l_r(
					vleft_x, vleft_y,
					vright_x, vright_y,
					vtop_x, vtop_y,
					normal_hypx, normal_hypy,
					normal_rightx, normal_righty,
					normal_leftx, normal_lefty,
					depth,
					i_elementData, left_right_element, right_right_element);

		TTsunamiVertexScalar hyp_mid_edge_x = (vleft_x+vright_x)*(TTsunamiVertexScalar)0.5;
		TTsunamiVertexScalar hyp_mid_edge_y = (vleft_y+vright_y)*(TTsunamiVertexScalar)0.5;

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vtop_x, vtop_y,
					vleft_x, vleft_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
	
					normal_leftx, normal_lefty,
					normal_hypx, normal_hypy,
					-normal_hypy, normal_hypx,
			
					depth+1,
					left_right_element, left_left_element, left_right_element);

		// LEFT children (LEFT + RIGHT)
		refine_l_r(
					vright_x, vright_y,
					vtop_x, vtop_y,
					hyp_mid_edge_x, hyp_mid_edge_y,
					
					normal_rightx, normal_righty,
					normal_hypy, -normal_hypx,
					normal_hypx, normal_hypy,
					
					depth+1,

					right_right_element, right_left_element, right_right_element);
	}


	inline void coarsen(
			TTsunamiVertexScalar vleft_x, TTsunamiVertexScalar vleft_y,
			TTsunamiVertexScalar vright_x, TTsunamiVertexScalar vright_y,
			TTsunamiVertexScalar vtop_x, TTsunamiVertexScalar vtop_y,

			TTsunamiDataScalar normal_hypx, TTsunamiDataScalar normal_hypy,
			TTsunamiDataScalar normal_rightx, TTsunamiDataScalar normal_righty,
			TTsunamiDataScalar normal_leftx, TTsunamiDataScalar normal_lefty,

			int depth,

			CTsunamiElementData *o_coarsed_elementData,

			CTsunamiElementData *i_left_elementData,
			CTsunamiElementData *i_right_elementData
	)
	{
		setupCoarsendElements(
			vleft_x,	vleft_y,
			vright_x,	vright_y,
			vtop_x,	vtop_y,

			normal_hypx,	normal_hypy,
			normal_rightx,	normal_righty,
			normal_leftx,	normal_lefty,

			depth,

			o_coarsed_elementData,
			i_left_elementData,
			i_right_elementData
		);
	}


	void setup_WithParameters(
			TTsunamiDataScalar p_cathetus_side_length,

			TTsunamiDataScalar p_refine_height_threshold,
			TTsunamiDataScalar p_coarsen_height_threshold,

			TTsunamiDataScalar p_refine_slope_threshold,
			TTsunamiDataScalar p_coarsen_slope_threshold,

			CTsunamiSimulationDataSets *i_cSimulationDataSets
	)
	{
		DEBUG_SETUP_DONE_SETUP;

		refine_height_threshold = p_refine_height_threshold;
		coarsen_height_threshold = p_coarsen_height_threshold;

		refine_slope_threshold = p_refine_slope_threshold;
		coarsen_slope_threshold = p_coarsen_slope_threshold;

		cathetus_side_length = p_cathetus_side_length;

		cSimulationDataSets = i_cSimulationDataSets;
	}


	void setup_WithKernel(const CAdaptive_Tsunami_2ndOrder &parent)
	{
		DEBUG_SETUP_DONE_SETUP;

		refine_height_threshold = parent.refine_height_threshold;
		coarsen_height_threshold = parent.coarsen_height_threshold;

		refine_slope_threshold = parent.refine_slope_threshold;
		coarsen_slope_threshold = parent.coarsen_slope_threshold;

		cathetus_side_length = parent.cathetus_side_length;

		cSimulationDataSets = parent.cSimulationDataSets;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
