#include "../../CTsunamiConfig.hpp"



#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0

#include "CAdaptive_Tsunami_0thOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Tsunami_0thOrder CAdaptive_Tsunami;
	}
}

#elif SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==1

#include "CAdaptive_Tsunami_1stOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Tsunami_1stOrder CAdaptive_Tsunami;
	}
}

#else

#include "CAdaptive_Tsunami_2ndOrder.hpp"
namespace sierpi
{
	namespace kernels
	{
		typedef CAdaptive_Tsunami_2ndOrder CAdaptive_Tsunami;
	}
}

#endif
