/*
 * CMainThreadingTBB.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGDUMMY_HPP_
#define CMAINTHREADINGDUMMY_HPP_

#include <pthread.h>

#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
public:
	void threading_setup()
	{
#if 0
		/*
		 * pin the first pid to the first cpu
		 */
		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);
		CPU_SET(0, &cpu_set);

		int retval = sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpu_set);
		assert(retval == 0);
#endif

		setMaxNumberOfThreads(1);
		setValueNumberOfThreadsToUse(1);
	}


	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			continue_simulation = simulation_loopIteration();

		} while(continue_simulation);
	}

	bool threading_simulationLoopIteration()
	{
		return simulation_loopIteration();
	}

	void threading_shutdown()
	{
	}

	void threading_setNumThreads(int i)
	{
	}

	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGDUMMY_HPP_ */
