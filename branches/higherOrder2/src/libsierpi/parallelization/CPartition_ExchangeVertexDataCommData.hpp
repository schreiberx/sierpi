/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_ExchangeFluxCommData.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITIONTREE_EXCHANGE_VERTEXDATA_COMM_HPP
#define CPARTITIONTREE_EXCHANGE_VERTEXDATA_COMM_HPP

#include "config.h"
#include "libsierpi/domain_triangulation/CDomain_BaseTriangle.hpp"
#include "CPartition_EdgeComm_InformationAdjacentPartitions.hpp"
#include <string.h>



/**
 * this class handles the communication with the adjacent edges based on the adjacency
 * information given in CPartitionTree_Node.
 */
template <	typename CPartition_TreeNode,			///< partition tree node
			typename CVertexData,					///< type of single edge element
			typename CStackAccessors_,				///< access to stacks
			typename CKernelWith_computeNetUpdates	///< kernel type offering 'computeNetUpdates'
		>
class CPartition_ExchangeVertexDataCommData	: public CStackAccessors_
{
private:
	typedef CPartition_EdgeComm_InformationAdjacentCluster<CPartition_TreeNode> CEdgeComm_InformationAdjacentPartition_;

private:
	CStack<CVertexData> *localHypExchangeEdgeStack;
	CStack<CVertexData> *localCatExchangeEdgeStack;

	CStack<CVertexData> *localHypEdgeStack;
	CStack<CVertexData> *localCatEdgeStack;

	/**
	 * Handler to root partitionTreeNode
	 */
	CPartition_TreeNode *cPartitionTreeNode;

	/**
	 * true, when traversal along hyp/cat is clockwise
	 */
	bool hypClockwise, catClockwise;


	/**
	 * Pointer to kernel offering method 'computeNetUpdates' to
	 * compute the flux updates
	 */
	CKernelWith_computeNetUpdates *cKernelWith_computeNetUpdates;


	/**
	 * Constructor
	 */
public:
	CPartition_ExchangeVertexDataCommData(
			CPartition_TreeNode *p_cPartitionTreeNode,
			CKernelWith_computeNetUpdates *p_cKernelWith_computeNetUpdates
	)
	{
		cPartitionTreeNode = p_cPartitionTreeNode;
		cKernelWith_computeNetUpdates = p_cKernelWith_computeNetUpdates;
	}


	/**
	 * return the information about where to find the communication data at the adjacent triangle stack
	 */
private:
	inline void p_getVertexCommDataPointersFromAdjacentPartition(
			CPartition_UniqueId &i_thisUniqueId,				///< unique id of partition to write data to
			CPartition_TreeNode *i_adjacentPartitionTreeNode,	///< pointer to adjacent partition

			CVertexData **o_adjacent_firstVertexCommElement,	///< output: pointer to first vertex to be exchanged

			CVertexData **o_adjacent_middleVertexCommElement,	///< output: pointer to starting point of middle vertex data to be exchanged
			int *o_middleVertexCommElementCount,				///< output: number of middle vertex comm elements stored on the stack

			CVertexData **o_adjacent_lastVertexCommElement,		///< output: pointer to last vertex to be exchanged

			bool *o_clockwise									///< output: true, if edge elements are stored with clockwise traversal
	)	const
	{
		int pos;

		CStack<CVertexData> *adjHypStack;
		CStack<CVertexData> *adjCatStack;

		/*
		 * HYP COMM:
		 *
		 * first of all, we search on the hyp communication information to find the appropriate subset
		 */
		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
					i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
				iter != i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_info = *iter;


			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				*o_middleVertexCommElementCount = adjacent_info.commElements-1;

				if (i_adjacentPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
				{
					assert(false);	// TODO

					adjHypStack = leftStackAccessor(i_adjacentPartitionTreeNode);
					*o_adjacent_middleVertexCommElement = adjHypStack->getElementPtrAtIndex(pos);

					/*
					 * Figure out first and last vertex pointer:
					 * Vertices on the entrance and exit of the 'V' curve are stored to the hypotenuse.
					 * Therefore the first and last vertex is adjacent to the block.
					 */
					*o_adjacent_firstVertexCommElement = *o_adjacent_middleVertexCommElement;
					(*o_adjacent_middleVertexCommElement)++;
					*o_adjacent_lastVertexCommElement = *o_adjacent_middleVertexCommElement+adjacent_info.commElements+1;

					*o_clockwise = true;
				}
				else
				{
					adjHypStack = rightStackAccessor(i_adjacentPartitionTreeNode);
#if 1
					std::cout << "ADJ HYP STACK: " << adjHypStack << std::endl;
					for (size_t i = 0; i < adjHypStack->getNumberOfElementsOnStack(); i++)
					{
						std::cout << adjHypStack->getElementAtIndex(i).validation << std::endl;
					}
					std::cout << std::endl;
#endif

					*o_adjacent_middleVertexCommElement = adjHypStack->getElementPtrAtIndex(pos);

					/*
					 * Figure out first and last vertex pointer:
					 * Vertices on the entrance and exit of the 'V' curve are stored to the hypotenuse.
					 * Therefore the first and last vertex is adjacent to the block.
					 */


					*o_adjacent_lastVertexCommElement = *o_adjacent_middleVertexCommElement;
					*o_adjacent_firstVertexCommElement = *o_adjacent_middleVertexCommElement+adjacent_info.commElements;

					// fix pointer to middle vertices
					(*o_adjacent_middleVertexCommElement)++;

					*o_clockwise = false;
				}

				return;
			}

			pos += adjacent_info.commElements;
		}



		/*
		 * CAT COMM
		 */
		pos = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
					i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
				iter != i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentPartition_ &adjacent_info = *iter;
			if (adjacent_info.uniqueId != i_thisUniqueId)
			{
				pos += adjacent_info.commElements;
				continue;
			}

			/**
			 * handle EDGE COMM
			 */
			if (i_adjacentPartitionTreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack())
			{
				adjCatStack = leftStackAccessor(i_adjacentPartitionTreeNode);
				adjHypStack = rightStackAccessor(i_adjacentPartitionTreeNode);

				*o_middleVertexCommElementCount = adjacent_info.commElements-1;
				*o_clockwise = true;

				*o_adjacent_middleVertexCommElement = adjCatStack->getElementPtrAtIndex(pos);

				assert(false);	// TODO
			}
			else
			{
				adjCatStack = rightStackAccessor(i_adjacentPartitionTreeNode);
				adjHypStack = leftStackAccessor(i_adjacentPartitionTreeNode);

				*o_middleVertexCommElementCount = adjacent_info.commElements-1;
				*o_clockwise = false;

				/*
				 * fix position on stack when elements have to be skipped
				 */
				if (i_adjacentPartitionTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				{
					pos += adjCatStack->getNumberOfElementsOnStack() - i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.getSumOfEdgeCommElementsOnCatheti();

					*o_adjacent_firstVertexCommElement = adjCatStack->getElementPtrAtIndex(pos);
					*o_adjacent_middleVertexCommElement = adjCatStack->getElementPtrAtIndex(pos+1);

//					std::cout << " >>>>>>>>> " << adjCatStack->getNumberOfElementsOnStack() << std::endl;
//					std::cout << "               FIRST: " << (**o_adjacent_firstVertexCommElement).validation << std::endl;

					iter++;
					if (iter == i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end())
					{
						*o_adjacent_lastVertexCommElement = adjHypStack->getTopElementPtr();
					}
					else
					{
						// TODO
						assert(false);
						*o_adjacent_lastVertexCommElement = adjCatStack->getElementPtrAtIndex(pos+adjacent_info.commElements);
					}
				}
				else
				{
					// TODO
					assert(false);
					if (iter == i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin())
					{
						std::cout << "FIRST VERTEX COMM ELEMENT ON HYP" << std::endl;
						*o_adjacent_firstVertexCommElement = adjCatStack->getElementPtrAtIndex(pos);
						*o_adjacent_lastVertexCommElement = adjHypStack->getTopElementPtr();
					}
				}
//				std::cout << "               FIRST: " << (**o_adjacent_firstVertexCommElement).validation << std::endl;
//				std::cout <<                 "LAST: " << (**o_adjacent_lastVertexCommElement).validation << std::endl;

				*o_adjacent_middleVertexCommElement = adjCatStack->getElementPtrAtIndex(pos);

				/*
				 * if
				 *   1) this edge comm is the first one in the RLE edge comm list and
				 *   2) the right or left edge which is closest to the traversal starting point is not of type boundary
				 *
				 * the first vertex is stored on the first element on the hypotenuse, otherwise
				 * on the vertex comm stacks
				 */
#if 0
				if (iter == i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin())
				{
					// scene 2
//					std::cout << "LAST VERTEX COMM ELEMENT ON HYP" << std::endl;

					*o_adjacent_firstVertexCommElement = adjCatStack->getElementPtrAtIndex(pos);
					*o_adjacent_lastVertexCommElement = adjHypStack->getTopElementPtr();
				}
				else
				{
					*o_adjacent_firstVertexCommElement = *o_adjacent_middleVertexCommElement;
					(*o_adjacent_middleVertexCommElement)++;

					iter++;
					if (iter == i_adjacentPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end())
					{
						std::cout << "LAST VERTEX COMM ELEMENT ON HYP" << std::endl;
						*o_adjacent_lastVertexCommElement = adjHypStack->getTopElementPtr();
						std::cout << (*o_adjacent_lastVertexCommElement)->validation << std::endl;
					}
					else
					{
						*o_adjacent_lastVertexCommElement = *o_adjacent_middleVertexCommElement+adjacent_info.commElements;
					}
				}
#endif
			}

			return;
		}

		assert(false);
	}



	/**
	 * this method is executed for every adjacent partition simulation to
	 * pull the edge communication data from an adjacent partition.
	 */
private:
	void p_pullVertexCommDataFromPartitionToCathetiStack(
			CStack<CVertexData> *io_cLocalExchangeStack,			///< stack to store edge element data to
			CStack<CVertexData> *io_cLocalStack,					///< stack with previously written data needed to compute the flux
			bool i_clockwise,										///< order in which to store data
			const CEdgeComm_InformationAdjacentPartition_ &i_informationAdjacentPartition,	///< information about adjacent partitions
			unsigned long local_edge_comm_stack_index,				///< index to local edge comm stack to start copying data

			CVertexData *io_firstVertexOnHypotenuse = nullptr,		///< if this pointer is not nullptr, the first vertex is stored at this position
			CVertexData *io_firstVertexOnHypotenuseExchangeStackPtr = nullptr,

			CVertexData *io_lastVertexOnHypotenuse = nullptr,		///< if this pointer is not nullptr, the last vertex is stored at this position
			CVertexData *io_lastVertexOnHypotenuseExchangeStackPtr = nullptr
	)
	{
		if (i_informationAdjacentPartition.commElements == 0)
			return;


		/*
		 * first we search for the adjacent partition
		 */
		CVertexData *adjacent_middleVertexCommElements = nullptr;
		CVertexData *adjacent_firstVertexCommElement = nullptr;
		CVertexData *adjacent_lastVertexCommElement = nullptr;

		int vertexCommDataCount = 0;
		bool adj_clockwise = true;

		p_getVertexCommDataPointersFromAdjacentPartition(
					cPartitionTreeNode->uniqueId,		///< unique id of this partition
					i_informationAdjacentPartition.partitionTreeNode,	///< pointer to adjacent partition tree node

					&adjacent_firstVertexCommElement,	///< pointer to first vertex

					&adjacent_middleVertexCommElements,	///< pointer to middle vertex data
					&vertexCommDataCount,				///< number of elements stored on stack

					&adjacent_lastVertexCommElement,	///< pointer to last vertex

					&adj_clockwise						///< adjacent data stored in clockwise order?
				);
/*
		std::cout << std::endl;
		std::cout << "first vertex:" << adjacent_firstVertexCommElement->validation << std::endl;
		std::cout << "vertexCommDataCount: " << vertexCommDataCount << std::endl;
		std::cout << "last vertex:" << adjacent_lastVertexCommElement->validation << std::endl;
		std::cout << std::endl;
*/

		if (i_informationAdjacentPartition.commElements-1 != vertexCommDataCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + partition id: " << cPartitionTreeNode->uniqueId << std::endl;
			assert(false);
		}


		CVertexData tmpEdgeElement;

		if (i_clockwise != adj_clockwise)
		{
			assert(false);	// TODO: implement

			CVertexData *local_edgeCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			CVertexData *local_exchangeEdgeCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			for (int i = 0; i < vertexCommDataCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_edgeCommStackElement,					/// left vertex data comm element
						adjacent_firstVertexCommElement,			/// adjacent right vertex data comm element

						local_exchangeEdgeCommStackElement			/// output: local vertex data
					);

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
			}
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			CVertexData *local_vertexCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			CVertexData *local_exchangeVertexCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			/*
			 * adjacent LAST VERTEX HANDLING
			 */
			std::cout << "LAST VERTEX HANDLING" << std::endl;

			if (io_firstVertexOnHypotenuse)
			{
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						io_firstVertexOnHypotenuse,				/// left vertex data comm element
						adjacent_lastVertexCommElement,			/// adjacent right vertex data comm element

						io_firstVertexOnHypotenuseExchangeStackPtr	/// output: local vertex data
					);
			}
			else
			{
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_vertexCommStackElement,			/// left vertex data comm element
						adjacent_lastVertexCommElement,			/// adjacent right vertex data comm element

						local_exchangeVertexCommStackElement	/// output: local vertex data
					);
			}

			local_vertexCommStackElement++;
			local_exchangeVertexCommStackElement++;

			/*
			 * MIDDLE VERTEX HANDLING
			 */
			size_t adj_i = vertexCommDataCount;

			std::cout << "MIDDLE VERTEX HANDLING " << vertexCommDataCount << std::endl;

			for (int i = 0; i < vertexCommDataCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_vertexCommStackElement,				/// left vertex data comm element
						&adjacent_middleVertexCommElements[adj_i],			/// adjacent right vertex data comm element

						local_exchangeVertexCommStackElement		/// output: local vertex data
					);

				local_vertexCommStackElement++;
				local_exchangeVertexCommStackElement++;
				adj_i--;
			}

			/*
			 * adjacent FIRST VERTEX handling
			 */
			std::cout << "FIRST VERTEX HANDLING" << std::endl;

			if (io_lastVertexOnHypotenuse)
			{
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						io_lastVertexOnHypotenuse,				/// left vertex data comm element
						adjacent_firstVertexCommElement,		/// adjacent right vertex data comm element

						io_lastVertexOnHypotenuseExchangeStackPtr	/// output: local vertex data
					);
			}
			else
			{
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_vertexCommStackElement,			/// left vertex data comm element
						adjacent_firstVertexCommElement,		/// adjacent right vertex data comm element

						local_exchangeVertexCommStackElement	/// output: local vertex data
					);
			}
		}
	}



	/**
	 * this method is executed for every adjacent partition simulation to
	 * pull the edge communication data from an adjacent partition.
	 */
private:
	void p_pullVertexCommDataFromPartitionToHypotenuseStack(
			CStack<CVertexData> *io_cLocalExchangeStack,	///< stack to store edge element data to
			CStack<CVertexData> *io_cLocalStack,			///< stack with previously written data needed to compute the flux
			bool i_clockwise,								///< order in which to store data
			const CEdgeComm_InformationAdjacentPartition_ &i_informationAdjacentPartition,	///< information about adjacent partitions
			unsigned long local_edge_comm_stack_index		///< index to local edge comm stack to start copying data
	)
	{
		if (i_informationAdjacentPartition.commElements == 0)
			return;

		/*
		 * first we search for the adjacent partition
		 */
		CVertexData *adjacent_middleVertexCommElements = nullptr;
		CVertexData *adjacent_firstVertexCommElement = nullptr;
		CVertexData *adjacent_lastVertexCommElement = nullptr;
		int vertexCommDataCount = 0;
		bool adj_clockwise = true;

		p_getVertexCommDataPointersFromAdjacentPartition(
					cPartitionTreeNode->uniqueId,		///< unique id of this partition
					i_informationAdjacentPartition.partitionTreeNode,	///< pointer to adjacent partition tree node

					&adjacent_firstVertexCommElement,	///< pointer to first vertex

					&adjacent_middleVertexCommElements,	///< pointer to middle vertices
					&vertexCommDataCount,				///< number of elements store don stack

					&adjacent_lastVertexCommElement,	///< pointer to last vertex

					&adj_clockwise						///< adjacent data stored in clockwise order?
		);

		if (i_informationAdjacentPartition.commElements-1 != vertexCommDataCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + partition id: " << cPartitionTreeNode->uniqueId << std::endl;
			assert(false);
		}

		CVertexData tmpEdgeElement;

		if (i_clockwise != adj_clockwise)
		{
			assert(false);	// TODO: implement

			CVertexData *local_edgeCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			CVertexData *local_exchangeEdgeCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			for (int i = 0; i < vertexCommDataCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_edgeCommStackElement,					/// left vertex data comm element
						adjacent_firstVertexCommElement,				/// adjacent right vertex data comm element

						local_exchangeEdgeCommStackElement			/// output: local vertex data
					);

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
			}
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			CVertexData *local_edgeCommStackElement = &(io_cLocalStack->getElementAtIndex(local_edge_comm_stack_index));
			CVertexData *local_exchangeEdgeCommStackElement = &(io_cLocalExchangeStack->getElementAtIndex(local_edge_comm_stack_index));

			/*
			 * FIRST VERTEX handling
			 */
			cKernelWith_computeNetUpdates->vertexCommLastJoin(
					local_edgeCommStackElement,				/// left vertex data comm element
					adjacent_firstVertexCommElement,		/// adjacent right vertex data comm element

					local_exchangeEdgeCommStackElement		/// output: local vertex data
				);

			local_edgeCommStackElement++;
			local_exchangeEdgeCommStackElement++;

			/*
			 * MIDDLE VERTEX HANDLING
			 */
			size_t adj_i = vertexCommDataCount-1;

			for (int i = 0; i < vertexCommDataCount; i++)
			{
				// compute net update and store to tmpEdgeElement to avoid race conditions
				cKernelWith_computeNetUpdates->vertexCommLastJoin(
						local_edgeCommStackElement,					/// left vertex data comm element
						&adjacent_middleVertexCommElements[adj_i],	/// adjacent right vertex data comm element

						local_exchangeEdgeCommStackElement			/// output: local vertex data
					);

				local_edgeCommStackElement++;
				local_exchangeEdgeCommStackElement++;
				adj_i--;
			}

			/*
			 * LAST VERTEX handling
			 */
			cKernelWith_computeNetUpdates->vertexCommLastJoin(
					local_edgeCommStackElement,					/// left vertex data comm element
					adjacent_lastVertexCommElement,				/// adjacent right vertex data comm element

					local_exchangeEdgeCommStackElement			/// output: local vertex data
				);
		}
	}




	/**
	 * this method is executed by the simulation running on the partition after global synch to
	 * read the communication stack elements from the adjacent partitions
	 *
	 * loads all adjacent vertex data elements
	 */
public:
	void pullVertexCommData()
	{
		/*
		 * setup stacks to store/load data to/from
		 */
		if (cPartitionTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			localHypExchangeEdgeStack = exchangeLeftStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = exchangeRightStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = leftStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = rightStackAccessor(cPartitionTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			localHypExchangeEdgeStack = exchangeRightStackAccessor(cPartitionTreeNode);
			localCatExchangeEdgeStack = exchangeLeftStackAccessor(cPartitionTreeNode);

			localHypEdgeStack = rightStackAccessor(cPartitionTreeNode);
			localCatEdgeStack = leftStackAccessor(cPartitionTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}

		assert(localHypExchangeEdgeStack->isEmpty());
		assert(localCatExchangeEdgeStack->isEmpty());


		/**
		 * TODO:
		 *
		 * setup bool variables to know when to execute the 'final update' call
		 * for the corresponding vertex data
		 */
		bool leftVertexShared = true;
		bool rightVertexShared = true;
		bool topVertexShared = true;

		if (cPartitionTreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			leftVertexShared = false;
			rightVertexShared = false;
		}

		if (cPartitionTreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			leftVertexShared = false;
			topVertexShared = false;
		}

		if (cPartitionTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			rightVertexShared = false;
			topVertexShared = false;
		}


		/*
		 * RECONSTRUCT HYPOTENUSE STACKS
		 */
#if 1
		std::cout << "LOCAL HYP STACK: " << localHypEdgeStack << std::endl;
		for (size_t i = 0; i < localHypEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localHypEdgeStack->getElementAtIndex(i).validation << std::endl;
#endif

		if (cPartitionTreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			/*
			 * Whenever the hypotenuse is of type boundary, there may not be any RLE stored since
			 * the RLE for the vertices setting up the hypotenuse cannot be separated.
			 */
			assert(cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.size() == 0);

			/*
			 * if the hyp edge type is 'boundary', we start with a reconstruction of the boundary
			 * vertices on the hypotenuse
			 *
			 * since vertices also have to be stored on the boundaries (due to non-available
			 * knowledge about adjacent vertices), those have to be stored on the edge
			 * communication stack.
			 */
			if (cPartitionTreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
			{
				/*
				 * handle first vertex
				 */
				localHypExchangeEdgeStack->push(localHypEdgeStack->getElementAtIndex(0));

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				{
					// TODO: search for adjacent partner and update
					//assert(false);
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}


				/*
				 * middle vertices
				 */
				for (unsigned int i = 1; i < localHypEdgeStack->getNumberOfElementsOnStack()-1; i++)
				{
					localHypExchangeEdgeStack->push(localHypEdgeStack->getElementAtIndex(i));

					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}


				/*
				 * last vertices
				 */
				localHypExchangeEdgeStack->push(localHypEdgeStack->getTopElementRef());

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				{
					// TODO: search for adjacent partner and update
					//assert(false);
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}
			}
			else
			{
				/*
				 * handle first vertex
				 */
				localHypExchangeEdgeStack->push(localHypEdgeStack->getElementAtIndex(0));

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				{
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}


				/*
				 * middle vertices
				 */
				for (unsigned int i = 1; i < localHypEdgeStack->getNumberOfElementsOnStack()-1; i++)
				{
					localHypExchangeEdgeStack->push(localHypEdgeStack->getElementAtIndex(i));

					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}


				/*
				 * last vertices
				 */
				localHypExchangeEdgeStack->push(localHypEdgeStack->getTopElementRef());

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				{
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localHypExchangeEdgeStack->getTopElementPtr());
				}
			}

			assert(localHypExchangeEdgeStack->getNumberOfElementsOnStack() == localHypEdgeStack->getNumberOfElementsOnStack());
		}
		else
		{
			/*
			 * hyp border type != boundary
			 *
			 * in this case we update the data on the hypotenuse using the vertex comm data
			 * from the adjacent sub-partition.
			 */

			assert(cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.size() != 0);

			unsigned long local_edge_comm_stack_index = 0;

			for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
							cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.begin();
					iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.hyp_adjacent_partitions.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

				p_pullVertexCommDataFromPartitionToHypotenuseStack(
						localHypExchangeEdgeStack,
						localHypEdgeStack,
						hypClockwise,
						cEdgeComm_InformationAdjacentPartition,
						local_edge_comm_stack_index
					);

				local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;

				localHypExchangeEdgeStack->incStackElementCounter(cEdgeComm_InformationAdjacentPartition.commElements);
			}


			/*
			 * increment the stack element counter by 1
			 */
			localHypExchangeEdgeStack->incStackElementCounter(1);
		}

#if 1
		std::cout << "LOCAL EXCHANGE HYP STACK: " << localHypExchangeEdgeStack << std::endl;
		for (size_t i = 0; i < localHypExchangeEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localHypExchangeEdgeStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;
		std::cout << std::endl;
#endif

		/*
		 * RECONSTRUCT CATHETI STACKS
		 */
#if 1
		std::cout << "LOCAL CAT STACK: " << localCatEdgeStack << std::endl;
		for (size_t i = 0; i < localCatEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localCatEdgeStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;
#endif


		/*
		 * right now we assume that all data was reconstructed on the hypotenuse vertex comm stack.
		 */
		if (	cPartitionTreeNode->cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY	&&
				cPartitionTreeNode->cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY
		)
		{
			std::cout << "> BOTH NOT BORDERS" << std::endl;
			/*
			 * directly handle this special default case
			 */
			assert(false);
		}
		else if (	cPartitionTreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY	&&
					cPartitionTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY
		)
		{
			std::cout << "> BOTH BORDERS" << std::endl;

			/*
			 * TODO: special case: if also the hypotenuse is of type 'border'
			 */
			assert (cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.size() == 0);

			/*
			 * reconstruct boundary vertices
			 */
			for (unsigned int i = 0; i < localCatEdgeStack->getNumberOfElementsOnStack(); i++)
			{
				localCatExchangeEdgeStack->push(localCatEdgeStack->getElementAtIndex(i));

				cKernelWith_computeNetUpdates->vertexCommLastTouch(localCatExchangeEdgeStack->getTopElementPtr());
			}
		}
		else if (cPartitionTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
		{
			/*
			 * in case that one of the catheti is of type border, the RLE edge comm data
			 * stored for the opposite edge does not include communicating the
			 * top vertex since it's stored on the catheti stack, not on the
			 * hypotenuse stack as it would be the case when both catheti are of type
			 * 'new'.
			 *
			 * the following two pointers are set whenever such a special
			 * circumstance is given to know whenever the first or last
			 * vertex comm data is stored at at the hypotenuse and to update
			 * the corresponding vertex data.
			 */
			CVertexData *ptr_firstVertexCommDataOnHypotenuse = nullptr;
			CVertexData *ptr_firstVertexCommDataOnHypotenuseExchangeStackPtr = nullptr;
			CVertexData *ptr_lastVertexCommDataOnHypotenuse = nullptr;
			CVertexData *ptr_lastVertexCommDataOnHypotenuseExchangeStackPtr = nullptr;

			/*
			 * we have to distinguish between even and odd traversals
			 */
			if (cPartitionTreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
			{
				/*
				 *   |\
				 *   |  _\
				 *  b| |\  \?
				 *   |   \   \
				 *   |_________\ First
				 * Last   n
				 */

				/*
				 * since the data on the hypotenuse was already handled, it is stored on the exchange edge stack
				 */
				ptr_firstVertexCommDataOnHypotenuse = &localHypEdgeStack->getElementAtIndex(0);
				ptr_firstVertexCommDataOnHypotenuseExchangeStackPtr = &localHypExchangeEdgeStack->getElementAtIndex(0);
			}
			else
			{
				/*           First
				 *            /\
				 *        n /    \ b
				 *        /  <<<<  \
				 * Last /____________\
				 *            ?
				 */
				ptr_lastVertexCommDataOnHypotenuse = localHypEdgeStack->getTopElementPtr();
				ptr_lastVertexCommDataOnHypotenuseExchangeStackPtr = localHypExchangeEdgeStack->getTopElementPtr();
			}

			if (ptr_firstVertexCommDataOnHypotenuse && ptr_lastVertexCommDataOnHypotenuse)
			{
				std::cerr << "invalid state?" << std::endl;
				assert(false);

				std::cout << "firstVertexOfCathetiOnHypotenuse && lastVertexOfCathetiOnHypotenuse" << std::endl;

				/*
				 * default case for inner sub-partition triangles: all edges are attached to an adjacent communication partner
				 */

				unsigned long local_edge_comm_stack_index = 0;

				for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
								cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
						iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
						iter++
				)
				{
					CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

					p_pullVertexCommDataFromPartitionToCathetiStack(
							localCatExchangeEdgeStack,
							localCatEdgeStack,
							catClockwise,
							cEdgeComm_InformationAdjacentPartition,
							local_edge_comm_stack_index,
							ptr_firstVertexCommDataOnHypotenuse,
							ptr_firstVertexCommDataOnHypotenuseExchangeStackPtr,
							ptr_lastVertexCommDataOnHypotenuse,
							ptr_lastVertexCommDataOnHypotenuseExchangeStackPtr
						);

					local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
					localCatExchangeEdgeStack->incStackElementCounter(cEdgeComm_InformationAdjacentPartition.commElements);
				}

				assert(localCatEdgeStack->getNumberOfElementsOnStack() == localCatExchangeEdgeStack->getNumberOfElementsOnStack());
				// RLE ZERO encoded vertex comm is stored on hypotenuse and is also handled by the hypotenuse processing
			}
			else if (!ptr_firstVertexCommDataOnHypotenuse && !ptr_lastVertexCommDataOnHypotenuse)
			{
				std::cerr << "invalid state?" << std::endl;
				assert(false);

				std::cout << "!ptr_firstVertexCommDataOnHypotenuse && !ptr_lastVertexCommDataOnHypotenuse" << std::endl;
				/*
				 * no edge is attached to an adjacent communication partner
				 * => copy all vertex com data from local to local_exchange stack
				 */

				for (unsigned int i = 0; i < localCatEdgeStack->getNumberOfElementsOnStack(); i++)
				{
					localCatExchangeEdgeStack->push(localCatEdgeStack->getElementAtIndex(i));

					// LAST TOUCH UPDATE
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localCatExchangeEdgeStack->getTopElementPtr());
				}

				assert(localCatEdgeStack->getNumberOfElementsOnStack() == localCatExchangeEdgeStack->getNumberOfElementsOnStack());
//				localCatEdgeStack->setStackElementCounter(0);
			}
			else if (!ptr_firstVertexCommDataOnHypotenuse)
			{
				std::cout << "!ptr_firstVertexCommDataOnHypotenuse" << std::endl;
				/*
				 * first vertex along cathetii is stored on cat edge comm stack!
				 */

				/*
				 * if only the first vertex is not on the hypotenuse, some data stored on the local vertex comm
				 * stack has to be loaded and updated to local exchange vertex comm stack
				 */

				unsigned int edgeCommElements =
						cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.getSumOfEdgeCommElementsOnCatheti();

				unsigned int verticesForLocalReconstruction = localCatEdgeStack->getNumberOfElementsOnStack() - edgeCommElements;

				std::cout << "verticesForLocalReconstruction: " << verticesForLocalReconstruction << std::endl;

				/*
				 * now we reconstruct the first `edgeCommElements` from the local stack
				 */
				for (unsigned int i = 0; i < verticesForLocalReconstruction; i++)
				{
					localCatExchangeEdgeStack->push(localCatEdgeStack->getElementAtIndex(i));
					cKernelWith_computeNetUpdates->vertexCommLastTouch(localCatExchangeEdgeStack->getTopElementPtr());
				}

				/*
				 * append default vertexComm data
				 */
				unsigned long local_edge_comm_stack_index = verticesForLocalReconstruction;

				for (	typename std::vector<CEdgeComm_InformationAdjacentPartition_>::iterator iter =
								cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.begin();
						iter != cPartitionTreeNode->cEdgeComm_InformationAdjacentPartitions.cat_adjacent_partitions.end();
						iter++
				)
				{
					CEdgeComm_InformationAdjacentPartition_ &cEdgeComm_InformationAdjacentPartition = *iter;

					p_pullVertexCommDataFromPartitionToCathetiStack(
							localCatExchangeEdgeStack,
							localCatEdgeStack,
							catClockwise,
							cEdgeComm_InformationAdjacentPartition,
							local_edge_comm_stack_index,
							ptr_firstVertexCommDataOnHypotenuse,
							ptr_firstVertexCommDataOnHypotenuseExchangeStackPtr,
							ptr_lastVertexCommDataOnHypotenuse,
							ptr_lastVertexCommDataOnHypotenuseExchangeStackPtr
						);

					local_edge_comm_stack_index += cEdgeComm_InformationAdjacentPartition.commElements;
					localCatExchangeEdgeStack->incStackElementCounter(cEdgeComm_InformationAdjacentPartition.commElements);
				}
			}
			else if (!ptr_lastVertexCommDataOnHypotenuse)
			{
				// TODO
				assert(false);
			}
		}
		else
		{
			// TODO
			assert(false);
		}

#if 1
		std::cout << "LOCAL EXCHANGE CAT STACK: " << localCatExchangeEdgeStack << std::endl;
		for (size_t i = 0; i < localCatExchangeEdgeStack->getNumberOfElementsOnStack(); i++)
			std::cout << localCatExchangeEdgeStack->getElementAtIndex(i).validation << std::endl;
		std::cout << std::endl;

		assert(localCatEdgeStack->getNumberOfElementsOnStack() == localCatExchangeEdgeStack->getNumberOfElementsOnStack());
#endif

	}
};

#endif
