/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTriangle_Factory.hpp
 *
 *  Created on: Mar 31, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTRIANGLE_FACTORY_HPP_
#define CTRIANGLE_FACTORY_HPP_

#include "lib/MDebugClass.hpp"
#include "libmath/CMath.hpp"
#include "config.h"
#include <string>
#include <ostream>
#include <cassert>


#include "CTriangle_Enums.hpp"
#include "CTriangle_StringEnums.hpp"

/**
 * \brief this container is the most generic way to describe a triangle for further
 * sierpi traversal with partitioning and parallelization
 *
 * This container stores:
 *  - it's vertex coordinates
 *  - the sierpi traversal
 *  - the edge types
 *  - the normal at the hypotenuse
 *  - ...
 *
 * Different traversators which need to read the triangle properties for the
 * root triangle can read the information from this generic triangle information class.
 */
class CTriangle_Factory	: private CTriangle_Enums
{
public:
	// type of scalar stored in triangle factory

	typedef CONFIG_SIERPI_TRIANGLE_FACTORY_SCALAR_TYPE TTriangleFactoryScalarType;

	// convenience abbreviation
	typedef TTriangleFactoryScalarType T;

	/**
	 * vertex coordinates for triangles in normalized view (hypotenuse is aligned at the x-axis)
	 *
	 * 0: left-bottom vertex (left end of hyp)
	 * 1: right-bottom vertex (right end of hyp)
	 * 2: top vertex
	 */
	TTriangleFactoryScalarType vertices[3][2];

	/**
	 * triangle type: K, H or V
	 */
	ETriangleTypes triangleType;

	/**
	 * traversal direction: forward/backward
	 */
	EDirection traversalDirection;

	struct
	{
		EEdgeType hyp;
		EEdgeType left;
		EEdgeType right;
	} edgeTypes;

	/**
	 * normal at hypotenuse
	 */
	ENormals hypNormal;

	inline int getHypNormalIdx()	const
	{
		return hypNormal;
	}
	inline int getRightNormalIdx()	const
	{
		return (hypNormal+5)&7;
	}
	inline int getLeftNormalIdx()	const
	{
		return (hypNormal+3)&7;
	}


	inline void setVertices(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y
		)
	{
		vertices[0][0] = i_vertex_left_x;
		vertices[0][1] = i_vertex_left_y;
		vertices[1][0] = i_vertex_right_x;
		vertices[1][1] = i_vertex_right_y;
		vertices[2][0] = i_vertex_top_x;
		vertices[2][1] = i_vertex_top_y;
	}

	/**
	 * even or odd
	 */
	EEvenOdd evenOdd;

	/**
	 * type of partition tree node (first child, second child, root node)
	 */
	EPartitionTreeNode partitionTreeNodeType;

	inline bool isFirstChild()	const
	{
		return (partitionTreeNodeType == NODE_FIRST_CHILD);
	}
	inline bool isSecondChild()	const
	{
		return (partitionTreeNodeType == NODE_SECOND_CHILD);
	}

	/**
	 * depth of recursion for this triangle factory relative to the domain triangles
	 *
	 * this value is increased during every split
	 */
	int recursionDepthFirstRecMethod;

	/**
	 * maximum allowed depth of recursion
	 */
	int maxDepth;

	/**
	 * minimum allowed depth of recursion
	 */
	int minDepth;

	/**
	 * return true, if the stack for data along the hypotenuse is stored in clockwise order
	 */
	inline bool isHypotenuseDataOnLeftStack()	const
	{
		return	(traversalDirection == CTriangle_Enums::DIRECTION_BACKWARD) ^
				(evenOdd == CTriangle_Enums::ODD);
	}
	inline bool isHypotenuseDataOnAClockwiseStack()	const
	{
		return isHypotenuseDataOnLeftStack();
	}

	inline bool isCathetusDataOnLeftStack()	const
	{
		return !isHypotenuseDataOnLeftStack();
	}
	inline bool isCathetusDataOnAClockwiseStack()	const
	{
		return isCathetusDataOnLeftStack();
	}


	/**
	 * CONSTRUCTOR
	 */
	CTriangle_Factory()
	{
		DEBUG_METHOD()

		triangleType = TRIANGLE_TYPE_INVALID;

		edgeTypes.hyp = EDGE_TYPE_INVALID;
		edgeTypes.left = EDGE_TYPE_INVALID;
		edgeTypes.right = EDGE_TYPE_INVALID;

		hypNormal = NORMAL_INVALID;

		evenOdd = EVEN_ODD_INVALID;

		traversalDirection = DIRECTION_INVALID;

		partitionTreeNodeType = NODE_INVALID;

		recursionDepthFirstRecMethod = 0;
		minDepth = -1;
		maxDepth = -1;
	}



	/**
	 * return the normal enum value for a vector at the hypotenuse
	 */
	static
	ENormals getTriangleHypNormalIdx(
			TTriangleFactoryScalarType hyp_nx,	///< x-component of normal vector at hyp
			TTriangleFactoryScalarType hyp_ny,	///< y-component of normal vector at hyp
			TTriangleFactoryScalarType epsilon = TTriangleFactoryScalarType(0.001)
	)
	{
		static int normal_table[8][3] = {
				{0, 1, NORMAL_N},
				{1, 1, NORMAL_NE},
				{1, 0, NORMAL_E},
				{1, -1, NORMAL_SE},
				{0, -1, NORMAL_S},
				{-1, -1, NORMAL_SW},
				{-1, 0, NORMAL_W},
				{-1, 1, NORMAL_NW}
		};

		for (int i = 0; i < 8; i++)
		{
			if (	CMath::abs(
						hyp_nx*(float)normal_table[i][1] -
						hyp_ny*(float)normal_table[i][0]
					) < epsilon
			)
			{
				if (	hyp_nx*(float)normal_table[i][0] +
						hyp_ny*(float)normal_table[i][1]
						> 0
				)
					return (ENormals)normal_table[i][2];
			}
		}

		return NORMAL_INVALID;
	}

	void setupParentFromChildFatories(
			CTriangle_Factory &i_firstChild,	///< first child from the traversal perspective
			CTriangle_Factory &i_secondChild	///< second child from the traversal perspective
	)
	{
		// so far this method is only available for forward traversals
		assert(i_firstChild.traversalDirection == DIRECTION_FORWARD);
		assert(i_firstChild.traversalDirection == i_secondChild.traversalDirection);

		// direction
		assert(i_firstChild.traversalDirection == i_secondChild.traversalDirection);
		traversalDirection = i_firstChild.traversalDirection;

		assert(i_firstChild.recursionDepthFirstRecMethod == i_secondChild.recursionDepthFirstRecMethod);
		recursionDepthFirstRecMethod = i_firstChild.recursionDepthFirstRecMethod-1;

		/**
		 * copy min/max depth information to children
		 */
		assert(i_firstChild.minDepth == i_secondChild.minDepth);
		assert(i_firstChild.minDepth == i_secondChild.minDepth);
		minDepth = i_firstChild.minDepth;
		maxDepth = i_firstChild.maxDepth;

		partitionTreeNodeType = NODE_JOINED;

		assert(i_firstChild.evenOdd == i_secondChild.evenOdd);

		// even odd + types
		if (i_firstChild.evenOdd == EVEN)
		{
			// set to odd type
			evenOdd = ODD;

			// type
			triangleType = TRIANGLE_TYPE_V;

			// vertices
			vertices[0][0] = i_secondChild.vertices[1][0];
			vertices[0][1] = i_secondChild.vertices[1][1];

			vertices[1][0] = i_firstChild.vertices[0][0];
			vertices[1][1] = i_firstChild.vertices[0][1];

			vertices[2][0] = i_firstChild.vertices[1][0];
			vertices[2][1] = i_firstChild.vertices[1][1];

			assert(i_firstChild.vertices[2][0] == i_secondChild.vertices[2][0]);
			assert(i_firstChild.vertices[2][1] == i_secondChild.vertices[2][1]);

			assert(i_firstChild.vertices[1][0] == i_secondChild.vertices[0][0]);
			assert(i_firstChild.vertices[1][1] == i_secondChild.vertices[0][1]);

			// normal
			hypNormal = (ENormals)(((int)i_firstChild.hypNormal+3) & 7);

			// edge types
#if DEBUG
			if (i_firstChild.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				assert(i_secondChild.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
			if (i_secondChild.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				assert(i_firstChild.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
#endif
//			assert(i_firstChild.edgeTypes.right == i_firstChild.edgeTypes.left);
			edgeTypes.hyp = i_firstChild.edgeTypes.left;
			edgeTypes.right = i_firstChild.edgeTypes.hyp;
			edgeTypes.left = i_secondChild.edgeTypes.hyp;
		}
		else
		{
			// set to even type
			evenOdd = EVEN;

			// type
			triangleType = TRIANGLE_TYPE_V;

			// vertices
			vertices[0][0] = i_firstChild.vertices[1][0];
			vertices[0][1] = i_firstChild.vertices[1][1];

			vertices[1][0] = i_secondChild.vertices[0][0];
			vertices[1][1] = i_secondChild.vertices[0][1];

			vertices[2][0] = i_firstChild.vertices[0][0];
			vertices[2][1] = i_firstChild.vertices[0][1];

			assert(i_firstChild.vertices[2][0] == i_secondChild.vertices[2][0]);
			assert(i_firstChild.vertices[2][1] == i_secondChild.vertices[2][1]);

			assert(i_firstChild.vertices[0][0] == i_secondChild.vertices[1][0]);
			assert(i_firstChild.vertices[0][1] == i_secondChild.vertices[1][1]);

			// normal
			hypNormal = (ENormals)(((int)i_firstChild.hypNormal+5) & 7);

			// edge types
#if DEBUG
			if (i_firstChild.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				assert(i_secondChild.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
			if (i_secondChild.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_BOUNDARY)
				assert(i_firstChild.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_BOUNDARY);
#endif
//			assert(i_firstChild.edgeTypes.right == i_secondChild.edgeTypes.left);

			edgeTypes.hyp = i_firstChild.edgeTypes.right;
			edgeTypes.right = i_secondChild.edgeTypes.hyp;
			edgeTypes.left = i_firstChild.edgeTypes.hyp;
		}
	}

	/**
	 * (split/join relevant) setup the data for the new factories of left and right child
	 */
	void setupChildFactories(
			CTriangle_Factory &o_firstChild,	///< first child from the traversal perspective
			CTriangle_Factory &o_secondChild	///< second child from the traversal perspective
	)
	{
		// so far this method is only available for forward traversals
		assert(traversalDirection == DIRECTION_FORWARD);

		// first/second
		o_firstChild.partitionTreeNodeType = NODE_FIRST_CHILD;
		o_secondChild.partitionTreeNodeType = NODE_SECOND_CHILD;

		// direction
		o_firstChild.traversalDirection = traversalDirection;
		o_secondChild.traversalDirection = traversalDirection;

		// recursion depth
		o_firstChild.recursionDepthFirstRecMethod = recursionDepthFirstRecMethod+1;
		o_secondChild.recursionDepthFirstRecMethod = recursionDepthFirstRecMethod+1;

		/**
		 * copy min/max depth information to children
		 */
		o_firstChild.minDepth = minDepth;
		o_firstChild.maxDepth = maxDepth;

		o_secondChild.minDepth = minDepth;
		o_secondChild.maxDepth = maxDepth;

		// types
		ETriangleTypes t1 = TRIANGLE_TYPE_H, t2 = TRIANGLE_TYPE_H;
		switch(triangleType)
		{
		case TRIANGLE_TYPE_K:
			t1 = TRIANGLE_TYPE_H;
			t2 = TRIANGLE_TYPE_V;
			break;

		case TRIANGLE_TYPE_H:
			t1 = TRIANGLE_TYPE_V;
			t2 = TRIANGLE_TYPE_K;
			break;

		case TRIANGLE_TYPE_V:
			t1 = TRIANGLE_TYPE_H;
			t2 = TRIANGLE_TYPE_K;
			break;

		default:
			assert(false);
			break;
		}

		// even odd + types
		if (evenOdd == EVEN)
		{
			// parent EVEN

			// even-odd
			o_firstChild.evenOdd = ODD;
			o_secondChild.evenOdd = ODD;

			// type
			o_firstChild.triangleType = t1;
			o_secondChild.triangleType = t2;


			TTriangleFactoryScalarType mx = (vertices[0][0]+vertices[1][0])*(TTriangleFactoryScalarType)0.5;
			TTriangleFactoryScalarType my = (vertices[0][1]+vertices[1][1])*(TTriangleFactoryScalarType)0.5;

			o_firstChild.vertices[0][0] = vertices[2][0];
			o_firstChild.vertices[0][1] = vertices[2][1];

			o_firstChild.vertices[1][0] = vertices[0][0];
			o_firstChild.vertices[1][1] = vertices[0][1];

			o_firstChild.vertices[2][0] = mx;
			o_firstChild.vertices[2][1] = my;

			o_secondChild.vertices[0][0] = vertices[1][0];
			o_secondChild.vertices[0][1] = vertices[1][1];

			o_secondChild.vertices[1][0] = vertices[2][0];
			o_secondChild.vertices[1][1] = vertices[2][1];

			o_secondChild.vertices[2][0] = mx;
			o_secondChild.vertices[2][1] = my;



			if (hypNormal != NORMAL_INVALID)
			{
				o_firstChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
				o_secondChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
			}

			if (edgeTypes.hyp != EDGE_TYPE_INVALID)
			{
				o_firstChild.edgeTypes.hyp = edgeTypes.left;
				o_firstChild.edgeTypes.left = EDGE_TYPE_NEW;
				o_firstChild.edgeTypes.right = edgeTypes.hyp;

				o_secondChild.edgeTypes.hyp = edgeTypes.right;
				o_secondChild.edgeTypes.left = edgeTypes.hyp;
				o_secondChild.edgeTypes.right = EDGE_TYPE_OLD;
			}
		}
		else
		{
			// parent ODD

			// even-odd
			o_firstChild.evenOdd = EVEN;
			o_secondChild.evenOdd = EVEN;

			// type
			o_firstChild.triangleType = t2;
			o_secondChild.triangleType = t1;

			TTriangleFactoryScalarType mx = (vertices[0][0]+vertices[1][0])*(TTriangleFactoryScalarType)0.5;
			TTriangleFactoryScalarType my = (vertices[0][1]+vertices[1][1])*(TTriangleFactoryScalarType)0.5;

			o_firstChild.vertices[0][0] = vertices[1][0];
			o_firstChild.vertices[0][1] = vertices[1][1];

			o_firstChild.vertices[1][0] = vertices[2][0];
			o_firstChild.vertices[1][1] = vertices[2][1];

			o_firstChild.vertices[2][0] = mx;
			o_firstChild.vertices[2][1] = my;


			o_secondChild.vertices[0][0] = vertices[2][0];
			o_secondChild.vertices[0][1] = vertices[2][1];

			o_secondChild.vertices[1][0] = vertices[0][0];
			o_secondChild.vertices[1][1] = vertices[0][1];

			o_secondChild.vertices[2][0] = mx;
			o_secondChild.vertices[2][1] = my;


			if (hypNormal != NORMAL_INVALID)
			{
				o_firstChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
				o_secondChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
			}

			if (edgeTypes.hyp != EDGE_TYPE_INVALID)
			{
				o_firstChild.edgeTypes.hyp = edgeTypes.right;
				o_firstChild.edgeTypes.left = edgeTypes.hyp;
				o_firstChild.edgeTypes.right = EDGE_TYPE_OLD;

				o_secondChild.edgeTypes.hyp = edgeTypes.left;
				o_secondChild.edgeTypes.left = EDGE_TYPE_NEW;
				o_secondChild.edgeTypes.right = edgeTypes.hyp;
			}
		}
	}


	/**
	 * setup the data for the new factories of left and right child without
	 * computing vertices.
	 */
	void setupChildFactoriesWithoutVertices(
			CTriangle_Factory &firstChild,	///< first child from the traversal perspective
			CTriangle_Factory &secondChild	///< second child from the traversal perspective
	)
	{
		// first/second
		firstChild.partitionTreeNodeType = NODE_FIRST_CHILD;
		secondChild.partitionTreeNodeType = NODE_SECOND_CHILD;

		// direction
		firstChild.traversalDirection = traversalDirection;
		secondChild.traversalDirection = traversalDirection;

		// recursion depth
		firstChild.recursionDepthFirstRecMethod = recursionDepthFirstRecMethod+1;
		secondChild.recursionDepthFirstRecMethod = recursionDepthFirstRecMethod+1;

		/**
		 * copy min/max depth information to children
		 */
		firstChild.minDepth = minDepth;
		firstChild.maxDepth = maxDepth;

		secondChild.minDepth = minDepth;
		secondChild.maxDepth = maxDepth;


		// so far this method is only available for forward traversals
		if (traversalDirection == DIRECTION_FORWARD)
		{
			// types
			ETriangleTypes t1, t2;
			switch(triangleType)
			{
			case TRIANGLE_TYPE_K:
				t1 = TRIANGLE_TYPE_H;
				t2 = TRIANGLE_TYPE_V;
				break;

			case TRIANGLE_TYPE_H:
				t1 = TRIANGLE_TYPE_V;
				t2 = TRIANGLE_TYPE_K;
				break;

			case TRIANGLE_TYPE_V:
				t1 = TRIANGLE_TYPE_H;
				t2 = TRIANGLE_TYPE_K;
				break;

			default:
				assert(false);
				break;
			}

			// even odd + types
			if (evenOdd == EVEN)
			{
				// parent EVEN

				// even-odd
				firstChild.evenOdd = ODD;
				secondChild.evenOdd = ODD;

				// type
				firstChild.triangleType = t1;
				secondChild.triangleType = t2;

				if (hypNormal != NORMAL_INVALID)
				{
					firstChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
					secondChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
				}

				if (edgeTypes.hyp != EDGE_TYPE_INVALID)
				{
					firstChild.edgeTypes.hyp = edgeTypes.left;
					firstChild.edgeTypes.left = EDGE_TYPE_NEW;
					firstChild.edgeTypes.right = edgeTypes.hyp;

					secondChild.edgeTypes.hyp = edgeTypes.right;
					secondChild.edgeTypes.left = edgeTypes.hyp;
					secondChild.edgeTypes.right = EDGE_TYPE_OLD;
				}
			}
			else
			{
				// parent ODD

				// even-odd
				firstChild.evenOdd = EVEN;
				secondChild.evenOdd = EVEN;

				// type
				firstChild.triangleType = t2;
				secondChild.triangleType = t1;

				if (hypNormal != NORMAL_INVALID)
				{
					firstChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
					secondChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
				}

				if (edgeTypes.hyp != EDGE_TYPE_INVALID)
				{
					firstChild.edgeTypes.hyp = edgeTypes.right;
					firstChild.edgeTypes.left = edgeTypes.hyp;
					firstChild.edgeTypes.right = EDGE_TYPE_NEW;

					secondChild.edgeTypes.hyp = edgeTypes.left;
					secondChild.edgeTypes.left = EDGE_TYPE_OLD;
					secondChild.edgeTypes.right = edgeTypes.hyp;
				}
			}
		}
		else
		{
			// types
			ETriangleTypes t1, t2;
			switch(triangleType)
			{
			case TRIANGLE_TYPE_K:
				t1 = TRIANGLE_TYPE_V;
				t2 = TRIANGLE_TYPE_H;
				break;

			case TRIANGLE_TYPE_H:
				t1 = TRIANGLE_TYPE_K;
				t2 = TRIANGLE_TYPE_V;
				break;

			case TRIANGLE_TYPE_V:
				t1 = TRIANGLE_TYPE_K;
				t2 = TRIANGLE_TYPE_H;
				break;

			default:
				assert(false);
				break;
			}

			// even odd + types
			if (evenOdd == EVEN)
			{
				// parent EVEN

				// even-odd
				firstChild.evenOdd = ODD;
				secondChild.evenOdd = ODD;

				// type
				firstChild.triangleType = t1;
				secondChild.triangleType = t2;

				if (hypNormal != NORMAL_INVALID)
				{
					firstChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
					secondChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
				}

				if (edgeTypes.hyp != EDGE_TYPE_INVALID)
				{
					firstChild.edgeTypes.hyp = edgeTypes.right;
					firstChild.edgeTypes.left = edgeTypes.hyp;
					firstChild.edgeTypes.right = EDGE_TYPE_NEW;

					secondChild.edgeTypes.hyp = edgeTypes.left;
					secondChild.edgeTypes.left = EDGE_TYPE_OLD;
					secondChild.edgeTypes.right = edgeTypes.hyp;
				}
			}
			else
			{
				// parent ODD

				// even-odd
				firstChild.evenOdd = EVEN;
				secondChild.evenOdd = EVEN;

				// type
				firstChild.triangleType = t2;
				secondChild.triangleType = t1;

				if (hypNormal != NORMAL_INVALID)
				{
					firstChild.hypNormal = (ENormals)(((int)hypNormal+3) & 7);
					secondChild.hypNormal = (ENormals)(((int)hypNormal+5) & 7);
				}

				if (edgeTypes.hyp != EDGE_TYPE_INVALID)
				{
					firstChild.edgeTypes.hyp = edgeTypes.left;
					firstChild.edgeTypes.left = EDGE_TYPE_NEW;
					firstChild.edgeTypes.right = edgeTypes.hyp;

					secondChild.edgeTypes.hyp = edgeTypes.right;
					secondChild.edgeTypes.left = edgeTypes.hyp;
					secondChild.edgeTypes.right = EDGE_TYPE_OLD;
				}
			}
		}
	}

	inline
	::std::ostream&
	streamVertices(::std::ostream &co)	const
	{
		co << " + Vertex 0: " << vertices[0][0] << " " << vertices[0][1] << std::endl;
		co << " + Vertex 1: " << vertices[1][0] << " " << vertices[1][1] << std::endl;
		co << " + Vertex 2: " << vertices[2][0] << " " << vertices[2][1] << std::endl;
		return co;
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, const CTriangle_Factory &m)
	{
		co << "TriangleFactory:" << std::endl;
		co << m.streamVertices(co) << std::endl;
		co << " + Triangle Type: " << CTriangle_StringEnums::getTriangleTypeString(m.triangleType) << std::endl;
		co << " + Traversal Direction: " << CTriangle_StringEnums::getTraversalDirectionString(m.traversalDirection) << std::endl;
		co << " + Edge Type Hyp: " << CTriangle_StringEnums::getEdgeTypeString(m.edgeTypes.hyp) << std::endl;
		co << " + Edge Type Right: " << CTriangle_StringEnums::getEdgeTypeString(m.edgeTypes.right) << std::endl;
		co << " + Edge Type Left: " << CTriangle_StringEnums::getEdgeTypeString(m.edgeTypes.left) << std::endl;
		co << " + Hyp Normal: " << CTriangle_StringEnums::getNormalString(m.hypNormal) << std::endl;
		co << " + EvenOdd: " << CTriangle_StringEnums::getEvenOddString(m.evenOdd) << std::endl;
		co << " + partitionTreeNodeType: " << m.partitionTreeNodeType << std::endl;
		co << " + traversalDepth: " << m.recursionDepthFirstRecMethod << std::endl;
		co << " + minDepth: " << m.minDepth << std::endl;
		co << " + maxDepth: " << m.maxDepth << std::endl;
		co << std::endl;
		return co;
	}
};



#endif
