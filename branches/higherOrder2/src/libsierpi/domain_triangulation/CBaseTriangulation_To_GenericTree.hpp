/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CBaseTriangulation_To_GenericTree.hpp
 *
 *  Created on: Oct 12, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CBASETRIANGULATION_TO_GENERICTREE_HPP_
#define CBASETRIANGULATION_TO_GENERICTREE_HPP_

#include "CDomain_BaseTriangulation.hpp"


/**
 * \brief converts a base triangulation to a generic tree structure
 *
 * base triangulation is given via CDomain_BaseTriangulation.
 */
template <typename CSimulation_PartitionHandler>
class CBaseTriangulation_To_GenericTree
{
	typedef CDomain_BaseTriangle<CSimulation_PartitionHandler> CDomain_BaseTriangle_;
	typedef CDomain_BaseTriangulation<CSimulation_PartitionHandler> CDomain_BaseTriangulation_;

	typedef CPartition_TreeNode<CSimulation_PartitionHandler> CPartition_TreeNode_;
	typedef CPartition_EdgeComm_InformationAdjacentCluster<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartition_;
	typedef CGenericTreeNode<CPartition_TreeNode_> CGenericTreeNode_;


private:
	/**
	 * maximum allowed unique id depth for root triangles
	 */
	int max_unique_id_depth;


	/**
	 * unique id counter for initialization
	 */
	int unique_id_counter;


	/*
	 * edge comm elements along a single cathetus
	 */
	int cat_edge_comm_elements;

	/*
	 * edge comm elements along a hypotenuse
	 */
	int hyp_edge_comm_elements;

	/*
	 * initial depth and thus number of triangles to setup stacks
	 */
	int initial_refinement_depth;

	/*
	 * minimum relative refinement depth
	 */
	int min_relative_depth;

	/*
	 * maximum relative refinement depth
	 */
	int max_relative_depth;

	/**
	 * pointer to domain root triangulation
	 */
	CDomain_BaseTriangulation_ *cDomain_RootTriangulation;


	/**
	 * mpi rank
	 */
	int mpi_rank;

public:
	/**
	 * number of root triangles
	 */
	unsigned long long number_of_initial_root_partitions;

	/**
	 * constructor
	 */
	CBaseTriangulation_To_GenericTree()	:
		cat_edge_comm_elements(-1),
		hyp_edge_comm_elements(-1)
	{
	}



private:
	/**
	 * recursive method to insert the new generic treenode given by `iter` below the given parent_node
	 */
	CGenericTreeNode_ *_setup_GenericTreeAndPartitions(
			typename std::list<CDomain_BaseTriangle_ >::iterator &iter,	///< iterator to current CDomain_RootTriangle
			CGenericTreeNode_ *parent_node,			///< parent node to setup 'parent'-link
			int current_depth						///< current tree traversal depth
	)
	{
		CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;
		assert(current_depth <= max_unique_id_depth);

		// leaf reached => create partition
		if (current_depth == max_unique_id_depth)
		{
			/****************************************************************
			 * allocate new partition node
			 ****************************************************************/

			// first allocate the generic tree node
			CGenericTreeNode_ *cGeneric_TreeNode = new CGenericTreeNode_(parent_node, true);

			/*
			 * setup unique id and increment unique id counter
			 */
			cDomain_BaseTriangle.uniqueId.setup(unique_id_counter + (1 << max_unique_id_depth), max_unique_id_depth);

#if CONFIG_ENABLE_MPI
			if (mpi_rank == cDomain_BaseTriangle.mpi_rank)
			{
#endif

				// then allocate the partition tree node
				CPartition_TreeNode_ *cPartitionTreeNode = new CPartition_TreeNode_(
						cDomain_BaseTriangle.triangleFactory	// triangle factory
					);

				// setup pointers
				cGeneric_TreeNode->cPartition_TreeNode = cPartitionTreeNode;
				cPartitionTreeNode->cGeneric_TreeNode = cGeneric_TreeNode;

				/*
				 * update partition node's adaptive depth values!
				 */
				assert(cPartitionTreeNode->cTriangleFactory.recursionDepthFirstRecMethod == 0);
				cPartitionTreeNode->cTriangleFactory.minDepth = min_relative_depth;
				cPartitionTreeNode->cTriangleFactory.maxDepth = max_relative_depth;

				/*
				 * setup unique id and increment unique id counter
				 */
				cPartitionTreeNode->uniqueId = cDomain_BaseTriangle.uniqueId;

				// setup pointer to allocated partition tree node
				cDomain_BaseTriangle.cPartition_user_ptr = cPartitionTreeNode;

				// setup genericTreeNode's variable to the parent node
				cGeneric_TreeNode->parent_node = parent_node;

				/*
				 * convert edges of type OLD to type NEW due to parallelization
				 */
				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD)
					cPartitionTreeNode->cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD)
					cPartitionTreeNode->cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

				if (cPartitionTreeNode->cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD)
					cPartitionTreeNode->cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

#if CONFIG_ENABLE_MPI
			}
#endif
			unique_id_counter++;

			// increment the iterator
			iter++;

			// return pointer to this node
			return cGeneric_TreeNode;
		}

		current_depth++;

		// create a new tree node with attached leaves
		/****************************************************************
		 * allocate new generic tree node
		 ****************************************************************/
		CGenericTreeNode_ *new_node = new CGenericTreeNode_(parent_node, true);

		// left traversal
		new_node->first_child_node =
			_setup_GenericTreeAndPartitions(
				iter,		// reference to iterator
				new_node,	// this (parent) node
				current_depth
			);

		if (iter != cDomain_RootTriangulation->domainRootTriangles.end())
		{
			// right traversal
			new_node->second_child_node =
				_setup_GenericTreeAndPartitions(
					iter,		// reference to iterator
					new_node,	// this (parent) node
					current_depth
				);
		}

		return new_node;
	}


	/**
	 * setup a root sub-partition tree node for a given partition
	 *
	 * the setup includes the information about the RLE encoded communication between the partitions
	 */
private:
	void _setup_PartitionEdgeCommunicationInformation(
			CPartition_TreeNode_ *io_partitionTreeNode,
			CDomain_BaseTriangle_ &i_cDomain_BaseTriangle
	)
	{
		/**
		 * setup hypotenuse adjacency information stack
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge != nullptr)
		{
			io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.hyp_adjacent_partitions.push_back(CEdgeComm_InformationAdjacentPartition_());
			CEdgeComm_InformationAdjacentPartition_ &adjacentTriangleInformation = io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.hyp_adjacent_partitions.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cPartition_user_ptr)
			{
				adjacentTriangleInformation.partitionTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->cPartition_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.partitionTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.hyp_edge->uniqueId;
			adjacentTriangleInformation.comm_elements = hyp_edge_comm_elements;
		}

		/**
		 * setup catheti adjacency information stack
		 */
		if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge != nullptr)
		{
			io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.push_back(CEdgeComm_InformationAdjacentPartition_());
			CEdgeComm_InformationAdjacentPartition_ &adjacentTriangleInformation = io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.back();

			if (i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cPartition_user_ptr)
			{
				adjacentTriangleInformation.partitionTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->cPartition_user_ptr;
			}
			else
			{
				adjacentTriangleInformation.partitionTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation.mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation.uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.left_edge->uniqueId;
			adjacentTriangleInformation.comm_elements = cat_edge_comm_elements;
		}


		if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge)
		{
			CEdgeComm_InformationAdjacentPartition_ *adjacentTriangleInformation;

			/**
			 * for odd traversal type, the adjacent partition comm information has to be inserted in reversed order
			 */
			if (io_partitionTreeNode->cTriangleFactory.evenOdd == CTriangle_Enums::ODD)
			{
				io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.insert(
						io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.begin(),
						CEdgeComm_InformationAdjacentPartition_()
					);
				adjacentTriangleInformation = &(io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.front());
			}
			else
			{
				io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.push_back(
						CEdgeComm_InformationAdjacentPartition_()
					);
				adjacentTriangleInformation = &(io_partitionTreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions.back());
			}

			if (i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cPartition_user_ptr)
			{
				adjacentTriangleInformation->partitionTreeNode = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->cPartition_user_ptr;
			}
			else
			{
				adjacentTriangleInformation->partitionTreeNode = nullptr;
#if CONFIG_ENABLE_MPI
				adjacentTriangleInformation->mpi_rank = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->mpi_rank;
#endif
			}

			adjacentTriangleInformation->uniqueId = i_cDomain_BaseTriangle.adjacent_triangles.right_edge->uniqueId;
			adjacentTriangleInformation->comm_elements = cat_edge_comm_elements;
		}
	}



#if CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
	/**
	 * search for the adjacent matching vertices and store them
	 */
	bool _setupVertexCommInformation(
			std::list<CEdgeComm_InformationAdjacentPartition_> &io_vertexInformationAdjacentPartitions,	///< list to fill with information about adjacent partitions
			CDomain_BaseTriangle<CSimulation_PartitionHandler> *i_baseTriangle,
			const CDomain_BaseTriangle<CSimulation_PartitionHandler> *i_originBaseTriangle,			///< origin base triangle of this vertex search
			const CDomain_BaseTriangle<CSimulation_PartitionHandler> *i_prevBaseTriangle,			///< base triangle triggering the execution of this function to find the matching edge and to avoid running backwards

			bool i_adjacentEdgeInClockwiseOrder						///< select vertex by taking the 1st vertex in clock order from the adjacent triangles point of view
	)
	{
		if (i_baseTriangle == nullptr)
			return false;

		std::cout << "origin: " << i_originBaseTriangle->cPartition_user_ptr->uniqueId << std::endl;
		std::cout << "base: " << i_baseTriangle->cPartition_user_ptr->uniqueId << std::endl;

		assert(io_vertexInformationAdjacentPartitions.size() < 8);

		/*
		 * stop setting up the tree when we are back at the base triangle where we started the vertex comm search
		 */
		if (i_baseTriangle == i_originBaseTriangle)
		{
			/*
			 * remove topmost element!
			 */
			io_vertexInformationAdjacentPartitions.pop_back();
			return true;
		}

#if DEBUG
		for(auto iter = io_vertexInformationAdjacentPartitions.begin(); iter != io_vertexInformationAdjacentPartitions.end(); iter++)
		{
			assert((*iter).uniqueId != i_baseTriangle->cPartition_user_ptr->uniqueId);
		}
#endif

		if (i_prevBaseTriangle != i_originBaseTriangle)
		{
			/*
			 * add vertex comm information
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				io_vertexInformationAdjacentPartitions.push_back(
						CEdgeComm_InformationAdjacentPartition_(i_baseTriangle->cPartition_user_ptr, i_baseTriangle->cPartition_user_ptr->uniqueId, 0)
					);
			}
			else
			{
				io_vertexInformationAdjacentPartitions.push_front(
						CEdgeComm_InformationAdjacentPartition_(i_baseTriangle->cPartition_user_ptr, i_baseTriangle->cPartition_user_ptr->uniqueId, 0)
					);
			}
		}


		if (i_baseTriangle->adjacent_triangles.hyp_edge == i_prevBaseTriangle)
		{
			/*
			 * search on hypotenuse edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.right_edge == i_prevBaseTriangle)
		{
			/*
			 * search on right edge for matching source edge
			 */

			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow left edge
				 */
				if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.left_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		if (i_baseTriangle->adjacent_triangles.left_edge == i_prevBaseTriangle)
		{
			/*
			 * search on left edge for matching source edge
			 */
			if (i_adjacentEdgeInClockwiseOrder)
			{
				/*
				 * follow right edge
				 */
				if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.right_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							true
						);
			}
			else
			{
				/*
				 * follow hyp edge
				 */
				if (i_baseTriangle->adjacent_triangles.hyp_edge != nullptr)
					return _setupVertexCommInformation(
							io_vertexInformationAdjacentPartitions,
							i_baseTriangle->adjacent_triangles.hyp_edge,
							i_originBaseTriangle,
							i_baseTriangle,
							false
						);
			}

			return false;
		}

		std::cerr << "INVALID STATE REACHED WHEN SEARCHING FOR VERTICES" << std::endl;
		assert(false);

		return false;
	}


	/*
	 * setup a root sub-partition tree node for a given partition after edge comm information was set-up !!!
	 *
	 * this setup inserts 0-length encoded edge communication information to account for vertex data
	 */
private:
	void _setup_PartitionVertexCommunicationInformation(
			CDomain_BaseTriangle<CSimulation_PartitionHandler> *i_baseTriangle
	)
	{
		/*
		 * here we start setting up the 0-length encoded vertex data.
		 *
		 * for the vertex shared by the left and hyp edge or the right and hyp edge, either the hypotenuse
		 * or the left/right RLE encoded part has to be used for updating the vertex.
		 */

		/*
		 * storage containers for left, right and top vertices
		 */
		std::list<CEdgeComm_InformationAdjacentPartition_>
				leftVertexInformationAdjacentPartitions,
				rightVertexInformationAdjacentPartitions,
				topVertexInformationAdjacentPartitions;

//		std::cout << "search on left edge for top vertices" << std::endl;
		// search on left edge for top vertices
		bool allLeftVerticesFound = _setupVertexCommInformation(
				leftVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.left_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
			);
//		std::cout << "search on left edge for top vertices end" << std::endl;

//		std::cout << "search on right edge for top vertices" << std::endl;
		// search on right edge for top vertices
		bool allTopVerticesFound = _setupVertexCommInformation(
				topVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
			);
//		std::cout << "search on right edge for top vertices end" << std::endl;

//		std::cout << "search on hyp edge for right vertices" << std::endl;
		// search on hyp edge for right vertices
		bool allRightVerticesFound = _setupVertexCommInformation(
				rightVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				true	// clockwise
		);
//		std::cout << "search on hyp edge for right vertices end" << std::endl;

//		std::cout << "search on right edge for more right vertices" << std::endl;
		if (!allRightVerticesFound)
		{
			// search on right edge for more right vertices
			_setupVertexCommInformation(
				rightVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.right_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}

//		std::cout << "search on right edge for more right vertices end" << std::endl;

//		std::cout << "search on left edge for more top vertices" << std::endl;
		if (!allTopVerticesFound)
		{
			// search on left edge for more top vertices
			_setupVertexCommInformation(
				topVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.left_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}
//		std::cout << "search on left edge for more top vertices end" << std::endl;

//		std::cout << "search on hyp edge for more left vertices" << std::endl;
		if (!allLeftVerticesFound)
		{
			// search on hyp edge for more left vertices
			_setupVertexCommInformation(
				leftVertexInformationAdjacentPartitions,
				i_baseTriangle->adjacent_triangles.hyp_edge,
				i_baseTriangle,
				i_baseTriangle,
				false	// anti-clockwise
			);
		}
//		std::cout << "search on hyp edge for more l vertices end" << std::endl;

#if 0
		std::cout << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << " TRIANGLE: " << i_baseTriangle->cPartition_user_ptr->uniqueId << std::endl;
		std::cout << "+++++++++++++++++++++++++++++++++++++++++" << std::endl;
		if (leftVertexInformationAdjacentPartitions.size() > 0)
		{
			std::cout << " left vertex adjacent partitions:" << std::endl;

			for (auto iter = leftVertexInformationAdjacentPartitions.begin();
					iter != leftVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}

		if (rightVertexInformationAdjacentPartitions.size() > 0)
		{
			std::cout << " right vertex adjacent partitions:" << std::endl;

			for (auto iter = rightVertexInformationAdjacentPartitions.begin();
					iter != rightVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}

		if (topVertexInformationAdjacentPartitions.size() > 0)
		{
			std::cout << " top vertex adjacent partitions:" << std::endl;

			for (auto iter = topVertexInformationAdjacentPartitions.begin();
					iter != topVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				std::cout << "    " << (*iter).uniqueId << std::endl;
			}
		}
#endif

		/*
		 * insert data into RLE edge comm information
		 */
		CPartition_TreeNode_ *cPartition_TreeNode = i_baseTriangle->cPartition_user_ptr;


		/*
		 * Extend RLE edge comm information for hypotenuse.
		 *
		 * The vertices shared by both catheti and the hypotenuse have to be handled by either the RLE
		 * stored for both catheti or for the hypotenuse.
		 *
		 * We do this since the root sub-partition triangle type is always set to 'V' to store the vertices
		 * close to the entrance and exit curve on the 'hypotenuse' stack.
		 *
		 * Another reason is also to have a unique storage thus increasing searches for adjacent
		 * communication vertex data.
		 */
		std::vector<CEdgeComm_InformationAdjacentPartition_> &hypEdgeCommInfo = cPartition_TreeNode->cEdgeComm_InformationAdjacentClusters.hyp_adjacent_partitions;

		if (cPartition_TreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			for (auto iter = rightVertexInformationAdjacentPartitions.begin();
					iter != rightVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				hypEdgeCommInfo.insert(hypEdgeCommInfo.begin(), *iter);
			}

			for (auto iter = leftVertexInformationAdjacentPartitions.begin();
					iter != leftVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				hypEdgeCommInfo.push_back(*iter);
			}
		}
		else
		{
			for (auto iter = rightVertexInformationAdjacentPartitions.begin();
					iter != rightVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				hypEdgeCommInfo.push_back(*iter);
			}

			for (auto iter = leftVertexInformationAdjacentPartitions.begin();
					iter != leftVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				hypEdgeCommInfo.insert(hypEdgeCommInfo.begin(), *iter);
			}
		}


		/*
		 * extend RLE edge comm information for both catheti
		 */
		std::vector<CEdgeComm_InformationAdjacentPartition_> &catEdgeCommInfo = cPartition_TreeNode->cEdgeComm_InformationAdjacentClusters.cat_adjacent_partitions;

		auto catInsertIter = catEdgeCommInfo.begin();

		if (cPartition_TreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack())
		{
			if (i_baseTriangle->adjacent_triangles.left_edge != nullptr)
			{
				assert(catInsertIter != catEdgeCommInfo.end());
				catInsertIter++;
			}

			for (auto iter = topVertexInformationAdjacentPartitions.begin();
					iter != topVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				catEdgeCommInfo.insert(catInsertIter, *iter);
			}
		}
		else
		{
			if (i_baseTriangle->adjacent_triangles.right_edge != nullptr)
			{
				assert(catInsertIter != catEdgeCommInfo.end());
				catInsertIter++;
			}

			for (auto iter = topVertexInformationAdjacentPartitions.begin();
					iter != topVertexInformationAdjacentPartitions.end();
					iter++
			)
			{
				catEdgeCommInfo.insert(catInsertIter, *iter);
			}
		}
	}
#endif



public:
	bool _cleanupUnusedNodes(CGenericTreeNode_ *node)
	{
		if (node->first_child_node)
		{
			if (_cleanupUnusedNodes(node->first_child_node))
			{
				delete node->first_child_node;
				node->first_child_node = nullptr;
			}
		}

		if (node->second_child_node)
		{
			if (_cleanupUnusedNodes(node->second_child_node))
			{
				delete node->second_child_node;
				node->second_child_node = nullptr;
			}
		}

		return (	node->first_child_node == nullptr &&
					node->second_child_node == nullptr &&
					node->cPartition_TreeNode == nullptr
		);
	}


public:
	/**
	 * setup a generic tree given by a base triangulation.
	 *
	 * this also initialized the RLE edge communication information.
	 */
	unsigned long long setup_GenericTree_From_BaseTriangulation(
			CDomain_BaseTriangulation_ &io_cDomain_RootTriangulation,	///< root triangulation

			int i_initial_refinement_depth,				///< initial depth of refinement
			int i_minimum_relative_refinement_depth,	///< minimum relative refinement depth
			int i_maximum_relative_refinement_depth,	///< maximum relative refinement depth

			int i_mpi_rank,						///< mpi rank for base triangles which should be setup

			CGenericTreeNode_ **o_rootNode		///< reference to root node
	)
	{
		// store mpi rank
		mpi_rank = i_mpi_rank;

		// there must not exist any preallocated tree structure
		assert(*o_rootNode == nullptr);

		cDomain_RootTriangulation = &io_cDomain_RootTriangulation;

		number_of_initial_root_partitions = cDomain_RootTriangulation->domainRootTriangles.size();

		initial_refinement_depth = i_initial_refinement_depth;
		min_relative_depth = i_minimum_relative_refinement_depth;
		max_relative_depth = i_maximum_relative_refinement_depth;


		/**
		 * check for valid initial depth
		 */
		assert(i_initial_refinement_depth >= 0);

		if ((i_initial_refinement_depth & 1) == 1)
		{
			if (!cDomain_RootTriangulation->oddInitialDepthFixApplied)
			{
				std::cerr << "An odd initial depth value is not allowed due to restrictions of hanging nodes along catheti and hypotenuse" << std::endl;
				assert(false);
			}
		}

		/**
		 * compute edge communication elements for initial triangulation
		 */
		cat_edge_comm_elements = 1 << (i_initial_refinement_depth/2);
		hyp_edge_comm_elements = 1 << ((i_initial_refinement_depth+1)/2);


		/**
		 * determine max_unique_id_depth
		 */
		unsigned long long d = number_of_initial_root_partitions-1;
		max_unique_id_depth = 0;
		for (; d > 0; d >>= 1)
			max_unique_id_depth++;


		/*
		 * nothing to do for empty domain size
		 */
		if (number_of_initial_root_partitions == 0)
		{
			*o_rootNode = new CGenericTreeNode_(nullptr, true);
			return 0;
		}


		/**
		 * assemble tree by allocating GenericTreeNodes and PartitionTreeNodes
		 */
		auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();


		/**
		 * setup the partition tree nodes for the root triangulation.
		 *
		 * also setup the simulation partition handler and create the unique IDs.
		 *
		 * the method _setup_GenericTreeAndPartitions recursively sets up the whole tree!
		 */
		unique_id_counter = 0;
		*o_rootNode = _setup_GenericTreeAndPartitions(
				iter,		// start with first element
				nullptr,	// no parent node
				0			// start with an initial tree depth of zero
		);


		/**
		 * setup clusters
		 */
		for (	auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();
				iter != cDomain_RootTriangulation->domainRootTriangles.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ &cDomain_BaseTriangle = *iter;

			CPartition_TreeNode_ *cPartition = cDomain_BaseTriangle.cPartition_user_ptr;

#if CONFIG_ENABLE_MPI
			if (cDomain_BaseTriangle.mpi_rank != mpi_rank)
			{
				assert(cPartition == nullptr);
				continue;
			}
#else
			assert(cPartition != nullptr);
#endif

			_setup_PartitionEdgeCommunicationInformation(
					cPartition,
					cDomain_BaseTriangle
			);
			cPartition->setup_SimulationClusterHandler(true, nullptr);
		}


#if CONFIG_ENABLE_MPI
		/*
		 * cleanup tree by removing unused nodes
		 */

		if (_cleanupUnusedNodes(*o_rootNode) == true)
		{
			std::cout << "RANK " << mpi_rank << ": Root cluster should be also deleted => more nodes than clusters available!" << std::endl;
		}
#endif

#if CONFIG_SIERPI_COMPILE_WITH_PARALLEL_VERTEX_COMM
		/**
		 * extend edge communication information for vertex data comm
		 */
		for (	auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();
				iter != cDomain_RootTriangulation->domainRootTriangles.end();
				iter++
		)
		{
			CDomain_BaseTriangle_ *cDomain_BaseTriangle = &(*iter);

			_setup_PartitionVertexCommunicationInformation(cDomain_BaseTriangle);
		}
#endif

		_validate();

		cDomain_RootTriangulation = nullptr;

		// return number of triangles
		return (unsigned long long)(1 << initial_refinement_depth)*io_cDomain_RootTriangulation.domainRootTriangles.size();
	}








	/**
	 * start some validation process based on the properties stored in the triangleWithPartition classes
	 *
	 * return true, if the validation failed
	 */
private:
	bool _validate()
	{
		bool valid = true;

#if DEBUG
		for (	auto iter = cDomain_RootTriangulation->domainRootTriangles.begin();
				iter != cDomain_RootTriangulation->domainRootTriangles.end();
				iter++
			)
		{
			CDomain_BaseTriangle_ &t = *iter;

			/**
			 * hypotenuse
			 */
			if (!t.adjacent_triangles.hyp_edge)
			{
				if (	t.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle_ &a = *t.adjacent_triangles.hyp_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * left edge
			 */
			if (!t.adjacent_triangles.left_edge)
			{
				if (	t.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (LEFT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_PartitionHandler> &a = *t.adjacent_triangles.left_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}


			/**
			 * right edge
			 */
			if (!t.adjacent_triangles.right_edge)
			{
				if (	t.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_BOUNDARY &&
						t.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID
						)
				{
					std::cerr << "INVALID TYPE (RIGHT EDGE)!";
					assert(false);
					valid = false;
				}
			}
			else
			{
				CDomain_BaseTriangle<CSimulation_PartitionHandler> &a = *t.adjacent_triangles.right_edge;

				bool found = false;

				if (a.adjacent_triangles.hyp_edge)
					if (a.adjacent_triangles.hyp_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (hyp)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.left_edge)
					if (a.adjacent_triangles.left_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (left)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (a.adjacent_triangles.right_edge)
					if (a.adjacent_triangles.right_edge->cPartition_user_ptr == t.cPartition_user_ptr)
					{
						if (a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_NEW && a.triangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_OLD)
						{
							std::cerr << "Edge type error (right)" << std::endl;
							assert(false);
							valid = false;
						}
						else
						{
							found = true;
						}
					}

				if (!found)
				{
					std::cout << "triangle factory for local triangle: " << std::endl;
					std::cout << t.triangleFactory << std::endl;
					std::cout << "triangle factory for adjacent triangle: " << std::endl;
					std::cout << a.triangleFactory << std::endl;
					std::cerr << "MISSING CONNECTIVITY (HYP EDGE)!";
					assert(false);
					valid = false;
				}
			}

			if (!valid)
			{
				std::cout << "TriangleWithPartition id: " << t.cPartition_user_ptr->uniqueId << std::endl;
				break;
			}
		}
#endif

		return valid;
	}

};


#endif /* CBASETRIANGULATION_TO_GENERICTREE_HPP_ */
