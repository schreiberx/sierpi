/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Feb 7, 2011
 *      Author: Martin Schreiber (schreiberx@gmail.com)
 */

#ifndef CPOINTINTRIANGLETEST_HPP_
#define CPOINTINTRIANGLETEST_HPP_

template <typename TVertexScalar>
class CPointInTriangleTest
{
public:
	static bool test(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TVertexScalar px, TVertexScalar py
	)
	{
		// http://de.wikipedia.org/wiki/Regul%C3%A4re_Matrix
		TVertexScalar ax = vx2-vx1;
		TVertexScalar ay = vy2-vy1;
		TVertexScalar bx = vx3-vx1;
		TVertexScalar by = vy3-vy1;
		TVertexScalar cx = px-vx1;
		TVertexScalar cy = py-vy1;

		TVertexScalar inv_det = (TVertexScalar)1.0/(ax*by - bx*ay);
/*

		std::cout << ax << " " << ay << std::endl;
		std::cout << bx << " " << by << std::endl;
		std::cout << cx << " " << cy << std::endl;
		std::cout << std::endl;
		std::cout << (ax*by - bx*ay) << std::endl;
		std::cout << std::endl;
*/

		TVertexScalar a = inv_det*(by*cx - bx*cy);
		if (a < 0)	return false;
		if (a > 1)	return false;

		TVertexScalar b = inv_det*(-ay*cx + ax*cy);
		if (b < 0)	return false;

		return (a + b <= (TVertexScalar)1.0);
	}
};

#endif /* CPOINTINTRIANGLETEST_HPP_ */
