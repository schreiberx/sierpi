SCONS_OPTS:=-Q -j4

all:	gnu	intel	serial	nosacsp


tests:	gnu intel iomp fancy fancy_intel nosacsp

# make targets for pproc servers
pproc:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release

dpproc:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=debug

iomp:
	# version with iomp
	scons $(SCONS_OPTS) --threading=iomp --compiler=gnu --enable-gui=off --mode=release

ipmo:
	# version with ipmo and omp
	scons $(SCONS_OPTS) --threading=ipmo --compiler=intel --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release

ipmo_gnu:
	# version with ipmo and omp using gnu compiler
	scons $(SCONS_OPTS) --threading=ipmo --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=release

mpi:
	scons --compiler=gnu --enable-mpi=on --enable-gui=off --mode=release --threading=off -j8
	scons --compiler=gnu --enable-mpi=on --enable-gui=on --mode=release --threading=off -j8
	

asagi_gui:
	scons --compiler=intel --enable-gui=on --mode=release --threading=omp --enable-asagi=on --tsunami-flux-solver=5 --fp-default-precision=double -j8

asagi_tbb:
	scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-asagi=on --tsunami-flux-solver=5 --fp-default-precision=double -j8

asagi:
	scons --compiler=intel --enable-gui=off --mode=release --threading=omp --enable-asagi=on --tsunami-flux-solver=5 --fp-default-precision=double -j8

omp:
	# version with OpenMP
	scons $(SCONS_OPTS) --threading=omp --enable-openmp-untied-tasks=on --enable-openmp-task-for-each-leaf=off --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --enable-openmp-untied-tasks=off --enable-openmp-task-for-each-leaf=off --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --enable-openmp-untied-tasks=on --enable-openmp-task-for-each-leaf=on --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --enable-openmp-untied-tasks=off --enable-openmp-task-for-each-leaf=on --compiler=gnu --enable-gui=off --mode=release

dfancy_rk2:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=debug --tsunami-runge-kutta-order=2

dfancy:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=debug

dfancy_scan:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=debug --enable-scan-split-and-join=on

dfancy_intel:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=on --mode=debug
        

fancy:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=release

fancy_intel:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=on --mode=release

fancy_asagi:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=release --use-asagi=true --use-mpicompiler=true

nosacsp:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release --sacsp=off


gnu:
	scons $(SCONS_OPTS) --threading=off --compiler=gnu --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=on --mode=release

fortran:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release --enable-fortran=true

fortran_asagi:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release --enable-fortran=true --use-asagi=true  --use-mpicompiler=true


#
# SERIAL
#
serial:	gnu_serial	intel_serial

gnu_serial_gui:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --enable-gui=on --mode=release

gnu_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --enable-gui=on --mode=release

gnu_serial_debug:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=gnu --enable-gui=on --mode=debug


intel_serial:
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --enable-gui=on --mode=release



#
# TEST
#
gnu_r:
	scons $(SCONS_OPTS) --threading=omp --compiler=gnu --enable-gui=off --mode=release

intel_r:
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release

intel:
	# OpenMP
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=release
#	scons $(SCONS_OPTS) --threading=off --compiler=intel --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=off --compiler=intel --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=off --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=on --mode=debug
	scons $(SCONS_OPTS) --threading=omp --compiler=intel --enable-gui=on --mode=release

tbb:
	# TBB
	scons $(SCONS_OPTS) --threading=tbb --compiler=intel --enable-gui=off --mode=release
	scons $(SCONS_OPTS) --threading=tbb --compiler=intel --enable-gui=on --mode=release


serial_intel:
	# serial
	scons $(SCONS_OPTS) --simulation=tsunami_serial --compiler=intel --enable-gui=off --mode=release



clean_gnu:
	rm -rf build/build_sierpi_gnu_*

clean_intel:
	rm -rf build/build_sierpi_intel_*

clean: clean_gnu clean_intel
	rm -f build/sierpi*
