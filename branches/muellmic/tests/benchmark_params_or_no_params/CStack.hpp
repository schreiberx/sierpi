/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: schreibm
 */

#ifndef CSTACK_HPP_
#define CSTACK_HPP_

#include <stdio.h>

template <typename T>
class CStack {
public:
	T *data;
	T *stack_ptr;

public:
#ifndef NDEBUG
	size_t size;

	size_t getSize()
	{
		return size;
	}
#endif

	/**
	 * swap data pointers with other stack
	 */
	void swap(CStack &stack)
	{
		std::swap(data, stack.data);
		std::swap(stack_ptr, stack.stack_ptr);

#ifndef NDEBUG
		std::swap(size, stack.size);
#endif
	}

	/*
	 * this class is initialized to a Stack to access the stack
	 * in a LIFO way. The stack data is not modified!
	 */
	class CLifoIter
	{
public:
		CStack<T> *stack;
		T *stack_ptr;

		inline void setup(CStack<T> *p_stack)
		{
			stack = p_stack;
			reset();
		}

		inline void setup(CStack<T> &p_stack)
		{
			stack = &p_stack;
			reset();
		}

		inline T getNextData()
		{
			stack_ptr--;
			assert(stack_ptr >= stack->data);
			return *stack_ptr;
		}

		inline T* getNextDataPtr()
		{
			stack_ptr--;
			assert(stack_ptr >= stack->data);
			return stack_ptr;
		}


		inline T& getNextDataRef()
		{
			stack_ptr--;
			assert(stack_ptr >= stack->data);
			return *stack_ptr;
		}

		inline T& getPreviousDataRef()
		{
			return *stack_ptr;
		}

		inline T* getPreviousDataPtr()
		{
			return stack_ptr;
		}

		inline T* getPreviousPreviousDataPtr()
		{
			return stack_ptr+1;
		}

		inline void reset()
		{
			stack_ptr = stack->stack_ptr;
		}

		inline bool isEmpty()
		{
			return stack_ptr == stack->data;
		}
	};


	/*
	 * FiFo Iterator for stacks without modifying the stack
	 */
	class CFifoIter
	{
public:
		CStack<T> *stack;
		T *stack_ptr;

		inline void setup(CStack<T> *p_stack)
		{
			stack = p_stack;
			reset();
		}

		inline void setup(CStack<T> &p_stack)
		{
			stack = &p_stack;
			reset();
		}

		inline T getNextData()
		{
			return *(stack_ptr++);
		}


		inline T* getNextDataPtr()
		{
			return stack_ptr++;
		}


		inline T* getPreviousDataPtr()
		{
			assert(stack_ptr-1 >= stack->data);
			return stack_ptr-1;
		}

		inline T* getPreviousPreviousDataPtr()
		{
			assert(stack_ptr-2 >= stack->data);
			return stack_ptr-2;
		}


		inline void reset()
		{
			stack_ptr = stack->data;
		}

		inline bool isEmpty()
		{
			return stack_ptr == stack->stack_ptr;
		}
	};


	class CIterator
	{
		T *ptr;
		CStack<T> *s;

public:
		inline CIterator(CStack<T> &p_s)
		{
			setup(p_s);
		}

		inline CIterator()
		{
		}

		inline void setup(CStack<T> &p_s)
		{
			s = &p_s;
			ptr = p_s.data;
		}

		inline T operator*()
		{
			return *ptr;
		}

		inline void operator++(int)
		{
			ptr++;
		}

		inline void operator--(int)
		{
			ptr--;
		}

		inline bool end()
		{
			return ptr == s->stack_ptr;
		}

		inline void moveToLastElement()
		{
			ptr = s->stack_ptr-1;
		}

		inline void moveToFirstElement()
		{
			ptr = s->data;
		}

		inline bool isFirstElement()
		{
			return (s->data == ptr);
		}
	};

	inline CStack()
	{
		data = NULL;
		stack_ptr = NULL;
#ifndef NDEBUG
		size = 0;
#endif
	}

	inline CStack(size_t p_size)
	{
		data = new T[p_size];
		stack_ptr = data;
#ifndef NDEBUG
		size = p_size;
#endif

	}

	inline void resize(size_t p_size)
	{
		if (data)
				delete[] data;

		data = new T[p_size];
		stack_ptr = data;

#ifndef NDEBUG
		size = p_size;
#endif
	}

	inline size_t getStackElementCounter()	const
	{
		return (int)(stack_ptr - data);
	}

	inline void setStackElementCounter(size_t elementCounter)
	{
		stack_ptr = data+elementCounter;
	}

	inline size_t getStackElementCounterEquals(T value)	const
	{
		size_t ret_val = 0;

		for (T *ptr = data; ptr != stack_ptr; ptr++)
			if (value == *ptr)
				ret_val++;

		return ret_val;
	}

	virtual ~CStack()
	{
		if (data != NULL)
			delete[] data;
	}

	inline void getElement(size_t i, T &v)
	{
		v = data[i];
	}

	inline T &getElement(size_t i)
	{
		return data[i];
	}

	/**
	 * move the stack pointer to the very beginning
	 */
	inline void clear()
	{
		stack_ptr = data;
	}

	inline void push(T v)
	{
		*stack_ptr = v;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push_PtrValue(T *v)
	{
		*stack_ptr = *v;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}


	inline void push_RefValue(T &v)
	{
		*stack_ptr = v;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}


	inline T *fakePush_ReturnPtr()
	{
		return stack_ptr++;
	}

	inline T &fakePush_ReturnRef()
	{
		return *(stack_ptr++);
	}

	inline void push(T v0, T v1)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push(T v0, T v1, T v2)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push(T v0, T v1, T v2, T v3)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push(T v0, T v1, T v2, T v3, T v4)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push(T v0, T v1, T v2, T v3, T v4, T v5)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void push(T v0, T v1, T v2, T v3, T v4, T v5, T v6)
	{
		*stack_ptr = v0;
		stack_ptr++;
		*stack_ptr = v1;
		stack_ptr++;
		*stack_ptr = v2;
		stack_ptr++;
		*stack_ptr = v3;
		stack_ptr++;
		*stack_ptr = v4;
		stack_ptr++;
		*stack_ptr = v5;
		stack_ptr++;
		*stack_ptr = v6;
		stack_ptr++;

		assert(stack_ptr != data+size);
	}

	inline void pop(T &v)
	{
		v = *stack_ptr--;
	}

	inline T pop()
	{
		stack_ptr--;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	inline T pop2()
	{
		stack_ptr -= 2;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	inline T pop3()
	{
		stack_ptr -= 3;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	inline T pop4()
	{
		stack_ptr -= 4;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	inline T pop5()
	{
		stack_ptr -= 5;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	inline T pop6()
	{
		stack_ptr -= 6;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	/*
	 * do a pop operation on stack and return the pointer instead
	 * of the stack element
	 */
	inline T* pop_ReturnPtr()
	{
		stack_ptr--;
		assert(stack_ptr >= data);
		return stack_ptr;
	}


	inline T& pop_ReturnRef()
	{
		stack_ptr--;
		assert(stack_ptr >= data);
		return *stack_ptr;
	}

	/**
	 * return true, if the stack is empty
	 */
	inline bool isEmpty()
	{
		return stack_ptr == data;
	}

	/**
	 * structure specific method
	 */
	inline bool structure_isValidQuad()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = data; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+2 == zeros;
	}


	/**
	 * structure specific method
	 */
	inline bool structure_isValidTriangle()	const
	{
		int zeros = 0;
		int ones = 0;

		for (T *p = data; p != stack_ptr; p++)
			if (*p == 0)	zeros++;
			else			ones++;

		return ones+1 == zeros;
	}

	/**
	 * return the number of triangles on the leafes.
	 *
	 * This can be computed by the total number of stored bits in the structure stack:
	 *
	 * When a triangle is refined, a '0' is replaced by '100'.
	 * Assuming that the domains border is 'triangle-like', we get the following series:
	 *   # triangles: 1 2 3
	 *       # zeros: 2 3 4
	 *        # ones: 1 2 3
	 * sizeof(stack): 3 5 7
	 *
	 * for this situation, the number of leaf triangles (zeros) would be:
	 *   (sizeof(stack)+1)/2
	 *
	 * for 2 triangles setting up a rectangular domain, we get:
	 *   (sizeof(stack)+2)/2
	 *
	 * since the first refinement of the rectangle isn't stored explicitly
	 * (as it doesn't make sense), a further fix has to be done:
	 *   #leaf triangles = (sizeof(stack)+3)/2
	 */
	unsigned int structure_getNumberOfTrianglesInQuad()	const
	{
		return (getStackElementCounter()+3) / 2;
	}

	unsigned int structure_getNumberOfTrianglesInTriangle()	const
	{
		return (getStackElementCounter()+1) / 2;
	}
};


inline
::std::ostream&
operator<<(::std::ostream &co, CStack<char> &stack)
{
	for (int i = stack.getStackElementCounter()-1; i >= 0 ; i--)
		co << (int)stack.getElement(i) << "|";
	co << "|";

	return co;
}


template <class T>
inline
::std::ostream&
operator<<(::std::ostream &co, CStack<T> &stack)
{
	for (int i = stack.getStackElementCounter()-1; i >= 0 ; i--)
		co << stack.getElement(i) << "|";
	co << "|";

	return co;
}

#endif /* CSTACK_H_ */
