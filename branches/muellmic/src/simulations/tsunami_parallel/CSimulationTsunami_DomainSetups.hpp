/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulationTsunami_DomainSetups.hpp
 *
 *  Created on: Jun 26, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATIONTSUNAMI_DOMAINS_HPP_
#define CSIMULATIONTSUNAMI_DOMAINS_HPP_

#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "lib/CTriangulationFromSVG.hpp"

#define DOMAIN_FILE_PATH	"data/domains/"

class CSimulationTsunami_DomainSetups
{
	typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;

public:
	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Triangle(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Quad(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Quad_PeriodicBoundaries(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
		assert(false);
		/*
		 * THIS DOMAIN TRIANGULATION IS NOT POSSIBLE WITH ONLY 2 TRIANGLES!
		 */
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_Strip_PeriodicBoundaries(CDomain_BaseTriangulation_ &domainBaseTriangulation, int stripSize = 1)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		int initial_displacement = -stripSize+1;


		/*
		 * !!! We have to use 4 triangles per column, to avoid adjacent triangle search problems for edge comm data exchange !!!
		 */
		assert(stripSize <= 1024);
#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[1024][4];
#endif

		for (int i = 0; i < stripSize; i++)
		{
			template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = -1.0+1.0;
			template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = 1.0+1.0;
			template_rootTriangle.vertices[2][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = -1.0+1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][0] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

			template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = 1.0+1.0;
			template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = -1.0+1.0;
			template_rootTriangle.vertices[2][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = 1.0+1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][1] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


			template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = -1.0-1.0;
			template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = 1.0-1.0;
			template_rootTriangle.vertices[2][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = -1.0-1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][2] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

			template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = 1.0-1.0;
			template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = -1.0-1.0;
			template_rootTriangle.vertices[2][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = 1.0-1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][3] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][2]->adjacent_triangles.left_edge = baseTriangles[i][1];
			baseTriangles[i][2]->triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
			baseTriangles[i][1]->adjacent_triangles.left_edge = baseTriangles[i][2];
			baseTriangles[i][1]->triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

			initial_displacement += 2;
		}

		domainBaseTriangulation.multiplyVertexCoordsWithScalar((T)1.0/(T)stripSize);

		return (T)stripSize;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2DCube_PeriodicBoundaries(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "ERROR: Domains with periodic boundaries not activated!" << std::endl;
#endif
		CTriangle_Factory template_rootTriangle;

		typedef typename CTriangle_Factory::TTriangleFactoryScalarType T;
		int initial_displacement = -3;


#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *baseTriangles[4][4];
#endif

		for (int i = 0; i < 4; i++)
		{
			template_rootTriangle.vertices[0][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = -1.0;
			template_rootTriangle.vertices[1][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = 1.0;
			template_rootTriangle.vertices[2][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = -1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][0] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

			template_rootTriangle.vertices[0][0] = -1.0+(T)initial_displacement;
			template_rootTriangle.vertices[0][1] = 1.0;
			template_rootTriangle.vertices[1][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[1][1] = -1.0;
			template_rootTriangle.vertices[2][0] = 1.0+(T)initial_displacement;
			template_rootTriangle.vertices[2][1] = 1.0;

#if CONFIG_PERIODIC_BOUNDARIES
			baseTriangles[i][1] = &
#endif
					domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

			initial_displacement += 2;
		}


		// top quad

		template_rootTriangle.vertices[0][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[0][1] = -1.0+2.0;
		template_rootTriangle.vertices[1][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[1][1] = 1.0+2.0;
		template_rootTriangle.vertices[2][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[2][1] = -1.0+2.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *topTriangleL = &
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[0][1] = 1.0+2.0;
		template_rootTriangle.vertices[1][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[1][1] = -1.0+2.0;
		template_rootTriangle.vertices[2][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[2][1] = 1.0+2.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *topTriangleR = &
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


		/// bottom quad

		template_rootTriangle.vertices[0][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[0][1] = -1.0-2.0;
		template_rootTriangle.vertices[1][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[1][1] = 1.0-2.0;
		template_rootTriangle.vertices[2][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[2][1] = -1.0-2.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *bottomTriangleL = &
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0+(T)-1.0;
		template_rootTriangle.vertices[0][1] = 1.0-2.0;
		template_rootTriangle.vertices[1][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[1][1] = -1.0-2.0;
		template_rootTriangle.vertices[2][0] = 1.0+(T)-1.0;
		template_rootTriangle.vertices[2][1] = 1.0-2.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ *bottomTriangleR = &
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);



#if CONFIG_PERIODIC_BOUNDARIES
		// left and right side
		baseTriangles[0][0]->setupRightEdgeConnection(baseTriangles[3][1]);
		baseTriangles[3][1]->setupRightEdgeConnection(baseTriangles[0][0]);

		// left-top corner edges
		baseTriangles[0][1]->setupLeftEdgeConnection(topTriangleL);
		topTriangleL->setupRightEdgeConnection(baseTriangles[0][1]);

		// left-bottom corner edges
		baseTriangles[0][0]->setupLeftEdgeConnection(bottomTriangleL);
		bottomTriangleL->setupRightEdgeConnection(baseTriangles[0][0]);

		// right-bottom corner edges
		baseTriangles[2][0]->setupLeftEdgeConnection(bottomTriangleR);
		bottomTriangleR->setupRightEdgeConnection(baseTriangles[2][0]);

		// top-bottom corner edges
		baseTriangles[2][1]->setupLeftEdgeConnection(topTriangleR);
		topTriangleR->setupRightEdgeConnection(baseTriangles[2][1]);

		// top edges with top edge of 3rd quad
		baseTriangles[3][1]->setupLeftEdgeConnection(topTriangleR);
		topTriangleR->setupLeftEdgeConnection(baseTriangles[3][1]);

		// bottom edges with bottom edge of 3rd quad
		baseTriangles[3][0]->setupLeftEdgeConnection(bottomTriangleL);
		bottomTriangleL->setupLeftEdgeConnection(baseTriangles[3][0]);
#endif


		domainBaseTriangulation.multiplyVertexCoordsWithScalar((T)1.0/(T)4);

		return (T)4;
	}



	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_QuadQuad_PeriodicBoundaries(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{
#if !CONFIG_PERIODIC_BOUNDARIES
		std::cerr << "Domains with periodic boundaries not activated!" << std::endl;
#endif

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0-1.0;
		template_rootTriangle.vertices[0][1] = -1.0+1.0;
		template_rootTriangle.vertices[1][0] = -1.0-1.0;
		template_rootTriangle.vertices[1][1] = 1.0+1.0;
		template_rootTriangle.vertices[2][0] = -1.0-1.0;
		template_rootTriangle.vertices[2][1] = -1.0+1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle11a =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0-1.0;
		template_rootTriangle.vertices[0][1] = 1.0+1.0;
		template_rootTriangle.vertices[1][0] = 1.0-1.0;
		template_rootTriangle.vertices[1][1] = -1.0+1.0;
		template_rootTriangle.vertices[2][0] = 1.0-1.0;
		template_rootTriangle.vertices[2][1] = 1.0+1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle11b =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);



		template_rootTriangle.vertices[0][0] = 1.0+1.0;
		template_rootTriangle.vertices[0][1] = -1.0+1.0;
		template_rootTriangle.vertices[1][0] = -1.0+1.0;
		template_rootTriangle.vertices[1][1] = 1.0+1.0;
		template_rootTriangle.vertices[2][0] = -1.0+1.0;
		template_rootTriangle.vertices[2][1] = -1.0+1.0;

		/*typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle21a =*/ domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0+1.0;
		template_rootTriangle.vertices[0][1] = 1.0+1.0;
		template_rootTriangle.vertices[1][0] = 1.0+1.0;
		template_rootTriangle.vertices[1][1] = -1.0+1.0;
		template_rootTriangle.vertices[2][0] = 1.0+1.0;
		template_rootTriangle.vertices[2][1] = 1.0+1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle21b =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);



		template_rootTriangle.vertices[0][0] = 1.0-1.0;
		template_rootTriangle.vertices[0][1] = -1.0-1.0;
		template_rootTriangle.vertices[1][0] = -1.0-1.0;
		template_rootTriangle.vertices[1][1] = 1.0-1.0;
		template_rootTriangle.vertices[2][0] = -1.0-1.0;
		template_rootTriangle.vertices[2][1] = -1.0-1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle12a =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0-1.0;
		template_rootTriangle.vertices[0][1] = 1.0-1.0;
		template_rootTriangle.vertices[1][0] = 1.0-1.0;
		template_rootTriangle.vertices[1][1] = -1.0-1.0;
		template_rootTriangle.vertices[2][0] = 1.0-1.0;
		template_rootTriangle.vertices[2][1] = 1.0-1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);



		template_rootTriangle.vertices[0][0] = 1.0+1.0;
		template_rootTriangle.vertices[0][1] = -1.0-1.0;
		template_rootTriangle.vertices[1][0] = -1.0+1.0;
		template_rootTriangle.vertices[1][1] = 1.0-1.0;
		template_rootTriangle.vertices[2][0] = -1.0+1.0;
		template_rootTriangle.vertices[2][1] = -1.0-1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle22a =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0+1.0;
		template_rootTriangle.vertices[0][1] = 1.0-1.0;
		template_rootTriangle.vertices[1][0] = 1.0+1.0;
		template_rootTriangle.vertices[1][1] = -1.0-1.0;
		template_rootTriangle.vertices[2][0] = 1.0+1.0;
		template_rootTriangle.vertices[2][1] = 1.0-1.0;

#if CONFIG_PERIODIC_BOUNDARIES
		typename CDomain_BaseTriangulation_::CDomain_BaseTriangle_ &baseTriangle22b =
#endif
				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


		/*
		 * |--------|--------|
		 * |      b |      b |
		 * |   11   |   12   |
		 * | a      | a      |
		 * -------------------
		 * |      b |      b |
		 * |   21   |   22   |
		 * | a      | a      |
		 * -------------------
		 */
#if CONFIG_PERIODIC_BOUNDARIES
		baseTriangle11b.adjacent_triangles.left_edge = &baseTriangle12a;
		baseTriangle11b.triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangle12a.adjacent_triangles.left_edge = &baseTriangle11b;
		baseTriangle12a.triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

		baseTriangle21b.adjacent_triangles.left_edge = &baseTriangle22a;
		baseTriangle21b.triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangle22a.adjacent_triangles.left_edge = &baseTriangle21b;
		baseTriangle22a.triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

		baseTriangle11a.adjacent_triangles.right_edge = &baseTriangle21b;
		baseTriangle11a.triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangle21b.adjacent_triangles.right_edge = &baseTriangle11a;
		baseTriangle21b.triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

		baseTriangle12a.adjacent_triangles.right_edge = &baseTriangle22b;
		baseTriangle12a.triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
		baseTriangle22b.adjacent_triangles.right_edge = &baseTriangle12a;
		baseTriangle22b.triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
#endif

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2OddTriangles1(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;
		template_rootTriangle.evenOdd = CTriangle_Enums::ODD;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}



	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2OddTriangles2(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 0.0;
		template_rootTriangle.vertices[0][1] = 0.0;
		template_rootTriangle.vertices[1][0] = -2.0;
		template_rootTriangle.vertices[1][1] = 0.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2EvenTriangles1(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 2.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -2.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 0.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_2EvenTriangles2(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 0.0;
		template_rootTriangle.vertices[2][1] = 0.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		return 1.0;
	}

	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_3Triangles(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -3.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_TriangleStrip(CDomain_BaseTriangulation_ &domainBaseTriangulation)
	{

		CTriangle_Factory template_rootTriangle;

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = -1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = -1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 1.0;
		template_rootTriangle.vertices[0][1] = 1.0;
		template_rootTriangle.vertices[1][0] = -1.0;
		template_rootTriangle.vertices[1][1] = 3.0;
		template_rootTriangle.vertices[2][0] = -1.0;
		template_rootTriangle.vertices[2][1] = 1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		template_rootTriangle.vertices[0][0] = 3.0;
		template_rootTriangle.vertices[0][1] = -1.0;
		template_rootTriangle.vertices[1][0] = 1.0;
		template_rootTriangle.vertices[1][1] = 1.0;
		template_rootTriangle.vertices[2][0] = 1.0;
		template_rootTriangle.vertices[2][1] = -1.0;

		domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_QuadTiles(CDomain_BaseTriangulation_ &domainBaseTriangulation, int tiles_x, int tiles_y)
	{

		CTriangle_Factory template_rootTriangle;
		// TODO: care about machine number inaccuracy

		for (double x = -1; x <= (double)tiles_x; x += 2.0)
		{
			for (double y = -1; y <= (double)tiles_y; y += 2.0)
			{
				template_rootTriangle.vertices[0][0] = 1.0+x;
				template_rootTriangle.vertices[0][1] = -1.0+y;
				template_rootTriangle.vertices[1][0] = -1.0+x;
				template_rootTriangle.vertices[1][1] = 1.0+y;
				template_rootTriangle.vertices[2][0] = -1.0+x;
				template_rootTriangle.vertices[2][1] = -1.0+y;

				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);


				template_rootTriangle.vertices[0][0] = -1.0+x;
				template_rootTriangle.vertices[0][1] = 1.0+y;
				template_rootTriangle.vertices[1][0] = 1.0+x;
				template_rootTriangle.vertices[1][1] = -1.0+y;
				template_rootTriangle.vertices[2][0] = 1.0+x;
				template_rootTriangle.vertices[2][1] = 1.0+y;

				domainBaseTriangulation.insert_DomainTriangle(template_rootTriangle);
			}
		}

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(0.5);

		return 2.0;
	}


	template <typename CDomain_BaseTriangulation_>
	static T setupTriangulation_SVG(
			CDomain_BaseTriangulation_ &domainBaseTriangulation,
			const char* filename = DOMAIN_FILE_PATH"test_domain.svg",
			double scale_factor = 0.1
	)
	{

		CTriangulationFromSVG triangulationSVG;


		if (!triangulationSVG.loadSVGFile(filename))
		{
			return false;
		}

	    for (	std::vector<CTriangulationFromSVG::CTriangle2D>::iterator iter = triangulationSVG.triangles.begin();
	    		iter != triangulationSVG.triangles.end();
	    		iter++
	    )
	   	{
			CTriangle_Factory rootTriangle;

			for (int i = 0; i < 3; i++)
	    	{
	    		rootTriangle.vertices[i][0] = iter->vertices[i][0];
	    		rootTriangle.vertices[i][1] = iter->vertices[i][1];
	    	}

			domainBaseTriangulation.insert_DomainTriangle(rootTriangle);
	   	}

		domainBaseTriangulation.multiplyVertexCoordsWithScalar(scale_factor);

		return 1.0/scale_factor;
	}
};

#endif /* CSIMULATIONTSUNAMI_DOMAINS_HPP_ */
