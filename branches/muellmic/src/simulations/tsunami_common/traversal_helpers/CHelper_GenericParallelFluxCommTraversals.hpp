/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_


#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CPartition_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals
{
public:

	/**
	 * run the edge comm traversals
	 */
	template<
		typename CPartition_,				/// partition type offering necessary structures, edge comm data, etc.
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,		/// Traversator including kernel
		typename TEdgeData,				/// type of edge communication data
		typename CStackAccessors,			/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue				/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			CPartition_ExchangeFluxCommData<CPartition_, TEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			CGenericTreeNode<CPartition_> *i_node,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_node->traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode<CPartition_> *i_cGenericTreeNode)
				{
					CPartition_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					(node->cCluster->*(i_simulationSubClass)).actionFirstPass(node->cStacks);
				}
		);

#if CONFIG_ENABLE_SINGLE_FLUX_CLUSTER
		/*
		 * Compute fluxes using uniqueIDs to avoid double flux evaluation:
		 *
		 * The sub-partition with the relation 'uniqueID < adjacentUniqueID' is responsible to compute the fluxes
		 * also in a writing manner for the adjacent one.
		 *
		 * in FLUX COMM PASS:
		 * 1) The responsible sub-partition first fetches the data from the adjacent partition
		 *    to the exchange edge comm data stacks.
		 *
		 * 2) The fluxes are computed for all fetched edge communication data by the responsible sub-partition.
		 *
		 * in SECOND PASS:
		 * 3) Storing the fluxes to the local_edge_comm_data_stack and exchange_edge_comm_data_stack, pulling
		 *    the edge communication data from the sub-partitions with 'uniqueID > adjacentUniqueID' fetches the
		 *    already computed fluxes.
		 */
		i_node->traverse_GenericTreeNode_Parallel_Scan(
				[=](CGenericTreeNode<CPartition_> *i_cGenericTreeNode)
				{
					CPartition_ *node = i_cGenericTreeNode->cPartition_TreeNode;

					// pull edge data only in one direction using uniqueIDs as relation and compute the flux
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_1stPass();
				}
		);
#endif


		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent partitions
		 */
		i_node->traverse_GenericTreeNode_Reduce_Parallel_Scan(
				[=](CGenericTreeNode<CPartition_> *i_cGenericTreeNode, TReduceValue *o_reduceValue)
				{
					CPartition_ *node = i_cGenericTreeNode->cPartition_TreeNode;

#if CONFIG_ENABLE_SINGLE_FLUX_CLUSTER
					// firstly, pull edge communication data from adjacent sub-partitions
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_2ndPass();
#else
					// firstly, pull edge communication data from adjacent sub-partitions and compute fluxes
					(node->cCluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluation();
#endif
					// run computation based on newly set-up stacks
					(node->cCluster->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, i_timestep_size, o_reduceValue);

					node->cCluster->cfl_domain_size_div_max_wave_speed_after_edge_comm = *o_reduceValue;
				},
				&(CReduceOperators::MIN<TReduceValue>),	// use minimum since the minimum timestep has to be selected
				o_cfl_reduce_value
			);
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
