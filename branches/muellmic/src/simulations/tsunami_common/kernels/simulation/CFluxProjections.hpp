/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxProjections.hpp
 *
 *  Created on: Dec 26, 2011
 *      Author: schreibm
 */

#ifndef CFLUXPROJECTIONS_HPP_
#define CFLUXPROJECTIONS_HPP_


class CFluxProjections
{
public:
	/**
	 * compute components of (qx,qy) along normal
	 */
	template <typename T>
	inline static void projectToNormalSpace(
			T &io_qx,
			T &io_qy,
			T i_normal_x,
			T i_normal_y
	)
	{
		// (nx, ny)
		T tmp = io_qx*i_normal_x + io_qy*i_normal_y;

		// (-ny,nx)
		io_qy = -io_qx*i_normal_y + io_qy*i_normal_x;

		io_qx = tmp;
	}



	/**
	 * compute components of (qx,qy) along normal
	 */
	template <typename T>
	inline static void projectToNormalSpaceAndInvert(
			T &io_qx,
			T &io_qy,
			T i_normal_x,
			T i_normal_y
	)
	{
		// (nx, ny)
		T tmp = -io_qx*i_normal_x - io_qy*i_normal_y;

		// (-ny,nx)
		io_qy = io_qx*i_normal_y - io_qy*i_normal_x;

		io_qx = tmp;
	}



	/**
	 * compute backprojected components of (qx,qy) along normal
	 */
	template <typename T>
	inline static void backprojectFromNormalSpace(
			T &io_qx,
			T &io_qy,
			T i_normal_x,
			T i_normal_y
	)
	{
		T tmp = io_qx*i_normal_x - io_qy*i_normal_y;
		io_qy = io_qx*i_normal_y + io_qy*i_normal_x;
		io_qx = tmp;
	}





	/**
	 * compute backprojected components of (qx,qy) along normal
	 */
	template <typename T>
	inline static void backprojectFromNormalSpaceAndInvert(
			T &io_qx,
			T &io_qy,
			T i_normal_x,
			T i_normal_y
	)
	{
		T tmp = -io_qx*i_normal_x + io_qy*i_normal_y;
		io_qy = -io_qx*i_normal_y - io_qy*i_normal_x;
		io_qx = tmp;
	}


};


#endif /* CFLUXPROJECTIONS_HPP_ */
