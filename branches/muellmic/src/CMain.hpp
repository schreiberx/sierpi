/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 17. April 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CMAIN_HPP
#define CMAIN_HPP


#include <iostream>
#include <string>
#include "config.h"
#include "lib/iRef.hpp"
#include "simulations/CSimulation.hpp"
#include "lib/CProcessMemoryInformation.hpp"
#include "lib/CStopwatch.hpp"

// IPMO
#if COMPILE_WITH_IPMO
	#include "mainthreading/CMainThreadingIPMO.hpp"
#else

// IOMP
#if COMPILE_WITH_IOMP
	#include "mainthreading/CMainThreadingIOMP.hpp"
#else

// OMP
#if COMPILE_WITH_OMP
	#include "mainthreading/CMainThreadingOMP.hpp"
#else

// ITBB
#if COMPILE_WITH_ITBB
	#include "mainthreading/CMainThreadingITBB.hpp"
#else

// TBB
#if COMPILE_WITH_TBB
	#include "mainthreading/CMainThreadingTBB.hpp"
#else

// DUMMY
	#include "mainthreading/CMainThreadingDummy.hpp"
#endif
#endif
#endif
#endif
#endif


template <typename CSimulation>
class CMain	:
	public CMainThreading
{
public:
	CProcessMemoryInformation cProcessMemoryInformation;

	int simulation_run_for_fixed_timesteps;
	int simulation_run_for_fixed_simulation_time;
	float simulation_run_with_fixed_timestepSize;

	int grid_initial_recursion_depth;
	int grid_min_relative_recursion_depth;
	int grid_max_relative_recursion_depth;

	int threading_max_number_of_threads;
	int threading_number_of_threads_to_use;
	int thread_affinity_mode;
	int thread_affinity_start_id;
	int thread_ipmo_scalability_graph_id;

	int splitting_partition_split_workload_size;
	int splitting_partition_join_workload_size;

	double adaptivity_refine_threshold, adaptivity_coarsen_threshold;

	int splitting_partition_scan_split_min_workload_size;
	int splitting_partition_scan_join_max_workload_size;

	int simulation_world_scene_id;
	int simulation_terrain_scene_id;
	int simulation_water_surface_scene_id;

	double simulation_parameter_cfl;

	bool setup_initial_column;
	int terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation;

	int verbosity_level;

	double output_simulation_data_after_each_n_realtime_seconds;
	int output_simulation_data_each_nth_timestep;
	double output_simulation_data_after_each_n_simulation_seconds;

	const char *output_simulation_data_filename;

	int partition_update_split_join_size_after_elapsed_timesteps;
	double partition_update_split_join_size_after_elapsed_scalar;

	int partition_split_and_join_every_nth_timestep;

	// pin the current thread to given core nr
	int threading_pin_to_core_nr_after_one_timestep;

	class CDartSamplings
	{
public:
		double position_x;
		double position_y;
		std::string output_file;
	};

	CDartSamplings *dart_sample_points;
	int dart_samplings_size;

	std::ofstream dartfile_stream;

	CSimulation *cSimulation;

	int getMaxNumberOfThreads()
	{
		return threading_max_number_of_threads;
	}

	void setMaxNumberOfThreads(int i_max_number_of_threads)
	{
		threading_max_number_of_threads = i_max_number_of_threads;

		if (cSimulation != nullptr)
			cSimulation->simulation_max_number_of_threads = i_max_number_of_threads;
	}

	int getNumberOfThreadsToUse()
	{
		return threading_number_of_threads_to_use;
	}

	int getThreadAffinityDistance()
	{
		return thread_affinity_mode;
	}

	int getThreadAffinityStartId()
	{
		return thread_affinity_start_id;
	}

	int getThreadIPMOScalabilityGraphId()
	{
		return thread_ipmo_scalability_graph_id;
	}

	void setValueNumberOfThreadsToUse(int i_number_of_threads_to_use)
	{
		threading_number_of_threads_to_use = i_number_of_threads_to_use;

		if (cSimulation != nullptr)
			cSimulation->simulation_number_of_threads = i_number_of_threads_to_use;
	}



	/**
	 * for a tsunami simulation, the approximated workload is given by the number
	 * of grid-cells
	 *
	 * \return simulation workload
	 */
	unsigned long long getSimulationWorkload()
	{
		if (cSimulation == nullptr)
			return 0;

		return cSimulation->number_of_triangles;
	}


	unsigned long long getSimulationSumWorkload()
	{
		if (cSimulation == nullptr)
			return 0;

		return simloop_sum_number_of_triangles;
	}


	int getVerboseLevel()
	{
		return verbosity_level;
	}



	/**
	 * constructor
	 */
	CMain()	:
		cSimulation(nullptr)
	{
		threading_max_number_of_threads = -1;
		threading_number_of_threads_to_use = -1;

		simulation_run_for_fixed_timesteps = -1;
		simulation_run_with_fixed_timestepSize = -1;
		simulation_run_for_fixed_simulation_time = -1;

		grid_initial_recursion_depth = -1;
		grid_min_relative_recursion_depth = -1;
		grid_max_relative_recursion_depth = -1;

		thread_affinity_mode = -1;
		thread_affinity_start_id = -1;
		thread_ipmo_scalability_graph_id = -1;

		splitting_partition_split_workload_size = -1;
		splitting_partition_join_workload_size = -1;

		splitting_partition_scan_split_min_workload_size = -1;
		splitting_partition_scan_join_max_workload_size = -1;

		simulation_world_scene_id = -666;
		simulation_terrain_scene_id = -666;
		simulation_water_surface_scene_id = -666;

		simulation_parameter_cfl = -1;

		setup_initial_column = true;
		terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation = -1;

		verbosity_level = 0;

		output_simulation_data_each_nth_timestep = 100;
		output_simulation_data_after_each_n_realtime_seconds = 1.0;
		output_simulation_data_after_each_n_simulation_seconds = 10.0;

		output_simulation_data_filename = "frame_%08i.vtk";

		output_timing_flags = OUTPUT_TIMING_NULL;
		output_data_flags = OUTPUT_DATA_NULL;

		partition_update_split_join_size_after_elapsed_timesteps = -1;
		partition_update_split_join_size_after_elapsed_scalar = -1;
		partition_split_and_join_every_nth_timestep = 1;

		threading_pin_to_core_nr_after_one_timestep = -1;

		adaptivity_refine_threshold = -666;
		adaptivity_coarsen_threshold = -666;

		dart_sample_points = nullptr;
		dart_samplings_size = 0;
	}


	/**
	 * setup dart sampling points
	 *
	 * Format:
	 *
	 * [double]/[double]/[outputfile],[double]/[double]/[outputfile],[double]/[double]/[outputfile],...
	 *
	 * [posx]/[posy]/[outputfile]
	 */
	bool setupDartSamplingPoints(const char *i_optarg)
	{
		std::string strarg = optarg;

		dart_samplings_size = 1;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == ',')
				dart_samplings_size++;


		size_t delimiter_pos;
		size_t old_pos = 0;

		dart_sample_points = new CDartSamplings[dart_samplings_size];

		for (int i = 0; i < dart_samplings_size; i++)
		{
			// search for next delimiter
			delimiter_pos = strarg.find(',', old_pos);

			std::string sampling_point_string;

			// if not found, we reached the end of the point array
			if (delimiter_pos == std::string::npos)
				sampling_point_string = strarg.substr(old_pos);
			else
				sampling_point_string = strarg.substr(old_pos, delimiter_pos-old_pos);

			old_pos = delimiter_pos+1;


			/*
			 * split single point
			 */
			size_t pos = sampling_point_string.find('/');
			if (pos == std::string::npos)
			{
				std::cout << "Invalid format for -D " << std::endl;
				std::cout << "   use e. g. -D 834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt " << std::endl;
				exit(-1);
			}
			std::string l = sampling_point_string.substr(0, pos);
			std::string r = sampling_point_string.substr(pos+1);


			/*
			 * search for appended filename
			 */
			pos = r.find('/', 1);
			if (pos != std::string::npos)
			{
				dart_sample_points[i].output_file = r.substr(pos+1);
				r = r.substr(0, pos);
			}

			dart_sample_points[i].position_x = std::atof(l.c_str());
			dart_sample_points[i].position_y = std::atof(r.c_str());
		}

		return true;
	}



	bool setupTwoInts(
			const char *i_optarg,
			int *o_integer1,
			int *o_integer2
	)
	{
		std::string strarg = i_optarg;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_integer1 = atoi(i_optarg);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_integer1 = std::atoi(l.c_str());
		*o_integer2 = std::atoi(r.c_str());

		return true;
	}



	bool setupTwoFloats(
			const char *i_optarg,
			double *o_float1,
			double *o_float2
	)
	{
		std::string strarg = i_optarg;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_float1 = atof(i_optarg);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_float1 = std::atof(l.c_str());
		*o_float2 = std::atof(r.c_str());

		return true;
	}


	/**
	 * parse the program start parameters
	 *
	 * \return true when all parameters are valid
	 */
	bool setupProgramParameters(
			int argc,		///< number of specified arguments
			char *argv[]	///< array with arguments
	)
	{
		char optchar;
		while ((optchar = getopt(argc, argv, "a:A:b:B:C:cD:d:f:F:G:g:h:i:k:L:l:n:N:o:pP:r:S:s:t:T:u:U:v:w:y:z:")) > 0)
		{
			switch(optchar)
			{
			case 'c':
				setup_initial_column = true;
				break;

			case 'D':
				if (!setupDartSamplingPoints(optarg))
					goto parameter_info;

				output_data_flags |= OUTPUT_DATA_STDOUT_BUOY;
				break;

			case 'v':
				verbosity_level = atoi(optarg);
				if (verbosity_level >= 2)
				{
					output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
					output_data_flags |= OUTPUT_DATA_STDOUT_MTPS_PER_FRAME;
				}

				if (output_timing_flags == OUTPUT_TIMING_NULL)
					output_timing_flags |= OUTPUT_TIMING_EACH_NTH_REAL_SECOND;
				break;

			case 't':
				simulation_run_for_fixed_timesteps = atoi(optarg);
				break;

			case 'T':
				terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation = atoi(optarg);
				break;

			case 'L':
				simulation_run_for_fixed_simulation_time = atof(optarg);
				break;

			case 's':
				simulation_run_with_fixed_timestepSize = atof(optarg);
				break;


			/*
			 * CFL
			 */
			case 'C':
				simulation_parameter_cfl = atof(optarg);
				break;


			/*
			 * VTK GRID
			 */
			case 'F':
				output_simulation_data_after_each_n_simulation_seconds = atof(optarg);
				output_data_flags |= OUTPUT_DATA_VTK_GRID;
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;
				break;

			case 'f':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				output_data_flags |= OUTPUT_DATA_VTK_GRID;
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				break;


			/*
			 * VTK clusters
			 */
			case 'p':
				output_data_flags |= OUTPUT_DATA_VTK_CLUSTERS;
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				break;


			/*
			 * VTK filename
			 */
			case 'g':
				output_simulation_data_filename = optarg;
				break;


			/*
			 * stdout verbose
			 */
			case 'B':
				output_simulation_data_after_each_n_simulation_seconds = atof(optarg);
				output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND;
				break;

			case 'b':
				output_simulation_data_each_nth_timestep = atoi(optarg);
				output_data_flags |= OUTPUT_DATA_STDOUT_VERBOSE;
				output_timing_flags = OUTPUT_TIMING_EACH_NTH_TIMESTEP;
				break;


#if SIMULATION_TSUNAMI_PARALLEL
			/*
			 * threading
			 */
			case 'n':
				threading_number_of_threads_to_use = atoi(optarg);
				break;

			case 'N':
				threading_max_number_of_threads = atoi(optarg);
				break;

			case 'A':
				thread_affinity_mode = atoi(optarg);
				break;

			case 'S':
				thread_affinity_start_id = atoi(optarg);
				break;

			case 'G':
				thread_ipmo_scalability_graph_id = atoi(optarg);
				break;


			case 'w':
				simulation_world_scene_id = atoi(optarg);
				break;

			case 'y':
				simulation_terrain_scene_id = atoi(optarg);
				break;

			case 'z':
				simulation_water_surface_scene_id = atoi(optarg);
				break;

#endif

			/*
			 * GRID
			 */
			case 'd':
				grid_initial_recursion_depth = atoi(optarg);
				break;

			case 'i':
				grid_min_relative_recursion_depth = atoi(optarg);
				break;

			case 'a':
				grid_max_relative_recursion_depth = atoi(optarg);
				break;

			/*
			 * SPLITTING
			 */
			case 'o':
				setupTwoInts(optarg, &splitting_partition_split_workload_size, &splitting_partition_scan_split_min_workload_size);

				// deactivate auto-mode
				partition_update_split_join_size_after_elapsed_timesteps = 0;
				break;

			case 'r':
				// adaptivity information
				if (!setupTwoFloats(optarg, &adaptivity_refine_threshold, &adaptivity_coarsen_threshold))
					exit(-1);
				break;

			case 'k':
				partition_split_and_join_every_nth_timestep = atoi(optarg);
				break;

			case 'u':
				partition_update_split_join_size_after_elapsed_timesteps = atoi(optarg);
				break;

			case 'U':
				partition_update_split_join_size_after_elapsed_scalar = atof(optarg);
				break;

			/*
			 * SCAN
			 */
			case 'l':
				setupTwoInts(optarg, &splitting_partition_join_workload_size, &splitting_partition_scan_join_max_workload_size);

				// deactivate auto-mode
				partition_update_split_join_size_after_elapsed_timesteps = 0;
				break;

			case 'P':
				threading_pin_to_core_nr_after_one_timestep = atoi(optarg);
				break;

			case 'h':
			default:
				goto parameter_info;
			}
		}

		assert(partition_split_and_join_every_nth_timestep >= 1);

		/**
		 * fix parameters
		 */
		if (splitting_partition_split_workload_size > 0 && splitting_partition_split_workload_size < 2)
		{
			std::cout << "Invalid split size: " << splitting_partition_split_workload_size << "   setting split to 2" << std::endl;
			splitting_partition_split_workload_size = 2;
		}

		if (splitting_partition_join_workload_size == -1)
		{
			if (splitting_partition_split_workload_size != -1)
				splitting_partition_join_workload_size = splitting_partition_split_workload_size/2;
		}
		else
		{
			if (splitting_partition_join_workload_size > splitting_partition_split_workload_size)
			{
				std::cerr << "WARNING: partition_join_workload_size > partition_split_workload_size" << std::endl;
			}
		}

		if (verbosity_level == -99)
		{
			if (output_simulation_data_after_each_n_realtime_seconds == -1)
			{
				std::cout << " setting verbose information output to 1 computation second. Use -B [int] to override." << std::endl;
				output_simulation_data_after_each_n_realtime_seconds = 1.0;
			}
		}

		return true;

parameter_info:
		std::cout << "usage: " << argv[0] << std::endl;
		std::cout << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << "  DOMAIN SETUP:" << std::endl;
		std::cout << "	-w [int]: world scene" << std::endl;
		std::cout << "	-y [int]: terrain" << std::endl;
		std::cout << "	-z [int]: water surface initialization" << std::endl;
#endif
		std::cout << "	-c : setup column" << std::endl;

		std::cout << std::endl;
		std::cout << "  SIMULATION:" << std::endl;
		std::cout << "	-d [int]: initial recursion depth" << std::endl;
		std::cout << "	-i [int]: min relative recursion depth" << std::endl;
		std::cout << "	-a [int]: max relative recursion depth" << std::endl;
		std::cout << "	-C [float]: cfl condition" << std::endl;
		std::cout << "	-t [int]: timesteps" << std::endl;
		std::cout << "	-L [float]: run for given simulation time" << std::endl;
		std::cout << "	-T [int]: terminate simulation after the given number of" << std::endl;
		std::cout << "			timesteps was executed consecutively with the same number of triangles" << std::endl;
		std::cout << "	-s [float]: timestep size" << std::endl;
		std::cout << "	-r [float]/[float]: adaptivity parameters (refine/coarsen)" << std::endl;


		std::cout << std::endl;
		std::cout << "  VERBOSITY / BACKENDS:" << std::endl;
		std::cout << "	-v [int]: verbose mode (0-10, -99 for tabular output)" << std::endl;
		std::cout << "	-f [int]: output .vtk files each [#nth] timestep" << std::endl;
		std::cout << "	-F [int]: output .vtk files each [n] simulation seconds" << std::endl;
		std::cout << "	-b [int]: output data each [#nth] timestep" << std::endl;
		std::cout << "	-B [double]: output data information each [n] simulation seconds" << std::endl;
		std::cout << "	-D [double]/[double]/[outputfile],...: comma separated list of dart station points [x]/[y]/[outputfile]" << std::endl;
		std::cout << "	-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;
		std::cout << "	-p : also write vtk files with partitions" << std::endl;

		std::cout << std::endl;
		std::cout << "  CLUSTERS: SPLITS/JOINS" << std::endl;
		std::cout << "	-o [int]: partition size when to request split" << std::endl;
		std::cout << "	-l [int]: partition size when to request join (both childs have to request a join)" << std::endl;
		std::cout << "	-k [int]: run split- and join-operations only every nth timestep" << std::endl;
		std::cout << "	-u [int]: elapsed timesteps when to update split and join parameters with table hints" << std::endl;
		std::cout << "	-U [double]: factor used for automatic computation of split/join parameter. default: 0 (disabled)" << std::endl;

#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << std::endl;
		std::cout << "  PARALLELIZATION:" << std::endl;
		std::cout << "	-n [int]: Number of threads to use" << std::endl;
		std::cout << "	-N [int]: Maximum number of cores installed in system" << std::endl;
		std::cout << "	-G [int]: Scalability graph for iPMO" << std::endl;
		std::cout << "	-S [int]: Set thread start affinity. default: -1 (disabled). This number represents the start of the core mapping." << std::endl;
		std::cout << "	-A [int]: Set thread affinities. default: -1 (disabled). This number represents the distance between 2 consecutive cores." << std::endl;
#endif

		return false;
	}


	/**
	 * create simulation and setup
	 */
	void simulationSetup()
	{
		cSimulation = new CSimulation(verbosity_level);

		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
		srand(0);

		/***************************************************
		 * SETUP
		 */

		if (grid_initial_recursion_depth != -1)
			cSimulation->grid_initial_recursion_depth = grid_initial_recursion_depth;

		if (grid_min_relative_recursion_depth != -1)
			cSimulation->grid_min_relative_recursion_depth = grid_min_relative_recursion_depth;

		if (grid_max_relative_recursion_depth != -1)
			cSimulation->grid_max_relative_recursion_depth = grid_max_relative_recursion_depth;


#if SIMULATION_TSUNAMI_PARALLEL
		cSimulation->simulation_max_number_of_threads = threading_max_number_of_threads;
		cSimulation->simulation_number_of_threads = threading_number_of_threads_to_use;

		if (simulation_world_scene_id != -666)
			cSimulation->simulation_world_scene_id = simulation_world_scene_id;

		if (simulation_terrain_scene_id != -666)
			cSimulation->simulation_terrain_scene_id = simulation_terrain_scene_id;

		if (simulation_water_surface_scene_id != -666)
			cSimulation->simulation_water_surface_scene_id = simulation_water_surface_scene_id;


		if (splitting_partition_split_workload_size != -1)
			cSimulation->splitting_partition_split_workload_size = splitting_partition_split_workload_size;

		if (splitting_partition_join_workload_size != -1)
			cSimulation->splitting_partition_join_workload_size = splitting_partition_join_workload_size;

		if (splitting_partition_scan_split_min_workload_size != -1)
			cSimulation->splitting_partition_scan_split_min_workload_size = splitting_partition_scan_split_min_workload_size;

		if (splitting_partition_scan_join_max_workload_size != -1)
			cSimulation->splitting_partition_scan_join_max_workload_size = splitting_partition_scan_join_max_workload_size;

		if (partition_update_split_join_size_after_elapsed_scalar != -1)
			cSimulation->partition_update_split_join_size_after_elapsed_scalar = partition_update_split_join_size_after_elapsed_scalar;

		if (partition_update_split_join_size_after_elapsed_timesteps != -1)
			cSimulation->partition_update_split_join_size_after_elapsed_timesteps = partition_update_split_join_size_after_elapsed_timesteps;

		if (simulation_parameter_cfl != -1)
			cSimulation->simulation_parameter_cfl = simulation_parameter_cfl;

		if (adaptivity_refine_threshold != -666)
			cSimulation->refine_threshold = adaptivity_refine_threshold;

		if (adaptivity_coarsen_threshold != -666)
			cSimulation->coarsen_threshold = adaptivity_coarsen_threshold;

#endif

#if SIMULATION_TSUNAMI_SERIAL
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			cSimulation->threading_pin_to_core_nr_after_one_timestep = threading_pin_to_core_nr_after_one_timestep;
#endif


		if (verbosity_level > 2)
		{
			std::cout << " + Number of threads to use: " << cSimulation->simulation_number_of_threads << std::endl;
			std::cout << " + Max number of threads (system): " << cSimulation->simulation_max_number_of_threads << std::endl;
			std::cout << " + Verbose: " << verbosity_level << std::endl;
			std::cout << " + Terminate simulation after #n equal timesteps: " << terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation << std::endl;
			std::cout << " + Timesteps: " << simulation_run_for_fixed_timesteps << std::endl;
			std::cout << " + Output .vtk files each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output .vtk files each n simulation seconds: " << output_simulation_data_after_each_n_simulation_seconds << std::endl;
			std::cout << " + Output data each #nth timestep: " << output_simulation_data_each_nth_timestep << std::endl;
			std::cout << " + Output data each n computation seconds: " << output_simulation_data_after_each_n_realtime_seconds << std::endl;
			std::cout << " + Output simulation data filename: " << output_simulation_data_filename << std::endl;
			std::cout << " +" << std::endl;
			std::cout << " + Timestep size: " << cSimulation->simulation_parameter_timestep_size << std::endl;
			std::cout << " + InitialDepth: " << cSimulation->grid_initial_recursion_depth << std::endl;
			std::cout << " + MinDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_min_relative_recursion_depth) << std::endl;
			std::cout << " + MaxDepth: " << (cSimulation->grid_initial_recursion_depth+cSimulation->grid_max_relative_recursion_depth) << std::endl;
			std::cout << " + WorldSceneId: " << cSimulation->simulation_world_scene_id << std::endl;
			std::cout << " + WaterSurfaceId: " << cSimulation->simulation_water_surface_scene_id << std::endl;
			std::cout << " + TerrainSceneId: " << cSimulation->simulation_terrain_scene_id << std::endl;
			std::cout << " + Cluster size when to request split: " << cSimulation->splitting_partition_split_workload_size << std::endl;
			std::cout << " + Cluster size when to request join (both childs have to request a join): " << cSimulation->splitting_partition_join_workload_size << std::endl;
			std::cout << " + Cluster minimum size when to allow split by scan: " << cSimulation->splitting_partition_scan_split_min_workload_size << std::endl;
			std::cout << " + Cluster maximum size when to allow join by scan: " << cSimulation->splitting_partition_scan_join_max_workload_size << std::endl;

			if (cSimulation->partition_update_split_join_size_after_elapsed_timesteps > 0)
			{
				std::cout << " + elapsed_timesteps_to_update_split_and_join_parameters: " << cSimulation->partition_update_split_join_size_after_elapsed_timesteps << std::endl;
				std::cout << " + elapsed_timesteps_to_udpate_split_and_join_scalar: " << cSimulation->partition_update_split_join_size_after_elapsed_scalar << std::endl;
			}

			if (dart_sample_points != nullptr)
			{
				std::cout << " + Using " << dart_samplings_size << " dartpoints:" << std::endl;
				for (int i = 0; i < dart_samplings_size; i++)
				{
					std::cout << "    > " << dart_sample_points[i].position_x << " " << dart_sample_points[i].position_y;
					if (!dart_sample_points[i].output_file.empty())
						std::cout << "   | writing dartdata to '" << dart_sample_points[i].output_file << "'";
					std::cout << std::endl;
				}

			}
			if (cSimulation->simulation_parameter_timestep_size == -1)
				std::cout << " + Timestep Size: adaptive CFL " << cSimulation->simulation_parameter_cfl << std::endl;
			else
				std::cout << " + Timestep Size: " << cSimulation->simulation_parameter_timestep_size << std::endl;

#if ADAPTIVE_CLUSTER_STACKS
			std::cout << " + ADAPTIVE_CLUSTER_STACKS enabled" << std::endl;
#endif

			if (verbosity_level > 2)
			{
				cSimulation->outputVerboseInformation();
			}
		}

		simloop_cStopwatch.start();

		/***************************************************
		 * reset & therefore setup the simulation with flat water surface
		 */

		cSimulation->reset_Simulation();


		/***************************************************
		 * remember initial number of triangles
		 */
		if (verbosity_level > 2)
			std::cout << " + Initial number of triangles after base triangulation: " << cSimulation->number_of_initial_triangles_after_domain_triangulation << std::endl;



		/*****************************************************
		 * setup adaptive simulation
		 */
		cSimulation->setup_GridDataWithAdaptiveSimulation();



		/*****************************************************
		 * setup initial split of partitions
		 *
		 * initial splitting of partitions should be executed before
		 * setting up column to avoid any preprocessing adaptive effects
		 */
		{
			size_t t = std::numeric_limits<size_t>::max();

			while (cSimulation->number_of_simulation_clusters != t)
			{
				t = cSimulation->number_of_simulation_clusters;

#if SIMULATION_TSUNAMI_PARALLEL
				cSimulation->setup_SplitJoinPartitions();
#endif

				if (verbosity_level > 1)
					std::cout << " + splitted to " << cSimulation->number_of_simulation_clusters << " partitions with " << cSimulation->number_of_triangles << " triangles" << std::endl;
			}
		}


		/***************************************************
		 * setup column
		 */
		if (setup_initial_column && cSimulation->simulation_water_surface_scene_id == 0)
		{
			if (verbosity_level > 0)
				std::cout << "[ SETUP COLUMN ]" << std::endl;

			cSimulation->number_of_triangles = cSimulation->setup_ColumnAt2DPosition();

			if (verbosity_level > 1)
				std::cout << " + refined to " << cSimulation->number_of_triangles << " triangles" << std::endl;

			if (verbosity_level > 0)
				std::cout << " + pre splitting partitions: " << cSimulation->number_of_simulation_clusters << " partitions" << std::endl;
		}

		if (verbosity_level >= 5)
			std::cout << "SETUP TIME: " << simloop_cStopwatch.getTimeSinceStart() << std::endl;
	}


	/**
	 * shutdown simulation class
	 */
	void shutdownSimulation()
	{
		delete cSimulation;
	}


	/***************************************************
	 * SIMULATION LOOP
	 */
	CStopwatch simloop_cStopwatch;

	unsigned long long simloop_sum_number_of_triangles;
	unsigned long long simloop_prev_number_of_triangles;
	unsigned long long simloop_sum_number_of_partitions;
	char simloop_char_buffer[1024];

	int simloop_outputVTKFrameCounter;
	int simloop_output_timing_each_nth_timestep_timestep_counter;
	int simloop_frameCounter;

	/*
	 * stopwatches for edge/adaptive/split-join traversals
	 */
	double simloop_edgeCommTime;
	double simloop_adaptiveTime;
	double simloop_splitJoinTime;
	double simloop_otherTime;

	/*
	 * output verbose information after output_verbose_information_after_each_n_seconds seconds
	 */
	double simloop_output_timing_each_nth_real_second__next_timestamp;
	double simloop_output_timing_each_nth_simulation_second__next_timestamp;
	double simloop_output_simulation_info_next_simulation_time;

	bool simloop_finishSimulation;
	int simloop_timestep_nr;

	double output_mtps_for_each_second_old_timestamp;
	unsigned int output_mtps_for_each_second_old_timestep;
	unsigned long long output_mtps_for_n_seconds_number_of_processed_triangles;
	double output_mtps_for_simulation_frame_mtps;
	double output_average_cells_for_simulation_frame;



	/**
	 * how is writing of data triggered?
	 */
	enum
	{
		OUTPUT_TIMING_NULL	= 0,

		OUTPUT_TIMING_EACH_NTH_TIMESTEP = 1,
		OUTPUT_TIMING_EACH_NTH_REAL_SECOND = 2,
		OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND = 3,
	};

	int output_timing_flags;



	/**
	 * which data should be written?
	 */
	enum
	{
		OUTPUT_DATA_NULL = 0,

		OUTPUT_DATA_STDOUT_VERBOSE = (1 << 1),
		OUTPUT_DATA_VTK_GRID = (1 << 2),
		OUTPUT_DATA_VTK_CLUSTERS = (1 << 3),
		OUTPUT_DATA_BUOY = (1 << 4),
		OUTPUT_DATA_STDOUT_BUOY = (1 << 5),
		OUTPUT_DATA_STDOUT_MTPS_PER_FRAME = (1 << 6)
	};

	int output_data_flags;


	/**
	 * this method is executed at the beginning of each simulation loop.
	 */
	void simulationLoopPrefix()
	{
		setValueNumberOfThreadsToUse(threading_number_of_threads_to_use);
		setMaxNumberOfThreads(threading_max_number_of_threads);

		if (verbosity_level > 1)
		{
			std::cout << "[ START ]" << std::endl;
		}

		simloop_cStopwatch.start();


		/***************************************************
		 * SIMULATION
		 */
		simloop_sum_number_of_triangles = 0;
		simloop_prev_number_of_triangles = 0;
		simloop_sum_number_of_partitions = 0;

		simloop_outputVTKFrameCounter = 0;
		simloop_output_timing_each_nth_timestep_timestep_counter = 0;
		simloop_frameCounter = 0;

		/*
		 * stopwatches for edge/adaptive/split-join traversals
		 */
		simloop_edgeCommTime = 0;
		simloop_adaptiveTime = 0;
		simloop_splitJoinTime = 0;
		simloop_otherTime = 0;

		simloop_output_timing_each_nth_simulation_second__next_timestamp = 0;
		simloop_output_timing_each_nth_real_second__next_timestamp = 0;

		simloop_finishSimulation = false;
		simloop_timestep_nr = 0;

		output_mtps_for_each_second_old_timestamp = -1.0;
		output_mtps_for_each_second_old_timestep = 0;
		output_mtps_for_n_seconds_number_of_processed_triangles = 0;
		output_mtps_for_simulation_frame_mtps = 0;
		output_average_cells_for_simulation_frame = 0;
	}



	/**
	 * simulation loop executing during each particular simulation step
	 *
	 * the separation into header/iteration/footer is necessary to be
	 * allowed to change the number of resources during each simulation when desired.
	 */
	bool simulationLoopIteration()
	{
		/***************************************************
		 * output some data/information?
		 */
		bool outputSimulationData = false;


		if (output_timing_flags != 0)
		{
			/*
			 * OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_SIMULATION_SECOND)
			{
				if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation->timestamp_for_timestep)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_simulation_second__next_timestamp += output_simulation_data_after_each_n_simulation_seconds;

					if (simloop_output_timing_each_nth_simulation_second__next_timestamp <= cSimulation->timestamp_for_timestep)
					{
						simloop_output_timing_each_nth_simulation_second__next_timestamp = cSimulation->timestamp_for_timestep;
						std::cout << "WARNING: Timestep size for accurate data output is too small, fixing..." << std::endl;
					}
				}
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_TIMESTEP
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_TIMESTEP)
			{
				/*************************************************************************
				 * verbose information (e. g. vtk files) each nth timestep
				 *************************************************************************
				 * n timesteps are computed when simloop_outputVTKFrameCounter equals 0
				 */
				if (simloop_output_timing_each_nth_timestep_timestep_counter == 0)
				{
					outputSimulationData = true;

					if (verbosity_level > 6)
						cProcessMemoryInformation.outputUsageInformation();
				}

				simloop_output_timing_each_nth_timestep_timestep_counter = (simloop_output_timing_each_nth_timestep_timestep_counter+1) % output_simulation_data_each_nth_timestep;
			}

			/*
			 * OUTPUT_TIMING_EACH_NTH_REAL_SECOND
			 */
			if (output_timing_flags == OUTPUT_TIMING_EACH_NTH_REAL_SECOND)
			{
				double timestamp = simloop_cStopwatch.getTimeSinceStart();
				if (simloop_output_timing_each_nth_real_second__next_timestamp <= timestamp)
				{
					outputSimulationData = true;

					simloop_output_timing_each_nth_real_second__next_timestamp += output_simulation_data_after_each_n_realtime_seconds;
					if (simloop_output_timing_each_nth_real_second__next_timestamp < timestamp)
					{
						std::cerr << "WARNING: Computation time lag. Simulation is running to slow to output information each " << output_simulation_data_after_each_n_realtime_seconds << " second(s)... fixing verbose output time" << std::endl;
						simloop_output_timing_each_nth_real_second__next_timestamp = timestamp;
					}
				}
			}

			/*
			 * update triangle throughput counter
			 */
			output_mtps_for_n_seconds_number_of_processed_triangles += cSimulation->number_of_triangles;

			simloop_outputVTKFrameCounter = (simloop_outputVTKFrameCounter+1) % output_simulation_data_each_nth_timestep;


			if (outputSimulationData)
			{
				/*
				 * OUTPUT_DATA_STDOUT_BUOY
				 */
				if (output_data_flags & OUTPUT_DATA_STDOUT_BUOY)
				{
					for (int i = 0; i < dart_samplings_size; i++)
					{
						double sampleValue = cSimulation->getDataSample(dart_sample_points[i].position_x, dart_sample_points[i].position_y, "h+b");

						if (dart_sample_points[i].output_file.empty())
						{
							std::cout << cSimulation->timestamp_for_timestep << ": " << sampleValue << std::endl;
						}
						else
						{
							std::ofstream s;

							if (simloop_timestep_nr == 0)
								s.open(dart_sample_points[i].output_file.c_str());
							else
								s.open(dart_sample_points[i].output_file.c_str(), std::ofstream::app);

							s << cSimulation->timestamp_for_timestep << "\t" << sampleValue << "\t" << cSimulation->number_of_triangles << std::endl;
						}
					}
				}

				/*
				 * OUTPUT_DATA_VTK_GRID
				 */
				if (output_data_flags & OUTPUT_DATA_VTK_GRID)
				{
					sprintf(simloop_char_buffer, output_simulation_data_filename, simloop_frameCounter);

					if (verbosity_level > 3)
					{
						std::cout << "=========================================" << std::endl;
						std::cout << "   + writing file " << simloop_char_buffer << std::endl;
					}

					cSimulation->writeTrianglesToVTKFile(simloop_char_buffer, true);


#if SIMULATION_TSUNAMI_PARALLEL
					if (output_data_flags & OUTPUT_DATA_VTK_CLUSTERS)
					{
						std::string partitionFile = "partitions_";
						partitionFile += simloop_char_buffer;

						cSimulation->writeClustersToVTKFile(partitionFile.c_str(), true);
					}
#endif
					simloop_frameCounter++;
				}

				/*
				 * OUTPUT_DATA_STDOUT_VERBOSE
				 */
				if (output_data_flags & OUTPUT_DATA_STDOUT_VERBOSE)
				{
					double timestamp = simloop_cStopwatch.getTimeSinceStart();

					if (output_mtps_for_each_second_old_timestamp == -1)
					{
						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = 0;
					}

					double delta = timestamp - output_mtps_for_each_second_old_timestamp;
					if (delta >= output_simulation_data_after_each_n_simulation_seconds)
					{
						/*
						 * more than output_simulation_data_after_each_n_simulation_seconds second
						 */
						if (output_simulation_data_after_each_n_simulation_seconds >= 0)
							output_mtps_for_simulation_frame_mtps = ((double)output_mtps_for_n_seconds_number_of_processed_triangles / delta)*0.000001;
						if (cSimulation->timestep_nr != 0)
							output_average_cells_for_simulation_frame = output_mtps_for_n_seconds_number_of_processed_triangles/(double)(cSimulation->timestep_nr - output_mtps_for_each_second_old_timestep);

						output_mtps_for_each_second_old_timestamp = timestamp;
						output_mtps_for_each_second_old_timestep = cSimulation->timestep_nr;
						output_mtps_for_n_seconds_number_of_processed_triangles = 0;
					}


					if (verbosity_level == -99)
					{
						/*
						 * create OUTPUT in tabular format
						 */
						if (simloop_timestep_nr == 0)
						{
							/*
							 * print header
							 */
							std::cout << "MTPS\t";
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "MTPS_FRAME\t";
							std::cout << "TIMESTEP\t";
							std::cout << "SIMTIME\t";
							std::cout << "CELLS\t";
							std::cout << "CELLS_FRAME\t";
							std::cout << "MB_PER_TIMESTEP\t";
							std::cout << "TIMESTEP_SIZE\t";
							std::cout << "PARTITIONS\t";
							std::cout << "SPLIT_SIZE\t";
							std::cout << "JOIN_SIZE";
							std::cout << std::endl;
						}

						/*
						 * print rows
						 */
						if (simloop_timestep_nr == 0)
						{
							std::cout << "0" << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "0" << '\t';
						}
						else
						{
							std::cout << ((double)simloop_sum_number_of_triangles/simloop_cStopwatch.getTimeSinceStart())*0.000001 << '\t';

							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << output_mtps_for_simulation_frame_mtps << '\t';
						}

						std::cout << simloop_timestep_nr << '\t';
						std::cout << cSimulation->timestamp_for_timestep << '\t';
						std::cout << cSimulation->number_of_triangles << '\t';
						std::cout << output_average_cells_for_simulation_frame << '\t';
						double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation->number_of_triangles)/(1024.0*1024.0);
						std::cout << elementdata_megabyte_per_timestep << '\t';
						std::cout << cSimulation->simulation_parameter_timestep_size << '\t';
						std::cout << cSimulation->number_of_simulation_clusters << '\t';
						std::cout << cSimulation->splitting_partition_split_workload_size << '\t';
						std::cout << cSimulation->splitting_partition_join_workload_size;
						std::cout << std::endl;
					}
					else
					{
						std::cout << "=========================================" << std::endl;

						if (simloop_timestep_nr != 0)
						{
							std::cout << "   + " << ((double)simloop_sum_number_of_triangles/simloop_cStopwatch.getTimeSinceStart())*0.000001 << " Overall MTPS (Mega Triangles per second)" << std::endl;
							if (output_data_flags & OUTPUT_DATA_STDOUT_MTPS_PER_FRAME)
								std::cout << "   + " << output_mtps_for_simulation_frame_mtps << " Last Second MTPS (Mega Triangles per second)" << std::endl;
						}

						std::cout << "   + " << simloop_timestep_nr << "\tTIMESTEP" << std::endl;
						std::cout << "   + " << cSimulation->timestamp_for_timestep << "\tSIMULATION_TIME" << std::endl;
						std::cout << "   + " << cSimulation->number_of_triangles << "\tTRIANGLES" << std::endl;
						std::cout << "   + " << simloop_cStopwatch.getTimeSinceStart() << " RT (REAL_TIME)" << std::endl;
						double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)cSimulation->number_of_triangles)/(1024.0*1024.0);
						std::cout << "   + " << elementdata_megabyte_per_timestep << "\tElementData Megabyte per Timestep (RW)" << std::endl;
						std::cout << "   + " << cSimulation->simulation_parameter_timestep_size << "\tTIMESTEP SIZE" << std::endl;
						std::cout << "   + " << cSimulation->number_of_simulation_clusters << "\tPARTITIONS" << std::endl;
						std::cout << "   + " << simloop_edgeCommTime << "/" << simloop_adaptiveTime << "/" << simloop_splitJoinTime << "\tTIMINGS (edgeComm/adaptive/splitJoin)" << std::endl;
					}
				}
			}
		}


		/*
		 * single simulation simloop_timestep_id
		 */
		bool run_split_and_join = simloop_timestep_nr % partition_split_and_join_every_nth_timestep == 0;
		if (verbosity_level > 2)
		{
			cSimulation->runSingleTimestepDetailedBenchmarks(
					&simloop_edgeCommTime,
					&simloop_adaptiveTime,
					&simloop_splitJoinTime,
					run_split_and_join
				);
		}
		else
		{
			cSimulation->runSingleTimestep(run_split_and_join);
		}


		/*
		 * increment timestep id
		 */
		simloop_timestep_nr++;


		/*
		 * increment counter of number of triangles processed so far
		 */
		simloop_sum_number_of_triangles += cSimulation->number_of_triangles;
		simloop_sum_number_of_partitions += cSimulation->number_of_simulation_clusters;


		/*
		 * verbose points
		 */
		if (verbosity_level > 5)
		{
			std::cout << "." << std::flush;
		}

		/*
		 * if timesteps are not set, we quit as soon as the initial number of triangles was reached
		 */
		if (simulation_run_for_fixed_timesteps == -1)
		{

			if (simulation_run_for_fixed_simulation_time != -1)
			{
				if (cSimulation->timestamp_for_timestep >= simulation_run_for_fixed_simulation_time)
					return false;
			}
			else if (terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation != -1)
			{
				static int consecutive_timesteps_with_equal_triangle_number_counter = 0;

				if (simloop_prev_number_of_triangles == cSimulation->number_of_triangles)
				{
					consecutive_timesteps_with_equal_triangle_number_counter++;

					if (consecutive_timesteps_with_equal_triangle_number_counter > terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation)
					{
						std::cout << std::endl;
						std::cout << " + Terminating after " << terminate_simulation_after_n_timesteps_with_equal_number_of_triangles_in_simulation << " timesteps with equal number of triangles -> EXIT" << std::endl;
						std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
						std::cout << std::endl;
						return false;
					}
				}

				simloop_prev_number_of_triangles = cSimulation->number_of_triangles;
			}
#if !COMPILE_SIMULATION_WITH_GUI
			else
			{
				if (cSimulation->number_of_initial_triangles_after_domain_triangulation == cSimulation->number_of_triangles)
				{
					std::cout << std::endl;
					std::cout << " + Initial number of triangles reached -> EXIT" << std::endl;
					std::cout << " + Final simloop_timestep_id: " << simloop_timestep_nr << std::endl;
					std::cout << std::endl;
					return false;
				}
			}
#endif
		}
		else
		{
			if (simloop_timestep_nr >= simulation_run_for_fixed_timesteps)
				return false;
		}

		return true;
	}


	/**
	 * this method is executed at the end of the simulation
	 */
	void simulationLoopSuffix()
	{
		simloop_cStopwatch.stop();

		if (verbosity_level > 1)
		{
			std::cout << "[ END ]" << std::endl;
			std::cout << std::endl;
		}


		if (verbosity_level > 2)
		{
			std::cout << std::endl;
			std::cout << "Timings for simulation phases:" << std::endl;
			std::cout << " + EdgeCommTime: " << simloop_edgeCommTime << std::endl;
			std::cout << " + AdaptiveTime: " << simloop_adaptiveTime << std::endl;
			std::cout << " + SplitJoinTime: " << simloop_splitJoinTime << std::endl;
			std::cout << std::endl;
		}

		double real_stoptime = simloop_cStopwatch();

		std::cout << simloop_timestep_nr << " TS (Timesteps)" << std::endl;
		std::cout << cSimulation->timestamp_for_timestep << " ST (SIMULATION_TIME)" << std::endl;
		std::cout << cSimulation->simulation_parameter_timestep_size << " TSS (Timestep size)" << std::endl;
		std::cout << real_stoptime << " RT (REAL_TIME)" << std::endl;
		std::cout << simloop_sum_number_of_triangles << " TP (Triangles processed)" << std::endl;
		std::cout << (double)real_stoptime/(double)simloop_timestep_nr << " ASPT (Averaged Seconds per Timestep)" << std::endl;
		std::cout << (double)simloop_sum_number_of_triangles/(double)simloop_timestep_nr << " TPST (Triangles Processed in Average per Simulation Timestep)" << std::endl;

	#if SIMULATION_TSUNAMI_PARALLEL
		std::cout << (double)simloop_sum_number_of_partitions/(double)simloop_timestep_nr << " PPST (Partitions Processed in Average per Simulation Timestep)" << std::endl;
	#endif

		double MTPS = ((double)simloop_sum_number_of_triangles/real_stoptime)*0.000001;
		std::cout << MTPS << " MTPS (Million Triangles per Second)" << std::endl;
		std::cout << ((double)simloop_sum_number_of_triangles/(simloop_cStopwatch()*(double)threading_number_of_threads_to_use))*0.000001 << " MTPSPT (Million Triangles per Second per Thread)" << std::endl;

		double elementdata_megabyte_per_timestep = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)simloop_timestep_nr)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_timestep << " EDMBPT (ElementData Megabyte per Timestep (RW))" << std::endl;

		double elementdata_megabyte_per_second = ((double)sizeof(CTsunamiElementData)*2.0)*((double)simloop_sum_number_of_triangles/(double)real_stoptime)/(1024.0*1024.0);
		std::cout << elementdata_megabyte_per_second << " EDMBPS (ElementData Megabyte per Second (RW))" << std::endl;
	}



	virtual ~CMain()
	{
		if (dart_sample_points)
		{
			delete []dart_sample_points;
			dart_sample_points = nullptr;
		}

#ifdef DEBUG
		if (!debug_ibase_list.empty())
		{
			std::cout << "MEMORY LEAK: iBase class missing in action" << std::endl;
			std::cout << "  + number of classes: " << debug_ibase_list.size() << std::endl;
		}
#endif

		if (verbosity_level > 5)
		{
			cProcessMemoryInformation.outputUsageInformation();
		}
	}

};

#endif
