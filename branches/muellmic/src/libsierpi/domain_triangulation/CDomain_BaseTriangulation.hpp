/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CDomain_BaseTriangulation.hpp
 *
 *  Created on: Mar 31, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CDOMAIN_BASE_TRIANGULATION_HPP_
#define CDOMAIN_BASE_TRIANGULATION_HPP_

#include <list>
#include <map>
#include <stdint.h>
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "CDomain_BaseTriangle.hpp"
#include "libsierpi/parallelization/CPartition_EdgeComm_InformationAdjacentPartitions.hpp"
#include "lib/iRef.hpp"
#include "libmath/CRegion2D.hpp"
#include "libmath/CMath.hpp"


/**
 * class storing all domain root triangles to assemble a valid domain built upon triangles
 */
template <typename CSimulation_PartitionHandler>
class CDomain_BaseTriangulation
{
public:
	/*
	 * type abbreviations for convenience
	 */
	typedef CDomain_BaseTriangle<CSimulation_PartitionHandler> CDomain_BaseTriangle_;

	typedef CPartition_TreeNode<CSimulation_PartitionHandler> CPartition_;
	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition_> CEdgeComm_InformationAdjacentPartition_;


	/**
	 * fake a tree node which does not store any data and which is only useful for the traversals
	 */
	typedef CGenericTreeNode<CPartition_TreeNode<CSimulation_PartitionHandler> > CGenericTree_;


	/**
	 * fix applied for odd initial depth
	 */
public:
	bool oddInitialDepthFixApplied;


	/**
	 * used rectangular domain region
	 */
public:
	CRegion2D<CTriangle_Factory::T> region;


	/**
	 * storage for all base domain triangles
	 */
public:
	std::list<CDomain_BaseTriangle_ > domainRootTriangles;



	/**
	 * constructor
	 */
public:
	CDomain_BaseTriangulation()
	{
		clear();
	}



	/**
	 * deconstructor
	 */
public:
	~CDomain_BaseTriangulation()
	{
		clear();
	}



	/**
	 * clear and free everything
	 */
public:
	void clear()
	{
		oddInitialDepthFixApplied = false;
		region.reset();
		domainRootTriangles.clear();
	}



	/**
	 * this method removes those root triangles which create invalid
	 * edge communication information due to inconsistent hanging nodes
	 */
public:
	void fixForOddDepth()
	{
		int evenCounter = 0;
		int oddCounter = 0;

		for (auto iter = domainRootTriangles.begin(); iter != domainRootTriangles.end(); iter++)
		{
			CDomain_BaseTriangle_ &rootTriangle = *iter;

			if (rootTriangle.triangleFactory.evenOdd == CTriangle_Enums::EVEN)
				evenCounter++;

			if (rootTriangle.triangleFactory.evenOdd == CTriangle_Enums::ODD)
				oddCounter++;
		}

		oddInitialDepthFixApplied = true;

		if (evenCounter == 0 || oddCounter == 0)
		{
			// no fix necessary
			return;
		}

		std::cerr << "WARNING: REMOVING ROOT TRIANGLES TO GET CONSISTENT EDGE COMM INFORMATION" << std::endl;


		if (evenCounter < oddCounter)
		{
			for (auto iter = domainRootTriangles.begin(); iter != domainRootTriangles.end(); /* no iter */ )
			{
				CDomain_BaseTriangle_ &rootTriangle = *iter;

				if (rootTriangle.triangleFactory.evenOdd == CTriangle_Enums::EVEN)
				{
					iter = domainRootTriangles.erase(iter);
					continue;
				}
				iter++;
			}
		}
		else
		{
			for (auto iter = domainRootTriangles.begin(); iter != domainRootTriangles.end(); /* no iter */ )
			{
				CDomain_BaseTriangle_ &rootTriangle = *iter;

				if (rootTriangle.triangleFactory.evenOdd == CTriangle_Enums::ODD)
				{
					iter = domainRootTriangles.erase(iter);
					continue;
				}
				iter++;
			}
		}
	}



	/**
	 * insert a new triangle into the domain grid
	 *
	 * this implementation is quite slow and should be called only during initialization!!!
	 */
public:
	CDomain_BaseTriangle_ &insert_DomainTriangle(
			CTriangle_Factory &i_newTriangleFactory,	///< factory for new triangle
			bool autoSetup = true
	)
	{
		// insert root triangle to list
		domainRootTriangles.push_back(CDomain_BaseTriangle_(i_newTriangleFactory));

		// extend maximum border
		region.extend(i_newTriangleFactory.vertices[0][0], i_newTriangleFactory.vertices[0][1]);
		region.extend(i_newTriangleFactory.vertices[1][0], i_newTriangleFactory.vertices[1][1]);
		region.extend(i_newTriangleFactory.vertices[2][0], i_newTriangleFactory.vertices[2][1]);

		CDomain_BaseTriangle_ &rootTriangle = domainRootTriangles.back();

		if (!autoSetup)
			return rootTriangle;

		// load new triangle factory and setup invalid values to valid values
		CTriangle_Factory &triangleFactory = rootTriangle.triangleFactory;

		setup_TriangleFactory(triangleFactory);

		return rootTriangle;
	}



	/**
	 * scale all vertices with given scalar value
	 */
public:
	void multiplyVertexCoordsWithScalar(
			CTriangle_Factory::T scalar = (CTriangle_Factory::T)1.0
	)
	{
		for (	auto iter = domainRootTriangles.begin();
				iter != domainRootTriangles.end();
				iter++
		)
		{
			for (int i = 0; i < 3; i++)
			{
				iter->triangleFactory.vertices[i][0] *= scalar;
				iter->triangleFactory.vertices[i][1] *= scalar;
			}
		}

		/*
		 * also scale region border
		 */
		region.scale(scalar);
	}



	/**
	 * check if 2 Edges coincide
	 *
	 * return 1 if the edge vertices coincide
	 * return -1 if the coincide by exchanging the vertices of one edge
	 * return 0 if they don't coincide
	 */
private:
	int _isSameEdge(
			CTriangle_Factory::T a1[2], CTriangle_Factory::T a2[2],	// 2 vertices for edge A
			CTriangle_Factory::T b1[2], CTriangle_Factory::T b2[2]	// 2 vertices for edge B
	)
	{
		if (	a1[0] ==  b1[0]	&&	a1[1] ==  b1[1]	&&
				a2[0] ==  b2[0]	&&	a2[1] ==  b2[1]
		)
		{
			return 1;
		}

		// check with exchanged vertices for one edge
		if (	a1[0] ==  b2[0]	&&	a1[1] ==  b2[1]	&&
				a2[0] ==  b1[0]	&&	a2[1] ==  b1[1]
		)
		{
			return -1;
		}

		return 0;
	}



	/**
	 * check for adjacent triangles
	 *
	 * the check is based on the vertex coordinates.
	 *
	 * the following conventions exist:
	 *
	 * when rotating the triangle until the hypotenuse is aligned at the x-axis,
	 * the vertex#0 is on the left most corner, vertex#1 on the right most corner
	 * and vertex#2 on the top most corner.
	 *
	 * then, the returned edge index can be interpreted as:
	 * 	0: hypotenuse
	 * 	1: right edge
	 * 	2: left edge
	 *
	 * if the vertices have to be exchanged, the direction of both triangles is equal!!!
	 */
private:
	bool _isAdjacent(	CTriangle_Factory::TTriangleFactoryScalarType i_vertices1[3][2],	//< vertices of the first triangle
			CTriangle_Factory::TTriangleFactoryScalarType i_vertices2[3][2],	//< vertices of the second triangle
			CTriangle_Enums::EEdgeEnum *o_adj_edge_idx1,	///< adjacent edge of first triangle
			CTriangle_Enums::EEdgeEnum *o_adj_edge_idx2,	///< adjacent edge of second triangle
			bool *o_equal_traversal_direction			///< true if traversal direction is equal
	)
	{
		for (int i1 = 0; i1 < 3; i1++)
		{
			for (int i2 = 0; i2 < 3; i2++)
			{
				int d = _isSameEdge(
						i_vertices1[i1], i_vertices1[(i1+1)%3],
						i_vertices2[i2], i_vertices2[(i2+1)%3]
				);
				if (d != 0)
				{
					*o_adj_edge_idx1 = (CTriangle_Enums::EEdgeEnum)i1;
					*o_adj_edge_idx2 = (CTriangle_Enums::EEdgeEnum)i2;
					*o_equal_traversal_direction = (d == -1);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * setup all default values for a triangle factory
	 * these are:
	 * - edge types
	 * - triangle type
	 * - traversal direction
	 * - partition tree node type
	 * - normal
	 * - odd/even value
	 */
private:
	void setup_TriangleFactory(CTriangle_Factory &triangleFactory){
		/*
		 * setup edge types to BORDER when not yet set
		 */
		if (triangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_INVALID)
			triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (triangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_INVALID)
			triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		if (triangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_INVALID)
			triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		if (triangleFactory.triangleType == CTriangle_Enums::TRIANGLE_TYPE_INVALID)
			triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		if (triangleFactory.traversalDirection == CTriangle_Enums::DIRECTION_INVALID)
			triangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;

		if (triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_INVALID)
			triangleFactory.partitionTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;


		/*
		 * setup normal
		 */
		if (triangleFactory.hypNormal == CTriangle_Enums::NORMAL_INVALID)
		{
			// compute the normal direction - x and y are exchanged!
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_y = -(triangleFactory.vertices[1][0] - triangleFactory.vertices[0][0]);
			typename CTriangle_Factory::TTriangleFactoryScalarType hyp_vec_x = triangleFactory.vertices[1][1] - triangleFactory.vertices[0][1];

			triangleFactory.hypNormal = triangleFactory.getTriangleHypNormalIdx(hyp_vec_x, hyp_vec_y);
		}


		/*
		 * setup even/odd value
		 *
		 * normals on the hypotenuse which are aligned at cartesian grids are initialized
		 * to `even`, otherwise `odd`
		 *
		 * this also reduces the amount of the recursive function jump table for the sierpi traversators!!!
		 */
		if (triangleFactory.evenOdd == CTriangle_Enums::EVEN_ODD_INVALID)
		{
			if ((triangleFactory.hypNormal & 1) == 0)
				triangleFactory.evenOdd = CTriangle_Enums::ODD;
			else
				triangleFactory.evenOdd = CTriangle_Enums::EVEN;
		}

#if DEBUG
		/*
		 * check whether the manual evenOdd initialization is valid
		 */
		if (triangleFactory.hypNormal != CTriangle_Enums::NORMAL_INVALID)
		{
			if (triangleFactory.evenOdd == CTriangle_Enums::EVEN)
				assert((triangleFactory.hypNormal & 1) == 1);
			if (triangleFactory.evenOdd == CTriangle_Enums::ODD)
				assert((triangleFactory.hypNormal & 1) == 0);
		}
#endif
	}

	/**
	 * assemble the setup triangle to a triangle grid and setup the simulation partition handlers!
	 *
	 * \return number of triangles in whole grid
	 */
public:
	void setup_AdjacencyInformation()
	{
		for (	auto iter = domainRootTriangles.begin();
				iter != domainRootTriangles.end();
				iter++
		)
		{
			// load currentTriangleWithPartition
			CDomain_BaseTriangle_ &currentTriangleWithPartition = *iter;

			/*
			 * if the vertices are valid, we search for an adjacent triangle for all borders
			 * which are not of type EDGE_TYPE_BOUNDARY
			 */
			for (	auto iter2 = domainRootTriangles.begin();
					iter2 != domainRootTriangles.end();
					iter2++
			)
			{
				CDomain_BaseTriangle_ &adjacentTriangle = *iter2;

				if(&adjacentTriangle == &currentTriangleWithPartition)
					continue;

				/*
				 * search for direct adjacent triangles
				 */
				CTriangle_Enums::EEdgeEnum adjEdgeIdxThisTriangle, adjEdgeIdxAdjacentTriangle;
				bool equal_traversal_direction;

				if (	_isAdjacent(
						currentTriangleWithPartition.triangleFactory.vertices,
						adjacentTriangle.triangleFactory.vertices,
						&adjEdgeIdxThisTriangle,			///< index of edge for this triangle
						&adjEdgeIdxAdjacentTriangle,		///< index of edge for adjacent triangle
						&equal_traversal_direction			///< true, if the triangles have the same traversal direction
				))
				{
					switch(adjEdgeIdxThisTriangle)
					{
					case CTriangle_Enums::EDGE_ENUM_HYP_EDGE:
						if (currentTriangleWithPartition.adjacent_triangles.hyp_edge == nullptr)
						{
							currentTriangleWithPartition.adjacent_triangles.hyp_edge = &adjacentTriangle;
							currentTriangleWithPartition.triangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
						}

						break;

					case CTriangle_Enums::EDGE_ENUM_RIGHT_EDGE:
						if (currentTriangleWithPartition.adjacent_triangles.right_edge == nullptr)
						{
							currentTriangleWithPartition.adjacent_triangles.right_edge = &adjacentTriangle;
							currentTriangleWithPartition.triangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
						}
						break;

					case CTriangle_Enums::EDGE_ENUM_LEFT_EDGE:
						if (currentTriangleWithPartition.adjacent_triangles.left_edge == nullptr)
						{
							currentTriangleWithPartition.adjacent_triangles.left_edge = &adjacentTriangle;
							currentTriangleWithPartition.triangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
						}
						break;
					}
				}
			}
		}
	}

public:


	void splitBaseTriangulation(int depth){

		std::cout << "split base triangle to depth: " << depth << std::endl;

		for (	auto iter = domainRootTriangles.begin();
				iter != domainRootTriangles.end();
				iter++
		){
			CDomain_BaseTriangle_ &triangle = *iter;
			std::cout << "before depth 0: " << std::endl;
			std::cout << "\t triangle: " << &triangle << std::endl;
			std::cout << "\t left: " << triangle.adjacent_triangles.left_edge << std::endl;
			std::cout << "\t right: " << triangle.adjacent_triangles.right_edge << std::endl;
			std::cout << "\t hyp: " << triangle.adjacent_triangles.hyp_edge << std::endl;
			std::cout << "" << std::endl;
		}

		for(int i = 0; i < depth; i++){
			std::list<CDomain_BaseTriangle_ > subDomainRootTriangles;


			for (	auto iter = domainRootTriangles.begin();
					iter != domainRootTriangles.end();
					iter++
			){
				CDomain_BaseTriangle_ &mainTriangle = *iter;

				CTriangle_Factory rightSubTriangleFactory;
				CTriangle_Factory leftSubTriangleFactory;

				// split triangle
				rightSubTriangleFactory.vertices[0][0] = mainTriangle.triangleFactory.vertices[1][0];
				rightSubTriangleFactory.vertices[0][1] = mainTriangle.triangleFactory.vertices[1][1];

				rightSubTriangleFactory.vertices[1][0] = mainTriangle.triangleFactory.vertices[2][0];
				rightSubTriangleFactory.vertices[1][1] = mainTriangle.triangleFactory.vertices[2][1];

				rightSubTriangleFactory.vertices[2][0] = (mainTriangle.triangleFactory.vertices[0][0] + mainTriangle.triangleFactory.vertices[1][0]) / 2.0;
				rightSubTriangleFactory.vertices[2][1] = (mainTriangle.triangleFactory.vertices[0][1] + mainTriangle.triangleFactory.vertices[1][1]) / 2.0;

				leftSubTriangleFactory.vertices[0][0] = mainTriangle.triangleFactory.vertices[2][0];
				leftSubTriangleFactory.vertices[0][1] = mainTriangle.triangleFactory.vertices[2][1];

				leftSubTriangleFactory.vertices[1][0] = mainTriangle.triangleFactory.vertices[0][0];
				leftSubTriangleFactory.vertices[1][1] = mainTriangle.triangleFactory.vertices[0][1];

				leftSubTriangleFactory.vertices[2][0] = (mainTriangle.triangleFactory.vertices[0][0] + mainTriangle.triangleFactory.vertices[1][0]) / 2.0;
				leftSubTriangleFactory.vertices[2][1] = (mainTriangle.triangleFactory.vertices[0][1] + mainTriangle.triangleFactory.vertices[1][1]) / 2.0;

				// setup edge_type, triangle_type, traversal direction,
				// partition tree node type, normal and even/odd
				setup_TriangleFactory(rightSubTriangleFactory);
				setup_TriangleFactory(leftSubTriangleFactory);

				// add children to list
				subDomainRootTriangles.push_back(CDomain_BaseTriangle_(rightSubTriangleFactory));
				CDomain_BaseTriangle_ &rightSubTriangle = subDomainRootTriangles.back();


				subDomainRootTriangles.push_back(CDomain_BaseTriangle_(leftSubTriangleFactory));
				CDomain_BaseTriangle_ &leftSubTriangle = subDomainRootTriangles.back();


				// connect subtriangles with parent
				rightSubTriangle.parent = &mainTriangle;
				rightSubTriangle.leftChild = nullptr;
				rightSubTriangle.rightChild = nullptr;
				leftSubTriangle.parent = &mainTriangle;
				leftSubTriangle.leftChild = nullptr;
				leftSubTriangle.rightChild = nullptr;
				mainTriangle.leftChild = &leftSubTriangle;
				mainTriangle.rightChild = &rightSubTriangle;



				std::cout << "depth: " << i << std::endl;
				std::cout << "\t triangle: " << &mainTriangle << std::endl;
				std::cout << "\t left child: " << mainTriangle.leftChild << std::endl;
				std::cout << "\t right child: " << mainTriangle.rightChild << std::endl;
				std::cout << "\t parent: " << mainTriangle.parent << std::endl;


				std::cout << "" << std::endl;
			}


			for (	auto iter = subDomainRootTriangles.begin();
					iter != subDomainRootTriangles.end();
					iter++
			){

				CDomain_BaseTriangle_ &subTriangle = *iter;

				CDomain_BaseTriangle_ &parent = *(subTriangle.parent);

				// connect with sibling
				if(&subTriangle == parent.leftChild){
					subTriangle.setupLeftEdgeConnection(parent.rightChild);
				}else if(&subTriangle == parent.rightChild){
					subTriangle.setupRightEdgeConnection(parent.leftChild);
				}else{
					std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
				}

				// connect with neighbours
				if(parent.adjacent_triangles.left_edge != nullptr){
					CDomain_BaseTriangle_ &leftNeighbour = *parent.adjacent_triangles.left_edge;
					if(&parent == leftNeighbour.adjacent_triangles.right_edge){
						if(&subTriangle == parent.leftChild){
							subTriangle.setupHypEdgeConnection(leftNeighbour.rightChild);
						}else if(&subTriangle == parent.rightChild){
							// do nothing
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}else if(&parent == leftNeighbour.adjacent_triangles.left_edge){
						if(&subTriangle == parent.leftChild){
							subTriangle.setupHypEdgeConnection(leftNeighbour.leftChild);
						}else if(&subTriangle == parent.rightChild){
							// do nothing
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}
				}

				if(parent.adjacent_triangles.right_edge != nullptr){
					CDomain_BaseTriangle_ &rightNeighbour = *parent.adjacent_triangles.right_edge;
					if(&parent == rightNeighbour.adjacent_triangles.left_edge){
						if(&subTriangle == parent.leftChild){
							// do nothing
						}else if(&subTriangle == parent.rightChild){
							subTriangle.setupHypEdgeConnection(rightNeighbour.leftChild);
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}else if(&parent == rightNeighbour.adjacent_triangles.right_edge){
						if(&subTriangle == parent.leftChild){
							// do nothing
						}else if(&subTriangle == parent.rightChild){
							subTriangle.setupHypEdgeConnection(rightNeighbour.rightChild);
						}else{
							std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
						}
					}
				}

				if(parent.adjacent_triangles.hyp_edge != nullptr){
					CDomain_BaseTriangle_ &hypNeighbour = *parent.adjacent_triangles.hyp_edge;
					if(&subTriangle == parent.leftChild){
						subTriangle.setupRightEdgeConnection(hypNeighbour.rightChild);
					}else if(&subTriangle == parent.rightChild){
						subTriangle.setupLeftEdgeConnection(hypNeighbour.leftChild);
					}else{
						std::cout << "fail: " << &subTriangle << " - " << parent.leftChild << ", " << &parent.rightChild << std::endl;
					}
				}



				std::cout << "after depth: " << i << std::endl;
				std::cout << "\t triangle: " << &subTriangle << std::endl;
				std::cout << "\t left: " << subTriangle.adjacent_triangles.left_edge << std::endl;
				std::cout << "\t right: " << subTriangle.adjacent_triangles.right_edge << std::endl;
				std::cout << "\t hyp: " << subTriangle.adjacent_triangles.hyp_edge << std::endl;
				std::cout << "" << std::endl;
			}

			// reset triangle list
			domainRootTriangles.swap(subDomainRootTriangles);
			subDomainRootTriangles.clear();
		}

	}


};

#endif /* CTRIANGLE_GLUE_H_ */
