/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTraversator_Setup2Pass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */



TRecursiveMethod sfcRecursiveMethod_Forward;
TRecursiveMethod sfcRecursiveMethod_Backward;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory triangleFactory;



/**
 * setup the initial partition traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory
)
{
	triangleFactory = p_triangleFactory;

	/*
	 * search for recursive function id for first pass
	 *
	 * all edges of type OLD  of this triangle have to be set to NEW
	 */
	sfcRecursiveMethod_Forward = getSFCMethod<SFC_METHOD_OLD_TO_NEW_EDGE_TYPES>(triangleFactory);

	/*
	 * search for recursive function id for second pass
	 *
	 * all edges of type NEW  of this triangle have to be set to OLD
	 */

	sfcRecursiveMethod_Backward = getSFCMethod<(SFC_METHOD_NEW_TO_OLD_EDGE_TYPES | SFC_METHOD_FORCE_BACKWARD)>(triangleFactory);
}



/**
 * setup the parameters with the one by the parent partition
 */
void setup_Partition(
		TThisClass &p_parent,			///< information of parent traversator
		CTriangle_Factory &i_triangleFactory	///< triangle factory of this sub-partition
)
{
	// make sure that this is really a root node
	assert(i_triangleFactory.partitionTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	setup_sfcMethods(i_triangleFactory);

	sfcRecursiveMethod_Forward = getSFCMethod<SFC_METHOD_OLD_TO_NEW_EDGE_TYPES>(triangleFactory);
	sfcRecursiveMethod_Backward = getSFCMethod<(SFC_METHOD_NEW_TO_OLD_EDGE_TYPES | SFC_METHOD_FORCE_BACKWARD)>(triangleFactory);

	/**
	 * SETUP KERNEL CLASS
	 */
	cKernelClass.setup_WithKernel(p_parent.cKernelClass);
}
