/*
 * CRusage.hpp
 *
 *  Created on: Jun 23, 2011
 *      Author: schreibm
 */

#ifndef CRUSAGE_HPP_
#define CRUSAGE_HPP_

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <stdio.h>


class CProcessMemoryInformation
{
	typedef unsigned long t_size;

	class CMemoryInformation
	{
public:
		// program size
		t_size size;

        // resident
		t_size resident;

        // shared pages
		t_size share;

        // code
		t_size text;

        // libraries
		t_size lib;

        // data + stack
		t_size data;

	};


	static std::string getStringFromSize(t_size size)
	{
	    char buf[32];

		if (size <= 1024)
		{
		    sprintf(buf, "%lu", size);

			return std::string(buf) + "B";
		}

		if (size <= 1024*1024)
		{
			double ns = size;
			ns /= 1024.0;

			sprintf(buf, "%f", ns);

			return std::string(buf) + "KB";
		}

		if (size <= 1024*1024*1024)
		{
			double ns = size;
			ns /= 1024.0*1024.0;

			sprintf(buf, "%f", ns);

			return std::string(buf) + "MB";
		}


		double ns = size;
		ns /= 1024.0*1024.0*1024.0;

		sprintf(buf, "%f", ns);

		return std::string(buf) + "GB";
	}

public:
	static void outputUsageInformation()
	{
		CMemoryInformation memoryInformation;

        FILE* file = fopen("/proc/self/statm", "r");
        if (file == NULL)
        {
        	std::cout << "ERROR: cannot open statm" << std::endl;
        	return;
        }

        int retval = fscanf(	file,
        		"%lu %lu %lu %lu %lu %lu",
        		&memoryInformation.size, &memoryInformation.resident, &memoryInformation.share, &memoryInformation.text, &memoryInformation.lib, &memoryInformation.data
        	);

        fclose(file);
        if (retval != 6)
        {
        	std::cerr << "Error during reading values from /proc/self/statm" << std::endl;
        	return;
        }

        // determine page size
        long page_size = sysconf(_SC_PAGESIZE);

        std::cout << " assuming page size " << page_size << std::endl;
        std::cout << " + Program size: " << getStringFromSize(memoryInformation.size*page_size) << std::endl;
        std::cout << " +     Resident: " << getStringFromSize(memoryInformation.resident*page_size) << std::endl;
        std::cout << " +       Shared: " << getStringFromSize(memoryInformation.share*page_size) << std::endl;
        std::cout << " +         Text: " << getStringFromSize(memoryInformation.text*page_size) << std::endl;
        std::cout << " +          Lib: " << getStringFromSize(memoryInformation.lib*page_size) << std::endl;
        std::cout << " +         Data: " << getStringFromSize(memoryInformation.data*page_size) << std::endl;
	}
};
#endif /* CRUSAGE_HPP_ */
