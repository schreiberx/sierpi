/*
 * CLittleBigEndian.hpp
 *
 *  Created on: Jun 11, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CLITTLE_BIG_ENDIAN
#define CLITTLE_BIG_ENDIAN

class CLittleBigEndian
{
public:
	template <typename T>
	inline static void swapBytes(T &data)
	{
		char *byte_data = (char*)&data;
		char tmp;

		if (sizeof(T) == 2)
		{
			tmp = byte_data[0];
			byte_data[0] = byte_data[1];
			byte_data[1] = tmp;

			return;
		}

		if (sizeof(T) == 4)
		{
			tmp = byte_data[0];
			byte_data[0] = byte_data[3];
			byte_data[3] = tmp;

			tmp = byte_data[1];
			byte_data[1] = byte_data[2];
			byte_data[2] = tmp;

			return;
		}

		if (sizeof(T) == 8)
		{
			tmp = byte_data[0];
			byte_data[0] = byte_data[7];
			byte_data[7] = tmp;

			tmp = byte_data[1];
			byte_data[1] = byte_data[6];
			byte_data[6] = tmp;

			tmp = byte_data[2];
			byte_data[2] = byte_data[5];
			byte_data[5] = tmp;

			tmp = byte_data[3];
			byte_data[3] = byte_data[4];
			byte_data[4] = tmp;

			return;
		}

		assert(false);
	}
};

#endif
