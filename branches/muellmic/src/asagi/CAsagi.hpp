/*
 * CAsagi.hpp
 *
 *  Created on: Feb 20, 2012
 *      Author: schreibm
 */

#ifndef CASAGI_HPP_
#define CASAGI_HPP_

#define ASAGI_NOMPI
#include "../../MA_rettenb/trunk/include/asagi.h"

#include "../simulations/tsunami_common/types/CTsunamiTypes.hpp"



class CAsagi
{
public:
	double bathymetry_size_x;
	double bathymetry_size_y;

	double bathymetry_min_x;
	double bathymetry_min_y;

	double bathymetry_max_x;
	double bathymetry_max_y;


	double displacements_size_x;
	double displacements_size_y;

	double displacements_min_x;
	double displacements_min_y;

	double displacements_max_x;
	double displacements_max_y;



public:
	/**
	 * singleton for bathymetry datasets
	 */
	static asagi::Grid *singleton_bathymetry;

	/**
	 * singleton for displacements
	 */
	static asagi::Grid *singleton_displacements;

	CAsagi(
			const char *i_filename_bathymetry,	///< filename for bathymetry data
			const char *i_filename_displacements	///< filename for displacement data
		);

	virtual ~CAsagi();
};


extern CAsagi *cAsagi;

CTsunamiSimulationTypes::TVertexScalar getAsagiElementBathymetryMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_depth
	);


CTsunamiSimulationTypes::TVertexScalar getAsagiElementDisplacementMethod(
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_mx,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_my,
		CTsunamiSimulationTypes::TVisualizationVertexScalar i_depth
	);

#endif /* CASAGI_HPP_ */
