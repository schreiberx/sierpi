#! /bin/bash

. ../tools.sh

#SPLIT_SIZES=`seqPow2 64 $((32*1024))`
SPLIT_SIZES="128 512 2048 4096"

INITIAL_DEPTH=12

PARAMS="-d $INITIAL_DEPTH -a 6"

echo "CPUS: $CPULIST"
echo "SPLIT SIZES: $SPLIT_SIZES"

for s in $SPLIT_SIZES; do
	echo "Split Size: $s"

	for n in $CPULIST; do
		THREADS=$((n+1))
		PARAMS_="-o $s -n $THREADS $PARAMS $@"

		EXEC_CMD="$EXEC $PARAMS_"
		echo "$EXEC_CMD" 1>&2
		OUTPUT=`$EXEC_CMD`
		VARNAME=MTPS$THREADS
		eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

		MTPS=$(eval echo \$$VARNAME)
		SPEEDUP=$(echo "scale=4; $MTPS/$MTPS1" | bc)
		SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

		OUTPUT_LINE="$THREADS	$MTPS	$SPEEDUP	$SPEEDUP_PER"

		echo "$OUTPUT_LINE"
	done
done
