#! /bin/bash


MODE="ipmo"
[ "$1" != "" ] && MODE="$1"

PROBLEMS=1
[ "$2" != "" ] && PROBLEMS="$2"

PARCOUNT=5
[ "$3" != "" ] && PARCOUNT="$3"



echo "MODE: $MODE"
echo "PARCOUNT: $PARCOUNT"
echo "PROBLEMS: $PROBLEMS"


. ../tools.sh


# adaptive splitting size
PARAMS=" -u 10 -U 10"
PARAMS+=" -T 1000"

# be verbose
#PARAMS+=" -v 5"

if [ "`uname -n`" == "e7mp" ]; then

	echo "Running on INWEST"

	export KMP_AFFINITY="scatter"
	PARAMS+=" -n 40"

fi





if 	[ "${MODE:0:4}" == "omp_" ]; then

	echo "Using OMP"

	EXEC=../../build/sierpi_intel_omp_tsunami_parallel_release
else

	echo "Using iPMO"

	EXEC=../../build/sierpi_intel_ipmo_tsunami_parallel_release
fi


if [ $PROBLEMS -eq 1 ]; then

	PROG0="$EXEC $PARAMS -d 12"
	PROG1="$EXEC $PARAMS -d 12"
	PROG2="$EXEC $PARAMS -d 10"
	PROG3="$EXEC $PARAMS -d 12"
	PROG4="$EXEC $PARAMS -d 10"

elif [ $PROBLEMS -eq 2 ]; then

	PROG0="$EXEC $PARAMS -d 11"
	PROG1="$EXEC $PARAMS -d 11"
	PROG2="$EXEC $PARAMS -d 11"
	PROG3="$EXEC $PARAMS -d 10"
	PROG4="$EXEC $PARAMS -d 10"

elif [ $PROBLEMS -eq 3 ]; then

	PROG0="$EXEC $PARAMS -d 8"
	PROG1="$EXEC $PARAMS -d 8"
	PROG2="$EXEC $PARAMS -d 8"
	PROG3="$EXEC $PARAMS -d 8"
	PROG4="$EXEC $PARAMS -d 6"

elif [ $PROBLEMS -eq 4 ]; then

	PROG0="$EXEC $PARAMS -d 18"
	PROG1="$EXEC $PARAMS -d 17"
	PROG2="$EXEC $PARAMS -d 17"
	PROG3="$EXEC $PARAMS -d 17"
	PROG4="$EXEC $PARAMS -d 17"
fi


echo ""
echo "Programs:"
echo " + $PROG0"
echo " + $PROG1"
echo " + $PROG2"
echo " + $PROG3"
echo " + $PROG4"
echo ""


#
# START
#

date
STARTSECONDS=`date +%s`

if [ "$MODE" == "ipmo" ]; then

	echo "Parallel execution - time interleaved"
	$PROG0 &
	sleep 2
	$PROG1 &
	sleep 2
	$PROG2 &
	sleep 4
	$PROG3 &
	sleep 2
	$PROG4 &


elif [ "$MODE" == "ipmo_t0" ]; then

	echo "iPMO: Parallel execution - starting all simulations at t=0"
	$PROG0 &
	$PROG1 &
	$PROG2 &
	$PROG3 &
	$PROG4 &

elif [ "$MODE" == "ipmo_sequential" ]; then

	echo "iPMO: Sequential execution"
	$PROG0
	$PROG1
	$PROG2
	$PROG3
	$PROG4

elif [ "$MODE" == "omp_sequential" ]; then

	echo "OMP: Sequential execution"
	$PROG0
	$PROG1
	$PROG2
	$PROG3
	$PROG4

elif [ "$MODE" == "ipmo_prog0" ]; then

	echo "iPMO: Prog0 parallel execution with ipmo ($PARCOUNT times)"
	for i in `seq 1 $PARCOUNT`; do
		$PROG0 &
	done

elif [ "$MODE" == "omp_t0" ]; then

	echo "OMP: Parallel execution with omp"
	$PROG0 &
	$PROG1 &
	$PROG2 &
	$PROG3 &
	$PROG4 &

elif [ "$MODE" == "omp_sequential" ]; then

	echo "OMP: Sequential execution with omp"
	$PROG0
	$PROG1
	$PROG2
	$PROG3
	$PROG4

elif [ "$MODE" == "omp_prog0_sequential" ]; then

	echo "OMP: Prog0 sequential execution ($PARCOUNT times)"
	for i in `seq 1 $PARCOUNT`; do
		$PROG0
	done

elif [ "$MODE" == "omp_prog0" ]; then

	echo "OMP: Prog0 parallel execution ($PARCOUNT times)"
	for i in `seq 1 $PARCOUNT`; do
		$PROG0 &
	done

elif [ "$MODE" == "omp_prog0_single_core" ]; then

	echo "OMP: Prog0 parallel execution ($PARCOUNT times)"
	for i in `seq 1 $PARCOUNT`; do
		$PROG0 -n 1 &
	done


else
	echo "Unknown mode '$MODE'"
	exit -1
fi



for job in `jobs -p`; do
	echo "Waiting for job $job"
	wait $job
done


date

ENDSECONDS=`date +%s`
echo "Seconds: $((ENDSECONDS-STARTSECONDS))"

