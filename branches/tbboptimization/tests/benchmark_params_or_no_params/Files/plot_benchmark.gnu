reset

set terminal svg size 700, 350
set output "benchmark_traversators.svg"

set datafile missing "-"
set xtics nomirror rotate by -45

set xlabel	"Domain size"
set ylabel	"Time (s)"


set style data linespoints
set key autotitle columnhead

plot 'output.dat' using 2:xtic(1),\
	'' using 3


set log y
set output "benchmark_traversators_log.svg"
plot 'output.dat' using 2:xtic(1),\
	'' using 3


