

#include "kernels.h"

#include <mmintrin.h>
#include <xmmintrin.h>
#include <string.h>

void memCpyBlock(float *__restrict__ dst, float *__restrict__ src, size_t size)
{
	memcpy(dst, src, size);
}


void typeFloatMulBlock(float *__restrict__ dst, float *__restrict__ src, size_t size)
{
	int p_size = size/4;
	int i;
	for (i = 0; i < p_size; i++)
		dst[i] = src[i]*0.5;
}


void typeFloatBlock(float *__restrict__ dst, float *__restrict__ src, size_t size)
{
	int p_size = size/4;
	int i;
	for (i = 0; i < p_size; i++)
		dst[i] = src[i];
}


void typeVec4SIMDBlock(float *__restrict__ dst, float *__restrict__ src, size_t size)
{
	int p_size = size/4;
	int i;
	for (i = 0; i < p_size; i+=4)
	{
		_mm_store_si128((__m128i*)&dst[i], _mm_load_si128((const __m128i*)&src[i]));
	}
}


void floatBlockAsInt4(float *__restrict__ dst, float *__restrict__ src, size_t size)
{
	int *idst = (int*)dst;
	int *isrc = (int*)src;

	int p_size = size/4;
	int i;
	for (i = 0; i < p_size; i+=4)
	{
		idst[i+0] = isrc[i+0];
		idst[i+1] = isrc[i+1];
		idst[i+2] = isrc[i+2];
		idst[i+3] = isrc[i+3];
	}
}
