/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * config.h
 *
 * Precompiler settings for compilation
 *
 *  Created on: Sep 27, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */



/**
 * switch between different simulations
 */
#if SIMULATION_DEFINED != 1
	#if 1
		#define SIMULATION_TSUNAMI_SERIAL								0
		#define SIMULATION_TSUNAMI_SERIAL_REGULAR						0
		#define SIMULATION_TSUNAMI_PARALLEL								1
	#elif 1
		#define SIMULATION_TSUNAMI_SERIAL								0
		#define SIMULATION_TSUNAMI_SERIAL_REGULAR						1
		#define SIMULATION_TSUNAMI_PARALLEL								0
	#else
		#define SIMULATION_TSUNAMI_SERIAL								1
		#define SIMULATION_TSUNAMI_SERIAL_REGULAR						0
		#define SIMULATION_TSUNAMI_PARALLEL								0
	#endif
#endif



/**
 * Resize stacks when the element data exceeds the stack size due to
 * adaptivity.
 *
 * The ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING gives the approximated
 * number of bytes to additionally extend the stack to avoid frequent
 * malloc/free operators.
 *
 * The ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING gives the approximated
 * number of bytes which the new max element data stack counter has to be fall
 * below the maximum to allow a shrinking of the stack.
 */
#ifndef ADAPTIVE_SUBPARTITION_STACKS
	#define ADAPTIVE_SUBPARTITION_STACKS 1
#endif

#if ADAPTIVE_SUBPARTITION_STACKS
	#ifndef ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING
		#define ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING	(128*8)
	#endif

	#define ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING_ELEMENTS(SizeOfElement)	(ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING / SizeOfElement)

	#ifndef ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING
		// subtract a little offset (16) to avoid flickering
		#define ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING	(128*8-16)
	#endif

	#define ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING_ELEMENTS(SizeOfElement)	((ADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING / SizeOfElement) + 32)
#endif



/**
 * during adaptive traversals a conforming grid should be gained.
 *
 * for sub-partitions already being in a conforming state and without any requests
 * from adjacent edge communication to refine, the adaptive traversal of such a
 * sub-partition can be skipped.
 */
#ifndef ADAPTIVE_CONFORMING_SUBPARTITION_SKIPPING_ACTIVE
	#define ADAPTIVE_CONFORMING_SUBPARTITION_SKIPPING_ACTIVE 1
#endif



/**
 * avoid last adaptive traversal if no adaptive refinements have to be executed
 */
#ifndef ADAPTIVE_SUBPARTITION_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED
	#define ADAPTIVE_SUBPARTITION_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED 0
#endif


/**
 * default floating point precision
 */
#ifdef DEFAULT_FLOATING_POINT_PRECISION_SINGLE
	#define DEFAULT_FLOATING_POINT_PRECISION	float
#else
	#ifdef DEFAULT_FLOATING_POINT_PRECISION_DOUBLE
		#define DEFAULT_FLOATING_POINT_PRECISION	double
	#else
		#define DEFAULT_FLOATING_POINT_PRECISION	float
	#endif
#endif



/**
 * scalar type used in the triangle factory describing in which format the 3 triangle vertices are stored
 */
#define TRIANGLE_FACTORY_SCALAR_TYPE DEFAULT_FLOATING_POINT_PRECISION



/**
 * Enable validations if debug mode is turned on
 */
#if DEBUG
	#define	COMPILE_WITH_VALIDATIONS	1
#else
	#define	COMPILE_WITH_VALIDATIONS	0
#endif



/**
 * Enable compilation with vertex data communication information
 */
#if 0
	#define COMPILE_WITH_PARALLEL_VERTEX_COMM 1
#else
	#define COMPILE_WITH_PARALLEL_VERTEX_COMM 0
#endif



/**
 * Enable gui by default
 */
#ifndef COMPILE_SIMULATION_WITH_GUI
	#define COMPILE_SIMULATION_WITH_GUI	1
#endif



/**
 * Reuse stacks from parent sub-partition
 */
#if 1
	#define CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS	0
#else
	#define CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS	1
#endif



#if ADAPTIVE_SUBPARTITION_STACKS && CPARTITION_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
	#error "adaptive sub-partition stacks in combination with parent's stack reusage not implemented yet!"
#endif



/**
 * Enable/disable compilation with validation stacks and validation tests
 */
#if DEBUG && COMPILE_WITH_VALIDATIONS
	#define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		1
#else
	#define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		0
#endif



/**
 * Enable/disable deletion of CPartition classes due to split operation
 *
 * The CPartition can be reused for a child node (TODO)
 */
#if 1
	#define DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION	1
#else
	#define DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION	0
#endif



/**************************************
 * OPENMP SPECIFIC
 **************************************/
/**
 * when compiled with omp, use this extra clause appended to #pragma omp task
 */


// untie tasks to be executed on other cpu after being idle and thus interrupted
//#define OPENMP_EXTRA_TASK_CLAUSE
#define OPENMP_EXTRA_TASK_CLAUSE	untied

/**
 * should a task be created for the second leaf of the generic tree or should
 * the computation be done by the currently running task
 *
 * default: deactivated since this boosts the performance. however, this may
 * change in the future!
 */
#define OPENMP_CREATE_TASK_FOR_SECOND_LEAF	0


/**
 * allow periodic boundaries?
 * in this case, the validation for the edge comm normals and vertices cannot be applied anymore!
 */
#ifndef CONFIG_PERIODIC_BOUNDARIES
	#define CONFIG_PERIODIC_BOUNDARIES	1
#endif

/**
 * use task priorities (only available via TBB)
 *
 * when activated, sub-partitions with larger amount of triagles get a higher priority.
 */

// TODO: not yet implemented
//#define USE_TASK_PRIORITIES	1
#define USE_TASK_PRIORITIES	0


/**
 * ASAGI
 */
#ifndef USE_ASAGI
	#define USE_ASAGI	1
#endif


/**
 * create code with normals as integer parameters for traversators
 *
 * see also configuration option config.internal_traversator_normal_as_int_parameter
 */
#define CONFIG_TRAVERSATORS_WITH_NORMALS_AS_INTS	1

/**
 * enable/disable compilation with tests for tsunami vertex coordinates
 */
#if DEBUG && COMPILE_WITH_VALIDATIONS
	#define COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION		1
#else
	#define COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION		0
#endif
