/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CGenericTreeNode.hpp
 *
 *  Created on: Jun 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 *
 */


#ifndef CGENERICTREENODE_HPP_
#define CGENERICTREENODE_HPP_

#include "config.h"

/*
 * we have to include the header files here to stay outside the class scope
 */
#if COMPILE_WITH_TBB
	#include <tbb/task.h>
	#include <tbb/tbb_thread.h>
#endif

#if COMPILE_WITH_TBBX
	#include <tbb/task.h>
	#include <tbb/tbb_thread.h>
	#include <tbb/recursive_mutex.h>
#endif

#if COMPILE_WITH_OMP
	#include <omp.h>
#endif



/**
 * \brief the generic tree node is used to allow tree traversals using a unified tree node system.
 *
 * in this way, the recursive tree traversals don't have to distinguish between domain triangulation nodes
 * and sub-trees of the root domain partition triangles.
 */
template <typename CPartition_TreeNode>
class CGenericTreeNode
{
	typedef CGenericTreeNode<CPartition_TreeNode> CGenericTreeNode_;
public:
	/**
	 * pointer to parent node
	 */
	CGenericTreeNode *parent_node;

	/**
	 * first and second child node
	 */
	CGenericTreeNode *first_child_node, *second_child_node;

	/**
	 * set this node to be a new leaf node during the next traversal
	 */
	bool delayed_child_deletion;


public:
	/**
	 * pointer to tree node (avoids casting for cleaner programming, a static_case can also be used).
	 */
	CPartition_TreeNode *cPartition_TreeNode;

	/**
	 * this variable is set to true when this tree node is one of the nodes for the base triangulation.
	 */
	bool base_triangulation_node;

#if USE_TASK_PRIORITIES
	int task_priority_hint;
#endif


	/*
	 * constructor
	 */
	CGenericTreeNode(
			CGenericTreeNode *i_parent_node,	///< parent generic tree node
			bool i_base_triangulation_node
	)	:
		parent_node(i_parent_node),
		first_child_node(nullptr),
		second_child_node(nullptr),
		delayed_child_deletion(false),
		cPartition_TreeNode(nullptr),
		base_triangulation_node(i_base_triangulation_node)
#if USE_TASK_PRIORITIES
		,
		task_priority_hint(-1)
#endif
	{
	}

	/**
	 * testers whether a link is valid
	 */
	inline bool isParentNodeValid()
	{
		if (parent_node == nullptr)
			return false;

		if (parent_node->cPartition_TreeNode == nullptr)
			return false;

		return true;
	}


	inline bool isFirstChildNodeValid()
	{
		if (first_child_node == nullptr)
			return false;

		if (first_child_node->cPartition_TreeNode == nullptr)
			return false;

		return true;
	}

	inline bool isSecondChildNodeValid()
	{
		if (second_child_node == nullptr)
			return false;

		if (second_child_node->cPartition_TreeNode == nullptr)
			return false;

		return true;
	}



public:
	/**
	 * mark the childs to be ready for deletion during the next traversal.
	 *
	 * this is necessary to have joined childs still accessible by adjacent partitions until
	 * the updates of the edge comm information is finished.
	 */
	void setDelayedChildDeletion()
	{
		delayed_child_deletion = true;
	}

	void unsetDelayedChildDeletion()
	{
		delayed_child_deletion = false;
	}



	/**
	 * deconstructor which deletes the childs which are only
	 */
	~CGenericTreeNode()
	{
		freeChilds();

		if (cPartition_TreeNode)
			delete cPartition_TreeNode;
	}



	/**
	 * free generic child tree nodes as well as specialized nodes if they exist
	 *
	 * the specialized nodes may not care about deleting their childs!
	 */
	void freeChilds()
	{
		if (first_child_node != nullptr)
		{
			delete first_child_node;
			first_child_node = nullptr;
		}

		if (second_child_node != nullptr)
		{
			delete second_child_node;
			second_child_node = nullptr;
		}

		delayed_child_deletion = false;
	}



	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool isLeaf()	const
	{
		if (delayed_child_deletion)
			return true;

		return (first_child_node == nullptr) && (second_child_node == nullptr);
	}



	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool hasLeaves()	const
	{
		if (delayed_child_deletion)
			return false;

		return (first_child_node != nullptr) || (second_child_node != nullptr);
	}


#if COMPILE_WITH_TBB
	#include "CGenericTreeNode_tbb.hpp"
#endif

#if COMPILE_WITH_TBBX
	#include "CGenericTreeNode_tbbx.hpp"
#endif

#if COMPILE_WITH_OMP
	#include "CGenericTreeNode_omp.hpp"
#endif

#if (!COMPILE_WITH_TBB) && (!COMPILE_WITH_OMP) && (!COMPILE_WITH_TBBX)
	#include "CGenericTreeNode_serial.hpp"
#endif

};

#endif
