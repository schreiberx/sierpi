/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelEdgeCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_

#include "libsierpi/parallelization/generic_tree/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelEdgeCommTraversals
{
public:
	/**
	 * run the edge comm traversals
	 */
	template<
		typename CPartition_,			/// partition type offering necessary structures, edge comm data, etc.
		typename CSimulation_SubPartitionHandler,	/// type of user-defined subpartition handler
		typename TEdgeCommTraversator,	/// Traversator including kernel
		typename TEdgeData,				/// type of edge communication data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename TTimestepSize,			/// size of time-step to set-up
		typename TReduceValue			/// value to use for reduction
	>
	static void action(
			TEdgeCommTraversator CSimulation_SubPartitionHandler::*i_simulationSubClass,
			CPartition_ExchangeEdgeCommData<CPartition_, TEdgeData, CStackAccessors> CSimulation_SubPartitionHandler::*i_simulationEdgeCommSubClass,
			CGenericTreeNode<CPartition_> *i_node,
			TTimestepSize i_timestep_size,
			TReduceValue *o_cfl_reduce_value
	)
	{
		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_node->traverse_PartitionTreeNode_Parallel(
				[=](CPartition_ *i_node)
				{
					(i_node->cSimulation_SubPartitionHandler->*(i_simulationSubClass)).actionFirstPass(i_node->cStacks);
				}
		);

		// second pass: setting all edges to type 'old' reads data from adjacent partitions
		*o_cfl_reduce_value = i_node->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_ *i_node) -> TReduceValue
				{
					// firstly, pull edge communication data from adjacent sub-partitions
					(i_node->cSimulation_SubPartitionHandler->*(i_simulationEdgeCommSubClass)).pullEdgeCommData();

					// run computation based on newly set-up stacks
					return (i_node->cSimulation_SubPartitionHandler->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, i_timestep_size);
				},
				&(CReduceOperators::MIN<TReduceValue>	// use minimum since the minimum timestep has to be selected
			)
		);

		i_node->i_simulationSubClass->cfl_domain_size_div_max_wave_speed_after_edge_comm = *o_cfl_reduce_value;
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
