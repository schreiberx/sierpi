#! /bin/bash

. ../tools.sh
. ../inc_environment_vars.sh

CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`

for i in `seq 1 $CPUS`; do
	CPULIST+=" $i"
done

PROBLEM_SIZES_IN_DEPTH=`seq 8 1 20`

EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"

# no adaptive refinement
#PARAMS="-a 0"
PARAMS="-u 10 -U 5 -h 10"


echo "CPUS: $CPULIST"

PROCLIST=$(echo $CPULIST | sed "y/ /,/")

echo "Triangle statistics"


for p in $PROBLEM_SIZES_IN_DEPTH; do
	THREADS=$CPUS

	PARAMS_=" -d $p -n $THREADS $PARAMS $@"

	EXEC_CMD="$EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	MTPS=`getValueFromString "$OUTPUT" "MTPS"`

	TRIANGLE_LIST=`echo -n "$OUTPUT" | grep  "	TRIANGLES" | sed "s/	TRIANGLES.*//" | sed "s/.*+ //"`

	AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
	TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
	TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
	SIMULATION_TIME=`getValueFromString "$OUTPUT" "ST"`

	echo "===================================="
	echo "Threads: $THREADS"
	echo "MTPS: $MTPS"
	echo "Initial Depth: $p"
	echo "Average Triangles: $AVERAGE_TRIANGLES"
	echo "Timesteps: $TIMESTEPS"
	echo "Simulation time (simSeconds): $SIMULATION_TIME"
	echo "Triangles:"
	for i in $TRIANGLE_LIST; do
		echo -en "$i\t"
	done
	echo
	echo
done
