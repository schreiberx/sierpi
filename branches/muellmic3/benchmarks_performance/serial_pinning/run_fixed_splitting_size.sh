#! /bin/bash

. ../tools.sh

#
# scons --tsunami-serial-pinning=on --simulation=tsunami_serial --hyperbolic-flux-solver=1
#

EXEC=../../build/sierpi_gnu_tsunami_serial_release
#EXEC=../../build/sierpi_gnu_tsunami_serial_debug

START_DEPTH=12

echo "Problem size with different number of CPUs using function optimization"

echo "THREADS	MTPS"
for PINNING_CORE in $CPULIST; do
	PARAMS_=" -d $START_DEPTH -P $PINNING_CORE $@"

	EXEC_CMD="$EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	VARNAME=MTPS$THREADS
	eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

	MTPS=$(eval echo "\$$VARNAME")

	echo "$THREADS	$DEPTH	$MTPS"
done
