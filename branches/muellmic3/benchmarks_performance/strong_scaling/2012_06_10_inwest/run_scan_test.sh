#! /bin/bash


DATE=2012_06_09


EXECS="../../build/sierpi_gnu_omp_scan_tsunami_parallel_release ../../build/sierpi_gnu_tbb_scan_tsunami_parallel_release"

for e in $EXECS; do
	FILENAME=`echo "$e" | sed "s/.*\///"`
	for o in 64 128 256 512 $((1*1024)) $((2*1024)) $((4*1024)) $((8*1024)) $((16*1024)) $((32*1024)); do
		for l in 64 128 256 512 $((1*1024)) $((2*1024)) $((4*1024)) $((8*1024)) $((16*1024)) $((32*1024)); do
			E="$e -d 20 -a 8 -t 100 -A 1 -n 40 -o $o -l $l"
			echo $E
			$E > "output_scan_test_"$DATE"_"$FILENAME"_a8_A1_d20_o"$o"_t100_n40_80to1.txt"
		done
	done
done
