#! /bin/bash

IPMO_VERSION="ipmo_2012_09_26"

THREADS="omp"
ITHREADS="ipmo"
ITHREADS_ASYNC="ipmo_async"

EXEC_THREADS="../../build/sierpi_intel_""$THREADS""_hyperbolic_parallel_tsunami_b0_release"
EXEC_ITHREADS="../../build/sierpi_intel_""$ITHREADS""_hyperbolic_parallel_tsunami_b0_release"
EXEC_ITHREADS_ASYNC="../../build/sierpi_intel_""$ITHREADS_ASYNC""_hyperbolic_parallel_tsunami_b0_release"

IPMO_PARAMS=" -n 40"
IPMO_PARAMS="$IPMO_PARAMS -v -99"

killall -w ${EXEC_THREADS/..\/..\/..\/build\//} 2>/dev/null
killall -w ${EXEC_ITHREADS/..\/..\/..\/build\//} 2>/dev/null
killall -w ${EXEC_ITHREADS_ASYNC/..\/..\/..\/build\//} 2>/dev/null

EXEC_IPMO="../../../../$IPMO_VERSION/build/server_ipmo_release"

#PARAMS=" -d 4 -o 8192 -B 1 -a 16 -v -99 -G 1"
PARAMS=" -d 4 -o 8192 -B 1 -a 16 -v 0 -G 1"


BENCHMARK_DEPTH_PATTERN=(5 6 5 6 5)
BENCHMARK_REFINE_PATTERN=(14 15 14 15 15)
BENCHMARK_SLEEP_PATTERN=(5 10 10 5 10)
BENCHMARK_LOOP_COUNTER="3"

#BENCHMARK_DEPTH_PATTERN=(5 6)
#BENCHMARK_REFINE_PATTERN=(12 10)
#BENCHMARK_SLEEP_PATTERN=(5 5)
#BENCHMARK_LOOP_COUNTER="2"

BENCHMARK_PATTERN_COUNTER=${#BENCHMARK_DEPTH_PATTERN[*]}
