/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION_CUSTOM_MATRICES_HPP_
#define CCOMPUTATION_CUSTOM_MATRICES_HPP_

#include <string.h>


#if SIMULATION_HYPERBOLIC_CONST_MATRICES_ID == 0

#	include "matrices_const_0/CComputation_Matrices_Const.hpp"

#elif SIMULATION_HYPERBOLIC_CONST_MATRICES_ID == 1

#	include "matrices_const_1/CComputation_Matrices_Const.hpp"

#else

#	error "unknown matrices id"

#endif


#endif /* CCOMPUTATION_CUSTOM_MATRICES_HPP_ */
