/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUPTSUNAMI_DOFS_NODAL_HPP_
#define CSETUPTSUNAMI_DOFS_NODAL_HPP_


#include "../../CDatasets.hpp"
#include "../../types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"

class CSetupTsunamiDOFs
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:
	static inline void setup(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiSimulationCellData *io_cCellData,

			CDatasets *cDatasets
	)	{

		T lod = sierpi::CTriangle_Tools::getLODFromDepth(i_depth);


		/**
		 * TODO: setup whole spectrum
		 */
		T world_coords[2];
		sierpi::CTriangle_PointProjections::referenceToWorld(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				(T)(1.0/3.0),		(T)(1.0/3.0),

				&world_coords[0],	&world_coords[1]
			);

		CTsunamiSimulationNodeData n;

		// initialize in case that getNodalData only updates the values
		n.h = io_cCellData->dofs.h[0];
		n.hu = io_cCellData->dofs.hu[0];
		n.hv = io_cCellData->dofs.hv[0];
		n.b = io_cCellData->dofs.b[0];

		cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

		io_cCellData->dofs.h[0] = n.h;
		io_cCellData->dofs.hu[0] = n.hu;
		io_cCellData->dofs.hv[0] = n.hv;
		io_cCellData->dofs.b[0] = n.b;

		if (io_cCellData->dofs.h[0] < 0)
		{
			assert(false);
			throw("negative depth detected");
		}

		if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
		{
			//momentum was updated -> project to reference space
			CTriangle_VectorProjections::worldToReference(
					&io_cCellData->dofs.hu[0],
					&io_cCellData->dofs.hv[0],
					-i_right_normal_x,
					-i_right_normal_y
				);
		}

		// set other spectras to 0
		for (int i = 1; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
#if 0
			io_cCellData->dofs.h[i] = 0;
			io_cCellData->dofs.hu[i] = 0;
			io_cCellData->dofs.hv[i] = 0;
			io_cCellData->dofs.b[i] = 0;
#else
			io_cCellData->dofs.h[i] = io_cCellData->dofs.h[0];
			io_cCellData->dofs.hu[i] = io_cCellData->dofs.hu[0];
			io_cCellData->dofs.hv[i] = io_cCellData->dofs.hv[0];
			io_cCellData->dofs.b[i] = io_cCellData->dofs.b[0];
#endif
		}
	}
};

#endif /* CSETUPTSUNAMI_DOFS_NODAL_HPP_ */
