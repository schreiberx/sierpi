/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 31. October 2012
 *      Author: Michael Müller <muellmic@in.tum.de>
 */

#ifndef CSPHEREPROJECTIONS_HPP_
#define CSPHEREPROJECTIONS_HPP_

#include "types/CTypes.hpp"
#include "CDatasets.hpp"
/**
 * This class contains helper methods to project
 * 2D world coordinates to 3D coordinates on
 * a unit sphere.
 */
class CCube_To_Sphere_Projection{
	typedef CHyperbolicTypes::CSimulationTypes::T T;
public:
	/**
	 * coordinate system:
	 *
	 *
	 *     z ^
	 *       |
	 *       |
	 *       |  /
	 *       | /
	 *       |/
	 *  ----   ---------->
	 *      /| 			y
	 *     / |
	 *    /  |
	 *   /
	 *  v x
	 */
	template <typename T>
	inline static void projectToSphere(
			CTsunamiSimulationCellData::FaceType face,
			T *vertex_x, 	// face based x-coordinate
			T *vertex_y, 	// face based y-coordinate,
			T *sphere_x,	// 3D x-coordinate on sphere
			T *sphere_y,	// 3D y-coordinate on sphere
			T *sphere_z		// 3D z-coordinate on sphere
	)
	{

		T x,y,z;


		switch(face){
		case CTsunamiSimulationCellData::left:
			x = *vertex_x;
			y = -1.0;
			z = *vertex_y;
			break;

		case CTsunamiSimulationCellData::front:
			x = 1.0;
			y = *vertex_x;
			z = *vertex_y;
			break;

		case CTsunamiSimulationCellData::right:
			x = *vertex_x;
			y = 1.0;
			z = *vertex_y;
			break;

		case CTsunamiSimulationCellData::back:
			x = -1.0;
			y = *vertex_x;
			z = *vertex_y;
			break;

		case CTsunamiSimulationCellData::top:
			x = *vertex_y;
			y = *vertex_x;
			z = 1.0;
			break;

		case CTsunamiSimulationCellData::bottom:
			x = *vertex_y;
			y = *vertex_x;
			z = -1.0;
			break;

		default:
			x=y=z=-1;
			break;
		}

		*sphere_x = x * sqrt(1.0 - y * y / 2.0 - z * z / 2.0 + y * y * z * z / 3.0);
		*sphere_y = y * sqrt(1.0 - x * x / 2.0 - z * z / 2.0 + x * x * z * z / 3.0);
		*sphere_z = z * sqrt(1.0 - x * x / 2.0 - y * y / 2.0 + x * x * y * y / 3.0);


	}
};

#endif
