/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUP_CELLDATA_VALIDATION_HPP_
#define CSETUP_CELLDATA_VALIDATION_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData_Depth.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * setup the validation data included in the CTsunamiSimulationCellData to the according
 * vertex coordinates.
 */
class CSetup_CellData_Validation	:
	public CHyperbolicTypes::CSimulationTypes
{
public:
	typedef CHyperbolicTypes::CSimulationTypes::T TVertexScalar;
	typedef sierpi::travs::CTraversator_VertexCoords_CellData_Depth<CSetup_CellData_Validation, CHyperbolicTypes> TRAV;

public:

	inline CSetup_CellData_Validation()
	{
	}

	virtual inline ~CSetup_CellData_Validation()
	{
	}


	inline void op_cell(
			T vx1, T vy1,
			T vx2, T vy2,
			T vx3, T vy3,
			int i_depth,
			CHyperbolicTypes::CSimulationTypes::CCellData *io_cCellData
		)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		io_cCellData->validation.set(vx1, vy1, vx2, vy2, vx3, vy3, i_depth);
#endif
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}


	inline void setup_WithKernel(
			CSetup_CellData_Validation &parent
	)
	{
	}

};

}
}

#endif
