/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Sep 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CKERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_
#define CKERNEL_SETUP_COLUMN_0TH_ORDER_TSUNAMI_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../../../subsimulation_generic/CDatasets.hpp"

#include "../common/CAdaptivity_0thOrder.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for simulation of 1st order
 */
class CSetup_Column_0thOrder	:
		public CAdaptivity_0thOrder
{
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData<CSetup_Column_0thOrder, CHyperbolicTypes>	TRAV;

	typedef CHyperbolicTypes::CSimulationTypes::T T;
	typedef T TVertexScalar;

	typedef typename CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	/*
	 * refinement/coarsening parameters
	 */
	T refine_threshold;
	T coarsen_threshold;

	// center of column
	T columnCenterX, columnCenterY;

	// radius of column
	T columnRadius;
	T columnRadiusSquared;


	CSetup_Column_0thOrder()	:
		columnCenterX(0),
		columnCenterY(0),
		columnRadius(0)
	{
	}



	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}


private:
	inline bool insideColumn(
			TVertexScalar px, TVertexScalar py
	)
	{
		TVertexScalar x = columnCenterX-px;
		TVertexScalar y = columnCenterY-py;

		return x*x+y*y < columnRadiusSquared;
	}



public:
	inline bool should_refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CHyperbolicTypes::CSimulationTypes::CCellData *o_cCellData
	)
	{
		/*
		 * check whether the line on the circle intersects with the triangle
		 */
		int counter = 0;
		counter += (int)insideColumn(i_vertex_left_x, i_vertex_left_y);
		counter += (int)insideColumn(i_vertex_right_x, i_vertex_right_y);
		counter += (int)insideColumn(i_vertex_top_x, i_vertex_top_y);

		if (counter == 0)
		{
			/*
			 * completely outside circle
			 */
			// nothing to to & no refinement request
			return false;
		}

		// ALWAYS REFINE WHEN INSIDE COLUMN!
		return true;
/*
		// circle intersects triangle
		if (counter == 1 || counter == 2)
			return true;

		// nothing to do & no refinement request
		return false;
*/
	}


	inline bool should_coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CHyperbolicTypes::CSimulationTypes::CCellData *element
	)
	{
		return false;
	}


	void setup_Parameters(
			T i_columnCenterX,
			T i_columnCenterY,
			T i_columnRadius,

			int i_methodId,
			CDatasets *i_cDatasets
	)
	{
		columnCenterX = i_columnCenterX;
		columnCenterY = i_columnCenterY;
		columnRadius = i_columnRadius;
		columnRadiusSquared = columnRadius*columnRadius;

		cDatasets = i_cDatasets;
	}



	void setup_WithKernel(
			CSetup_Column_0thOrder &parent
	)
	{
		columnCenterX = parent.columnCenterX;
		columnCenterY = parent.columnCenterY;
		columnRadius = parent.columnRadius;
		columnRadiusSquared = parent.columnRadiusSquared;

		cDatasets = parent.cDatasets;
	}

};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
