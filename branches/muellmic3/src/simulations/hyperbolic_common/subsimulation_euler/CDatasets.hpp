/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mar 16, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMISIMULATION_DATASETS_HPP_
#define CTSUNAMISIMULATION_DATASETS_HPP_

#include <cmath>
#include <stdexcept>

#include "../CParameters.hpp"

#if CONFIG_ENABLE_ASAGI
#	include "../datasets_common/CAsagi.hpp"
#endif

#if CONFIG_ENABLE_NETCDF
#	include "../datasets_common/CSimpleNetCDF.hpp"
#endif

#include "../datasets_common/CDataSet_Interface.hpp"



class CDatasets	:
	public CHyperbolicTypes::CSimulationTypes,		///< import simulation types
	public CDataSet_Interface						///< this class has to provide the same dataset interface as each dataset
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:

	/**
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use simple netcdf
		 */
		SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF = -2,

		/*
		 * use asagi to determine water surface height
		 */
		SIMULATION_WATER_HEIGHT_ASAGI = -1,

		/*
		 * set simulation water surface height to zero
		 */
		SIMULATION_WATER_HEIGHT_DEFAULT = 0,

		SIMULATION_INTERACTIVE_UPDATE = 8
	};


	/**
	 * possible choices to setup terrain distance
	 */
	enum
	{
		SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF = -2,
		SIMULATION_TERRAIN_HEIGHT_ASAGI = -1,

		SIMULATION_TERRAIN_HEIGHT_DEFAULT = 0
	};


	/**
	 * simulation parameters for hyperbolic simulation
	 */
	CParameters &cParameters_Datasets;

	/**
	 * ASAGI dataset interface
	 */
#if CONFIG_ENABLE_ASAGI
	CAsagi cAsagi;
#endif

	/**
	 * simple netcdf interface
	 */
#if CONFIG_ENABLE_NETCDF
	CSimpleNetCDF cSimpleNetCDF;
#endif

	/**
	 * verbosity level
	 */
	int verbosity_level;



public:
	/**
	 * constructor
	 */
	CDatasets(
			CParameters &i_cSimulationHyperbolic_Parameters,
			int i_verbosity_level
	)	:
		cParameters_Datasets(i_cSimulationHyperbolic_Parameters),
#if CONFIG_ENABLE_ASAGI
		cAsagi(i_verbosity_level),
#endif
#if CONFIG_ENABLE_NETCDF
		cSimpleNetCDF(i_verbosity_level),
#endif
		verbosity_level(i_verbosity_level)
	{
	}


	void loadDatasets()
	{
		loadDatasets(cParameters_Datasets.simulation_datasets);
	}

	/**
	 * \brief setup datasets
	 *
	 * This is important for the datafile based simulation scenarios
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			if (	i_datasets[0].length() > 0		&&
					i_datasets[1].length() > 0
			)
			{
				cAsagi.loadDatasets(i_datasets);


				if (!cAsagi.isDatasetLoaded())
					return false;

				cAsagi.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_origin_x,
						&cParameters_Datasets.simulation_dataset_default_domain_origin_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				return true;
			}
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			if (	i_datasets[0].length() > 0		&&
					i_datasets[1].length() > 0
			)
			{
				cSimpleNetCDF.loadDatasets(i_datasets);

				if (!cSimpleNetCDF.isDatasetLoaded())
					return false;

				cSimpleNetCDF.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_origin_x,
						&cParameters_Datasets.simulation_dataset_default_domain_origin_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				return true;
			}
			break;
#endif
		}

		return true;
	}



public:
	/**
	 * deconstructor
	 */
	virtual ~CDatasets()
	{
	}




public:
	/**
	 * terrain dimensions
	 */
	inline void getOriginAndSize(
			T *o_origin_x,	///< origins x-coordinate
			T *o_origin_y,	///< origins y-coordinate
			T *o_size_x,	///< size of terrain in x-dimension
			T *o_size_y		///< size of terrain in y-dimension
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			cAsagi.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;

#endif

		default:
			*o_origin_x = 0;
			*o_origin_y = 0;

			*o_size_x = cParameters_Datasets.simulation_dataset_default_domain_size_x;
			*o_size_y = cParameters_Datasets.simulation_dataset_default_domain_size_y;
			break;
		}
	}


	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 *
	 * this version loads the node data by loading the timestamp from the dataset timestamp
	 */
	inline bool getBenchmarkNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		return getBenchmarkNodalData(i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp, o_cNodeData);
	}


	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 */
	inline bool getBenchmarkNodalData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			return cAsagi.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif
		}
		return false;
	}




	inline void getNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		if (cParameters_Datasets.simulation_dataset_1_id == SIMULATION_INTERACTIVE_UPDATE)
		{
			T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
			T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry >= cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
				return;

			o_cNodeData->r = std::max<T>(2, o_cNodeData->r*1.1);
			return;
		}

		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
		default:
			o_cNodeData->r = getDatasetValue(0, i_x, i_y, i_level_of_detail);
			o_cNodeData->ru = getDatasetValue(1, i_x, i_y, i_level_of_detail);
			o_cNodeData->rv = getDatasetValue(2, i_x, i_y, i_level_of_detail);
			o_cNodeData->e = getDatasetValue(3, i_x, i_y, i_level_of_detail);

			break;
		}
	}



public:
	/**
	 * terrain setup
	 */
	inline T getDatasetValue(
			int i_dataset_id,
			T i_x,
			T i_y,
			T i_level_of_detail
	)
	{
		if (i_dataset_id == 0)
		{
			/*
			 * bathymetry
			 */

			switch(cParameters_Datasets.simulation_dataset_0_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_TERRAIN_HEIGHT_ASAGI:
					return cAsagi.getDatasetValue(0, i_x, i_y, i_level_of_detail);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(0, i_x, i_y, i_level_of_detail);
#endif

				case SIMULATION_TERRAIN_HEIGHT_DEFAULT:
				default:
					return cParameters_Datasets.simulation_dataset_default_values[0];
			}
		}


		if (i_dataset_id == 1)
		{
			/*
			 * displacements
			 */

			switch(cParameters_Datasets.simulation_dataset_1_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
					return cAsagi.getDatasetValue(1, i_x, i_y, i_level_of_detail);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(1, i_x, i_y, i_level_of_detail);
#endif

				case SIMULATION_WATER_HEIGHT_DEFAULT:
				default:
					return cParameters_Datasets.simulation_dataset_default_values[1];
			}
		}

		for (int i = 0; i < 4; i++)
			std::cout << cParameters_Datasets.simulation_dataset_default_values[i] << std::endl;
		std::cout << std::endl;

		assert(i_dataset_id < 4);
		return cParameters_Datasets.simulation_dataset_default_values[i_dataset_id];
	}


public:
	/**
	 * return whether the maximum function should be used to update the water surface height
	 */
	inline bool getUseMaximumFunctionForSurfaceHeightSetup()
	{
		return false;
	}


	/**
	 * get boundary data at given position
	 */
	bool getBoundaryData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data	///< nodal data is written to this position
	)
	{
		std::cerr << "getBoundaryData not implemented yet" << std::endl;
		assert(false);
		return false;
	}


	/**
	 * return true if the dataset is loaded
	 */
	bool isDatasetLoaded()
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:		// setup with asagi
			return cAsagi.isDatasetLoaded();
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.isDatasetLoaded();
#endif

		default:
			return true;
		}
	}


	/**
	 * output verbose information
	 */
	void outputVerboseInformation()
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
			cAsagi.outputVerboseInformation();
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.outputVerboseInformation();
			break;
#endif


		case SIMULATION_WATER_HEIGHT_DEFAULT:
			std::cout << "Water setup: default displacement" << std::endl;
			break;

		default:
			std::cout << "Water setup: [unknown]" << std::endl;
			break;
		}


		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_DEFAULT:
			std::cout << "Terrain setup: default" << std::endl;
			break;
		}
	}
};



#endif /* CTSUNAMISIMULATION_DATASETS_HPP_ */
