/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: September 10, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTSIMULATIONSPECIFICDATA_HPP_
#define COUTPUTSIMULATIONSPECIFICDATA_HPP_


#include "../../hyperbolic_parallel/CSimulationHyperbolic_Cluster.hpp"
#include "../../../libsierpi/cluster/CDomainClusters.hpp"
#include "../subsimulation_generic/CConfig.hpp"
#include "../CParameters.hpp"

class COutputSimulationSpecificData
{
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;
	typedef CHyperbolicTypes::CSimulationTypes::T	T;

	CParameters &cParameters;
	CDatasets &cDatasets;
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;


public:
	COutputSimulationSpecificData(
			CParameters &i_cParameters,
			CDatasets &i_cDatasets,
			sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &i_cDomainClusters

	)	:
		cParameters(i_cParameters),
		cDatasets(i_cDatasets),
		cDomainClusters(i_cDomainClusters)
	{
	}



	/**
	 * \brief return a data sample at the given position
	 *
	 * this is useful e. g. to get bouy data
	 */
	void p_getDataSample(
			T i_sample_pos_x,
			T i_sample_pos_y,
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_sample_pos_x,
							i_sample_pos_y
						))
							return;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CGetNodeDataSample<CHyperbolicTypes>::TRAV cGetNodeDataSample;

					cGetNodeDataSample.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cGetNodeDataSample.cKernelClass.setup(
							i_sample_pos_x,
							i_sample_pos_y,
							o_cNodeData
						);

					cGetNodeDataSample.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
	}



	void output(
			const std::string &i_identifier,
			const std::string &i_parameters
	) {
	}
};


#endif /* COUTPUTSIMULATIONSPECIFICDATA_HPP_ */
