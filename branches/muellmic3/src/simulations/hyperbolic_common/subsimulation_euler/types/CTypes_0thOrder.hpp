/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Aug 27, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEULER_TYPES_0TH_ORDER_HPP
#define CEULER_TYPES_0TH_ORDER_HPP

#include "CTypes_Common.hpp"

#include <iostream>
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CValidation_EdgeData.hpp"

/**
 * Edge communication data during tsunami simulation
 */
class CEulerSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CEulerSimulationNodeData dofs[1];

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

	T CFL1_scalar;


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[])
	{
		dofs[0].setupDefaultValues(i_default_values);
	}



	friend
	::std::ostream&
	operator<<(
			::std::ostream& os,
			 const CEulerSimulationEdgeData &d
	)	{
		os << "Euler CEdgeData Information:" << std::endl;
		os << " + rho: " << d.dofs[0].r << std::endl;
		os << " + rhou: " << d.dofs[0].ru << std::endl;
		os << " + rhou: " << d.dofs[0].rv << std::endl;
		os << " + e: " << d.dofs[0].e << std::endl;
		os << " + cfl: " << d.dofs[0].cfl1_timestep_size << std::endl;
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;
		os << " + vx: " << d.dofs[0].ru/d.dofs[0].r << std::endl;
		os << " + vy: " << d.dofs[0].rv/d.dofs[0].r << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};


#include "simulations/hyperbolic_common/subsimulation_generic/types/CValidation_CellData.hpp"

class CEulerSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CEulerSimulationNodeData dofs[1];

	T cfl_domain_size_div_max_wave_speed;

#if SIMULATION_HYPERBOLIC_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA || SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 2
	#if 0
		unsigned refine:1;
		unsigned coarsen:1;
	#else
		bool refine;
		bool coarsen;
	#endif
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif



	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(T default_values[])
	{
		dofs[0].setupDefaultValues(default_values);

		cfl_domain_size_div_max_wave_speed = std::numeric_limits<T>::infinity();
	}



	friend
	::std::ostream&
	operator<<
	(
			::std::ostream& os,
			 const CEulerSimulationCellData &d
	)	{
		os << d.dofs[0] << ", cfl_domain_size_div_max_wave_speed: " << d.cfl_domain_size_div_max_wave_speed << std::endl;
		return os;
	}



	void outputVerboseData(
			std::ostream &os,	///< cout
			T x_axis_x,			///< x-axis of new basis (x-component)
			T x_axis_y			///< x-axis of new basis (y-component)
	) const
	{
		os << "Euler CCellData Information:" << std::endl;
		dofs[0].outputVerboseData(os, x_axis_x, x_axis_y);
		os << ", cfl_domain_size_div_max_wave_speed: " << cfl_domain_size_div_max_wave_speed << std::endl;
	}


	/**
	 * compute and store node data
	 */
	void computeNodeData(
			T i_x,			///< coordinate in reference system
			T i_y,			///< coordinate in reference system
			CEulerSimulationNodeData *o_NodeData
	)	{
		o_NodeData->r = 0;
		o_NodeData->ru = 0;
		o_NodeData->rv = 0;
		o_NodeData->e = 0;


		o_NodeData->r	+= dofs[0].r;
		o_NodeData->ru	+= dofs[0].ru;
		o_NodeData->rv	+= dofs[0].rv;
		o_NodeData->e	+= dofs[0].e;

/*
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T weight = CTsunamiSimulationBasisFunctions::eval(i, i_x, i_y);

			o_NodeData->r	+= weight * dofs[i].r;
			o_NodeData->ru	+= weight * dofs[i].ru;
			o_NodeData->rv	+= weight * dofs[i].rv;
			o_NodeData->e	+= weight * dofs[i].e;
		}
*/
	}

};


#endif
