/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: September 10, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTSIMULATIONSPECIFICDATA_HPP_
#define COUTPUTSIMULATIONSPECIFICDATA_HPP_


class COutputSimulationSpecificData
{
	typedef CHyperbolicTypes::CSimulationTypes::T	T;

	CParameters &cParameters;
	CDatasets &cDatasets;
	CGrid_1D<T> &cGrid1D;

	bool setup_done;


	class CDartSamplings
	{
public:
		T position_x;
		T position_y;
		std::string output_file;
	};


	CDartSamplings *sampling_points;
	int dart_samplings_size;

	std::ofstream dartfile_stream;




	/**
	 * setup dart sampling points
	 *
	 * Format:
	 *
	 * [double]/[double]/[outputfile],[double]/[double]/[outputfile],[double]/[double]/[outputfile],...
	 *
	 * [posx]/[posy]/[outputfile]
	 */
	bool setupDartSamplingPoints(
			const std::string &i_dart_sampling_points_string
	)
	{
		dart_samplings_size = 1;
		for (unsigned int i = 0; i < i_dart_sampling_points_string.size(); i++)
			if (i_dart_sampling_points_string[i] == ',')
				dart_samplings_size++;

		size_t delimiter_pos;
		size_t old_pos = 0;

		if (sampling_points != nullptr)
			delete [] sampling_points;

		sampling_points = new CDartSamplings[dart_samplings_size];

		for (int i = 0; i < dart_samplings_size; i++)
		{
			// search for next delimiter
			delimiter_pos = i_dart_sampling_points_string.find(',', old_pos);

			std::string sampling_point_string;

			// if not found, we reached the end of the point array
			if (delimiter_pos == std::string::npos)
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos);
			else
				sampling_point_string = i_dart_sampling_points_string.substr(old_pos, delimiter_pos-old_pos);

			old_pos = delimiter_pos+1;


			/*
			 * split single point
			 */
			size_t pos = sampling_point_string.find('/');
			if (pos == std::string::npos)
			{
				std::cout << "Invalid format for -D " << std::endl;
				std::cout << "   use e. g. -D 834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,2551805.95807/1737033.44135/21414.txt,2067322.03614/1688081.28844/21415.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt,14504102.9787/2982362.63316/32412.txt,9587628.42429/4546598.48709/43412.txt,5469762.33805/4355009.65196/46404.txt,6175228.13135/-147774.285934/51407.txt,5070673.2031/-4576701.68506/51425.txt,1321998.22866/-2869613.28102/52402.txt,381553.066367/-3802522.22537/52403.txt,-1123410.10612/-2776560.61408/52405.txt,2757153.33743/-4619941.45812/52406.txt " << std::endl;
				exit(-1);
			}
			std::string l = sampling_point_string.substr(0, pos);
			std::string r = sampling_point_string.substr(pos+1);


			/*
			 * search for appended filename
			 */
			pos = r.find('/', 1);
			if (pos != std::string::npos)
			{
				sampling_points[i].output_file = r.substr(pos+1);
				r = r.substr(0, pos);
			}

			sampling_points[i].position_x = std::atof(l.c_str());
			sampling_points[i].position_y = std::atof(r.c_str());
		}

		return true;
	}





public:
	COutputSimulationSpecificData(
			CParameters &i_cParameters,
			CDatasets &i_cDatasets,
			CGrid_1D<T> &i_cGrid1D
	)	:
		cParameters(i_cParameters),
		cDatasets(i_cDatasets),
		cGrid1D(i_cGrid1D),
		setup_done(false),
		sampling_points(nullptr),
		dart_samplings_size(0)
	{
	}



public:
	virtual ~COutputSimulationSpecificData()
	{
		if (sampling_points)
		{
			delete []sampling_points;
			sampling_points = nullptr;
		}
	}


	/**
	 * \brief return a data sample at the given position
	 *
	 * this is useful e. g. to get bouy data
	 */
	void p_getDataSample(
			T i_sample_pos_x,
			T i_sample_pos_y,
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData
	)
	{
		// cGrid1D
		std::cout << "TODO" << std::endl;
		assert(false);
		exit(-1);
#if 0
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_sample_pos_x,
							i_sample_pos_y
						))
							return;

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CGetNodeDataSample<CHyperbolicTypes>::TRAV cGetNodeDataSample;

					cGetNodeDataSample.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cGetNodeDataSample.cKernelClass.setup(
							i_sample_pos_x,
							i_sample_pos_y,
							o_cNodeData
						);

					cGetNodeDataSample.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
#endif
	}


	void setup(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		if (i_identifier == "dart")
		{
			if (!setupDartSamplingPoints(i_parameters))
			{
				throw(std::runtime_error("wrong format for dart sampling points"));
			}
		}
	}



	void outputDartStationData(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		if (cParameters.verbosity_level > 4)
			std::cout << "Writing sampling point data" << std::endl;

		for (int i = 0; i < dart_samplings_size; i++)
		{
			// output string buffer
			std::stringstream specificDataStreamBuf;
			specificDataStreamBuf << cParameters.simulation_timestamp_for_timestep << "\t";


			/*
			 * SIMULATION DATA
			 */
			CHyperbolicTypes::CSimulationTypes::CNodeData cSimulationNodeData;
			p_getDataSample(sampling_points[i].position_x, sampling_points[i].position_y, &cSimulationNodeData);

			std::vector<T> simulationData;
			cSimulationNodeData.getSimulationSpecificBenchmarkData(&simulationData);

			for (size_t j = 0; j < simulationData.size(); j++)
				specificDataStreamBuf << simulationData[j] << "\t";


			/*
			 * REFERENCE DATA (if available)
			 */
			CHyperbolicTypes::CSimulationTypes::CNodeData cReferenceNodeData;

			if (	cDatasets.getBenchmarkNodalData(
						sampling_points[i].position_x,
						sampling_points[i].position_y,
						0,
						cParameters.simulation_timestamp_for_timestep,
						&cReferenceNodeData
				)
			)	{
				std::vector<T> referenceData;
				cReferenceNodeData.getSimulationSpecificBenchmarkData(&referenceData);

				for (size_t j = 0; j < referenceData.size(); j++)
					specificDataStreamBuf << referenceData[j] << "\t";
			}

			specificDataStreamBuf << cParameters.number_of_local_cells << "\t";
			specificDataStreamBuf << cParameters.number_of_global_cells << "\t";

			if (sampling_points[i].output_file.empty())
			{
				std::cout << specificDataStreamBuf.str() << std::endl;
			}
			else
			{
				std::ofstream s;

				if (cParameters.simulation_timestep_nr == 0)
					s.open(sampling_points[i].output_file.c_str());
				else
					s.open(sampling_points[i].output_file.c_str(), std::ofstream::app);

				s << specificDataStreamBuf.str() << std::endl;
			}
		}
	}


	/**
	 * storage class for final benchmark value
	 */
	class CBenchmarkValue
	{
		CMainThreading_Lock lock;

public:
		T v0x, v0y;
		T v1x, v1y;
		T v2x, v2y;

		T timestamp;

		int depth;

		T max_horizon_value;

		CBenchmarkValue()	:
			max_horizon_value(-std::numeric_limits<T>::infinity())
		{
		}


		/**
		 * test and update maximum horizon water height
		 */
		inline void update(
				T i_v0x, T i_v0y,
				T i_v1x, T i_v1y,
				T i_v2x, T i_v2y,
				int i_depth,
				T i_max_horizon_value
			)
		{
			// TODO/INFO: this is expected to create a bunch of false sharing conflicts

			if (i_max_horizon_value <= max_horizon_value)
				return;

			lock.lock();

				if (i_max_horizon_value > max_horizon_value)
				{
					v0x = i_v0x;	v0y = i_v0y;
					v1x = i_v1x;	v1y = i_v1y;
					v2x = i_v2x;	v2y = i_v2y;

					depth = i_depth;

					max_horizon_value = i_max_horizon_value;
				}

			lock.unlock();
		}
	};


	// maximum benchmark value
	CBenchmarkValue cBenchmarkValue_max;


	void getMaxBenchmarkValue(
			CBenchmarkValue *io_cBenchmarkValue
	)	{

		/**
		 * SWOSB benchmark traversator and kernel to store the maximum water height and position
		 */

		int number_of_cells = cGrid1D.getNumberOfCells();
		for (int i = 0; i < number_of_cells; i++)
		{
			CCellData_1D *i_cCellData = &(cGrid1D.getCellData(i));

			for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				if (i_cCellData->dofs.h[i] > SIMULATION_TSUNAMI_DRY_THRESHOLD)
				{
					T horizon_h = i_cCellData->dofs.h[i] + i_cCellData->dofs.b[i];
					io_cBenchmarkValue->update(i_cCellData->position_x, 0, i_cCellData->position_x, 0, i_cCellData->position_x, 0, -1, horizon_h);
				}
			}
		}

		io_cBenchmarkValue->timestamp = cParameters.simulation_timestamp_for_timestep;
	}



	void outputSWOSBDataToFile(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		CBenchmarkValue cBenchmarkValue;

		getMaxBenchmarkValue(&cBenchmarkValue);

		std::ofstream s;
		if (cParameters.simulation_timestep_nr == 0)
			s.open(i_parameters.c_str());
		else
			s.open(i_parameters.c_str(), std::ofstream::app);

		T px = (cBenchmarkValue.v0x + cBenchmarkValue.v1x + cBenchmarkValue.v2x) * (1.0/3.0);
		T py = (cBenchmarkValue.v0y + cBenchmarkValue.v1y + cBenchmarkValue.v2y) * (1.0/3.0);

		if (cBenchmarkValue.max_horizon_value > cBenchmarkValue_max.max_horizon_value)
			cBenchmarkValue_max = cBenchmarkValue;

		s << cParameters.simulation_timestamp_for_timestep << "\t" << cBenchmarkValue.max_horizon_value << "\t" << px << "\t" << py << std::endl;
	}



	void outputConsoleMaxSWOSBData(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		CBenchmarkValue cBenchmarkValue;

		getMaxBenchmarkValue(&cBenchmarkValue);

		if (cBenchmarkValue.max_horizon_value > cBenchmarkValue_max.max_horizon_value)
			cBenchmarkValue_max = cBenchmarkValue;

		T px = (cBenchmarkValue_max.v0x + cBenchmarkValue_max.v1x + cBenchmarkValue_max.v2x) * (1.0/3.0);
		T py = (cBenchmarkValue_max.v0y + cBenchmarkValue_max.v1y + cBenchmarkValue_max.v2y) * (1.0/3.0);

		std::cout << "MaxRunup:\t" << cBenchmarkValue_max.timestamp << "\t" << cBenchmarkValue_max.max_horizon_value << "\t" << px << "\t" << py << "\t" << cBenchmarkValue_max.depth << std::endl;
	}



	void output(
			const std::string &i_identifier,
			const std::string &i_parameters
	)	{
		if (i_identifier.size() == 0)
			return;

		if (!setup_done)
		{
			setup(i_identifier, i_parameters);
			setup_done = true;
		}

		if (i_identifier == "dart")
		{
			outputDartStationData(i_identifier, i_parameters);
			return;
		}

		if (i_identifier == "swosb")
		{
			outputSWOSBDataToFile(i_identifier, i_parameters);
			return;
		}

		if (i_identifier == "swosb_max_to_cout")
		{
			outputConsoleMaxSWOSBData(i_identifier, i_parameters);
			return;
		}
	}
};


#endif /* COUTPUTSIMULATIONSPECIFICDATA_HPP_ */
