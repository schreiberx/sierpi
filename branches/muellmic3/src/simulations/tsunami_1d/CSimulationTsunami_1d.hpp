/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: August 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>, Breuer Alexander <breuera@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_1D_HPP_
#define CSIMULATIONTSUNAMI_1D_HPP_

#include <iostream>
#include <stdexcept>

#include "config.h"
#include "lib/CStopwatch.hpp"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "../hyperbolic_common/traversators/CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_EdgeComm_Normals_Depth.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_Setup_Column.hpp"

#if CONFIG_SIERPI_ENABLE_GUI
#	include "libgl/draw/CGlDrawWireframeFromVertexArray.hpp"
#	include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalArray.hpp"
#endif

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"
#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif


#include "libmath/CVector.hpp"
#include "lib/CStopwatch.hpp"

#include "../hyperbolic_common/CParameters.hpp"
#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_CellData.hpp"

#include "CSimulation_MainInterface.hpp"
#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulation_MainGuiInterface.hpp"
#endif
#include "CSimulation_MainInterface_FileOutput.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

#include "CGrid_1D.hpp"

#include "COutputSimulationSpecificData.hpp"


class CSimulationTsunami_1d:
	public CParameters,
	public CSimulation_MainInterface,
	public CSimulation_MainInterface_FileOutput
#if CONFIG_SIERPI_ENABLE_GUI
	,
	public CSimulation_MainGuiInterface
#endif
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CGrid_1D<T> cGrid1D;

#if CONFIG_SIERPI_ENABLE_GUI
	CGlDrawTrianglesWithVertexAndNormalArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesFromVertexAndNormalArray;
	CGlDrawWireframeFromVertexArray<CHyperbolicTypes::CVisualizationTypes::T> cOpenGL_Vertices_Wireframe_Root_Tsunami;
#endif

public:
	CTriangle_Factory cTriangleFactory;

	/**
	 * datasets to get bathymetry or water surface parameters
	 */
	CDatasets cDatasets;

	/**
	 * handler to simulation parameters to use same code as for parallel version
	 */
	CParameters &cParameters;

	/**
	 * output of simulation specific data
	 */
	COutputSimulationSpecificData cOutputSimulationSpecificData;

public:
	/**
	 * constructor for serial tsunami simulation
	 */
	CSimulationTsunami_1d(
			int i_verbosity_level
	)	:
		cDatasets(*this, i_verbosity_level),
		cParameters(*this),
		cOutputSimulationSpecificData((CParameters&)(*this), cDatasets, cGrid1D)
	{
		verbosity_level = i_verbosity_level;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		if (threading_pin_to_core_nr_after_one_timestep != -1)
			testPinToCore(-2);
#endif

		/**
		 * setup depth for root triangle
		 */
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.maxDepth = grid_initial_recursion_depth + grid_max_relative_recursion_depth;

//		reset();
	}


#if CONFIG_SIERPI_ENABLE_GUI

	/**
	 * render BATHYMETRY
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		switch(i_bathymetry_visualization_method % 4)
		{
			case 2:
			{
				CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[5][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1, 1, 1));
				cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_cells*2
					);

				cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();

				return "benchmark data";
			}

			case 3:
				// do not render bathymetry
				return "blank";

			default:
			{
				CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

				cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_cells*2
					);

				cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}

			return "simple";
		}
	}


	/**
	 * render water surface
	 */
	const char *render_DOFs(
			int i_surface_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		CGridDataArrays<2,6> cGridDataArrays(cParameters.number_of_local_cells);


		const char *ret_str = nullptr;
		T *v;
		T *n;
		T t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE1);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[1][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "x-momentum";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE2);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[2][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "y-momentum";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "bathymetry";
				break;


			case 5:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[5][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "benchmark data";
				break;


			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE0 + CGridDataArrays_Enums::VALUE3);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 2; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[0][i]+cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "surface height";
				break;
		}

		cCommonShaderPrograms.cHeightColorBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		cOpenGL_Vertices_Wireframe_Root_Tsunami.render(
				cGridDataArrays.triangle_vertex_buffer,
				cParameters.number_of_local_cells*2
			);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;
	}



	void p_render_Wireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
	}

	void render_Wireframe(
			int i_visualization_render_wireframe,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
			p_render_Wireframe(cCommonShaderPrograms.cBlinn);
	}


	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}

	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
	}



	void p_applyTranslateAndScaleToVertex(
			CHyperbolicTypes::CVisualizationTypes::T *io_vertex
	)
	{
		io_vertex[0] = (io_vertex[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
		io_vertex[1] = (io_vertex[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;
		io_vertex[2] = (io_vertex[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_z;
	}


	void storeDOFsToGridDataArrays(
			CGridDataArrays<2,6> *io_cGridDataArrays,
			int i_flags
	)
	{
		i_flags |=
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::NORMALS;

//		size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(number_of_local_cells);

		CHyperbolicTypes::CVisualizationTypes::T *v = &(io_cGridDataArrays->triangle_vertex_buffer[0]);

		for (int i = 0; i < cParameters.number_of_local_cells; i++)
		{
			CCellData_1D &c = cGrid1D.getCellData(i);

//			T d;

			if (i_flags & CGridDataArrays_Enums::VALUE0)
				io_cGridDataArrays->dof_element[0][i] = c.dofs.h[0];

			if (i_flags & CGridDataArrays_Enums::VALUE1)
				io_cGridDataArrays->dof_element[1][i] = c.dofs.hu[0];

			if (i_flags & CGridDataArrays_Enums::VALUE2)
				io_cGridDataArrays->dof_element[2][i] = c.dofs.hv[0];

			if (i_flags & CGridDataArrays_Enums::VALUE3)
				io_cGridDataArrays->dof_element[3][i] = c.dofs.b[0];

			if (i_flags & CGridDataArrays_Enums::VALUE5)
			{
				CTsunamiSimulationNodeData benchmark_data;
				cDatasets.getBenchmarkNodalData(c.getPositionX(), 0, 0, &benchmark_data);
//				std::cout << c.getPositionX() << std::endl;
				io_cGridDataArrays->dof_element[5][i] = benchmark_data.h;
			}

			v[0] = c.getPositionX() - c.getCellSizeX()*0.5;
			v[1] = 0;
			v[2] = 0;
			v+=3;

			v[0] = c.getPositionX() + c.getCellSizeX()*0.5;
			v[1] = 0;
			v[2] = 0;
			v+=3;
		}



		/*
		 * postprocessing
		 */
		if (	cParameters.visualization_scale_x != 1.0 ||
				cParameters.visualization_scale_y != 1.0 ||
				cParameters.visualization_scale_z != 1.0 ||

				cParameters.visualization_translate_x != 1.0 ||
				cParameters.visualization_translate_y != 1.0 ||
				cParameters.visualization_translate_z != 1.0
		)
		{
			CHyperbolicTypes::CVisualizationTypes::T *v = &(io_cGridDataArrays->triangle_vertex_buffer[0]);

			for (int i = 0; i < cParameters.number_of_local_cells; i++)
			{
				for (int vn = 0; vn < 2; vn++)
				{
					p_applyTranslateAndScaleToVertex(v);
					v += 3;
				}

				io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;
				io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;
			}
		}
	}

#endif



	void setup_TraversatorsAndKernels()
	{
		cTriangleFactory.vertices[0][0] = 0.5;
		cTriangleFactory.vertices[0][1] = -0.5;
		cTriangleFactory.vertices[1][0] = -0.5;
		cTriangleFactory.vertices[1][1] = 0.5;
		cTriangleFactory.vertices[2][0] = -0.5;
		cTriangleFactory.vertices[2][1] = -0.5;

		cTriangleFactory.evenOdd = CTriangle_Enums::EVEN;
		cTriangleFactory.hypNormal = CTriangle_Enums::NORMAL_NE;
		cTriangleFactory.recursionDepthFirstRecMethod = 0;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_BOUNDARY;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_BOUNDARY;

		cTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_FORWARD;
		cTriangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_V;

		cTriangleFactory.clusterTreeNodeType = CTriangle_Enums::NODE_ROOT_TRIANGLE;

		for (int i = 0; i < 3; i++)
		{
			cTriangleFactory.vertices[i][0] = (cTriangleFactory.vertices[i][0]*simulation_dataset_default_domain_size_x) + simulation_dataset_default_domain_translate_x;
			cTriangleFactory.vertices[i][1] = (cTriangleFactory.vertices[i][1]*simulation_dataset_default_domain_size_y) + simulation_dataset_default_domain_translate_y;
		}
	}

	void setup()
	{
		reset();
	}

	void reset()
	{
		cDatasets.loadDatasets();

		setup_TraversatorsAndKernels();
		p_setup_InitialTriangulation();

		reset_simulation_parameters();

		if (cParameters.simulation_domain_strip_boundary_condition_dataset_right)
		{
			cGrid1D.setBoundaryConditionsLeftAndRight(
					EBoundaryConditions::BOUNDARY_CONDITION_BOUNCE_BACK,
					EBoundaryConditions::BOUNDARY_CONDITION_DATASET
				);
		}
		else
		{
			cGrid1D.setBoundaryConditionsLeftAndRight(
					EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW,
					EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW
				);
		}
	}


	void clean()
	{
	}

	void setup_RadialDamBreak(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius
	)
	{
	}


private:
	void p_setup_InitialTriangulation()
	{
		number_of_local_cells = 1 << grid_initial_recursion_depth;
//		number_of_local_cells = cSetup_Structure_CellData.setup(cStacks, grid_initial_recursion_depth, simulation_parameter_cell_data_setup);
		number_of_global_cells = number_of_local_cells;

		number_of_local_initial_cells_after_domain_triangulation = number_of_local_cells;
		number_of_global_initial_cells_after_domain_triangulation = number_of_local_initial_cells_after_domain_triangulation;

		p_setup_initial_grid_data();
	}


private:
	void p_setup_initial_grid_data()
	{
		cGrid1D.setup(
				number_of_local_cells,			///< i_numberOfCells number of the cells in the initial grid.
				&cDatasets,	///< tsunami simulation scenario
				&cParameters	///< parameters
			);

#if 0
		// setup element data with respect to vertex positions
		sierpi::kernels::CSetup_CellData::TRAV cSetup_CellData;
		cSetup_CellData.setup_sfcMethods(cTriangleFactory);
		cSetup_CellData.cKernelClass.setup_Parameters(&cDatasets, true);
		cSetup_CellData.action(cStacks);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		cSetup_TsunamiCellData_Validation.action(cStacks);
#endif
#endif

	}


private:
	void p_simulation_adaptive_traversals()
	{
#if 0
		bool repeat_traversal = cTsunami_Adaptive.actionFirstTraversal(cStacks);

		while(repeat_traversal)
			repeat_traversal = cTsunami_Adaptive.actionMiddleTraversals_Serial(cStacks);

		cTsunami_Adaptive.actionLastTraversal_Serial(cStacks);

		number_of_local_cells = cStacks->element_data_stacks.getNumberOfElementsOnStack();
#endif
	}

public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{
#if 0
		long long prev_number_of_local_cells;
		long long prev_number_of_local_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		T coarsen_threshold_backup = adaptive_coarsen_parameter_0;
		adaptive_coarsen_parameter_0 = -9999999;

//		updateClusterParameters();

		int max_setup_iterations = (grid_max_relative_recursion_depth + grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 10;

		int iterations;
		for (iterations = 0; iterations < max_setup_iterations; iterations++)
		{
			prev_number_of_local_cells = number_of_local_cells;
			prev_number_of_local_clusters = number_of_local_clusters;

			// setup grid data
			p_setup_initial_grid_data();

			// run single timestep
			p_simulation_edge_comm();

			// refine / coarsen grid
			p_simulation_adaptive_traversals();

			// split/join clusters
//			p_simulation_cluster_split_and_join();


			if (verbosity_level >= 5)
				std::cout << " > triangles: " << number_of_local_cells << ", number_of_local_clusters: " << number_of_local_clusters << std::endl;

			if (	prev_number_of_local_cells == number_of_local_cells	&&
					prev_number_of_local_clusters == number_of_local_clusters
			)
				break;
		}

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		// update cluster parameters
		adaptive_coarsen_parameter_0 = coarsen_threshold_backup;
//		updateClusterParameters();
#endif
	}

private:
	/**
	 * timestep edge comm
	 */
	void p_simulation_edge_comm()
	{
//		cGrid1D.printCellValues();

		simulation_parameter_global_timestep_size = cGrid1D.runSingleTimestep(simulation_parameter_cfl);

//		cGrid1D.printCellValues();


#if 0
		cTsunami_EdgeComm.actionFirstPass(cStacks);

		if (simulation_parameter_adaptive_timestep_size_with_cfl)
		{
			// adaptive timestep size
			T cfl1_value;
			cTsunami_EdgeComm.actionMiddlePass_computeClusterBorderCFL(cStacks, &cfl1_value);

			simulation_parameter_global_timestep_size = cfl1_value * simulation_parameter_cfl;
		}

		cTsunami_EdgeComm.actionSecondPass_Serial(cStacks, simulation_parameter_global_timestep_size);
#endif
	}


public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_CellDataAt2DPosition(
			CHyperbolicTypes::CVisualizationTypes::T x,
			CHyperbolicTypes::CVisualizationTypes::T y,
			CHyperbolicTypes::CVisualizationTypes::T radius = 0.3
	)
	{
#if 0
		CTsunamiSimulationCellData element_data_modifier;

		element_data_modifier.setup(simulation_dataset_water_surface_default_displacement-simulation_dataset_default_values[0], -simulation_dataset_default_values[0]);

		cModify_OneElementValue_SelectByPoint.cKernelClass.setup(x, y, &element_data_modifier);
		cModify_OneElementValue_SelectByPoint.action(cStacks);

		cTsunami_Adaptive.setup_KernelClass(simulation_dataset_default_domain_size_x, adaptive_refine_parameter_0, -1, adaptive_refine_parameter_1, -999, &cDatasets);
		p_simulation_adaptive_traversals();
		cTsunami_Adaptive.setup_KernelClass(simulation_dataset_default_domain_size_x, adaptive_refine_parameter_0, adaptive_coarsen_parameter_0, adaptive_refine_parameter_1, adaptive_coarsen_parameter_1, &cDatasets);
#endif
	}


#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
public:
	inline void testPinToCore(int pinning_core)
	{
		if (pinning_core == -1)
			return;


		cpu_set_t cpu_set;
		CPU_ZERO(&cpu_set);

		// pin to first core for initialization to assure placement of allocated data
		if (pinning_core == -2)
			CPU_SET(0, &cpu_set);
		else
			CPU_SET(pinning_core, &cpu_set);

		std::cout << "Pinning application to core " << pinning_core << std::endl;

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		std::cout << std::endl;
		std::cout << "WARNING: Adaptive cluster stacks activated!!!" << std::endl;
		std::cout << std::endl;
#endif

		int err;
		err = pthread_setaffinity_np(
				pthread_self(),
				sizeof(cpu_set_t),
				&cpu_set
			);


		if (err != 0)
		{
			std::cout << err << std::endl;
			perror("pthread_setaffinity_np");
			assert(false);
			exit(-1);
		}

		if (pinning_core >= 0)
			threading_pin_to_core_nr_after_one_timestep = -1;
	}
#endif

public:
	void runSingleTimestep()
	{
		simulation_dataset_benchmark_input_timestamp = simulation_timestamp_for_timestep;

		p_simulation_edge_comm();
		p_simulation_adaptive_traversals();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	CStopwatch cStopwatch;


public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
	)
	{
		simulation_dataset_benchmark_input_timestamp = simulation_timestamp_for_timestep;

		// simulation timestep
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_simulation_adaptive_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		simulation_timestep_nr++;
		simulation_timestamp_for_timestep += simulation_parameter_global_timestep_size;

#if CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
		testPinToCore(threading_pin_to_core_nr_after_one_timestep);
#endif
	}


	/***************************************************************************************
	 * SAMPLE DATA
	 ***************************************************************************************/
	double getDataSample(
			double i_sample_pos_x,
			double i_sample_pos_y,
			const char *i_sample_information
		)
	{
		return 0;
	}

public:
	void debugOutput(CVector<2,float> &planePosition)
	{
	}


	void writeSimulationDataToFile(
			const char *i_filename,
			const char *i_information_string = nullptr
	)
	{
		cGrid1D.writeSimulationDataToFile(i_filename, i_information_string, cParameters.simulation_timestamp_for_timestep);

#if 0
		CSimulationTsunami_GridDataArrays cGridDataArrays(number_of_local_cells);

		sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

		int flags =
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::H	|
				CSimulationTsunami_GridDataArrays::HU	|
				CSimulationTsunami_GridDataArrays::HV	|
				CSimulationTsunami_GridDataArrays::B	|
				CSimulationTsunami_GridDataArrays::CFL_VALUE_HINT;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				&cGridDataArrays,
				0,
				flags
			);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

		cVtkXMLTrianglePolyData.setup();
		cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
		cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.h);
		cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.hu);
		cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.hv);
		cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.b);
		cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.cfl_value_hint);

		cVtkXMLTrianglePolyData.write(i_filename);
#endif
	}


	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		std::cout << "writeSimulationClustersDataToFile() not implemented (meaningless for serial version)" << std::endl;
	}

#if CONFIG_SIERPI_ENABLE_GUI

	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	bool gui_key_down_event(
			int key
	)
	{
		switch(key)
		{
			case 'j':
				runSingleTimestep();
				break;

			case 'c':
				setup_RadialDamBreak(simulation_dataset_breaking_dam_posx, simulation_dataset_breaking_dam_posy, simulation_dataset_breaking_dam_radius);
				break;

			case 't':
				grid_initial_recursion_depth += 1;
				reset();
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			case 'g':
				if (grid_initial_recursion_depth > 0)
					grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << grid_initial_recursion_depth << std::endl;
				reset();
				break;

			case 'G':
				if (grid_max_relative_recursion_depth > 0)
					grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << grid_max_relative_recursion_depth << std::endl;
				reset();
				break;

			default:
				return false;
		}

		return true;
	}


	bool gui_mouse_motion_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	bool gui_mouse_button_down_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}
#endif


	void outputVerboseInformation()
	{
		((CParameters&)(*this)).outputVerboseInformation();
	}

	void output_simulationSpecificData(
			const std::string &i_identifier,
			const std::string &i_parameter
	)	{
		cOutputSimulationSpecificData.output(i_identifier, i_parameter);
//		std::cout << "output_simulationSpecificData() not implemented" << std::endl;
	}

	void debug_OutputCellData(
				T i_position_x,		///< x-coordinate of triangle cell
				T i_position_y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputCellData() not implemented" << std::endl;
	}

	void debug_OutputClusterInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputClusterInformation() not implemented" << std::endl;
	}

	void debug_OutputEdgeCommunicationInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		)
	{
		std::cout << "debug_OutputEdgeCommunicationInformation() not implemented" << std::endl;
	}

	void output_ClusterTreeInformation()
	{
	}

	void action_Validation()
	{

	}

	virtual ~CSimulationTsunami_1d()
	{
		clean();
	}

	void updateScanDatasets(
			int use_number_of_threads = -1
	)
	{
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T	*o_origin_x,	///< origin of domain in world-space
			T	*o_origin_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	)
	{
		*o_origin_x = cParameters.simulation_dataset_default_domain_translate_x;
		*o_origin_y = cParameters.simulation_dataset_default_domain_translate_y;

		*o_size_x = cParameters.simulation_dataset_default_domain_size_x;
		*o_size_y = cParameters.simulation_dataset_default_domain_size_y;
	}

};

#endif /* CTSUNAMI_HPP_ */
