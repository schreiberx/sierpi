/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: 15. March 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#if SIMULATION_TSUNAMI_SERIAL
	#include "simulations/tsunami_serial/CSimulationTsunami_Serial.hpp"
	typedef CSimulationTsunami_Serial		CSimulation;
#endif

#if SIMULATION_HYPERBOLIC_PARALLEL
	#include "simulations/hyperbolic_parallel/CSimulationHyperbolic_Parallel.hpp"
	typedef CSimulationHyperbolic_Parallel		CSimulation;
#endif

#if SIMULATION_TSUNAMI_1D
	#include "simulations/tsunami_1d/CSimulationTsunami_1d.hpp"
	typedef CSimulationTsunami_1d			CSimulation;
#endif
