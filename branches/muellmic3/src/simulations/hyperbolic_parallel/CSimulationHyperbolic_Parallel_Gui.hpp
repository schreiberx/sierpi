/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_GUI_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_GUI_HPP_


#include "config.h"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../hyperbolic_common/CParameters.hpp"

#include "libsierpi/cluster/CDomainClusters.hpp"
#include "CSimulationHyperbolic_Cluster.hpp"


/*
 * Write data stored in recursive SFC format to 1D arrays data structure:
 *
 * COutputGridDataArrays represents the container.
 * COutputGridDataArrays implements the kernel which writes the data.
 */
#include "libsierpi/CGridDataArrays.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

/*
 * root renderer:
 *
 * setup buffers,
 * render specific primitives stored in buffer
 */
#include "libgl/draw/CGlDrawWireframeFromVertexArray.hpp"
#include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalArray.hpp"

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#	include "../hyperbolic_common/kernels/backends/tsunami/COpenGL_Vertices_Smooth_Cell_Tsunami.hpp"
#endif

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"
#endif



/*
 * interfaces which have to be implemented
 */
#include "CSimulation_MainInterface.hpp"
#include "CSimulation_MainGuiInterface.hpp"

/**
 * \brief Main class for parallel Hyperbolic Simulation
 *
 * This class is the central point of a parallel Hyperbolic simulation.
 *
 * sets up the simulation.
 * It manages all sub-clusters, creates the initial domain triangulation and
 */
class CSimulationHyperbolic_Parallel_Gui	:
	public CSimulation_MainGuiInterface,
	public CSimulation_MainInterface
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGenericTreeNode<CCluster_TreeNode_> CGenericTreeNode_;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	/**
	 * root renderer
	 */
	CGlDrawTrianglesWithVertexAndNormalArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesWithVertexAndNormalArray;
	CGlDrawWireframeFromVertexArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawWireframeFromVertexArray;
//	COpenGL_Vertices_Cluster_Root_Tsunami cOpenGL_Vertices_Cluster_Root_Tsunami;

	/**
	 * reference to simulation parameters
	 */
	CParameters &cParameters;

	/**
	 * reference to clusters to start traversals
	 */
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;

public:
	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationHyperbolic_Parallel_Gui(
			CParameters &i_cParameters,
			sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &i_cDomainClusters
	)	:
			cParameters(i_cParameters),
			cDomainClusters(i_cDomainClusters)
	{
	}


	/**
	 * Deconstructor
	 */
	virtual ~CSimulationHyperbolic_Parallel_Gui()
	{
	}


public:

	/**
	 * ignore key-up events
	 */
	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}


	/**
	 * handle key-down event
	 */
	bool gui_key_down_event(
			int i_key	///< pressed key (ascii)
	)
	{
		switch(i_key)
		{
			case 'i':
				cParameters.simulation_random_raindrops_activated ^= true;
				break;

			case 'z':
				cParameters.simulation_world_scene_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cParameters.simulation_world_scene_id << std::endl;
				break;

			case 'x':
				cParameters.simulation_world_scene_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cParameters.simulation_world_scene_id << std::endl;
				break;

			case 'Z':
				cParameters.simulation_dataset_0_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cParameters.simulation_dataset_0_id << std::endl;
				break;

			case 'X':
				cParameters.simulation_dataset_0_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cParameters.simulation_dataset_0_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 'y':
				cParameters.grid_initial_cluster_splits += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 'h':
				if (cParameters.grid_initial_cluster_splits > 0)
					cParameters.grid_initial_cluster_splits -= 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 't':
				cParameters.grid_initial_recursion_depth += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				cParameters.grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'g':
				if (cParameters.grid_initial_recursion_depth > 0)
					cParameters.grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'G':
				if (cParameters.grid_max_relative_recursion_depth > 0)
					cParameters.grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'P':
				output_ClusterTreeInformation();
				break;

			case 'c':
				setup_RadialDamBreak(
						cParameters.simulation_dataset_breaking_dam_posx,
						cParameters.simulation_dataset_breaking_dam_posy,
						cParameters.simulation_dataset_breaking_dam_radius
					);
				break;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			default:
				return false;
		}

		return true;
	}


	/**
	 * handle gui mouse motion event
	 */
	bool gui_mouse_motion_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, cParameters.simulation_dataset_breaking_dam_radius);
		}

		return true;
	}


	/**
	 * handle mouse button event
	 */
	bool gui_mouse_button_down_event(
		T i_mouse_coord_x,	///< x-coordinate of mouse cursor
		T i_mouse_coord_y,	///< y-coordinate of mouse cursor
		int i_button			///< pressed button id
	)
	{
		return gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);
	}


	/**
	 * render boundaries. For SWE, this are the bathymetries
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{
		switch(i_bathymetry_visualization_method % 4)
		{
			case 1:
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				CHelper_GenericParallelVertexDataCommTraversals::action<
					CHyperbolicTypes::CVisualizationTypes::CNodeData,
					sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV,
					CSimulationHyperbolic_Cluster,
					sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::T>,
					sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV::CKernelClass
				>(
					&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
					cDomainClusters,
					[&](sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<0>::TRAV *i_traversator)
					{
#if 0
						i_traversator->cKernelClass.setup(
								cParameters.visualization_data0_scale_factorsurface_default_displacement,
								cParameters.visualization_data0_scale_factor,
								&cGlDrawTrianglesWithVertexAndNormalArray
							);
#endif
					}
				);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
#endif
				return "smooth";


			case 2:
			{
				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

				CDatasets *cDatasets = getDatasets();
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5, 0, cDatasets);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						T s = cGridDataArrays.dof_element[5][i];
						v[1] = (s+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cGlDrawTrianglesWithVertexAndNormalArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();

				return "benchmark value";
			}
				break;

			case 3:
				// do not render bathymetry
				return "blank";


			default:
			{
				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3, 0, nullptr);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = cGridDataArrays.dof_element[3][i]*
								cParameters.visualization_data1_scale_factor*
								cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cGlDrawTrianglesWithVertexAndNormalArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();
			}
				return "simple";
		}

		return "";
	}


	/**
	 * translate and scale vertex with specific visualization parameters
	 */
	inline void p_applyTranslateAndScaleToVertex(
			T *io_vertex
	)
	{
		io_vertex[0] = (io_vertex[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
		io_vertex[1] = (io_vertex[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;
		io_vertex[2] = (io_vertex[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_z;
	}



	/**
	 * store the DOFs and grid data to an array
	 */
	void storeDOFsToGridDataArrays(
			CGridDataArrays<3,6> *io_cGridDataArrays,
			int i_flags,
			int i_preprocessing_mode,
			CDatasets *i_cDatasets		///< datasets to get benchmark data
	)
	{
		i_flags |=
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::NORMALS;

		bool run_postprocessing =
				cParameters.visualization_scale_x != 1.0 ||
				cParameters.visualization_scale_y != 1.0 ||
				cParameters.visualization_scale_z != 1.0 ||

				cParameters.visualization_translate_x != 1.0 ||
				cParameters.visualization_translate_y != 1.0 ||
				cParameters.visualization_translate_z != 1.0;

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

				// We instantiate it right here to avoid any overhead due to split/join operations
				sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

				cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
				cOutputGridDataArrays.cKernelClass.setup(
						io_cGridDataArrays,
						i_cDatasets,
						offset,
						i_flags,
						i_preprocessing_mode
					);

				cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);


				/*
				 * postprocessing
				 */
				if (run_postprocessing)
				{
					for (size_t i = offset; i < offset+i_cGenericTreeNode->workload_in_subtree; i++)
					{
						T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

						for (int vn = 0; vn < 3; vn++)
						{
							p_applyTranslateAndScaleToVertex(v);
							v += 3;
						}

						if (i_flags & CGridDataArrays_Enums::VALUE0)
							io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;

						if (i_flags & CGridDataArrays_Enums::VALUE3)
							io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;

						if (i_flags & CGridDataArrays_Enums::VALUE5)
							io_cGridDataArrays->dof_element[5][i] *= cParameters.visualization_scale_z;
					}
				}
			}
		);

		assert(io_cGridDataArrays->number_of_triangle_cells == cParameters.number_of_local_cells);
	}


	virtual CDatasets *getDatasets() = 0;

	/**
	 * render water surface
	 */
	const char *render_DOFs(
		int i_surface_visualization_method,
		CCommonShaderPrograms &i_cCommonShaderPrograms
	)
	{
		CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

		const char *ret_str = nullptr;
		T *v;
		T *n;
		T t;

		switch(i_surface_visualization_method % 8)
		{
			case -1:
				return "none";
				break;

			case 1:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE1, 0, nullptr);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[1][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "VALUE1";
				break;

			case 2:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE2, 0, nullptr);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[2][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "VALUE2";
				break;

			case 3:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE3, 0, nullptr);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] = (cGridDataArrays.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "VALUE3";
				break;


#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==1
			case 4:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE0 + CGridDataArrays_Enums::VALUE4, 1, nullptr);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						v[1] += cParameters.visualization_translate_z;
						v[1] *= cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "aligned surface";
				break;
#endif

			case 5:
			{
				CDatasets *cDatasets = getDatasets();
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE5, 0, cDatasets);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						T s = cGridDataArrays.dof_element[5][i];
						v[1] = (s+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				ret_str = "benchmark value";
			}
				break;

			default:
				storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VALUE0 + CGridDataArrays_Enums::VALUE3, 0, nullptr);

				v = cGridDataArrays.triangle_vertex_buffer;
				n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						if (cGridDataArrays.dof_element[0][i] > 0)
							v[1] = (cGridDataArrays.dof_element[0][i] + cGridDataArrays.dof_element[3][i] + cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;
						else
							v[1] = std::numeric_limits<T>::infinity();

						v += 3;
						n += 3;
					}
				}

				ret_str = "h+b";
				break;
		}

		i_cCommonShaderPrograms.cHeightColorBlinn.use();
		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

		cGlDrawTrianglesWithVertexAndNormalArray.render(
				cGridDataArrays.triangle_vertex_buffer,
				cGridDataArrays.triangle_normal_buffer,
				cParameters.number_of_local_cells*3
			);

		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
		i_cCommonShaderPrograms.cHeightColorBlinn.disable();

		return ret_str;

#if 0
			case 5:
				// smooth renderer
#if 0

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
				i_cCommonShaderPrograms.cHeightColorBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();


				CHelper_GenericParallelVertexDataCommTraversals::action
				<
					CCluster_TreeNode_,
					CSimulationHyperbolic_Cluster,
					sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<CTsunamiSimulationTypes,0>::TRAV,
					CTsunamiSimulationTypes::TVisualizationVertexData,
					CStackAccessorMethodsTsunamiVertexData<CCluster_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
				>
				(
						&CSimulationHyperbolic_Cluster::cOpenGL_Vertices_Smooth_Cell_Tsunami,
						&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
						rootGenericTreeNode,
						[&](CCluster_TreeNode_ *node)
						{
							node->cCluster->cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(
									cParameters.visualization_water_surface_default_displacement,
									cParameters.visualization_data0_scale_factor,
									&cGlDrawTrianglesWithVertexAndNormalArray
							);
						}
				);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				i_cCommonShaderPrograms.cHeightColorBlinn.disable();
#endif

#endif
				return "smooth surface renderer with height color shader";

			case 6:
				// simple aligned renderer
				p_render_surfaceSmooth(i_cCommonShaderPrograms.cBlinn);
				return "smooth surface";
		}
#endif
		return "[none]";
	}


	/**
	 * render a wireframe
	 */
	void render_Wireframe(
			int i_visualization_render_wireframe,			///< submode
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader programs
		)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
		{
			CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

			storeDOFsToGridDataArrays(&cGridDataArrays, 0, 0, nullptr);

			T *v, *l;
			T *lines = new T[cParameters.number_of_local_cells*2*3*3];

			v = cGridDataArrays.triangle_vertex_buffer;
			l = lines;

			for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
			{
				for (int vn = 0; vn < 2; vn++)
				{
					l[0] = v[0];
					l[1] = v[2];
					l[2] = -v[1];
					l += 3;

					l[0] = v[3+0];
					l[1] = v[3+2];
					l[2] = -v[3+1];
					l += 3;

					v += 3;
				}

				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				v -= 6;

				l[0] = v[0];
				l[1] = v[2];
				l[2] = -v[1];
				l += 3;

				v += 9;
			}

			cCommonShaderPrograms.cBlinn.use();
			cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0,0,0.5));
			cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0,0,0));
			cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(0.1,0.1,0.1));


			cGlDrawWireframeFromVertexArray.initRendering();

			cGlDrawWireframeFromVertexArray.render(
					lines,
					cParameters.number_of_local_cells*3*2
				);

			cGlDrawWireframeFromVertexArray.shutdownRendering();
			cCommonShaderPrograms.cBlinn.disable();

			delete [] lines;
		}
	}


	/**
	 * highlight the cluster borders
	 */
	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,		///< submode
			CCommonShaderPrograms &i_cCommonShaderPrograms	///< shaders
	)
	{
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 1 || i_visualization_render_cluster_borders % 3 == 2)
		{
			CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_clusters*2);

			cDomainClusters.traverse_GenericTreeNode_Parallel(
				[&](CGenericTreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t id = cGridDataArrays.getNextTriangleCellStartId(2);

					T *v = &(cGridDataArrays.triangle_vertex_buffer[id*3*3]);

					v[0] = node->cTriangleFactory.vertices[0][0];
					v[1] = node->cTriangleFactory.vertices[0][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
					v += 3;

					v[0] = node->cTriangleFactory.vertices[1][0];
					v[1] = node->cTriangleFactory.vertices[1][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
					v += 3;

					v[0] = node->cTriangleFactory.vertices[1][0];
					v[1] = node->cTriangleFactory.vertices[1][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
					v += 3;

					v[0] = node->cTriangleFactory.vertices[2][0];
					v[1] = node->cTriangleFactory.vertices[2][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
					v += 3;

					v[0] = node->cTriangleFactory.vertices[2][0];
					v[1] = node->cTriangleFactory.vertices[2][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
					v += 3;

					v[0] = node->cTriangleFactory.vertices[0][0];
					v[1] = node->cTriangleFactory.vertices[0][1];
					v[2] = 0;
					p_applyTranslateAndScaleToVertex(v);
					std::swap(v[1], v[2]);
					v[2] = -v[2];
				}
			);

			GLSL::vec3 c(0.8, 0.1, 0.1);
			i_cCommonShaderPrograms.cBlinn.use();
			i_cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
			i_cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
			i_cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);

				glDisable(GL_DEPTH_TEST);
				glDisable(GL_CULL_FACE);

				cGlDrawWireframeFromVertexArray.initRendering();

				cGlDrawWireframeFromVertexArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_clusters*3*2
					);

				cGlDrawWireframeFromVertexArray.shutdownRendering();

				glEnable(GL_DEPTH_TEST);
				glEnable(GL_CULL_FACE);

			i_cCommonShaderPrograms.cBlinn.disable();
		}
	}



	/**
	 * render the cluster scan information
	 */
	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
#if CONFIG_ENABLE_SCAN_DATA
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 2)
		{
			cCommonShaderPrograms.cBlinn.use();

			cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cDomainClusters.traverse_GenericTreeNode_Serial(
					[&](CGenericTreeNode_ *i_cGenericTreeNode)
					{
						CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

						CHyperbolicTypes::CVisualizationTypes::TVisualizationVertexScalar vertex_buffer[3*3];

						int thread_mod_id;
						if (i_cGenericTreeNode->workload_thread_id == -1)
							thread_mod_id = 6;
						else
							thread_mod_id = (i_cGenericTreeNode->workload_thread_id+1) % 6;

						static const CHyperbolicTypes::CVisualizationTypes::TVisualizationVertexScalar ct[7][3] = {
								{1.0, 0.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 0.0, 1.0},
								{1.0, 1.0, 0.0},
								{1.0, 0.0, 1.0},
								{0.0, 1.0, 1.0},
								{1.0, 1.0, 1.0},
						};

						GLSL::vec3 c(ct[thread_mod_id][0], ct[thread_mod_id][1], ct[thread_mod_id][2]);

						cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);


						T *v = vertex_buffer;

						v[0] = node->cTriangleFactory.vertices[0][0];
						v[1] = -node->cTriangleFactory.vertices[0][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);
						v += 3;

						v[0] = node->cTriangleFactory.vertices[1][0];
						v[1] = -node->cTriangleFactory.vertices[1][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);
						v += 3;

						v[0] = node->cTriangleFactory.vertices[2][0];
						v[1] = -node->cTriangleFactory.vertices[2][1];
						v[2] = 0.001;
						p_applyTranslateAndScaleToVertex(v);
						std::swap(v[1], v[2]);


						static const T normals[3][3] = {
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0}
						};

						cGlDrawTrianglesWithVertexAndNormalArray.render(vertex_buffer, &(normals[0][0]), 3);
					}
				);

				CGlErrorCheck();

			cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();

			cCommonShaderPrograms.cBlinn.disable();
		}
#endif
	}



	/**
	 * render a smoothed surface using node based parallelization
	 */
	void p_render_surfaceSmooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if 0

#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0
	#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cShaderBlinn.use();
		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CCluster_TreeNode_,
			CSimulationHyperbolic_Cluster,
			sierpi::kernels::COpenGL_Vertices_Smooth_Cell_Tsunami<CHyperbolicTypes,0>::TRAV,
			CHyperbolicTypes::CVisualizationTypes::CVertexData,
			CStackAccessorMethodsTsunamiVertexData<CCluster_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
			&CSimulationHyperbolic_Cluster::cOpenGL_Vertices_Smooth_Cell_Tsunami,
			&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData_WaterSurface,
			rootGenericTreeNode,
			[&](CCluster_TreeNode_ *node)
			{
#if 0
	// TODO
			node->cCluster->cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(
					visualization_water_surface_default_displacement, visualization_data0_scale_factor,
					&cGlDrawTrianglesWithVertexAndNormalArray
			);
#endif
			}
		);

		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
		cShaderBlinn.disable();
	#endif

#else

		cShaderBlinn.use();
		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGenericTreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Cell_Tsunami<2>::TRAV cOpenGL_Vertices_Cell_Tsunami_aligned;

				cOpenGL_Vertices_Cell_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Cell_Tsunami_aligned.cKernelClass.setup(
						cParameters.visualization_water_surface_default_displacement,
						cParameters.visualization_data0_scale_factor,
						&cGlDrawTrianglesWithVertexAndNormalArray
					);

				cOpenGL_Vertices_Cell_Tsunami_aligned.action(node->cStacks);
			}
		);

		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
		cShaderBlinn.disable();

#endif

#endif
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T	*o_translate_x,	///< origin of domain in world-space
			T	*o_translate_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	)
	{
		*o_translate_x = cParameters.simulation_dataset_default_domain_translate_x;
		*o_translate_y = cParameters.simulation_dataset_default_domain_translate_y;

		*o_size_x = cParameters.simulation_dataset_default_domain_size_x;
		*o_size_y = cParameters.simulation_dataset_default_domain_size_y;
	}

};


#endif /* CSIMULATION_HYPERBOLIC_PARALLEL_HPP_ */
