/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CSimulation_MainGuiInterface.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_MAINGUIINTERFACE_HPP_
#define CSIMULATION_MAINGUIINTERFACE_HPP_

#include "mainvis/CCommonShaderPrograms.hpp"

/**
 * \brief Interface descriptions which have to be offered by the simulation class
 */
class CSimulation_MainGuiInterface
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * output cell data at given position
	 */
	virtual void debug_OutputCellData(
				T i_position_x,		///< x-coordinate of triangle cell
				T i_position_y		///< y-coordinate of triangle cell
		) = 0;

	/**
	 * output CCluster_EdgeComm_InformationAdjacentClusters for cluster at given position
	 */
	virtual void debug_OutputEdgeCommunicationInformation(
				T i_position_x,	///< x-coordinate of triangle cell
				T i_position_y	///< y-coordinate of triangle cell
		) = 0;

	/**
	 * output information about cluster at given position
	 */
	virtual void debug_OutputClusterInformation(
				T x,	///< x-coordinate of triangle cell
				T y		///< y-coordinate of triangle cell
		) = 0;

	/**
	 * handle key-down event which was not handled in main driver
	 */
	virtual bool gui_key_down_event(
			int i_key_id	///< key
		) = 0;


	/**
	 * handle key event which was not handled in main driver
	 */
	virtual bool gui_key_up_event(
			int i_key_id	///< key
		) = 0;

	/**
	 * handle mouse motion event which was not handled in main driver
	 */
	virtual bool gui_mouse_motion_event(
			T i_position_x,	///< mouse x-coordinate in 2D
			T i_position_y,	///< mouse y-coordinate in 2D
			int button		///< mouse button
		) = 0;

	/**
	 * handle mouse button down event which was not handled in main driver
	 */
	virtual bool gui_mouse_button_down_event(
			T i_mouse_coord_x,	///< mouse x-coordinate in 2D
			T i_mouse_coord_y,	///< mouse y-coordinate in 2D
			int button			///< mouse button
		) = 0;

	/**
	 * render some degree of freedom
	 *
	 * \return information string about what was rendered
	 */
	virtual const char* render_boundaries(
			int i_dof1_visualization_method,	///< sub-visualization method
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader program to use for rendering
		) = 0;

	/**
	 * render some degree of freedom
	 *
	 * \return information string about what was rendered
	 */
	virtual const char* render_DOFs(
			int i_dof2_visualization_method,	///< sub-visualization method
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader program to use for rendering
		) = 0;

	/**
	 * render triangle grid wireframe
	 */
	virtual void render_Wireframe(
			int i_wireframe_visualization_method,	///< sub-visualization method
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader program to use for rendering
		) = 0;

	/**
	 * render cluster borders
	 */
	virtual void render_ClusterBorders(
			int i_cluster_borders_visualization_method,	///< sub-visualization method
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader program to use for rendering
		) = 0;

	/**
	 * render cluster scans
	 */
	virtual void render_ClusterScans(
			int i_cluster_borders_visualization_method,	///< sub-visualization method
			CCommonShaderPrograms &cCommonShaderPrograms	///< shader program to use for rendering
		) = 0;

	/**
	 * load terrain origin coordinate and size
	 */
	virtual void getOriginAndSize(
			T	*o_origin_x,	///< origin of domain in world-space
			T	*o_origin_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	) = 0;

	virtual ~CSimulation_MainGuiInterface()
	{
	}
};


#endif /* CSIMULATION_MAININTERFACE_HPP_ */
