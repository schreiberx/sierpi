/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CCluster_ExchangeEdgeCommData.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CPARTITIONTREE_NODE_EDGECOMM_HPP
#define CPARTITIONTREE_NODE_EDGECOMM_HPP

#include "config.h"
#include "libsierpi/grid/CDomain_BaseTriangle.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"
#include <string.h>
#include <typeinfo>

#if CONFIG_ENABLE_MPI
	#include <mpi.h>
#endif


namespace sierpi
{

/**
 * this class handles the communication with the adjacent edges based on the adjacency
 * information given in CClusterTree_Node.
 */
template <	typename t_CCluster_TreeNode,
			typename TEdgeElement,
			typename CStackAccessors
		>
class CCluster_ExchangeEdgeCommData	: public CStackAccessors
{
private:
	typedef CCluster_EdgeComm_InformationAdjacentCluster<t_CCluster_TreeNode> CEdgeComm_InformationAdjacentCluster_;

private:
	CStack<TEdgeElement> *hypEdgeRecvStack;
	CStack<TEdgeElement> *catEdgeRecvStack;

#if CONFIG_ENABLE_MPI
	CStack<TEdgeElement> *hypEdgeSendStack;
	CStack<TEdgeElement> *catEdgeSendStack;
#endif

	/**
	 * Handler to root clusterTreeNode
	 */
	t_CCluster_TreeNode *cClusterTreeNode;

#if DEBUG && CONFIG_ENABLE_MPI
	/**
	 * this variable has to be false when calling exchangeEdgeCommData.
	 *
	 * true when exchangeEdgeCommData was executed.
	 *
	 * false after execution of  exchangeEdgeCommData_MPIWait.
	 */
	bool exchange_edge_comm_data_running;
#endif



	/**
	 * constructor
	 */
public:
	CCluster_ExchangeEdgeCommData(
			t_CCluster_TreeNode *p_cClusterTreeNode
	)	:
		hypEdgeRecvStack(nullptr),
		catEdgeRecvStack(nullptr)
#if DEBUG && CONFIG_ENABLE_MPI
		,
		exchange_edge_comm_data_running(false)
#endif
	{
		cClusterTreeNode = p_cClusterTreeNode;
	}



	/**
	 * return the information about where to find the communication data at the adjacent triangle stack
	 */
private:
	void _getEdgeCommDataPointersFromAdjacentCluster(
			CCluster_UniqueId &i_thisUniqueId,				///< unique id of cluster to write data to
			t_CCluster_TreeNode *i_adjacentClusterTreeNode,	///< pointer to adjacent cluster
			TEdgeElement **o_src_edgeElements,					///< output: pointer to starting point of data to be exchanged
			int *o_edgeElementCount,							///< output: number of elements stored on the stack
			bool *o_clockwise									///< output: true, if edge elements are stored with clockwise traversal
	)	const
	{
		int pos;

		/*
		 * first of all, we search on the hyp communication information to find the appropriate subset
		 */
		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
					i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			if (adjacent_info.comm_elements == 0)
				continue;
#endif

			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjHypStack;

				if (i_adjacentClusterTreeNode->cTriangleFactory.isHypotenuseDataOnLeftStack())
				{
					/*
					 * clockwise stack is always the left one
					 */
					adjHypStack = this->leftStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjHypStack = this->rightStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.comm_elements;
				*o_src_edgeElements = adjHypStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.comm_elements;
		}

		pos = 0;
		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
					i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != i_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &adjacent_info = *iter;

			if (adjacent_info.uniqueId == i_thisUniqueId)
			{
				CStack<TEdgeElement> *adjCatStack;
				if (i_adjacentClusterTreeNode->cTriangleFactory.isCathetusDataOnLeftStack())
				{
					adjCatStack = this->leftStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = true;
				}
				else
				{
					adjCatStack = this->rightStackAccessor(i_adjacentClusterTreeNode);
					*o_clockwise = false;
				}

				*o_edgeElementCount = adjacent_info.comm_elements;
				*o_src_edgeElements = adjCatStack->getElementPtrAtIndex(pos);

				return;
			}

			pos += adjacent_info.comm_elements;
		}

		std::cout << "FATAL ERROR: Communication partner not found! 1" << std::endl;
		assert(false);
	}



	/**
	 * this method is executed for every adjacent cluster simulation to
	 * pull the edge communication data from an adjacent cluster.
	 */
private:
	inline void _pullEdgeCommDataFromSharedMemoryCluster(
			CStack<TEdgeElement> *o_cStack,					///< stack to store edge element data to
			bool i_clockwise,								///< order in which to store data
			const CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster	///< information about adjacent clusters
	)
	{

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		if (i_informationAdjacentCluster.comm_elements == 0)
			return;
#endif

		TEdgeElement *src_edgeElements = NULL;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

		/*
		 * first we search for the adjacent cluster
		 */
		_getEdgeCommDataPointersFromAdjacentCluster(
					cClusterTreeNode->uniqueId,
					i_informationAdjacentCluster.clusterTreeNode,
					&src_edgeElements,
					&edgeElementCount,
					&adj_clockwise
				);

		if (i_informationAdjacentCluster.comm_elements != edgeElementCount)
		{
			std::cerr << "EDGE COMM FAILURE 2: comm Elements mismatch" << std::endl;
			std::cerr << " + cluster id: " << cClusterTreeNode->uniqueId << std::endl;
			assert(false);
		}

		if (i_clockwise != adj_clockwise)
		{
			/*
			 * directly copy stack elements
			 */
			memcpy(o_cStack->getStackPtr(), src_edgeElements, sizeof(TEdgeElement)*edgeElementCount);
		}
		else
		{
			/*
			 * reverse stack elements
			 */
			size_t adj_i = edgeElementCount-1;

			for (int i = 0; i < edgeElementCount; i++)
			{
				memcpy(&(o_cStack->getStackPtr()[i]), &(src_edgeElements[adj_i]), sizeof(TEdgeElement));
				adj_i--;
			}
		}
	}


#if CONFIG_ENABLE_MPI

	/**
	 * this method is executed to exchange MPI communication data
	 */
private:
	inline void _sendAndReceiveEdgeCommDataToDistributedMemoryCluster_pass1(
			CStack<TEdgeElement> *o_cStack,			///< stack to store edge element data to
			CStack<TEdgeElement> *i_cStack,			///< stack to load edge element data from
			CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
			unsigned long local_edge_comm_stack_index,			///< index to local edge comm stack to start copying data
			bool edge_clockwise
	)
	{

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		if (i_informationAdjacentCluster.comm_elements == 0)
			return;
#endif

		/*
		 * directly send and receive stack elements
		 */
#if 0
		std::cout << "MPI ISend:" << std::endl;
		std::cout << " + From: " << cClusterTreeNode->uniqueId << std::endl;
		std::cout << " + DST Rank: " << i_informationAdjacentCluster.mpi_rank << std::endl;
		std::cout << " + Tag: " << edge_clockwise << std::endl;
		std::cout << " + Size: " << i_informationAdjacentCluster.comm_elements << std::endl;
#endif
		/*
		 * Problem description for using pair of unique IDs:
		 *
		 * Clusters for Node 0: 10, 11
		 * Clusters for Node 1: 12
		 *
		 * Using SOURCE cluster id only:
		 *   Node 0:
		 *     MSGSend(rank=1, tag=10)	// send data for Cluster 10
		 *     MSGSend(rank=1, tag=11)	// send data for Cluster 11
		 *     MSGRecv(rank=0, tag=12)	// recv data from cluster 12
		 *     MSGRecv(rank=0, tag=12)	// recv data from cluster 12 (!!! NON-UNIQUE RECV !!!)
		 *   Node 1:
		 *     ...
		 *
		 * Using DESTINATION cluster id only:
		 *   Node 0:
		 *     MSGSend(rank=1, tag=12)	// send data for Cluster 10 to Cluster 12
		 *     MSGSend(rank=1, tag=12)	// send data for Cluster 11 to Cluster 12 (!!! NON-UNIQUE RECV !!!)
		 *     ...
		 *   Node 1:
		 *     ...
		 *
		 * To allow arbitrary order of execution of clusters, we have to use a combination
		 * of source and destination tags to create unique send/recv tags!
		 *
		 * Now one problem still remains:
		 * The maximum value of a tag can be restricted (see MPI spec) to 32768.
		 * Therefore we have to use the assumptions about the correct order of sending
		 * and receiving MPI messages by using appropriate base triangulations.
		 *
		 * Those base triangulations have to follow the Sierpinski SFC with their
		 * cluster being aligned at grids created by a recursively Sierpinski SFC
		 * based bisected base triangle.
		 *
		 * We further have to distinguish between the right an left aligned edge
		 * to allow periodic boundary conditions.
		 */

		i_informationAdjacentCluster.mpi_last_send_data_ptr = i_cStack->getElementPtrAtIndex(local_edge_comm_stack_index);

		MPI_Isend(
				i_informationAdjacentCluster.mpi_last_send_data_ptr,
				sizeof(TEdgeElement)*i_informationAdjacentCluster.comm_elements,	///< size
				MPI_BYTE,									///< data type
				i_informationAdjacentCluster.mpi_rank,		///< destination rank
				edge_clockwise,
				MPI_COMM_WORLD,								///< communicator
				&(i_informationAdjacentCluster.mpi_requests[0])
			);

		i_informationAdjacentCluster.mpi_last_recv_data_ptr = o_cStack->getElementPtrAtIndex(local_edge_comm_stack_index);
	}


	/**
	 * swap e1 and e2
	 */
	inline void swapEdgeElement(TEdgeElement *e1, TEdgeElement *e2)
	{
		TEdgeElement tmp;
		memcpy(&tmp, e1, sizeof(TEdgeElement));
		memcpy(e1, e2, sizeof(TEdgeElement));
		memcpy(e2, &tmp, sizeof(TEdgeElement));
	}


	/**
	 * this method is executed to wait for the MPI requests
	 */
private:
	void _receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
		CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster,	///< information about adjacent clusters
		bool edge_clockwise
	)
	{

#if 0
		std::cout << "MPI IRecv:" << std::endl;
		std::cout << " + Receiver: " << cClusterTreeNode->uniqueId << std::endl;
		std::cout << " + SRC Rank: " << i_informationAdjacentCluster.mpi_rank << std::endl;
		std::cout << " + Tag: " << edge_clockwise << std::endl;
		std::cout << " + Size: " << i_informationAdjacentCluster.comm_elements << std::endl;
#endif

#if 0
		std::cout << "MPI IRecv: SRC Rank: " << i_informationAdjacentCluster.mpi_rank << ", Tag: " << edge_clockwise << ", Size: " << i_informationAdjacentCluster.comm_elements << std::endl;
#if DEBUG
		MPI_Status status;

	    MPI_Probe(i_informationAdjacentCluster.mpi_rank, edge_clockwise, MPI_COMM_WORLD, &status);
		int size;
	    MPI_Get_count(&status, MPI_BYTE, &size);
		std::cout << "                                       MPI_RECV finished with size " << size << " " << i_informationAdjacentCluster.comm_elements << std::endl;
#endif
#endif

		MPI_Recv(
				i_informationAdjacentCluster.mpi_last_recv_data_ptr,					///< destination pointer
				sizeof(TEdgeElement)*i_informationAdjacentCluster.comm_elements,
				MPI_BYTE,
				i_informationAdjacentCluster.mpi_rank,
				edge_clockwise,					///< tag is set to the unique id of this cluster.
				MPI_COMM_WORLD,
				0
			);



		// assume that the adjacent clusters stack direction forces a reversal of the communication data

		/*
		 * reverse elements
		 */
		TEdgeElement *adjacent_edge_element = (TEdgeElement*)i_informationAdjacentCluster.mpi_last_recv_data_ptr;

		size_t switch_id = i_informationAdjacentCluster.comm_elements-1;
		for (int i = 0; i < i_informationAdjacentCluster.comm_elements/2; i++)
		{
			swapEdgeElement(&(adjacent_edge_element[i]), &(adjacent_edge_element[switch_id]));
			switch_id--;
		}

		MPI_Wait(&(i_informationAdjacentCluster.mpi_requests[0]), 0);
	}

#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

private:
	void validateEdgeCommDataLengthFromCluster(
			CStack<TEdgeElement> *i_stack,			///< stack to load edge element data from
			bool i_clockwise,							///< order in which to store data
			CEdgeComm_InformationAdjacentCluster_ &i_informationAdjacentCluster	///< information about adjacent clusters
	)	const
	{
		TEdgeElement *src_edgeElements = NULL;
		int edgeElementCount = 0;
		bool adj_clockwise = true;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		if (i_informationAdjacentCluster.comm_elements == 0)
			return;
#endif

		if (i_informationAdjacentCluster.clusterTreeNode != nullptr)
		{
			_getEdgeCommDataPointersFromAdjacentCluster(
						cClusterTreeNode->uniqueId,
						i_informationAdjacentCluster.clusterTreeNode,
						&src_edgeElements,
						&edgeElementCount,
						&adj_clockwise
					);

#if DEBUG
			if (i_informationAdjacentCluster.comm_elements != edgeElementCount)
			{
				std::cerr << "EDGE COMM FAILURE 1: comm Elements mismatch" << std::endl;
				std::cerr << " + cluster id: " << cClusterTreeNode->uniqueId << std::endl;
				assert(i_informationAdjacentCluster.comm_elements != edgeElementCount);
			}
#endif
		}
	}
#endif



	/**
	 * this method is executed by the simulation running on the cluster after global synch to
	 * read the communication stack elements from the adjacent clusters.
	 *
	 * for shared memory systems, this read is handled by accessing the adjacent clusters.
	 *
	 * for distributed memory systems (MPI), additional:
	 *   - send operation has to be executed for adjacent clusters stored
	 *     on a different node and a
	 *   - receive operation to read the adjacent data previously send by an adjacent node.
	 */
public:
	void exchangeEdgeCommData_SM()
	{
		bool hypClockwise, catClockwise;

		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			hypEdgeRecvStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			catEdgeRecvStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			hypEdgeRecvStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			catEdgeRecvStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}

		assert(hypEdgeRecvStack->isEmpty());
		assert(catEdgeRecvStack->isEmpty());

		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			/*
			 * besides a single if branch which is selected by setting const_int_UniqueIDDependendLoad to a fixed value,
			 * all other if branches are expected to be removed.
			 */
#if CONFIG_ENABLE_MPI
			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
			{
#endif

				_pullEdgeCommDataFromSharedMemoryCluster(
					hypEdgeRecvStack,
					hypClockwise,
					cEdgeComm_InformationAdjacentCluster
				);

#if CONFIG_ENABLE_MPI
			}
#endif

			hypEdgeRecvStack->incStackElementCounter(cEdgeComm_InformationAdjacentCluster.comm_elements);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

#if CONFIG_ENABLE_MPI
			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode != nullptr)
			{
#endif

				_pullEdgeCommDataFromSharedMemoryCluster(
						catEdgeRecvStack,
						catClockwise,
						cEdgeComm_InformationAdjacentCluster
					);

#if CONFIG_ENABLE_MPI
			}
#endif

			catEdgeRecvStack->incStackElementCounter(cEdgeComm_InformationAdjacentCluster.comm_elements);

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
	}


#if CONFIG_ENABLE_MPI
	/**
	 * Exchange the edge communication data on a distributed memory system.
	 *
	 * !!!
	 * IMPORTANT: THIS HAS TO BE EXECUTED SERIAL TO ASSURE CORRECT ORDER!!!
	 * !!!
	 */
	void exchangeEdgeCommData_DM_pass1()
	{
#if DEBUG
		assert(exchange_edge_comm_data_running == false);
		exchange_edge_comm_data_running = true;
#endif
		bool hypClockwise, catClockwise;

		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			hypEdgeRecvStack = this->exchangeLeftStackAccessor(cClusterTreeNode);
			catEdgeRecvStack = this->exchangeRightStackAccessor(cClusterTreeNode);

			hypEdgeSendStack = this->leftStackAccessor(cClusterTreeNode);
			catEdgeSendStack = this->rightStackAccessor(cClusterTreeNode);

			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			hypEdgeRecvStack = this->exchangeRightStackAccessor(cClusterTreeNode);
			catEdgeRecvStack = this->exchangeLeftStackAccessor(cClusterTreeNode);

			hypEdgeSendStack = this->rightStackAccessor(cClusterTreeNode);
			catEdgeSendStack = this->leftStackAccessor(cClusterTreeNode);

			hypClockwise = false;
			catClockwise = true;
		}

		assert(hypEdgeRecvStack->isEmpty());
		assert(catEdgeRecvStack->isEmpty());

		unsigned long local_edge_comm_stack_index;

		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			/*
			 * besides a single if branch which is selected by setting const_int_UniqueIDDependendLoad to a fixed value,
			 * all other if branches are expected to be removed.
			 */
			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_sendAndReceiveEdgeCommDataToDistributedMemoryCluster_pass1(
					hypEdgeRecvStack,	///< receive stack
					hypEdgeSendStack,	///< send stack
					cEdgeComm_InformationAdjacentCluster,
					local_edge_comm_stack_index,
					hypClockwise
				);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}


		local_edge_comm_stack_index = 0;

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = *iter;

			/*
			 * besides a single if branch which is selected by setting const_int_UniqueIDDependendLoad to a fixed value,
			 * all other if branches are expected to be removed.
			 */

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_sendAndReceiveEdgeCommDataToDistributedMemoryCluster_pass1(
					catEdgeRecvStack,	///< receive stack
					catEdgeSendStack,	///< send stack
					cEdgeComm_InformationAdjacentCluster,
					local_edge_comm_stack_index,
					catClockwise
				);
			}

			local_edge_comm_stack_index += cEdgeComm_InformationAdjacentCluster.comm_elements;
		}
	}



	void exchangeEdgeCommData_DM_pass2()
	{
#if DEBUG && CONFIG_ENABLE_MPI
		assert(exchange_edge_comm_data_running == true);
		exchange_edge_comm_data_running = false;
#endif
		bool hypClockwise, catClockwise;

		if (cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack())
		{
			hypClockwise = true;
			catClockwise = false;
		}
		else
		{
			hypClockwise = false;
			catClockwise = true;
		}


		int hyp_comm_data_size = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size();

		for (int i = hyp_comm_data_size-1; i >= 0; i--)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters[i];

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
						cEdgeComm_InformationAdjacentCluster,
						hypClockwise
					);
			}
		}


		int cat_comm_data_size = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();

		for (int i = cat_comm_data_size-1; i >= 0; i--)
		{
			CEdgeComm_InformationAdjacentCluster_ &cEdgeComm_InformationAdjacentCluster = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters[i];

			if (cEdgeComm_InformationAdjacentCluster.clusterTreeNode == nullptr)
			{
				_receiveEdgeCommDataFromDistributedMemoryCluster_pass2(
						cEdgeComm_InformationAdjacentCluster,
						catClockwise
					);
			}
		}
	}
#endif


#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

public:
	void validateCommDataLength()	const
	{
		bool hypClockwise = cClusterTreeNode->cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				iter++
		)
		{
			// hypEdgeStack was not initialized yet -> ignore
			if (hypEdgeRecvStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromCluster(hypEdgeRecvStack, hypClockwise, *iter);
		}


		bool catClockwise = cClusterTreeNode->cTriangleFactory.isCathetusDataOnAClockwiseStack();

		for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				iter++
		)
		{
			// catEdgeStack was not initialized yet -> ignore
			if (catEdgeRecvStack == nullptr)
				continue;

			validateEdgeCommDataLengthFromCluster(catEdgeRecvStack, catClockwise, *iter);
		}
	}
#endif

	/**
	 * update the sizes of the edge pieces which have to be exchanged with the adjacent clusters
	 *
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * ! this method is intended to update the edge communication information for adaptivity !
	 * ! AND                                                                                 !
	 * ! for the split/join information which is given as a parameter                        !
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 *
	 *
	 * stack elements have to be of type 'char'.
	 * 	-1: coarsening done
	 * 	 0: nothing
	 * 	 1: refinement done
	 */
public:
	void updateEdgeCommSizeAndSplitJoinInformation(
			CStack<char>* p_leftEdgeStack,
			CStack<char>* p_rightEdgeStack,
			CCluster_SplitJoin_EdgeComm_Information &p_adaptive_splitJoinInformation,
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation
	)
	{
		CStack<char> *catEdgeStack, *hypEdgeStack;

		bool even = (p_adaptive_splitJoinInformation.triangleFactory.evenOdd == CTriangle_Enums::EVEN);

		if (even)
		{
			catEdgeStack = p_leftEdgeStack;
			hypEdgeStack = p_rightEdgeStack;
		}
		else
		{
			catEdgeStack = p_rightEdgeStack;
			hypEdgeStack = p_leftEdgeStack;
		}

		CStackReaderTopDown<char> readerTopDown;

		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);
		assert(p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack != -1 || !p_adaptive_splitJoinInformation.splitting_permitted);

		/*******************************************************
		 * COPY-COPY-COPY
		 *******************************************************/

		// copy fixed variables
		p_splitJoinInformation.splitting_permitted = p_adaptive_splitJoinInformation.splitting_permitted;
		p_splitJoinInformation.joining_permitted = p_adaptive_splitJoinInformation.joining_permitted;

		// splitting and joining requests can be either triggered by the adaptive process (p_adaptive_splitJoinInformation) or by the user (p_splitJoinInformation)
		if (p_splitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::NO_OPERATION)
			p_splitJoinInformation.splitJoinRequests = p_adaptive_splitJoinInformation.splitJoinRequests;

		p_splitJoinInformation.update_edge_comm_required = p_adaptive_splitJoinInformation.update_edge_comm_required;


		/*******************************************************
		 * UPDATE OF EDGE COMM REALLY REQUIRED?
		 *******************************************************/
		if (p_splitJoinInformation.update_edge_comm_required == false)
			return;


		/*******************************************************
		 * HYPOTENUSE
		 *******************************************************/
		/*
		 * setup the adaptive edge comm updates for the hyp
		 */
		readerTopDown.setup(*hypEdgeStack);

		if (p_splitJoinInformation.splitting_permitted || p_splitJoinInformation.joining_permitted)
		{
			p_splitJoinInformation.number_of_shared_edge_comm_elements = p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

			// number of edge comm elements which have to be handled before switching to the second sub-triangle
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
						- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}


			if (cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0)
			{
#if DEBUG
				bool secondTriangleActive = false;
#endif
				/*
				 * iterate over the adjacency information about clusters next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
							cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
						iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentCluster_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_cluster_edge_comm_size = 0;

					/**
					 * variable to determine whether the previous edge comm element was
					 * one of a triangle which should be coarsened
					 */
					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent cluster
					 */
					for (int c = 0; c < i.comm_elements; c++)
					{
						if (edgeCommRemainingCounter == 0)
						{
#if DEBUG
							assert(secondTriangleActive == false);
							// switch to second triangle
#endif
							/*
							 * compute the remaining edge elements which have to be considered for this edge.
							 * for the second triangle, this is used as a variable to check whether all edge information
							 * is popped from the adjacency stacks.
							 */
							if (even)
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
							}
							else
							{
								edgeCommRemainingCounter =
										p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
										- p_adaptive_splitJoinInformation.number_of_shared_edge_comm_elements;

								/*
								 * switch counter about edge elements to edge element counter of second subtriangle.
								 */
								splitJoinInformationElementsOnStackPtr =
										&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
							}

							assert(edgeCommRemainingCounter != 0);
#if DEBUG
							secondTriangleActive = true;
#endif
						}

						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// one edge element less which we have to consider
						edgeCommRemainingCounter--;

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_cluster_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_cluster_edge_comm_size += 2;
							// comm elements are increased by one edgeComm element due to refinement
							*splitJoinInformationElementsOnStackPtr += 1;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_cluster_edge_comm_size += 1;

						/*
						 * comm elements are reduced by one edgeComm element
						 */
						*splitJoinInformationElementsOnStackPtr -= 1;
					}

					/**
					 * setup new edge communication size for this edge
					 */
					i.comm_elements = new_cluster_edge_comm_size;

					assert(within_coarsening == false);
				}

				assert(edgeCommRemainingCounter == 0);
			}
		}
		else
		{
			readerTopDown.setup(*hypEdgeStack);

			/*
			 * no further splitting possible
			 */

			if (cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0)
			{
				/*
				 * iterate over the adjacency information about clusters next to the _hypotenuse_
				 */
				for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter =
							cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
						iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
						iter++
				)
				{
					// convenient accessor
					CEdgeComm_InformationAdjacentCluster_ &i = *iter;

					// counter variable to track the new edge communication size
					unsigned int new_cluster_edge_comm_size = 0;

					bool within_coarsening = false;

					/*
					 * iterate over all edge communication elements for the current adjacent cluster
					 */
					for (int c = 0; c < i.comm_elements; c++)
					{
						/**
						 * trigger:
						 * 	 1: refine
						 *	 0: nothing
						 * 	-1: coarsen
						 */
						char trigger = readerTopDown.getNextData();

						// default: edge comm size is not modified for this edgeData
						if (trigger == 0)
						{
							new_cluster_edge_comm_size += 1;
							continue;
						}

						// refinement is triggered
						if (trigger == 1)
						{
							new_cluster_edge_comm_size += 2;
							continue;
						}

						// coarsening -> drop one element
						assert(trigger == -1);

						within_coarsening = !within_coarsening;

						if (!within_coarsening)
							continue;

						/*
						 * we increase the number of edge communication elements for the
						 * 2nd coarsened edge element only.
						 */
						new_cluster_edge_comm_size += 1;
					}

					assert(within_coarsening == false);

					/**
					 * setup new edge communication size for this edge
					 */
					i.comm_elements = new_cluster_edge_comm_size;
				}
			}
		}



		/*******************************************************
		 * CATHETUS
		 *******************************************************/
		if (p_splitJoinInformation.splitting_permitted)
		{
			int edgeCommRemainingCounter;

			// pointer to counter for elements on stack which is modified
			int *splitJoinInformationElementsOnStackPtr;

			if (even)
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			}
			else
			{
				// remaining edge comm elements to detect when to switch to next sub-triangle
				edgeCommRemainingCounter =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				// edge comm element counter which has to be updated
				p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

				p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack =
						p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				// pointer to current edge comm element counter to be modified during the edge comm analysis
				splitJoinInformationElementsOnStackPtr = &p_splitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;
			}

			readerTopDown.setup(*catEdgeStack);
#if DEBUG
			bool secondTriangleActive = false;
#endif
			/*
			 * iterate over the adjacency information about clusters next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentCluster_ &i = *iter;

				// compute new cluster communication size
				unsigned int new_cluster_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.comm_elements; c++)
				{
					if (edgeCommRemainingCounter == 0)
					{
#if DEBUG
						assert(secondTriangleActive == false);
						// switch to second triangle
#endif
						/*
						 * compute the remaining edge elements which have to be considered for this edge.
						 * for the second triangle, this is used as a variable to check whether all edge information
						 * is popped from the adjacency stacks.
						 */
						if (even)
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
						}
						else
						{
							edgeCommRemainingCounter =
									p_adaptive_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

							/*
							 * switch counter about edge elements to edge element counter of second subtriangle.
							 */
							splitJoinInformationElementsOnStackPtr =
									&p_splitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;
						}

						assert(edgeCommRemainingCounter != 0);
//						if (edgeCommRemainingCounter == 0)
//							break;
#if DEBUG
						secondTriangleActive = true;
#endif
					}

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// one edge element less which we have to consider
					edgeCommRemainingCounter--;

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_cluster_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_cluster_comm_size += 2;
						*splitJoinInformationElementsOnStackPtr += 1;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_cluster_comm_size += 1;

					*splitJoinInformationElementsOnStackPtr -= 1;
				}

				i.comm_elements = new_cluster_comm_size;

				assert(within_coarsening == false);
			}

			p_splitJoinInformation.first_triangle.number_of_elements = p_adaptive_splitJoinInformation.first_triangle.number_of_elements;
			p_splitJoinInformation.second_triangle.number_of_elements = p_adaptive_splitJoinInformation.second_triangle.number_of_elements;

			assert(edgeCommRemainingCounter == 0);
		}
		else
		{
			readerTopDown.setup(*catEdgeStack);

			/*
			 * iterate over the adjacency information about clusters next to the _cathetus_
			 */
			for (	typename std::vector<CEdgeComm_InformationAdjacentCluster_>::iterator iter = cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					iter != cClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			)
			{
				CEdgeComm_InformationAdjacentCluster_ &i = *iter;

				// compute new cluster communication size
				unsigned int new_cluster_comm_size = 0;

				bool within_coarsening = false;

				for (int c = 0; c < i.comm_elements; c++)
				{

					/**
					 * trigger:
					 * 	 1: refine
					 *	 0: nothing
					 * 	-1: coarsen
					 */
					char trigger = readerTopDown.getNextData();

					// default: edge comm size is not modified for this edgeData
					if (trigger == 0)
					{
						new_cluster_comm_size += 1;
						continue;
					}

					// refinement is triggered
					if (trigger == 1)
					{
						new_cluster_comm_size += 2;
						continue;
					}

					// coarsening -> drop one element
					assert(trigger == -1);

					within_coarsening = !within_coarsening;

					if (!within_coarsening)
						continue;

					new_cluster_comm_size += 1;
				}

				i.comm_elements = new_cluster_comm_size;
			}
		}
	}
};

}

#endif
