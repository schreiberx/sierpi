/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRAVERSATOR_VERTEXCOORDS_NORMALS_ELEMENTDATA_PARALLEL_HPP_
#define CTRAVERSATOR_VERTEXCOORDS_NORMALS_ELEMENTDATA_PARALLEL_HPP_

#include "libsierpi/stacks/CFBStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "libsierpi/triangle/CTriangle_Normals.hpp"

namespace sierpi
{
namespace travs
{

template <class t_CKernelClass, typename t_CTypes>
class CTraversator_VertexCoords_Normals_CellData
{
public:
	/**
	 * implementation of kernel class
	 */
	t_CKernelClass cKernelClass;

	/**
	 * a few typedefs for convenience
	 */
	typedef typename t_CTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CTypes::CSimulationTypes::T TVertexScalar;
	typedef typename t_CTypes::CSimulationStacks CSimulationStacks;
	typedef CTraversator_VertexCoords_Normals_CellData<t_CKernelClass, t_CTypes> TThisClass;

	/**
	 * stack and lifo handlers used in this traversator
	 */
	CStackReaderTopDown<char> structure_lifo_in;
	CStackReaderTopDown<CCellData> element_data_lifo;


	/**
	 * include automagically generated code
	 */
#define SIERPI_PARALLEL_CODE	1
#include "auto/CTraversator_VertexCoords_Normals_CellData_autogenerated.hpp"
#undef SIERPI_PARALLEL_CODE
#include "../CTraversatorClassInc_SetupSinglePass.hpp"


	void action(
			CSimulationStacks *i_cSimulationStacks
	)
	{
		CFBStacks<char> &structure_stacks = i_cSimulationStacks->structure_stacks;
		CFBStacks<CCellData> &element_data_stacks = i_cSimulationStacks->element_data_stacks;

		assert(structure_stacks.direction == element_data_stacks.direction);
		assert (structure_stacks.direction == CFBStacks<char>::FORWARD);

		structure_lifo_in.setup(structure_stacks.forward);
		element_data_lifo.setup(element_data_stacks.forward);

		cKernelClass.traversal_pre_hook();

#if CONFIG_TRAVERSATORS_WITH_NORMALS_AS_INTS
		(this->*sfcRecursiveMethod_Forward)(
				cTriangleFactory.vertices[0][0], cTriangleFactory.vertices[0][1],
				cTriangleFactory.vertices[1][0], cTriangleFactory.vertices[1][1],
				cTriangleFactory.vertices[2][0], cTriangleFactory.vertices[2][1],
				cTriangleFactory.hypNormal
			);
#else
		(this->*sfcRecursiveMethod_Forward)(
				cTriangleFactory.vertices[0][0], cTriangleFactory.vertices[0][1],
				cTriangleFactory.vertices[1][0], cTriangleFactory.vertices[1][1],
				cTriangleFactory.vertices[2][0], cTriangleFactory.vertices[2][1]
			);
#endif

		cKernelClass.traversal_post_hook();
	}
};

}
}

#endif /* CSTRUCTURE_VERTEXDATA_H_ */
