#! /bin/bash

cd ..

scons --xml-config=./scenarios/single_wave_on_simple_beach_1d.xml -j 4 || exit -1
CFL="0.5"
#CFL="0.1"

echo "Using CFL $CFL"

for d in `seq 0 20`; do
	MAX=`./build/sierpi_intel_omp_tsunami_1d_netcdf_b0_release -c scenarios/single_wave_on_simple_beach_1d.xml -d $d -a 0 -C $CFL | grep "MaxRunup:" | tail -n 1`
	echo "$d	$MAX"
done
