/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef CTRAVERSATORS_CSTRUCTURE_ELEMENT_SETUP_H_
#define CTRAVERSATORS_CSTRUCTURE_ELEMENT_SETUP_H_
/*
 * Include string to make strlen works
 */
#include <string.h>
#include "CFBStacks.hpp"

namespace sierp
{
namespace travs
{

template <typename TElement>
class CSetup_Structure_ElementData
{
public:
	CStack<char> *structure_stack;
	CStack<TElement> *element_data_stack;
	TElement *element_value;


	void setup(	CFBStacks<char> &p_structure_stacks,
				CFBStacks<TElement> &p_element_data_stacks,
				const char *structure_string,
				TElement *p_element_value,
				bool reversed = false
	)
	{
		element_value = p_element_value;

		if (p_structure_stacks.direction == p_structure_stacks.FORWARD)
		{
			structure_stack = &p_structure_stacks.forward;
			element_data_stack = &p_element_data_stacks.forward;
		}
		else
		{
			structure_stack = &p_structure_stacks.backward;
			element_data_stack = &p_element_data_stacks.backward;
		}

		if (reversed)
		{

			size_t length = strlen(structure_string);

			for (int i = length-1; i >= 0; i--)
			{
				if (structure_string[i] == '0')
				{
					structure_stack->push(0);
					element_data_stack->push(*element_value);
				}
				else
				{
					structure_stack->push(1);
				}
			}
		}
		else
		{
			for (const char *c = structure_string; *c != '\0'; c++)
			{
				if (*c == '0')
				{
					structure_stack->push(0);
					element_data_stack->push(*element_value);
				}
				else
				{
					structure_stack->push(1);
				}
			}
		}

		assert(structure_stack->structure_isValidQuad());
	}


	void setup(	CFBStacks<char> &p_structure_stacks,
				CFBStacks<TElement> &p_element_data_stacks,
				int depth,
				TElement *p_element_value)
	{
		assert(depth > 0);
		assert(p_structure_stacks.direction == p_element_data_stacks.direction);

		element_value = p_element_value;

		if (p_structure_stacks.direction == p_structure_stacks.FORWARD)
		{
			structure_stack = &p_structure_stacks.forward;
			element_data_stack = &p_element_data_stacks.forward;
		}
		else
		{
			structure_stack = &p_structure_stacks.backward;
			element_data_stack = &p_element_data_stacks.backward;
		}

		assert(structure_stack->isEmpty());
		assert(element_data_stack->isEmpty());

		pushStackElement(depth);
		pushStackElement(depth);
	}

private:
	void pushStackElement(int depth)
	{
		depth--;

		if (depth == 0)
		{
			structure_stack->push(0);
			element_data_stack->push_PtrValue(element_value);
			return;
		}

		pushStackElement(depth);
		pushStackElement(depth);

		// postfix push
		structure_stack->push(1);
	}
};

}
}

#endif /* CSTRUCTURE_SETUP_H_ */
