. ../tools.sh
. ../inc_environment_vars.sh

CPUS=`grep 'processor.*:' /proc/cpuinfo  | wc -l`

# used number of threads
THREADS=`seq $CPUS -1 1`

# set KMP_AFFINITY

# executable
EXEC="../../build/sierpi_intel_omp_tsunami_parallel_release"
