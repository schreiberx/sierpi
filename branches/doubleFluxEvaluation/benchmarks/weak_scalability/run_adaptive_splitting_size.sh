#! /bin/bash

. ../tools.sh

START_DEPTH=12

# adaptive splitting size
PARAMS="-u 10 -U 5"

echo "Problem size with different number of CPUs using function optimization"

echo "THREADS	INITIAL_DEPTH	MTPS	SPEEDUP_PER	TIMESTEPS	TIMESTEP_SIZE	SECONDS_FOR_SIMULATION	AVERAGE_TRIANGLES	AVERAGE_PARTITIONS"
for THREADS in $CPULIST; do
	THREADS=$((THREADS+1))
	DEPTH=$((THREADS-1+START_DEPTH))
	PARAMS_=" -d $DEPTH -n $THREADS $PARAMS $@"

	EXEC_CMD="$EXEC $PARAMS_"
	echo "$EXEC_CMD" 1>&2
	OUTPUT="`$EXEC_CMD`"

	VARNAME=MTPS$THREADS
	eval $VARNAME=`getValueFromString "$OUTPUT" "MTPS"`

	MTPS=$(eval echo "\$$VARNAME")
	SPEEDUP_PER=$(echo "scale=4; $MTPS/($MTPS1*$THREADS)" | bc)

	AVERAGE_TRIANGLES=`getValueFromString "$OUTPUT" "TPST"`
	AVERAGE_PARTITIONS=`getValueFromString "$OUTPUT" "PPST"`
	TIMESTEPS=`getValueFromString "$OUTPUT" "TS"`
	TIMESTEP_SIZE=`getValueFromString "$OUTPUT" "TSS"`
	SECONDS_FOR_SIMULATION=`getValueFromString "$OUTPUT" "SFS"`

	echo "$THREADS	$DEPTH	$MTPS	$SPEEDUP_PER	$TIMESTEPS	$TIMESTEP_SIZE	$SECONDS_FOR_SIMULATION	$AVERAGE_TRIANGLES	$AVERAGE_PARTITIONS"
done
