/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 3, 2011
 *      Author: schreibm
 */

#ifndef CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT
#define CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT

#include "libsierpi/traversators/vertices/CTraversator_Vertices_ElementData.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{

template <typename p_CSimulationStacks>
class CModify_OneElementValue_SelectByPoint
{
public:
	typedef typename p_CSimulationStacks::TElementData	TElementData;
	typedef float TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertex;

	typedef sierpi::travs::CTraversator_Vertices_ElementData<CModify_OneElementValue_SelectByPoint<p_CSimulationStacks>, p_CSimulationStacks> TRAV;

	TElementData *new_element_value;

	TVertexScalar px, py;


	inline CModify_OneElementValue_SelectByPoint()
	{
	}


	inline bool elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TElementData *element)
	{
		if (!CPointInTriangleTest<TVertexScalar>::test(vx1, vy1, vx2, vy2, vx3, vy3, px, py))
			return false;

		*element = *new_element_value;
		return true;
	}

	inline void setup(
			TVertexScalar p_px,
			TVertexScalar p_py,
			TElementData *p_new_element_value
	)
	{
		px = p_px;
		py = p_py;

		new_element_value = p_new_element_value;
	}

	inline void setup_WithKernel(
			CModify_OneElementValue_SelectByPoint &parent
	)
	{
		px = parent.px;
		py = parent.py;

		new_element_value = parent.new_element_value;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}
};

}
}

#endif /* CMODIFY_ONEVERTEX_HPP_ */
