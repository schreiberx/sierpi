/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef COPENGL_VERTICES_ZDEPTH_DISPLACEMENT_HPP_
#define COPENGL_VERTICES_ZDEPTH_DISPLACEMENT_HPP_

#include "libgl/incgl3.h"
#include "libsierpi/traversators/vertices/CTraversator_Vertices_Depth.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{


class COpenGL_Vertices_ZDepthDisplacement
{
public:
	typedef sierpi::travs::CTraversator_Vertices_Depth<sierpi::kernels::COpenGL_Vertices_ZDepthDisplacement> TRAV;

	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

private:
	TVertexScalar *vertex_buffer;
	TVertexScalar *current_triangle;
	TVertexScalar *last_triangle;
	size_t max_triangles;

public:

	inline COpenGL_Vertices_ZDepthDisplacement(
			size_t p_max_triangles = 16
	)	:
			max_triangles(p_max_triangles)
	{
		vertex_buffer = new TVertexScalar[3*3*max_triangles];
		last_triangle = vertex_buffer+3*3*(max_triangles-1);
	}

	inline ~COpenGL_Vertices_ZDepthDisplacement()
	{
		delete vertex_buffer;
	}

	inline void renderOpenGLVertexArray(size_t triangles)
	{
		glDrawArrays(GL_TRIANGLES, 0, triangles*3);
	}

	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			int depth)
	{
		TVertexScalar y = (TVertexScalar)(depth-2)*0.05;
		current_triangle[0*3+0] = vx1;
		current_triangle[0*3+1] = y;
		current_triangle[0*3+2] = -vy1;

		current_triangle[1*3+0] = vx2;
		current_triangle[1*3+1] = y;
		current_triangle[1*3+2] = -vy2;

		current_triangle[2*3+0] = vx3;
		current_triangle[2*3+1] = y;
		current_triangle[2*3+2] = -vy3;

		assert(current_triangle <= last_triangle);
		if (current_triangle == last_triangle)
		{
			renderOpenGLVertexArray(max_triangles);
			current_triangle = vertex_buffer;
			return;
		}

		current_triangle += 3*3;
	}

	inline void traversal_pre_hook()
	{
		current_triangle = vertex_buffer;

		glVertexAttrib3f(1, 0, 1, 0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vertex_buffer);
		glEnableVertexAttribArray(0);
	}

	inline void traversal_post_hook()
	{
		if (current_triangle > vertex_buffer)
		{
			renderOpenGLVertexArray(
					(size_t)(current_triangle-vertex_buffer)/(3*3));
		}

		glDisableVertexAttribArray(0);
	}
};

}
}

#endif /* CSTRUCTURE_VERTEXHANDLER_OPENGL_H_ */
