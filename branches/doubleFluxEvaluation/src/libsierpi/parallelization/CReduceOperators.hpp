/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CReduceOperators.hpp
 *
 *  Created on: Jul 26, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CREDUCEOPERATORS_HPP_
#define CREDUCEOPERATORS_HPP_

class CReduceOperators
{
public:
	template <typename T>
	inline static T AND(const T &a, const T &b)
	{
		return a && b;
	}

	template <typename T>
	inline static T OR(const T &a, const T &b)
	{
		return a || b;
	}

	template <typename T>
	inline static T ADD(const T &a, const T &b)
	{
		return a + b;
	}

	template <typename T>
	inline static T MAX(const T &a, const T &b)
	{
		return std::max<T>(a, b);
	}

	template <typename T>
	inline static T MIN(const T &a, const T &b)
	{
		return std::min<T>(a, b);
	}
};

#endif /* CREDUCEOPERATORS_H_ */
