/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_TreeNode.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_TREENODE_HPP_
#define CPARTITION_TREENODE_HPP_

#include "config.h"
#include "CPartition_SplitJoinInformation.hpp"
#include "CPartition_EdgeComm_InformationAdjacentPartitions.hpp"
#include "CPartition_SplitJoinActions.hpp"
#include "CPartition_UniqueId.hpp"
#include "CGenericTreeNode.hpp"
#include <vector>

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
#endif

/**
 * \brief Container for all data and methods which is needed to execute the computations on this subpartition
 * in parallel without requesting any data from the adjacent cells.
 *
 * All important features for parallel processing are included in this partition.
 * Among others, this basic features are:
 *  - cPartition_SplitJoinActions				- to run split and join operations
 *  - cPartition_SplitJoinInformation			- information which is necessary to do fast split/joins.
 *  											  this information is created during the last backward adaptive traversal.
 *  - cPartition_AdaptiveSplitJoinInformation	- split/join informations after adaptive step
 *  - cEdgeComm_InformationAdjacentPartitions	- information about adjacent partitions to communicate with
 *
 * A pointer to the Simulation_PartitionHandler is also stored which implements the application developer
 * handlers.
 *
 * Methods of GenericTreeNode are inherited directly to this class to get a node of the domain triangulation tree!
 */
template <typename CSimulation_SubPartitionHandler>
class CPartition_TreeNode
{
public:
	/*
	 * typedefs inherited from CSimulation_SubPartitionHandler !!!
	 */
	typedef CSimulation_SubPartitionHandler CSimulation_SubPartitionHandler_;
	typedef typename CSimulation_SubPartitionHandler::CEdgeData CEdgeData_;
	typedef typename CSimulation_SubPartitionHandler::CElementData CElementData_;

	typedef CGenericTreeNode<CPartition_TreeNode<CSimulation_SubPartitionHandler> > CGeneric_TreeNode_;

	/*
	 * more typedefs for convenience
	 */
	typedef CPartition_TreeNode<CSimulation_SubPartitionHandler> CPartition_TreeNode_;

	typedef CPartition_EdgeComm_InformationAdjacentPartition<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartition_;
	typedef CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartitions_;

	typedef CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> CSimulationStacks_;


public:
	/**
	 * user's simulation handler which actually runs all the computations
	 */
	CSimulation_SubPartitionHandler *cSimulation_SubPartitionHandler;

	/**
	 * generic tree node
	 */
	CGeneric_TreeNode_ *cGeneric_TreeNode;

	/**
	 * partition's unique ID
	 */
	CPartition_UniqueId uniqueId;

	/**
	 * all stacks we need during the simulation
	 */
	CSimulationStacks_ *cStacks;


	/**
	 * this flag is set to true, if the stacks are reused from a different node (the parent node)
	 * and should not be neither allocated nor deallocated
	 */
	bool reuse_parent_stacks;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	/**
	 * validation stacks needed?
	 */
	CValidationStacks *cValidationStacks;
#endif



	/**
	 * the instance of the triangle factory for this partition
	 *
	 * IMPORTANT: this is no reference!
	 */
	CTriangle_Factory triangleFactory;

	/**
	 * split and join actions
	 */
	CPartition_SplitJoinActions<CSimulation_SubPartitionHandler> cPartition_SplitJoinActions;

	/**
	 * split/join information which should be used for data exchange
	 */
	CPartition_SplitJoinInformation cPartition_SplitJoinInformation;

	/**
	 * split/join information right after the adaptive step
	 */
	CPartition_SplitJoinInformation cPartition_AdaptiveSplitJoinInformation;

	/**
	 * here the adjacency information to other partitions is stored
	 */
	CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> cEdgeComm_InformationAdjacentPartitions;


	/**
	 * constructor
	 *
	 * only initialize classes. other things like setting up the stack has to be handled by execution of different methods.
	 */
	CPartition_TreeNode(
			CTriangle_Factory &i_triangleFactory	///< the triangle factory for this triangle
	)	:
		cSimulation_SubPartitionHandler(nullptr),
		cGeneric_TreeNode(nullptr),
		cStacks(nullptr),
		reuse_parent_stacks(false),
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks(nullptr),
#endif
		triangleFactory(i_triangleFactory),
		cPartition_SplitJoinActions(*this),
		cPartition_SplitJoinInformation(triangleFactory),
		cPartition_AdaptiveSplitJoinInformation(triangleFactory)
	{
	}


	/**
	 * set the unique id of this partition
	 */
	void setUniqueId(
			CPartition_UniqueId &i_uniqueId		///< new unique id
	)
	{
		uniqueId = i_uniqueId;
	}



	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * !!! allocate and setup new SIMULATION PARTITION HANDLER !!!
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	void setup_SimulationPartitionHandler(
			bool initialSetup,					///< run the initial setup (allocate simulation partition handler and call constructor)
			CSimulation_SubPartitionHandler *i_subPartitionHandler_parameterSetup	///< partition handler to setup parameters
	)
	{
		if (cSimulation_SubPartitionHandler)
		{
			cSimulation_SubPartitionHandler->setup(this, i_subPartitionHandler_parameterSetup);
			return;
		}

		cSimulation_SubPartitionHandler = new CSimulation_SubPartitionHandler(this, i_subPartitionHandler_parameterSetup);
	}



	/**
	 * deconstructor
	 */
	virtual ~CPartition_TreeNode()
	{
		freeStacks();

		if (cSimulation_SubPartitionHandler)
		{
			delete cSimulation_SubPartitionHandler;
			cSimulation_SubPartitionHandler = nullptr;
		}
	}



	/**
	 * this method cares about the memory allocation for the stacks
	 */
	void setup_EmptyStacks(
			size_t i_max_depth_OR_elements_on_stack,	///< maximum number of elements stored on the stack
			unsigned int i_flags						///< flags to setup the stacks
	)
	{
		assert(cStacks == nullptr);

		cStacks = new CSimulationStacks_(
					i_max_depth_OR_elements_on_stack,
					i_flags
				);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		assert(cValidationStacks == nullptr);

		cValidationStacks = new CValidationStacks(	triangleFactory.maxDepth-triangleFactory.recursionDepthFirstRecMethod	);
#endif
	}



	/**
	 * setup stacks during SPLIT operation!
	 */
	void setup_SplittedChildStacks(
			CPartition_SplitJoinInformation &splitJoinInformation,
			bool i_reuse_parent_stacks
	)
	{
		reuse_parent_stacks = i_reuse_parent_stacks;

// never reuse parent's stacks for validation data
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks = new CValidationStacks(triangleFactory.maxDepth - triangleFactory.recursionDepthFirstRecMethod);
#endif

		/*
		 * allocate stacks
		 *
		 * only the simulation knows which stacks to use
		 */
		if (reuse_parent_stacks)
		{
			assert(triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD);

			cStacks = cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks;

			// set parent's stack to be reused by the child node
			cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks = nullptr;

			// second sub-triangle
			cStacks->structure_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements*2-1
				);

			cStacks->element_data_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements
				);
		}
		else
		{
			assert(cStacks == nullptr);

#if !ADAPTIVE_SUBPARTITION_STACKS
			cStacks = new CSimulationStacks_(
						// new maximum depth
						triangleFactory.maxDepth - triangleFactory.recursionDepthFirstRecMethod,
						CSimulationStacks_Enums::ELEMENT_STACKS			|
						CSimulationStacks_Enums::ADAPTIVE_STACKS		|
						CSimulationStacks_Enums::EDGE_COMM_STACKS		|
						CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS
					);
#endif

			if (triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_FIRST_CHILD)
			{
#if ADAPTIVE_SUBPARTITION_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.first_triangle.number_of_elements+(ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING/sizeof(CElementData_)),
							CSimulationStacks_Enums::ELEMENT_STACKS			|
							CSimulationStacks_Enums::ADAPTIVE_STACKS		|
							CSimulationStacks_Enums::EDGE_COMM_STACKS		|
							CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS
						);
#endif
				// first sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks->structure_stacks.forward,
						1,
						splitJoinInformation.first_triangle.number_of_elements*2-1
					);

				cStacks->element_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks->element_data_stacks.forward,
						0,
						splitJoinInformation.first_triangle.number_of_elements
					);
			}
			else if (triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD)
			{
#if ADAPTIVE_SUBPARTITION_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.second_triangle.number_of_elements+(ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING/sizeof(CElementData_)),
							CSimulationStacks_Enums::ELEMENT_STACKS			|
							CSimulationStacks_Enums::ADAPTIVE_STACKS		|
							CSimulationStacks_Enums::EDGE_COMM_STACKS		|
							CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS
						);
#endif

				// second sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks->structure_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements*2,
						splitJoinInformation.second_triangle.number_of_elements*2-1
					);

				cStacks->element_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cPartition_TreeNode->cStacks->element_data_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements,
						splitJoinInformation.second_triangle.number_of_elements
					);
			}
			else
			{
				assert(false);
			}
		}

	}



	/**
	 * setup stacks during JOIN operation!
	 */
	void setup_JoinedStacks(
			CPartition_SplitJoinInformation &p_splitJoinInformation,
			bool p_reuse_second_child_stacks
	)
	{
		CSimulationStacks_* cFirstChildStacks = cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cStacks;
		CSimulationStacks_* cSecondChildStacks = cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cStacks;

		assert(cStacks == nullptr);

		if (p_reuse_second_child_stacks)
		{
			size_t first_triangle_number_of_elements = cFirstChildStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG
			size_t second_triangle_number_of_elements = cSecondChildStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

			if (cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

			/*
			 * SECOND SUB-TRIANGLE
			 */
			/*
			 * the stack of the second child was reused => steal it
			 */
			cStacks = cSecondChildStacks;

			// set second child node stack to be not allocated
			cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cStacks = nullptr;


			/*
			 * FIRST SUB-TRIANGLE
			 */
			/*
			 * structure data
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);


			cStacks->structure_stacks.forward.push(1);


			/*
			 * element data
			 */
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cFirstChildStacks->element_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);
		}
		else
		{
			size_t first_triangle_number_of_elements = cFirstChildStacks->element_data_stacks.forward.getNumberOfElementsOnStack();
			size_t second_triangle_number_of_elements = cSecondChildStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG

			if (cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

#if !ADAPTIVE_SUBPARTITION_STACKS
			setup_EmptyStacks(triangleFactory.maxDepth-triangleFactory.recursionDepthFirstRecMethod, cFirstChildStacks->flags);
#else
			setup_EmptyStacks(first_triangle_number_of_elements+second_triangle_number_of_elements+(ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING/sizeof(CElementData_)), cFirstChildStacks->flags);
#endif
			/*
			 * SECOND SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cSecondChildStacks->structure_stacks.forward,
					0,
					second_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cSecondChildStacks->element_data_stacks.forward,
					0,
					second_triangle_number_of_elements
				);

			/*
			 * FIRST SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->element_data_stacks.forward.pushChunksFrom(
					cFirstChildStacks->element_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);

			/*
			 * parent split element
			 */
			cStacks->structure_stacks.forward.push(1);
		}
	}



	/**
	 * this function is called for each leaf nodes
	 *
	 * if a splitting is requested, the partition is split
	 */
	void fun_splitAtLeaves(
			bool splitAllWhenPossible = false
	)
	{
		// is a splitting permitted?
		if (!cSimulation_SubPartitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.splitting_permitted)
			return;

		if (!splitAllWhenPossible)
			if (!(cPartition_SplitJoinInformation.splitJoinRequests == CPartition_SplitJoinInformation_Enums::SPLIT))
				return;

		cPartition_SplitJoinActions.pass1_splitPartition();

		// reset split request flag
		cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::NO_OPERATION;
	}



	/**
	 * this function is executed for each mid-node of the tree
	 *
	 * it tests, whether both childs are leaf nodes and requestes a join operation in this case.
	 */
	static void fun_testAndJoinAtMidNodes(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode,
			bool i_joinAllWhenPossible = false
	)
	{
#if DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		if (i_cGeneric_TreeNode->base_triangulation_node)
			return;
#else
		if (i_cGeneric_TreeNode->cPartition_TreeNode == nullptr)
			return;
#endif

		/*
		 * 1) check whether both child nodes really exist.
		 *
		 * a single sub-partition cannot be joined
		 */
		CPartition_TreeNode_ *first_node = i_cGeneric_TreeNode->first_child_node->cPartition_TreeNode;
		CPartition_TreeNode_ *second_node = i_cGeneric_TreeNode->second_child_node->cPartition_TreeNode;

		if (first_node == nullptr)
			return;

		if (second_node == nullptr)
			return;


		/*
		 * 2) check if both child nodes are leaves
		 */
		if (!first_node->cGeneric_TreeNode->isLeaf())
			return;

		if (!second_node->cGeneric_TreeNode->isLeaf())
			return;

#if !DELETE_CPARTITIONS_DUE_TO_SPLIT_OPERATION
		CPartition_SplitJoinActions<CSimulation_SubPartitionHandler> &cPartition_SplitJoinActions = i_cGeneric_TreeNode->cPartition_TreeNode->cPartition_SplitJoinActions;
		assert(cPartition_SplitJoinActions.transferState == CPartition_SplitJoinActions_Enums::NO_TRANSFER);
#endif

		/*
		 * check if there was a split operation at the childs
		 */
		if (first_node->cPartition_SplitJoinActions.transferState == CPartition_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;

		if (second_node->cPartition_SplitJoinActions.transferState == CPartition_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;


		/*
		 * 3) check if both child nodes are permitted to join and also requested a join operation
		 */
		if (!first_node->cPartition_SplitJoinInformation.joining_permitted)
			return;

		if (!i_joinAllWhenPossible)
			if (!(first_node->cPartition_SplitJoinInformation.splitJoinRequests == CPartition_SplitJoinInformation_Enums::JOIN))
				return;

		if (!second_node->cPartition_SplitJoinInformation.joining_permitted)
			return;

		if (!i_joinAllWhenPossible)
			if (!(second_node->cPartition_SplitJoinInformation.splitJoinRequests == CPartition_SplitJoinInformation_Enums::JOIN))
				return;

		/*
		 * this method is executed by a static method since this class
		 * (CParititon_TreeNode) may be deleted due to a split operation
		 */
		CPartition_SplitJoinActions<CSimulation_SubPartitionHandler>::pass1_joinChildPartitions(i_cGeneric_TreeNode);
	}



	/**
	 * free stacks and reallocate them with empty stacks
	 */
	void resetStacks(
			unsigned int i_flags,		///< stack flags
			size_t initial_recursion_depth		///< initial recursion depth
	)
	{
		freeStacks();

#if ADAPTIVE_SUBPARTITION_STACKS
		setup_EmptyStacks((1 << initial_recursion_depth) + (ADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING/sizeof(CElementData_)), i_flags);
#else
		setup_EmptyStacks(triangleFactory.maxDepth-triangleFactory.recursionDepthFirstRecMethod, i_flags);
#endif
	}



	/**
	 * free the stacks
	 */
	void freeStacks()
	{
		if (cStacks != nullptr)
		{
			delete cStacks;
			cStacks = nullptr;
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		if (cValidationStacks != nullptr)
		{
			delete cValidationStacks;
			cValidationStacks = nullptr;
		}
#endif
	}




	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CPartition_TreeNode &p
	)
	{
		co << "UniqueId: " << p.uniqueId << std::endl;
		co << p.triangleFactory << std::endl;
		co << "splitJoinActions:" << std::endl;
		co << p.cPartition_SplitJoinActions << std::endl;
		co << std::endl;

		co << p.cEdgeComm_InformationAdjacentPartitions << std::endl;

		co << "***************************************************************" << std::endl;
		co << std::endl;

		return co;
	}

};

#endif
