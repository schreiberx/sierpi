/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CPartition_UniqueId.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CPARTITION_UNIQUEID_HPP_
#define CPARTITION_UNIQUEID_HPP_

#include "lib/intToBinString.hpp"

/**
 * \brief unique id for each partition
 *
 * uniqueIDs:
 * ==========
 *   - those are unique also for sub-triangles.
 *   - to get even unique IDs for the sub-triangles, they are bit-combined.
 *   - the maximum number of bits used for the unique root domain triangles IDs is given in by max_unique_id_depth
 *   - the root unique IDs are initialized with "(1 << max_unique_id_depth) + triangle_enum_id" in the class CDomain_BaseTriangulation
 *   - then during the split operation, the parent's uniqueID is shifted to the left by a single bit and the lowest bit
 *     is set to 0 or 1 to account for first/second child node.
 */
class CPartition_UniqueId
{
public:
	/**
	 * unique id of partition
	 */
	unsigned int rawUniqueId;

	/**
	 * unique id depth of partition to split up further partitions with an unique id
	 *
	 * if the partition is split up into 2 distinct triangles, their new ids are
	 * created by using the old uniqueId and using a prefix of 0 or 1 of the
	 * uniqueIdDepth-th less significant parent uniqueId
	 */
	unsigned int uniqueId_depth;

	/**
	 * constructor
	 */
public:
	CPartition_UniqueId()	:
		rawUniqueId(0),
		uniqueId_depth(0)
	{
	}

public:
	void setup(	int i_rawUniqueId,
				int i_uniqueId_depth
	)
	{
		rawUniqueId = i_rawUniqueId;
		assert(rawUniqueId != 0);
		uniqueId_depth = i_uniqueId_depth;
	}

	/**
	 * setup uniqueId by rawUniqueId
	 */
public:
	CPartition_UniqueId(int i_rawUniqueId)
	{
		rawUniqueId = i_rawUniqueId;

		assert(rawUniqueId != 0);

		uniqueId_depth = 0;
		for (int i = rawUniqueId; i > 0; i >>= 1)
			uniqueId_depth++;
	}

	/**
	 * setup uniqueId by other uniqueId
	 */
public:
	CPartition_UniqueId(
			const CPartition_UniqueId &cPartition_UniqueId
	)
	{
		rawUniqueId = cPartition_UniqueId.rawUniqueId;
		uniqueId_depth = cPartition_UniqueId.uniqueId_depth;
	}

	/**
	 * return the parents raw unique id
	 */
public:
	unsigned int getParentRawUniqueId()	const
	{
		return rawUniqueId >> 1;
	}

	/**
	 * return true, if the uniqueId was initialized
	 */
	bool isValid()	const
	{
		return rawUniqueId != 0;
	}


	/**
	 * setup uniqueId for this parent assuming that i_parent_cPartition_UniqueId
	 * is a child
	 */
public:
	void setupParentFromChild(
			CPartition_UniqueId &i_child_cPartition_UniqueId
	)
	{
		rawUniqueId = (i_child_cPartition_UniqueId.rawUniqueId >> 1);
		uniqueId_depth = i_child_cPartition_UniqueId.uniqueId_depth-1;
	}

	/**
	 * setup uniqueId for this child assuming that this is the first child
	 * and the uniqueId given as a parameter is the parent's uniqueId
	 */
public:
	void setupFirstChildFromParent(
			CPartition_UniqueId &i_parent_cPartition_UniqueId
	)
	{
		rawUniqueId = i_parent_cPartition_UniqueId.rawUniqueId << 1;
		uniqueId_depth = i_parent_cPartition_UniqueId.uniqueId_depth+1;
	}

	/**
	 * setup uniqueId for this child assuming that this is the first child
	 * and the uniqueId given as a parameter is the parent's uniqueId
	 */
public:
	void setupSecondChildFromParent(
			CPartition_UniqueId &i_parent_cPartition_UniqueId
	)
	{
		rawUniqueId = (i_parent_cPartition_UniqueId.rawUniqueId << 1) | 1;
		uniqueId_depth = i_parent_cPartition_UniqueId.uniqueId_depth+1;
	}


	/**
	 * return true if this is the first child
	 */
public:
	bool isFirstChild()	const
	{
		assert(rawUniqueId != 0);

		return (rawUniqueId & 1) == 0;
	}


	/**
	 * return true if this is the second child
	 */
public:
	bool isSecondChild()	const
	{
		assert(rawUniqueId != 0);

		return (rawUniqueId & 1) == 1;
	}

	friend
	inline
	bool operator==(CPartition_UniqueId &o1, CPartition_UniqueId &o2)
	{
		assert(o1.rawUniqueId != 0);
		assert(o2.rawUniqueId != 0);

		return (o1.rawUniqueId == o2.rawUniqueId);
	}


	friend
	inline
	bool operator!=(const CPartition_UniqueId &o1, const CPartition_UniqueId &o2)
	{
		assert(o1.rawUniqueId != 0);
		assert(o2.rawUniqueId != 0);

		return (o1.rawUniqueId != o2.rawUniqueId);
	}

	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			const CPartition_UniqueId &p
	)
	{
//		co << "UniqueId: " << p.rawUniqueId << " (" << intToBinString(p.rawUniqueId) << ") | " << p.uniqueId_depth;
		co << "(" << intToBinString(p.rawUniqueId) << " | " << p.uniqueId_depth << ")";
		return co;
	}
};

#endif /* CPARTITION_UNIQUEID_H_ */
