/*
 * CLinesFromSVG.h
 *
 *  Created on: Dec 7, 2011
 *      Author: schreibm
 */

#ifndef CLINESFROMSVG_H_
#define CLINESFROMSVG_H_

#include <list>
#include <string>
#include <vector>
#include <libxml/parser.h>


/**
 * \brief extract lines from SVG
 *
 *
 */
class CLinesFromSVG
{
public:

	std::list< std::list<std::vector<float> > > lineList;


	bool loadSVGFile(const char *filename);

public:
	bool setupTransform(std::string &transformString);

	bool searchDrawingArea(xmlNode * a_node);

	bool searchPath(xmlNode * a_node);

	bool newLineFromString(std::string &triangleString);

	CLinesFromSVG();

	virtual ~CLinesFromSVG();

	float transform_x, transform_y;
};

#endif /* CLINESFROMSVG_H_ */
