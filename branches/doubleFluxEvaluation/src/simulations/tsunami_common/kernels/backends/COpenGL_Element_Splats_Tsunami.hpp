/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COPENGL_ELEMENT_SPLATS_TSUNAMI_HPP_
#define COPENGL_ELEMENT_SPLATS_TSUNAMI_HPP_

#include "libgl/incgl3.h"
#include "COpenGL_Element_Splats_Root_Tsunami.hpp"
#include "libsierpi/traversators/vertices/CTraversator_Vertices_ElementData_Depth.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libgl/core/CGlBuffer.hpp"
#include "libgl/core/CGlVertexArrayObject.hpp"

namespace sierpi
{
namespace kernels
{

template <typename CSimulationStacks, typename p_TVertexScalar>
class COpenGL_Element_Splats_Tsunami
{
public:
	typedef p_TVertexScalar TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;
	typedef typename CSimulationStacks::TElementData TElementData;

	typedef sierpi::travs::CTraversator_Vertices_ElementData_Depth<COpenGL_Element_Splats_Tsunami<CSimulationStacks,TVertexScalar>, CSimulationStacks> TRAV;

	COpenGL_Element_Splats_Root_Tsunami<TVertexScalar> *cOpenGL_Element_Splats_Root_Tsunami;

private:
	GLfloat *point_buffer;
	GLfloat *current_triangle_point;

	GLfloat *last_triangle;
	size_t max_vertices;

	TVertexScalar scale_min;
	TVertexScalar scale_factor;


public:
	inline COpenGL_Element_Splats_Tsunami()
	{
		max_vertices = OPENGL_ELEMENT_SPLATS_MAX_VERTICES_IN_BUFFER;
		assert(max_vertices % 3 == 0);

		// allocate array storing 4 components per vertex
		point_buffer = new GLfloat[4*max_vertices];

		last_triangle = point_buffer+4*(max_vertices-3);
	}

	virtual inline ~COpenGL_Element_Splats_Tsunami()
	{
		delete [] point_buffer;
	}



	inline void renderOpenGLVertexArray()
	{
		cOpenGL_Element_Splats_Root_Tsunami->renderOpenGLVertexArray(
				point_buffer,
				(size_t)(current_triangle_point-point_buffer) >> 2
			);
	}


	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			int depth,
			TElementData *element
	)
	{
		TVertexScalar cathetus_length = getUnitCathetusLengthForDepth(depth)*(1024.0*2.0);
		TVertexScalar hyp_length = getUnitHypotenuseLengthForDepth(depth)*(1024.0*2.0);

		current_triangle_point[0*4+0] = (vx1+vx2)*0.5;
		current_triangle_point[0*4+1] = (vy1+vy2)*0.5;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		current_triangle_point[0*4+2] = element->dofs.h;
#else
		current_triangle_point[0*4+2] = element->hyp_edge.h;
#endif
		current_triangle_point[0*4+3] = hyp_length;

		current_triangle_point[1*4+0] = (vx2+vx3)*0.5;
		current_triangle_point[1*4+1] = (vy2+vy3)*0.5;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		current_triangle_point[0*4+2] = element->dofs.h;
#else
		current_triangle_point[1*4+2] = element->right_edge.h;
#endif
		current_triangle_point[1*4+3] = cathetus_length;

		current_triangle_point[2*4+0] = (vx3+vx1)*0.5;
		current_triangle_point[2*4+1] = (vy3+vy1)*0.5;
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		current_triangle_point[0*4+2] = element->dofs.h;
#else
		current_triangle_point[2*4+2] = element->left_edge.h;
#endif
		current_triangle_point[2*4+3] = cathetus_length;

		current_triangle_point += 3*4;

		if (current_triangle_point > last_triangle)
		{
			renderOpenGLVertexArray();
			current_triangle_point = point_buffer;
			return;
		}

		CGlErrorCheck();
	}


	inline void traversal_pre_hook()
	{
		current_triangle_point = point_buffer;
	}


	inline void traversal_post_hook()
	{
		if (current_triangle_point > point_buffer)
		{
			renderOpenGLVertexArray();
		}
	}


	void setup_WithKernel(
			COpenGL_Element_Splats_Tsunami &parent_kernel
	)
	{
		scale_min = parent_kernel.scale_min;
		scale_factor = parent_kernel.scale_factor;
		cOpenGL_Element_Splats_Root_Tsunami = parent_kernel.cOpenGL_Element_Splats_Root_Tsunami;
	}


	inline void setup_WithParameters(
			const TVertexScalar p_min,
			const TVertexScalar p_max,
			COpenGL_Element_Splats_Root_Tsunami<TVertexScalar> *p_cOpenGL_Element_Splats_Root_Tsunami
	)
	{
		scale_min = p_min;
		scale_factor = 0.2f/(p_max-p_min);
		cOpenGL_Element_Splats_Root_Tsunami = p_cOpenGL_Element_Splats_Root_Tsunami;
	}
};


}
}

#endif
