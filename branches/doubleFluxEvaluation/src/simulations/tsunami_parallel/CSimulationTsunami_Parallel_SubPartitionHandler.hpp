/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 4, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CTSUNAMI_PARALLEL_PARTITION_HANDLER_HPP_
#define CTSUNAMI_PARALLEL_PARTITION_HANDLER_HPP_

#include "config.h"

// typedefs for types related to basic tsunami simulation
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Element_Splats_Tsunami.hpp"
#endif

// specialized traversators (to speed up compilation due to parallel processing)
#include "../tsunami_common/traversators/CSpecialized_Tsunami_EdgeComm_Normals_Depth.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element.hpp"
#include "../tsunami_common/traversators/CSpecialized_Tsunami_Setup_Column.hpp"

// structure setup traversator
#include "libsierpi/traversators/setup/CSetup_Structure_ElementData.hpp"

#include "libsierpi/parallelization/CPartition_TreeNode.hpp"
#include "libsierpi/domain_triangulation/CDomain_BaseTriangulation.hpp"
#include "libsierpi/parallelization/CPartition_ExchangeEdgeCommData.hpp"

#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_ElementData_Normal_SelectByPoint.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
	#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif

#include "../tsunami_common/kernels/backends/COutputVTK_Vertices_Element_Tsunami.hpp"

#include "libsierpi/parallelization/CStackAccessorMethods.hpp"


/**
 * \brief Tsunami Simulation SubPartition Handler
 *
 * This class implements all kinds of sub-partition related user-defined data storages.
 */
class CSimulationTsunami_Parallel_SubPartitionHandler
{
public:
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_SubPartitionHandler> CPartition_TreeNode_;
	typedef CPartition_EdgeComm_InformationAdjacentPartitions<CPartition_TreeNode_> CEdgeComm_InformationAdjacentPartition_;

	typedef CTsunamiElementData	CElementData;
	typedef CTsunamiEdgeData	CEdgeData;

	typedef CSimulationStacks<CElementData, CEdgeData> CTsunamiSimulationStacks;

	/**
	 * pointer to handler of this partition node
	 */
	CPartition_TreeNode_ *cPartition_TreeNode;


	/// mouse interactions
	sierpi::kernels::CModify_OneElementValue_SelectByPoint<CTsunamiSimulationStacks>::TRAV cModify_OneElementValue_SelectByPoint;

	/// element-wise debug output
	sierpi::kernels::CStringOutput_ElementData_Normal_SelectByPoint<CTsunamiSimulationStacks>::TRAV cStringOutput_ElementData_SelectByPoint;

	/// parallel setup
	sierpi::travs::CSetup_Structure_ElementData<CTsunamiSimulationStacks> cSetup_Structure_ElementData;

#if COMPILE_SIMULATION_WITH_GUI
	/// render
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,2>::TRAV cOpenGL_Vertices_Element_Tsunami_aligned;
	sierpi::kernels::COpenGL_Vertices_Element_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar,0>::TRAV cOpenGL_Vertices_Element_Tsunami_simple;
	sierpi::kernels::COpenGL_Vertices_Wireframe_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar>::TRAV cOpenGL_Vertices_Wireframe_Tsunami;
	sierpi::kernels::COpenGL_Element_Splats_Tsunami<CTsunamiSimulationStacks,TTsunamiVertexScalar>::TRAV cOpenGL_Element_Splats_Tsunami;
#endif

	sierpi::kernels::COutputVTK_Vertices_Element_Tsunami<CTsunamiSimulationStacks>::TRAV cOutputVTK_Vertices_Element_Tsunami;

	/**
	 * TIMESTEP
	 */
	/// edge communication and simulation without adaptivity
	sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth cTsunami_EdgeComm;


	/**
	 * INTER-PARTITION-EDGE COMMUNICATION
	 */
	/// handler for edge communication with adjacent partitions
	CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, CTsunamiEdgeData, CStackAccessorMethodsTsunamiEdgeData<CPartition_TreeNode_, CTsunamiEdgeData> > cPartition_ExchangeEdgeCommData_Tsunami;


	/**
	 * TSUNAMI ADAPTIVITY
	 */
	/// adaptivity - refine/coarsen
	sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element cTsunami_Adaptive;


	/**
	 * WATER COLUMN SETUP ADAPTIVITY
	 */
	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Tsunami_Setup_Column cSetup_Column;

	/**
	 * EDGE COMM FOR "WATER COLUMN" AND "TSUNAMI" ADAPTIVITY
	 */
	/// handler for adaptivity information exchange with adjacent partitions
	CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessorMethodsAdaptivityEdgeData<CPartition_TreeNode_> > cPartition_ExchangeEdgeCommData_Adaptivity;


#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	// validate edge communication
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;

	/// handler for edge communication with adjacent partitions
	CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, CValEdgeData, CStackAccessorMethodsValidationEdgeData<CPartition_TreeNode_> > cPartitionTree_Node_EdgeComm_Validation;
#endif


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	// this traversal is used to setup the element data vertex coordinates during the initial setup
	sierpi::kernels::CSetup_TsunamiElementData_Validation::TRAV cSetup_TsunamiElementData_Validation;
#endif


	/**
	 * fast accessor for triangle factory
	 */
	CTriangle_Factory &triangleFactory;



	/**
	 * constructor for setup
	 *
	 * this constructor is called whenever a split or merge operation is executed.
	 */
	CSimulationTsunami_Parallel_SubPartitionHandler(
			CPartition_TreeNode_ *p_cPartitionTreeNode,							///< pointer to partition tree node of this partition
			CSimulationTsunami_Parallel_SubPartitionHandler *i_subPartitionHandler_parameterSetup	///< partition handler to setup parameters
		)	:
		cPartition_TreeNode(p_cPartitionTreeNode),
		cPartition_ExchangeEdgeCommData_Tsunami(p_cPartitionTreeNode),
		cPartition_ExchangeEdgeCommData_Adaptivity(p_cPartitionTreeNode),
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cPartitionTree_Node_EdgeComm_Validation(p_cPartitionTreeNode),
#endif
		triangleFactory(p_cPartitionTreeNode->triangleFactory)

	{
		setup(p_cPartitionTreeNode, i_subPartitionHandler_parameterSetup);
	}



	/**
	 * setup method to initialize traversal methods and parameters
	 */
	void setup(
			CPartition_TreeNode_ *p_cPartitionTreeNode,							///< pointer to partition tree node of this partition
			CSimulationTsunami_Parallel_SubPartitionHandler *i_subPartitionHandler_parameterSetup	///< partition handler to setup parameters
		)
	{
		if (i_subPartitionHandler_parameterSetup == nullptr)
		{
			/*
			 * this has to be a domain root sub-partition and its parameters
			 * are set-up at another place.
			 */
#if COMPILE_SIMULATION_WITH_GUI
			cOpenGL_Vertices_Element_Tsunami_simple.setup_sfcMethods(triangleFactory);
			cOpenGL_Vertices_Element_Tsunami_aligned.setup_sfcMethods(triangleFactory);
			cOpenGL_Vertices_Wireframe_Tsunami.setup_sfcMethods(triangleFactory);
			cOpenGL_Element_Splats_Tsunami.setup_sfcMethods(triangleFactory);
#endif
			cOutputVTK_Vertices_Element_Tsunami.setup_sfcMethods(triangleFactory);

			cStringOutput_ElementData_SelectByPoint.setup_sfcMethods(triangleFactory);

			cModify_OneElementValue_SelectByPoint.setup_sfcMethods(triangleFactory);
			cSetup_Column.setup_sfcMethods(triangleFactory);

			cTsunami_EdgeComm.setup_sfcMethods(triangleFactory);
			cTsunami_Adaptive.setup_sfcMethods(triangleFactory);
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			cEdgeComm_ValidateComm.setup_sfcMethods(triangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
			cSetup_TsunamiElementData_Validation.setup_sfcMethods(triangleFactory);
#endif
			return;
		}


		/**
		 * SETUP traversals and parameters based on another node
		 */
#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Vertices_Element_Tsunami_simple.setup_Partition(i_subPartitionHandler_parameterSetup->cOpenGL_Vertices_Element_Tsunami_simple, triangleFactory);
		cOpenGL_Vertices_Element_Tsunami_aligned.setup_Partition(i_subPartitionHandler_parameterSetup->cOpenGL_Vertices_Element_Tsunami_aligned, triangleFactory);
		cOpenGL_Vertices_Wireframe_Tsunami.setup_Partition(i_subPartitionHandler_parameterSetup->cOpenGL_Vertices_Wireframe_Tsunami, triangleFactory);
		cOpenGL_Element_Splats_Tsunami.setup_Partition(i_subPartitionHandler_parameterSetup->cOpenGL_Element_Splats_Tsunami, triangleFactory);
#endif
		cOutputVTK_Vertices_Element_Tsunami.setup_Partition(i_subPartitionHandler_parameterSetup->cOutputVTK_Vertices_Element_Tsunami, triangleFactory);

		cStringOutput_ElementData_SelectByPoint.setup_Partition(i_subPartitionHandler_parameterSetup->cStringOutput_ElementData_SelectByPoint, triangleFactory);

		cModify_OneElementValue_SelectByPoint.setup_Partition(i_subPartitionHandler_parameterSetup->cModify_OneElementValue_SelectByPoint, triangleFactory);
		cSetup_Column.setup_Partition(i_subPartitionHandler_parameterSetup->cSetup_Column, triangleFactory);

		cTsunami_EdgeComm.setup_Partition(i_subPartitionHandler_parameterSetup->cTsunami_EdgeComm, triangleFactory);

		cTsunami_Adaptive.setup_Partition(i_subPartitionHandler_parameterSetup->cTsunami_Adaptive, triangleFactory);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cEdgeComm_ValidateComm.setup_Partition(i_subPartitionHandler_parameterSetup->cEdgeComm_ValidateComm, triangleFactory);
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		cSetup_TsunamiElementData_Validation.setup_Partition(i_subPartitionHandler_parameterSetup->cSetup_TsunamiElementData_Validation, triangleFactory);
#endif
	}

};



#endif
