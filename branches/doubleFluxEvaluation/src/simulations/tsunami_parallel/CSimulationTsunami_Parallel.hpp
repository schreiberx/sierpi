/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CSIMULATIONTSUNAMI_PARALLEL_HPP_
#define CSIMULATIONTSUNAMI_PARALLEL_HPP_

#include "CSimulationTsunami_Parallel_SubPartitionHandler.hpp"
#include "CSimulationTsunami_DomainSetups.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "../tsunami_common/CSimulationCommon.hpp"
#include "libmath/CVector.hpp"
#include "lib/CStopwatch.hpp"

#include "libsierpi/parallelization/CHelper_GenericParallelAdaptivityTraversals.hpp"
#include "libsierpi/parallelization/CHelper_GenericParallelEdgeCommTraversals.hpp"
#include "libsierpi/domain_triangulation/CBaseTriangulation_To_GenericTree.hpp"

#include "libsierpi/parallelization/CSplitJoinTuning.hpp"
#include "CSplitJoin_TsunamiTuningTable.hpp"

#if COMPILE_SIMULATION_WITH_GUI
	#include "libgl/shaders/shader_blinn/CShaderBlinn.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Wireframe_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Element_Splats_Root_Tsunami.hpp"
	#include "../tsunami_common/kernels/backends/COpenGL_Vertices_Element_Root_Tsunami.hpp"
#endif

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	#include "../tsunami_common/types/CTsunamiTypes.hpp"
	#include "../tsunami_common/kernels/CSetup_TsunamiElementData_Validation.hpp"
#endif



/**
 * \brief Main class for parallel Tsunami Simulation
 *
 * This class is the central point of a parallel Tsunami simulation.
 *
 * It manages all sub-partitions, creates the initial domain triangulation and
 * sets up the simulation.
 */
class CSimulationTsunami_Parallel	: public CSimulationCommon
{
	/**
	 * Convenient typedefs
	 */
	typedef CPartition_TreeNode<CSimulationTsunami_Parallel_SubPartitionHandler> CPartition_TreeNode_;

	/*
	 * Typedefs. among others used by partition handler
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;
	typedef TTsunamiVertexScalar TVertexScalar;
	typedef CGenericTreeNode<CPartition_TreeNode_> CGenericTreeNode_;


public:
	/**
	 * Generic tsunami configuration
	 */
//	CSimulationTsunami_Config config;

	/**
	 * Base domain triangulation
	 */
	CDomain_BaseTriangulation<CSimulationTsunami_Parallel_SubPartitionHandler> cDomain_RootTriangulation;

	/**
	 * Pointer to root generic node
	 */
	CGenericTreeNode_ *rootGenericTreeNode;


	/**
	 * Abstraction to handle the adaptive traversals for simulation by one function call
	 */
	CGenericParallelAdaptivityTraversals<
		CPartition_TreeNode_,
		CSimulationTsunami_Parallel_SubPartitionHandler,
		sierpi::travs::CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element,
		CStackAccessorMethodsAdaptivityEdgeData<CPartition_TreeNode_>
	> cParallelAdaptivityTraversals_Tsunami;


	/**
	 * Abstraction to handle the adaptive traversals for column setup by one function call
	 */
	CGenericParallelAdaptivityTraversals<
		CPartition_TreeNode_,
		CSimulationTsunami_Parallel_SubPartitionHandler,
		sierpi::travs::CSpecialized_Tsunami_Setup_Column,
		CStackAccessorMethodsAdaptivityEdgeData<CPartition_TreeNode_>
	> cParallelAdaptivityTraversals_SetupColumn;


	/**
	 * Abstraction to handle the edge comm traversals for a timestep
	 */
	CGenericParallelEdgeCommTraversals<
		CPartition_TreeNode_,
		CSimulationTsunami_Parallel_SubPartitionHandler,
		sierpi::travs::CSpecialized_Tsunami_EdgeComm_Normals_Depth,
		CTsunamiEdgeData,
		CStackAccessorMethodsTsunamiEdgeData<CPartition_TreeNode_, CTsunamiEdgeData>,
		TTsunamiDataScalar,	// reduce value
		TTsunamiDataScalar	// timestep size
	> cParallelEdgeCommTraversals_Tsunami;

#if COMPILE_SIMULATION_WITH_GUI
	COpenGL_Element_Splats_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Element_Splats_Root_Tsunami;
	COpenGL_Vertices_Element_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Element_Root_Tsunami;
	COpenGL_Vertices_Wireframe_Root_Tsunami<TTsunamiVertexScalar> cOpenGL_Vertices_Wireframe_Root_Tsunami;

	CGlVertexArrayObject render_partitions_vao;
	CGlBuffer render_partitions_buffer;
#endif


	/**
	 * Join/split all partitions during next step
	 */
	bool joinAllPartitionsInNextStep;
	bool splitAllPartitionsInNextStep;

	/**
	 * tuning for the split and join parameters
	 */
	CSplitJoinTuning<CSplitJoin_TsunamiTuningTable> cSplitJoinTuning;



#if COMPILE_SIMULATION_WITH_GUI
	/**
	 * set callback handler which has to be executed when parameters on gui have to be updated
	 */
	void(*callback_update_gui_func)(void *user_data);
	void *callback_update_gui_func_user_data;
#endif



	/**
	 * Start the parallel Tsunami simulation
	 */
	CSimulationTsunami_Parallel()	:
		 rootGenericTreeNode(nullptr),
		 joinAllPartitionsInNextStep(false),
		 splitAllPartitionsInNextStep(false)
#if COMPILE_SIMULATION_WITH_GUI
		,
		callback_update_gui_func(nullptr),
		callback_update_gui_func_user_data(nullptr)
#endif
	{
//		setSplitJoinSizes(50, 25);
	}


	/**
	 * free allocated data
	 */
	void clear()
	{
		/**
		 * free generic tree
		 */
		if (rootGenericTreeNode != nullptr)
		{
			delete rootGenericTreeNode;
			rootGenericTreeNode = nullptr;
		}
	}


	/**
	 * Constructor
	 */
	~CSimulationTsunami_Parallel()
	{
		clear();
	}


	/**
	 * Reset the simulation
	 */
	void reset_Simulation()
	{
		clear();

		/**
		 * reset the world: setup triangulation of "scene"
		 */
		setup_World_PartitionTreeNodes_SimulationPartitionHandlers(world_scene_id);


		/**
		 * reset the partition handlers for each triangle of the basic
		 * triangulation created by resetWorld()
		 */
		reset_PartitionHandlers();


#if COMPILE_SIMULATION_WITH_GUI
		cOpenGL_Element_Splats_Root_Tsunami.setupTexture(512, 512);
		cOpenGL_Element_Splats_Root_Tsunami.setupRegion(cDomain_RootTriangulation.region);

		if (cOpenGL_Element_Splats_Root_Tsunami.error())
		{
			std::cout << cOpenGL_Element_Splats_Root_Tsunami.error << std::endl;
		}

		render_partitions_vao.bind();
			render_partitions_buffer.bind();
			render_partitions_buffer.resize(6*3*sizeof(GLfloat));

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(0);
		render_partitions_vao.unbind();
#endif


		/**
		 * to the split/join after a full reset!!!
		 */
		unsigned long long old_number_of_triangles = 0;
		while (old_number_of_triangles != number_of_triangles)
		{
			old_number_of_triangles = number_of_triangles;
			setup_SplitJoinPartitions();
		}

		resetCommon();
	}


private:
	/**
	 * Setup:
	 *  - new world triangulation
	 *  - root partition tree nodes
	 *  - simulation partition handlers
	 */
	void setup_World_PartitionTreeNodes_SimulationPartitionHandlers(int i = 0)
	{
		cDomain_RootTriangulation.clear();

		switch(i)
		{
			case -4:	CSimulationTsunami_DomainSetups::setupTriangulation_2DCube_PeriodicBoundaries(cDomain_RootTriangulation);		break;
			case -3:	CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small_even_and_odd.svg", 0.5);		break;
			case -2:	CSimulationTsunami_DomainSetups::setupTriangulation_Strip_PeriodicBoundaries(cDomain_RootTriangulation, 10);				break;
			case -1:	CSimulationTsunami_DomainSetups::setupTriangulation_QuadQuad_PeriodicBoundaries(cDomain_RootTriangulation);				break;

			case 0:		CSimulationTsunami_DomainSetups::setupTriangulation_Triangle(cDomain_RootTriangulation);			break;
			case 1:		CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles1(cDomain_RootTriangulation);		break;
			case 2:		CSimulationTsunami_DomainSetups::setupTriangulation_2OddTriangles2(cDomain_RootTriangulation);		break;
			case 3:		CSimulationTsunami_DomainSetups::setupTriangulation_Quad(cDomain_RootTriangulation);				break;
			case 4:		CSimulationTsunami_DomainSetups::setupTriangulation_3Triangles(cDomain_RootTriangulation);			break;
			case 5:		CSimulationTsunami_DomainSetups::setupTriangulation_QuadTiles(cDomain_RootTriangulation, 1, 1);		break;
			case 6:		CSimulationTsunami_DomainSetups::setupTriangulation_TriangleStrip(cDomain_RootTriangulation);		break;
			case 7:		CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small.svg", 0.5);		break;
			case 8:		CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_small_odd.svg", 0.5);	break;
			case 9:		CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain.svg");					break;
			case 10:	CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_triangle.svg", 2.0);			break;
			case 11:	CSimulationTsunami_DomainSetups::setupTriangulation_SVG(cDomain_RootTriangulation, DOMAIN_FILE_PATH"test_domain_cross.svg", 0.5);		break;

			default:	CSimulationTsunami_DomainSetups::setupTriangulation_QuadTiles(cDomain_RootTriangulation, 1, 1);		break;
		}

		/*
		 * remove triangles which are available for domain triangulations with odd depth
		 */
		if (initial_recursion_depth & 1)
		{
			cDomain_RootTriangulation.fixForOddDepth();
		}

		/*
		 * setup the adjacency informations
		 */
		cDomain_RootTriangulation.setup_AdjacencyInformation();


		/*
		 * convert to generic tree
		 */
		CBaseTriangulation_To_GenericTree<CSimulationTsunami_Parallel_SubPartitionHandler> cBaseTriangulation_To_GenericTree;

		number_of_triangles = cBaseTriangulation_To_GenericTree.setup_GenericTree_From_BaseTriangulation(
				cDomain_RootTriangulation,		///< root triangles
				initial_recursion_depth,		///< initial recursion depth
				initial_recursion_depth-min_relative_recursion_depth,
				initial_recursion_depth+max_relative_recursion_depth,
				rootGenericTreeNode						///< reference to rootNode handed back!
			);

		assert(rootGenericTreeNode != nullptr);
	}



private:
	/**
	 * reset the partition handlers to specific values.
	 *
	 * especially care about the initialization of the structure stacks!
	 */
	void reset_PartitionHandlers()
	{
		/***************************************************************************************
		 * STACKS: setup the stacks (only the memory allocation) of the partitions
		 ***************************************************************************************/
		unsigned int stackInitializationFlags =
					CSimulationStacks_Enums::ELEMENT_STACKS						|
					CSimulationStacks_Enums::ADAPTIVE_STACKS					|
					CSimulationStacks_Enums::EDGE_COMM_STACKS					|
					CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS
				;

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->resetStacks(stackInitializationFlags, initial_recursion_depth);
				}
		);


		/***************************************************************************************
		 * SETUP STRUCTURE STACK and ELEMENT DATA
		 ***************************************************************************************/

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->resetStacks(stackInitializationFlags, initial_recursion_depth);
					node->cSimulation_SubPartitionHandler->cSetup_Structure_ElementData.setup(node->cStacks, initial_recursion_depth, &element_data_setup);
				}
		);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		/***************************************************************************************
		 * SETUP vertex coordinates in element data for debugging purposes
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cSetup_TsunamiElementData_Validation.action(node->cStacks);
				}
			);
#endif


		/***************************************************************************************
		 * EDGE COMM: setup boundary parameters
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cTsunami_EdgeComm.setBoundaryDirichlet(&dirichlet_boundary_edge_data);
				}
			);

		/***************************************************************************************
		 * EDGE COMM: setup generic parameters
		 ***************************************************************************************/

		auto fun = [=](CPartition_TreeNode_ *node)
		{
			node->cSimulation_SubPartitionHandler->cTsunami_EdgeComm.setParameters(timestep_size, domain_length, (TTsunamiDataScalar)9.81);
		};
		rootGenericTreeNode->traverse_PartitionTreeNode_MidAndLeafNodes<true>(fun, fun);


		/***************************************************************************************
		 * ADAPTIVITY
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cTsunami_Adaptive.setup_KernelClass(
							domain_length,

							refine_height_threshold,
							coarsen_height_threshold,

							refine_slope_threshold,
							coarsen_slope_threshold
						);
				}
			);


#if COMPILE_SIMULATION_WITH_GUI

		/***************************************************************************************
		 * VISUALIZATION
		 ***************************************************************************************/
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cOpenGL_Vertices_Element_Tsunami_simple.kernelClass.setup(
							surface_base_height, surface_max_height,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);
				}
			);

		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cOpenGL_Vertices_Element_Tsunami_aligned.kernelClass.setup(
							surface_base_height, surface_max_height,
							&cOpenGL_Vertices_Element_Root_Tsunami
						);
				}
			);

		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cOpenGL_Vertices_Wireframe_Tsunami.kernelClass.setup(
							surface_base_height, surface_max_height,
							&cOpenGL_Vertices_Wireframe_Root_Tsunami
						);
				}
			);

		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cOpenGL_Element_Splats_Tsunami.kernelClass.setup_WithParameters(
							surface_base_height, surface_max_height,
							&cOpenGL_Element_Splats_Root_Tsunami
						);
				}
			);
#endif
	}



/***************************************************************************************
 * ADAPTIVITY
 ***************************************************************************************/
private:
	void p_adaptive_timestep()
	{
		number_of_triangles = cParallelAdaptivityTraversals_Tsunami.action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cTsunami_Adaptive,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeEdgeCommData_Adaptivity,
				rootGenericTreeNode,
				partition_split_workload_size,
				partition_join_workload_size
			);
	}

	unsigned long long p_adaptive_setup_column(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius
	)
	{
		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cSetup_Column.setup_KernelClass(
							x,
							y,
							radius,

							&element_data_modifier,
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
							&element_data_modifier.dofs,
#else
							&element_data_modifier.hyp_edge,
#endif

							&element_data_setup,
#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS == 0
							&element_data_setup.dofs
#else
							&element_data_setup.hyp_edge
#endif
						);
				}
			);

		unsigned long long prev_number_of_triangles;
		do
		{
			prev_number_of_triangles = number_of_triangles;

			number_of_triangles = cParallelAdaptivityTraversals_SetupColumn.action(
					&CSimulationTsunami_Parallel_SubPartitionHandler::cSetup_Column,
					&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeEdgeCommData_Adaptivity,
					rootGenericTreeNode,
					partition_split_workload_size,
					partition_join_workload_size
				);

		} while (prev_number_of_triangles != number_of_triangles);

		return number_of_triangles;
	}



/***************************************************************************************
 * MODIFY_SINGLE_ELEMENT
 ***************************************************************************************/
private:
	void p_setup_element_data(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			CTsunamiElementData *elementData
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cModify_OneElementValue_SelectByPoint.kernelClass.setup(
							x,
							y,
							elementData
						);

					node->cSimulation_SubPartitionHandler->cModify_OneElementValue_SelectByPoint.action(node->cStacks);
				}
			);
	}



/***************************************************************************************
 * EDGE COMM
 ***************************************************************************************/

private:
	void p_edge_comm_timestep()
	{
		if (adaptive_timestep_size)
		{
			// adaptive timestep size

			timestep_size = cParallelEdgeCommTraversals_Tsunami.reduceValue*SIMULATION_TSUNAMI_CFL;

			if (timestep_size < 0.000001)
				timestep_size = 0.000001;
		}

		cParallelEdgeCommTraversals_Tsunami.action(
				&CSimulationTsunami_Parallel_SubPartitionHandler::cTsunami_EdgeComm,
				&CSimulationTsunami_Parallel_SubPartitionHandler::cPartition_ExchangeEdgeCommData_Tsunami,
				rootGenericTreeNode,
				timestep_size
			);
	}



/***************************************************************************************
 * SPLIT/JOINS for SETUP
 *
 * This splits all sub-partitions into appropriate sizes for the initialization.
 ***************************************************************************************/

public:
	void setup_SplitJoinPartitions()
	{
		p_adaptive_timestep();
		splitOrJoinPartitions();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


	inline void updateSplitJoinSizes()
	{
		if (partition_update_split_join_size_after_elapsed_timesteps != 0)
		{
			if (timestep_nr % partition_update_split_join_size_after_elapsed_timesteps == 0)
			{
				if (partition_update_split_join_size_after_elapsed_scalar != 0)
				{
					/**
					 * compute the split/join sizes by using `partition_update_split_join_size_after_elapsed_scalar`
					 */
					partition_split_workload_size = std::sqrt((double)number_of_triangles)*partition_update_split_join_size_after_elapsed_scalar;
					partition_join_workload_size = partition_split_workload_size / 2;
				}
				else
				{
					/**
					 * lookup the best split/join in a table
					 */
					cSplitJoinTuning.updateSplitJoin(
							partition_split_workload_size,
							partition_join_workload_size,
							number_of_triangles
						);
				}

#if 0
				std::cout << "Update Split/Join workload size: " << partition_split_workload_size << "/" << partition_join_workload_size << std::endl;
#endif
			}
		}

	}


/***************************************************************************************
 * TIMESTEP
 ***************************************************************************************/

public:
	inline void runSingleTimestep()
	{
		// simulation timestep
		p_edge_comm_timestep();

		// adaptive timestep
		p_adaptive_timestep();

		// split/join operations
		splitOrJoinPartitions();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		timestep_nr++;
		time_for_timestep += timestep_size;

		updateSplitJoinSizes();
	}



	CStopwatch cStopwatch;

	/***************************************************************************************
	 * run a single TIMESTEP and update the detailed benchmarks
	 ***************************************************************************************/
public:
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,
			double *io_adaptiveTime,
			double *io_splitJoinTime
			)
	{
		// simulation timestep
		cStopwatch.start();
		p_edge_comm_timestep();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		// adaptive timestep
		cStopwatch.start();
		p_adaptive_timestep();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		// split/join operations
		cStopwatch.start();
		splitOrJoinPartitions();
		*io_splitJoinTime += cStopwatch.getTimeSinceStart();

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		timestep_nr++;
		time_for_timestep += timestep_size;

		updateSplitJoinSizes();
	}


/***************************************************************************************
 * REFINE / COARSEN
 ***************************************************************************************/


public:
	/**
	 * setup column at 2d position with given radius
	 */
	unsigned long long setup_ColumnAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		return p_adaptive_setup_column(
				x,
				y,
				radius
			);
	}

public:
	/**
	 * setup element data at given 2d position
	 */
	void setup_ElementDataAt2DPosition(
			TTsunamiVertexScalar x,
			TTsunamiVertexScalar y,
			TTsunamiVertexScalar radius = 0.3
	)
	{
		p_setup_element_data(x, y, &element_data_modifier);
	}

#if 0
public:
	/**
	 * coarsen element at given 2d position
	 */
	void coarsenAt2DPosition(
			float p_x,
			float p_y
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cModify_OneElementValue_SelectByPoint.kernelClass.setup(p_x, p_y, &element_data_setup);
					node->cSimulation_SubPartitionHandler->cModify_OneElementValue_SelectByPoint.action(node->cStacks);
				}
		);

		p_adaptive_timestep();
	}
#endif


/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	void writeTrianglesToVTKFile(const char *p_filename)
	{
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(p_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_handler_partitions << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;
		vtkfile << "ASCII" << std::endl;
		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_triangles*3) << " float" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOutputVTK_Vertices_Element_Tsunami.kernelClass.setup(&vtkfile);
					node->cSimulation_SubPartitionHandler->cOutputVTK_Vertices_Element_Tsunami.action(node->cStacks);
				}
		);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "TRIANGLE_STRIPS " << (number_of_triangles) << " " << (number_of_triangles*4) << std::endl;
		for (unsigned long long i = 0; i < number_of_triangles; i++)
			vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

		vtkfile.close();
	}



	void writePartitionsToVTKFile(const char *p_filename)
	{
		char *i_additional_vtk_info_string = NULL;

		std::ofstream vtkfile;
		vtkfile.open(p_filename);

		vtkfile << "# vtk DataFile Version 5.0" << std::endl;
		vtkfile << "Sierpi VTK File, " << initial_recursion_depth << " initial_recursion_depth, " << number_of_triangles << " triangles, " << number_of_simulation_handler_partitions << "partitions";
		if (i_additional_vtk_info_string != NULL)
			vtkfile << ": " << i_additional_vtk_info_string;
		vtkfile << std::endl;
		vtkfile << "ASCII" << std::endl;
		vtkfile << "DATASET POLYDATA" << std::endl;

		// output 3 x #triangles vertices
		vtkfile << "POINTS " << (number_of_simulation_handler_partitions*3) << " float" << std::endl;

		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					vtkfile << node->triangleFactory.vertices[0][0] << " " << 0 << " " << -node->triangleFactory.vertices[0][1] << std::endl;
					vtkfile << node->triangleFactory.vertices[1][0] << " " << 0 << " " << -node->triangleFactory.vertices[1][1] << std::endl;
					vtkfile << node->triangleFactory.vertices[2][0] << " " << 0 << " " << -node->triangleFactory.vertices[2][1] << std::endl;
				}
		);

		// output 3 x #triangles vertices
		vtkfile << std::endl;
		vtkfile << "TRIANGLE_STRIPS " << (number_of_simulation_handler_partitions) << " " << (number_of_simulation_handler_partitions*4) << std::endl;
		for (unsigned long long i = 0; i < number_of_simulation_handler_partitions; i++)
			vtkfile << "3 " << (i*3+0) << " " << (i*3+1) << " " << (i*3+2) << std::endl;

		vtkfile.close();
	}


public:
/*****************************************************************************************************************
 * DEBUG: output element data
 *****************************************************************************************************************/
	void debugOutputElementData(
			const CVector<2,float> &planePosition
	)
	{
		// TRAVERSAL
		rootGenericTreeNode->traverse_PartitionTreeNode<false>(
			[&](CPartition_TreeNode_ *node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *partitionHandler = node->cSimulation_SubPartitionHandler;

					std::cout << "PartitionAndStackInformation:" << std::endl;
					std::cout << "  + Structure Stacks.direction = " << node->cStacks->structure_stacks.direction << std::endl;
					std::cout << "  + Structure Stacks.forward.getStackElementCounter() = " << node->cStacks->structure_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + Structure Stacks.backward.getStackElementCounter() = " << node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + ElementData Stacks.direction = " << node->cStacks->element_data_stacks.direction << std::endl;
					std::cout << "  + ElementData Stacks.forward.getStackElementCounter() = " << node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + ElementData Stacks.backward.getStackElementCounter() = " << node->cStacks->element_data_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + EdgeComm Stacks.left = " << (void*)node->cStacks->edge_data_comm_left_edge_stack.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm Stacks.right = " << (void*)node->cStacks->edge_data_comm_right_edge_stack.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.left = " << (void*)node->cStacks->edge_data_comm_exchange_left_edge_stack.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.right = " << (void*)node->cStacks->edge_data_comm_exchange_right_edge_stack.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + splitJoin.elements_in_first_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.first_triangle.number_of_elements << std::endl;
					std::cout << "  + splitJoin.elements_in_second_triangle" << partitionHandler->cPartition_TreeNode->cPartition_SplitJoinInformation.second_triangle.number_of_elements << std::endl;
					std::cout << std::endl;
				}
		);
	}

/***************************************************************************************
 * DEBUG: output information about triangle partition
 ***************************************************************************************/
public:
	void debugOutputPartition(
			const CVector<2,float> &planePosition
	)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
				{
					if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
							node->triangleFactory.vertices[0][0], node->triangleFactory.vertices[0][1],
							node->triangleFactory.vertices[1][0], node->triangleFactory.vertices[1][1],
							node->triangleFactory.vertices[2][0], node->triangleFactory.vertices[2][1],
							planePosition.data[0], planePosition.data[1]
						))
							return;

					std::cout << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << "* Partition information at " << planePosition << ":" << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << std::endl;
					std::cout << *node << std::endl;
					std::cout << node->cPartition_SplitJoinInformation << std::endl;
				}
		);
	}


/*****************************************************************************************************************
 * SPLIT / JOIN
 *****************************************************************************************************************/


public:
	void splitAndJoinRandomized()
	{
		if ((rand() & 1) == 0)
		{
			splitOrJoinPartitionRandomNTimes(10);
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}



public:
	size_t splitOrJoinPartitions()
	{
		/***************************************************************************************
		 * ACTION: run the split or join operations and update the edge communication information
		 ***************************************************************************************/

		// access cTestVariables.allSplit directly within anonymous function does not work.
		// therefore we create a new storage to variables.
		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes<true>(
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					cGenericTreeNode->cPartition_TreeNode->fun_splitAtLeaves(splitAllPartitionsInNextStep);
				}
				,
				[&](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_TreeNode_::fun_testAndJoinAtMidNodes(cGenericTreeNode, joinAllPartitionsInNextStep);
				}
			);

		number_of_simulation_handler_partitions =
			rootGenericTreeNode->traverse_PartitionTreeNode_Reduce<true>(
					[&](CPartition_TreeNode_ *node) -> unsigned long long
					{
						return node->cPartition_SplitJoinActions.pass2_updateAdjacentPartitionInformation();
					},
					&CReduceOperators::ADD<unsigned long long>
				);

		static auto fun2 = [=](CGenericTreeNode_ *cGenericTreeNode)
				{
					CPartition_SplitJoinActions<CSimulationTsunami_Parallel_SubPartitionHandler>::pass3_swapAndCleanAfterUpdatingEdgeComm(cGenericTreeNode);
				};

		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes<true>(fun2, fun2);

		return number_of_simulation_handler_partitions;
	}


	void markPartitionForSplitting(CVector<2,float> pos)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
			{
				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->triangleFactory.vertices[0][0], node->triangleFactory.vertices[0][1],
						node->triangleFactory.vertices[1][0], node->triangleFactory.vertices[1][1],
						node->triangleFactory.vertices[2][0], node->triangleFactory.vertices[2][1],
						pos.data[0], pos.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
			}
		);
	}


	void splitPartition(const CVector<2,float> &planePosition)
	{
		markPartitionForSplitting(planePosition);
		splitOrJoinPartitions();
	}



	void markPartitionForJoining(const CVector<2,float> &planePosition)
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[&](CPartition_TreeNode_ *node)
			{
				if (!CPointInTriangleTest<TTsunamiVertexScalar>::test(
						node->triangleFactory.vertices[0][0], node->triangleFactory.vertices[0][1],
						node->triangleFactory.vertices[1][0], node->triangleFactory.vertices[1][1],
						node->triangleFactory.vertices[2][0], node->triangleFactory.vertices[2][1],
						planePosition.data[0], planePosition.data[1]
					))
				{
					return;
				}

				node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
			}
		);
	}



	/**
	 * ACTION: random split and join partition at random positions
	 */
	void splitOrJoinPartitionRandomNTimes(const int count)
	{
		for (int i = 0; i < count; i++)
			rootGenericTreeNode->traverse_PartitionTreeNode<true>(
				[&](CPartition_TreeNode_ *node)
				{
					int r = random() % 6;

					if (r == 0)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
					}
					else if (r < 6)
					{
						node->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
					}
				}
			);

		splitOrJoinPartitions();
	}


	/**
	 * output information about the underlying tree
	 */
	void outputGenericTreeInformation()
	{
		rootGenericTreeNode->traverse_GenericTreeNode_MidAndLeafNodes_Depth<true>(
				[=](CGenericTreeNode_ *genericTreeNode, int depth)
				{
					// LEAF

					for (int i = 0; i < depth; i++)
						std::cout << "        ";

					std::cout << "|------- (" << depth << ") ";

					if (genericTreeNode->cPartition_TreeNode == nullptr)
					{
						std::cout << "Empty Leaf (ERROR!)" << std::endl;
						return;
					}

					std::cout << "UniqueId: " << genericTreeNode->cPartition_TreeNode->uniqueId;

#if DEBUG
					std::cout << std::endl;
					for (int i = 0; i < depth+1; i++)
						std::cout <<  "        ";

					std::cout << "    timestep size: " << genericTreeNode->cPartition_TreeNode->cSimulation_SubPartitionHandler->cTsunami_EdgeComm.getTimestepSize() << std::endl;
#endif
				}
				,
				[=](CGenericTreeNode_ *cGenericTreeNode, int depth)
				{
					if (cGenericTreeNode->cPartition_TreeNode == nullptr)
						return;

					// MIDDLE NODE

					for (int i = 0; i < depth; i++)
						std::cout << "        ";

					std::cout << "|------- (" << depth << ") ";


					if (cGenericTreeNode->cPartition_TreeNode == nullptr)
					{
						std::cout << "Empty MidNode" << std::endl;
						return;
					}

					std::cout << "MidNode: " << cGenericTreeNode->cPartition_TreeNode->uniqueId << std::endl;
				}
			);
	}



/*****************************************************************************************************************
 * GUI STUFF & VISUALIZATION
 *****************************************************************************************************************/

#if COMPILE_SIMULATION_WITH_GUI

	/**
	 * set callback handler which has to be executed when parameters on gui have to be updated
	 */
	void setCallbackParameterUpdate(
			void (*p_callback_update_gui_func)(void* user_data),
			void *p_callback_update_gui_func_user_data
	)
	{
		callback_update_gui_func = p_callback_update_gui_func;
		callback_update_gui_func_user_data = p_callback_update_gui_func_user_data;
	}


	inline void callbackParameterUpdate()
	{
		if (callback_update_gui_func != nullptr)
			callback_update_gui_func(callback_update_gui_func_user_data);
	}


public:
	void keydown(int key)
	{
		switch(key)
		{
			case 'z':
				world_scene_id--;
				reset_Simulation();
				callbackParameterUpdate();
				std::cout << "WorldSceneId: " << world_scene_id << std::endl;
				break;

			case 'x':
				world_scene_id++;
				reset_Simulation();
				callbackParameterUpdate();
				std::cout << "WorldSceneId: " << world_scene_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 't':
				initial_recursion_depth += 1;
				reset_Simulation();
				callbackParameterUpdate();
				std::cout << "Setting initial recursion depth to " << initial_recursion_depth << std::endl;
				break;

			case 'T':
				max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << max_relative_recursion_depth << std::endl;
				reset_Simulation();
				callbackParameterUpdate();
				break;

			case 'g':
				if (initial_recursion_depth > 0)
					initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << initial_recursion_depth << std::endl;
				reset_Simulation();
				callbackParameterUpdate();
				break;

			case 'G':
				if (max_relative_recursion_depth > 0)
					max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << max_relative_recursion_depth << std::endl;
				reset_Simulation();
				callbackParameterUpdate();
				break;

			case 'P':
				outputGenericTreeInformation();
				break;

			case 'c':
				p_adaptive_setup_column(-0.5, 0.4, 0.2);
				break;

			case ',':
				splitAllPartitionsInNextStep = true;
				p_adaptive_timestep();
				splitOrJoinPartitions();
				splitAllPartitionsInNextStep = false;
				break;

			case '.':
				joinAllPartitionsInNextStep = true;
				p_adaptive_timestep();
				splitOrJoinPartitions();
				joinAllPartitionsInNextStep = false;
				break;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			case 'I':
				splitAndJoinRandomized();
				break;


			case 'y':
				splitOrJoinPartitionRandomNTimes(1);
				break;

		}
	}

	void render_surfaceDefault(
			CShaderBlinn &cShaderBlinn
	)
	{
		render_surfaceVerticesElement(cShaderBlinn);
	}

	void render_surfaceVerticesElement(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}


	void render_surfaceWithHeightColors(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_aligned.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWithHeightColors_simple(
			CShaderHeightColorBlinn &cShaderColorHeightBlinn
	)
	{
		cShaderColorHeightBlinn.use();

		cOpenGL_Vertices_Element_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Element_Tsunami_simple.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Element_Root_Tsunami.shutdownRendering();
		cShaderColorHeightBlinn.disable();
	}


	void render_surfaceWireframe(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();
		cOpenGL_Vertices_Wireframe_Root_Tsunami.initRendering();

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Vertices_Wireframe_Tsunami.action(node->cStacks);
				}
		);

		cOpenGL_Vertices_Wireframe_Root_Tsunami.shutdownRendering();
		cShaderBlinn.disable();
	}

	void render_surfaceSmooth(
			GLSL::mat4 &p_pvm_matrix,
			CShaderBlinn &cShaderBlinn
	)
	{
		cOpenGL_Element_Splats_Root_Tsunami.initRenderSplats(p_pvm_matrix);

		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					node->cSimulation_SubPartitionHandler->cOpenGL_Element_Splats_Tsunami.action(node->cStacks);
				}
		);

		cOpenGL_Element_Splats_Root_Tsunami.shutdownRenderSplats();

		cOpenGL_Element_Splats_Root_Tsunami.renderWaterSurface(p_pvm_matrix);
	}



	void render_subPartitionBorders(
			CShaderBlinn &cShaderBlinn
	)
	{
		cShaderBlinn.use();

		GLSL::vec3 c(0.8, 0.1, 0.1);
		cShaderBlinn.material_ambient_color3_uniform.set(c);
		cShaderBlinn.material_diffuse_color3_uniform.set(c);
		cShaderBlinn.material_specular_color3_uniform.set(c);

		render_partitions_vao.bind();
			render_partitions_buffer.bind();

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

			CGlBuffer *tmp_render_partitions_buffer = &render_partitions_buffer;

			rootGenericTreeNode->traverse_PartitionTreeNode_Serial(
				[&](CPartition_TreeNode_ *node)
				{
					GLfloat vertex_buffer[6*3];

					vertex_buffer[0*3+0] = node->triangleFactory.vertices[0][0];
					vertex_buffer[0*3+1] = 0;
					vertex_buffer[0*3+2] = -node->triangleFactory.vertices[0][1];

					vertex_buffer[1*3+0] = node->triangleFactory.vertices[1][0];
					vertex_buffer[1*3+1] = 0;
					vertex_buffer[1*3+2] = -node->triangleFactory.vertices[1][1];

					vertex_buffer[2*3+0] = node->triangleFactory.vertices[1][0];
					vertex_buffer[2*3+1] = 0;
					vertex_buffer[2*3+2] = -node->triangleFactory.vertices[1][1];

					vertex_buffer[3*3+0] = node->triangleFactory.vertices[2][0];
					vertex_buffer[3*3+1] = 0;
					vertex_buffer[3*3+2] = -node->triangleFactory.vertices[2][1];

					vertex_buffer[4*3+0] = node->triangleFactory.vertices[2][0];
					vertex_buffer[4*3+1] = 0;
					vertex_buffer[4*3+2] = -node->triangleFactory.vertices[2][1];

					vertex_buffer[5*3+0] = node->triangleFactory.vertices[0][0];
					vertex_buffer[5*3+1] = 0;
					vertex_buffer[5*3+2] = -node->triangleFactory.vertices[0][1];

					tmp_render_partitions_buffer->subData(0, sizeof(vertex_buffer), vertex_buffer);
					glDrawArrays(GL_LINES, 0, 6);
				}
			);

			CGlErrorCheck();

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

		render_partitions_vao.unbind();

		cShaderBlinn.disable();
	}
#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

	void action_Validation_EdgeCommLength()
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
			{
				node->cSimulation_SubPartitionHandler->cPartition_ExchangeEdgeCommData_Adaptivity.validateCommDataLength();
			}
		);
	}


	void action_Validation_EdgeCommMidpointsAndNormals()	const
	{
		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *worker = node->cSimulation_SubPartitionHandler;

					/*
					 * setup fake validation element data stack
					 */
					if (node->cValidationStacks->element_data_stacks.forward.getNumberOfElementsOnStack() != node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack())
					{
						node->cValidationStacks->element_data_stacks.forward.clear();

						for (unsigned int i = 0; i < node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack(); i++)
						{
							node->cValidationStacks->element_data_stacks.forward.push(CValElementData());
						}
					}

					worker->cEdgeComm_ValidateComm.kernelClass.noElementDataChecks = true;

					worker->cEdgeComm_ValidateComm.actionFirstPass(
							node->cStacks->structure_stacks,
							node->cValidationStacks->element_data_stacks,
							node->cValidationStacks->edge_data_comm_left_edge_stack,
							node->cValidationStacks->edge_data_comm_right_edge_stack,
							node->cValidationStacks->edge_comm_buffer
						);
				}
		);


		rootGenericTreeNode->traverse_SimulationPartitionHandler<true>(
			[=](CSimulationTsunami_Parallel_SubPartitionHandler *worker)
				{
					worker->cPartitionTree_Node_EdgeComm_Validation.pullEdgeCommData();
				}
		);


		rootGenericTreeNode->traverse_PartitionTreeNode<true>(
			[=](CPartition_TreeNode_ *node)
				{
					CSimulationTsunami_Parallel_SubPartitionHandler *worker = node->cSimulation_SubPartitionHandler;

					/*
					 * stacks are cleared here since there may be some data left from the last traversal
					 */
					node->cValidationStacks->edge_data_comm_left_edge_stack.clear();
					node->cValidationStacks->edge_data_comm_right_edge_stack.clear();

					/**
					 * second pass
					 */
					worker->cEdgeComm_ValidateComm.actionSecondPass(
							node->cStacks->structure_stacks,
							node->cValidationStacks->element_data_stacks,
							node->cValidationStacks->edge_data_comm_exchange_left_edge_stack,		/// !!! here we use the "exchange stacks"!
							node->cValidationStacks->edge_data_comm_exchange_right_edge_stack,
							node->cValidationStacks->edge_comm_buffer
						);
				}
		);
	}


	void action_Validation()
	{
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
	}

#endif



};

#endif /* CTSUNAMI_HPP_ */
