+ Flux solver: lax friedrich
+ Order of basis functions: 1
 + Number of threads to use: 4
 + Initial recursion depth: 12
 + Verbose: 4
 + Timesteps: 1
 + Timestep size: 0.617632
 + Output .vtk files each #nth timestep: -1
 + Output simulation data filename: frame_%08i.vtk
 + InitialDepth: 12
 + MinDepth: 12
 + MaxDepth: 18
 + WorldSceneId: 3
 + Cluster size when to request split: 4096
 + Cluster size when to request join (both childs have to request a join): 2048
 + ADAPTIVE_SUBPARTITION_STACKS enabled
[ SETUP ]
 + initial number of triangles: 0
 + refined to 9946 triangles
 + pre splitting clusters
[ START ]
=========================================
   + 0	TIMESTEP
   + 9946	TRIANGLES
   + 0.986465	ElementData Megabyte per Timestep (RW)
   + 0	PARTITIONS
[ END ]


Timings for simulation phases:
 + EdgeCommTime: 0.002002
 + AdaptiveTime: 0.002231
 + SplitJoinTime: 0.000503

1 TS (Timesteps)
0.617632 TSS (Timestep size)
0.004812 SFS (Overall seconds for Simulation)
10020 TP (Triangles processed)
0.004812 ASPT (Averaged Seconds per Timestep)
10020 TPST (Triangles Processed in Average per Simulation Timestep)
4 PPST (Clusters Processed in Average per Simulation Timestep)
2.08229 MTPS (Million Triangles per Second)
0.520574 MTPSPT (Million Triangles per Second per Thread)
0.993805 EDMBPT (ElementData Megabyte per Timestep (RW))
206.526 EDMBPS (ElementData Megabyte per Second (RW))
   + 9946	TRIANGLES
   + 23577	TRIANGLES
   + 32150	TRIANGLES
   + 40202	TRIANGLES
   + 47304	TRIANGLES
   + 53234	TRIANGLES
   + 58529	TRIANGLES
   + 62784	TRIANGLES
   + 67195	TRIANGLES
   + 71606	TRIANGLES
   + 76269	TRIANGLES
   + 80864	TRIANGLES
   + 85835	TRIANGLES
   + 90766	TRIANGLES
   + 95897	TRIANGLES
   + 101095	TRIANGLES
   + 106455	TRIANGLES
   + 111939	TRIANGLES
   + 117530	TRIANGLES
   + 123162	TRIANGLES
   + 128958	TRIANGLES
   + 134975	TRIANGLES
   + 140909	TRIANGLES
   + 147082	TRIANGLES
   + 153275	TRIANGLES
   + 159440	TRIANGLES
   + 166041	TRIANGLES
   + 172402	TRIANGLES
   + 178811	TRIANGLES
   + 185339	TRIANGLES
   + 191883	TRIANGLES
   + 198431	TRIANGLES
   + 204685	TRIANGLES
   + 211065	TRIANGLES
   + 217306	TRIANGLES
   + 223213	TRIANGLES
   + 228933	TRIANGLES
   + 234280	TRIANGLES
   + 239468	TRIANGLES
   + 244417	TRIANGLES
   + 248632	TRIANGLES
   + 253064	TRIANGLES
   + 257654	TRIANGLES
   + 261907	TRIANGLES
   + 266195	TRIANGLES
   + 270896	TRIANGLES
   + 275656	TRIANGLES
   + 281141	TRIANGLES
   + 286920	TRIANGLES
   + 292670	TRIANGLES
   + 298522	TRIANGLES
   + 305452	TRIANGLES
   + 312011	TRIANGLES
   + 319105	TRIANGLES
   + 326526	TRIANGLES
   + 334025	TRIANGLES
   + 341750	TRIANGLES
   + 349762	TRIANGLES
   + 358117	TRIANGLES
   + 366471	TRIANGLES
   + 374999	TRIANGLES
   + 383766	TRIANGLES
   + 392900	TRIANGLES
   + 402203	TRIANGLES
   + 411401	TRIANGLES
