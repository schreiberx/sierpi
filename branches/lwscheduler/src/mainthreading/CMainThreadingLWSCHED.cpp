/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CMainThreadingTBB.cpp
 *
 *  Created on: May 8, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CMainThreadingLWSCHED.hpp"

#include <tbb/atomic.h>
#include <tbb/task.h>

#include <sys/types.h>
#include <unistd.h>
#include <linux/unistd.h>
#include <errno.h>
#include <pthread.h>
#include <cassert>
#include <signal.h>
#include <cassert>

#include <sstream>
#include <ostream>

#include "CAffinityMap.hpp"

/*
 * pieces of the following codes are from TBB 4.0, src/perf.cpp
 * http://threadingbuildingblocks.org/
 *
 * the code was slightly modified.
 */

tbb::atomic<int> m_nThreads;

class AffinitySetterTask :
	public tbb::task
{
	static pid_t gettid()
	{
		return (pid_t)syscall(__NR_gettid);
	}

	static bool PinTheThread(
			int i_core_id,
			tbb::atomic<int>& nRemainingThreadsForPinning
	)
	{
		cpu_set_t target_mask;
		CPU_ZERO(&target_mask);
		CPU_SET(i_core_id, &target_mask);
		assert(CPU_ISSET(i_core_id, &target_mask));

		int res = sched_setaffinity(gettid(), sizeof(cpu_set_t), &target_mask);
		assert(res == 0);

		--nRemainingThreadsForPinning;
		while (nRemainingThreadsForPinning)
			__TBB_Yield();

		return true;
	}

public:
	int m_core_id;

	tbb::task* execute ()
	{
		PinTheThread(m_core_id, m_nThreads);
		return NULL;
	}

public:
	AffinitySetterTask(int core_id) :
		m_core_id(core_id)
	{
	}
};
//
//
//bool CMainThreadingTBB_setAffinities(
//		int i_num_threads,
//		int i_max_cores,
//		int i_pinning_distance,
//		int i_pinning_start_id,
//		int i_verbose_level
//)
//{
//	if (i_pinning_start_id < 0)
//		i_pinning_start_id = 0;
//
//	assert(i_num_threads < 1024);
//	int affinityMap[1024];
//
//	if (!CAffinityMap::setup(i_num_threads, i_max_cores, i_pinning_distance, i_pinning_start_id, i_verbose_level, affinityMap))
//		return true;
//
//	/*
//	 * pin the first pid to the first affinity map
//	 */
//	cpu_set_t cpu_set;
//	CPU_ZERO(&cpu_set);
//	CPU_SET(affinityMap[0], &cpu_set);
//
//	int retval = sched_setaffinity(getpid(), sizeof(cpu_set_t), &cpu_set);
//	assert(retval == 0);
//
//	m_nThreads = i_num_threads;
//
//	tbb::task_list tl;
//	for (int i = 0; i < i_num_threads; i++)
//	{
//		tbb::task &t = *new(tbb::task::allocate_root()) AffinitySetterTask(affinityMap[i]);
//		t.set_affinity(tbb::task::affinity_id(i+1));
//		tl.push_back(t);
//	}
//	tbb::task::spawn_root_and_wait(tl);
//
//
//	return true;
//}
