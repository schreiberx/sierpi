/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_HYBRID_HPP_
#define CFLUXSOLVER_HYBRID_HPP_

#include "libmath/CMath.hpp"
#include <iostream>
#include <cassert>

#include "tsunami_solver/Hybrid.hpp"

/**
 * \brief Hybrid flux solver
 */
template <typename T>
class CFluxSolver_Hybrid	: public solver::Hybrid<T>
{
	/**
	 * This method is executed by the cellData computing kernel method
	 * to get the net updates for given fluxes
	 */
public:
	void op_edge_edge(
			const CTsunamiSimulationDOFs &i_edgeData_left,		///< edge data on left (left) edge
			const CTsunamiSimulationDOFs &i_edgeData_right,		///< edge data on right (outer) edge

			CTsunamiSimulationDOFs *o_edgeFlux_left,		///< output for left flux
			CTsunamiSimulationDOFs *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		solver::Hybrid<T>::op_edge_edge(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.hu, i_edgeData_right.hu,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left->h, o_edgeFlux_right->h,
				o_edgeFlux_left->hu, o_edgeFlux_right->hu,
				*o_max_wave_speed_left
		);

		*o_max_wave_speed_right = *o_max_wave_speed_left;

		o_edgeFlux_left->hv = 0;
		o_edgeFlux_right->hv = 0;
	}
};



#endif
