/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_


#include "../../tsunami_config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// edge projections
#include "../../CProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 */

/**
 * update scheme
 *
 * specifies how to update element data:
 * 0: default update
 * 1: store updates only
 *
 * this gets important for Runge-Kutta methods
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Tsunami_0thOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE == 2
	using CEdgeComm_TsunamiParameters::setAdaptivityParameters;
#endif

	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiSimulationEdgeData CEdgeData;
	typedef CTsunamiSimulationCellData CCellData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Tsunami_0thOrder<t_storeElementUpdatesOnly>, CTsunamiSimulationStacksAndTypes> TRAV;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;


	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_0thOrder()
	{
		boundary_dirichlet.dofs_center.h = CMath::numeric_inf<T>();
		boundary_dirichlet.dofs_center.hu = CMath::numeric_inf<T>();
		boundary_dirichlet.dofs_center.hv = CMath::numeric_inf<T>();
	}

	/**
	 * RK2 helper function - first step
	 *
	 * compute yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void rk2_step1_cell_update_with_edges(
			CTsunamiSimulationCellData &i_f_t0,
			CTsunamiSimulationCellData &i_f_slope_t0,
			CTsunamiSimulationCellData *o_f_t1_approx,
			T i_timestep_size
	)
	{
		o_f_t1_approx->dofs_center.h	=	i_f_t0.dofs_center.h	+ i_timestep_size*i_f_slope_t0.dofs_center.h;
		o_f_t1_approx->dofs_center.hu	=	i_f_t0.dofs_center.hu	+ i_timestep_size*i_f_slope_t0.dofs_center.hu;
		o_f_t1_approx->dofs_center.hv	=	i_f_t0.dofs_center.hv	+ i_timestep_size*i_f_slope_t0.dofs_center.hv;
		o_f_t1_approx->dofs_center.b	=	i_f_t0.dofs_center.b;

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void rk2_step2_cell_update_with_edges(
			CTsunamiSimulationCellData &i_f_slope_t0,
			CTsunamiSimulationCellData &i_f_slope_t1,
			CTsunamiSimulationCellData *io_f_t0_t1,
			T i_timestep_size
	)
	{
		io_f_t0_t1->dofs_center.h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_center.h + i_f_slope_t1.dofs_center.h);
		io_f_t0_t1->dofs_center.hu	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_center.hu + i_f_slope_t1.dofs_center.hu);
		io_f_t0_t1->dofs_center.hv	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs_center.hv + i_f_slope_t1.dofs_center.hv);

		// TODO: which CFL to use?
//		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}



	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_0thOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs_center = i_cellData->dofs_center;
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CProjections::toHypEdgeSpace(&o_edge_comm_data->dofs_center.hu, &o_edge_comm_data->dofs_center.hv);


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cellData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs_center = i_cellData->dofs_center;
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CProjections::toRightEdgeSpace(&o_edge_comm_data->dofs_center.hu, &o_edge_comm_data->dofs_center.hv);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_cellData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented cellData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void op_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		o_edge_comm_data->dofs_center = i_cellData->dofs_center;
		o_edge_comm_data->CFL1_scalar = getCFLCellFactor(i_depth);

		CProjections::toLeftEdgeSpace(&o_edge_comm_data->dofs_center.hu, &o_edge_comm_data->dofs_center.hv);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION

		i_cellData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// set to -1 since this flux may never be used for cfl criteria!
		o_edge_comm_data->CFL1_scalar = -1;

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			o_edge_comm_data->dofs_center.h = boundary_dirichlet.dofs_center.h;
			o_edge_comm_data->dofs_center.hu = boundary_dirichlet.dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = boundary_dirichlet.dofs_center.hv;

#if !SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS
			o_edge_comm_data->dofs_center.b = boundary_dirichlet.b;
#else
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;
			if (i_cellData->dofs_center.b < 0)
				o_edge_comm_data->dofs_center.h = -i_cellData->dofs_center.b;
			else
				o_edge_comm_data->dofs_center.h = 0;
#endif
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;
			o_edge_comm_data->dofs_center.hu = 0;
			o_edge_comm_data->dofs_center.hv = 0;

			// rotate edge comm data to normal space
			CProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toHypEdgeSpace(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs_center.h = i_cellData->dofs_center.h;
			o_edge_comm_data->dofs_center.hu = -i_cellData->dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = -i_cellData->dofs_center.hv;
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;

			// rotate edge comm data to normal space
			CProjections::toHypEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;
		}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_cellData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// set to -1 since this flux may never be used for cfl criteria!
		o_edge_comm_data->CFL1_scalar = -1;

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			o_edge_comm_data->dofs_center.h = boundary_dirichlet.dofs_center.h;
			o_edge_comm_data->dofs_center.hu = boundary_dirichlet.dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = boundary_dirichlet.dofs_center.hv;

#if !SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS
			o_edge_comm_data->dofs_center.b = boundary_dirichlet.b;
#else
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;
			if (i_cellData->dofs_center.b < 0)
				o_edge_comm_data->dofs_center.h = -i_cellData->dofs_center.b;
			else
				o_edge_comm_data->dofs_center.h = 0;
#endif
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;
			o_edge_comm_data->dofs_center.hu = 0;
			o_edge_comm_data->dofs_center.hv = 0;

			// rotate edge comm data to normal space
			CProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toRightEdgeSpace(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs_center.h = i_cellData->dofs_center.h;
			o_edge_comm_data->dofs_center.hu = -i_cellData->dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = -i_cellData->dofs_center.hv;
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;

			// rotate edge comm data to normal space
			CProjections::toRightEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;
		}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_cellData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void op_boundary_cell_to_edge_left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiSimulationCellData *i_cellData,

			CTsunamiSimulationEdgeData *o_edge_comm_data
	)
	{
		// set to -1 since this flux may never be used for cfl criteria!
		o_edge_comm_data->CFL1_scalar = -1;

		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			o_edge_comm_data->dofs_center.h = boundary_dirichlet.dofs_center.h;
			o_edge_comm_data->dofs_center.hu = boundary_dirichlet.dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = boundary_dirichlet.dofs_center.hv;

#if !SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS
			o_edge_comm_data->dofs_center.b = boundary_dirichlet.b;
#else
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;
			if (i_cellData->dofs_center.b < 0)
				o_edge_comm_data->dofs_center.h = -i_cellData->dofs_center.b;
			else
				o_edge_comm_data->dofs_center.h = 0;
#endif
			break;

		case BOUNDARY_CONDITION_VELOCITY_ZERO:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;
			o_edge_comm_data->dofs_center.hu = 0;
			o_edge_comm_data->dofs_center.hv = 0;

			// rotate edge comm data to normal space
			CProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_OUTFLOW:
			o_edge_comm_data->dofs_center = i_cellData->dofs_center;

			// rotate edge comm data to normal space
			CProjections::toLeftEdgeSpace(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->dofs_center.h = i_cellData->dofs_center.h;
			o_edge_comm_data->dofs_center.hu = -i_cellData->dofs_center.hu;
			o_edge_comm_data->dofs_center.hv = -i_cellData->dofs_center.hv;
			o_edge_comm_data->dofs_center.b = i_cellData->dofs_center.b;

			// rotate edge comm data to normal space
			CProjections::toLeftEdgeSpaceAndInvert(
					&o_edge_comm_data->dofs_center.hu,
					&o_edge_comm_data->dofs_center.hv
				);
			break;
		}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_cellData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}


	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the cellData within one timestep.
	 */
	inline void op_cell(
			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiSimulationCellData *io_cellData,	///< element data

			CTsunamiSimulationEdgeData *i_hyp_edge_net_update,	///< incoming net update from hypotenuse
			CTsunamiSimulationEdgeData *i_right_edge_net_update,	///< incoming net update from right edge
			CTsunamiSimulationEdgeData *i_left_edge_net_update	///< incoming net update from left edge
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_hyp_edge_net_update->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_net_update->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_net_update->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_cellData->validation.testEdgeMidpoints(
				i_hyp_edge_net_update->validation,
				i_right_edge_net_update->validation,
				i_left_edge_net_update->validation
			);
#endif

		CProjections::fromHypEdgeSpace(
				&i_hyp_edge_net_update->dofs_center.hu,
				&i_hyp_edge_net_update->dofs_center.hv
			);

		CProjections::fromRightEdgeSpace(
				&i_right_edge_net_update->dofs_center.hu,
				&i_right_edge_net_update->dofs_center.hv
			);

		CProjections::fromLeftEdgeSpace(
				&i_left_edge_net_update->dofs_center.hu,
				&i_left_edge_net_update->dofs_center.hv
			);

		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);

		if (CMath::isNan(io_cellData->dofs_center.h))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*cathetus_real_length;

		if (t_storeElementUpdatesOnly)
		{
			// store updates only
			TTsunamiDataScalar tmp = -(T)1.0/((T)0.5*cat_length*cat_length);
			io_cellData->dofs_center.h =  tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.h + cat_length*(i_right_edge_net_update->dofs_center.h + i_left_edge_net_update->dofs_center.h));
			io_cellData->dofs_center.hu = tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.hu + cat_length*(i_right_edge_net_update->dofs_center.hu + i_left_edge_net_update->dofs_center.hu));
			io_cellData->dofs_center.hv = tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.hv + cat_length*(i_right_edge_net_update->dofs_center.hv + i_left_edge_net_update->dofs_center.hv));
		}
		else
		{
			// apply updates
			TTsunamiDataScalar tmp = -(T)timestep_size/((T)0.5*cat_length*cat_length);
			io_cellData->dofs_center.h +=  tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.h + cat_length*(i_right_edge_net_update->dofs_center.h + i_left_edge_net_update->dofs_center.h));
			io_cellData->dofs_center.hu += tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.hu + cat_length*(i_right_edge_net_update->dofs_center.hu + i_left_edge_net_update->dofs_center.hu));
			io_cellData->dofs_center.hv += tmp * (hyp_length*i_hyp_edge_net_update->dofs_center.hv + cat_length*(i_right_edge_net_update->dofs_center.hv + i_left_edge_net_update->dofs_center.hv));

			if (io_cellData->dofs_center.h < 0.001)
				io_cellData->dofs_center.h = 0;
		}

#if SIMULATION_TSUNAMI_ADAPTIVITY_MODE==2

		// don't scale with timestep size to have normalized mass over one second
		T m =	CMath::max(
					CMath::abs(i_hyp_edge_net_update->dofs_center.h*hyp_length),
					CMath::abs(i_right_edge_net_update->dofs_center.h*cat_length),
					CMath::abs(i_left_edge_net_update->dofs_center.h*cat_length)
				);

		/*
		 * TODO: MAKE THRESHOLD HEIGHT MORE FLEXIBLE!
		 */
		T inv_h;
		if (io_cellData->dofs_center.h > 0.01)
			inv_h = (T)1.0/io_cellData->dofs_center.h;
		else
			inv_h = 0.01;

		// rescale with height to get update for unit area over unit timestep
		io_cellData->refine = (m*inv_h > refine_threshold);
		io_cellData->coarsen = (m*inv_h < coarsen_threshold);

#endif


#if 0
		if (io_cellData->dofs_center.b < 0)
		{
			if (io_cellData->dofs_center.h + io_cellData->dofs.b > 1.0)
			{
				std::cout << "timestep_size: " << timestep_size << std::endl;
				std::cout << "cat_length: " << cat_length << std::endl;

				std::cout << "net update h: " << i_hyp_edge_net_update->h << std::endl;
				std::cout << "net update hu: " << i_hyp_edge_net_update->hu << std::endl;
				std::cout << "net update hv: " << i_hyp_edge_net_update->hv << std::endl;
				std::cout << "net update b: " << i_hyp_edge_net_update->b << std::endl;

				std::cout << "cellData h: " << io_cellData->dofs.h << std::endl;
				std::cout << "cellData hu: " << io_cellData->dofs.hu << std::endl;
				std::cout << "cellData hv: " << io_cellData->dofs.hv << std::endl;
				std::cout << "cellData b: " << io_cellData->dofs_center.b << std::endl;

				std::cout << "tmp: " << -(T)timestep_size/((T)0.5*cat_length*cat_length) << std::endl;
				std::cout << std::endl;
			}
		}
#endif
	}





	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void op_edge_edge(
			const CTsunamiSimulationEdgeData &i_edgeFlux_left,	///< edge data on left edge
			const CTsunamiSimulationEdgeData &i_edgeFlux_right,	///< edge data on right edge
			CTsunamiSimulationEdgeData &o_edgeNetUpdates_left,		///< output for left flux
			CTsunamiSimulationEdgeData &o_edgeNetUpdates_right		///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		CTsunamiSimulationEdgeData io_edgeData_right = i_edgeFlux_right;

		io_edgeData_right.dofs_center.hu = -i_edgeFlux_right.dofs_center.hu;
		io_edgeData_right.dofs_center.hv = -i_edgeFlux_right.dofs_center.hv;
		io_edgeData_right.dofs_center.h = i_edgeFlux_right.dofs_center.h;
		io_edgeData_right.dofs_center.b = i_edgeFlux_right.dofs_center.b;

		T o_max_wave_speed_left;
		T o_max_wave_speed_right;

		fluxSolver.op_edge_edge(
				i_edgeFlux_left.dofs_center,
				io_edgeData_right.dofs_center,
				&o_edgeNetUpdates_left.dofs_center,
				&o_edgeNetUpdates_right.dofs_center,
				&o_max_wave_speed_left,
				&o_max_wave_speed_right,
				gravitational_constant
			);

		o_edgeNetUpdates_right.dofs_center.hu = -o_edgeNetUpdates_right.dofs_center.hu;
		o_edgeNetUpdates_right.dofs_center.hv = -o_edgeNetUpdates_right.dofs_center.hv;

		o_edgeNetUpdates_left.CFL1_scalar = i_edgeFlux_left.CFL1_scalar / o_max_wave_speed_left;
		o_edgeNetUpdates_right.CFL1_scalar = i_edgeFlux_right.CFL1_scalar / o_max_wave_speed_right;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edgeNetUpdates_left.validation = i_edgeFlux_left.validation;
		o_edgeNetUpdates_right.validation = i_edgeFlux_right.validation;
#endif
	}




	/**
	 * run a SIMD evaluation on the fluxes stored on the edge comm buffer and
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_stack(
			CStack<CTsunamiSimulationEdgeData> *io_edge_comm_buffer
	)
	{
		assert((io_edge_comm_buffer->getNumberOfElementsOnStack() & 1) == 0);

		op_edge_edge_buffer_stack(io_edge_comm_buffer->getStartPtr(), io_edge_comm_buffer->getNumberOfElementsOnStack());
	}



	/**
	 * run a SIMD evaluation on the fluxes stored on the edge data array.
	 * store the net updates back to the same position.
	 */
	inline void op_edge_edge_buffer_stack(
		CTsunamiSimulationEdgeData *io_edge_data_array,
		size_t i_edge_comm_elements
	)
	{
		for (size_t i = 0; i < i_edge_comm_elements; i+=2)
		{
			CTsunamiSimulationEdgeData ed0 = io_edge_data_array[i];
			CTsunamiSimulationEdgeData ed1 = io_edge_data_array[i+1];

			op_edge_edge(ed0, ed1, io_edge_data_array[i], io_edge_data_array[i+1]);

			updateCFL1Value(io_edge_data_array[i].CFL1_scalar);
			updateCFL1Value(io_edge_data_array[i+1].CFL1_scalar);
		}
	}

};

}
}



#endif
