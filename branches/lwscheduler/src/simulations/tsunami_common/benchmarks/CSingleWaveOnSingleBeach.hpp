/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CBENCHMARK_SINGLE_WAVE_ON_SINGLE_BEACH_HPP
#define CBENCHMARK_SINGLE_WAVE_ON_SINGLE_BEACH_HPP

#include <cmath>


template <typename T>
class CSingleWaveOnSingleBeach
{
	/**
	 * see
	 * "Standards, Criteria, and Procedures for NOAA Evaluation of Tsunami Numerical Models"
	 * page 26ff. for more details about this benchmark.
	 *
	 * Sketch:
	 *  _
	 * / \
	 *  |  *
	 * y|   *         ***
	 *  |    *       * | *
	 *  |     *     ** H **
	 *  |      ******  | *************************************
	 *          *                      |
	 *           *                     |
	 *            *                    d
	 *             *                   |
	 *              *                  |
	 *               *****************************************
	 *         |-X_0-|--------------L----------------|
	 *         |-----------------X_s-----------------|
	 *
	 *
	 *         ---------------------------->
	 *                                    x
	 */

private:
  //dimensional variables
  //! initial max wave height (surface elevation)
	T H;

	//! water height from the sea floor up to the surface in the constant bathymetry area.
	T d;

	//! slope of the "simple" beach
	T beach_slope;

	//! midpoint of initial wave
	T Xsd;

	//! used gravity constant
	T gravity;

	//non-dimensional variables
	//! ratio: max. surface elevation / water height up to the surface
	T Hd;

	//! wave midpoint
	T Xs;

	//! start position of beach
	T X0;

	T tau;

	T gamma;	// gamma value used for surface wave elevation

//	T simulation_domain_length;		// length of simulation domain
//	T dimensionless_scale_factor;	// scale factor for dimensionless space-parameters
//	T simulation_domain_translate;	// displacement of simulation domain relative in real-world-space
  public:


	/**
	 * Constructor of the "single wave on a simple beach"-benchmark
	 *
	 * @param i_H
	 * @param i_d
	 * @param i_gravity
	 */
	CSingleWaveOnSingleBeach(
			const T i_H = (T) 0.019,		///< maximum height of the initial displacement.
			const T i_d = (T) 1.0,			///< water height from the sea floor up to the surface in the constant bathymetry area.
			const T i_gravity = (T) 1.0		///< gravity constant
	)
	{
		setup( i_H,
			 i_d,
			 i_gravity
			 );
	}


	void setup(
			const T i_H,
			const T i_d = 1,
			const T i_gravity = 9.81,
			T i_simulation_domain_length = 1.0					///< domain length to scale the height (x-coordinate is given normalized)
	)
	{
		//set problem specific variables
	//		origin=70.;
	//		d = 1.;
	//		H = 0.019*d;
	//		gamma = sqrt(.75*H/d);
	//		L = acosh(sqrt(20.))/gamma;
	//		xZero = d*19.85;
	//		xOne = xZero + L;
	//		g=1.;
	//		tau=sqrt(d/g);

		// set member variables to input
		H = i_H;
		d = i_d;
		gravity = i_gravity;

		// start of beach
		X0 = 19.85 * d;

		// dimensionless H
		Hd = H/d;

		gamma = CMath::sqrt(0.25*3.0*Hd);

		// distance: start of the beach - midpoint of the initial displacement

		T l_L = (1.0/CMath::cosh(CMath::sqrt(20.)))/gamma; //(C++11)

		// midpoint of wave
		Xs = X0 + l_L;

//		// midpoint of wave
//		Xs = 40.0;

//		// height of wave
//		H = 0.0185;

		// angle of beach
		// d = 1 (dimensionless)
		beach_slope = d/X0;

		tau = std::sqrt(d/gravity);


//		simulation_domain_length = i_simulation_domain_length;
//		dimensionless_scale_factor = i_simulation_domain_length/(Xs*0.8);
//		simulation_domain_translate = simulation_domain_length*0.8;
//
//		Xs *= dimensionless_scale_factor;
//		X0 *= dimensionless_scale_factor;
//
//		H *= dimensionless_scale_factor;

#if 0
//		std::cout << std::endl;
//		std::cout << "simulation_domain_length: " << simulation_domain_length << std::endl;
//		std::cout << "dimensionless_scale_factor: " << dimensionless_scale_factor << std::endl;
//		std::cout << "simulation_domain_translate: " << simulation_domain_translate << std::endl;

		std::cout << std::endl;
		std::cout << "H: " << H << std::endl;
		std::cout << "d: " << d << std::endl;
		std::cout << "H/d: " << Hd << std::endl;
		std::cout << "g: " << gravity << std::endl;
		std::cout << "beach_slope: " << beach_slope << std::endl;
		std::cout << "gamma: " << gamma << std::endl;
		std::cout << "X0 (start of beach slope): " << X0 << std::endl;
		std::cout << "Xs (midpoint of initial wave): " << Xs << std::endl;
		std::cout << "tau: " << tau << std::endl;

		std::cout << std::endl;
#endif
	}


	/**
	 * Get the bathymetry at a specific coordinate.
	 *
	 * @return bathymetry.
	 */
	inline T getBathymetry(
			T i_x	///< coordinate within the 1D-domain.
	)
	{
		T x = i_x;//*simulation_domain_length + simulation_domain_translate;

		//constant bathymetry after X_0
		if (x > X0)
			return -d;//-dimensionless_scale_factor;

#if 0
		std::cout << (X0-x) << std::endl;
		std::cout << (X0-x)*beach_slope << std::endl;
		std::cout << std::endl;
#endif

		//"simple" beach else
		return -d+(X0-x)*beach_slope;//-dimensionless_scale_factor;
	}

	/**
	 * Get the water height (height relative to the sea floor) at a specific coordiante at timestep t=0.
	 *
	 * @return water height.
	 */
	inline const T getWaterHeight(
			const T i_x		///< i_x coordinate within the 1D-domain.
	) {
		//		if(cells[i].getCellData().b < (T)0)
		//			cells[i].getCellData().h -= cells[i].getCellData().b;
		T l_waterHeight;

		if( getBathymetry(i_x) < (T)0 )
		l_waterHeight = getWaterSurfaceElevation(i_x) - getBathymetry(i_x);
		else
		l_waterHeight =	getWaterSurfaceElevation(i_x);

		return l_waterHeight;
	}


	/**
	 * Get the surface elevation (height relative to the water surface) at a specific coordinate.
	 *
	 * @return water height.
	 */
	inline const T getWaterSurfaceElevation(
			const T i_x		///< i_x coordinate within the 1D-domain.
	) {
//		2*cosh(gamma*(x-xOne)/d)/(cosh(2* gamma*(x-xOne)/d)+1);
//		initialWaveForm *= H*initialWaveForm;

		T x = i_x;//*simulation_domain_length + simulation_domain_translate;

		T surfaceElevation = (T)2 * std::cosh(gamma*(x-Xs)/d)/(std::cosh((T)2* gamma*(x-Xs)/d)+(T)1);
		surfaceElevation *= Hd * surfaceElevation;

//		// parameter for sech(x)
//		T sech_x = gamma * ((x - Xs)/d);///dimensionless_scale_factor);
//
//		T sech_tanh_x = CMath::tanh(sech_x);
//
//		T sech = 1.0 - sech_tanh_x*sech_tanh_x;
//
//		T h = H * sech*sech;
//
//		return h;

//		std::cout << surfaceElevation << " " << getWaterSurfaceElevation(i_x, 0, 5) << std::endl;

		return surfaceElevation;
	}


	/**
	 * return surface elevation (water displacement) for a given timestep
	 */
	const T getWaterSurfaceElevation(
			const T i_x,			///< coordinate within the 1D-domain.
			const T i_t,			///< timestep
			const int i_max_iters	///< maximum number of iterations to compute solution
	)
	{
		T alpha = CMath::PI<T>() / CMath::sqrt((T)3.0 * H);

		T pre_factor = ((T)4.0 * CMath::PI<T>() * CMath::PI<T>()) / ((T)3.0 * alpha * alpha)
				* CMath::pow(X0 / i_x, (T)0.25);

		T theta_prime = X0 - Xs - i_t - (T)2.0 * CMath::sqrt(i_x * X0);
/*
		std::cout << std::endl;
		std::cout << ((T)4.0 * CMath::PI<T>() * CMath::PI<T>()) / ((T)3.0 * alpha * alpha) << std::endl;
		std::cout << CMath::pow(X0 / i_x, (T)0.25) << std::endl;
		std::cout << "X0: " << X0 << std::endl;
		std::cout << "i_x: " << i_x << std::endl;
		std::cout << "alpha: " << alpha << std::endl;
		std::cout << "pre_factor: " << pre_factor << std::endl;
		std::cout << "theta_prime: " << theta_prime << std::endl;
*/
		T sum = 0;

		T sign = (T)1.0;
		for (int n = 0; n < i_max_iters; n++)
		{
//			std::cout << "> " << sum << std::endl;
			// TODO: is alpha really alpha? it's 'a' in the benchmark description (page 21)
			sum += sign * (T)n * CMath::exp(-(CMath::PI<T>() / alpha) * theta_prime * (T)n);
			sign *= -(T)1.0;
		}

		return pre_factor * sum;
	}

	/**
	 * Get the value of tau (gamma/d).
	 *
	 * @return tau.
	 */
	T getTau() {
		return tau;
	}

	/**
	 * Get the water height from the sea floor up to the surface in the constant bathymetry area.
	 *
	 * @return d.
	 */
	T getD() {
		return d;
	}

	/**
	 * Get the dimensionless height.
	 *
	 * @return dimensionless height.
	 */
	T getHd() {
		return Hd;
	}

	/**
	 * Get the initial momentum according to the formula for the velocity in the SWOSB-benchmark:
	 *	 u(x,0) = - \sqrt{g / d} \eta(x,0)
	 *
	 * @return
	 */
	inline const T getMomentum(
			const T i_x		///< get momentum at given coordinate
	) {
		T l_momentum = getWaterSurfaceElevation(i_x);
		l_momentum *= getWaterHeight(i_x);
		l_momentum *= -std::sqrt(gravity / d);

		//TODO: drytol
		if(getWaterHeight(i_x) < 0.00001)
		return (T)0.;

		return l_momentum;
	}
};

#endif
