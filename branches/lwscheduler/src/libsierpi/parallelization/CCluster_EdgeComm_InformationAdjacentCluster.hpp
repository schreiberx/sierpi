/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CEdgeComm_InformationAdjacentCluster.hpp
 *
 *  Created on: Jul 19, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTER_HPP_
#define CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTER_HPP_

#include "CCluster_TreeNode.hpp"
#include "CCluster_UniqueId.hpp"

#if CONFIG_ENABLE_MPI
	#include <mpi.h>
#endif


namespace sierpi
{


/**
 * information about a single adjacent cluster
 */
template <typename t_CCluster_TreeNode>
class CCluster_EdgeComm_InformationAdjacentCluster
{
public:
	/**
	 * the cluster tree node
	 */
	t_CCluster_TreeNode *clusterTreeNode;

	/**
	 * the uniqueId of the adjacent cluster (should be equal to clusterTreeNode->uniqueId)
	 */
	CCluster_UniqueId uniqueId;

	/**
	 * the number of elements on the communication stack which have to be exchanged
	 */
	int comm_elements;

#if CONFIG_ENABLE_MPI
	/*
	 * mpi rank of adjacent cluster
	 */
	int mpi_rank;

	/*
	 * mpi request handler to be used for receive operations
	 */
	MPI_Request mpi_requests[2];

	void *mpi_last_send_data_ptr;
	void *mpi_last_recv_data_ptr;
#endif


	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster()	:
			clusterTreeNode(nullptr),
			comm_elements(-1)
#if CONFIG_ENABLE_MPI
			,
			mpi_rank(-1)
#endif
	{
	}


	/**
	 * constructor to create edge communication information element
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster(
			t_CCluster_TreeNode *i_clusterTreeNode,	///< cluster tree node
			CCluster_UniqueId &i_uniqueId,	///< unique id
			size_t i_commElements				///< number of communication elements
#if CONFIG_ENABLE_MPI
			,
			int i_mpi_rank						///< mpi rank
#endif
		)
	{
		clusterTreeNode = i_clusterTreeNode;
		uniqueId = i_uniqueId;
		comm_elements = i_commElements;
#if CONFIG_ENABLE_MPI
		mpi_rank = i_mpi_rank;
#endif
	}


	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster(
			t_CCluster_TreeNode *i_clusterTreeNode	///< cluster tree node
	)
	{
		clusterTreeNode = i_clusterTreeNode;
		uniqueId = i_clusterTreeNode->uniqueId;
		comm_elements = -1;
#if CONFIG_ENABLE_MPI
		mpi_rank = i_clusterTreeNode->mpi_rank;
#endif
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, const CCluster_EdgeComm_InformationAdjacentCluster &m)
	{
		co << "   + comm elements: " << m.comm_elements << std::endl;
		co << "   + uniqueId: " << m.uniqueId << std::endl;
		if (m.clusterTreeNode != nullptr)
		{
			co << "   + adj uniqueId: " << m.clusterTreeNode->uniqueId << std::endl;
#if CONFIG_ENABLE_MPI
			assert(m.mpi_rank < 0);
#endif
		}
		else
		{
			co << "   + adj uniqueId not avail (MPI)" << std::endl;
#if CONFIG_ENABLE_MPI
			co << "   + mpi rank: " << m.mpi_rank << std::endl;
			assert(m.mpi_rank >= 0);
#endif
		}
		return co;
	}
};

}

#endif /* CEDGECOMM_INFORMATIONADJACENTPARTITION_HPP_ */
