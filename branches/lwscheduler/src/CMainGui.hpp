/*
 * CMainGui.hpp
 *
 *  Created on: Apr 18, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINGUI_HPP_
#define CMAINGUI_HPP_


#include <stdlib.h>
#include <sstream>

#include "config.h"

#include "libmath/CMath.hpp"
#include "libgl/draw/CGlDrawSphere.hpp"
#include "libgl/core/CGlState.hpp"
#include "lib/CEyeBall.hpp"

#include "mainvis/CCommonShaderPrograms.hpp"
#include "mainvis/CRenderWindow.hpp"

#include "libgl/hud/CGlFreeType.hpp"
#include "libgl/hud/CGlWindow.hpp"
#include "libgl/hud/CGlRenderOStream.hpp"
#include "libgl/hud/CGlHudConfig.hpp"

#include "simulations/CSimulation.hpp"

#include "mainvis/CGuiConfig.hpp"

#include "libgl/engine/CTime.hpp"
#include "libgl/engine/camera/iCamera.hpp"
#include "libgl/engine/camera/CCamera1stPerson.hpp"
#include "libgl/engine/iInputState.hpp"


#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "lib/CProcessMemoryInformation.hpp"

#include "lib/CLinesFromSVG.hpp"


#include "CMain.hpp"

#include "CSimulation_MainGuiParameters.hpp"
#include "CSimulation_MainGuiInterface.hpp"


class CMainGui :
		public CRenderWindowEventCallbacks,
		public CMain
{
	using CMain::cSimulation;
	using CMain::cSimulation_MainParameters;
	using CMain::cSimulation_MainInterface;

	using CMain::simulation_loopPrefix;
	using CMain::simulation_loopSuffix;

	using CMain::threading_simulationLoopIteration;
	using CMain::threading_setNumThreads;

	CSimulation_MainGuiParameters *cSimulation_MainGuiParameters;
	CSimulation_MainGuiInterface *cSimulation_MainGuiInterface;

//	typedef CONFIG_SIERPI_GUI_FLOATING_POINT_PRECISION TVertexScalar;
	typedef float TVertexScalar;

public:
	CError error;
	bool quit;

	CRenderWindow cRenderWindow;

	CGuiConfig cGuiConfig;
	CGlFreeType cGlFreeType;
	CGlRenderOStream cGlRenderOStream;

	CProcessMemoryInformation cProcessMemoryInformation;

	// parameter for model matrix
	CEyeBall<TVertexScalar> cModelEyeBall;

	// camera control
	CCamera1stPerson<TVertexScalar> camera;
	CTime cTime;
	CVector<3,TVertexScalar> player_velocity;
	iInputState inputState;

	// commonly used shader programs
	CCommonShaderPrograms cCommonShaderPrograms;


	// some opengl parameters for perspective matrix
	TVertexScalar perspective_zoom;
	TVertexScalar zoom;

	TVertexScalar near_plane;
	TVertexScalar far_plane;

	/**
	 * size of domain for simulation after scaling
	 */
	TVertexScalar domain_visualization_size;


    /**
     * import lines from SVG file
     */
    CLinesFromSVG cLinesFromSVG;

    /**
     * enumeration value for screenshots
     */
	int screenshotEnumeration;

	/**
	 * Number of threads was updated during the last step.
	 *
	 * For TBB, the number of threads has to be set by the thread which created the initial threads.
	 */
	int simulation_update_number_of_threads_updated;


	/**
	 * name for the surface visualization description
	 */
	const char *dofs_vizualization_description;


	/**
	 * name for the bathymetry visualization description
	 */
	const char *boundary_vizualization_description;


#if CONFIG_ENABLE_MPI
	class CMPISharedEventData
	{
	public:
		int mpi_shared_event_type;	// 0: key down, 1: key up, 2: mouse
		int mpi_shared_event_id;
		TVertexScalar mpi_shared_mouse_point_on_plane_x, mpi_shared_mouse_point_on_plane_y;
		int mpi_shared_mouse_button;

		CMPISharedEventData(
				int i_mpi_shared_event_type,
				int i_mpi_shared_event_id = 0,
				TVertexScalar i_mpi_shared_mouse_point_on_plane_x = 0,
				TVertexScalar i_mpi_shared_mouse_point_on_plane_y = 0,
				int i_mpi_shared_mouse_button = 0
		)	:
			mpi_shared_event_type(i_mpi_shared_event_type),
			mpi_shared_event_id(i_mpi_shared_event_id),
			mpi_shared_mouse_point_on_plane_x(i_mpi_shared_mouse_point_on_plane_x),
			mpi_shared_mouse_point_on_plane_y(i_mpi_shared_mouse_point_on_plane_y),
			mpi_shared_mouse_button(i_mpi_shared_mouse_button)
		{
		}


		CMPISharedEventData() :
			mpi_shared_event_type(-1),
			mpi_shared_event_id(0),
			mpi_shared_mouse_point_on_plane_x(0),
			mpi_shared_mouse_point_on_plane_y(0),
			mpi_shared_mouse_button(0)
		{
		}
	};

	std::list<CMPISharedEventData> cMPISharedEventDataList;

#endif

	/**
	 * CONSTRUCTOR
	 */
	CMainGui(
			int i_argc,			///< number of specified arguments
			char *i_argv[]		///< array with arguments
	)	:
		CMain(i_argc, i_argv),
		quit(false),
		cRenderWindow(*this, "Sierpi", 800, 600, false, 3, 3),
		cGlRenderOStream(cGlFreeType),
		cCommonShaderPrograms(0),
		screenshotEnumeration(0),
		simulation_update_number_of_threads_updated(-1)
	{
	}


	void setupGui()
	{
		/**********************************************
		 * SETUP Simulation GUI related interfaces
		 */
		cSimulation_MainGuiParameters = &(CSimulation_MainGuiParameters&)(*cSimulation);
		cSimulation_MainGuiInterface = &(CSimulation_MainGuiInterface&)(*cSimulation);


		/**********************************************
		 * CONFIGURATION HUD
		 */
		cGlFreeType.setup(12);
		CError_AppendReturn(cGlFreeType);


		/**********************************************
		 * LOAD SIERPI SVG
		 */
		cLinesFromSVG.loadSVGFile("data/setupGraphics/sierpi.svg");


		/***********************************************
		 * HUD
		 */
		// then we add the other configuration parameters and setup the HUD itsetup
		cGuiConfig.setup();

		// first we add the configuration parameters of the simulation itself
		cSimulation_MainGuiParameters->setupCGlHudConfigSimulation(cGuiConfig.cGlHudConfigMainRight);

		// first we add the configuration parameters of the simulation itself
		cSimulation_MainGuiParameters->setupCGlHudConfigVisualization(cGuiConfig.cGlHudConfigVisualization);

		cGuiConfig.assembleGui(cGlFreeType, cGlRenderOStream);

		cSimulation_MainGuiParameters->setupCGuiCallbackHandlers(cGuiConfig);

		// activate HUD
		cGuiConfig.setHudVisibility(true);


		/**********************************************
		 * COMMON SHADERS
		 */
		if (cCommonShaderPrograms.error())
		{
			error = cCommonShaderPrograms.error;
			return;
		}


		/**********************************************
		 * INSTALL CCONFIG CALLBACK HANDLERS
		 */
		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.simulation_update_number_of_threads_updated = c.cSimulation_MainParameters->simulation_threading_number_of_threads;
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cSimulation_MainParameters->simulation_threading_number_of_threads);
		}


		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.callback_take_screenshot();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cGuiConfig.take_screenshot);
		}
	}



	virtual ~CMainGui()
	{
	}



	void takeNextScreenshotImage()
	{
		char buffer[1024];
		sprintf(buffer, "screenshot_%08i.bmp", screenshotEnumeration);

		cRenderWindow.saveScreenshotWithThread(buffer);

		screenshotEnumeration++;
	}



	void callback_take_screenshot()
	{
		takeNextScreenshotImage();
		cGuiConfig.take_screenshot = false;
	}



	/**
	 * reset view (rotation + position)
	 */
	void resetView()
	{
		std::cout << "[ RESET View ]" << std::endl;


		/*
		 * update scale & translate parameters for visualization
		 */
		TVertexScalar max_size = CMath::max(cSimulation_MainParameters->simulation_parameter_domain_size_x, cSimulation_MainParameters->simulation_parameter_domain_size_y);

		cSimulation_MainParameters->visualization_domain_scale_x = (TVertexScalar)1.0/max_size;
		cSimulation_MainParameters->visualization_domain_scale_y = (TVertexScalar)1.0/max_size;

		cSimulation_MainParameters->visualization_domain_translate_x = -cSimulation_MainParameters->simulation_parameter_domain_origin_x;
		cSimulation_MainParameters->visualization_domain_translate_y = -cSimulation_MainParameters->simulation_parameter_domain_origin_y;

		cModelEyeBall.reset();
		camera.reset();

		domain_visualization_size =
			CMath::max(
				cSimulation_MainParameters->simulation_parameter_domain_size_x*cSimulation_MainParameters->visualization_domain_scale_x,
				cSimulation_MainParameters->simulation_parameter_domain_size_y*cSimulation_MainParameters->visualization_domain_scale_y
			);

		camera.setPosition(
				CVector<3,TVertexScalar>(
						0,
						domain_visualization_size*0.5,
						domain_visualization_size*1.1
				)
			);

		perspective_zoom = 1.0;

		far_plane = domain_visualization_size * (TVertexScalar)10.0;
		near_plane = domain_visualization_size * (TVertexScalar)0.01;

		camera.rotate(M_PI*0.1, 0, 0);

		camera.computeMatrices();

		cGuiConfig.visualization_enabled = true;
	}



	/**
	 * reset the simulation
	 */
	void reset()
	{
		std::cout << "[ RESET Simulation ]" << std::endl;

		// suffix first to output some interesting data
		simulation_loopSuffix();

		cSimulation_MainInterface->reset();

		/*****************************************************
		 * setup adaptive simulation
		 */
		cSimulation_MainInterface->setup_GridDataWithAdaptiveSimulation();

		simulation_loopPrefix();

		cProcessMemoryInformation.outputUsageInformation();


		/*
		 * reset view here to match to simulation data
		 */
		resetView();
	}


	bool gui_key_down(int key)
	{
		TVertexScalar px, py;

		switch(key)
		{
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			simulation_update_number_of_threads_updated = key-'0';
			break;

		case 'q':
			quit = true;
			break;

		case 'e':
			resetView();
			break;

		case 'r':
			reset();
			break;

		case 'v':
			cGuiConfig.visualization_enabled ^= true;
			break;

		case 'o':
			cGuiConfig.dofs_visualization_method++;
			break;

		case 'O':
			cGuiConfig.dofs_visualization_method--;
			break;


		case 'b':
			cGuiConfig.boundary_visualization_method++;
			break;

		case 'B':
			cGuiConfig.boundary_visualization_method--;
			break;

		case 'u':
			cGuiConfig.visualization_render_wireframe++;
			break;

		case 'U':
			cGuiConfig.visualization_render_wireframe--;
			break;

		case 'p':
			cGuiConfig.visualization_render_cluster_borders++;
			break;

		case 'a':	player_velocity[0] = CMath::max(player_velocity[0]-(TVertexScalar)1, (TVertexScalar)-1);	break;
		case 'd':	player_velocity[0] = CMath::min(player_velocity[0]+(TVertexScalar)1, (TVertexScalar)+1);	break;
		case 'w':	player_velocity[2] = CMath::max(player_velocity[2]-(TVertexScalar)1, (TVertexScalar)-1);	break;
		case 's':	player_velocity[2] = CMath::min(player_velocity[2]+(TVertexScalar)1, (TVertexScalar)+1);	break;


#if 0
		/*
		 * deactivated (drawing sierpi logo)
		 */
		case 'S':
			{
				TVertexScalar scale = 0.5;

				for (auto i = cLinesFromSVG.lineList.begin(); i != cLinesFromSVG.lineList.end(); i++)
				{
					auto &list = *i;
					for (auto d = list.begin(); d != list.end(); d++)
					{
						cSimulation->setup_CellDataAt2DPosition((*d)[0]*scale, (*d)[1]*scale);
					}
				}
			}
			break;
#endif

		case 'V':
			cProcessMemoryInformation.outputUsageInformation();
			break;

		case 'l':
			cGuiConfig.run_simulation_timesteps ^= true;
			break;

		case 'm':
			computeMouseOnPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputCellData(px, py);
			break;

		case 'M':
			computeMouseOnPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputEdgeCommunicationInformation(px, py);
			break;

		case 'n':
			computeMouseOnPlaneCoords(&px, &py);
			cSimulation_MainGuiInterface->debug_OutputClusterInformation(px, py);
			break;


/*
 * FULLSCREEN
 */
		case 'F':
			cRenderWindow.setWindowFullscreenState(!cRenderWindow.fullscreen_active);
			break;


/*
 * show/hide GUI
 */
		case ' ':
			cGuiConfig.setHudVisibility(!cGuiConfig.hud_visible);
			break;


/*
 * record window
 */
		case 'R':
			cGuiConfig.take_screenshot_series = !cGuiConfig.take_screenshot_series;
			break;

		default:
			return false;
		}

		return true;
	}


#if CONFIG_ENABLE_MPI
	/**
	 * synchronize keystrokes via MPI allreduce
	 */
	void mpi_test_and_sync_keystroke()
	{
		CMPISharedEventData event;

		while (true)
		{
			int mpi_event_node = -666;
			int mpi_event_node_output = 0;

			// if we have some data to send, request sending data by setting mpi_event_node to 0 or positive value
			if (cMPISharedEventDataList.size() > 0)
				mpi_event_node = mpi_rank;

			MPI_Allreduce(&mpi_event_node, &mpi_event_node_output, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD);

			// nothing to do?
			if (mpi_event_node_output < 0)
				break;

			if (mpi_event_node_output == mpi_rank)
			{
				event = cMPISharedEventDataList.front();
				cMPISharedEventDataList.pop_front();
			}


			/*
			 * Broadcast message
			 */
			MPI_Bcast(&event, sizeof(event), MPI_BYTE, mpi_event_node_output, MPI_COMM_WORLD);

			/*
			 * handle event
			 */
			assert(event.mpi_shared_event_type != -1);

			switch(event.mpi_shared_event_type)
			{
			case 0:
				if (!gui_key_down(event.mpi_shared_event_id))
					cSimulation_MainGuiInterface->gui_key_down_event(event.mpi_shared_event_id);
				break;

			case 1:
				if (!gui_key_up(event.mpi_shared_event_id))
					cSimulation_MainGuiInterface->gui_key_up_event(event.mpi_shared_event_id);
				break;

			case 2:
				switch(event.mpi_shared_event_id)
				{
				case -1:
					callback_mouse_button_up_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;

				case -2:
					callback_mouse_button_down_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;

				case -3:
					callback_mouse_motion_simulation(event.mpi_shared_mouse_point_on_plane_x, event.mpi_shared_mouse_point_on_plane_y, event.mpi_shared_mouse_button);
					break;
				}
				break;
			}
		}
	}
#endif


	/**
	 * callback method called whenever a key is pressed
	 */
	void callback_key_down(
			int key,
			int mod,
			int scancode,
			int unicode
	)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

#if !CONFIG_ENABLE_MPI

		if (!gui_key_down(key))
			cSimulation_MainGuiInterface->gui_key_down_event(key);

#else

		cMPISharedEventDataList.push_back(CMPISharedEventData(0, key));

#endif
	}



	bool gui_key_up(int key)
	{
		switch(key)
		{
			case 'a':	player_velocity[0] += 1;	break;
			case 'd':	player_velocity[0] -= 1;	break;
			case 'w':	player_velocity[2] += 1;	break;
			case 's':	player_velocity[2] -= 1;	break;
			default:
				return false;
		}

		return true;
	}

	void callback_key_up(int key, int mod, int scancode, int unicode)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}


#if !CONFIG_ENABLE_MPI

		if (!gui_key_up(key))
			cSimulation_MainGuiInterface->gui_key_up_event(key);

#else

		cMPISharedEventDataList.push_back(CMPISharedEventData(1, key));

#endif
	}


	void callback_quit()
	{
		quit = true;
	}


	void callback_mouse_motion_simulation(TVertexScalar px, TVertexScalar py, int button)
	{
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_LEFT);
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_RIGHT);
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])
		{
			cSimulation_MainGuiInterface->gui_mouse_motion_event(px, py, CRenderWindow::MOUSE_BUTTON_MIDDLE);

			perspective_zoom += (TVertexScalar)(inputState.relative_mouse_y);
		}
	}


	void callback_mouse_motion(int x, int y)
	{
		inputState.update((TVertexScalar)x*2.0/(TVertexScalar)cRenderWindow.window_width-1.0, (TVertexScalar)y*2.0/(TVertexScalar)cRenderWindow.window_height-1.0);

		TVertexScalar px, py;
		computeMouseOnPlaneCoords(&px, &py);

		int button = -1;

		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			button = CRenderWindow::MOUSE_BUTTON_LEFT;
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])
			button = CRenderWindow::MOUSE_BUTTON_RIGHT;
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])
			button = CRenderWindow::MOUSE_BUTTON_MIDDLE;


#if CONFIG_ENABLE_MPI

		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -3, px, py, button));

#else
		callback_mouse_motion_simulation(px, py, button);
#endif

		cGuiConfig.mouse_motion(x, cRenderWindow.window_height - y);
	}



	void callback_mouse_button_down_simulation(
			TVertexScalar px,
			TVertexScalar py,
			int button
	)	{
		cSimulation_MainGuiInterface->gui_mouse_button_down_event(px, py, button);

		if (button <= 3)
			inputState.mouse_buttons[button] = true;
	}



	void callback_mouse_button_down(
			int button
	)	{
		// if the button was pressed down within the config window, do nothing
		if (cGuiConfig.mouse_button_down(button))
			return;

		TVertexScalar px, py;
		computeMouseOnPlaneCoords(&px, &py);

#if CONFIG_ENABLE_MPI

		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -2, px, py, button));

#else

		callback_mouse_button_down_simulation(px, py, button);
#endif
	}



	void callback_mouse_button_up_simulation(
			TVertexScalar px,
			TVertexScalar py,
			int button
	)	{
		if (button <= 3)
			inputState.mouse_buttons[button] = false;
	}



	void callback_mouse_button_up(int button)
	{
		// if the button was pressed down within the config window, do nothing
		cGuiConfig.mouse_button_up(button);

#if CONFIG_ENABLE_MPI

		cMPISharedEventDataList.push_back(CMPISharedEventData(2, -1, 0, 0, button));

#else
		callback_mouse_button_up_simulation(0, 0, button);
#endif
	}


	void callback_mouse_wheel(int x, int y)
	{
		if (cGuiConfig.mouse_wheel(x, y))
			return;

		perspective_zoom += (TVertexScalar)y*(-0.1);
	}

	void callback_viewport_changed(int width, int height)
	{
	}


	GLSL::vec3 light_view_pos;

	void setupBlinnShader_Surface(
			CMatrix4<TVertexScalar> &local_view_model_matrix,
			CMatrix4<TVertexScalar> &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}



	void setupBlinnShader_Boundaries(
			CMatrix4<TVertexScalar> &local_view_model_matrix,
			CMatrix4<TVertexScalar> &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(0.7,0.45,0.2));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.2,0.2,0.5));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.2,0.2,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}



	void setupHeightColorShader(
			CMatrix4<TVertexScalar> &local_view_model_matrix,
			CMatrix4<TVertexScalar> &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cHeightColorBlinn.use();
		cCommonShaderPrograms.cHeightColorBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cHeightColorBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cHeightColorBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cHeightColorBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		cCommonShaderPrograms.cHeightColorBlinn.setupColorScaleAndOffset(0.025, 10);
	}



	/**
	 * render world scene
	 */
	void renderWorld()
	{
		CGlErrorCheck();

		CMatrix4<TVertexScalar> local_model_matrix = cModelEyeBall.rotationMatrix;
		local_model_matrix.loadIdentity();

		CMatrix4<TVertexScalar> local_view_model_matrix = camera.view_matrix * local_model_matrix;
		CMatrix4<TVertexScalar> local_pvm_matrix = camera.projection_matrix*local_view_model_matrix;

		// DOF1 (BATHYMETRY)
		setupBlinnShader_Boundaries(local_view_model_matrix, local_pvm_matrix);
		boundary_vizualization_description = cSimulation_MainGuiInterface->render_boundaries(cGuiConfig.boundary_visualization_method, cCommonShaderPrograms);

		// DOF2 (WATER SURFACE)
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
		dofs_vizualization_description = cSimulation_MainGuiInterface->render_DOFs(cGuiConfig.dofs_visualization_method, cCommonShaderPrograms);

		// CLUSTER SCANS
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		cSimulation_MainGuiInterface->render_ClusterScans(cGuiConfig.visualization_render_cluster_borders, cCommonShaderPrograms);

		// WIREFRAME
		cSimulation_MainGuiInterface->render_Wireframe(cGuiConfig.visualization_render_wireframe, cCommonShaderPrograms);

		// CLUSTERS
		setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
		cSimulation_MainGuiInterface->render_ClusterBorders(cGuiConfig.visualization_render_cluster_borders, cCommonShaderPrograms);


		cGlFreeType.viewportChanged(CVector<2,int>(cRenderWindow.window_width, cRenderWindow.window_height));
		cGlFreeType.setColor(GLSL::vec3(1,1,1));
		cGlFreeType.setPosition(CVector<2,int>(10, 30));
		cGlRenderOStream << "Rendering of DOFs: " << dofs_vizualization_description << std::endl;
		cGlFreeType.setPosition(CVector<2,int>(10, 10));
		cGlRenderOStream << "Rendering of boundary: " << boundary_vizualization_description << std::endl;

	}


	void computeMouseOnPlaneCoords(
			TVertexScalar *o_px,
			TVertexScalar *o_py
	)	{
		CMatrix4<TVertexScalar> inv_projection_matrix = camera.projection_matrix.getInverse();

		// dirty hack: get view center from view matrix
		CVector<3,TVertexScalar> ray_start = camera.getPosition();

		// end position is somewhere at the far plane
		CVector<4,TVertexScalar> unproj_space_vector4 = inv_projection_matrix*vec4f(inputState.mouse_x, -inputState.mouse_y, -1, 1);
		CVector<4,TVertexScalar> unproj_space_vector3 = unproj_space_vector4 / unproj_space_vector4[3];

		CVector<3,TVertexScalar> ray = camera.view_matrix.getTranspose3x3() * unproj_space_vector3;

		// compute point on plane
		CVector<3,TVertexScalar> pos = ray_start-ray*(ray_start[1]/ray[1]);
		*o_px = pos[0]/cSimulation_MainParameters->visualization_domain_scale_x;
		*o_py = -pos[2]/cSimulation_MainParameters->visualization_domain_scale_y;

//		std::cout << *o_px << " " << *o_py << std::endl;
		*o_px -= cSimulation_MainParameters->visualization_domain_translate_x;
		*o_py -= cSimulation_MainParameters->visualization_domain_translate_y;
	}


	bool simulationTimesteps()
	{
		if (cGuiConfig.run_simulation_timesteps)
		{
			/*
			 * the number of available openmp threads are set only during initialization when not running
			 * in interactive mode (GUI)
			 */

			for (int i = 0; i < cGuiConfig.visualization_simulation_steps_per_frame; i++)
				threading_simulationLoopIteration();
		}
		return true;
	}

	TVertexScalar getRand01()
	{
		return ((TVertexScalar)::random())*(1.0/(TVertexScalar)RAND_MAX);
	}

	void raindropsRefine(
			bool undelayed = false
	)
	{
		static TVertexScalar lastTicks = -1.0;
		static TVertexScalar nextRaindropAbsoluteTicks = 1.0;
		static TVertexScalar newRaindropRelativeTicks = 1.0;

		TVertexScalar recentTicks = cRenderWindow.getTicks();

		if (recentTicks > nextRaindropAbsoluteTicks || undelayed)
		{
			lastTicks = recentTicks;
			nextRaindropAbsoluteTicks = lastTicks+getRand01()*newRaindropRelativeTicks;

			cSimulation_MainInterface->setup_RadialDamBreak(
					(getRand01()*2.0-1.0)*cSimulation_MainParameters->simulation_parameter_domain_size_x,
					(getRand01()*2.0-1.0)*cSimulation_MainParameters->simulation_parameter_domain_size_y,
					CMath::max(
							CMath::min(
									0.1, 5000.0/(TVertexScalar)cSimulation_MainParameters->number_of_local_cells*getRand01()),
									0.01
							)
						*cSimulation_MainParameters->simulation_parameter_domain_size_x
					);
		}
	}


	/**
	 * render simulation data to screen
	 */
	void render()
	{
		camera.moveRelative(player_velocity*(TVertexScalar)cTime.frame_elapsed_seconds*domain_visualization_size*0.5);

		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
		{
			camera.rotate(	-inputState.relative_mouse_y*cTime.frame_elapsed_seconds*zoom*600.0,
							-inputState.relative_mouse_x*cTime.frame_elapsed_seconds*zoom*600.0,
							0
						);
		}


		zoom = CMath::exp(perspective_zoom)*0.2;

		TVertexScalar fzoom = zoom*near_plane;

		camera.frustum(-fzoom*cRenderWindow.aspect_ratio, fzoom*cRenderWindow.aspect_ratio, -fzoom, fzoom, near_plane, far_plane);
		camera.computeMatrices();

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		light_view_pos = camera.view_matrix*GLSL::vec3(2.0*domain_visualization_size*0.3, 4.0*domain_visualization_size*0.3, -6.0f*domain_visualization_size*0.3);

		renderWorld();

		cGuiConfig.render();

		inputState.clearRelativeMovement();
	}


	/**
	 * run the simulation loop
	 */
	void run()
	{
		// update view using simulation parameters
		resetView();

		simulation_loopPrefix();

		while (!quit)
		{
			cTime.update();

			if (simulation_update_number_of_threads_updated != -1)
			{
				if (cSimulation->simulation_threading_number_of_threads > simulation_update_number_of_threads_updated)
				{
					// shrink
					cSimulation->updateScanDatasets(simulation_update_number_of_threads_updated);

					threading_setNumThreads(simulation_update_number_of_threads_updated);
				}
				else
				{
					// increase
					threading_setNumThreads(simulation_update_number_of_threads_updated);

					cSimulation->updateScanDatasets(simulation_update_number_of_threads_updated);
				}

				simulation_update_number_of_threads_updated = -1;
			}

			std::ostringstream buf;

			if (cTime.fpsUpdatedInLastFrame)
			{
				buf << "Sierpi running @ " << cTime.fps << " FPS";
				buf << " | ";
#if CONFIG_ENABLE_MPI
				buf << "MPI rank: " << cSimulation->simulation_mpi_rank;
#endif
				buf << " | (" << cSimulation->number_of_local_cells << "/" << cSimulation->number_of_global_cells << ") Cells";
				buf << " | (" << cSimulation->number_of_local_clusters << "/" << cSimulation->number_of_global_clusters << ") Clusters";
				buf << " | ";
				buf << ((TVertexScalar)cSimulation->number_of_local_cells*cGuiConfig.visualization_simulation_steps_per_frame*cTime.fps)*0.000001 << " Mega Triangles per second (inaccurate)";
			}

			if (cSimulation->simulation_random_raindrops_activated)
			{
				raindropsRefine();
			}

			if (cGuiConfig.visualization_enabled)
			{
				glClearColor(0,0,0,0);
				glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				/*
				 * SIMULATION TIMESTEP
				 */
				simulationTimesteps();

				/*
				 * UPDATE FRAME
				 */
				render();

				if (cTime.fpsUpdatedInLastFrame)
				{
					cRenderWindow.setWindowTitle(buf.str().c_str());
				}

				cRenderWindow.eventLoop();

				if (cGuiConfig.take_screenshot_series)
				{
					takeNextScreenshotImage();
				}

				cRenderWindow.swapBuffer();
			}
			else
			{
				cRenderWindow.eventLoop();

				simulationTimesteps();
			}

#if CONFIG_ENABLE_MPI
			mpi_test_and_sync_keystroke();
#endif
		}

		simulation_loopSuffix();
	}
};




#endif /* CMAINGUI_HPP_ */
