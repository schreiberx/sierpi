
This document contains information about the files in the scenario directory.


===========================
XML Configuration Files:
===========================

Those XML files are separated into scopes (see the scenario directory for a few examples):

* compiler:
	Compile options: This options are equal to the scons compilation parameters (see `scons --help`).
	XML specified compilation options can be used via `scons --xml-config=[configuration_file.xml]`.
	The idea is to describe everything inside the xml configuration file including compilation (e. g. selecting a special flux) and simulation (e. g. domain size) parameters.

* threading:
	To select e. g. the number of cores, affinity masks, etc.

* sierpi:
	Parameters specialized for sierpi framework.

* swe/tsunami:
	Parameters related to a special simulation (e. g. SWE / Tsunami)


Each scope has several parameters in the following format:
<param name="[name-of-parameter]" value="[value-of-parameter]" />

Use an asterisk "*" for an attribute value to load default parameters from the Sierpi simulation framework or compilation option.



===========================
Starting a Simulation:
===========================

To forward this xml configuration to the Sierpi Framework, use e. g.
	`./build/sierpi_omp_release -c [xml_configuration_file.xml]`
to load the parameters from the xml file.

It is also possible to mix the still existing program parameters and XML file based parameters:
While the verbosity level can be overwritten by the xml file using
	`./build/sierpi_omp_release -v 0 -c [xml_configuration_file.xml]`,
the verbosity level is forced to be set to '0' using
	`./build/sierpi_omp_release -c [xml_configuration_file.xml] -v 0`.

Thus the order of evaluation is from left to right with the XML file being evaluated immediately when the corresponding '-c' program argument is handled.
