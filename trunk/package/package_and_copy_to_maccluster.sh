#! /bin/bash

PACKAGE_DIRS_AND_FILES="data doc src scenarios"
PACKAGE_DIRS_AND_FILES="$PACKAGE_DIRS_AND_FILES copyright.txt Makefile README SConstruct CCompileXMLOptions.py run_unittests.py run_valgrind"
 

PACKAGE_NAME="sierpi_`date +%Y_%m_%d`"
PACKAGE_DIR="$PACKAGE_NAME"
PACKAGE_TARBALL="$PACKAGE_NAME.tar.bz2"

echo "Creating package $PACKAGE_NAME"
rm -f -r "$PACKAGE_DIR"
mkdir "$PACKAGE_DIR"

echo " + copying files"
for file in $PACKAGE_DIRS_AND_FILES; do
	cp -r "../$file" "$@" "$PACKAGE_DIR"
done
mkdir "$PACKAGE_DIR/benchmarks_analytical"
cp -r "../benchmarks_analytical/cubed_sphere" "$PACKAGE_DIR/benchmarks_analytical/cubed_sphere"

echo " + removing svn information"
# remove svn from package directory
cd "$PACKAGE_DIR" && { find ./ -name ".svn" | xargs rm -Rf; } && cd ..

echo " + creating tarball $PACKAGE_TARBALL"
rm -f "$PACKAGE_TARBALL"
tar cjf "$PACKAGE_TARBALL" "$PACKAGE_DIR"

echo " + cleaning up"
rm -r "$PACKAGE_DIR"

echo " + copying to lxhalle..."
scp "$PACKAGE_TARBALL" lxhalle:~/tmp/

echo " + copying to mac cluster..."
ssh lxhalle scp "~/tmp/$PACKAGE_TARBALL" di68zip@mac-login-amd.tum-mac.cos.lrz.de:~/workspace

echo "Done!"


#ssh -L <local-port>:<gateway-address>:<ssh-port-target-server> <target-user>@<target-server>
#ssh -L 4242:lxhalle:22 di68zip@mac-login-amd.tum-mac.cos.lrz.de
#scp -P 4242 di68zip@127.0.0.1:/home/hpc/pr63so/di68zip/local_vars.sh .


