#! /usr/bin/python

import sys
import subprocess
import os
import time
import commands

base_depth=10

parallel_compilation_processes = 4

parallel_simulation_processes = -1

default_run_params=" -v -1 -n "+str(parallel_simulation_processes)
default_run_params_mpi=" -v -1 -n 2"
default_compile_params=" --enable-exit-on-small-timestep=on --enable-exit-on-instability-thresholds=on"

gccv = commands.getoutput('g++ -v').splitlines()

#if commands.getoutput('uname -n') in ['atsccs61']:
#	mpiexec='mpirun.openmpi'
#else:
#	mpiexec='mpirun'

mpiexec='mpirun'



def testGCC():
	#
	# TEST GCC VERSION
	#
	reqversion = [4,6,1]

	#
	# get gcc version using -v instead of -dumpversion since SUSE gnu compiler
	# returns only 2 instead of 3 digits with -dumpversion
	#
	gccv = commands.getoutput('g++ -v').splitlines()
	gccversion = gccv[-1].split(' ')[2].split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ERROR: At least GCC Version 4.6.1 necessary.'
			sys.exit(-1)


def testIntelCompiler():
	#
	# TEST INTEL VERSION
	#
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) > int(reqversion[i])):
			break
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'At least ICPC Version 12.1 necessary.'
			Exit(1)



mainmode = None
if len(sys.argv) > 1:
	mainmode = sys.argv[1]

subtests = None
if len(sys.argv) > 2:
	subtests = int(sys.argv[2])


prev_compiler_cmd=''


#
# executed the unit tests given by i
#
def unitTest(i, subtest):
	global prev_compiler_cmd
	cmd_append=' >/dev/null'

	CRED = '\033[91m'
	CGREEN = '\033[92m'
	CDEFAULT = '\033[0m'

	def print_err(s):
		print CRED+s+CDEFAULT

	def print_ok(s):
		print CGREEN+s+CDEFAULT

	print "Running subtest "+str(subtest)

	compiler_cmd = i[0]+' -j '+str(parallel_compilation_processes)

	if compiler_cmd != prev_compiler_cmd:
		print "COMPILING: "+compiler_cmd
		p = subprocess.Popen(['make clean'+cmd_append], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO MAKE CLEAN!")
			return

		prev_compiler_cmd = compiler_cmd

		p = subprocess.Popen([compiler_cmd+cmd_append], shell=True)
		p.wait()
		if p.returncode != 0:
			print_err(" > FAILED TO COMPILE! ("+compiler_cmd+")")
			sys.exit(-1)
	else:
		print "Using previously compiled program"

	startTime = time.time()

	build_dir='./build/'
	files = os.listdir(build_dir)
	match=None
	for j in files:
		print j
		if not os.path.isdir(build_dir+j):
			print "not isdir "+j
			if match != None:
				print "Two executables found, conflicting builds"
				sys.exit(-1)

			match=build_dir+j

	if match==None:
		print "No executable found!"
		sys.exit(-1)

	exec_cmd = match
	print "FOUND EXECUTABLE: "+exec_cmd

	exec_cmd_and_params=(i[1]%exec_cmd)+' '+cmd_append

	print "EXECUTING: "+exec_cmd_and_params

	p = subprocess.Popen([exec_cmd_and_params], shell=True)
	p.wait()
	if p.returncode != 0:
		print_err(" > TEST FAILED")
		sys.exit(-1)
	else:
		print_ok(" > TEST OK ("+str(time.time() - startTime)+" Seconds)")

	print "*"*60


#
# GNU Compiler tests
#
if mainmode == None or mainmode=='gnu_compiler':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: gnu_compiler"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=off --simulation=tsunami_serial --mode=debug'+default_compile_params, '%s -d 9'],
		['scons --compiler=gnu --threading=off --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s'],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s -d 7'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, '%s -d 8 -o 4 '+default_run_params+' -a 6'],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# EVEN/ODD Triangle tests
#
if mainmode == None or mainmode=='even_odd':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: even_odd"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 7'+default_run_params]
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# RK2 tests
#
if mainmode == None or mainmode=='rk':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: rk2"
	print "*"*60

	tests = [
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=2'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=2'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 7'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=3'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=3'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 7'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=4'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 3'+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-runge-kutta-order=4'+default_compile_params, '%s -d '+str(base_depth-4)+' -w 7'+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# Special compile options
#
if mainmode == None or mainmode=='sco':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: sco"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-based-split-and-join=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-based-split-and-join=on --enable-scan-force-threading-traversal=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-data=on --enable-scan-threading=on --enable-scan-based-split-and-join=on --enable-scan-force-threading-traversal=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-scan-force-threading-traversal=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -L 100 '+default_run_params],

		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-skip-adaptive-conforming-clusters=on'+default_compile_params, '%s -d '+str(base_depth-5)+' '+default_run_params],

		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-hyperbolic-cluster-local-time-stepping=on'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --enable-opencl=on'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug  --hyperbolic-adaptivity-mode=2'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release --enable-skip-adaptive-conforming-clusters=on'+default_compile_params, '%s -d '+str(base_depth-3)+' -o 4098 '+default_run_params+' -a 6'],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# adaptivity modes
#
if mainmode == None or mainmode=='adaptivity_modes':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: adaptivity_modes"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-adaptivity-mode=1'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --hyperbolic-adaptivity-mode=2'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
#		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=debug --fp-default-precision=double --hyperbolic-flux-solver-id=5 --hyperbolic-adaptivity-mode=3 --enable-augumented-riemann-eigen-coefficients=on'+default_compile_params, '%s -d '+str(base_depth-3)+' '+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)




#
# Intel Compiler tests
#
if mainmode == None or mainmode=='intel_compiler':
	testIntelCompiler()

	print
	print "*"*60
	print "* MAIN TESTS: intel_compiler"
	print "*"*60

	tests = [
#		['scons --compiler=intel --threading=off --simulation=tsunami_serial --mode=release'+default_compile_params, '%s -d '+str(base_depth+2)],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, '%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release'+default_compile_params, '%s -d '+str(base_depth-4)+' -o 4'+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# TBB Tests
#
if mainmode == None or mainmode=='tbb':
	print
	print "*"*60
	print "* MAIN TESTS: tbb"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release'+default_compile_params, '%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release'+default_compile_params, '%s -d '+str(base_depth-4)+' -o 4'+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)




#
# benchmarks
#
if mainmode == None or mainmode=='benchmarks_analytical':
	print
	print "*"*60
	print "* MAIN TESTS: benchmarks_analytical"
	print "*"*60

	tests = [
		['scons --xml-config=./scenarios/single_wave_on_simple_beach_1d.xml '+default_compile_params, '%s -c ./scenarios/single_wave_on_simple_beach_1d.xml'],
		['scons --xml-config=./scenarios/single_wave_on_simple_beach_2d.xml '+default_compile_params, '%s -c ./scenarios/single_wave_on_simple_beach_2d.xml -d 3'],
		['scons --xml-config=./scenarios/solitary_wave_on_composite_beach_1d.xml '+default_compile_params, '%s -c ./scenarios/solitary_wave_on_composite_beach_1d.xml -d 3'],
		['scons --xml-config=./scenarios/solitary_wave_on_composite_beach_2d.xml '+default_compile_params, '%s -c ./scenarios/solitary_wave_on_composite_beach_2d.xml -d 3 -L 20']
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)




#
# EULER
# 
if mainmode == None or mainmode=='euler':
	print
	print "*"*60
	print "* MAIN TESTS: euler"
	print "*"*60

	tests = [
		['scons --simulation=hyperbolic_parallel --sub-simulation=euler --mode=debug', '%s -a 2 -d '+str(base_depth-4)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)





#
# sphere
#
if mainmode == None or mainmode=='sphere':
	print
	print "*"*60
	print "* MAIN TESTS: sphere"
	print "*"*60

	tests = [
		['scons --xml-config=./scenarios/cubedSphere_intel.xml '+default_compile_params, '%s -c ./scenarios/cubedSphere_intel.xml -L 100 -d 10 '+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)

#
# MPI
#
if mainmode == None or mainmode=='mpi':
	print
	print "*"*60
	print "* MAIN TESTS: mpi"
	print "*"*60
	mpip=" -L 1000"

	tests = [
		['scons --enable-all-to-all-load-balancing=on --enable-mpi=on --mode=debug ', mpiexec+' -np 4 %s -d 4 -a 12 -r 0.5 -v 5 '+default_run_params_mpi, True],
		['scons --enable-mpi=on --mode=debug ', mpiexec+' -np 4 %s -d 4 -a 12 -r 0.5 -v 5 '+default_run_params_mpi, True],
		['scons --simulation=hyperbolic_parallel --threading=off --enable-mpi=on --mode=debug', mpiexec+' -np 3 %s -d '+str(base_depth)+default_run_params_mpi, True],
		['scons --simulation=hyperbolic_parallel --threading=omp --enable-mpi=on --mode=debug', mpiexec+' -np 2 %s -d '+str(base_depth)+default_run_params_mpi, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --mode=debug', mpiexec+' -np 3 %s -d '+str(base_depth-2)+default_run_params_mpi, True],
		['scons --simulation=hyperbolic_parallel --threading=omp --enable-mpi=on --mode=debug', mpiexec+' -np 4 %s -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --enable-mpi-split-and-join=off --mode=debug', mpiexec+' -np 3 %s -I 1 -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --enable-mpi-cluster-migration=off --mode=debug', mpiexec+' -np 3 %s -I 1 -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --enable-scan-based-split-and-join=on --mode=debug', mpiexec+' -np 3 %s -I 1 -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --enable-scan-based-split-and-join=on --enable-mpi-immediate-request-free-migration=on --mode=debug', mpiexec+' -np 3 %s -I 1 -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
		['scons --simulation=hyperbolic_parallel --threading=tbb --enable-mpi=on --mode=debug --enable-single-flux-evaluation-between-clusters=on', mpiexec+' -np 3 %s -d '+str(base_depth-2)+default_run_params_mpi+' '+mpip, True],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# FLUX TESTS
#
if mainmode == None or mainmode=='fluxes':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: fluxes"
	print "*"*60

	tests = [
		# 1st order with fixed number of timesteps
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=0 --hyperbolic-basis-functions-degree=1'+default_compile_params,
				'%s -d '+str(base_depth-4)+' -L 2000'+default_run_params+' -C 0.1'],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=1 --hyperbolic-basis-functions-degree=1'+default_compile_params,
				'%s -d '+str(base_depth-2)+' -L 2000'+default_run_params+' -C 0.1'],
		# 0th order
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=2 --hyperbolic-basis-functions-degree=0'+default_compile_params,
				'%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=3 --hyperbolic-basis-functions-degree=0'+default_compile_params,
				'%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=4 --hyperbolic-basis-functions-degree=0'+default_compile_params,
				'%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=intel --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=5 --hyperbolic-basis-functions-degree=0 --fp-default-precision=double'+default_compile_params,
				'%s -d '+str(base_depth-2)+default_run_params],
		['scons --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=release --hyperbolic-flux-solver-id=6 --hyperbolic-basis-functions-degree=0'+default_compile_params,
				'%s -d '+str(base_depth-2)+default_run_params],
	]


	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# TYPE AND DEGREE TESTS
#

if mainmode == None or mainmode=='type_and_degree':
	testGCC()

	print
	print "*"*60
	print "* MAIN TESTS: type_and_degree"
	print "*"*60

	tests=[]

	# D=2

	#types=[0,1,2,3]
	subsimulation=['tsunami', 'euler']
	types=[0,1,3]
	degrees=[0,1,2]

	for ss in subsimulation:
		for t in types:
			for d in degrees:
				tests.append(
					[
						'scons --compiler=gnu --simulation=hyperbolic_parallel --sub-simulation='+ss+' --mode=debug --hyperbolic-basis-functions-type='+str(t)+' --hyperbolic-basis-functions-degree='+str(d)+' --simulation-linear-velocity-damping-factor=0.001 '+default_compile_params,
						'%s -C '+str(float(0.25)/float(d+1))+' -d '+str(base_depth-4)+' -L 1000'+default_run_params
					]
				)

	# D=3

	subsimulation=['euler_ml']
	types=[0]
	#types=[0,1]
	degrees=[0,1,2]

	for ss in subsimulation:
		for t in types:
			for d in degrees:
				tests.append(
					[
						'scons --compiler=gnu --simulation=hyperbolic_parallel --sub-simulation='+ss+' --mode=debug --hyperbolic-basis-functions-type='+str(t)+' --hyperbolic-basis-functions-degree='+str(d)+' --simulation-linear-velocity-damping-factor=0.001 --simulation-number-of-layers=10 '+default_compile_params,
						'%s -C 0.01 -d '+str(base_depth-4)+' -L 100 '+default_run_params
					]
				)


	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)


#
# TSUNAMI
#
if mainmode == None or mainmode=='tsunami':
	print
	print "*"*60
	print "* MAIN TESTS: tsunami"
	print "*"*60

	tests = [
		['scons --xml-config=./scenarios/netcdf_intel_release.xml '+default_compile_params, '%s -c ./scenarios/netcdf_intel_release.xml -A 1 -t 20000 -r 1/0.1 -v 3  -o 16384 -u 6 -U 6 -b 10 -a 12 -d 4 -C 0.5 -D 834498.794062/528004.720595/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21401.txt,935356.566012/-817289.628677/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21413.txt,545735.266126/62716.4740303/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21418.txt,1058466.21575/765077.767857/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21419.txt -v 0'],
#		['scons --xml-config=./scenarios/netcdf_intel_release.xml '+default_compile_params, '%s -c ./scenarios/netcdf_intel_release.xml -A 1 -t 20000 -r 1/0.1 -v 3  -o 16384 -u 6 -U 6 -b 10 -a 18 -d 4 -C 0.5 -D 834498.794062/528004.720595/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21401.txt,935356.566012/-817289.628677/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21413.txt,545735.266126/62716.4740303/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21418.txt,1058466.21575/765077.767857/output_d4_a18_cfl0.5_rk1_refine1_coarsen0.1_dart21419.txt -v 5'],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# terrain backends (asagi/netcdf)
#
if mainmode == None or mainmode=='terrain':
	print
	print "*"*60
	print "* MAIN TESTS: terrain"
	print "*"*60

	tests = [
		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release --enable-netcdf=on'+default_compile_params, '%s -d '+str(base_depth-2)+default_run_params],
#
# asagi currently not able to compile
#		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release --enable-asagi=on'+default_compile_params, '%s -d '+str(base_depth-2)+default_run_params],
#		['scons --compiler=intel --threading=tbb --simulation=hyperbolic_parallel --mode=release --enable-mpi=on --enable-asagi=on'+default_compile_params, '%s -d '+str(base_depth-2)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# iPMO
#
if mainmode == None or mainmode=='ipmo':
	print
	print "*"*60
	print "* MAIN TESTS: iPMO"
	print "*"*60

	tests = [
		['scons --threading=ipmo', 'echo %s -d 8 -a 4'],
		['scons --xml-config=./scenarios/itbb_async_mpi.xml', 'echo dummy %s', True],
		['scons --xml-config=./scenarios/ipmo_async_mpi.xml', 'echo dummy %s', True],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)



#
# GUI Tests
#

if not os.environ.has_key('DISPLAY'):
	print 'DISPLAY environment variable not set => skipping GUI tests'
else:

	if mainmode == None or mainmode=='gui':
		print
		print "*"*60
		print "* MAIN TESTS: gui"
		print "*"*60

		tests = [
			['scons --enable-gui=on --compiler=gnu --threading=off --simulation=tsunami_serial --mode=debug'+default_compile_params, '%s -L 100 -d '+str(base_depth-2)],
			['scons --enable-gui=on --compiler=gnu --threading=omp --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s -L 100 -d '+str(base_depth-2)+default_run_params],
			['scons --enable-gui=on --compiler=gnu --threading=tbb --simulation=hyperbolic_parallel --mode=debug'+default_compile_params, '%s -L 100 -d '+str(base_depth-2)+default_run_params],

			# TODO: also enable node based visualization during execution
			['scons --enable-gui=on --compiler=gnu --threading=tbb --simulation=hyperbolic_parallel --enable-traversals-with-nodes=on --mode=debug'+default_compile_params, '%s -L 100 -d '+str(base_depth-2)+default_run_params],
		]

		if subtests != None:
			unitTest(tests[subtests], subtests)
			sys.exit(0)

		for i in range(0, len(tests)):
			unitTest(tests[i], i)



#
# VALGRIND
# 
if mainmode == None or mainmode=='valgrind':
	print
	print "*"*60
	print "* MAIN TESTS: valgrind"
	print "*"*60

	valgrind_prefix='valgrind --leak-check=full --show-reachable=yes --track-origins=yes --tool=memcheck --trace-children=yes --error-exitcode=1'

	tests = [
		['scons --simulation=hyperbolic_parallel --mode=debug', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=hyperbolic_parallel --mode=debug --hyperbolic-basis-functions-degree=1', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=hyperbolic_parallel --mode=debug --sub-simulation=euler', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=tsunami_1d --mode=debug', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=tsunami_serial --mode=debug', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=hyperbolic_parallel --mode=debug --hyperbolic-flux-solver-id=5 --fp-default-precision=double --compiler=intel', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],
		['scons --simulation=hyperbolic_parallel --mode=debug --enable-augumented-riemann-eigen-coefficients=on --hyperbolic-flux-solver-id=5 --fp-default-precision=double --compiler=intel', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params, True],

#		['scons --compiler=intel --simulation=hyperbolic_parallel --threading=omp --mode=debug', valgrind_prefix+' %s -L 10 -a 2 -d '+str(base_depth-4)+default_run_params],
	]

	if subtests != None:
		unitTest(tests[subtests], subtests)
		sys.exit(0)

	for i in range(0, len(tests)):
		unitTest(tests[i], i)

