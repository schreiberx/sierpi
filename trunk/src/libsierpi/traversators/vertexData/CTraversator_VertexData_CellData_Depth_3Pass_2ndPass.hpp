/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTRAVERSATOR_VERTEXDATA_ELEMENTDATA_DEPTH_HPP_
#define CTRAVERSATOR_VERTEXDATA_ELEMENTDATA_DEPTH_HPP_

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"

namespace sierpi
{
namespace travs
{


template <
	class p_CKernelClass,
	typename p_CHyperbolicTypes
>
class CTraversator_VertexData_CellData_Depth_3PassTraversal
{
public:
	typedef p_CKernelClass CKernelClass;

	typedef typename CKernelClass::CCellData CCellData;
	typedef typename CKernelClass::TVertexScalar TVertexScalar;
	typedef typename CKernelClass::TVisualizationVertexData TVisualizationVertexData;
	typedef typename CKernelClass::TVisualizationVertexData TVertexData;

	typedef typename p_CHyperbolicTypes::CSimulationStacks CSimulationStacks;


	/**
	 * implementation of kernel class
	 */
	CKernelClass cKernelClass;

	/**
	 * a few typedefs which could be utilized by the traversator to get more informations
	 */
	typedef CTraversator_VertexData_CellData_Depth_3PassTraversal<CKernelClass, p_CHyperbolicTypes> TThisClass;


	/**
	 * stack and lifo handlers used in this traversator
	 */
	CStackReaderTopDown<char> structure_lifo_in;

	/**
	 * lifo and fifo iterator for element data
	 */
	CStackReaderTopDown<CCellData> element_data_lifo;
	CStackReaderBottomUp<CCellData> element_data_fifo;

	/**
	 * pointer to the corresponding communication stacks
	 */
	CStack<TVisualizationVertexData> *vertex_data_comm_left_edge_stack;
	CStack<TVisualizationVertexData> *vertex_data_comm_right_edge_stack;

	/**
	 * pointer to stack used for edge communication in forward order
	 */
	CStack<TVisualizationVertexData> *vertex_data_comm_buffer_input;
	/**
	 * pointer to stack used for edge communication in forward order
	 */
	CStack<TVisualizationVertexData> *vertex_data_comm_buffer_output;

	/**
	 * include automagically generated code
	 */
#include "auto/CTraversator_VertexData_CellData_3Pass_2ndPass.hpp"
#include "../CTraversatorClassInc_SetupTwoPass.hpp"



	/**
	 * run the first traversal
	 */
	void action(
			CSimulationStacks *io_cSimulationStacks
	)
	{
		structure_lifo_in.setup(io_cSimulationStacks->structure_stacks.backward);
		element_data_fifo.setup(io_cSimulationStacks->cell_data_stacks.forward);

		assert(!io_cSimulationStacks->structure_stacks.backward.isEmpty());

		vertex_data_comm_left_edge_stack = &io_cSimulationStacks->vertex_data_comm_exchange_right_edge_stack;
		vertex_data_comm_right_edge_stack = &io_cSimulationStacks->vertex_data_comm_exchange_left_edge_stack;

		vertex_data_comm_buffer_input = &io_cSimulationStacks->vertex_data_comm_buffer;
		vertex_data_comm_buffer_output = &io_cSimulationStacks->vertex_data_comm_buffer_semi;

		// pre hook
		cKernelClass.traversal_pre_hook();

		// SFC traversals
		(this->*sfcRecursiveMethod_Backward)
			(
				cTriangleFactory.vertices[0][0], cTriangleFactory.vertices[0][1],
				cTriangleFactory.vertices[1][0], cTriangleFactory.vertices[1][1],
				cTriangleFactory.vertices[2][0], cTriangleFactory.vertices[2][1],
				cTriangleFactory.recursionDepthFirstRecMethod
			);

		// post hook
		cKernelClass.traversal_post_hook();

		assert(vertex_data_comm_buffer_input->isEmpty());
		assert(vertex_data_comm_left_edge_stack->isEmpty());
		assert(vertex_data_comm_right_edge_stack->isEmpty());
		assert(structure_lifo_in.isEmpty());
		assert(element_data_fifo.isEmpty());

		return;
	}

};

}
}

#endif /* CSTRUCTURE_VERTEXDATA_H_ */
