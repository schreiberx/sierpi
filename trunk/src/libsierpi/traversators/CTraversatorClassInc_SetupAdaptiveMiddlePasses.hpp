/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CTraversatorClassInc_SetupAdaptiveMiddlePasses.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


TRecursiveMethod sfcRecursiveMethod_Forward;
TRecursiveMethod sfcRecursiveMethod_Backward;

/**
 * the triangle factory for this traversal
 */
CTriangle_Factory cTriangleFactory;


/**
 * depth limiters to restrict maximum/minimum refinement
 */
int depth_limiter_min;
int depth_limiter_max;


/**
 * setup the initial cluster traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &i_cTriangleFactory	///< triangle factory to find the first method
)
{
	cTriangleFactory = i_cTriangleFactory;

	sfcRecursiveMethod_Forward = getSFCMethod<SFC_METHOD_OLD_TO_NEW_EDGE_TYPES>(cTriangleFactory);
	sfcRecursiveMethod_Backward = getSFCMethod<SFC_METHOD_NEW_TO_OLD_EDGE_TYPES | SFC_METHOD_FORCE_BACKWARD>(cTriangleFactory);
}


/**
 * setup the parameters with the one by the parent cluster
 */
void setup_Cluster(
		TThisClass &i_parentTraversator,		///< information of parent traversator
		CTriangle_Factory &i_triangleFactory	///< triangle factory to find the first method
)
{
	// make sure that this is really a root node
	assert(i_triangleFactory.clusterTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);

	setup_sfcMethods(i_triangleFactory);

	cKernelClass.setup_WithKernel(i_parentTraversator.cKernelClass);
}
