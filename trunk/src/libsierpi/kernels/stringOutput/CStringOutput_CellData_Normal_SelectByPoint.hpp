/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Jan 12, 2011
 *      Author: schreibm
 */

#ifndef KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_NORMAL_HPP_
#define KERNEL_CSTRING_OUTPUT_ELEMENT_DATA_NORMAL_HPP_

#include <cmath>
#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "libmath/CVertex2d.hpp"

#include "libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#if CONFIG_BENCHMARK_SPHERE
#include "simulations/hyperbolic_common/subsimulation_benchmark_sphere/tsunami_benchmarks/CDeformationalFlow.hpp"
#endif

namespace sierpi
{
namespace kernels
{

template <typename t_CSimulationStacksAndTypes>
class CStringOutput_CellData_Normal_SelectByPoint
{
public:
	typedef t_CSimulationStacksAndTypes CSimulationStacks_;
	typedef typename t_CSimulationStacksAndTypes::CSimulationTypes::CCellData	CCellData_;
	typedef float TVertexScalar;
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

private:
	typedef CVertex2d<TVertexScalar> TVertex;

	TVertexScalar px, py;

public:
	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<CStringOutput_CellData_Normal_SelectByPoint<t_CSimulationStacksAndTypes>, t_CSimulationStacksAndTypes> TRAV;

	CStringOutput_CellData_Normal_SelectByPoint()	:
		px(0),
		py(0)
	{
	}



	inline bool op_cell(
			TVertexScalar v0x, TVertexScalar v0y,
			TVertexScalar v1x, TVertexScalar v1y,
			TVertexScalar v2x, TVertexScalar v2y,

			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,

			const CCellData_ *i_cCellData
	)
	{
		if (!CPointInTriangleTest<TVertexScalar>::test(v0x, v0y, v1x, v1y, v2x, v2y, px, py))
			return false;

		std::cout << std::endl;
		std::cout << "========================" << std::endl;
		std::cout << "| ELEMENT DATA (" << px << ", " << py << ")" << std::endl;
		std::cout << "========================" << std::endl;
#if !CONFIG_BENCHMARK_SPHERE
		std::cout << "  Element Vertices: " << std::endl;
		std::cout << "   +  left: " << v0x << ", " << v0y << "    " << std::endl;
		std::cout << "   + right: " << v1x << ", " << v1y << "    " << std::endl;
		std::cout << "   +   top: " << v2x << ", " << v2y << "    " << std::endl;
		std::cout << std::endl;
		std::cout << "  Normals: " << std::endl;
		std::cout << "   +   hyp: " << hyp_normal_x << ", " << hyp_normal_y << std::endl;
		std::cout << "   + right: " << right_normal_x << ", " << right_normal_y << std::endl;
		std::cout << "   +  left: " << left_normal_x << ", " << left_normal_y << std::endl;
		std::cout << std::endl;
#endif
		std::cout << "  Data stored in element (world space): " << std::endl;
		i_cCellData->outputVerboseData(std::cout, -right_normal_x, -right_normal_y);
		std::cout << std::endl;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1 // sphere mapping

#if !CONFIG_BENCHMARK_SPHERE
		std::cout << "Cell Data" << std::endl;
		std::cout << "face id: " << i_cCellData->face << std::endl;

		for(int i = 0; i < 3; i++)
			std::cout << "vertex " << i << ": " << i_cCellData->vertices[i][0] << ", " << i_cCellData->vertices[i][1] << std::endl;
#endif

		for(int i = 0; i < 3; i++)
			std::cout << "length of edge " << i << ": " << i_cCellData->edgeLength[i] << std::endl;

		std::cout << "cell area: " << i_cCellData->cellArea << std::endl;
		T s = (i_cCellData->edgeLength[0] + i_cCellData->edgeLength[1] + i_cCellData->edgeLength[2]) / 2.0;
		T area = std::sqrt( s * (s - i_cCellData->edgeLength[0]) * (s - i_cCellData->edgeLength[1]) * (s - i_cCellData->edgeLength[2]) );
		std::cout << area << std::endl;
		std::cout << "sphere coordinates:" << std::endl;
		T lat, lon;
		for(int i = 0; i < 3; i++){
			std::cout << "vertex " << i << ":  x: " << i_cCellData->coordinates3D[i][0] << " y: " << i_cCellData->coordinates3D[i][1] << " z: " << i_cCellData->coordinates3D[i][2] << std::endl;

			CCube_To_Sphere_Projection::project3DToLLDeg(i_cCellData->coordinates3D[i][0],
					i_cCellData->coordinates3D[i][1],
					i_cCellData->coordinates3D[i][2],
					&lat, &lon);
			std::cout << "vertex " << i << ":  lat: " << lat << " lon: " << lon << std::endl;
		}
#endif
#if CONFIG_BENCHMARK_SPHERE
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		std::cout << "fluxes on h/r/l: " << *(i_cCellData->fluxes[0]) << ", " << *(i_cCellData->fluxes[1]) << ", " << *(i_cCellData->fluxes[2]) << std::endl;
		std::cout << "fluxes*edges on h/r/l: " << *(i_cCellData->fluxes[0]) * i_cCellData->edgeLength[0]
				<< ", " << *(i_cCellData->fluxes[1]) * i_cCellData->edgeLength[1]
				<< ", " << *(i_cCellData->fluxes[2]) * i_cCellData->edgeLength[2]
				<< std::endl;
		std::cout << "flux*edge sum: " << *(i_cCellData->fluxes[0]) * i_cCellData->edgeLength[0] +
				*(i_cCellData->fluxes[1]) * i_cCellData->edgeLength[1] +
				*(i_cCellData->fluxes[2]) * i_cCellData->edgeLength[2] << std::endl;
		T x,y;
		T s_x, s_y, s_z;
//		CDeformationalFlow::element_centroid(i_cCellData->coordinates3D, &s_x, &s_y, &s_z);
//		CDeformationalFlow::compute_worldspaceVelocity(s_x, s_y, s_z, &x, &y, true, (T)right_normal_x, (T)right_normal_y, i_cCellData);
		if(1){
		std::cout << std::endl << "### flux computation on hypotenuse ###" << std::endl;
		CDeformationalFlow::edge_center(i_cCellData->coordinates3D[CDeformationalFlow::E_HYP], i_cCellData->coordinates3D[(CDeformationalFlow::E_HYP+1)%3],
				&s_x, &s_y, &s_z);
		CDeformationalFlow::compute_worldspaceVelocity(s_x, s_y, s_z, &x, &y, true,
				(T)right_normal_x, (T)right_normal_y, i_cCellData, CDeformationalFlow::E_HYP);
		std::cout << std::endl << "### flux computation on right edge ###" << std::endl;
		CDeformationalFlow::edge_center(i_cCellData->coordinates3D[CDeformationalFlow::E_RIGHT], i_cCellData->coordinates3D[(CDeformationalFlow::E_RIGHT+1)%3],
				&s_x, &s_y, &s_z);
		CDeformationalFlow::compute_worldspaceVelocity(s_x, s_y, s_z, &x, &y, true,
				(T)right_normal_x, (T)right_normal_y, i_cCellData, CDeformationalFlow::E_RIGHT);
		std::cout << std::endl << "### flux computation on left edge ###" << std::endl;
		CDeformationalFlow::edge_center(i_cCellData->coordinates3D[CDeformationalFlow::E_LEFT], i_cCellData->coordinates3D[(CDeformationalFlow::E_LEFT+1)%3],
				&s_x, &s_y, &s_z);
		CDeformationalFlow::compute_worldspaceVelocity(s_x, s_y, s_z, &x, &y, true,
				(T)right_normal_x, (T)right_normal_y, i_cCellData, CDeformationalFlow::E_LEFT);
		}
#else
		T v_x, v_y;
		std::cout << std::endl << "### flux computation on hypotenuse ###" << std::endl;
		CDeformationalFlow::worldspace_velocities_2d(i_cCellData, CDeformationalFlow::E_HYP, &v_x, &v_y, true);
		std::cout << std::endl << "### flux computation on right edge ###" << std::endl;
		CDeformationalFlow::worldspace_velocities_2d(i_cCellData, CDeformationalFlow::E_RIGHT, &v_x, &v_y, true);
		std::cout << std::endl << "### flux computation on left edge ###" << std::endl;
		CDeformationalFlow::worldspace_velocities_2d(i_cCellData, CDeformationalFlow::E_LEFT, &v_x, &v_y, true);
#endif
#endif

		return true;
	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}


	inline void setup(
			TVertexScalar p_px,
			TVertexScalar p_py
	)
	{
		px = p_px;
		py = p_py;
	}


	inline void setup_WithKernel(
			CStringOutput_CellData_Normal_SelectByPoint<t_CSimulationStacksAndTypes> &parent
	)
	{
		px = parent.px;
		py = parent.py;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
