/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
#ifndef CSETUP_ELEMENT_DATA_VERTICES_HPP_
#define CSETUP_ELEMENT_DATA_VERTICES_HPP_

#include "CValidateTypes.hpp"
#include "libsierpi/traversators/vertices/CTraversator_Vertices_CellData.hpp"

namespace sierpi
{
namespace kernels
{

class CSetup_CellData_Vertices_Validate
{
public:
	typedef CValCellData CCellData;

	typedef sierpi::travs::CTraversator_Vertices_CellData<CSetup_CellData_Vertices_Validate> TRAV;

	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

public:

	inline CSetup_CellData_Vertices_Validate()
	{
	}

	inline void initBorderVertices(TVertexScalar v[4][2])
	{
		v[0][0] = -1;
		v[0][1] = -1;

		v[1][0] = 1;
		v[1][1] = -1;

		v[2][0] = 1;
		v[2][1] = 1;

		v[3][0] = -1;
		v[3][1] = 1;
	}

	inline void op_cell(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CValCellData *element)
	{
		element->hyp[0] = (vx1+vx2)*0.5;
		element->hyp[1] = (vy1+vy2)*0.5;

		element->right[0] = (vx2+vx3)*0.5;
		element->right[1] = (vy2+vy3)*0.5;

		element->left[0] = (vx3+vx1)*0.5;
		element->left[1] = (vy3+vy1)*0.5;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}
};

}
}

#endif
