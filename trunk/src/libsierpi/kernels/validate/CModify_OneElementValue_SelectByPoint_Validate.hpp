/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *
 *  Created on: Feb 3, 2011
 *      Author: schreibm
 */

#ifndef CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT_VALIDATE
#define CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT_VALIDATE

#include "libsierpi/traversators/vertices/CTraversator_Vertices_CellData.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "CValidateTypes.hpp"

namespace sierpi
{
namespace kernels
{

class CModify_OneElementValue_SelectByPoint_Validate
{
public:
	typedef CValCellData CCellData;

	typedef sierpi::travs::CTraversator_Vertices_CellData<CModify_OneElementValue_SelectByPoint_Validate> TRAV;

	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertex;

	TVertexScalar px, py;

	inline CModify_OneElementValue_SelectByPoint_Validate()
	{
	}

	inline bool op_cell(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CValCellData *element)
	{
		if (!CPointInTriangleTest<TVertexScalar>::test(vx1, vy1, vx2, vy2, vx3, vy3, px, py))
			return false;

		element->refine = true;
		return true;
	}

	inline void setup_RootCluster(
			TVertexScalar p_px,
			TVertexScalar p_py
	)
	{
		px = p_px;
		py = p_py;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}
};

}
}

#endif /* CMODIFY_ONEVERTEX_HPP_ */
