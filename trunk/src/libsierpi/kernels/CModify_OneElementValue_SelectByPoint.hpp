/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 3, 2011
 *      Author: schreibm
 */

#ifndef CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT
#define CMODIFY_ONEELEMENTVALUE_SELECTBYPOINT

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_CellData.hpp"
#include "libmath/CPointInTriangleTest.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{

template <typename t_CSimulationStacks>
class CModify_OneElementValue_SelectByPoint
{
public:
	typedef typename t_CSimulationStacks::CSimulationTypes::CCellData	CCellData_;
	typedef float TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertex;

	typedef sierpi::travs::CTraversator_VertexCoords_CellData<CModify_OneElementValue_SelectByPoint<t_CSimulationStacks>, t_CSimulationStacks> TRAV;

	CCellData_ *new_element_value;

	TVertexScalar px, py;


	inline CModify_OneElementValue_SelectByPoint()
	{
	}


	inline bool op_cell(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			CCellData_ *element)
	{
		if (!CPointInTriangleTest<TVertexScalar>::test(vx1, vy1, vx2, vy2, vx3, vy3, px, py))
			return false;

		*element = *new_element_value;
		return true;
	}

	inline void setup(
			TVertexScalar p_px,
			TVertexScalar p_py,
			CCellData_ *p_new_element_value
	)
	{
		px = p_px;
		py = p_py;

		new_element_value = p_new_element_value;
	}

	inline void setup_WithKernel(
			CModify_OneElementValue_SelectByPoint &parent
	)
	{
		px = parent.px;
		py = parent.py;

		new_element_value = parent.new_element_value;
	}

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}
};

}
}

#endif /* CMODIFY_ONEVERTEX_HPP_ */
