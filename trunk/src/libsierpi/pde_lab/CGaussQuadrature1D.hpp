/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGAUSSQUADRATURE_1D_HPP_
#define CGAUSSQUADRATURE_1D_HPP_


namespace sierpi
{
namespace pdelab
{



/*
 * weights+positions: see http://de.wikipedia.org/wiki/Gau%C3%9F-Quadratur
 *
 * Gaussian quadrature using N points can provide an accuracy for polynomials of degree up to 2N-1
 */


template <typename T, int t_degreeOfPolynomial>
class CGaussQuadrature1D
{
#define CGAUSSQUADRATURE1D_NUMBER_OF_VALUES		(10)

	static T *abscissa;

public:
	static inline const T* getPositions()
	{
		static T *abscissa;
		static bool setup = false;
		static int S = (t_degreeOfPolynomial/2);

		if (!setup)
		{
			static T l_abscissa[CGAUSSQUADRATURE1D_NUMBER_OF_VALUES][CGAUSSQUADRATURE1D_NUMBER_OF_VALUES];

			l_abscissa[0][0] = 0;
			l_abscissa[0][1] = 0;
			l_abscissa[0][2] = 0;
			l_abscissa[0][3] = 0;
			l_abscissa[0][4] = 0;
			l_abscissa[0][5] = 0;
			l_abscissa[0][6] = 0;
			l_abscissa[0][7] = 0;
			l_abscissa[0][8] = 0;
			l_abscissa[0][9] = 0;
			l_abscissa[1][0] = -0.57735026918962576451e0;
			l_abscissa[1][1] = 0.57735026918962576451e0;
			l_abscissa[1][2] = 0;
			l_abscissa[1][3] = 0;
			l_abscissa[1][4] = 0;
			l_abscissa[1][5] = 0;
			l_abscissa[1][6] = 0;
			l_abscissa[1][7] = 0;
			l_abscissa[1][8] = 0;
			l_abscissa[1][9] = 0;
			l_abscissa[2][0] = -0.77459666924148337704e0;
			l_abscissa[2][1] = 0;
			l_abscissa[2][2] = 0.77459666924148337704e0;
			l_abscissa[2][3] = 0;
			l_abscissa[2][4] = 0;
			l_abscissa[2][5] = 0;
			l_abscissa[2][6] = 0;
			l_abscissa[2][7] = 0;
			l_abscissa[2][8] = 0;
			l_abscissa[2][9] = 0;
			l_abscissa[3][0] = -0.86113631159405257522e0;
			l_abscissa[3][1] = -0.33998104358485626480e0;
			l_abscissa[3][2] = 0.33998104358485626480e0;
			l_abscissa[3][3] = 0.86113631159405257522e0;
			l_abscissa[3][4] = 0;
			l_abscissa[3][5] = 0;
			l_abscissa[3][6] = 0;
			l_abscissa[3][7] = 0;
			l_abscissa[3][8] = 0;
			l_abscissa[3][9] = 0;
			l_abscissa[4][0] = -0.90617984593866399280e0;
			l_abscissa[4][1] = -0.53846931010568309104e0;
			l_abscissa[4][2] = 0;
			l_abscissa[4][3] = 0.53846931010568309104e0;
			l_abscissa[4][4] = 0.90617984593866399280e0;
			l_abscissa[4][5] = 0;
			l_abscissa[4][6] = 0;
			l_abscissa[4][7] = 0;
			l_abscissa[4][8] = 0;
			l_abscissa[4][9] = 0;
			l_abscissa[5][0] = -0.93246951420315202781e0;
			l_abscissa[5][1] = -0.66120938646626451366e0;
			l_abscissa[5][2] = -0.23861918608319690863e0;
			l_abscissa[5][3] = 0.23861918608319690863e0;
			l_abscissa[5][4] = 0.66120938646626451366e0;
			l_abscissa[5][5] = 0.93246951420315202781e0;
			l_abscissa[5][6] = 0;
			l_abscissa[5][7] = 0;
			l_abscissa[5][8] = 0;
			l_abscissa[5][9] = 0;
			l_abscissa[6][0] = -0.94910791234275852453e0;
			l_abscissa[6][1] = -0.74153118559939443986e0;
			l_abscissa[6][2] = -0.40584515137739716691e0;
			l_abscissa[6][3] = 0;
			l_abscissa[6][4] = 0.40584515137739716691e0;
			l_abscissa[6][5] = 0.74153118559939443986e0;
			l_abscissa[6][6] = 0.94910791234275852453e0;
			l_abscissa[6][7] = 0;
			l_abscissa[6][8] = 0;
			l_abscissa[6][9] = 0;
			l_abscissa[7][0] = -0.96028985649753623168e0;
			l_abscissa[7][1] = -0.79666647741362673959e0;
			l_abscissa[7][2] = -0.52553240991632898582e0;
			l_abscissa[7][3] = -0.18343464249564980494e0;
			l_abscissa[7][4] = 0.18343464249564980494e0;
			l_abscissa[7][5] = 0.52553240991632898582e0;
			l_abscissa[7][6] = 0.79666647741362673959e0;
			l_abscissa[7][7] = 0.96028985649753623168e0;
			l_abscissa[7][8] = 0;
			l_abscissa[7][9] = 0;
			l_abscissa[8][0] = -0.96816023950762608984e0;
			l_abscissa[8][1] = -0.83603110732663579430e0;
			l_abscissa[8][2] = -0.61337143270059039731e0;
			l_abscissa[8][3] = -0.32425342340380892904e0;
			l_abscissa[8][4] = 0;
			l_abscissa[8][5] = 0.32425342340380892904e0;
			l_abscissa[8][6] = 0.61337143270059039731e0;
			l_abscissa[8][7] = 0.83603110732663579430e0;
			l_abscissa[8][8] = 0.96816023950762608984e0;
			l_abscissa[8][9] = 0;
			l_abscissa[9][0] = -0.97390652851717172008e0;
			l_abscissa[9][1] = -0.86506336668898451073e0;
			l_abscissa[9][2] = -0.67940956829902440623e0;
			l_abscissa[9][3] = -0.43339539412924719080e0;
			l_abscissa[9][4] = -0.14887433898163121088e0;
			l_abscissa[9][5] = 0.14887433898163121088e0;
			l_abscissa[9][6] = 0.43339539412924719080e0;
			l_abscissa[9][7] = 0.67940956829902440623e0;
			l_abscissa[9][8] = 0.86506336668898451073e0;
			l_abscissa[9][9] = 0.97390652851717172008e0;

			abscissa = &(l_abscissa[0][0]);
			setup = true;
		}

		return &(abscissa[S*CGAUSSQUADRATURE1D_NUMBER_OF_VALUES]);
	}

	static inline const T* getWeights()
	{
		static T *weight;
		static bool setup = false;
		static int S = (t_degreeOfPolynomial/2);

		if (!setup)
		{
			static T l_weight[CGAUSSQUADRATURE1D_NUMBER_OF_VALUES][CGAUSSQUADRATURE1D_NUMBER_OF_VALUES];

			l_weight[0][0] = 2;
			l_weight[0][1] = 0;
			l_weight[0][2] = 0;
			l_weight[0][3] = 0;
			l_weight[0][4] = 0;
			l_weight[0][5] = 0;
			l_weight[0][6] = 0;
			l_weight[0][7] = 0;
			l_weight[0][8] = 0;
			l_weight[0][9] = 0;
			l_weight[1][0] = 0.10000000000000000000e1;
			l_weight[1][1] = 0.10000000000000000000e1;
			l_weight[1][2] = 0;
			l_weight[1][3] = 0;
			l_weight[1][4] = 0;
			l_weight[1][5] = 0;
			l_weight[1][6] = 0;
			l_weight[1][7] = 0;
			l_weight[1][8] = 0;
			l_weight[1][9] = 0;
			l_weight[2][0] = 0.55555555555555555554e0;
			l_weight[2][1] = 0.88888888888888888888e0;
			l_weight[2][2] = 0.55555555555555555554e0;
			l_weight[2][3] = 0;
			l_weight[2][4] = 0;
			l_weight[2][5] = 0;
			l_weight[2][6] = 0;
			l_weight[2][7] = 0;
			l_weight[2][8] = 0;
			l_weight[2][9] = 0;
			l_weight[3][0] = 0.34785484513745385736e0;
			l_weight[3][1] = 0.65214515486254614268e0;
			l_weight[3][2] = 0.65214515486254614268e0;
			l_weight[3][3] = 0.34785484513745385736e0;
			l_weight[3][4] = 0;
			l_weight[3][5] = 0;
			l_weight[3][6] = 0;
			l_weight[3][7] = 0;
			l_weight[3][8] = 0;
			l_weight[3][9] = 0;
			l_weight[4][0] = 0.23692688505618908748e0;
			l_weight[4][1] = 0.47862867049936646810e0;
			l_weight[4][2] = 0.56888888888888888888e0;
			l_weight[4][3] = 0.47862867049936646810e0;
			l_weight[4][4] = 0.23692688505618908748e0;
			l_weight[4][5] = 0;
			l_weight[4][6] = 0;
			l_weight[4][7] = 0;
			l_weight[4][8] = 0;
			l_weight[4][9] = 0;
			l_weight[5][0] = 0.17132449237917034505e0;
			l_weight[5][1] = 0.36076157304813860752e0;
			l_weight[5][2] = 0.46791393457269104736e0;
			l_weight[5][3] = 0.46791393457269104736e0;
			l_weight[5][4] = 0.36076157304813860752e0;
			l_weight[5][5] = 0.17132449237917034505e0;
			l_weight[5][6] = 0;
			l_weight[5][7] = 0;
			l_weight[5][8] = 0;
			l_weight[5][9] = 0;
			l_weight[6][0] = 0.12948496616886969320e0;
			l_weight[6][1] = 0.27970539148927666792e0;
			l_weight[6][2] = 0.38183005050511894488e0;
			l_weight[6][3] = 0.41795918367346938776e0;
			l_weight[6][4] = 0.38183005050511894488e0;
			l_weight[6][5] = 0.27970539148927666792e0;
			l_weight[6][6] = 0.12948496616886969320e0;
			l_weight[6][7] = 0;
			l_weight[6][8] = 0;
			l_weight[6][9] = 0;
			l_weight[7][0] = 0.10122853629037625915e0;
			l_weight[7][1] = 0.22238103445337447028e0;
			l_weight[7][2] = 0.31370664587788728726e0;
			l_weight[7][3] = 0.36268378337836198294e0;
			l_weight[7][4] = 0.36268378337836198294e0;
			l_weight[7][5] = 0.31370664587788728726e0;
			l_weight[7][6] = 0.22238103445337447028e0;
			l_weight[7][7] = 0.10122853629037625915e0;
			l_weight[7][8] = 0;
			l_weight[7][9] = 0;
			l_weight[8][0] = 0.81274388361574411802e-1;
			l_weight[8][1] = 0.18064816069485740418e0;
			l_weight[8][2] = 0.26061069640293546190e0;
			l_weight[8][3] = 0.31234707704000284008e0;
			l_weight[8][4] = 0.33023935500125976316e0;
			l_weight[8][5] = 0.31234707704000284008e0;
			l_weight[8][6] = 0.26061069640293546190e0;
			l_weight[8][7] = 0.18064816069485740418e0;
			l_weight[8][8] = 0.81274388361574411802e-1;
			l_weight[8][9] = 0;
			l_weight[9][0] = 0.66671344308688137460e-1;
			l_weight[9][1] = 0.14945134915058059497e0;
			l_weight[9][2] = 0.21908636251598204542e0;
			l_weight[9][3] = 0.26926671930999635524e0;
			l_weight[9][4] = 0.29552422471475287018e0;
			l_weight[9][5] = 0.29552422471475287018e0;
			l_weight[9][6] = 0.26926671930999635524e0;
			l_weight[9][7] = 0.21908636251598204542e0;
			l_weight[9][8] = 0.14945134915058059497e0;
			l_weight[9][9] = 0.66671344308688137460e-1;

			weight = &(l_weight[0][0]);
			setup = true;
		}

		return &(weight[S*CGAUSSQUADRATURE1D_NUMBER_OF_VALUES]);
	}


#undef CGAUSSQUADRATURE1D_NUMBER_OF_VALUES
};



}
}


#endif
