/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CTriangle_Normals.cpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "CTriangle_Normals.hpp"

namespace sierpi
{

#define H	((CTriangle_Factory::TTriangleFactoryScalarType)(1.0/std::sqrt(2.0)))
CTriangle_Factory::TTriangleFactoryScalarType CTriangle_Normals::normal_table[8][2] = {
		{0, 1},
		{H, H},
		{1, 0},
		{H, -H},
		{0, -1},
		{-H, -H},
		{-1, 0},
		{-H, H}
};

}
