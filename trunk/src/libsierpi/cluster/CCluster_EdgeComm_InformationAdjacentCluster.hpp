/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CEdgeComm_InformationAdjacentCluster.hpp
 *
 *  Created on: Jul 19, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_EDGECOMM_INFORMATION_ADJACENTCLUSTER_HPP_
#define CCLUSTER_EDGECOMM_INFORMATION_ADJACENTCLUSTER_HPP_

#include "CCluster_TreeNode.hpp"
#include "CCluster_UniqueId.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"



namespace sierpi
{


/**
 * information about a single adjacent cluster
 */
template <typename t_CCluster_TreeNode>
class CCluster_EdgeComm_InformationAdjacentCluster
{
public:
	/**
	 * the cluster tree node
	 */
	t_CCluster_TreeNode *cCluster_TreeNode;

	/**
	 * the cCluster_UniqueId of the adjacent cluster (should be equal to cCluster_TreeNode->cCluster_UniqueId)
	 */
	CCluster_UniqueId cCluster_UniqueId;

	/**
	 * the number of elements on the communication stack which have to be exchanged
	 */
	int edge_comm_elements;

#if CONFIG_ENABLE_MPI
	/*
	 * mpi rank of adjacent cluster
	 */
	int mpi_rank;

	/*
	 * mpi request handler to be used for receive operations
	 */
	MPI_Request mpi_requests[2];

	void *mpi_last_send_data_ptr;
	void *mpi_last_recv_data_ptr;
#endif

	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster()	:
			cCluster_TreeNode(nullptr),
			edge_comm_elements(-1)
#if CONFIG_ENABLE_MPI
			,
			mpi_rank(-1),
			mpi_last_send_data_ptr(nullptr),
			mpi_last_recv_data_ptr(nullptr)
#endif
	{
#if CONFIG_ENABLE_MPI && DEBUG
		mpi_requests[0] = MPI_REQUEST_NULL;
		mpi_requests[1] = MPI_REQUEST_NULL;
#endif
	}


	/**
	 * constructor to create edge communication information element
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster(
			t_CCluster_TreeNode *i_cCluster_TreeNode,	///< cluster tree node
			const CCluster_UniqueId &i_uniqueId,			///< unique id
			size_t i_commElements							///< number of communication elements
#if CONFIG_ENABLE_MPI
			,
			int i_mpi_rank						///< mpi rank
#endif
	) {
		cCluster_TreeNode = i_cCluster_TreeNode;
		cCluster_UniqueId = i_uniqueId;
		edge_comm_elements = i_commElements;
#if CONFIG_ENABLE_MPI
		mpi_rank = i_mpi_rank;
		mpi_requests[0] = MPI_REQUEST_NULL;
		mpi_requests[1] = MPI_REQUEST_NULL;
#endif
	}


	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentCluster(
			t_CCluster_TreeNode *i_cCluster_TreeNode	///< cluster tree node
	)
	{
		cCluster_TreeNode = i_cCluster_TreeNode;
		cCluster_UniqueId = i_cCluster_TreeNode->cCluster_UniqueId;
		edge_comm_elements = -1;
#if CONFIG_ENABLE_MPI
		mpi_rank = i_cCluster_TreeNode->mpi_rank;
		mpi_requests[0] = MPI_REQUEST_NULL;
		mpi_requests[1] = MPI_REQUEST_NULL;
#endif
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, const CCluster_EdgeComm_InformationAdjacentCluster &m)
	{
		co << "(";
		co << "Elems: " << m.edge_comm_elements << ", ";
		co << "Id: " << m.cCluster_UniqueId << ", ";

		if (m.cCluster_TreeNode != nullptr)
		{
			co << "adj id: " << m.cCluster_TreeNode->cCluster_UniqueId;
#if CONFIG_ENABLE_MPI
			co << ", mpi rank: " << m.mpi_rank;
//			assert(m.mpi_rank < 0);
#endif
		}
		else
		{
			co << "adj id not available (MPI)";
#if CONFIG_ENABLE_MPI
			co << ", mpi rank: " << m.mpi_rank;
			assert(m.mpi_rank >= 0);
#endif
		}

		co << ")";

		return co;
	}
};

}

#endif /* CEDGECOMM_INFORMATIONADJACENTPARTITION_HPP_ */
