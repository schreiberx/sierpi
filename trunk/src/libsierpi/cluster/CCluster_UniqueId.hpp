/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CCluster_UniqueId.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_UNIQUEID_HPP_
#define CCLUSTER_UNIQUEID_HPP_

#include "lib/intToBinString.hpp"
#include "../parallelization/CMigration.hpp"


namespace sierpi
{

/**
 * \brief unique id for each cluster
 *
 * cCluster_UniqueIds:
 * ==========
 *   - those are unique also for sub-triangles.
 *   - to get even unique IDs for the sub-triangles, they are bit-combined.
 *   - the maximum number of bits used for the unique root domain triangles IDs is given in by max_unique_id_depth
 *   - the root unique IDs are initialized with "(1 << max_unique_id_depth) + triangle_enum_id" in the class CDomain_BaseTriangulation
 *   - then during the split operation, the parent's cCluster_UniqueId is shifted to the left by a single bit and the lowest bit
 *     is set to 0 or 1 to account for first/second child node.
 */
class CCluster_UniqueId	:
		public CMigration_RawClass
{
public:
	/**
	 * unique id of cluster
	 */
	unsigned long long raw_unique_id;

	/**
	 * unique id depth of cluster to split up further clusters with an unique id
	 *
	 * if the cluster is split up into 2 distinct triangles, their new ids are
	 * created by using the old cCluster_UniqueId and using a prefix of 0 or 1 of the
	 * cCluster_UniqueIdDepth-th less significant parent cCluster_UniqueId
	 */
	unsigned int unique_id_depth;

	/**
	 * constructor
	 */
public:
	inline CCluster_UniqueId()	:
		raw_unique_id(1),			// set raw_unique_id to 1 to avoid assert failures
		unique_id_depth(0)
	{
	}

public:
	inline void setup(
			unsigned long long i_rawUniqueId,
			unsigned long i_uniqueId_depth
	)
	{
		assert(i_rawUniqueId > 0);
		raw_unique_id = i_rawUniqueId;
		unique_id_depth = i_uniqueId_depth;
	}

	/**
	 * setup root unique id
	 */
	inline void setupRoot()
	{
		raw_unique_id = 1;
		unique_id_depth = 0;
	}

	inline bool isBoundary()	const
	{
		return raw_unique_id == 0;
	}



	/**
	 * convert to left child
	 */
	inline void convertToFirstChild()
	{
		raw_unique_id = raw_unique_id << 1;
		unique_id_depth = unique_id_depth+1;
	}


	/**
	 * convert to left child
	 */
	inline void convertToSecondChild()
	{
		raw_unique_id = (raw_unique_id << 1) | 1;
		unique_id_depth = unique_id_depth+1;
	}


	/**
	 * setup cCluster_UniqueId by rawUniqueId
	 */
public:
	inline CCluster_UniqueId(unsigned long long i_rawUniqueId)
	{
		assert(i_rawUniqueId > 0);
		raw_unique_id = i_rawUniqueId;

		assert(raw_unique_id != 0);

		unique_id_depth = 0;
		for (int i = raw_unique_id; i > 0; i >>= 1)
			unique_id_depth++;
	}

	/**
	 * setup cCluster_UniqueId by other cCluster_UniqueId
	 */
public:
	inline CCluster_UniqueId(
			const CCluster_UniqueId &cCluster_UniqueId
	)
	{
		raw_unique_id = cCluster_UniqueId.raw_unique_id;
		unique_id_depth = cCluster_UniqueId.unique_id_depth;
		// remove assertion since raw_unique_id=0 represents a boundary marker for node based parallelization
//		assert(raw_unique_id > 0);
	}

	/**
	 * return the parents raw unique id
	 */
public:
	unsigned int getParentRawUniqueId()	const
	{
		assert(raw_unique_id > 1);
		return raw_unique_id >> 1;
	}

	/**
	 * return true, if the cCluster_UniqueId was initialized
	 */
	bool isValid()	const
	{
		assert(raw_unique_id > 1);
		return raw_unique_id != 0;
	}


	/**
	 * setup cCluster_UniqueId for this parent assuming that i_parent_cCluster_UniqueId
	 * is a child
	 */
public:
	void setupParentFromChild(
			CCluster_UniqueId &i_child_cCluster_UniqueId
	)
	{
		raw_unique_id = (i_child_cCluster_UniqueId.raw_unique_id >> 1);
		unique_id_depth = i_child_cCluster_UniqueId.unique_id_depth-1;

		assert(raw_unique_id > 1);
	}



	/**
	 * setup cCluster_UniqueId for this child assuming that this is the first child
	 * and the cCluster_UniqueId given as a parameter is the parent's cCluster_UniqueId
	 */
public:
	void setupFirstChildFromParent(
			CCluster_UniqueId &i_parent_cCluster_UniqueId
	)
	{
		raw_unique_id = i_parent_cCluster_UniqueId.raw_unique_id << 1;
		unique_id_depth = i_parent_cCluster_UniqueId.unique_id_depth+1;

		assert(raw_unique_id > 1);
	}



	/**
	 * setup cCluster_UniqueId for this child assuming that this is the first child
	 * and the cCluster_UniqueId given as a parameter is the parent's cCluster_UniqueId
	 */
public:
	void setupSecondChildFromParent(
			CCluster_UniqueId &i_parent_cCluster_UniqueId
	)
	{
		raw_unique_id = (i_parent_cCluster_UniqueId.raw_unique_id << 1) | 1;
		unique_id_depth = i_parent_cCluster_UniqueId.unique_id_depth+1;

		assert(raw_unique_id > 1);
	}



	/**
	 * return true if this is the first child
	 */
public:
	bool isFirstChild()	const
	{
		assert(raw_unique_id != 0);

		return (raw_unique_id & 1) == 0;
	}


	/**
	 * return true if this is the second child
	 */
public:
	bool isSecondChild()	const
	{
		assert(raw_unique_id != 0);

		return (raw_unique_id & 1) == 1;
	}

	bool followFirstChildForDepth(int i_depth)	const
	{
		assert(raw_unique_id != 0);

		return !(bool)((raw_unique_id >> (unique_id_depth-i_depth-1))&1);
	}


	friend
	inline
	bool operator==(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif

#if DEBUG
		if (o1.raw_unique_id == o2.raw_unique_id)
			assert(o1.unique_id_depth == o2.unique_id_depth);
#endif
		return (o1.raw_unique_id == o2.raw_unique_id);
	}


	friend
	inline
	bool operator!=(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif

		return (o1.raw_unique_id != o2.raw_unique_id);
	}


	friend inline bool operator<(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
/*
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif
*/
		if (o1.raw_unique_id == 0)
			return true;
		if (o2.raw_unique_id == 0)
			return true;

		unsigned int max_depth = std::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 < shifted_o2;
	}

	friend inline bool operator<=(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif

		unsigned int max_depth = std::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 <= shifted_o2;
	}


	friend inline bool operator>(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
/*
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif
*/
		if (o1.raw_unique_id == 0)
			return true;
		if (o2.raw_unique_id == 0)
			return true;

		unsigned int max_depth = std::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 > shifted_o2;
	}


	friend inline bool operator>=(const CCluster_UniqueId &o1, const CCluster_UniqueId &o2)
	{
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		assert(o1.raw_unique_id != 0);
		assert(o2.raw_unique_id != 0);
#endif

		unsigned int max_depth = std::max(o1.unique_id_depth, o2.unique_id_depth);

		unsigned long long shifted_o1 = o1.raw_unique_id << (max_depth-o1.unique_id_depth);
		unsigned long long shifted_o2 = o2.raw_unique_id << (max_depth-o2.unique_id_depth);

		return shifted_o1 >= shifted_o2;
	}

	friend
	inline
	::std::ostream&
	operator<<(
		::std::ostream &co,
		const CCluster_UniqueId &p
	)
	{
		co << "(" << intToBinString(p.raw_unique_id) << " | " << p.raw_unique_id << " | " << p.unique_id_depth << ")";

		if (p.raw_unique_id == 0)
			co << "[BORDER MARKER]";

		return co;
	}
};

}

#endif /* CCLUSTER_UNIQUEID_H_ */
