/*
 * CGlobalComm.hpp
 *
 *  Created on: Jul 5, 2012
 *      Author: schreibm
 */

#ifndef CGLOBALCOMM_HPP_
#define CGLOBALCOMM_HPP_

#if CONFIG_ENABLE_MPI
#	include <mpi.h>
#	include <stdio.h>
#endif

#include <assert.h>
#include <iostream>

namespace sierpi
{

/**
 * class to abtract global communication and synchronization.
 *
 * usually MPI is used for communication
 */
class CGlobalComm
{
	/**
	 * \brief Run a global OR reduce operation on boolean flags
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline bool reduceBooleanOr(
			bool i_flag		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_flag;

#else
		// request break statement
		char r_input = i_flag;
		char r_output;
		MPI_Allreduce(&r_input, &r_output, 1, MPI_BYTE, MPI_BOR, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * \brief Run a global reduce operation on boolean flags
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline bool reduceBooleanAnd(
			bool i_flag		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_flag;

#else
		// request break statement
		char r_input = i_flag;
		char r_output;
		MPI_Allreduce(&r_input, &r_output, 1, MPI_BYTE, MPI_BAND, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * \brief Run a global reduce operation on float values
	 */
public:
	static inline float reduceMin(
			float i_value		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_value;

#else
		// request break statement
		float r_output;
		MPI_Allreduce(&i_value, &r_output, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * \brief Run a global reduce operation on float values
	 */
public:
	static inline double reduceMin(
			double i_value		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_value;

#else
		// request break statement
		double r_output;
		MPI_Allreduce(&i_value, &r_output, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
		return r_output;
#endif
	}




	/**
	 * \brief Run a global reduce operation on float values
	 */
public:
	static inline float reduceMax(
			float i_value		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_value;

#else
		// request break statement
		float r_output;
		MPI_Allreduce(&i_value, &r_output, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * \brief Run a global reduce operation on float values
	 */
public:
	static inline double reduceMax(
			double i_value		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_value;

#else
		// request break statement
		double r_output;
		MPI_Allreduce(&i_value, &r_output, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
		return r_output;
#endif
	}


	/**
	 * \brief Run a global reduce operation on float values
	 */
public:
	static inline int reduceMax(
			int i_value		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_value;

#else
		// request break statement
		int r_output;
		MPI_Allreduce(&i_value, &r_output, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * \brief Run a global reduce operation
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline long long reduceLongLongSum(
			long long i_input		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_input;

#else

		// request break statement
		long long r_output;
		MPI_Allreduce(&i_input, &r_output, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
		return r_output;
#endif
	}


	/**
	 * \brief Run a global reduce operation
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline int reduceIntSum(
			int i_input		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_input;

#else

		// request break statement
		int r_output;
		MPI_Allreduce(&i_input, &r_output, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		return r_output;
#endif
	}


	/**
	 * \brief Run a global reduce operation
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline double reduceDoubleSum(
			double i_input		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_input;

#else

		// request break statement
		double r_output;
		MPI_Allreduce(&i_input, &r_output, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		return r_output;
#endif
	}

	/**
	 * \brief Run a global reduce operation
	 *
	 * This is useful to e. g. continue a loop when there
	 * are still hanging nodes on other nodes
	 */
public:
	static inline float reduceFloatSum(
			float i_input		///< boolean flag
	)
	{
#if !CONFIG_ENABLE_MPI

		return i_input;

#else

		// request break statement
		float r_output;
		MPI_Allreduce(&i_input, &r_output, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
		return r_output;
#endif
	}



	/**
	 * execute a global barrier
	 */
public:
	static inline void barrier()
	{
#if CONFIG_ENABLE_MPI
		MPI_Barrier(MPI_COMM_WORLD);
#endif
	}



	/**
	 * return communicator size (#nodes in MPI comm world)
	 */
	static inline int getCommSize()
	{
#if CONFIG_ENABLE_MPI
		int ntasks;
		MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
		assert(ntasks > 0);
		return ntasks;
#else
		return 1;
#endif
	}


	/**
	 * return communicator rank
	 */
	static inline int getCommRank()
	{
#if CONFIG_ENABLE_MPI
		int rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		return rank;
#else
		return 0;
#endif
	}
};

}

#endif /* CGLOBALCOMM_HPP_ */
