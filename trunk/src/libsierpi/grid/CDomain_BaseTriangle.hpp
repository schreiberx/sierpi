/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CDomain_BaseTriangle.hpp
 *
 *  Created on: Mar 31, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDOMAIN_BASE_TRIANGLE_WITH_PARTITION_HPP_
#define CDOMAIN_BASE_TRIANGLE_WITH_PARTITION_HPP_

#include <vector>
#include "libsierpi/triangle/CTriangle_Factory.hpp"
#include "libsierpi/cluster/CCluster_TreeNode.hpp"
#include "../cluster/CCluster_UniqueId.hpp"

namespace sierpi
{


/**
 * Storage for a root triangle used for the initial domain triangulation.
 *
 * this data structure is of no use after the domain root triangle tree is set up.
 */
template <typename CSimulation_ClusterHandler>
class CDomain_BaseTriangle
{
	typedef CDomain_BaseTriangle<CSimulation_ClusterHandler> CDomain_Triangle_;
	typedef CCluster_TreeNode<CSimulation_ClusterHandler> CCluster_;

public:
	/**
	 * sierpi triangle properties of the current triangle
	 */
	CTriangle_Factory cTriangleFactory;

	/**
	 * unique id which is used to
	 * a) setup unique id for cluster
	 * b) to know the adjacent id for the mpi communication
	 */
	CCluster_UniqueId cCluster_UniqueId;


#if CONFIG_ENABLE_MPI
	/**
	 * MPI rank associated with this base triangle
	 */
	int mpi_rank;
#endif

	/**
	 * "anchors" to the adjacent triangles to setup e. g. the initial edge comm
	 */
	class CAdjacent_TrianglesWithCluster
	{
public:
		CDomain_Triangle_ *hyp_edge;
		CDomain_Triangle_ *left_edge;
		CDomain_Triangle_ *right_edge;

		CAdjacent_TrianglesWithCluster()
		{
			hyp_edge = nullptr;
			left_edge = nullptr;
			right_edge = nullptr;
		}

		CAdjacent_TrianglesWithCluster(const CAdjacent_TrianglesWithCluster &i)	:
			hyp_edge(i.hyp_edge),
			left_edge(i.left_edge),
			right_edge(i.right_edge)
		{

		}
	};


	/**
	 * pointers to adjacent triangles
	 */
	CAdjacent_TrianglesWithCluster adjacent_triangles;


	/**
	 * pointer to allocated root cluster node
	 *
	 * this is used for convenience to speedup the initialization routines when
	 * converting the list to a binary tree and setting up the cClusters
	 */
	CCluster_ *cCluster_user_ptr;


	/**
	 * pointer to the hierarchical neighbours
	 *
	 * this is used for efficiently setting edge neighbours of subtriangles
	 */
	CDomain_Triangle_ *parent;
	CDomain_Triangle_ *leftChild;
	CDomain_Triangle_ *rightChild;


	/**
	 * setup connection with right edge
	 */
	void setupRightEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.right_edge = i_baseTriangle;
		cTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	/**
	 * setup connection with left edge
	 */
	void setupLeftEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.left_edge = i_baseTriangle;
		cTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	/**
	 * setup connection with hyp edge
	 */
	void setupHypEdgeConnection(CDomain_BaseTriangle *i_baseTriangle)
	{
		adjacent_triangles.hyp_edge = i_baseTriangle;
		cTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;
	}

	CDomain_BaseTriangle(
			CTriangle_Factory &i_newTriangleFactory		///< factory for new triangle
	)	:
		cTriangleFactory(i_newTriangleFactory),
		cCluster_user_ptr(nullptr)
	{
	}


	/**
	 * copy constructor
	 */
	CDomain_BaseTriangle(
			const CDomain_BaseTriangle<CSimulation_ClusterHandler> &i		///< factory for new triangle
	)	:
		cTriangleFactory(i.cTriangleFactory),
#if CONFIG_ENABLE_MPI
		mpi_rank(-1),
#endif
		adjacent_triangles(i.adjacent_triangles),
		cCluster_user_ptr(i.cCluster_user_ptr)
	{
	}



	/**
	 * Setup the triangle factory to a given triangle factory.
	 *
	 * This method is useful when the triangleWithCluster was initialized previously
	 * without a given triangleFactory.
	 */
	void setTriangleFactory(CTriangle_Factory &p_triangleFactory)
	{
		cTriangleFactory = p_triangleFactory;
	}
};

}

#endif /* CTRIANGLE_GLUE_H_ */
