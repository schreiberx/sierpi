/*
 * CGlNormalizeByAlphaChannel.hpp
 *
 *  Created on: Sep 7, 2011
 *      Author: schreibm
 */

#ifndef CGLNORMALIZEBYALPHACHANNEL_HPP_
#define CGLNORMALIZEBYALPHACHANNEL_HPP_

#include "libgl/incgl3.h"
#include "simulations/hyperbolic_common/CTsunamiTypes.hpp"
#include "libmath/CGlSlMath.hpp"
#include "libgl/core/CGlFbo.hpp"
#include "libgl/core/CGlProgram.hpp"
#include "libgl/core/CGlViewport.hpp"
#include "libgl/draw/CGlDrawTexturedQuad.hpp"
#include "lib/CError.hpp"

#include "libgl/core/CGlFbo.hpp"

class CGlNormalizeByAlphaChannel
{
	/**
	 * helper stuff
	 */
	CGlDrawTexturedQuad cGlDrawTexturedQuad;

	/*
	 * variables related to rendering splats
	 */
	/// FBO to render the splats to a texture
	CGlFbo cGlFbo;

	/// viewport for FBO
	CGlViewport cGlViewport;

	/// program for rendering splats
	CGlProgram cGlProgram;

	/// pvm matrix
	GLSL::mat4 pvm_matrix;
	/// uniform to access pvm matrix for splats rendering vertex shader
	CGlUniform uniform_pvm_matrix;

public:
	CError error;

	/**
	 * texture storing the rendered splats
	 */
	CGlTexture cGlTexture;



	CGlNormalizeByAlphaChannel()
	{
		setupTexture(256, 256);
		pvm_matrix = GLSL::ortho(-1.f, 1.f, -1.f, 1.f, -1.f, 1.f);

		cGlProgram.initVertFragShadersFromDirectory("normalize_by_alpha_channel");
		CError_AppendReturn(cGlProgram);

		cGlProgram.link();
		if (cGlProgram.error())
		{
			error << "info Log: linking: " << cGlProgram.error.getString() << std::endl;
			return;
		}

		cGlProgram.setupUniform(uniform_pvm_matrix, "pvm_matrix");

		cGlFbo.bind();
		cGlFbo.bindTexture(cGlTexture);
		cGlFbo.unbind();

		CGlErrorCheck();
	}



	/**
	 * setup the texture size
	 */
	void setupTexture(
			GLint p_width,
			GLint p_height
	)
	{
		cGlTexture.bind();
		cGlTexture.setTextureParameters(GL_TEXTURE_2D, GL_R16F);
		cGlTexture.resize(p_width, p_height);
		cGlTexture.unbind();

		CGlErrorCheck();
	}


	inline void normalizeByAlphaChannel(
			CGlTexture &p_cGlTexture		///< texture to be normalized
	)
	{
		cGlFbo.bind();

			cGlViewport.saveState();
			cGlViewport.setSize(cGlTexture.width, cGlTexture.height);

				cGlProgram.use();
				uniform_pvm_matrix.set(pvm_matrix);

					glDisable(GL_CULL_FACE);
					glDisable(GL_DEPTH_TEST);

						p_cGlTexture.bind();

						GLfloat vertices[4][4] = {
									{-1.0, -1.0, 0.0, 1.0},
									{ 1.0, -1.0, 0.0, 1.0},
									{-1.0,  1.0, 0.0, 1.0},
									{ 1.0,  1.0, 0.0, 1.0},
								};

						GLfloat texcoords[4][2] = {
									{0.0, 0.0},
									{1.0, 0.0},
									{0.0, 1.0},
									{1.0, 1.0},
								};

						// index, size, type, normalized, stride, pointer
						glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, vertices);
						glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, texcoords);

						glEnableVertexAttribArray(0);
						glEnableVertexAttribArray(1);
						CGlErrorCheck();

						glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

						glDisableVertexAttribArray(1);
						glDisableVertexAttribArray(0);

					glEnable(GL_DEPTH_TEST);
					glEnable(GL_CULL_FACE);

				cGlProgram.disable();
			cGlViewport.restoreState();

		cGlFbo.unbind();
	}

};

#endif /* CGLNORMALIZEBYALPHACHANNEL_HPP_ */
