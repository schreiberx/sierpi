/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_HPP_


#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>

#include "global_config.h"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "libsierpi/grid/CDomain_BaseTriangulationSetup.hpp"
#include "libsierpi/grid/CBaseTriangulation_To_GenericTree.hpp"
#include "libsierpi/parallelization/CMigration.hpp"

#include "lib/CStopwatch.hpp"

#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../hyperbolic_common/CParameters.hpp"
#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/modifiers/CSetup_CellData.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/backends/CGetNodeDataSample.hpp"
#include "../hyperbolic_common/subsimulation_generic/COutputSimulationSpecificData.hpp"


#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelAdaptivityTraversals.hpp"
#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==2
	#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals_RK2.hpp"
#else
	#include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelFluxCommTraversals.hpp"
#endif


#include "libmath/CVector.hpp"


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	#include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif

#include "CSimulation_MainInterface.hpp"

#include "CSimulationHyperbolic_Parallel_FileOutput.hpp"
#include "CSimulationHyperbolic_Cluster.hpp"

#include "CSplitJoin_TuningTable.hpp"


#if CONFIG_SIERPI_ENABLE_GUI
	#include "CSimulationHyperbolic_Parallel_Gui.hpp"
#endif


#if CONFIG_ENABLE_THREADING
	#include "mainthreading/CMainThreading.hpp"
#endif

#include "CSimulationHyperbolic_WorkloadBalancing.hpp"


#if CONFIG_BENCHMARK_SPHERE
#include "../hyperbolic_common/subsimulation_benchmark_sphere/tsunami_benchmarks/CBenchmarkOutput.hpp"
#include "../hyperbolic_common/subsimulation_benchmark_sphere/tsunami_benchmarks/CGlobal.hpp"
#endif



/**
 * \brief Main class for parallel hyperbolic Simulation
 *
 * This class is the central point of a parallelized hyperbolic
 * simulation models based on edge-oriented models.
 *
 * Sets up the simulation.
 * It manages all sub-clusters, creates the initial domain triangulation and
 */
class CSimulationHyperbolic_Parallel	:
#if CONFIG_SIERPI_ENABLE_GUI
	public CSimulationHyperbolic_Parallel_Gui			///< GUI?
#else
	public CSimulation_MainInterface					///< simulation control interface
#endif
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	typedef CHyperbolicTypes::CSimulationTypes::T	T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;


public:
	/**
	 * output of simulation specific data
	 */
	COutputSimulationSpecificData cOutputSimulationSpecificData;


	/**
	 * simulation parameter
	 */
	class CParametersExtended	:
			public CParameters
	{
		CSimulationHyperbolic_Parallel &parent;

	public:
		CParametersExtended(
				CSimulationHyperbolic_Parallel &i_parent
		)	:
			parent(i_parent)
		{

		}

	public:
		void updateClusterParameters()
		{
			parent.updateClusterParameters();
		}
	};

	CParametersExtended cParameters;


	/**
	 * datasets to get bathymetry, water surface parameters, benchmark values, etc.
	 */
	CDatasets cDatasets;

	/**
	 * threading handler
	 */
#if CONFIG_ENABLE_THREADING
	CMainThreading *cMainThreading;
#endif



	/**
	 * workload balancing
	 */
	CSimulationHyperbolic_WorkloadBalancing cWorkloadBalancing;

	/**
	 * access to domain clusters
	 */
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> cDomainClusters;


	/**
	 * file output backend
	 */
	CSimulationHyperbolic_Parallel_FileOutput cFileOutput;

#if CONFIG_BENCHMARK_SPHERE
	/**
	 * benchmark output backend
	 */
	CBenchmarkOutput cBenchmarkOutput;
#endif


	/**
	 * stopwatch to measure time periods
	 */
	CStopwatch cStopwatch;


	/**
	 * constructor for parallel hyperbolic simulations
	 */
	CSimulationHyperbolic_Parallel(
			int i_verbosity_level
	)	:
#if CONFIG_SIERPI_ENABLE_GUI
		CSimulationHyperbolic_Parallel_Gui(cParameters, cDomainClusters),
#endif
		cOutputSimulationSpecificData(cParameters, cDatasets, cDomainClusters),
		cParameters(*this),
		cDatasets(cParameters, i_verbosity_level),

#if CONFIG_ENABLE_THREADING
		cMainThreading(nullptr),
#endif

		cWorkloadBalancing(cParameters, cDomainClusters
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
			, cDatasets
#endif
		),
		cFileOutput(cParameters, cDomainClusters, cDatasets)
#if CONFIG_BENCHMARK_SPHERE
		, cBenchmarkOutput(cParameters, cDomainClusters, cDatasets)
#endif
	{
		cParameters.verbosity_level = i_verbosity_level;

#if SIMULATION_MULTILAYER_ENABLED
		cParameters.flops_cell_update = CDG_MatrixComputations_3D::getNumberOfFlops();
#else
		cParameters.flops_cell_update = CDG_MatrixComputations_2D::getNumberOfFlops();
#endif

		cParameters.flops_cell_update *= SIMULATION_NUMBER_OF_LAYERS;
	}


	/**
	 * virtual Deconstructor
	 */
	virtual ~CSimulationHyperbolic_Parallel()
	{
	}



#if CONFIG_ENABLE_THREADING
	/**
	 * setup main threading handler for iTBB & MPI
	 */
	void setupMainThreading(CMainThreading *i_cMainThreading)
	{
		cMainThreading = i_cMainThreading;
	}
#endif


	/**
	 * Reset the simulation
	 */
	void reset()
	{
		// setup matrices for hyperbolic simulations
		sierpi::travs::CSpecialized_EdgeComm_Normals_Depth::setupMatrices(cParameters.verbosity_level);

		// reset parameters
		cParameters.reset();

		// validate configuration
		cParameters.config_validateAndFixParameters();

		/*
		 * setup scenarios
		 */
		cDatasets.loadDatasets();

		/*
		 * LOD displacement value to overcome
		 *
		 * issues with oversampling
		 */
		T lod_displacement_value = ((T)cParameters.grid_initial_cluster_splits)*(T)0.5;

		// add 1 to oversample slightly!
		// otherwise some terrain pops up suddently
		lod_displacement_value = (T)0;

		cDatasets.setLevelOfDetailDisplacementValue(lod_displacement_value);

		/*
		 * reset the world: setup triangulation of "scene"
		 */
		p_setup_DomainClusters(cParameters.simulation_world_scene_id);

		/*
		 * update scan data if scan data is enabled
		 */
#if CONFIG_ENABLE_SCAN_DATA
		cWorkloadBalancing.create_initial_scan_data((1 << cParameters.grid_initial_recursion_depth_with_initial_cluster_splits));
#endif

#if DEBUG
		cDomainClusters.validate_correct_alignment_of_ids_in_generic_tree();
#endif

		cParameters.simulation_parameter_global_timestep_size = cParameters.simulation_parameter_minimum_timestep_size;

		/***************************************************************************************
		 * STACKS: setup the stacks (only the memory allocation) of the clusters
		 *
		 * We keep this setup variable and not defined with precompiler
		 ***************************************************************************************/
		unsigned int stackInitializationFlags =
				sierpi::CSimulationStacks_Enums::ELEMENT_STACKS							|
				sierpi::CSimulationStacks_Enums::ADAPTIVE_STACKS						|
				sierpi::CSimulationStacks_Enums::EDGE_COMM_STACKS						|
				sierpi::CSimulationStacks_Enums::EDGE_COMM_PARALLEL_EXCHANGE_STACKS;

#if CONFIG_SIERPI_ENABLE_GUI
		stackInitializationFlags |=
				sierpi::CSimulationStacks_Enums::VERTEX_COMM_STACKS						|
				sierpi::CSimulationStacks_Enums::VERTEX_COMM_PARALLEL_EXCHANGE_STACKS;
#endif


		/***************************************************************************************
		 * SETUP STRUCTURE STACK and ELEMENT DATA
		 ***************************************************************************************/

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): resetStacks / setupStacks" << std::endl;

		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_node)
			{
				// setup structure stack and element data to default values
				i_node->cCluster_TreeNode->resetStacks(stackInitializationFlags, cParameters.grid_initial_recursion_depth_with_initial_cluster_splits);

				sierpi::travs::CSetup_Structure_CellData<CHyperbolicTypes> cSetup_Structure_CellData;
				cSetup_Structure_CellData.setup(i_node->cCluster_TreeNode->cStacks, cParameters.grid_initial_recursion_depth_with_initial_cluster_splits, cParameters.simulation_parameter_cell_data_setup);
			}
		);


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		/***************************************************************************************
		 * SETUP vertex coordinates in element data for debugging purposes
		 ***************************************************************************************/
		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): setup validation" << std::endl;

		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cSetup_CellData_Validation.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
			}
		);
#endif

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): updateClusterParameters" << std::endl;

		updateClusterParameters();

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): reset_simulation_parameters" << std::endl;

		// call reset for CParameters
		cParameters.reset();


#if CONFIG_ENABLE_SCAN_DATA

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): setup_ScanDatasets" << std::endl;

		cWorkloadBalancing.updateClusterScanInformation();

#endif


		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): cWorkloadBalancing.cluster_split_and_join_and_migration" << std::endl;

		cWorkloadBalancing.cluster_split_and_join_and_migration();

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (reset_Simulation): DONE" << std::endl;


		if (cParameters.verbosity_level > 10)
			output_ClusterTreeInformation();

#if CONFIG_SIERPI_ENABLE_GUI
		static_cast<CSimulationHyperbolic_Parallel_Gui*>(this)->resetGui();
#endif
	}



private:
	/**
	 * Setup domain cluster:
	 *  - new world triangulation
	 *  - root cluster tree nodes
	 *  - simulation cluster handlers
	 */
	void p_setup_DomainClusters(
			int i_world_scene_id = 0		///< base triangulation to set-up
	)
	{
		sierpi::CDomain_BaseTriangulation<CSimulationHyperbolic_Cluster> cDomain_BaseTriangulation;

		sierpi::CDomain_BaseTriangulationSetup::setupBaseTriangulation(
			i_world_scene_id,
			&cDomain_BaseTriangulation,
			cParameters.verbosity_level,
			cParameters.grid_initial_cluster_splits,
			cParameters.parallelization_mpi_rank,
			cParameters.parallelization_mpi_size
		);

		cParameters.grid_initial_recursion_depth_with_initial_cluster_splits = cParameters.grid_initial_recursion_depth-cParameters.grid_initial_cluster_splits;

		cParameters.simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain = cDomain_BaseTriangulation.length_of_catheti_for_unit_domain;

		cDatasets.getOriginAndSize(
				&cParameters.simulation_dataset_default_domain_translate_x,
				&cParameters.simulation_dataset_default_domain_translate_y,
				&cParameters.simulation_dataset_default_domain_size_x,
				&cParameters.simulation_dataset_default_domain_size_y
		);

#if 0
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << "+ " << cParameters.simulation_dataset_default_domain_translate_x << ", " << cParameters.simulation_dataset_default_domain_translate_y << std::endl;
		std::cout << "+ " << cParameters.simulation_dataset_default_domain_size_x << ", " << cParameters.simulation_dataset_default_domain_size_y << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
		exit(-1);
#endif

		/*
		 * scale & translate base triangulation to match world-scene
		 */
		cDomain_BaseTriangulation.scaleAndTranslate(
				cParameters.simulation_dataset_default_domain_translate_x,
				cParameters.simulation_dataset_default_domain_translate_y,
				cParameters.simulation_dataset_default_domain_size_x,
				cParameters.simulation_dataset_default_domain_size_y
		);


		/*
		 * convert to generic tree
		 */
		sierpi::CBaseTriangulation_To_GenericTree<CSimulationHyperbolic_Cluster> cBaseTriangulation_To_GenericTree;

		cParameters.number_of_local_cells = cBaseTriangulation_To_GenericTree.setup_GenericTree_And_Clusters_From_BaseTriangulation(
			cDomain_BaseTriangulation,				///< root triangles
			cParameters.grid_initial_recursion_depth_with_initial_cluster_splits,			///< initial recursion depth
			cParameters.grid_initial_recursion_depth_with_initial_cluster_splits-cParameters.grid_min_relative_recursion_depth,
			cParameters.grid_initial_recursion_depth_with_initial_cluster_splits+cParameters.grid_max_relative_recursion_depth,
			cParameters.parallelization_mpi_rank,
			cParameters.grid_initial_cluster_splits,
			&cDomainClusters						///< domain clusters
		);

		cParameters.number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);

		cWorkloadBalancing.autotune_split_join_sizes();

		cParameters.number_of_local_clusters = cBaseTriangulation_To_GenericTree.number_of_initial_local_base_clusters;
		cParameters.number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_clusters);

		cParameters.number_of_local_initial_cells_after_domain_triangulation = cParameters.number_of_local_cells;
		cParameters.number_of_global_initial_cells_after_domain_triangulation = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_initial_cells_after_domain_triangulation);

		if (cParameters.verbosity_level >= 10)
		{
			for (int i = 0; i < cParameters.parallelization_mpi_size; i++)
			{
				// TODO: replace this with sending the output to the first rank
				if (i == cParameters.parallelization_mpi_rank)
				{
					std::cout << "***************************************************************" << std::endl;
					std::cout << "GENERIC TREE INFORMATION FOR RANK " << cParameters.parallelization_mpi_rank << std::endl;
					output_ClusterTreeInformation();
					std::cout << "***************************************************************" << std::endl;
				}
				sierpi::CGlobalComm::barrier();
			}
		}
	}



	/***************************************************************************************
	 * ADAPTIVITY
	 ***************************************************************************************/
private:
	/**
	 * run adaptive traversal
	 */
	void p_adaptivity_simulation_traversals()
	{
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		assert(cWorkloadBalancing.cluster_workload_scans_valid);
#endif

		CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationHyperbolic_Cluster::cAdaptiveTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeEdgeCommData_Adaptivity,
				cDomainClusters,
				&cParameters.number_of_local_cells
			);

		cParameters.number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cWorkloadBalancing.cluster_workload_scans_valid = false;
#endif

	}



private:
	long long p_adaptivity_setup_radial_dam_break_traversals(
			bool i_split_and_join_and_migration = true,
			bool i_use_inf_setup_criteria = true
	)
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_genericTreeNode)
				{
					i_genericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cSetupColumnTraversal.setup_KernelClass(
							cParameters.simulation_dataset_breaking_dam_posx,
							cParameters.simulation_dataset_breaking_dam_posy,
							cParameters.simulation_dataset_breaking_dam_radius,

							2,			// REFINE ONLY
							&cDatasets
						);
				}
			);

		long long prev_number_of_local_cells;
		long long prev_number_of_local_clusters;

#if DEBUG
		validate_global_information();
#endif

		bool repeat_traversal;

#if CONFIG_ENABLE_THREADING
		int timestep = 0;
#endif



		do
		{
#if CONFIG_ENABLE_MPI
			if (cParameters.load_balancing_statistics_each_nth_timestep != -1)
			{
				if (cParameters.load_balancing_statistics_each_nth_timestep == 0)
					p_output_load_balancing_statistics();
				else if ((cParameters.simulation_timestep_nr % cParameters.load_balancing_statistics_each_nth_timestep) == 0)
					p_output_load_balancing_statistics();
			}
#endif

#if CONFIG_ENABLE_THREADING
			if ((timestep & 1) == 0)
				cMainThreading->threading_updateResourceUtilization();
			timestep++;
#endif

			prev_number_of_local_cells = cParameters.number_of_local_cells;
			prev_number_of_local_clusters = cParameters.number_of_local_clusters;

			/*
			 * setup column refinements
			 */
			CHelper_GenericParallelAdaptivityTraversals::action<false>(
				&CSimulationHyperbolic_Cluster::cSetupColumnTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeEdgeCommData_Adaptivity,
				cDomainClusters,
				&cParameters.number_of_local_cells
			);

			/*
			 * split & join
			 */
			cParameters.number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);

			cWorkloadBalancing.autotune_split_join_sizes();

			repeat_traversal = false;
			if (i_split_and_join_and_migration)
			{
				repeat_traversal = cWorkloadBalancing.cluster_split_and_join_and_migration();
				cParameters.number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_clusters);
			}

			if (cParameters.verbosity_level > 8)
				if (sierpi::CGlobalComm::getCommRank() == 0)
					std::cout << " > triangles: " << cParameters.number_of_global_cells << ", number_of_simulation_clusters: " << cParameters.number_of_global_clusters << std::endl;

#if DEBUG
			validate_global_information();
#endif

			/*
			 * run workload balancing until nothing changes
			 */
			repeat_traversal |= p_setup_do_workload_balancing();

			repeat_traversal = sierpi::CGlobalComm::reduceBooleanOr(
					(prev_number_of_local_cells != cParameters.number_of_local_cells)		||
					(prev_number_of_local_clusters != cParameters.number_of_local_clusters)	||
					repeat_traversal
				);

		}
		while (repeat_traversal);


#if DEBUG
		validate_global_information();
#endif

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cWorkloadBalancing.cluster_workload_scans_valid = false;
#endif

		// always run p_simulation_cluster_split_and_join in the end to set cluster_workload_scans_valid
		cWorkloadBalancing.cluster_split_and_join_and_migration();


		/**
		 * finally setup element data values
		 */
		int backupSetupSurfaceMethod = cParameters.simulation_dataset_1_id;

		if (i_use_inf_setup_criteria)
			cParameters.simulation_dataset_1_id = CDatasets::SIMULATION_INTERACTIVE_UPDATE;

		p_setup_initial_cell_data();

		cParameters.simulation_dataset_1_id = backupSetupSurfaceMethod;


		return cParameters.number_of_local_cells;
	}



	/***************************************************************************************
	 * SETUP
	 ***************************************************************************************/
public:
	/**
	 * setup adaptive grid data
	 */
	void setup_GridDataWithAdaptiveSimulation()
	{


		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): START" << std::endl;

		long long prev_number_of_local_cells;
		long long prev_number_of_simulation_clusters;

		/*
		 * temporarily deactivate coarsening
		 */
		T coarsen_threshold_backup = cParameters.adaptive_coarsen_parameter_0;
		cParameters.adaptive_coarsen_parameter_0 = -9999999;

		updateClusterParameters();
		int max_setup_iterations = (cParameters.grid_max_relative_recursion_depth + cParameters.grid_min_relative_recursion_depth + 1);

		max_setup_iterations *= 20;

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement" << std::endl;

#if CONFIG_ENABLE_THREADING
		cMainThreading->threading_updateResourceUtilization();
#endif

		if (cParameters.simulation_dataset_1_id == CDatasets::SIMULATION_COEFF_CYLINDER)
		{
			if (cParameters.verbosity_level > 9)
				std::cout << "Setting up radial dam break" << std::endl;

			p_adaptivity_setup_radial_dam_break_traversals(true);
		}


		int iterations = 0;
		for (; iterations < max_setup_iterations; iterations++)
		{
			cWorkloadBalancing.autotune_split_join_sizes();

#if CONFIG_ENABLE_THREADING
			if ((iterations&1) == 0)
				cMainThreading->threading_updateResourceUtilization();
#endif

			prev_number_of_local_cells = cParameters.number_of_local_cells;
			prev_number_of_simulation_clusters = cParameters.number_of_local_clusters;

			// setup grid data
			if (cParameters.verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;
			p_setup_initial_cell_data();

			// run single time-step
			if (cParameters.verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_edge_comm" << std::endl;
			p_simulation_edge_comm();

			// refine / coarsen grid
			if (cParameters.verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_adaptive_traversals" << std::endl;
			p_adaptivity_simulation_traversals();

			// split/join clusters and data migration
			if (cParameters.verbosity_level > 99)
				std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_simulation_cluster_split_and_join_and_migration" << std::endl;

			cWorkloadBalancing.cluster_split_and_join_and_migration();

			if (cParameters.verbosity_level >= 5)
				std::cout << " > triangles: " << cParameters.number_of_local_cells << ", number_of_simulation_clusters: " << cParameters.number_of_local_clusters << std::endl;

			/*
			 * run workload balancing until nothing changes
			 */
			p_setup_do_workload_balancing();

			/*
			 * run workload balancing until nothing changes
			 */
			bool repeat_traversal = p_setup_do_workload_balancing();

			if (	!sierpi::CGlobalComm::reduceBooleanOr(
						prev_number_of_local_cells != cParameters.number_of_local_cells	||
						prev_number_of_simulation_clusters != cParameters.number_of_local_clusters	||
						repeat_traversal
					)
			)
				break;
		}


		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): iterative refinement / p_setup_initial_grid_data" << std::endl;

		p_setup_initial_cell_data();

		if (iterations == max_setup_iterations)
		{
			std::cerr << "WARNING: max iterations (" << max_setup_iterations << ") for setup reached" << std::endl;
			std::cerr << "WARNING: TODO: Use maximum displacement datasets!!!" << std::endl;
		}

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): updateClusterParameters" << std::endl;

		// update cluster parameters
		cParameters.adaptive_coarsen_parameter_0 = coarsen_threshold_backup;
		updateClusterParameters();


		/*****************************************************
		 * setup initial split of clusters
		 *
		 * initial splitting of clusters should be executed before
		 * setting up column to avoid any preprocessing adaptive effects
		 */

		if (cParameters.verbosity_level > 99)
			std::cout << "HYPERBOLIC RESET (setup_GridDataWithAdaptiveSimulation): setup_SplitJoinClusters" << std::endl;

		p_setup_do_workload_balancing();

		if (cParameters.verbosity_level >= 5)
			std::cout << " + split to " << cParameters.number_of_local_clusters << "/" << cParameters.number_of_local_clusters  << " clusters with " << cParameters.number_of_local_cells << "/" << cParameters.number_of_local_cells << " cells" << std::endl;
	}



	/**
	 * update scans data for clusters
	 */
	void updateClusterScanInformation(
			int i_new_number_of_threads = -1	///< new number of threads for which the scan datasets have to be updated
	) {
#if CONFIG_ENABLE_SCAN_DATA
		cWorkloadBalancing.updateClusterScanInformation(i_new_number_of_threads);
#endif
	}



	/***************************************************************************************
	 * setup cell data
	 ***************************************************************************************/

	/**
	 * setup element data specified by simulation_terrain_scene_id and simulation_water_surface_scene_id
	 */
private:
	void p_setup_initial_cell_data()
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_genericTreeNode)
				{
					// setup element data with respect to vertex positions
					sierpi::kernels::CSetup_CellData<CHyperbolicTypes>::TRAV cSetup_CellData;
					cSetup_CellData.setup_sfcMethods(i_genericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cSetup_CellData.cKernelClass.setup_Parameters(&cDatasets);
					cSetup_CellData.action(i_genericTreeNode->cCluster_TreeNode->cStacks);
				}
		);
	}



private:
	/**
	 * set element data at coordinate at (x,y) to *cellData
	 */
	void p_setup_cell_data_at_coordinate(
			T x,				///< x-coordinate inside of specific triangle
			T y,				///< y-coordinate inside of specific triangle
			CHyperbolicTypes::CSimulationTypes::CCellData *i_cellData	///< set element data at (x,y) to this data
	)
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_genericTreeNode)
				{
					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::CModify_OneElementValue_SelectByPoint<CHyperbolicTypes>::TRAV cModify_OneElementValue_SelectByPoint;

					cModify_OneElementValue_SelectByPoint.setup_sfcMethods(i_genericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cModify_OneElementValue_SelectByPoint.cKernelClass.setup(
							x,
							y,
							i_cellData
						);

					cModify_OneElementValue_SelectByPoint.action(i_genericTreeNode->cCluster_TreeNode->cStacks);
				}
			);
	}



	/***************************************************************************************
	 * EDGE COMM
	 ***************************************************************************************/
private:
	void p_simulation_edge_comm()
	{
		T local_timestep_size;

		CHelper_GenericParallelFluxCommTraversals::action<CHyperbolicTypes::CSimulationTypes::CCellData>(
				&CSimulationHyperbolic_Cluster::cEdgeCommTraversal,
				&CSimulationHyperbolic_Cluster::cCluster_ExchangeFluxCommData,
				cDomainClusters,

				cParameters.simulation_parameter_cfl,
				&local_timestep_size
			);

		cParameters.simulation_parameter_global_timestep_size = sierpi::CGlobalComm::reduceMin(local_timestep_size);

		if (cParameters.simulation_parameter_global_timestep_size < cParameters.simulation_parameter_minimum_timestep_size)
		{
			if (cParameters.simulation_timestep_nr != 0)
			{
				std::ostringstream s;
				s << "Simulation stopped due to small timestep " << cParameters.simulation_parameter_global_timestep_size << std::endl;

#if CONFIG_EXIT_ON_SMALL_TIMESTEP
				throw(std::runtime_error(s.str()));
#else
				std::cerr << "EMERGENCY (simulation probably unstable): fixing time-step size from " << cParameters.simulation_parameter_global_timestep_size << " to " << cParameters.simulation_parameter_minimum_timestep_size << std::endl;
#endif
			}

			cParameters.simulation_parameter_global_timestep_size = cParameters.simulation_parameter_minimum_timestep_size;
		}
		else if (std::abs(cParameters.simulation_parameter_global_timestep_size) > cParameters.simulation_parameter_maximum_timestep_size)
		{
			cParameters.simulation_parameter_global_timestep_size = cParameters.simulation_parameter_maximum_timestep_size;
		}
	}



	/***************************************************************************************
	 * UPDATE CLUSTER SIMULATION PARAMETERS
	 *
	 * This has to be executed whenever the simulation parameters are updated
	 ***************************************************************************************/
public:
	void updateClusterParameters()
	{
		/***************************************************************************************
		 * EDGE COMM: setup boundary parameters
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					EBoundaryConditions boundary_condition = (EBoundaryConditions)cParameters.simulation_domain_boundary_condition;

					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.setBoundaryDirichlet(&cParameters.simulation_domain_boundary_dirichlet_edge_nodal_data);

					/**
					 * check
					 */
					if (cParameters.simulation_domain_strip_boundary_condition_dataset_right)
					{
						sierpi::CTriangle_Factory &cTriangle_Factory = i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory;

						// vertical hypotenuse
						if (
							(cTriangle_Factory.vertices[0][0] - cTriangle_Factory.vertices[1][0]) == 0	&&
							(cTriangle_Factory.vertices[0][0] == (cParameters.simulation_dataset_default_domain_size_x*(T)0.5 + cParameters.simulation_dataset_default_domain_translate_x))
						) {
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
						else if (
							(cTriangle_Factory.vertices[1][0] - cTriangle_Factory.vertices[2][0]) == 0	&&
							(cTriangle_Factory.vertices[1][0] == (cParameters.simulation_dataset_default_domain_size_x*(T)0.5 + cParameters.simulation_dataset_default_domain_translate_x))
						) {
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
						else if (
							(cTriangle_Factory.vertices[2][0] - cTriangle_Factory.vertices[0][0]) == 0	&&
							(cTriangle_Factory.vertices[2][0] == (cParameters.simulation_dataset_default_domain_size_x*(T)0.5 + cParameters.simulation_dataset_default_domain_translate_x))
						) {
							boundary_condition = EBoundaryConditions::BOUNDARY_CONDITION_DATASET;
						}
					}

					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.setBoundaryCondition(boundary_condition);
				}
			);



		/***************************************************************************************
		 * EDGE COMM: setup generic parameters
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.setParameters(
							cParameters.simulation_parameter_global_timestep_size,
							cParameters.simulation_dataset_default_domain_size_x*cParameters.simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,
#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
							cParameters.simulation_dataset_default_domain_size_z,
#endif
							cParameters.simulation_parameter_gravitation,

							cParameters.adaptive_refine_parameter_0,
							cParameters.adaptive_coarsen_parameter_0,

							&cDatasets
						);

					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.setAdaptivityParameters(
							cParameters.adaptive_refine_parameter_0,
							cParameters.adaptive_coarsen_parameter_0
						);
				}
			);


		/***************************************************************************************
		 * ADAPTIVITY
		 ***************************************************************************************/
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cAdaptiveTraversal.setup_KernelClass(
							cParameters.simulation_dataset_default_domain_size_x*cParameters.simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,

							cParameters.adaptive_refine_parameter_0,
							cParameters.adaptive_coarsen_parameter_0,

							cParameters.adaptive_refine_parameter_1,
							cParameters.adaptive_coarsen_parameter_1,

							&cDatasets
						);
				}
			);
	}




public:
	/**
	 * SPLIT/JOINS for SETUP
	 *
	 * This splits all sub-clusters into appropriate sizes for the initialization.
	 */
	inline bool p_setup_do_workload_balancing()
	{
		long long prev_number_of_local_cells = 0;
		long long prev_number_of_local_clusters = 0;

		bool something_changed = false;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
		do
		{
			prev_number_of_local_cells = cParameters.number_of_local_cells;
			prev_number_of_local_clusters = cParameters.number_of_local_clusters;

#if CONFIG_ENABLE_MPI
			if (cParameters.load_balancing_statistics_each_nth_timestep != -1)
			{
				if (cParameters.load_balancing_statistics_each_nth_timestep == 0)
					p_output_load_balancing_statistics();
				else if ((cParameters.simulation_timestep_nr % cParameters.load_balancing_statistics_each_nth_timestep) == 0)
					p_output_load_balancing_statistics();
			}
#endif

			p_adaptivity_simulation_traversals();

			something_changed = cWorkloadBalancing.cluster_split_and_join_and_migration();

			something_changed = sierpi::CGlobalComm::reduceBooleanOr(
					prev_number_of_local_cells != cParameters.number_of_local_cells			||
					prev_number_of_local_clusters != cParameters.number_of_local_clusters	||
					something_changed
			);
		}
		while (something_changed);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		return something_changed;
	}



#if CONFIG_ENABLE_PRINT_RLE_STATISTICS

	void p_print_RLE_Statistics()
	{
		// cell counter
		unsigned long long cells = 0;

		// rle element counter
		unsigned long long rle_info_counter = 0;

		// overall edges and vertices if encoded without RLE.
		// each vertex is considered as taking one work.
		unsigned long long cluster_boundary_edges_and_vertices = 0;

		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&cells,&rle_info_counter,&cluster_boundary_edges_and_vertices](CGeneric_TreeNode_ *i_genericTreeNode)
				{
					cells += i_genericTreeNode->workload_in_subtree;

					for (	auto i = i_genericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
							i != i_genericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
							i++
					)
					{
						rle_info_counter++;

						if ((*i).edge_comm_elements == 0)
							cluster_boundary_edges_and_vertices++;
						else
							cluster_boundary_edges_and_vertices += (*i).edge_comm_elements;
					}

					for (	auto i = i_genericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
							i != i_genericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
							i++
					)
					{
						rle_info_counter++;

						if ((*i).edge_comm_elements == 0)
							cluster_boundary_edges_and_vertices++;
						else
							cluster_boundary_edges_and_vertices += (*i).edge_comm_elements;
					}

				}
		);

		static bool header_printed = false;
		if (!header_printed)
		{
			std::cerr << "Timestep\tRLE_info_counter\tboundary_non_rle_encoded_edges_and_vertices\tmemory_saving_factor" << std::endl;
			header_printed = true;
		}

		std::cerr << cParameters.simulation_timestep_nr << ":\t" <<
				cells << "\t" <<
				rle_info_counter << "\t" <<
				cluster_boundary_edges_and_vertices << "\t" <<
				((double)cluster_boundary_edges_and_vertices/(double)rle_info_counter) <<
				std::endl;
	}

#endif



#if CONFIG_ENABLE_MPI
	void p_output_load_balancing_statistics()
	{
		int mpi_rank = sierpi::CGlobalComm::getCommRank();
		int mpi_size = sierpi::CGlobalComm::getCommSize();

		/*
		 * loadbalancing statistics
		 */
		std::stringstream load_balancing_stats;

		double average = (double)cParameters.number_of_global_cells / (double)sierpi::CGlobalComm::getCommSize();
		double rel_deviation = (average - (double)cParameters.number_of_local_cells) / average;

		load_balancing_stats << mpi_rank << "\t" << cParameters.number_of_local_clusters << "\t" << cParameters.number_of_local_cells << "\t" << average << "\t" << rel_deviation << std::endl;


		if (mpi_rank == 0)
		{
			std::cout << std::endl;
			std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << "+ LOAD BALANCING STATISTICS FOR TIMESTEP " << cParameters.simulation_timestep_nr << std::endl;
			std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << "Rank\tlocal_clusters\tlocal_cells\ttarget_average\trel_deviation" << std::endl;

			std::cout << load_balancing_stats.str();

			for (int i = 1; i < mpi_size; i++)
			{
				MPI_Status status;
				MPI_Probe(i, 666, MPI_COMM_WORLD, &status);

				int length;
				MPI_Get_count(&status, MPI_BYTE, &length);

				char *buf = new char[length+1];
				MPI_Recv(buf, length, MPI_BYTE, i, 666, MPI_COMM_WORLD, &status);
				buf[length] = '\0';

				std::cout << buf;

				delete [] buf;
			}
		}
		else
		{
			std::string s = load_balancing_stats.str();
			MPI_Send((void*)s.c_str(), s.length(), MPI_BYTE, 0, 666, MPI_COMM_WORLD);
		}
	}
#endif


/***************************************************************************************
 * TIMESTEP
 ***************************************************************************************/

public:
	inline void runSingleTimestep()
	{
#if CONFIG_ENABLE_THREADING
		cMainThreading->threading_updateResourceUtilization();
#endif

#if CONFIG_BENCHMARK_SPHERE
		/*
		 * adjust timestep in final simulation step
		 */
		if (cParameters.simulation_run_for_fixed_simulation_time != -1){
			if(cParameters.simulation_timestamp_for_timestep
					+ cParameters.simulation_parameter_global_timestep_size
					> cParameters.simulation_run_for_fixed_simulation_time){
//				std::cout << "timestamp: " << cParameters.simulation_timestamp_for_timestep << std::endl;
//				std::cout << "final timestamp: " << cParameters.simulation_run_for_fixed_simulation_time << std::endl;
//				std::cout << "old timestep size: " << cParameters.simulation_parameter_global_timestep_size << std::endl;
				T new_timestep_size = cParameters.simulation_run_for_fixed_simulation_time - cParameters.simulation_timestamp_for_timestep;
				if (new_timestep_size == (T)0)
					new_timestep_size = cParameters.simulation_parameter_global_timestep_size;
				cParameters.simulation_parameter_cfl = - (new_timestep_size);
//				std::cout << "new timestep size: " << cParameters.simulation_parameter_cfl << std::endl;
			}
		}
//		T max_timestep_size = (T)1 / ((T)cParameters.grid_initial_recursion_depth / (T)2);
//		if (cParameters.simulation_parameter_global_timestep_size > max_timestep_size)
//			cParameters.simulation_parameter_global_timestep_size = max_timestep_size;
//		CGlobal& gx = CGlobal::getInstance();
//		gx.setTS(cParameters.simulation_timestamp_for_timestep);
#endif
//		std::cout << "asdlkjh " << cParameters.simulation_parameter_global_timestep_size << std::endl;
		cParameters.simulation_dataset_benchmark_input_timestamp = cParameters.simulation_timestamp_for_timestep;

		// simulation timestep
		p_simulation_edge_comm();

		// adaptive timestep
		p_adaptivity_simulation_traversals();

		if (cParameters.simulation_timestep_nr % cParameters.cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations
			cWorkloadBalancing.cluster_split_and_join_and_migration();
		}

#if CONFIG_ENABLE_PRINT_RLE_STATISTICS
		p_print_RLE_Statistics();
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

#if CONFIG_BENCHMARK_SPHERE
		if (cParameters.simulation_timestamp_for_timestep == 0){
			CGlobal& gx = CGlobal::getInstance();
			if (gx.getBenchmarkType() != CGlobal::E_SOLID_BODY_ROTATION){
				cParameters.simulation_parameter_cfl = - cParameters.simulation_parameter_global_timestep_size;
			}
		}
//		if (gx.getBenchmarkType() != CGlobal::E_SOLID_BODY_ROTATION){
//			T max_timestep_size = (T)1 / ((T)cParameters.grid_initial_recursion_depth / (T)2);
//			if (cParameters.simulation_parameter_global_timestep_size > max_timestep_size){
//				cParameters.simulation_parameter_global_timestep_size = max_timestep_size;
//			}
//		}
#endif

		cParameters.simulation_timestep_nr++;
		cParameters.simulation_timestamp_for_timestep += cParameters.simulation_parameter_global_timestep_size;

#if SIMULATION_EXIT_LOCAL_CELLS_EXCEED_VALUE > 0 && CONFIG_ENABLE_MPI
		if (cParameters.number_of_local_cells > cParameters.simulation_threshold_exit_if_exceeding_local_number_of_cells)
		{
			std::cerr << "NUMBER OF LOCAL CELLS EXCEEDED THRESHOLD: " << cParameters.simulation_threshold_exit_if_exceeding_local_number_of_cells << std::endl;
			exit(-1);
		}
#endif

		cWorkloadBalancing.autotune_split_join_sizes();
	}




	/***************************************************************************************
	 * run a single TIMESTEP and update the detailed benchmarks
	 ***************************************************************************************/
public:
	/**
	 * execute a single time-step
	 */
	inline void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,	///< add time taken for edge communication traversal to this value
			double *io_adaptiveTime,	///< add time taken for adaptive traversal to this value
			double *io_splitJoinTime	///< add time taken for split/joins to this value

#if CONFIG_ENABLE_MPI
			,
			double *io_clusterMigration_dm	///< add time taken for cluster migration to this value
#endif
	) {
#if CONFIG_ENABLE_THREADING
			cMainThreading->threading_updateResourceUtilization();
#endif

		if (cParameters.verbosity_level >= 9)
			std::cout << "timestep nr: " << cParameters.simulation_timestep_nr << std::endl;

		cParameters.simulation_dataset_benchmark_input_timestamp = cParameters.simulation_timestamp_for_timestep;

		if (cParameters.verbosity_level >= 9)
			std::cout << "simulation pass" << std::endl;

		// simulation timestep pass
		cStopwatch.start();
		p_simulation_edge_comm();
		*io_edgeCommTime += cStopwatch.getTimeSinceStart();

		if (cParameters.verbosity_level >= 9)
			std::cout << "simulation pass FIN" << std::endl;

		if (cParameters.verbosity_level >= 9)
			std::cout << "adaptivity pass" << std::endl;

		// adaptivity pass
		cStopwatch.start();
		p_adaptivity_simulation_traversals();
		*io_adaptiveTime += cStopwatch.getTimeSinceStart();

		if (cParameters.verbosity_level >= 9)
			std::cout << "adaptivity pass FIN" << std::endl;

		if (cParameters.simulation_timestep_nr % cParameters.cluster_split_and_join_every_nth_timestep == 0)
		{
			// split/join operations for SM
			cWorkloadBalancing.cluster_split_and_join_and_migration_verbose(
					io_splitJoinTime
#if CONFIG_ENABLE_MPI
					,
					io_clusterMigration_dm
#endif
				);
		}

#if CONFIG_ENABLE_PRINT_RLE_STATISTICS
		p_print_RLE_Statistics();
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif

		cParameters.simulation_timestep_nr++;
		cParameters.simulation_timestamp_for_timestep += cParameters.simulation_parameter_global_timestep_size;

		cWorkloadBalancing.autotune_split_join_sizes();
	}



	/***************************************************************************************
	 * Radial dam break
	 ***************************************************************************************/

public:
	/**
	 * setup column at 2D position with given radius
	 */
	void setup_RadialDamBreak(
		T i_position_x,		///< x-coordinate of center of column to set-up
		T i_position_y,		///< y-coordinate of center of column to set-up
		T i_radius			///< radius of column to setup
	) {
		if (cParameters.verbosity_level > 9)
			std::cout << "setup_RadialDamBreak: position=(" << i_position_x <<  ", " << i_position_y << "), radius=" << i_radius << std::endl;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
		if (!CCube_To_Sphere_Projection::isValidCoordinate(i_position_x, i_position_y))
		{
			std::cout << "invalid coordinate for radial dam break: position=(" << i_position_x <<  ", " << i_position_y << "), radius=" << i_radius << std::endl;
			return;
		}
#endif

		cParameters.simulation_dataset_breaking_dam_posx = i_position_x;
		cParameters.simulation_dataset_breaking_dam_posy = i_position_y;
		cParameters.simulation_dataset_breaking_dam_radius = i_radius;

		if (cParameters.verbosity_level >= 5)
			std::cout << "Radial dam break setup at (" << cParameters.simulation_dataset_breaking_dam_posx << ", " << cParameters.simulation_dataset_breaking_dam_posy << ") with radius " << cParameters.simulation_dataset_breaking_dam_radius << std::endl;

		cParameters.number_of_local_cells = p_adaptivity_setup_radial_dam_break_traversals(true);
	}



public:
/*****************************************************************************************************************
 * DEBUG: Output
 *****************************************************************************************************************/

	void debug_OutputEdgeCommunicationInformation(
			T x,	///< x-coordinate of triangle cell
			T y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *cCluster_TreeNode = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							cCluster_TreeNode->cTriangleFactory.vertices[0][0], cCluster_TreeNode->cTriangleFactory.vertices[0][1],
							cCluster_TreeNode->cTriangleFactory.vertices[1][0], cCluster_TreeNode->cTriangleFactory.vertices[1][1],
							cCluster_TreeNode->cTriangleFactory.vertices[2][0], cCluster_TreeNode->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "Cluster ID: " << cCluster_TreeNode->cCluster_UniqueId << std::endl;
					std::cout << "Adjacent Communication Information" << std::endl;
					std::cout << cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters << std::endl;
					std::cout << std::endl;
				}
		);
	}



#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

public:
	/**
	 * setup cluster for migration
	 */
#if 0
	void update_ClustersForMigrationByNumberOfClusters(
			bool i_forward_or_backward,				///< forward or backward migration?
			int i_number_of_clusters_to_migrate = 1	///< number of unmarked clusters to migrate
	) {
		// forbidden
		assert(false);

		int dst_mpi_rank = cParameters.parallelization_mpi_rank + (i_forward_or_backward ? 1 : -1);

		if (dst_mpi_rank >= cParameters.parallelization_mpi_size)
			return;

		if (dst_mpi_rank < 0)
			return;

		if (i_forward_or_backward)
		{
			cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
					{
						CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

						if (i_number_of_clusters_to_migrate <= 0)
							return;

						if (!node->isMigrationRequested())
							i_number_of_clusters_to_migrate--;

						node->requestMigrationToRank(dst_mpi_rank);
					}
			);
		}
		else
		{
			cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
					{
						CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

						if (i_number_of_clusters_to_migrate <= 0)
							return;

						if (!node->isMigrationRequested())
							i_number_of_clusters_to_migrate--;

						node->requestMigrationToRank(dst_mpi_rank);
					}
			);
		}
	}
#endif

#endif



public:
/*****************************************************************************************************************
 * DEBUG: output element data
 *****************************************************************************************************************/
	void debug_OutputCellData(
			T i_coord_x,		///< x-coordinate of triangle cell
			T i_coord_y		///< y-coordinate of triangle cell
	)
	{
		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							i_coord_x, i_coord_y
						))
							return;

					/// output element data at given point
					sierpi::kernels::CStringOutput_CellData_Normal_SelectByPoint<CHyperbolicTypes>::TRAV cStringOutput_CellData_SelectByPoint;

					cStringOutput_CellData_SelectByPoint.setup_sfcMethods(node->cTriangleFactory);
					cStringOutput_CellData_SelectByPoint.cKernelClass.setup(i_coord_x, i_coord_y);
					cStringOutput_CellData_SelectByPoint.action(node->cStacks);
				}
		);
	}


/***************************************************************************************
 * DEBUG: output information about triangle cluster
 ***************************************************************************************/
public:
	void debug_OutputClusterInformation(
			T x,	///< x-coordinate of triangle cell
			T y		///< y-coordinate of triangle cell
	) {
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					if (!CPointInTriangleTest<T>::test(
							node->cTriangleFactory.vertices[0][0], node->cTriangleFactory.vertices[0][1],
							node->cTriangleFactory.vertices[1][0], node->cTriangleFactory.vertices[1][1],
							node->cTriangleFactory.vertices[2][0], node->cTriangleFactory.vertices[2][1],
							x, y
						))
							return;

					std::cout << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << "* Cluster information at " << x << ", " << y << ":" << std::endl;
					std::cout << "***************************************************************" << std::endl;
					std::cout << std::endl;
					std::cout << *node << std::endl;
					std::cout << "min CFL cell_size / max_speed after edge comm: " << node->cSimulation_Cluster->local_timestep_size << std::endl;

#if 0
					CSimulationHyperbolic_Cluster *clusterHandler = node->cCluster;

					std::cout << std::endl;
					std::cout << "ClusterAndStackInformation:" << std::endl;
					std::cout << "  + Structure Stacks.direction = " << node->cStacks->structure_stacks.direction << std::endl;
					std::cout << "  + Structure Stacks.forward.getStackElementCounter() = " << node->cStacks->structure_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + Structure Stacks.backward.getStackElementCounter() = " << node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + CellData Stacks.direction = " << node->cStacks->cell_data_stacks.direction << std::endl;
					std::cout << "  + CellData Stacks.forward.getStackElementCounter() = " << node->cStacks->cell_data_stacks.forward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + CellData Stacks.backward.getStackElementCounter() = " << node->cStacks->cell_data_stacks.backward.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + EdgeComm Stacks.left = " << (void*)node->cStacks->edge_data_comm_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm Stacks.right = " << (void*)node->cStacks->edge_data_comm_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.left = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.left.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + EdgeComm X Stacks.right = " << (void*)node->cStacks->edge_data_comm_exchange_edge_stacks.right.getNumberOfElementsOnStack() << std::endl;
					std::cout << "  + " << std::endl;
					std::cout << "  + splitJoin.elements_in_first_triangle" << clusterHandler->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements << std::endl;
					std::cout << "  + splitJoin.elements_in_second_triangle" << clusterHandler->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements << std::endl;
					std::cout << std::endl;
#endif
				}
		);
	}


	/**
	 * output simulation specific data - e. g. dart smpling points
	 */
	void output_simulationSpecificData(
		const std::string &i_identifier,
		const std::string &i_parameters
	)
	{
		cOutputSimulationSpecificData.output(i_identifier, i_parameters);
	}


	/**
	 * output information about the underlying tree
	 */
	void output_ClusterTreeInformation()
	{
		cDomainClusters.traverse_GenericTreeNode_LeafAndPreorderMidNodes_Depth_Serial(
				[=](CGeneric_TreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					assert(cGenericTreeNode->first_child_node == nullptr);
					assert(cGenericTreeNode->second_child_node == nullptr);
					assert(cGenericTreeNode->cCluster_TreeNode != nullptr);
#endif
					// LEAF
					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;
					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "no parent node found";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first child" : "second child");

					std::cout << "-> LEAF  [" << first_or_second_child << "]: (depth: " << depth;
					std::cout << ", base_triangle: " << cGenericTreeNode->base_triangulation_node;

					if (false)
					{
#if CONFIG_ENABLE_SCAN_DATA
						std::cout << ", workload (start/size/end): " <<
									cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index <<
									" , thread(start/end): " << cGenericTreeNode->workload_thread_id_start << ", " << cGenericTreeNode->workload_thread_id_end <<
									" , thread_id: " << cGenericTreeNode->workload_thread_id;
#else
						if (cGenericTreeNode->cCluster_TreeNode == nullptr)
							std::cout << ", cGenericTreeNode->cCluster_TreeNode == nullptr !!!";
						else if (cGenericTreeNode->cCluster_TreeNode->cStacks == nullptr)
							std::cout << ", cGenericTreeNode->cCluster_TreeNode->cStacks == nullptr !!!";
						else if (cGenericTreeNode->cCluster_TreeNode->cStacks->structure_stacks.forward.getMaxNumberOfElements() == 0)
							std::cout << ", cGenericTreeNode->cCluster_TreeNode->cStacks->structure_stacks.forward.stack_ptr == nullptr !!!";
						else
							std::cout << ", workload (size): " << cGenericTreeNode->cCluster_TreeNode->cStacks->structure_stacks.structure_getNumberOfTrianglesInTriangle();
#endif
						if (cGenericTreeNode->cCluster_TreeNode == nullptr)
						{
							std::cout << ", empty Leaf (ERROR!)" << std::endl;
							return;
						}
					}

					std::cout << ")";

					std::cout << " cCluster_UniqueId: " << cGenericTreeNode->cCluster_TreeNode->cCluster_UniqueId;

#if DEBUG
					std::cout << "   timestep size: " << cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.getTimestepSize();
#endif
					std::cout << std::endl;
				},
				[=](CGeneric_TreeNode_ *cGenericTreeNode, int depth)
				{
#if DEBUG
					if (cGenericTreeNode->first_child_node)
						assert(cGenericTreeNode->first_child_node->parent_node == cGenericTreeNode);

					if (cGenericTreeNode->second_child_node)
						assert(cGenericTreeNode->second_child_node->parent_node == cGenericTreeNode);
#endif

					// MIDDLE NODE
					for (int i = 0; i < depth; i++)
						std::cout << "    ";

					const char *first_or_second_child;

					if (cGenericTreeNode->parent_node == nullptr)
						first_or_second_child = "parent non existing";
					else
						first_or_second_child = (cGenericTreeNode->parent_node->first_child_node == cGenericTreeNode ? "first" : "second");

					if (cGenericTreeNode->first_child_node)
						std::cout << "[FIRST CHILD EXISTING]";

					if (cGenericTreeNode->second_child_node)
						std::cout << "[SECOND CHILD EXISTING]";

					std::cout << "| MID NODE [parents " << first_or_second_child << " child]: (";
					std::cout << "depth: " << depth;
					std::cout << ", base_triangle: " << cGenericTreeNode->base_triangulation_node;

#if CONFIG_ENABLE_SCAN_DATA
					std::cout << ", workload (start/size/end): " << cGenericTreeNode->workload_scan_start_index << ", " << cGenericTreeNode->workload_in_subtree << ", " << cGenericTreeNode->workload_scan_end_index;
#endif
					std::cout << ")";


#if CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
//					assert(cGenericTreeNode->cCluster_TreeNode == nullptr);
#else
					if (cGenericTreeNode->cCluster_TreeNode == nullptr)
						std::cout << " cGenericTreeNode->cCluster_TreeNode == nullptr" << std::endl;

					std::cout << " cCluster_UniqueId: " << cGenericTreeNode->cCluster_TreeNode->cCluster_UniqueId << std::endl;
#endif
					std::cout << std::endl;
				}
			);
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T	*o_translate_x,	///< origin of domain in world-space
			T	*o_translate_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	)
	{
		*o_translate_x = cParameters.simulation_dataset_default_domain_translate_x;
		*o_translate_y = cParameters.simulation_dataset_default_domain_translate_y;

		*o_size_x = cParameters.simulation_dataset_default_domain_size_x;
		*o_size_y = cParameters.simulation_dataset_default_domain_size_y;
	}



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS

	void action_Validation_EdgeCommLength()
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cSimulation_Cluster->cCluster_ExchangeEdgeCommData_Adaptivity.validateCommDataLength();
			}
		);
	}


	void action_Validation_EdgeCommMidpointsAndNormals()
	{
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				CSimulationHyperbolic_Cluster *worker = node->cSimulation_Cluster;

				/*
				 * setup fake validation element data stack
				 */
				if (node->cValidationStacks->cell_data_stacks.forward.getNumberOfElementsOnStack() != node->cStacks->cell_data_stacks.forward.getNumberOfElementsOnStack())
				{
					node->cValidationStacks->cell_data_stacks.forward.clear();

					for (unsigned int i = 0; i < node->cStacks->cell_data_stacks.forward.getNumberOfElementsOnStack(); i++)
					{
						node->cValidationStacks->cell_data_stacks.forward.push(CValCellData());
					}
				}

				worker->cEdgeComm_ValidateComm.cKernelClass.noCellDataChecks = true;

				worker->cEdgeComm_ValidateComm.actionFirstPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->cell_data_stacks,
						node->cValidationStacks->edge_data_comm_left_edge_stack,
						node->cValidationStacks->edge_data_comm_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);
			}
		);

#if CONFIG_ENABLE_MPI

		/*
		 * TODO: run this on one thread while running the the shared memory communications on another thread
		 */
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cSimulation_Cluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_DM_pass1();
			}
		);

#endif

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_SM_and_DM_Wait();
			}
		);

#if CONFIG_ENABLE_MPI

		cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				node->cSimulation_Cluster->cCluster_ExchangeEdgeCommData_Validation.exchangeEdgeCommData_DM_pass2();
			}
		);

#endif

		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				CSimulationHyperbolic_Cluster *cSimulation_Cluster = node->cSimulation_Cluster;

				/*
				 * stacks are cleared here since there may be some data left from the last traversal
				 */
				node->cValidationStacks->edge_data_comm_left_edge_stack.clear();
				node->cValidationStacks->edge_data_comm_right_edge_stack.clear();

				/**
				 * second pass
				 */
				// "backup" backward stack
				size_t number_of_elements_on_backward_stack = node->cStacks->structure_stacks.backward.getNumberOfElementsOnStack();

				cSimulation_Cluster->cEdgeComm_ValidateComm.actionSecondPass(
						node->cStacks->structure_stacks,
						node->cValidationStacks->cell_data_stacks,
						node->cValidationStacks->edge_data_comm_exchange_left_edge_stack,		/// !!! here we use the "exchange stacks"!
						node->cValidationStacks->edge_data_comm_exchange_right_edge_stack,
						node->cValidationStacks->edge_comm_buffer
					);

				node->cStacks->structure_stacks.backward.setStackElementCounter(number_of_elements_on_backward_stack);
			}
		);
	}

#endif

	void action_Validation()
	{
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		action_Validation_EdgeCommLength();
		action_Validation_EdgeCommMidpointsAndNormals();
#endif
	}


	void exception_output()
	{
		std::cout << "**************************************************" << std::endl;
		std::cout << "* EXCEPTION OUTPUT                               *" << std::endl;
		std::cout << "**************************************************" << std::endl;
		std::cout << std::endl;

		output_ClusterTreeInformation();
	}

	void outputVerboseInformation()
	{
		cParameters.outputVerboseInformation();
		cDatasets.outputVerboseInformation();
	}

#if DEBUG
	void validate_global_information()
	{
		if (cParameters.verbosity_level > 19)
			std::cout << "validate_global_information" << std::endl;

		if (cParameters.verbosity_level > 9)
		{
#if CONFIG_ENABLE_MPI
			std::cout << "cParameters.number_of_local_cells for rank " << sierpi::CGlobalComm::getCommRank() << ": " << cParameters.number_of_local_cells << std::endl;
#else
			std::cout << "cParameters.number_of_local_cells: " << cParameters.number_of_local_cells << std::endl;
#endif
		}

		long long test_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);

		if (test_global_cells != cParameters.number_of_global_cells)
		{
			std::cerr << "validate_global_information: test_global_cells (" << test_global_cells << ") != cParameters.number_of_global_cells (" << cParameters.number_of_global_cells << ")" << std::endl;
			assert(false);
			exit(-1);
		}
	}
#endif


	/**
	 * dataset accessor for visualization of benchmark data
	 */
	CDatasets *getDatasets()
	{
		return &cDatasets;
	}
};


#endif /* CSIMULATION_HYPERBOLIC_PARALLEL_HPP_ */
