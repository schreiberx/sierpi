/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mai 4, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHYPERBOLIC_PARALLEL_CLUSTER_HANDLER_HPP_
#define CHYPERBOLIC_PARALLEL_CLUSTER_HANDLER_HPP_


// import simulation types
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"


// specialized traversators (to speed up compilation due to parallel processing)
#include "../hyperbolic_common/traversators/CSpecialized_EdgeComm_Normals_Depth.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_Cell.hpp"
#include "../hyperbolic_common/traversators/CSpecialized_Setup_Column.hpp"


// structure setup traversator
#include "libsierpi/traversators/setup/CSetup_Structure_CellData.hpp"

#include "libsierpi/parallelization/CMigration.hpp"
#include "libsierpi/cluster/CCluster_TreeNode.hpp"
#include "libsierpi/cluster/CCluster_ExchangeEdgeCommData.hpp"
#include "libsierpi/cluster/CCluster_ExchangeFluxCommData.hpp"

#include "libsierpi/grid/CDomain_BaseTriangulation.hpp"

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS
#	include "libsierpi/cluster/CCluster_ExchangeVertexDataCommData.hpp"
#	include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays_ContinousSurface_2Pass_1stPass.hpp"
#	include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays_ContinousSurface_2Pass_2ndPass.hpp"

#elif CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#	include "libsierpi/cluster/CCluster_ExchangeVertexDataCommData.hpp"
#	include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays_ContinousSurface.hpp"
#endif


#include "libsierpi/kernels/CModify_OneElementValue_SelectByPoint.hpp"
#include "libsierpi/kernels/stringOutput/CStringOutput_CellData_Normal_SelectByPoint.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
	#include "libsierpi/kernels/validate/CEdgeComm_ValidateComm.hpp"
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	#include "../hyperbolic_common/subsimulation_generic/kernels/CSetup_CellData_Validation.hpp"
#endif

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

#include "libsierpi/parallelization/CStackAccessorMethods.hpp"



/**
 * \brief Hyperbolic Simulation Cluster Handler
 *
 * This class implements all kinds of sub-cluster related user-defined data storages.
 */
class CSimulationHyperbolic_Cluster
{
public:
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * "exports" for CCluster_TreeNode
	 */
	typedef CHyperbolicTypes::CSimulationTypes CSimulationTypes;
	typedef CHyperbolicTypes::CVisualizationTypes CVisualizationTypes;
	typedef CHyperbolicTypes::CSimulationStacks CSimulationStacks;

	/**
	 * pointer to handler of this cluster node
	 */
	CCluster_TreeNode_ *cCluster_TreeNode;

	/**
	 * TIMESTEP: EDGE COMM
	 */
	/// edge communication and simulation without adaptivity
	sierpi::travs::CSpecialized_EdgeComm_Normals_Depth cEdgeCommTraversal;

	/**
	 * HYPERBOLIC ADAPTIVITY
	 */
	/// adaptivity - refine/coarsen
	sierpi::travs::CSpecialized_AdaptiveConformingGrid_VertexCoords_Normals_Depth_DepthLimiters_CellData cAdaptiveTraversal;

	/**
	 * WATER COLUMN SETUP ADAPTIVITY
	 */
	/// setup of a 'water' column
	sierpi::travs::CSpecialized_Setup_Column cSetupColumnTraversal;

	/**
	 * EDGE COMM FOR ADAPTIVE TRAVERSALS
	 */
	/// handler for adaptivity information exchange with adjacent clusters
	sierpi::CCluster_ExchangeEdgeCommData<
			CCluster_TreeNode_,
			char,
			sierpi::CStackAccessorMethodsAdaptivityEdgeData<CCluster_TreeNode_>
		> cCluster_ExchangeEdgeCommData_Adaptivity;


	/**
	 * INTER-PARTITION-EDGE COMMUNICATION
	 */
	/// handler for flux communication with adjacent clusters
	sierpi::CCluster_ExchangeFluxCommData<
			CCluster_TreeNode_,
			CHyperbolicTypes::CSimulationTypes::CEdgeData,
			sierpi::CStackAccessorMethodsSimulationEdgeData<CCluster_TreeNode_, CHyperbolicTypes::CSimulationTypes::CEdgeData>,
			sierpi::travs::CSpecialized_EdgeComm_Normals_Depth::CKernelClass
		> cCluster_ExchangeFluxCommData;




#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS

	sierpi::CStack<sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::CVertexPackage> cTemporaryVertices;

	/**
	 * INTER-PARTITION-VERTEX-DATA COMMUNICATION
	 */
	/// handler for vertex communication with adjacent clusters
	sierpi::CCluster_ExchangeVertexDataCommData<
			CCluster_TreeNode_,
			CHyperbolicTypes::CVisualizationTypes::CNodeData,
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::CNodeData>,
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::TRAV::CKernelClass
		> cCluster_ExchangeVertexDataCommData_2Pass_1stPass;

	/// handler for vertex communication with adjacent clusters
	sierpi::CCluster_ExchangeVertexDataCommData<
			CCluster_TreeNode_,
			CHyperbolicTypes::CVisualizationTypes::CNodeData,
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::CNodeData>,
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_2ndPass::TRAV::CKernelClass
		> cCluster_ExchangeVertexDataCommData_2Pass_2ndPass;

#elif CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

	/**
	 * INTER-PARTITION-VERTEX-DATA COMMUNICATION
	 */
	/// handler for vertex communication with adjacent clusters
	sierpi::CCluster_ExchangeVertexDataCommData<
			CCluster_TreeNode_,
			CHyperbolicTypes::CVisualizationTypes::CNodeData,
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::CNodeData>,
			sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV::CKernelClass
		> cCluster_ExchangeVertexDataCommData;

#endif



#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	// validate edge communication
	sierpi::kernels::CEdgeComm_ValidateComm::TRAV cEdgeComm_ValidateComm;

	/// handler for edge communication with adjacent clusters
	sierpi::CCluster_ExchangeEdgeCommData<CCluster_TreeNode_, CValEdgeData, sierpi::CStackAccessorMethodsValidationEdgeData<CCluster_TreeNode_> > cCluster_ExchangeEdgeCommData_Validation;
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	// this traversal is used to setup the element data vertex coordinates during the initial setup
	sierpi::kernels::CSetup_CellData_Validation::TRAV cSetup_CellData_Validation;
#endif

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
	// only computed explicit euler updates are stored to this element data sets
	/**
	 * see phd thesis, Martin Schreiber 2013, 2.13.2. Explicit higher-order RK
	 */
	sierpi::CStack<CHyperbolicTypes::CSimulationTypes::CCellData> cStackCellDataRK_D[SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER];

	// second order Euler updates
	sierpi::CStack<CHyperbolicTypes::CSimulationTypes::CCellData> cStackCellDataRK_V_0;
#endif

	/*
	 * fast accessor for triangle factory
	 */
	sierpi::CTriangle_Factory &cTriangleFactory;

	/**
	 * stored cfl condition values after edge communication!
	 *
	 * This is used in the ongoing adaptive step to finally update the cfl to it's accurate number
	 */
	CHyperbolicTypes::CSimulationTypes::T local_timestep_size;

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	sierpi::CMigrationAtomic<CHyperbolicTypes::CSimulationTypes::T> migration_local_timestep_size;
#endif

	/**
	 * local timestamp
	 */
//	CHyperbolicTypes::CSimulationTypes::T local_timestamp_for_current_timestep;


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	/**
	 * migrate simulation cluster
	 */
	inline void migration_send(int i_dst_rank)
	{
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		// only save local timestep
		sierpi::CMigration::sendAtomic(local_timestep_size, i_dst_rank);
//		sierpi::CMigration::sendAtomic(local_timestamp_for_current_timestep, i_dst_rank);
#else
		migration_local_timestep_size.send(local_timestep_size, i_dst_rank);
#endif

		cEdgeCommTraversal.migration_send(i_dst_rank);
		cAdaptiveTraversal.migration_send(i_dst_rank);
		cSetupColumnTraversal.migration_send(i_dst_rank);

//		cCluster_ExchangeEdgeCommData_Adaptivity
//		cCluster_ExchangeFluxCommData

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
//		cCluster_ExchangeVertexDataCommData_WaterSurface
//		cCluster_ExchangeVertexDataCommData_Terrain
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
//		cEdgeComm_ValidateComm
//		cCluster_ExchangeEdgeCommData_Validation
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		// this traversal is used to setup the element data vertex coordinates during the initial setup
//		sierpi::kernels::CSetup_CellData_Validation::TRAV cSetup_CellData_Validation;
#endif
	}


	inline void migration_send_postprocessing(
			int i_dst_rank
	) {
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION

		migration_local_timestep_size.wait();

		cEdgeCommTraversal.migration_send_postprocessing(i_dst_rank);
		cAdaptiveTraversal.migration_send_postprocessing(i_dst_rank);
		cSetupColumnTraversal.migration_send_postprocessing(i_dst_rank);


//		cCluster_ExchangeEdgeCommData_Adaptivity
//		cCluster_ExchangeFluxCommData

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
//		cCluster_ExchangeVertexDataCommData_WaterSurface
//		cCluster_ExchangeVertexDataCommData_Terrain
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
//		cEdgeComm_ValidateComm
//		cCluster_ExchangeEdgeCommData_Validation
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		// this traversal is used to setup the element data vertex coordinates during the initial setup
//		sierpi::kernels::CSetup_CellData_Validation::TRAV cSetup_CellData_Validation;
#endif


#endif
	}

	/**
	 * setup simulation cluster due to migration
	 */
	inline void migration_recv(int i_src_rank)
	{
		// only save local timestep
		sierpi::CMigration::recvAtomic(local_timestep_size, i_src_rank);
//		sierpi::CMigration::recvAtomic(local_timestamp_for_current_timestep, i_src_rank);

		cEdgeCommTraversal.migration_recv(i_src_rank);
		cAdaptiveTraversal.migration_recv(i_src_rank);
		cSetupColumnTraversal.migration_recv(i_src_rank);

//		cCluster_ExchangeEdgeCommData_Adaptivity
//		cCluster_ExchangeFluxCommData

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
//		cCluster_ExchangeVertexDataCommData_WaterSurface
//		cCluster_ExchangeVertexDataCommData_Terrain
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
//		cEdgeComm_ValidateComm
//		cCluster_ExchangeEdgeCommData_Validation
#endif


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		// this traversal is used to setup the element data vertex coordinates during the initial setup
//		sierpi::kernels::CSetup_CellData_Validation::TRAV cSetup_CellData_Validation;
#endif

	}
#endif


	/**
	 * constructor for setup
	 *
	 * this constructor is called whenever a split or merge operation is executed.
	 */
	CSimulationHyperbolic_Cluster(
			CCluster_TreeNode_ *i_cCluster_TreeNode,				///< pointer to cluster tree node of this cluster
			CSimulationHyperbolic_Cluster *i_cluster_parameterSetup	///< cluster handler to setup parameters
		)	:
		cCluster_TreeNode(i_cCluster_TreeNode),
		cCluster_ExchangeEdgeCommData_Adaptivity(i_cCluster_TreeNode),
		cCluster_ExchangeFluxCommData(i_cCluster_TreeNode, cEdgeCommTraversal.cKernelClass),

#if CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS
		cCluster_ExchangeVertexDataCommData_2Pass_1stPass(i_cCluster_TreeNode, nullptr),
		cCluster_ExchangeVertexDataCommData_2Pass_2ndPass(i_cCluster_TreeNode, nullptr),
#elif CONFIG_SIERPI_ENABLE_GUI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cCluster_ExchangeVertexDataCommData(i_cCluster_TreeNode, nullptr),
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cCluster_ExchangeEdgeCommData_Validation(i_cCluster_TreeNode),
#endif
		cTriangleFactory(i_cCluster_TreeNode->cTriangleFactory)
	{
		setup(i_cluster_parameterSetup);
	}



	/**
	 * setup method to initialize traversal methods and parameters
	 */
	void setup(
			CSimulationHyperbolic_Cluster *i_cluster_parameterSetup	///< cluster handler to setup parameters
		)
	{
		if (i_cluster_parameterSetup == nullptr)
		{
			/*
			 * A) this is either a domain root sub-cluster and its parameters
			 * are set-up at another place.
			 *
			 * B) or this is a cluster migrated from another mpi node with the setup
			 * achieved via migration recv's
			 */
			cSetupColumnTraversal.setup_sfcMethods(cTriangleFactory);

			cEdgeCommTraversal.setup_sfcMethods(cTriangleFactory);
			cAdaptiveTraversal.setup_sfcMethods(cTriangleFactory);
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			cEdgeComm_ValidateComm.setup_sfcMethods(cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
			cSetup_CellData_Validation.setup_sfcMethods(cTriangleFactory);
#endif

			local_timestep_size = 0;
//			local_timestamp_for_current_timestep = 0;

			return;
		}


		/**
		 * SETUP traversals and parameters based on another node
		 */
		cSetupColumnTraversal.setup_Cluster(i_cluster_parameterSetup->cSetupColumnTraversal, cTriangleFactory);

		cEdgeCommTraversal.setup_Cluster(i_cluster_parameterSetup->cEdgeCommTraversal, cTriangleFactory);

		cAdaptiveTraversal.setup_Cluster(i_cluster_parameterSetup->cAdaptiveTraversal, cTriangleFactory);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cEdgeComm_ValidateComm.setup_Cluster(i_cluster_parameterSetup->cEdgeComm_ValidateComm, cTriangleFactory);
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		cSetup_CellData_Validation.setup_Cluster(i_cluster_parameterSetup->cSetup_CellData_Validation, cTriangleFactory);
#endif

		local_timestep_size = i_cluster_parameterSetup->local_timestep_size;
//		local_timestamp_for_current_timestep = i_cluster_parameterSetup->local_timestamp_for_current_timestep;
	}

};



#endif
