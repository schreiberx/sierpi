/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_FILE_OUTPUT_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_FILE_OUTPUT_HPP_


#include "global_config.h"

#include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "../hyperbolic_common/subsimulation_generic/kernels/backends/CGetNodeDataSample.hpp"

#include "CSimulationHyperbolic_Cluster.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"

#include "lib/CVtkXMLTrianglePolyData.hpp"

#include "CSimulation_MainInterface_FileOutput.hpp"

#include "libsierpi/CGridDataArrays.hpp"
#include "mainthreading/CMainThreading_Locks.hpp"

#if CONFIG_BENCHMARK_SPHERE
#include "simulations/hyperbolic_common/subsimulation_benchmark_sphere/tsunami_benchmarks/CBenchmarkErrors.hpp"
#endif


#define FILE_OUTPUT_WITH_WRITER_TASK	(CONFIG_ENABLE_WRITER_TASK && (COMPILE_WITH_ITBB || CONFIG_THREADING_TBB))

#if CONFIG_WRITER_THREAD_WITH_PTHREAD
#	include <pthread.h>
#endif


class CSimulationHyperbolic_Parallel_FileOutput	:
	public CSimulation_MainInterface_FileOutput
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	/**
	 * typedefs for domain clusters
	 */
	typedef sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> CDomainClusters_;

#if FILE_OUTPUT_WITH_WRITER_TASK
	CMainThreading_Lock tasked_output_lock;
#endif


	CGridDataArrays<3,
#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
	12
#else
	6
#endif
	> cGridDataArrays;


	/**
	 * writer task to write grid data in background
	 */
	class CWriterTask
#if FILE_OUTPUT_WITH_WRITER_TASK
		:
		public tbb::task
#endif
	{
		CSimulationHyperbolic_Parallel_FileOutput &cSimulationHyperbolic_Parallel_FileOutput;

		CGridDataArrays<3,
#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
	12
#else
	6
#endif
	> &cGridDataArrays;
		CParameters &cParameters;

	public:
		CWriterTask(
				CSimulationHyperbolic_Parallel_FileOutput &i_cSimulationHyperbolic_Parallel_FileOutput
		) :
			cSimulationHyperbolic_Parallel_FileOutput(i_cSimulationHyperbolic_Parallel_FileOutput),
			cGridDataArrays(cSimulationHyperbolic_Parallel_FileOutput.cGridDataArrays),
			cParameters(cSimulationHyperbolic_Parallel_FileOutput.cParameters)
		{
		}



		/**
		 * starting point for execution
		 */
#if FILE_OUTPUT_WITH_WRITER_TASK
		tbb::task*
#else
		void *
#endif
		execute()
		{
			if (cParameters.verbosity_level > 9)
				std::cout << " SIMULATION DATA: WRITER TASK start" << std::endl;

			/*
			 * post-processing
			 */
			if (cParameters.verbosity_level > 9)
				std::cout << " SIMULATION DATA: postprocessing" << std::endl;

			if (	cParameters.visualization_scale_x != 1.0 ||
					cParameters.visualization_scale_y != 1.0 ||
					cParameters.visualization_scale_z != 1.0 ||

					cParameters.visualization_translate_x != 1.0 ||
					cParameters.visualization_translate_y != 1.0 ||
					cParameters.visualization_translate_z != 1.0
			) {
				for (int i = 0; i < cGridDataArrays.number_of_triangle_cells; i++)
				{
					T *v = &(cGridDataArrays.triangle_vertex_buffer[3*3*i]);

					for (int vn = 0; vn < 3; vn++)
					{
						v[3*vn+0] = (v[3*vn+0]-cParameters.simulation_dataset_default_domain_translate_x + cParameters.visualization_translate_x)*cParameters.visualization_scale_x + cParameters.simulation_dataset_default_domain_translate_x;
						v[3*vn+1] = (v[3*vn+1]-cParameters.simulation_dataset_default_domain_translate_y + cParameters.visualization_translate_y)*cParameters.visualization_scale_y + cParameters.simulation_dataset_default_domain_translate_y;
						v[3*vn+2] = (v[3*vn+2] + cParameters.visualization_translate_y)*cParameters.visualization_scale_z;
					}
				}
			}

#if !CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
			/*
			 * VTK Writer
			 */
			if (cParameters.verbosity_level > 9)
				std::cout << " SIMULATION DATA: output to file" << std::endl;

			{
				CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

					cVtkXMLTrianglePolyData.setup();
					cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
					cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.dof_element[0]);
#if !CONFIG_BENCHMARK_SPHERE
					cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.dof_element[1]);
					cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.dof_element[2]);
					cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.dof_element[3]);

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
					cVtkXMLTrianglePolyData.setCellDataFloat("eigen_coefficient", cGridDataArrays.dof_element[4]);
#else
					cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.dof_element[4]);
#endif
#endif

				cVtkXMLTrianglePolyData.write(cSimulationHyperbolic_Parallel_FileOutput.output_filename.c_str());

				if (cParameters.verbosity_level > 9)
					std::cout << " SIMULATION DATA: vtk write finished" << std::endl;

				// CVtkXMLTrianglePolyData is deallocated here due to the scope {}
			}
#endif

			if (cParameters.verbosity_level > 9)
				std::cout << " SIMULATION DATA: WRITER TASK end" << std::endl;

#if FILE_OUTPUT_WITH_WRITER_TASK
			// deactivate lock if finished
			cSimulationHyperbolic_Parallel_FileOutput.tasked_output_lock.unlock();
#endif

			return nullptr;
		}

	};



public:
	CParameters &cParameters;
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;
	CDatasets &cDatasets;

	std::string output_filename;
	std::string information_string;

#if CONFIG_WRITER_THREAD_WITH_PTHREAD
	pthread_t pthread_id;
	CWriterTask *cWriterTask;
#endif


	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationHyperbolic_Parallel_FileOutput(
			CParameters &i_cParameters,
			CDomainClusters_ &i_cDomainClusters,
			CDatasets &i_cDatasets
	)	:
			cParameters(i_cParameters),
			cDomainClusters(i_cDomainClusters),
			cDatasets(i_cDatasets)
#if CONFIG_WRITER_THREAD_WITH_PTHREAD
			,
			pthread_id(0),
			cWriterTask(nullptr)
#endif
	{
	}


#if CONFIG_WRITER_THREAD_WITH_PTHREAD
	static void *start_pthread(void *i_user_data)
	{
		CWriterTask *cWriterTask = (CWriterTask*)i_user_data;

		cWriterTask->execute();

		return 0;
	}
#endif



	virtual ~CSimulationHyperbolic_Parallel_FileOutput()
	{
#if CONFIG_WRITER_THREAD_WITH_PTHREAD

		void *retval;
		if (cWriterTask)
		{
			if (cParameters.verbosity_level > 9)
				std::cout << "	SIMULATION_DATA: pthread join..." << std::flush;

			pthread_join(pthread_id, &retval);

			if (cParameters.verbosity_level > 9)
				std::cout << " OK" << std::endl;

			delete cWriterTask;
			cWriterTask = nullptr;
		}

#elif FILE_OUTPUT_WITH_WRITER_TASK

		tasked_output_lock.lock();
		tasked_output_lock.unlock();

#endif
	}


/***************************************************************************************
 * OUTPUT CURRENT TRIANGULATION TO VTK FILE
 ***************************************************************************************/
public:
	/**
	 * \brief write simulation data to file.
	 *
	 * If additional information can be included in the file, also write i_information_string to the file.
	 *
	 * The output is
	 */
	void writeSimulationDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		// wait for lock to write data to buffer
#if FILE_OUTPUT_WITH_WRITER_TASK

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: WRITER_TASK: waiting for output lock..." << std::flush;

		tasked_output_lock.lock();

		if (cParameters.verbosity_level > 9)
			std::cout << "	OK" << std::endl;

#endif

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: setting up output data" << std::endl;

		int flags =
				CGridDataArrays_Enums::VERTICES		|
#if !CONFIG_SPHERICAL_DISTORTED_GRID_MODE
				CGridDataArrays_Enums::VALUE0		|
				CGridDataArrays_Enums::VALUE1		|
				CGridDataArrays_Enums::VALUE2		|
				CGridDataArrays_Enums::VALUE3		|
				CGridDataArrays_Enums::VALUE4;
#else
				CGridDataArrays_Enums::VALUE0
				| CGridDataArrays_Enums::VALUE1
				| CGridDataArrays_Enums::VALUE2
				| CGridDataArrays_Enums::VALUE3
				| CGridDataArrays_Enums::VALUE4
#if CONFIG_BENCHMARK_SPHERE
				| CGridDataArrays_Enums::VALUE5
				| CGridDataArrays_Enums::VALUE6
#endif
				;

		if (cParameters.benchmark_output_vtk_grid_type == 1)
			flags = flags | CGridDataArrays_Enums::VERTICES3D;
		if (cParameters.benchmark_output_vtk_grid_type == 2)
			flags = flags | CGridDataArrays_Enums::VERTICESLL;
#endif

		cGridDataArrays.reset(cParameters.number_of_local_cells, 1, 1, flags);

#if SIMULATION_MULTILAYER_ENABLED
		int i_layer = cParameters.visualization_multi_layer_id;
#endif

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = cGridDataArrays.getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

					cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cOutputGridDataArrays.cKernelClass.setup(
							&cGridDataArrays,
							nullptr,
							offset,
							flags,
							0
#if SIMULATION_MULTILAYER_ENABLED
							,
							i_layer
#endif
						);

					cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);

		assert(cGridDataArrays.number_of_triangle_cells == cParameters.number_of_local_cells);

		output_filename = i_filename;

		if (i_information_string == nullptr)
			information_string = "";
		else
			information_string = i_information_string;


#if CONFIG_WRITER_THREAD_WITH_PTHREAD

		void *retval;

		if (cWriterTask)
		{
			if (cParameters.verbosity_level > 9)
				std::cout << "	SIMULATION_DATA: pthread join..." << std::flush;

			pthread_join(pthread_id, &retval);

			if (cParameters.verbosity_level > 9)
				std::cout << " OK" << std::endl;

			delete cWriterTask;
		}

		cWriterTask = new CWriterTask(*this);

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: pthread create" << std::endl;

		pthread_create(&pthread_id, 0, start_pthread, cWriterTask);

#elif FILE_OUTPUT_WITH_WRITER_TASK

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: starting" << std::endl;

		// task is freed by TBB
		tbb::task *cWriterTask = new(tbb::task::allocate_root()) CWriterTask(*this);
		tbb::task::enqueue(*cWriterTask);

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: starting in background successful" << std::endl;

#else

		if (cParameters.verbosity_level > 9)
			std::cout << "	SIMULATION_DATA: execution of writer task " << std::endl;

		CWriterTask cWriterTask(*this);
		cWriterTask.execute();

#endif
	}



	/**
	 * output clusters to vtk file
	 */
	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		CONFIG_DEFAULT_FLOATING_POINT_TYPE *cluster_vertex_buffer = new CONFIG_DEFAULT_FLOATING_POINT_TYPE[cParameters.number_of_local_clusters*3*3];
		size_t local_cluster_offset = 0;

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t offset = local_cluster_offset;
					local_cluster_offset++;

					CONFIG_DEFAULT_FLOATING_POINT_TYPE *v = &(cluster_vertex_buffer[offset*3*3]);

					v[0*3+0] = node->cTriangleFactory.vertices[0][0];
					v[0*3+1] = node->cTriangleFactory.vertices[0][1];
					v[0*3+2] = 0;

					v[1*3+0] = node->cTriangleFactory.vertices[1][0];
					v[1*3+1] = node->cTriangleFactory.vertices[1][1];
					v[1*3+2] = 0;

					v[2*3+0] = node->cTriangleFactory.vertices[2][0];
					v[2*3+1] = node->cTriangleFactory.vertices[2][1];
					v[2*3+2] = 0;
				}
		);


		assert(local_cluster_offset == (size_t)cParameters.number_of_local_clusters);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;
			cVtkXMLTrianglePolyData.setup();
			cVtkXMLTrianglePolyData.setTriangleVertexCoords(cluster_vertex_buffer, cParameters.number_of_local_clusters);
			delete cluster_vertex_buffer;

#if CONFIG_ENABLE_SCAN_DATA
			int *cluster_id_buffer = new int[cParameters.number_of_local_clusters];
			size_t local_cluster_info_offset = 0;

			cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = local_cluster_info_offset;
					local_cluster_info_offset++;

//					assert(offset < cParameters.number_of_local_clusters);
					cluster_id_buffer[offset] = i_cGenericTreeNode->workload_thread_id;
				}
			);

			assert(local_cluster_info_offset == (size_t)cParameters.number_of_local_clusters);

			cVtkXMLTrianglePolyData.setCellDataInt("cluster_id", cluster_id_buffer);
			delete cluster_id_buffer;
#endif


		cVtkXMLTrianglePolyData.write(i_filename);
	}
};


#endif
