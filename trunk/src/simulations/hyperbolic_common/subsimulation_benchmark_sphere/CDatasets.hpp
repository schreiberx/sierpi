/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mar 16, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMISIMULATION_DATASETS_HPP_
#define CTSUNAMISIMULATION_DATASETS_HPP_

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cassert>

// only include when compiled with sierpi
#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CParameters.hpp"
#	include "../datasets_common/CParameters_Datasets.hpp"
#endif

#if CONFIG_ENABLE_ASAGI
#	include "../datasets_common/CAsagi.hpp"
#endif

#if CONFIG_ENABLE_NETCDF
#	include "../datasets_common/CSimpleNetCDF.hpp"
#endif

#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
#	include "../datasets_common/CSimpleNetCDF_cubedSphere.hpp"
#endif


#if CONFIG_ENABLE_SWE_BENCHMARKS
#	include "tsunami_benchmarks/CSingleWaveOnSimpleBeach.hpp"
#	include "tsunami_benchmarks/CSingleWaveOnCompositeBeach.hpp"
#	include "tsunami_benchmarks/CParabolicCup.hpp"
# include "tsunami_benchmarks/CDamBreak.hpp"
# include "tsunami_benchmarks/CDamBreakWetDry.hpp"
#endif

#include "../datasets_common/CDataSet_Interface.hpp"

#include "libmath/CMatrix.hpp"
#include "libmath/CVector.hpp"


class CDatasets	:
	public CHyperbolicTypes::CSimulationTypes	///< import tsunami simulation types
#if 0
	,
	public CDataSet_Interface					///< this class has to provide the same dataset interface as each dataset
#endif
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:

	/**
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use simple netcdf
		 */
		SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE = -3,

		/*
		 * use simple netcdf
		 */
		SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF = -2,

		/*
		 * use asagi to determine water surface height
		 */
		SIMULATION_WATER_HEIGHT_ASAGI = -1,

		/*
		 * set simulation water surface height to zero
		 */
		SIMULATION_WATER_HEIGHT_DEFAULT = 0,

		/*
		 * setup water surface height with cylinder parameters (simulation_dataset_cylinder*)
		 */
		SIMULATION_COEFF_CYLINDER = 1,
		SIMULATION_WATER_HEIGHT_LINEAR_SLOPE = 2,
		SIMULATION_WATER_HEIGHT_SQUARE = 3,
		SIMULATION_WATER_HEIGHT_SQUARE_DIST = 4,
		SIMULATION_WATER_HEIGHT_PARABOLA = 5,
		SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY = 6,
		SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF = 7,
		SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK = 8,
		SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 100,
		SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH = 101,
		SIMULATION_WATER_HEIGHT_PARABOLIC_CUP = 104,
		SIMULATION_WATER_HEIGHT_DAM_BREAK= 105,
		SIMULATION_WATER_HEIGHT_DAM_BREAK_WET_DRY= 106,

		SIMULATION_COSINE_BELL = 42,
		SIMULATION_SLOTTED_CYLINDER = 43,
		SIMULATION_TUM = 66,

		SIMULATION_INTERACTIVE_UPDATE = 9
	};


	/**
	 * possible choices to setup terrain distance
	 */
	enum
	{
		SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE = -3,
		SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF = -2,
		SIMULATION_TERRAIN_HEIGHT_ASAGI = -1,

		SIMULATION_TERRAIN_HEIGHT_DEFAULT = 0,

		SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE = 1,
		SIMULATION_TERRAIN_HEIGHT_PARABOLA = 2,
		SIMULATION_TERRAIN_HEIGHT_FANCY = 3,
		SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH = 100,
		SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH = 101,
		SIMULATION_TERRAIN_HEIGHT_PARABOLIC_CUP = 104,
		SIMULATION_TERRAIN_HEIGHT_DAM_BREAK = 105,
		SIMULATION_TERRAIN_HEIGHT_DAM_BREAK_WET_DRY = 106
	};


	/**
	 * simulation parameters for tsunami simulation
	 */
	CParameters_Datasets &cParameters_Datasets;

	/**
	 * ASAGI dataset interface
	 */
#if CONFIG_ENABLE_ASAGI
	CAsagi cAsagi;
#endif


#if CONFIG_ENABLE_NETCDF
	/**
	 * simple netcdf interface
	 */
	CSimpleNetCDF cSimpleNetCDF;
#endif

#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
	/**
	 * simple netcdf interface
	 */
	CSimpleNetCDF_cubedSphere cSimpleNetCDF_cubedSphere;
#endif


#if CONFIG_ENABLE_SWE_BENCHMARKS
	/**
	 * analytical benchmark: single wave on single beach
	 */
	CSingleWaveOnSimpleBeach<T> cSingleWaveOnSimpleBeach;

	/**
	 * analytical benchmark: single wave on composite beach
	 */
	CSingleWaveOnCompositeBeach<T> cSingleWaveOnCompositeBeach;

	/**
	 * analytical benchmark: Parabolic Cup
	 */
	CParabolicCup<T> cParabolicCup;
	
	CDamBreak<T> cDamBreak;

	/**
	 * Analytical benchmark: 1D wet-dry dam break
	 */
	CDamBreakWetDry<T> cDamBreakWetDry;
#endif

	/**
	 * verbosity level
	 */
	int verbosity_level;

	/**
	 * level of detail displacement.
	 *
	 * This value has to be set whenever an initial cluster splitting
	 * is done which requests a higher LOD.
	 *
	 * This value is added to each data sampling request.
	 */
private:
	T level_of_detail_displacement_value;

	bool datasets_loaded;

public:
	/**
	 * constructor
	 */
	CDatasets(
			CParameters_Datasets &i_cParameters_Datasets,
			int i_verbosity_level
	)	:
		cParameters_Datasets(i_cParameters_Datasets),
#if CONFIG_ENABLE_ASAGI
		cAsagi(i_verbosity_level),
#endif
#if CONFIG_ENABLE_NETCDF
		cSimpleNetCDF(i_verbosity_level),
#endif
#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		cSimpleNetCDF_cubedSphere(i_verbosity_level),
#endif
		verbosity_level(i_verbosity_level),
		level_of_detail_displacement_value(0),
		datasets_loaded(false)
	{
	}


	void setLevelOfDetailDisplacementValue(T i_level_of_detail_displacement_value)
	{
		level_of_detail_displacement_value = i_level_of_detail_displacement_value;
	}


	void loadDatasets()
	{
		// avoid loading datasets multiple times in case of resetting the simulation
		if (datasets_loaded)
			return;

		loadDatasets(cParameters_Datasets.simulation_datasets);
	}

	/**
	 * \brief setup datasets
	 *
	 * This is important for the datafile based simulation scenarios
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			if (	i_datasets[0].length() > 0		&&
					i_datasets[1].length() > 0
			)
			{
				cAsagi.loadDatasets(i_datasets, cParameters_Datasets.simulation_dataset_asagi_grid_type_id);

				if (!cAsagi.isDatasetLoaded())
					return false;

				cAsagi.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_translate_x,
						&cParameters_Datasets.simulation_dataset_default_domain_translate_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				datasets_loaded = true;
				return true;
			}
			else
			{
				if (verbosity_level > 5)
					std::cout << "Warning: Datasets for simple ASAGI not loaded!" << std::endl;
			}
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			if (i_datasets[0].length() > 0)
			{
				cSimpleNetCDF.loadDatasets(i_datasets);

				if (!cSimpleNetCDF.isDatasetLoaded())
					return false;

				cSimpleNetCDF.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_translate_x,
						&cParameters_Datasets.simulation_dataset_default_domain_translate_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				datasets_loaded = true;
				return true;
			}
			else
			{
				if (verbosity_level > 5)
					std::cout << "Warning: Datasets for simple NETCDF not loaded!" << std::endl;
			}
			break;
#endif


#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
			if (i_datasets[0].length() > 0)
			{
				cSimpleNetCDF_cubedSphere.loadDatasets(i_datasets);

				if (!cSimpleNetCDF_cubedSphere.isDatasetLoaded())
					return false;

				cSimpleNetCDF_cubedSphere.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_translate_x,
						&cParameters_Datasets.simulation_dataset_default_domain_translate_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				datasets_loaded = true;
				return true;
			}
			else
			{
				if (verbosity_level > 5)
					std::cout << "Warning: Datasets for simple NETCDF not loaded!" << std::endl;
			}
			break;
#endif
		}

		return false;
	}



public:
	/**
	 * deconstructor
	 */
	virtual ~CDatasets()
	{
	}



	inline void fixWindowAlignment(
			T *io_origin_x,	///< origins x-coordinate
			T *io_origin_y,	///< origins y-coordinate
			T *io_size_x,	///< size of terrain in x-dimension
			T *io_size_y		///< size of terrain in y-dimension
	)	{
		T delta_x = 0;
		T delta_y = 0;

		/*
		 * fix window alignment
		 */
		switch(cParameters_Datasets.simulation_dataset_window_alignment)
		{
		case CParameters_Datasets::WINDOW_ALIGNMENT_INNER_QUAD:
			if (*io_size_x < *io_size_y)
			{
				delta_y = *io_size_y - *io_size_x;
				*io_size_y = *io_size_x;
			}
			else
			{
				delta_x = *io_size_x - *io_size_y;
				*io_size_x = *io_size_y;
			}
			break;

		case CParameters_Datasets::WINDOW_ALIGNMENT_OUTER_QUAD:
			if (*io_size_x < *io_size_y)
			{
				delta_x = *io_size_x - *io_size_y;
				*io_size_x = *io_size_y;
			}
			else
			{
				delta_y = *io_size_y - *io_size_x;
				*io_size_y = *io_size_x;
			}
			break;

		case CParameters_Datasets::WINDOW_ALIGNMENT_NONE:
			break;
		}

		*io_origin_x -= delta_x*(T)0.5;
		*io_origin_y -= delta_y*(T)0.5;
	}


public:
	/**
	 * terrain dimensions
	 */
	inline void getOriginAndSize(
			T *o_origin_x,	///< origins x-coordinate
			T *o_origin_y,	///< origins y-coordinate
			T *o_size_x,	///< size of terrain in x-dimension
			T *o_size_y		///< size of terrain in y-dimension
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			cAsagi.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			fixWindowAlignment(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			fixWindowAlignment(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
			cSimpleNetCDF_cubedSphere.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			fixWindowAlignment(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_SWE_BENCHMARKS
		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			cSingleWaveOnSimpleBeach.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			cSingleWaveOnCompositeBeach.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
			
		case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK:
			cDamBreak.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;

		case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK_WET_DRY:
			cDamBreakWetDry.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif


		default:
			*o_origin_x = 0;
			*o_origin_y = 0;

			*o_size_x = cParameters_Datasets.simulation_dataset_default_domain_size_x;
			*o_size_y = cParameters_Datasets.simulation_dataset_default_domain_size_y;
			break;
		}
	}



	inline void getNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	) {
		i_level_of_detail += level_of_detail_displacement_value;

		if (cParameters_Datasets.simulation_dataset_1_id == SIMULATION_INTERACTIVE_UPDATE)
		{

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE

				T point_sphere_x, point_sphere_y, point_sphere_z;
				CCube_To_Sphere_Projection::project2DTo3D(
						i_x, i_y,
						point_sphere_x,
						point_sphere_y,
						point_sphere_z
					);

				T center_sphere_x, center_sphere_y, center_sphere_z;
				CCube_To_Sphere_Projection::project2DTo3D(
						cParameters_Datasets.simulation_dataset_breaking_dam_posx, cParameters_Datasets.simulation_dataset_breaking_dam_posy,
						center_sphere_x,
						center_sphere_y,
						center_sphere_z
					);

				T alpha = std::acos(point_sphere_x * center_sphere_x + point_sphere_y * center_sphere_y + point_sphere_z * center_sphere_z);

				T distance = EARTH_RADIUS_IN_METERS * alpha;

				if (distance < cParameters_Datasets.simulation_dataset_breaking_dam_radius)
					o_cNodeData->h = std::max(o_cNodeData->h, cParameters_Datasets.simulation_dataset_breaking_dam_height-o_cNodeData->b+cParameters_Datasets.simulation_dataset_default_nodal_values[1]);

#else

			T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
			T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry >= cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
				return;

//			T horizon = o_cNodeData->b+o_cNodeData->h;
//			if (horizon < cParameters_Datasets.simulation_dataset_default_nodal_values[1])

			o_cNodeData->h = std::max(
						(T)cParameters_Datasets.simulation_dataset_breaking_dam_height,	// always add at least this height
						std::max(
								o_cNodeData->h,	// do not remove already existing height
								-o_cNodeData->b+cParameters_Datasets.simulation_dataset_breaking_dam_height)
						);

#endif
			return;
		}

		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_SWE_BENCHMARKS
		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH :
			cSingleWaveOnSimpleBeach.getNodalData(i_x, i_y, i_level_of_detail, o_cNodeData);
			break;

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			cSingleWaveOnCompositeBeach.getNodalData(i_x, i_y, i_level_of_detail, o_cNodeData);
			break;

		case SIMULATION_WATER_HEIGHT_PARABOLIC_CUP:
			cParabolicCup.getNodalData(i_x, i_y, i_level_of_detail, o_cNodeData);
			break;
		case SIMULATION_WATER_HEIGHT_DAM_BREAK:
			cDamBreak.getNodalData(i_x, i_y, i_level_of_detail, o_cNodeData);
			break;

		case SIMULATION_WATER_HEIGHT_DAM_BREAK_WET_DRY:
			cDamBreakWetDry.getNodalData(i_x, i_y, i_level_of_detail, o_cNodeData);
			break;
#endif

		default:
			o_cNodeData->b = getDatasetValue(0, i_x, i_y, i_level_of_detail);
			o_cNodeData->h = std::max<T>(getDatasetValue(1, i_x, i_y, i_level_of_detail) - o_cNodeData->b, 0);
			o_cNodeData->hu = 0;
			o_cNodeData->hv = 0;

			break;
		}
	}




	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 *
	 * this version loads the node data by loading the timestamp from the dataset timestamp
	 */
	inline bool getBenchmarkNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		return getBenchmarkNodalData(i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp, o_cNodeData);
	}


	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 */
	inline bool getBenchmarkNodalData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			return cAsagi.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif


#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
			return cSimpleNetCDF_cubedSphere.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif


#if CONFIG_ENABLE_SWE_BENCHMARKS
		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			return cSingleWaveOnSimpleBeach.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			return cSingleWaveOnCompositeBeach.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_PARABOLIC_CUP:
			return cParabolicCup.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_DAM_BREAK:
			return cDamBreak.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_DAM_BREAK_WET_DRY:
			return cDamBreakWetDry.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif

		}
		return false;
	}


public:
	/**
	 * terrain setup
	 *
	 * dataset 0: terrain
	 * dataset 1: displacements
	 */
	inline T getDatasetValue(
			int i_dataset_id,
			T i_x,
			T i_y,
			T i_level_of_detail
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		if (i_dataset_id == 0)
		{
			/*
			 * bathymetry
			 */
			switch(cParameters_Datasets.simulation_dataset_0_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_TERRAIN_HEIGHT_ASAGI:
					return cAsagi.getDatasetValue(0, i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(0, i_x, i_y, i_level_of_detail);
#endif


#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
				case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
					return cSimpleNetCDF_cubedSphere.getDatasetValue(0, i_x, i_y, i_level_of_detail);
#endif

#if CONFIG_ENABLE_SWE_BENCHMARKS
				case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
					return cSingleWaveOnSimpleBeach.getDatasetValue(0, i_x, i_y, i_level_of_detail);
					
				case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
					return cSingleWaveOnCompositeBeach.getDatasetValue(0, i_x,i_y,i_level_of_detail);

				case SIMULATION_TERRAIN_HEIGHT_PARABOLIC_CUP:
					return cParabolicCup.getDatasetValue(0, i_x,i_y,i_level_of_detail);
					
				case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK:
					return cDamBreak.getDatasetValue(0, i_x,i_y,i_level_of_detail);

				case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK_WET_DRY:
					return cDamBreakWetDry.getDatasetValue(0, i_x,i_y,i_level_of_detail);
#endif


				case SIMULATION_TERRAIN_HEIGHT_DEFAULT:
					return -cParameters_Datasets.simulation_dataset_default_nodal_values[0];

				case SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE:
					return (i_x-cParameters_Datasets.simulation_dataset_default_domain_translate_x)/(cParameters_Datasets.simulation_dataset_default_domain_size_x*0.5)*cParameters_Datasets.simulation_dataset_default_nodal_values[0];

				case SIMULATION_TERRAIN_HEIGHT_PARABOLA:
					return -(T)2.0*((T)1.0-std::max(i_x*i_x, i_y*i_y))*cParameters_Datasets.simulation_dataset_default_nodal_values[0] +
							cParameters_Datasets.simulation_dataset_default_nodal_values[0]*(T)0.3;

				case SIMULATION_TERRAIN_HEIGHT_FANCY:
				{
					T d = (T)-1.0;

					d = std::max(d, i_x*i_x*(T)0.9-(T)0.8);

					if (i_x*i_x + i_y*i_y < (T)0.05)
						d = (T)0.15;

					T nx = i_x-(T)1.0;
					d = std::max(d, nx*nx*nx*(T)0.3+nx*nx*(T)1.0+nx+(T)0.29);

					d -= 0.1;
					return d*cParameters_Datasets.simulation_dataset_default_nodal_values[0]*0.01;
				}

				default:
					return -cParameters_Datasets.simulation_dataset_default_nodal_values[0];
			}
		}

		if (i_dataset_id == 1)
		{
			/*
			 * displacements
			 */
			switch(cParameters_Datasets.simulation_dataset_1_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
					return cAsagi.getDatasetValue(1, i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(1, i_x, i_y, i_level_of_detail);
#endif


#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
				case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
					return cSimpleNetCDF_cubedSphere.getDatasetValue(1, i_x, i_y, i_level_of_detail);
#endif

				case SIMULATION_WATER_HEIGHT_DEFAULT:
					return cParameters_Datasets.simulation_dataset_default_nodal_values[1];

				case SIMULATION_COSINE_BELL:
				{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
					T lat, lon;
					CCube_To_Sphere_Projection::project2DToLL(i_x, i_y, &lat, &lon);
					T h_max, r, lat1, lon1, lat2, lon2, b, c;
					T r1, r2, h1, h2;
					h_max = (T)cParameters_Datasets.simulation_dataset_cosine_bell_height;
					r = (T)cParameters_Datasets.simulation_dataset_cosine_bell_radius;
					lat1 = lat2 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_cosine_bell_poslat);
					lon1 = lon2 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_cosine_bell_poslon);
					if(cParameters_Datasets.simulation_dataset_benchmark_mirror==1){
						lat2 = -lat1;
					}else if(cParameters_Datasets.simulation_dataset_benchmark_mirror==2){
						lon2 = -lon1;
					}
					r1 = acos(sin(lat1) * sin(lat) + cos(lat1) * cos(lat) * cos(lon - lon1));
					r2 = acos(sin(lat2) * sin(lat) + cos(lat2) * cos(lat) * cos(lon - lon2));
					h1 = h_max * (T)0.5 * (1 + cos(M_PI * r1 / r));
					h2 = h_max * (T)0.5 * (1 + cos(M_PI * r2 / r));
					b = (T)cParameters_Datasets.simulation_dataset_cosine_bell_background; //0.015;
					c = (T)cParameters_Datasets.simulation_dataset_cosine_bell_coefficient; //0.985;
					if (r1 < r){
						return b + c * h1;
					} else if (r2 < r){
						return b + c * h2;
					}
					return b;
#else
					return 0;
#endif
				}

				case SIMULATION_SLOTTED_CYLINDER:
				{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
					T lat, lon;
					CCube_To_Sphere_Projection::project2DToLL(i_x, i_y, &lat, &lon);
					T r, lat1, lon1, lat2, lon2, b, c;
					T r1, r2, dlat1, dlat2, dlon1, dlon2;
					r = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_radius;
					lat1 = lat2 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_slotted_cylinder_poslat);
					lon1 = lon2 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_slotted_cylinder_poslon);
					if(cParameters_Datasets.simulation_dataset_benchmark_mirror==1){
						lat2 = -lat1;
					}else if(cParameters_Datasets.simulation_dataset_benchmark_mirror==2){
						lon2 = -lon1;
					}
					r1 = acos(sin(lat1) * sin(lat) + cos(lat1) * cos(lat) * cos(lon - lon1));
					r2 = acos(sin(lat2) * sin(lat) + cos(lat2) * cos(lat) * cos(lon - lon2));
					b = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_background;
					c = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_height;
					dlat1 = lat - lat1;
					dlat2 = lat - lat2;
					dlon1 = std::abs(lon - lon1);
					dlon2 = std::abs(lon - lon2);
					dlon1 = dlon1 * cos(lat);
					dlon2 = dlon2 * cos(lat);
					T rdiv6 = r / (T)6;
					T r5div12 = r * (T)5 / (T)12;
					if (	(r1 <= r && dlon1 >= rdiv6) ||
							(r2 <= r && dlon2 >= rdiv6) ||
							(r1 <= r && dlon1 < rdiv6 && dlat1 < - r5div12)
							)
						return c;
					if (	cParameters_Datasets.simulation_dataset_benchmark_mirror &&
							(r2 <= r && dlon2 < rdiv6 && dlat2 > r5div12)
							)
						return c;
					return b;
#else
					T dx, dy, d;
					T cx, cy, r;
					r = cParameters_Datasets.simulation_dataset_breaking_dam_radius;
					cx = cParameters_Datasets.simulation_dataset_breaking_dam_posx;
					cy = cParameters_Datasets.simulation_dataset_breaking_dam_posy;
					dx = std::abs(i_x - cx);
					dy = (i_y - cy);
					d = dx * dx + dy * dy;
					if (
						(d <= r*r && dx >= r/6) ||
						(d <= r*r && dx < r/6 && dy > 5/12*r)
					){
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					}
					return 0;
#endif
				}

				case SIMULATION_TUM:
				{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
					T lat, lon;
					CCube_To_Sphere_Projection::project2DToLL(i_x, i_y, &lat, &lon);
					T r, lat1, lon1, lat2, lon2, b, c;
					T r1, r2, dlat, dlon;
					r = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_radius;
					lat1 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_slotted_cylinder_poslat);
					lon1 = CCube_To_Sphere_Projection::angleDegToRad((T)cParameters_Datasets.simulation_dataset_slotted_cylinder_poslon);

					lat1 = lat1 - 0.25*r;
					lon1 = lon1 - 0.5*r;
					r1 = acos(sin(lat1) * sin(lat) + cos(lat1) * cos(lat) * cos(lon - lon1));
					b = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_background;
					c = (T)cParameters_Datasets.simulation_dataset_slotted_cylinder_height;
					dlat = lat - lat1;
					dlon = lon - lon1;
//					dlon = dlon * cos(lat);
					T rdiv10 = r / (T)10;
					if (dlat <= r / (T)2 && dlon <= r && dlat >= 0 && dlon >= 0){
						if ( dlat >= rdiv10 * (T)4 &&
								( dlon <= rdiv10 * (T)4 || dlon >= rdiv10 * (T)5)
								)
							return c;
						else if ( dlat >= rdiv10 && dlat <= rdiv10 * (T)4 &&
								( (dlon >= rdiv10 && dlon <= rdiv10 * (T)2)
									|| (dlon >= rdiv10 * (T)3 && dlon <= rdiv10 * (T)4)
									|| (dlon >= rdiv10 * (T)5 && dlon <= rdiv10 * (T)6)
									|| (dlon >= rdiv10 * (T)7 && dlon <= rdiv10 * (T)8)
									|| (dlon >= rdiv10 * (T)9 && dlon <= rdiv10 * (T)10)
									)
								)
							return c;
						else if ( dlat <= rdiv10 &&
								( (dlon >= rdiv10 && dlon <= rdiv10 * (T)2)
									|| (dlon >= rdiv10 * (T)3 && dlon <= rdiv10 * (T)6)
									|| (dlon >= rdiv10 * (T)7 && dlon <= rdiv10 * (T)8)
									|| (dlon >= rdiv10 * (T)9 && dlon <= rdiv10 * (T)10)
									)
								)
							return c;
					}
					return b;
#else
					return 0;
#endif
				}

				case SIMULATION_COEFF_CYLINDER:
				{
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE

					T point_sphere_x, point_sphere_y, point_sphere_z;
					CCube_To_Sphere_Projection::project2DTo3D(
							i_x, i_y,
							point_sphere_x,
							point_sphere_y,
							point_sphere_z
						);

					T center_sphere_x, center_sphere_y, center_sphere_z;
					CCube_To_Sphere_Projection::project2DTo3D(
							cParameters_Datasets.simulation_dataset_breaking_dam_posx, cParameters_Datasets.simulation_dataset_breaking_dam_posy,
							center_sphere_x,
							center_sphere_y,
							center_sphere_z
						);

//					std::cout << center_sphere_x << ", " << center_sphere_y << ", " << center_sphere_z << std::endl;
					T a = cParameters_Datasets.simulation_dataset_benchmark_rotation_angle_z;
					T b = cParameters_Datasets.simulation_dataset_benchmark_rotation_angle_x;
					if(b){
						b = b / (T)180 * (T)M_PI;
						CMatrix3<T> r_x;
						r_x.genRotation(b, CVector<3, T>((T)1, (T)0, (T) 0));
						CVector<3, T> center2 = r_x * CVector<3, T>(center_sphere_x, center_sphere_y, center_sphere_z);
						center_sphere_x = center2[0]; center_sphere_y = center2[1]; center_sphere_z = center2[2];
					}
					if(a){
						a = a / (T)180 * (T)M_PI;
						CMatrix3<T> r_z;
						r_z.genRotation(a, CVector<3, T>((T)0, (T)0, (T) 1));
						CVector<3, T> center = r_z * CVector<3, T>(center_sphere_x, center_sphere_y, center_sphere_z);
						center_sphere_x = center[0]; center_sphere_y = center[1]; center_sphere_z = center[2];
					}

					T alpha = std::acos(point_sphere_x * center_sphere_x + point_sphere_y * center_sphere_y + point_sphere_z * center_sphere_z);

					T distance = EARTH_RADIUS_IN_METERS * alpha;

					if (distance < cParameters_Datasets.simulation_dataset_breaking_dam_radius){
//						std::cout << distance << std::endl;
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					} else
						return 0;

#else
					CHyperbolicTypes::CSimulationTypes::T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
					CHyperbolicTypes::CSimulationTypes::T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

					if (rx*rx + ry*ry < cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					else
						return 0;
#endif
				}

				case SIMULATION_WATER_HEIGHT_LINEAR_SLOPE:
					return (i_x-cParameters_Datasets.simulation_dataset_default_domain_translate_x)/(cParameters_Datasets.simulation_dataset_default_domain_size_x*0.5)*cParameters_Datasets.simulation_dataset_default_nodal_values[0];

				case SIMULATION_WATER_HEIGHT_SQUARE:
				{
					if (i_x > -1.0+1.0/std::pow(2.0, 2.0)+0.0001)
						return 0;
					return cParameters_Datasets.simulation_dataset_breaking_dam_height;
				}

				case SIMULATION_WATER_HEIGHT_SQUARE_DIST:
				{
					if (i_x < -1.0+1.0/std::pow(2.0, 4.0)+0.0001 || i_x > -1.0+1.0/std::pow(2.0, 2.0)+0.0001)
						return 0;
					return cParameters_Datasets.simulation_dataset_breaking_dam_height;
				}

				case SIMULATION_WATER_HEIGHT_PARABOLA:
					return 1.0-std::max(i_x*i_x, i_y*i_y);


					/*
					 * setup a cylinder relative to bathymetry if bathymetry is higher than 0
					 */
				case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
				{
					CHyperbolicTypes::CSimulationTypes::T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
					CHyperbolicTypes::CSimulationTypes::T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

					if (rx*rx + ry*ry > cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
						return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method

					return cParameters_Datasets.simulation_dataset_breaking_dam_height;
				}

				case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF:
				{
					CHyperbolicTypes::CSimulationTypes::T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
					CHyperbolicTypes::CSimulationTypes::T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

					if (rx*rx + ry*ry < cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					else
						return -99999999.0;	// return huge negative value since cylinder is set-up by using max() function with currently existing value and return value of this method
				}


				case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK:
				{
					CHyperbolicTypes::CSimulationTypes::T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
					CHyperbolicTypes::CSimulationTypes::T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

					if (rx*rx + ry*ry < cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					else
						return 0;
				}

#if CONFIG_ENABLE_SWE_BENCHMARKS
				case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
					return cSingleWaveOnSimpleBeach.getDatasetValue(1, i_x, i_y, i_level_of_detail);
				
				case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
					return cSingleWaveOnCompositeBeach.getDatasetValue(1, i_x,i_y,i_level_of_detail);
					
				case SIMULATION_TERRAIN_HEIGHT_PARABOLIC_CUP:
					return cParabolicCup.getDatasetValue(0, i_x,i_y,i_level_of_detail);

				case SIMULATION_WATER_HEIGHT_DAM_BREAK:
					return cDamBreak.getDatasetValue(0, i_x,i_y,i_level_of_detail);

				case SIMULATION_WATER_HEIGHT_DAM_BREAK_WET_DRY:
					return cDamBreakWetDry.getDatasetValue(0, i_x,i_y,i_level_of_detail);
#endif

				default:
					return cParameters_Datasets.simulation_dataset_1_id;
			}
		}

		throw(std::runtime_error("unknown dataset id"));
		return 0;
	}


	/**
	 * get boundary data at given position
	 */
	bool getBoundaryData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		return getBoundaryData(
				i_x,				///< x-coordinate in world-space
				i_y,				///< y-coordinate in world-space
				i_level_of_detail,	///< level of detail (0 = coarsest level)
				cParameters_Datasets.simulation_dataset_benchmark_input_timestamp,		///< timestamp for boundary data
				o_nodal_data		///< nodal data is written to this position
		);
	}

	/**
	 * get boundary data at given position
	 */
	bool getBoundaryData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

#if CONFIG_ENABLE_SWE_BENCHMARKS
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
			case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
				return cSingleWaveOnCompositeBeach.getBoundaryData(i_x,i_y,i_level_of_detail,i_timestamp,o_nodal_data);
				break;
		}
#endif

		//std::cerr << "getBoundaryData not implemented yet" << std::endl;
		assert(false);
		return false;
	}


	/**
	 * return true if the dataset is loaded
	 */
	bool isDatasetLoaded()
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:		// setup with asagi
			return cAsagi.isDatasetLoaded();
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.isDatasetLoaded();
#endif

#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
			return cSimpleNetCDF_cubedSphere.isDatasetLoaded();
#endif

		default:
			return true;
		}
	}


	/**
	 * output verbose information
	 */
	void outputVerboseInformation()
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
			cAsagi.outputVerboseInformation();
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.outputVerboseInformation();
			break;
#endif

#if CONFIG_ENABLE_NETCDF_CUBED_SPHERE
		case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF_CUBED_SPHERE:
			cSimpleNetCDF_cubedSphere.outputVerboseInformation();
			break;
#endif


		case SIMULATION_WATER_HEIGHT_DEFAULT:
			std::cout << "Water setup: default displacement" << std::endl;
			break;

		case SIMULATION_COEFF_CYLINDER:
			std::cout << "Water setup: cylinder" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_LINEAR_SLOPE:
			std::cout << "Water setup: linear slope" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_SQUARE:
			std::cout << "Water setup: square" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_SQUARE_DIST:
			std::cout << "Water setup: square dist" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_PARABOLA:
			std::cout << "Water setup: parabola" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY:
			std::cout << "Water setup: cylinder relative to bathymetry" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK_OUTER_AREA_MINUS_INF:
			std::cout << "Water setup: radial dam break outer area minus inf" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_RADIAL_DAM_BREAK:
			std::cout << "Water setup: radial dam break" << std::endl;
			break;

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
		{
			std::cout << "Water setup: single wave on single beach" << std::endl;
#if CONFIG_ENABLE_SWE_BENCHMARKS
			cSingleWaveOnSimpleBeach.outputVerboseInformation();
#endif
			break;
		}

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
		{
			std::cout << "Water setup: single wave on Composite beach" << std::endl;
#if CONFIG_ENABLE_SWE_BENCHMARKS
			cSingleWaveOnCompositeBeach.outputVerboseInformation();
#endif
			break;
		}


		case SIMULATION_WATER_HEIGHT_PARABOLIC_CUP:
		{
			std::cout << "Water setup: parabolic cup" << std::endl;
#if CONFIG_ENABLE_SWE_BENCHMARKS
			cParabolicCup.outputVerboseInformation();
#endif
			break;
		}
		
		case SIMULATION_WATER_HEIGHT_DAM_BREAK:
		{
			std::cout << "Water setup: dam break" << std::endl;
#if CONFIG_ENABLE_SWE_BENCHMARKS
			cDamBreak.outputVerboseInformation();
#endif
			break;
		}

		case SIMULATION_WATER_HEIGHT_DAM_BREAK_WET_DRY:
		{
			std::cout << "Water setup: dam break wet dry" << std::endl;
#if CONFIG_ENABLE_SWE_BENCHMARKS
			cDamBreakWetDry.outputVerboseInformation();
#endif
			break;
		}

		default:
			std::cout << "Water setup: [unknown]" << std::endl;
			break;
		}


		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_DEFAULT:
			std::cout << "Terrain setup: default" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_LINEAR_SLOPE:
			std::cout << "Terrain setup: linear slope" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_PARABOLA:
			std::cout << "Terrain setup: parabola" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_FANCY:
			std::cout << "Terrain setup: fancy" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			std::cout << "Terrain setup: single wave on single beach" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			std::cout << "Terrain setup: single wave on composite beach" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_PARABOLIC_CUP:
			std::cout << "Terrain setup: parabolic cup" << std::endl;
			break;

		case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK:
			std::cout << "Terrain setup: dam break" << std::endl;
			break;
		}
	}
};



#endif /* CTSUNAMISIMULATION_DATASETS_HPP_ */
