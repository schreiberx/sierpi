/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 12, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOWX_HPP_
#define CDEFORMATIONAL_FLOWX_HPP_


//#include "CSimulation_MainParameters.hpp"
//#include "../../CParameters.hpp"
#include "../CDatasets.hpp"


//#include "CMain.hpp"

//extern CONFIG_DEFAULT_FLOATING_POINT_TYPE simulation_timestamp_for_timestep;



class CGlobal
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	enum EBenchmark_Type {
		E_2D = -1,
		E_DEF_FLOW_CASE1 = 1,
		E_DEF_FLOW_CASE2 = 2,
		E_DEF_FLOW_CASE4 = 4,
		E_DEF_FLOW_CASE5 = 10,
		E_SOLID_BODY_ROTATION = 5,
		E_DEF_FLOW_LON_ONLY = 6
	};

	T simulation_timestamp_for_timestep;
	EBenchmark_Type benchmark_type;
	T benchmark_body_rotation_alpha;
	T benchmark_body_rotation_roundtime;
	CDatasets *cDatasets;

	T l1_n = 0; T l1_d = 0;
	T l2_n = 0; T l2_d = 0;
	T linf_n = 0; T linf_d = 0;
	T cellcount = 0;
	T error_cellarea = 0;
	T error_edgelength = 0;
	T edgelengths_sphere = 0;
	T edgelengths_flat = 0;
	T cellareas_sphere = 0;
	T cellareas_flat = 0;

	void setDatasets(CDatasets *i_cDatasets){
		cDatasets = i_cDatasets;
	}
	CDatasets *getDatasets(){
		return cDatasets;
	}

	void setTS(T ts){
		simulation_timestamp_for_timestep = ts;
	}
	T getTS(){
		return simulation_timestamp_for_timestep;
	}

	void setBenchmarkType(int i_benchmark_type){
		benchmark_type = EBenchmark_Type(i_benchmark_type);
	}
	EBenchmark_Type getBenchmarkType(){
		return benchmark_type;
	}

	void setAlpha(T alpha){
		benchmark_body_rotation_alpha = alpha;
	}
	T getAlpha(){
		return benchmark_body_rotation_alpha;
	}

	void setRoundTime(T time){
		benchmark_body_rotation_roundtime = time;
	}
	T getRoundTime(){
		return benchmark_body_rotation_roundtime;
	}

    static CGlobal& getInstance()
    {
        static CGlobal    instance; // Guaranteed to be destroyed.
                              // Instantiated on first use.
        return instance;
    }
private:
    CGlobal() {};                   // Constructor? (the {} brackets) are needed here.
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singleton appearing.
    CGlobal(CGlobal const&);              // Don't Implement
    void operator=(CGlobal const&); // Don't implement
};

#endif



