/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 29, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOWY_HPP_
#define CDEFORMATIONAL_FLOWY_HPP_


#include "global_config.h"

#include "simulations/hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#include "CDeformationalFlow.hpp"



class CBenchmarkErrors
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;


public:


	template <typename T>
	inline static void compute_error_measures(
			const CCellData *i_cellData,
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,
			T *l1_n, T *l1_d,
			T *l2_n, T *l2_d,
			T *linf_n, T *linf_d
	){
#if CONFIG_BENCHMARK_SPHERE>0
		CGlobal& g = CGlobal::getInstance();
		CDatasets *cDatasets = g.getDatasets();

		T world_coords[2];
		sierpi::CTriangle_PointProjections::referenceToWorld(
			i_vertex_coord_left_x, i_vertex_coord_left_y,
			i_vertex_coord_right_x, i_vertex_coord_right_y,
			i_vertex_coord_top_x, i_vertex_coord_top_y,
#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT
			(T)(0.5),			(T)(0.5),
#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER
			(T)(0.25),			(T)(0.25),
#else
			(T)(1.0/3.0),		(T)(1.0/3.0),
#endif

			&world_coords[0],	&world_coords[1]
		);

		if (g.getBenchmarkType() == CGlobal::E_SOLID_BODY_ROTATION){
			T lat, lon, lat_rotated, lon_rotated, v_lat, u_lon, lat_moved, lon_moved, timestep;
			timestep = g.getTS();
			CCube_To_Sphere_Projection::project2DToLL(world_coords[0], world_coords[1], &lat, &lon);
//			lat = M_PI/(T)3; lon = 0*M_PI/(T)2;
			CDeformationalFlow::rotateLatLon(lat, lon, (T)0, g.getAlpha(), &lat_rotated, &lon_rotated);
//			CDeformationalFlow::compute_latlonVelocity(lat, lon, &v_lat, &u_lon);
			lat_moved = lat_rotated;
			lon_moved = lon_rotated - (T)2 * M_PI * timestep / g.getRoundTime();
			CDeformationalFlow::rotateLatLon(lat_moved, lon_moved, (T)0, -g.getAlpha(), &lat_moved, &lon_moved);
//			lat_moved = lat - v_lat * timestep;
//			lon_moved = lon - u_lon * timestep;
//			T dlat, dlon, dlatm, dlonm, dlatr, dlonr;
//			dlat = CCube_To_Sphere_Projection::angleRadToDeg(lat);
//			dlon = CCube_To_Sphere_Projection::angleRadToDeg(lon);
//			dlatm = CCube_To_Sphere_Projection::angleRadToDeg(lat_moved);
//			dlonm = CCube_To_Sphere_Projection::angleRadToDeg(lon_moved);
//			dlatr = CCube_To_Sphere_Projection::angleRadToDeg(lat_rotated);
//			dlonr = CCube_To_Sphere_Projection::angleRadToDeg(lon_rotated);
//			std::cout << "lat,lon: " << dlat << ", " << dlon << " rotated: " << dlatr << ", " << dlonr << std::endl;
//			std::cout << "lat,lon: " << dlat << ", " << dlon << " moved: " << dlatm << ", " << dlonm << std::endl;
//			std::cout << "lat,lon: " << lat << ", " << lon << " moved: " << lat_moved << ", " << lon_moved << std::endl; //", v,u: " << v_lat << ", " << u_lon << std::endl;
			CCube_To_Sphere_Projection::projectLLTo2D(lat_moved, lon_moved, &world_coords[0], &world_coords[1]);
		}

		T h_T, h_N, I1_d, I1_n, I2_d, I2_n;
		h_T = cDatasets->getDatasetValue(1, world_coords[0], world_coords[1], 0);
		h_N = i_cellData->dofs.h[0];
//		h_N = h;

//		std::cout << h_N << ", " << h_T << std::endl;
		I1_n = std::abs(h_N - h_T) * i_cellData->cellArea;
		I1_d = std::abs(h_T) * i_cellData->cellArea;
//		std::cout << I_n << ", " << I_d << std::endl;
//		g.addErrorL1(I_n, I_d);

		I2_n = (h_N - h_T) * (h_N - h_T) * i_cellData->cellArea;
		I2_d = h_T * h_T * i_cellData->cellArea;

		*l1_n = I1_n;
		*l1_d = I1_d;
		*l2_n = I2_n;
		*l2_d = I2_d;

		*linf_n = std::abs(h_N - h_T);
		*linf_d = std::abs(h_T);
#endif
	}

	/*
	 * compute global errors l1, l2
	 * by summing up cell-local error numerator/denominator integrals
	 */
	template <typename T>
	inline static void compute_global_errors(
			CGridDataArrays<3,
#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
	12
#else
	6
#endif
	> &cGridDataArrays,
			T *o_l1_n, T *o_l1_d,
			T *o_l2_n, T *o_l2_d,
			T *o_linf_n, T *o_linf_d
	){
		T l1_n, l1_d, l2_n, l2_d, linf_n, linf_d;
		l1_n = 0;
		l1_d = 0;
		linf_n = 0;
		linf_d = 0;
		for (int i = 0; i < cGridDataArrays.number_of_triangle_cells; i++){
			l1_n += cGridDataArrays.dof_element[2][i];
			l1_d += cGridDataArrays.dof_element[3][i];
			l2_n += cGridDataArrays.dof_element[4][i];
			l2_d += cGridDataArrays.dof_element[5][i];
			linf_n = std::max(linf_n, cGridDataArrays.dof_element[6][i]);
			linf_d = std::max(linf_d, cGridDataArrays.dof_element[7][i]);
		}
//		T l1 = l1_n / l1_d;
//		T l2 = std::sqrt(l2_n / l2_d);
//		*o_l1 = l1;
//		*o_l2 = l2;
		*o_l1_n = l1_n;
		*o_l1_d = l1_d;
		*o_l2_n = l2_n;
		*o_l2_d = l2_d;
		*o_linf_n = linf_n;
		*o_linf_d = linf_d;
	}

#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
	/*
	 * average values for approximated edge length and cell area values
	 * compared to real spherical values
	 */
	template <typename T>
	inline static void compute_sphere_grid_error(
			CGridDataArrays<3,12> &cGridDataArrays,
			int cell_count,
			T *o_edges_sphere,
			T *o_edges_flat,
			T *o_area_sphere,
			T *o_area_flat,
			T *o_error_edges,
			T *o_error_area
	){
		T e_s, e_f, a_s, a_f, error_e, error_a;
		e_s = 0;
		e_f = 0;
		a_s = 0;
		a_f = 0;
		error_e = 0;
		error_a = 0;
		for (int i = 0; i < cGridDataArrays.number_of_triangle_cells; i++){
			e_s += cGridDataArrays.dof_element[10][i];
			e_f += cGridDataArrays.dof_element[11][i];
			a_s += cGridDataArrays.dof_element[8][i];
			a_f += cGridDataArrays.dof_element[9][i];
			error_e += std::abs(cGridDataArrays.dof_element[11][i] / cGridDataArrays.dof_element[10][i] - (T)1);
			error_a += std::abs(cGridDataArrays.dof_element[9][i] / cGridDataArrays.dof_element[8][i] - (T)1);
		}
		*o_edges_sphere = e_s; // / (T)cell_count / (T)3;
		*o_edges_flat = e_f; // / (T)cell_count / (T)3;
		*o_area_sphere = a_s; // / (T)cell_count;
		*o_area_flat = a_f; // / (T)cell_count;
		*o_error_edges = error_e; // / (T)cell_count / (T)3;
		*o_error_area = error_a; // / (T)cell_count;
	}
#endif

	template <typename T>
	inline static int sgn(T val) {
	    return (T(0) < val) - (val < T(0));
	}
};

#endif



