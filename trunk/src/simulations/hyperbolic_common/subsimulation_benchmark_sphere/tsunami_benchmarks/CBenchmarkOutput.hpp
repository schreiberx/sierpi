/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CBENCHMARK_ERRORS_HPP_
#define CBENCHMARK_ERRORS_HPP_


#include "global_config.h"

#include "../../subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "../../subsimulation_generic/kernels/backends/CGetNodeDataSample.hpp"

#include "../../../hyperbolic_parallel/CSimulationHyperbolic_Cluster.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"

#include "libsierpi/CGridDataArrays.hpp"

#include "CBenchmarkErrors.hpp"
#include "CGlobal.hpp"

#include "libsierpi/parallelization/CGlobalComm.hpp"



class CBenchmarkOutput
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	/**
	 * typedefs for domain clusters
	 */
	typedef sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> CDomainClusters_;

#if FILE_OUTPUT_WITH_WRITER_TASK
	CMainThreading_Lock tasked_output_lock;
#endif


	CGridDataArrays<3,
#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
	12
#else
	6
#endif
	> cGridDataArrays;

public:
	CParameters &cParameters;
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;
	CDatasets &cDatasets;

	std::string output_filename;
	std::string information_string;


	CBenchmarkOutput(
			CParameters &i_cParameters,
			CDomainClusters_ &i_cDomainClusters,
			CDatasets &i_cDatasets
	)	:
			cParameters(i_cParameters),
			cDomainClusters(i_cDomainClusters),
			cDatasets(i_cDatasets)
	{
	}
	virtual ~CBenchmarkOutput()
	{
	}



/***************************************************************************************
 * OUTPUT BENCHMARK DATA
 ***************************************************************************************/
public:
	void benchmark_output(
	)
	{

		int flags =
				CGridDataArrays_Enums::VERTICES		|
				CGridDataArrays_Enums::VALUE0
				| CGridDataArrays_Enums::VALUE2
				| CGridDataArrays_Enums::VALUE3
				| CGridDataArrays_Enums::VALUE4
				| CGridDataArrays_Enums::VALUE5
				| CGridDataArrays_Enums::VALUE6
				| CGridDataArrays_Enums::VALUE7
				| CGridDataArrays_Enums::VALUE8
				| CGridDataArrays_Enums::VALUE9
				| CGridDataArrays_Enums::VALUE10
				| CGridDataArrays_Enums::VALUE11
				| CGridDataArrays_Enums::VERTICES3D
				;

		cGridDataArrays.reset(cParameters.number_of_local_cells, 1, 1, flags);

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					size_t offset = cGridDataArrays.getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

					// We instantiate it right here to avoid any overhead due to split/join operations
					sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

					cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
					cOutputGridDataArrays.cKernelClass.setup(
							&cGridDataArrays,
							nullptr,
							offset,
							flags,
							0
						);

					cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);
				}
		);

#if CONFIG_BENCHMARK_SPHERE_ERROR_NORMS
		/*
		 * compute global errors l1, l2
		 * by summing up cell-local error numerator/denominator integrals
		 */
		CGlobal& g = CGlobal::getInstance();
		T l1_n, l1_d, l2_n, l2_d, cellcount, linf_n, linf_d;
		cellcount = cGridDataArrays.number_of_triangle_cells;
		CBenchmarkErrors::compute_global_errors<T>(cGridDataArrays, &l1_n, &l1_d, &l2_n, &l2_d, &linf_n, &linf_d);
		g.l1_n = sierpi::CGlobalComm::reduceDoubleSum(l1_n);
		g.l1_d = sierpi::CGlobalComm::reduceDoubleSum(l1_d);
		g.l2_n = sierpi::CGlobalComm::reduceDoubleSum(l2_n);
		g.l2_d = sierpi::CGlobalComm::reduceDoubleSum(l2_d);
		g.linf_n = sierpi::CGlobalComm::reduceMax(linf_n);
		g.linf_d = sierpi::CGlobalComm::reduceMax(linf_d);
		g.cellcount = sierpi::CGlobalComm::reduceDoubleSum(cellcount);
		if (sierpi::CGlobalComm::getCommRank() == 0){
			std::cout << "cell count: " << g.cellcount << std::endl;
			std::cout << "global error l1: " << g.l1_n / g.l1_d << std::endl;
			std::cout << "global error l2: " << std::sqrt(g.l2_n / g.l2_d) << std::endl;
			std::cout << "global error linf: " << std::sqrt(g.linf_n / g.linf_d) << std::endl;
		}
#if 0
		T e_s, e_f, a_s, a_f, error_e, error_a;
		CBenchmarkErrors::compute_sphere_grid_error(
				cGridDataArrays, cParameters.number_of_local_cells,
				&e_s, &e_f, &a_s, &a_f, &error_e, &error_a);
		g.error_cellarea = sierpi::CGlobalComm::reduceDoubleSum(error_a);
		g.error_edgelength = sierpi::CGlobalComm::reduceDoubleSum(error_e);
		g.edgelengths_flat = sierpi::CGlobalComm::reduceDoubleSum(e_f);
		g.edgelengths_sphere = sierpi::CGlobalComm::reduceDoubleSum(e_s);
		g.cellareas_flat = sierpi::CGlobalComm::reduceDoubleSum(a_f);
		g.cellareas_sphere = sierpi::CGlobalComm::reduceDoubleSum(a_s);
		if (sierpi::CGlobalComm::getCommRank() == 0){
			std::cout << "average edge length sphere: " << g.edgelengths_sphere / g.cellcount / (T)3 << std::endl;
			std::cout << "average edge length grid: " << g.edgelengths_flat / g.cellcount / (T)3 << std::endl;
			std::cout << "relative error edges: " << (g.edgelengths_flat - g.edgelengths_sphere) / g.edgelengths_sphere << std::endl;
			std::cout << "relative avg error edges: " << g.error_edgelength / g.cellcount / (T)3 << std::endl;
			std::cout << "average cell area sphere: " << g.cellareas_sphere << std::endl;
			std::cout << "average cell area grid: " << g.cellareas_flat << std::endl;
			std::cout << "relative error area: " << (g.cellareas_flat - g.cellareas_sphere) / g.cellareas_sphere << std::endl;
			std::cout << "relative avg error area: " << g.error_cellarea / g.cellcount << std::endl;
		}
#endif
#endif

	}



	/**
	 * output clusters to vtk file
	 */
	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
	}

};


#endif
