/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jun 05, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_GEOCLAW_AUGMENTED_RIEMANN_SIMD_HPP_
#define CFLUXSOLVER_GEOCLAW_AUGMENTED_RIEMANN_SIMD_HPP_



#if CONFIG_DEFAULT_FLOATING_POINT_TYPE_SINGLE
#	define FLOAT32
#elif CONFIG_DEFAULT_FLOATING_POINT_TYPE_DOUBLE
#	define FLOAT64
#endif

#define SUPPRESS_SOLVER_DEBUG_OUTPUT
//#define COUNTFLOPS

#include "geoclaw_augmentedriemann_simd/AugRie_SIMD.hpp"

#define FLUX_SIMD_COMPONENTS	VECTOR_LENGTH

template <typename T>
class CFluxSolver_GeoClaw_AugmentedRiemann_SIMD
{
public:
	solver::AugRie_SIMD fluxSolver;

	CFluxSolver_GeoClaw_AugmentedRiemann_SIMD()	:
		fluxSolver(
				SIMULATION_TSUNAMI_DRY_THRESHOLD,
				SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION,
				0.000001,
				10,
				SIMULATION_TSUNAMI_ZERO_THRESHOLD
			)
	{
	}

public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	inline void op_edge_edge(
			const CSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeData &i_edgeData_right,	///< edge data on right (outer) edge

			CSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,					///< maximum wave speed
			T *o_max_wave_speed_right,					///< maximum wave speed

			T i_gravitational_constant					///< gravitational constant
	)
	{
		fluxSolver.computeNetUpdates(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.hu, i_edgeData_right.hu,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left->h, o_edgeFlux_right->h,
				o_edgeFlux_left->hu, o_edgeFlux_right->hu,
				*o_max_wave_speed_left
		);

		o_edgeFlux_left->hv = 0;
		o_edgeFlux_right->hv = 0;

		*o_max_wave_speed_right = *o_max_wave_speed_left;


#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1
		// add fluxes for higher order basis functions to convert "net-updates" to "flux-updates"
		o_edgeFlux_left->h		+= i_edgeData_left.hu;
		o_edgeFlux_left->hu		+= (i_edgeData_left.hu*i_edgeData_left.hu)/i_edgeData_left.h + (T)0.5*i_gravitational_constant*i_edgeData_left.h*i_edgeData_left.h;
		o_edgeFlux_left->hv		+= (i_edgeData_left.hu*i_edgeData_left.hv)/i_edgeData_left.h;

		o_edgeFlux_right->h		-= i_edgeData_right.hu;
		o_edgeFlux_right->hu	-= (i_edgeData_right.hu*i_edgeData_right.hu)/i_edgeData_right.h + (T)0.5*i_gravitational_constant*i_edgeData_right.h*i_edgeData_right.h;
		o_edgeFlux_right->hv	-= (i_edgeData_right.hu*i_edgeData_right.hv)/i_edgeData_right.h;
#endif
	}


	template <int N>
	void op_edge_edge(
			const CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,			///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T max_wave_speed = 0;

		for (int i = 0; i < N; i++)
		{
			CSimulationNodeData i_left, i_right;

			i_left.h = i_edgeData_left.h[i];
			i_left.hu = i_edgeData_left.hu[i];
			i_left.hv = i_edgeData_left.hv[i];
			i_left.b = i_edgeData_left.b[i];

			i_right.h = i_edgeData_right.h[N-1-i];
			i_right.hu = -i_edgeData_right.hu[N-1-i];
			i_right.hv = -i_edgeData_right.hv[N-1-i];
			i_right.b = i_edgeData_right.b[N-1-i];

			CSimulationNodeData o_left, o_right;

			T o_max_wave_speed;

			fluxSolver.computeNetUpdates(
					i_left.h, i_right.h,
					i_left.hu, i_right.hu,
					i_left.b, i_right.b,

					o_left.h, o_right.h,
					o_left.hu, o_right.hu,
					
					o_max_wave_speed
			);

			o_edgeFlux_left->h[i] = o_left.h;
			o_edgeFlux_left->hu[i] = o_left.hu;
			o_edgeFlux_left->hv[i] = 0;
			o_edgeFlux_left->b[i] = 0;

			o_edgeFlux_right->h[N-1-i] = o_right.h;
			o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
			o_edgeFlux_right->hv[N-1-i] = 0;
			o_edgeFlux_right->b[N-1-i] = 0;

			// determine maximum wave speed across the current edge for which the fluxes are computed
			max_wave_speed = std::max(max_wave_speed, o_max_wave_speed);

			assert(max_wave_speed >= 0);
		}

		*o_max_wave_speed_left = max_wave_speed;
		*o_max_wave_speed_right = max_wave_speed;
	}

	inline void op_edge_edge_simd(
			T *i_left_h,	T *i_right_h,
			T *i_left_hu,	T *i_right_hu,
			T *i_left_b,	T *i_right_b,

			T *o_left_h,	T *o_right_h,
			T *o_left_hu,	T *o_right_hu,

			T &o_wave_speed
	)
	{
		fluxSolver.computeNetUpdates_SIMD(
				i_left_h,	i_right_h,
				i_left_hu,	i_right_hu,
				i_left_b,	i_right_b,

				o_left_h,	o_right_h,
				o_left_hu,	o_right_hu,

				o_wave_speed
		);
	}
};


#endif
