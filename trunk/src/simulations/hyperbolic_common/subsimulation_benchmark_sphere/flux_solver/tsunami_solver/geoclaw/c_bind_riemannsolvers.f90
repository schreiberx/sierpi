module c_bind_riemannsolvers
    implicit none
    contains

    ! input: (h, hu, hv, b)
    ! output: (h, hu, hv, max_wavespeed)

    subroutine c_bind_rpn2( &
            i_eL, i_eR, &
            o_eL, o_eR &
    ) bind(c, name='c_bind_rpn2')

!======================================================================
!    subroutine rpn2(ixy,maxm,meqn,mwaves,maux,mbc,mx,&
!      ql,qr,auxl,auxr,fwave,s,amdq,apdq)
!======================================================================
!
! Solves normal Riemann problems for the 2D SHALLOW WATER equations
!    with topography:
!    #      h_t + (hu)_x + (hv)_y = 0    #
!    #      (hu)_t + (hu^2 + 0.5gh^2)_x + (huv)_y = -ghb_x    #
!    #      (hv)_t + (huv)_x + (hv^2 + 0.5gh^2)_y = -ghb_y    #

! On input, ql contains the state vector at the left edge of each cell
!    qr contains the state vector at the right edge of each cell
!
! This data is along a slice in the x-direction if ixy=1
!    or the y-direction if ixy=2.

!  Note that the i'th Riemann problem has left state qr(i-1,:)
!    and right state ql(i,:)
!  From the basic clawpack routines, this routine is called with
!    ql = qr
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!             !
!    # This Riemann solver is for the shallow water equations.    !
!             !
!    It allows the user to easily select a Riemann solver in    !
!    riemannsolvers_geo.f. this routine initializes all the variables    !
!    for the shallow water equations, accounting for wet dry boundary    !
!    dry cells, wave speeds etc.         !
!             !
!    David George, Vancouver WA, Feb. 2009    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!    use geoclaw_module, only: g => grav, drytol => dry_tolerance
!    use geoclaw_module, only: earth_radius, deg2rad
!    use amr_module, only: mcapa


    implicit none

    ! NEW
    ! input: (h, hu, hv, b)
    double precision, dimension(4)  :: i_eL
    double precision, dimension(4)  :: i_eR
    ! output: (h, hu, hv, max_wavespeed)
    double precision, dimension(4)  :: o_eL
    double precision, dimension(4)  :: o_eR

    !input
    !integer maxm,meqn,maux,mwaves,mbc,mx,ixy

#define mwaves 3


    !local only
    integer m,i,mw,maxiter
    double precision wall(3)
    double precision fw(3,3)
    double precision sw(3)

    double precision sqrt_g_hL, sqrt_g_hR


    double precision hR,hL,huR,huL,uR,uL,hvR,hvL,vR,vL,phiR,phiL
    double precision bR,bL,sL,sR,sRoe1,sRoe2,sE1,sE2,uhat,chat
    double precision s1m,s2m
    double precision hstar,hstartest,hstarHLL,sLtest,sRtest
    double precision tw,dxdc

    logical rare1,rare2

    double precision, parameter :: g = DBLE(SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION)
    double precision, parameter :: drytol = DBLE(SIMULATION_TSUNAMI_DRY_THRESHOLD)


    !loop through Riemann problems at each grid cell
!    do i=2-mbc,mx+mbc



!-----------------------Initializing-----------------------------------
#if DEBUG
  !inform of a bad riemann problem from the start
  if(i_eL(1).lt.0.d0).or.(i_eR(1) .lt. 0.d0)) then
    write(*,*) 'Negative input: hl,hr',i_eL(1),i_eR(1)
  endif
#endif


  hL = i_eL(1)
  huL = i_eL(2)
  hvL = i_eL(3)
  bL = i_eL(4)

  hR = i_eR(1)
  huR = -i_eR(2)
  hvR = -i_eR(3)
  bR = i_eR(4)

  !skip problem if in a completely dry area
  if (hL <= drytol .and. hR <= drytol) then
    o_eL = 0
    o_eR = 0
    return
  endif

  !zero (small) negative values if they exist
  if (hL.lt.0.d0) then
    hL = 0.d0
  endif

  if (hR.lt.0.d0) then
    hR = 0.d0
  endif


  !check for wet/dry boundary
  if (hR.gt.drytol) then
    uR=huR/hR
    vR=hvR/hR
    phiR = 0.5d0*g*hR**2 + huR**2/hR
  else
    hR = 0.d0
    huR = 0.d0
    hvR = 0.d0
    uR = 0.d0
    vR = 0.d0
    phiR = 0.d0
  endif

  if (hL.gt.drytol) then
    uL=huL/hL
    vL=hvL/hL
    phiL = 0.5d0*g*hL**2 + huL**2/hL
  else
    hL=0.d0
    huL=0.d0
    hvL=0.d0
    uL=0.d0
    vL=0.d0
    phiL = 0.d0
  endif

  wall = 1.d0
  if (hR.le.drytol) then
    call riemanntype(hL,hL,uL,-uL,hstar,s1m,s2m,rare1,rare2,1,drytol,g)
    hstartest=max(hL,hstar)
    if (hstartest+bL.lt.bR) then !right state should become ghost values that mirror left for wall problem
!       bR=hstartest+bL
      wall(2)=0.d0
      wall(3)=0.d0
      hR=hL
      huR=-huL
      bR=bL
      phiR=phiL
      uR=-uL
      vR=vL
    elseif (hL+bL.lt.bR) then
      bR=hL+bL
    endif
  elseif (hL.le.drytol) then ! right surface is lower than left topo
    call riemanntype(hR,hR,-uR,uR,hstar,s1m,s2m,rare1,rare2,1,drytol,g)
    hstartest=max(hR,hstar)
    if (hstartest+bR.lt.bL) then  !left state should become ghost values that mirror right
!      bL=hstartest+bR
      wall(1)=0.d0
      wall(2)=0.d0
      hL=hR
      huL=-huR
      bL=bR
      phiL=phiR
      uL=-uR
      vL=vR
    elseif (hR+bR.lt.bL) then
      bL=hR+bR
    endif
  endif

  !determine wave speeds
  sL=uL-sqrt(g*hL) ! 1 wave speed of left state
  sR=uR+sqrt(g*hR) ! 2 wave speed of right state

  uhat=(sqrt(g*hL)*uL + sqrt(g*hR)*uR)/(sqrt(g*hR)+sqrt(g*hL)) ! Roe average

!  sqrt_g_hL = sqrt(g*hL)
!  sqrt_g_hR = sqrt(g*hR)
!
!  !determine wave speeds
!  sL=uL-sqrt_g_hL ! 1 wave speed of left state
!  sR=uR+sqrt_g_hR ! 2 wave speed of right state
!
!  uhat=(sqrt_g_hL*uL + sqrt_g_hR*uR)/(sqrt_g_hR+sqrt_g_hL) ! Roe average
  chat=sqrt(g*0.5d0*(hR+hL)) ! Roe average
  sRoe1=uhat-chat ! Roe wave speed 1 wave
  sRoe2=uhat+chat ! Roe wave speed 2 wave

  sE1 = min(sL,sRoe1) ! Eindfeldt speed 1 wave
  sE2 = max(sR,sRoe2) ! Eindfeldt speed 2 wave

  !--------------------end initializing...finally----------
  !solve Riemann problem.

  maxiter = 1

  sw = 0
  fw = 0

  call riemann_aug_JCP(maxiter,3,3,hL,hR,huL, &
    huR,hvL,hvR,bL,bR,uL,uR,vL,vR,phiL,phiR,sE1,sE2, &
         drytol,g,sw,fw)

!      !eliminate ghost fluxes for wall
  do mw=1,3
    sw(mw)=sw(mw)*wall(mw)
    do i=1,3
      fw(i,mw)=fw(i,mw)*wall(mw)
    enddo
  enddo

  o_eL = 0
  o_eR = 0
  do i=1,3
    do mw=1,3
      if (sw(mw) < 0.d0) then
        o_eL(i) = o_eL(i) + fw(i,mw)
      else if (sw(mw) > 0.d0) then
        o_eR(i) = o_eR(i) + fw(i,mw)
      else
        o_eL(i) = o_eL(i) + 0.5d0 * fw(i,mw)
        o_eR(i) = o_eR(i) + 0.5d0 * fw(i,mw)
      endif
    enddo
  enddo


!        if ((sw(1) < 0.001) .and. (sw(1) > -0.001)) then
!          WRITE(0,*) 1, sw(1)
!        end if
!        if ((sw(2) < 0.001) .and. (sw(2) > -0.001)) then
!          WRITE(0,*) 2, sw(2)
!        end if
!        if ((sw(3) < 0.001) .and. (sw(3) > -0.001)) then
!          WRITE(0,*) 3, sw(mw)
!        end if

  o_eR(2) = -o_eR(2)
  o_eR(3) = -o_eR(3)

  o_eL(4) = abs(sw(1))
  o_eR(4) = abs(sw(3))

  return
end subroutine

end module
