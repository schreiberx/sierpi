#! /bin/bash

PACKAGE_DIRS_AND_FILES_DATASETS="../../datasets_common package/Makefile"
PACKAGE_DIRS_AND_FILES_TSUNAMI="../CDatasets.hpp CHyperbolicTypes.hpp main.cpp Makefile"

PACKAGE_NAME="tsunami_datasets_`date +%Y_%m_%d`"
PACKAGE_DIR="$PACKAGE_NAME"
PACKAGE_TARBALL="$PACKAGE_NAME.tar.bz2"

echo "Creating package $PACKAGE_NAME"
rm -f -r "$PACKAGE_DIR"
mkdir "$PACKAGE_DIR"

mkdir "$PACKAGE_DIR/benchmarks_tsunami"

echo " + copying files: datasets"
for file in $PACKAGE_DIRS_AND_FILES_DATASETS; do
	cp -r "../$file" "$@" "$PACKAGE_DIR"
done
echo " + copying files: tsunami"
for file in $PACKAGE_DIRS_AND_FILES_TSUNAMI; do
	cp -r "../$file" "$@" "$PACKAGE_DIR/benchmarks_tsunami"
done

echo " + removing svn information"
# remove svn from package directory
cd "$PACKAGE_DIR" && { find ./ -name ".svn" | xargs rm -Rf; } && cd ..

echo " + creating tarball $PACKAGE_TARBALL"
rm -f "$PACKAGE_TARBALL"
tar cjf "$PACKAGE_TARBALL" "$PACKAGE_DIR"

echo " + cleaning up"
rm -r "$PACKAGE_DIR"

echo "Done!"
