/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDG_MATRICES_2D_NODAL_ON_THE_FLY_HPP__
#define CDG_MATRICES_2D_NODAL_ON_THE_FLY_HPP__


#include "CDG_MatrixComputations_2D_Nodal_OnTheFly_Defines.hpp"

#include <string.h>
#include "../subsimulation_generic/CConfig.hpp"
//#include "../subsimulation_generic/types/CTypes.hpp"

#include "libsierpi/pde_lab/CGaussQuadrature1D_TriangleEdge.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature2D_TriangleArea.hpp"
#include "libmath/CMatrixOperations.hpp"

#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"

#include "CBasisFunctions2D.hpp"

class CAsdf
{

};



class CDG_MatrixComputations_2D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;
	static const int EN = SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS;

private:
	// mass
	static T timestep_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T timestep_inv_mass_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	// stiffness
	static T timestep_stiffness_u[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T timestep_stiffness_v[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	// edge comm
	static T timestep_dofs_to_face[3][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T timestep_fluxes_to_dofs[3][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS];

	// adaptive
	static T adaptivity_refine_left[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T adaptivity_refine_right[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	static T adaptivity_coarsen_left[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
	static T adaptivity_coarsen_right[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

	typedef sierpi::pdelab::CGaussQuadrature2D_TriangleArea<T,SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER> CGaussQuadratureTriangleArea;
	typedef sierpi::pdelab::CGaussQuadrature1D_TriangleEdge<T,SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER> CGaussQuadratureTriangleEdge;

public:
	static CBasisFunctions2D<SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS, SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE> cBasisFunctions2D;


	static inline int getNumberOfFlops()
	{
		int single_quantity_flops =
				N*N	+	/// mass matrix
				N*N +	/// stiffness
				N*N +	/// stiffness
				EN*N*3		+	// edge comm
				N*EN*3		+	// flux quadrature
				0;

		// multiply with 3 since we have 3 conserved quantities
		single_quantity_flops *= 3;

		// multiply with 2 since we have an add operation per multiplication
		single_quantity_flops *= 2;

		// TODO: flux computations missing

		return single_quantity_flops;
	}

	/*********************************************************
	 * INV MASS
	 *********************************************************/
	static inline void mul_timestep_inv_mass(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += timestep_inv_mass_matrix[i][j]*i_input[j];
		}
	}


	static inline void madd_timestep_inv_mass(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += timestep_inv_mass_matrix[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS U
	 *********************************************************/
	static inline void mul_timestep_stiffness_u(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += timestep_stiffness_u[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * STIFFNESS U
	 *********************************************************/
	static inline void madd_timestep_stiffness_u(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += timestep_stiffness_u[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS V
	 *********************************************************/
	static inline void madd_timestep_stiffness_v(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += timestep_stiffness_v[i][j]*i_input[j];
		}
	}


	/*
	 *
	 */
	static inline void mul_timestep_stiffness_dofs_to_nodes(
		const T i_input[N],
		T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = i_input[i];
		}
	}

	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX HYP
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_hyp_face(
			const T i_input[N],
			T o_output[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= timestep_dofs_to_face[0][i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_hyp_project_to_edge_space(
			T io_hu[EN],
			T io_hv[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,0>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}


	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX RIGHT
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_right_face(
			const T i_input[N],
			T o_output[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= timestep_dofs_to_face[1][i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_right_project_to_edge_space(
			T io_hu[EN],
			T io_hv[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,1>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}




	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX LEFT
	 *********************************************************/

public:
	static inline void mul_timestep_dofs_to_left_face(
			const T i_input[N],
			T o_output[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= timestep_dofs_to_face[2][i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_left_project_to_edge_space(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,2>(
					&(io_hu[i]),
					&(io_hv[i])
			);
		}
	}





	/*********************************************************
	 * FLUX QUADRATURE
	 *********************************************************/

	static inline void madd_timestep_hyp_fluxes_to_dofs(
			const T i_input[EN],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= timestep_fluxes_to_dofs[0][i][j] * i_input[j];
		}
	}


	static inline void madd_timestep_right_fluxes_to_dofs(
			const T i_input[EN],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= timestep_fluxes_to_dofs[1][i][j] * i_input[j];
		}
	}


	static inline void madd_timestep_left_fluxes_to_dofs(
			const T i_input[EN],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= timestep_fluxes_to_dofs[2][i][j] * i_input[j];
		}
	}



	/*********************************************************
	 * ADAPTIVITY REFINEMENT
	 *********************************************************/

public:
	static inline void mul_adaptivity_refine_left(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_refine_left[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_left_child(
			T io_hu[N],
			T io_hv[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);
		}
	}



public:
	static inline void mul_adaptivity_refine_right(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_refine_right[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_right_child(
			T io_hu[N],
			T io_hv[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
			);
		}
	}



	/*********************************************************
	 * ADAPTIVITY COARSENING
	 *********************************************************/

public:
	static inline void mul_adaptivity_coarsen_left(
			const T i_input[N],
			T o_output[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_coarsen_left[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_left_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
			);
		}
	}



public:
	static inline void mul_adaptivity_coarsen_right(
			const T i_input[],
			T o_output[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_coarsen_right[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_right_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
			);
		}
	}



	/**
	 * convert polynomial to DOFs
	 *
	 * TODO: only approximation for 0th order poly
	 */
	static inline void mul_convert_poly_to_dofs(
		const T i_input[N],
		T o_output[N]
	) {
		T t = i_input[0]/(T)N;

		// set other polynomial coefficients to 0
		for (int i = 0; i < N; i++)
			o_output[i] = t;
	}


	/**
	 * convert DOFs to polynomial
	 *
	 * TODO: only approximation for 0th order poly
	 */
	static inline void mul_convert_dofs_to_poly(
		const T i_input[N],
		T o_output[N]
	) {
		// average
		T t = 0;
		for (int i = 0; i < N; i++)
			t += i_input[i];
		t /= (T)N;
		o_output[0] = t;

		// set other polynomial coefficients to 0
		for (int i = 1; i < N; i++)
			o_output[i] = 0;
	}


	/**
	 * convert DOFs to polynomial
	 *
	 * TODO: only approximation for 0th order poly
	 */
	static inline void madd_convert_dofs_to_poly(
		const T i_input[N],
		T o_output[N]
	) {
		// average
		T t = 0;
		for (int i = 0; i < N; i++)
			t += i_input[i];
		t /= (T)N;
		o_output[0] += t;

		// set other polynomial coefficients to 0
		for (int i = 1; i < N; i++)
			o_output[i] = 0;
	}


	/**
	 * convert DOFs to 0th order representation
	 */
	static inline void mul_convert_dofs_to_0th_order(
		const T i_input[N],
		T *o_output
	) {
		// average
		T t = 0;
		for (int i = 0; i < N; i++)
			t += i_input[i];
		t /= (T)N;
		*o_output = t;
	}



	/**
	 * convert DOFs to 0th order representation
	 */
	static inline void madd_convert_dofs_to_0th_order(
		const T i_input[N],
		T *o_output
	) {
		// average
		T t = 0;
		for (int i = 0; i < N; i++)
			t += i_input[i];
		t /= (T)N;
		*o_output += t;
	}


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
	static void setup(int i_verbosity_level)
	{
		/*
		 * setup nodal basis functions by using gauss quadrature points
		 */

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 0

		static T coords[1][2] =
		{
				{1.0/3.0, 1.0/3.0},
		};

		cBasisFunctions2D.setup(&coords[0][0], i_verbosity_level);
/*
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 1

	#if 0
		static T coords[3][2] =
		{
				{0, 0},
				{1, 0},
				{0, 1}
		};
	#else
		static T coords[3][2] =
		{
				{0.5, 0.5},
				{0, 0.5},
				{0.5, 0.0}
		};
	#endif

		cBasisFunctions2D.setup(&coords[0][0], i_verbosity_level);
*/
#else

		cBasisFunctions2D.setup(CGaussQuadratureTriangleArea::getCoords(), i_verbosity_level);

#endif


//		cBasisFunctions2D.setup(CGaussQuadratureTriangleArea::getCoords(), i_verbosity_level);

#if 0
		for (int i = 0; i < 100000; i++)
		{
			std::cout << "." << std::flush;
			static const int N = 10;

			T im[N][N];

			for (int i = 0; i < N; i++)
				for (int j = 0; j < N; j++)
					im[i][j] = ((T)rand()/(T)RAND_MAX);

			T om[N][N];

			T tm[N][N];

			T d = CMatrixOperations::getInverse<T,N>(im, om);

			CMatrixOperations::multiply<T,N>(im, om, tm);

			T tol = 0.0001/std::abs(d);

			for (int i = 0; i < N; i++)
			{
				for (int j = 0; j < N; j++)
				{
					if (i == j)
					{
						if (std::abs(tm[i][i] - 1.0) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
					else
					{
						if (std::abs(tm[i][j]) > tol)
						{
							std::cout << "ERROR " << i << ", " << j << std::endl;
							goto error;
							assert(false);
						}
					}
				}
			}

			continue;

			error:
			std::cout << "TOL: " << tol << std::endl;

			std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			std::cout << "Determinant: " << d << std::endl;
			std::cout << std::endl;

			CMatrixOperations::print<T,N>(im);
			CMatrixOperations::print<T,N>(om);
			CMatrixOperations::print<T,N>(tm);

			goto ok;
		}
		ok:
		exit(-1);
#endif

		T *gauss_2d_weights = CGaussQuadratureTriangleArea::getWeights();
		T *gauss_2d_coords = CGaussQuadratureTriangleArea::getCoords();

		if (i_verbosity_level > 5)
		{
			std::cout << "Using " << cBasisFunctions2D.getNumberOfFunctions() << " basis functions for " << CGaussQuadratureTriangleArea::getNumCoords() << " sampling points" << std::endl;
		}


		/*
		 * MASS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				timestep_mass_matrix[i][j] = 0;

				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					timestep_mass_matrix[i][j] += gauss_2d_weights[k] * 0.5 * (
							cBasisFunctions2D.eval(i, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1]) *
							cBasisFunctions2D.eval(j, gauss_2d_coords[2*k + 0], gauss_2d_coords[2*k + 1])
					);
				}
			}
		}


		T determinant = CMatrixOperations::getInverse<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_mass_matrix, timestep_inv_mass_matrix);

		if (determinant < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
		{
			std::cout << "Determinant " << determinant << " detected for mass matrix" << std::endl;
			std::cout << std::endl;
			std::cout << "Mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_mass_matrix);
			std::cout << "Inverse mass matrix:" << std::endl;
			CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_inv_mass_matrix);
#if 0
			std::cout << "Emergency exit" << std::endl;
			exit(-1);
#endif
		}

#if 0
		/*
		 * cleanup matrices
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			{
				if (std::abs(timestep_inv_mass_matrix[i][j]) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					timestep_inv_mass_matrix[i][j] = 0;
				if (std::abs(timestep_inv_mass_matrix[i][j]-1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					timestep_inv_mass_matrix[i][j] = 1;
				if (std::abs(timestep_inv_mass_matrix[i][j]+1.0) < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
					timestep_inv_mass_matrix[i][j] = -1;
			}
		}
#endif

		//		assert(CGaussQuadratureTriangleArea::getNumCoords() == SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);

		/*
		 * STIFFNESS
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				timestep_stiffness_u[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					timestep_stiffness_u[i][j] += gauss_2d_weights[k] * 0.5 * (
							cBasisFunctions2D.eval_dx(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							cBasisFunctions2D.eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
					);
				}

				timestep_stiffness_v[i][j] = 0;
				for (int k = 0; k < CGaussQuadratureTriangleArea::getNumCoords(); k++)
				{
					timestep_stiffness_v[i][j] += gauss_2d_weights[k] * 0.5 * (
							cBasisFunctions2D.eval_dy(	i, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1]) *
							cBasisFunctions2D.eval(		j, gauss_2d_coords[2*k+0], gauss_2d_coords[2*k+1])
					);
				}
			}
		}

		assert(CGaussQuadratureTriangleEdge::getNumPoints() == SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS);

		/*
		 * Edge comm data nodal weights
		 */
		p_computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),		timestep_dofs_to_face[0]);
		p_computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	timestep_dofs_to_face[1]);
		p_computeEdgeCommDataNodalWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),	timestep_dofs_to_face[2]);

		/*
		 * flux matrices
		 */
		p_computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsHypEdge(),	M_SQRT2,	timestep_fluxes_to_dofs[0]);
		p_computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsRightEdge(),	1.0,		timestep_fluxes_to_dofs[1]);
		p_computeEdgeFluxQuardatureWeights(CGaussQuadratureTriangleEdge::getCoordsLeftEdge(),	1.0,		timestep_fluxes_to_dofs[2]);


		T *cell_data_nodal_points = cBasisFunctions2D.getNodalCoords();

		/*
		 * Adaptivity: left refine matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T child_nodal_points[2];
			sierpi::CTriangle_PointProjections::leftChildToReference(&cell_data_nodal_points[i*2], child_nodal_points);

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				adaptivity_refine_left[i][j] = cBasisFunctions2D.eval(j, child_nodal_points[0], child_nodal_points[1]);

				assert(child_nodal_points[0] >= 0.0);
				assert(child_nodal_points[1] >= 0.0);
				assert(child_nodal_points[0] + child_nodal_points[1] <= 1.0);	// maybe this failes due to machine accuracy
				assert(child_nodal_points[0] >= child_nodal_points[1]);
			}
		}



		/*
		 * Adaptivity: right refine matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T child_nodal_points[2];
			sierpi::CTriangle_PointProjections::rightChildToReference(&cell_data_nodal_points[i*2], child_nodal_points);

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				adaptivity_refine_right[i][j] = cBasisFunctions2D.eval(j, child_nodal_points[0], child_nodal_points[1]);

				assert(child_nodal_points[0] >= 0.0);
				assert(child_nodal_points[1] >= 0.0);
				assert(child_nodal_points[0] + child_nodal_points[1] <= 1.0);	// maybe this failes due to machine accuracy
				assert(child_nodal_points[0] <= child_nodal_points[1]);
			}
		}



		T *ref_coord = cBasisFunctions2D.getNodalCoords();

		/*
		 * Adaptivity: left coarsen matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			assert(ref_coord[0] >= 0.0);
			assert(ref_coord[1] >= 0.0);
			assert(ref_coord[0] + ref_coord[1] <= 1.0);	// maybe this failes due to machine accuracy

			T child_nodal_points[2];
			sierpi::CTriangle_PointProjections::referenceToLeftChild(&ref_coord[i*2], child_nodal_points);

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T scalar;

				// inside left triangle
				if (ref_coord[i*2+0] > ref_coord[i*2+1])
					scalar = 1.0;
				// inside right triangle
				else if (ref_coord[i*2+0] < ref_coord[i*2+1])
					scalar = 0.0;
				// on borderline
				else
					scalar = 0.5;

				adaptivity_coarsen_left[i][j] = cBasisFunctions2D.eval(j, child_nodal_points[0], child_nodal_points[1])*scalar;
			}
		}



		/*
		 * Adaptivity: right coarsen matrix
		 */
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			assert(ref_coord[0] >= 0.0);
			assert(ref_coord[1] >= 0.0);
			assert(ref_coord[0] + ref_coord[1] <= 1.0);	// maybe this failes due to machine accuracy

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				T scalar;

				// inside left triangle
				if (ref_coord[i*2+0] > ref_coord[i*2+1])
					scalar = 0.0;
				// inside right triangle
				else if (ref_coord[i*2+0] < ref_coord[i*2+1])
					scalar = 1.0;
				// on borderline
				else
					scalar = 0.5;

				T child_nodal_points[2];
				sierpi::CTriangle_PointProjections::referenceToRightChild(&ref_coord[i*2], child_nodal_points);

				adaptivity_coarsen_right[i][j] = cBasisFunctions2D.eval(j, child_nodal_points[0], child_nodal_points[1])*scalar;
			}
		}


	}



private:
	static void p_computeEdgeCommDataNodalWeights(
			const T *edge_sampling_coords,
			T o_edge_comm_data_nodal_weight_matrix[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS][SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS]
	)
	{
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; j++)
			{
				o_edge_comm_data_nodal_weight_matrix[i][j] = cBasisFunctions2D.eval(j, edge_sampling_coords[0], edge_sampling_coords[1]);
			}
			edge_sampling_coords += 2;
		}
	}



private:
	static void p_computeEdgeFluxQuardatureWeights(
			const T *i_edge_sampling_coords,
			T i_edge_length,
			T o_edge_flux_quadrature_weights[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS][SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
	)	{
		const T *gauss_1d_weights = CGaussQuadratureTriangleEdge::getWeights();

		// for each DOF on the edge
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			const T *edge_sampling_coords = i_edge_sampling_coords;

			for (int j = 0; j < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; j++)
			{
				T weight = cBasisFunctions2D.eval(i, edge_sampling_coords[0], edge_sampling_coords[1]);

				// multiply weight with gauss quadrature coefficients
				weight *= gauss_1d_weights[j];

				// multiply weight with 0.5 since integration interval is not [-1,1] but [0,1]
				weight *= 0.5;

				// multiply with edge length
				weight *= i_edge_length;

				// store negative value to use MADD operation
				o_edge_flux_quadrature_weights[i][j] = -weight;

				edge_sampling_coords += 2;
			}
		}
	}



public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Integration cell order: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
		std::cout << " + Integration edge order: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;
		std::cout << std::endl;

		std::cout << "timestep_mass_matrix:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_mass_matrix);

		std::cout << "timestep_inv_mass_matrix:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_inv_mass_matrix);

		std::cout << "timestep_stiffness_u:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_stiffness_u);

		std::cout << "timestep_stiffness_v:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(timestep_stiffness_v);

		const char *edge_string[] = {"hyp", "right", "left"};

		for (int e = 0; e < 3; e++)
		{
			std::cout << "timestep_dofs_to_" << edge_string[e] << "_face:" << std::endl;
			CMatrixOperations::printNxM<T,SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&timestep_dofs_to_face[e][0][0]);

			std::cout << "timestep_" << edge_string[e] << "_fluxes_to_dofs (" << e << "):" << std::endl;
			CMatrixOperations::printNxM<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS,SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS>(&timestep_fluxes_to_dofs[e][0][0]);
		}

		std::cout << "adaptivity_refine_left:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&adaptivity_refine_left[0][0]);

		std::cout << "adaptivity_refine_right:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&adaptivity_refine_right[0][0]);

		std::cout << "adaptivity_coarsen_left:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&adaptivity_coarsen_left[0][0]);

		std::cout << "adaptivity_coarsen_right:" << std::endl;
		CMatrixOperations::print<T,SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS>(&adaptivity_coarsen_right[0][0]);

		std::cout << "Flops per cell update using matrix multiplications: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;

	}
};


#endif /* CEDGECOMM_HYPERBOLIC_ORDER_1_MATRICES_HPP_ */
