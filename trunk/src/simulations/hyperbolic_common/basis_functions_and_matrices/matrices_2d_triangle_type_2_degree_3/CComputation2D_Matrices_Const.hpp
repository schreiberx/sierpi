
/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 19, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION2D_MATRICES_CONST_HPP_
#define CCOMPUTATION2D_MATRICES_CONST_HPP_

#include <string.h>
#include <assert.h>

#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"

#define SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS (3)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS (10)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS (10)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS (4)
#define SIMULATION_HYPERBOLIC_CFL	(0.5/(T)(2*3+1))
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS)
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS*2)

class CDG_MatrixComputations_2D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;



public:
        static inline void mul_edge_comm_hyp_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,0>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_right_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,1>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_left_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,2>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }

public:
        static inline void mul_adaptivity_project_momentum_reference_to_left_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_left_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                        );
                }
        }



public:
        static inline void mul_adaptivity_project_momentum_reference_to_right_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_right_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                        );
                }
        }



#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

public:
	static inline void mul_edge_comm_transform_to_edge_space(
		const T i_rotationMatrix[2][2],
		const T i_inverseTransformationMatrix[2][2],
		T io_hu[10],
		T io_hv[10]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			/*
			 * scale and shear from reference space
			 */
			CTriangle_VectorProjections::matrixTransformation<T>(
				i_inverseTransformationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

			/*
			 * rotate edge comm data to edge space
			 */
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace<T>(
				i_rotationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

		}
	}
#endif


	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void mul_timestep_inv_mass(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{200.,-1200.,2100.,-1120.,-1200.,4200.,-3360.,2100.,-3360.,-1120.},
			{-1200.,10800.,-22680.,13440.,5400.,-30240.,30240.,-7560.,20160.,3360.},
			{2100.,-22680.,52920.,-33600.,-7560.,52920.,-60480.,8820.,-30240.,-3360.},
			{-1120.,13440.,-33600.,22400.,3360.,-26880.,33600.,-3360.,13440.,1120.},
			{-1200.,5400.,-7560.,3360.,10800.,-30240.,20160.,-22680.,30240.,13440.},
			{4200.,-30240.,52920.,-26880.,-30240.,141120.,-120960.,52920.,-120960.,-26880.},
			{-3360.,30240.,-60480.,33600.,20160.,-120960.,120960.,-30240.,90720.,13440.},
			{2100.,-7560.,8820.,-3360.,-22680.,52920.,-30240.,52920.,-60480.,-33600.},
			{-3360.,20160.,-30240.,13440.,30240.,-120960.,90720.,-60480.,120960.,33600.},
			{-1120.,3360.,-3360.,1120.,13440.,-26880.,13440.,-33600.,33600.,22400.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void madd_timestep_inv_mass(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{200.,-1200.,2100.,-1120.,-1200.,4200.,-3360.,2100.,-3360.,-1120.},
			{-1200.,10800.,-22680.,13440.,5400.,-30240.,30240.,-7560.,20160.,3360.},
			{2100.,-22680.,52920.,-33600.,-7560.,52920.,-60480.,8820.,-30240.,-3360.},
			{-1120.,13440.,-33600.,22400.,3360.,-26880.,33600.,-3360.,13440.,1120.},
			{-1200.,5400.,-7560.,3360.,10800.,-30240.,20160.,-22680.,30240.,13440.},
			{4200.,-30240.,52920.,-26880.,-30240.,141120.,-120960.,52920.,-120960.,-26880.},
			{-3360.,30240.,-60480.,33600.,20160.,-120960.,120960.,-30240.,90720.,13440.},
			{2100.,-7560.,8820.,-3360.,-22680.,52920.,-30240.,52920.,-60480.,-33600.},
			{-3360.,20160.,-30240.,13440.,30240.,-120960.,90720.,-60480.,120960.,33600.},
			{-1120.,3360.,-3360.,1120.,13440.,-26880.,13440.,-33600.,33600.,22400.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_u(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.2968818307e-2,0.,0.,.2968818308e-2,.4786185948e-2,.7500000000e-1,.4786185949e-2,.3630735908e-1,.3630735915e-1,.3541939907e-2},
			{.2038709991e-2,-.1956835478e-2,-.1956835478e-2,.2038709992e-2,-.1768061335e-2,.3214285714e-1,-.1768061337e-2,.2525008707e-1,.2525008708e-1,.4062675657e-2},
			{.5937636616e-2,.9572371899e-2,.7261471815e-1,.7083879812e-2,0.,.1500000000,.7261471821e-1,0.,.9572371899e-2,.5937636617e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{.6116129973e-2,-.5304184008e-2,.7575026124e-1,.1218802697e-1,-.5870506436e-2,.9642857143e-1,.7575026126e-1,-.5870506436e-2,-.5304184020e-2,.6116129977e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_u(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.2968818307e-2,0.,0.,.2968818308e-2,.4786185948e-2,.7500000000e-1,.4786185949e-2,.3630735908e-1,.3630735915e-1,.3541939907e-2},
			{.2038709991e-2,-.1956835478e-2,-.1956835478e-2,.2038709992e-2,-.1768061335e-2,.3214285714e-1,-.1768061337e-2,.2525008707e-1,.2525008708e-1,.4062675657e-2},
			{.5937636616e-2,.9572371899e-2,.7261471815e-1,.7083879812e-2,0.,.1500000000,.7261471821e-1,0.,.9572371899e-2,.5937636617e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{.6116129973e-2,-.5304184008e-2,.7575026124e-1,.1218802697e-1,-.5870506436e-2,.9642857143e-1,.7575026126e-1,-.5870506436e-2,-.5304184020e-2,.6116129977e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_v(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.5937636616e-2,0.,0.,.5937636617e-2,.9572371896e-2,.1500000000,.9572371899e-2,.7261471817e-1,.7261471830e-1,.7083879812e-2},
			{.6116129973e-2,-.5870506436e-2,-.5870506436e-2,.6116129977e-2,-.5304184007e-2,.9642857143e-1,-.5304184008e-2,.7575026123e-1,.7575026125e-1,.1218802697e-1},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2968818307e-2,.4786185949e-2,.3630735908e-1,.3541939907e-2,0.,.7500000000e-1,.3630735911e-1,0.,.4786185949e-2,.2968818308e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2038709991e-2,-.1768061336e-2,.2525008708e-1,.4062675657e-2,-.1956835478e-2,.3214285714e-1,.2525008709e-1,-.1956835478e-2,-.1768061340e-2,.2038709992e-2},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_v(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.5937636616e-2,0.,0.,.5937636617e-2,.9572371896e-2,.1500000000,.9572371899e-2,.7261471817e-1,.7261471830e-1,.7083879812e-2},
			{.6116129973e-2,-.5870506436e-2,-.5870506436e-2,.6116129977e-2,-.5304184007e-2,.9642857143e-1,-.5304184008e-2,.7575026123e-1,.7575026125e-1,.1218802697e-1},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2968818307e-2,.4786185949e-2,.3630735908e-1,.3541939907e-2,0.,.7500000000e-1,.3630735911e-1,0.,.4786185949e-2,.2968818308e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2038709991e-2,-.1768061336e-2,.2525008708e-1,.4062675657e-2,-.1956835478e-2,.3214285714e-1,.2525008709e-1,-.1956835478e-2,-.1768061340e-2,.2038709992e-2},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_dofs_to_nodes
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_dofs_to_nodes(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2827198835,0.,0.,.7993053257e-1,0.,.2259795086e-1},
			{1.,0.,0.,0.,.7172801165,0.,0.,.5144907654,0.,.3690339962},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,0.,0.,0.,0.,0.,0.},
			{1.,.3333333333,.1111111111,.3703703704e-1,.3333333333,.1111111111,.3703703704e-1,.1111111111,.3703703704e-1,.3703703704e-1},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,.7172801165,.2027893510,.5733258171e-1,.5144907654,.1454567693,.3690339962},
			{1.,.7172801165,.5144907654,.3690339962,0.,0.,0.,0.,0.,0.},
			{1.,.7172801165,.5144907654,.3690339962,.2827198835,.2027893510,.1454567693,.7993053257e-1,.5733258171e-1,.2259795086e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_dofs_to_nodes
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_dofs_to_nodes(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2827198835,0.,0.,.7993053257e-1,0.,.2259795086e-1},
			{1.,0.,0.,0.,.7172801165,0.,0.,.5144907654,0.,.3690339962},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,0.,0.,0.,0.,0.,0.},
			{1.,.3333333333,.1111111111,.3703703704e-1,.3333333333,.1111111111,.3703703704e-1,.1111111111,.3703703704e-1,.3703703704e-1},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,.7172801165,.2027893510,.5733258171e-1,.5144907654,.1454567693,.3690339962},
			{1.,.7172801165,.5144907654,.3690339962,0.,0.,0.,0.,0.,0.},
			{1.,.7172801165,.5144907654,.3690339962,.2827198835,.2027893510,.1454567693,.7993053257e-1,.5733258171e-1,.2259795086e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_hyp_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,.7236067977,.2000000000,.5527864047e-1,.5236067977,.1447213596,.3788854381},
			{1.,.7236067977,.5236067977,.3788854381,.2763932023,.2000000000,.1447213596,.7639320228e-1,.5527864047e-1,.2111456181e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_hyp_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,.7236067977,.2000000000,.5527864047e-1,.5236067977,.1447213596,.3788854381},
			{1.,.7236067977,.5236067977,.3788854381,.2763932023,.2000000000,.1447213596,.7639320228e-1,.5527864047e-1,.2111456181e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_left_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2763932023,0.,0.,.7639320228e-1,0.,.2111456181e-1},
			{1.,0.,0.,0.,.7236067977,0.,0.,.5236067977,0.,.3788854381},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_left_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2763932023,0.,0.,.7639320228e-1,0.,.2111456181e-1},
			{1.,0.,0.,0.,.7236067977,0.,0.,.5236067977,0.,.3788854381},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_right_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.},
			{1.,.7236067977,.5236067977,.3788854381,0.,0.,0.,0.,0.,0.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_right_face(
		const T i_input[10],
		T o_output[4]
	) {
		static const T matrix[4][10] = 
		{
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.},
			{1.,.7236067977,.5236067977,.3788854381,0.,0.,0.,0.,0.,0.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 4; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_hyp_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.1178511302,-.5892556509,-.5892556509,-.1178511302},
			{0.,-.1628662565,-.4263893946,-.1178511302},
			{0.,-.4501512632e-1,-.3085382643,-.1178511302},
			{-.3367175148e-2,-.4912642510e-2,-.2307896179,-.1144839550},
			{-.1178511302,-.4263893945,-.1628662562,0.},
			{0.,-.1178511302,-.1178511302,0.},
			{.3367175148e-2,-.4010248391e-1,-.7774864633e-1,-.3367175148e-2},
			{-.1178511302,-.3085382644,-.4501512606e-1,0.},
			{-.3367175148e-2,-.7774864636e-1,-.4010248375e-1,.3367175148e-2},
			{-.1144839550,-.2307896179,-.4912642314e-2,-.3367175148e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_hyp_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.1178511302,-.5892556509,-.5892556509,-.1178511302},
			{0.,-.1628662565,-.4263893946,-.1178511302},
			{0.,-.4501512632e-1,-.3085382643,-.1178511302},
			{-.3367175148e-2,-.4912642510e-2,-.2307896179,-.1144839550},
			{-.1178511302,-.4263893945,-.1628662562,0.},
			{0.,-.1178511302,-.1178511302,0.},
			{.3367175148e-2,-.4010248391e-1,-.7774864633e-1,-.3367175148e-2},
			{-.1178511302,-.3085382644,-.4501512606e-1,0.},
			{-.3367175148e-2,-.7774864636e-1,-.4010248375e-1,.3367175148e-2},
			{-.1144839550,-.2307896179,-.4912642314e-2,-.3367175148e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_left_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.1151638345,-.3015028324,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.3183050109e-1,-.2181694990,-.8333333333e-1},
			{0.,0.,0.,0.},
			{-.2380952381e-2,-.3473762831e-2,-.1631929039,-.8095238095e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_left_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.1151638345,-.3015028324,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.3183050109e-1,-.2181694990,-.8333333333e-1},
			{0.,0.,0.,0.},
			{-.2380952381e-2,-.3473762831e-2,-.1631929039,-.8095238095e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_right_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{-.8333333333e-1,-.3015028323,-.1151638342,0.},
			{-.8333333333e-1,-.2181694990,-.3183050095e-1,0.},
			{-.8095238095e-1,-.1631929039,-.347376273e-2,-.2380952381e-2},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_right_fluxes_to_dofs(
		const T i_input[4],
		T o_output[10]
	) {
		static const T matrix[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{-.8333333333e-1,-.3015028323,-.1151638342,0.},
			{-.8333333333e-1,-.2181694990,-.3183050095e-1,0.},
			{-.8095238095e-1,-.1631929039,-.347376273e-2,-.2380952381e-2},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 4; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,0.,.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,0.,-.5000000000,-.5000000000,0.,-1.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,.5000000000,.5000000000,.5000000000,1.,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000},
			{0.,.5000000000,0.,0.,-.5000000000,.5000000000,0.,-1.,.5000000000,-1.500000000},
			{0.,0.,-1.,0.,0.,0.,-1.,1.,0.,3.},
			{0.,0.,0.,1.500000000,0.,0.,.5000000000,0.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000,.5000000000,.5000000000,-1.,1.500000000},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,.5000000000,-.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,0.,.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,0.,-.5000000000,-.5000000000,0.,-1.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,.5000000000,.5000000000,.5000000000,1.,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000},
			{0.,.5000000000,0.,0.,-.5000000000,.5000000000,0.,-1.,.5000000000,-1.500000000},
			{0.,0.,-1.,0.,0.,0.,-1.,1.,0.,3.},
			{0.,0.,0.,1.500000000,0.,0.,.5000000000,0.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000,.5000000000,.5000000000,-1.,1.500000000},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,.5000000000,-.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,.5000000000,.5000000000,.5000000000,0.,0.,0.,0.,0.,0.},
			{0.,-.5000000000,-1.,-1.500000000,.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,0.,.5000000000,1.500000000,0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-1.500000000,-.5000000000,-.5000000000,-.5000000000,0.,0.,0.},
			{0.,0.,1.,3.,0.,0.,0.,-1.,-1.,0.},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,.5000000000,1.500000000,0.,.5000000000,1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-1.500000000,0.,0.,-.5000000000,0.,.5000000000,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,.5000000000,.5000000000,.5000000000,0.,0.,0.,0.,0.,0.},
			{0.,-.5000000000,-1.,-1.500000000,.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,0.,.5000000000,1.500000000,0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-1.500000000,-.5000000000,-.5000000000,-.5000000000,0.,0.,0.},
			{0.,0.,1.,3.,0.,0.,0.,-1.,-1.,0.},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,.5000000000,1.500000000,0.,.5000000000,1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-1.500000000,0.,0.,-.5000000000,0.,.5000000000,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,.5000000000,0.,-.1250000000,.5000000000,.1250000000,.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,.1250000000,0.,-.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.5000000000,.7500000000,0.,0.,.2500000000,-.5000000000,-.2500000000,-.7500000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,-.1250000000,0.,.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,.5000000000,0.,-.1250000000,.5000000000,.1250000000,.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,.1250000000,0.,-.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.5000000000,.7500000000,0.,0.,.2500000000,-.5000000000,-.2500000000,-.7500000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,-.1250000000,0.,.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000},
			{0.,.5000000000,.5000000000,.3750000000,-.5000000000,0.,.1250000000,-.5000000000,-.1250000000,-.3750000000},
			{0.,0.,-.5000000000,-.7500000000,0.,0.,-.2500000000,.5000000000,.2500000000,.7500000000},
			{0.,0.,0.,.3750000000,0.,0.,.1250000000,0.,-.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,0.,.1250000000,0.,0.,-.1250000000,0.,.1250000000,-.1250000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000},
			{0.,.5000000000,.5000000000,.3750000000,-.5000000000,0.,.1250000000,-.5000000000,-.1250000000,-.3750000000},
			{0.,0.,-.5000000000,-.7500000000,0.,0.,-.2500000000,.5000000000,.2500000000,.7500000000},
			{0.,0.,0.,.3750000000,0.,0.,.1250000000,0.,-.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,0.,.1250000000,0.,0.,-.1250000000,0.,.1250000000,-.1250000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void mul_convert_poly_to_dofs(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void madd_convert_poly_to_dofs(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_poly(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_poly(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_0th_order(
		const T i_input[10],
		T o_output[1]
	) {
		static const T matrix[1][10] = 
		{
			{1.,.3333333333,.1666666667,.1000000000,.3333333333,.8333333333e-1,.3333333333e-1,.1666666667,.3333333333e-1,.1000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_0th_order(
		const T i_input[10],
		T o_output[1]
	) {
		static const T matrix[1][10] = 
		{
			{1.,.3333333333,.1666666667,.1000000000,.3333333333,.8333333333e-1,.3333333333e-1,.1666666667,.3333333333e-1,.1000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

public:
	static inline int getNumberOfFlops()
	{
		return 3200;
	}


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
public:
	static void setup(int i_verbosity_level)
	{
	}


public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 0" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
//		std::cout << " + Integration cell degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
//		std::cout << " + Integration edge degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;
		std::cout << std::endl;

		std::cout << std::endl;
		std::cout << "timestep_inv_mass:" << std::endl;
		static const T timestep_inv_mass[10][10] = 
		{
			{200.,-1200.,2100.,-1120.,-1200.,4200.,-3360.,2100.,-3360.,-1120.},
			{-1200.,10800.,-22680.,13440.,5400.,-30240.,30240.,-7560.,20160.,3360.},
			{2100.,-22680.,52920.,-33600.,-7560.,52920.,-60480.,8820.,-30240.,-3360.},
			{-1120.,13440.,-33600.,22400.,3360.,-26880.,33600.,-3360.,13440.,1120.},
			{-1200.,5400.,-7560.,3360.,10800.,-30240.,20160.,-22680.,30240.,13440.},
			{4200.,-30240.,52920.,-26880.,-30240.,141120.,-120960.,52920.,-120960.,-26880.},
			{-3360.,30240.,-60480.,33600.,20160.,-120960.,120960.,-30240.,90720.,13440.},
			{2100.,-7560.,8820.,-3360.,-22680.,52920.,-30240.,52920.,-60480.,-33600.},
			{-3360.,20160.,-30240.,13440.,30240.,-120960.,90720.,-60480.,120960.,33600.},
			{-1120.,3360.,-3360.,1120.,13440.,-26880.,13440.,-33600.,33600.,22400.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_inv_mass[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_inv_mass[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_u:" << std::endl;
		static const T timestep_stiffness_u[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.2968818307e-2,0.,0.,.2968818308e-2,.4786185948e-2,.7500000000e-1,.4786185949e-2,.3630735908e-1,.3630735915e-1,.3541939907e-2},
			{.2038709991e-2,-.1956835478e-2,-.1956835478e-2,.2038709992e-2,-.1768061335e-2,.3214285714e-1,-.1768061337e-2,.2525008707e-1,.2525008708e-1,.4062675657e-2},
			{.5937636616e-2,.9572371899e-2,.7261471815e-1,.7083879812e-2,0.,.1500000000,.7261471821e-1,0.,.9572371899e-2,.5937636617e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{.6116129973e-2,-.5304184008e-2,.7575026124e-1,.1218802697e-1,-.5870506436e-2,.9642857143e-1,.7575026126e-1,-.5870506436e-2,-.5304184020e-2,.6116129977e-2}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_u[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_u[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_v:" << std::endl;
		static const T timestep_stiffness_v[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.9479576517e-2,.4109354505e-1,.4109354505e-1,.9479576533e-2,.4109354505e-1,.2250000000,.4109354506e-1,.4109354505e-1,.4109354508e-1,.9479576533e-2},
			{.5937636616e-2,0.,0.,.5937636617e-2,.9572371896e-2,.1500000000,.9572371899e-2,.7261471817e-1,.7261471830e-1,.7083879812e-2},
			{.6116129973e-2,-.5870506436e-2,-.5870506436e-2,.6116129977e-2,-.5304184007e-2,.9642857143e-1,-.5304184008e-2,.7575026123e-1,.7575026125e-1,.1218802697e-1},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2968818307e-2,.4786185949e-2,.3630735908e-1,.3541939907e-2,0.,.7500000000e-1,.3630735911e-1,0.,.4786185949e-2,.2968818308e-2},
			{.2380952381e-2,-.2546189257e-2,.6459860211e-2,-.5207357466e-3,-.2546189255e-2,.4285714286e-1,.1565468383e-1,.6459860218e-2,.1565468384e-1,-.5207357466e-3},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{.2038709991e-2,-.1768061336e-2,.2525008708e-1,.4062675657e-2,-.1956835478e-2,.3214285714e-1,.2525008709e-1,-.1956835478e-2,-.1768061340e-2,.2038709992e-2},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_v[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_v[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_dofs_to_nodes:" << std::endl;
		static const T timestep_stiffness_dofs_to_nodes[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2827198835,0.,0.,.7993053257e-1,0.,.2259795086e-1},
			{1.,0.,0.,0.,.7172801165,0.,0.,.5144907654,0.,.3690339962},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,0.,0.,0.,0.,0.,0.},
			{1.,.3333333333,.1111111111,.3703703704e-1,.3333333333,.1111111111,.3703703704e-1,.1111111111,.3703703704e-1,.3703703704e-1},
			{1.,.2827198835,.7993053257e-1,.2259795086e-1,.7172801165,.2027893510,.5733258171e-1,.5144907654,.1454567693,.3690339962},
			{1.,.7172801165,.5144907654,.3690339962,0.,0.,0.,0.,0.,0.},
			{1.,.7172801165,.5144907654,.3690339962,.2827198835,.2027893510,.1454567693,.7993053257e-1,.5733258171e-1,.2259795086e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_dofs_to_nodes[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_dofs_to_nodes[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_hyp_face:" << std::endl;
		static const T timestep_dofs_to_hyp_face[4][10] = 
		{
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,.7236067977,.2000000000,.5527864047e-1,.5236067977,.1447213596,.3788854381},
			{1.,.7236067977,.5236067977,.3788854381,.2763932023,.2000000000,.1447213596,.7639320228e-1,.5527864047e-1,.2111456181e-1},
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 4; j++)
		{
			std::cout << timestep_dofs_to_hyp_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_hyp_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_left_face:" << std::endl;
		static const T timestep_dofs_to_left_face[4][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,.2763932023,0.,0.,.7639320228e-1,0.,.2111456181e-1},
			{1.,0.,0.,0.,.7236067977,0.,0.,.5236067977,0.,.3788854381},
			{1.,0.,0.,0.,1.,0.,0.,1.,0.,1.}
		};
		for (int j = 0; j < 4; j++)
		{
			std::cout << timestep_dofs_to_left_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_left_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_right_face:" << std::endl;
		static const T timestep_dofs_to_right_face[4][10] = 
		{
			{1.,1.,1.,1.,0.,0.,0.,0.,0.,0.},
			{1.,.7236067977,.5236067977,.3788854381,0.,0.,0.,0.,0.,0.},
			{1.,.2763932023,.7639320228e-1,.2111456181e-1,0.,0.,0.,0.,0.,0.},
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 4; j++)
		{
			std::cout << timestep_dofs_to_right_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_right_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_hyp_fluxes_to_dofs:" << std::endl;
		static const T timestep_hyp_fluxes_to_dofs[10][4] = 
		{
			{-.1178511302,-.5892556509,-.5892556509,-.1178511302},
			{0.,-.1628662565,-.4263893946,-.1178511302},
			{0.,-.4501512632e-1,-.3085382643,-.1178511302},
			{-.3367175148e-2,-.4912642510e-2,-.2307896179,-.1144839550},
			{-.1178511302,-.4263893945,-.1628662562,0.},
			{0.,-.1178511302,-.1178511302,0.},
			{.3367175148e-2,-.4010248391e-1,-.7774864633e-1,-.3367175148e-2},
			{-.1178511302,-.3085382644,-.4501512606e-1,0.},
			{-.3367175148e-2,-.7774864636e-1,-.4010248375e-1,.3367175148e-2},
			{-.1144839550,-.2307896179,-.4912642314e-2,-.3367175148e-2}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_hyp_fluxes_to_dofs[j][0];
			for (int i = 1; i < 4; i++)
			{
				std::cout << ", " << timestep_hyp_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_left_fluxes_to_dofs:" << std::endl;
		static const T timestep_left_fluxes_to_dofs[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.1151638345,-.3015028324,-.8333333333e-1},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,-.3183050109e-1,-.2181694990,-.8333333333e-1},
			{0.,0.,0.,0.},
			{-.2380952381e-2,-.3473762831e-2,-.1631929039,-.8095238095e-1}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_left_fluxes_to_dofs[j][0];
			for (int i = 1; i < 4; i++)
			{
				std::cout << ", " << timestep_left_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_right_fluxes_to_dofs:" << std::endl;
		static const T timestep_right_fluxes_to_dofs[10][4] = 
		{
			{-.8333333333e-1,-.4166666667,-.4166666667,-.8333333333e-1},
			{-.8333333333e-1,-.3015028323,-.1151638342,0.},
			{-.8333333333e-1,-.2181694990,-.3183050095e-1,0.},
			{-.8095238095e-1,-.1631929039,-.347376273e-2,-.2380952381e-2},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.},
			{0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_right_fluxes_to_dofs[j][0];
			for (int i = 1; i < 4; i++)
			{
				std::cout << ", " << timestep_right_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_left:" << std::endl;
		static const T adaptivity_coarsen_left[10][10] = 
		{
			{.5000000000,0.,0.,0.,.5000000000,0.,0.,.5000000000,0.,.5000000000},
			{0.,-.5000000000,0.,0.,-.5000000000,-.5000000000,0.,-1.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,.5000000000,.5000000000,.5000000000,1.,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000},
			{0.,.5000000000,0.,0.,-.5000000000,.5000000000,0.,-1.,.5000000000,-1.500000000},
			{0.,0.,-1.,0.,0.,0.,-1.,1.,0.,3.},
			{0.,0.,0.,1.500000000,0.,0.,.5000000000,0.,-.5000000000,-1.500000000},
			{0.,0.,.5000000000,0.,0.,-.5000000000,.5000000000,.5000000000,-1.,1.500000000},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,.5000000000,-.5000000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_coarsen_left[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_coarsen_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_right:" << std::endl;
		static const T adaptivity_coarsen_right[10][10] = 
		{
			{.5000000000,.5000000000,.5000000000,.5000000000,0.,0.,0.,0.,0.,0.},
			{0.,-.5000000000,-1.,-1.500000000,.5000000000,.5000000000,.5000000000,0.,0.,0.},
			{0.,0.,.5000000000,1.500000000,0.,-.5000000000,-1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,-.5000000000,.5000000000},
			{0.,-.5000000000,-1.,-1.500000000,-.5000000000,-.5000000000,-.5000000000,0.,0.,0.},
			{0.,0.,1.,3.,0.,0.,0.,-1.,-1.,0.},
			{0.,0.,0.,-1.500000000,0.,0.,.5000000000,0.,.5000000000,-1.500000000},
			{0.,0.,.5000000000,1.500000000,0.,.5000000000,1.,.5000000000,.5000000000,0.},
			{0.,0.,0.,-1.500000000,0.,0.,-.5000000000,0.,.5000000000,1.500000000},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-.5000000000,-.5000000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_coarsen_right[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_coarsen_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_left:" << std::endl;
		static const T adaptivity_refine_left[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,.5000000000,0.,-.1250000000,.5000000000,.1250000000,.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,.1250000000,0.,-.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.5000000000,.7500000000,0.,0.,.2500000000,-.5000000000,-.2500000000,-.7500000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,-.1250000000,0.,.1250000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_refine_left[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_refine_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_right:" << std::endl;
		static const T adaptivity_refine_right[10][10] = 
		{
			{1.,.5000000000,.2500000000,.1250000000,.5000000000,.2500000000,.1250000000,.2500000000,.1250000000,.1250000000},
			{0.,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.5000000000,-.3750000000,-.5000000000,-.3750000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,.2500000000,.3750000000,.2500000000,.3750000000,.3750000000},
			{0.,0.,0.,-.1250000000,0.,0.,-.1250000000,0.,-.1250000000,-.1250000000},
			{0.,.5000000000,.5000000000,.3750000000,-.5000000000,0.,.1250000000,-.5000000000,-.1250000000,-.3750000000},
			{0.,0.,-.5000000000,-.7500000000,0.,0.,-.2500000000,.5000000000,.2500000000,.7500000000},
			{0.,0.,0.,.3750000000,0.,0.,.1250000000,0.,-.1250000000,-.3750000000},
			{0.,0.,.2500000000,.3750000000,0.,-.2500000000,-.1250000000,.2500000000,-.1250000000,.3750000000},
			{0.,0.,0.,-.3750000000,0.,0.,.1250000000,0.,.1250000000,-.3750000000},
			{0.,0.,0.,.1250000000,0.,0.,-.1250000000,0.,.1250000000,-.1250000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_refine_right[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_refine_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_poly_to_dofs:" << std::endl;
		static const T convert_poly_to_dofs[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << convert_poly_to_dofs[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_poly_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_poly:" << std::endl;
		static const T convert_dofs_to_poly[10][10] = 
		{
			{1.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,1.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,1.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,1.},
			{0.,1.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,1.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,1.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,1.,0.,0.,0.},
			{0.,0.,0.,1.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << convert_dofs_to_poly[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_dofs_to_poly[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_0th_order:" << std::endl;
		static const T convert_dofs_to_0th_order[1][10] = 
		{
			{1.,.3333333333,.1666666667,.1000000000,.3333333333,.8333333333e-1,.3333333333e-1,.1666666667,.3333333333e-1,.1000000000}
		};
		for (int j = 0; j < 1; j++)
		{
			std::cout << convert_dofs_to_0th_order[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_dofs_to_0th_order[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif
