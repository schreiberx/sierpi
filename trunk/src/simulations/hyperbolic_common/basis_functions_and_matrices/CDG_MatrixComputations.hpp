/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDG_MATRIX_COMPUTATIONS_HPP_
#define CDG_MATRIX_COMPUTATIONS_HPP_

#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DIMENSIONS==2
#	include "CDG_MatrixComputations_2D.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DIMENSIONS==3
#	include "CDG_MatrixComputations_3D.hpp"
#else
#	error "Invalid SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DIMENSIONS"
#endif



#endif /* CComputation2D_Matrices_HPP_ */
