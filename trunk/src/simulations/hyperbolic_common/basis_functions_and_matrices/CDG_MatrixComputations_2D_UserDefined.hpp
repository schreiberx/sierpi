/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDG_MATRICES_2D_USERDEFINED_HPP_
#define CDG_MATRICES_2D_USERDEFINED_HPP_


#include <string.h>


#if SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 0
#	include "userdefined_matrices_2d_id_0/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 1
#	include "userdefined_matrices_2d_id_1/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 2
#	include "userdefined_matrices_2d_id_2/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 3
#	include "userdefined_matrices_2d_id_3/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 4
#	include "userdefined_matrices_2d_id_4/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 5
#	include "userdefined_matrices_2d_id_5/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 6
#	include "userdefined_matrices_2d_id_6/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 7
#	include "userdefined_matrices_2d_id_7/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 8
#	include "userdefined_matrices_2d_id_8/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_USERDEFINED_MATRICES_ID == 9
#	include "userdefined_matrices_2d_id_9/CComputation2D_Matrices_Const.hpp"
#else
#	error "unknown matrices id"
#endif


#endif /* CCOMPUTATION_CUSTOM_MATRICES_HPP_ */
