
#define CONFIG_DEFAULT_FLOATING_POINT_TYPE float

#include "CHyperbolicTypes.hpp"

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../../datasets_common/CParameters_Datasets.hpp"
#else
#	include "../datasets_common/CParameters_Datasets.hpp"
#endif

typedef CParameters_Datasets	CParameters;

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CDatasets.hpp"
#else
#	include "CDatasets.hpp"
#endif


int main(int argc, char *argv[])
{
	int verbosity_level;

	CParameters_Datasets cParameters_Datasets;
	CDatasets cDatasets(cParameters_Datasets, verbosity_level);

	return 1;
}
