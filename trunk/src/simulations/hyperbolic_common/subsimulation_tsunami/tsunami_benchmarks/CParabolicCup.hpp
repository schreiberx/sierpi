/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sept 03, 2012
 *      Author: Alexander Breuer <breuera@in.tum.de>,
 *              Martin Schreiber <martin.schreiber@in.tum.de>,
 *              Aditya Ghantasala <shine.aditya@gmail.com>
 */



#ifndef CPARABOLICCUP_HPP_
#define CPARABOLICCUP_HPP_

#include <cmath>

#if CONFIG_ENABLE_NETCDF
#	include "netcdfcpp.h"
#endif

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CConfig.hpp"
#	include "../types/CTypes.hpp"
#endif

template <typename T>
class CParabolicCup	:
	public CDataSet_Interface
{
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	/*!
	 * Defines a benchmark class for simulating the Oscillations in a parabolic
	 * bathymetry defined by the formula
	 *
	 * 				      x
	 * y = - h0*( 0.5 - (---)^2)
	 *                    a
	 *     \                                              /  -------------------------- +h0/2
	 *      \                                            /				^
	 *        \                                        /				|
	 *         \                                      /					|
	 *           \                                  /					|
	 *            \--------------------------------/  					|
	 *              \                            /						h0
	 *               \                          /						|
	 *                 \                      /							|
	 *                  \ 					 /							|
	 *                    \				   /							|
	 *                     \              /  							|
	 *                       \\        //								|
	 *                         \\    //----->Parabolic Profile			v
	 *                           \\//    ---------------------------------------------- -h0/2
	 *   -a													+a
	 *    |													|
	 *    |----------------------2*a -----------------------|
	 *    |                       							|
	 */

private:
	// #################### VAIRABLES #######################

	T h;
	///< Height of the Parabolic Profile
	T a;
	///< Half width of the Parabola. Care must be taken that half of the given domain size is equal to "a".
	T B;
	///< Initial Slope of the Water in the cup.
	T tau;
	///< Linear Damping coefficient.
	T g;
	///< Gravitational constant (By default it is 9.81)
	T p;
	///< Calculation constant.
	T s;
	///< Calculation constant.

	/**
	 * Method to evaluate the height formula from the reference paper
	 *
	 * @param i_x		- X coordinate
	 * @param i_time 	- time at which height at i_x is needed.
	 * @return 			- Returns the value of height at the given time and x coordinate
	 *
	 */
	T p_evaluateHeightFormula(T i_x, T i_time){

		T l_X,l_Y,l_Z;

		l_X = (a*a*B*B*std::exp(-tau*i_time))/(8*g*g*h);
		l_Y = (tau*tau/4)-(s*s);
		l_Z = std::exp(-tau*i_time/2)/g;

		T l_height = l_X*(-s*tau*std::sin(2*s*i_time) + l_Y*std::cos(2*s*i_time)) - (B*B*std::exp(-tau*i_time))/(4*g)  - l_Z*i_x*(B*s*std::cos(s*i_time) + ((tau*B/2)*std::sin(s*i_time)));

		return l_height;

	}

public:


	/**
	 * Constructor for the Class CParabolic Cup
	 *
	 * @param i_h		- Height of parabola(Optional --> set to 10.0)
	 * @param i_a		- Width of Parabola(Optional --> set to 4.0)
	 * @param i_tau		- Linear Damping constant(Optional --> set to 0.001)
	 * @param i_B		- Initial Slope of water in the Parabola (Optional --> set to 2.0)
	 */
	CParabolicCup(const T i_h = T(10.0), const T i_a = T(4.0), const T i_tau = T(0.1), const T i_B = T(2.5) ){

		setup(i_h,i_a,i_tau,i_B);

	}
	/**
	 * @param i_h		- Height of parabola(Optional --> set to 10.0)
	 * @param i_a		- Width of Parabola(Optional --> set to 4.0)
	 * @param i_tau		- Linear Damping constant(Optional --> set to 0.001)
	 * @param i_B		- Initial Slope of water in the Parabola (Optional --> set to 3.0)
	 */
	void setup(const T i_h = T(10.0), const T i_a = T(4.0), const T i_tau = T(0.1), const T i_B = T(2.5)){

		//Variable Initialization
		h = i_h;
		a = i_a;
		tau = i_tau;
		B = i_B;
		g = 9.81;//TODO Hard coded ... Check !!
		p = std::sqrt(8*g*h/(a*a));
		s = (std::sqrt((p*p)-(tau*tau)))/2;
	}

	/**
	 * Returns the Bathymetry at a given co ordinate
	 *
	 * @param i_x 				- X coordinate
	 * @param i_y 				- Y coordinate
	 * @param i_level_of_detail	- Grid Fineness
	 * @return 					- Returns the Bathymetry at the given X coordinate
	 *
	 */
	T p_getBathymetryData(T i_x, T i_y, T i_level_of_detail){

		T l_bathymetry=0.;

		l_bathymetry = -h*(0.5-std::pow((i_x/a),2));

		return l_bathymetry;
	}


	/**
	 * Returns the water total height at any given location at initial time.
	 *
	 * @param i_x 					- x Coordinate
	 * @param i_y 					- y Coordinate (in 1D case its Zero)
	 * @param i_level_of_detail 	- Grid Fineness
	 */
	T p_getWaterSurfaceData(T i_x, T i_y, T i_level_of_detail){

		T l_elevation =  p_getWaterSurfaceElevation(i_x,i_y,i_level_of_detail);
		T l_bathyMetry =  p_getBathymetryData(i_x,i_y,i_level_of_detail);

		//total Height = elevation - bathymetry inside the parabola
		if((l_elevation - l_bathyMetry)>0)
			return (l_elevation-l_bathyMetry);
		else
			return 0.0;
	}


	/**
	 * Returns the wave velocity at any given location at initial time.
	 *
	 * @param i_x 					- x coordinate
	 * @param i_y 					- y Coordinate (in 1D case its Zero)
	 * @param i_level_of_detail 	- Grid Fineness
	 */
	T p_getVelocity(T i_x,T i_y, T i_level_of_detail){

		return B*std::exp(-tau*0/2)*std::sin(s*0);

	}



	/**
	 * Returns the momentum at any given location at initial time.
	 *
	 * @param i_x					- x coordinate
	 * @param i_y 					- y Coordinate (in 1D case its Zero)
	 * @param i_level_of_detail 	- Grid Fineness
	 *
	 */
	T p_getMomentum(T i_x, T i_y, T i_level_of_detail){

		//Momentum is nothing but velocity multiplied by the height at that point

		return p_getWaterSurfaceElevation(i_x,i_y,i_level_of_detail)*p_getVelocity(i_x,i_y,i_level_of_detail);

	}
	/**
	 * Method provides the initial surface elevation data at given coordinates.
	 *
	 * @param i_x				- X Coordinate
	 * @param i_y 				- Y Coordinate
	 * @param i_level_of_detail - Fineness of Grid
	 * @param o_nodal_data 		- Pointer to the object containing all the data about the node.
	 */
	T p_getWaterSurfaceElevation(T i_x, T i_y, T i_level_of_detail){

		T l_elevation =  p_evaluateHeightFormula(i_x,0.0);
		T l_bathyMetry =  p_getBathymetryData(i_x,i_y,i_level_of_detail);

		// total Height = elevation - bathymetry inside the parabola
		if((l_elevation - l_bathyMetry)>0)
			return ( l_elevation );
		else
			return 0.0;
	}

	/**
	 * This function sets the value for the ghost cell according to the INFLOW boundary condition
	 *          @param  	i_ghostVelocity 	- A reference to the Momentum value on the ghost cell
	 * 			@param		time 				- Simulation global time
	 * 			@param		i_ghostVelocity 	- A reference to the velocity on the ghost cell
	 * 			@param		i_ghostHeight  		- A reference to the total Height on the ghost cell
	 *
	 */

	bool getBoundaryData(T i_x, T i_y, T i_level_of_detail, T i_time, CNodeData *o_nodal_data){

		//Nothing to do here as out let boundary conditions are already implemented in the frame work.
		assert(false);
		return false;
	}

	/**
	 * Method provides the initial data at given coordinates.
	 * @param i_x				- X Coordinate
	 * @param i_y 				- Y Coordinate
	 * @param i_level_of_detail - Fineness of Grid
	 * @param o_nodal_data 		- Pointer to the object containing all the data about the node.
	 */
	void getNodalData(T i_x,T i_y,T i_level_of_detail, CNodeData *o_nodal_data){

		o_nodal_data->h =  p_getWaterSurfaceData(i_x,i_y,i_level_of_detail);
		o_nodal_data->b =  p_getBathymetryData(i_x,i_y,i_level_of_detail);
		o_nodal_data->hu = p_getMomentum(i_x,i_y,i_level_of_detail);
		o_nodal_data->hv = 0; //TODO Hard coded ... Check if its OK.

	}




	/**
	 * Method puts out the description of the current benchmark
	 */
	void outputVerboseInformation(){

	}


	/**
	 * return dataset value
	 * TODO Adapt this method
	 * id 0: bathymetry
	 * id 1: displacements
	 */
	CHyperbolicTypes::CSimulationTypes::T getDatasetValue(
			int i_dataset_id,		///< id of dataset to get value from
			T i_x,					///< x-coordinate in model-space
			T i_y,					///< x-coordinate in model-space
			T i_level_of_detail		///< level of detail (0 = coarsest level)
	)
	{
		if (i_dataset_id == 0)
			return p_getBathymetryData(i_x, i_y, i_level_of_detail);

		if (i_dataset_id == 1)
			return p_getWaterSurfaceElevation(i_x, i_y, i_level_of_detail);

		throw(std::runtime_error("invalid dataset id"));
		return 1;
	}


	///< BenchMarking stuff ...


	/**
	 * Method to provide the Benchmark Data at a given location and time
	 *
	 * @param i_x				- X coordinate
	 * @param i_y				- Y coordinate
	 * @param i_level_of_detail	- Grid Fineness
	 * @param i_time			- Simulation Global time
	 * @param o_nodal_data 		- pointer for the nodal data object.
	 */
	bool getBenchmarkNodalData(T i_x, T i_y, T i_level_of_detail, T i_time, CNodeData *o_nodal_data){


		T l_bathyMetry = p_getBathymetryData(i_x,i_y,i_level_of_detail);

		//Here the surface elevation is calculated at a given point i_x and given time i_time
		T l_elevation = p_evaluateHeightFormula(i_x, i_time);

		if((l_elevation - l_bathyMetry)<=0){

			l_elevation = 0.0;
		}

		o_nodal_data->h = l_elevation;
		return true;
		return true;
	}

	//Destructor
	~CParabolicCup(){

	}




	/**
	 * load the datasets specified via cSimulationParameters
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)	{
		return false;
	}


	/**
	 * return true if the dataset was successfully loaded
	 */
	bool isDatasetLoaded()
	{
		return false;
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T *o_translate_x,	///< origin of domain in world-space
			T *o_translate_y,	///< origin of domain in world-space

			T *o_size_x,	///< size of domain in world-space
			T *o_size_y		///< size of domain in world-space
	)	{
		*o_size_x = 100;
		*o_size_y = 100;

		*o_translate_x = 0;
		*o_translate_y = 0;
	}

};

#endif /* CPARABOLICCUP_HPP_ */
