/** @file
 * This file is part of the Sierpi project.
 *
 * @author Alexander Breuer (breuera AT in.tum.de, http://www5.in.tum.de/wiki/index.php/Dipl.-Math._Alexander_Breuer)
 *
 * @section LICENSE
 * Copyright (c) 2013 Technische Universitaet Muenchen
 * Technische Universitaet Muenchen
 * Department of Informatics
 * Chair of Scientific Computing
 * http://www5.in.tum.de/
 *
 * For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
  *
 * @section DESCRIPTION
 * Two dimensional simulation of a partial dam failure.
 **/



#ifndef CBENCHMARK_DAM_BREAK_HPP
#define CBENCHMARK_DAM_BREAK_HPP

#include <cmath>
#include <cassert>
#include "../../datasets_common/CDataSet_Interface.hpp"

#if CONFIG_ENABLE_NETCDF
#	include "helper_routines/Variable.hpp"
#endif

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CConfig.hpp"
#	include "../types/CTypes.hpp"
#endif


template <typename T>
class CDamBreak	:
	public CDataSet_Interface
{
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	/**
	 * References:
	 * a) TODO
	 *
	 *
	 * Sketch:
	 *  
	 * (0,200)         (100, 200)          (200,200)
	 *    **************************************
	 *    *                *                   *
	 *    *                *                   *
	 *    *             (100,170)              *
	 *    *                  _                 *
	 *    *             ______\                *
	 *    *                  _/                *
	 *    *                                    *
	 *    *             (100,95)               *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    *                *                   *
	 *    ***************************************
	 *  (0,0)           (100,0)               (200,0)
	 */

	//private:
	//! initial water height in the reservoir
	T reservoirHeight;
	
	//! initial water height in the "river"
	T riverHeight;
	
	//! position of the dam wall without failure
	T damWall[2];

public:
	/**
	 * Constructor of the "dam break"-benchmark.
	 */
	CDamBreak() {
	  // initialize benchmark specifications
	  reservoirHeight = 10;
	  riverHeight = 5;
	  damWall[0] = 97.5;
	  damWall[1] = 102.5;
	}


	/**
	 * Set up the parameters of the benchmark.
	 */
	//void setup()	{ }


	/**
	 * Get the bathymetry.
	 *
	 * \return bathymetry at given coordinates.
	 */
	T p_getBathymetryData(
			T i_x,					///< x coordinate
			T i_y,					///< y coordinate
			T i_level_of_detail		///< refinement level
	) {
	   // set up 15m high dam wall
	   if( i_x > damWall[0] && i_x < damWall[1] &&  ( i_y < 95 || i_y > 170  ) ) {
	     return 15;
     }
     
	   // same bathymetry level everywhere
     return 0;
	}

	/**
	 * Get water height.
	 *
	 * @return water height at given coordinates.
	 */
	T p_getWaterHeight(
			const T i_x,				///< x coordinate
			const T i_y,				///< y Coordinate
			const T i_level_of_detail	///< refinement level
	) {
		if( i_x < damWall[0] ) // reservoir
			return reservoirHeight;
	  else if( i_x > damWall[1] ) // river
	    return riverHeight;
	  else if( i_y >= 95 && i_y <= 170) // broken area
	    return riverHeight;
	  
	  // on the wall
	  return 0;
	}



	/**
	 * Get surface elevation.
	 *
	 * @return surface elevation at specific coordinates.
	 */
	/*T p_getWaterSurfaceElevation(
			T i_x,					///< x coordinate
			T i_y,					///< y coordinate
			T i_level_of_detail		///< refinement level
	) {
	  return 0;
	}*/

	/**
	 * Get the initial zero momentum
	 *
	 * @return initial momentum at a given coordinates.
	 */
	T p_getMomentum(
			T i_x,					///< x Coordinate
			T i_y,					///< y Coordinate
			T i_level_of_detail		///< refinement level
	) {
		return 0;
	}


	/**
	 * Prints the specifications of the benchmark.
	 */
	void outputVerboseInformation(){
		std::cout << std::endl;
		std::cout << "reservoirHeight: " << reservoirHeight<< std::endl;
		std::cout << "riverHeight: " << riverHeight << std::endl;
		std::cout << "damWall[0]: " << damWall[0] << std::endl;
		std::cout << "damWall[1]: " << damWall[1] << std::endl;
		std::cout << std::endl;
	}

	/**
	 * Assigns bathymetry, height and momentum.
	 *
	 * @param o_nodal_data vector (b,h,hu,hv)^T at given coordinates.
	 */
	void getNodalData(
			T i_x,                 ///< x coordinate
			T i_y,                 ///< y coordinate
			T i_level_of_detail,   ///< level of detail (0 = coarsest level)
			CNodeData *o_cNodeData ///< pointer for the nodal data object
	){
		o_cNodeData->b = p_getBathymetryData(i_x, i_y, i_level_of_detail);
		o_cNodeData->h = p_getWaterHeight(i_x, i_y, i_level_of_detail);
		o_cNodeData->hu = p_getMomentum(i_x, i_y, i_level_of_detail);
		o_cNodeData->hv = p_getMomentum(i_x, i_y, i_level_of_detail);
	}

	/**
	 * Obtains the boundary cell values
	 */
	bool getBoundaryData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< x-coordinate in world-space
			T i_level_of_detail,		///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CNodeData *o_nodal_data	///< pointer for the nodal data object.
	){
		// nothing to do here as out let boundary conditions are already implemented in the frame work.
		assert(false);
		return false;
	}



	/**
	 * Return dataset value
	 *
	 * id 0: bathymetry
	 * id 1: displacements
	 */
	CHyperbolicTypes::CSimulationTypes::T getDatasetValue(
			int i_dataset_id,		///< id of dataset to get value from
			T i_x,					///< x-coordinate in model-space
			T i_y,					///< x-coordinate in model-space
			T i_level_of_detail		///< level of detail (0 = coarsest level)
	)
	{
		if (i_dataset_id == 0)
			return p_getBathymetryData(i_x, i_y, i_level_of_detail);

		if (i_dataset_id == 1)
			return 0;

		throw(std::runtime_error("invalid dataset id"));
		return 1;
	}
	
		/**
	 * Method to provide the Benchmark Data at a given location and time
	 *
	 * return true to signal that valid benchmark data is available.
	 */
	bool getBenchmarkNodalData(
			T i_x,					///< X coordinate
			T i_y,					///< Y coordinate
			T i_level_of_detail,	///< Grid Fineness
			T i_time,				///< Simulation Global time
			CNodeData *o_nodal_data	///< pointer for the nodal data object.
	)
	{
		o_nodal_data->h = -1;
		o_nodal_data->hu = 0;
		o_nodal_data->hv = 0;
		o_nodal_data->b = -1;

		return true;
	}


	/**
	 * load the datasets specified via cSimulationParameters
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		return false;
	}


	/**
	 * return true if the dataset was successfully loaded
	 */
	bool isDatasetLoaded()
	{
		return false;
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T *o_translate_x,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_translate_y,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_size_x,	///< size of domain in world-space
			T *o_size_y		///< size of domain in world-space
	)
	{
		*o_size_x = 200;
		*o_size_y = 200;

		*o_translate_x = 100;
		*o_translate_y = 100;
	}
};

#endif
