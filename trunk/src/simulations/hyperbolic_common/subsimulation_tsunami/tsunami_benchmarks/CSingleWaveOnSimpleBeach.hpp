/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *      Author: Alexander Breuer <breuera@in.tum.de>,
 *              Martin Schreiber <martin.schreiber@in.tum.de>,
 *              Aditya Ghantasala <shine.aditya@gmail.com>
 */


#ifndef CBENCHMARK_SINGLE_WAVE_ON_SINGLE_BEACH_HPP
#define CBENCHMARK_SINGLE_WAVE_ON_SIMPLE_BEACH_HPP

#include <cmath>
#include <cassert>
#include "../../datasets_common/CDataSet_Interface.hpp"

#if CONFIG_ENABLE_NETCDF
#	include "helper_routines/Variable.hpp"
#endif

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CConfig.hpp"
#	include "../types/CTypes.hpp"
#endif


template <typename T>
class CSingleWaveOnSimpleBeach	:
	public CDataSet_Interface
{
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	/**
	 * References:
	 * a) "Standards, Criteria, and Procedures for NOAA Evaluation of Tsunami Numerical Models"
	 *   page 26ff.
	 *
	 * b) "Validation of GeoClaw" F.I. González, et. al. (2011)
	 *
	 *
	 * Sketch:
	 *  
	 *  
	 *  *
	 *   *                                       ***
	 *    *                                     * | *
	 *     *                                   *  H  *
	 *      ***********************************   |   ******
	 *       *                      |
	 *        *                     |
	 *         *                    d
	 *          *                   |
	 *           *                  |
	 *            ******************************************
	 *      |-X_0-|--------------L----------------|
	 *      |-----------------X_1-----------------|
	 *
	 *
	 *      |---------------------------->
	 *      0                         x
	 */

	//private:
	//dimensional variables
	/// initial max wave height (surface elevation)
	T H;

	/// water height with respect to sea level in the constant bathymetry area.
	T d;
	
	/// used gravity constant.
	T gravity;

	//non-dimensional variables.

  /// slope of the "simple" beach.
  T beachSlope;

	/// ratio: max. surface elevation / water height up to the surface
	T Hd;

	/// midpoint of the initial wave.
	T X1;

	/// toe of the beach.
	T X0;

	T tau;
	T gamma;


#if CONFIG_ENABLE_NETCDF
  /// runtime verification.
	benchMark::CVerify<T> l_benchmarkVerify;
#endif

  /**
   * Computes the areakosinus hyperbolicus of x.
   */ 
	inline T arccosh(const T x)
	{
	  assert(x >= 0);
//		const T s = std::sqrt(x);
		return std::log(x+std::sqrt(x*x-(T)1.0));
	}

  /**
   * Computes the hyperbolic secant of x.
   */
	inline T sech(const T x)
	{
		return (T)1.0/std::cosh(x);
	}

public:
	/**
	 * Constructor of the "single wave on a simple beach"-benchmark.
	 */
	CSingleWaveOnSimpleBeach(
			const T i_H = 0.019,	 ///< initial maximum surface elevation.
			const T i_d = 1.,	     ///< water height with respect to sea level in the constant bathymetry area.
			const T i_gravity = 1. ///< gravity constant.
	) {
		setup( i_H, i_d, i_gravity);
	}


	/**
	 * Set up the parameters of the benchmark.
	 */
	void setup( const T i_H,      ///< initial maximum surface elevation.
	            const T i_d,      ///< water height with respect to sea level in the constant bathymetry area.
	            const T i_gravity ///< gravity constant.
	)	{

		/*
		 * set member variables to input
		 */
		H = i_H;
		d = i_d;
		gravity = i_gravity;

		/*
		 * compute dependent variables
		 */

		// cotangens of the angle between the sloping beach and sea surface.
		T cotOfbeachSlope = (T)19.85;

		X0 = d * cotOfbeachSlope;
		Hd = H/d;
		gamma = std::sqrt((3.0*H)/(4.0*d));

		// distance: midpoint of the initial wave - position of the toe of the beach
		T L = arccosh(std::sqrt(20.0))/gamma;

		X1 = X0 + L;
		beachSlope = d/X0;
		tau = std::sqrt(d/gravity);

#if CONFIG_ENABLE_NETCDF
		//Setup of the benchmark Variable
		l_benchmarkVerify.setup("data/tsunami_benchmarks/SingleWaveOnaSimpleBeach_BenchMark_Values_1000.nc","time","x","y","h");
#endif
	}


	/**
	 * Get the bathymetry.
	 *
	 * \return bathymetry at given coordinates.
	 */
	T p_getBathymetryData(
			T i_x,					///< x coordinate
			T i_y,					///< y coordinate
			T i_level_of_detail		///< refinement level
	) {
		//  constant bathymetry after the toe.
		if (i_x > X0)
			return -d;

		//  "simple" beach else.
		return -d+(X0-i_x)*beachSlope;
	}

	/**
	 * Get water height.
	 *
	 * @return water height at given coordinates.
	 */
	T p_getWaterSufaceHeight(
			const T i_x,				///< x coordinate
			const T i_y,				///< y Coordinate
			const T i_level_of_detail	///< refinement level
	) {
		T l_waterHeight;

		// wet region.
		if (i_x > 0)
			l_waterHeight = p_getWaterSurfaceElevation(i_x,i_y,i_level_of_detail) - p_getBathymetryData(i_x,i_y,i_level_of_detail);
		// dry region
		else
			l_waterHeight =  (T) 0.;

		return l_waterHeight;
	}



	/**
	 * Get surface elevation.
	 *
	 * @return surface elevation at specific coordinates.
	 */
	T p_getWaterSurfaceElevation(
			T i_x,					///< x coordinate
			T i_y,					///< y coordinate
			T i_level_of_detail		///< refinement level
	) {
		// dry region
		if (i_x < 0)
			return 0;

		T s = sech(gamma*(i_x-X1)/d);
		return s*s*H;
	}

	/**
	 * Get the initial momentum.
	 *
	 * Derived from the particle velocity of the linearized SWEs:
	 *   u(x,0) = - \sqrt{g / d} \eta(x,0)
	 *
	 * @return initial momentum at a given coordinates.
	 */
	T p_getMomentum(
			T i_x,					///< x Coordinate
			T i_y,					///< y Coordinate
			T i_level_of_detail		///< refinement level
	) {
		T l_elevation = p_getWaterSurfaceElevation(i_x, i_y, i_level_of_detail);
		T l_velocity = -std::sqrt(gravity / d) * l_elevation;

		T l_h = l_elevation - p_getBathymetryData(i_x, i_x, i_level_of_detail);
		T l_momentum = l_velocity * l_h;

		if (l_h < SIMULATION_TSUNAMI_ZERO_THRESHOLD)
			return (T)0.;

		return l_momentum;
	}


	/**
	 * Prints the specifications of the benchmark.
	 */
	void outputVerboseInformation(){
		std::cout << std::endl;
		std::cout << "H: " << H << std::endl;
		std::cout << "d: " << d << std::endl;
		std::cout << "H/d: " << Hd << std::endl;
		std::cout << "g: " << gravity << std::endl;
		std::cout << "beach_slope: " << beachSlope << std::endl;
		std::cout << "gamma: " << gamma << std::endl;
		std::cout << "X0 (start of beach slope): " << X0 << std::endl;
		std::cout << "Xs (midpoint of initial wave): " << X1 << std::endl;
		std::cout << "tau: " << tau << std::endl;

		std::cout << std::endl;
	}

	/**
	 * Assigns bathymetry, height and momentum.
	 *
	 * @param o_nodal_data vector (b,h,hu,hv)^T at given coordinates.
	 */
	void getNodalData(
			T i_x,                 ///< x coordinate
			T i_y,                 ///< y coordinate
			T i_level_of_detail,   ///< level of detail (0 = coarsest level)
			CNodeData *o_cNodeData ///< pointer for the nodal data object
	){
		o_cNodeData->b = p_getBathymetryData(i_x, i_y, i_level_of_detail);
		o_cNodeData->h = p_getWaterSufaceHeight(i_x, i_y, i_level_of_detail);
		o_cNodeData->hu = p_getMomentum(i_x, i_y, i_level_of_detail);
		o_cNodeData->hv = 0;
	}

	/**
	 * Obtains the boundary cell values
	 */
	bool getBoundaryData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< x-coordinate in world-space
			T i_level_of_detail,		///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CNodeData *o_nodal_data	///< pointer for the nodal data object.
	){
		// nothing to do here as out let boundary conditions are already implemented in the frame work.
		assert(false);
		return false;
	}



	/**
	 * Return dataset value
	 *
	 * id 0: bathymetry
	 * id 1: displacements
	 */
	CHyperbolicTypes::CSimulationTypes::T getDatasetValue(
			int i_dataset_id,		///< id of dataset to get value from
			T i_x,					///< x-coordinate in model-space
			T i_y,					///< x-coordinate in model-space
			T i_level_of_detail		///< level of detail (0 = coarsest level)
	)
	{
		if (i_dataset_id == 0)
			return p_getBathymetryData(i_x, i_y, i_level_of_detail);

		if (i_dataset_id == 1)
			return p_getWaterSurfaceElevation(i_x, i_y, i_level_of_detail);

		throw(std::runtime_error("invalid dataset id"));
		return 1;
	}

	/**
	 * Method to provide the Benchmark Data at a given location and time
	 *
	 * return true to signal that valid benchmark data is available.
	 */
	bool getBenchmarkNodalData(
			T i_x,					///< X coordinate
			T i_y,					///< Y coordinate
			T i_level_of_detail,	///< Grid Fineness
			T i_time,				///< Simulation Global time
			CNodeData *o_nodal_data	///< pointer for the nodal data object.
	)
	{
#if CONFIG_ENABLE_NETCDF
		T l_dummy=0;
		l_benchmarkVerify.getBenchMarkNodalData(i_time, i_x, i_y, i_level_of_detail, &l_dummy);
		o_nodal_data->h = l_dummy;
#else
		o_nodal_data->h = -1;
		o_nodal_data->hu = 0;
		o_nodal_data->hv = 0;
		o_nodal_data->b = -1;
#endif

		return true;
	}



	/**
	 * load the datasets specified via cSimulationParameters
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		return false;
	}


	/**
	 * return true if the dataset was successfully loaded
	 */
	bool isDatasetLoaded()
	{
		return false;
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T *o_translate_x,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_translate_y,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_size_x,	///< size of domain in world-space
			T *o_size_y		///< size of domain in world-space
	)
	{
		*o_size_x = X1*3;
		*o_size_y = *o_size_x;

		*o_translate_x = X1*0.5;
		*o_translate_y = 0;
	}
};

#endif
