/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *      Author: Alexander Breuer <breuera@in.tum.de>,
 */


#ifndef CBENCHMARK_DAMBREAK_WETDRY_HPP
#define CBENCHMARK_DAMBREAK_WETDRY_HPP

#include <cmath>
#include <cassert>
#include "../../datasets_common/CDataSet_Interface.hpp"

#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CConfig.hpp"
#	include "../types/CTypes.hpp"
#endif


template <typename T>
class CDamBreakWetDry	:
public CDataSet_Interface {
  //private:
  const T m_reservoirHeight;
  const T m_damPosition;

  public:
    /**
     * Constructor of the dam break scenario with wet-dry interface. The analytical solution is a single rarefaction wave.
     *
     * @param  i_reservoirHeight initial height of the water reservoir.
     * @param  i_damPosition position in spatial x-dimension of the dam before failure.
     */
	   CDamBreakWetDry( const T i_reservoirHeight   = 10,
			                 const T i_damPosition       = 0 ):
       m_reservoirHeight( i_reservoirHeight ),
       m_damPosition(     i_damPosition     ) {}; 

     /**
      * Get the bathymetry.
      *
      * @param i_x x-coordinate.
      * @param i_y y-coordinate.
      * @param i_levelOfDetail refinement level.
      * @return bathymetry at given coordinates.
      */
      T p_getBathymetryData( T i_x,
                             T i_y,
                             T i_levelOfDetail ) {
        return 0;
      }

     /**
      * Get the height of the water column at a specific location.
      *
      * @param i_x x-coordinate.
      * @param i_y y-coordinate.
      * @param i_levelOfDetail refinement level.
      * @return height of the water column at given coordinates.
      */
      T p_getWaterSufaceHeight( T i_x,
                                T i_y,
                                T i_levelOfDetail ) {
        if( i_x < m_damPosition ) {
          return m_reservoirHeight;
        }
        else {
          return 0;
        }
      }

     /**
      * Get the surface elevation.
      *
      * @param i_x x-coordinate.
      * @param i_y y-coordinate.
      * @param i_levelOfDetail refinement level.
      * @return surface at given coordinates.
      */
      T p_getWaterSurfaceElevation( T i_x,
                                    T i_y,
                                    T i_levelOfDetail ) {
        return 0;
      }

     /**
      * Get the surface elevation.
      *
      * @param i_x x-coordinate.
      * @param i_y y-coordinate.
      * @param i_levelOfDetail refinement level.
      * @return momentum at given coordinates.
      */
      T p_getMomentum( T i_x,
                       T i_y,
                       T i_levelOfDetail ) {
        return 0;
      }


      /**
       * Prints the specifications of the benchmark.
       */
      void outputVerboseInformation(){
        std::cout << std::endl;
        std::cout << "height of the reservoir: "             << m_reservoirHeight << std::endl;
        std::cout << "position of the dam, which failed: : " << m_damPosition << std::endl;
		    std::cout << std::endl;
      }

      /**
       * Assigns bathymetry, height and momentum.
       *
       * @param i_x x-coordinate.
       * @param i_y y-coordinate.
       * @param i_levelOfDetail refinement level.
       * @param o_cNodeData vector pointer for the nodal data object
        */
      void getNodalData(
		T          i_x,
		T          i_y,
		T          i_levelOfDetail,
		CSimulationNodeData *o_cNodeData
	){
		o_cNodeData->b = p_getBathymetryData(    i_x, i_y, i_levelOfDetail);
		o_cNodeData->h = p_getWaterSufaceHeight( i_x, i_y, i_levelOfDetail);
		o_cNodeData->hu = p_getMomentum(         i_x, i_y, i_levelOfDetail);
		o_cNodeData->hv = 0;
	}

// TODO (breuera): some more methods, which I don't know what they are good for..

	/**
	 * Obtains the boundary cell values
	 */
	bool getBoundaryData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< x-coordinate in world-space
			T i_level_of_detail,		///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CSimulationNodeData *o_nodal_data	///< pointer for the nodal data object.
	){
		// nothing to do here as out let boundary conditions are already implemented in the frame work.
		assert(false);
		return false;
	}



	/**
	 * Return dataset value
	 *
	 * id 0: bathymetry
	 * id 1: displacements
	 */
	CHyperbolicTypes::CSimulationTypes::T getDatasetValue(
			int i_dataset_id,		///< id of dataset to get value from
			T i_x,					///< x-coordinate in model-space
			T i_y,					///< x-coordinate in model-space
			T i_level_of_detail		///< level of detail (0 = coarsest level)
	)
	{
		if (i_dataset_id == 0)
			return p_getBathymetryData(i_x, i_y, i_level_of_detail);

		if (i_dataset_id == 1)
			return p_getWaterSurfaceElevation(i_x, i_y, i_level_of_detail);

		throw(std::runtime_error("invalid dataset id"));
		return 1;
	}

	/**
	 * Method to provide the Benchmark Data at a given location and time
	 *
	 * return true to signal that valid benchmark data is available.
	 */
	bool getBenchmarkNodalData(
			T i_x,					///< X coordinate
			T i_y,					///< Y coordinate
			T i_level_of_detail,	///< Grid Fineness
			T i_time,				///< Simulation Global time
			CSimulationNodeData *o_nodal_data	///< pointer for the nodal data object.
	)
	{
		return true;
	}



	/**
	 * load the datasets specified via cSimulationParameters
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		return false;
	}


	/**
	 * return true if the dataset was successfully loaded
	 */
	bool isDatasetLoaded()
	{
		return false;
	}


	/**
	 * load terrain origin coordinate and size
	 */
	void getOriginAndSize(
			T *o_translate_x,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_translate_y,	///< center of simulation data if the simulation domain is centered around (0,0)
			T *o_size_x,	///< size of domain in world-space
			T *o_size_y		///< size of domain in world-space
	)
	{
		*o_size_x = 10;
		*o_size_y = 10;

		*o_translate_x = 0;
		*o_translate_y = 0;
	}
};

#endif
