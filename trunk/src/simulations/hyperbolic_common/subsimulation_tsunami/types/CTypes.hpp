/*
 * CTsunamiTypes.hpp
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CTSUNAMITYPES_HPP_
#define CTSUNAMITYPES_HPP_

#include "libsierpi/parallelization/CGlobalComm.hpp"
#include <vector>
#include <iostream>
#include <limits>

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"

#include "../../subsimulation_generic/types/CValidation_EdgeData.hpp"
#include "../../subsimulation_generic/types/CValidation_CellData.hpp"
#include "../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
#include "../libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#include "../libsierpi/grid/CSphereCellData.hpp"
#endif

/**
 * Degree of freedoms for one point in tsunami simulation
 */
class CSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h;	// height (relative to bathymetry)
	T hu;	// x-component of momentum
	T hv;	// y-component of momentum


	union	{
		T b;					// bathymetry (specified with negative values if below horizon)
		T cfl1_timestep_size;	// helper variable to determine the maximum allowed timestep size
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b+h;
	}

	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeData &d
	 )	{
		os << "CSimulationNodeData:" << std::endl;
		os << " + h: " << d.h << std::endl;
		os << " + hu: " << d.hu << std::endl;
		os << " + hv: " << d.hv << std::endl;
		os << " + b/max_wave_speed: " << d.b << std::endl;
		os << " + vx: " << d.hu/d.h << std::endl;
		os << " + vy: " << d.hv/d.h << std::endl;
		os << " + horizon: " << (d.h + d.b) << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,		///< cout
			T x_axis_x,				///< x-axis of new basis (x-component)
			T x_axis_y				///< x-axis of new basis (y-component)
	) const
	{
		os << "Tsunami DOF Information:" << std::endl;
		os << " + h: " << h << std::endl;

		T thu = hu;
		T thv = hv;
		CTriangle_VectorProjections::referenceToWorld(&thu, &thv, x_axis_x, x_axis_y);

		os << " + hu: " << thu << std::endl;
		os << " + hv: " << thv << std::endl;

		os << " + b/max_wave_speed: " << b << std::endl;

		os << " + vx: " << thu/h << std::endl;
		os << " + vy: " << thv/h << std::endl;
		os << " + horizon: " << (h + b) << std::endl;

		os << std::endl;
	}
};





/**
 * Degree of freedoms for one point in tsunami simulation
 *
 * TODO [schreibm]
 */
template <int N>
class CSimulationNodeDataSOA
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T h[N];	// height
	T hu[N];	// x-component of momentum
	T hv[N];	// y-component of momentum

	union	{
		T b[N];	// bathymetry
		T cfl1_timestep_size[N];	// max wave speed for flux computations
	};


	/**
	 * return simulation specific benchmark data
	 *
	 * for tsunami simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = b[0]+h[0];
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultCellValues(
		T default_values[4]
	)
	{
		CSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS> poly;

		poly.h[0] = default_values[0];
		poly.hu[0] = 0;
		poly.hv[0] = 0;
		poly.b[0] = -default_values[0];

		for (int i = 1; i < N; i++)
		{
			poly.h[i] = 0;
			poly.hu[i] = 0;
			poly.hv[i] = 0;
			poly.b[i] = 0;
		}

		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly.h, this->h);
		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly.hu, this->hu);
		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly.hv, this->hv);
		CDG_MatrixComputations_2D::mul_convert_poly_to_dofs(poly.b, this->b);
	}



	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultEdgeValues(T default_nodal_dof_values[4])
	{
		// Edge data is stored nodal-wise
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			this->h[i] = default_nodal_dof_values[0];
			this->hu[i] = default_nodal_dof_values[1];
			this->hv[i] = default_nodal_dof_values[2];
			this->b[i] = default_nodal_dof_values[3];
		}
	}


	static inline void outputVector(
			std::ostream &os,					///< output stream
			const T *v,
			int i_size
	)
	{
		os << "[";
		for (int i = 0; i < i_size-1; i++)
			os << " " << v[i];
		os << " " << v[i_size-1] << " ]" << std::endl;
	}



	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeDataSOA &d
	 )	{
		os << "CSimulationNodeDataSOA:" << std::endl;
		os << " + h:";
		outputVector(os, d.h, N);
		os << " + hu:";
		outputVector(os, d.hu, N);
		os << " + hv:";
		outputVector(os, d.hv, N);

		os << " + b/max_wave_speed:";
		outputVector(os, d.b, N);

		T hb[N];
		for (int i = 0; i < N; i++)
			hb[i] = d.h[i]+d.b[i];

		os << " + horizon:";
		outputVector(os, hb, N);

		os << std::endl;

		return os;
	}


	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,	///< output stream
			T x_axis_x,			///< x-axis of new basis (x-component)
			T x_axis_y			///< x-axis of new basis (y-component)
	) const
	{
		os << "CSimulationNodeDataSOA:" << std::endl;
		os << " + h:";
		outputVector(os, h, N);

		T whu[N];
		T whv[N];

		for (int i = 0; i < N; i++)
		{
			whu[i] = hu[i];
			whv[i] = hv[i];
			CTriangle_VectorProjections::referenceToWorld(&whu[i], &whv[i], x_axis_x, x_axis_y);
		}

		os << " + whu:";
		outputVector(os, whu, N);
		os << " + whv:";
		outputVector(os, whv, N);

		os << " + b/max_wave_speed:";
		outputVector(os, b, N);

		T hb[N];
		for (int i = 0; i < N; i++)
			hb[i] = h[i]+b[i];


		os << " + horizon:";
		outputVector(os, hb, N);

		os << std::endl;
	}
};


/**
 * Edge communication data during tsunami simulation
 */
class CSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS> dofs;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
	T max_eigen_coefficient;
#endif

	T CFL1_scalar;

	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[4])
	{
		T ddofs[4];
		ddofs[0] = i_default_values[0];
		ddofs[1] = 0;
		ddofs[2] = 0;
		ddofs[3] = -i_default_values[0];

		dofs.setupDefaultEdgeValues(ddofs);
	}


	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationEdgeData &d
	 )	{

		os << "Edge Data [h, hu, hv, b/max_wav_speed, vx, vy, horizon]" << std::endl;
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS; i++)
		{
			std::cout << " + " << i << ": [" <<
					d.dofs.h[i] << ", " <<
					d.dofs.hu[i] << ", " <<
					d.dofs.hv[i] << ", " <<
					d.dofs.b[i] << ", " <<
					d.dofs.hu[i]/d.dofs.h[i] << ", " <<
					d.dofs.hv[i]/d.dofs.h[i] << ", " <<
					(d.dofs.h[i]+d.dofs.b[i]) << "]" << std::endl;
		}
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};




/**
 * TODO [schreibm]
 */
class CSimulationCellData
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		: public CSphereCellData
#endif
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * weights for basis functions
	 */
	CSimulationNodeDataSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS> dofs;


#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		T max_eigen_coefficient;
#endif



#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
	bool refine;
	bool coarsen;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(
			T default_values[4]
	)
	{
		T ddofs[4];
		ddofs[0] = default_values[0];
		ddofs[1] = 0;
		ddofs[2] = 0;
		ddofs[3] = -default_values[0];

		dofs.setupDefaultCellValues(ddofs);
	}



	friend
	::std::ostream&
	 operator<<	(
			 ::std::ostream& os,
			  const CSimulationCellData &d
	 )	{
		d.outputVerboseData(os, 1, 0);
		return os;
	}



	void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	) const
	{
		dofs.outputVerboseData(os, x_axis_x, x_axis_y);

#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		os << "max_eigen_coefficient: " << max_eigen_coefficient << std::endl;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << validation << std::endl;
#endif
	}
};

#include "../../subsimulation_generic/types/CVisualizationVertexData.hpp"



/**
 * simulation types for simulation
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CSimulationEdgeData CEdgeData;
	};


	/**
	 * This class implements all types which are used for visualization
	 */

	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};

	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;
};




#endif /* CTSUNAMITYPES_HPP_ */
