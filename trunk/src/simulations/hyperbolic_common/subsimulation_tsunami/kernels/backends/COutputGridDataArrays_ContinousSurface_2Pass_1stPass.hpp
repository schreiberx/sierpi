/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Juli 30, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_2PASS_1STPASS_HPP_
#define COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_2PASS_1STPASS_HPP_

#include "libsierpi/traversators/vertexData/CTraversator_VertexData_CellData_Depth.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "../../../subsimulation_generic/types/CValidation_NodeData.hpp"
#include "../../../subsimulation_generic/types/CVisualizationVertexData.hpp"

#include "../../../subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#	include "libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#endif

namespace sierpi
{
namespace kernels
{


class COutputGridDataArrays_ContinousSurface_2Pass_1stPass
{
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::T 			TVertexScalar;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	TVisualizationVertexData;
	typedef TVertexScalar	T;

	class CVertexPackage
	{
	public:
		TVertexScalar vertices[3][3];
		TVertexScalar displacements[3];
	};


	typedef sierpi::travs::CTraversator_VertexData_CellData_Depth<COutputGridDataArrays_ContinousSurface_2Pass_1stPass, CHyperbolicTypes> TRAV;


private:
	CStack<CVertexPackage> *cTemporaryVertices;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * preprocessing mode
	 * 0: default
	 * 1: displace with h+b
	 * 2: displace with b
	 */
	int preprocessing_mode;

	/**
	 * scalar for displacements
	 */
	TVertexScalar displacement_or_scaling_value;

	/**
	 * operation due to parallelization:
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void op_node_node_join_middle_touch(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		// VERTEX DISPLACEMENT
		o_vertexData->scalars[0] = i_vertexData1->scalars[0] + i_vertexData2->scalars[0];

		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeVertexData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}



public:
	inline void op_node_node_join_middle_touch(
			const TVisualizationVertexData *i_vertexData_to_join,	///< adjacent vertex data
			TVisualizationVertexData *io_vertexData					///< local vertex data
	)
	{
#if DEBUG
		io_vertexData->validation.testVertex(i_vertexData_to_join->validation);
#endif

		io_vertexData->scalars[0] += i_vertexData_to_join->scalars[0];

		io_vertexData->normalization_factor += i_vertexData_to_join->normalization_factor;
	}



	/**
	 * final touch: normalize
	 */
public:
	inline void op_node_finishHimTouch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		p_normalizeVertexData(io_vertexData);
	}



	/**
	 * first touch for vertex data
	 */
private:
	inline void p_actionFirstTouchVertexData(
		T vec_mx, T vec_my,		// vector from corner to midpoint
		int i_depth,
		CCellData *i_cellData,
		TVisualizationVertexData *o_vertexData
	) {
		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];

			if (i_cellData->dofs.h[0] < SIMULATION_TSUNAMI_DRY_THRESHOLD)
				elevation = i_cellData->dofs.b[0]-0.000001;
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE3)
		{
			elevation = i_cellData->dofs.b[0];

//			if (i_cellData->dofs.h[0] >= SIMULATION_TSUNAMI_DRY_THRESHOLD)
//				elevation = std::numeric_limits<TVertexScalar>::infinity();
		}
		else
		{
			assert(false);
		}


		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		o_vertexData->scalars[0] = elevation*l;
		o_vertexData->normalization_factor = l;
	}



public:
	/**
	 * first touch for vertex data at left vertex
	 */
public:
	inline void op_node_first_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}



	/**
	 * first touch for vertex data at right vertex
	 */
public:
	inline void op_node_first_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}



	/**
	 * first touch for vertex data at top vertex
	 */
public:
	inline void op_node_first_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}


	/**
	 * normalize vertex data after vertex data was written
	 */
private:
	inline void p_normalizeVertexData(
			TVisualizationVertexData *io_vertexData
	)
	{
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->scalars[0] *= inv_fac;
	}




private:
	/*
	 * middle touch vertex
	 */
	inline void p_actionMiddleTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{

		T elevation;

		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];

			if (i_cellData->dofs.h[0] < SIMULATION_TSUNAMI_DRY_THRESHOLD)
				elevation = i_cellData->dofs.b[0]-0.000001;
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE3)
		{
			elevation = i_cellData->dofs.b[0];

//			if (i_cellData->dofs.h[0] >= SIMULATION_TSUNAMI_DRY_THRESHOLD)
//				elevation = std::numeric_limits<TVertexScalar>::infinity();
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->scalars[0] += elevation*l;
		io_vertexData->normalization_factor += l;

	}



	/*
	 * middle touch on left vertex
	 */
public:
	inline void op_node_middle_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	/*
	 * middle touch on right vertex
	 */
public:
	inline void op_node_middle_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	/*
	 * middle touch on top vertex
	 */
public:
	inline void op_node_middle_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	/*
	 * last touch on vertex: normalize
	 */
private:
	inline void p_actionLastTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{
		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];

			if (i_cellData->dofs.h[0] < SIMULATION_TSUNAMI_DRY_THRESHOLD)
				elevation = i_cellData->dofs.b[0]-0.000001;
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE3)
		{
			elevation = i_cellData->dofs.b[0];

//			if (i_cellData->dofs.h[0] >= SIMULATION_TSUNAMI_DRY_THRESHOLD)
//				elevation = std::numeric_limits<TVertexScalar>::infinity();
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->scalars[0] += elevation*l;
		io_vertexData->normalization_factor += l;

		p_normalizeVertexData(io_vertexData);
	}


	/*
	 * last touch on left vertex
	 */
public:
	inline void op_node_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}


	/*
	 * last touch on right vertex
	 */
public:
	inline void op_node_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}


	/*
	 * last touch on top vertex
	 */
public:
	inline void op_node_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	/*
	 * operation executed on each cell after creating vertex data
	 */
	inline void op_cell(
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,

			int i_depth,

			CCellData *i_cCellData,							///< cell data
			TVisualizationVertexData *i_leftVertexData,		///< left vertex data
			TVisualizationVertexData *i_rightVertexData,	///< right vertex data
			TVisualizationVertexData *i_topVertexData		///< top vertex data
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_leftVertexData->validation.testVertex(i_vertex_coord_left_x, i_vertex_coord_left_y);
		i_rightVertexData->validation.testVertex(i_vertex_coord_right_x, i_vertex_coord_right_y);
		i_topVertexData->validation.testVertex(i_vertex_coord_top_x, i_vertex_coord_top_y);
#endif

		CVertexPackage &v = cTemporaryVertices->fakePush_returnRef();

		if (output_flags & CGridDataArrays_Enums::VERTICES)
		{
			v.vertices[0][0] = i_vertex_coord_left_x;
			v.vertices[0][1] = i_vertex_coord_left_y;
			v.vertices[0][2] = i_leftVertexData->scalars[0];
			v.vertices[1][0] = i_vertex_coord_right_x;
			v.vertices[1][1] = i_vertex_coord_right_y;
			v.vertices[1][2] = i_rightVertexData->scalars[0];
			v.vertices[2][0] = i_vertex_coord_top_x;
			v.vertices[2][1] = i_vertex_coord_top_y;
			v.vertices[2][2] = i_topVertexData->scalars[0];

			// backup elevations
			v.displacements[0] = i_leftVertexData->scalars[0];
			v.displacements[1] = i_rightVertexData->scalars[0];
			v.displacements[2] = i_topVertexData->scalars[0];

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1 && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

			if (preprocessing_mode)
			{
				T average_vertex_x = (i_vertex_coord_left_x+i_vertex_coord_right_x+i_vertex_coord_top_x)*(TVertexScalar)(1.0/3.0);
				T average_vertex_y = (i_vertex_coord_left_y+i_vertex_coord_right_y+i_vertex_coord_top_y)*(TVertexScalar)(1.0/3.0);

				// determine face
				CCube_To_Sphere_Projection::EFaceType faceType =
						CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(average_vertex_x, average_vertex_y);

				// project 2D coordinate to cube face
				CCube_To_Sphere_Projection::project2DTo2DFace(
						faceType,
						&v.vertices[0][0], &v.vertices[0][1],
						&v.vertices[1][0], &v.vertices[1][1],
						&v.vertices[2][0], &v.vertices[2][1]
					);

				// project to sphere
				for (int j = 0; j < 3; j++)
				{
					CCube_To_Sphere_Projection::project2DFaceTo3D(
							faceType,
							v.vertices[j][0], v.vertices[j][1],
							&v.vertices[j][0], &v.vertices[j][1], &v.vertices[j][2]
						);
				}

				for (int i = 0; i < 3; i++)
				{
					v.vertices[i][0] += v.displacements[i]*v.vertices[i][0]*displacement_or_scaling_value;
					v.vertices[i][1] += v.displacements[i]*v.vertices[i][1]*displacement_or_scaling_value;
					v.vertices[i][2] += v.displacements[i]*v.vertices[i][2]*displacement_or_scaling_value;
				}
			}
#else

			for (int i = 0; i < 3; i++)
				v.vertices[i][2] *= displacement_or_scaling_value;
#endif
		}


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(
				i_vertex_coord_left_x, i_vertex_coord_left_y,
				i_vertex_coord_right_x, i_vertex_coord_right_y,
				i_vertex_coord_top_x, i_vertex_coord_top_y
			);
#endif
	}


	inline void traversal_pre_hook()
	{
	}


	inline void traversal_post_hook()
	{
	}


	inline void setup(
			CStack<CVertexPackage> *io_cTemporaryVertices,
			CGridDataArrays<3,6> *o_GridDataArrays,
			CDatasets *i_cDatasets,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing_mode,
			TVertexScalar i_displacement_or_scaling_value
	) {
		cTemporaryVertices = io_cTemporaryVertices;
		output_flags = i_output_flags;

		preprocessing_mode = i_preprocessing_mode;
		displacement_or_scaling_value = i_displacement_or_scaling_value;
	}


	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputGridDataArrays_ContinousSurface_2Pass_1stPass &parent_kernel
	) {
		assert(false);
	}
};


}
}

#endif
