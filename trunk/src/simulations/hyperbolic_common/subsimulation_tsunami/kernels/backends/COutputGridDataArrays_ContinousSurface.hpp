/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_HPP_
#define COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_HPP_

#include "libsierpi/traversators/vertexData/CTraversator_VertexData_CellData_Depth.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "../../../subsimulation_generic/types/CValidation_NodeData.hpp"
#include "../../../subsimulation_generic/types/CVisualizationVertexData.hpp"

#include "../../../subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"


namespace sierpi
{
namespace kernels
{


class COutputGridDataArrays_ContinousSurface
{
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::T 			TVertexScalar;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	TVisualizationVertexData;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexData_CellData_Depth<COutputGridDataArrays_ContinousSurface, CHyperbolicTypes> TRAV;



private:
	CGridDataArrays<3,6> *cGridDataArrays;

	/**
	 * datasets to access benchmark data
	 */
	CDatasets *cDatasets;

	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing;



	/**
	 * operation due to parallelization:
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void op_node_node_join_middle_touch(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		o_vertexData->normal[0] = i_vertexData1->normal[0] + i_vertexData2->normal[0];
		o_vertexData->normal[1] = i_vertexData1->normal[1] + i_vertexData2->normal[1];
		o_vertexData->normal[2] = i_vertexData1->normal[2] + i_vertexData2->normal[2];

		o_vertexData->height = i_vertexData1->height + i_vertexData2->height;

		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeVertexData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}



public:
	inline void op_node_node_join_middle_touch(
			const TVisualizationVertexData *i_vertexData_to_join,	///< adjacent vertex data
			TVisualizationVertexData *io_vertexData					///< local vertex data
	)
	{
#if DEBUG
		io_vertexData->validation.testVertex(i_vertexData_to_join->validation);
#endif

		io_vertexData->normal[0] += i_vertexData_to_join->normal[0];
		io_vertexData->normal[1] += i_vertexData_to_join->normal[1];
		io_vertexData->normal[2] += i_vertexData_to_join->normal[2];

		io_vertexData->height += i_vertexData_to_join->height;

		io_vertexData->normalization_factor += i_vertexData_to_join->normalization_factor;
	}



	/**
	 * final touch: normalize
	 */
public:
	inline void op_node_finishHimTouch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		p_normalizeVertexData(io_vertexData);
	}



private:
	inline void p_computeNormal(
			T i_vec_mx,		T i_vec_my,		T i_elevation,
			T o_normal[3]
	)
	{
		// face tangential vector
		T fx = i_vec_mx;
		T fy = i_vec_my;
		T fz = -i_elevation;

		// orthogonal on plane
		T ox = -i_vec_my;
		T oy = i_vec_mx;
		T oz = 0;

		T nx = fy*oz - fz*oy;
		T ny = fz*ox - fx*oz;
		T nz = fx*oy - fy*ox;

		T inv = (T)1.0/std::sqrt(nx*nx+ny*ny+nz*nz);
		nx *= inv;
		ny *= inv;
		nz *= inv;

		o_normal[0] = nx;
		o_normal[1] = ny;
		o_normal[2] = nz;
	}



	/**
	 * first touch for vertex data
	 */
private:
	inline void p_actionFirstTouchVertexData(
		T vec_mx, T vec_my,		// vector from corner to midpoint
		int i_depth,
		CCellData *i_cellData,
		TVisualizationVertexData *o_vertexData
	) {
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		o_vertexData->height = elevation*l;
		o_vertexData->normalization_factor = l;

		p_computeNormal(vec_mx, vec_my, elevation, o_vertexData->normal);



#elif SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 1

#error "not supported so far"
		o_vertexData->normal[0] = 0;
		o_vertexData->normal[1] = 1;
		o_vertexData->normal[2] = 0;

		/*
		 * TODO: fix hyp_edge to care about left/right/hyp edge
		 */
		if (p_visualizationType == 0)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.elevation+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			o_vertexData->height = i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		o_vertexData->normalization_factor = 1;
#endif
	}



public:
	/**
	 * first touch for vertex data at left vertex
	 */
public:
	inline void op_node_first_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}



	/**
	 * first touch for vertex data at right vertex
	 */
public:
	inline void op_node_first_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}



	/**
	 * first touch for vertex data at top vertex
	 */
public:
	inline void op_node_first_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}


	/**
	 * normalize vertex data after vertex data was written
	 */
private:
	inline void p_normalizeVertexData(
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0
		/*
		 * normalize
		 */
		TVertexScalar inv_len = ((TVertexScalar)1.0)/std::sqrt(io_vertexData->normal[0]*io_vertexData->normal[0]+io_vertexData->normal[1]*io_vertexData->normal[1]+io_vertexData->normal[2]*io_vertexData->normal[2]);
		io_vertexData->normal[0] *= inv_len;
		io_vertexData->normal[1] *= inv_len;
		io_vertexData->normal[2] *= inv_len;

		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->height *= inv_fac;
#else
		/*
		 * normalize
		 */
		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->normal[0] *= inv_fac;
		io_vertexData->normal[1] *= inv_fac;
		io_vertexData->normal[2] *= inv_fac;

		io_vertexData->height *= inv_fac;
#endif
	}




private:
	/*
	 * middle touch vertex
	 */
	inline void p_actionMiddleTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;

		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->height += elevation*l;
		io_vertexData->normalization_factor += l;

		T normal[3];
		p_computeNormal(vec_mx, vec_my, elevation, normal);

		io_vertexData->normal[0] += normal[0];
		io_vertexData->normal[1] += normal[1];
		io_vertexData->normal[2] += normal[2];



#elif SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;


		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.elevation+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif
	}



	/*
	 * middle touch on left vertex
	 */
public:
	inline void op_node_middle_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	/*
	 * middle touch on right vertex
	 */
public:
	inline void op_node_middle_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	/*
	 * middle touch on top vertex
	 */
public:
	inline void op_node_middle_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	/*
	 * last touch on vertex: normalize
	 */
private:
	inline void p_actionLastTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->height += elevation*l;
		io_vertexData->normalization_factor += l;


		T normal[3];
		p_computeNormal(vec_mx, vec_my, elevation, normal);

		io_vertexData->normal[0] += normal[0];
		io_vertexData->normal[1] += normal[1];
		io_vertexData->normal[2] += normal[2];


#elif SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 1

		io_vertexData->normal[0] += 0;
		io_vertexData->normal[1] += 1;
		io_vertexData->normal[2] += 0;

		if (p_visualizationType == 0)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.elevation+i_cellData->dofs_hyp_edge.b;
		}
		else if (p_visualizationType == 3)
		{
			io_vertexData->height += i_cellData->dofs_hyp_edge.b;
		}
		else
		{
			assert(false);
		}

		io_vertexData->normalization_factor += 1;
#endif

		p_normalizeVertexData(io_vertexData);
	}


	/*
	 * last touch on left vertex
	 */
public:
	inline void op_node_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}


	/*
	 * last touch on right vertex
	 */
public:
	inline void op_node_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}


	/*
	 * last touch on top vertex
	 */
public:
	inline void op_node_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	/*
	 * operation executed on each cell after creating vertex data
	 */
	inline void op_cell(
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,

			int i_depth,

			CCellData *i_cCellData,							///< cell data
			TVisualizationVertexData *i_leftVertexData,		///< left vertex data
			TVisualizationVertexData *i_rightVertexData,	///< right vertex data
			TVisualizationVertexData *i_topVertexData		///< top vertex data
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_leftVertexData->validation.testVertex(i_vertex_coord_left_x, i_vertex_coord_left_y);
		i_rightVertexData->validation.testVertex(i_vertex_coord_right_x, i_vertex_coord_right_y);
		i_topVertexData->validation.testVertex(i_vertex_coord_top_x, i_vertex_coord_top_y);
#endif

		T *v;

		if (output_flags & CGridDataArrays_Enums::VERTICES)
		{
			v = &(cGridDataArrays->triangle_vertex_buffer[cell_write_index * 3 * 3]);

			v[0 * 3 + 0] = i_vertex_coord_left_x;
			v[0 * 3 + 1] = i_vertex_coord_left_y;
			v[0 * 3 + 2] = i_leftVertexData->height;
			v[1 * 3 + 0] = i_vertex_coord_right_x;
			v[1 * 3 + 1] = i_vertex_coord_right_y;
			v[1 * 3 + 2] = i_rightVertexData->height;
			v[2 * 3 + 0] = i_vertex_coord_top_x;
			v[2 * 3 + 1] = i_vertex_coord_top_y;
			v[2 * 3 + 2] = i_topVertexData->height;
		}

		if (output_flags & CGridDataArrays_Enums::NORMALS)
		{
			T *n = &(cGridDataArrays->triangle_normal_buffer[cell_write_index * 3 * 3]);

#if 0
			n[0*3+0] = 0;
			n[0*3+1] = 0;
			n[0*3+2] = 1;
			n[1*3+0] = 0;
			n[1*3+1] = 0;
			n[1*3+2] = 1;
			n[2*3+0] = 0;
			n[2*3+1] = 0;
			n[2*3+2] = 1;
#else
			n[0*3+0] = i_leftVertexData->normal[0];
			n[0*3+1] = i_leftVertexData->normal[1];
			n[0*3+2] = i_leftVertexData->normal[2];
			n[1*3+0] = i_rightVertexData->normal[0];
			n[1*3+1] = i_rightVertexData->normal[1];
			n[1*3+2] = i_rightVertexData->normal[2];
			n[2*3+0] = i_topVertexData->normal[0];
			n[2*3+1] = i_topVertexData->normal[1];
			n[2*3+2] = i_topVertexData->normal[2];
#endif
		}

		cell_write_index++;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(
				i_vertex_coord_left_x, i_vertex_coord_left_y,
				i_vertex_coord_right_x, i_vertex_coord_right_y,
				i_vertex_coord_top_x, i_vertex_coord_top_y
			);
#endif
	}


	inline void traversal_pre_hook()
	{
	}


	inline void traversal_post_hook()
	{
	}


	inline void setup(
			CGridDataArrays<3,6> *o_GridDataArrays,
			CDatasets *i_cDatasets,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing
	) {
		cGridDataArrays = o_GridDataArrays;
		cDatasets = i_cDatasets;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
		preprocessing = i_preprocessing;
	}


	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputGridDataArrays_ContinousSurface &parent_kernel) {
		assert(false);
	}
};


}
}

#endif
