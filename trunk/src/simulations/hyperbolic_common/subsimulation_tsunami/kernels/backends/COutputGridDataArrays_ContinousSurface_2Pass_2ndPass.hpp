/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Juli 30, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_2PASS_2NDPASS_HPP_
#define COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_2PASS_2NDPASS_HPP_

#include "COutputGridDataArrays_ContinousSurface_2Pass_1stPass.hpp"

#include "libsierpi/traversators/vertexData/CTraversator_VertexData_CellData_Depth.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "../../../subsimulation_generic/types/CValidation_NodeData.hpp"
#include "../../../subsimulation_generic/types/CVisualizationVertexData.hpp"

#include "../../../subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"


namespace sierpi
{
namespace kernels
{


class COutputGridDataArrays_ContinousSurface_2Pass_2ndPass
{
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::T 			TVertexScalar;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	TVisualizationVertexData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	CNodeData;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexData_CellData_Depth<COutputGridDataArrays_ContinousSurface_2Pass_2ndPass, CHyperbolicTypes> TRAV;

	typedef COutputGridDataArrays_ContinousSurface_2Pass_1stPass::CVertexPackage CVertexPackage;


private:
	CStack<CVertexPackage> *cTemporaryVertices;

	CStackReaderTopDown<CVertexPackage> cTemporaryVerticesTopDownReader;

	CStackReaderBottomUp<CVertexPackage> cTemporaryVerticesBottomUpReader;

	int read_counter;

	CVertexPackage *currentVertexPackage;
	TVertexScalar current_face_normal[3];


	CGridDataArrays<3,6> *cGridDataArrays;

	/**
	 * datasets to access benchmark data
	 */
	CDatasets *cDatasets;

	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing_mode;


	void p_vertexProcessingPreHook()
	{
		if (read_counter == 0)
		{
			// fetch next vertex package
			currentVertexPackage = cTemporaryVerticesTopDownReader.getNextDataPtr();

			T reconstructed_v[3][3];

			T vmin[3];
			vmin[0] = currentVertexPackage->vertices[0][0];
			vmin[1] = currentVertexPackage->vertices[0][1];
			vmin[2] = currentVertexPackage->vertices[0][2];

			for (int j = 1; j < 3; j++)
			{
				vmin[0] = std::min(vmin[0], currentVertexPackage->vertices[j][0]);
				vmin[1] = std::min(vmin[1], currentVertexPackage->vertices[j][1]);
				vmin[2] = std::min(vmin[2], currentVertexPackage->vertices[j][2]);
			}

			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 3; i++)
				{
					if (currentVertexPackage->vertices[j][i] == std::numeric_limits<TVertexScalar>::infinity())
						reconstructed_v[j][i] = vmin[i];
					else
						reconstructed_v[j][i] = currentVertexPackage->vertices[j][i];
				}
			}


			// compute edge direction vectors
			TVertexScalar v1[3];
			v1[0] = reconstructed_v[1][0] - reconstructed_v[0][0];
			v1[1] = reconstructed_v[1][1] - reconstructed_v[0][1];
			v1[2] = reconstructed_v[1][2] - reconstructed_v[0][2];


			if (	reconstructed_v[1][0] == std::numeric_limits<TVertexScalar>::infinity() ||
					reconstructed_v[0][0] == std::numeric_limits<TVertexScalar>::infinity()
				)
			{

			}

			TVertexScalar v2[3];
			v2[0] = reconstructed_v[2][0] - reconstructed_v[0][0];
			v2[1] = reconstructed_v[2][1] - reconstructed_v[0][1];
			v2[2] = reconstructed_v[2][2] - reconstructed_v[0][2];

			// compute x-product
			current_face_normal[0] = v1[1]*v2[2] - v1[2]*v2[1];
			current_face_normal[1] = v1[2]*v2[0] - v1[0]*v2[2];
			current_face_normal[2] = v1[0]*v2[1] - v1[1]*v2[0];
/*
 * do not normalize to account for surface area
			// normalize
			TVertexScalar inv_n = TVertexScalar(1.0)
							/ std::sqrt(
								current_face_normal[0]*current_face_normal[0] +
								current_face_normal[1]*current_face_normal[1] +
								current_face_normal[2]*current_face_normal[2]
							);

			current_face_normal[0] *= inv_n;
			current_face_normal[1] *= inv_n;
			current_face_normal[2] *= inv_n;
*/
			read_counter = 2;
		}
		else
		{
			read_counter--;
		}
	}


	/**
	 * operation due to parallelization:
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void op_node_node_join_middle_touch(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		o_vertexData->scalars[0] = i_vertexData1->scalars[0] + i_vertexData2->scalars[0];
		o_vertexData->scalars[1] = i_vertexData1->scalars[1] + i_vertexData2->scalars[1];
		o_vertexData->scalars[2] = i_vertexData1->scalars[2] + i_vertexData2->scalars[2];

//		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeNormalData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}



public:
	inline void op_node_node_join_middle_touch(
			const TVisualizationVertexData *i_vertexData_to_join,	///< adjacent vertex data
			TVisualizationVertexData *io_vertexData					///< local vertex data
	)
	{
#if DEBUG
		io_vertexData->validation.testVertex(i_vertexData_to_join->validation);
#endif

		io_vertexData->scalars[0] += i_vertexData_to_join->scalars[0];
		io_vertexData->scalars[1] += i_vertexData_to_join->scalars[1];
		io_vertexData->scalars[2] += i_vertexData_to_join->scalars[2];

//		io_vertexData->normalization_factor += i_vertexData_to_join->normalization_factor;
	}



	/**
	 * final touch: normalize
	 */
public:
	inline void op_node_finishHimTouch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		p_normalizeNormalData(io_vertexData);
	}


	/**
	 * first touch for vertex data
	 */
private:
	inline void p_actionFirstTouchVertexData(
		int i_depth,
		CCellData *i_cellData,
		TVisualizationVertexData *o_vertexData
	) {
		p_vertexProcessingPreHook();

//		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
//		o_vertexData->normalization_factor = l;

		o_vertexData->scalars[0] = current_face_normal[0];
		o_vertexData->scalars[1] = current_face_normal[1];
		o_vertexData->scalars[2] = current_face_normal[2];
	}



public:
	/**
	 * first touch for vertex data at left vertex
	 */
public:
	inline void op_node_first_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[0][0], currentVertexPackage->vertices[0][1]);
#endif
	}



	/**
	 * first touch for vertex data at right vertex
	 */
public:
	inline void op_node_first_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[1][0], currentVertexPackage->vertices[1][1]);
#endif
	}



	/**
	 * first touch for vertex data at top vertex
	 */
public:
	inline void op_node_first_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionFirstTouchVertexData(
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[2][0], currentVertexPackage->vertices[2][1]);
#endif
	}


	/**
	 * normalize vertex data after vertex data was written
	 */
private:
	inline void p_normalizeNormalData(
			TVisualizationVertexData *io_vertexData
	)
	{
		/*
		 * normalize
		 */
		TVertexScalar inv_len = ((TVertexScalar)1.0)/std::sqrt(io_vertexData->scalars[0]*io_vertexData->scalars[0]+io_vertexData->scalars[1]*io_vertexData->scalars[1]+io_vertexData->scalars[2]*io_vertexData->scalars[2]);
		io_vertexData->scalars[0] *= inv_len;
		io_vertexData->scalars[1] *= inv_len;
		io_vertexData->scalars[2] *= inv_len;
	}




private:
	/*
	 * middle touch vertex
	 */
	inline void p_actionMiddleTouchVertexData(
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
		)
	{
		p_vertexProcessingPreHook();

//		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
//		io_vertexData->normalization_factor += l;

		io_vertexData->scalars[0] += current_face_normal[0];
		io_vertexData->scalars[1] += current_face_normal[1];
		io_vertexData->scalars[2] += current_face_normal[2];
	}



	/*
	 * middle touch on left vertex
	 */
public:
	inline void op_node_middle_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(
				depth, i_cellData, o_vertexData
		);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[0][0], currentVertexPackage->vertices[0][1]);
#endif
	}

	/*
	 * middle touch on right vertex
	 */
public:
	inline void op_node_middle_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[1][0], currentVertexPackage->vertices[1][1]);
#endif
	}

	/*
	 * middle touch on top vertex
	 */
public:
	inline void op_node_middle_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionMiddleTouchVertexData(
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[2][0], currentVertexPackage->vertices[2][1]);
#endif
	}


	/*
	 * last touch on vertex: normalize
	 */
private:
	inline void p_actionLastTouchVertexData(
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{
		p_vertexProcessingPreHook();

//		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
//		io_vertexData->normalization_factor += l;

		io_vertexData->scalars[0] += current_face_normal[0];
		io_vertexData->scalars[1] += current_face_normal[1];
		io_vertexData->scalars[2] += current_face_normal[2];

		p_normalizeNormalData(io_vertexData);
	}


	/*
	 * last touch on left vertex
	 */
public:
	inline void op_node_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[0][0], currentVertexPackage->vertices[0][1]);
#endif
	}


	/*
	 * last touch on right vertex
	 */
public:
	inline void op_node_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[1][0], currentVertexPackage->vertices[1][1]);
#endif
	}


	/*
	 * last touch on top vertex
	 */
public:
	inline void op_node_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		p_actionLastTouchVertexData(depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
		o_vertexData->validation.testVertex(currentVertexPackage->vertices[2][0], currentVertexPackage->vertices[2][1]);
#endif
	}


	/*
	 * operation executed on each cell after creating vertex data
	 */
	inline void op_cell(
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,

			int i_depth,

			CCellData *i_cCellData,							///< cell data
			TVisualizationVertexData *i_leftVertexData,		///< left vertex data
			TVisualizationVertexData *i_rightVertexData,	///< right vertex data
			TVisualizationVertexData *i_topVertexData		///< top vertex data
	)
	{
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_leftVertexData->validation.testVertex(i_vertex_coord_left_x, i_vertex_coord_left_y);
		i_rightVertexData->validation.testVertex(i_vertex_coord_right_x, i_vertex_coord_right_y);
		i_topVertexData->validation.testVertex(i_vertex_coord_top_x, i_vertex_coord_top_y);
#endif

		CVertexPackage *vp = cTemporaryVerticesBottomUpReader.getNextDataPtr();

		if (output_flags & CGridDataArrays_Enums::VERTICES)
		{
			TVertexScalar *v = &(cGridDataArrays->triangle_vertex_buffer[cell_write_index * 3 * 3]);
			memcpy(v, &(vp->vertices[0][0]), sizeof(TVertexScalar)*3*3);

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE != 1 || !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

			assert(std::abs(vp->vertices[0][0] - i_vertex_coord_left_x) < 0.00001);
			assert(std::abs(vp->vertices[0][1] - i_vertex_coord_left_y) < 0.00001);

			assert(std::abs(vp->vertices[1][0] - i_vertex_coord_right_x) < 0.00001);
			assert(std::abs(vp->vertices[1][1] - i_vertex_coord_right_y) < 0.00001);

			assert(std::abs(vp->vertices[2][0] - i_vertex_coord_top_x) < 0.00001);
			assert(std::abs(vp->vertices[2][1] - i_vertex_coord_top_y) < 0.00001);

#endif
		}

		if (output_flags & CGridDataArrays_Enums::NORMALS)
		{
			T *n = &(cGridDataArrays->triangle_normal_buffer[cell_write_index * 3 * 3]);

			n[0*3+0] = i_leftVertexData->scalars[0];
			n[0*3+1] = i_leftVertexData->scalars[1];
			n[0*3+2] = i_leftVertexData->scalars[2];

			n[1*3+0] = i_rightVertexData->scalars[0];
			n[1*3+1] = i_rightVertexData->scalars[1];
			n[1*3+2] = i_rightVertexData->scalars[2];

			n[2*3+0] = i_topVertexData->scalars[0];
			n[2*3+1] = i_topVertexData->scalars[1];
			n[2*3+2] = i_topVertexData->scalars[2];
		}

		if (output_flags & (CGridDataArrays_Enums::VALUE0 |CGridDataArrays_Enums::VALUE3))
		{
			if (cGridDataArrays->dof_variables_multiplier == 3)
			{
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 0] = vp->displacements[0];
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 1] = vp->displacements[1];
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 2] = vp->displacements[2];
			}
			else
			{
				cGridDataArrays->dof_element[3][cell_write_index] = vp->displacements[0];
			}

		}

		cell_write_index++;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(
				i_vertex_coord_left_x, i_vertex_coord_left_y,
				i_vertex_coord_right_x, i_vertex_coord_right_y,
				i_vertex_coord_top_x, i_vertex_coord_top_y
			);
#endif
	}


	inline void traversal_pre_hook()
	{
		cTemporaryVerticesTopDownReader.setup(cTemporaryVertices);
		cTemporaryVerticesBottomUpReader.setup(cTemporaryVertices);

		read_counter = 0;
	}


	inline void traversal_post_hook()
	{
		cTemporaryVertices->clear();

	}


	inline void setup(
			CStack<CVertexPackage> *io_cTemporaryVertices,
			CGridDataArrays<3,6> *o_GridDataArrays,
			CDatasets *i_cDatasets,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing_mode
	) {
		cTemporaryVertices = io_cTemporaryVertices;
		cGridDataArrays = o_GridDataArrays;
		cDatasets = i_cDatasets;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
		preprocessing_mode = i_preprocessing_mode;

	}


	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
		COutputGridDataArrays_ContinousSurface_2Pass_1stPass &parent_kernel
	) {
		assert(false);
	}
};


}
}

#endif
