/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: 13 April, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_VELOCITYUPWINDING_HPP_
#define CFLUXSOLVER_VELOCITYUPWINDING_HPP_


template <typename T>
class CFluxSolver_VelocityUpwinding
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a upwinding method based on the velocity component in combination with a numerical
	 * diffusion similar to the lax-friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	inline void op_edge_edge(
			const CSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeData &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed
			T i_gravitational_constant				///< gravitational constant
	)
	{

#if 1
		const T epsilon = 0.05;

		T max_bathymetry = std::max(i_edgeData_left.b, i_edgeData_right.b);

		T absolute_h_left = i_edgeData_left.b + i_edgeData_left.h;
		T absolute_h_right = i_edgeData_right.b + i_edgeData_right.h;

		T moving_h_left = absolute_h_left - max_bathymetry;
		T moving_h_right = absolute_h_right - max_bathymetry;

		if (	moving_h_left <= epsilon	||
				moving_h_right <= epsilon
		)
		{
			o_edgeFlux_right->h = 0;
			o_edgeFlux_right->hu = 0;
			o_edgeFlux_right->hv = 0;

			o_edgeFlux_left->h = 0;
			o_edgeFlux_left->hu = 0;
			o_edgeFlux_left->hv = 0;

			*o_max_wave_speed_left = 0;
			*o_max_wave_speed_right = 0;
			return;
		}

#else
		T moving_h_left = i_edgeData_left.h;
		T moving_h_right = i_edgeData_right.h;
#endif

		T left_vx = i_edgeData_left.hu/moving_h_left;
		T right_vx = i_edgeData_right.hu/moving_h_right;

#if 0
		/**
		 * TODO: update to non-intrinsic version below
		 */
// 4 ops
		__m128 m_left_right_hu_hv = _mm_set_ps(i_edgeData_right.hv, i_edgeData_right.hu, i_edgeData_left.hv, i_edgeData_left.hu);
		__m128 m_left_h_right_h = _mm_set_ps(i_edgeData_right.h, i_edgeData_right.h, i_edgeData_left.h, i_edgeData_left.h);
		__m128 m_left_right_vx_vy = _mm_div_ps(m_left_right_hu_hv, m_left_h_right_h);

		__m128 m_gravity = _mm_set1_ps(i_gravitational_constant);

		/*
		T left_lambda = std::sqrt<T>(left_vx*left_vx+left_vy*left_vy) + std::sqrt<T>(config.gravity*i_edgeData_left.h);
		T right_lambda = std::sqrt<T>(right_vx*right_vx+right_vy*right_vy) + std::sqrt<T>(config.gravity*i_edgeData_right.h);
		*/

// 7*2 ops (scalar)
		__m128 m_left_up = _mm_dp_ps(m_left_right_vx_vy, m_left_right_vx_vy, 16*3 + 7);
		__m128 m_left_bottom = _mm_dp_ps(m_left_right_vx_vy, m_left_right_vx_vy, 16*12 + 7);

		__m128 m_right_side = _mm_mul_ps(m_gravity, m_left_h_right_h);

		__m128 m_sqrt_helper1 = _mm_blend_ps(m_left_up, m_right_side, 2);		// r0, r1
		__m128 m_sqrt_helper2 = _mm_blend_ps(m_left_bottom, m_right_side, 8);	// r2, r3
		__m128 m_sqrt = _mm_sqrt_ps(_mm_blend_ps(m_sqrt_helper1, m_sqrt_helper2, 12));

		T sqrt_stuff[4];
		_mm_store_ps(sqrt_stuff, m_sqrt);

		/*
		T lambda = std::max(left_lambda, right_lambda);
		*/
		T lambda = std::max(sqrt_stuff[0] + sqrt_stuff[1], sqrt_stuff[2] + sqrt_stuff[3]);

#else

		/**
		 * compute eigenvalues for left and outer DOFs
		 */
		T left_grav_h = std::sqrt(i_gravitational_constant*moving_h_left);
		T right_grav_h = std::sqrt(i_gravitational_constant*moving_h_right);

		T left_lambda_add = std::abs(left_vx + left_grav_h);
		T left_lambda_sub = std::abs(left_vx - left_grav_h);

		T right_lambda_add = std::abs(right_vx + right_grav_h);
		T right_lambda_sub = std::abs(right_vx - right_grav_h);

		T left_lambda = std::max(left_lambda_add, left_lambda_sub);
		T right_lambda = std::max(right_lambda_add, right_lambda_sub);

		T lambda = std::max(left_lambda, right_lambda);

#endif

		/**
		 * upwinding
		 */
		if (left_vx > right_vx)
		{
			o_edgeFlux_left->h = i_edgeData_left.hu;
			o_edgeFlux_left->hu = (left_vx*i_edgeData_left.hu + (T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left));
			o_edgeFlux_left->hv = 0;
		}
		else
		{
			o_edgeFlux_left->h = (T)0.5*i_edgeData_right.hu;
			o_edgeFlux_left->hu = (right_vx*i_edgeData_right.hu + (T)0.5*i_gravitational_constant*(moving_h_right*moving_h_right));
			o_edgeFlux_left->hv = 0;
		}

		o_edgeFlux_right->h = -o_edgeFlux_left->h;
		o_edgeFlux_right->hu = -o_edgeFlux_left->hu;
		o_edgeFlux_right->hv = -o_edgeFlux_left->hv;

#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==0
		// remove fluxes for 0th order basis functions

		o_edgeFlux_left->h -= i_edgeData_left.hu;
		o_edgeFlux_left->hu -= left_vx*i_edgeData_left.hu + (T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left);

		o_edgeFlux_right->h += i_edgeData_right.hu;
		o_edgeFlux_right->hu += right_vx*i_edgeData_right.hu + (T)0.5*i_gravitational_constant*(moving_h_right*moving_h_right);
#endif



		/*
		 * add numerical diffusion for stability reason
		 */
#if 1
		T diffusion_h = (T)0.5*(T)lambda*(moving_h_left - moving_h_right);
		T diffusion_hu = (T)0.5*(T)lambda*(i_edgeData_left.hu - i_edgeData_right.hu);

		o_edgeFlux_right->h -= diffusion_h;
		o_edgeFlux_right->hu -= diffusion_hu;

		o_edgeFlux_left->h += diffusion_h;
		o_edgeFlux_left->hu += diffusion_hu;
#endif


		/**
		 * CFL condition
		 */
		*o_max_wave_speed_left = lambda;
		*o_max_wave_speed_right = *o_max_wave_speed_left;
//		o_max_wave_speed = std::sqrt(o_edgeFlux_left.hu*o_edgeFlux_left.hu + o_edgeFlux_left.hv*o_edgeFlux_left.hv);
	}


	template <int N>
	void op_edge_edge(
			const CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
		T wave_speed_left = 0;
		T wave_speed_right = 0;

		for (int i = 0; i < N; i++)
		{
			T l_wave_speed_left = 0;
			T l_wave_speed_right = 0;

			/*
			 * TODO: SIMD
			 */

			CSimulationNodeData i_left, i_right;

			i_left.h = i_edgeData_left.h[i];
			i_left.hu = i_edgeData_left.hu[i];
			i_left.hv = i_edgeData_left.hv[i];
			i_left.b = i_edgeData_left.b[i];

			i_right.h = i_edgeData_right.h[N-1-i];
			i_right.hu = -i_edgeData_right.hu[N-1-i];
			i_right.hv = -i_edgeData_right.hv[N-1-i];
			i_right.b = i_edgeData_right.b[N-1-i];

			CSimulationNodeData o_left, o_right;

			op_edge_edge(
					i_left,			///< edge data on left (left) edge
					i_right,		///< edge data on right (outer) edge

					&o_left,		///< output for left flux
					&o_right,		///< output for outer flux

					&l_wave_speed_left,				///< maximum wave speed
					&l_wave_speed_right,				///< maximum wave speed

					i_gravitational_constant				///< gravitational constant
			);

			o_edgeFlux_left->h[i] = o_left.h;
			o_edgeFlux_left->hu[i] = o_left.hu;
			o_edgeFlux_left->hv[i] = o_left.hv;
			o_edgeFlux_left->b[i] = o_left.b;

			o_edgeFlux_right->h[N-1-i] = o_right.h;
			o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
			o_edgeFlux_right->hv[N-1-i] = -o_right.hv;
			o_edgeFlux_right->b[N-1-i] = o_right.b;

			wave_speed_left = std::max(wave_speed_left, l_wave_speed_left);
			wave_speed_right = std::max(wave_speed_right, l_wave_speed_right);
		}

		*o_max_wave_speed_left = wave_speed_left;
		*o_max_wave_speed_right = wave_speed_right;
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
