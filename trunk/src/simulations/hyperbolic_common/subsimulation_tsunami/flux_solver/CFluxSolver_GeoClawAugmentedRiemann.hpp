/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_GEOCLAWAUGRIE_HPP_
#define CFLUXSOLVER_GEOCLAWAUGRIE_HPP_

#include <cmath>
#include <iostream>
#include <cassert>

#include "tsunami_solver/AugRieGeoClaw.hpp"


#define USE_NEW_INTERFACE 1

/**
 * \brief AugumentedRiemann GeoClaw flux solver
 */
template <typename T>
class CFluxSolver_GeoClawAugmentedRiemann
{
public:
	CFluxSolver_GeoClawAugmentedRiemann()
	{
	}



	/**
	 * This method is executed by the cell-data computing kernel method
	 * to get the net updates for given fluxes.
	 *
	 * IMPORTANT: The right edge flux component is already rotated!
	 */
#if 0
public:
	static
	inline void op_edge_edge(
			const CSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeData &i_edgeData_right,		///< edge data on right (outer) edge
			CSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeData *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			T *o_eigen_coefficients_left,		///< maximum eigen coefficient
			T *o_eigen_coefficients_right,		///< maximum eigen coefficient
#endif

			T i_gravitational_constant				///< gravitational constant
	)	{

#if USE_NEW_INTERFACE

		c_bind_rpn2(
			&(i_edgeData_left.h), &(i_edgeData_right.h),
			&(o_edgeFlux_left->h), &(o_edgeFlux_right->h)
		);
		*o_max_wave_speed_left = o_edgeFlux_left->b;
		*o_max_wave_speed_right = o_edgeFlux_right->b;

#else

		T max_wave_speeds[3];
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		T eigen_coefficients[3];
#endif

		T edgeData_left[3] = {i_edgeData_left.h, i_edgeData_left.hu, i_edgeData_left.b};
		T edgeData_right[3] = {i_edgeData_right.h, i_edgeData_right.hu, i_edgeData_right.b};

		T netUpdate_left[3];
		T netUpdate_right[3];

		c_bind_geoclaw_riemann_aug_JCP(
				1,
				edgeData_left,
				edgeData_right,
				SIMULATION_TSUNAMI_DRY_THRESHOLD,
				i_gravitational_constant,
				netUpdate_left,
				netUpdate_right,
				max_wave_speeds
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
				,
				eigen_coefficients
#endif
			);

		o_edgeFlux_left->h = netUpdate_left[0];
		o_edgeFlux_left->hu = netUpdate_left[1];
		o_edgeFlux_left->hv = 0;
		o_edgeFlux_left->b = netUpdate_left[2];

		o_edgeFlux_right->h = netUpdate_right[0];
		o_edgeFlux_right->hu = netUpdate_right[1];
		o_edgeFlux_right->hv = 0;
		o_edgeFlux_right->b = netUpdate_right[2];
/*
		T max_wave_speed = std::abs(max_wave_speeds[0]);
		max_wave_speed = std::max(max_wave_speed, std::abs(max_wave_speeds[1]));
		max_wave_speed = std::max(max_wave_speed, std::abs(max_wave_speeds[2]));

		*o_max_wave_speed_left = max_wave_speed;
		*o_max_wave_speed_right = max_wave_speed;
*/
		*o_max_wave_speed_left = std::abs(max_wave_speeds[0]);
		*o_max_wave_speed_right = std::abs(max_wave_speeds[2]);

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		*o_eigen_coefficients_left = eigen_coefficients[0];
		*o_eigen_coefficients_right = eigen_coefficients[2];
#endif


#if SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS != 1
		// add fluxes for higher order basis functions to convert "net-updates" to "flux-updates"
		o_edgeFlux_left->h	+= i_edgeData_left.hu;
		o_edgeFlux_left->hu	+= (i_edgeData_left.hu*i_edgeData_left.hu)/i_edgeData_left.h + (T)0.5*i_gravitational_constant*i_edgeData_left.h*i_edgeData_left.h;
		o_edgeFlux_left->hv	+= (i_edgeData_left.hu*i_edgeData_left.hv)/i_edgeData_left.h;

		o_edgeFlux_right->h		-= i_edgeData_right.hu;
		o_edgeFlux_right->hu	-= (i_edgeData_right.hu*i_edgeData_right.hu)/i_edgeData_right.h + (T)0.5*i_gravitational_constant*i_edgeData_right.h*i_edgeData_right.h;
		o_edgeFlux_right->hv	-= (i_edgeData_right.hu*i_edgeData_right.hv)/i_edgeData_right.h;
#endif

#endif
	}
#endif

public:
	template <int N>
	inline
	void op_edge_edge(
			CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			CSimulationNodeDataSOA<N> &i_edgeData_right,	///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,	///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			T *o_max_eigen_coefficient_left,		///< maximum eigen coefficient
			T *o_max_eigen_coefficient_right,		///< maximum eigen coefficient
#endif

			T i_gravitational_constant				///< gravitational constant
	)
	{

		if (N == 1)
		{
			/*
			 * only a single DOF per cell
			 */

#if USE_NEW_INTERFACE

			// assure correct alignment since we rely on that
			assert(i_edgeData_left.h + 3 == i_edgeData_left.b);

			c_bind_rpn2(
					i_edgeData_left.h, i_edgeData_right.h,
					o_edgeFlux_left->h, o_edgeFlux_right->h
				);

			*o_max_wave_speed_left = o_edgeFlux_left->b[0];
			*o_max_wave_speed_right = o_edgeFlux_right->b[0];

#else
			T edgeData_left[3] = {i_edgeData_left.h[0], i_edgeData_left.hu[0], i_edgeData_left.b[0]};
			T edgeData_right[3] = {i_edgeData_right.h[0], -i_edgeData_right.hu[0], i_edgeData_right.b[0]};


			T max_wave_speeds[3];
			T netUpdate_left[3];
			T netUpdate_right[3];

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
			T eigen_coefficients[3];
#endif

			c_bind_geoclaw_riemann_aug_JCP(
					1,
					edgeData_left,
					edgeData_right,
					SIMULATION_TSUNAMI_DRY_THRESHOLD,
					i_gravitational_constant,
					netUpdate_left,
					netUpdate_right,
					max_wave_speeds
	#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
					,
					eigen_coefficients
	#endif
				);

			o_edgeFlux_left->h[0] = netUpdate_left[0];
			o_edgeFlux_left->hu[0] = netUpdate_left[1];
			o_edgeFlux_left->hv[0] = 0;
			o_edgeFlux_left->b[0] = netUpdate_left[2];

			o_edgeFlux_right->h[0] = netUpdate_right[0];
			o_edgeFlux_right->hu[0] = -netUpdate_right[1];
			o_edgeFlux_right->hv[0] = 0;
			o_edgeFlux_right->b[0] = netUpdate_right[2];

			*o_max_wave_speed_left = std::abs(max_wave_speeds[0]);
			*o_max_wave_speed_right = std::abs(max_wave_speeds[2]);
#endif
			return;
		}
		else
		{
			std::cerr << "TODO: untested feature!" << std::endl;
			exit(-1);

#if 0
			T wave_speed_left = 0;
			T wave_speed_right = 0;


			for (int i = 0; i < N; i++)
			{
				T l_wave_speed_left = 0;
				T l_wave_speed_right = 0;

				/*
				 * Eigencoefficients:
				 *
				 * See "Finite Volume Methods for Hyperbolic Problems",
				 * 15.5 "An Alternative Wave-Propagation Implementation of Approximate Riemann Solvers"
				 * Page 334,
				 * \beta_i = Eigencoefficients
				 */
				T l_max_eigen_coefficient_left = 0;
				T l_max_eigen_coefficient_right = 0;


				/*
				 * TODO: SIMD
				 */
				CSimulationNodeData i_left, i_right;

				i_left.h = i_edgeData_left.h[i];
				i_left.hu = i_edgeData_left.hu[i];
				i_left.hv = i_edgeData_left.hv[i];
				i_left.b = i_edgeData_left.b[i];

				i_right.h = i_edgeData_right.h[N-1-i];
				i_right.hu = -i_edgeData_right.hu[N-1-i];
				i_right.hv = -i_edgeData_right.hv[N-1-i];
				i_right.b = i_edgeData_right.b[N-1-i];

				CSimulationNodeData o_left, o_right;

				op_edge_edge(
						i_left,			///< edge data on left (left) edge
						i_right,		///< edge data on right (outer) edge

						&o_left,		///< output for left flux
						&o_right,		///< output for outer flux

						&l_wave_speed_left,				///< maximum wave speed left
						&l_wave_speed_right,			///< maximum wave speed right

	#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
						&l_max_eigen_coefficient_left,	///< maximum eigen-coefficient left
						&l_max_eigen_coefficient_right,	///< maximum eigen-coefficient right
	#endif

						i_gravitational_constant		///< gravitational constant
				);

				o_edgeFlux_left->h[i] = o_left.h;
				o_edgeFlux_left->hu[i] = o_left.hu;
				o_edgeFlux_left->hv[i] = o_left.hv;
				o_edgeFlux_left->b[i] = o_left.b;

				o_edgeFlux_right->h[N-1-i] = o_right.h;
				o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
				o_edgeFlux_right->hv[N-1-i] = -o_right.hv;
				o_edgeFlux_right->b[N-1-i] = o_right.b;

				// determine maximum wave speed across the current edge for which the fluxes are computed
				wave_speed_left = std::max(wave_speed_left, l_wave_speed_left);
				wave_speed_right = std::max(wave_speed_right, l_wave_speed_right);

	//			assert(wave_speed_left >= 0);
	//			assert(wave_speed_right >= 0);

	#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
				// determine maximum eigen coefficient with augumented riemann solvers
				max_eigen_coefficient_left = std::max(max_eigen_coefficient_left, std::abs(l_max_eigen_coefficient_left));
				max_eigen_coefficient_right = std::max(max_eigen_coefficient_right, std::abs(l_max_eigen_coefficient_right));

	//			assert(max_eigen_coefficient_left >= 0);
	//			assert(max_eigen_coefficient_right >= 0);
	#endif
			}
			*o_max_wave_speed_left = wave_speed_left;
			*o_max_wave_speed_right = wave_speed_right;
#endif
		}

		assert(false);

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
//		*o_max_eigen_coefficient_left = eigen_coefficient[0];
//		*o_max_eigen_coefficient_right = eigen_coefficient[1];
#endif
	}
};



#endif
