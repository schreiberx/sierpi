/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFLUXSOLVER_AUGRIE_HPP_
#define CFLUXSOLVER_AUGRIE_HPP_

#include <cmath>
#include <iostream>
#include <cassert>

#include "tsunami_solver/AugRie.hpp"

/**
 * \brief AugumentedRiemann flux solver
 */
template <typename T>
class CFluxSolver_AugumentedRiemann	: public solver::AugRie<T>
{
	using solver::AugRie<T>::computeNetUpdates;

public:
	CFluxSolver_AugumentedRiemann()	:
		solver::AugRie<T>(
				SIMULATION_TSUNAMI_DRY_THRESHOLD,
				SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION,
				0.000001,
				10,
				SIMULATION_TSUNAMI_ZERO_THRESHOLD
			)
	{
	}

	/**
	 * This method is executed by the cellData computing kernel method
	 * to get the net updates for given fluxes
	 */
public:
	inline void op_edge_edge(
			const CSimulationNodeData &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeData &i_edgeData_right,		///< edge data on right (outer) edge
			CSimulationNodeData *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeData *o_edgeFlux_right,		///< output for outer flux
			T *o_max_wave_speed_left,					///< maximum wave speed
			T *o_max_wave_speed_right,					///< maximum wave speed
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
      T *o_eigen_coefficients_left,		///< eigen-coefficient of the first wave family
      T *o_eigen_coefficients_right,	///< eigen-coefficient of the third wave family
#endif
			T i_gravitational_constant				///< gravitational constant
	)
	{
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
      T l_eigen_coefficients[3]; //< eigen-coefficients of the three wave families
#endif
		solver::AugRie<T>::computeNetUpdates(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.hu, i_edgeData_right.hu,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left->h, o_edgeFlux_right->h,
				o_edgeFlux_left->hu, o_edgeFlux_right->hu,
				*o_max_wave_speed_left
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
       ,l_eigen_coefficients
#endif
		);

		*o_max_wave_speed_right = *o_max_wave_speed_left;

#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==1

#error "ugly hack"
//		T max_bathymetry = std::max(i_edgeData_left.b, i_edgeData_right.b);

//		T absolute_h_left = i_edgeData_left.b + i_edgeData_left.h;
//		T absolute_h_right = i_edgeData_right.b + i_edgeData_right.h;

//		T moving_h_left = absolute_h_left - max_bathymetry;
//		T moving_h_right = absolute_h_right - max_bathymetry;

		T left_vx = i_edgeData_left.hu/i_edgeData_left.h;
		T left_vy = i_edgeData_left.hv/i_edgeData_left.h;

		T right_vx = i_edgeData_right.hu/i_edgeData_left.h;
		T right_vy = i_edgeData_right.hv/i_edgeData_left.h;

		// remove fluxes for 0th order basis functions
/*
		o_edgeFlux_left.h += i_edgeData_left.hu;
		o_edgeFlux_left.hu += left_vx*i_edgeData_left.hu + (T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left);
		o_edgeFlux_left.hv += left_vx*i_edgeData_left.hu + left_vy*i_edgeData_left.hu;

		o_edgeFlux_right.h -= i_edgeData_right.hu;
		o_edgeFlux_right.hu -= right_vx*i_edgeData_right.hu + (T)0.5*i_gravitational_constant*(moving_h_right*moving_h_right);
		o_edgeFlux_right.hv -= right_vx*i_edgeData_right.hu + right_vy*i_edgeData_right.hu;
*/

		o_edgeFlux_left->h += i_edgeData_left.hu;
		o_edgeFlux_left->hu += left_vx*i_edgeData_left.hu + (T)0.5*i_gravitational_constant*(i_edgeData_left.h*i_edgeData_left.h);
		o_edgeFlux_left->hv += left_vx*i_edgeData_left.hu + left_vy*i_edgeData_left.hu;


		o_edgeFlux_right->h -= i_edgeData_right.hu;
		o_edgeFlux_right->hu -= right_vx*i_edgeData_right.hu + (T)0.5*i_gravitational_constant*(i_edgeData_right.h*i_edgeData_right.h);
		o_edgeFlux_right->hv -= right_vx*i_edgeData_right.hu + right_vy*i_edgeData_right.hu;
#endif

		o_edgeFlux_left->hv = 0;
		o_edgeFlux_right->hv = 0;
		
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
    // set eigen-coefficients, Remark: The strength of the second family / corrector wave is ignored.
		*o_eigen_coefficients_left = std::abs(l_eigen_coefficients[0]);
		*o_eigen_coefficients_right = std::abs(l_eigen_coefficients[2]);
#endif
	}



	template <int N>
	inline
	void op_edge_edge(
			const CSimulationNodeDataSOA<N> &i_edgeData_left,		///< edge data on left (left) edge
			const CSimulationNodeDataSOA<N> &i_edgeData_right,		///< edge data on right (outer) edge

			CSimulationNodeDataSOA<N> *o_edgeFlux_left,		///< output for left flux
			CSimulationNodeDataSOA<N> *o_edgeFlux_right,		///< output for outer flux

			T *o_max_wave_speed_left,				///< maximum wave speed
			T *o_max_wave_speed_right,				///< maximum wave speed
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
      T *o_eigen_coefficients_left,		///< eigen-coefficient of the first wave family
      T *o_eigen_coefficients_right,	///< eigen-coefficient of the third wave family
#endif
      T i_gravitational_constant				///< gravitational constant
	)
	{
		T wave_speed_left = 0;
		T wave_speed_right = 0;

#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		//! eigen-coefficients
    T l_eigen_coefficients[3];
#endif

		for (int i = 0; i < N; i++)
		{
			T l_wave_speed_left = 0;
			T l_wave_speed_right = 0;

			/*
			 * TODO: SIMD
			 */

			CSimulationNodeData i_left, i_right;

			i_left.h = i_edgeData_left.h[i];
			i_left.hu = i_edgeData_left.hu[i];
			i_left.hv = i_edgeData_left.hv[i];
			i_left.b = i_edgeData_left.b[i];

			i_right.h = i_edgeData_right.h[N-1-i];
			i_right.hu = -i_edgeData_right.hu[N-1-i];
			i_right.hv = -i_edgeData_right.hv[N-1-i];
			i_right.b = i_edgeData_right.b[N-1-i];

			CSimulationNodeData o_left, o_right;

			op_edge_edge(
					i_left,			///< edge data on left (left) edge
					i_right,		///< edge data on right (outer) edge

					&o_left,		///< output for left flux
					&o_right,		///< output for outer flux

					&l_wave_speed_left,				///< maximum wave speed
					&l_wave_speed_right,				///< maximum wave speed
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
					o_eigen_coefficients_left,		///< eigen-coefficient of the first wave family
					o_eigen_coefficients_right,	///< eigen-coefficient of the third wave family
#endif
					i_gravitational_constant				///< gravitational constant
			);

			o_edgeFlux_left->h[i] = o_left.h;
			o_edgeFlux_left->hu[i] = o_left.hu;
			o_edgeFlux_left->hv[i] = o_left.hv;
			o_edgeFlux_left->b[i] = o_left.b;

			o_edgeFlux_right->h[N-1-i] = o_right.h;
			o_edgeFlux_right->hu[N-1-i] = -o_right.hu;
			o_edgeFlux_right->hv[N-1-i] = -o_right.hv;
			o_edgeFlux_right->b[N-1-i] = o_right.b;

			wave_speed_left = std::max(wave_speed_left, l_wave_speed_left);
			wave_speed_right = std::max(wave_speed_right, l_wave_speed_right);
		}
		
#if CONFIG_TSUNAMI_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
    // Zeroth degree basis functions only
    assert(N == 1);
#endif

		*o_max_wave_speed_left = wave_speed_left;
		*o_max_wave_speed_right = wave_speed_right;
	}
};



#endif
