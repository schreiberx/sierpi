/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: July 19, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#if CONFIG_ENABLE_MPI
	#include <mpi.h>
#endif

#include <cassert>
#include <cmath>
#include <stdexcept>

#include "netcdf/CDataSamplingSet_SimpleNetCDF.hpp"
#include "CSimpleNetCDF_cubedSphere.hpp"
#include "../../../libsierpi/grid/CCube_To_Sphere_Projection.hpp"



CSimpleNetCDF_cubedSphere::CSimpleNetCDF_cubedSphere(
		int i_verbosity_level
)	:
	verbosity_level(i_verbosity_level),
	is_dataset_loaded(false)
{
	for (int i = 0; i < 6; i++)
	{
		cSimpleNetCDF_TerrainData_private[i] = nullptr;
		cSimpleNetCDF_SurfaceDisplacementData_private[i] = nullptr;
	}
}

bool CSimpleNetCDF_cubedSphere::loadDatasets(
		const std::vector<std::string> &i_datasets,
		int i_dataset_id
)
{
	clear();

	if (i_datasets.size() == 0)
		return false;

	if (i_datasets.size() < 2)
		throw(std::runtime_error("Number of datasets != 2"));

	is_dataset_loaded = false;

	// cube face files
	const char* cube_faces[] = {
			"front",
			"right",
			"back",
			"left",
			"top",
			"bottom"
	};

	// load netcdf cube face data
	for (int i = 0; i < 6; i++)
	{
		std::string face_file = i_datasets[0];
		face_file += "/";
		face_file += cube_faces[i];
		face_file += ".nc";

//		if (verbosity_level > 5)
			std::cout << "Loading cube face file " << face_file << std::endl;

		cSimpleNetCDF_TerrainData_private[i] = new CDataSamplingSet_SimpleNetCDF<T>(verbosity_level);
		if (cSimpleNetCDF_TerrainData_private[i]->loadDataset(face_file.c_str()) == false)
		{
			std::cerr << "ERROR while loading cube face file " << face_file << std::endl;
			clear();
			return false;
		}
	}


	/*
	 * provide domain sizes which match to the 2D projected cubed sphere faces
	 */
	domain_min_x = 0;
	domain_max_x = 8;

	domain_min_y = 0;
	domain_max_y = 8;

	domain_length_x = 8;
	domain_length_y = 8;

	domain_center_x = 0;
	domain_center_y = 0;

#if 0
	if (i_datasets[1].length() > 0)
	{
		cSimpleNetCDF_SurfaceDisplacementData_private = new CDataSamplingSet_SimpleNetCDF<T>(verbosity_level);
		if (cSimpleNetCDF_SurfaceDisplacementData_private->loadDataset(i_datasets[1].c_str()) == false)
		{
			delete cSimpleNetCDF_TerrainData_private;
			cSimpleNetCDF_TerrainData_private = nullptr;
			return false;
		}

		/**
		 * DISPLACEMENTS
		 */
		displacements_min_x = cSimpleNetCDF_SurfaceDisplacementData_private->getXMin();
		displacements_max_x = cSimpleNetCDF_SurfaceDisplacementData_private->getXMax();

		displacements_min_y = cSimpleNetCDF_SurfaceDisplacementData_private->getYMin();
		displacements_max_y = cSimpleNetCDF_SurfaceDisplacementData_private->getYMax();

		displacements_size_x = displacements_max_x - displacements_min_x;
		displacements_size_y = displacements_max_y - displacements_min_y;

		displacements_center_x = ((T)0.5)*(displacements_min_x+displacements_max_x);
		displacements_center_y = ((T)0.5)*(displacements_min_y+displacements_max_y);
	}
#endif

	is_dataset_loaded = true;

	outputVerboseInformation();

	return true;
}


void CSimpleNetCDF_cubedSphere::clear()
{
	for (int i = 0; i < 6; i++)
	{
		if (cSimpleNetCDF_TerrainData_private[i] != nullptr)
		{
			delete cSimpleNetCDF_TerrainData_private[i];
			cSimpleNetCDF_TerrainData_private[i] = nullptr;
		}

		if (cSimpleNetCDF_SurfaceDisplacementData_private[i] != nullptr)
		{
			delete cSimpleNetCDF_SurfaceDisplacementData_private[i];
			cSimpleNetCDF_SurfaceDisplacementData_private[i] = nullptr;
		}
	}

	is_dataset_loaded = false;
}


CSimpleNetCDF_cubedSphere::~CSimpleNetCDF_cubedSphere()
{
#if 0
	if (cSimpleNetCDF_TerrainData_private != nullptr)
		delete cSimpleNetCDF_TerrainData_private;

	if (cSimpleNetCDF_SurfaceDisplacementData_private != nullptr)
		delete cSimpleNetCDF_SurfaceDisplacementData_private;
#endif
}



void CSimpleNetCDF_cubedSphere::outputVerboseInformation()
{
	std::cout << "Simple NetCDF for Cubed Sphere:" << std::endl;
	std::cout << " + domain min/max x: " << domain_min_x << ", " << domain_max_x << std::endl;
	std::cout << " + domain min/max y: " << domain_min_y << ", " << domain_max_y << std::endl;
	std::cout << " + domain length x/y: " << domain_length_x << ", " << domain_length_y << std::endl;
	std::cout << " + domain center x/y: " << domain_center_x << ", " << domain_center_y << std::endl;

	size_t min_array_size = std::min(cSimpleNetCDF_TerrainData_private[0]->getArraySizeX(), cSimpleNetCDF_TerrainData_private[0]->getArraySizeY());

	T log2_array_size = std::log(static_cast<T>(min_array_size)) / std::log((T)2.0);
	std::cout << " + Max meaningful cartesian-grid refinement depth: " << log2_array_size << std::endl;
	std::cout << " + Max meaningful triangle-grid refinement depth: " << (log2_array_size*2) << std::endl;
	std::cout << " + Terrain raw-data cell size: ("
			<< (cSimpleNetCDF_TerrainData_private[0]->getXMax() - cSimpleNetCDF_TerrainData_private[0]->getXMin())/(T)cSimpleNetCDF_TerrainData_private[0]->getArraySizeX() << ", "
			<< (cSimpleNetCDF_TerrainData_private[0]->getYMax() - cSimpleNetCDF_TerrainData_private[0]->getYMin())/(T)cSimpleNetCDF_TerrainData_private[0]->getArraySizeY() << ")"
			<< std::endl;
}



void CSimpleNetCDF_cubedSphere::getOriginAndSize(
		T *o_translate_x,
		T *o_translate_y,

		T *o_size_x,
		T *o_size_y
)
{
	if (!is_dataset_loaded)
		throw(std::runtime_error("No bathymetry dataset loaded!"));

	// sphere
	*o_translate_x = 0;
	*o_translate_y = 0;

	*o_size_x = 8;
	*o_size_y = 8;
}





/**
 * get nodal data for given coordinate
 */
void CSimpleNetCDF_cubedSphere::getNodalData(
		T i_x,
		T i_y,
		T i_level_of_detail,
		CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data
)
{
#if 0
	if (cSimpleNetCDF_SurfaceDisplacementData_private)
	{
		o_nodal_data->b = cSimpleNetCDF_TerrainData_private->getFloat2D(i_x, i_y, i_level_of_detail);
		o_nodal_data->hu = 0;
		o_nodal_data->hv = 0;
//		o_nodal_data->h = -o_nodal_data->b + cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D(i_x, i_y, i_level_of_detail);
		o_nodal_data->h = std::max(-o_nodal_data->b + cSimpleNetCDF_SurfaceDisplacementData_private->getFloat2D_BorderValue(i_x, i_y, i_level_of_detail), (T)0);
		return;
	}
#endif
	throw(std::runtime_error("No bathymetry data loaded"));
}


/**
 * get dataset value for given coordinate and dataset id
 */
CHyperbolicTypes::CSimulationTypes::T CSimpleNetCDF_cubedSphere::getDatasetValue(
		int i_dataset_id,		///< id of dataset to get value from
		T i_x,					///< x-coordinate in model-space
		T i_y,					///< y-coordinate in model-space
		T i_level_of_detail		///< level of detail (0 = coarsest level)
)
{
	switch(i_dataset_id)
	{
	case 0:
	{
		if (!is_dataset_loaded)
			throw(std::runtime_error("No bathymetry data loaded"));

		CCube_To_Sphere_Projection::EFaceType face_id = CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(i_x, i_y);

		CCube_To_Sphere_Projection::project2DTo2DFace(face_id, &i_x, &i_y);

		i_x = (i_x + (T)1)*(T)0.5;
		i_y = (i_y + (T)1)*(T)0.5;

		if (face_id ==CCube_To_Sphere_Projection::EFaceType::E_ERROR)
			return -1;

		return cSimpleNetCDF_TerrainData_private[face_id]->getFloat2D(i_x, i_y, i_level_of_detail);
	}

	case 1:
#if 0
		if (cSimpleNetCDF_SurfaceDisplacementData_private == nullptr)
			throw(std::runtime_error("No displacement data data loaded"));

		return cSimpleNetCDF_SurfaceDisplacementData_private[i]->getFloat2D_BorderValue(i_x, i_y, i_level_of_detail);
#endif
		throw(std::runtime_error("No displacement data loaded"));

	default:
		throw(std::runtime_error("Invalid dataset id"));
	}
}




/**
 * get nodal data for benchmark
 */
bool CSimpleNetCDF_cubedSphere::getBenchmarkNodalData(
		T i_x,
		T i_y,
		T i_level_of_detail,
		T i_timestamp,
		CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data
)
{
	return false;
}



/**
 * get boundary data at given position
 */
bool CSimpleNetCDF_cubedSphere::getBoundaryData(
		T i_x,
		T i_y,
		T i_level_of_detail,
		T i_timestamp,
		CHyperbolicTypes::CSimulationTypes::CNodeData *o_nodal_data
)
{
	throw(std::runtime_error("No boundary implemented yet"));
	return false;
}



bool CSimpleNetCDF_cubedSphere::isDatasetLoaded()
{
	return is_dataset_loaded;
}
