/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 22, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK4_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK4_HPP_

#include "global_config.h"

#include "libsierpi/generic_tree/CGeneric_TreeNode.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/cluster/CCluster_ExchangeFluxCommData.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals
{
public:


	template<
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler
		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data
		typename CCellData,				/// type of cell data
		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T
	>
	static inline void singleAction(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size,
			int t_rungeKuttaStage
	)
	{
		typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;
					CSimulation_Cluster *cSimulation_Cluster = i_node->cSimulation_Cluster;

					size_t stack_size = i_node->cStacks->cell_data_stacks.forward.getMaxNumberOfElements();

					if (t_rungeKuttaStage == 1)
					{
						/*
						 * allocate additional element stacks for runge-kutta buffer
						 */
						if (stack_size != cSimulation_Cluster->cStackCellDataRK_V_0.getMaxNumberOfElements())
						{
							// resize cStackCellDataRK2 if all element data stack elements don't fit into it
							cSimulation_Cluster->cStackCellDataRK_V_0.resize(stack_size);

							for (int i = 0; i < SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER; i++)
								cSimulation_Cluster->cStackCellDataRK_D[i].resize(stack_size);
						}

						/*
						 * Backup U values to V[0]
						 */
						i_node->cStacks->cell_data_stacks.forward.copyStackElements(&(cSimulation_Cluster->cStackCellDataRK_V_0));
					}

					// compute D_i based on V_i
					(cSimulation_Cluster->*(i_simulationSubClass)).actionFirstPass(i_node->cStacks);
				}
		);



#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using cCluster_UniqueIds as relation and compute the flux
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pushEdgeCommData_DM_pass1();
			}
		);
#endif


#if CONFIG_ENABLE_MPI
		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using cCluster_UniqueIds as relation and compute the flux
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
			}
		);
#endif

		T cfl1_reduce_value = std::numeric_limits<T>::infinity();

		/*
		 * middle pass: pull edge comm data & compute global timestep size
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode, T *o_reduceValue)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// firstly, pull edge communication data from adjacent sub-clusters and compute fluxes
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluation_SM_and_if_DM_Wait();

				// run computation based on newly set-up stacks
				(node->cSimulation_Cluster->*(i_simulationSubClass)).actionMiddlePass_computeClusterBorderCFL(node->cStacks, o_reduceValue);
			},
			&(sierpi::CReduceOperators::MIN<T>),	// use minimum since the minimum timestep has to be selected
			&cfl1_reduce_value
		);

		/**
		 * timestep size
		 */
		CONFIG_DEFAULT_FLOATING_POINT_TYPE timestep_size;

		if (t_rungeKuttaStage == 1)
		{
			timestep_size = i_cfl_value * cfl1_reduce_value;

			if (i_cfl_value < 0)
				timestep_size = -i_cfl_value;

			*o_timestep_size = timestep_size;
		}
		else
		{
			// use timestep size from 1st wavespeed computation
			timestep_size = *o_timestep_size;
		}


		/*
		 * second pass: setting all edges to type 'old' reads data from adjacent clusters
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;
				CSimulation_Cluster *cSimulation_Cluster = i_node->cSimulation_Cluster;

				/*
				 * read data and do timestep
				 */
				(cSimulation_Cluster->*(i_simulationSubClass)).actionSecondPass_Parallel(i_node->cStacks, timestep_size);

				// i_node->cStacks->cell_data_stacks.forward now contains the updated D_i stored
				i_node->cStacks->cell_data_stacks.forward.swap(cSimulation_Cluster->cStackCellDataRK_D[t_rungeKuttaStage-1]);

				// copy data to computation stack used for next iteration
				cSimulation_Cluster->cStackCellDataRK_V_0.copyStackElements(&(i_node->cStacks->cell_data_stacks.forward));

				const size_t n = cSimulation_Cluster->cStackCellDataRK_V_0.getNumberOfElementsOnStack();

				/*
				 * first pass
				 */
				if (t_rungeKuttaStage == 1)
				{
					for (size_t i = 0; i < n; i++)
					{
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),		// V_2 +
								cSimulation_Cluster->cStackCellDataRK_D[0].getElementAtIndex(i),		// D_1
								timestep_size*(T)0.5													// a_{2,1}
							);
					}
				}


				/*
				 * second pass
				 */
				if (t_rungeKuttaStage == 2)
				{
					for (size_t i = 0; i < n; i++)
					{
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),		// V_0 +
								cSimulation_Cluster->cStackCellDataRK_D[1].getElementAtIndex(i),		// D_1
								timestep_size*(T)0.5													// a_{3,1}
							);
					}
				}


				/*
				 * third pass
				 */
				if (t_rungeKuttaStage == 3)
				{
					for (size_t i = 0; i < n; i++)
					{
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),		// V_0 +
								cSimulation_Cluster->cStackCellDataRK_D[2].getElementAtIndex(i),		// D_3
								timestep_size*(T)1.0													// b_{3}
							);
					}
				}


				/*
				 * last pass: b
				 */
				if (t_rungeKuttaStage == SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER)
				{
					for (size_t i = 0; i < n; i++)
					{
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),
								cSimulation_Cluster->cStackCellDataRK_D[0].getElementAtIndex(i),
								timestep_size*(T)(1.0/6.0)
							);
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),
								cSimulation_Cluster->cStackCellDataRK_D[1].getElementAtIndex(i),
								timestep_size*(T)(2.0/6.0)
							);
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),
								cSimulation_Cluster->cStackCellDataRK_D[2].getElementAtIndex(i),
								timestep_size*(T)(2.0/6.0)
							);
						TFluxCommTraversator::CKernelClass::rk_cell_update(
								&(i_node->cStacks->cell_data_stacks.forward.getElementAtIndex(i)),
								cSimulation_Cluster->cStackCellDataRK_D[3].getElementAtIndex(i),
								timestep_size*(T)(1.0/6.0)
							);
					}
				}

				/*
				 * shutdown in last pass
				 */
				i_node->cStacks->valid_backward_adaptivity_state_flags = true;
/*
				if (t_rungeKuttaStage == SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER)
				{
					i_node->cStacks->valid_backward_adaptivity_state_flags = false;
					i_node->cStacks->adaptivity_state_flag_stacks.backward.clear();
				}
*/
			}
		);
	}



	/**
	 * run the edge comm traversals
	 */
	template<
		typename CCellData,				/// cell data
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,	/// type of user-defined cluster handler

		typename TFluxCommTraversator,	/// Traversator including kernel
		typename CEdgeData,				/// type of edge communication data

		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T						/// value to use for reduction
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size
	)
	{
		for (int stage = 1; stage <= SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER; stage++)
		{
			singleAction
				<
					CCluster_TreeNode_,
					CSimulation_Cluster,
					TFluxCommTraversator,	/// Traversator including kernel
					CEdgeData,				/// type of edge communication data
					CCellData,
					CStackAccessors,		/// accessors to adjacent stacks
					T						/// ...
				>(i_simulationSubClass, i_simulationFluxCommSubClass, i_cDomainClusters, i_cfl_value, o_timestep_size, stage);
		}
	}
};

#endif
