/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CHelper_GenericParallelAdaptivityTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_

#include "global_config.h"

#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/cluster/CCluster_ExchangeEdgeCommData.hpp"
#include "libsierpi/generic_tree/CGeneric_TreeNode.hpp"
#include <cmath>


/**
 * \brief this class implements helper methods which abstract the different phases for adaptive traversals
 *
 * in particular:
 *  a) first forward traversal
 *  b) finished?
 *    c) edge communication
 *    d) backward traversal
 *       forward traversal
 *    finished? not->goto (b)
 *  e) last traversal
 *
 */
class CHelper_GenericParallelAdaptivityTraversals
{
public:
	/**
	 * run the adaptive method
	 */
	template<
		bool i_avoid_scan_based_traversals,
		typename CCluster_TreeNode_,
		typename CSimulation_Cluster,
		typename CAdaptiveTraversatorClass,
		typename CStackAccessors
	>
	static void action(
			CAdaptiveTraversatorClass CSimulation_Cluster::*p_adaptiveTraversator_SubClass,
			sierpi::CCluster_ExchangeEdgeCommData<CCluster_TreeNode_, char, CStackAccessors> CSimulation_Cluster::*p_simulationExchangeEdgeComm_SubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainCluster,

			long long *o_number_of_local_grid_cells
	)
	{
		typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

		/**
		 * if this variable is set to true, there are still some hanging nodes to be deleted.
		 *
		 * IMPORTANT: initialize with false for MPI zero-cluster nodes and for scan traversals!
		 */
		bool remaining_hanging_node = false;

		/****************************************************
		 * FIRST PASS
		 ****************************************************/
		/**
		 * the first pass is executed to mark the corresponding triangles for begin split or coarsened.
		 * also edge refinement/coarsen information is pushed/pulled to the stack.
		 *
		 * finally, the data for the adjacent clusters is stored to the stacks and need to be exchanged.
		 */

		/**
		 * the first pass checks, whether a triangle should be refined or coarsened.
		 * therefore the element data is necessary. After this step, the elementdoTraversal_SimulationHandler_SubClass_Method_Reduce
		 * data may not be touched in the middle passes.
		 */
		// first pass: setting all edges to type 'new' creates data to exchange with neighbors

		if (i_avoid_scan_based_traversals)
		{
			i_cDomainCluster.traverse_GenericTreeNode_Reduce_Parallel
			(
				[=](CGeneric_TreeNode_ *node, bool *o_reduceValue)
				{
					*o_reduceValue = (node->cCluster_TreeNode->cSimulation_Cluster->*(p_adaptiveTraversator_SubClass)).actionFirstTraversal(node->cCluster_TreeNode->cStacks);
				},
				&sierpi::CReduceOperators::OR<bool>,
				&remaining_hanging_node
			);
		}
		else
		{
			i_cDomainCluster.traverse_GenericTreeNode_Reduce_Parallel_Scan
			(
				[=](CGeneric_TreeNode_ *node, bool *o_reduceValue)
				{
					*o_reduceValue = (node->cCluster_TreeNode->cSimulation_Cluster->*(p_adaptiveTraversator_SubClass)).actionFirstTraversal(node->cCluster_TreeNode->cStacks);
				},
				&sierpi::CReduceOperators::OR<bool>,
				&remaining_hanging_node
			);
		}

		remaining_hanging_node = sierpi::CGlobalComm::reduceBooleanOr(remaining_hanging_node);

		/****************************************************
		 * MIDDLE PASSES
		 ****************************************************/
		/**
		 * this is the loop over all clusters to avoid all hanging nodes
		 *
		 * there's already some data prepared on the stack from the first pass.
		 */
		int c = 0;
		while (remaining_hanging_node)
		{
			if (c > 10)
				std::cout << "Warning: more than " << c << " adaptive middle traversals. Maybe something went wrong" << std::endl;

			/*
			 * WARNING: moving pullEdgeCommData() to the next parallel reduce  traversal does not work!!!
			 *
			 * Since both middle traversals (forward/backward) write to the edgeComm but
			 * _ALSO_ the exchangeEdgeComm stacks, pulling the edge comm data within the next traversal is not allowed!
			 */

			// first pass: setting all edges to type 'new' creates data to exchange with neighbors

#if CONFIG_ENABLE_MPI
			/*
			 * TODO: run this on one thread while running the the shared memory communications on another thread
			 */
			i_cDomainCluster.traverse_GenericTreeNode_Serial(
				[=](CGeneric_TreeNode_ *node)
				{
					(node->cCluster_TreeNode->cSimulation_Cluster->*p_simulationExchangeEdgeComm_SubClass).exchangeEdgeCommData_DM_pass1();
				}
			);
#endif

			if (i_avoid_scan_based_traversals)
			{
				i_cDomainCluster.traverse_GenericTreeNode_Parallel(
					[=](CGeneric_TreeNode_ *node)
					{
						(node->cCluster_TreeNode->cSimulation_Cluster->*p_simulationExchangeEdgeComm_SubClass).exchangeEdgeCommData_SM_and_DM_Wait();
					}
				);
			}
			else
			{
				i_cDomainCluster.traverse_GenericTreeNode_Parallel_Scan(
					[=](CGeneric_TreeNode_ *node)
					{
						(node->cCluster_TreeNode->cSimulation_Cluster->*p_simulationExchangeEdgeComm_SubClass).exchangeEdgeCommData_SM_and_DM_Wait();
					}
				);
			}


			/*
			 * The second traversal actually consists out of 2 passes:
			 *
			 * 1) The first BACKWARD TRAVERSAL reads data from the adjacent clusters.
			 *    all cluster borders are set t type 'old'
			 *
			 * 2) The second FORWARD TRAVERSAL spreads data to adjacent clusters.
			 *    if SecondPass returns true, there are
			 *    still refinements to do and we rerun this loop.
			 *    then all cluster borders are set to type 'new'
			 */

#if CONFIG_ENABLE_MPI
			/*
			 * TODO: run this on one thread while running the the shared memory communications on another thread
			 */
			i_cDomainCluster.traverse_GenericTreeNode_Serial_Reversed(
				[=](CGeneric_TreeNode_ *node)
				{
					(node->cCluster_TreeNode->cSimulation_Cluster->*p_simulationExchangeEdgeComm_SubClass).exchangeEdgeCommData_DM_pass2();
				}
			);
#endif

			/*
			 * set to false to avoid any MPI node zero-cluster problems since those don't use any reduction operation
			 * this also has to be set for scan based traversals
			 */
			remaining_hanging_node = false;

			if (i_avoid_scan_based_traversals)
			{
				i_cDomainCluster.traverse_GenericTreeNode_Reduce_Parallel(
					[=](CGeneric_TreeNode_ *node, bool *o_reduceValue)
					{
						*o_reduceValue = (node->cCluster_TreeNode->cSimulation_Cluster->*(p_adaptiveTraversator_SubClass)).actionMiddleTraversals_Parallel(node->cCluster_TreeNode->cStacks);
					},
					&(sierpi::CReduceOperators::OR<bool>),
					&remaining_hanging_node
				);
			}
			else
			{
				i_cDomainCluster.traverse_GenericTreeNode_Reduce_Parallel_Scan(
					[=](CGeneric_TreeNode_ *node, bool *o_reduceValue)
					{
						*o_reduceValue = (node->cCluster_TreeNode->cSimulation_Cluster->*(p_adaptiveTraversator_SubClass)).actionMiddleTraversals_Parallel(node->cCluster_TreeNode->cStacks);
					},
					&(sierpi::CReduceOperators::OR<bool>),
					&remaining_hanging_node
				);
			}

			remaining_hanging_node = sierpi::CGlobalComm::reduceBooleanOr(remaining_hanging_node);

			c++;
		}


		/****************************************************
		 * LAST PASS
		 ****************************************************/
		/**
		 * after we have the states to create a regular grid, the state-refinement information is stored on the stack.
		 *
		 * !!! this information is used later on to change the information about the adjacent clusters !!!
		 *
		 * this traversal do the actual refinement
		 */
		long long l_number_of_local_cells = 0;


		if (i_avoid_scan_based_traversals || (!CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL && !CONFIG_ENABLE_ALL_TO_ALL_LOADBALANCING))
		{
			/*
			 * execute this only, if
			 * * scan based traversals should be avoided
			 * * no forced traversals are activated ANDif no all-to-all load balancing is required since this relies on the traversals
			 */
			i_cDomainCluster.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
				[=](	CGeneric_TreeNode_ *i_cGenericTreeNode,
						long long *o_number_of_local_triangles
				) {
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;

					CSimulation_Cluster *worker = i_node->cSimulation_Cluster;
					CAdaptiveTraversatorClass &adaptiveSubClass = (worker->*(p_adaptiveTraversator_SubClass));
					sierpi::CCluster_ExchangeEdgeCommData<CCluster_TreeNode_, char, CStackAccessors> &simulationEdgeCommSubClass = (worker->*(p_simulationExchangeEdgeComm_SubClass));

					/**
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent cluster, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							i_node->cStacks,
							worker->cCluster_TreeNode->cCluster_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					long long number_of_triangles = i_node->cStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&i_node->cStacks->adaptive_comm_edge_stacks.left,
							&i_node->cStacks->adaptive_comm_edge_stacks.right,
							worker->cCluster_TreeNode->cCluster_AdaptiveSplitJoinInformation,
							worker->cCluster_TreeNode->cCluster_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					i_node->cStacks->adaptive_comm_edge_stacks.clear();

					i_cGenericTreeNode->workload_in_subtree = number_of_triangles;

					*o_number_of_local_triangles = number_of_triangles;
				},
				[=](	CGeneric_TreeNode_ *i_cGenericTreeNode,
						long long *i_number_of_local_cells
				) {
					i_cGenericTreeNode->workload_in_subtree = *i_number_of_local_cells;
				},
				&sierpi::CReduceOperators::ADD,
				&l_number_of_local_cells
			);
		}
		else
		{
			i_cDomainCluster.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *i_node = i_cGenericTreeNode->cCluster_TreeNode;

					CSimulation_Cluster *worker = i_node->cSimulation_Cluster;
					CAdaptiveTraversatorClass &adaptiveSubClass = (worker->*(p_adaptiveTraversator_SubClass));
					sierpi::CCluster_ExchangeEdgeCommData<CCluster_TreeNode_, char, CStackAccessors> &simulationEdgeCommSubClass = (worker->*(p_simulationExchangeEdgeComm_SubClass));

					/*
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent cluster, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							i_node->cStacks,
							worker->cCluster_TreeNode->cCluster_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					long long number_of_triangles = i_node->cStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&i_node->cStacks->adaptive_comm_edge_stacks.left,
							&i_node->cStacks->adaptive_comm_edge_stacks.right,
							worker->cCluster_TreeNode->cCluster_AdaptiveSplitJoinInformation,
							worker->cCluster_TreeNode->cCluster_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					i_node->cStacks->adaptive_comm_edge_stacks.clear();

					i_cGenericTreeNode->workload_in_subtree = number_of_triangles;
				}
			);

			i_cDomainCluster.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
				[](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					long long *o_number_of_local_triangles
				) {
					*o_number_of_local_triangles = i_cGenericTreeNode->workload_in_subtree;
				},
				[=](	CGeneric_TreeNode_ *i_cGenericTreeNode,
						long long *i_number_of_local_triangles
				) {
					i_cGenericTreeNode->workload_in_subtree = *i_number_of_local_triangles;
				},
				&sierpi::CReduceOperators::ADD,
				&l_number_of_local_cells
			);
		}

#if DEBUG
		sierpi::CGlobalComm::barrier();
#endif

		*o_number_of_local_grid_cells = l_number_of_local_cells;
	}
};


#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
