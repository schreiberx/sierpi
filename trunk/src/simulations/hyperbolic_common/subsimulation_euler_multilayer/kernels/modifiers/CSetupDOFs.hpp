/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 13, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSETUP_DOFS_HPP_
#define CSETUP_DOFS_HPP_


#include "../../CDatasets.hpp"
#include "../../types/CTypes.hpp"
#include "libsierpi/triangle/CTriangle_Tools.hpp"
#include "libsierpi/triangle/CTriangle_PointProjections.hpp"
#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_TYPE == 3
#	include "../../../basis_functions_and_matrices/CBasisFunctions2D.hpp"
#endif
#include "../../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"

class CSetupDOFs
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:

	static inline void setup(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,		///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CSimulationCellData *io_cCellData,

			CDatasets *cDatasets
	)	{
		T lod = sierpi::CTriangle_Tools::getLODFromDepth(i_depth);


		/*
		 * project MIDPOINT from reference to world space
		 */
		T world_coords[2];
		sierpi::CTriangle_PointProjections::referenceToWorld(
			i_vertex_left_x,	i_vertex_left_y,
			i_vertex_right_x,	i_vertex_right_y,
			i_vertex_top_x,		i_vertex_top_y,
#if CONFIG_ENABLE_FV_SAMPLING_POINT_AT_HYP_MIDPOINT
			(T)(0.5),			(T)(0.5),
#elif CONFIG_ENABLE_FV_SAMPLING_POINT_AT_QUARTER
			(T)(0.25),			(T)(0.25),
#else
			(T)(1.0/3.0),		(T)(1.0/3.0),
#endif

			&world_coords[0],	&world_coords[1]
		);

		// get min/max layer for setup
		int min_layer_id, max_layer_id;
		cDatasets->getMinMaxLayer(&min_layer_id, &max_layer_id);

		/*
		 * RECONSTRUCT MONOMIAL
		 */
		CSimulationNodeData n;

		T poly_coefficients_r[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T poly_coefficients_ru[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T poly_coefficients_rv[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T poly_coefficients_rw[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T poly_coefficients_e[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

		/*
		 * SETUP
		 */
		for (int j = min_layer_id; j < max_layer_id; j++)
		{
			// initialize in case that getNodalData only updates the values
			CDG_MatrixComputations_3D::mul_convert_dofs_to_poly(io_cCellData->dofs.layers[j].r, poly_coefficients_r);
			n.r = poly_coefficients_r[0];

			CDG_MatrixComputations_3D::mul_convert_dofs_to_poly(io_cCellData->dofs.layers[j].ru, poly_coefficients_ru);
			n.ru = poly_coefficients_ru[0];

			CDG_MatrixComputations_3D::mul_convert_dofs_to_poly(io_cCellData->dofs.layers[j].rv, poly_coefficients_rv);
			n.rv = poly_coefficients_rv[0];

			CDG_MatrixComputations_3D::mul_convert_dofs_to_poly(io_cCellData->dofs.layers[j].rw, poly_coefficients_rw);
			n.rw = poly_coefficients_rw[0];

			CDG_MatrixComputations_3D::mul_convert_dofs_to_poly(io_cCellData->dofs.layers[j].e, poly_coefficients_e);
			n.e = poly_coefficients_e[0];

			// load nodal data
			cDatasets->getNodalData(world_coords[0], world_coords[1], lod, &n);

			/*
			 * setup DOFs
			 */
			// reset poly coefficients. we later on need only the first one
			poly_coefficients_r[0] = n.r;
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly_coefficients_r, io_cCellData->dofs.layers[j].r);

			poly_coefficients_ru[0] = n.ru;
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly_coefficients_ru, io_cCellData->dofs.layers[j].ru);

			poly_coefficients_rv[0] = n.rv;
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly_coefficients_rv, io_cCellData->dofs.layers[j].rv);

			poly_coefficients_rw[0] = n.rw;
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly_coefficients_rw, io_cCellData->dofs.layers[j].rw);

			poly_coefficients_e[0] = n.e;
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly_coefficients_e, io_cCellData->dofs.layers[j].e);
		}


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		io_cCellData->setupDistortedGrid(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				i_hyp_normal_x,		i_hyp_normal_y,
				i_right_normal_x,	i_right_normal_y,
				i_left_normal_x,	i_left_normal_y
			);
#endif

		for (int j = min_layer_id; j < max_layer_id; j++)
		{
			if (io_cCellData->dofs.layers[j].r[0] < 0)
			{
				std::cout << "ERROR: negative density detected" << std::endl;
				assert(false);
				exit(-1);
			}
		}

		if (cDatasets->cParameters_Datasets.simulation_dataset_1_id != CDatasets::SIMULATION_INTERACTIVE_UPDATE)
		{
			for (int j = min_layer_id; j < max_layer_id; j++)
			{
				for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
				{
					//momentum was updated -> project to reference space
					CTriangle_VectorProjections::worldToReference(
							&io_cCellData->dofs.layers[j].ru[i],
							&io_cCellData->dofs.layers[j].rv[i],
							-i_right_normal_x,
							-i_right_normal_y
						);
				}
			}
		}
	}

};

#endif /* CSETUPEULER_DOFS_NODAL_HPP_ */
