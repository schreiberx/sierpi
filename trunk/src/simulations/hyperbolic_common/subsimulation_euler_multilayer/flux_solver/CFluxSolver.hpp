/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CFluxSolver.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_HPP_
#define CFLUXSOLVER_HPP_

#include "simulations/hyperbolic_common/subsimulation_generic/CConfig.hpp"


/****************************************************************
 * SOLVER: LAX FRIEDRICHS
 ****************************************************************/
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==1

#include "CFluxSolver_LaxFriedrich.hpp"

template <typename T>
class CFluxSolver : public CFluxSolver_LaxFriedrich<T>
{
};

#else

#error "unknown flux solver, only lax-friedrichs (1) available"

#endif




#endif /* CFLUXSOLVER_HPP_ */
