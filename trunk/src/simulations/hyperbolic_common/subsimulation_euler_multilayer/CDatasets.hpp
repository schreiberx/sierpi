/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mar 16, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEULER_MULTILAYERSIMULATION_DATASETS_HPP_
#define CEULER_MULTILAYERSIMULATION_DATASETS_HPP_

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cassert>

// only include when compiled with sierpi
#ifndef CONFIG_COMPILE_WITHOUT_SIERPI
#	include "../CParameters.hpp"
#	include "../datasets_common/CParameters_Datasets.hpp"
#endif

#if CONFIG_ENABLE_ASAGI
#	include "../datasets_common/CAsagi.hpp"
#endif

#if CONFIG_ENABLE_NETCDF
#	include "../datasets_common/CSimpleNetCDF.hpp"
#endif

#include "../datasets_common/CDataSet_Interface.hpp"



class CDatasets	:
	public CHyperbolicTypes::CSimulationTypes	///< import simulation types
{
	typedef CHyperbolicTypes::CSimulationTypes::T T;

public:

	/**
	 * possible choices to setup water height
	 */
	enum
	{
		/*
		 * use simple netcdf
		 */
//		SIMULATION_RHO_HEIGHT_SIMPLE_NETCDF = -2,

		/*
		 * use asagi to determine water surface height
		 */
//		SIMULATION_WATER_HEIGHT_ASAGI = -1,

		SIMULATION_COEFF_DEFAULT = 0,

		/*
		 * setup water surface height with cylinder parameters (simulation_dataset_cylinder*)
		 */
		SIMULATION_COEFF_CYLINDER = 4,
//		SIMULATION_WATER_HEIGHT_CYLINDER_RELATIVE_TO_BATHYMETRY = 6,

		SIMULATION_INTERACTIVE_UPDATE = 9
	};



	/**
	 * simulation parameters for simulation
	 */
	CParameters_Datasets &cParameters_Datasets;

	/**
	 * ASAGI dataset interface
	 */
#if CONFIG_ENABLE_ASAGI
	CAsagi cAsagi;
#endif


#if CONFIG_ENABLE_NETCDF
	/**
	 * simple netcdf interface
	 */
	CSimpleNetCDF cSimpleNetCDF;
#endif


#if CONFIG_ENABLE_SWE_BENCHMARKS
	/**
	 * analytical benchmark: single wave on single beach
	 */
	CSingleWaveOnSimpleBeach<T> cSingleWaveOnSimpleBeach;

	/**
	 * analytical benchmark: single wave on composite beach
	 */
	CSingleWaveOnCompositeBeach<T> cSingleWaveOnCompositeBeach;

	/**
	 * analytical benchmark: Parabolic Cup
	 */
	CParabolicCup<T> cParabolicCup;
	
	CDamBreak<T> cDamBreak;
#endif


	/**
	 * verbosity level
	 */
	int verbosity_level;


	/**
	 * level of detail displacement.
	 *
	 * This value has to be set whenever an initial cluster splitting
	 * is done which requests a higher LOD.
	 *
	 * This value is added to each data sampling request.
	 */
private:
	T level_of_detail_displacement_value;


public:
	/**
	 * constructor
	 */
	CDatasets(
			CParameters_Datasets &i_cParameters_Datasets,
			int i_verbosity_level
	)	:
		cParameters_Datasets(i_cParameters_Datasets),
#if CONFIG_ENABLE_ASAGI
		cAsagi(i_verbosity_level),
#endif
#if CONFIG_ENABLE_NETCDF
		cSimpleNetCDF(i_verbosity_level),
#endif
		verbosity_level(i_verbosity_level),
		level_of_detail_displacement_value(0)
	{
	}


	void setLevelOfDetailDisplacementValue(T i_level_of_detail_displacement_value)
	{
		level_of_detail_displacement_value = i_level_of_detail_displacement_value;
	}


	void loadDatasets()
	{
		loadDatasets(cParameters_Datasets.simulation_datasets);
	}

	/**
	 * \brief setup datasets
	 *
	 * This is important for the datafile based simulation scenarios
	 */
	bool loadDatasets(
			const std::vector<std::string> &i_datasets
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			if (	i_datasets[0].length() > 0		&&
					i_datasets[1].length() > 0
			)
			{
				cAsagi.loadDatasets(i_datasets);


				if (!cAsagi.isDatasetLoaded())
					return false;

				cAsagi.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_translate_x,
						&cParameters_Datasets.simulation_dataset_default_domain_translate_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				return true;
			}
			else
			{
				if (verbosity_level > 5)
					std::cout << "Warning: Datasets for simple ASAGI not loaded!" << std::endl;
			}
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			if (i_datasets[0].length() > 0)
			{
				cSimpleNetCDF.loadDatasets(i_datasets);

				if (!cSimpleNetCDF.isDatasetLoaded())
					return false;

				cSimpleNetCDF.getOriginAndSize(
						&cParameters_Datasets.simulation_dataset_default_domain_translate_x,
						&cParameters_Datasets.simulation_dataset_default_domain_translate_y,
						&cParameters_Datasets.simulation_dataset_default_domain_size_x,
						&cParameters_Datasets.simulation_dataset_default_domain_size_y
				);

				return true;
			}
			else
			{
				if (verbosity_level > 5)
					std::cout << "Warning: Datasets for simple NETCDF not loaded!" << std::endl;
			}
			break;
#endif
		}

		return false;
	}


#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER

public:
	void getMinMaxLayer(
			int *o_min_layer,
			int *o_max_layer
	)
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
			case SIMULATION_INTERACTIVE_UPDATE:
			case SIMULATION_COEFF_CYLINDER:
//				*o_min_layer = SIMULATION_NUMBER_OF_LAYERS/4;
//				*o_max_layer = SIMULATION_NUMBER_OF_LAYERS - SIMULATION_NUMBER_OF_LAYERS/4;
				*o_min_layer = 0;
				*o_max_layer = SIMULATION_NUMBER_OF_LAYERS/2;
				break;

			default:
				*o_min_layer = 0;
				*o_max_layer = SIMULATION_NUMBER_OF_LAYERS;
				break;
		}
	}

#endif


public:
	/**
	 * deconstructor
	 */
	virtual ~CDatasets()
	{
	}



	inline void fixWindowAlignment(
			T *io_origin_x,	///< origins x-coordinate
			T *io_origin_y,	///< origins y-coordinate
			T *io_size_x,	///< size of terrain in x-dimension
			T *io_size_y		///< size of terrain in y-dimension
	)	{
		T delta_x = 0;
		T delta_y = 0;

		/*
		 * fix window alignment
		 */
		switch(cParameters_Datasets.simulation_dataset_window_alignment)
		{
		case CParameters_Datasets::WINDOW_ALIGNMENT_INNER_QUAD:
			if (*io_size_x < *io_size_y)
			{
				delta_y = *io_size_y - *io_size_x;
				*io_size_y = *io_size_x;
			}
			else
			{
				delta_x = *io_size_x - *io_size_y;
				*io_size_x = *io_size_y;
			}
			break;

		case CParameters_Datasets::WINDOW_ALIGNMENT_OUTER_QUAD:
			if (*io_size_x < *io_size_y)
			{
				delta_x = *io_size_x - *io_size_y;
				*io_size_x = *io_size_y;
			}
			else
			{
				delta_y = *io_size_y - *io_size_x;
				*io_size_y = *io_size_x;
			}
			break;

		case CParameters_Datasets::WINDOW_ALIGNMENT_NONE:
			break;
		}

		*io_origin_x -= delta_x*(T)0.5;
		*io_origin_y -= delta_y*(T)0.5;
	}


public:
	/**
	 * terrain dimensions
	 */
	inline void getOriginAndSize(
			T *o_origin_x,	///< origins x-coordinate
			T *o_origin_y,	///< origins y-coordinate
			T *o_size_x,	///< size of terrain in x-dimension
			T *o_size_y		///< size of terrain in y-dimension
	)
	{
		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			cAsagi.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			fixWindowAlignment(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			fixWindowAlignment(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif

#if CONFIG_ENABLE_SWE_BENCHMARKS
		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			cSingleWaveOnSimpleBeach.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;

		case SIMULATION_TERRAIN_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			cSingleWaveOnCompositeBeach.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
			
	  case SIMULATION_TERRAIN_HEIGHT_DAM_BREAK:
			cDamBreak.getOriginAndSize(o_origin_x, o_origin_y, o_size_x, o_size_y);
			break;
#endif


		default:
			*o_origin_x = 0;
			*o_origin_y = 0;

			*o_size_x = cParameters_Datasets.simulation_dataset_default_domain_size_x;
			*o_size_y = cParameters_Datasets.simulation_dataset_default_domain_size_y;
			break;
		}
	}


	inline void getNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CSimulationNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		if (cParameters_Datasets.simulation_dataset_1_id == SIMULATION_INTERACTIVE_UPDATE)
		{
			T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
			T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

			if (rx*rx + ry*ry >= cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
				return;

			o_cNodeData->r = 1.1;
//			o_cNodeData->r = std::max<T>(2, o_cNodeData->r*1.1);
			return;
		}

		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
		default:
			o_cNodeData->r = getDatasetValue(0, i_x, i_y, i_level_of_detail);
			o_cNodeData->ru = getDatasetValue(1, i_x, i_y, i_level_of_detail);
			o_cNodeData->rv = getDatasetValue(2, i_x, i_y, i_level_of_detail);
			o_cNodeData->e = getDatasetValue(3, i_x, i_y, i_level_of_detail);

			break;
		}
	}




	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 *
	 * this version loads the node data by loading the timestamp from the dataset timestamp
	 */
	inline bool getBenchmarkNodalData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CSimulationNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		return getBenchmarkNodalData(i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp, o_cNodeData);
	}


	/**
	 * store benchmarks nodal data for given timestamp to o_nodal_data
	 */
	inline bool getBenchmarkNodalData(
			T i_x,		///< x-coordinate in world-space
			T i_y,		///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CSimulationNodeData *o_cNodeData	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			return cAsagi.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif


#if CONFIG_ENABLE_SWE_BENCHMARKS
		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_SINGLE_BEACH:
			return cSingleWaveOnSimpleBeach.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
			return cSingleWaveOnCompositeBeach.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_PARABOLIC_CUP:
			return cParabolicCup.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);

		case SIMULATION_WATER_HEIGHT_DAM_BREAK:
			return cDamBreak.getBenchmarkNodalData(i_x, i_y, i_level_of_detail, i_timestamp, o_cNodeData);
#endif

		}
		return false;
	}


public:
	/**
	 * terrain setup
	 *
	 * dataset 0: terrain
	 * dataset 1: displacements
	 */
	inline T getDatasetValue(
			int i_dataset_id,
			T i_x,
			T i_y,
			T i_level_of_detail
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		if (i_dataset_id == 0)
		{
			/*
			 * bathymetry
			 */

			switch(cParameters_Datasets.simulation_dataset_0_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_TERRAIN_HEIGHT_ASAGI:
					return cAsagi.getDatasetValue(0, i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(0, i_x, i_y, i_level_of_detail);
#endif

				case SIMULATION_COEFF_CYLINDER:
				{
				
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE

					T point_sphere_x, point_sphere_y, point_sphere_z;
					CCube_To_Sphere_Projection::project2DTo3D(
							i_x, i_y,
							point_sphere_x,
							point_sphere_y,
							point_sphere_z
						);

					T center_sphere_x, center_sphere_y, center_sphere_z;
					CCube_To_Sphere_Projection::project2DTo3D(
							cParameters_Datasets.simulation_dataset_breaking_dam_posx, cParameters_Datasets.simulation_dataset_breaking_dam_posy,
							center_sphere_x,
							center_sphere_y,
							center_sphere_z
						);

				T alpha = std::acos(point_sphere_x * center_sphere_x + point_sphere_y * center_sphere_y + point_sphere_z * center_sphere_z);

				T distance = EARTH_RADIUS_IN_METERS * alpha;

				if (distance < cParameters_Datasets.simulation_dataset_breaking_dam_radius)
					return cParameters_Datasets.simulation_dataset_breaking_dam_height;
				else
					return 0;

#else
					CHyperbolicTypes::CSimulationTypes::T rx = (i_x - cParameters_Datasets.simulation_dataset_breaking_dam_posx);
					CHyperbolicTypes::CSimulationTypes::T ry = (i_y - cParameters_Datasets.simulation_dataset_breaking_dam_posy);

					if (rx*rx + ry*ry < cParameters_Datasets.simulation_dataset_breaking_dam_radius*cParameters_Datasets.simulation_dataset_breaking_dam_radius)
						return cParameters_Datasets.simulation_dataset_breaking_dam_height;
					else
						return 0;
#endif
				}

				case SIMULATION_COEFF_DEFAULT:
				default:
					return cParameters_Datasets.simulation_dataset_default_nodal_values[0];
			}
		}

		if (i_dataset_id == 1)
		{
			/*
			 * displacements
			 */
			switch(cParameters_Datasets.simulation_dataset_1_id)
			{
#if CONFIG_ENABLE_ASAGI
				case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
					return cAsagi.getDatasetValue(1, i_x, i_y, i_level_of_detail, cParameters_Datasets.simulation_dataset_benchmark_input_timestamp);
#endif

#if CONFIG_ENABLE_NETCDF
				case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
					return cSimpleNetCDF.getDatasetValue(1, i_x, i_y, i_level_of_detail);
#endif


				case SIMULATION_COEFF_DEFAULT:
				default:
					return cParameters_Datasets.simulation_dataset_default_nodal_values[1];
			}
		}

		if (i_dataset_id == 2)
			return cParameters_Datasets.simulation_dataset_default_nodal_values[2];

		if (i_dataset_id == 3)
			return cParameters_Datasets.simulation_dataset_default_nodal_values[3];

		return 0;
	}


	/**
	 * get boundary data at given position
	 */
	bool getBoundaryData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			CSimulationNodeData *o_nodal_data	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

		return getBoundaryData(
				i_x,				///< x-coordinate in world-space
				i_y,				///< y-coordinate in world-space
				i_level_of_detail,	///< level of detail (0 = coarsest level)
				cParameters_Datasets.simulation_dataset_benchmark_input_timestamp,		///< timestamp for boundary data
				o_nodal_data		///< nodal data is written to this position
		);
	}

	/**
	 * get boundary data at given position
	 */
	bool getBoundaryData(
			T i_x,					///< x-coordinate in world-space
			T i_y,					///< y-coordinate in world-space
			T i_level_of_detail,	///< level of detail (0 = coarsest level)
			T i_timestamp,			///< timestamp for boundary data
			CSimulationNodeData *o_nodal_data	///< nodal data is written to this position
	)
	{
		i_level_of_detail += level_of_detail_displacement_value;

#if CONFIG_ENABLE_SWE_BENCHMARKS
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
			case SIMULATION_WATER_HEIGHT_SINGLE_WAVE_ON_COMPOSITE_BEACH:
				return cSingleWaveOnCompositeBeach.getBoundaryData(i_x,i_y,i_level_of_detail,i_timestamp,o_nodal_data);
				break;
		}
#endif

		//std::cerr << "getBoundaryData not implemented yet" << std::endl;
		assert(false);
		return false;
	}


	/**
	 * return true if the dataset is loaded
	 */
	bool isDatasetLoaded()
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:		// setup with asagi
			return cAsagi.isDatasetLoaded();
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			return cSimpleNetCDF.isDatasetLoaded();
#endif

		default:
			return true;
		}
	}


	/**
	 * output verbose information
	 */
	void outputVerboseInformation()
	{
		switch(cParameters_Datasets.simulation_dataset_1_id)
		{
#if CONFIG_ENABLE_ASAGI
		case SIMULATION_WATER_HEIGHT_ASAGI:		// setup with asagi
			cAsagi.outputVerboseInformation();
			break;
#endif

#if CONFIG_ENABLE_NETCDF
		case SIMULATION_WATER_HEIGHT_SIMPLE_NETCDF:
			cSimpleNetCDF.outputVerboseInformation();
			break;
#endif


		case SIMULATION_COEFF_DEFAULT:
			std::cout << "Water setup: default displacement" << std::endl;
			break;

		case SIMULATION_COEFF_CYLINDER:
			std::cout << "Water setup: cylinder" << std::endl;
			break;

		default:
			std::cout << "Water setup: [unknown]" << std::endl;
			break;
		}


		switch(cParameters_Datasets.simulation_dataset_0_id)
		{
/*
		case SIMULATION_TERRAIN_HEIGHT_ASAGI:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_SIMPLE_NETCDF:
			// no output since output was already done for water information
			break;

		case SIMULATION_TERRAIN_HEIGHT_DEFAULT:
			std::cout << "Terrain setup: default" << std::endl;
			break;
*/
		}
	}
};



#endif /* CEULER_MULTILAYERSIMULATION_DATASETS_HPP_ */
