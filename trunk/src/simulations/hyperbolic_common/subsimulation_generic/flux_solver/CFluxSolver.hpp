/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "../CConfig.hpp"


#if CONFIG_SUB_SIMULATION_TSUNAMI

#	define CONFIG_SUBSIMULATION_STRING "tsunami"
#	include "../../subsimulation_tsunami/flux_solver/CFluxSolver.hpp"

#elif CONFIG_SUB_SIMULATION_EULER

#	define CONFIG_SUBSIMULATION_STRING "euler"
#	include "../../subsimulation_euler/flux_solver/CFluxSolver.hpp"

#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER

#	define CONFIG_SUBSIMULATION_STRING "euler multilayer"
#	include "../../subsimulation_euler_multilayer/flux_solver/CFluxSolver.hpp"

#elif CONFIG_SUB_SIMULATION_BENCHMARK_SPHERE

#	define CONFIG_SUBSIMULATION_STRING "benchmark sphere"
#	include "../../subsimulation_benchmark_sphere/flux_solver/CFluxSolver.hpp"

#else
#	error "unknown sub-simulation"
#endif
