/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CVISUALIZATION_NODE_DATA_HPP
#define CVISUALIZATION_NODE_DATA_HPP

#include "global_config.h"

#include "CValidation_NodeData.hpp"

/**
 * This datastructure intends to be an accumulator for vertex related data.
 *
 * For surface visualization, the normal as well as the height is needed. The normal
 * as well as the height has to be firstly scaled by some weight (e. g.
 * the triangle surface) with the weight being added to the normalization_factor.
 *
 * Then during the last touch, the vertex data has to be scaled by the inverse of
 * normalization_factor.
 */
class CVisualizationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS

	/**
	 * the (vertex displacement value and its height) or the normal to the surface
	 */
	T scalars[3];

#else

	/**
	 * the normal to the surface
	 */
	T normal[3];

	/**
	 * height of surface
	 */
	T height;

#endif

	/**
	 * normalization factor
	 */
	T normalization_factor;


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_NodeData validation;
#endif

};

#endif
