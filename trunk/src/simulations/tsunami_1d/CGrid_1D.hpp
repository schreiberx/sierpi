/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: 1. August 2012
 *      Author: Breuer Alexander <breuera@in.tum.de>, Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGRID1D_HPP_
#define CGRID1D_HPP_

#include <cassert>
#include <cmath>
#include <vector>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <limits>

#include "CCellData_1D.hpp"
#include "CEdgeComm_Tsunami_1D.hpp"

#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../hyperbolic_common/subsimulation_generic/CDatasets.hpp"

#include "libsierpi/grid/CBoundaryConditions.hpp"


/**
 * Representation of the discrete one dimensional domain.
 */
template <typename T>
class CGrid_1D {

private:
	/// left domain boundary
	T x_min;

	/// right domain boundary
	T x_max;

	/// number of cells
	int number_of_cells;

	/// vector containing all grid-cells
	std::vector<CCellData_1D> grid_cells;



	/**
	 * Edge Buffer
	 */
	class CTsunamiSimulationEdgeBuffer_
	{
		/// left edge data
		CSimulationEdgeData leftEdgeData;

		/// right edge data
		CSimulationEdgeData rightEdgeData;


	public:
		/**
		 * Get a reference to the data for the left cell (stored on the edge).
		 *
		 * \return reference to left cell data.
		 */
		CSimulationEdgeData& getLeftEdgeDataRef() {
			return leftEdgeData;
		}

		/**
		 * Get a reference to the data for the right cell (stored on the edge).
		 *
		 * \return reference to right cell data.
		 */
		CSimulationEdgeData& getRightEdgeDataRef() {
			return rightEdgeData;
		}

		/**
		 * Get a pointer to the data for the left cell (stored on the edge).
		 *
		 * \return pointer to left cell data.
		 */
		CSimulationEdgeData* getLeftEdgeDataPtr() {
			return &leftEdgeData;
		}

		/**
		 * Get a pointer to the data for the right cell (stored on the edge).
		 *
		 * \return pointer to right cell data.
		 */
		CSimulationEdgeData* getRightEdgeDataPtr() {
			return &rightEdgeData;
		}


		std::string getLeftEdgeDataString() {
			std::ostringstream s;
			s << ", h: " << leftEdgeData.dofs.h[0];
			s << ", hu: " << leftEdgeData.dofs.hu[0];
			s << ", hv: " << leftEdgeData.dofs.hv[0];
			s << ", b: " << leftEdgeData.dofs.b[0];
			s << ", cfl: " << leftEdgeData.CFL1_scalar;
			return s.str();
		}

		std::string getRightEdgeDataString() {
			std::ostringstream s;
			s << ", h: " << rightEdgeData.dofs.h[0];
			s << ", hu: " << rightEdgeData.dofs.hu[0];
			s << ", hv: " << rightEdgeData.dofs.hv[0];
			s << ", b: " << rightEdgeData.dofs.b[0];
			s << ", cfl: " << rightEdgeData.CFL1_scalar;
			return s.str();
		}
	};

	/// vector, which holds the edges
	std::vector<CTsunamiSimulationEdgeBuffer_> edge_buffer_data;

	CEdgeComm_Tsunami_1D cEdgeComm;


	/// boundary condition for the left boundary
	EBoundaryConditions left_boundary_condition;

	/// boundary condition for the right boundary
	EBoundaryConditions right_boundary_condition;

	/// simulation scenarios
	CDatasets *cDatasets;


	/// flux solver
	CFluxSolver<T> cFluxSolver;

	/// define a zero tolerance if not defined before
	T zeroTolerance;


public:
	/**
	 * Constructor of the grid.
	 *
	 * Sets up a regular 1D grid with grid spacing (i_xMax - i_xMin)/i_numberOfCells.
	 */
	CGrid_1D()	:
		x_min(0),
		x_max(0),
		left_boundary_condition(EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW),
		right_boundary_condition(EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW)
	{
	}

	void setBoundaryConditionsLeftAndRight(
			EBoundaryConditions i_left_boundary_condition,
			EBoundaryConditions i_right_boundary_condition
	) {
		left_boundary_condition = i_left_boundary_condition;
		right_boundary_condition = i_right_boundary_condition;
	}


	virtual ~CGrid_1D() {
		// TODO Auto-generated destructor stub
	}


	CCellData_1D &getCellData(int i)
	{
		return grid_cells[i];
	}

	int getNumberOfCells()
	{
		return number_of_cells;
	}

	/**
	 * setup grid structure
	 */
	void setup(
			const int i_number_of_cells,		///< i_numberOfCells number of the cells in the initial grid.
			CDatasets *i_cTsunamiSimulationScenarios,
			CParameters *i_cTsunamiSimulationParameters
	) {
		number_of_cells = i_number_of_cells;

		cDatasets = i_cTsunamiSimulationScenarios;

		T origin_x, origin_y;
		T size_x, size_y;

		cDatasets->getOriginAndSize(
				&origin_x, &origin_y,
				&size_x, &size_y
			);

		x_min = origin_x - size_x*(T)0.5;
		x_max = x_min + size_x;

		// assert a proper defined domain
		assert(std::abs(x_max - x_min) > SIMULATION_TSUNAMI_ZERO_THRESHOLD);
		assert(i_number_of_cells > 0);

		// compute cell size
		T l_delta_x = (x_max - x_min)/(T)number_of_cells;
		T l_h_delta_x = (T)0.5*l_delta_x;

		T l_left_cell_center = x_min+l_h_delta_x;

		// generate the grid
		grid_cells.resize(number_of_cells);
		for(int i = 0; i < number_of_cells; i++) {
			grid_cells[i].setup(l_delta_x, l_left_cell_center+(T)i*l_delta_x, i_cTsunamiSimulationScenarios);
		}

		// generate edge data structure
		edge_buffer_data.resize(number_of_cells+1);

		/*
		 * parameters for edge communication
		 */
		cEdgeComm.setParameters(
				i_cTsunamiSimulationParameters->simulation_parameter_global_timestep_size,	///< timestep size
				i_cTsunamiSimulationParameters->simulation_dataset_default_domain_size_x,	///< length of a square (a single catheti)
				i_cTsunamiSimulationParameters->simulation_parameter_gravitation,			///< gravitational constant

				i_cTsunamiSimulationParameters->adaptive_refine_parameter_0,
				i_cTsunamiSimulationParameters->adaptive_coarsen_parameter_0,

				cDatasets
			);
	}



	/**
	 * compute the flux values and store them to the corresponding edges.
	 */
	void ops_cell_to_edge() {

		const unsigned int s = grid_cells.size();

#if 0
		/* not worth to run for small problem sizes (overhead to large) */
#	if CONFIG_THREADING_OMP
#		pragma omp parallel for schedule(static)
#	endif
#endif

		for(unsigned int i = 0; i < s; i++) {
			cEdgeComm.op_1d_cell_to_edge_left(grid_cells[i], edge_buffer_data[i].getRightEdgeDataPtr());
			cEdgeComm.op_1d_cell_to_edge_right(grid_cells[i], edge_buffer_data[i+1].getLeftEdgeDataPtr());
		}
	}

	/**
	 * Set the ghost cells according to the specified boundary conditions.
	 */
	void ops_boundary_edges()
	{
		// left boundary
		switch (left_boundary_condition)
		{
		case EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW:
			cEdgeComm.op_1d_boundary_cell_to_edge_left(grid_cells[0], edge_buffer_data[0].getLeftEdgeDataPtr());
			break;

		case EBoundaryConditions::BOUNDARY_CONDITION_BOUNCE_BACK:
			cEdgeComm.op_1d_boundary_cell_to_edge_left(grid_cells[0], edge_buffer_data[0].getLeftEdgeDataPtr());
			edge_buffer_data[0].getLeftEdgeDataPtr()->dofs.hu[0] = -edge_buffer_data[0].getLeftEdgeDataPtr()->dofs.hu[0];
			break;

		default:
			assert(false);
			break;
		}


		// right boundary
		switch (right_boundary_condition)
		{
		case EBoundaryConditions::BOUNDARY_CONDITION_OUTFLOW:
			cEdgeComm.op_1d_boundary_cell_to_edge_right(grid_cells[number_of_cells-1], edge_buffer_data[number_of_cells].getRightEdgeDataPtr());
			break;

		case EBoundaryConditions::BOUNDARY_CONDITION_BOUNCE_BACK:
			cEdgeComm.op_1d_boundary_cell_to_edge_right(grid_cells[0], edge_buffer_data[0].getRightEdgeDataPtr());
			edge_buffer_data[0].getRightEdgeDataPtr()->dofs.hu[0] = -edge_buffer_data[0].getRightEdgeDataPtr()->dofs.hu[0];
			break;


		case EBoundaryConditions::BOUNDARY_CONDITION_DATASET:
			{
				CHyperbolicTypes::CSimulationTypes::CEdgeData *edge_data = edge_buffer_data[number_of_cells].getRightEdgeDataPtr();

				CSimulationNodeData n;
				if (!cDatasets->getBoundaryData(0, 0, 0, &n))
				{
					// use default outflow conditio
					cEdgeComm.op_1d_boundary_cell_to_edge_right(grid_cells[number_of_cells-1], edge_buffer_data[number_of_cells].getRightEdgeDataPtr());
					edge_data->CFL1_scalar = std::numeric_limits<T>::infinity();
					break;
				}

				edge_data->dofs.h[0] = n.h;
				edge_data->dofs.hu[0] = -n.hu;
				edge_data->dofs.hv[0] = n.hv;
				edge_data->dofs.b[0] = n.b;

				edge_data->CFL1_scalar = std::numeric_limits<T>::infinity();
			}
			break;

		default:
			assert(false);
			break;
		}
	}


	/**
	 * Compute net-updates from edge data and store them there.
	 */
	void ops_edge_edge()
	{
		unsigned int s = edge_buffer_data.size();

#if 1

#if 0
		/* not worth to run for small problem sizes (overhead to large) */
#	if CONFIG_THREADING_OMP
#		pragma omp parallel for
#	endif
#endif

		for(unsigned int i = 0; i < s; i++) {
			CSimulationEdgeData netupdates_left_edge;
			CSimulationEdgeData netupdates_right_edge;

			//TODO: hard coded tolerances
			CSimulationEdgeData &flux_left_edge = edge_buffer_data[i].getLeftEdgeDataRef();
			CSimulationEdgeData &flux_right_edge = edge_buffer_data[i].getRightEdgeDataRef();

			cEdgeComm.op_edge_edge(
					flux_left_edge,
					flux_right_edge,
					netupdates_left_edge,
					netupdates_right_edge
			);

			flux_left_edge = netupdates_left_edge;
			flux_right_edge = netupdates_right_edge;

#if 0
#	if CONFIG_THREADING_OMP
#		pragma omp critical
#	endif
#endif
			{
				cEdgeComm.updateCFL1Value(flux_left_edge.CFL1_scalar);
				cEdgeComm.updateCFL1Value(flux_right_edge.CFL1_scalar);
			}
		}

#else

		std::vector<CTsunamiSimulationEdgeBuffer_> edge_buffer_data_output;
		edge_buffer_data_output.resize(s);

		T min_cfl1_scalar = std::numeric_limits<T>::infinity();

#if 0
#if CONFIG_THREADING_OMP
#	pragma omp parallel for default(shared)
#endif
#endif

		for (unsigned int i = 0; i < s; i++) {
			CSimulationEdgeData netupdates_left_edge;
			CSimulationEdgeData netupdates_right_edge;

			CSimulationEdgeData flux_left_edge = edge_buffer_data[i].getLeftEdgeDataRef();
			CSimulationEdgeData flux_right_edge = edge_buffer_data[i].getRightEdgeDataRef();

			cEdgeComm.op_edge_edge(
					flux_left_edge,
					flux_right_edge,
					netupdates_left_edge,
					netupdates_right_edge
			);

			edge_buffer_data_output[i].getLeftEdgeDataRef() = netupdates_left_edge;
			edge_buffer_data_output[i].getRightEdgeDataRef() = netupdates_right_edge;

			T m = std::min(netupdates_left_edge.CFL1_scalar, netupdates_right_edge.CFL1_scalar);

//			min_cfl1_scalar = std::min(min_cfl1_scalar, netupdates_left_edge.CFL1_scalar);
//			min_cfl1_scalar = std::min(min_cfl1_scalar, netupdates_right_edge.CFL1_scalar);

#pragma omp flush (min_cfl1_scalar)
			if (min_cfl1_scalar > m)
			{
#pragma omp critical
				if (min_cfl1_scalar > m)
					min_cfl1_scalar = m;
			}
		}

		cEdgeComm.updateCFL1Value(min_cfl1_scalar);

		edge_buffer_data_output.swap(edge_buffer_data);
#endif
	}


	/**
	 * update cell values (global timestep) with the specified timestep size
	 */
	void ops_cell()
	{
		unsigned int s = grid_cells.size();

#if 0
		/* not worth to run for small problem sizes (overhead to large) */
#	if CONFIG_THREADING_OMP
#		pragma omp parallel for
#	endif
#endif

		for(unsigned int i = 0; i < s; i++) {

			cEdgeComm.op_1d_cell(
					&grid_cells[i],
					edge_buffer_data[i].getRightEdgeDataRef(),
					edge_buffer_data[i+1].getLeftEdgeDataRef()
				);
		}
	}


	/**
	 * execute a single timestep
	 */
	T runSingleTimestep(
			T i_cfl_value	///< cfl condition
	) {
		T global_timestep_size;

		cEdgeComm.traversal_pre_hook();

		// store fluxes
		ops_cell_to_edge();

		// store boundary values
		ops_boundary_edges();

//		printEdgeValues();

		// compute net updates and store timestep width
		ops_edge_edge();

//		printEdgeValues();

		global_timestep_size = cEdgeComm.getCFL1TimestepSize()*i_cfl_value;

		if (std::isinf(global_timestep_size))
		{
			std::cout << "WARNING: infinity timestep size detected - maybe everything is dry?" << std::endl;
			global_timestep_size = 999999;
			std::cout << "Timestep size fixed to " << global_timestep_size << std::endl;
		}
		cEdgeComm.setTimestepSize(global_timestep_size);

		// cell updates
		ops_cell();

		cEdgeComm.traversal_post_hook();

		return global_timestep_size;
	}


#if 0
	/**
	 * Sets the maximum allowed local time step width for each cell with respect to the given CFL number.
	 */
	void setLocalTimeStepSizes(
			const T i_cfl_number		///< CFL number
	) {
		for(unsigned int i = 0; i < grid_cells.size(); i++) {

			//compute maximum speed of the waves propgating inside the cell
			T l_cfl1_scalar = edge_buffer_data[i].getRightEdgeDataRef().CFL1_scalar;
			l_cfl1_scalar = std::max(l_cfl1_scalar, std::abs(edge_buffer_data[i+1].getLeftEdgeDataRef().CFL1_scalar));
		}
	}

	/**
	 * Compute the maximum allowed global time step with respect to the given CFL-number
	 *
	 * \return maximum allowed time step width.
	 */
	T computeMaximumGlobalTimeStepSize(
			const T i_cfl_number		///< CFL number
	) {
		T l_globalTimeStepWidth = std::numeric_limits<T>::max();

//      for(unsigned int i = 0; i < cells.size(); i++) {
//        //compute maximum speed of the waves propgating inside the cell
//        T l_maximumWaveSpeed = edges[i].getRightCellData().waveSpeed;
//        l_maximumWaveSpeed = std::max( l_maximumWaveSpeed, std::abs(edges[i+1].getLeftCellData().waveSpeed) );
//
//        //update the global time step width
//        if(l_maximumWaveSpeed > zeroTol)
//          l_globalTimeStepWidth = std::min(l_globalTimeStepWidth, cells[i].getCellWidth()/l_maximumWaveSpeed);
//      }

		setLocalTimeStepSizes(i_cfl_number);

		for(unsigned int i = 0; i < grid_cells.size(); i++) {
			l_globalTimeStepWidth = std::min( l_globalTimeStepWidth, grid_cells[i].getTimeStepWidth() );
		}

		assert( l_globalTimeStepWidth > SIMULATION_TSUNAMI_ZERO_THRESHOLD );
		assert( l_globalTimeStepWidth < std::numeric_limits<T>::max() );

		return l_globalTimeStepWidth;
	}
#endif

	/**
	 * Get the number of grid cells.
	 *
	 * \return number of grid cells.
	 */
	size_t getNumberOfCells() const {
		return grid_cells.size();
	}

	/**
	 * Get the number of edges.
	 *
	 * \return number of edges.
	 */
	int getNumberOfEdges() const {
		return edge_buffer_data.size();
	}

	/**
	 * Print the values of the cells in the grid
	 */
	void printCellValues() {
		for(unsigned int i = 0; i < grid_cells.size(); i++) {
			std::cout << " cell " << i << ": " << grid_cells[i].toString() << std::endl;
		}
	}

	/**
	 * Print the values of the cell in the grid at position i_position_x
	 */
	void printCellValue(
			T i_position_x
	) {
		for(unsigned int i = 0; i < grid_cells.size(); i++) {
			if (	(grid_cells[i].getCellLeftX() < i_position_x) &&
					(grid_cells[i].getCellRightX() > i_position_x)
			)
			{
				std::cout << " cell " << i << ": " << grid_cells[i].toString() << std::endl;
				break;
			}
		}
	}


	/**
	 * Print the values of the cell in the grid at position i_position_x
	 */
	void setup_RadialDamBreak(
			T i_position_x,
			T i_radius,
			T i_height
	) {
		for(unsigned int i = 0; i < grid_cells.size(); i++)
		{
			if (	(grid_cells[i].getCellLeftX()-i_radius < i_position_x) &&
					(grid_cells[i].getCellRightX()+i_radius > i_position_x)
			)
			{
				grid_cells[i].setWaterHeight(
						std::max(	grid_cells[i].getBathymetry() + grid_cells[i].getWaterHeight(),
									i_height
								)
					);
			}
		}
	}

	/**
	 * Print the values of the edges in the grid
	 */
	void printEdgeValues() {
		for(unsigned int i = 0; i < edge_buffer_data.size(); i++) {
			std::cout << " edge " << i << " - left: " << edge_buffer_data[i].getLeftEdgeDataString() << " - right: " << edge_buffer_data[i].getRightEdgeDataString() << std::endl;
		}
	}

#if 0
	/**
	 * Call the verification routine for a specified time.
	 *
	 * @param i_simulationTime simulation time.
	 */
	void callVerifactionForGlobalTimeStep( const T i_simulationTime ) {
		std::cout << "callVerifactionForGlobalTimeStep(...) called" << std::endl;
		for( unsigned int i = 0; i < grid_cells.size(); i++ ) {
			T l_midPoint = computeMidPoint(i);
			//define something
			//call your verification routine:
			// routine(i_simulationTime, l_midPoint, cells[i].getWaterHeight(), cells[i].getMomentum);
		}
	}
#endif


#if 0
	//***********************************************************************************************
	//******************************Adaptivity routines**********************************************
	//***********************************************************************************************
	void refineCell( const unsigned int i_cellId,
		 const int i_numberOfCells
	) {
		//assert valid input data
		assert(i_cellId < grid_cells.size());
		assert(i_numberOfCells > 0);
		//TODO
		assert(false);
	}


	/**
	 * Coarsen connected cells to one cell. This cell will hold the average values of the original cells.
	 */
	void coarsenCells(
			const unsigned int i_idFirstCell,	///< ID of the first cell to coarsen
			const unsigned int i_idLastCell	///< ID of last cell to coarsen
	) {
		// assert valid ids
		//      assert(i_idFirstCell > 0); //unsigned..
		assert(i_idLastCell < grid_cells.size());
		assert(i_idFirstCell < i_idLastCell);

		// cell variables of the new coarsened cell
		T l_totalLength = (T)0.;
		T l_midWaterHeight = (T)0.;
		T l_midMomentum = (T)0.;
		T l_midBathymetry = (T)0.;

		// compute quantities
		for(unsigned int i = i_idFirstCell; i <= i_idLastCell; i++) {
		l_totalLength += grid_cells[i].getCellWidth();
		l_midWaterHeight += grid_cells[i].getWaterHeight() * grid_cells[i].getCellWidth();
		l_midMomentum += grid_cells[i].getMomentum() *  grid_cells[i].getCellWidth();
		l_midBathymetry += grid_cells[i].getBathymetry() * grid_cells[i].getCellWidth();
		}
		l_midWaterHeight /= l_totalLength;
		l_midMomentum /= l_totalLength;
		l_midBathymetry /= l_totalLength;

		//remove the cells (except for one, which is the coarsened cell
		grid_cells.erase( grid_cells.begin()+i_idFirstCell, grid_cells.begin()+i_idLastCell );
		//and their edges
		edge_buffer_data.erase( edge_buffer_data.begin()+i_idFirstCell+1, edge_buffer_data.begin()+i_idLastCell+1 );

		//assign new values to the coarsened cell
		grid_cells[i_idFirstCell].getCellData().h = l_midWaterHeight;
		grid_cells[i_idFirstCell].getCellData().hu = l_midMomentum;
		grid_cells[i_idFirstCell].getCellData().b = l_midBathymetry;
		grid_cells[i_idFirstCell].setCellWidth(l_totalLength);

		//update the edge positions
		edge_buffer_data[i_idFirstCell+1].setPosition( edge_buffer_data[i_idFirstCell].getPosition() + l_totalLength );

		assert( grid_cells.size() + 1 == edge_buffer_data.size());
	}
#endif

	//***********************************************************************************************
	//**************************************IO routines**********************************************
	//***********************************************************************************************
	/**
	 * Write the current grid data to a VTK file.
	 */
	void writeSimulationDataToFile(
			const char *i_filename,				///< filename for current timestep
			const char *i_information_string,		///< additional information
			T i_simulation_time
	)	{

		bool one_dimensional_output = false;

		std::ofstream vtkFile;
		vtkFile.open(i_filename);

		// version identifier
		vtkFile << "# vtk DataFile Version 3.0" << std::endl;
		// description
		vtkFile << "Results from the 1D test code: Alexander Breuer, Martin Schreiber" << std::endl;
		//file format
		vtkFile << "ASCII" << std::endl;

		// geometry description
		vtkFile << "DATASET RECTILINEAR_GRID" << std::endl;

		if (one_dimensional_output)
			vtkFile << "DIMENSIONS " << getNumberOfEdges() << " 1 1" << std::endl;
		else
			vtkFile << "DIMENSIONS " << getNumberOfEdges() << " 2 1" << std::endl;

		vtkFile << "X_COORDINATES " << grid_cells.size()+1 << " float" << std::endl;

		T p = 0;
		T s = 0;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
		{
			p = grid_cells[i].getPositionX();
			s = grid_cells[i].getCellSizeX();
			vtkFile << (p - s*(T)0.5) << std::endl;
		}
		vtkFile << (p + s*(T)0.5) << std::endl;

		if (one_dimensional_output)
		{
			vtkFile << "Y_COORDINATES 1 float" << std::endl;
			vtkFile << "0" << std::endl;
		}
		else
		{
			vtkFile << "Y_COORDINATES " << 2 << " float" << std::endl;
			vtkFile << "-1" << std::endl;
			vtkFile << "1" << std::endl;
		}

		vtkFile << "Z_COORDINATES 1 float" << std::endl;
		vtkFile << "0" << std::endl;

		vtkFile << "CELL_DATA " << getNumberOfCells() << std::endl;

		vtkFile << "SCALARS h float 1" << std::endl;
		vtkFile << "LOOKUP_TABLE default" << std::endl;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
			vtkFile << grid_cells[i].getWaterHeight() << std::endl;

		vtkFile << "SCALARS hu float 1" << std::endl;
		vtkFile << "LOOKUP_TABLE default" << std::endl;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
			vtkFile << grid_cells[i].getMomentum() << std::endl;

		vtkFile << "SCALARS b float 1" << std::endl;
		vtkFile << "LOOKUP_TABLE default" << std::endl;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
			vtkFile << grid_cells[i].getBathymetry() << std::endl;

		vtkFile << "SCALARS h_b float 1" << std::endl;
		vtkFile << "LOOKUP_TABLE default" << std::endl;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
			vtkFile << grid_cells[i].getWaterHeight()+grid_cells[i].getBathymetry() << std::endl;

		vtkFile << "SCALARS benchmark float 1" << std::endl;
		vtkFile << "LOOKUP_TABLE default" << std::endl;

		CSimulationNodeData dofs;
		for(unsigned int i = 0; i < grid_cells.size(); i++)
		{
			cDatasets->getBenchmarkNodalData(grid_cells[i].getPositionX(), 0, 0, i_simulation_time, &dofs);
			vtkFile << (dofs.h-dofs.b) << std::endl;
		}

		vtkFile.close();
	}
};

#endif /* GRID_HPP_ */
