/*
 * CSimulationTsunami_1d_FileOutput.hpp
 *
 *  Created on: Mar 18, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_1D_FILEOUTPUT_HPP_
#define CSIMULATIONTSUNAMI_1D_FILEOUTPUT_HPP_


#include "CGrid_1D.hpp"

class CSimulationTsunami_1d_FileOutput	:
	public CSimulation_MainInterface_FileOutput
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	CParameters &cParameters;

	CGrid_1D<T> &cGrid_1D;

	CDatasets &cDatasets;


public:
	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationTsunami_1d_FileOutput(
			CParameters &i_cParameters,
			CDatasets &i_cDatasets,
			CGrid_1D<T> &i_cGrid_1D
	)	:
		cParameters(i_cParameters),
		cGrid_1D(i_cGrid_1D),
		cDatasets(i_cDatasets)
	{

	}

	void writeSimulationDataToFile(
			const char *i_filename,
			const char *i_information_string = nullptr
	)
	{
		cGrid_1D.writeSimulationDataToFile(i_filename, i_information_string, cParameters.simulation_timestamp_for_timestep);

#if 0
		CSimulationTsunami_GridDataArrays cGridDataArrays(number_of_local_cells);

		sierpi::kernels::CSimulationTsunami_OutputGridDataArrays::TRAV cOutputGridDataArrays;

		int flags =
				CSimulationTsunami_GridDataArrays::VERTICES	|
				CSimulationTsunami_GridDataArrays::H	|
				CSimulationTsunami_GridDataArrays::HU	|
				CSimulationTsunami_GridDataArrays::HV	|
				CSimulationTsunami_GridDataArrays::B	|
				CSimulationTsunami_GridDataArrays::CFL_VALUE_HINT;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				&cGridDataArrays,
				0,
				flags
			);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

		cVtkXMLTrianglePolyData.setup();
		cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
		cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.h);
		cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.hu);
		cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.hv);
		cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.b);
		cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.cfl_value_hint);

		cVtkXMLTrianglePolyData.write(i_filename);
#endif
	}


	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		std::cout << "writeSimulationClustersDataToFile() not implemented (meaningless for serial version)" << std::endl;
	}
};


#endif /* CSIMULATIONTSUNAMI_1D_FILEOUTPUT_HPP_ */
