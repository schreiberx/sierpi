/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 */

#ifndef SIERPINSKI_CONFIG_H
#define SIERPINSKI_CONFIG_H	1

/*
 * global_config.h
 *
 * Precompiler settings for compilation
 *
 *  Created on: Sep 27, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


/**
 * Resize stacks when the element data exceeds the stack size due to
 * adaptivity.
 *
 * The CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING gives the approximated
 * number of bytes to additionally extend the stack to avoid frequent
 * malloc/free operators.
 *
 * The CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING gives the approximated
 * number of bytes which the new max element data stack counter has to be fall
 * below the maximum to allow a shrinking of the stack.
 */
#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
#	error "CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS not defined"
#endif

#if 0

	#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING
			#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING	(128*8)
		#endif

		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CElement)	(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING / sizeof(CElement))

		#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING
			// subtract a little offset (16) to avoid flickering
			#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING	(128*8-16)
		#endif

		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING_FOR_ELEMENT(CElement)	((CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING / sizeof(CElement)) + 32)
	#endif

#else

	#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS

		#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING
			#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING		(128)
		#endif

		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CElement)	(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING)


		#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING
			#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING		(128)
		#endif

		#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING_FOR_ELEMENT(CElement)	(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING)

	#endif

#endif


/**
 * during adaptive traversals a conforming grid should be gained.
 *
 * for sub-clusters already being in a conforming state and without any requests
 * from adjacent edge communication to refine, the adaptive traversal of such a
 * sub-cluster can be skipped.
 */
#ifndef CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE
#error "CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE not defined"
#endif



/**
 * avoid last adaptive traversal if no adaptive refinements have to be executed
 */
//#define CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED 1

#ifndef CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED
#	error "CONFIG_SIERPI_ADAPTIVE_CLUSTER_AVOID_LAST_TRAVERSAL_IF_NOTHING_CHANGED not defined"
#endif

#ifndef CONFIG_ENABLE_PRINT_RLE_STATISTICS
#	define	CONFIG_ENABLE_PRINT_RLE_STATISTICS 1
#endif


/**
 * default floating point precision
 */
#if CONFIG_DEFAULT_FLOATING_POINT_TYPE_SINGLE
	#define CONFIG_DEFAULT_FLOATING_POINT_TYPE		float
#elif CONFIG_DEFAULT_FLOATING_POINT_TYPE_DOUBLE
	#define CONFIG_DEFAULT_FLOATING_POINT_TYPE		double
#else
	#error "CONFIG_DEFAULT_FLOATING_POINT_TYPE_SINGLE and CONFIG_DEFAULT_FLOATING_POINT_TYPE_DOUBLE not defined"
#endif


/**
 * scalar type used in the triangle factory describing in which format the 3 triangle vertices are stored
 */
#define CONFIG_SIERPI_TRIANGLE_FACTORY_SCALAR_TYPE CONFIG_DEFAULT_FLOATING_POINT_TYPE


/**
 * number of vector elements being processed at once by SIMD operations
 *
 * this is necessary to apply a padding to specific buffer stacks.
 */
#define CONFIG_BUFFER_PADDING_FOR_SIMD_ELEMENTS		8



/**
 * create scan data
 */
#ifndef CONFIG_ENABLE_SCAN_DATA
#	error "CONFIG_ENABLE_SCAN_DATA not defined"
#	define CONFIG_ENABLE_SCAN_DATA		1
#endif


/**
 * enable opencl
 */
#ifndef CONFIG_ENABLE_OPENCL
#	error "CONFIG_ENABLE_OPENCL not defined"
#	define CONFIG_ENABLE_OPENCL	1
#endif


#ifndef CONFIG_ENABLE_THREADING
#	error "CONFIG_ENABLE_THREADING not defined"
#endif


/**
 * assign clusters to single thread according to the scan workload information
 */
#ifndef CONFIG_ENABLE_SCAN_THREADING
#error "CONFIG_ENABLE_SCAN_THREADING not defined"
#	define CONFIG_ENABLE_SCAN_THREADING	1
#endif




/**
 * activate writer task for TBB
 */
#ifndef CONFIG_ENABLE_WRITER_TASK
#error "CONFIG_ENABLE_WRITER_TASK not defined"
#	define CONFIG_ENABLE_WRITER_TASK	1
#endif



/**
 * activate writer thread
 */
#ifndef CONFIG_WRITER_THREAD_WITH_PTHREAD
#error "CONFIG_WRITER_THREAD_WITH_PTHREAD not defined"
#	define CONFIG_WRITER_THREAD_WITH_PTHREAD	1
#endif



/**
 * enable scan split and join distribution
 */
#ifndef CONFIG_ENABLE_SCAN_BASED_SPLIT_AND_JOIN
#	error "CONFIG_ENABLE_SCAN_BASED_SPLIT_AND_JOIN not defined"
#endif

/**
 * force scan traversal whenever it is possible
 */
#ifndef CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL
#	error "CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL not defined"
#endif


/**
 * Enable compilation with vertex data communication information
 */
#ifndef CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#	define CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA 1
#	error "CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA is not defined"
#endif


#define CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS		1



/**
 * distorted grid mode
 */
#ifndef CONFIG_SPHERICAL_DISTORTED_GRID_MODE
#	error "CONFIG_SPHERICAL_DISTORTED_GRID_MODE is not defined"
#	define CONFIG_SPHERICAL_DISTORTED_GRID_MODE 0
#endif




/**
 * MPI
 */
//#undef CONFIG_ENABLE_MPI

#ifndef CONFIG_ENABLE_MPI
#error "CONFIG_ENABLE_MPI not defined"
#define CONFIG_ENABLE_MPI 1
#endif


/**
 * MPI split and join
 */
//#undef CONFIG_ENABLE_MPI_SPLIT_AND_JOIN

#ifndef CONFIG_ENABLE_MPI_SPLIT_AND_JOIN
#error "CONFIG_ENABLE_MPI_SPLIT_AND_JOIN not defined"
#define CONFIG_ENABLE_MPI_SPLIT_AND_JOIN 1
#endif


/**
 * MPI split and join
 */
//#undef CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

#ifndef CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
#error "CONFIG_ENABLE_MPI_CLUSTER_MIGRATION not defined"
#define CONFIG_ENABLE_MPI_CLUSTER_MIGRATION 1
#endif



/**
 * COMPILE_WITH_ITBB_ASYNC_MPI
 */
/*
#ifndef COMPILE_WITH_ITBB_ASYNC_MPI
#error "COMPILE_WITH_ITBB_ASYNC_MPI not defined"
#define COMPILE_WITH_ITBB_ASYNC_MPI 0
#endif
*/



/**
 * COMPILE_WITH_ITBB_ASYNC_MPI
 */
#ifndef COMPILE_WITH_ITBB_ASYNC_MPI_INVADE_THRESHOLD
#error "COMPILE_WITH_ITBB_ASYNC_MPI_INVADE_THRESHOLD not defined"
#define COMPILE_WITH_ITBB_ASYNC_MPI_INVADE_THRESHOLD 0.1
#endif




/**
 * Enable gui by default
 */
#ifndef CONFIG_SIERPI_ENABLE_GUI
#error "CONFIG_SIERPI_ENABLE_GUI not defined"
#define CONFIG_SIERPI_ENABLE_GUI 1
#endif


/**
 * Reuse stacks from parent sub-cluster
 */
#ifndef CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
#	define CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS	0
#endif



#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS && CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
#	error "adaptive sub-cluster stacks in combination with parent's stack reusage not implemented yet!"
#endif



/**
 * Enable/disable compilation with validation stacks and validation tests
 */
#if DEBUG
#	define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		1
#else
#	define COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS		0
#endif



/**
 * Enable/disable deletion of CCluster classes due to split operation
 */
#if 1
#	define CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION		1
#else
#	define CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION		0
#endif

#ifndef CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING
#	define CONFIG_SIERPI_MAIN_PROCESS_TO_CORE_PINNING	0
#endif


/**************************************
 * OPENMP SPECIFIC
 **************************************/
/**
 * when compiled with omp, use this extra clause appended to #pragma omp task
 */


// untie tasks to be executed on other cpu after being idle and thus interrupted
//#define OPENMP_EXTRA_TASK_CLAUSE
#ifndef OPENMP_EXTRA_TASK_CLAUSE
#	define OPENMP_EXTRA_TASK_CLAUSE	untied
#endif

/**
 * should a task be created for the second leaf of the generic tree or should
 * the computation be done by the currently running task
 *
 * TODO: default: deactivated since this boosts the performance. however, this may
 * change in the future!
 *
 * 2012-05-06: for many-core systems it seems to be faster
 */
#ifndef OPENMP_CREATE_TASK_FOR_SECOND_LEAF
#	define OPENMP_CREATE_TASK_FOR_SECOND_LEAF	1
#endif


/**************************************
 * SIMULATION SPECIFIC
 **************************************/
/**
 * allow periodic boundaries?
 * in this case, the validation for the edge comm normals and vertices cannot be applied anymore!
 */
#ifndef CONFIG_PERIODIC_BOUNDARIES
	#define CONFIG_PERIODIC_BOUNDARIES	1
#endif


/**
 * create code with normals as integer parameters for traversators
 *
 * see also configuration option config.internal_traversator_normal_as_int_parameter
 */
#define CONFIG_TRAVERSATORS_WITH_NORMALS_AS_INTS	1

#ifndef CONFIG_TBB_TASK_AFFINITIES
#	define CONFIG_TBB_TASK_AFFINITIES 0
#endif

#ifndef CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL
#	define CONFIG_ENABLE_SCAN_FORCE_SCAN_TRAVERSAL		0
#endif




#ifndef CONFIG_EXIT_ON_SMALL_TIMESTEP
#	error "CONFIG_EXIT_ON_SMALL_TIMESTEP not defined"
#	define CONFIG_EXIT_ON_SMALL_TIMESTEP 1
#endif


#ifndef CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS
#	error "CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS not defined"
#	define CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS 1
#endif

/*
 * threshold for CONFIG_EXIT_ON_INSTABILITY_THRESHOLDS and CONFIG_EXIT_ON_SMALL_NETUPDATE_CFL
 */
#ifndef CONFIG_EXIT_THRESHOLD_CFL_SIZE
#	define CONFIG_EXIT_THRESHOLD_CFL_SIZE (0.0000001)
#endif

#ifndef CONFIG_EXIT_THRESHOLD_MAX_VELOCITY
#	define CONFIG_EXIT_THRESHOLD_MAX_VELOCITY (1000)
#endif




#ifndef CONFIG_ENABLE_ASAGI
# 	error "CONFIG_ENABLE_ASAGI not defined"
#	define CONFIG_ENABLE_ASAGI 1
#endif


#ifndef CONFIG_ENABLE_NETCDF
#	error "CONFIG_ENABLE_NETCDF not defined"
#	define CONFIG_ENABLE_NETCDF 1
#endif

#ifndef CONFIG_ENABLE_SWE_BENCHMARKS
#	error "CONFIG_ENABLE_SWE_BENCHMARKS not defined"
#	define CONFIG_ENABLE_SWE_BENCHMARKS 1
#endif

#ifndef CONFIG_ENABLE_LIBXML
#	error "CONFIG_ENABLE_LIBXML not defined"
#	define CONFIG_ENABLE_LIBXML 1
#endif


#endif
