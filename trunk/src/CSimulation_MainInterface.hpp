/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CSimulation_MainInterface.hpp
 *
 *  Created on: Jul 4, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_MAININTERFACE_HPP_
#define CSIMULATION_MAININTERFACE_HPP_


/**
 * \brief interface descriptions which have to be offered by the simulation class
 */
class CSimulation_MainInterface
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * reset simulation
	 */
	virtual void reset() = 0;


	/**
	 * setup grid with adaptive simulation (e. g. displacement data)
	 */
	virtual void setup_GridDataWithAdaptiveSimulation() = 0;


	/**
	 * output verbose information
	 */
	virtual void outputVerboseInformation() = 0;


	enum ESimulationOutputType
	{
		SIMULATION_DATA_FILE,
		SIMULATION_DATA_BOUY,
		SIMULATION_DATA_INTERACTIVE
	};


	/**
	 * output simulation specific data - e. g. dart smpling points
	 */
	virtual void output_simulationSpecificData(
			const std::string &i_identifier,
			const std::string &i_parameters
	) = 0;


	/**
	 * output information about cluster tree
	 */
	virtual void output_ClusterTreeInformation() = 0;


	/**
	 * run validation routines
	 */
	virtual void action_Validation() = 0;


	/**
	 * run a single simulation timestep
	 */
	virtual void runSingleTimestep() = 0;


	/**
	 * run a single timestep including detailed benchmarks about different phases
	 */
	virtual void runSingleTimestepDetailedBenchmarks(
			double *io_edgeCommTime,			///< add time taken for edge communication traversal to this value
			double *io_adaptiveTime,			///< add time taken for adaptive traversal to this value
			double *io_splitJoinTime			///< add time taken for split/joins to this value
#if CONFIG_ENABLE_MPI
			,
			double *io_cluster_migration_mpi	///< add time taken for cluster migration to this value
#endif
	) = 0;


	/**
	 * run a radial dam break scenario with given parameters
	 */
	virtual void setup_RadialDamBreak(
			T i_x,			///< x-coordinate of center of column to set-up
			T i_y,			///< y-coordinate of center of column to set-up
			T i_radius		///< radius of column to setup
	) = 0;

	/**
	 * load terrain origin coordinate and size
	 */
	virtual void getOriginAndSize(
			T	*o_origin_x,	///< origin of domain in world-space
			T	*o_origin_y,	///< origin of domain in world-space
			T	*o_size_x,		///< size of domain in world-space
			T	*o_size_y		///< size of domain in world-space
	) = 0;

	virtual ~CSimulation_MainInterface()
	{
	}
};


#endif /* CSIMULATION_MAININTERFACE_HPP_ */
