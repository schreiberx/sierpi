#! /bin/bash

# output
#SBATCH -o /home/schreibm/workspace/sierpi/benchmarks_field/tohoku_2011_mac_velocity_conserving_wsm_performance_runs/2014_01_22_performance/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release_bdz_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001.txt
#SBATCH -e /home/schreibm/workspace/sierpi/benchmarks_field/tohoku_2011_mac_velocity_conserving_wsm_performance_runs/2014_01_22_performance/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release_bdz_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001.err
# working directory
#SBATCH -D /home/schreibm/workspace/sierpi/benchmarks_field/tohoku_2011_mac_velocity_conserving_wsm_performance_runs
# job description
#SBATCH -J sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release_bdz_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001
#SBATCH --get-user-env
#SBATCH --partition=wsm
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/schreibm/workspace/sierpi/benchmarks_field/tohoku_2011_mac_velocity_conserving_wsm_performance_runs

export KMP_AFFINITY="compact"

mpiexec.hydra -genv OMP_NUM_THREADS 32 -envall -ppn 2 -n 1 ../../build/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release_bdz -c tohoku_netcdf_intel_release.xml -A 1  -t 20000  -r 25000/5000 -v 3  -I 4  -u 1 -U 20 -b 10 -a 12 -d 10 -C 0.5 -D 834498.794062/528004.720595/output_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001_dart21401.txt,935356.566012/-817289.628677/output_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001_dart21413.txt,545735.266126/62716.4740303/output_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001_dart21418.txt,1058466.21575/765077.767857/output_d10_a12_cfl0.5_rk1_refine25000_coarsen5000_t032_n001_dart21419.txt
