#! /bin/sh

source /etc/profile.d/modules.sh

#source $HOME/bin/scons_vars.sh
#source $HOME/bin/intel_vars.sh
#source $HOME/bin/tbb_vars.sh

#module unload ccomp
#module load ccomp/intel/13.1

#module unload fortran
#module load fortran/intel/13.1

#module unload mpi.intel
#module load mpi.intel/4.1

#module load ccomp/intel/12.1
#module load netcdf/mpi/4.2
#module load tbb/4.1

. $HOME/bin/intel_vars.sh
. $HOME/bin/local_vars.sh

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$NETCDF_BASE/lib/pkgconfig

. /lrz/sys/intel/icc_131_146/composer_xe_2013.2.146/bin/compilervars.sh intel64
