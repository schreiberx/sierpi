###
### Sierpi Benchmark analysis###
### Display dart data
### Contact: breuera@in.tum.de, schreibm@in.tum.de
###

# path to benchmark directory
benchmark_dir <- getwd()

## datasets to analyze
benchmark_output <- "2013_12_29"
benchmark_measured_data <- "tohoku_dart_data"



visualize_darts <- c("21401", "21413", "21418", "21419")
#visualize_darts = c("21401", "21413")
#visualize_darts = c("21418", "21419")
#visualize_darts = c("21401")
#visualize_darts = c("21413")
#visualize_darts = c("21418")
visualize_darts = c("21419")


# 0: bouy data plots
# 1: error plots
# 2: cell distribution
# 3: cell distribution + error in legend
plotMode <- 1

plot_points <- 1
plot_points <- 1
points_number_of <- 30


# output data to file (only for plotMode 1 and 3)
writeErrorToFile <- sprintf("%s_error.txt", benchmark_output)
#writeErrorToFile <- ""

################################
###### CELL DISTRIBUTION #######
################################

cellDist_plotMode_yrange <- c(0, 2**(22+1)*1.1)



##########################
###### ERROR PLOTS #######
##########################

# benchmark file to compare with
error_benchmark_dataset_baseline <- sprintf("output_d22_a00_cfl0.5_rk1_refine50_coarsen10_t064_n006_dart%s.txt", visualize_darts[1])

# we compute 20000 seconds and sample each 10 seconds to compare the data
error_spline_sampling_points <- 80000

# 0: euclidian
# 1: 1-norm
use_norm = 0

# error area with min/max
# sample over entire area
error_area_xmin <- 0
error_area_xmax <- 20000
#error_area_xmax <- 5000

error_plotMode_yrange <- c(-0.5, 0.5)


# Don't consider depth 28, since this is getting unstable
limit_d <- c(10,16,22)
#limit_d <- 8
limit_d <- -1
limit_d_even <- -1

limit_a <- c(0, 6, 12)
limit_a <- -1

param_xlim <- c(0,20000)

#limit_refine_level <- c(50,500,5000,50000)
#limit_refine_level <- c(5000,50000)
limit_refine_level <- c(5000,50000)
limit_refine_level <- -1

limit_max_level <- c(10,16,22)
limit_max_level <- -1
#limit_max_level <- c(22)

#limit_cfl <- c(0.5)
limit_cfl <- -1

limit_rk <- -1

line_width <- 1

#benchmark_output_group_regexp <- "output_d([0-9]*)_a([0-9]*)_cfl(.*)_dart([0-9]*)\\.txt"
#benchmark_output_group_regexp <- "^output_d([0-9]*)_a([0-9]*)_cfl(.*)_rk([0-9])_refine.*_coarsen.*_dart([0-9]*)\\.txt$"
benchmark_output_group_regexp <- "^output_d([0-9]*)_a([0-9]*)_cfl(.*)_rk([0-9])_refine(.*)_coarsen(.*)_t.*dart([0-9]*)\\.txt$"


# error 1-norm for d=6, a=0
max_error_norm <- 0.026121
max_error_norm <- -1
#max_error_norm <- max_error_norm*0.9
#smax_error_norm <- max_error_norm*1.1

#max_number_of_cells <- 2**15
max_number_of_cells <- 2097152
max_number_of_cells <- -1
#max_number_of_cells <- max_number_of_cells*0.9

#################################################################################
#################################################################################
#################################################################################


if (plotMode == 3 || plotMode == 1)
{
  writeErrorToFile <- paste(benchmark_dir, writeErrorToFile, sep="/")
  if (writeErrorToFile != "")
  {
    headers<-c("depth", "adapt", "refine", "coarsen", "errorNorm")
    
    if (plotMode == 3)
      headers <- c(headers, "avgCellsPerTimestep")
    
    write(headers,
          ncolumns = length(headers),
          file = writeErrorToFile,
          append = FALSE,
          sep = "\t"
    )
  }
}



benchmark_measured_data_list <- list(
  "21401" = "21401-03142011_notide.txt",
  "21413" = "21413_5day-03142011_notide.txt",
  "21414" = "21414_5day-03132011_notide.txt",
  "21415" = "21415_5day-03132011_notide.txt",
  "21418" = "21418_5day-03132011_notide.txt",
  "21419" = "21419_5day-03142011_notide.txt",
  "32412" = "32412_5day-03132011_notide.txt",
  "43412" = "43412_5day-03132011_notide.txt",
  "46404" = "46404_5day-03132011_notide.txt",
  "51407" = "51407_5day-03132011_notide.txt",
  "51425" = "51425-03182011_notide.txt",
  "52402" = "52402_5day-03132011_notide.txt",
  "52403" = "52403-03182011_notide.txt",
  "52405" = "52405-03182011_notide.txt",
  "52406" = "52406-03182011_notide.txt"
)


benchmark_ylim <- list(
  "21401" = c(-0.5,0.8),
  "21413" = c(-0.8,1.2),
  "21414" = NULL,
  "21415" = NULL,
  "21418" = c(-1.7,2.2),
  "21419" = c(-0.5,0.7),
  "32412" = NULL,
  "43412" = NULL,
  "46404" = NULL,
  "51407" = NULL,
  "51425" = NULL,
  "52402" = NULL,
  "52403" = NULL,
  "52405" = NULL,
  "52406" = NULL
)


benchmark_output_dir <- paste(benchmark_dir, benchmark_output, sep="/")
benchmark_measured_data_dir <- paste(benchmark_dir, benchmark_measured_data, sep="/")


attach(mtcars)
par(mfrow=c(length(visualize_darts), 1))

#use_dart_nr <- visualize_darts[1]
#par(mfrow=c(1, 1))

#list.files(path=benchmark_output_dir, pattern=benchmark_output_group_regexp)

for (use_dart_nr in visualize_darts)
{
  benchmark_measured_data_filename = benchmark_measured_data_list[[use_dart_nr]]
  
  param_ylim = benchmark_ylim[[use_dart_nr]]
  
  # find dart file
  benchmark_measured_data_list_file <- paste(paste(benchmark_dir, benchmark_measured_data, sep="/"), benchmark_measured_data_filename, sep="/")
  
  # BENCHMARK OUTPUT FILES
  benchmark_output_files <- list.files(path=benchmark_output_dir, pattern=benchmark_output_group_regexp)
  
  if (length(benchmark_output_files) == 0)
  {
    print("NO files found")
    return
  }
  
  # READ DART DATASET
  benchmark_measured_data_list_file_dataset <- read.table(benchmark_measured_data_list_file)
  
  tmp_benchmark_output_files <- c()

  # use limitations to draw only a subset of images
  for (benchmark_output_filename in benchmark_output_files)
  {
    s <- sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4 \\5 \\6 \\7", benchmark_output_filename)

    tmp <- unlist(strsplit(s, split=" "))
    nd <- as.integer(tmp[1])
    na <- as.integer(tmp[2])
    ncfl <- as.double(tmp[3])
    nrk <- as.integer(tmp[4])
    refine <- tmp[5]
    ndart <- as.integer(tmp[7])
  
    if (limit_d != -1 && length(which(limit_d == nd)) == 0)
    {
      print("limit_d failed")
      next
    }

    if (limit_d_even)
      if ((nd %% 2) == 1)
      {
        print("limit_d_even failed")
        next
      }
    
    if (limit_a != -1 && length(which(limit_a == na)) == 0)
    {
      print("limit_a failed")
      next
    }
    
    max_level <- na+nd
    if (limit_max_level != -1 && length(which(limit_max_level == max_level)) == 0)
    {
      print("limit_max_level failed")
      next
    }
  
    if (limit_refine_level != -1 && length(which(limit_refine_level == refine)) == 0)
    {
      print("limit_refine_level failed")
      next
    }
    
    if (limit_cfl != -1 && length(which(limit_cfl == ncfl)) == 0)
    {
      print("limit_cfl failed")
      next
    }
    
    if (limit_rk != -1 && length(which(limit_rk == nrk)) == 0)
    {
      print("limit_rk failed")
      next
    }
    
    if (use_dart_nr != ndart)
    {
      print("use_dart_nr failed")
      next
    }

    tmp_benchmark_output_files <- c(tmp_benchmark_output_files, benchmark_output_filename)
  }

  benchmark_output_files <- tmp_benchmark_output_files

  rainbow_map <- c("black", rainbow(length(benchmark_output_files)))

  # plotting symbols
  plot_pch <- 1:length(benchmark_output_files)
  for (i in 1:length(benchmark_output_files))
  {
    plot_pch[i] <- (i %% 10) + 1
  }


  # load error baseline data?
  if (plotMode == 1 || plotMode == 3)
  {
    # ERROR PLOTS?
    error_benchmark_output_file <- paste(benchmark_output_dir, error_benchmark_dataset_baseline, sep="/")
    benchmark_baseline_dataset <- read.table(error_benchmark_output_file, sep=",", header=TRUE)
    
    # append timestep 20000 if required
    if (tail(benchmark_baseline_dataset$time, n=1) < error_area_xmax)
    {
      append(benchmark_baseline_dataset$time, error_area_xmax)
      append(benchmark_baseline_dataset$surface_elevation, 0)
    }
    
    # compute approximating spline
    interpolated_baseline_dataset <- spline(benchmark_baseline_dataset$time,benchmark_baseline_dataset$surface_elevation, n=error_spline_sampling_points, method="natural", xmin=error_area_xmin, xmax=error_area_xmax, ties=0)
  }

  if (plotMode == 0)
  {
    # DART PLOTS
    
    plot(benchmark_measured_data_list_file_dataset, type="l", xlab="", ylab="", ylim=param_ylim, xlim=param_xlim, col=rainbow_map[1])
    title(main=paste("Dart station ", use_dart_nr, sep=""), xlab="time since earthquake (seconds)", ylab="height relative to sealevel (meters)")
  
    legend_data <- c("Bouy station data")
    c <- 2
  }else if (plotMode == 1)
  {
    # start error plot
    plot(c(error_area_xmin, error_area_xmax), c(999999,9999999), type="l", xlab="", ylab="", ylim=error_plotMode_yrange, param_xlim, col="black")
#    text(3550, 0.4, error_benchmark_dataset_baseline, cex=0.8)
    title(main=paste("Error plot ", use_dart_nr, sep=""), xlab="time since earthquake (seconds)", ylab="deviation of reference solution (meters)")
    
    legend_data <- c()
    c <- 1
  }else if (plotMode == 2 || plotMode == 3)
  {
    # start error plot
    plot(param_xlim, c(-99999999,-9999999), type="l", xlab="", ylab="", ylim=cellDist_plotMode_yrange, xlim=param_xlim, col="black")
    title(main=paste("Cell distribution plot "), xlab="time since earthquake (seconds)", ylab="cells")

    legend_data <- c()
    c <- 1
  }

  print(benchmark_output_files)
  for (benchmark_output_filename in benchmark_output_files)
  {
    tmp <- unlist(strsplit(sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4 \\5 \\6 \\7", benchmark_output_filename), split=" "))
    nd <- tmp[1]
    na <- tmp[2]
    ncfl <- tmp[3]
    nrk <- tmp[4]
    refine <- tmp[5]
    coarsen <- tmp[6]

    dataset_name <- ""
    dataset_name <- paste(paste(dataset_name, "d=", sep=""), nd, sep="")
    dataset_name <- paste(paste(dataset_name, "/", sep=""), na, sep="")

    dataset_name <- paste(dataset_name, ", ", sep="")
    dataset_name <- paste(paste(dataset_name, "rc=", sep=""), refine, sep="")
    dataset_name <- paste(paste(dataset_name, ",", sep=""), coarsen, sep="")

    # benchmark output filename
    benchmark_output_file <- paste(benchmark_output_dir, benchmark_output_filename, sep="/")
    benchmark_single_dataset <- read.table(benchmark_output_file, sep=",", header=TRUE)
        
    # compute error norm
    if (plotMode == 1 || plotMode == 3)
    {      
      # append timestep 20000 if required
      if (tail(benchmark_single_dataset$time, n=1) < error_area_xmax)
      {
        append(benchmark_single_dataset$time, error_area_xmax)
        append(benchmark_single_dataset$surface_elevation, 0)
      }
      
      # compute approximating spline
      interpolated_single_dataset <- spline(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation, n=error_spline_sampling_points, method="natural", xmin=error_area_xmin, xmax=error_area_xmax, ties=0)
      
      # error data
      error_data <- interpolated_baseline_dataset$y - interpolated_single_dataset$y

      #Compute Euclidian norm
      if (use_norm == 0)
      {
        normalized_data <- error_data
        norm <- sqrt(sum(as.double(normalized_data)^2))/error_spline_sampling_points
      }else
      {
        #Compute 1-norm
        norm <- sum(abs(as.double(error_data)))/error_spline_sampling_points
      }

      if (max_error_norm != -1)
      {
        if (norm > max_error_norm)
          next
      }
      
      if (max_number_of_cells != -1)
      {
        if (avgTotalCells > max_number_of_cells)
          next
      }
      
      if (writeErrorToFile != "")
      {
        data<-c(nd, na, refine, coarsen, norm)

        if (plotMode == 3)
        {
          avgTotalCells <- sum(as.double(benchmark_single_dataset$number_of_global_cells))/length(benchmark_single_dataset$number_of_global_cells)
          data <- c(data, avgTotalCells)
        }

        write(data,
              ncolumns = length(data),
              file = writeErrorToFile,
              append = TRUE,
              sep = "\t"
        )
      }

      dataset_name <- paste(dataset_name, ", ", sep="")
      dataset_name <- paste(paste(dataset_name, "error=", sep=""), round(norm, digits=6), sep="")
    }

    if (plotMode == 0)
    {
      # bouy data plots
      lines(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation, type="l", col=rainbow_map[c], lwd=line_width)
      
      if (plot_points)
        point_base_set <- list(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation)
    }else if (plotMode == 1)
    {
      # ERROR PLOTS      
      lines(interpolated_single_dataset$x, error_data, type="l", col=rainbow_map[c], lwd=line_width)
      
      if (plot_points)
        point_base_set <- list(interpolated_single_dataset$x, error_data)
    }else if (plotMode == 2 || plotMode == 3)
    {
      # triangle distriubtion plots

      lines(benchmark_single_dataset$time, benchmark_single_dataset$number_of_global_cells, type="l", col=rainbow_map[c], lwd=line_width)
      
      if (plot_points)
        point_base_set <- list(benchmark_single_dataset$time, benchmark_single_dataset$number_of_global_cells)
      
    }
    
    if (plot_points)
    {
      point_set <- spline(point_base_set[[1]], point_base_set[[2]], n=points_number_of)
      points(point_set[[1]], point_set[[2]], type="p", col=rainbow_map[c], pch=plot_pch[c])
    }
    

    legend_data <- c(legend_data, dataset_name)
    c <- c+1
  }

  #legend("topright", legend_data, text.width=10000, col=rainbow_map, lty=1, pch=plot_pch, cex=0.5)

  if (plot_points)
  {
#    legend("topright", legend_data, col=rainbow_map, lty=1, lwd=line_width, pch=plot_pch, cex=1)
    legend("topleft", legend_data, col=rainbow_map, lty=1, lwd=line_width, pch=plot_pch, cex=0.7)
  }else
  {
    legend("topright", legend_data, col=rainbow_map, lty=1, lwd=line_width, cex=0.8)
  }

  #dev.off()
}