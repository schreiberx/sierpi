#! /usr/bin/python

import sys
import time
import commands
import os
import math
import re

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''

dart_stations = '834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt'
p = re.compile('([^/]*).txt')


#
# Sierpinski parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set=[
		'sierpi_intel_mpi_omp_scan_force_threading_sacsp_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release'
	]

	# initialization depth
	sierpi_sim_depth_set=[10, 16, 22]

	# ADAPTIVE DEPTH SET
	sierpi_sim_adaptive_depth_set=[0, 6, 12]

	# maximum refinement depth
	sierpi_sim_max_depth = 22

	# THREADS SET
	# Use 2 threads since cluster is runned in hyperthreading mode!
	#
	sierpi_sim_threads_set=[16]

	sierpi_refinement_params=[[50,10], [500,100], [5000,1000], [50000,10000]]

	# MPI NODE SET
	sierpi_sim_mpi_nodes_set=[6]

	sierpi_max_cores = 256

	cfl=0.5



# bin set
for bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			for refinement_params in sierpi_refinement_params:

				if depth+adaptive_depth > sierpi_sim_max_depth:
					continue

				if bin.find('nothreading') > 0:
					sierpi_sim_threads_set_ = [1]
				else:
					sierpi_sim_threads_set_ = sierpi_sim_threads_set

				# threads set
				for num_threads in sierpi_sim_threads_set_:

					# threads set
					for num_mpi_nodes in sierpi_sim_mpi_nodes_set:

						if sierpi_max_cores < num_mpi_nodes*num_threads:
							continue

						# estimate number of cells
						max_cells=2**(depth+adaptive_depth)

						def get_estimated_runtime(max_cells, splitting_size):

							# estimate clusters
							clusters=max_cells/splitting_size
							clusters+=1

							# clusters per thread
							clusters_per_thread=clusters/num_threads
							clusters_per_thread+=1

							# cells per thread
							cells_per_thread=clusters_per_thread*splitting_size

							return cells_per_thread*0.0001

						estimated_runtime_string="24:00:00"

						initial_domain_splits=int(math.log(num_mpi_nodes, 2))

						sierpi_run=bin
						sierpi_run+=' -c tohoku_netcdf_intel_release.xml'
						sierpi_run+=' -A 1 '
						sierpi_run+=' -t 20000 '
						sierpi_run+=' -r '+str(refinement_params[0])+'/'+str(refinement_params[1])
						sierpi_run+=' -v 3 '
						sierpi_run+=' -I 12 '
						sierpi_run+=' -u 1 -U 20'
						sierpi_run+=' -b 10'
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -d '+str(depth)
						sierpi_run+=' -C '+str(cfl)

						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params

						id = 'd'+str(depth).zfill(2)
						id += '_a'+str(adaptive_depth).zfill(2)
						id += '_cfl'+str(cfl)
						id += '_rk1_refine'+str(refinement_params[0])+'_coarsen'+str(refinement_params[1])

						id += '_t'+str(num_threads).zfill(3)
						id += '_n'+str(num_mpi_nodes).zfill(3)

						job_description_string = bin+'_'+id

						sierpi_run += ' -D '+p.sub('output_'+id+'_dart\\1.txt', dart_stations)

						job_filepath = jobfile_directory+'/'+job_description_string+'.cmd'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'
						error_output_filepath = jobfile_directory+'/'+job_description_string+'.err'

						# assume 63 available GB
						mem_per_cpu=(63*1024)/num_threads+1

						job_file_content="""#! /bin/bash

# output
#SBATCH -o """+output_filepath+"""
#SBATCH -e """+error_output_filepath+"""
# working directory
#SBATCH -D """+working_directory+"""
# job description
#SBATCH -J """+job_description_string+"""
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks="""+str(num_mpi_nodes)+"""
#SBATCH --cpus-per-task="""+str(num_threads)*2+"""
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time="""+estimated_runtime_string+"""

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd """+working_directory+"""

export KMP_AFFINITY="compact"

mpiexec.hydra -genv OMP_NUM_THREADS """+str(num_threads)+""" -envall -ppn """+str(16/num_threads)+""" -n """+str(num_mpi_nodes)+""" ../../build/"""+sierpi_run+"""
"""

						print "Writing jobfile '"+job_filepath+"'"
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()
