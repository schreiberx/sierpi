#! /bin/bash

for i in sierpi_intel_mpi_omp_scan_force_threading_sacsp_hyperbolic_parallel_tsunami_netcdf_t*cmd; do
	i=${i/.cmd/.txt}
	echo "x $i";
	grep GMCPS $i 2> /dev/null;
done
