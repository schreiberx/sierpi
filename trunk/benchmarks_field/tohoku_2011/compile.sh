#! /bin/bash

P=`pwd`

cd ../..
#make clean

scons --xml-config=$P/tohoku_netcdf_intel_release.xml --enable-exit-on-instability-thresholds=on --mode=release --threading=omp --enable-scan-force-threading-traversal=on -j 4

#scons --xml-config=$P/tohoku_netcdf_intel_release.xml --enable-exit-on-instability-thresholds=on --mode=release --threading=omp --enable-scan-force-threading-traversal=on --enable-gui=on -j 4
