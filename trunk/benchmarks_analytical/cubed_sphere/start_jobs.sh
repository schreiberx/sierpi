#! /bin/bash

cd $1

for i in *.cmd; 
do
	echo " + $i";
	sbatch $i;
done
