#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 11.03.2013

@author: Flo
'''


import matplotlib.pyplot as pyplot
import matplotlib.dates as mdates
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import os, re, sys

# import matplotlib as mpl
# mpl.rcParams['text.usetex']=True
# mpl.rcParams['text.latex.unicode']=True

pyplot.rc('text', usetex=True)
# pyplot.rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Sans serif']})
pyplot.rc('font',**{'family':'serif','serif':['Computer Modern']})
# pyplot.rc('font', family='serif')

folder = "test9"

basepath = "/home/flo/workspace/sierpi_kleinfl/benchmarks_field/cubed_sphere/test"
basepath = "../cubed_sphere_results"

mindepth = 10
maxdepth = 20

xticks_stepsize = 1

filefmt = "pdf"

distr_set = {"cosine": ("o", "cosine bell"), "cylinder": ("v", "slotted cylinder")}
cfl_set = [0.5, 0.6, 0.75]

a_set = [0, 4, 6, 8]

old = True
old = False
if old: distr_set = ["cosine"]

multipage = 1
singlepage = 1

fontsize = 18


def plotBars(ax, ydatalist, labellist=[], datawidth=0.8, colorset=None, xticklabels=[], ylim=[], 
			xlabel="", ylabel="", title="", legendsize=6, twinax=False, twinaxcut=0, twinylabel=""):
	dwidth = datawidth
	bwidth = dwidth / len(ydatalist)
	x = np.arange(len(ydatalist[0]))
	if twinax:
		ax2 = ax.twinx()
	for ctr in range(len(ydatalist)):
		color = colorset[ctr] if colorset else None
		label = labellist[ctr] if labellist else None
		if twinax:
			ax.bar(x[:twinaxcut] + ctr*bwidth, ydatalist[ctr][:twinaxcut], bwidth, label=label, color=color)
			ax2.bar(x[twinaxcut:] + ctr*bwidth, ydatalist[ctr][twinaxcut:], bwidth, label=label, color=color)
		else:
			ax.bar(x + ctr*bwidth, ydatalist[ctr], bwidth, label=label, color=color)
	ax.set_xticks(x+dwidth/2)
	if xticklabels: ax.set_xticklabels(xticklabels)
	ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
	if ylim: ax.set_ylim(ylim)
	ax.set_xlabel(xlabel, fontsize=fontsize)
	ax.set_ylabel(ylabel, fontsize=fontsize)
	if twinax:
		ax2.set_ylabel(twinylabel, fontsize=fontsize)
		ax2.yaxis.set_tick_params(labelsize=fontsize)
	if legendsize: ax.legend(prop={'size':legendsize})
	ax.set_title(title)
	ax.xaxis.set_tick_params(labelsize=fontsize)
	ax.yaxis.set_tick_params(labelsize=fontsize)


class Plot():
	
	def __init__(self, mode=""):
		self.sbrtimesteps = False
		if mode == "m":
			multipage = 1
			singlepage = 0
		elif mode == "s":
			multipage = 0
			singlepage = 1
		elif mode in ("sm", "ms"):
			multipage = singlepage = 1
		elif mode == "sbr":
			multipage = singlepage = 1
			self.sbrtimesteps = True
		self.adapt = mode == "adapt"
# 		self.figure = pyplot.figure("test")
		
	def savetofile(self, filename, extension, prefix=""):
		name = "{}_{}".format(prefix, filename) if prefix else filename
		pyplot.savefig(os.path.join(basepath, "{}.{}".format(name, extension)))
		
	def addtomultipage(self, pdfpages):
		pdfpages.savefig(bbox_inches="tight", pad_inches=0.7)

	def cflSet(self, benchmark):
		cflset = [0.3, 0.4] if benchmark == "deformational_flow" else cfl_set
		cflset = [0.5]
		return cflset
	
# 	def writeCSV(self, filename):
	
# 	def addPlot(self, xdata, ydata, label="", marker="", linestyle="-", linewidth=1.0):
# 		ax.plot(xdata, y, label=label, marker=distr_set[distr][0], linestyle=linestyle, linewidth=linewidth)
	
	def savePlot(self, fig, ax, xlabel="", ylabel="", title="", 
			singleplot=False, logscale=False, ylim=[], filename="", pdfpages=None, pdfpageslog=None,
			adjust_xticks=True, xlim=[]):
		ax.set_ylabel(ylabel, fontsize=fontsize)
		ax.set_xlabel(xlabel, fontsize=fontsize)
		fig.set_size_inches(9, 6)
		fig.tight_layout()
		xstart, xend = ax.get_xlim()
		if adjust_xticks:
			ax.set_xlim(xstart-0.5, xend+0.5)
			ax.xaxis.set_ticks(np.arange(xstart, xend+xticks_stepsize, xticks_stepsize))
		ax.xaxis.set_tick_params(labelsize=fontsize)
		ax.yaxis.set_tick_params(labelsize=fontsize)
		if xlim:
			ax.set_xlim(xlim)
		if not singleplot:
			ax.legend(prop={'size':8})
		if logscale:
			ax.set_yscale('log')
		else:
			if ylim:
				ax.set_ylim(ylim)
		if singlepage: 
			self.savetofile(filename, filefmt)
		if multipage:
			ax.set_title(title, fontsize=fontsize) 
			if pdfpages: self.addtomultipage(pdfpages)
			if pdfpageslog:
				if ylim: 
					ax.set_ylim([0.01,1.1])
				ax.set_yscale('log')
				self.addtomultipage(pdfpageslog)
		pyplot.clf()

	def getDataPerTS(self, depths):
		benchmark = "solid_body_rotation"
		filename = "d{depth}_a00_cfl0.5_{distr}_rk1_refine50000_coarsen10000_maccluster_solid_body_rotation_equiangular.txt"
		ydata = {elem: [] for elem in distr_set}
		for distr in distr_set:
			for depth in depths:
				depthdata = []
				ydata[distr].append(depthdata)
				fmatch = filename.format(depth=depth, distr=distr)
				for fname in os.listdir(basepath):
					if fname != fmatch:
						continue
					with open(os.path.join(basepath, fname)) as f:
						text = f.readlines()
						ts = l1 = l2 = linf = None
						for line in text:
							line = line.strip()
							match = re.match("current timestep: (.*)", line)
							if match:
								ts = float(match.group(1))
								l1 = l2 = linf = 0
							match = re.match("global error l1: (.*)", line)
							if match:
								l1 = float(match.group(1))
							match = re.match("global error l2: (.*)", line)
							if match:
								l2 = float(match.group(1))
							match = re.match("global error linf: (.*)", line)
							if match:
								linf = float(match.group(1))
							if ts is not None and l1 and l2 and linf:
								depthdata.append([ts, l1, l2, linf])
# 								ydata[distr].append([ts, l1, l2, linf])
		return ydata
		

	def getData(self, depths, proj, benchmark):
		cflset = self.cflSet(benchmark)
		xdata = depths
		ydata = []
		yadapt = []
		filename = "sierpi_intel_mpi_release_bdz_{proj}_d{depth}_a00_cfl0.25_rk1_refine50_coarsen10_t064_n008.txt"
		filename = "maccluster_{benchmark}{projection}_d{depth}_a00_cfl{cfl}_{distr}.txt"
		filename = "d{depth}_a{adapt}_cfl{cfl}_{distr}_maccluster_{benchmark}{projection}.txt"
		filename2 = "d{depth}_a{adapt}_cfl{cfl}_{distr}_rk1_refine50000_coarsen10000_maccluster_{benchmark}{projection}.txt"
		if old: filename = "maccluster_{benchmark}{projection}_d{depth}_a00_cfl{cfl}_rk1_refine50_coarsen10_t064_n001.txt"
		for p in proj:
			for cfl in cflset:
				for distr in distr_set:
					y = []
					for depth in depths:
						for adapt in a_set:
							if adapt and depth != 14:
								continue
							stradapt = str(adapt).zfill(2)
							strdepth = "d{}".format(depth)
							ts = l1 = l2 = linf = de = da = avgS = avgG = totalTS = totalRT = EDMBPT = MCP = 0
							for fname in os.listdir(basepath):
								fmatch1 = filename.format(benchmark=benchmark, projection=proj[p], depth=depth, cfl=cfl, distr=distr, adapt=stradapt)
								fmatch2 = filename2.format(benchmark=benchmark, projection=proj[p], depth=depth, cfl=cfl, distr=distr, adapt=stradapt)
								if fname != fmatch1 and fname != fmatch2:
									continue
								with open(os.path.join(basepath, fname)) as f:
									text = f.readlines()
									for line in text:
										line = line.strip()
										match = re.match("current timestep: (.*)", line)
										if match:
											sts = match.group(1)
											if sts != "5" and sts != "1036800" and sts != "0":
												print(fname)
												sts = 5
											ts = int(sts)
										match = re.match("global error l1: (.*)", line)
										if match:
											l1 = float(match.group(1))
										match = re.match("global error l2: (.*)", line)
										if match:
											l2 = float(match.group(1))
										match = re.match("global error linf: (.*)", line)
										if match:
											linf = float(match.group(1))
										match = re.match("relative avg error edges: (.*)", line)
										if match:
											de = float(match.group(1))
										match = re.match("relative avg error area: (.*)", line)
										if match:
											da = float(match.group(1))
										match = re.match("average cell area sphere: (.*)", line)
										if match:
											avgS = float(match.group(1))
										match = re.match("average cell area grid: (.*)", line)
										if match:
											avgG = float(match.group(1))
										match = re.match("(.*) TS \(Timesteps\)", line)
										if match:
											totalTS = float(match.group(1))
										match = re.match("(.*) RT \(REAL_TIME\)", line)
										if match:
											totalRT = float(match.group(1))
										match = re.match("(.*) MCP \(Million Cells processed\) \(local\)", line)
										if match:
											MCP = float(match.group(1))
										match = re.match("(.*) EDMBPT \(CellData Megabyte per Timestep \(RW\)\)", line)
										if match:
											EDMBPT = float(match.group(1))
										match = re.match("(.*) GMCP \(Global Million Cells Processed\) \(global\)", line)
										if match:
											GMCP = float(match.group(1))
	# 						print(l1, l2, linf, de, da, ts)
							if ts and l1 and l2: # and linf and de and da:
								if adapt:
									ya = [l1, l2, linf, de, da, avgS, avgG, totalTS, totalRT, MCP, EDMBPT, GMCP]
									yadapt.append({"projection": p, "cfl": cfl, "distr": distr, "ydata": ya, "depth": depth, "adapt": adapt})
								else:
									y.append([l1, l2, linf, de, da, avgS, avgG, totalTS, totalRT, MCP, EDMBPT, GMCP])
					if y:
						ydict = {"projection": p, "cfl": cfl, "distr": distr, "ydata": y}
# 						print(ydict)
						ydata.append(ydict)
		return ydata, yadapt
	
	
	def plotErrorsAdaptivity(self, benchmark, proj, ydata, yadapt, depths, plotlist, pdfpages):
# 		print(ydata)
# 		print(yadapt)
		cflset = [0.5]
		if not yadapt:
			return
		for depth in depths:
			for adapt in a_set:
				tsplotsets = []
				xxplotsets = []
				for distr in distr_set:
					for cfl in cflset:
						plotsets = []
						plotsets2 = []
						for p in proj:
							for data in yadapt:
								if not (data["projection"] == p and data["cfl"] == cfl 
									and data["distr"] == distr and data["adapt"] == adapt and data["depth"] == depth):
									continue
								y = data["ydata"]
								label = "d {depth}, a {adapt}, {proj}".format(depth=data["depth"], adapt=data["adapt"], proj=p)
								a0depth = data["depth"] + data["adapt"]
								y2 = self.getDataByDepth(ydata, cfl, a0depth, p, distr, depths)
								label2 = "d {depth}, a {adapt}, {proj}".format(depth=a0depth, adapt=0, proj=p)
								if not y2: 
									continue
								plotsets.append((y[:3], label))
								plotsets.append((y2[:3], label2))
								plotsets2.append((y[:3]+[y[11]], label))
								plotsets2.append((y2[:3]+[y2[11]], label2))
								tsplotsets.append((y[7:9], label+", {}".format(distr)))
								tsplotsets.append((y2[7:9], label2+", {}".format(distr)))
								xxplotsets.append((y[9:11], label+", {}".format(distr)))
								xxplotsets.append((y2[9:11], label2+", {}".format(distr)))
						if plotsets:
							self.plotErrorNormBars(plotsets, pdfpages, benchmark, depth, adapt, distr)
							self.plotErrorNormsAdaptivity(plotsets2, pdfpages, benchmark, depth, adapt, distr)
				if tsplotsets:
					self.plotTSandRT(tsplotsets, pdfpages, benchmark, depth, adapt)
				if xxplotsets:
					self.plotMCPandEDMBPT(xxplotsets, pdfpages, benchmark, depth, adapt)
	
	
	def plotErrorNormsAdaptivity(self, plotsets, pdfpages, benchmark, depth, adapt, distr):
		colorset = self.colorSet()
		dwidth = 0.8
		bwidth = dwidth / len(plotsets)
		x = np.arange(3)
		fig, ax = pyplot.subplots()
		fig.set_size_inches(9, 6)
		ydatalist = [elem[0] for elem in plotsets]
		labellist = [elem[1] for elem in plotsets]
		xlabels = ["$l_1$", "$l_2$", "$l_\\infty$", "MCP"]
		plotBars(ax, ydatalist, labellist, colorset=["r", "b", "g", "orange", "k"], ylim=[0,1],
				xticklabels=xlabels, xlabel="", ylabel="Error norm", title="", twinax=True, twinaxcut=3, twinylabel="Million cells processed (MCP)")
		fig.tight_layout()
		if multipage: self.addtomultipage(pdfpages)
		filename = "error_norms_adaptivity_{}_d{}_a{}".format(distr, depth, adapt)
		if singlepage: self.savetofile(filename, "pdf")
		with open(os.path.join(basepath, filename+".csv"), "w") as csvfile:
			csvfile.write(";".join(["depth", "adapt", "l1", "l2", "linf", "MCP"])+"\n")
			csvfile.write(";".join([str(elem) for elem in [depth, adapt] + ydatalist[0]])+"\n")
			csvfile.write(";".join([str(elem) for elem in [depth+adapt, 0] + ydatalist[1]])+"\n")
		pyplot.clf()
	
	
	def plotErrorNormBars(self, plotsets, pdfpages, benchmark, depth, adapt, distr):
		colorset = self.colorSet()
		dwidth = 0.8
		bwidth = dwidth / len(plotsets)
		x = np.arange(3)
		fig, ax = pyplot.subplots()
		ctr = 0
		for y, label in plotsets:
			ax.bar(x + ctr*bwidth, y, bwidth, label=label, color=colorset[ctr])
			ctr += 1
		ax.set_xticks(x+dwidth/2)
		ax.set_xticklabels(["$l_1$", "$l_2$", "$l_\\infty$"])
		ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
		ax.set_ylim(0,1)
		ax.set_xlabel("Error norm")
		ax.set_ylabel("Error")
		ax.legend(prop={'size':6})
		ax.set_title("Error norms $l_1$, $l_2$, $l_\\infty$ for {}".format(benchmark.replace("_", " "))
						+ " with {}: \n".format(distr_set[distr][1]) 
						+ "comparison of d {}, a {} and d {}, a 0".format(depth, adapt, depth+adapt))
		if multipage: self.addtomultipage(pdfpages)
		pyplot.clf()
	
	
	def plotMCPandEDMBPT(self, plotsets, pdfpages, benchmark, depth, adapt):
		colorset = self.colorSet()
		dwidth = 0.8
		bwidth = dwidth / len(plotsets)
		x = np.arange(2)
		fig, ax = pyplot.subplots()
		ax2 = ax.twinx()
		ctr = 0
		for y, label in plotsets:
			ax.bar(x[:1] + ctr*bwidth, y[:1], bwidth, label=label, color=colorset[ctr])
			ax2.bar(x[1:] + ctr*bwidth, y[1:], bwidth, label=label, color=colorset[ctr])
			ctr += 1
		ax.set_xticks(x+dwidth/2)
		ax.set_xticklabels(["MCP", "EDMBPT"])
		ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
		ax.set_ylabel("MCP")
		ax2.set_ylabel("EDMBPT")
		ax2.legend(prop={'size':6})
		ax.set_title("Million cells processed (MCP) and Cell data megabyte per timestep (EDMBPT) for {} \n".format(benchmark.replace("_", " "))
						+ " with cosine bell and slotted cylinder distributions \n" 
						+ " comparing d {}, a {} and d {}, a 0".format(depth, adapt, depth+adapt))
		if multipage: self.addtomultipage(pdfpages)
		pyplot.clf()
	
	
	def plotTSandRT(self, tsplotsets, pdfpages, benchmark, depth, adapt):
		colorset = self.colorSet()
		dwidth = 0.8
		bwidth = dwidth / len(tsplotsets)
		x = np.arange(2)
		fig, ax = pyplot.subplots()
		ctr = 0
		for y, label in tsplotsets:
			ax.bar(x + ctr*bwidth, y, bwidth, label=label, color=colorset[ctr])
			ctr += 1
		ax.set_xticks(x+dwidth/2)
		ax.set_xticklabels(["Timesteps (TS)", "Real time seconds (RT)"])
		ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
		ax.legend(prop={'size':6})
		ax.set_title("Simulation timesteps and real time simulation seconds for {} \n".format(benchmark.replace("_", " "))
						+ " with cosine bell and slotted cylinder distributions \n" 
						+ " comparing d {}, a {} and d {}, a 0".format(depth, adapt, depth+adapt))
		if multipage: self.addtomultipage(pdfpages)
		pyplot.clf()
	
	
	def plotSbrErrorsPerTS(self, depths, ydata, pdfpages):
		errors = [0, "$l_1$", "$l_2$", "$l_\\infty$"]
		for distr in distr_set:
			for d in range(len(ydata[distr])):
				data = ydata[distr][d]
				depth = depths[d]
				fig = pyplot.figure()
				ax = fig.add_subplot(1,1,1)
# 				print(data)
				for i in range(1, 4):
					x = [elem[0] for elem in data]
					y = [elem[i] for elem in data]
# 					print(x,y)
					label = "{}, {}, d{}".format(errors[i], distr_set[distr][1], depth)
					ax.plot(x, y, label=label)
				title = "Error norms for solid body rotation\n during one complete rotation of a {} [d{}].".format(distr_set[distr][1], depth)
				filename = "error_norms_sbr_{}_d{}".format(distr, depth)
# 				ax.set_xlim([0,1036800])
				self.savePlot(fig, ax, xlabel="Timestamp [$s$]", ylabel="Error norm", title=title, 
							ylim=[0,1], filename=filename, pdfpages=pdfpages, adjust_xticks=False, xlim=[0,1036800])
	
	
	def getDataByDepth(self, ydata, cfl, depth, projection, distr, depths):
		if not depth in depths:
			return []
		for data in ydata:
			if data["projection"] == projection and data["cfl"] == cfl and data["distr"] == distr:
				return data["ydata"][depths.index(depth)] if len(data["ydata"]) > depths.index(depth) else []
	
	
	def plotErrors(self, benchmark, proj, ydata, depths, plotlist, pdfpages, pdfpageslog):
		cflset = self.cflSet(benchmark)
		for pset in plotlist:
			done = False
			singleplot = pset["index"] in [3,4]
			ydatacpy = ydata[:]
			fig = pyplot.figure()
# 			print(fig.get_size_inches())
			ax = fig.add_subplot(1,1,1)
			csvfile = open(os.path.join(basepath, "{}_{}.csv".format(benchmark,pset["filename"])), "w")
			csvfile.write(";".join(["distribution", "cfl", "projection", "depth", "value"])+"\n")
			for distr in distr_set:
				for cfl in cflset:
					for p in proj:
						for data in ydatacpy:
							if data["projection"] == p and data["cfl"] == cfl and data["distr"] == distr and not done:
								y = [d[pset["index"]] for d in data["ydata"]]
								if pset["index"] == 4: y = y[:9]
								label = "{distr}, cfl {cfl}, {proj}".format(distr=distr, cfl=cfl, proj=p)
								linestyle = "-"
								xdata = depths[:len(y)]
								ax.plot(xdata, y, label=label, marker=distr_set[distr][0], linestyle=linestyle, linewidth=1.0)
								ydatacpy.remove(data)
								done = singleplot
								for i in range(len(y)):
									line = ";".join([distr, str(cfl), p, str(xdata[i]), str(y[i])])
									csvfile.write(line+"\n")
			logscale = pset["index"] > 2
			self.savePlot(fig, ax, xlabel=pset["xlabel"], ylabel=pset["ylabel"], title=pset["title"].format(benchmark=benchmark.replace("_", " ")), 
						singleplot=singleplot, logscale=logscale, ylim=[0.0,1.0], filename="{}_{}".format(benchmark,pset["filename"]), 
						pdfpages=pdfpages, pdfpageslog=pdfpageslog)
			csvfile.close()
			
		
	def plotErrorDiffs(self, benchmark, proj, ydata, depths, plotlist, pdfpages):
		cflset = self.cflSet(benchmark)
		colorset = self.colorSet()
		for pset in plotlist:
			ydatacpy = ydata[:]
			csvdata = []
			csvavg = []
			for distr in distr_set:
				fig = pyplot.figure()
				ax = fig.add_subplot(1,1,1)
				plotsets = []
				for cfl in cflset:
					for p in proj:
						for data in ydatacpy:
							if data["projection"] == p and data["cfl"] == cfl and data["distr"] == distr:
								y = [d[pset["index"]] for d in data["ydata"]]
								y = [abs(y[i]/y[i-2]-1) if i > 1 else 0 for i in range(len(y))]
								y = y[2:]
								label = "{distr}, cfl {cfl}, {proj}".format(distr=distr, cfl=cfl, proj=p)
								xdata = depths[2:len(y)+2]
								plotsets.append((y, label))
								ydatacpy.remove(data)
								for i in range(len(y)):
									csvdata.append([distr, cfl, p, xdata[i], y[i]])
								yavg = sum(y) / len(y)
								csvavg.append([distr, cfl, p, "avg", yavg])
# 								ax.plot(xdata, y, label=label, marker=distr_set[distr], linestyle=linestyle, linewidth=1.0)
				linestyle = "-"
				dwidth = 0.8
				bwidth = dwidth / len(plotsets)
				x = np.arange(len(depths))
				x = x[:len(y)]
				fig, ax = pyplot.subplots()
				ctr = 0
				for y, label in plotsets:
					ax.bar(x + ctr*bwidth, y[:], bwidth, label=label, color=colorset[ctr])
					ctr += 1
				ax.set_xticks(x+dwidth/2)
				ax.set_xticklabels(xdata)
				ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
				ax.set_ylabel(pset["ylabel"] + ": Relative change $\\frac{|E_{d}-E_{d-2}|}{|E_{d-2}|}$")
				ax.set_xlabel(pset["xlabel"])
				ax.legend(prop={'size':6})
				if singlepage: self.savetofile(pset["filename"], filefmt, prefix="{}_diff_{}".format(benchmark,distr))
				ax.set_title(pset["title"].format(benchmark=benchmark.replace("_", " "))[:-1] 
							+ " with {}: \n".format(distr_set[distr][1]) 
							+ "Relative reduction of error E: $\\frac{|E_{d}-E_{d-1}|}{|E_{d-1}|}$")
				if multipage: self.addtomultipage(pdfpages)
				pyplot.clf()
			csvfile = open(os.path.join(basepath, "{}_{}_diff.csv".format(benchmark,pset["filename"])), "w")
			csvfile.write(";".join(["distribution", "cfl", "projection", "depth", "value"])+"\n")
			for line in csvdata + csvavg:
				csvfile.write(";".join([str(elem) for elem in line])+"\n")
			csvfile.close()
	
	def plotAvgAreas(self, xdata, ydata, proj):
		for p in proj:
			fig = pyplot.figure()
			ax = fig.add_subplot(1,1,1)
			linestyle = "-"
			x = xdata[p]
			label = "Sphere"
			y = [d[4] for d in ydata[p]]
			ax.plot(x, y, label=label, linestyle=linestyle, color="red", linewidth=1.0)
			label = "Polyhedron"
			y = [d[5] for d in ydata[p]]
			ax.plot(x, y, label=label, linestyle=linestyle, color="blue", linewidth=1.0)
			ax.set_ylabel("Average cell area")
			ax.set_xlabel("Refinement depth")
			ax.set_yscale('log')
			ax.legend(prop={'size':6})
			self.savetofile("cell_areas", filefmt, prefix="")
			pyplot.clf()
			
	
	def colorSet(self):
		result = []
		for i in [255, 128, 192, 160, 224, 96]: #[255, 128, 64, 192, 32, 96, 160, 224]
			for t in [(i, 0, 0), (0, i, 0), (0, 0, i), (i, 0, i), (i, i, 0), (0, i, i)]:
				newcol = "#" + hex(t[0])[2:].zfill(2) + hex(t[1])[2:].zfill(2) + hex(t[2])[2:].zfill(2)
				result.append(newcol)
		return result
		
	def action(self):
		pdfpages = pdfpagesdiff = pdfpageslog = pdfpagesadapt = None
		plotlist = [
				{"index": 0, "ylabel": "Global error $l_1$", "xlabel": "Refinement depth", 
					"filename": "error_l1", "title": "Error norm $l_1$ for {benchmark}."},
				{"index": 1, "ylabel": "Global error $l_2$", "xlabel": "Refinement depth", 
					"filename": "error_l2", "title": "Error norm $l_2$ for {benchmark}."},
				{"index": 2, "ylabel": "Global error $l_\infty$", "xlabel": "Refinement depth", 
					"filename": "error_linf", "title": "Error norm $l_\infty$ for {benchmark}."},
# 				{"index": 3, "ylabel": "Relative error in edge lengths", "xlabel": "Refinement depth", "filename": "error_edges", "title": ""},
# 				{"index": 4, "ylabel": "Relative error in triangle areas", "xlabel": "Refinement depth", "filename": "error_areas", "title": ""}
				]
		proj = {"equiangular": "_equiangular"}#, "equidistant": ""}
		depths = range(mindepth, maxdepth + 1)
		benchmarks = ["deformational_flow", "solid_body_rotation"]
		if self.sbrtimesteps:
			ydata = self.getDataPerTS(depths)
			if multipage: pdfpages = PdfPages(os.path.join(basepath, 'error_norms_sbr.pdf'))
			self.plotSbrErrorsPerTS(depths, ydata, pdfpages)
			if multipage: pdfpages.close()
			return
		if multipage:
			if not self.adapt:
				pdfpages = PdfPages(os.path.join(basepath, 'error_norms.pdf'))
				pdfpageslog = PdfPages(os.path.join(basepath, 'error_norms_logscale.pdf'))
				pdfpagesdiff = PdfPages(os.path.join(basepath, 'error_norms_diffs.pdf'))
			pdfpagesadapt = PdfPages(os.path.join(basepath, 'error_norms_adaptivity.pdf'))
		for benchmark in benchmarks:
			ydata, yadapt = self.getData(depths, proj, benchmark)
			if not ydata:
				continue
			self.plotErrorsAdaptivity(benchmark, proj, ydata, yadapt, depths, plotlist, pdfpagesadapt)
			if not self.adapt:
				self.plotErrors(benchmark, proj, ydata, depths, plotlist, pdfpages, pdfpageslog)
				self.plotErrorDiffs(benchmark, proj, ydata, depths, plotlist, pdfpagesdiff)
		if multipage:
			if not self.adapt:
				pdfpages.close()
				pdfpageslog.close()
				pdfpagesdiff.close()
			pdfpagesadapt.close()
	

if __name__ == "__main__":
	if len(sys.argv) > 1:
		folder = sys.argv[1]
	basepath = os.path.join(basepath, folder)
	if not os.path.exists(basepath):
		os.makedirs(basepath)
	mode = sys.argv[2] if len(sys.argv) > 2 else ""
	plot = Plot(mode)
	plot.action()
