#! /usr/bin/python

import sys
import time
import commands
import os
import math
import re

if len(sys.argv) < 2:
	print("Use "+sys.argv[0]+" [output dir]")
	sys.exit(-1)

output_dir=sys.argv[1]

mode = "" if len(sys.argv) < 3 else sys.argv[2]

# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''




#
# Sierpinski parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set_sbr = [
# 		'maccluster_solid_body_rotation',
		'maccluster_solid_body_rotation_equiangular'
	]
	sierpi_bin_set_df = [
# 		'maccluster_deformational_flow',
		'maccluster_deformational_flow_equiangular'
    ]
	if mode == "sbr":
		sierpi_bin_set = sierpi_bin_set_sbr
	elif mode == "df":
		sierpi_bin_set = sierpi_bin_set_df
	else:
		sierpi_bin_set = sierpi_bin_set_sbr + sierpi_bin_set_df

	min_depth = 10
	max_depth = 20
	# initialization depth
	sierpi_sim_depth_set = range(min_depth, max_depth+1)

	# ADAPTIVE DEPTH SET
	sierpi_sim_adaptive_depth_set=[0]

	# maximum refinement depth
	sierpi_sim_max_depth = 28

	# THREADS SET
	# Use threads since cluster is run in hyperthreading mode!
	#
	sierpi_sim_threads_set=[1]

	sierpi_refinement_params=[[50000,10000]]

	# MPI NODE SET
	sierpi_sim_mpi_nodes_set=[512]

	sierpi_max_cores = 1024

	if mode == "sbr":
		cfl_set = [0.5]
	else:
		cfl_set = [0.5]
	
	distr_set = {42: "cosine", 43: "cylinder"}


def create_xml(id, bin, distr):
	xmlbin = bin.replace("_equiangular", "")
	filename = "{bin}_{id}.xml".format(bin=bin, id=id)
	output_interval = 1036800 if xmlbin == 'maccluster_solid_body_rotation' else 5.0
	radius = 0.5 if distr != 66 else 1.0
	with open('{bin}_tmpl.xml'.format(bin=xmlbin), "r") as tmpl:
		with open(os.path.join(jobfile_directory, filename), "w") as xml:
			text = tmpl.read()
			xml.write(text.format(dataset=distr, error_norms="1", vtp=xmlbin, output_interval=output_interval, 
								vtk_type=0, cradius=radius, df_case=1))
	return filename


# bin set
for bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			for refinement_params in sierpi_refinement_params:

				# do not create duplicate benchmarks for different refinement params in case that there's no adaptivity
				if adaptive_depth == 0:
					refinement_params = sierpi_refinement_params[0]

				if depth+adaptive_depth > sierpi_sim_max_depth:
					continue
				
				for cfl in cfl_set:
					
					for distr in distr_set:

						if bin.find('nothreading') > 0:
							sierpi_sim_threads_set_ = [1]
						else:
							sierpi_sim_threads_set_ = sierpi_sim_threads_set
							
						num_threads = sierpi_sim_threads_set_[0]
						num_mpi_nodes = sierpi_sim_mpi_nodes_set[0]
		
						estimated_runtime_string="24:00:00"
		
						id = 'd'+str(depth).zfill(2)
						id += '_a'+str(adaptive_depth).zfill(2)
						id += '_cfl'+str(cfl)
						id += '_'+distr_set[distr]
						id += '_rk1_refine'+str(refinement_params[0])+'_coarsen'+str(refinement_params[1])
		
						sierpi_run=bin
						sierpi_run+=' -c {xml}'.format(xml=create_xml(id, bin, distr))
# 						sierpi_run+=' -A 1 '
						sierpi_run+=' -r '+str(refinement_params[0])+'/'+str(refinement_params[1])
						sierpi_run+=' -I 12 '	# initial split
# 						sierpi_run+=' -u 1 -U 20'
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -d '+str(depth)
						sierpi_run+=' -C '+str(cfl)
		
						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params
		
# 						job_description_string = bin+'_'+id
						job_description_string = id+'_'+bin
		
						job_filepath = jobfile_directory+'/'+job_description_string+'.cmd'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'
						error_output_filepath = jobfile_directory+'/'+job_description_string+'.err'
		
						job_file_content="""#! /bin/bash

# output
#SBATCH -o """+output_filepath+"""
#SBATCH -e """+error_output_filepath+"""
# working directory
#SBATCH -D """+working_directory+"""
# job description
#SBATCH -J """+id+"""
#SBATCH --get-user-env
#SBATCH --partition=bdz
#SBATCH --ntasks="""+str(num_mpi_nodes)+"""
#SBATCH --cpus-per-task="""+str(num_threads)+"""
#SBATCH --mail-type=end
#SBATCH --mail-user=kleinfl@in.tum.de
#SBATCH --export=NONE
#SBATCH --time="""+estimated_runtime_string+"""

cd """+working_directory+"""

source /etc/profile.d/modules.sh

source ./inc_vars.sh

export KMP_AFFINITY="compact"

cd """+output_dir+"""

mpiexec.hydra -genv OMP_NUM_THREADS """+str(num_threads)+""" -envall -ppn """+str(64/num_threads)+""" -n """+str(num_mpi_nodes)+""" ../../../build/"""+sierpi_run+"""
"""

						print("Writing jobfile '"+job_filepath+"'")
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()
