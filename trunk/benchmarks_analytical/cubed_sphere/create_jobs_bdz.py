#! /usr/bin/python

import sys
import time
import commands
import os
import math
import re

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''

#dart_stations = '834498.794062/528004.720595/21401.txt,935356.566012/-817289.628677/21413.txt,545735.266126/62716.4740303/21418.txt,1058466.21575/765077.767857/21419.txt'
dart_stations = "a45.000000/a-180.000000/lat45.0lon-180.0.csv,a45.000000/a-157.500000/lat45.0lon-157.5.csv,a45.000000/a-135.000000/lat45.0lon-135.0.csv,a45.000000/a-112.500000/lat45.0lon-112.5.csv,a45.000000/a-90.000000/lat45.0lon-90.0.csv,a45.000000/a-67.500000/lat45.0lon-67.5.csv,a45.000000/a-45.000000/lat45.0lon-45.0.csv,a45.000000/a-22.500000/lat45.0lon-22.5.csv,a45.000000/a0.000000/lat45.0lon0.0.csv,a45.000000/a22.500000/lat45.0lon22.5.csv,a45.000000/a45.000000/lat45.0lon45.0.csv,a45.000000/a67.500000/lat45.0lon67.5.csv,a45.000000/a90.000000/lat45.0lon90.0.csv,a45.000000/a112.500000/lat45.0lon112.5.csv,a45.000000/a135.000000/lat45.0lon135.0.csv,a45.000000/a157.500000/lat45.0lon157.5.csv"
dart_stations += ",a0.000000/a-180.000000/lat0.0lon-180.0.csv,a0.000000/a-157.500000/lat0.0lon-157.5.csv,a0.000000/a-135.000000/lat0.0lon-135.0.csv,a0.000000/a-112.500000/lat0.0lon-112.5.csv,a0.000000/a-90.000000/lat0.0lon-90.0.csv,a0.000000/a-67.500000/lat0.0lon-67.5.csv,a0.000000/a-45.000000/lat0.0lon-45.0.csv,a0.000000/a-22.500000/lat0.0lon-22.5.csv,a0.000000/a0.000000/lat0.0lon0.0.csv,a0.000000/a22.500000/lat0.0lon22.5.csv,a0.000000/a45.000000/lat0.0lon45.0.csv,a0.000000/a67.500000/lat0.0lon67.5.csv,a0.000000/a90.000000/lat0.0lon90.0.csv,a0.000000/a112.500000/lat0.0lon112.5.csv,a0.000000/a135.000000/lat0.0lon135.0.csv,a0.000000/a157.500000/lat0.0lon157.5.csv"
dart_stations = [elem.split("/") for elem in dart_stations.split(",")]

p = re.compile('([^/]*).txt')


#
# Sierpinski parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set=[
		'sierpi_intel_mpi_release_bdz_equiangular',
		'sierpi_intel_mpi_release_bdz_equidistant'
	]

	# initialization depth
#	sierpi_sim_depth_set=[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
	sierpi_sim_depth_set = range(10, 27)

	# ADAPTIVE DEPTH SET
	sierpi_sim_adaptive_depth_set=[0]

	# maximum refinement depth
	sierpi_sim_max_depth = 28

	# THREADS SET
	# Use threads since cluster is runned in hyperthreading mode!
	#
	sierpi_sim_threads_set=[64]

	sierpi_refinement_params=[[50,10], [500,100], [5000,1000], [50000,10000]]

	# MPI NODE SET
	sierpi_sim_mpi_nodes_set=[8]

	sierpi_max_cores = 1024

	cfl=0.5


# bin set
for bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			for refinement_params in sierpi_refinement_params:

				# do not create duplicate benchmarks for different refinement params in case that there's no adaptivity
				if adaptive_depth == 0:
					refinement_params = sierpi_refinement_params[0]

				if depth+adaptive_depth > sierpi_sim_max_depth:
					continue

				if bin.find('nothreading') > 0:
					sierpi_sim_threads_set_ = [1]
				else:
					sierpi_sim_threads_set_ = sierpi_sim_threads_set

				# threads set
				for num_threads in sierpi_sim_threads_set_:

					# threads set
					for num_mpi_nodes in sierpi_sim_mpi_nodes_set:

#						if sierpi_max_cores < num_mpi_nodes*num_threads:
#							continue

						# estimate number of cells
						max_cells=2**(depth+adaptive_depth)

						def get_estimated_runtime(max_cells, splitting_size):

							# estimate clusters
							clusters=max_cells/splitting_size
							clusters+=1

							# clusters per thread
							clusters_per_thread=clusters/num_threads
							clusters_per_thread+=1

							# cells per thread
							cells_per_thread=clusters_per_thread*splitting_size

							return cells_per_thread*0.0001

						estimated_runtime_string="24:00:00"

						initial_domain_splits=int(math.log(num_mpi_nodes, 2))

						sierpi_run=bin
						sierpi_run+=' -c ../intel_release_equidistant.xml'
						sierpi_run+=' -A 1 '
#						sierpi_run+=' -t 20000 '
						sierpi_run+=' -r '+str(refinement_params[0])+'/'+str(refinement_params[1])
#						sierpi_run+=' -v 1 '
						sierpi_run+=' -I 12 '	# initial split
						sierpi_run+=' -u 1 -U 20'
#						sierpi_run+=' -b 10'
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -d '+str(depth)
						sierpi_run+=' -C '+str(cfl)

						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params

						id = 'd'+str(depth).zfill(2)
						id += '_a'+str(adaptive_depth).zfill(2)
						id += '_cfl'+str(cfl)
						id += '_rk1_refine'+str(refinement_params[0])+'_coarsen'+str(refinement_params[1])

						id += '_t'+str(num_threads).zfill(3)
						id += '_n'+str(num_mpi_nodes).zfill(3)

						job_description_string = bin+'_'+id

#						sierpi_run += ' -D '+p.sub('output_'+id+'_dart\\1.txt', dart_stations)
						sierpi_run += ' -D ' + ",".join(["{lat}/{lon}/output_{id}_dart_{dart}".format(lat=elem[0], lon=elem[1], id=job_description_string, dart=elem[2]) for elem in dart_stations])

						job_filepath = jobfile_directory+'/'+job_description_string+'.cmd'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'
						error_output_filepath = jobfile_directory+'/'+job_description_string+'.err'

						# assume 63 available GB
						mem_per_cpu=(63*1024)/num_threads+1

						job_file_content="""#! /bin/bash

# output
#SBATCH -o """+output_filepath+"""
#SBATCH -e """+error_output_filepath+"""
# working directory
#SBATCH -D """+working_directory+"""
# job description
#SBATCH -J """+job_description_string+"""
#SBATCH --get-user-env
#SBATCH --partition=bdz
#SBATCH --ntasks="""+str(num_mpi_nodes)+"""
#SBATCH --cpus-per-task="""+str(num_threads)+"""
#SBATCH --mail-type=end
#SBATCH --mail-user=kleinfl@in.tum.de
#SBATCH --export=NONE
#SBATCH --time="""+estimated_runtime_string+"""

cd """+working_directory+"""

source /etc/profile.d/modules.sh

source ./inc_vars.sh

export KMP_AFFINITY="compact"

cd """+output_dir+"""

mpiexec.hydra -genv OMP_NUM_THREADS """+str(num_threads)+""" -envall -ppn """+str(64/num_threads)+""" -n """+str(num_mpi_nodes)+""" ../../../build/"""+sierpi_run+"""
"""

						print "Writing jobfile '"+job_filepath+"'"
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()
