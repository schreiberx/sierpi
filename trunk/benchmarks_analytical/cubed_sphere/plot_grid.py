#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 11.03.2013

@author: Flo
'''

import matplotlib as mpl
import matplotlib.pyplot as pyplot
import matplotlib.dates as mdates
import matplotlib.image as mpimg
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import os, re, sys
import subprocess as sp

# import matplotlib as mpl
# mpl.rcParams['text.usetex']=True
# mpl.rcParams['text.latex.unicode']=True

pyplot.rc('text', usetex=True)
pyplot.rc('font',**{'family':'serif','serif':['Computer Modern']})
pyplot.rc("xtick", direction="out")
pyplot.rc("ytick", direction="out")
pyplot.rc('figure', figsize=(7,7))

folder = "vtp"

basepath = "/home/flo/workspace/sierpi_kleinfl/benchmarks_field/cubed_sphere/test"
basepath = "../cubed_sphere_results"

mindepth = 10
maxdepth = 20

xticks_stepsize = 1

filefmt = "pdf"

distr_set = {"cosine": ("o", "cosine bell"), "cylinder": ("v", "slotted cylinder")}
cfl_set = [0.5, 0.6, 0.75]

old = True
old = False
if old: distr_set = ["cosine"]

multipage = 1
singlepage = 1


class Plot():
	
	def __init__(self):
		self.figure = pyplot.figure("test")
		
	def savetofile(self, filename, extension, prefix=""):
		name = "{}_{}".format(prefix, filename) if prefix else filename
		pyplot.savefig(os.path.join(basepath, "{}.{}".format(name, extension)), bbox_inches="tight")
		
	def addtomultipage(self, pdfpages):
		pdfpages.savefig(bbox_inches="tight", pad_inches=0.7)		
	
	def colorSet(self):
		result = []
		for i in [255, 128, 192, 160, 224, 96]: #[255, 128, 64, 192, 32, 96, 160, 224]
			for t in [(i, 0, 0), (0, i, 0), (0, 0, i), (i, 0, i), (i, i, 0), (0, i, i)]:
				newcol = "#" + hex(t[0])[2:].zfill(2) + hex(t[1])[2:].zfill(2) + hex(t[2])[2:].zfill(2)
				result.append(newcol)
		return result
		
	def convertImage(self, filename, ax, title):
		filepath = os.path.join(basepath, filename)
		filepathtrim = filepath.replace(".png", "_trim.png")
		sp.call(["convert", "-trim", filepath, filepathtrim])
		img = mpimg.imread(filepathtrim)
		imgplot = ax.imshow(img)
		xmin, xmax = ax.get_xlim()
		ax.set_xticks([xmin, (xmax-xmin)*0.25, (xmax-xmin)*0.5, (xmax-xmin)*0.75, xmax])
		ax.set_xticklabels(["$-\\pi$", "$-\\frac{\\pi}{2}$", "0", "$\\frac{\\pi}{2}$", "$\\pi$"], fontsize=10)
		ymin, ymax = ax.get_ylim()
		ax.set_yticks([ymin, (ymin-ymax)/2, ymax])
		ax.set_yticklabels(["$-\\frac{\\pi}{2}$", "0", "$\\frac{\\pi}{2}$"], fontsize=10)
# 		ax.set_xlabel("Longitude $\\lambda$")
# 		ax.set_ylabel("Latitude $\\theta$")
		ax.set_title(title, fontsize=12)
		ax.grid(b=True, which='major', color='grey', linestyle=':')
		minor_locator = AutoMinorLocator(5)
		minor_locator = MultipleLocator((xmax-xmin)/4/5)
		ax.xaxis.set_minor_locator(minor_locator)
		minor_locator = MultipleLocator((ymin-ymax)/2/5)
		ax.yaxis.set_minor_locator(minor_locator)
		return imgplot
	
	
	def createSubplot(self, benchmark, distr, timestep, ax, letter):
		filename = '{}_{}_t{}.png'.format(benchmark, distr, timestep)
		if timestep == 0:
			title = "Initial distribution at $t=t_0$"
		elif timestep == 2:
			title = "Rotated" if benchmark == "sbr" else "Deformed"
			title += " distribution at $t=T_{max}/2$"
		elif timestep == 4:
			title = "Final distribution at $t=T_{max}$"
		return self.convertImage(filename, ax, "({}) {}".format(letter, title))
	
	
	def colormap(self, ax, benchmark):
		vmax = 1.0 if benchmark == "df" else 1000
		cmap = mpl.cm.jet
		norm = mpl.colors.Normalize(vmin=0.1, vmax=vmax)
		cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cmap, norm=norm, orientation='horizontal')
	
	
	def subplotPositions(self):
		width = .4
		height = .25
		left = .05
		right = .55
		top = .7
		mid = .4
		bot = .1
		positions = [
					[left, top, width, height], [right, top, width, height],
					[left, mid, width, height], [right, mid, width, height],
					[left, bot, width, height], [right, bot, width, height]
					]
		return positions
	
	
	def action(self):
		benchmarks = ["sbr", "df"]
		distr_set = ["cosine", "cylinder"]
		timesteps = [0, 2, 4]
		positions = self.subplotPositions()
		letters = ["a", "d", "b", "e", "c", "f"]
		for benchmark in benchmarks:
			fig = pyplot.figure(1)
			ctr = 0
			for ts in timesteps:
				for distr in distr_set:
					ax = pyplot.axes(positions[ctr])
					img = self.createSubplot(benchmark, distr, ts, ax, letters[ctr])
					ymin = -0.15 if ctr == 4 else -0.6
					ax.plot([1.12, 1.12], [ymin, 1.15], transform=ax.transAxes, clip_on=False, color="k")
# 					ax.vlines(1.3, 0.3, 3, clip_on=False, transform=ax.transAxes)
					ctr += 1
			pyplot.rc("xtick", direction="in")
			ax = pyplot.axes([0.2, 0.05, 0.6, 0.02])
			self.colormap(ax, benchmark)
			self.savetofile(benchmark, "pdf", folder)
			pyplot.clf()
	

if __name__ == "__main__":
	if len(sys.argv) > 1:
		folder = sys.argv[1]
	basepath = os.path.join(basepath, folder)
	if not os.path.exists(basepath):
		os.makedirs(basepath)
	plot = Plot()
	plot.action()
