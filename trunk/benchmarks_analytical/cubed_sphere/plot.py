#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 11.03.2013

@author: Flo
'''


import matplotlib.pyplot as pyplot
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import os, re, sys

# import matplotlib as mpl
# mpl.rcParams['text.usetex']=True
# mpl.rcParams['text.latex.unicode']=True

# pyplot.rc('text', usetex=True)
# pyplot.rc('font', family='serif')

pyplot.rc('text', usetex=True)
pyplot.rc('font',**{'family':'serif','serif':['Computer Modern']})
pyplot.rc('figure', figsize=(9,6))

folder = "test11"

basepath = "/home/flo/workspace/sierpi_kleinfl/benchmarks_field/cubed_sphere/test"
basepath = "../cubed_sphere_results"

mindepth = 10
maxdepth = 23

filefmt = "pdf"

singlepage = 1
multipage = 1

standalone = 1

fontsize = 18


def plotBars(ax, ydatalist, labellist=[], datawidth=0.8, colorset=None, xticklabels=[], ylim=[], 
			xlabel="", ylabel="", title="", legendsize=6, twinax=False, twinaxcut=0, twinylabel=""):
	dwidth = datawidth
	bwidth = dwidth / len(ydatalist)
	x = np.arange(len(ydatalist[0]))
	if twinax:
		ax2 = ax.twinx()
	for ctr in range(len(ydatalist)):
		color = colorset[ctr] if colorset else None
		label = labellist[ctr] if labellist else None
		if twinax:
			ax.bar(x[:twinaxcut] + ctr*bwidth, ydatalist[ctr][:twinaxcut], bwidth, label=label, color=color)
			ax2.bar(x[twinaxcut:] + ctr*bwidth, ydatalist[ctr][twinaxcut:], bwidth, label=label, color=color)
		else:
			ax.bar(x + ctr*bwidth, ydatalist[ctr], bwidth, label=label, color=color)
	ax.set_xticks(x+dwidth/2)
	if xticklabels: ax.set_xticklabels(xticklabels)
	ax.set_xlim(x[0]-0.5, x[-1]+0.5+dwidth)
	if ylim: ax.set_ylim(ylim)
	ax.set_xlabel(xlabel, fontsize=fontsize)
	ax.set_ylabel(ylabel, fontsize=fontsize)
	if twinax:
		ax2.set_ylabel(twinylabel, fontsize=fontsize)
		ax2.yaxis.set_tick_params(labelsize=fontsize)
	if legendsize: ax.legend(prop={'size':legendsize})
	ax.set_title(title)
	ax.xaxis.set_tick_params(labelsize=fontsize)
	ax.yaxis.set_tick_params(labelsize=fontsize)



class Plot():
	
	def __init__(self):
		self.figure = pyplot.figure("test")
		
	def savetofile(self, filename, extension, prefix=""):
		name = "{}_{}".format(prefix, filename) if prefix else filename
		pyplot.savefig(os.path.join(basepath, "{}.{}".format(name, extension)))
		
	def addtomultipage(self, pdfpages):
		pdfpages.savefig(bbox_inches="tight", pad_inches=0.7)
	
	def savePlot(self, fig, ax, xlabel="", ylabel="", title="", forcesinglepage=False, xticks_stepsize=1,
			singleplot=False, logscale=False, ylim=[], filename="", pdfpages=None, pdfpageslog=None,
			adjust_xticks=True, xlim=[]):
		fontsize = 18
		ax.set_ylabel(ylabel, fontsize=fontsize)
		ax.set_xlabel(xlabel, fontsize=fontsize)
		fig.set_size_inches(9, 6)
		fig.tight_layout()
		xstart, xend = ax.get_xlim()
		if adjust_xticks:
			ax.set_xlim(xstart-0.5, xend+0.5)
			ax.xaxis.set_ticks(np.arange(xstart, xend+xticks_stepsize, xticks_stepsize))
		ax.xaxis.set_tick_params(labelsize=fontsize)
		ax.yaxis.set_tick_params(labelsize=fontsize)
		if xlim:
			ax.set_xlim(xlim)
		if not singleplot:
			ax.legend(prop={'size':8})
		if logscale:
			ax.set_yscale('log')
		else:
			if ylim:
				ax.set_ylim(ylim)
		if singlepage: 
			self.savetofile(filename, filefmt)
		if multipage:
			ax.set_title(title, fontsize=fontsize) 
			if pdfpages: self.addtomultipage(pdfpages)
			if pdfpageslog:
				if ylim: 
					ax.set_ylim([0.01,1.1])
				ax.set_yscale('log')
				self.addtomultipage(pdfpageslog)
		pyplot.clf()
	
	def getdata(self, filename):
		with open(os.path.join(basepath, filename)) as f:
			data = f.readlines()[1:]
			data = [elem.split(",") for elem in data]
			xdata = [float(elem[0]) for elem in data]
			ydata = [float(elem[1]) for elem in data]
			return xdata, ydata
		
	def plotTS(self, depths, proj):
		xdata = {}
		ydata = {}
		filename = "sierpi_intel_mpi_release_bdz_{proj}_d{depth}_a00_cfl0.25_rk1_refine50_coarsen10_t064_n008.txt"
		for p in proj:
			xdata[p] = []
			ydata[p] = []
			for depth in depths:
				strdepth = "d{}".format(depth)
				ts = 0
				for filename in [elem for elem in os.listdir(basepath) if elem.startswith("sierpi") and p in elem and strdepth in elem]:
					with open(os.path.join(basepath, filename)) as f:
						text = f.readlines()
						for line in text:
							match = re.match("(.*) TS \(Timesteps\)", line.strip())
							if match:
								ts = match.groups()[0]
				ts = int(ts)
				xdata[p].append(depth)
				ydata[p].append(ts)
		fig, ax = pyplot.subplots()
		plotBars(ax, [ydata["equiangular"], ydata["equidistant"]], labellist=["equiangular", "equidistant"], 
				datawidth=0.5, xticklabels=depths, ylabel="Simulation Timesteps", xlabel="Refinement Depth",
				colorset=["r", "b"])
		self.savetofile("ts", filefmt, prefix=folder)
		pyplot.clf()
		
		ydata_diff = []
		for ctr in range(len(depths)):
			diff = float(ydata["equiangular"][ctr]) / ydata["equidistant"][ctr] - 1
			diff *= -100
			ydata_diff.append(diff)
		fig, ax = pyplot.subplots()
		plotBars(ax, [ydata_diff], xticklabels=depths, ylabel="Timesteps saved by EA relative to ED timesteps [\%]", xlabel="Refinement Depth",
				colorset=["g"])
		self.savetofile("ts_diff", filefmt, prefix=folder)
		pyplot.clf()
		avg = float(sum(ydata_diff)) / len(ydata_diff)
		print(ydata_diff)
		print(avg)
	
	def colorSet(self):
		result = []
#		for i in range(4, num + 4):
#			cdec = 256 * 256 * 256 / (num + 8) * i
#			chex = "#" + hex(cdec)[2:].zfill(6)
#			result.append(chex)
		for i in [255, 128, 192, 160, 224, 96]: #[255, 128, 64, 192, 32, 96, 160, 224]
			for t in [(i, 0, 0), (0, i, 0), (i, 0, 0), (0, i, i), (i, 0, i), (i, i, 0)]:
				newcol = "#" + hex(t[0])[2:].zfill(2) + hex(t[1])[2:].zfill(2) + hex(t[2])[2:].zfill(2)
				result.append(newcol)
		return result
		
		
	def variancePlot(self, fulldatalist, depth, pdfpages):
		# [ ( [(x,y), ...], label, lon, lat ), ... ]
		data_0 = [elem for elem in fulldatalist if elem[3] == 0]
		data_E = [[data_0[i][0][j][1] for i in range(len(data_0))] for j in range(len(data_0[0][0]))]
# 		data_E = [[] for i in range(len(data_0[0][0]))]
# 		for i in range(len(data_0)):
# 			data = data_0[i][0]
# 			for j in range(len(data)):
# 				data_E[j].append(data[j][1])
# 		print(data_E)
		E = [sum(elem)/len(elem) for elem in data_E]
		data_V = [[(elem - E[i])**2 for elem in data_E[i]] for i in range(len(E))]
		V = [sum(elem)/len(elem) for elem in data_V]
		data_x = [data_0[0][0][i][0] for i in range(len(data_0[0][0]))]
		fig, ax = pyplot.subplots()
		ax.plot(data_x, V)
		ax.set_xlabel("Simulation time")
		ax.set_ylabel("Variance")
		ax.set_title("Depth {}".format(depth))
		ax.set_ylim([0,0.0002])
		if multipage: pdfpages.savefig()
# 		if singlepage: self.savetofile(depth, "pdf", "variance")
		pyplot.clf()
		
		
	def action(self):
		proj = {"equiangular": "ea", "equidistant": "ed"}
		depths = range(mindepth, maxdepth + 1)
		self.plotTS(depths, proj)
		colorset = self.colorSet()
		for p in proj:
			pp = PdfPages(os.path.join(basepath, '{}_{}.pdf'.format(p, folder)))
			ppVariance = PdfPages(os.path.join(basepath, "{}_variance.pdf".format(p)))
			maxdatalist45 = []
			maxdatalist0 = []
			maxsurfacelist45 = []
			maxsurfacelist0 = []
			for depth in depths:
				dint = depth
				depth = "d{}".format(depth)
				print((p, depth))
	#			self.plot(depth)
				xdata = None
				ydatalist = []
				fulldatalist = []
				cctr = 0
				filelist = [elem for elem in os.listdir(basepath) if "output" in elem and depth in elem and p in elem]
				filelist.sort()
				colorset = self.colorSet()
				fig, ax = pyplot.subplots()
				for filename in filelist:
					xdata, ydata = self.getdata(filename)
					lat, lon = re.match(".*dart_lat(.*)lon(.*)\.csv", filename).groups()
					label = filename.split("dart_")[1].replace(".csv", "")
					linestyle = "-"
					dashes = []
					color = colorset[cctr]
					if float(lat) == 0:
						linestyle = "--"
						dashes = (2, 1.2)
					ax.plot(xdata, ydata, label=label, linestyle=linestyle, color=color, linewidth=0.5, dashes=dashes)
					ydatalist.append((ydata, label))
					fulldatalist.append(([[xdata[ctr], ydata[ctr]] for ctr in range(len(xdata))], label, float(lon), float(lat)))
					cctr += 1
				title = "Surface elevation for darts at 0$\,^{\\circ}$N/45$\,^{\\circ}$N "+"[{}]".format(depth)
				self.savePlot(fig, ax, ylabel="Surface elevation", xlabel="Simulation time", title=title, 
							adjust_xticks=False, ylim=[-0.6, 0.8], filename="rdb_{}_{}".format(p, depth), pdfpages=pp)
				pyplot.clf()
				
#				baseline = ydatalist[0][0]
#				for ydata in ydatalist[1:]:
#					err = [ydata[0][ctr] - baseline[ctr] for ctr in range(len(baseline))]
#					pyplot.plot(xdata, err, label=ydata[1])
#				pyplot.title(u"Surface elevation differences for darts at 20°N")
#				pyplot.ylabel("Surface elevation")
#				pyplot.xlabel("Simulation time")
#				pyplot.legend(prop={'size':6})
#				self.savetofile("errors", "svg", prefix=p+depth)
#				pyplot.clf()
			
				self.variancePlot(fulldatalist, depth, ppVariance)
				
# 				if (dint < 13):
# 					continue
				
				def addMaxdata(datalist, latitude, datatype):
					maxdata = [[elem[2], sorted(elem[0], key=lambda x: x[1])[-1][datatype]] for elem in fulldatalist if elem[3] == latitude]
					maxdata = sorted(maxdata, key=lambda x: x[0])
					datalist.append((maxdata, depth))
				
				addMaxdata(maxdatalist45, 45, 0)
				addMaxdata(maxdatalist0, 0, 0)
				addMaxdata(maxsurfacelist45, 45, 1)
				addMaxdata(maxsurfacelist0, 0, 1)
			pp.close()
			ppVariance.close()
			
			cctr = 0
			for maxdata, label in maxdatalist0:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			if standalone: pyplot.title("Time of maximum elevation for darts at the equator")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Simulation time")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([43500, 44400])
			self.savetofile("0max", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
				
			cctr = 0
			for maxdata, label in maxdatalist45:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			if standalone: pyplot.title("Time of maximum elevation for darts at 45$\,^{\\circ}$N")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Simulation time")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([20800, 21600])
			self.savetofile("45max", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
				
			cctr = 0
			for maxdata, label in maxsurfacelist45:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			if standalone: pyplot.title("Maximum surface elevation for darts at 45$\,^{\\circ}$N")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Surface elevation")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([0.0, 0.8])
			self.savetofile("45maxsurface", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
				
			cctr = 0
			for maxdata, label in maxsurfacelist0:
				pyplot.plot([elem[0] for elem in maxdata], [elem[1] for elem in maxdata], label=label, color=colorset[cctr])
				cctr += 1
			if standalone: pyplot.title("Maximum surface elevation for darts at the equator")
			pyplot.xlabel("Longitude")
			pyplot.ylabel("Surface elevation")
			pyplot.legend(prop={'size':6})
			pyplot.ylim([0.0, 0.8])
			self.savetofile("0maxsurface", filefmt, prefix="{}_{}".format(p, folder))
			pyplot.clf()
			
			def velocities(datalist, longitude, diff=False):
				angle = np.pi/4.0 if longitude == 45 else np.pi/2.0
				radius_earth = 6378137
				radius_dam = 500000
				if diff: 
					longitude = "45to0"
					radius_dam = 0
				xdata = [int(elem[1][1:]) for elem in datalist]
				maxdata = [sum([elem[1] for elem in data[0]])/len(data[0]) for data in datalist]
				fig, ax = pyplot.subplots()
				ax.plot(xdata, maxdata)
				self.savePlot(fig, ax, xlabel="Refinement depth", ylabel="Average time of maximum elevation", 
							filename="{}_{}_{}_avgmaxtime".format(p, folder, longitude), forcesinglepage=True)
# 				pyplot.legend(prop={'size':6})
# 				self.savetofile("avgmaxtime", filefmt, prefix="{}_{}_{}".format(p, folder, longitude))
				pyplot.clf()
				maxdata = [(angle * radius_earth - radius_dam) / elem for elem in maxdata]
				print(xdata, maxdata)
				fig, ax = pyplot.subplots()
				ax.plot(xdata, maxdata)
				self.savePlot(fig, ax, xlabel="Refinement depth", ylabel="Average velocity", 
							filename="{}_{}_{}_velocities".format(p, folder, longitude), forcesinglepage=True)
# 				pyplot.plot(xdata, maxdata)
# 				pyplot.legend(prop={'size':6})
# 				self.savetofile("velocities", filefmt, prefix="{}_{}_{}".format(p, folder, longitude))
				pyplot.clf()
				
			maxdatalist45to0 = [([[maxdatalist0[i][0][j][0],maxdatalist0[i][0][j][1]-maxdatalist45[i][0][j][1]] for j in range(len(maxdatalist0[i][0]))], 
								maxdatalist0[i][1]) for i in range(len(maxdatalist0))]
# 			x=([[-180.0, 43621.5], [-157.5, 43700.0], [-135.0, 43621.5], [-112.5, 43700.0], [-90.0, 43621.5], [-67.5, 43700.0], 
# 			[-45.0, 43621.5], [-22.5, 43700.0], [0.0, 43621.5], [22.5, 43700.0], [45.0, 43621.5], [67.5, 43700.0], [90.0, 43621.5], 
# 			[112.5, 43700.0], [135.0, 43621.5], [157.5, 43700.0]], 'd13')
				
			velocities(maxdatalist0, 0)
			velocities(maxdatalist45, 45)
			velocities(maxdatalist45to0, 45, diff=True)
	

if __name__ == "__main__":
	if len(sys.argv) > 1:
		folder = sys.argv[1]
	basepath = os.path.join(basepath, folder)
	if not os.path.exists(basepath):
		os.makedirs(basepath)
	plot = Plot()
	plot.action()
