#! /bin/bash

#. $HOME/bin/local_vars.sh
#. $HOME/bin/intel_vars.sh
#. $HOME/bin/scons_vars.sh

. inc_vars.sh

COMPILE_DIR=`pwd`

cd ../..
#make clean

#scons --xml-config=$COMPILE_DIR/maccluster_solid_body_rotation.xml --threading=omp --enable-mpi=on -j 4
#scons --xml-config=$COMPILE_DIR/maccluster_deformational_flow.xml --threading=omp --enable-mpi=on -j 4
scons --xml-config=$COMPILE_DIR/maccluster_solid_body_rotation_equiangular.xml --threading=off --enable-mpi=on -j 4
scons --xml-config=$COMPILE_DIR/maccluster_deformational_flow_equiangular.xml --threading=off --enable-mpi=on -j 4

