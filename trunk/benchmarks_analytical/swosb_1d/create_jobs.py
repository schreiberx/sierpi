#! /usr/bin/python

import sys
import time
import commands
import os
import stat

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''


#
# Sierpi parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set=[
		'sierpi_intel_omp_tsunami_1d_netcdf_type0_degree0_am2_debug'
	]

	# DEPTH SET
	sierpi_sim_depth_set=range(8, 13)

	# ADAPTIVE DEPTH SET
	sierpi_sim_adaptive_depth_set=[0]

	# SPLITTING SIZE SET
	sierpi_sim_splitting_size_set=[1024]

	# THREADS SET
	sierpi_sim_threads_set=[4]

	sierpi_sim_fixedtime=-1
	sierpi_sim_fixedtimesteps=100
	sierpi_sim_initial_split=0

	# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# there's no adaptivity for 1D !!!
	# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	sierpi_adaptivity_parameters_set=[
		"0/0",
		"0.0000000001/0.000000000001",
		"0.00000001/0.0000000001",
		"0.000001/0.00000001",
		"0.0001/0.000001",
		"0.01/0.0001"
	]

	sim_clusters='uv3'

	sierpi_extra_params='-A 1'

	estimated_runtime_scalar=1.0




# bin set
for bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			if bin.find('scan_data') > 0:
				sierpi_sim_splitting_size_set_ = [0]
			else:
				sierpi_sim_splitting_size_set_ = sierpi_sim_splitting_size_set

			# splitting sizes
			for splitting_size in sierpi_sim_splitting_size_set_:

				# threads set
				for num_threads in sierpi_sim_threads_set:

					# threads set
					for adaptivity_parameters in sierpi_adaptivity_parameters_set:

						# estimate number of cells
						max_cells=2**(depth+adaptive_depth)

						def get_estimated_runtime(max_cells, splitting_size):

							# estimate clusters
							clusters=max_cells/splitting_size
							clusters+=1

							# clusters per thread
							clusters_per_thread=clusters/num_threads
							clusters_per_thread+=1

							# cells per thread
							cells_per_thread=clusters_per_thread*splitting_size

							return cells_per_thread*0.0001

						if splitting_size == 0:
							estimated_runtime=get_estimated_runtime(max_cells, 16*1024)
						else:
							estimated_runtime=get_estimated_runtime(max_cells, splitting_size)

						estimated_runtime*=estimated_runtime_scalar

						estimated_runtime_string=time.strftime("%H:%M:%S", time.gmtime(estimated_runtime))


						sierpi_run=bin
						sierpi_run+=' -c ../../scenarios/single_wave_on_simple_beach_1d_bouy_sampling.xml'
						sierpi_run+=' -d '+str(depth)
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -r '+adaptivity_parameters
						sierpi_run+=' -n '+str(num_threads)
						sierpi_run+=' -o '+str(splitting_size)

						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params

						rbin = bin
						rbin = rbin.replace('intel_', '')
						rbin = rbin.replace('release_', '')
						rbin = rbin.replace('debug_', '')
						rbin = rbin.replace('omp_', '')
						rbin = rbin.replace('tsunami_1d_', '')
						rbin = rbin.replace('netcdf_', '')
						rbin = rbin.replace('type0_', '')
						rbin = rbin.replace('degree0_', '')

						job_description_string = rbin+'_d'+str(depth).zfill(2)+'_a'+str(adaptive_depth).zfill(2)+'_s'+str(splitting_size).zfill(6)+'_t'+str(num_threads).zfill(3)+'_r'+adaptivity_parameters.replace('/', '-')
						job_filepath = jobfile_directory+'/'+job_description_string+'.sh'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'

						info = ""
						info += "depth\t"+str(depth)+"\n"
						info += "adaptive_depth\t"+str(adaptive_depth)+"\n"
						info += "splitting_size\t"+str(splitting_size)+"\n"
						info += "num_threads\t"+str(num_threads)+"\n"
						info += "adaptivity_parameters\t"+str(adaptivity_parameters)+"\n"

						exec_string="../../build/"+sierpi_run

						job_file_content="""#! /bin/bash
cd """+working_directory+"""

source ./inc_vars.sh

export OMP_NUM_THREADS="""+str(num_threads)+"""

export KMP_AFFINITIES=compact

# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

echo \""""+exec_string+"""\"
"""+exec_string+""" > """+output_filepath+"""

mv \"0_25.csv\" \""""+jobfile_directory+"/"+job_description_string+"""_0.25.csv\"
mv \"9_95.csv\" \""""+jobfile_directory+"/"+job_description_string+"""_9.95.csv\"

echo \""""+info+"""bouy\t0.25\" > """+jobfile_directory+"/"+job_description_string+"""_0.25.info

echo \""""+info+"""bouy\t9.95\" > """+jobfile_directory+"/"+job_description_string+"""_9.95.info

"""

#						print job_file_content

						print "Writing jobfile '"+job_filepath+"'"
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()

						os.chmod(job_filepath, stat.S_IEXEC | stat.S_IREAD | stat.S_IWRITE)
