#! /bin/bash

cd ../..

make clean

scons --enable-swe-benchmarks=off --xml-config=./scenarios/single_wave_on_simple_beach_1d_bouy_sampling.xml --hyperbolic-adaptivity-mode=2 $@ -j 4 || exit -1
