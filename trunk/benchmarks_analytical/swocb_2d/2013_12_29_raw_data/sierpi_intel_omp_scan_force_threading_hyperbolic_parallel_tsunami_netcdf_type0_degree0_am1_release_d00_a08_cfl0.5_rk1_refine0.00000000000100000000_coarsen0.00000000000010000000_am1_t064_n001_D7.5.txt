./build/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release -c ./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml -d 0 -w -16 -a 8 -r 0.00000000000100000000/0.00000000000010000000 -C 0.5 -v 3 -n 64 -D -0.00000001/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g5_0_0.csv,-2.18/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g6_2_18.csv,-4.36/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g7_4_36.csv,-5.82/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g8_5_82.csv,-7.29/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g9_7_29.csv,-7.76/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000100000000_coarsen0.00000000000010000000_am1_t064_n001_D7.5_dart_g10_7_76.csv
Loading NetCDF data data/tsunami_benchmarks/SingleWaveOnaSimpleBeach_BenchMark_Values_1000.nc
 + Initial number of triangles after base triangulation: 512
 + Number of threads to use: 64
 + Max number of threads (system): 1
 + Verbose: 3
 + Terminate simulation after #n equal timesteps: -1
 + Timesteps: -1

 + Output data each #nth timestep: -100
 + Output data each n simulation seconds: -1
 + Output data each n computation seconds: -1

 + Output .vtp grid data: off
 + Output .vtk cluster data: off

Output simulation specific data:
 + Output simulation specific data each n simulation seconds: 0.05
 + Output simulation specific: on

 + Output simulation grid filename: frame_%08i.vtp
 + Output simulation cluster filename: 

 + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS: [enabled]
 + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING: 128
 + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_SHRINK_EXTRA_PADDING: 112
 + CONFIG_ENABLE_WRITER_TASK: [disabled]
 + CONFIG_WRITER_THREAD_WITH_PTHREAD: [disabled]
 +
 + Compile options:
   - Flux solver: augumented riemann (geoclaw)
   - RK ORDER: 1
   - Order of basis functions: 0
   - Bathymetry kernels: [enabled]
   - Adaptivity mode: 1
   - Gravitation: 9.81
   - Dry threshold: 0.001
   - Zero threshold: 1e-06

 + Grid options:
   - initial recursion depth: 0
   - min relative recursion depth: 0
   - max relative recursion depth: 8

 + Parallelization options:
   - split size: 4
   - join size: 2

 + Simulation options:
   - CFL / neg. fixed timestep: 0.5
   - Adaptive timestep size with CFL: [enabled]
   - Timestep size: 0.0117170304609379
   - Minimum timestep size: 1e-06
   - Gravitation: 9.81
   - World Scene ID: -16
   - Terrain Scene ID: 101
   - Displacement Scene ID: 101

 + Adaptive:
   - conforming cluster skipping: [DEACTIVATED]
   - refine threshold: 1e-12
   - coarsen threshold: 1e-13
   - domain boundary condition: bounce back
Water setup: single wave on Composite beach
Terrain setup: single wave on composite beach
[ START ]
WARNING: Timestep size for accurate simulation specific data output is too small. Fixing the problem for this timestep...
[ END ]


Timings for simulation phases:
 + EdgeCommTime: 427.269
 + AdaptiveTime: 37.0333
 + SplitJoinTime: 51.4942

51947 TS (Timesteps)
305.001 ST (SIMULATION_TIME)
0.000732314 TSS (Timestep size)
525.752 RT (REAL_TIME)
5928835234 CP (Cells processed)
0.0101209 ASPT (Averaged Seconds per Timestep)
114132 CPST (Cells Processed in Average per Simulation Timestep)
594.456 ACPST (Averaged number of clusters per Simulation Timestep)
11.2769 MCPS (Million Cells per Second) (local)
0.00091774 MCPSPT (Million Clusters per Second per Thread)
6.96609 EDMBPT (CellData Megabyte per Timestep (RW))
688.286 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.563844 GFLOPS
[ END ]
EXIT STATUS: OK
