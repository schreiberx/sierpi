#! /bin/bash
# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/.//sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5.txt
#SBATCH -e /home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/.//sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5.err
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/../../
# job description
#SBATCH -J sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5
#SBATCH --get-user-env
#SBATCH --partition=wsm
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00


source /etc/profile.d/modules.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/../../

source /home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/inc_vars.sh

export KMP_AFFINITIES=compact

echo "./build/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release -c ./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml -d 0 -w -16 -a 8 -r 0.00000000000010000000/0.00000000000001000000 -C 0.5 -v 3 -n 64 -D -0.00000001/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g5_0_0.csv,-2.18/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g6_2_18.csv,-4.36/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g7_4_36.csv,-5.82/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g8_5_82.csv,-7.29/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g9_7_29.csv,-7.76/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g10_7_76.csv"
./build/sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release -c ./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml -d 0 -w -16 -a 8 -r 0.00000000000010000000/0.00000000000001000000 -C 0.5 -v 3 -n 64 -D -0.00000001/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g5_0_0.csv,-2.18/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g6_2_18.csv,-4.36/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g7_4_36.csv,-5.82/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g8_5_82.csv,-7.29/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g9_7_29.csv,-7.76/0/output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g10_7_76.csv
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g5_0_0.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g6_2_18.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g7_4_36.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g8_5_82.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g9_7_29.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"
mv "output_d00_a08_cfl0.5_rk1_refine0.00000000000010000000_coarsen0.00000000000001000000_am1_t064_n001_D7.5_dart_g10_7_76.csv" "/home/hpc/pr63so/di69fol/workspace/sierpi_2013_12_29/benchmarks_analytical/swocb_2d_hr_params/./"

