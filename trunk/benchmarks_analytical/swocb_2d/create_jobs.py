#! /usr/bin/python

import sys
import time
import commands
import os
import re
import stat

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=''


#
# Sierpi parameter sets
#

# BIN SET
sierpi_bin_set=[
	'sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release',
	'sierpi_intel_omp_scan_force_threading_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release'
]

# DEPTH SET
sierpi_sim_depth_set=[0,2,4,6,8]

# ADAPTIVE DEPTH SET
sierpi_sim_adaptive_depth_set=[0,2,6,8]

sierpi_sim_max_depth=8

cfl=0.5

num_mpi_nodes=1

# SPLITTING SIZE SET
sierpi_sim_splitting_size_set=[1024*4]

# THREADS SET
sierpi_sim_threads_set=[64]

sierpi_sim_fixedtime=-1
sierpi_sim_fixedtimesteps=100	# not used
sierpi_sim_initial_split=0

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# there's no adaptivity for 1D !!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sierpi_adaptivity_parameters_set=[
	[0.000000001, 0.0000000001],
	[0.00000001, 0.000000001],
	[0.0000001, 0.00000001],
	[0.000001, 0.0000001],
]

estimated_runtime_scalar=1.0


dart_stations = '-0.00000001/0/g5_0_0.csv,-2.18/0/g6_2_18.csv,-4.36/0/g7_4_36.csv,-5.82/0/g8_5_82.csv,-7.29/0/g9_7_29.csv,-7.76/0/g10_7_76.csv'
p = re.compile('([^/]*).csv')



# bin set
for sierpi_bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			if depth+adaptive_depth > sierpi_sim_max_depth:
				continue

			if sierpi_bin.find('scan_data') > 0:
				sierpi_sim_splitting_size_set_ = [0]
			else:
				sierpi_sim_splitting_size_set_ = sierpi_sim_splitting_size_set

			# splitting sizes
			for splitting_size in sierpi_sim_splitting_size_set_:

				# threads set
				for num_threads in sierpi_sim_threads_set:

					# threads set
					for adaptivity_parameters in sierpi_adaptivity_parameters_set:

						if adaptive_depth == 0:
							if len(sierpi_adaptivity_parameters_set) > 1:
								if adaptivity_parameters == sierpi_adaptivity_parameters_set[1]:
									break

						adaptivity_parameters = ["%.12f" % adaptivity_parameters[0], "%.12f" % adaptivity_parameters[1]]

						# estimate number of cells
						max_cells=2**(depth+adaptive_depth)

						def get_estimated_runtime(max_cells, splitting_size):

							# estimate clusters
							clusters=max_cells/splitting_size
							clusters+=1

							# clusters per thread
							clusters_per_thread=clusters/num_threads
							clusters_per_thread+=1

							# cells per thread
							cells_per_thread=clusters_per_thread*splitting_size

							return cells_per_thread*0.0001

						if splitting_size == 0:
							estimated_runtime=get_estimated_runtime(max_cells, 16*1024)
						else:
							estimated_runtime=get_estimated_runtime(max_cells, splitting_size)

						estimated_runtime*=estimated_runtime_scalar

						estimated_runtime_string=time.strftime("%H:%M:%S", time.gmtime(estimated_runtime))

						world_scene_id=-16
						world_scene_related_depth=depth

						while world_scene_related_depth > 6:
							world_scene_id=world_scene_id-1
							world_scene_related_depth=world_scene_related_depth-2

						sierpi_run=sierpi_bin
						sierpi_run+=' -c ./scenarios/solitary_wave_on_composite_beach_2d_bouy_sampling.xml'
						sierpi_run+=' -d '+str(world_scene_related_depth)
						sierpi_run+=' -w '+str(world_scene_id)
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -r '+str(adaptivity_parameters[0])+'/'+str(adaptivity_parameters[1])
						sierpi_run+=' -C 0.5'
						sierpi_run+=' -v 3'
						sierpi_run+=' -n '+str(num_threads)

						cartesian_depth = 7.5+float(depth)/2.0

                                                id = 'd'+str(depth).zfill(2)
                                                id += '_a'+str(adaptive_depth).zfill(2)
                                                id += '_cfl'+str(cfl)
                                                id += '_rk1_refine'+str(adaptivity_parameters[0])+'_coarsen'+str(adaptivity_parameters[1])

						if '_am2_' in sierpi_bin:
							id += '_am1'
						else:
							id += '_am2'

                                                id += '_t'+str(num_threads).zfill(3)
                                                id += '_n'+str(num_mpi_nodes).zfill(3)
						id += '_D'+str(cartesian_depth)

						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params

						job_description_string = sierpi_bin+'_'+id

						sierpi_run += ' -D '+p.sub('output_'+id+'_dart_\\1.csv', dart_stations)

						job_filepath = jobfile_directory+'/'+job_description_string+'.cmd'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'
						output_error_filepath = jobfile_directory+'/'+job_description_string+'.err'

						exec_string="./build/"+sierpi_run

						csv_ids=['g5_0_0', 'g6_2_18', 'g7_4_36', 'g8_5_82', 'g9_7_29', 'g10_7_76']
						c = ''
						for f in csv_ids:
							c += 'mv "output_'+id+'_dart_'+f+'.csv" "'+jobfile_directory+'"'+"\n"

						job_file_content="""#! /bin/bash
# output
#SBATCH -o """+output_filepath+"""
#SBATCH -e """+output_error_filepath+"""
# working directory
#SBATCH -D """+working_directory+"""/../../
# job description
#SBATCH -J """+job_description_string+"""
#SBATCH --get-user-env
#SBATCH --partition=wsm
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00


source /etc/profile.d/modules.sh

cd """+working_directory+"""/../../

source """+working_directory+"""/inc_vars.sh

export KMP_AFFINITIES=compact

echo \""""+exec_string+"""\"
"""+exec_string+"""
"""+c+"""
"""

#						print job_file_content

						print "Writing jobfile '"+job_filepath+"'"
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()
