###
### Sierpinski Benchmark analysis###
### Contact: breuera@in.tum.de, schreibm@in.tum.de
###

# clear workspace
rm(list=ls())

# path to benchmark directory
benchmark_dir <- getwd()

## datasets to analyze
benchmark_output <- "2013_12_29_raw_data"
benchmark_measured_data <- "analytic_data"
benchmark_error_baseline_dir <- "analytic_data"

sampling_points=c('g4', 'g5_0_0', 'g6_2_18', 'g7_4_36', 'g8_5_82', 'g9_7_29', 'g10_7_76')
benchmark_output_group_regexp <- "^output_d([0-9]*)_a([0-9]*)_cfl(.*)_rk([0-9])_refine(.*)_coarsen(.*)_am(.)_t.*dart_(.*)\\.csv$"


# select sampling point to plot data for
#sampling_point_ids=c(2,3)
#sampling_point_ids=c(4,5)
#sampling_point_ids=c(6,7)
#sampling_point_ids=c(6)
sampling_point_ids=c(5)

# 0: bouy data plots
# 1: error plots
# 2: cell distribution
# 3: cell distribution + error in legend + file output
plotMode <- 1

plot_points <- 1
points_number_of <- 30



limit_d <- c(10,16,22)
limit_d <- -1
#limit_d <- c(6)

limit_a <- c(0, 6, 12)
limit_a <- -1
#limit_a <- c(0)

limit_refine_level <- c(1000,100,10)
#limit_refine_level <- c(0.0000000001, 0.00000000001)
limit_refine_level <- -1

limit_am <- c(1,2)
limit_am <- c(2)

#limit_max_level <- c(8)
limit_max_level <- c(8)
#limit_max_level <- -1
#limit_cfl <- c(0.5)
limit_cfl <- -1
limit_rk <- -1

line_width <- 1
line_width_match <- 1

# error 1-norm for d=6, a=0
max_error_norm <- 3.80382185819001e-05
#max_error_norm <- -1
max_error_norm <- max_error_norm*0.9
#max_error_norm <- max_error_norm*1.1

#max_number_of_cells <- 2**15
max_number_of_cells <- 32768
#max_number_of_cells <- -1
#max_number_of_cells <- max_number_of_cells*0.9


#################################################################################
#################################################################################
#################################################################################

# visualization of sampling points

param_xlim <- c(265,295)
#param_xlim <- c(265,320)
param_ylim <- c(-0.001, 0.015)



##########################
###### ERROR PLOTS #######
##########################

# benchmark file to compare with
error_benchmark_dataset_baseline<-"ts3a_analytical.txt"
#error_benchmark_dataset_baseline<-""
error_spline_sampling_points <- 100000

# 0: euclidian
# 1: 1-norm
use_norm = 1

# error area with min/max
error_area_xmin <- 270
error_area_xmax <- 295

error_plotMode_yrange <- c(-0.0013, 0.0015)



################################
###### CELL DISTRIBUTION #######
################################

cellDist_plotMode_yrange <- c(0, 2**(17)*1.1)


#####################################################

# the benchmark simulation data was scaled by this factor -> rescale
scale_factor = 10000.0



benchmark_output_dir <- paste(benchmark_dir, benchmark_output, sep="/")
benchmark_error_baseline_dir <- paste(benchmark_dir, benchmark_error_baseline_dir, sep="/")
benchmark_measured_data_dir <- paste(benchmark_dir, benchmark_measured_data, sep="/")



attach(mtcars)
par(mfrow=c(length(sampling_point_ids), 1))

#list.files(path=benchmark_output_dir, pattern=benchmark_output_group_regexp)


# find dart file
benchmark_measured_data_list_file <- paste(paste(benchmark_dir, benchmark_measured_data, sep="/"), "ts3a_analytical.txt", sep="/")

# READ DART DATASET
benchmark_measured_data_list_file_dataset <- read.table(benchmark_measured_data_list_file, blank.lines.skip = TRUE, skip=6)


for (sampling_point_id in sampling_point_ids)
{
  if (plotMode == 1 || plotMode == 3)
  {
    # output data to file (only for plotMode 1 and 3)
    writeErrorToFile <- sprintf("%s_%s_error.txt", benchmark_output, sampling_points[sampling_point_id])
    writeErrorToFile <- paste(benchmark_dir, writeErrorToFile, sep="/")

    if (writeErrorToFile != "")
    {
      headers<-c("depth", "adapt", "refine", "coarsen", "am", "errorNorm")
      
      if (plotMode == 3)
        headers <- c(headers, "avgCellsPerTimestep")
      
      write(headers,
            ncolumns = length(headers),
            file = writeErrorToFile,
            append = FALSE,
            sep = "\t"
      )
    }
  }
  
  
  #benchmark_measured_data_filename = benchmark_measured_data_list[[use_dart_nr]]

  # BENCHMARK OUTPUT FILES
  benchmark_output_files <- list.files(path=benchmark_output_dir, pattern=benchmark_output_group_regexp)
#  print(benchmark_output_files)
  
  if (length(benchmark_output_files) == 0)
  {
    print("NO files found")
    return
  }
  
  tmp_benchmark_output_files <- c()

  # use limitations to draw only a subset of images
  for (benchmark_output_filename in benchmark_output_files)
  {
    s <- sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4 \\5 \\6 \\7 \\8", benchmark_output_filename)
    tmp <- unlist(strsplit(s, split=" "))
    nd <- as.integer(tmp[1])
    na <- as.integer(tmp[2])
    ncfl <- as.double(tmp[3])
    nrk <- as.integer(tmp[4])
    refine <- as.double(tmp[5])
    coarsen <- as.double(tmp[6])
    am <- as.integer(tmp[7])
    gauge_name <- tmp[8]
    
    if (limit_d != -1 && length(which(limit_d == nd)) == 0)
    {
#      print("limit_d failed")
      next
    }
    
    if (limit_am != -1 && length(which(limit_am == am)) == 0)
    {
#      print("limit_am failed")
      next
    }
    
    if (limit_a != -1 && length(which(limit_a == na)) == 0)
    {
#      print("limit_a failed")
      next
    }
    
    if (limit_refine_level != -1 && length(which(limit_refine_level == refine)) == 0)
    {
#      print("limit_refine_level failed")
      next
    }
    
    max_level <- na+nd
    if (limit_max_level != -1 && length(which(limit_max_level == max_level)) == 0)
    {
#      print("limit_max_level failed")
      next
    }
    
    if (limit_cfl != -1 && length(which(limit_cfl == ncfl)) == 0)
    {
#      print("limit_cfl failed")
      next
    }
    
    if (limit_rk != -1 && length(which(limit_rk == nrk)) == 0)
    {
#      print("limit_rk failed")
      next
    }
    if (sampling_points[sampling_point_id] != gauge_name)
    {
#      print("Gauge match failed")
      next
    }

    tmp_benchmark_output_files <- c(tmp_benchmark_output_files, benchmark_output_filename)
  }

  benchmark_output_files <- tmp_benchmark_output_files

  rainbow_map <- c("black", rainbow(length(benchmark_output_files)))

  # plotting symbols
  plot_pch <- 1:length(benchmark_output_files)
  for (i in 1:length(benchmark_output_files))
  {
    plot_pch[i] <- (i %% 10) + 1
  }

  benchmark_baseline_dataset <- data.frame(
    time=benchmark_measured_data_list_file_dataset[[1]],
    surface_elevation=benchmark_measured_data_list_file_dataset[[sampling_point_id+1]]
  )

  # append timestep 20000 if required
  if (tail(benchmark_baseline_dataset$time, n=1) < error_area_xmax)
  {
    append(benchmark_baseline_dataset$time, error_area_xmax)
    append(benchmark_baseline_dataset$surface_elevation, 0)
  }

  # load error baseline data?
  if (plotMode == 1 || plotMode == 3)
  {
    # compute approximating spline
    interpolated_baseline_dataset <- spline(benchmark_baseline_dataset$time,benchmark_baseline_dataset$surface_elevation, n=error_spline_sampling_points, method="natural", xmin=error_area_xmin, xmax=error_area_xmax, ties=0)
  }

  if (plotMode == 0)
  {
    # DART PLOTS
    plot(benchmark_baseline_dataset$time, benchmark_baseline_dataset$surface_elevation, type="l", xlab="", ylab="", ylim=param_ylim, xlim=param_xlim, col=rainbow_map[1], lwd=2)
    title(main=paste("Gauge ", sampling_points[sampling_point_id], sep=""), xlab="time (seconds)", ylab="height")
    plot_pch[1] <- NA

    legend_data <- c("Analytic data")
    c <- 2
  }else if (plotMode == 1)
  {
    # start error plot
    plot(c(error_area_xmin, error_area_xmax), c(999999,9999999), type="l", xlab="", ylab="", ylim=error_plotMode_yrange, xlim=c(error_area_xmin, error_area_xmax), col="black")
    text(3550, 0.4, error_benchmark_dataset_baseline, cex=0.8)
    title(main=paste("Error plot ", sampling_points[sampling_point_id], sep=""), xlab="time (seconds)", ylab="height")
    
    legend_data <- c()
    c <- 1
  }else if (plotMode == 2 || plotMode == 3)
  {
    # start error plot
    plot(param_xlim, c(-99999999,-9999999), type="l", xlab="", ylab="", ylim=cellDist_plotMode_yrange, xlim=param_xlim, col="black")
    title(main=paste("Cell distribution plot "), xlab="time (seconds)", ylab="cells")

    legend_data <- c()
    c <- 1
  }


  plotted_files = c()
  for (benchmark_output_filename in benchmark_output_files)
  {
    line_width_plot <- line_width_match

    tmp <- unlist(strsplit(sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4 \\5 \\6 \\7 \\8", benchmark_output_filename), split=" "))
    nd <- tmp[1]
    na <- tmp[2]
    ncfl <- tmp[3]
    nrk <- tmp[4]
    refine <- as.numeric(tmp[5])*scale_factor
    coarsen <- as.numeric(tmp[6])*scale_factor
    am <- as.numeric(tmp[7])
#    refine <- sprintf("%.11f", refine)
#    coarsen <- sprintf("%.11f", coarsen)
#    refine <- sprintf("%f", refine)
#    coarsen <- sprintf("%f", coarsen)
#    dataset_name <- sprintf("d=%s, a=%s, cfl=%s, rc=%s/%s, am=%s", nd, na, ncfl, refine, coarsen, am)
    dataset_name <- sprintf("d=%s/%s, rc=%.e/%.e", nd, na, refine, coarsen)
    
    # benchmark output filename
    benchmark_output_file <- paste(benchmark_output_dir, benchmark_output_filename, sep="/")
    benchmark_single_dataset <- read.table(benchmark_output_file, sep=",", col.names=c("time", "surface_elevation", "reference_data_INVALID", "number_of_local_cells", "number_of_global_cells"), header=FALSE)

    # compute error norm
    if (plotMode == 1 || plotMode == 3)
    {
      # append timestep 20000 if required
      if (tail(benchmark_single_dataset$time, n=1) < error_area_xmax)
      {
        append(benchmark_single_dataset$time, error_area_xmax)
        append(benchmark_single_dataset$surface_elevation, 0)
      }
      
      benchmark_single_dataset$surface_elevation <- benchmark_single_dataset$surface_elevation*scale_factor
      
      # compute approximating spline
      interpolated_single_dataset <- spline(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation, n=error_spline_sampling_points, method="natural", xmin=error_area_xmin, xmax=error_area_xmax, ties=0)
      
      # error data
      error_data <- interpolated_baseline_dataset$y - interpolated_single_dataset$y
      
      
      #Compute Euclidian norm
      if (use_norm == 0)
      {
        normalized_data <- error_data
        norm <- sqrt(sum(normalized_data^2))/error_spline_sampling_points
      }else
      {
        #Compute 1-norm
        norm <- sum(abs(error_data))/error_spline_sampling_points
      }
      
      if (max_error_norm != -1)
      {
        if (norm > max_error_norm)
          line_width_plot <- line_width

        if (norm > max_error_norm)
          next
      }

      if (writeErrorToFile != "")
      {
        data <- c(nd, na, refine, coarsen, am, norm)
        
#        if (plotMode == 3)
        {
          avgTotalCells <- sum(benchmark_single_dataset$number_of_global_cells)/length(benchmark_single_dataset$number_of_global_cells)
          data <- c(data, avgTotalCells)
        }

        if (max_number_of_cells != -1)
        {
#          if (avgTotalCells > max_number_of_cells)
#            line_width_plot <- line_width
          if (avgTotalCells > max_number_of_cells)
            next
        }
        
        write(data,
              ncolumns = length(data),
              file = writeErrorToFile,
              append = TRUE,
              sep = "\t"
        )
      }

      norm_plot <- sprintf("%e", norm)
      dataset_name <- paste(dataset_name, ", ", sep="")
      #      dataset_name <- paste(paste(dataset_name, "error=", sep=""), round(norm, digits=6), sep="")
      dataset_name <- paste(paste(dataset_name, "error=", sep=""), norm_plot, sep="")
    }

    if (plotMode == 0)
    {
      # bouy data plots
      lines(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation*scale_factor, type="l", col=rainbow_map[c], lwd=line_width_plot)

      if (plot_points)
        point_base_set <- list(benchmark_single_dataset$time, benchmark_single_dataset$surface_elevation*scale_factor)

    }else if (plotMode == 1)
    {
      # ERROR PLOTS      
      lines(interpolated_single_dataset$x, error_data, type="l", col=rainbow_map[c], lwd=line_width_plot)

      if (plot_points)
        point_base_set <- list(interpolated_single_dataset$x, error_data)
      
    }else if (plotMode == 2 || plotMode == 3)
    {
      # triangle distriubtion plots
      lines(benchmark_single_dataset$time, benchmark_single_dataset$number_of_global_cells, type="l", col=rainbow_map[c], lwd=line_width_plot)
      
      if (plot_points)
        point_base_set <- list(benchmark_single_dataset$time, benchmark_single_dataset$number_of_global_cells)

    }

    if (plot_points)
    {
      point_set <- spline(point_base_set[[1]], point_base_set[[2]], n=points_number_of)
      points(point_set[[1]], point_set[[2]], type="p", col=rainbow_map[c], pch=plot_pch[c])
    }
    
    legend_data <- c(legend_data, dataset_name)
    c <- c+1
    
    plotted_files = c(plotted_files, benchmark_output_filename)
  }

  print(plotted_files)

#  legend("topright", legend_data, col=rainbow_map, lty=1, cex=0.5, pch=plot_pch)
  legend("topleft", legend_data, col=rainbow_map, lty=1, cex=0.5, pch=plot_pch)
  
  #  legend("topright", legend_data, text.width=10000, col=rainbow_map, lty=1, pch=plot_pch, cex=0.7)

  #dev.off()
}
