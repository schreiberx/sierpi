WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'swe



WARNING: In scope 'swe', parameter '

', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'visualization-dof-method' not handled with value '1'
' not handled with value '1'
' not handled with value '1'
1'
1'
1'
' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.619589844447338
global error l2: 0.449689382970536
global error linf: 0.861040639202765
average edge length sphere: 24426600398.9397
average edge length grid: 24425833760.983
relative error edges: -3.13853726748094e-05
relative avg error edges: 1.03938059046665e-05
average cell area sphere: 511207893389747
average cell area grid: 511142644567860
relative error area: -0.000127636569642258
relative avg error area: 0.00012652553768378

++++++++++ MPI RANK 0 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.773 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649625 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945776 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.888 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1335 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472888 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.771 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649598 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945816 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.919 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1376 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472908 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.7714 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649603 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945808 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.912 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1368 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472904 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.772 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649611 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945797 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.904 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1356 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472898 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.7664 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649533 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.094591 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.992 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1474 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472955 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.7716 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649606 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945804 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.91 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1364 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472902 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.7707 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649593 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945823 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.925 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1384 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472912 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
46.7703 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0649587 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0945831 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
738.931 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.1392 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00472916 GFLOPS


++++++++++ SUMMARY ++++++++++
0.756656532561666 GMCPS (Global Million Cells per Second) (global)
35.38944 GMCP (Global Million Cells Processed) (global)
11.5456624231211 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
720 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0378328266280833 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 720
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
