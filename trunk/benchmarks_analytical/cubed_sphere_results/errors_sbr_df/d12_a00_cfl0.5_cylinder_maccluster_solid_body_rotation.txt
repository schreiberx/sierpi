WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
'


4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
visualization-boundary-method' not handled with value '1'
1'
', parameter 'visualization-boundary-method' not handled with value '1'


WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.777718326654582
global error l2: 0.517848290075033
global error linf: 0.840840150699809
average edge length sphere: 24059502745.0983
average edge length grid: 24058703173.9771
relative error edges: -3.32330692624167e-05
relative avg error edges: 1.03943385765539e-05
average cell area sphere: 511207893411148
average cell area grid: 511133508765753
relative error area: -0.000145507622933186
relative avg error area: 0.000126520800576766

++++++++++ MPI RANK 0 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.5932 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652657 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941383 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.455 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6776 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470691 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.59 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652635 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941414 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.48 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6809 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470707 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.5958 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652675 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941357 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.435 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.675 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470679 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.5975 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652686 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941341 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.422 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6733 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047067 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.597 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652682 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941346 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.427 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6739 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470673 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.598 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652689 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941337 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.419 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6729 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470668 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.5949 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652668 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0941366 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.442 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6759 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00470683 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1480 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
96.5955 RT (REAL_TIME)
9093120 CP (Cells processed)
0.0652672 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.094136 MCPS (Million Cells per Second) (local)
9.09312 MCP (Million Cells processed) (local)
735.438 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.6753 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047068 GFLOPS


++++++++++ SUMMARY ++++++++++
0.753090423112924 GMCPS (Global Million Cells per Second) (global)
72.74496 GMCP (Global Million Cells Processed) (global)
11.491247911269 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1480 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0376545211556462 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1480
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
