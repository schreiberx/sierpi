WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4visualization-dof-method' not handled with value '4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''

WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swevisualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe
WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1' not handled with value '1'

'
current timestep: 5
cell count: 196608
global error l1: 0.138130727431492
global error l2: 0.292831029766195
global error linf: 0.610020951054165
average edge length sphere: 48120394718.9395
average edge length grid: 48119994878.3898
relative error edges: -8.3091701963839e-06
relative avg error edges: 2.59879208983585e-06
average cell area sphere: 511207893452450
average cell area grid: 511189294315102
relative error area: -3.63827272372311e-05
relative avg error area: 3.16332049609726e-05

++++++++++ MPI RANK 0 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.171 RT (REAL_TIME)
113369088 CP (Cells processed)
0.0360223 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682244 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.254 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.895 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341122 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.17 RT (REAL_TIME)
113369088 CP (Cells processed)
0.0360221 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682249 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.259 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341124 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.17 RT (REAL_TIME)
113369088 CP (Cells processed)
0.036022 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682249 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.259 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341125 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.169 RT (REAL_TIME)
113369088 CP (Cells processed)
0.0360218 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682254 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.263 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.905 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341127 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.169 RT (REAL_TIME)
113369088 CP (Cells processed)
0.036022 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682251 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.26 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.902 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341125 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.171 RT (REAL_TIME)
113369088 CP (Cells processed)
0.0360223 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682244 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.254 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.895 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341122 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.169 RT (REAL_TIME)
113369088 CP (Cells processed)
0.036022 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.68225 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.26 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.901 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341125 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
4613 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000151889 TSS (Timestep size)
166.169 RT (REAL_TIME)
113369088 CP (Cells processed)
0.0360219 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.682252 MCPS (Million Cells per Second) (local)
113.369 MCP (Million Cells processed) (local)
666.262 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
707.903 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0341126 GFLOPS


++++++++++ SUMMARY ++++++++++
5.45799353410222 GMCPS (Global Million Cells per Second) (global)
906.952704 GMCP (Global Million Cells Processed) (global)
10.4102965051693 GCPSPT (Global Clusters per Second per Thread) (global)
0.196608 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
4613 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.272899676705111 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 4613
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	24576	24576	0
1	1536	24576	24576	0
2	1536	24576	24576	0
3	1536	24576	24576	0
4	1536	24576	24576	0
5	1536	24576	24576	0
6	1536	24576	24576	0
7	1536	24576	24576	0
