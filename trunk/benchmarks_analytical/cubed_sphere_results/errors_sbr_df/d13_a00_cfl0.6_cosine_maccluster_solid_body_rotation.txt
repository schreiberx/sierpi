WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'swe


WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter '

visualization-dof-method'
1'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe' not handled with value '
visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 98304
global error l1: 0.438890213428547
global error l2: 0.320695839731308
global error linf: 0.569690217850657
average edge length sphere: 34012453270.6244
average edge length grid: 34011888179.2457
relative error edges: -1.6614249320589e-05
relative avg error edges: 5.20401743095295e-06
average cell area sphere: 511207893410386
average cell area grid: 511170423493384
relative error area: -7.32968279332838e-05
relative avg error area: 6.42753136905095e-05

++++++++++ MPI RANK 0 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.876 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658277 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186669 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.177 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.688 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933346 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.875 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658271 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186671 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.182 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.689 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933353 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.874 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658265 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186672 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.189 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.691 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933362 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.875 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658268 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186672 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.187 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.69 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933359 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.875 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658272 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186671 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.182 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.689 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933353 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.876 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658275 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.18667 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.179 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.688 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933349 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.874 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0658261 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186674 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.194 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.692 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00933368 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
103.873 RT (REAL_TIME)
19390464 CP (Cells processed)
0.065826 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.186674 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
729.195 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
193.692 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0093337 GFLOPS


++++++++++ SUMMARY ++++++++++
1.49337205005603 GMCPS (Global Million Cells per Second) (global)
155.123712 GMCP (Global Million Cells Processed) (global)
11.3935245518191 GCPSPT (Global Clusters per Second per Thread) (global)
0.098304 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1578 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0746686025028016 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1578
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	12288	12288	0
1	3072	12288	12288	0
2	3072	12288	12288	0
3	3072	12288	12288	0
4	3072	12288	12288	0
5	3072	12288	12288	0
6	3072	12288	12288	0
7	3072	12288	12288	0
