WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '
', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'


1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 5
cell count: 49152
global error l1: 0.197125982837288
global error l2: 0.394674239166787
global error linf: 0.707339955708095
average edge length sphere: 24059502745.0983
average edge length grid: 24058703173.9771
relative error edges: -3.32330692630509e-05
relative avg error edges: 1.03943385765539e-05
average cell area sphere: 511207893411148
average cell area grid: 511133508765753
relative error area: -0.000145507622932085
relative avg error area: 0.000126520800576766

++++++++++ MPI RANK 0 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.588 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637792 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963323 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.596 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9541 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481661 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.586 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637787 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963331 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.602 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.955 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481665 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.585 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637783 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963337 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.607 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9556 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481668 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.583 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637773 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963353 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.619 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9572 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481676 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.582 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637769 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963358 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.623 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9578 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481679 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.585 RT (REAL_TIME)
13928448 CP (Cells processed)
0.063778 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963341 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.611 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9561 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481671 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.584 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637777 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963346 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.614 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9565 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481673 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
2267 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000830357 TSS (Timestep size)
144.583 RT (REAL_TIME)
13928448 CP (Cells processed)
0.0637772 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0963354 MCPS (Million Cells per Second) (local)
13.9284 MCP (Million Cells processed) (local)
752.62 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.9573 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00481677 GFLOPS


++++++++++ SUMMARY ++++++++++
0.770674124531987 GMCPS (Global Million Cells per Second) (global)
111.427584 GMCP (Global Million Cells Processed) (global)
11.7595539021604 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
2267 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0385337062265994 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 2267
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
