WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'swe4'

'

WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method4'
', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter '' not handled with value '1'
' not handled with value '1'
WARNING: In scope 'swevisualization-dof-method' not handled with value '1'
' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 375296
global error l1: 0.0959458563302428
global error l2: 0.0761108569644846
global error linf: 0.261188621380893
average edge length sphere: 58672304411.5233
average edge length grid: 58671933822.7331
relative error edges: -6.3162474000061e-06
relative avg error edges: 1.36141625229404e-06
average cell area sphere: 511207893515566
average cell area grid: 511191267521919
relative error area: -3.25229595586609e-05
relative avg error area: 1.65809818445935e-05

++++++++++ MPI RANK 0 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500560801 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491083 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4122.45 ACPST (Averaged number of clusters per Simulation Timestep)
0.275118 MCPS (Million Cells per Second) (local)
500.561 MCP (Million Cells processed) (local)
360.86 CPSPT (Clusters per Second per Thread)
50.9546 EDMBPT (CellData Megabyte per Timestep (RW))
285.462 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0137559 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500560678 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491083 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4084.65 ACPST (Averaged number of clusters per Simulation Timestep)
0.275118 MCPS (Million Cells per Second) (local)
500.561 MCP (Million Cells processed) (local)
357.552 CPSPT (Clusters per Second per Thread)
50.9546 EDMBPT (CellData Megabyte per Timestep (RW))
285.462 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0137559 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500561512 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491084 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2032.14 ACPST (Averaged number of clusters per Simulation Timestep)
0.275119 MCPS (Million Cells per Second) (local)
500.562 MCP (Million Cells processed) (local)
177.885 CPSPT (Clusters per Second per Thread)
50.9547 EDMBPT (CellData Megabyte per Timestep (RW))
285.462 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0137559 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500563334 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491085 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1862.9 ACPST (Averaged number of clusters per Simulation Timestep)
0.275119 MCPS (Million Cells per Second) (local)
500.563 MCP (Million Cells processed) (local)
163.07 CPSPT (Clusters per Second per Thread)
50.9549 EDMBPT (CellData Megabyte per Timestep (RW))
285.463 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.013756 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500562580 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491085 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1908.41 ACPST (Averaged number of clusters per Simulation Timestep)
0.275119 MCPS (Million Cells per Second) (local)
500.563 MCP (Million Cells processed) (local)
167.053 CPSPT (Clusters per Second per Thread)
50.9548 EDMBPT (CellData Megabyte per Timestep (RW))
285.463 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0137559 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.45 RT (REAL_TIME)
500567104 CP (Cells processed)
0.1785 ASPT (Averaged Seconds per Timestep)
0.0491089 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2005.97 ACPST (Averaged number of clusters per Simulation Timestep)
0.27512 MCPS (Million Cells per Second) (local)
500.567 MCP (Million Cells processed) (local)
175.593 CPSPT (Clusters per Second per Thread)
50.9553 EDMBPT (CellData Megabyte per Timestep (RW))
285.463 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.013756 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.44 RT (REAL_TIME)
500559552 CP (Cells processed)
0.178499 ASPT (Averaged Seconds per Timestep)
0.0491082 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2694.33 ACPST (Averaged number of clusters per Simulation Timestep)
0.275117 MCPS (Million Cells per Second) (local)
500.56 MCP (Million Cells processed) (local)
235.849 CPSPT (Clusters per Second per Thread)
50.9545 EDMBPT (CellData Megabyte per Timestep (RW))
285.46 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0137558 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
10193 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
104.606 TSS (Timestep size)
1819.45 RT (REAL_TIME)
500532529 CP (Cells processed)
0.1785 ASPT (Averaged Seconds per Timestep)
0.0491055 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3080.12 ACPST (Averaged number of clusters per Simulation Timestep)
0.275101 MCPS (Million Cells per Second) (local)
500.533 MCP (Million Cells processed) (local)
269.618 CPSPT (Clusters per Second per Thread)
50.9518 EDMBPT (CellData Megabyte per Timestep (RW))
285.444 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.013755 GFLOPS


++++++++++ SUMMARY ++++++++++
2.20092953250463 GMCPS (Global Million Cells per Second) (global)
4004.46809 GMCP (Global Million Cells Processed) (global)
3.72554663543733 GCPSPT (Global Clusters per Second per Thread) (global)
0.39286452369273 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
10193 GNT (Global Number of Timesteps) (global)

7637848 MSNGCells (Migration Sent Number of Global Cells)
324314 MSNGCluster (Migration Sent Number of Global Cluster)
0.00190731953691518 MRC (Migrated Relative Cells)
0.110046476625231 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 10193
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	5273	46912	46912	0
1	8303	46912	46912	0
2	4782	46922	46912	-0.000213165
3	2039	46905	46912	0.000149216
4	1483	46894	46912	0.000383697
5	2497	46932	46912	-0.00042633
6	1960	46911	46912	2.13165e-05
7	1519	46908	46912	8.5266e-05
