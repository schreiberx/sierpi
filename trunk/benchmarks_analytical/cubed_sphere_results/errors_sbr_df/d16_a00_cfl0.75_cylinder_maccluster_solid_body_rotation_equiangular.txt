WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope '
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''

sweWARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe
WARNING: In scope 'sweWARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-dof-method'
visualization-boundary-method' not handled with value '1'
' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 786432
global error l1: 0.317771494055842
global error l2: 0.309386550202684
global error linf: 0.863943878556255
average edge length sphere: 97712240375.7479
average edge length grid: 97712048675.4142
relative error edges: -1.96188658644813e-06
relative avg error edges: 6.49708904860269e-07
average cell area sphere: 511207892973087
average cell area grid: 511203815220689
relative error area: -7.97670077890831e-06
relative avg error area: 7.90763733658977e-06

++++++++++ MPI RANK 0 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.177 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546892 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.7975 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.843 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898751 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.176 RT (REAL_TIME)
282525696 CP (Cells processed)
0.054689 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79751 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.845 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898755 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.175 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546888 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79752 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.847 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.1 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898759 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.173 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546879 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79755 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.854 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.13 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898773 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.175 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546886 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79752 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.848 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.11 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898762 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.174 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546882 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79754 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.852 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.12 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898768 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.174 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0546882 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79754 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.852 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.12 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898768 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
157.173 RT (REAL_TIME)
282525696 CP (Cells processed)
0.054688 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79754 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
438.853 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1865.13 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0898772 GFLOPS


++++++++++ SUMMARY ++++++++++
14.3802141043599 GMCPS (Global Million Cells per Second) (global)
2260.205568 GMCP (Global Million Cells Processed) (global)
6.85702042787545 GCPSPT (Global Clusters per Second per Thread) (global)
0.786432 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
2874 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.719010705217993 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 2874
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	98304	98304	0
1	1536	98304	98304	0
2	1536	98304	98304	0
3	1536	98304	98304	0
4	1536	98304	98304	0
5	1536	98304	98304	0
6	1536	98304	98304	0
7	1536	98304	98304	0
