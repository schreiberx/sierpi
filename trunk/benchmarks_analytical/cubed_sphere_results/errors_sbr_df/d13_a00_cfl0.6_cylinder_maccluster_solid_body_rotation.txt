WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter ''
4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '14'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value ''
swe', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
'
WARNING: In scope 'swe', parameter '1'
WARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 98304
global error l1: 0.66948689619813
global error l2: 0.473018246897842
global error linf: 0.842855761837808
average edge length sphere: 34012453270.6244
average edge length grid: 34011888179.2457
relative error edges: -1.66142493214862e-05
relative avg error edges: 5.20401743095295e-06
average cell area sphere: 511207893410386
average cell area grid: 511170423493384
relative error area: -7.32968279341396e-05
relative avg error area: 6.42753136905095e-05

++++++++++ MPI RANK 0 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.492 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674856 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182083 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.263 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.929 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910416 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.492 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674852 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182084 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.267 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.93 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910422 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.492 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674853 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182084 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.266 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.93 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0091042 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.492 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674851 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182085 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.268 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.931 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910423 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.491 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674849 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182085 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.271 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.931 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910426 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.489 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674835 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182089 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.285 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.935 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910445 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.491 RT (REAL_TIME)
19390464 CP (Cells processed)
0.0674847 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182086 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.272 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.932 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910428 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1578 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
317.309 TSS (Timestep size)
106.491 RT (REAL_TIME)
19390464 CP (Cells processed)
0.067485 ASPT (Averaged Seconds per Timestep)
0.012288 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.182085 MCPS (Million Cells per Second) (local)
19.3905 MCP (Million Cells processed) (local)
711.269 CPSPT (Clusters per Second per Thread)
12.75 EDMBPT (CellData Megabyte per Timestep (RW))
188.931 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00910424 GFLOPS


++++++++++ SUMMARY ++++++++++
1.45668096690471 GMCPS (Global Million Cells per Second) (global)
155.123712 GMCP (Global Million Cells Processed) (global)
11.1135938026788 GCPSPT (Global Clusters per Second per Thread) (global)
0.098304 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1578 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0728340483452357 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1578
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	12288	12288	0
1	3072	12288	12288	0
2	3072	12288	12288	0
3	3072	12288	12288	0
4	3072	12288	12288	0
5	3072	12288	12288	0
6	3072	12288	12288	0
7	3072	12288	12288	0
