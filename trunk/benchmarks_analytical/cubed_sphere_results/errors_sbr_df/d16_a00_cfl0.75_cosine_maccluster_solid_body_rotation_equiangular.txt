WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'4'


WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter '
visualization-boundary-method' not handled with value '1'
1'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter '
'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1visualization-boundary-method' not handled with value '1'
'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 786432
global error l1: 0.105469102457451
global error l2: 0.0804946173360933
global error linf: 0.271290892108674
average edge length sphere: 97712240375.7477
average edge length grid: 97712048675.4143
relative error edges: -1.96188658332493e-06
relative avg error edges: 6.49708904860269e-07
average cell area sphere: 511207892973088
average cell area grid: 511203815220690
relative error area: -7.97670077817474e-06
relative avg error area: 7.90763733658977e-06

++++++++++ MPI RANK 0 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.676 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541671 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81483 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.073 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.06 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907414 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.675 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541667 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81484 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.077 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907422 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.675 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541665 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81485 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.078 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907424 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.675 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541665 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81485 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.078 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907424 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.668 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541641 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81493 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.098 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.17 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907465 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.674 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541665 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81485 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.078 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907425 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.674 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541664 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81485 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.079 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907427 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
2874 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
39.1961 TSS (Timestep size)
155.673 RT (REAL_TIME)
282525696 CP (Cells processed)
0.0541661 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81486 MCPS (Million Cells per Second) (local)
282.526 MCP (Million Cells processed) (local)
443.081 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1883.1 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0907431 GFLOPS


++++++++++ SUMMARY ++++++++++
14.518861886748 GMCPS (Global Million Cells per Second) (global)
2260.205568 GMCP (Global Million Cells Processed) (global)
6.92313284242059 GCPSPT (Global Clusters per Second per Thread) (global)
0.786432 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
2874 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.725943094337401 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 2874
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	98304	98304	0
1	1536	98304	98304	0
2	1536	98304	98304	0
3	1536	98304	98304	0
4	1536	98304	98304	0
5	1536	98304	98304	0
6	1536	98304	98304	0
7	1536	98304	98304	0
