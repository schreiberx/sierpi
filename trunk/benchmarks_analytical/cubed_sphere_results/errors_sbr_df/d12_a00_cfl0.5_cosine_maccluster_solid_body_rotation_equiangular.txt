WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'

'


4'

'
WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter '' not handled with value '1'
visualization-boundary-method' not handled with value '1'
'
', parameter 'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
1'
visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.473309479974906
global error l2: 0.340949751090359
global error linf: 0.575480435661292
average edge length sphere: 24426600398.9397
average edge length grid: 24425833760.983
relative error edges: -3.13853726760587e-05
relative avg error edges: 1.03938059046665e-05
average cell area sphere: 511207893389747
average cell area grid: 511142644567860
relative error area: -0.000127636569644336
relative avg error area: 0.00012652553768378

++++++++++ MPI RANK 0 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4258 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642831 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955772 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.697 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1707 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477886 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.424 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642815 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955796 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.716 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1732 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477898 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4266 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642839 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.095576 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.688 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1695 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047788 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4218 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642795 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955826 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.739 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1763 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477913 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4234 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642809 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955805 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.723 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1741 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477903 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4211 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642788 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955837 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.747 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1774 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477918 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4234 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642809 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955805 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.723 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1741 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477903 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1080 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
862.72 TSS (Timestep size)
69.4231 RT (REAL_TIME)
6635520 CP (Cells processed)
0.0642806 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0955809 MCPS (Million Cells per Second) (local)
6.63552 MCP (Million Cells processed) (local)
746.726 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.1745 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00477904 GFLOPS


++++++++++ SUMMARY ++++++++++
0.764641022732899 GMCPS (Global Million Cells per Second) (global)
53.08416 GMCP (Global Million Cells Processed) (global)
11.6674960744156 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1080 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.038232051136645 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1080
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
