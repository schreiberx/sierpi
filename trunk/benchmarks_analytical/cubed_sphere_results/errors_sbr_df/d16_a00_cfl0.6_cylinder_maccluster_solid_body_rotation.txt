WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '44'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
'
WARNING: In scope 'swe



WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1visualization-boundary-method' not handled with value '1'
' not handled with value '1'
' not handled with value '1'
' not handled with value '1'

'
current timestep: 1036800
cell count: 786432
global error l1: 0.392912502555999
global error l2: 0.346667230391124
global error linf: 0.859970258432212
average edge length sphere: 96241484020.718
average edge length grid: 96241284093.5654
relative error edges: -2.07734902187983e-06
relative avg error edges: 6.49710999197756e-07
average cell area sphere: 511207894009254
average cell area grid: 511203243442894
relative error area: -9.09721155472965e-06
relative avg error area: 7.91002763904297e-06

++++++++++ MPI RANK 0 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.918 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547479 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79558 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.373 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897788 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.919 RT (REAL_TIME)
490045440 CP (Cells processed)
0.054748 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79557 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.373 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897787 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.918 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547478 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79558 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.374 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897789 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.917 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547477 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79558 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.374 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897791 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.919 RT (REAL_TIME)
490045440 CP (Cells processed)
0.054748 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79557 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.372 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897786 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.92 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547482 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79557 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.371 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897784 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.917 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547476 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79558 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.375 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897792 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
4985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
142.311 TSS (Timestep size)
272.917 RT (REAL_TIME)
490045440 CP (Cells processed)
0.0547477 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.79558 MCPS (Million Cells per Second) (local)
490.045 MCP (Million Cells processed) (local)
438.375 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1863.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0897791 GFLOPS


++++++++++ SUMMARY ++++++++++
14.3646173583376 GMCPS (Global Million Cells per Second) (global)
3920.36352 GMCP (Global Million Cells Processed) (global)
6.84958331982499 GCPSPT (Global Clusters per Second per Thread) (global)
0.786432 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
4985 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.718230867916881 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 4985
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	98304	98304	0
1	1536	98304	98304	0
2	1536	98304	98304	0
3	1536	98304	98304	0
4	1536	98304	98304	0
5	1536	98304	98304	0
6	1536	98304	98304	0
7	1536	98304	98304	0
