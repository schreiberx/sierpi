WARNING: In scope 'swe', parameter 'visualization-dof-methodWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4' not handled with value '4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'

WARNING: In scope 'swe
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1
WARNING: In scope 'sweWARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter ''
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.727796718815278
global error l2: 0.497330061359813
global error linf: 0.842652087827241
average edge length sphere: 24059502745.0983
average edge length grid: 24058703173.9771
relative error edges: -3.32330692624167e-05
relative avg error edges: 1.03943385765539e-05
average cell area sphere: 511207893411147
average cell area grid: 511133508765753
relative error area: -0.000145507622932941
relative avg error area: 0.000126520800576766

++++++++++ MPI RANK 0 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9554 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647977 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948181 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.767 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3831 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474091 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9524 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647947 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948226 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.801 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3877 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474113 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9536 RT (REAL_TIME)
6064128 CP (Cells processed)
0.064796 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948207 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.787 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3858 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474104 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9511 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647934 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948244 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.816 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3896 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474122 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9537 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647961 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948206 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.786 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3856 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474103 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9509 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647932 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948247 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.818 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3899 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474124 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9531 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647954 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0948215 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.793 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3866 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474108 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
987 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
243.464 TSS (Timestep size)
63.9521 RT (REAL_TIME)
6064128 CP (Cells processed)
0.0647944 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.094823 MCPS (Million Cells per Second) (local)
6.06413 MCP (Million Cells processed) (local)
740.804 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
98.3881 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00474115 GFLOPS


++++++++++ SUMMARY ++++++++++
0.758575654357241 GMCPS (Global Million Cells per Second) (global)
48.513024 GMCP (Global Million Cells Processed) (global)
11.5749458977851 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
987 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0379287827178621 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 987
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
