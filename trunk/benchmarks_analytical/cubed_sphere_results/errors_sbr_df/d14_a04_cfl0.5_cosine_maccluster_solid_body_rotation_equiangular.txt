WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter '
'
''visualization-boundary-method' not handled with value '1'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter '


4'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'WARNING: In scope '
1'
visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
sweWARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 423228
global error l1: 0.0802535433881776
global error l2: 0.0635928359023033
global error linf: 0.23656850223032
average edge length sphere: 60560212980.5116
average edge length grid: 60559854018.0841
relative error edges: -5.92736402055552e-06
relative avg error edges: 1.2072222423442e-06
average cell area sphere: 511207893639030
average cell area grid: 511192998166643
relative error area: -2.913779809038e-05
relative avg error area: 1.47558402806311e-05

++++++++++ MPI RANK 0 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.39 RT (REAL_TIME)
434068525 CP (Cells processed)
0.19981 ASPT (Averaged Seconds per Timestep)
0.0512478 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4575.01 ACPST (Averaged number of clusters per Simulation Timestep)
0.256482 MCPS (Million Cells per Second) (local)
434.069 MCP (Million Cells processed) (local)
357.762 CPSPT (Clusters per Second per Thread)
53.1746 EDMBPT (CellData Megabyte per Timestep (RW))
266.125 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128241 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.39 RT (REAL_TIME)
434069862 CP (Cells processed)
0.19981 ASPT (Averaged Seconds per Timestep)
0.0512479 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4315.77 ACPST (Averaged number of clusters per Simulation Timestep)
0.256483 MCPS (Million Cells per Second) (local)
434.07 MCP (Million Cells processed) (local)
337.491 CPSPT (Clusters per Second per Thread)
53.1747 EDMBPT (CellData Megabyte per Timestep (RW))
266.127 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128242 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.39 RT (REAL_TIME)
434068954 CP (Cells processed)
0.19981 ASPT (Averaged Seconds per Timestep)
0.0512478 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2108.57 ACPST (Averaged number of clusters per Simulation Timestep)
0.256483 MCPS (Million Cells per Second) (local)
434.069 MCP (Million Cells processed) (local)
164.889 CPSPT (Clusters per Second per Thread)
53.1746 EDMBPT (CellData Megabyte per Timestep (RW))
266.127 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128242 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.39 RT (REAL_TIME)
434072608 CP (Cells processed)
0.19981 ASPT (Averaged Seconds per Timestep)
0.0512482 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1895.56 ACPST (Averaged number of clusters per Simulation Timestep)
0.256485 MCPS (Million Cells per Second) (local)
434.073 MCP (Million Cells processed) (local)
148.232 CPSPT (Clusters per Second per Thread)
53.1751 EDMBPT (CellData Megabyte per Timestep (RW))
266.128 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128243 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.39 RT (REAL_TIME)
434070532 CP (Cells processed)
0.19981 ASPT (Averaged Seconds per Timestep)
0.051248 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1986.25 ACPST (Averaged number of clusters per Simulation Timestep)
0.256483 MCPS (Million Cells per Second) (local)
434.071 MCP (Million Cells processed) (local)
155.323 CPSPT (Clusters per Second per Thread)
53.1748 EDMBPT (CellData Megabyte per Timestep (RW))
266.127 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128242 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.4 RT (REAL_TIME)
434074252 CP (Cells processed)
0.199811 ASPT (Averaged Seconds per Timestep)
0.0512484 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2056.43 ACPST (Averaged number of clusters per Simulation Timestep)
0.256484 MCPS (Million Cells per Second) (local)
434.074 MCP (Million Cells processed) (local)
160.811 CPSPT (Clusters per Second per Thread)
53.1753 EDMBPT (CellData Megabyte per Timestep (RW))
266.127 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128242 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.4 RT (REAL_TIME)
434069217 CP (Cells processed)
0.199811 ASPT (Averaged Seconds per Timestep)
0.0512478 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3214.96 ACPST (Averaged number of clusters per Simulation Timestep)
0.256482 MCPS (Million Cells per Second) (local)
434.069 MCP (Million Cells processed) (local)
251.407 CPSPT (Clusters per Second per Thread)
53.1746 EDMBPT (CellData Megabyte per Timestep (RW))
266.125 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128241 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
8470 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
32.6706 TSS (Timestep size)
1692.4 RT (REAL_TIME)
434046652 CP (Cells processed)
0.199811 ASPT (Averaged Seconds per Timestep)
0.0512452 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3520.43 ACPST (Averaged number of clusters per Simulation Timestep)
0.256468 MCPS (Million Cells per Second) (local)
434.047 MCP (Million Cells processed) (local)
275.294 CPSPT (Clusters per Second per Thread)
53.1719 EDMBPT (CellData Megabyte per Timestep (RW))
266.111 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0128234 GFLOPS


++++++++++ SUMMARY ++++++++++
2.0518517375658 GMCPS (Global Million Cells per Second) (global)
3472.540602 GMCP (Global Million Cells Processed) (global)
3.61564077917006 GCPSPT (Global Clusters per Second per Thread) (global)
0.409981180873672 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
8470 GNT (Global Number of Timesteps) (global)

8485824 MSNGCells (Migration Sent Number of Global Cells)
359774 MSNGCluster (Migration Sent Number of Global Cluster)
0.00244367825301482 MRC (Migrated Relative Cells)
0.10259258687829 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 8470
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	6376	52896	52903.5	0.000141768
1	11010	52912	52903.5	-0.00016067
2	3109	52916	52903.5	-0.000236279
3	2178	52882	52903.5	0.0004064
4	1865	52901	52903.5	4.72559e-05
5	2641	52929	52903.5	-0.00048201
6	2157	52899	52903.5	8.50605e-05
7	1669	52893	52903.5	0.000198475
