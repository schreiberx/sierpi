WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
sweWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'


WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'', parameter '
WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'

visualization-dof-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '', parameter 'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
1'
WARNING: In scope 'swe', parameter '' not handled with value '1'
visualization-boundary-method' not handled with value '1'
4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 1572864
global error l1: 0.330979744863867
global error l2: 0.316611270137067
global error linf: 0.865208432225436
average edge length sphere: 136052254252.356
average edge length grid: 136052112965.802
relative error edges: -1.03847271562112e-06
relative avg error edges: 3.25267891230661e-07
average cell area sphere: 511207895837324
average cell area grid: 511205551335773
relative error area: -4.58619980325688e-06
relative avg error area: 4.04842004199768e-06

++++++++++ MPI RANK 0 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.385 RT (REAL_TIME)
996409344 CP (Cells processed)
0.081765 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40455 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.524 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.95 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120227 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.385 RT (REAL_TIME)
996409344 CP (Cells processed)
0.081765 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40455 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.524 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.95 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120227 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.383 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817647 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40456 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.525 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.96 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120228 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.385 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817649 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40455 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.524 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.96 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120228 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.384 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817648 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40456 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.525 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.96 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120228 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.387 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817654 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40454 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.523 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.94 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120227 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.374 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817629 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40461 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.532 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2495.02 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120231 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
5068 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
146.799 TSS (Timestep size)
414.387 RT (REAL_TIME)
996409344 CP (Cells processed)
0.0817653 ASPT (Averaged Seconds per Timestep)
0.196608 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
2.40454 MCPS (Million Cells per Second) (local)
996.409 MCP (Million Cells processed) (local)
293.523 CPSPT (Clusters per Second per Thread)
204 EDMBPT (CellData Megabyte per Timestep (RW))
2494.95 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.120227 GFLOPS


++++++++++ SUMMARY ++++++++++
19.2364506970432 GMCPS (Global Million Cells per Second) (global)
7971.274752 GMCP (Global Million Cells Processed) (global)
4.5863272421463 GCPSPT (Global Clusters per Second per Thread) (global)
1.572864 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
5068 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.96182253485216 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 5068
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	196608	196608	0
1	1536	196608	196608	0
2	1536	196608	196608	0
3	1536	196608	196608	0
4	1536	196608	196608	0
5	1536	196608	196608	0
6	1536	196608	196608	0
7	1536	196608	196608	0
