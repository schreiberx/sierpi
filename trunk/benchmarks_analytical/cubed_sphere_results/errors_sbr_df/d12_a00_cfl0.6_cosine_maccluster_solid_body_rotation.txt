WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'

WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter '

visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
' not handled with value '1'
1'
current timestep: 1036800
cell count: 49152
global error l1: 0.529018773661201
global error l2: 0.379234768569935
global error linf: 0.61039858371707
average edge length sphere: 24059502745.0983
average edge length grid: 24058703173.9771
relative error edges: -3.32330692630509e-05
relative avg error edges: 1.03943385765539e-05
average cell area sphere: 511207893411148
average cell area grid: 511133508765753
relative error area: -0.000145507622933919
relative avg error area: 0.000126520800576766

++++++++++ MPI RANK 0 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3619 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651759 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942679 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.468 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8122 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047134 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3581 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651728 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942725 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.504 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8169 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00471362 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3602 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651745 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.09427 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.484 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8143 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047135 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.36 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651744 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942702 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.486 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8145 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00471351 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3604 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651747 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942697 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.482 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.814 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00471348 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.359 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651735 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942714 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.495 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8158 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00471357 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3602 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651745 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.09427 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.484 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8143 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0047135 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1233 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
663.973 TSS (Timestep size)
80.3573 RT (REAL_TIME)
7575552 CP (Cells processed)
0.0651722 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0942733 MCPS (Million Cells per Second) (local)
7.57555 MCP (Million Cells processed) (local)
736.51 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.8178 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00471367 GFLOPS


++++++++++ SUMMARY ++++++++++
0.754164903743845 GMCPS (Global Million Cells per Second) (global)
60.604416 GMCP (Global Million Cells Processed) (global)
11.507643184568 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1233 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0377082451871923 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1233
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
