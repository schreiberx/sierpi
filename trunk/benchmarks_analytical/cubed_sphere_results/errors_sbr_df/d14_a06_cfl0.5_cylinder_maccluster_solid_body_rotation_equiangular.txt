WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope ''
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'sweWARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
4'
'
WARNING: In scope '
visualization-dof-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe' not handled with value '' not handled with value '1'
4'
visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
4'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 1017746
global error l1: 0.18513436524673
global error l2: 0.234363289472452
global error linf: 0.871195884872773
average edge length sphere: 72829886310.9352
average edge length grid: 72829529625.6121
relative error edges: -4.89751311049717e-06
relative avg error edges: 5.02010893406484e-07
average cell area sphere: 511207893962421
average cell area grid: 511192990243315
relative error area: -2.91539299018935e-05
relative avg error area: 2.1867688348519e-05

++++++++++ MPI RANK 0 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.08 RT (REAL_TIME)
1688495133 CP (Cells processed)
0.3416 ASPT (Averaged Seconds per Timestep)
0.099411 MCPPT (Million Cells Processed in Average per Simulation Timestep)
8131.56 ACPST (Averaged number of clusters per Simulation Timestep)
0.291016 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
371.943 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.11 RT (REAL_TIME)
1688502393 CP (Cells processed)
0.341602 ASPT (Averaged Seconds per Timestep)
0.0994114 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2636.74 ACPST (Averaged number of clusters per Simulation Timestep)
0.291015 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
120.606 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.1 RT (REAL_TIME)
1688502478 CP (Cells processed)
0.341602 ASPT (Averaged Seconds per Timestep)
0.0994114 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2375.31 ACPST (Averaged number of clusters per Simulation Timestep)
0.291016 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
108.648 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.09 RT (REAL_TIME)
1688497596 CP (Cells processed)
0.341601 ASPT (Averaged Seconds per Timestep)
0.0994111 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2388.5 ACPST (Averaged number of clusters per Simulation Timestep)
0.291015 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
109.251 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.08 RT (REAL_TIME)
1688496605 CP (Cells processed)
0.341601 ASPT (Averaged Seconds per Timestep)
0.099411 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2418.7 ACPST (Averaged number of clusters per Simulation Timestep)
0.291016 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
110.633 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.08 RT (REAL_TIME)
1688503213 CP (Cells processed)
0.3416 ASPT (Averaged Seconds per Timestep)
0.0994114 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2469.13 ACPST (Averaged number of clusters per Simulation Timestep)
0.291017 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
112.939 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.958 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.1 RT (REAL_TIME)
1688504971 CP (Cells processed)
0.341601 ASPT (Averaged Seconds per Timestep)
0.0994115 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2577.19 ACPST (Averaged number of clusters per Simulation Timestep)
0.291016 MCPS (Million Cells per Second) (local)
1688.5 MCP (Million Cells processed) (local)
117.882 CPSPT (Clusters per Second per Thread)
103.149 EDMBPT (CellData Megabyte per Timestep (RW))
301.958 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145508 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
16985 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
4.26202 TSS (Timestep size)
5802.11 RT (REAL_TIME)
1688450115 CP (Cells processed)
0.341602 ASPT (Averaged Seconds per Timestep)
0.0994083 MCPPT (Million Cells Processed in Average per Simulation Timestep)
5585.03 ACPST (Averaged number of clusters per Simulation Timestep)
0.291006 MCPS (Million Cells per Second) (local)
1688.45 MCP (Million Cells processed) (local)
255.462 CPSPT (Clusters per Second per Thread)
103.146 EDMBPT (CellData Megabyte per Timestep (RW))
301.948 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0145503 GFLOPS


++++++++++ SUMMARY ++++++++++
2.32811704126686 GMCPS (Global Million Cells per Second) (global)
13507.952504 GMCP (Global Million Cells Processed) (global)
2.55344231686709 GCPSPT (Global Clusters per Second per Thread) (global)
0.795287165381219 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
16985 GNT (Global Number of Timesteps) (global)

28816410 MSNGCells (Migration Sent Number of Global Cells)
782778 MSNGCluster (Migration Sent Number of Global Cluster)
0.00213328682326145 MRC (Migrated Relative Cells)
0.116405852063343 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 16985
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	20246	127216	127218	1.76861e-05
1	2602	127219	127218	-5.89538e-06
2	2647	127238	127218	-0.000155245
3	2131	127231	127218	-0.000100221
4	3784	127167	127218	0.000402851
5	2569	127249	127218	-0.000241711
6	2195	127212	127218	4.91282e-05
7	2039	127214	127218	3.34072e-05
