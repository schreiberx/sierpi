WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter ''
WARNING: In scope 'swe', parameter ''
WARNING: In scope 'swe', parameter '

visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'sweWARNING: In scope 'swevisualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 196608
global error l1: 0.280876492669585
global error l2: 0.210012510102603
global error linf: 0.443504311052805
average edge length sphere: 48855536280.7835
average edge length grid: 48855152896.4632
relative error edges: -7.84730553568129e-06
relative avg error edges: 2.59875878049845e-06
average cell area sphere: 511207893400564
average cell area grid: 511191580793809
relative error area: -3.19099273796435e-05
relative avg error area: 3.16333898639406e-05

++++++++++ MPI RANK 0 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.2554 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367434 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668856 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.179 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.003 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334428 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.254 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367427 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668867 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.191 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.015 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334434 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.254 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367427 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668868 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.191 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.015 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334434 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.254 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367427 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668868 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.191 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.016 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334434 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.2541 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367427 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668867 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.19 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.015 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334433 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.2529 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367422 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668877 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.2 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.025 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334438 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.2534 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367424 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668873 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.196 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.021 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334436 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
2157 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
219.529 TSS (Timestep size)
79.2543 RT (REAL_TIME)
53010432 CP (Cells processed)
0.0367428 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.668865 MCPS (Million Cells per Second) (local)
53.0104 MCP (Million Cells processed) (local)
653.189 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
694.013 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0334433 GFLOPS


++++++++++ SUMMARY ++++++++++
5.35093994618643 GMCPS (Global Million Cells per Second) (global)
424.083456 GMCP (Global Million Cells Processed) (global)
10.2061079906205 GCPSPT (Global Clusters per Second per Thread) (global)
0.196608 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
2157 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.267546997309321 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 2157
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	24576	24576	0
1	1536	24576	24576	0
2	1536	24576	24576	0
3	1536	24576	24576	0
4	1536	24576	24576	0
5	1536	24576	24576	0
6	1536	24576	24576	0
7	1536	24576	24576	0
