#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di68zip/workspace/sierpi_2014_05_07/cubed_sphere/errors11/d14_a04_cfl0.5_cylinder_maccluster_solid_body_rotation_equiangular.txt
#SBATCH -e /home/hpc/pr63so/di68zip/workspace/sierpi_2014_05_07/cubed_sphere/errors11/d14_a04_cfl0.5_cylinder_maccluster_solid_body_rotation_equiangular.err
# working directory
#SBATCH -D /home/hpc/pr63so/di68zip/workspace/sierpi_2014_05_07/cubed_sphere
# job description
#SBATCH -J d14_a04_cfl0.5_cylinder
#SBATCH --get-user-env
#SBATCH --partition=bdz
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=64
#SBATCH --mail-type=end
#SBATCH --mail-user=kleinfl@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00

cd /home/hpc/pr63so/di68zip/workspace/sierpi_2014_05_07/cubed_sphere

source /etc/profile.d/modules.sh

source ./inc_vars.sh

export KMP_AFFINITY="compact"

cd errors11

mpiexec.hydra -genv OMP_NUM_THREADS 64 -envall -ppn 1 -n 8 ../../build/maccluster_solid_body_rotation_equiangular -c d14_a04_cfl0.5_cylinder_maccluster_solid_body_rotation_equiangular.xml -A 1  -r 50000/10000 -I 12  -u 1 -U 20 -a 4 -d 14 -C 0.5
