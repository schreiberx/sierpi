WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
'
WARNING: In scope 'swe
'
WARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''
'

WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-boundary-method' not handled with value '1'
' not handled with value '1'
' not handled with value '1'
current timestep: 1036800
cell count: 393216
global error l1: 0.257093410636991
global error l2: 0.194507458568677
global error linf: 0.437829850888004
average edge length sphere: 68025883012.5669
average edge length grid: 68025600444.9426
relative error edges: -4.15382515866637e-06
relative avg error edges: 1.30105811982037e-06
average cell area sphere: 511207893298165
average cell area grid: 511198525308620
relative error area: -1.83252052006385e-05
relative avg error area: 1.60690838596938e-05

++++++++++ MPI RANK 0 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.541 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422065 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16456 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.633 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.34 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.058228 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.542 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422066 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16456 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.632 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.34 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582279 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.54 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422061 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16457 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.638 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.36 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582285 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.54 RT (REAL_TIME)
155516928 CP (Cells processed)
0.042206 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16457 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.639 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.36 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582286 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.541 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422064 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16456 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.635 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.35 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582282 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.541 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422063 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16457 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.635 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.35 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582283 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.539 RT (REAL_TIME)
155516928 CP (Cells processed)
0.0422059 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16458 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.641 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.36 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582289 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
3164 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
65.5917 TSS (Timestep size)
133.54 RT (REAL_TIME)
155516928 CP (Cells processed)
0.042206 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16457 MCPS (Million Cells per Second) (local)
155.517 MCP (Million Cells processed) (local)
568.639 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1208.36 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0582287 GFLOPS


++++++++++ SUMMARY ++++++++++
9.31654018457071 GMCPS (Global Million Cells per Second) (global)
1244.135424 GMCP (Global Million Cells Processed) (global)
8.88494509179183 GCPSPT (Global Clusters per Second per Thread) (global)
0.393216 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
3164 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.465827009228536 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 3164
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	49152	49152	0
1	1536	49152	49152	0
2	1536	49152	49152	0
3	1536	49152	49152	0
4	1536	49152	49152	0
5	1536	49152	49152	0
6	1536	49152	49152	0
7	1536	49152	49152	0
