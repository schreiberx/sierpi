WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''

'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope '
WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4sweWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '', parameter 'visualization-boundary-method' not handled with value '1'

visualization-boundary-method' not handled with value '1'

WARNING: In scope 'swe', parameter ''
', parameter '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-dof-methodvisualization-boundary-method' not handled with value '1'
' not handled with value '1'
' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 5
cell count: 786432
global error l1: 0.296396238900862
global error l2: 0.413480001982977
global error linf: 0.900970151604312
average edge length sphere: 96241484020.7176
average edge length grid: 96241284093.5656
relative error edges: -2.07734901474523e-06
relative avg error edges: 6.49710999197756e-07
average cell area sphere: 511207894009253
average cell area grid: 511203243442894
relative error area: -9.09721155228449e-06
relative avg error area: 7.91002763904297e-06

++++++++++ MPI RANK 0 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.371 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.054284 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81092 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.119 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1879.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.090546 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.375 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542844 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81091 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.116 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1878.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905453 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.375 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542844 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81091 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.116 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1878.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905454 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.376 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542845 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81091 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.116 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1878.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905453 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.376 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542845 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.8109 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.115 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1878.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905452 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.379 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542847 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.8109 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.113 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1878.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905448 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.371 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.054284 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81092 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.119 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1879.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.090546 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
674.366 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0542837 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.81093 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
442.122 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1879.02 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0905466 GFLOPS


++++++++++ SUMMARY ++++++++++
14.4872902552105 GMCPS (Global Million Cells per Second) (global)
9769.844736 GMCP (Global Million Cells Processed) (global)
6.90807831535842 GCPSPT (Global Clusters per Second per Thread) (global)
0.786432 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
12423 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.724364512760527 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 12423
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	98304	98304	0
1	1536	98304	98304	0
2	1536	98304	98304	0
3	1536	98304	98304	0
4	1536	98304	98304	0
5	1536	98304	98304	0
6	1536	98304	98304	0
7	1536	98304	98304	0
