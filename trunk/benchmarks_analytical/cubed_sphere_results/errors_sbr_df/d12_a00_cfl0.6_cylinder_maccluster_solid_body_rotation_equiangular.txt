WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'

WARNING: In scope '



4'
swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
1'
'
' not handled with value '1'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.67510511257346
global error l2: 0.473278753264166
global error linf: 0.844360840462207
average edge length sphere: 24426600398.9397
average edge length grid: 24425833760.983
relative error edges: -3.13853726751217e-05
relative avg error edges: 1.03938059046665e-05
average cell area sphere: 511207893389747
average cell area grid: 511142644567860
relative error area: -0.000127636569644703
relative avg error area: 0.00012652553768378

++++++++++ MPI RANK 0 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.8018 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642242 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956649 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.382 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2617 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478324 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.8004 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642227 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956671 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.399 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.264 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478336 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.8006 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642228 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956669 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.398 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2637 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478334 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.7976 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642196 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956717 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.435 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2688 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478359 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.7989 RT (REAL_TIME)
5529600 CP (Cells processed)
0.064221 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956696 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.419 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2665 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478348 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.8002 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642224 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956676 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.403 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2644 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478338 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.7983 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642203 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956707 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.427 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2677 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478353 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
900 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1054.74 TSS (Timestep size)
57.8009 RT (REAL_TIME)
5529600 CP (Cells processed)
0.0642232 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0956664 MCPS (Million Cells per Second) (local)
5.5296 MCP (Million Cells processed) (local)
747.394 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
99.2632 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00478332 GFLOPS


++++++++++ SUMMARY ++++++++++
0.765344831828438 GMCPS (Global Million Cells per Second) (global)
44.2368 GMCP (Global Million Cells Processed) (global)
11.6782353489447 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
900 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0382672415914219 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 900
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
