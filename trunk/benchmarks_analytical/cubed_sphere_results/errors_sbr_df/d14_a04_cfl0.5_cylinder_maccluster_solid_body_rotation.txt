WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'visualization-boundary-method' not handled with value '1'


WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method
WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
WARNING: In scope 'swe', parameter '' not handled with value '1'

visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope '4swe'', parameter 'visualization-dof-method' not handled with value '
4'WARNING: In scope '
WARNING: In scope 'sweswe', parameter '', parameter 'visualization-boundary-methodvisualization-boundary-method' not handled with value '' not handled with value '1'1
'
current timestep: 1036800
cell count: 459526
global error l1: 0.282865048363329
global error l2: 0.291492525592925
global error linf: 0.867123512306764
average edge length sphere: 62649955271.3903
average edge length grid: 62649591747.9267
relative error edges: -5.80245368097297e-06
relative avg error edges: 1.11185091600436e-06
average cell area sphere: 511207893289462
average cell area grid: 511191614062221
relative error area: -3.18446320073636e-05
relative avg error area: 1.3628247546207e-05

++++++++++ MPI RANK 0 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557618094 CP (Cells processed)
0.188637 ASPT (Averaged Seconds per Timestep)
0.0514883 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4566.08 ACPST (Averaged number of clusters per Simulation Timestep)
0.272949 MCPS (Million Cells per Second) (local)
557.618 MCP (Million Cells processed) (local)
378.213 CPSPT (Clusters per Second per Thread)
53.4241 EDMBPT (CellData Megabyte per Timestep (RW))
283.211 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136474 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557619373 CP (Cells processed)
0.188637 ASPT (Averaged Seconds per Timestep)
0.0514884 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4353.88 ACPST (Averaged number of clusters per Simulation Timestep)
0.27295 MCPS (Million Cells per Second) (local)
557.619 MCP (Million Cells processed) (local)
360.636 CPSPT (Clusters per Second per Thread)
53.4242 EDMBPT (CellData Megabyte per Timestep (RW))
283.212 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557620686 CP (Cells processed)
0.188637 ASPT (Averaged Seconds per Timestep)
0.0514885 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2201.03 ACPST (Averaged number of clusters per Simulation Timestep)
0.27295 MCPS (Million Cells per Second) (local)
557.621 MCP (Million Cells processed) (local)
182.314 CPSPT (Clusters per Second per Thread)
53.4244 EDMBPT (CellData Megabyte per Timestep (RW))
283.213 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557620005 CP (Cells processed)
0.188637 ASPT (Averaged Seconds per Timestep)
0.0514885 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2110.46 ACPST (Averaged number of clusters per Simulation Timestep)
0.27295 MCPS (Million Cells per Second) (local)
557.62 MCP (Million Cells processed) (local)
174.811 CPSPT (Clusters per Second per Thread)
53.4243 EDMBPT (CellData Megabyte per Timestep (RW))
283.212 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557620419 CP (Cells processed)
0.188637 ASPT (Averaged Seconds per Timestep)
0.0514885 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2067.52 ACPST (Averaged number of clusters per Simulation Timestep)
0.27295 MCPS (Million Cells per Second) (local)
557.62 MCP (Million Cells processed) (local)
171.255 CPSPT (Clusters per Second per Thread)
53.4243 EDMBPT (CellData Megabyte per Timestep (RW))
283.212 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.95 RT (REAL_TIME)
557623334 CP (Cells processed)
0.188638 ASPT (Averaged Seconds per Timestep)
0.0514888 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2143.41 ACPST (Averaged number of clusters per Simulation Timestep)
0.27295 MCPS (Million Cells per Second) (local)
557.623 MCP (Million Cells processed) (local)
177.54 CPSPT (Clusters per Second per Thread)
53.4246 EDMBPT (CellData Megabyte per Timestep (RW))
283.212 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.94 RT (REAL_TIME)
557620185 CP (Cells processed)
0.188638 ASPT (Averaged Seconds per Timestep)
0.0514885 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2715.01 ACPST (Averaged number of clusters per Simulation Timestep)
0.272949 MCPS (Million Cells per Second) (local)
557.62 MCP (Million Cells processed) (local)
224.886 CPSPT (Clusters per Second per Thread)
53.4243 EDMBPT (CellData Megabyte per Timestep (RW))
283.211 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136475 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
10830 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1.52013 TSS (Timestep size)
2042.95 RT (REAL_TIME)
557587986 CP (Cells processed)
0.188638 ASPT (Averaged Seconds per Timestep)
0.0514855 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2978.89 ACPST (Averaged number of clusters per Simulation Timestep)
0.272933 MCPS (Million Cells per Second) (local)
557.588 MCP (Million Cells processed) (local)
246.743 CPSPT (Clusters per Second per Thread)
53.4212 EDMBPT (CellData Megabyte per Timestep (RW))
283.194 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0136466 GFLOPS


++++++++++ SUMMARY ++++++++++
2.18358027588185 GMCPS (Global Million Cells per Second) (global)
4460.930082 GMCP (Global Million Cells Processed) (global)
3.74296508071759 GCPSPT (Global Clusters per Second per Thread) (global)
0.411904901385042 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
10830 GNT (Global Number of Timesteps) (global)

8563804 MSNGCells (Migration Sent Number of Global Cells)
398522 MSNGCluster (Migration Sent Number of Global Cluster)
0.00191972529613812 MRC (Migrated Relative Cells)
0.109179013794092 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 10830
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	6414	57440	57440.8	1.30569e-05
1	12171	57440	57440.8	1.30569e-05
2	2284	57456	57440.8	-0.000265491
3	2305	57441	57440.8	-4.35231e-06
4	2327	57418	57440.8	0.00039606
5	2302	57465	57440.8	-0.000422174
6	2083	57438	57440.8	4.78754e-05
7	1813	57428	57440.8	0.000221968
