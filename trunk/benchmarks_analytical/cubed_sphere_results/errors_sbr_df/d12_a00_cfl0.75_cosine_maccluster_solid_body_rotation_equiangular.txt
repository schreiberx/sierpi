WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter ''
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '', parameter 'visualization-boundary-method' not handled with value '1'
1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '' not handled with value '1'
1'
1'
visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 49152
global error l1: 0.347793702347322
global error l2: 0.252957712845514
global error linf: 0.498069654346168
average edge length sphere: 24426600398.9397
average edge length grid: 24425833760.9831
relative error edges: -3.138537267356e-05
relative avg error edges: 1.03938059046665e-05
average cell area sphere: 511207893389747
average cell area grid: 511142644567860
relative error area: -0.000127636569643481
relative avg error area: 0.00012652553768378

++++++++++ MPI RANK 0 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.159 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654986 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938036 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.84 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3304 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469018 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1582 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654974 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938052 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.853 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3321 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469026 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1578 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654969 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.093806 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.859 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3329 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0046903 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1556 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654939 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938102 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.892 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3373 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469051 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1572 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654962 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.093807 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.867 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3339 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469035 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1544 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654923 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938126 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.911 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3397 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469063 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1559 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654943 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938096 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.888 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3367 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469048 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
720 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
1342.76 TSS (Timestep size)
47.1554 RT (REAL_TIME)
4423680 CP (Cells processed)
0.0654936 ASPT (Averaged Seconds per Timestep)
0.006144 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3072 ACPST (Averaged number of clusters per Simulation Timestep)
0.0938107 MCPS (Million Cells per Second) (local)
4.42368 MCP (Million Cells processed) (local)
732.896 CPSPT (Clusters per Second per Thread)
6.375 EDMBPT (CellData Megabyte per Timestep (RW))
97.3377 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.00469053 GFLOPS


++++++++++ SUMMARY ++++++++++
0.750464904948145 GMCPS (Global Million Cells per Second) (global)
35.38944 GMCP (Global Million Cells Processed) (global)
11.4511856834129 GCPSPT (Global Clusters per Second per Thread) (global)
0.049152 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
720 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.0375232452474072 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 720
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3072	6144	6144	0
1	3072	6144	6144	0
2	3072	6144	6144	0
3	3072	6144	6144	0
4	3072	6144	6144	0
5	3072	6144	6144	0
6	3072	6144	6144	0
7	3072	6144	6144	0
