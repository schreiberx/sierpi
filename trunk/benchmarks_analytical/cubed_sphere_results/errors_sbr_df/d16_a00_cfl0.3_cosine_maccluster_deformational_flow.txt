WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope ''
'
1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '

WARNING: In scope 'swe
1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
'
current timestep: 5
cell count: 786432
global error l1: 0.0900355679100254
global error l2: 0.198917314021975
global error linf: 0.503451638711324
average edge length sphere: 96241484020.7177
average edge length grid: 96241284093.5657
relative error edges: -2.07734901442814e-06
relative avg error edges: 6.49710999197756e-07
average cell area sphere: 511207894009255
average cell area grid: 511203243442893
relative error area: -9.09721155741935e-06
relative avg error area: 7.91002763904297e-06

++++++++++ MPI RANK 0 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.343 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543623 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80831 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.482 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.3 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904156 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.345 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543625 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80831 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.481 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.29 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904153 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.333 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543615 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80834 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.489 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.33 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904169 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.343 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543623 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80831 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.483 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.3 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904156 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.342 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543622 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80831 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.483 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.3 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904157 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.34 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.054362 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80832 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.485 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.31 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.090416 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.334 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543616 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80834 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.489 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.33 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904168 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
12423 TS (Timesteps)
5 ST (SIMULATION_TIME)
3.03738e-05 TSS (Timestep size)
675.334 RT (REAL_TIME)
1221230592 CP (Cells processed)
0.0543616 ASPT (Averaged Seconds per Timestep)
0.098304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.80834 MCPS (Million Cells per Second) (local)
1221.23 MCP (Million Cells processed) (local)
441.488 CPSPT (Clusters per Second per Thread)
102 EDMBPT (CellData Megabyte per Timestep (RW))
1876.33 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0904168 GFLOPS


++++++++++ SUMMARY ++++++++++
14.4665768655595 GMCPS (Global Million Cells per Second) (global)
9769.844736 GMCP (Global Million Cells Processed) (global)
6.89820140150046 GCPSPT (Global Clusters per Second per Thread) (global)
0.786432 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
12423 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.723328843277974 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 12423
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	98304	98304	0
1	1536	98304	98304	0
2	1536	98304	98304	0
3	1536	98304	98304	0
4	1536	98304	98304	0
5	1536	98304	98304	0
6	1536	98304	98304	0
7	1536	98304	98304	0
