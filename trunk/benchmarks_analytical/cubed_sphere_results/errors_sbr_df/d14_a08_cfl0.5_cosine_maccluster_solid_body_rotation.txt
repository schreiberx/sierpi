WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4swe
WARNING: In scope 'swe', parameter '
'
'

'
', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swevisualization-dof-methodvisualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 2112312
global error l1: 0.0233566490037064
global error l2: 0.0196955881184764
global error linf: 0.141366073215159
average edge length sphere: 84090921104.1774
average edge length grid: 84090550204.5082
relative error edges: -4.4106981384435e-06
relative avg error edges: 2.41883943012135e-07
average cell area sphere: 511207897859723
average cell area grid: 511191033329612
relative error area: -3.29895726981514e-05
relative avg error area: 0.000134493792347111

++++++++++ MPI RANK 0 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.3 RT (REAL_TIME)
12644737705 CP (Cells processed)
0.467318 ASPT (Averaged Seconds per Timestep)
0.328717 MCPPT (Million Cells Processed in Average per Simulation Timestep)
8080.44 ACPST (Averaged number of clusters per Simulation Timestep)
0.70341 MCPS (Million Cells per Second) (local)
12644.7 MCP (Million Cells processed) (local)
270.173 CPSPT (Clusters per Second per Thread)
341.075 EDMBPT (CellData Megabyte per Timestep (RW))
729.857 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351705 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.3 RT (REAL_TIME)
12644771673 CP (Cells processed)
0.467318 ASPT (Averaged Seconds per Timestep)
0.328717 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2256.21 ACPST (Averaged number of clusters per Simulation Timestep)
0.703412 MCPS (Million Cells per Second) (local)
12644.8 MCP (Million Cells processed) (local)
75.4374 CPSPT (Clusters per Second per Thread)
341.076 EDMBPT (CellData Megabyte per Timestep (RW))
729.859 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351706 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.3 RT (REAL_TIME)
12644784426 CP (Cells processed)
0.467318 ASPT (Averaged Seconds per Timestep)
0.328718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2219.77 ACPST (Averaged number of clusters per Simulation Timestep)
0.703413 MCPS (Million Cells per Second) (local)
12644.8 MCP (Million Cells processed) (local)
74.2192 CPSPT (Clusters per Second per Thread)
341.077 EDMBPT (CellData Megabyte per Timestep (RW))
729.86 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351707 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.3 RT (REAL_TIME)
12644748718 CP (Cells processed)
0.467318 ASPT (Averaged Seconds per Timestep)
0.328717 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2238.83 ACPST (Averaged number of clusters per Simulation Timestep)
0.703411 MCPS (Million Cells per Second) (local)
12644.7 MCP (Million Cells processed) (local)
74.8564 CPSPT (Clusters per Second per Thread)
341.076 EDMBPT (CellData Megabyte per Timestep (RW))
729.857 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351705 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.3 RT (REAL_TIME)
12644788402 CP (Cells processed)
0.467318 ASPT (Averaged Seconds per Timestep)
0.328718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2232.34 ACPST (Averaged number of clusters per Simulation Timestep)
0.703413 MCPS (Million Cells per Second) (local)
12644.8 MCP (Million Cells processed) (local)
74.6392 CPSPT (Clusters per Second per Thread)
341.077 EDMBPT (CellData Megabyte per Timestep (RW))
729.86 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351707 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.5 RT (REAL_TIME)
12644757931 CP (Cells processed)
0.467322 ASPT (Averaged Seconds per Timestep)
0.328717 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2141.01 ACPST (Averaged number of clusters per Simulation Timestep)
0.703406 MCPS (Million Cells per Second) (local)
12644.8 MCP (Million Cells processed) (local)
71.5852 CPSPT (Clusters per Second per Thread)
341.076 EDMBPT (CellData Megabyte per Timestep (RW))
729.853 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351703 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.4 RT (REAL_TIME)
12644749011 CP (Cells processed)
0.46732 ASPT (Averaged Seconds per Timestep)
0.328717 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2175.69 ACPST (Averaged number of clusters per Simulation Timestep)
0.703409 MCPS (Million Cells per Second) (local)
12644.7 MCP (Million Cells processed) (local)
72.7451 CPSPT (Clusters per Second per Thread)
341.076 EDMBPT (CellData Megabyte per Timestep (RW))
729.856 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351705 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
38467 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
27.2913 TSS (Timestep size)
17976.4 RT (REAL_TIME)
12644680212 CP (Cells processed)
0.467321 ASPT (Averaged Seconds per Timestep)
0.328715 MCPPT (Million Cells Processed in Average per Simulation Timestep)
6773.47 ACPST (Averaged number of clusters per Simulation Timestep)
0.703403 MCPS (Million Cells per Second) (local)
12644.7 MCP (Million Cells processed) (local)
226.473 CPSPT (Clusters per Second per Thread)
341.074 EDMBPT (CellData Megabyte per Timestep (RW))
729.85 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0351702 GFLOPS


++++++++++ SUMMARY ++++++++++
5.62727824337909 GMCPS (Global Million Cells per Second) (global)
101158.018078 GMCP (Global Million Cells Processed) (global)
1.8361887459251 GCPSPT (Global Clusters per Second per Thread) (global)
2.62973504765123 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
38467 GNT (Global Number of Timesteps) (global)

124543406 MSNGCells (Migration Sent Number of Global Cells)
939424 MSNGCluster (Migration Sent Number of Global Cluster)
0.00123117616673 MRC (Migrated Relative Cells)
0.281363912168954 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 38467
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	17958	264091	264039	-0.000196941
1	2055	264045	264039	-2.27239e-05
2	2865	263986	264039	0.000200728
3	2064	263986	264039	0.000200728
4	4046	264073	264039	-0.000128769
5	2054	264075	264039	-0.000136343
6	2802	263960	264039	0.000299198
7	2272	264096	264039	-0.000215877
