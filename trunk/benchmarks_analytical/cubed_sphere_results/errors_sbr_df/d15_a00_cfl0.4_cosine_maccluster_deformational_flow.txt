WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter '
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter ''
WARNING: In scope 'swe
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe'
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter '', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe
', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter '' not handled with value '1'
visualization-boundary-method' not handled with value '1'
current timestep: 5
cell count: 393216
global error l1: 0.110230981922029
global error l2: 0.239791640066938
global error linf: 0.555925287661982
average edge length sphere: 68025883012.5669
average edge length grid: 68025600444.9426
relative error edges: -4.15382515821775e-06
relative avg error edges: 1.30105811982037e-06
average cell area sphere: 511207893298166
average cell area grid: 511198525308620
relative error area: -1.83252052013721e-05
relative avg error area: 1.60690838596938e-05

++++++++++ MPI RANK 0 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.065 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421145 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.875 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583553 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.065 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421145 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.1671 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.875 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583551 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.064 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421143 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.878 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583555 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.064 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421142 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.879 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583556 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.063 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421142 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.88 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583557 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.061 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421138 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16712 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.884 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1211 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583561 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.065 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421144 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.876 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583553 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
5914 TS (Timesteps)
5 ST (SIMULATION_TIME)
0.000208502 TSS (Timestep size)
249.064 RT (REAL_TIME)
290684928 CP (Cells processed)
0.0421142 ASPT (Averaged Seconds per Timestep)
0.049152 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
1.16711 MCPS (Million Cells per Second) (local)
290.685 MCP (Million Cells processed) (local)
569.878 CPSPT (Clusters per Second per Thread)
51 EDMBPT (CellData Megabyte per Timestep (RW))
1210.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0583556 GFLOPS


++++++++++ SUMMARY ++++++++++
9.33688206937087 GMCPS (Global Million Cells per Second) (global)
2325.479424 GMCP (Global Million Cells Processed) (global)
8.90434462487304 GCPSPT (Global Clusters per Second per Thread) (global)
0.393216 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
5914 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.466844103468544 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 5914
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	49152	49152	0
1	1536	49152	49152	0
2	1536	49152	49152	0
3	1536	49152	49152	0
4	1536	49152	49152	0
5	1536	49152	49152	0
6	1536	49152	49152	0
7	1536	49152	49152	0
