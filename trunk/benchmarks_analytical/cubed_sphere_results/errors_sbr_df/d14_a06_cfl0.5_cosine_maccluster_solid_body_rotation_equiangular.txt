WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'

WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
', parameter 'visualization-boundary-method' not handled with value '1'
'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
', parameter 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value ''
' not handled with value '1'
visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'
4'WARNING: In scope 'swe', parameter '
visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 968090
global error l1: 0.040026131435289
global error l2: 0.0327379576436052
global error linf: 0.172854234809554
average edge length sphere: 71295600538.2703
average edge length grid: 71295241312.4407
relative error edges: -5.03854132503276e-06
relative avg error edges: 5.27776558858351e-07
average cell area sphere: 511207896123933
average cell area grid: 511192861720757
relative error area: -2.94095675945754e-05
relative avg error area: 2.09987365575248e-05

++++++++++ MPI RANK 0 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.41 RT (REAL_TIME)
2049377774 CP (Cells processed)
0.373412 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
8302.14 ACPST (Averaged number of clusters per Simulation Timestep)
0.326157 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
347.393 CPSPT (Clusters per Second per Thread)
126.37 EDMBPT (CellData Megabyte per Timestep (RW))
338.42 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163079 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.44 RT (REAL_TIME)
2049382891 CP (Cells processed)
0.373414 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2543.86 ACPST (Averaged number of clusters per Simulation Timestep)
0.326156 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
106.444 CPSPT (Clusters per Second per Thread)
126.37 EDMBPT (CellData Megabyte per Timestep (RW))
338.419 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163078 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.43 RT (REAL_TIME)
2049382739 CP (Cells processed)
0.373414 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2325.37 ACPST (Averaged number of clusters per Simulation Timestep)
0.326157 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
97.3022 CPSPT (Clusters per Second per Thread)
126.37 EDMBPT (CellData Megabyte per Timestep (RW))
338.419 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163078 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.42 RT (REAL_TIME)
2049384579 CP (Cells processed)
0.373413 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2320.06 ACPST (Averaged number of clusters per Simulation Timestep)
0.326157 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
97.0799 CPSPT (Clusters per Second per Thread)
126.371 EDMBPT (CellData Megabyte per Timestep (RW))
338.42 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163079 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.41 RT (REAL_TIME)
2049379317 CP (Cells processed)
0.373412 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2269.04 ACPST (Averaged number of clusters per Simulation Timestep)
0.326157 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
94.9452 CPSPT (Clusters per Second per Thread)
126.37 EDMBPT (CellData Megabyte per Timestep (RW))
338.42 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163079 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.41 RT (REAL_TIME)
2049389151 CP (Cells processed)
0.373412 ASPT (Averaged Seconds per Timestep)
0.121792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2276.51 ACPST (Averaged number of clusters per Simulation Timestep)
0.326159 MCPS (Million Cells per Second) (local)
2049.39 MCP (Million Cells processed) (local)
95.2581 CPSPT (Clusters per Second per Thread)
126.371 EDMBPT (CellData Megabyte per Timestep (RW))
338.422 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163079 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.43 RT (REAL_TIME)
2049377180 CP (Cells processed)
0.373413 ASPT (Averaged Seconds per Timestep)
0.121791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2522.58 ACPST (Averaged number of clusters per Simulation Timestep)
0.326156 MCPS (Million Cells per Second) (local)
2049.38 MCP (Million Cells processed) (local)
105.554 CPSPT (Clusters per Second per Thread)
126.37 EDMBPT (CellData Megabyte per Timestep (RW))
338.419 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163078 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
16827 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
14.6831 TSS (Timestep size)
6283.44 RT (REAL_TIME)
2049335521 CP (Cells processed)
0.373414 ASPT (Averaged Seconds per Timestep)
0.121789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
6654.45 ACPST (Averaged number of clusters per Simulation Timestep)
0.326149 MCPS (Million Cells per Second) (local)
2049.34 MCP (Million Cells processed) (local)
278.447 CPSPT (Clusters per Second per Thread)
126.367 EDMBPT (CellData Megabyte per Timestep (RW))
338.411 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0163074 GFLOPS


++++++++++ SUMMARY ++++++++++
2.60924831184344 GMCPS (Global Million Cells per Second) (global)
16395.009152 GMCP (Global Million Cells Processed) (global)
2.38754667850626 GCPSPT (Global Clusters per Second per Thread) (global)
0.974327518393059 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
16827 GNT (Global Number of Timesteps) (global)

36922532 MSNGCells (Migration Sent Number of Global Cells)
775996 MSNGCluster (Migration Sent Number of Global Cluster)
0.0022520544000655 MRC (Migrated Relative Cells)
0.130462415592172 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 16827
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	18416	121024	121011	-0.000105362
1	2833	120989	121011	0.000183867
2	2579	121028	121011	-0.000138417
3	1891	120992	121011	0.000159076
4	3743	121057	121011	-0.000378064
5	1896	120981	121011	0.000249977
6	2591	120998	121011	0.000109494
7	1994	121021	121011	-8.0571e-05
