WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4''

WARNING: In scope 'sweWARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
4''WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '', parameter 'visualization-boundary-method' not handled with value '1'


4''
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1

WARNING: In scope 'swe', parameter ''
'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-boundary-method' not handled with value '1'
WARNING: In scope 'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 488988
global error l1: 0.260725587732149
global error l2: 0.279229928798977
global error linf: 0.869321785535632
average edge length sphere: 63789479015.6032
average edge length grid: 63789126394.4475
relative error edges: -5.52788894373514e-06
relative avg error edges: 1.04483890391553e-06
average cell area sphere: 511207893652595
average cell area grid: 511193325938615
relative error area: -2.8496653045913e-05
relative avg error area: 1.28245098237182e-05

++++++++++ MPI RANK 0 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.22 RT (REAL_TIME)
439103334 CP (Cells processed)
0.202039 ASPT (Averaged Seconds per Timestep)
0.0513932 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4887 ACPST (Averaged number of clusters per Simulation Timestep)
0.254372 MCPS (Million Cells per Second) (local)
439.103 MCP (Million Cells processed) (local)
377.943 CPSPT (Clusters per Second per Thread)
53.3254 EDMBPT (CellData Megabyte per Timestep (RW))
263.936 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127186 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.22 RT (REAL_TIME)
439102297 CP (Cells processed)
0.202039 ASPT (Averaged Seconds per Timestep)
0.0513931 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4436.62 ACPST (Averaged number of clusters per Simulation Timestep)
0.254372 MCPS (Million Cells per Second) (local)
439.102 MCP (Million Cells processed) (local)
343.113 CPSPT (Clusters per Second per Thread)
53.3253 EDMBPT (CellData Megabyte per Timestep (RW))
263.936 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127186 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.22 RT (REAL_TIME)
439104113 CP (Cells processed)
0.202039 ASPT (Averaged Seconds per Timestep)
0.0513933 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2326.99 ACPST (Averaged number of clusters per Simulation Timestep)
0.254373 MCPS (Million Cells per Second) (local)
439.104 MCP (Million Cells processed) (local)
179.962 CPSPT (Clusters per Second per Thread)
53.3255 EDMBPT (CellData Megabyte per Timestep (RW))
263.937 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127187 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.23 RT (REAL_TIME)
439103518 CP (Cells processed)
0.20204 ASPT (Averaged Seconds per Timestep)
0.0513932 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2186.67 ACPST (Averaged number of clusters per Simulation Timestep)
0.254372 MCPS (Million Cells per Second) (local)
439.104 MCP (Million Cells processed) (local)
169.109 CPSPT (Clusters per Second per Thread)
53.3255 EDMBPT (CellData Megabyte per Timestep (RW))
263.936 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127186 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.22 RT (REAL_TIME)
439105122 CP (Cells processed)
0.202039 ASPT (Averaged Seconds per Timestep)
0.0513934 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2166.19 ACPST (Averaged number of clusters per Simulation Timestep)
0.254373 MCPS (Million Cells per Second) (local)
439.105 MCP (Million Cells processed) (local)
167.526 CPSPT (Clusters per Second per Thread)
53.3257 EDMBPT (CellData Megabyte per Timestep (RW))
263.937 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127187 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.24 RT (REAL_TIME)
439104777 CP (Cells processed)
0.202041 ASPT (Averaged Seconds per Timestep)
0.0513933 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2286.16 ACPST (Averaged number of clusters per Simulation Timestep)
0.254371 MCPS (Million Cells per Second) (local)
439.105 MCP (Million Cells processed) (local)
176.802 CPSPT (Clusters per Second per Thread)
53.3256 EDMBPT (CellData Megabyte per Timestep (RW))
263.935 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127186 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.23 RT (REAL_TIME)
439103870 CP (Cells processed)
0.20204 ASPT (Averaged Seconds per Timestep)
0.0513932 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3163.79 ACPST (Averaged number of clusters per Simulation Timestep)
0.254372 MCPS (Million Cells per Second) (local)
439.104 MCP (Million Cells processed) (local)
244.676 CPSPT (Clusters per Second per Thread)
53.3255 EDMBPT (CellData Megabyte per Timestep (RW))
263.936 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127186 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
8544 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
54.4989 TSS (Timestep size)
1726.24 RT (REAL_TIME)
439078953 CP (Cells processed)
0.202041 ASPT (Averaged Seconds per Timestep)
0.0513903 MCPPT (Million Cells Processed in Average per Simulation Timestep)
3252 ACPST (Averaged number of clusters per Simulation Timestep)
0.254356 MCPS (Million Cells per Second) (local)
439.079 MCP (Million Cells processed) (local)
251.497 CPSPT (Clusters per Second per Thread)
53.3225 EDMBPT (CellData Megabyte per Timestep (RW))
263.919 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0127178 GFLOPS


++++++++++ SUMMARY ++++++++++
2.03496192974239 GMCPS (Global Million Cells per Second) (global)
3512.805984 GMCP (Global Million Cells Processed) (global)
3.73169338635605 GCPSPT (Global Clusters per Second per Thread) (global)
0.41114302247191 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
8544 GNT (Global Number of Timesteps) (global)

8752302 MSNGCells (Migration Sent Number of Global Cells)
430622 MSNGCluster (Migration Sent Number of Global Cluster)
0.0024915295688568 MRC (Migrated Relative Cells)
0.10174809648712 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 8544
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	7937	61124	61123.5	-8.18016e-06
1	11996	61116	61123.5	0.000122702
2	2663	61124	61123.5	-8.18016e-06
3	2376	61139	61123.5	-0.000253585
4	2488	61113	61123.5	0.000171783
5	2605	61117	61123.5	0.000106342
6	2187	61125	61123.5	-2.45405e-05
7	1933	61130	61123.5	-0.000106342
