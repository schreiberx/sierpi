WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter '
'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1''

WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'visualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value 'WARNING: In scope 'swe', parameter '
WARNING: In scope 'WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
1'
visualization-boundary-method' not handled with value '1'
swe', parameter 'visualization-boundary-method' not handled with value '1'
1'
swe', parameter 'visualization-dof-method' not handled with value '4'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 196608
global error l1: 0.444794117003101
global error l2: 0.370161385035008
global error linf: 0.859238133089013
average edge length sphere: 48855536280.7836
average edge length grid: 48855152896.4633
relative error edges: -7.84730553458814e-06
relative avg error edges: 2.59875878049845e-06
average cell area sphere: 511207893400564
average cell area grid: 511191580793808
relative error area: -3.1909927383678e-05
relative avg error area: 3.16333898639406e-05

++++++++++ MPI RANK 0 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0599 RT (REAL_TIME)
35340288 CP (Cells processed)
0.036203 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678838 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.928 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.361 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339419 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0593 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362026 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678847 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.936 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.37 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339423 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0588 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362022 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678854 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.943 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.377 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339427 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0592 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362025 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678849 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.938 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.372 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339424 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0589 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362023 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678852 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.941 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.375 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339426 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0583 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362019 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.67886 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.949 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.383 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.033943 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0617 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362043 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678815 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.905 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.337 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339407 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
1438 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
459.924 TSS (Timestep size)
52.0607 RT (REAL_TIME)
35340288 CP (Cells processed)
0.0362035 ASPT (Averaged Seconds per Timestep)
0.024576 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1536 ACPST (Averaged number of clusters per Simulation Timestep)
0.678829 MCPS (Million Cells per Second) (local)
35.3403 MCP (Million Cells processed) (local)
662.919 CPSPT (Clusters per Second per Thread)
25.5 EDMBPT (CellData Megabyte per Timestep (RW))
704.351 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0339414 GFLOPS


++++++++++ SUMMARY ++++++++++
5.43074302431343 GMCPS (Global Million Cells per Second) (global)
282.722304 GMCP (Global Million Cells Processed) (global)
10.3583202825802 GCPSPT (Global Clusters per Second per Thread) (global)
0.196608 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
1438 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.271537151215671 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 1438
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1536	24576	24576	0
1	1536	24576	24576	0
2	1536	24576	24576	0
3	1536	24576	24576	0
4	1536	24576	24576	0
5	1536	24576	24576	0
6	1536	24576	24576	0
7	1536	24576	24576	0
