WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'WARNING: In scope '4'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4sweWARNING: In scope 'swe', parameter 'visualization-boundary-method'

'
'
', parameter '' not handled with value '1'
WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter 'visualization-boundary-methodWARNING: In scope 'swe', parameter 'visualization-boundary-methodvisualization-dof-methodvisualization-boundary-method' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value 'visualization-boundary-method' not handled with value '1'
' not handled with value '1'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '4'' not handled with value '1'
' not handled with value '4'

4WARNING: In scope 'swe', parameter 'WARNING: In scope 'swe', parameter ''visualization-boundary-method' not handled with value '1'
visualization-boundary-method' not handled with value '1'

WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '1'
current timestep: 1036800
cell count: 909772
global error l1: 0.199761080937548
global error l2: 0.243661873971274
global error linf: 0.86994465458804
average edge length sphere: 71247517584.6032
average edge length grid: 71247150329.89
relative error edges: -5.15463170648614e-06
relative avg error edges: 5.61617749936284e-07
average cell area sphere: 511207895771493
average cell area grid: 511191264872782
relative error area: -3.25325544619452e-05
relative avg error area: 1.72940048429811e-05

++++++++++ MPI RANK 0 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.81 RT (REAL_TIME)
2078606505 CP (Cells processed)
0.313214 ASPT (Averaged Seconds per Timestep)
0.0994453 MCPPT (Million Cells Processed in Average per Simulation Timestep)
7843.83 ACPST (Averaged number of clusters per Simulation Timestep)
0.317499 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
391.297 CPSPT (Clusters per Second per Thread)
103.184 EDMBPT (CellData Megabyte per Timestep (RW))
329.436 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.8 RT (REAL_TIME)
2078605005 CP (Cells processed)
0.313214 ASPT (Averaged Seconds per Timestep)
0.0994453 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2577.52 ACPST (Averaged number of clusters per Simulation Timestep)
0.317499 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
128.582 CPSPT (Clusters per Second per Thread)
103.184 EDMBPT (CellData Megabyte per Timestep (RW))
329.436 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.8 RT (REAL_TIME)
2078612173 CP (Cells processed)
0.313214 ASPT (Averaged Seconds per Timestep)
0.0994456 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2312.42 ACPST (Averaged number of clusters per Simulation Timestep)
0.3175 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
115.358 CPSPT (Clusters per Second per Thread)
103.185 EDMBPT (CellData Megabyte per Timestep (RW))
329.438 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.81 RT (REAL_TIME)
2078605966 CP (Cells processed)
0.313214 ASPT (Averaged Seconds per Timestep)
0.0994453 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2349.88 ACPST (Averaged number of clusters per Simulation Timestep)
0.317499 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
117.226 CPSPT (Clusters per Second per Thread)
103.184 EDMBPT (CellData Megabyte per Timestep (RW))
329.436 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.81 RT (REAL_TIME)
2078617735 CP (Cells processed)
0.313214 ASPT (Averaged Seconds per Timestep)
0.0994459 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2384.26 ACPST (Averaged number of clusters per Simulation Timestep)
0.317501 MCPS (Million Cells per Second) (local)
2078.62 MCP (Million Cells processed) (local)
118.941 CPSPT (Clusters per Second per Thread)
103.185 EDMBPT (CellData Megabyte per Timestep (RW))
329.438 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.85 RT (REAL_TIME)
2078606794 CP (Cells processed)
0.313217 ASPT (Averaged Seconds per Timestep)
0.0994454 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2319.44 ACPST (Averaged number of clusters per Simulation Timestep)
0.317497 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
115.707 CPSPT (Clusters per Second per Thread)
103.184 EDMBPT (CellData Megabyte per Timestep (RW))
329.434 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0158749 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.82 RT (REAL_TIME)
2078613364 CP (Cells processed)
0.313215 ASPT (Averaged Seconds per Timestep)
0.0994457 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2461.9 ACPST (Averaged number of clusters per Simulation Timestep)
0.3175 MCPS (Million Cells per Second) (local)
2078.61 MCP (Million Cells processed) (local)
122.814 CPSPT (Clusters per Second per Thread)
103.185 EDMBPT (CellData Megabyte per Timestep (RW))
329.437 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.015875 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
20902 TS (Timesteps)
1.0368e+06 ST (SIMULATION_TIME)
0.882147 TSS (Timestep size)
6546.84 RT (REAL_TIME)
2078547570 CP (Cells processed)
0.313216 ASPT (Averaged Seconds per Timestep)
0.0994425 MCPPT (Million Cells Processed in Average per Simulation Timestep)
5015.84 ACPST (Averaged number of clusters per Simulation Timestep)
0.317489 MCPS (Million Cells per Second) (local)
2078.55 MCP (Million Cells processed) (local)
250.219 CPSPT (Clusters per Second per Thread)
103.181 EDMBPT (CellData Megabyte per Timestep (RW))
329.425 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.0158744 GFLOPS


++++++++++ SUMMARY ++++++++++
2.53998405862253 GMCPS (Global Million Cells per Second) (global)
16628.815112 GMCP (Global Million Cells Processed) (global)
2.65652878944374 GCPSPT (Global Clusters per Second per Thread) (global)
0.795560956463496 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
20902 GNT (Global Number of Timesteps) (global)

28735020 MSNGCells (Migration Sent Number of Global Cells)
766946 MSNGCluster (Migration Sent Number of Global Cluster)
0.00172802139003884 MRC (Migrated Relative Cells)
0.126999202931126 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 20902
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	18355	113728	113722	-5.71572e-05
1	2748	113734	113722	-0.000109918
2	2515	113712	113722	8.35374e-05
3	1864	113701	113722	0.000180265
4	3516	113750	113722	-0.000250612
5	1984	113704	113722	0.000153885
6	2306	113694	113722	0.000241819
7	1821	113749	113722	-0.000241819
