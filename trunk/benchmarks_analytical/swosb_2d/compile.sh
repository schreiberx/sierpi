#! /bin/bash

cd ../..

scons --xml-config=./scenarios/single_wave_on_simple_beach_2d_bouy_sampling.xml --hyperbolic-adaptivity-mode=2 -j 4 $@ || exit -1
