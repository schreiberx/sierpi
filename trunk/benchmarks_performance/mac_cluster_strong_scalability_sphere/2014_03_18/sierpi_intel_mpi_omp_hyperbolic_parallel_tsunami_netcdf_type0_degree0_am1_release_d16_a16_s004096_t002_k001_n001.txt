WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
337.891 RT (REAL_TIME)
1843151346 CP (Cells processed)
3.37891 ASPT (Averaged Seconds per Timestep)
18.4315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4722.47 ACPST (Averaged number of clusters per Simulation Timestep)
5.45488 MCPS (Million Cells per Second) (local)
1843.15 MCP (Million Cells processed) (local)
698.816 CPSPT (Clusters per Second per Thread)
12655.9 EDMBPT (CellData Megabyte per Timestep (RW))
3745.57 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.272744 GFLOPS

++++++++++ SUMMARY ++++++++++
5.45487553068688 GMCPS (Global Million Cells per Second) (global)
1843.151346 GMCP (Global Million Cells Processed) (global)
349.408176698405 GCPSPT (Global Clusters per Second per Thread) (global)
18.43151346 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.272743776534344 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4799	18719408	1.87194e+07	0
