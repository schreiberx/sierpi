WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
314.308 RT (REAL_TIME)
921574788 CP (Cells processed)
3.14308 ASPT (Averaged Seconds per Timestep)
9.21575 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2324.25 ACPST (Averaged number of clusters per Simulation Timestep)
2.93208 MCPS (Million Cells per Second) (local)
921.575 MCP (Million Cells processed) (local)
739.482 CPSPT (Clusters per Second per Thread)
6327.95 EDMBPT (CellData Megabyte per Timestep (RW))
2013.3 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.146604 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
314.308 RT (REAL_TIME)
921576558 CP (Cells processed)
3.14308 ASPT (Averaged Seconds per Timestep)
9.21577 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2398.22 ACPST (Averaged number of clusters per Simulation Timestep)
2.93208 MCPS (Million Cells per Second) (local)
921.577 MCP (Million Cells processed) (local)
763.016 CPSPT (Clusters per Second per Thread)
6327.96 EDMBPT (CellData Megabyte per Timestep (RW))
2013.3 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.146604 GFLOPS


++++++++++ SUMMARY ++++++++++
5.86415555107357 GMCPS (Global Million Cells per Second) (global)
1843.151346 GMCP (Global Million Cells Processed) (global)
751.248634794107 GCPSPT (Global Clusters per Second per Thread) (global)
18.43151346 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

110286 MSNGCells (Migration Sent Number of Global Cells)
28 MSNGCluster (Migration Sent Number of Global Cluster)
5.98355063627823e-05 MRC (Migrated Relative Cells)
0.293207777553679 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2364	9360627	9.3597e+06	-9.86142e-05
1	2435	9358781	9.3597e+06	9.86142e-05
