#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_17/benchmarks_performance/mac_cluster_strong_scalability_sphere/2014_03_18//sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d16_a16_s004096_t016_k001_n008.txt
# error
#SBATCH -e /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_17/benchmarks_performance/mac_cluster_strong_scalability_sphere/2014_03_18//sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d16_a16_s004096_t016_k001_n008.err
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_17/benchmarks_performance/mac_cluster_strong_scalability_sphere
# job description
#SBATCH -J sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release_d16_a16_s004096_t016_k001_n008
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=32
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=03:00:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_17/benchmarks_performance/mac_cluster_strong_scalability_sphere

#export KMP_AFFINITY=granularity=fine,compact,1,0
mpiexec.hydra -genv OMP_NUM_THREADS 16 -envall -ppn 1 -n 8 ../../build/sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am1_release  -c ../../scenarios/cubedSphere_swe_release.xml -d 16 -a 16 -t -1 -L 100 -k 1 -r 0.01/0.005 -n 16 -N 16 -o 4096
