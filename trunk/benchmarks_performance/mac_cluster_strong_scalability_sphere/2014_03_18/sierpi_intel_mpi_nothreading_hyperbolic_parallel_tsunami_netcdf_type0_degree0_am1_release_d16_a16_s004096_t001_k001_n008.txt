WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
Warning from RANK Warning from RANK 7: more nodes than initial clusters available!
6: more nodes than initial clusters available!
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230404370 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.30404 MCPPT (Million Cells Processed in Average per Simulation Timestep)
604.09 ACPST (Averaged number of clusters per Simulation Timestep)
2.45281 MCPS (Million Cells per Second) (local)
230.404 MCP (Million Cells processed) (local)
643.096 CPSPT (Clusters per Second per Thread)
1582.06 EDMBPT (CellData Megabyte per Timestep (RW))
1684.21 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122641 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230386452 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.30386 MCPPT (Million Cells Processed in Average per Simulation Timestep)
577.03 ACPST (Averaged number of clusters per Simulation Timestep)
2.45262 MCPS (Million Cells per Second) (local)
230.386 MCP (Million Cells processed) (local)
614.288 CPSPT (Clusters per Second per Thread)
1581.94 EDMBPT (CellData Megabyte per Timestep (RW))
1684.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122631 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230395059 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.30395 MCPPT (Million Cells Processed in Average per Simulation Timestep)
566.66 ACPST (Averaged number of clusters per Simulation Timestep)
2.45271 MCPS (Million Cells per Second) (local)
230.395 MCP (Million Cells processed) (local)
603.249 CPSPT (Clusters per Second per Thread)
1582 EDMBPT (CellData Megabyte per Timestep (RW))
1684.15 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122636 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.935 RT (REAL_TIME)
230388907 CP (Cells processed)
0.93935 ASPT (Averaged Seconds per Timestep)
2.30389 MCPPT (Million Cells Processed in Average per Simulation Timestep)
576.47 ACPST (Averaged number of clusters per Simulation Timestep)
2.45264 MCPS (Million Cells per Second) (local)
230.389 MCP (Million Cells processed) (local)
613.69 CPSPT (Clusters per Second per Thread)
1581.96 EDMBPT (CellData Megabyte per Timestep (RW))
1684.1 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122632 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.935 RT (REAL_TIME)
230386244 CP (Cells processed)
0.93935 ASPT (Averaged Seconds per Timestep)
2.30386 MCPPT (Million Cells Processed in Average per Simulation Timestep)
661.83 ACPST (Averaged number of clusters per Simulation Timestep)
2.45261 MCPS (Million Cells per Second) (local)
230.386 MCP (Million Cells processed) (local)
704.562 CPSPT (Clusters per Second per Thread)
1581.94 EDMBPT (CellData Megabyte per Timestep (RW))
1684.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122631 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230400000 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
562.5 ACPST (Averaged number of clusters per Simulation Timestep)
2.45277 MCPS (Million Cells per Second) (local)
230.4 MCP (Million Cells processed) (local)
598.82 CPSPT (Clusters per Second per Thread)
1582.03 EDMBPT (CellData Megabyte per Timestep (RW))
1684.18 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122638 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230399550 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
587.76 ACPST (Averaged number of clusters per Simulation Timestep)
2.45276 MCPS (Million Cells per Second) (local)
230.4 MCP (Million Cells processed) (local)
625.711 CPSPT (Clusters per Second per Thread)
1582.03 EDMBPT (CellData Megabyte per Timestep (RW))
1684.18 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122638 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
93.9347 RT (REAL_TIME)
230390764 CP (Cells processed)
0.939347 ASPT (Averaged Seconds per Timestep)
2.30391 MCPPT (Million Cells Processed in Average per Simulation Timestep)
586.13 ACPST (Averaged number of clusters per Simulation Timestep)
2.45267 MCPS (Million Cells per Second) (local)
230.391 MCP (Million Cells processed) (local)
623.976 CPSPT (Clusters per Second per Thread)
1581.97 EDMBPT (CellData Megabyte per Timestep (RW))
1684.11 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.122633 GFLOPS


++++++++++ SUMMARY ++++++++++
19.6216080122848 GMCPS (Global Million Cells per Second) (global)
1843.151346 GMCP (Global Million Cells Processed) (global)
628.424061709171 GCPSPT (Global Clusters per Second per Thread) (global)
18.43151346 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

596710 MSNGCells (Migration Sent Number of Global Cells)
150 MSNGCluster (Migration Sent Number of Global Cluster)
0.000323747205349411 MRC (Migrated Relative Cells)
0.98108040061424 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	621	2341608	2.33993e+06	-0.000718826
1	582	2339674	2.33993e+06	0.000107696
2	576	2338308	2.33993e+06	0.000691475
3	585	2341037	2.33993e+06	-0.000474801
4	675	2339561	2.33993e+06	0.000155988
5	571	2338816	2.33993e+06	0.000474374
6	596	2341685	2.33993e+06	-0.000751733
7	593	2338719	2.33993e+06	0.000515828
