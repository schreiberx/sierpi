WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-dof-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'
WARNING: In scope 'swe', parameter 'visualization-boundary-method' not handled with value '6'

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
95.8352 RT (REAL_TIME)
460790822 CP (Cells processed)
0.958352 ASPT (Averaged Seconds per Timestep)
4.60791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1181.12 ACPST (Averaged number of clusters per Simulation Timestep)
4.80816 MCPS (Million Cells per Second) (local)
460.791 MCP (Million Cells processed) (local)
616.224 CPSPT (Clusters per Second per Thread)
3164 EDMBPT (CellData Megabyte per Timestep (RW))
3301.5 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.240408 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
95.8359 RT (REAL_TIME)
460783966 CP (Cells processed)
0.958359 ASPT (Averaged Seconds per Timestep)
4.60784 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1143.13 ACPST (Averaged number of clusters per Simulation Timestep)
4.80805 MCPS (Million Cells per Second) (local)
460.784 MCP (Million Cells processed) (local)
596.4 CPSPT (Clusters per Second per Thread)
3163.95 EDMBPT (CellData Megabyte per Timestep (RW))
3301.43 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.240403 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
95.8359 RT (REAL_TIME)
460786244 CP (Cells processed)
0.958359 ASPT (Averaged Seconds per Timestep)
4.60786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1224.33 ACPST (Averaged number of clusters per Simulation Timestep)
4.80808 MCPS (Million Cells per Second) (local)
460.786 MCP (Million Cells processed) (local)
638.764 CPSPT (Clusters per Second per Thread)
3163.97 EDMBPT (CellData Megabyte per Timestep (RW))
3301.44 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.240404 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
12.7221 ST (SIMULATION_TIME)
0.125918 TSS (Timestep size)
95.8353 RT (REAL_TIME)
460790314 CP (Cells processed)
0.958353 ASPT (Averaged Seconds per Timestep)
4.6079 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1173.89 ACPST (Averaged number of clusters per Simulation Timestep)
4.80815 MCPS (Million Cells per Second) (local)
460.79 MCP (Million Cells processed) (local)
612.452 CPSPT (Clusters per Second per Thread)
3164 EDMBPT (CellData Megabyte per Timestep (RW))
3301.49 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.240407 GFLOPS


++++++++++ SUMMARY ++++++++++
19.2324347928773 GMCPS (Global Million Cells per Second) (global)
1843.151346 GMCP (Global Million Cells Processed) (global)
307.979983558925 GCPSPT (Global Clusters per Second per Thread) (global)
18.43151346 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

178918 MSNGCells (Migration Sent Number of Global Cells)
48 MSNGCluster (Migration Sent Number of Global Cluster)
9.7071931951922e-05 MRC (Migrated Relative Cells)
0.961621739643864 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1203	4681282	4.67985e+06	-0.000305565
1	1161	4679345	4.67985e+06	0.000108337
2	1246	4678377	4.67985e+06	0.000315181
3	1189	4680404	4.67985e+06	-0.000117952
