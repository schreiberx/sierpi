#! /bin/sh

source /etc/profile.d/modules.sh

#source $HOME/bin/scons_vars.sh
#source $HOME/bin/intel_vars.sh
#source $HOME/bin/tbb_vars.sh

#module unload ccomp
#module load ccomp/intel/12.1
module load netcdf
module load tbb

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$NETCDF_BASE/lib/pkgconfig

#. /lrz/sys/intel/icc_121_367/composer_xe_2011_sp1.13.367/bin/compilervars.sh intel64

