#! /usr/bin/python

import sys
import time
import commands
import os
import re



if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


filelist=os.listdir(jobfile_directory)
filelist.sort()

#          sierpi_intel_mpi_hyperbolic_parallel_tsunami_b0_release_d22_a08_s1073741824_t002_I000_n001.cmd
re_string='sierpi_intel_(.*)_hyperbolic_parallel_tsunami_.*release_(.*)\.(.*)'



print 'parallelization_type\taffinity_and_scheduling_method\tmpi_nodes\tthreads\tskip_sjm\tdepth\tadapt_depth\tsplitting_size\treal_time\tcells_per_timestep\tgmcps'

for i in filelist: 
	m=re.match(re_string, i)

	if m == None:
		print 'File pattern not recognized: '+jobfile_directory+'/'+i
		sys.exit(-1)

	g=m.groups()

	if g[2]!='txt':
		continue

	# threading type
	parallelization_type=g[0]

	# affinity and scheduling method
#	affinity_and_scheduling_method=g[1]
	affinity_and_scheduling_method=None

	if affinity_and_scheduling_method==None:
		affinity_and_scheduling_method='default'
	else:
		affinity_and_scheduling_method=affinity_and_scheduling_method[1:]

	# extract simulation parameters
	sim_parameters=g[1]

	s=sim_parameters.split('_');

	depth=s[0][1:]
	adapt_depth=s[1][1:]
	splitting_size=s[2][1:]
	threads=s[3][1:]
#	initial_splits=s[4][1:]
	skip_sjm=s[4][1:]
	mpi_nodes=s[5][1:]

	# MTPS
	job_output_filepath=jobfile_directory+'/'+i

	f=open(job_output_filepath, 'r')
	lines=f.readlines()
	f.close()


	def extract_benchmark_info(lines, searchtag):

		for line in lines:
			if line.find(searchtag) >= 0:
				return float(line.replace(searchtag, ''))
		return None


	def search_tag(lines, searchtag):
		for line in lines:
			if line.find(searchtag) >= 0:
				return True
		return False



	gmcps=None
	real_time=None
	error=True

	if search_tag(lines, 'DUE TO TIME LIMIT'):
		gmcps='time_limit'

	elif search_tag(lines, 'CANCELLED '):
		gmcps='cancelled'

	elif search_tag(lines, 'NODE FAILURE'):
		gmcps='node_failure'

	elif search_tag(lines, 'terminate called recursively'):
		gmcps='term_called_rec'

	elif search_tag(lines, 'Assertion'):
		gmcps='assertion_failed'

#		acpst='exit_status'

	else:
		gmcps=extract_benchmark_info(lines, ' GMCPS (Global Million Cells per Second) (global)')
		gmcppt=extract_benchmark_info(lines, ' GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)')

		real_time=extract_benchmark_info(lines, ' RT (REAL_TIME)')
		error = False

#	if error:
#		sys.stderr.write(i+"\n")

#	if mcps == None:
#		print 'Invalid benchmark output file '+i
#		sys.exit(-1)

	print parallelization_type+'\t'+affinity_and_scheduling_method+'\t'+mpi_nodes.zfill(3)+'\t'+threads.zfill(3)+'\t'+skip_sjm.zfill(3)+'\t'+depth.zfill(2)+'\t'+adapt_depth.zfill(2)+'\t'+splitting_size.zfill(6)+'\t'+str(real_time)+'\t'+str(gmcppt)+'\t'+str(gmcps)

