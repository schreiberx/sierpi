#! /bin/bash

. ~/bin/scons_vars.sh

. inc_vars.sh

cd ../..

DEFAULT_OPTS=" --xml-config=scenarios/cubedSphere_swe_release.xml"
DEFAULT_OPTS+=" -j 4"

#DEFAULT_OPTS+=" --adaptive-cluster-stack-grow-extra-padding=4096  --adaptive-cluster-stack-shrink-extra-padding=4096"


#make clean

scons $DEFAULT_OPTS
scons $DEFAULT_OPTS --threading=tbb
scons $DEFAULT_OPTS --threading=omp

#scons $DEFAULT_OPTS --threading=off --enable-scan-based-split-and-join=on
#scons $DEFAULT_OPTS --threading=tbb
#scons $DEFAULT_OPTS --threading=tbb --enable-scan-based-split-and-join=on
#scons $DEFAULT_OPTS --threading=tbb --enable-skip-adaptive-conforming-clusters=on
#scons $DEFAULT_OPTS --threading=tbb --enable-scan-based-split-and-join=on --enable-skip-adaptive-conforming-clusters=on

#scons $DEFAULT_OPTS --threading=tbb
#scons $DEFAULT_OPTS --threading=omp

