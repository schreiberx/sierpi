#! /bin/bash


echo "itbb parallel"
./run_itbb_parallel.sh

echo "itbb delayed"
./run_itbb_delayed.sh

echo "tbb pinned"
./run_tbb_pinned_parallel.sh

echo "omp pinned"
./run_omp_pinned_parallel.sh

echo "itbb delayed"
./run_itbb_delayed.sh

echo "parallel delayed"
./run_tbb_parallel_delayed.sh

echo "parallel"
./run_tbb_parallel.sh

echo "sequential"
./run_tbb_sequential.sh
