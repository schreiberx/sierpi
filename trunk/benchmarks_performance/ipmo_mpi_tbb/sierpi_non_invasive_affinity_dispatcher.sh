#! /bin/sh

. ./params.sh

cd ../../

EXEC="./build/sierpi_gnu_mpi_tbb_hyperbolic_parallel_tsunami_b0_release"


#
# scons --xml-config=./scenarios/itbb_async_mpi.xml  --threading=tbb
#

test -z "$OMPI_COMM_WORLD_RANK" && { echo "Execute with mpirun & OpenMPI"; exit 1; }

test "$OMPI_COMM_WORLD_SIZE" -eq "1" && { echo "Start with 'mpirun -np [MPI_NODES]'"; exit 1; }


echo "Rank $OMPI_COMM_WORLD_RANK"
$EXEC $PARAMS -n $((NCORES/OMPI_COMM_WORLD_SIZE)) -N $((NCORES)) -S $((NCORES*OMPI_COMM_WORLD_RANK/MPINODES))
