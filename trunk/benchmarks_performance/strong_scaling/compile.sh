#! /bin/bash

cd ../../

DEFAULT_OPTS="--compiler=intel --enable-gui=off --mode=release --enable-bathymetry-kernels=on --hyperbolic-degree-of-basis-functions=1 -j 4"

make clean

scons $DEFAULT_OPTS --threading=omp && \
scons $DEFAULT_OPTS --threading=tbb && \
scons $DEFAULT_OPTS --threading=tbb --enable-scan-split-and-join=on && \
scons $DEFAULT_OPTS --threading=tbb --enable-scan-split-and-join=on --enable-tbb-task-affinities=on && \
scons $DEFAULT_OPTS --threading=tbb --enable-scan-split-and-join=on --enable-tbb-task-affinities=on --skip-adaptive-conforming-sub-clusters=on && \
scons $DEFAULT_OPTS --threading=tbb --enable-scan-split-and-join=on --skip-adaptive-conforming-sub-clusters=on && \
scons $DEFAULT_OPTS --threading=tbb --skip-adaptive-conforming-sub-clusters=on && \
scons $DEFAULT_OPTS --threading=tbb --enable-force-scan-traversal=on
