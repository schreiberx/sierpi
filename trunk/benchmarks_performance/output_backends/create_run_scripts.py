#! /usr/bin/python2

import sys
import time
import commands
import os
import stat
import math

if len(sys.argv) < 2:
	print "Use "+sys.argv[0]+" [output dir]"
	sys.exit(-1)

output_dir=sys.argv[1]


# working directory
working_directory=os.path.abspath('.')


# create job directory
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

jobfile_directory=os.path.abspath('.')+'/'+output_dir


sierpi_extra_params=' -v 10 -A 1'



#
# Sierpi parameter sets
#
if True:
	# BIN SET
	sierpi_bin_set=[
		'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_NOOUTPUT_release',
		'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_DEFAULT_release',
		'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_PTHREAD_release',
		'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_PTHREAD_LASTCORE_release',
		'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_TASK_release'
	]

	sierpi_skip_split_and_join_parameter_set=[1]

	sierpi_sim_fixedtimesteps=201
	sierpi_output_writer_timesteps=[25,50,100]

	sierpi_sim_depth_set=[10]
	sierpi_sim_adaptive_depth_set=[16]
#	sierpi_sim_adaptive_depth_set=[4]

	# splitting sizes
	sierpi_sim_splitting_size_set=[1024*4]
	sierpi_sim_threads_set=[40]

	sierpi_sim_initial_split=10

	sierpi_adaptivity_parameters='0.01/0.005'

	estimated_runtime_scalar=1.0




# bin set
for sierpi_bin in sierpi_bin_set:

	# depth set
	for depth in sierpi_sim_depth_set:

		# adaptive depth set
		for adaptive_depth in sierpi_sim_adaptive_depth_set:

			# splitting sizes
			for splitting_size in sierpi_sim_splitting_size_set:

				# threads set
				for num_threads in sierpi_sim_threads_set:

					# write output every N timesteps
					for output_writer_timesteps in sierpi_output_writer_timesteps:

						# special handling of NOOUTPUT  binary
						if sierpi_bin == 'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_NOOUTPUT_release':
							bin='sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_DEFAULT_release'
						else:
							bin = sierpi_bin

						if sierpi_bin == 'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_PTHREAD_LASTCORE_release':
							bin='sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_PTHREAD_release'
							use_num_threads = num_threads-1
						else:
							use_num_threads = num_threads


						sierpi_run=bin

						if sierpi_bin != 'sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_NOOUTPUT_release':
							sierpi_run+=' -f'
							sierpi_run+=' -B '+str(output_writer_timesteps)

						# output grid data
						sierpi_run+=' -d '+str(depth)
						sierpi_run+=' -a '+str(adaptive_depth)
						sierpi_run+=' -L '+str(sierpi_sim_fixedtimesteps)
						sierpi_run+=' -n '+str(use_num_threads)
						sierpi_run+=' -o '+str(splitting_size)

						if sierpi_extra_params != '':
							sierpi_run+=' '+sierpi_extra_params

						job_description_string = sierpi_bin+'_d'+str(depth).zfill(2)+'_a'+str(adaptive_depth).zfill(2)+'_s'+str(splitting_size).zfill(6)+'_t'+str(num_threads).zfill(4)
						job_description_string += '_B'+str(output_writer_timesteps).zfill(4)
						job_description_string += '_n'+str(num_threads).zfill(2)

						job_filepath = jobfile_directory+'/'+job_description_string+'.sh'
						output_filepath = jobfile_directory+'/'+job_description_string+'.txt'

						job_file_content="""#! /bin/bash

rm -f frame_*
# wait at least 10 seconds for flushing disk
sleep 10
cd """+working_directory+"""
taskset -c 0-"""+str(num_threads-1)+""" ../../build/"""+sierpi_run+""" > """+output_filepath+"""
"""

						print "Writing jobfile '"+job_filepath+"'"
						f=open(job_filepath, 'w')
						f.write(job_file_content)
						f.close()

						os.chmod(job_filepath, stat.S_IEXEC | stat.S_IREAD | stat.S_IWRITE)
