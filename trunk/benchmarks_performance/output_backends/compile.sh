#! /bin/bash

cd ../..

OPTS="--compiler=intel --mode=release --threading=tbb -j 4"

OPTS1="						--program-binary-name=sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_DEFAULT_release"
OPTS2="--enable-writer-task=on			--program-binary-name=sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_TASK_release"
OPTS3="--enable-writer-thread-with-pthread=on	--program-binary-name=sierpi_intel_nothreading_hyperbolic_parallel_tsunami_type0_degree0_am1_WRITER_PTHREAD_release"


echo "scons $OPTS $OPTS1"
scons $OPTS $OPTS1

echo "scons $OPTS $OPTS2"
scons $OPTS $OPTS2

echo "scons $OPTS $OPTS3"
scons $OPTS $OPTS3

