#! /bin/bash

DIR=""
if [ -z $1 ]; then
	echo "usage: $0 [dir]"
	exit -1
fi

DIR=$1

echo -e "Version\tB\tMCPS"
C=1
for i in $DIR/*.txt; do
	V=`echo "$i" | sed "s/.*_am1_//" | sed "s/_release.*\.txt//"`
	B=`echo "$i" | sed "s/.*_B0*//" | sed "s/_n.*\.txt//"`
	MCPS=`tail -n 20 $i | grep " MCPS" | sed "s/ MCPS.*//"`

	if [ -z "$MCPS" ]; then
		continue
	fi

	echo -e "$V\t$B\t$MCPS"
done
