#! /bin/bash

. inc_vars.sh

cd ../..

DEFAULT_OPTS="--xml-config=scenarios/asagi_mpi_tbb_intel_release.xml"
DEFAULT_OPTS+=" -j 8"
DEFAULT_OPTS+=" --mode=release"

scons $DEFAULT_OPTS
scons $DEFAULT_OPTS --threading=off

#scons $DEFAULT_OPTS --threading=off --mode=debug
