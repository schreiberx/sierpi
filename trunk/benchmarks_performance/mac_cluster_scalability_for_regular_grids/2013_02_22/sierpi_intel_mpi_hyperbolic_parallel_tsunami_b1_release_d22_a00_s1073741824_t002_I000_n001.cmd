#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids/2013_02_22//sierpi_intel_mpi_hyperbolic_parallel_tsunami_b1_release_d22_a00_s1073741824_t002_I000_n001.txt
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids
# job description
#SBATCH -J sierpi_intel_mpi_hyperbolic_parallel_tsunami_b1_release_d22_a00_s1073741824_t002_I000_n001
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=12:00:00

source /etc/profile.d/modules.sh

source ./modules.sh


# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids

mpiexec.hydra -envall -ppn 1 -n 1 ../../build/sierpi_intel_mpi_hyperbolic_parallel_tsunami_b1_release -d 22 -a 0 -t -1 -L 10 -I 0 -r 0.01/0.005 -n 2 -o 1073741824
