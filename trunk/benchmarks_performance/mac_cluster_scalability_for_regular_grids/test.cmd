#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids/test.txt
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids
# job description
#SBATCH -J sierpi_intel_mpi_hyperbolic_parallel_tsunami_b1_release_d26_a00_s1073741824_t001_n002
#SBATCH --get-user-env
#SBATCH --ntasks=2

#SBATCH --cpus-per-task=1
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=10:10:10

#SBATCH --partition=snb

source /etc/profile.d/modules.sh

source ./inc_vars.sh


# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

echo "lkjasdf"

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2013_02_22_mac_cluster/benchmarks_performance/mac_cluster_scalability_for_regular_grids

srun_ps ../../build/sierpi_intel_mpi_hyperbolic_parallel_tsunami_b1_release -d 26 -a 0 -t -1 -L 100 -I 0 -r 0.01/0.005 -n 1 -o 1073741824 -A 1
