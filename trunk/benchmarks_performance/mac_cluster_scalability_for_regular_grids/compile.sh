#! /bin/sh


source /etc/profile.d/modules.sh

source ./modules.sh

. ~/bin/scons_vars.sh

cd ../../

DEFAULT_OPTS="--compiler=intel --enable-gui=off --mode=release --enable-bathymetry-kernels=off --hyperbolic-degree-of-basis-functions=1 -j 4 --enable-libxml=off --enable-mpi=on"

#make clean

scons $DEFAULT_OPTS --threading=off
