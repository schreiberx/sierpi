#! /bin/bash

cd ../..

scons --sub-simulation=euler --enable-libxml=off --threading=omp --enable-traversals-with-nodes=on --enable-print-rle-statistics=on -j4

