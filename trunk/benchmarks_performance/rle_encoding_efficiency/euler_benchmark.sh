#! /bin/sh

EXEC="../../build/sierpi_gnu_omp_hyperbolic_parallel_euler_withnodes_type0_degree0_am1_debug"
PARAMS="-d 6 -a 14 -t 100"
THRESHOLDS="16384 8192 4096 2048 1024 512 256 128 64 32 16"


for i in $THRESHOLDS; do
	$EXEC $PARAMS -o $i 2> euler_output_$i.txt
done
