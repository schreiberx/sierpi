#! /bin/sh

for i in swe_output_*; do
	ID=`echo $i | sed "s/swe_output_//" | sed "s/\.txt//"`
	AVG=`awk -F'\t' '{ sum += $5 } END { print sum / (NR-1) }'  < $i`

	MIN=`awk -F '\t' '{if(min==""){min=$5}; if($5< min) {min=$5}} END { print min }' < $i`
	MAX=`awk -F '\t' '{if(max==""){max=$5}; if($5> max) {max=$5}} END { print max }' < $i`

#	MAX=`awk -F '\t' '{if(min=="")min=max=$5}; if($5>max) {max=$5}; if($5< min) {min=$5}'`
#	MIN=`awk -F'\t' 'minv=0; {minv  = min($5, minv) } END { print minv }'  < $i`
	echo "$ID\t$AVG\t$MIN\t$MAX"
done
