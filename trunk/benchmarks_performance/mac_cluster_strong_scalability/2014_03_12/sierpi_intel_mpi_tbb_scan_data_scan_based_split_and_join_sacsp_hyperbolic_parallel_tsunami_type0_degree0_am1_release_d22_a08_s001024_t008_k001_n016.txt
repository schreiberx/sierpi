
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477301254 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1575.72 ACPST (Averaged number of clusters per Simulation Timestep)
23.3562 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
963.827 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
712.775 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16781 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477319481 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
643.19 ACPST (Averaged number of clusters per Simulation Timestep)
23.3571 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
393.423 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
712.803 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16786 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477297152 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
506.68 ACPST (Averaged number of clusters per Simulation Timestep)
23.356 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
309.923 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
712.769 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.1678 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477285376 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77285 MCPPT (Million Cells Processed in Average per Simulation Timestep)
546.86 ACPST (Averaged number of clusters per Simulation Timestep)
23.3554 MCPS (Million Cells per Second) (local)
477.285 MCP (Million Cells processed) (local)
334.5 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
712.751 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16777 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4358 RT (REAL_TIME)
477300736 CP (Cells processed)
0.204358 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
816.51 ACPST (Averaged number of clusters per Simulation Timestep)
23.3561 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
499.435 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
712.771 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.1678 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4361 RT (REAL_TIME)
477298176 CP (Cells processed)
0.204361 ASPT (Averaged Seconds per Timestep)
4.77298 MCPPT (Million Cells Processed in Average per Simulation Timestep)
911.53 ACPST (Averaged number of clusters per Simulation Timestep)
23.3556 MCPS (Million Cells per Second) (local)
477.298 MCP (Million Cells processed) (local)
557.549 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
712.757 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16778 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4363 RT (REAL_TIME)
477284352 CP (Cells processed)
0.204363 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
955.13 ACPST (Averaged number of clusters per Simulation Timestep)
23.3548 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
584.213 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
712.732 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16774 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4358 RT (REAL_TIME)
477345585 CP (Cells processed)
0.204358 ASPT (Averaged Seconds per Timestep)
4.77346 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1014.12 ACPST (Averaged number of clusters per Simulation Timestep)
23.3583 MCPS (Million Cells per Second) (local)
477.346 MCP (Million Cells processed) (local)
620.309 CPSPT (Clusters per Second per Thread)
145.674 EDMBPT (CellData Megabyte per Timestep (RW))
712.84 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16792 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477314506 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
821.62 ACPST (Averaged number of clusters per Simulation Timestep)
23.3569 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
502.565 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
712.796 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16785 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4355 RT (REAL_TIME)
477310976 CP (Cells processed)
0.204355 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
752.65 ACPST (Averaged number of clusters per Simulation Timestep)
23.357 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
460.382 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
712.799 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16785 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477294007 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.77294 MCPPT (Million Cells Processed in Average per Simulation Timestep)
899.43 ACPST (Averaged number of clusters per Simulation Timestep)
23.3559 MCPS (Million Cells per Second) (local)
477.294 MCP (Million Cells processed) (local)
550.158 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
712.764 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16779 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4359 RT (REAL_TIME)
477304045 CP (Cells processed)
0.204359 ASPT (Averaged Seconds per Timestep)
4.77304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
813.58 ACPST (Averaged number of clusters per Simulation Timestep)
23.3562 MCPS (Million Cells per Second) (local)
477.304 MCP (Million Cells processed) (local)
497.643 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
712.775 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16781 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4358 RT (REAL_TIME)
477341856 CP (Cells processed)
0.204358 ASPT (Averaged Seconds per Timestep)
4.77342 MCPPT (Million Cells Processed in Average per Simulation Timestep)
735.81 ACPST (Averaged number of clusters per Simulation Timestep)
23.3581 MCPS (Million Cells per Second) (local)
477.342 MCP (Million Cells processed) (local)
450.074 CPSPT (Clusters per Second per Thread)
145.673 EDMBPT (CellData Megabyte per Timestep (RW))
712.833 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16791 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.436 RT (REAL_TIME)
477283840 CP (Cells processed)
0.20436 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
476.32 ACPST (Averaged number of clusters per Simulation Timestep)
23.3551 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
291.349 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
712.741 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16776 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4357 RT (REAL_TIME)
477310034 CP (Cells processed)
0.204357 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
494.57 ACPST (Averaged number of clusters per Simulation Timestep)
23.3567 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
302.516 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
712.789 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16783 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.4356 RT (REAL_TIME)
477305306 CP (Cells processed)
0.204356 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1566.07 ACPST (Averaged number of clusters per Simulation Timestep)
23.3565 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
957.929 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
712.785 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.16783 GFLOPS


++++++++++ SUMMARY ++++++++++
373.702 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
64.6546 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3180 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446767 MRC (Migrated Relative Cells)
18.6851 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1649	4812932	4.81295e+06	3.14257e-06
1	918	4811959	4.81295e+06	0.000205306
2	656	4813824	4.81295e+06	-0.000182191
3	745	4812800	4.81295e+06	3.05686e-05
4	1280	4813824	4.81295e+06	-0.000182191
5	1482	4812800	4.81295e+06	3.05686e-05
6	1603	4812800	4.81295e+06	3.05686e-05
7	1663	4812091	4.81295e+06	0.00017788
8	1139	4812683	4.81295e+06	5.4878e-05
9	1140	4812800	4.81295e+06	3.05686e-05
10	1435	4815457	4.81295e+06	-0.000521484
11	1044	4811121	4.81295e+06	0.000379419
12	884	4812754	4.81295e+06	4.01261e-05
13	574	4813312	4.81295e+06	-7.58111e-05
14	635	4812906	4.81295e+06	8.54466e-06
15	1624	4813091	4.81295e+06	-2.98933e-05
