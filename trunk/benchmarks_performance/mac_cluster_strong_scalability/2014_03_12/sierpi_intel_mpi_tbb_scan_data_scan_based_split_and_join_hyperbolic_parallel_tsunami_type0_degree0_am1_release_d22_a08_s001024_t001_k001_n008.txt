
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954620735 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54621 MCPPT (Million Cells Processed in Average per Simulation Timestep)
212.56 ACPST (Averaged number of clusters per Simulation Timestep)
5.4223 MCPS (Million Cells per Second) (local)
954.621 MCP (Million Cells processed) (local)
120.735 CPSPT (Clusters per Second per Thread)
291.327 EDMBPT (CellData Megabyte per Timestep (RW))
165.475 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271115 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954579456 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54579 MCPPT (Million Cells Processed in Average per Simulation Timestep)
175.76 ACPST (Averaged number of clusters per Simulation Timestep)
5.42206 MCPS (Million Cells per Second) (local)
954.579 MCP (Million Cells processed) (local)
99.8326 CPSPT (Clusters per Second per Thread)
291.315 EDMBPT (CellData Megabyte per Timestep (RW))
165.468 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271103 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954612224 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54612 MCPPT (Million Cells Processed in Average per Simulation Timestep)
190.75 ACPST (Averaged number of clusters per Simulation Timestep)
5.42225 MCPS (Million Cells per Second) (local)
954.612 MCP (Million Cells processed) (local)
108.347 CPSPT (Clusters per Second per Thread)
291.325 EDMBPT (CellData Megabyte per Timestep (RW))
165.474 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271112 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954587953 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54588 MCPPT (Million Cells Processed in Average per Simulation Timestep)
96.93 ACPST (Averaged number of clusters per Simulation Timestep)
5.42211 MCPS (Million Cells per Second) (local)
954.588 MCP (Million Cells processed) (local)
55.0567 CPSPT (Clusters per Second per Thread)
291.317 EDMBPT (CellData Megabyte per Timestep (RW))
165.47 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271105 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954648008 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54648 MCPPT (Million Cells Processed in Average per Simulation Timestep)
167.72 ACPST (Averaged number of clusters per Simulation Timestep)
5.42245 MCPS (Million Cells per Second) (local)
954.648 MCP (Million Cells processed) (local)
95.2659 CPSPT (Clusters per Second per Thread)
291.335 EDMBPT (CellData Megabyte per Timestep (RW))
165.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271123 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954600100 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.546 MCPPT (Million Cells Processed in Average per Simulation Timestep)
176.37 ACPST (Averaged number of clusters per Simulation Timestep)
5.42218 MCPS (Million Cells per Second) (local)
954.6 MCP (Million Cells processed) (local)
100.179 CPSPT (Clusters per Second per Thread)
291.321 EDMBPT (CellData Megabyte per Timestep (RW))
165.472 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271109 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954632368 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54632 MCPPT (Million Cells Processed in Average per Simulation Timestep)
135.19 ACPST (Averaged number of clusters per Simulation Timestep)
5.42236 MCPS (Million Cells per Second) (local)
954.632 MCP (Million Cells processed) (local)
76.7887 CPSPT (Clusters per Second per Thread)
291.331 EDMBPT (CellData Megabyte per Timestep (RW))
165.477 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271118 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
176.055 RT (REAL_TIME)
954615852 CP (Cells processed)
1.76055 ASPT (Averaged Seconds per Timestep)
9.54616 MCPPT (Million Cells Processed in Average per Simulation Timestep)
221.46 ACPST (Averaged number of clusters per Simulation Timestep)
5.42227 MCPS (Million Cells per Second) (local)
954.616 MCP (Million Cells processed) (local)
125.791 CPSPT (Clusters per Second per Thread)
291.326 EDMBPT (CellData Megabyte per Timestep (RW))
165.475 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.271114 GFLOPS


++++++++++ SUMMARY ++++++++++
43.378 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
97.7495 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1755136 MSNGCells (Migration Sent Number of Global Cells)
1570 MSNGCluster (Migration Sent Number of Global Cluster)
0.000229823 MRC (Migrated Relative Cells)
2.1689 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	211	9624891	9.62589e+06	0.000104224
1	255	9626624	9.62589e+06	-7.58111e-05
2	277	9626624	9.62589e+06	-7.58111e-05
3	171	9624891	9.62589e+06	0.000104224
4	229	9625483	9.62589e+06	4.27233e-05
5	219	9626578	9.62589e+06	-7.10324e-05
6	150	9626066	9.62589e+06	-1.78425e-05
7	220	9625997	9.62589e+06	-1.06743e-05
