
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6042 RT (REAL_TIME)
954620723 CP (Cells processed)
0.206042 ASPT (Averaged Seconds per Timestep)
9.54621 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1360.36 ACPST (Averaged number of clusters per Simulation Timestep)
46.3313 MCPS (Million Cells per Second) (local)
954.621 MCP (Million Cells processed) (local)
825.292 CPSPT (Clusters per Second per Thread)
291.327 EDMBPT (CellData Megabyte per Timestep (RW))
1413.92 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31657 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6044 RT (REAL_TIME)
954582528 CP (Cells processed)
0.206044 ASPT (Averaged Seconds per Timestep)
9.54583 MCPPT (Million Cells Processed in Average per Simulation Timestep)
596.7 ACPST (Averaged number of clusters per Simulation Timestep)
46.3291 MCPS (Million Cells per Second) (local)
954.583 MCP (Million Cells processed) (local)
361.998 CPSPT (Clusters per Second per Thread)
291.315 EDMBPT (CellData Megabyte per Timestep (RW))
1413.85 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31646 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6045 RT (REAL_TIME)
954600960 CP (Cells processed)
0.206045 ASPT (Averaged Seconds per Timestep)
9.54601 MCPPT (Million Cells Processed in Average per Simulation Timestep)
924.62 ACPST (Averaged number of clusters per Simulation Timestep)
46.3298 MCPS (Million Cells per Second) (local)
954.601 MCP (Million Cells processed) (local)
560.934 CPSPT (Clusters per Second per Thread)
291.321 EDMBPT (CellData Megabyte per Timestep (RW))
1413.87 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31649 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6044 RT (REAL_TIME)
954622777 CP (Cells processed)
0.206044 ASPT (Averaged Seconds per Timestep)
9.54623 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1073.03 ACPST (Averaged number of clusters per Simulation Timestep)
46.331 MCPS (Million Cells per Second) (local)
954.623 MCP (Million Cells processed) (local)
650.972 CPSPT (Clusters per Second per Thread)
291.328 EDMBPT (CellData Megabyte per Timestep (RW))
1413.91 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31655 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6043 RT (REAL_TIME)
954629576 CP (Cells processed)
0.206043 ASPT (Averaged Seconds per Timestep)
9.5463 MCPPT (Million Cells Processed in Average per Simulation Timestep)
849.33 ACPST (Averaged number of clusters per Simulation Timestep)
46.3316 MCPS (Million Cells per Second) (local)
954.63 MCP (Million Cells processed) (local)
515.262 CPSPT (Clusters per Second per Thread)
291.33 EDMBPT (CellData Megabyte per Timestep (RW))
1413.93 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31658 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6043 RT (REAL_TIME)
954593968 CP (Cells processed)
0.206043 ASPT (Averaged Seconds per Timestep)
9.54594 MCPPT (Million Cells Processed in Average per Simulation Timestep)
955.34 ACPST (Averaged number of clusters per Simulation Timestep)
46.3299 MCPS (Million Cells per Second) (local)
954.594 MCP (Million Cells processed) (local)
579.576 CPSPT (Clusters per Second per Thread)
291.319 EDMBPT (CellData Megabyte per Timestep (RW))
1413.88 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31649 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6042 RT (REAL_TIME)
954630320 CP (Cells processed)
0.206042 ASPT (Averaged Seconds per Timestep)
9.5463 MCPPT (Million Cells Processed in Average per Simulation Timestep)
707.71 ACPST (Averaged number of clusters per Simulation Timestep)
46.3317 MCPS (Million Cells per Second) (local)
954.63 MCP (Million Cells processed) (local)
429.347 CPSPT (Clusters per Second per Thread)
291.33 EDMBPT (CellData Megabyte per Timestep (RW))
1413.93 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31659 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
20.6041 RT (REAL_TIME)
954615852 CP (Cells processed)
0.206041 ASPT (Averaged Seconds per Timestep)
9.54616 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1274.56 ACPST (Averaged number of clusters per Simulation Timestep)
46.3313 MCPS (Million Cells per Second) (local)
954.616 MCP (Million Cells processed) (local)
773.243 CPSPT (Clusters per Second per Thread)
291.326 EDMBPT (CellData Megabyte per Timestep (RW))
1413.92 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.31656 GFLOPS


++++++++++ SUMMARY ++++++++++
370.646 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
73.3848 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1755136 MSNGCells (Migration Sent Number of Global Cells)
1682 MSNGCluster (Migration Sent Number of Global Cluster)
0.000229822 MRC (Migrated Relative Cells)
18.5323 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1544	9624891	9.62589e+06	0.000104224
1	791	9626624	9.62589e+06	-7.58111e-05
2	1476	9626624	9.62589e+06	-7.58111e-05
3	1743	9624891	9.62589e+06	0.000104224
4	1197	9625483	9.62589e+06	4.27233e-05
5	1369	9626578	9.62589e+06	-7.10324e-05
6	846	9626066	9.62589e+06	-1.78425e-05
7	1369	9625997	9.62589e+06	-1.06743e-05
