
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
939.032 RT (REAL_TIME)
7636896720 CP (Cells processed)
9.39032 ASPT (Averaged Seconds per Timestep)
76.369 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9573.8 ACPST (Averaged number of clusters per Simulation Timestep)
8.13273 MCPS (Million Cells per Second) (local)
7636.9 MCP (Million Cells processed) (local)
1019.54 CPSPT (Clusters per Second per Thread)
2330.6 EDMBPT (CellData Megabyte per Timestep (RW))
248.191 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.406636 GFLOPS

++++++++++ SUMMARY ++++++++++
8.13273 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
1019.54 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.406636 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9672	77007154	7.70072e+07	0
