
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7102 RT (REAL_TIME)
954620723 CP (Cells processed)
0.757102 ASPT (Averaged Seconds per Timestep)
9.54621 MCPPT (Million Cells Processed in Average per Simulation Timestep)
441.51 ACPST (Averaged number of clusters per Simulation Timestep)
12.6089 MCPS (Million Cells per Second) (local)
954.621 MCP (Million Cells processed) (local)
291.579 CPSPT (Clusters per Second per Thread)
291.327 EDMBPT (CellData Megabyte per Timestep (RW))
384.793 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630444 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7102 RT (REAL_TIME)
954579456 CP (Cells processed)
0.757102 ASPT (Averaged Seconds per Timestep)
9.54579 MCPPT (Million Cells Processed in Average per Simulation Timestep)
233.39 ACPST (Averaged number of clusters per Simulation Timestep)
12.6083 MCPS (Million Cells per Second) (local)
954.579 MCP (Million Cells processed) (local)
154.134 CPSPT (Clusters per Second per Thread)
291.315 EDMBPT (CellData Megabyte per Timestep (RW))
384.776 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630416 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7103 RT (REAL_TIME)
954612224 CP (Cells processed)
0.757103 ASPT (Averaged Seconds per Timestep)
9.54612 MCPPT (Million Cells Processed in Average per Simulation Timestep)
304.01 ACPST (Averaged number of clusters per Simulation Timestep)
12.6088 MCPS (Million Cells per Second) (local)
954.612 MCP (Million Cells processed) (local)
200.772 CPSPT (Clusters per Second per Thread)
291.325 EDMBPT (CellData Megabyte per Timestep (RW))
384.789 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630438 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7103 RT (REAL_TIME)
954609453 CP (Cells processed)
0.757103 ASPT (Averaged Seconds per Timestep)
9.54609 MCPPT (Million Cells Processed in Average per Simulation Timestep)
271.74 ACPST (Averaged number of clusters per Simulation Timestep)
12.6087 MCPS (Million Cells per Second) (local)
954.609 MCP (Million Cells processed) (local)
179.46 CPSPT (Clusters per Second per Thread)
291.324 EDMBPT (CellData Megabyte per Timestep (RW))
384.788 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630436 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7102 RT (REAL_TIME)
954626504 CP (Cells processed)
0.757102 ASPT (Averaged Seconds per Timestep)
9.54627 MCPPT (Million Cells Processed in Average per Simulation Timestep)
259.28 ACPST (Averaged number of clusters per Simulation Timestep)
12.609 MCPS (Million Cells per Second) (local)
954.627 MCP (Million Cells processed) (local)
171.232 CPSPT (Clusters per Second per Thread)
291.329 EDMBPT (CellData Megabyte per Timestep (RW))
384.795 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630448 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7102 RT (REAL_TIME)
954600112 CP (Cells processed)
0.757102 ASPT (Averaged Seconds per Timestep)
9.546 MCPPT (Million Cells Processed in Average per Simulation Timestep)
315.29 ACPST (Averaged number of clusters per Simulation Timestep)
12.6086 MCPS (Million Cells per Second) (local)
954.6 MCP (Million Cells processed) (local)
208.222 CPSPT (Clusters per Second per Thread)
291.321 EDMBPT (CellData Megabyte per Timestep (RW))
384.784 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.63043 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7102 RT (REAL_TIME)
954632368 CP (Cells processed)
0.757102 ASPT (Averaged Seconds per Timestep)
9.54632 MCPPT (Million Cells Processed in Average per Simulation Timestep)
230.93 ACPST (Averaged number of clusters per Simulation Timestep)
12.609 MCPS (Million Cells per Second) (local)
954.632 MCP (Million Cells processed) (local)
152.509 CPSPT (Clusters per Second per Thread)
291.331 EDMBPT (CellData Megabyte per Timestep (RW))
384.797 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630452 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
75.7101 RT (REAL_TIME)
954615852 CP (Cells processed)
0.757101 ASPT (Averaged Seconds per Timestep)
9.54616 MCPPT (Million Cells Processed in Average per Simulation Timestep)
430.9 ACPST (Averaged number of clusters per Simulation Timestep)
12.6088 MCPS (Million Cells per Second) (local)
954.616 MCP (Million Cells processed) (local)
284.572 CPSPT (Clusters per Second per Thread)
291.326 EDMBPT (CellData Megabyte per Timestep (RW))
384.791 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.630441 GFLOPS


++++++++++ SUMMARY ++++++++++
100.87 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
102.655 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1755136 MSNGCells (Migration Sent Number of Global Cells)
1624 MSNGCluster (Migration Sent Number of Global Cluster)
0.000229822 MRC (Migrated Relative Cells)
5.04351 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	454	9624891	9.62589e+06	0.000104224
1	315	9626624	9.62589e+06	-7.58111e-05
2	462	9626624	9.62589e+06	-7.58111e-05
3	437	9624891	9.62589e+06	0.000104224
4	339	9625483	9.62589e+06	4.27233e-05
5	420	9626578	9.62589e+06	-7.10324e-05
6	269	9626066	9.62589e+06	-1.78425e-05
7	438	9625997	9.62589e+06	-1.06743e-05
