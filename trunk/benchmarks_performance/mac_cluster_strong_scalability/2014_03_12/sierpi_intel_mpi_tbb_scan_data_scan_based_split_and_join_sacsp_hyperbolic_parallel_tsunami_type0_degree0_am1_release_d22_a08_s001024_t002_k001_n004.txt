
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
148.707 RT (REAL_TIME)
1909200191 CP (Cells processed)
1.48707 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
381.76 ACPST (Averaged number of clusters per Simulation Timestep)
12.8387 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
128.36 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
391.806 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641935 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
148.707 RT (REAL_TIME)
1909200177 CP (Cells processed)
1.48707 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
292.34 ACPST (Averaged number of clusters per Simulation Timestep)
12.8387 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
98.2942 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
391.806 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641935 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
148.707 RT (REAL_TIME)
1909248108 CP (Cells processed)
1.48707 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
330.84 ACPST (Averaged number of clusters per Simulation Timestep)
12.839 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
111.239 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
391.816 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641951 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
148.707 RT (REAL_TIME)
1909248214 CP (Cells processed)
1.48707 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
354.4 ACPST (Averaged number of clusters per Simulation Timestep)
12.839 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
119.161 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
391.816 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641951 GFLOPS


++++++++++ SUMMARY ++++++++++
51.3555 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
57.1318 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
718 MSNGCluster (Migration Sent Number of Global Cluster)
9.453e-05 MRC (Migrated Relative Cells)
2.56777 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	459	19251515	1.92518e+07	1.42065e-05
1	453	19251515	1.92518e+07	1.42065e-05
2	438	19252061	1.92518e+07	-1.41545e-05
3	367	19252063	1.92518e+07	-1.42584e-05
