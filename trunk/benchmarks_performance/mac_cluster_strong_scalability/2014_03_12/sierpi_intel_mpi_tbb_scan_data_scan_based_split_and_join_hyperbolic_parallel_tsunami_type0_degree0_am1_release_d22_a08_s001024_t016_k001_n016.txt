
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6029 RT (REAL_TIME)
477301254 CP (Cells processed)
0.386029 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2279.52 ACPST (Averaged number of clusters per Simulation Timestep)
12.3644 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
369.065 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
377.331 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618219 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6029 RT (REAL_TIME)
477320521 CP (Cells processed)
0.386029 ASPT (Averaged Seconds per Timestep)
4.77321 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1158.18 ACPST (Averaged number of clusters per Simulation Timestep)
12.3649 MCPS (Million Cells per Second) (local)
477.321 MCP (Million Cells processed) (local)
187.515 CPSPT (Clusters per Second per Thread)
145.667 EDMBPT (CellData Megabyte per Timestep (RW))
377.347 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618245 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.603 RT (REAL_TIME)
477296128 CP (Cells processed)
0.38603 ASPT (Averaged Seconds per Timestep)
4.77296 MCPPT (Million Cells Processed in Average per Simulation Timestep)
815.2 ACPST (Averaged number of clusters per Simulation Timestep)
12.3642 MCPS (Million Cells per Second) (local)
477.296 MCP (Million Cells processed) (local)
131.985 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
377.327 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618212 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6029 RT (REAL_TIME)
477285376 CP (Cells processed)
0.386029 ASPT (Averaged Seconds per Timestep)
4.77285 MCPPT (Million Cells Processed in Average per Simulation Timestep)
907.67 ACPST (Averaged number of clusters per Simulation Timestep)
12.364 MCPS (Million Cells per Second) (local)
477.285 MCP (Million Cells processed) (local)
146.956 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
377.318 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618198 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.603 RT (REAL_TIME)
477300736 CP (Cells processed)
0.38603 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1435.82 ACPST (Averaged number of clusters per Simulation Timestep)
12.3644 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
232.466 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
377.33 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618218 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6034 RT (REAL_TIME)
477300224 CP (Cells processed)
0.386034 ASPT (Averaged Seconds per Timestep)
4.773 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1665.22 ACPST (Averaged number of clusters per Simulation Timestep)
12.3642 MCPS (Million Cells per Second) (local)
477.3 MCP (Million Cells processed) (local)
269.604 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
377.326 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618211 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6034 RT (REAL_TIME)
477284352 CP (Cells processed)
0.386034 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1737.99 ACPST (Averaged number of clusters per Simulation Timestep)
12.3638 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
281.385 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
377.313 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618189 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6036 RT (REAL_TIME)
477349681 CP (Cells processed)
0.386036 ASPT (Averaged Seconds per Timestep)
4.7735 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1835.45 ACPST (Averaged number of clusters per Simulation Timestep)
12.3654 MCPS (Million Cells per Second) (local)
477.35 MCP (Million Cells processed) (local)
297.163 CPSPT (Clusters per Second per Thread)
145.676 EDMBPT (CellData Megabyte per Timestep (RW))
377.363 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618271 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6035 RT (REAL_TIME)
477310416 CP (Cells processed)
0.386035 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1434.02 ACPST (Averaged number of clusters per Simulation Timestep)
12.3644 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
232.171 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
377.332 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618221 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6025 RT (REAL_TIME)
477309952 CP (Cells processed)
0.386025 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1316.16 ACPST (Averaged number of clusters per Simulation Timestep)
12.3647 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
213.095 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
377.342 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618236 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6035 RT (REAL_TIME)
477294003 CP (Cells processed)
0.386035 ASPT (Averaged Seconds per Timestep)
4.77294 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1663 ACPST (Averaged number of clusters per Simulation Timestep)
12.364 MCPS (Million Cells per Second) (local)
477.294 MCP (Million Cells processed) (local)
269.243 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
377.319 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.6182 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6034 RT (REAL_TIME)
477303033 CP (Cells processed)
0.386034 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1394.5 ACPST (Averaged number of clusters per Simulation Timestep)
12.3643 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
225.773 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
377.327 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618213 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.603 RT (REAL_TIME)
477341892 CP (Cells processed)
0.38603 ASPT (Averaged Seconds per Timestep)
4.77342 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1221.55 ACPST (Averaged number of clusters per Simulation Timestep)
12.3654 MCPS (Million Cells per Second) (local)
477.342 MCP (Million Cells processed) (local)
197.775 CPSPT (Clusters per Second per Thread)
145.673 EDMBPT (CellData Megabyte per Timestep (RW))
377.363 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618271 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6029 RT (REAL_TIME)
477283840 CP (Cells processed)
0.386029 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
769.55 ACPST (Averaged number of clusters per Simulation Timestep)
12.3639 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
124.594 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
377.318 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618197 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6029 RT (REAL_TIME)
477310034 CP (Cells processed)
0.386029 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
940.11 ACPST (Averaged number of clusters per Simulation Timestep)
12.3646 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
152.208 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
377.338 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618231 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
38.6028 RT (REAL_TIME)
477305306 CP (Cells processed)
0.386028 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2262.69 ACPST (Averaged number of clusters per Simulation Timestep)
12.3645 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
366.342 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
377.335 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.618226 GFLOPS


++++++++++ SUMMARY ++++++++++
197.831 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
14.4427 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3310 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446767 MRC (Migrated Relative Cells)
9.89156 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2398	4812932	4.81295e+06	3.14257e-06
1	1687	4811959	4.81295e+06	0.000205306
2	1081	4813824	4.81295e+06	-0.000182191
3	1273	4812800	4.81295e+06	3.05686e-05
4	2315	4813824	4.81295e+06	-0.000182191
5	2803	4812800	4.81295e+06	3.05686e-05
6	2916	4812800	4.81295e+06	3.05686e-05
7	3028	4812091	4.81295e+06	0.00017788
8	2109	4812683	4.81295e+06	5.4878e-05
9	2038	4812800	4.81295e+06	3.05686e-05
10	2694	4815457	4.81295e+06	-0.000521484
11	1841	4811121	4.81295e+06	0.000379419
12	1534	4812754	4.81295e+06	4.01261e-05
13	932	4813312	4.81295e+06	-7.58111e-05
14	1258	4812906	4.81295e+06	8.54466e-06
15	2342	4813091	4.81295e+06	-2.98933e-05
