
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
345.552 RT (REAL_TIME)
3818400352 CP (Cells processed)
3.45552 ASPT (Averaged Seconds per Timestep)
38.184 MCPPT (Million Cells Processed in Average per Simulation Timestep)
284.99 ACPST (Averaged number of clusters per Simulation Timestep)
11.0501 MCPS (Million Cells per Second) (local)
3818.4 MCP (Million Cells processed) (local)
41.2369 CPSPT (Clusters per Second per Thread)
1165.28 EDMBPT (CellData Megabyte per Timestep (RW))
337.223 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.552507 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
345.552 RT (REAL_TIME)
3818496334 CP (Cells processed)
3.45552 ASPT (Averaged Seconds per Timestep)
38.185 MCPPT (Million Cells Processed in Average per Simulation Timestep)
340.22 ACPST (Averaged number of clusters per Simulation Timestep)
11.0504 MCPS (Million Cells per Second) (local)
3818.5 MCP (Million Cells processed) (local)
49.2284 CPSPT (Clusters per Second per Thread)
1165.31 EDMBPT (CellData Megabyte per Timestep (RW))
337.232 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.552521 GFLOPS


++++++++++ SUMMARY ++++++++++
22.1006 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
22.6163 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

361472 MSNGCells (Migration Sent Number of Global Cells)
242 MSNGCluster (Migration Sent Number of Global Cluster)
4.73317e-05 MRC (Migrated Relative Cells)
1.10503 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	402	38503030	3.85036e+07	1.42324e-05
1	382	38504126	3.85036e+07	-1.42324e-05
