
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477297158 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
588.85 ACPST (Averaged number of clusters per Simulation Timestep)
4.69712 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
579.492 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
143.345 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234856 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477312813 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77313 MCPPT (Million Cells Processed in Average per Simulation Timestep)
624.41 ACPST (Averaged number of clusters per Simulation Timestep)
4.69728 MCPS (Million Cells per Second) (local)
477.313 MCP (Million Cells processed) (local)
614.487 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
143.349 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234864 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477290496 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.7729 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.63 ACPST (Averaged number of clusters per Simulation Timestep)
4.69706 MCPS (Million Cells per Second) (local)
477.29 MCP (Million Cells processed) (local)
573.371 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
143.343 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234853 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477323264 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77323 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.67 ACPST (Averaged number of clusters per Simulation Timestep)
4.69738 MCPS (Million Cells per Second) (local)
477.323 MCP (Million Cells processed) (local)
573.41 CPSPT (Clusters per Second per Thread)
145.667 EDMBPT (CellData Megabyte per Timestep (RW))
143.353 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234869 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477331456 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77331 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.68 ACPST (Averaged number of clusters per Simulation Timestep)
4.69746 MCPS (Million Cells per Second) (local)
477.331 MCP (Million Cells processed) (local)
573.42 CPSPT (Clusters per Second per Thread)
145.67 EDMBPT (CellData Megabyte per Timestep (RW))
143.355 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234873 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477290496 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.7729 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.63 ACPST (Averaged number of clusters per Simulation Timestep)
4.69706 MCPS (Million Cells per Second) (local)
477.29 MCP (Million Cells processed) (local)
573.371 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
143.343 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234853 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477265920 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77266 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.6 ACPST (Averaged number of clusters per Simulation Timestep)
4.69681 MCPS (Million Cells per Second) (local)
477.266 MCP (Million Cells processed) (local)
573.342 CPSPT (Clusters per Second per Thread)
145.65 EDMBPT (CellData Megabyte per Timestep (RW))
143.335 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234841 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477335853 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77336 MCPPT (Million Cells Processed in Average per Simulation Timestep)
630.65 ACPST (Averaged number of clusters per Simulation Timestep)
4.6975 MCPS (Million Cells per Second) (local)
477.336 MCP (Million Cells processed) (local)
620.628 CPSPT (Clusters per Second per Thread)
145.671 EDMBPT (CellData Megabyte per Timestep (RW))
143.356 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234875 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477309904 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
623.19 ACPST (Averaged number of clusters per Simulation Timestep)
4.69725 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
613.287 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
143.349 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234862 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477323264 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77323 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.67 ACPST (Averaged number of clusters per Simulation Timestep)
4.69738 MCPS (Million Cells per Second) (local)
477.323 MCP (Million Cells processed) (local)
573.41 CPSPT (Clusters per Second per Thread)
145.667 EDMBPT (CellData Megabyte per Timestep (RW))
143.353 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234869 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477288891 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77289 MCPPT (Million Cells Processed in Average per Simulation Timestep)
595.18 ACPST (Averaged number of clusters per Simulation Timestep)
4.69704 MCPS (Million Cells per Second) (local)
477.289 MCP (Million Cells processed) (local)
585.722 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
143.342 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234852 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477302521 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
607.3 ACPST (Averaged number of clusters per Simulation Timestep)
4.69717 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
597.649 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
143.346 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234859 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477300932 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
619.85 ACPST (Averaged number of clusters per Simulation Timestep)
4.69716 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
610 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
143.346 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234858 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477315072 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.66 ACPST (Averaged number of clusters per Simulation Timestep)
4.6973 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
573.401 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
143.35 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234865 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477302356 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77302 MCPPT (Million Cells Processed in Average per Simulation Timestep)
610.17 ACPST (Averaged number of clusters per Simulation Timestep)
4.69717 MCPS (Million Cells per Second) (local)
477.302 MCP (Million Cells processed) (local)
600.473 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
143.346 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234859 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
101.615 RT (REAL_TIME)
477306330 CP (Cells processed)
1.01615 ASPT (Averaged Seconds per Timestep)
4.77306 MCPPT (Million Cells Processed in Average per Simulation Timestep)
595.66 ACPST (Averaged number of clusters per Simulation Timestep)
4.69721 MCPS (Million Cells per Second) (local)
477.306 MCP (Million Cells processed) (local)
586.194 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
143.348 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.234861 GFLOPS


++++++++++ SUMMARY ++++++++++
75.1553 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
588.854 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3391488 MSNGCells (Migration Sent Number of Global Cells)
414 MSNGCluster (Migration Sent Number of Global Cluster)
0.000444093 MRC (Migrated Relative Cells)
3.75777 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	593	4809860	4.81295e+06	0.000641421
1	635	4812983	4.81295e+06	-7.45385e-06
2	588	4816896	4.81295e+06	-0.000820469
3	587	4808704	4.81295e+06	0.000881606
4	588	4816896	4.81295e+06	-0.000820469
5	587	4808704	4.81295e+06	0.000881606
6	588	4816896	4.81295e+06	-0.000820469
7	641	4814139	4.81295e+06	-0.000247639
8	631	4814731	4.81295e+06	-0.000370641
9	587	4808704	4.81295e+06	0.000881606
10	601	4811361	4.81295e+06	0.000329554
11	613	4812145	4.81295e+06	0.00016666
12	627	4814802	4.81295e+06	-0.000385393
13	588	4816896	4.81295e+06	-0.000820469
14	616	4811370	4.81295e+06	0.000327684
15	602	4812067	4.81295e+06	0.000182866
