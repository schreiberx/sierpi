
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477301254 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
385.43 ACPST (Averaged number of clusters per Simulation Timestep)
5.66879 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
457.766 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
172.998 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283439 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477319469 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
63.35 ACPST (Averaged number of clusters per Simulation Timestep)
5.66901 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
75.2393 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
173.005 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283451 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477297152 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
124.79 ACPST (Averaged number of clusters per Simulation Timestep)
5.66874 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
148.21 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
172.996 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283437 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477282304 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77282 MCPPT (Million Cells Processed in Average per Simulation Timestep)
121.02 ACPST (Averaged number of clusters per Simulation Timestep)
5.66857 MCPS (Million Cells per Second) (local)
477.282 MCP (Million Cells processed) (local)
143.733 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
172.991 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283429 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477303808 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
145.77 ACPST (Averaged number of clusters per Simulation Timestep)
5.66882 MCPS (Million Cells per Second) (local)
477.304 MCP (Million Cells processed) (local)
173.128 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
172.999 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283441 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477308416 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77308 MCPPT (Million Cells Processed in Average per Simulation Timestep)
172.67 ACPST (Averaged number of clusters per Simulation Timestep)
5.66887 MCPS (Million Cells per Second) (local)
477.308 MCP (Million Cells processed) (local)
205.076 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283444 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477294592 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77295 MCPPT (Million Cells Processed in Average per Simulation Timestep)
125.35 ACPST (Averaged number of clusters per Simulation Timestep)
5.66871 MCPS (Million Cells per Second) (local)
477.295 MCP (Million Cells processed) (local)
148.875 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
172.995 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283436 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477312813 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77313 MCPPT (Million Cells Processed in Average per Simulation Timestep)
164.1 ACPST (Averaged number of clusters per Simulation Timestep)
5.66893 MCPS (Million Cells per Second) (local)
477.313 MCP (Million Cells processed) (local)
194.898 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
173.002 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283446 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477328840 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77329 MCPPT (Million Cells Processed in Average per Simulation Timestep)
129.92 ACPST (Averaged number of clusters per Simulation Timestep)
5.66912 MCPS (Million Cells per Second) (local)
477.329 MCP (Million Cells processed) (local)
154.303 CPSPT (Clusters per Second per Thread)
145.669 EDMBPT (CellData Megabyte per Timestep (RW))
173.008 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283456 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1979 RT (REAL_TIME)
477299712 CP (Cells processed)
0.841979 ASPT (Averaged Seconds per Timestep)
4.773 MCPPT (Million Cells Processed in Average per Simulation Timestep)
132.56 ACPST (Averaged number of clusters per Simulation Timestep)
5.66878 MCPS (Million Cells per Second) (local)
477.3 MCP (Million Cells processed) (local)
157.439 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
172.998 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283439 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477304247 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
136.04 ACPST (Averaged number of clusters per Simulation Timestep)
5.66883 MCPS (Million Cells per Second) (local)
477.304 MCP (Million Cells processed) (local)
161.571 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
172.999 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283441 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477295865 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77296 MCPPT (Million Cells Processed in Average per Simulation Timestep)
185.13 ACPST (Averaged number of clusters per Simulation Timestep)
5.66872 MCPS (Million Cells per Second) (local)
477.296 MCP (Million Cells processed) (local)
219.874 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
172.996 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283436 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477349040 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.77349 MCPPT (Million Cells Processed in Average per Simulation Timestep)
116.16 ACPST (Averaged number of clusters per Simulation Timestep)
5.66936 MCPS (Million Cells per Second) (local)
477.349 MCP (Million Cells processed) (local)
137.96 CPSPT (Clusters per Second per Thread)
145.675 EDMBPT (CellData Megabyte per Timestep (RW))
173.015 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283468 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477283840 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
112.9 ACPST (Averaged number of clusters per Simulation Timestep)
5.66858 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
134.089 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
172.991 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283429 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.198 RT (REAL_TIME)
477310034 CP (Cells processed)
0.84198 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
67.2 ACPST (Averaged number of clusters per Simulation Timestep)
5.6689 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
79.8119 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
173.001 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283445 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
84.1981 RT (REAL_TIME)
477305306 CP (Cells processed)
0.841981 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
379.4 ACPST (Averaged number of clusters per Simulation Timestep)
5.66884 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
450.604 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
172.999 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283442 GFLOPS


++++++++++ SUMMARY ++++++++++
90.7016 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
190.161 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3026 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446769 MRC (Migrated Relative Cells)
4.53508 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	402	4812932	4.81295e+06	3.14257e-06
1	60	4811959	4.81295e+06	0.000205306
2	164	4813824	4.81295e+06	-0.000182191
3	164	4812800	4.81295e+06	3.05686e-05
4	217	4813824	4.81295e+06	-0.000182191
5	258	4812800	4.81295e+06	3.05686e-05
6	227	4812800	4.81295e+06	3.05686e-05
7	230	4812091	4.81295e+06	0.00017788
8	145	4812683	4.81295e+06	5.4878e-05
9	196	4812800	4.81295e+06	3.05686e-05
10	194	4815457	4.81295e+06	-0.000521484
11	227	4811121	4.81295e+06	0.000379419
12	114	4812754	4.81295e+06	4.01261e-05
13	153	4813312	4.81295e+06	-7.58111e-05
14	65	4812906	4.81295e+06	8.54466e-06
15	389	4813091	4.81295e+06	-2.98933e-05
