
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8304 RT (REAL_TIME)
1909200179 CP (Cells processed)
0.988304 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
663.73 ACPST (Averaged number of clusters per Simulation Timestep)
19.3179 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
167.896 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
589.537 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965897 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8305 RT (REAL_TIME)
1909213485 CP (Cells processed)
0.988305 ASPT (Averaged Seconds per Timestep)
19.0921 MCPPT (Million Cells Processed in Average per Simulation Timestep)
590 ACPST (Averaged number of clusters per Simulation Timestep)
19.3181 MCPS (Million Cells per Second) (local)
1909.21 MCP (Million Cells processed) (local)
149.245 CPSPT (Clusters per Second per Thread)
582.646 EDMBPT (CellData Megabyte per Timestep (RW))
589.54 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965903 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8305 RT (REAL_TIME)
1909234808 CP (Cells processed)
0.988305 ASPT (Averaged Seconds per Timestep)
19.0923 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.62 ACPST (Averaged number of clusters per Simulation Timestep)
19.3183 MCPS (Million Cells per Second) (local)
1909.23 MCP (Million Cells processed) (local)
147.379 CPSPT (Clusters per Second per Thread)
582.652 EDMBPT (CellData Megabyte per Timestep (RW))
589.547 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965914 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8304 RT (REAL_TIME)
1909248214 CP (Cells processed)
0.988304 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
683.01 ACPST (Averaged number of clusters per Simulation Timestep)
19.3184 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
172.773 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
589.552 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965922 GFLOPS


++++++++++ SUMMARY ++++++++++
77.2727 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
39.8308 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
782 MSNGCluster (Migration Sent Number of Global Cluster)
9.45301e-05 MRC (Migrated Relative Cells)
3.86364 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	758	19251515	1.92518e+07	1.42065e-05
1	918	19251515	1.92518e+07	1.42065e-05
2	765	19252061	1.92518e+07	-1.41545e-05
3	727	19252063	1.92518e+07	-1.42584e-05
