
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.44 RT (REAL_TIME)
1909223731 CP (Cells processed)
0.2244 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2378.56 ACPST (Averaged number of clusters per Simulation Timestep)
85.0814 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
662.479 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
2596.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.25407 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.4399 RT (REAL_TIME)
1909223725 CP (Cells processed)
0.224399 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2378.56 ACPST (Averaged number of clusters per Simulation Timestep)
85.0815 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
662.479 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
2596.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.25407 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.4399 RT (REAL_TIME)
1909224580 CP (Cells processed)
0.224399 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2408.34 ACPST (Averaged number of clusters per Simulation Timestep)
85.0815 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
670.774 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
2596.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.25408 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.44 RT (REAL_TIME)
1909224684 CP (Cells processed)
0.2244 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2408.34 ACPST (Averaged number of clusters per Simulation Timestep)
85.0814 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
670.773 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
2596.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.25407 GFLOPS


++++++++++ SUMMARY ++++++++++
340.326 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
41.6641 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

720896 MSNGCells (Migration Sent Number of Global Cells)
88 MSNGCluster (Migration Sent Number of Global Cluster)
9.43964e-05 MRC (Migrated Relative Cells)
17.0163 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2403	19248443	1.92518e+07	0.000173776
1	2404	19256635	1.92518e+07	-0.000251743
2	2432	19246941	1.92518e+07	0.000251795
3	2433	19255135	1.92518e+07	-0.000173828
