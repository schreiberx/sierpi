
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3596 RT (REAL_TIME)
954609983 CP (Cells processed)
0.533596 ASPT (Averaged Seconds per Timestep)
9.5461 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1213.26 ACPST (Averaged number of clusters per Simulation Timestep)
17.8901 MCPS (Million Cells per Second) (local)
954.61 MCP (Million Cells processed) (local)
568.436 CPSPT (Clusters per Second per Thread)
291.324 EDMBPT (CellData Megabyte per Timestep (RW))
545.964 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894507 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3596 RT (REAL_TIME)
954613760 CP (Cells processed)
0.533596 ASPT (Averaged Seconds per Timestep)
9.54614 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.3 ACPST (Averaged number of clusters per Simulation Timestep)
17.8902 MCPS (Million Cells per Second) (local)
954.614 MCP (Million Cells processed) (local)
545.966 CPSPT (Clusters per Second per Thread)
291.325 EDMBPT (CellData Megabyte per Timestep (RW))
545.966 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.89451 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3596 RT (REAL_TIME)
954621952 CP (Cells processed)
0.533596 ASPT (Averaged Seconds per Timestep)
9.54622 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
17.8904 MCPS (Million Cells per Second) (local)
954.622 MCP (Million Cells processed) (local)
545.97 CPSPT (Clusters per Second per Thread)
291.327 EDMBPT (CellData Megabyte per Timestep (RW))
545.97 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894518 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3596 RT (REAL_TIME)
954601777 CP (Cells processed)
0.533596 ASPT (Averaged Seconds per Timestep)
9.54602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1213.25 ACPST (Averaged number of clusters per Simulation Timestep)
17.89 MCPS (Million Cells per Second) (local)
954.602 MCP (Million Cells processed) (local)
568.431 CPSPT (Clusters per Second per Thread)
291.321 EDMBPT (CellData Megabyte per Timestep (RW))
545.959 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894499 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3592 RT (REAL_TIME)
954633162 CP (Cells processed)
0.533592 ASPT (Averaged Seconds per Timestep)
9.54633 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1205.86 ACPST (Averaged number of clusters per Simulation Timestep)
17.8907 MCPS (Million Cells per Second) (local)
954.633 MCP (Million Cells processed) (local)
564.973 CPSPT (Clusters per Second per Thread)
291.331 EDMBPT (CellData Megabyte per Timestep (RW))
545.981 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894535 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3592 RT (REAL_TIME)
954591396 CP (Cells processed)
0.533592 ASPT (Averaged Seconds per Timestep)
9.54591 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1202.48 ACPST (Averaged number of clusters per Simulation Timestep)
17.8899 MCPS (Million Cells per Second) (local)
954.591 MCP (Million Cells processed) (local)
563.389 CPSPT (Clusters per Second per Thread)
291.318 EDMBPT (CellData Megabyte per Timestep (RW))
545.957 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894496 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3592 RT (REAL_TIME)
954616004 CP (Cells processed)
0.533592 ASPT (Averaged Seconds per Timestep)
9.54616 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1202.51 ACPST (Averaged number of clusters per Simulation Timestep)
17.8904 MCPS (Million Cells per Second) (local)
954.616 MCP (Million Cells processed) (local)
563.403 CPSPT (Clusters per Second per Thread)
291.326 EDMBPT (CellData Megabyte per Timestep (RW))
545.971 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894519 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
53.3592 RT (REAL_TIME)
954608682 CP (Cells processed)
0.533592 ASPT (Averaged Seconds per Timestep)
9.54609 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1205.83 ACPST (Averaged number of clusters per Simulation Timestep)
17.8902 MCPS (Million Cells per Second) (local)
954.609 MCP (Million Cells processed) (local)
564.959 CPSPT (Clusters per Second per Thread)
291.323 EDMBPT (CellData Megabyte per Timestep (RW))
545.967 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.894512 GFLOPS


++++++++++ SUMMARY ++++++++++
143.122 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
140.173 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1769472 MSNGCells (Migration Sent Number of Global Cells)
216 MSNGCluster (Migration Sent Number of Global Cluster)
0.000231701 MRC (Migrated Relative Cells)
7.15609 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1228	9622843	9.62589e+06	0.000316984
1	1175	9625600	9.62589e+06	3.05686e-05
2	1175	9625600	9.62589e+06	3.05686e-05
3	1229	9631035	9.62589e+06	-0.000534054
4	1218	9623437	9.62589e+06	0.000255275
5	1214	9623506	9.62589e+06	0.000248107
6	1215	9631698	9.62589e+06	-0.000602931
7	1218	9623435	9.62589e+06	0.000255483
