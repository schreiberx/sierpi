
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
674.473 RT (REAL_TIME)
3818400368 CP (Cells processed)
6.74473 ASPT (Averaged Seconds per Timestep)
38.184 MCPPT (Million Cells Processed in Average per Simulation Timestep)
106.65 ACPST (Averaged number of clusters per Simulation Timestep)
5.66131 MCPS (Million Cells per Second) (local)
3818.4 MCP (Million Cells processed) (local)
15.8123 CPSPT (Clusters per Second per Thread)
1165.28 EDMBPT (CellData Megabyte per Timestep (RW))
172.769 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283065 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
674.473 RT (REAL_TIME)
3818496322 CP (Cells processed)
6.74473 ASPT (Averaged Seconds per Timestep)
38.185 MCPPT (Million Cells Processed in Average per Simulation Timestep)
154.78 ACPST (Averaged number of clusters per Simulation Timestep)
5.66145 MCPS (Million Cells per Second) (local)
3818.5 MCP (Million Cells processed) (local)
22.9483 CPSPT (Clusters per Second per Thread)
1165.31 EDMBPT (CellData Megabyte per Timestep (RW))
172.774 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283072 GFLOPS


++++++++++ SUMMARY ++++++++++
11.3228 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
19.3803 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

361472 MSNGCells (Migration Sent Number of Global Cells)
242 MSNGCluster (Migration Sent Number of Global Cluster)
4.73317e-05 MRC (Migrated Relative Cells)
0.566138 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	181	38503030	3.85036e+07	1.42065e-05
1	154	38504124	3.85036e+07	-1.42065e-05
