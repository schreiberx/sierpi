
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
236.652 RT (REAL_TIME)
3818447456 CP (Cells processed)
2.36652 ASPT (Averaged Seconds per Timestep)
38.1845 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4757.12 ACPST (Averaged number of clusters per Simulation Timestep)
16.1353 MCPS (Million Cells per Second) (local)
3818.45 MCP (Million Cells processed) (local)
1005.09 CPSPT (Clusters per Second per Thread)
1165.3 EDMBPT (CellData Megabyte per Timestep (RW))
492.409 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.806763 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
236.652 RT (REAL_TIME)
3818449264 CP (Cells processed)
2.36652 ASPT (Averaged Seconds per Timestep)
38.1845 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4816.68 ACPST (Averaged number of clusters per Simulation Timestep)
16.1353 MCPS (Million Cells per Second) (local)
3818.45 MCP (Million Cells processed) (local)
1017.67 CPSPT (Clusters per Second per Thread)
1165.3 EDMBPT (CellData Megabyte per Timestep (RW))
492.409 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.806763 GFLOPS


++++++++++ SUMMARY ++++++++++
32.2705 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
505.689 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

360448 MSNGCells (Migration Sent Number of Global Cells)
44 MSNGCluster (Migration Sent Number of Global Cluster)
4.71982e-05 MRC (Migrated Relative Cells)
1.61353 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4807	38505078	3.85036e+07	-3.89834e-05
1	4865	38502076	3.85036e+07	3.89834e-05
