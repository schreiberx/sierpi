
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
31.426 RT (REAL_TIME)
1909205299 CP (Cells processed)
0.31426 ASPT (Averaged Seconds per Timestep)
19.0921 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1944.34 ACPST (Averaged number of clusters per Simulation Timestep)
60.7524 MCPS (Million Cells per Second) (local)
1909.21 MCP (Million Cells processed) (local)
386.69 CPSPT (Clusters per Second per Thread)
582.643 EDMBPT (CellData Megabyte per Timestep (RW))
1854.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.03762 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
31.426 RT (REAL_TIME)
1909217593 CP (Cells processed)
0.31426 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2072.51 ACPST (Averaged number of clusters per Simulation Timestep)
60.7528 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
412.181 CPSPT (Clusters per Second per Thread)
582.647 EDMBPT (CellData Megabyte per Timestep (RW))
1854.03 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.03764 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
31.4259 RT (REAL_TIME)
1909227640 CP (Cells processed)
0.314259 ASPT (Averaged Seconds per Timestep)
19.0923 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1830.25 ACPST (Averaged number of clusters per Simulation Timestep)
60.7532 MCPS (Million Cells per Second) (local)
1909.23 MCP (Million Cells processed) (local)
364.001 CPSPT (Clusters per Second per Thread)
582.65 EDMBPT (CellData Megabyte per Timestep (RW))
1854.04 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.03766 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
31.4259 RT (REAL_TIME)
1909246166 CP (Cells processed)
0.314259 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2022.45 ACPST (Averaged number of clusters per Simulation Timestep)
60.754 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
402.226 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
1854.06 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.0377 GFLOPS


++++++++++ SUMMARY ++++++++++
243.012 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
24.4546 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
808 MSNGCluster (Migration Sent Number of Global Cluster)
9.45303e-05 MRC (Migrated Relative Cells)
12.1506 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2324	19251515	1.92518e+07	1.42065e-05
1	3347	19251515	1.92518e+07	1.42065e-05
2	2597	19252061	1.92518e+07	-1.41545e-05
3	2254	19252063	1.92518e+07	-1.42584e-05
