
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477301254 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1067.33 ACPST (Averaged number of clusters per Simulation Timestep)
20.2642 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
1132.86 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
618.414 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01321 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477319469 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
348.61 ACPST (Averaged number of clusters per Simulation Timestep)
20.265 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
370.014 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
618.439 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01325 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477297152 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
309.8 ACPST (Averaged number of clusters per Simulation Timestep)
20.264 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
328.82 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
618.409 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0132 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477285376 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.77285 MCPPT (Million Cells Processed in Average per Simulation Timestep)
285.24 ACPST (Averaged number of clusters per Simulation Timestep)
20.2635 MCPS (Million Cells per Second) (local)
477.285 MCP (Million Cells processed) (local)
302.752 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
618.393 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01318 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.554 RT (REAL_TIME)
477300736 CP (Cells processed)
0.23554 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
424.49 ACPST (Averaged number of clusters per Simulation Timestep)
20.2641 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
450.551 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
618.413 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01321 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5541 RT (REAL_TIME)
477302272 CP (Cells processed)
0.235541 ASPT (Averaged Seconds per Timestep)
4.77302 MCPPT (Million Cells Processed in Average per Simulation Timestep)
538.6 ACPST (Averaged number of clusters per Simulation Timestep)
20.2641 MCPS (Million Cells per Second) (local)
477.302 MCP (Million Cells processed) (local)
571.663 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
618.411 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0132 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477280256 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.7728 MCPPT (Million Cells Processed in Average per Simulation Timestep)
521.92 ACPST (Averaged number of clusters per Simulation Timestep)
20.2633 MCPS (Million Cells per Second) (local)
477.28 MCP (Million Cells processed) (local)
553.963 CPSPT (Clusters per Second per Thread)
145.654 EDMBPT (CellData Megabyte per Timestep (RW))
618.387 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01316 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5542 RT (REAL_TIME)
477333305 CP (Cells processed)
0.235542 ASPT (Averaged Seconds per Timestep)
4.77333 MCPPT (Million Cells Processed in Average per Simulation Timestep)
536.49 ACPST (Averaged number of clusters per Simulation Timestep)
20.2653 MCPS (Million Cells per Second) (local)
477.333 MCP (Million Cells processed) (local)
569.421 CPSPT (Clusters per Second per Thread)
145.671 EDMBPT (CellData Megabyte per Timestep (RW))
618.449 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01327 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5542 RT (REAL_TIME)
477328840 CP (Cells processed)
0.235542 ASPT (Averaged Seconds per Timestep)
4.77329 MCPPT (Million Cells Processed in Average per Simulation Timestep)
463.75 ACPST (Averaged number of clusters per Simulation Timestep)
20.2651 MCPS (Million Cells per Second) (local)
477.329 MCP (Million Cells processed) (local)
492.216 CPSPT (Clusters per Second per Thread)
145.669 EDMBPT (CellData Megabyte per Timestep (RW))
618.442 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01326 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5537 RT (REAL_TIME)
477312000 CP (Cells processed)
0.235537 ASPT (Averaged Seconds per Timestep)
4.77312 MCPPT (Million Cells Processed in Average per Simulation Timestep)
386.53 ACPST (Averaged number of clusters per Simulation Timestep)
20.2648 MCPS (Million Cells per Second) (local)
477.312 MCP (Million Cells processed) (local)
410.264 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
618.433 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01324 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.554 RT (REAL_TIME)
477295031 CP (Cells processed)
0.23554 ASPT (Averaged Seconds per Timestep)
4.77295 MCPPT (Million Cells Processed in Average per Simulation Timestep)
452.71 ACPST (Averaged number of clusters per Simulation Timestep)
20.2639 MCPS (Million Cells per Second) (local)
477.295 MCP (Million Cells processed) (local)
480.502 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
618.404 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01319 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.554 RT (REAL_TIME)
477292793 CP (Cells processed)
0.23554 ASPT (Averaged Seconds per Timestep)
4.77293 MCPPT (Million Cells Processed in Average per Simulation Timestep)
492.36 ACPST (Averaged number of clusters per Simulation Timestep)
20.2637 MCPS (Million Cells per Second) (local)
477.293 MCP (Million Cells processed) (local)
522.586 CPSPT (Clusters per Second per Thread)
145.658 EDMBPT (CellData Megabyte per Timestep (RW))
618.4 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01319 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.554 RT (REAL_TIME)
477349040 CP (Cells processed)
0.23554 ASPT (Averaged Seconds per Timestep)
4.77349 MCPPT (Million Cells Processed in Average per Simulation Timestep)
432.82 ACPST (Averaged number of clusters per Simulation Timestep)
20.2661 MCPS (Million Cells per Second) (local)
477.349 MCP (Million Cells processed) (local)
459.391 CPSPT (Clusters per Second per Thread)
145.675 EDMBPT (CellData Megabyte per Timestep (RW))
618.474 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01331 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.554 RT (REAL_TIME)
477283840 CP (Cells processed)
0.23554 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
302.61 ACPST (Averaged number of clusters per Simulation Timestep)
20.2634 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
321.188 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
618.39 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01317 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477310034 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
255.36 ACPST (Averaged number of clusters per Simulation Timestep)
20.2646 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
271.038 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
618.427 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01323 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5539 RT (REAL_TIME)
477305306 CP (Cells processed)
0.235539 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1061.59 ACPST (Averaged number of clusters per Simulation Timestep)
20.2644 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
1126.76 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
618.419 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01322 GFLOPS


++++++++++ SUMMARY ++++++++++
324.23 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
130.687 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3106 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446769 MRC (Migrated Relative Cells)
16.2115 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1111	4812932	4.81295e+06	3.14257e-06
1	488	4811959	4.81295e+06	0.000205306
2	397	4813824	4.81295e+06	-0.000182191
3	394	4812800	4.81295e+06	3.05686e-05
4	669	4813824	4.81295e+06	-0.000182191
5	849	4812800	4.81295e+06	3.05686e-05
6	876	4812800	4.81295e+06	3.05686e-05
7	848	4812091	4.81295e+06	0.00017788
8	610	4812683	4.81295e+06	5.4878e-05
9	586	4812800	4.81295e+06	3.05686e-05
10	716	4815457	4.81295e+06	-0.000521484
11	623	4811121	4.81295e+06	0.000379419
12	505	4812754	4.81295e+06	4.01261e-05
13	367	4813312	4.81295e+06	-7.58111e-05
14	311	4812906	4.81295e+06	8.54466e-06
15	1095	4813091	4.81295e+06	-2.98933e-05
