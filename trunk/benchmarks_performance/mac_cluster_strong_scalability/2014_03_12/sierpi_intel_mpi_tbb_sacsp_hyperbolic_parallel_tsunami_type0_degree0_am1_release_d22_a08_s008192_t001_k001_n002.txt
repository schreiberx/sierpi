
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
465.936 RT (REAL_TIME)
3818447472 CP (Cells processed)
4.65936 ASPT (Averaged Seconds per Timestep)
38.1845 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4757.12 ACPST (Averaged number of clusters per Simulation Timestep)
8.19522 MCPS (Million Cells per Second) (local)
3818.45 MCP (Million Cells processed) (local)
1020.98 CPSPT (Clusters per Second per Thread)
1165.3 EDMBPT (CellData Megabyte per Timestep (RW))
250.098 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.409761 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
465.936 RT (REAL_TIME)
3818449238 CP (Cells processed)
4.65936 ASPT (Averaged Seconds per Timestep)
38.1845 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4816.68 ACPST (Averaged number of clusters per Simulation Timestep)
8.19522 MCPS (Million Cells per Second) (local)
3818.45 MCP (Million Cells processed) (local)
1033.76 CPSPT (Clusters per Second per Thread)
1165.3 EDMBPT (CellData Megabyte per Timestep (RW))
250.098 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.409761 GFLOPS


++++++++++ SUMMARY ++++++++++
16.3904 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
1027.37 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

360448 MSNGCells (Migration Sent Number of Global Cells)
44 MSNGCluster (Migration Sent Number of Global Cluster)
4.71982e-05 MRC (Migrated Relative Cells)
0.819522 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4807	38505078	3.85036e+07	-3.89834e-05
1	4865	38502076	3.85036e+07	3.89834e-05
