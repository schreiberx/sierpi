
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
99.7537 RT (REAL_TIME)
3818417760 CP (Cells processed)
0.997537 ASPT (Averaged Seconds per Timestep)
38.1842 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1264.7 ACPST (Averaged number of clusters per Simulation Timestep)
38.2785 MCPS (Million Cells per Second) (local)
3818.42 MCP (Million Cells processed) (local)
158.478 CPSPT (Clusters per Second per Thread)
1165.29 EDMBPT (CellData Megabyte per Timestep (RW))
1168.17 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.91392 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
99.7537 RT (REAL_TIME)
3818478926 CP (Cells processed)
0.997537 ASPT (Averaged Seconds per Timestep)
38.1848 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1266 ACPST (Averaged number of clusters per Simulation Timestep)
38.2791 MCPS (Million Cells per Second) (local)
3818.48 MCP (Million Cells processed) (local)
158.641 CPSPT (Clusters per Second per Thread)
1165.31 EDMBPT (CellData Megabyte per Timestep (RW))
1168.18 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.91395 GFLOPS


++++++++++ SUMMARY ++++++++++
76.5575 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
19.8199 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

361472 MSNGCells (Migration Sent Number of Global Cells)
292 MSNGCluster (Migration Sent Number of Global Cluster)
4.73319e-05 MRC (Migrated Relative Cells)
3.82788 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1686	38503030	3.85036e+07	1.42065e-05
1	1497	38504124	3.85036e+07	-1.42065e-05
