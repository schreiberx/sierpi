
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
143.965 RT (REAL_TIME)
3818400368 CP (Cells processed)
1.43965 ASPT (Averaged Seconds per Timestep)
38.184 MCPPT (Million Cells Processed in Average per Simulation Timestep)
659.68 ACPST (Averaged number of clusters per Simulation Timestep)
26.5231 MCPS (Million Cells per Second) (local)
3818.4 MCP (Million Cells processed) (local)
114.555 CPSPT (Clusters per Second per Thread)
1165.28 EDMBPT (CellData Megabyte per Timestep (RW))
809.42 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.32615 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
143.965 RT (REAL_TIME)
3818496322 CP (Cells processed)
1.43965 ASPT (Averaged Seconds per Timestep)
38.185 MCPPT (Million Cells Processed in Average per Simulation Timestep)
665.3 ACPST (Averaged number of clusters per Simulation Timestep)
26.5237 MCPS (Million Cells per Second) (local)
3818.5 MCP (Million Cells processed) (local)
115.531 CPSPT (Clusters per Second per Thread)
1165.31 EDMBPT (CellData Megabyte per Timestep (RW))
809.44 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.32619 GFLOPS


++++++++++ SUMMARY ++++++++++
53.0468 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
28.7609 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

361472 MSNGCells (Migration Sent Number of Global Cells)
242 MSNGCluster (Migration Sent Number of Global Cluster)
4.73317e-05 MRC (Migrated Relative Cells)
2.65234 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	897	38503030	3.85036e+07	1.42065e-05
1	787	38504124	3.85036e+07	-1.42065e-05
