
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
78.7297 RT (REAL_TIME)
7636896720 CP (Cells processed)
0.787297 ASPT (Averaged Seconds per Timestep)
76.369 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9573.8 ACPST (Averaged number of clusters per Simulation Timestep)
97.0015 MCPS (Million Cells per Second) (local)
7636.9 MCP (Million Cells processed) (local)
760.022 CPSPT (Clusters per Second per Thread)
2330.6 EDMBPT (CellData Megabyte per Timestep (RW))
2960.25 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.85008 GFLOPS

++++++++++ SUMMARY ++++++++++
97.0015 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
47.5014 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
4.85008 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9672	77007154	7.70072e+07	0
