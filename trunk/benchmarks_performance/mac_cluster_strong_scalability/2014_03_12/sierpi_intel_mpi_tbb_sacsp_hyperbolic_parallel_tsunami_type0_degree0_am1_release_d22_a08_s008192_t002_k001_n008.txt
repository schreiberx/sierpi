
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954609971 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.5461 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1213.26 ACPST (Averaged number of clusters per Simulation Timestep)
13.7696 MCPS (Million Cells per Second) (local)
954.61 MCP (Million Cells processed) (local)
875.023 CPSPT (Clusters per Second per Thread)
291.324 EDMBPT (CellData Megabyte per Timestep (RW))
420.215 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.68848 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954613760 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54614 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.3 ACPST (Averaged number of clusters per Simulation Timestep)
13.7697 MCPS (Million Cells per Second) (local)
954.614 MCP (Million Cells processed) (local)
840.433 CPSPT (Clusters per Second per Thread)
291.325 EDMBPT (CellData Megabyte per Timestep (RW))
420.217 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688483 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954621952 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54622 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
13.7698 MCPS (Million Cells per Second) (local)
954.622 MCP (Million Cells processed) (local)
840.44 CPSPT (Clusters per Second per Thread)
291.327 EDMBPT (CellData Megabyte per Timestep (RW))
420.22 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688489 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954601773 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1213.25 ACPST (Averaged number of clusters per Simulation Timestep)
13.7695 MCPS (Million Cells per Second) (local)
954.602 MCP (Million Cells processed) (local)
875.015 CPSPT (Clusters per Second per Thread)
291.321 EDMBPT (CellData Megabyte per Timestep (RW))
420.211 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688474 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954633168 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54633 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1205.86 ACPST (Averaged number of clusters per Simulation Timestep)
13.7699 MCPS (Million Cells per Second) (local)
954.633 MCP (Million Cells processed) (local)
869.686 CPSPT (Clusters per Second per Thread)
291.331 EDMBPT (CellData Megabyte per Timestep (RW))
420.225 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688497 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954591412 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54591 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1202.48 ACPST (Averaged number of clusters per Simulation Timestep)
13.7693 MCPS (Million Cells per Second) (local)
954.591 MCP (Million Cells processed) (local)
867.248 CPSPT (Clusters per Second per Thread)
291.318 EDMBPT (CellData Megabyte per Timestep (RW))
420.207 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688467 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954616004 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54616 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1202.51 ACPST (Averaged number of clusters per Simulation Timestep)
13.7697 MCPS (Million Cells per Second) (local)
954.616 MCP (Million Cells processed) (local)
867.27 CPSPT (Clusters per Second per Thread)
291.326 EDMBPT (CellData Megabyte per Timestep (RW))
420.218 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688484 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
69.3273 RT (REAL_TIME)
954608686 CP (Cells processed)
0.693273 ASPT (Averaged Seconds per Timestep)
9.54609 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1205.83 ACPST (Averaged number of clusters per Simulation Timestep)
13.7696 MCPS (Million Cells per Second) (local)
954.609 MCP (Million Cells processed) (local)
869.664 CPSPT (Clusters per Second per Thread)
291.323 EDMBPT (CellData Megabyte per Timestep (RW))
420.214 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.688479 GFLOPS


++++++++++ SUMMARY ++++++++++
110.157 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
431.549 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1769472 MSNGCells (Migration Sent Number of Global Cells)
216 MSNGCluster (Migration Sent Number of Global Cluster)
0.000231701 MRC (Migrated Relative Cells)
5.50785 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1228	9622843	9.62589e+06	0.000316984
1	1175	9625600	9.62589e+06	3.05686e-05
2	1175	9625600	9.62589e+06	3.05686e-05
3	1229	9631035	9.62589e+06	-0.000534054
4	1218	9623435	9.62589e+06	0.000255483
5	1214	9623506	9.62589e+06	0.000248107
6	1215	9631698	9.62589e+06	-0.000602931
7	1218	9623437	9.62589e+06	0.000255275
