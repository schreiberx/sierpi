#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_11/benchmarks_performance/mac_cluster_strong_scalability/2014_03_12//sierpi_intel_mpi_omp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s008192_t002_k001_n004.txt
# error
#SBATCH -e /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_11/benchmarks_performance/mac_cluster_strong_scalability/2014_03_12//sierpi_intel_mpi_omp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s008192_t002_k001_n004.err
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_11/benchmarks_performance/mac_cluster_strong_scalability
# job description
#SBATCH -J sierpi_intel_mpi_omp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s008192_t002_k001_n004
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=4
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=24:00:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_11/benchmarks_performance/mac_cluster_strong_scalability

#export KMP_AFFINITY=granularity=fine,compact,1,0
mpiexec.hydra -genv OMP_NUM_THREADS 2 -envall -ppn 8 -n 4 ../../build/sierpi_intel_mpi_omp_hyperbolic_parallel_tsunami_type0_degree0_am1_release -d 22 -a 8 -t -1 -L 100 -k 1 -I 3 -r 0.01/0.005 -n 2 -N 2 -o 8192
