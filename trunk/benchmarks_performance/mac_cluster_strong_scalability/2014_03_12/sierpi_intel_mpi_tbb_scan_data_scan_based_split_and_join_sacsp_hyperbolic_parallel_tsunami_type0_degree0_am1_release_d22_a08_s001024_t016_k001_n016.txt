
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5576 RT (REAL_TIME)
477301254 CP (Cells processed)
0.375576 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2279.52 ACPST (Averaged number of clusters per Simulation Timestep)
12.7085 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
379.338 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
387.833 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635426 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5577 RT (REAL_TIME)
477320521 CP (Cells processed)
0.375577 ASPT (Averaged Seconds per Timestep)
4.77321 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1158.18 ACPST (Averaged number of clusters per Simulation Timestep)
12.709 MCPS (Million Cells per Second) (local)
477.321 MCP (Million Cells processed) (local)
192.734 CPSPT (Clusters per Second per Thread)
145.667 EDMBPT (CellData Megabyte per Timestep (RW))
387.848 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.63545 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5576 RT (REAL_TIME)
477296128 CP (Cells processed)
0.375576 ASPT (Averaged Seconds per Timestep)
4.77296 MCPPT (Million Cells Processed in Average per Simulation Timestep)
815.2 ACPST (Averaged number of clusters per Simulation Timestep)
12.7084 MCPS (Million Cells per Second) (local)
477.296 MCP (Million Cells processed) (local)
135.658 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
387.828 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635418 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5576 RT (REAL_TIME)
477285376 CP (Cells processed)
0.375576 ASPT (Averaged Seconds per Timestep)
4.77285 MCPPT (Million Cells Processed in Average per Simulation Timestep)
907.67 ACPST (Averaged number of clusters per Simulation Timestep)
12.7081 MCPS (Million Cells per Second) (local)
477.285 MCP (Million Cells processed) (local)
151.046 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
387.82 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635405 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5579 RT (REAL_TIME)
477300736 CP (Cells processed)
0.375579 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1435.82 ACPST (Averaged number of clusters per Simulation Timestep)
12.7084 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
238.934 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
387.829 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.63542 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5584 RT (REAL_TIME)
477300224 CP (Cells processed)
0.375584 ASPT (Averaged Seconds per Timestep)
4.773 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1665.22 ACPST (Averaged number of clusters per Simulation Timestep)
12.7082 MCPS (Million Cells per Second) (local)
477.3 MCP (Million Cells processed) (local)
277.105 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
387.824 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635411 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5585 RT (REAL_TIME)
477284352 CP (Cells processed)
0.375585 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1737.99 ACPST (Averaged number of clusters per Simulation Timestep)
12.7078 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
289.214 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
387.81 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635388 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5584 RT (REAL_TIME)
477349681 CP (Cells processed)
0.375584 ASPT (Averaged Seconds per Timestep)
4.7735 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1835.45 ACPST (Averaged number of clusters per Simulation Timestep)
12.7095 MCPS (Million Cells per Second) (local)
477.35 MCP (Million Cells processed) (local)
305.432 CPSPT (Clusters per Second per Thread)
145.676 EDMBPT (CellData Megabyte per Timestep (RW))
387.864 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635476 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5576 RT (REAL_TIME)
477310416 CP (Cells processed)
0.375576 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1434.02 ACPST (Averaged number of clusters per Simulation Timestep)
12.7087 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
238.637 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
387.84 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635437 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5572 RT (REAL_TIME)
477309952 CP (Cells processed)
0.375572 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1316.16 ACPST (Averaged number of clusters per Simulation Timestep)
12.7089 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
219.026 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
387.844 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635444 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5636 RT (REAL_TIME)
477294003 CP (Cells processed)
0.375636 ASPT (Averaged Seconds per Timestep)
4.77294 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1663 ACPST (Averaged number of clusters per Simulation Timestep)
12.7063 MCPS (Million Cells per Second) (local)
477.294 MCP (Million Cells processed) (local)
276.697 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
387.765 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635314 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5617 RT (REAL_TIME)
477303033 CP (Cells processed)
0.375617 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1394.5 ACPST (Averaged number of clusters per Simulation Timestep)
12.7072 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
232.035 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
387.792 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635359 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5579 RT (REAL_TIME)
477341892 CP (Cells processed)
0.375579 ASPT (Averaged Seconds per Timestep)
4.77342 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1221.55 ACPST (Averaged number of clusters per Simulation Timestep)
12.7095 MCPS (Million Cells per Second) (local)
477.342 MCP (Million Cells processed) (local)
203.278 CPSPT (Clusters per Second per Thread)
145.673 EDMBPT (CellData Megabyte per Timestep (RW))
387.863 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635475 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5579 RT (REAL_TIME)
477283840 CP (Cells processed)
0.375579 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
769.55 ACPST (Averaged number of clusters per Simulation Timestep)
12.7079 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
128.061 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
387.816 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635397 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5576 RT (REAL_TIME)
477310034 CP (Cells processed)
0.375576 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
940.11 ACPST (Averaged number of clusters per Simulation Timestep)
12.7087 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
156.445 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
387.84 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635437 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
37.5575 RT (REAL_TIME)
477305306 CP (Cells processed)
0.375575 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2262.69 ACPST (Averaged number of clusters per Simulation Timestep)
12.7086 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
376.537 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
387.837 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635432 GFLOPS


++++++++++ SUMMARY ++++++++++
203.334 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
14.8444 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3310 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446767 MRC (Migrated Relative Cells)
10.1667 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2398	4812932	4.81295e+06	3.14257e-06
1	1687	4811959	4.81295e+06	0.000205306
2	1081	4813824	4.81295e+06	-0.000182191
3	1273	4812800	4.81295e+06	3.05686e-05
4	2315	4813824	4.81295e+06	-0.000182191
5	2803	4812800	4.81295e+06	3.05686e-05
6	2916	4812800	4.81295e+06	3.05686e-05
7	3028	4812091	4.81295e+06	0.00017788
8	2109	4812683	4.81295e+06	5.4878e-05
9	2038	4812800	4.81295e+06	3.05686e-05
10	2694	4815457	4.81295e+06	-0.000521484
11	1841	4811121	4.81295e+06	0.000379419
12	1534	4812754	4.81295e+06	4.01261e-05
13	932	4813312	4.81295e+06	-7.58111e-05
14	1258	4812906	4.81295e+06	8.54466e-06
15	2342	4813091	4.81295e+06	-2.98933e-05
