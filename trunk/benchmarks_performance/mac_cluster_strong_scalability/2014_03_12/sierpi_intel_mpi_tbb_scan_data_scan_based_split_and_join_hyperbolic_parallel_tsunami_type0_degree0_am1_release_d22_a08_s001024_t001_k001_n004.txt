
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
336.752 RT (REAL_TIME)
1909200179 CP (Cells processed)
3.36752 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
209.87 ACPST (Averaged number of clusters per Simulation Timestep)
5.66945 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
62.3217 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
173.018 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283472 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
336.752 RT (REAL_TIME)
1909200173 CP (Cells processed)
3.36752 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
99.02 ACPST (Averaged number of clusters per Simulation Timestep)
5.66945 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
29.4044 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
173.018 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283472 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
336.753 RT (REAL_TIME)
1909248120 CP (Cells processed)
3.36753 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
194.65 ACPST (Averaged number of clusters per Simulation Timestep)
5.66959 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
57.8021 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
173.022 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.283479 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
336.752 RT (REAL_TIME)
1909248214 CP (Cells processed)
3.36752 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
183.4 ACPST (Averaged number of clusters per Simulation Timestep)
5.66959 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
54.4614 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
173.022 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.28348 GFLOPS


++++++++++ SUMMARY ++++++++++
22.6781 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
50.9974 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
718 MSNGCluster (Migration Sent Number of Global Cluster)
9.453e-05 MRC (Migrated Relative Cells)
1.1339 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	253	19251515	1.92518e+07	1.42324e-05
1	173	19251515	1.92518e+07	1.42324e-05
2	237	19252063	1.92518e+07	-1.42324e-05
3	182	19252063	1.92518e+07	-1.42324e-05
