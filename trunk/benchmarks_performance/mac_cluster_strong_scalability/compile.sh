#! /bin/bash

. ~/bin/scons_vars.sh

. inc_vars.sh

cd ../..

DEFAULT_OPTS="--compiler=intel --enable-gui=off --enable-mpi=on --mode=release --enable-libxml=off"
DEFAULT_OPTS+=" -j 4"

#DEFAULT_OPTS+=" --adaptive-cluster-stack-grow-extra-padding=4096  --adaptive-cluster-stack-shrink-extra-padding=4096"


#make clean

scons $DEFAULT_OPTS --threading=off
#scons $DEFAULT_OPTS --threading=off --enable-scan-based-split-and-join=on
#scons $DEFAULT_OPTS --threading=tbb
#scons $DEFAULT_OPTS --threading=tbb --enable-scan-based-split-and-join=on

scons $DEFAULT_OPTS --threading=omp --enable-skip-adaptive-conforming-clusters=on
scons $DEFAULT_OPTS --threading=tbb --enable-skip-adaptive-conforming-clusters=on
scons $DEFAULT_OPTS --threading=tbb --enable-scan-based-split-and-join=on --enable-skip-adaptive-conforming-clusters=on

#scons $DEFAULT_OPTS --threading=tbb
#scons $DEFAULT_OPTS --threading=omp

