
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
1890.84 RT (REAL_TIME)
30391556400 CP (Cells processed)
18.9084 ASPT (Averaged Seconds per Timestep)
303.916 MCPPT (Million Cells Processed in Average per Simulation Timestep)
37569.6 ACPST (Averaged number of clusters per Simulation Timestep)
16.0731 MCPS (Million Cells per Second) (local)
30391.6 MCP (Million Cells processed) (local)
993.466 CPSPT (Clusters per Second per Thread)
9274.77 EDMBPT (CellData Megabyte per Timestep (RW))
490.511 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.803654 GFLOPS

++++++++++ SUMMARY ++++++++++
16.0731 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
496.733 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.803654 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	37744	305186938	3.05187e+08	0
