
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
90.4678 RT (REAL_TIME)
7597860917 CP (Cells processed)
0.904678 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2855.11 ACPST (Averaged number of clusters per Simulation Timestep)
83.9841 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
197.246 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
2562.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.19921 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
90.4682 RT (REAL_TIME)
7597856901 CP (Cells processed)
0.904682 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2413.96 ACPST (Averaged number of clusters per Simulation Timestep)
83.9837 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
166.768 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
2562.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.19919 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
90.468 RT (REAL_TIME)
7597922769 CP (Cells processed)
0.90468 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2542.21 ACPST (Averaged number of clusters per Simulation Timestep)
83.9847 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
175.629 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
2563.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.19923 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
90.468 RT (REAL_TIME)
7597915815 CP (Cells processed)
0.90468 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2972.52 ACPST (Averaged number of clusters per Simulation Timestep)
83.9846 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
205.357 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
2563.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
4.19923 GFLOPS


++++++++++ SUMMARY ++++++++++
335.937 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
11.6406 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1512448 MSNGCells (Migration Sent Number of Global Cells)
842 MSNGCluster (Migration Sent Number of Global Cluster)
4.97653e-05 MRC (Migrated Relative Cells)
16.7969 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	3594	76296999	7.62967e+07	-3.46673e-06
1	3870	76296993	7.62967e+07	-3.38809e-06
2	3709	76296478	7.62967e+07	3.36187e-06
3	3442	76296468	7.62967e+07	3.49294e-06
