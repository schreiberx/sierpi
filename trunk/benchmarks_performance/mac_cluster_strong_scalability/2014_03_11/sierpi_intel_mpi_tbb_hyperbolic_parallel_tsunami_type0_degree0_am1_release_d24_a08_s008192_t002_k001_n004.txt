
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
755.558 RT (REAL_TIME)
7597888553 CP (Cells processed)
7.55558 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.72 ACPST (Averaged number of clusters per Simulation Timestep)
10.056 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
620.053 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
306.885 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.5028 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
755.559 RT (REAL_TIME)
7597880459 CP (Cells processed)
7.55559 ASPT (Averaged Seconds per Timestep)
75.9788 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.71 ACPST (Averaged number of clusters per Simulation Timestep)
10.056 MCPS (Million Cells per Second) (local)
7597.88 MCP (Million Cells processed) (local)
620.051 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
306.884 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.502798 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
755.56 RT (REAL_TIME)
7597898191 CP (Cells processed)
7.5556 ASPT (Averaged Seconds per Timestep)
75.979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.1 ACPST (Averaged number of clusters per Simulation Timestep)
10.056 MCPS (Million Cells per Second) (local)
7597.9 MCP (Million Cells processed) (local)
623.055 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
306.884 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.502799 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
755.558 RT (REAL_TIME)
7597889197 CP (Cells processed)
7.55558 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.08 ACPST (Averaged number of clusters per Simulation Timestep)
10.056 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
623.055 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
306.885 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.5028 GFLOPS


++++++++++ SUMMARY ++++++++++
40.2239 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
310.777 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1441792 MSNGCells (Migration Sent Number of Global Cells)
176 MSNGCluster (Migration Sent Number of Global Cluster)
4.74405e-05 MRC (Migrated Relative Cells)
2.0112 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9411	76296997	7.62967e+07	-3.44051e-06
1	9411	76296995	7.62967e+07	-3.4143e-06
2	9461	76296478	7.62967e+07	3.36187e-06
3	9461	76296468	7.62967e+07	3.49294e-06
