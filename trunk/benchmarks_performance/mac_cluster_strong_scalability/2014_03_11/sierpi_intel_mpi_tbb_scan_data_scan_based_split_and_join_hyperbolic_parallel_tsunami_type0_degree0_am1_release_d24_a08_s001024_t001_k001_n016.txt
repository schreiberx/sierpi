
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899468804 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
514.31 ACPST (Averaged number of clusters per Simulation Timestep)
4.92427 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
133.332 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
150.277 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246214 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899476529 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
73.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.9243 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
19.0597 CPSPT (Clusters per Second per Thread)
579.674 EDMBPT (CellData Megabyte per Timestep (RW))
150.278 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246215 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899469312 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
177.46 ACPST (Averaged number of clusters per Simulation Timestep)
4.92428 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
46.0056 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
150.277 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246214 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899447296 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
169.54 ACPST (Averaged number of clusters per Simulation Timestep)
4.92422 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
43.9524 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
150.275 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246211 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899393024 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9939 MCPPT (Million Cells Processed in Average per Simulation Timestep)
186.57 ACPST (Averaged number of clusters per Simulation Timestep)
4.92408 MCPS (Million Cells per Second) (local)
1899.39 MCP (Million Cells processed) (local)
48.3673 CPSPT (Clusters per Second per Thread)
579.649 EDMBPT (CellData Megabyte per Timestep (RW))
150.271 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246204 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899496960 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
211.94 ACPST (Averaged number of clusters per Simulation Timestep)
4.92435 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
54.9443 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
150.279 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246217 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899489280 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
133.21 ACPST (Averaged number of clusters per Simulation Timestep)
4.92433 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
34.534 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
150.279 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246216 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899484293 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
221.45 ACPST (Averaged number of clusters per Simulation Timestep)
4.92431 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
57.4097 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
150.278 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246216 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899498371 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
197.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.92435 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
51.206 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
150.279 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246218 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899487232 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
170.46 ACPST (Averaged number of clusters per Simulation Timestep)
4.92432 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
44.1909 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
150.278 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246216 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899436154 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
181.71 ACPST (Averaged number of clusters per Simulation Timestep)
4.92419 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
47.1074 CPSPT (Clusters per Second per Thread)
579.662 EDMBPT (CellData Megabyte per Timestep (RW))
150.274 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.24621 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899493326 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
251.13 ACPST (Averaged number of clusters per Simulation Timestep)
4.92434 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
65.1041 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
150.279 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246217 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899529680 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9953 MCPPT (Million Cells Processed in Average per Simulation Timestep)
150.24 ACPST (Averaged number of clusters per Simulation Timestep)
4.92443 MCPS (Million Cells per Second) (local)
1899.53 MCP (Million Cells processed) (local)
38.9489 CPSPT (Clusters per Second per Thread)
579.69 EDMBPT (CellData Megabyte per Timestep (RW))
150.282 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246222 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899434496 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
170.7 ACPST (Averaged number of clusters per Simulation Timestep)
4.92418 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
44.2531 CPSPT (Clusters per Second per Thread)
579.661 EDMBPT (CellData Megabyte per Timestep (RW))
150.274 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246209 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899484455 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
81.99 ACPST (Averaged number of clusters per Simulation Timestep)
4.92432 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
21.2555 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
150.278 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246216 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
385.736 RT (REAL_TIME)
1899467184 CP (Cells processed)
3.85736 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
500.62 ACPST (Averaged number of clusters per Simulation Timestep)
4.92427 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
129.783 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
150.277 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246214 GFLOPS


++++++++++ SUMMARY ++++++++++
78.7885 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
54.9659 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6857728 MSNGCells (Migration Sent Number of Global Cells)
3444 MSNGCluster (Migration Sent Number of Global Cluster)
0.000225645 MRC (Migrated Relative Cells)
3.93943 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	547	19074876	1.90742e+07	-3.62991e-05
1	74	19074027	1.90742e+07	8.21136e-06
2	248	19073536	1.90742e+07	3.3953e-05
3	244	19074560	1.90742e+07	-1.97322e-05
4	286	19070976	1.90742e+07	0.000168166
5	313	19079168	1.90742e+07	-0.000261315
6	244	19070976	1.90742e+07	0.000168166
7	326	19074849	1.90742e+07	-3.48835e-05
8	244	19074189	1.90742e+07	-2.81794e-07
9	262	19073024	1.90742e+07	6.07955e-05
10	271	19073843	1.90742e+07	1.78579e-05
11	327	19076446	1.90742e+07	-0.000118609
12	149	19074193	1.90742e+07	-4.91502e-07
13	250	19074048	1.90742e+07	7.1104e-06
14	80	19074326	1.90742e+07	-7.46428e-06
15	518	19073901	1.90742e+07	1.48171e-05
