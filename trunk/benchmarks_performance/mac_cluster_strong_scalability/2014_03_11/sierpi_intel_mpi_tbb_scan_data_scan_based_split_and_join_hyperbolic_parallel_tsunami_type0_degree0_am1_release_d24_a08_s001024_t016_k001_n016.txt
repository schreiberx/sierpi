
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6467 RT (REAL_TIME)
1899468806 CP (Cells processed)
0.606467 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4323.11 ACPST (Averaged number of clusters per Simulation Timestep)
31.3203 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
445.522 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
955.818 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56601 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6466 RT (REAL_TIME)
1899479601 CP (Cells processed)
0.606466 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1832.65 ACPST (Averaged number of clusters per Simulation Timestep)
31.3205 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
188.866 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
955.825 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56602 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6464 RT (REAL_TIME)
1899466752 CP (Cells processed)
0.606464 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1290.93 ACPST (Averaged number of clusters per Simulation Timestep)
31.3203 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
133.039 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
955.821 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56602 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6466 RT (REAL_TIME)
1899445760 CP (Cells processed)
0.606466 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1470.78 ACPST (Averaged number of clusters per Simulation Timestep)
31.3199 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
151.573 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
955.808 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.566 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6465 RT (REAL_TIME)
1899394048 CP (Cells processed)
0.606465 ASPT (Averaged Seconds per Timestep)
18.9939 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1982.05 ACPST (Averaged number of clusters per Simulation Timestep)
31.3191 MCPS (Million Cells per Second) (local)
1899.39 MCP (Million Cells processed) (local)
204.263 CPSPT (Clusters per Second per Thread)
579.649 EDMBPT (CellData Megabyte per Timestep (RW))
955.783 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56595 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6472 RT (REAL_TIME)
1899459072 CP (Cells processed)
0.606472 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2041.3 ACPST (Averaged number of clusters per Simulation Timestep)
31.3198 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
210.366 CPSPT (Clusters per Second per Thread)
579.669 EDMBPT (CellData Megabyte per Timestep (RW))
955.804 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56599 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6473 RT (REAL_TIME)
1899494400 CP (Cells processed)
0.606473 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2042.34 ACPST (Averaged number of clusters per Simulation Timestep)
31.3204 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
210.473 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
955.821 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56602 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6473 RT (REAL_TIME)
1899553413 CP (Cells processed)
0.606473 ASPT (Averaged Seconds per Timestep)
18.9955 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2239.58 ACPST (Averaged number of clusters per Simulation Timestep)
31.3213 MCPS (Million Cells per Second) (local)
1899.55 MCP (Million Cells processed) (local)
230.8 CPSPT (Clusters per Second per Thread)
579.698 EDMBPT (CellData Megabyte per Timestep (RW))
955.851 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56607 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6471 RT (REAL_TIME)
1899462025 CP (Cells processed)
0.606471 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2221.36 ACPST (Averaged number of clusters per Simulation Timestep)
31.3199 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
228.923 CPSPT (Clusters per Second per Thread)
579.67 EDMBPT (CellData Megabyte per Timestep (RW))
955.808 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.566 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6467 RT (REAL_TIME)
1899497472 CP (Cells processed)
0.606467 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1934.84 ACPST (Averaged number of clusters per Simulation Timestep)
31.3207 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
199.397 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
955.832 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56604 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6463 RT (REAL_TIME)
1899426426 CP (Cells processed)
0.606463 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2178.24 ACPST (Averaged number of clusters per Simulation Timestep)
31.3197 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
224.482 CPSPT (Clusters per Second per Thread)
579.659 EDMBPT (CellData Megabyte per Timestep (RW))
955.803 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56599 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6468 RT (REAL_TIME)
1899492814 CP (Cells processed)
0.606468 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2346.54 ACPST (Averaged number of clusters per Simulation Timestep)
31.3206 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
241.824 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
955.828 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56603 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6467 RT (REAL_TIME)
1899529680 CP (Cells processed)
0.606467 ASPT (Averaged Seconds per Timestep)
18.9953 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2041.63 ACPST (Averaged number of clusters per Simulation Timestep)
31.3213 MCPS (Million Cells per Second) (local)
1899.53 MCP (Million Cells processed) (local)
210.402 CPSPT (Clusters per Second per Thread)
579.69 EDMBPT (CellData Megabyte per Timestep (RW))
955.849 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56606 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6467 RT (REAL_TIME)
1899434496 CP (Cells processed)
0.606467 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1191.57 ACPST (Averaged number of clusters per Simulation Timestep)
31.3197 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
122.798 CPSPT (Clusters per Second per Thread)
579.661 EDMBPT (CellData Megabyte per Timestep (RW))
955.801 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56598 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6466 RT (REAL_TIME)
1899485991 CP (Cells processed)
0.606466 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1539.78 ACPST (Averaged number of clusters per Simulation Timestep)
31.3205 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
158.684 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
955.827 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56603 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
60.6461 RT (REAL_TIME)
1899465648 CP (Cells processed)
0.606461 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4262.04 ACPST (Averaged number of clusters per Simulation Timestep)
31.3205 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
439.233 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
955.826 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.56602 GFLOPS


++++++++++ SUMMARY ++++++++++
501.124 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
14.065 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6827008 MSNGCells (Migration Sent Number of Global Cells)
3654 MSNGCluster (Migration Sent Number of Global Cluster)
0.000224633 MRC (Migrated Relative Cells)
25.0562 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4666	19074874	1.90742e+07	-3.62008e-05
1	2813	19074027	1.90742e+07	8.20481e-06
2	1835	19073536	1.90742e+07	3.39464e-05
3	2189	19074560	1.90742e+07	-1.97387e-05
4	3270	19070976	1.90742e+07	0.000168159
5	3343	19079168	1.90742e+07	-0.000261322
6	3366	19070976	1.90742e+07	0.000168159
7	3657	19075873	1.90742e+07	-8.85752e-05
8	3335	19073165	1.90742e+07	5.33968e-05
9	3114	19073024	1.90742e+07	6.0789e-05
10	3554	19073843	1.90742e+07	1.78514e-05
11	3253	19076446	1.90742e+07	-0.000118616
12	2684	19074193	1.90742e+07	-4.98055e-07
13	1554	19074048	1.90742e+07	7.10384e-06
14	2217	19074326	1.90742e+07	-7.47083e-06
15	4536	19073901	1.90742e+07	1.48106e-05
