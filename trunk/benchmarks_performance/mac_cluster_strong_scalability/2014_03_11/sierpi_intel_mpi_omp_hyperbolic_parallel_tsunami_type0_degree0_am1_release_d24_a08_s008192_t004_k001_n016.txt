
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899453444 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2339.92 ACPST (Averaged number of clusters per Simulation Timestep)
16.2799 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
501.375 CPSPT (Clusters per Second per Thread)
579.667 EDMBPT (CellData Megabyte per Timestep (RW))
496.822 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.813993 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899485233 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2392.41 ACPST (Averaged number of clusters per Simulation Timestep)
16.2801 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
512.623 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
496.83 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814007 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899446272 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.66 ACPST (Averaged number of clusters per Simulation Timestep)
16.2798 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
496.82 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
496.82 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.81399 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899503616 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.73 ACPST (Averaged number of clusters per Simulation Timestep)
16.2803 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
496.835 CPSPT (Clusters per Second per Thread)
579.683 EDMBPT (CellData Megabyte per Timestep (RW))
496.835 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814015 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899479040 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
16.2801 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
496.831 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
496.831 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814007 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899462656 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.68 ACPST (Averaged number of clusters per Simulation Timestep)
16.2799 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
496.823 CPSPT (Clusters per Second per Thread)
579.67 EDMBPT (CellData Megabyte per Timestep (RW))
496.823 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.813995 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899479040 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
16.2801 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
496.828 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
496.828 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814003 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.676 RT (REAL_TIME)
1899459737 CP (Cells processed)
1.16676 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2413.63 ACPST (Averaged number of clusters per Simulation Timestep)
16.2798 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
517.164 CPSPT (Clusters per Second per Thread)
579.669 EDMBPT (CellData Megabyte per Timestep (RW))
496.819 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.813988 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899489169 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2390.5 ACPST (Averaged number of clusters per Simulation Timestep)
16.2801 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
512.211 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
496.829 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814005 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.674 RT (REAL_TIME)
1899487232 CP (Cells processed)
1.16674 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.71 ACPST (Averaged number of clusters per Simulation Timestep)
16.2802 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
496.833 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
496.833 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814011 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899430006 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2343.66 ACPST (Averaged number of clusters per Simulation Timestep)
16.2796 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
502.176 CPSPT (Clusters per Second per Thread)
579.66 EDMBPT (CellData Megabyte per Timestep (RW))
496.815 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.813982 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899491794 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2362.23 ACPST (Averaged number of clusters per Simulation Timestep)
16.2802 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
506.155 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
496.831 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814008 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.674 RT (REAL_TIME)
1899499976 CP (Cells processed)
1.16674 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2387.25 ACPST (Averaged number of clusters per Simulation Timestep)
16.2803 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
511.519 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
496.837 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814017 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.674 RT (REAL_TIME)
1899438080 CP (Cells processed)
1.16674 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.65 ACPST (Averaged number of clusters per Simulation Timestep)
16.2798 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
496.82 CPSPT (Clusters per Second per Thread)
579.663 EDMBPT (CellData Megabyte per Timestep (RW))
496.82 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.813991 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899506981 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9951 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2365.07 ACPST (Averaged number of clusters per Simulation Timestep)
16.2803 MCPS (Million Cells per Second) (local)
1899.51 MCP (Million Cells processed) (local)
506.764 CPSPT (Clusters per Second per Thread)
579.684 EDMBPT (CellData Megabyte per Timestep (RW))
496.836 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.814016 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
116.675 RT (REAL_TIME)
1899444148 CP (Cells processed)
1.16675 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2344.11 ACPST (Averaged number of clusters per Simulation Timestep)
16.2798 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
502.274 CPSPT (Clusters per Second per Thread)
579.664 EDMBPT (CellData Megabyte per Timestep (RW))
496.82 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.81399 GFLOPS


++++++++++ SUMMARY ++++++++++
260.48 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
125.782 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6799360 MSNGCells (Migration Sent Number of Global Cells)
830 MSNGCluster (Migration Sent Number of Global Cluster)
0.000223725 MRC (Migrated Relative Cells)
13.024 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2350	19076924	1.90742e+07	-0.000143669
1	2404	19069931	1.90742e+07	0.000222952
2	2329	19079168	1.90742e+07	-0.000261315
3	2328	19070976	1.90742e+07	0.000168166
4	2328	19070976	1.90742e+07	0.000168166
5	2329	19079168	1.90742e+07	-0.000261315
6	2328	19070976	1.90742e+07	0.000168166
7	2426	19075873	1.90742e+07	-8.85687e-05
8	2399	19075213	1.90742e+07	-5.39669e-05
9	2328	19070976	1.90742e+07	0.000168166
10	2360	19077939	1.90742e+07	-0.000196883
11	2374	19072350	1.90742e+07	9.61312e-05
12	2405	19071121	1.90742e+07	0.000160564
13	2329	19079168	1.90742e+07	-0.000261315
14	2370	19068182	1.90742e+07	0.000314646
15	2357	19077997	1.90742e+07	-0.000199923
