
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899468806 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4323.11 ACPST (Averaged number of clusters per Simulation Timestep)
34.5457 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
491.403 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
1054.25 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72728 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899479601 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1832.65 ACPST (Averaged number of clusters per Simulation Timestep)
34.5459 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
208.315 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
1054.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72729 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9841 RT (REAL_TIME)
1899466752 CP (Cells processed)
0.549841 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1290.93 ACPST (Averaged number of clusters per Simulation Timestep)
34.5458 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
146.739 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
1054.25 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72729 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899445760 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1470.78 ACPST (Averaged number of clusters per Simulation Timestep)
34.5452 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
167.182 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
1054.24 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72726 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9844 RT (REAL_TIME)
1899394048 CP (Cells processed)
0.549844 ASPT (Averaged Seconds per Timestep)
18.9939 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1982.05 ACPST (Averaged number of clusters per Simulation Timestep)
34.5443 MCPS (Million Cells per Second) (local)
1899.39 MCP (Million Cells processed) (local)
225.297 CPSPT (Clusters per Second per Thread)
579.649 EDMBPT (CellData Megabyte per Timestep (RW))
1054.21 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72721 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9853 RT (REAL_TIME)
1899459072 CP (Cells processed)
0.549853 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2041.3 ACPST (Averaged number of clusters per Simulation Timestep)
34.5448 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
232.028 CPSPT (Clusters per Second per Thread)
579.669 EDMBPT (CellData Megabyte per Timestep (RW))
1054.22 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72724 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9855 RT (REAL_TIME)
1899494400 CP (Cells processed)
0.549855 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2042.34 ACPST (Averaged number of clusters per Simulation Timestep)
34.5453 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
232.145 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
1054.24 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72727 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9858 RT (REAL_TIME)
1899553413 CP (Cells processed)
0.549858 ASPT (Averaged Seconds per Timestep)
18.9955 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2239.58 ACPST (Averaged number of clusters per Simulation Timestep)
34.5463 MCPS (Million Cells per Second) (local)
1899.55 MCP (Million Cells processed) (local)
254.563 CPSPT (Clusters per Second per Thread)
579.698 EDMBPT (CellData Megabyte per Timestep (RW))
1054.27 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72731 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9847 RT (REAL_TIME)
1899462025 CP (Cells processed)
0.549847 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2221.36 ACPST (Averaged number of clusters per Simulation Timestep)
34.5453 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
252.497 CPSPT (Clusters per Second per Thread)
579.67 EDMBPT (CellData Megabyte per Timestep (RW))
1054.24 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72726 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899497472 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1934.84 ACPST (Averaged number of clusters per Simulation Timestep)
34.5462 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
219.931 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
1054.27 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72731 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899426426 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2178.24 ACPST (Averaged number of clusters per Simulation Timestep)
34.5449 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
247.598 CPSPT (Clusters per Second per Thread)
579.659 EDMBPT (CellData Megabyte per Timestep (RW))
1054.23 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72725 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9845 RT (REAL_TIME)
1899492814 CP (Cells processed)
0.549845 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2346.54 ACPST (Averaged number of clusters per Simulation Timestep)
34.546 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
266.728 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
1054.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.7273 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899529680 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9953 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2041.63 ACPST (Averaged number of clusters per Simulation Timestep)
34.5468 MCPS (Million Cells per Second) (local)
1899.53 MCP (Million Cells processed) (local)
232.07 CPSPT (Clusters per Second per Thread)
579.69 EDMBPT (CellData Megabyte per Timestep (RW))
1054.28 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72734 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899434496 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1191.57 ACPST (Averaged number of clusters per Simulation Timestep)
34.545 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
135.444 CPSPT (Clusters per Second per Thread)
579.661 EDMBPT (CellData Megabyte per Timestep (RW))
1054.23 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.72725 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9843 RT (REAL_TIME)
1899485991 CP (Cells processed)
0.549843 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1539.78 ACPST (Averaged number of clusters per Simulation Timestep)
34.546 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
175.025 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
1054.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.7273 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
54.9838 RT (REAL_TIME)
1899465648 CP (Cells processed)
0.549838 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4262.04 ACPST (Averaged number of clusters per Simulation Timestep)
34.5459 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
484.466 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
1054.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.7273 GFLOPS


++++++++++ SUMMARY ++++++++++
552.729 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
15.5134 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6827008 MSNGCells (Migration Sent Number of Global Cells)
3654 MSNGCluster (Migration Sent Number of Global Cluster)
0.000224633 MRC (Migrated Relative Cells)
27.6365 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4666	19074874	1.90742e+07	-3.62008e-05
1	2813	19074027	1.90742e+07	8.20481e-06
2	1835	19073536	1.90742e+07	3.39464e-05
3	2189	19074560	1.90742e+07	-1.97387e-05
4	3270	19070976	1.90742e+07	0.000168159
5	3343	19079168	1.90742e+07	-0.000261322
6	3366	19070976	1.90742e+07	0.000168159
7	3657	19075873	1.90742e+07	-8.85752e-05
8	3335	19073165	1.90742e+07	5.33968e-05
9	3114	19073024	1.90742e+07	6.0789e-05
10	3554	19073843	1.90742e+07	1.78514e-05
11	3253	19076446	1.90742e+07	-0.000118616
12	2684	19074193	1.90742e+07	-4.98055e-07
13	1554	19074048	1.90742e+07	7.10384e-06
14	2217	19074326	1.90742e+07	-7.47083e-06
15	4536	19073901	1.90742e+07	1.48106e-05
