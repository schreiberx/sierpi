
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
1116.96 RT (REAL_TIME)
7597861941 CP (Cells processed)
11.1696 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
274.65 ACPST (Averaged number of clusters per Simulation Timestep)
6.80227 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
24.5891 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
207.589 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.340113 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
1116.96 RT (REAL_TIME)
7597862533 CP (Cells processed)
11.1696 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
130.51 ACPST (Averaged number of clusters per Simulation Timestep)
6.80227 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
11.6844 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
207.589 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.340113 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
1116.96 RT (REAL_TIME)
7597916107 CP (Cells processed)
11.1696 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
261.7 ACPST (Averaged number of clusters per Simulation Timestep)
6.80232 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
23.4297 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
207.59 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.340116 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
1116.96 RT (REAL_TIME)
7597915815 CP (Cells processed)
11.1696 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
219.13 ACPST (Averaged number of clusters per Simulation Timestep)
6.80232 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
19.6184 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
207.59 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.340116 GFLOPS


++++++++++ SUMMARY ++++++++++
27.2092 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
19.8304 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1543168 MSNGCells (Migration Sent Number of Global Cells)
796 MSNGCluster (Migration Sent Number of Global Cluster)
5.07761e-05 MRC (Migrated Relative Cells)
1.36046 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	350	76296999	7.62967e+07	-3.46673e-06
1	234	76296993	7.62967e+07	-3.38809e-06
2	336	76296478	7.62967e+07	3.36187e-06
3	218	76296468	7.62967e+07	3.49294e-06
