
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
301.636 RT (REAL_TIME)
7597861941 CP (Cells processed)
3.01636 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
880.56 ACPST (Averaged number of clusters per Simulation Timestep)
25.1888 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
72.9819 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
768.701 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.25944 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
301.637 RT (REAL_TIME)
7597875333 CP (Cells processed)
3.01637 ASPT (Averaged Seconds per Timestep)
75.9788 MCPPT (Million Cells Processed in Average per Simulation Timestep)
691.33 ACPST (Averaged number of clusters per Simulation Timestep)
25.1888 MCPS (Million Cells per Second) (local)
7597.88 MCP (Million Cells processed) (local)
57.2983 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
768.702 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.25944 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
301.637 RT (REAL_TIME)
7597903307 CP (Cells processed)
3.01637 ASPT (Averaged Seconds per Timestep)
75.979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
788.36 ACPST (Averaged number of clusters per Simulation Timestep)
25.1889 MCPS (Million Cells per Second) (local)
7597.9 MCP (Million Cells processed) (local)
65.3402 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
768.705 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.25945 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
301.636 RT (REAL_TIME)
7597915815 CP (Cells processed)
3.01636 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
890.08 ACPST (Averaged number of clusters per Simulation Timestep)
25.189 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
73.7709 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
768.707 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.25945 GFLOPS


++++++++++ SUMMARY ++++++++++
100.756 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
16.837 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1512448 MSNGCells (Migration Sent Number of Global Cells)
822 MSNGCluster (Migration Sent Number of Global Cluster)
4.97653e-05 MRC (Migrated Relative Cells)
5.03778 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1066	76296999	7.62967e+07	-3.46673e-06
1	1072	76296993	7.62967e+07	-3.38809e-06
2	1076	76296478	7.62967e+07	3.36187e-06
3	983	76296468	7.62967e+07	3.49294e-06
