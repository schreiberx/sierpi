
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0928 RT (REAL_TIME)
1899453430 CP (Cells processed)
0.680928 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2339.92 ACPST (Averaged number of clusters per Simulation Timestep)
27.8951 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
429.546 CPSPT (Clusters per Second per Thread)
579.667 EDMBPT (CellData Megabyte per Timestep (RW))
851.289 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39475 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0928 RT (REAL_TIME)
1899485235 CP (Cells processed)
0.680928 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2392.41 ACPST (Averaged number of clusters per Simulation Timestep)
27.8955 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
439.182 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
851.304 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39478 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0928 RT (REAL_TIME)
1899446272 CP (Cells processed)
0.680928 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.66 ACPST (Averaged number of clusters per Simulation Timestep)
27.8949 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
425.643 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
851.286 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39475 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0928 RT (REAL_TIME)
1899503616 CP (Cells processed)
0.680928 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.73 ACPST (Averaged number of clusters per Simulation Timestep)
27.8958 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
425.656 CPSPT (Clusters per Second per Thread)
579.683 EDMBPT (CellData Megabyte per Timestep (RW))
851.312 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39479 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0929 RT (REAL_TIME)
1899479040 CP (Cells processed)
0.680929 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
27.8954 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
425.65 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
851.301 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39477 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0937 RT (REAL_TIME)
1899462656 CP (Cells processed)
0.680937 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.68 ACPST (Averaged number of clusters per Simulation Timestep)
27.8948 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
425.642 CPSPT (Clusters per Second per Thread)
579.67 EDMBPT (CellData Megabyte per Timestep (RW))
851.283 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39474 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0935 RT (REAL_TIME)
1899479040 CP (Cells processed)
0.680935 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
27.8952 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
425.646 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
851.293 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39476 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0936 RT (REAL_TIME)
1899459723 CP (Cells processed)
0.680936 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2413.63 ACPST (Averaged number of clusters per Simulation Timestep)
27.8948 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
443.072 CPSPT (Clusters per Second per Thread)
579.669 EDMBPT (CellData Megabyte per Timestep (RW))
851.282 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39474 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0934 RT (REAL_TIME)
1899489167 CP (Cells processed)
0.680934 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2390.5 ACPST (Averaged number of clusters per Simulation Timestep)
27.8953 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
438.827 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
851.298 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39477 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0928 RT (REAL_TIME)
1899487232 CP (Cells processed)
0.680928 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.71 ACPST (Averaged number of clusters per Simulation Timestep)
27.8956 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
425.652 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
851.305 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39478 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.093 RT (REAL_TIME)
1899430006 CP (Cells processed)
0.68093 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2343.66 ACPST (Averaged number of clusters per Simulation Timestep)
27.8947 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
430.232 CPSPT (Clusters per Second per Thread)
579.66 EDMBPT (CellData Megabyte per Timestep (RW))
851.277 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39473 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.093 RT (REAL_TIME)
1899491786 CP (Cells processed)
0.68093 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2362.23 ACPST (Averaged number of clusters per Simulation Timestep)
27.8956 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
433.641 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
851.305 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39478 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0929 RT (REAL_TIME)
1899500014 CP (Cells processed)
0.680929 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2387.25 ACPST (Averaged number of clusters per Simulation Timestep)
27.8957 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
438.234 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
851.309 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39479 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0929 RT (REAL_TIME)
1899438080 CP (Cells processed)
0.680929 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.65 ACPST (Averaged number of clusters per Simulation Timestep)
27.8948 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
425.641 CPSPT (Clusters per Second per Thread)
579.663 EDMBPT (CellData Megabyte per Timestep (RW))
851.282 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39474 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0931 RT (REAL_TIME)
1899506961 CP (Cells processed)
0.680931 ASPT (Averaged Seconds per Timestep)
18.9951 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2365.07 ACPST (Averaged number of clusters per Simulation Timestep)
27.8957 MCPS (Million Cells per Second) (local)
1899.51 MCP (Million Cells processed) (local)
434.161 CPSPT (Clusters per Second per Thread)
579.684 EDMBPT (CellData Megabyte per Timestep (RW))
851.31 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39479 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
68.0931 RT (REAL_TIME)
1899444142 CP (Cells processed)
0.680931 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2344.11 ACPST (Averaged number of clusters per Simulation Timestep)
27.8948 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
430.314 CPSPT (Clusters per Second per Thread)
579.664 EDMBPT (CellData Megabyte per Timestep (RW))
851.283 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39474 GFLOPS


++++++++++ SUMMARY ++++++++++
446.324 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
53.8808 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6799360 MSNGCells (Migration Sent Number of Global Cells)
830 MSNGCluster (Migration Sent Number of Global Cluster)
0.000223725 MRC (Migrated Relative Cells)
22.3162 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2350	19076922	1.90742e+07	-0.000143564
1	2404	19069931	1.90742e+07	0.000222952
2	2329	19079168	1.90742e+07	-0.000261315
3	2328	19070976	1.90742e+07	0.000168166
4	2328	19070976	1.90742e+07	0.000168166
5	2329	19079168	1.90742e+07	-0.000261315
6	2328	19070976	1.90742e+07	0.000168166
7	2426	19075875	1.90742e+07	-8.86735e-05
8	2399	19075213	1.90742e+07	-5.39669e-05
9	2328	19070976	1.90742e+07	0.000168166
10	2360	19077939	1.90742e+07	-0.000196883
11	2374	19072350	1.90742e+07	9.61312e-05
12	2405	19071121	1.90742e+07	0.000160564
13	2329	19079168	1.90742e+07	-0.000261315
14	2370	19068182	1.90742e+07	0.000314646
15	2357	19077997	1.90742e+07	-0.000199923
