
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899468800 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
934.62 ACPST (Averaged number of clusters per Simulation Timestep)
11.3807 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
279.99 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
347.312 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569036 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899476531 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
221.89 ACPST (Averaged number of clusters per Simulation Timestep)
11.3808 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
66.473 CPSPT (Clusters per Second per Thread)
579.674 EDMBPT (CellData Megabyte per Timestep (RW))
347.314 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569039 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899469312 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
270.35 ACPST (Averaged number of clusters per Simulation Timestep)
11.3807 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
80.9905 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
347.312 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569036 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899447296 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
281.75 ACPST (Averaged number of clusters per Simulation Timestep)
11.3806 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
84.4056 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
347.308 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.56903 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899393024 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9939 MCPPT (Million Cells Processed in Average per Simulation Timestep)
318.6 ACPST (Averaged number of clusters per Simulation Timestep)
11.3803 MCPS (Million Cells per Second) (local)
1899.39 MCP (Million Cells processed) (local)
95.445 CPSPT (Clusters per Second per Thread)
579.649 EDMBPT (CellData Megabyte per Timestep (RW))
347.298 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569013 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.903 RT (REAL_TIME)
1899496960 CP (Cells processed)
1.66903 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
369.55 ACPST (Averaged number of clusters per Simulation Timestep)
11.3809 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
110.708 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
347.317 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569044 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899489280 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
333.44 ACPST (Averaged number of clusters per Simulation Timestep)
11.3808 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
99.8907 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
347.315 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569042 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.903 RT (REAL_TIME)
1899494527 CP (Cells processed)
1.66903 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
360.55 ACPST (Averaged number of clusters per Simulation Timestep)
11.3809 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
108.012 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
347.316 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569043 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899488141 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
395.29 ACPST (Averaged number of clusters per Simulation Timestep)
11.3808 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
118.419 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
347.315 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569041 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899487232 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
295.19 ACPST (Averaged number of clusters per Simulation Timestep)
11.3808 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
88.4321 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
347.316 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569042 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899440250 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
338.47 ACPST (Averaged number of clusters per Simulation Timestep)
11.3806 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
101.398 CPSPT (Clusters per Second per Thread)
579.663 EDMBPT (CellData Megabyte per Timestep (RW))
347.308 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569029 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899489238 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
403 ACPST (Averaged number of clusters per Simulation Timestep)
11.3809 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
120.729 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
347.316 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569043 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899529692 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9953 MCPPT (Million Cells Processed in Average per Simulation Timestep)
344.37 ACPST (Averaged number of clusters per Simulation Timestep)
11.3811 MCPS (Million Cells per Second) (local)
1899.53 MCP (Million Cells processed) (local)
103.165 CPSPT (Clusters per Second per Thread)
579.69 EDMBPT (CellData Megabyte per Timestep (RW))
347.323 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569055 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899434496 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
254.99 ACPST (Averaged number of clusters per Simulation Timestep)
11.3805 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
76.389 CPSPT (Clusters per Second per Thread)
579.661 EDMBPT (CellData Megabyte per Timestep (RW))
347.306 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569026 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899485991 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
227.51 ACPST (Averaged number of clusters per Simulation Timestep)
11.3808 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
68.1567 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
347.316 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569042 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.902 RT (REAL_TIME)
1899465644 CP (Cells processed)
1.66902 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
918.97 ACPST (Averaged number of clusters per Simulation Timestep)
11.3807 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
275.302 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
347.312 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569036 GFLOPS


++++++++++ SUMMARY ++++++++++
182.092 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
58.6846 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6857728 MSNGCells (Migration Sent Number of Global Cells)
3462 MSNGCluster (Migration Sent Number of Global Cluster)
0.000225644 MRC (Migrated Relative Cells)
9.1046 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1007	19074874	1.90742e+07	-3.62008e-05
1	309	19074027	1.90742e+07	8.20481e-06
2	373	19073536	1.90742e+07	3.39464e-05
3	402	19074560	1.90742e+07	-1.97387e-05
4	507	19070976	1.90742e+07	0.000168159
5	565	19079168	1.90742e+07	-0.000261322
6	540	19070976	1.90742e+07	0.000168159
7	559	19074849	1.90742e+07	-3.48901e-05
8	526	19074189	1.90742e+07	-2.88348e-07
9	466	19073024	1.90742e+07	6.0789e-05
10	522	19073843	1.90742e+07	1.78514e-05
11	521	19076446	1.90742e+07	-0.000118616
12	416	19074193	1.90742e+07	-4.98055e-07
13	341	19074048	1.90742e+07	7.10384e-06
14	293	19074326	1.90742e+07	-7.47083e-06
15	976	19073901	1.90742e+07	1.48106e-05
