
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
219.157 RT (REAL_TIME)
7597888553 CP (Cells processed)
2.19157 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.72 ACPST (Averaged number of clusters per Simulation Timestep)
34.6687 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
534.418 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1058 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.73343 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
219.16 RT (REAL_TIME)
7597880459 CP (Cells processed)
2.1916 ASPT (Averaged Seconds per Timestep)
75.9788 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.71 ACPST (Averaged number of clusters per Simulation Timestep)
34.6682 MCPS (Million Cells per Second) (local)
7597.88 MCP (Million Cells processed) (local)
534.41 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1057.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.73341 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
219.16 RT (REAL_TIME)
7597898191 CP (Cells processed)
2.1916 ASPT (Averaged Seconds per Timestep)
75.979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.1 ACPST (Averaged number of clusters per Simulation Timestep)
34.6683 MCPS (Million Cells per Second) (local)
7597.9 MCP (Million Cells processed) (local)
537 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1057.99 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.73342 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
219.158 RT (REAL_TIME)
7597889197 CP (Cells processed)
2.19158 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.08 ACPST (Averaged number of clusters per Simulation Timestep)
34.6686 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
537.003 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1058 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.73343 GFLOPS


++++++++++ SUMMARY ++++++++++
138.674 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
66.9635 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1441792 MSNGCells (Migration Sent Number of Global Cells)
176 MSNGCluster (Migration Sent Number of Global Cluster)
4.74405e-05 MRC (Migrated Relative Cells)
6.93369 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9411	76296997	7.62967e+07	-3.44051e-06
1	9411	76296995	7.62967e+07	-3.4143e-06
2	9461	76296478	7.62967e+07	3.36187e-06
3	9461	76296468	7.62967e+07	3.49294e-06
