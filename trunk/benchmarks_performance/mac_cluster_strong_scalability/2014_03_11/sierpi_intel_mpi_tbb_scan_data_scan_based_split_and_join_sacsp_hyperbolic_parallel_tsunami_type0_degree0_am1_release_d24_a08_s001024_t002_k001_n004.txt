
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
592.619 RT (REAL_TIME)
7597861939 CP (Cells processed)
5.92619 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
506.66 ACPST (Averaged number of clusters per Simulation Timestep)
12.8208 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
42.7475 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
391.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641041 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
592.619 RT (REAL_TIME)
7597862527 CP (Cells processed)
5.92619 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
345.79 ACPST (Averaged number of clusters per Simulation Timestep)
12.8208 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
29.1747 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
391.26 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641041 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
592.619 RT (REAL_TIME)
7597916125 CP (Cells processed)
5.92619 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
436.85 ACPST (Averaged number of clusters per Simulation Timestep)
12.8209 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
36.8575 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
391.263 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641045 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
592.619 RT (REAL_TIME)
7597915823 CP (Cells processed)
5.92619 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
444.77 ACPST (Averaged number of clusters per Simulation Timestep)
12.8209 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
37.5258 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
391.263 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.641045 GFLOPS


++++++++++ SUMMARY ++++++++++
51.2834 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
18.2882 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1543168 MSNGCells (Migration Sent Number of Global Cells)
796 MSNGCluster (Migration Sent Number of Global Cluster)
5.07761e-05 MRC (Migrated Relative Cells)
2.56417 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	653	76296997	7.62967e+07	-3.44707e-06
1	538	76296993	7.62967e+07	-3.39464e-06
2	596	76296478	7.62967e+07	3.35532e-06
3	476	76296468	7.62967e+07	3.48639e-06
