
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.533 RT (REAL_TIME)
7597888553 CP (Cells processed)
4.91533 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.72 ACPST (Averaged number of clusters per Simulation Timestep)
15.4575 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
953.113 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
471.727 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.772877 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.534 RT (REAL_TIME)
7597880459 CP (Cells processed)
4.91534 ASPT (Averaged Seconds per Timestep)
75.9788 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.71 ACPST (Averaged number of clusters per Simulation Timestep)
15.4575 MCPS (Million Cells per Second) (local)
7597.88 MCP (Million Cells processed) (local)
953.109 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
471.725 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.772874 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.534 RT (REAL_TIME)
7597898191 CP (Cells processed)
4.91534 ASPT (Averaged Seconds per Timestep)
75.979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.1 ACPST (Averaged number of clusters per Simulation Timestep)
15.4575 MCPS (Million Cells per Second) (local)
7597.9 MCP (Million Cells processed) (local)
957.726 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
471.726 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.772876 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.533 RT (REAL_TIME)
7597889197 CP (Cells processed)
4.91533 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.08 ACPST (Averaged number of clusters per Simulation Timestep)
15.4575 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
957.727 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
471.727 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.772877 GFLOPS


++++++++++ SUMMARY ++++++++++
61.8301 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
477.709 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1441792 MSNGCells (Migration Sent Number of Global Cells)
176 MSNGCluster (Migration Sent Number of Global Cluster)
4.74405e-05 MRC (Migrated Relative Cells)
3.09151 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9411	76296997	7.62967e+07	-3.44051e-06
1	9411	76296995	7.62967e+07	-3.4143e-06
2	9461	76296478	7.62967e+07	3.36187e-06
3	9461	76296468	7.62967e+07	3.49294e-06
