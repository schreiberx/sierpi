
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
120.736 RT (REAL_TIME)
7597888565 CP (Cells processed)
1.20736 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.72 ACPST (Averaged number of clusters per Simulation Timestep)
62.9297 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
485.031 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1920.46 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.14649 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
120.737 RT (REAL_TIME)
7597880473 CP (Cells processed)
1.20737 ASPT (Averaged Seconds per Timestep)
75.9788 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9369.71 ACPST (Averaged number of clusters per Simulation Timestep)
62.9292 MCPS (Million Cells per Second) (local)
7597.88 MCP (Million Cells processed) (local)
485.027 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1920.45 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.14646 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
120.737 RT (REAL_TIME)
7597898201 CP (Cells processed)
1.20737 ASPT (Averaged Seconds per Timestep)
75.979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.1 ACPST (Averaged number of clusters per Simulation Timestep)
62.9295 MCPS (Million Cells per Second) (local)
7597.9 MCP (Million Cells processed) (local)
487.378 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1920.46 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.14648 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
120.736 RT (REAL_TIME)
7597889185 CP (Cells processed)
1.20736 ASPT (Averaged Seconds per Timestep)
75.9789 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9415.08 ACPST (Averaged number of clusters per Simulation Timestep)
62.9299 MCPS (Million Cells per Second) (local)
7597.89 MCP (Million Cells processed) (local)
487.38 CPSPT (Clusters per Second per Thread)
2318.69 EDMBPT (CellData Megabyte per Timestep (RW))
1920.47 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
3.14649 GFLOPS


++++++++++ SUMMARY ++++++++++
251.718 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
30.3878 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1441792 MSNGCells (Migration Sent Number of Global Cells)
176 MSNGCluster (Migration Sent Number of Global Cluster)
4.74405e-05 MRC (Migrated Relative Cells)
12.5859 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	9411	76296999	7.62967e+07	-3.46673e-06
1	9411	76296993	7.62967e+07	-3.38809e-06
2	9461	76296478	7.62967e+07	3.36187e-06
3	9461	76296468	7.62967e+07	3.49294e-06
