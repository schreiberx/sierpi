Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
208.14 RT (REAL_TIME)
2068885831 CP (Cells processed)
2.0814 ASPT (Averaged Seconds per Timestep)
20.6889 MCPPT (Million Cells Processed in Average per Simulation Timestep)
246.64 ACPST (Averaged number of clusters per Simulation Timestep)
9.93988 MCPS (Million Cells per Second) (local)
2068.89 MCP (Million Cells processed) (local)
118.497 CPSPT (Clusters per Second per Thread)
631.374 EDMBPT (CellData Megabyte per Timestep (RW))
303.341 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.496994 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
208.14 RT (REAL_TIME)
1749184813 CP (Cells processed)
2.0814 ASPT (Averaged Seconds per Timestep)
17.4918 MCPPT (Million Cells Processed in Average per Simulation Timestep)
244.24 ACPST (Averaged number of clusters per Simulation Timestep)
8.40389 MCPS (Million Cells per Second) (local)
1749.18 MCP (Million Cells processed) (local)
117.344 CPSPT (Clusters per Second per Thread)
533.809 EDMBPT (CellData Megabyte per Timestep (RW))
256.466 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.420194 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
208.14 RT (REAL_TIME)
1913466028 CP (Cells processed)
2.0814 ASPT (Averaged Seconds per Timestep)
19.1347 MCPPT (Million Cells Processed in Average per Simulation Timestep)
416.63 ACPST (Averaged number of clusters per Simulation Timestep)
9.19317 MCPS (Million Cells per Second) (local)
1913.47 MCP (Million Cells processed) (local)
200.168 CPSPT (Clusters per Second per Thread)
583.943 EDMBPT (CellData Megabyte per Timestep (RW))
280.553 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.459659 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
208.14 RT (REAL_TIME)
1905360042 CP (Cells processed)
2.0814 ASPT (Averaged Seconds per Timestep)
19.0536 MCPPT (Million Cells Processed in Average per Simulation Timestep)
414.21 ACPST (Averaged number of clusters per Simulation Timestep)
9.15423 MCPS (Million Cells per Second) (local)
1905.36 MCP (Million Cells processed) (local)
199.006 CPSPT (Clusters per Second per Thread)
581.47 EDMBPT (CellData Megabyte per Timestep (RW))
279.365 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.457711 GFLOPS


++++++++++ SUMMARY ++++++++++
36.6912 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
158.754 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1869518 MSNGCells (Migration Sent Number of Global Cells)
12 MSNGCluster (Migration Sent Number of Global Cluster)
0.000244258 MRC (Migrated Relative Cells)
1.83456 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	288	20804923	1.92518e+07	-0.0806748
1	286	17659195	1.92518e+07	0.0827244
2	482	19271519	1.92518e+07	-0.00102487
3	480	19271517	1.92518e+07	-0.00102476
