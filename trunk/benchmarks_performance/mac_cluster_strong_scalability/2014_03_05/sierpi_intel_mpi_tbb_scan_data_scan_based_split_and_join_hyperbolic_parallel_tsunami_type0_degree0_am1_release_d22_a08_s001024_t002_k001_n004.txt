Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
189.58 RT (REAL_TIME)
1909200179 CP (Cells processed)
1.8958 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
381.76 ACPST (Averaged number of clusters per Simulation Timestep)
10.0707 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
100.686 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
307.332 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503534 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
189.58 RT (REAL_TIME)
1909200173 CP (Cells processed)
1.8958 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
292.34 ACPST (Averaged number of clusters per Simulation Timestep)
10.0707 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
77.1019 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
307.332 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503533 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
189.58 RT (REAL_TIME)
1909248120 CP (Cells processed)
1.8958 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
330.84 ACPST (Averaged number of clusters per Simulation Timestep)
10.0709 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
87.2559 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
307.34 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503546 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
189.58 RT (REAL_TIME)
1909248214 CP (Cells processed)
1.8958 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
354.4 ACPST (Averaged number of clusters per Simulation Timestep)
10.0709 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
93.4697 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
307.34 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503546 GFLOPS


++++++++++ SUMMARY ++++++++++
40.2832 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
44.8141 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
718 MSNGCluster (Migration Sent Number of Global Cluster)
9.453e-05 MRC (Migrated Relative Cells)
2.01416 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	459	19251515	1.92518e+07	1.42324e-05
1	453	19251515	1.92518e+07	1.42324e-05
2	438	19252063	1.92518e+07	-1.42324e-05
3	367	19252063	1.92518e+07	-1.42324e-05
