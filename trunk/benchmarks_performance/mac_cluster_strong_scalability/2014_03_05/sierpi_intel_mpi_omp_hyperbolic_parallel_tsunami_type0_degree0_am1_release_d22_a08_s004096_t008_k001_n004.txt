Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
59.3293 RT (REAL_TIME)
1909223751 CP (Cells processed)
0.593293 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4730.18 ACPST (Averaged number of clusters per Simulation Timestep)
32.1801 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
996.594 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
982.059 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.60901 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
59.331 RT (REAL_TIME)
1909219637 CP (Cells processed)
0.59331 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4730.17 ACPST (Averaged number of clusters per Simulation Timestep)
32.1791 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
996.564 CPSPT (Clusters per Second per Thread)
582.648 EDMBPT (CellData Megabyte per Timestep (RW))
982.029 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.60896 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
59.3305 RT (REAL_TIME)
1909228670 CP (Cells processed)
0.593305 ASPT (Averaged Seconds per Timestep)
19.0923 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4767.44 ACPST (Averaged number of clusters per Simulation Timestep)
32.1796 MCPS (Million Cells per Second) (local)
1909.23 MCP (Million Cells processed) (local)
1004.43 CPSPT (Clusters per Second per Thread)
582.65 EDMBPT (CellData Megabyte per Timestep (RW))
982.043 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.60898 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
59.3295 RT (REAL_TIME)
1909224666 CP (Cells processed)
0.593295 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
4767.43 ACPST (Averaged number of clusters per Simulation Timestep)
32.18 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
1004.44 CPSPT (Clusters per Second per Thread)
582.649 EDMBPT (CellData Megabyte per Timestep (RW))
982.056 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.609 GFLOPS


++++++++++ SUMMARY ++++++++++
128.719 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
125.063 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

737280 MSNGCells (Migration Sent Number of Global Cells)
180 MSNGCluster (Migration Sent Number of Global Cluster)
9.65418e-05 MRC (Migrated Relative Cells)
6.43594 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	4769	19252539	1.92518e+07	-3.89834e-05
1	4769	19252539	1.92518e+07	-3.89834e-05
2	4798	19251039	1.92518e+07	3.89314e-05
3	4798	19251037	1.92518e+07	3.90353e-05
