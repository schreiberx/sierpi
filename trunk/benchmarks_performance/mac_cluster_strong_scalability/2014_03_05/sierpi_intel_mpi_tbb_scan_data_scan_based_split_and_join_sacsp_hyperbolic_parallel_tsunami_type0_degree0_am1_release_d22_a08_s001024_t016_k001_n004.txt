Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.2738 RT (REAL_TIME)
2068885811 CP (Cells processed)
0.522738 ASPT (Averaged Seconds per Timestep)
20.6889 MCPPT (Million Cells Processed in Average per Simulation Timestep)
27.92 ACPST (Averaged number of clusters per Simulation Timestep)
39.5778 MCPS (Million Cells per Second) (local)
2068.89 MCP (Million Cells processed) (local)
3.33819 CPSPT (Clusters per Second per Thread)
631.374 EDMBPT (CellData Megabyte per Timestep (RW))
1207.82 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.97889 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.2738 RT (REAL_TIME)
1749184813 CP (Cells processed)
0.522738 ASPT (Averaged Seconds per Timestep)
17.4918 MCPPT (Million Cells Processed in Average per Simulation Timestep)
30.48 ACPST (Averaged number of clusters per Simulation Timestep)
33.462 MCPS (Million Cells per Second) (local)
1749.18 MCP (Million Cells processed) (local)
3.64427 CPSPT (Clusters per Second per Thread)
533.809 EDMBPT (CellData Megabyte per Timestep (RW))
1021.18 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.6731 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.2738 RT (REAL_TIME)
1913466020 CP (Cells processed)
0.522738 ASPT (Averaged Seconds per Timestep)
19.1347 MCPPT (Million Cells Processed in Average per Simulation Timestep)
74.81 ACPST (Averaged number of clusters per Simulation Timestep)
36.6047 MCPS (Million Cells per Second) (local)
1913.47 MCP (Million Cells processed) (local)
8.94449 CPSPT (Clusters per Second per Thread)
583.943 EDMBPT (CellData Megabyte per Timestep (RW))
1117.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.83023 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.2738 RT (REAL_TIME)
1905360042 CP (Cells processed)
0.522738 ASPT (Averaged Seconds per Timestep)
19.0536 MCPPT (Million Cells Processed in Average per Simulation Timestep)
64.01 ACPST (Averaged number of clusters per Simulation Timestep)
36.4496 MCPS (Million Cells per Second) (local)
1905.36 MCP (Million Cells processed) (local)
7.65322 CPSPT (Clusters per Second per Thread)
581.47 EDMBPT (CellData Megabyte per Timestep (RW))
1112.35 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.82248 GFLOPS


++++++++++ SUMMARY ++++++++++
146.094 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
0.36844 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1869518 MSNGCells (Migration Sent Number of Global Cells)
12 MSNGCluster (Migration Sent Number of Global Cluster)
0.000244258 MRC (Migrated Relative Cells)
7.30471 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	29	20804923	1.92518e+07	-0.0806748
1	33	17659195	1.92518e+07	0.0827245
2	83	19271519	1.92518e+07	-0.00102484
3	74	19271519	1.92518e+07	-0.00102484
