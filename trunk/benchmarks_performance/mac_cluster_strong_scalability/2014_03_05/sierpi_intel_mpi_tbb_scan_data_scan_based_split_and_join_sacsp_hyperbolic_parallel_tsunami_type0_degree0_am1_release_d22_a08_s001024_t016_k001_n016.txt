Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK Warning from RANK Warning from RANK 912: more nodes than initial clusters available!Warning from RANK Warning from RANK 2: more nodes than initial clusters available!Warning from RANK : more nodes than initial clusters available!
Warning from RANK 
13: more nodes than initial clusters available!Warning from RANK 6: more nodes than initial clusters available!

Warning from RANK 7: more nodes than initial clusters available!
10: more nodes than initial clusters available!
8: more nodes than initial clusters available!
5: more nodes than initial clusters available!
Warning from RANK 
Warning from RANK Warning from RANK 11: more nodes than initial clusters available!
4: more nodes than initial clusters available!
14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
476968794 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.76969 MCPPT (Million Cells Processed in Average per Simulation Timestep)
16.14 ACPST (Averaged number of clusters per Simulation Timestep)
10.0893 MCPS (Million Cells per Second) (local)
476.969 MCP (Million Cells processed) (local)
2.1338 CPSPT (Clusters per Second per Thread)
145.559 EDMBPT (CellData Megabyte per Timestep (RW))
307.901 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.504465 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
333625817 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
3.33626 MCPPT (Million Cells Processed in Average per Simulation Timestep)
100.34 ACPST (Averaged number of clusters per Simulation Timestep)
7.05717 MCPS (Million Cells per Second) (local)
333.626 MCP (Million Cells processed) (local)
13.2655 CPSPT (Clusters per Second per Thread)
101.815 EDMBPT (CellData Megabyte per Timestep (RW))
215.368 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.352859 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
419430400 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.8722 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.132206 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
270.758 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.44361 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
838860800 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
8.38861 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
17.7444 MCPS (Million Cells per Second) (local)
838.861 MCP (Million Cells processed) (local)
0.264412 CPSPT (Clusters per Second per Thread)
256 EDMBPT (CellData Megabyte per Timestep (RW))
541.516 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.887219 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
419430400 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.8722 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.132206 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
270.758 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.44361 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
419430400 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.87219 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.132206 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
270.758 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.44361 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
425721856 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.25722 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1.12 ACPST (Averaged number of clusters per Simulation Timestep)
9.00528 MCPS (Million Cells per Second) (local)
425.722 MCP (Million Cells processed) (local)
0.148071 CPSPT (Clusters per Second per Thread)
129.92 EDMBPT (CellData Megabyte per Timestep (RW))
274.819 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.450264 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2748 RT (REAL_TIME)
484602157 CP (Cells processed)
0.472748 ASPT (Averaged Seconds per Timestep)
4.84602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
65.99 ACPST (Averaged number of clusters per Simulation Timestep)
10.2507 MCPS (Million Cells per Second) (local)
484.602 MCP (Million Cells processed) (local)
8.72425 CPSPT (Clusters per Second per Thread)
147.889 EDMBPT (CellData Megabyte per Timestep (RW))
312.828 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.512537 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
403237834 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.03238 MCPPT (Million Cells Processed in Average per Simulation Timestep)
86.56 ACPST (Averaged number of clusters per Simulation Timestep)
8.52967 MCPS (Million Cells per Second) (local)
403.238 MCP (Million Cells processed) (local)
11.4437 CPSPT (Clusters per Second per Thread)
123.058 EDMBPT (CellData Megabyte per Timestep (RW))
260.305 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.426483 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
629145600 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
6.29146 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
13.3083 MCPS (Million Cells per Second) (local)
629.146 MCP (Million Cells processed) (local)
0.264412 CPSPT (Clusters per Second per Thread)
192 EDMBPT (CellData Megabyte per Timestep (RW))
406.137 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.665415 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
399915447 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
3.99915 MCPPT (Million Cells Processed in Average per Simulation Timestep)
41.81 ACPST (Averaged number of clusters per Simulation Timestep)
8.45941 MCPS (Million Cells per Second) (local)
399.915 MCP (Million Cells processed) (local)
5.52754 CPSPT (Clusters per Second per Thread)
122.045 EDMBPT (CellData Megabyte per Timestep (RW))
258.161 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.42297 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2746 RT (REAL_TIME)
481167141 CP (Cells processed)
0.472746 ASPT (Averaged Seconds per Timestep)
4.81167 MCPPT (Million Cells Processed in Average per Simulation Timestep)
60.56 ACPST (Averaged number of clusters per Simulation Timestep)
10.1781 MCPS (Million Cells per Second) (local)
481.167 MCP (Million Cells processed) (local)
8.00641 CPSPT (Clusters per Second per Thread)
146.841 EDMBPT (CellData Megabyte per Timestep (RW))
310.612 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.508906 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
563531908 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
5.63532 MCPPT (Million Cells Processed in Average per Simulation Timestep)
93.63 ACPST (Averaged number of clusters per Simulation Timestep)
11.9204 MCPS (Million Cells per Second) (local)
563.532 MCP (Million Cells processed) (local)
12.3784 CPSPT (Clusters per Second per Thread)
171.976 EDMBPT (CellData Megabyte per Timestep (RW))
363.781 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.596018 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
419430400 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.8722 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.132206 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
270.758 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.44361 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
465571410 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.65571 MCPPT (Million Cells Processed in Average per Simulation Timestep)
62.84 ACPST (Averaged number of clusters per Simulation Timestep)
9.84822 MCPS (Million Cells per Second) (local)
465.571 MCP (Million Cells processed) (local)
8.30783 CPSPT (Clusters per Second per Thread)
142.081 EDMBPT (CellData Megabyte per Timestep (RW))
300.544 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.492411 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
47.2747 RT (REAL_TIME)
456826330 CP (Cells processed)
0.472747 ASPT (Averaged Seconds per Timestep)
4.56826 MCPPT (Million Cells Processed in Average per Simulation Timestep)
17.5 ACPST (Averaged number of clusters per Simulation Timestep)
9.66324 MCPS (Million Cells per Second) (local)
456.826 MCP (Million Cells processed) (local)
2.31361 CPSPT (Clusters per Second per Thread)
139.412 EDMBPT (CellData Megabyte per Timestep (RW))
294.899 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.483162 GFLOPS


++++++++++ SUMMARY ++++++++++
161.543 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
0.286355 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

5489672 MSNGCells (Migration Sent Number of Global Cells)
28 MSNGCluster (Migration Sent Number of Global Cluster)
0.000834157 MRC (Migrated Relative Cells)
8.07715 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	30	4818052	4.81295e+06	-0.00106065
1	158	3403959	4.81295e+06	0.29275
2	1	4194304	4.81295e+06	0.128537
3	2	8388608	4.81295e+06	-0.742925
4	1	4194304	4.81295e+06	0.128537
5	1	4194304	4.81295e+06	0.128537
6	2	4718592	4.81295e+06	0.0196044
7	95	4551995	4.81295e+06	0.0542188
8	117	4077453	4.81295e+06	0.152816
9	2	6291456	4.81295e+06	-0.307194
10	56	4115041	4.81295e+06	0.145006
11	75	4787569	4.81295e+06	0.00527289
12	105	5756882	4.81295e+06	-0.196124
13	1	4194304	4.81295e+06	0.128537
14	95	4721256	4.81295e+06	0.0190509
15	27	4599075	4.81295e+06	0.0444368
