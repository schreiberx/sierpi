Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!Warning from RANK Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK Warning from RANK 
14: more nodes than initial clusters available!
10: more nodes than initial clusters available!8: more nodes than initial clusters available!

Warning from RANK 9: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477309446 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77309 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1176.65 ACPST (Averaged number of clusters per Simulation Timestep)
20.3219 MCPS (Million Cells per Second) (local)
477.309 MCP (Million Cells processed) (local)
626.211 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
620.174 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01609 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477304641 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1222.94 ACPST (Averaged number of clusters per Simulation Timestep)
20.3217 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
650.847 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
620.168 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01608 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477306880 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77307 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.3 ACPST (Averaged number of clusters per Simulation Timestep)
20.3217 MCPS (Million Cells per Second) (local)
477.307 MCP (Million Cells processed) (local)
620.17 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
620.17 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01609 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477302784 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.29 ACPST (Averaged number of clusters per Simulation Timestep)
20.3216 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
620.165 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
620.165 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01608 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4877 RT (REAL_TIME)
477310976 CP (Cells processed)
0.234877 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
20.3217 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
620.169 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
620.169 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01609 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4884 RT (REAL_TIME)
477298688 CP (Cells processed)
0.234884 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.28 ACPST (Averaged number of clusters per Simulation Timestep)
20.3206 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
620.135 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
620.135 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01603 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4882 RT (REAL_TIME)
477315072 CP (Cells processed)
0.234882 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.32 ACPST (Averaged number of clusters per Simulation Timestep)
20.3215 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
620.162 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
620.162 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01607 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4882 RT (REAL_TIME)
477298997 CP (Cells processed)
0.234882 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1234.27 ACPST (Averaged number of clusters per Simulation Timestep)
20.3208 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
656.857 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
620.142 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01604 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.488 RT (REAL_TIME)
477301712 CP (Cells processed)
0.23488 ASPT (Averaged Seconds per Timestep)
4.77302 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1219.62 ACPST (Averaged number of clusters per Simulation Timestep)
20.3211 MCPS (Million Cells per Second) (local)
477.302 MCP (Million Cells processed) (local)
649.065 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
620.15 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01605 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477319168 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.33 ACPST (Averaged number of clusters per Simulation Timestep)
20.3223 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
620.187 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
620.187 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01611 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4876 RT (REAL_TIME)
477297083 CP (Cells processed)
0.234876 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1182.4 ACPST (Averaged number of clusters per Simulation Timestep)
20.3212 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
629.269 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
620.155 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01606 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4876 RT (REAL_TIME)
477310707 CP (Cells processed)
0.234876 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.09 ACPST (Averaged number of clusters per Simulation Timestep)
20.3218 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
638.682 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
620.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01609 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477305012 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1217.2 ACPST (Averaged number of clusters per Simulation Timestep)
20.3217 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
647.793 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
620.169 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01609 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4874 RT (REAL_TIME)
477310976 CP (Cells processed)
0.234874 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
20.322 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
620.177 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
620.177 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0161 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477290066 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.7729 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.58 ACPST (Averaged number of clusters per Simulation Timestep)
20.321 MCPS (Million Cells per Second) (local)
477.29 MCP (Million Cells processed) (local)
638.946 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
620.149 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01605 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.4875 RT (REAL_TIME)
477314522 CP (Cells processed)
0.234875 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1184.33 ACPST (Averaged number of clusters per Simulation Timestep)
20.3221 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
630.298 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
620.18 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0161 GFLOPS


++++++++++ SUMMARY ++++++++++
325.145 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
78.9776 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3407872 MSNGCells (Migration Sent Number of Global Cells)
832 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446239 MRC (Migrated Relative Cells)
16.2572 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1190	4813956	4.81295e+06	-0.000209617
1	1229	4812983	4.81295e+06	-7.45385e-06
2	1175	4812800	4.81295e+06	3.05686e-05
3	1175	4812800	4.81295e+06	3.05686e-05
4	1175	4812800	4.81295e+06	3.05686e-05
5	1175	4812800	4.81295e+06	3.05686e-05
6	1175	4812800	4.81295e+06	3.05686e-05
7	1244	4814139	4.81295e+06	-0.000247639
8	1221	4810637	4.81295e+06	0.000479981
9	1175	4812800	4.81295e+06	3.05686e-05
10	1193	4815457	4.81295e+06	-0.000521484
11	1209	4812145	4.81295e+06	0.00016666
12	1226	4810706	4.81295e+06	0.000465645
13	1175	4812800	4.81295e+06	3.05686e-05
14	1205	4815464	4.81295e+06	-0.000522938
15	1192	4812067	4.81295e+06	0.000182866
