Warning from RANK 15: more nodes than initial clusters available!Warning from RANK 9: more nodes than initial clusters available!

Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
1898132472 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
18.9813 MCPPT (Million Cells Processed in Average per Simulation Timestep)
67.71 ACPST (Averaged number of clusters per Simulation Timestep)
11.3878 MCPS (Million Cells per Second) (local)
1898.13 MCP (Million Cells processed) (local)
5.0778 CPSPT (Clusters per Second per Thread)
579.264 EDMBPT (CellData Megabyte per Timestep (RW))
347.527 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.569389 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
1315881009 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
13.1588 MCPPT (Million Cells Processed in Average per Simulation Timestep)
265.24 ACPST (Averaged number of clusters per Simulation Timestep)
7.89458 MCPS (Million Cells per Second) (local)
1315.88 MCP (Million Cells processed) (local)
19.8912 CPSPT (Clusters per Second per Thread)
401.575 EDMBPT (CellData Megabyte per Timestep (RW))
240.924 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.394729 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
10.0654 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.0749934 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
307.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503272 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
3355443200 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
33.5544 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
20.1309 MCPS (Million Cells per Second) (local)
3355.44 MCP (Million Cells processed) (local)
0.149987 CPSPT (Clusters per Second per Thread)
1024 EDMBPT (CellData Megabyte per Timestep (RW))
614.346 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.00654 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
10.0655 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.0749935 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
307.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503273 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
10.0655 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.0749935 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
307.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503273 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
10.0654 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.0749933 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
307.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503272 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1907553425 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
19.0755 MCPPT (Million Cells Processed in Average per Simulation Timestep)
329.97 ACPST (Averaged number of clusters per Simulation Timestep)
11.4443 MCPS (Million Cells per Second) (local)
1907.55 MCP (Million Cells processed) (local)
24.7456 CPSPT (Clusters per Second per Thread)
582.139 EDMBPT (CellData Megabyte per Timestep (RW))
349.252 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.572215 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
1616988055 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
16.1699 MCPPT (Million Cells Processed in Average per Simulation Timestep)
259.66 ACPST (Averaged number of clusters per Simulation Timestep)
9.70106 MCPS (Million Cells per Second) (local)
1616.99 MCP (Million Cells processed) (local)
19.4728 CPSPT (Clusters per Second per Thread)
493.466 EDMBPT (CellData Megabyte per Timestep (RW))
296.053 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.485053 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
2516582400 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
25.1658 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
15.0981 MCPS (Million Cells per Second) (local)
2516.58 MCP (Million Cells processed) (local)
0.149987 CPSPT (Clusters per Second per Thread)
768 EDMBPT (CellData Megabyte per Timestep (RW))
460.759 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.754907 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
1564516474 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
15.6452 MCPPT (Million Cells Processed in Average per Simulation Timestep)
121.85 ACPST (Averaged number of clusters per Simulation Timestep)
9.38623 MCPS (Million Cells per Second) (local)
1564.52 MCP (Million Cells processed) (local)
9.1379 CPSPT (Clusters per Second per Thread)
477.453 EDMBPT (CellData Megabyte per Timestep (RW))
286.445 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.469311 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.682 RT (REAL_TIME)
1933760394 CP (Cells processed)
1.66682 ASPT (Averaged Seconds per Timestep)
19.3376 MCPPT (Million Cells Processed in Average per Simulation Timestep)
164.69 ACPST (Averaged number of clusters per Simulation Timestep)
11.6015 MCPS (Million Cells per Second) (local)
1933.76 MCP (Million Cells processed) (local)
12.3506 CPSPT (Clusters per Second per Thread)
590.137 EDMBPT (CellData Megabyte per Timestep (RW))
354.049 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.580074 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
2228120608 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
22.2812 MCPPT (Million Cells Processed in Average per Simulation Timestep)
282.51 ACPST (Averaged number of clusters per Simulation Timestep)
13.3675 MCPS (Million Cells per Second) (local)
2228.12 MCP (Million Cells processed) (local)
21.1864 CPSPT (Clusters per Second per Thread)
679.968 EDMBPT (CellData Megabyte per Timestep (RW))
407.945 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.668377 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
10.0654 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.0749933 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
307.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.503272 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1846185253 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
18.4619 MCPPT (Million Cells Processed in Average per Simulation Timestep)
171.85 ACPST (Averaged number of clusters per Simulation Timestep)
11.0761 MCPS (Million Cells per Second) (local)
1846.19 MCP (Million Cells processed) (local)
12.8876 CPSPT (Clusters per Second per Thread)
563.411 EDMBPT (CellData Megabyte per Timestep (RW))
338.017 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.553807 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
166.681 RT (REAL_TIME)
1819785136 CP (Cells processed)
1.66681 ASPT (Averaged Seconds per Timestep)
18.1979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
89.83 ACPST (Averaged number of clusters per Simulation Timestep)
10.9178 MCPS (Million Cells per Second) (local)
1819.79 MCP (Million Cells processed) (local)
6.73666 CPSPT (Clusters per Second per Thread)
555.354 EDMBPT (CellData Megabyte per Timestep (RW))
333.183 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.545888 GFLOPS


++++++++++ SUMMARY ++++++++++
182.333 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
1.03251 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

16532710 MSNGCells (Migration Sent Number of Global Cells)
22 MSNGCluster (Migration Sent Number of Global Cluster)
0.000661269 MRC (Migrated Relative Cells)
9.11666 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	78	19101500	1.90742e+07	-0.00143211
1	308	13269995	1.90742e+07	0.304296
2	1	16777216	1.90742e+07	0.120423
3	2	33554432	1.90742e+07	-0.759154
4	1	16777216	1.90742e+07	0.120423
5	1	16777216	1.90742e+07	0.120423
6	1	16777216	1.90742e+07	0.120423
7	384	19788577	1.90742e+07	-0.0374534
8	298	15880333	1.90742e+07	0.167444
9	2	25165824	1.90742e+07	-0.319366
10	147	15735603	1.90742e+07	0.175031
11	193	19400030	1.90742e+07	-0.0170831
12	336	22552721	1.90742e+07	-0.182369
13	1	16777216	1.90742e+07	0.120423
14	195	18593046	1.90742e+07	0.0252245
15	106	18258797	1.90742e+07	0.0427482
