Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!

Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899468804 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
512.31 ACPST (Averaged number of clusters per Simulation Timestep)
4.92079 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
132.72 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
150.171 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246039 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899476529 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
73.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.92081 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
19.0462 CPSPT (Clusters per Second per Thread)
579.674 EDMBPT (CellData Megabyte per Timestep (RW))
150.171 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.24604 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899469312 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
177.46 ACPST (Averaged number of clusters per Simulation Timestep)
4.92079 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
45.973 CPSPT (Clusters per Second per Thread)
579.672 EDMBPT (CellData Megabyte per Timestep (RW))
150.171 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.24604 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899447296 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
169.54 ACPST (Averaged number of clusters per Simulation Timestep)
4.92073 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
43.9213 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
150.169 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246037 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899393024 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9939 MCPPT (Million Cells Processed in Average per Simulation Timestep)
186.57 ACPST (Averaged number of clusters per Simulation Timestep)
4.92059 MCPS (Million Cells per Second) (local)
1899.39 MCP (Million Cells processed) (local)
48.3331 CPSPT (Clusters per Second per Thread)
579.649 EDMBPT (CellData Megabyte per Timestep (RW))
150.165 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.24603 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899496960 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
211.94 ACPST (Averaged number of clusters per Simulation Timestep)
4.92086 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
54.9054 CPSPT (Clusters per Second per Thread)
579.68 EDMBPT (CellData Megabyte per Timestep (RW))
150.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246043 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899489280 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
133.21 ACPST (Averaged number of clusters per Simulation Timestep)
4.92084 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
34.5095 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246042 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899484293 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
221.45 ACPST (Averaged number of clusters per Simulation Timestep)
4.92083 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
57.3691 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246041 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899498371 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
197.52 ACPST (Averaged number of clusters per Simulation Timestep)
4.92086 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
51.1698 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
150.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246043 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899487232 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
170.46 ACPST (Averaged number of clusters per Simulation Timestep)
4.92084 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
44.1596 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246042 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899436154 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
181.71 ACPST (Averaged number of clusters per Simulation Timestep)
4.92071 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
47.074 CPSPT (Clusters per Second per Thread)
579.662 EDMBPT (CellData Megabyte per Timestep (RW))
150.168 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246035 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899493326 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
251.13 ACPST (Averaged number of clusters per Simulation Timestep)
4.92085 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
65.0581 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246043 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899529680 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9953 MCPPT (Million Cells Processed in Average per Simulation Timestep)
149.24 ACPST (Averaged number of clusters per Simulation Timestep)
4.92095 MCPS (Million Cells per Second) (local)
1899.53 MCP (Million Cells processed) (local)
38.6623 CPSPT (Clusters per Second per Thread)
579.69 EDMBPT (CellData Megabyte per Timestep (RW))
150.175 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246047 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899434496 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
170.7 ACPST (Averaged number of clusters per Simulation Timestep)
4.9207 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
44.2217 CPSPT (Clusters per Second per Thread)
579.661 EDMBPT (CellData Megabyte per Timestep (RW))
150.168 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246035 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899484455 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
81.99 ACPST (Averaged number of clusters per Simulation Timestep)
4.92083 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
21.2404 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246041 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
386.009 RT (REAL_TIME)
1899467184 CP (Cells processed)
3.86009 ASPT (Averaged Seconds per Timestep)
18.9947 MCPPT (Million Cells Processed in Average per Simulation Timestep)
501.62 ACPST (Averaged number of clusters per Simulation Timestep)
4.92079 MCPS (Million Cells per Second) (local)
1899.47 MCP (Million Cells processed) (local)
129.95 CPSPT (Clusters per Second per Thread)
579.671 EDMBPT (CellData Megabyte per Timestep (RW))
150.17 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.246039 GFLOPS


++++++++++ SUMMARY ++++++++++
78.7328 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
54.8946 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6857728 MSNGCells (Migration Sent Number of Global Cells)
3444 MSNGCluster (Migration Sent Number of Global Cluster)
0.000225645 MRC (Migrated Relative Cells)
3.93664 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	545	19074876	1.90742e+07	-3.62991e-05
1	74	19074027	1.90742e+07	8.21136e-06
2	248	19073536	1.90742e+07	3.3953e-05
3	244	19074560	1.90742e+07	-1.97322e-05
4	286	19070976	1.90742e+07	0.000168166
5	313	19079168	1.90742e+07	-0.000261315
6	244	19070976	1.90742e+07	0.000168166
7	326	19074849	1.90742e+07	-3.48835e-05
8	244	19074189	1.90742e+07	-2.81794e-07
9	262	19073024	1.90742e+07	6.07955e-05
10	271	19073843	1.90742e+07	1.78579e-05
11	327	19076446	1.90742e+07	-0.000118609
12	148	19074193	1.90742e+07	-4.91502e-07
13	250	19074048	1.90742e+07	7.1104e-06
14	80	19074326	1.90742e+07	-7.46428e-06
15	519	19073901	1.90742e+07	1.48171e-05
