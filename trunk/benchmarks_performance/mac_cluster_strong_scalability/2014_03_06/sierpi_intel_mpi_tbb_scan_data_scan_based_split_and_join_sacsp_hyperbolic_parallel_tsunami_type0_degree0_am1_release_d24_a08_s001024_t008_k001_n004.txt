Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
249.567 RT (REAL_TIME)
8247178293 CP (Cells processed)
2.49567 ASPT (Averaged Seconds per Timestep)
82.4718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
19.88 ACPST (Averaged number of clusters per Simulation Timestep)
33.0459 MCPS (Million Cells per Second) (local)
8247.18 MCP (Million Cells processed) (local)
0.995723 CPSPT (Clusters per Second per Thread)
2516.84 EDMBPT (CellData Megabyte per Timestep (RW))
1008.48 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.65229 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
249.567 RT (REAL_TIME)
6940783749 CP (Cells processed)
2.49567 ASPT (Averaged Seconds per Timestep)
69.4078 MCPPT (Million Cells Processed in Average per Simulation Timestep)
32.02 ACPST (Averaged number of clusters per Simulation Timestep)
27.8113 MCPS (Million Cells per Second) (local)
6940.78 MCP (Million Cells processed) (local)
1.60377 CPSPT (Clusters per Second per Thread)
2118.16 EDMBPT (CellData Megabyte per Timestep (RW))
848.732 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.39056 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
249.567 RT (REAL_TIME)
7631781759 CP (Cells processed)
2.49567 ASPT (Averaged Seconds per Timestep)
76.3178 MCPPT (Million Cells Processed in Average per Simulation Timestep)
35.83 ACPST (Averaged number of clusters per Simulation Timestep)
30.58 MCPS (Million Cells per Second) (local)
7631.78 MCP (Million Cells processed) (local)
1.79461 CPSPT (Clusters per Second per Thread)
2329.03 EDMBPT (CellData Megabyte per Timestep (RW))
933.229 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.529 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
249.567 RT (REAL_TIME)
7571812595 CP (Cells processed)
2.49567 ASPT (Averaged Seconds per Timestep)
75.7181 MCPPT (Million Cells Processed in Average per Simulation Timestep)
40.95 ACPST (Averaged number of clusters per Simulation Timestep)
30.3397 MCPS (Million Cells per Second) (local)
7571.81 MCP (Million Cells processed) (local)
2.05105 CPSPT (Clusters per Second per Thread)
2310.73 EDMBPT (CellData Megabyte per Timestep (RW))
925.896 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.51699 GFLOPS


++++++++++ SUMMARY ++++++++++
121.777 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
0.201411 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

7573480 MSNGCells (Migration Sent Number of Global Cells)
14 MSNGCluster (Migration Sent Number of Global Cluster)
0.000248518 MRC (Migrated Relative Cells)
6.08885 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	20	82703143	7.62967e+07	-0.083967
1	34	70120225	7.62967e+07	0.0809538
2	39	76181790	7.62967e+07	0.00150655
3	44	76181780	7.62967e+07	0.00150668
