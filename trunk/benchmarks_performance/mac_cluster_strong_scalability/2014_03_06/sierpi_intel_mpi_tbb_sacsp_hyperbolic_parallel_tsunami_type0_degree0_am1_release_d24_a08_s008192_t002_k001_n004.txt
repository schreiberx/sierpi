Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.184 RT (REAL_TIME)
8247178281 CP (Cells processed)
4.91184 ASPT (Averaged Seconds per Timestep)
82.4718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
335.95 ACPST (Averaged number of clusters per Simulation Timestep)
16.7904 MCPS (Million Cells per Second) (local)
8247.18 MCP (Million Cells processed) (local)
34.198 CPSPT (Clusters per Second per Thread)
2516.84 EDMBPT (CellData Megabyte per Timestep (RW))
512.403 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.839521 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.184 RT (REAL_TIME)
6940718225 CP (Cells processed)
4.91184 ASPT (Averaged Seconds per Timestep)
69.4072 MCPPT (Million Cells Processed in Average per Simulation Timestep)
332.97 ACPST (Averaged number of clusters per Simulation Timestep)
14.1306 MCPS (Million Cells per Second) (local)
6940.72 MCP (Million Cells processed) (local)
33.8947 CPSPT (Clusters per Second per Thread)
2118.14 EDMBPT (CellData Megabyte per Timestep (RW))
431.232 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.70653 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.184 RT (REAL_TIME)
7631847323 CP (Cells processed)
4.91184 ASPT (Averaged Seconds per Timestep)
76.3185 MCPPT (Million Cells Processed in Average per Simulation Timestep)
548.2 ACPST (Averaged number of clusters per Simulation Timestep)
15.5377 MCPS (Million Cells per Second) (local)
7631.85 MCP (Million Cells processed) (local)
55.804 CPSPT (Clusters per Second per Thread)
2329.05 EDMBPT (CellData Megabyte per Timestep (RW))
474.172 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.776883 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
491.184 RT (REAL_TIME)
7571812597 CP (Cells processed)
4.91184 ASPT (Averaged Seconds per Timestep)
75.7181 MCPPT (Million Cells Processed in Average per Simulation Timestep)
545.19 ACPST (Averaged number of clusters per Simulation Timestep)
15.4154 MCPS (Million Cells per Second) (local)
7571.81 MCP (Million Cells processed) (local)
55.4976 CPSPT (Clusters per Second per Thread)
2310.73 EDMBPT (CellData Megabyte per Timestep (RW))
470.442 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.770772 GFLOPS


++++++++++ SUMMARY ++++++++++
61.8741 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
22.4243 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

7442408 MSNGCells (Migration Sent Number of Global Cells)
12 MSNGCluster (Migration Sent Number of Global Cluster)
0.000243794 MRC (Migrated Relative Cells)
3.09371 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	389	82703143	7.62967e+07	-0.083967
1	387	70120225	7.62967e+07	0.0809538
2	640	76181790	7.62967e+07	0.00150655
3	638	76181780	7.62967e+07	0.00150668
