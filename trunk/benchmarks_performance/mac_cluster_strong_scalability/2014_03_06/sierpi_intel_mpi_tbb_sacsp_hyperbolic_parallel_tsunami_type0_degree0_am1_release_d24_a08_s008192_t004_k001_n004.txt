Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
304.149 RT (REAL_TIME)
8247178281 CP (Cells processed)
3.04149 ASPT (Averaged Seconds per Timestep)
82.4718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
335.95 ACPST (Averaged number of clusters per Simulation Timestep)
27.1156 MCPS (Million Cells per Second) (local)
8247.18 MCP (Million Cells processed) (local)
27.6139 CPSPT (Clusters per Second per Thread)
2516.84 EDMBPT (CellData Megabyte per Timestep (RW))
827.501 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.35578 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
304.149 RT (REAL_TIME)
6940718225 CP (Cells processed)
3.04149 ASPT (Averaged Seconds per Timestep)
69.4072 MCPPT (Million Cells Processed in Average per Simulation Timestep)
332.97 ACPST (Averaged number of clusters per Simulation Timestep)
22.8201 MCPS (Million Cells per Second) (local)
6940.72 MCP (Million Cells processed) (local)
27.369 CPSPT (Clusters per Second per Thread)
2118.14 EDMBPT (CellData Megabyte per Timestep (RW))
696.414 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.14101 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
304.149 RT (REAL_TIME)
7631847323 CP (Cells processed)
3.04149 ASPT (Averaged Seconds per Timestep)
76.3185 MCPPT (Million Cells Processed in Average per Simulation Timestep)
548.2 ACPST (Averaged number of clusters per Simulation Timestep)
25.0924 MCPS (Million Cells per Second) (local)
7631.85 MCP (Million Cells processed) (local)
45.0601 CPSPT (Clusters per Second per Thread)
2329.05 EDMBPT (CellData Megabyte per Timestep (RW))
765.761 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.25462 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
304.149 RT (REAL_TIME)
7571812597 CP (Cells processed)
3.04149 ASPT (Averaged Seconds per Timestep)
75.7181 MCPPT (Million Cells Processed in Average per Simulation Timestep)
545.19 ACPST (Averaged number of clusters per Simulation Timestep)
24.8951 MCPS (Million Cells per Second) (local)
7571.81 MCP (Million Cells processed) (local)
44.8127 CPSPT (Clusters per Second per Thread)
2310.73 EDMBPT (CellData Megabyte per Timestep (RW))
759.737 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.24475 GFLOPS


++++++++++ SUMMARY ++++++++++
99.9232 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
9.05348 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

7442408 MSNGCells (Migration Sent Number of Global Cells)
12 MSNGCluster (Migration Sent Number of Global Cluster)
0.000243794 MRC (Migrated Relative Cells)
4.99616 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	389	82703143	7.62967e+07	-0.083967
1	387	70120225	7.62967e+07	0.0809538
2	640	76181790	7.62967e+07	0.00150655
3	638	76181780	7.62967e+07	0.00150668
