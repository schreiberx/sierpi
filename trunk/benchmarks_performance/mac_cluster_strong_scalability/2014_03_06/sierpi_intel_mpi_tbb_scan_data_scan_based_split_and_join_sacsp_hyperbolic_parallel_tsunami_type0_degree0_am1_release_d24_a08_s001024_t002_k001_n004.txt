Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
673.88 RT (REAL_TIME)
8247178293 CP (Cells processed)
6.7388 ASPT (Averaged Seconds per Timestep)
82.4718 MCPPT (Million Cells Processed in Average per Simulation Timestep)
8 ACPST (Averaged number of clusters per Simulation Timestep)
12.2383 MCPS (Million Cells per Second) (local)
8247.18 MCP (Million Cells processed) (local)
0.593577 CPSPT (Clusters per Second per Thread)
2516.84 EDMBPT (CellData Megabyte per Timestep (RW))
373.485 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.611917 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
673.88 RT (REAL_TIME)
6940783749 CP (Cells processed)
6.7388 ASPT (Averaged Seconds per Timestep)
69.4078 MCPPT (Million Cells Processed in Average per Simulation Timestep)
13.75 ACPST (Averaged number of clusters per Simulation Timestep)
10.2997 MCPS (Million Cells per Second) (local)
6940.78 MCP (Million Cells processed) (local)
1.02021 CPSPT (Clusters per Second per Thread)
2118.16 EDMBPT (CellData Megabyte per Timestep (RW))
314.323 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.514987 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
673.88 RT (REAL_TIME)
7641906670 CP (Cells processed)
6.7388 ASPT (Averaged Seconds per Timestep)
76.4191 MCPPT (Million Cells Processed in Average per Simulation Timestep)
16.13 ACPST (Averaged number of clusters per Simulation Timestep)
11.3402 MCPS (Million Cells per Second) (local)
7641.91 MCP (Million Cells processed) (local)
1.1968 CPSPT (Clusters per Second per Thread)
2332.12 EDMBPT (CellData Megabyte per Timestep (RW))
346.074 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.567008 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
673.88 RT (REAL_TIME)
7561687684 CP (Cells processed)
6.7388 ASPT (Averaged Seconds per Timestep)
75.6169 MCPPT (Million Cells Processed in Average per Simulation Timestep)
23.57 ACPST (Averaged number of clusters per Simulation Timestep)
11.2211 MCPS (Million Cells per Second) (local)
7561.69 MCP (Million Cells processed) (local)
1.74883 CPSPT (Clusters per Second per Thread)
2307.64 EDMBPT (CellData Megabyte per Timestep (RW))
342.441 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.561056 GFLOPS


++++++++++ SUMMARY ++++++++++
45.0994 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
0.569927 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

8506664 MSNGCells (Migration Sent Number of Global Cells)
20 MSNGCluster (Migration Sent Number of Global Cluster)
0.000278723 MRC (Migrated Relative Cells)
2.25497 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	8	82703143	7.62967e+07	-0.083967
1	15	70120225	7.62967e+07	0.0809538
2	17	76181790	7.62967e+07	0.00150655
3	24	76181780	7.62967e+07	0.00150668
