Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1898132472 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
18.9813 MCPPT (Million Cells Processed in Average per Simulation Timestep)
67.71 ACPST (Averaged number of clusters per Simulation Timestep)
9.9996 MCPS (Million Cells per Second) (local)
1898.13 MCP (Million Cells processed) (local)
17.8352 CPSPT (Clusters per Second per Thread)
579.264 EDMBPT (CellData Megabyte per Timestep (RW))
305.164 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.49998 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1315881009 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
13.1588 MCPPT (Million Cells Processed in Average per Simulation Timestep)
265.24 ACPST (Averaged number of clusters per Simulation Timestep)
6.93223 MCPS (Million Cells per Second) (local)
1315.88 MCP (Million Cells processed) (local)
69.8659 CPSPT (Clusters per Second per Thread)
401.575 EDMBPT (CellData Megabyte per Timestep (RW))
211.555 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.346611 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.83845 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.263406 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
269.728 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441923 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
3355443200 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
33.5544 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
17.6769 MCPS (Million Cells per Second) (local)
3355.44 MCP (Million Cells processed) (local)
0.526813 CPSPT (Clusters per Second per Thread)
1024 EDMBPT (CellData Megabyte per Timestep (RW))
539.456 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.883845 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.83845 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.263406 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
269.728 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441923 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.83845 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.263406 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
269.728 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441923 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.83845 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.263406 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
269.728 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441923 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1907553425 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
19.0755 MCPPT (Million Cells Processed in Average per Simulation Timestep)
329.97 ACPST (Averaged number of clusters per Simulation Timestep)
10.0492 MCPS (Million Cells per Second) (local)
1907.55 MCP (Million Cells processed) (local)
86.9162 CPSPT (Clusters per Second per Thread)
582.139 EDMBPT (CellData Megabyte per Timestep (RW))
306.678 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.502462 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1616988055 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.1699 MCPPT (Million Cells Processed in Average per Simulation Timestep)
259.66 ACPST (Averaged number of clusters per Simulation Timestep)
8.5185 MCPS (Million Cells per Second) (local)
1616.99 MCP (Million Cells processed) (local)
68.3961 CPSPT (Clusters per Second per Thread)
493.466 EDMBPT (CellData Megabyte per Timestep (RW))
259.964 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.425925 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
2516582400 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
25.1658 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
13.2577 MCPS (Million Cells per Second) (local)
2516.58 MCP (Million Cells processed) (local)
0.526813 CPSPT (Clusters per Second per Thread)
768 EDMBPT (CellData Megabyte per Timestep (RW))
404.592 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.662884 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1564516474 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
15.6452 MCPPT (Million Cells Processed in Average per Simulation Timestep)
121.85 ACPST (Averaged number of clusters per Simulation Timestep)
8.24207 MCPS (Million Cells per Second) (local)
1564.52 MCP (Million Cells processed) (local)
32.0961 CPSPT (Clusters per Second per Thread)
477.453 EDMBPT (CellData Megabyte per Timestep (RW))
251.528 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.412103 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1933760394 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
19.3376 MCPPT (Million Cells Processed in Average per Simulation Timestep)
164.69 ACPST (Averaged number of clusters per Simulation Timestep)
10.1873 MCPS (Million Cells per Second) (local)
1933.76 MCP (Million Cells processed) (local)
43.3804 CPSPT (Clusters per Second per Thread)
590.137 EDMBPT (CellData Megabyte per Timestep (RW))
310.891 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.509365 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
2228120608 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
22.2812 MCPPT (Million Cells Processed in Average per Simulation Timestep)
282.51 ACPST (Averaged number of clusters per Simulation Timestep)
11.738 MCPS (Million Cells per Second) (local)
2228.12 MCP (Million Cells processed) (local)
74.4149 CPSPT (Clusters per Second per Thread)
679.968 EDMBPT (CellData Megabyte per Timestep (RW))
358.216 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.586901 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1677721600 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
16.7772 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.83845 MCPS (Million Cells per Second) (local)
1677.72 MCP (Million Cells processed) (local)
0.263406 CPSPT (Clusters per Second per Thread)
512 EDMBPT (CellData Megabyte per Timestep (RW))
269.728 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441922 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1846185253 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
18.4619 MCPPT (Million Cells Processed in Average per Simulation Timestep)
171.85 ACPST (Averaged number of clusters per Simulation Timestep)
9.72593 MCPS (Million Cells per Second) (local)
1846.19 MCP (Million Cells processed) (local)
45.2664 CPSPT (Clusters per Second per Thread)
563.411 EDMBPT (CellData Megabyte per Timestep (RW))
296.812 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.486297 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
189.821 RT (REAL_TIME)
1819785136 CP (Cells processed)
1.89821 ASPT (Averaged Seconds per Timestep)
18.1979 MCPPT (Million Cells Processed in Average per Simulation Timestep)
89.83 ACPST (Averaged number of clusters per Simulation Timestep)
9.58686 MCPS (Million Cells per Second) (local)
1819.79 MCP (Million Cells processed) (local)
23.6618 CPSPT (Clusters per Second per Thread)
555.354 EDMBPT (CellData Megabyte per Timestep (RW))
292.568 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.479343 GFLOPS


++++++++++ SUMMARY ++++++++++
160.107 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
14.5064 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

16532710 MSNGCells (Migration Sent Number of Global Cells)
22 MSNGCluster (Migration Sent Number of Global Cluster)
0.000661269 MRC (Migrated Relative Cells)
8.00533 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	78	19101500	1.90742e+07	-0.00143211
1	308	13269995	1.90742e+07	0.304296
2	1	16777216	1.90742e+07	0.120423
3	2	33554432	1.90742e+07	-0.759154
4	1	16777216	1.90742e+07	0.120423
5	1	16777216	1.90742e+07	0.120423
6	1	16777216	1.90742e+07	0.120423
7	384	19788577	1.90742e+07	-0.0374534
8	298	15880333	1.90742e+07	0.167444
9	2	25165824	1.90742e+07	-0.319366
10	147	15735603	1.90742e+07	0.175031
11	193	19400030	1.90742e+07	-0.0170831
12	336	22552721	1.90742e+07	-0.182369
13	1	16777216	1.90742e+07	0.120423
14	195	18593046	1.90742e+07	0.0252245
15	106	18258797	1.90742e+07	0.0427482
