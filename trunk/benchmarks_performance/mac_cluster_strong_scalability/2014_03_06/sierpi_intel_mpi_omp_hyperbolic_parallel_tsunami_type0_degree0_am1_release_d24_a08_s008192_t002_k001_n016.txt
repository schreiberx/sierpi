Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
2: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.312 RT (REAL_TIME)
1899453444 CP (Cells processed)
2.15312 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2339.92 ACPST (Averaged number of clusters per Simulation Timestep)
8.82185 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
543.378 CPSPT (Clusters per Second per Thread)
579.667 EDMBPT (CellData Megabyte per Timestep (RW))
269.221 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441092 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.312 RT (REAL_TIME)
1899485233 CP (Cells processed)
2.15312 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2392.41 ACPST (Averaged number of clusters per Simulation Timestep)
8.822 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
555.567 CPSPT (Clusters per Second per Thread)
579.677 EDMBPT (CellData Megabyte per Timestep (RW))
269.226 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.4411 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.312 RT (REAL_TIME)
1899446272 CP (Cells processed)
2.15312 ASPT (Averaged Seconds per Timestep)
18.9945 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.66 ACPST (Averaged number of clusters per Simulation Timestep)
8.82182 MCPS (Million Cells per Second) (local)
1899.45 MCP (Million Cells processed) (local)
538.441 CPSPT (Clusters per Second per Thread)
579.665 EDMBPT (CellData Megabyte per Timestep (RW))
269.22 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441091 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.312 RT (REAL_TIME)
1899503616 CP (Cells processed)
2.15312 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.73 ACPST (Averaged number of clusters per Simulation Timestep)
8.82208 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
538.457 CPSPT (Clusters per Second per Thread)
579.683 EDMBPT (CellData Megabyte per Timestep (RW))
269.229 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441104 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.312 RT (REAL_TIME)
1899479040 CP (Cells processed)
2.15312 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
8.82197 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
538.45 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
269.225 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441098 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.313 RT (REAL_TIME)
1899462656 CP (Cells processed)
2.15313 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.68 ACPST (Averaged number of clusters per Simulation Timestep)
8.82185 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
538.443 CPSPT (Clusters per Second per Thread)
579.67 EDMBPT (CellData Megabyte per Timestep (RW))
269.222 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441093 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.313 RT (REAL_TIME)
1899479040 CP (Cells processed)
2.15313 ASPT (Averaged Seconds per Timestep)
18.9948 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.7 ACPST (Averaged number of clusters per Simulation Timestep)
8.82193 MCPS (Million Cells per Second) (local)
1899.48 MCP (Million Cells processed) (local)
538.448 CPSPT (Clusters per Second per Thread)
579.675 EDMBPT (CellData Megabyte per Timestep (RW))
269.224 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441096 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.315 RT (REAL_TIME)
1899459737 CP (Cells processed)
2.15315 ASPT (Averaged Seconds per Timestep)
18.9946 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2413.63 ACPST (Averaged number of clusters per Simulation Timestep)
8.82179 MCPS (Million Cells per Second) (local)
1899.46 MCP (Million Cells processed) (local)
560.489 CPSPT (Clusters per Second per Thread)
579.669 EDMBPT (CellData Megabyte per Timestep (RW))
269.22 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441089 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.314 RT (REAL_TIME)
1899489169 CP (Cells processed)
2.15314 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2390.5 ACPST (Averaged number of clusters per Simulation Timestep)
8.82194 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
555.119 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
269.224 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441097 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.313 RT (REAL_TIME)
1899487232 CP (Cells processed)
2.15313 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.71 ACPST (Averaged number of clusters per Simulation Timestep)
8.82198 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
538.451 CPSPT (Clusters per Second per Thread)
579.678 EDMBPT (CellData Megabyte per Timestep (RW))
269.226 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441099 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.314 RT (REAL_TIME)
1899430006 CP (Cells processed)
2.15314 ASPT (Averaged Seconds per Timestep)
18.9943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2343.66 ACPST (Averaged number of clusters per Simulation Timestep)
8.82168 MCPS (Million Cells per Second) (local)
1899.43 MCP (Million Cells processed) (local)
544.243 CPSPT (Clusters per Second per Thread)
579.66 EDMBPT (CellData Megabyte per Timestep (RW))
269.216 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441084 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.314 RT (REAL_TIME)
1899491794 CP (Cells processed)
2.15314 ASPT (Averaged Seconds per Timestep)
18.9949 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2362.23 ACPST (Averaged number of clusters per Simulation Timestep)
8.82197 MCPS (Million Cells per Second) (local)
1899.49 MCP (Million Cells processed) (local)
548.555 CPSPT (Clusters per Second per Thread)
579.679 EDMBPT (CellData Megabyte per Timestep (RW))
269.225 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441098 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.313 RT (REAL_TIME)
1899499976 CP (Cells processed)
2.15313 ASPT (Averaged Seconds per Timestep)
18.995 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2387.25 ACPST (Averaged number of clusters per Simulation Timestep)
8.82204 MCPS (Million Cells per Second) (local)
1899.5 MCP (Million Cells processed) (local)
554.368 CPSPT (Clusters per Second per Thread)
579.681 EDMBPT (CellData Megabyte per Timestep (RW))
269.227 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441102 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.313 RT (REAL_TIME)
1899438080 CP (Cells processed)
2.15313 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2318.65 ACPST (Averaged number of clusters per Simulation Timestep)
8.82176 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
538.437 CPSPT (Clusters per Second per Thread)
579.663 EDMBPT (CellData Megabyte per Timestep (RW))
269.219 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441088 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.314 RT (REAL_TIME)
1899506981 CP (Cells processed)
2.15314 ASPT (Averaged Seconds per Timestep)
18.9951 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2365.07 ACPST (Averaged number of clusters per Simulation Timestep)
8.82204 MCPS (Million Cells per Second) (local)
1899.51 MCP (Million Cells processed) (local)
549.215 CPSPT (Clusters per Second per Thread)
579.684 EDMBPT (CellData Megabyte per Timestep (RW))
269.227 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441102 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
215.314 RT (REAL_TIME)
1899444148 CP (Cells processed)
2.15314 ASPT (Averaged Seconds per Timestep)
18.9944 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2344.11 ACPST (Averaged number of clusters per Simulation Timestep)
8.82175 MCPS (Million Cells per Second) (local)
1899.44 MCP (Million Cells processed) (local)
544.348 CPSPT (Clusters per Second per Thread)
579.664 EDMBPT (CellData Megabyte per Timestep (RW))
269.218 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.441088 GFLOPS


++++++++++ SUMMARY ++++++++++
141.15 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
272.638 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

6799360 MSNGCells (Migration Sent Number of Global Cells)
830 MSNGCluster (Migration Sent Number of Global Cluster)
0.000223725 MRC (Migrated Relative Cells)
7.05752 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2350	19076924	1.90742e+07	-0.000143669
1	2404	19069931	1.90742e+07	0.000222952
2	2329	19079168	1.90742e+07	-0.000261315
3	2328	19070976	1.90742e+07	0.000168166
4	2328	19070976	1.90742e+07	0.000168166
5	2329	19079168	1.90742e+07	-0.000261315
6	2328	19070976	1.90742e+07	0.000168166
7	2426	19075873	1.90742e+07	-8.85687e-05
8	2399	19075213	1.90742e+07	-5.39669e-05
9	2328	19070976	1.90742e+07	0.000168166
10	2360	19077939	1.90742e+07	-0.000196883
11	2374	19072350	1.90742e+07	9.61312e-05
12	2405	19071121	1.90742e+07	0.000160564
13	2329	19079168	1.90742e+07	-0.000261315
14	2370	19068182	1.90742e+07	0.000314646
15	2357	19077997	1.90742e+07	-0.000199923
