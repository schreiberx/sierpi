Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
207.586 RT (REAL_TIME)
7597861941 CP (Cells processed)
2.07586 ASPT (Averaged Seconds per Timestep)
75.9786 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1647.58 ACPST (Averaged number of clusters per Simulation Timestep)
36.6011 MCPS (Million Cells per Second) (local)
7597.86 MCP (Million Cells processed) (local)
99.2108 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
1116.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.83005 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
207.586 RT (REAL_TIME)
7597865093 CP (Cells processed)
2.07586 ASPT (Averaged Seconds per Timestep)
75.9787 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1286.1 ACPST (Averaged number of clusters per Simulation Timestep)
36.601 MCPS (Million Cells per Second) (local)
7597.87 MCP (Million Cells processed) (local)
77.4437 CPSPT (Clusters per Second per Thread)
2318.68 EDMBPT (CellData Megabyte per Timestep (RW))
1116.97 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.83005 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
207.586 RT (REAL_TIME)
7597913553 CP (Cells processed)
2.07586 ASPT (Averaged Seconds per Timestep)
75.9791 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1376.7 ACPST (Averaged number of clusters per Simulation Timestep)
36.6012 MCPS (Million Cells per Second) (local)
7597.91 MCP (Million Cells processed) (local)
82.8992 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
1116.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.83006 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
207.586 RT (REAL_TIME)
7597915815 CP (Cells processed)
2.07586 ASPT (Averaged Seconds per Timestep)
75.9792 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1694.66 ACPST (Averaged number of clusters per Simulation Timestep)
36.6013 MCPS (Million Cells per Second) (local)
7597.92 MCP (Million Cells processed) (local)
102.046 CPSPT (Clusters per Second per Thread)
2318.7 EDMBPT (CellData Megabyte per Timestep (RW))
1116.98 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.83007 GFLOPS


++++++++++ SUMMARY ++++++++++
146.405 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
11.3 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1512448 MSNGCells (Migration Sent Number of Global Cells)
830 MSNGCluster (Migration Sent Number of Global Cluster)
4.97653e-05 MRC (Migrated Relative Cells)
7.32023 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2036	76296999	7.62967e+07	-3.46673e-06
1	2033	76296993	7.62967e+07	-3.38809e-06
2	1962	76296478	7.62967e+07	3.36187e-06
3	1974	76296468	7.62967e+07	3.49294e-06
