
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.211661 ST (SIMULATION_TIME)
0.0020967 TSS (Timestep size)
2978.42 RT (REAL_TIME)
30391556426 CP (Cells processed)
29.7842 ASPT (Averaged Seconds per Timestep)
303.916 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1762.31 ACPST (Averaged number of clusters per Simulation Timestep)
10.2039 MCPS (Million Cells per Second) (local)
30391.6 MCP (Million Cells processed) (local)
59.1693 CPSPT (Clusters per Second per Thread)
9274.77 EDMBPT (CellData Megabyte per Timestep (RW))
311.399 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.510196 GFLOPS

++++++++++ SUMMARY ++++++++++
10.2039 GMCPS (Global Million Cells per Second) (global)
30391.6 GMCP (Global Million Cells Processed) (global)
59.1693 GCPSPT (Global Clusters per Second per Thread) (global)
303.916 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
0.510196 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	2054	305186938	3.05187e+08	0
