#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability/2014_03_06//sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_sacsp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d24_a08_s001024_t004_k001_n001.txt
# error
#SBATCH -e /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability/2014_03_06//sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_sacsp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d24_a08_s001024_t004_k001_n001.err
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability
# job description
#SBATCH -J sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_sacsp_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d24_a08_s001024_t004_k001_n001
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=01:00:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability

#export KMP_AFFINITY=granularity=fine,compact,1,0
mpiexec.hydra -genv OMP_NUM_THREADS 4 -envall -ppn 4 -n 1 ../../build/sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_sacsp_hyperbolic_parallel_tsunami_type0_degree0_am1_release -d 24 -a 8 -t -1 -L 100 -k 1 -r 0.01/0.005 -n 4 -N 4 -o 1024
