Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 25: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 26: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 27: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 28: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 29: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 30: more nodes than initial clusters available!
Warning from RANK 31: more nodes than initial clusters available!
Warning from RANK 16: more nodes than initial clusters available!
Warning from RANK 17: more nodes than initial clusters available!
Warning from RANK 18: more nodes than initial clusters available!
Warning from RANK 19: more nodes than initial clusters available!
Warning from RANK 20: more nodes than initial clusters available!
Warning from RANK 21: more nodes than initial clusters available!
Warning from RANK 22: more nodes than initial clusters available!
Warning from RANK 23: more nodes than initial clusters available!
Warning from RANK 24: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58527 MCPS (Million Cells per Second) (local)
3.58527 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.414 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58527 MCPS (Million Cells per Second) (local)
3.58527 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.414 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58527 MCPS (Million Cells per Second) (local)
3.58527 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.414 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13145686 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31457e+06 CPST (Cells Processed in Average per Simulation Timestep)
644.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.58403 MCPS (Million Cells per Second) (local)
3.58403 MCPSPT (Million Clusters per Second per Thread)
40.1174 EDMBPT (CellData Megabyte per Timestep (RW))
109.376 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193538 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13157638 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31576e+06 CPST (Cells Processed in Average per Simulation Timestep)
636.3 ACPST (Averaged number of clusters per Simulation Timestep)
3.58729 MCPS (Million Cells per Second) (local)
3.58729 MCPSPT (Million Clusters per Second per Thread)
40.1539 EDMBPT (CellData Megabyte per Timestep (RW))
109.475 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193714 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66793 RT (REAL_TIME)
13148160 CP (Cells processed)
0.366793 ASPT (Averaged Seconds per Timestep)
1.31482e+06 CPST (Cells Processed in Average per Simulation Timestep)
642 ACPST (Averaged number of clusters per Simulation Timestep)
3.58463 MCPS (Million Cells per Second) (local)
3.58463 MCPSPT (Million Clusters per Second per Thread)
40.125 EDMBPT (CellData Megabyte per Timestep (RW))
109.394 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.19357 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66801 RT (REAL_TIME)
13152455 CP (Cells processed)
0.366801 ASPT (Averaged Seconds per Timestep)
1.31525e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.8 ACPST (Averaged number of clusters per Simulation Timestep)
3.58572 MCPS (Million Cells per Second) (local)
3.58572 MCPSPT (Million Clusters per Second per Thread)
40.1381 EDMBPT (CellData Megabyte per Timestep (RW))
109.427 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193629 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66803 RT (REAL_TIME)
13150696 CP (Cells processed)
0.366803 ASPT (Averaged Seconds per Timestep)
1.31507e+06 CPST (Cells Processed in Average per Simulation Timestep)
633.2 ACPST (Averaged number of clusters per Simulation Timestep)
3.58523 MCPS (Million Cells per Second) (local)
3.58523 MCPSPT (Million Clusters per Second per Thread)
40.1327 EDMBPT (CellData Megabyte per Timestep (RW))
109.412 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193602 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66802 RT (REAL_TIME)
13152256 CP (Cells processed)
0.366802 ASPT (Averaged Seconds per Timestep)
1.31523e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.2 ACPST (Averaged number of clusters per Simulation Timestep)
3.58565 MCPS (Million Cells per Second) (local)
3.58565 MCPSPT (Million Clusters per Second per Thread)
40.1375 EDMBPT (CellData Megabyte per Timestep (RW))
109.425 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193625 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66797 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366797 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58514 MCPS (Million Cells per Second) (local)
3.58514 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.41 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193598 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66801 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366801 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58511 MCPS (Million Cells per Second) (local)
3.58511 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.409 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193596 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66802 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366802 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.5851 MCPS (Million Cells per Second) (local)
3.5851 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.409 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193596 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66797 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366797 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58515 MCPS (Million Cells per Second) (local)
3.58515 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.41 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193598 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66793 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366793 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58519 MCPS (Million Cells per Second) (local)
3.58519 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.411 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.1936 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66792 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366792 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.5852 MCPS (Million Cells per Second) (local)
3.5852 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.412 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193601 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13158400 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31584e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.5 ACPST (Averaged number of clusters per Simulation Timestep)
3.5875 MCPS (Million Cells per Second) (local)
3.5875 MCPSPT (Million Clusters per Second per Thread)
40.1562 EDMBPT (CellData Megabyte per Timestep (RW))
109.482 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193725 GFLOPS


++++++++++ MPI RANK 16 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66795 RT (REAL_TIME)
13144064 CP (Cells processed)
0.366795 ASPT (Averaged Seconds per Timestep)
1.31441e+06 CPST (Cells Processed in Average per Simulation Timestep)
641.8 ACPST (Averaged number of clusters per Simulation Timestep)
3.58349 MCPS (Million Cells per Second) (local)
3.58349 MCPSPT (Million Clusters per Second per Thread)
40.1125 EDMBPT (CellData Megabyte per Timestep (RW))
109.359 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193509 GFLOPS


++++++++++ MPI RANK 17 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66796 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366795 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58516 MCPS (Million Cells per Second) (local)
3.58516 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.41 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193599 GFLOPS


++++++++++ MPI RANK 18 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.6679 RT (REAL_TIME)
13152256 CP (Cells processed)
0.36679 ASPT (Averaged Seconds per Timestep)
1.31523e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.2 ACPST (Averaged number of clusters per Simulation Timestep)
3.58578 MCPS (Million Cells per Second) (local)
3.58578 MCPSPT (Million Clusters per Second per Thread)
40.1375 EDMBPT (CellData Megabyte per Timestep (RW))
109.429 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193632 GFLOPS


++++++++++ MPI RANK 19 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13149683 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31497e+06 CPST (Cells Processed in Average per Simulation Timestep)
643.7 ACPST (Averaged number of clusters per Simulation Timestep)
3.58512 MCPS (Million Cells per Second) (local)
3.58512 MCPSPT (Million Clusters per Second per Thread)
40.1296 EDMBPT (CellData Megabyte per Timestep (RW))
109.409 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193596 GFLOPS


++++++++++ MPI RANK 20 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13157140 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31571e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.5 ACPST (Averaged number of clusters per Simulation Timestep)
3.58715 MCPS (Million Cells per Second) (local)
3.58715 MCPSPT (Million Clusters per Second per Thread)
40.1524 EDMBPT (CellData Megabyte per Timestep (RW))
109.471 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193706 GFLOPS


++++++++++ MPI RANK 21 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66789 RT (REAL_TIME)
13145983 CP (Cells processed)
0.366789 ASPT (Averaged Seconds per Timestep)
1.3146e+06 CPST (Cells Processed in Average per Simulation Timestep)
635.8 ACPST (Averaged number of clusters per Simulation Timestep)
3.58407 MCPS (Million Cells per Second) (local)
3.58407 MCPSPT (Million Clusters per Second per Thread)
40.1184 EDMBPT (CellData Megabyte per Timestep (RW))
109.377 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.19354 GFLOPS


++++++++++ MPI RANK 22 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66797 RT (REAL_TIME)
13148160 CP (Cells processed)
0.366797 ASPT (Averaged Seconds per Timestep)
1.31482e+06 CPST (Cells Processed in Average per Simulation Timestep)
642 ACPST (Averaged number of clusters per Simulation Timestep)
3.58459 MCPS (Million Cells per Second) (local)
3.58459 MCPSPT (Million Clusters per Second per Thread)
40.125 EDMBPT (CellData Megabyte per Timestep (RW))
109.393 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193568 GFLOPS


++++++++++ MPI RANK 23 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66794 RT (REAL_TIME)
13152256 CP (Cells processed)
0.366794 ASPT (Averaged Seconds per Timestep)
1.31523e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.2 ACPST (Averaged number of clusters per Simulation Timestep)
3.58574 MCPS (Million Cells per Second) (local)
3.58574 MCPSPT (Million Clusters per Second per Thread)
40.1375 EDMBPT (CellData Megabyte per Timestep (RW))
109.428 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.19363 GFLOPS


++++++++++ MPI RANK 24 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58526 MCPS (Million Cells per Second) (local)
3.58526 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.413 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ MPI RANK 25 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13144064 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31441e+06 CPST (Cells Processed in Average per Simulation Timestep)
641.8 ACPST (Averaged number of clusters per Simulation Timestep)
3.58359 MCPS (Million Cells per Second) (local)
3.58359 MCPSPT (Million Clusters per Second per Thread)
40.1125 EDMBPT (CellData Megabyte per Timestep (RW))
109.362 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193514 GFLOPS


++++++++++ MPI RANK 26 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13156352 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31564e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.58694 MCPS (Million Cells per Second) (local)
3.58694 MCPSPT (Million Clusters per Second per Thread)
40.15 EDMBPT (CellData Megabyte per Timestep (RW))
109.465 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193695 GFLOPS


++++++++++ MPI RANK 27 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13152256 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31523e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.2 ACPST (Averaged number of clusters per Simulation Timestep)
3.58582 MCPS (Million Cells per Second) (local)
3.58582 MCPSPT (Million Clusters per Second per Thread)
40.1375 EDMBPT (CellData Megabyte per Timestep (RW))
109.431 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193634 GFLOPS


++++++++++ MPI RANK 28 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150771 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31508e+06 CPST (Cells Processed in Average per Simulation Timestep)
634.7 ACPST (Averaged number of clusters per Simulation Timestep)
3.58542 MCPS (Million Cells per Second) (local)
3.58542 MCPSPT (Million Clusters per Second per Thread)
40.133 EDMBPT (CellData Megabyte per Timestep (RW))
109.418 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193613 GFLOPS


++++++++++ MPI RANK 29 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58526 MCPS (Million Cells per Second) (local)
3.58526 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.414 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ MPI RANK 30 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58526 MCPS (Million Cells per Second) (local)
3.58526 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.413 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ MPI RANK 31 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
3.66785 RT (REAL_TIME)
13150208 CP (Cells processed)
0.366785 ASPT (Averaged Seconds per Timestep)
1.31502e+06 CPST (Cells Processed in Average per Simulation Timestep)
642.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.58526 MCPS (Million Cells per Second) (local)
3.58526 MCPSPT (Million Clusters per Second per Thread)
40.1313 EDMBPT (CellData Megabyte per Timestep (RW))
109.413 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.193604 GFLOPS


++++++++++ SUMMARY ++++++++++
114.731 GMCPS (Global Million Cells per Second) (global)
114.731 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
