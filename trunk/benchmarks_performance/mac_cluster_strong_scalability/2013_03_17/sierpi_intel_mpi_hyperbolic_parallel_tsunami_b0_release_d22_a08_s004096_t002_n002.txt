Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
53.5758 RT (REAL_TIME)
210417163 CP (Cells processed)
5.35758 ASPT (Averaged Seconds per Timestep)
2.10417e+07 CPST (Cells Processed in Average per Simulation Timestep)
10262.3 ACPST (Averaged number of clusters per Simulation Timestep)
3.92747 MCPS (Million Cells per Second) (local)
3.92747 MCPSPT (Million Clusters per Second per Thread)
642.142 EDMBPT (CellData Megabyte per Timestep (RW))
119.857 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212083 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
53.5758 RT (REAL_TIME)
210404025 CP (Cells processed)
5.35758 ASPT (Averaged Seconds per Timestep)
2.10404e+07 CPST (Cells Processed in Average per Simulation Timestep)
10261.8 ACPST (Averaged number of clusters per Simulation Timestep)
3.92722 MCPS (Million Cells per Second) (local)
3.92722 MCPSPT (Million Clusters per Second per Thread)
642.102 EDMBPT (CellData Megabyte per Timestep (RW))
119.849 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21207 GFLOPS


++++++++++ SUMMARY ++++++++++
7.85469 GMCPS (Global Million Cells per Second) (global)
7.85469 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
