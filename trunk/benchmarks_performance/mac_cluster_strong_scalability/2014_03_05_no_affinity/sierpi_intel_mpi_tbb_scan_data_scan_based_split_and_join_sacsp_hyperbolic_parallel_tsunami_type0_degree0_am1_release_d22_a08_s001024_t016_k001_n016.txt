Warning from RANK Warning from RANK 10: more nodes than initial clusters available!
6: more nodes than initial clusters available!
Warning from RANK Warning from RANK 13: more nodes than initial clusters available!Warning from RANK 2: more nodes than initial clusters available!

9: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK Warning from RANK Warning from RANK Warning from RANK Warning from RANK 11: more nodes than initial clusters available!8: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!14: more nodes than initial clusters available!7: more nodes than initial clusters available!

5: more nodes than initial clusters available!


Warning from RANK Warning from RANK 4: more nodes than initial clusters available!
3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
476968794 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
4.76969 MCPPT (Million Cells Processed in Average per Simulation Timestep)
16.14 ACPST (Averaged number of clusters per Simulation Timestep)
10.2489 MCPS (Million Cells per Second) (local)
476.969 MCP (Million Cells processed) (local)
2.16756 CPSPT (Clusters per Second per Thread)
145.559 EDMBPT (CellData Megabyte per Timestep (RW))
312.771 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.512445 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
333625817 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
3.33626 MCPPT (Million Cells Processed in Average per Simulation Timestep)
100.34 ACPST (Averaged number of clusters per Simulation Timestep)
7.1688 MCPS (Million Cells per Second) (local)
333.626 MCP (Million Cells processed) (local)
13.4754 CPSPT (Clusters per Second per Thread)
101.815 EDMBPT (CellData Megabyte per Timestep (RW))
218.774 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.35844 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5385 RT (REAL_TIME)
419430400 CP (Cells processed)
0.465385 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
9.01254 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.134297 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
275.041 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.450627 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
838860800 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
8.38861 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
18.025 MCPS (Million Cells per Second) (local)
838.861 MCP (Million Cells processed) (local)
0.268594 CPSPT (Clusters per Second per Thread)
256 EDMBPT (CellData Megabyte per Timestep (RW))
550.081 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.901252 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
419430400 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
9.01253 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.134297 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
275.04 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.450626 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
419430400 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
9.01252 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.134297 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
275.04 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.450626 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5387 RT (REAL_TIME)
425721856 CP (Cells processed)
0.465387 ASPT (Averaged Seconds per Timestep)
4.25722 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1.12 ACPST (Averaged number of clusters per Simulation Timestep)
9.14769 MCPS (Million Cells per Second) (local)
425.722 MCP (Million Cells processed) (local)
0.150412 CPSPT (Clusters per Second per Thread)
129.92 EDMBPT (CellData Megabyte per Timestep (RW))
279.165 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.457385 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5388 RT (REAL_TIME)
484602157 CP (Cells processed)
0.465388 ASPT (Averaged Seconds per Timestep)
4.84602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
65.99 ACPST (Averaged number of clusters per Simulation Timestep)
10.4129 MCPS (Million Cells per Second) (local)
484.602 MCP (Million Cells processed) (local)
8.86223 CPSPT (Clusters per Second per Thread)
147.889 EDMBPT (CellData Megabyte per Timestep (RW))
317.776 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.520643 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5387 RT (REAL_TIME)
403237834 CP (Cells processed)
0.465387 ASPT (Averaged Seconds per Timestep)
4.03238 MCPPT (Million Cells Processed in Average per Simulation Timestep)
86.56 ACPST (Averaged number of clusters per Simulation Timestep)
8.66458 MCPS (Million Cells per Second) (local)
403.238 MCP (Million Cells processed) (local)
11.6247 CPSPT (Clusters per Second per Thread)
123.058 EDMBPT (CellData Megabyte per Timestep (RW))
264.422 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.433229 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
629145600 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
6.29146 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
13.5188 MCPS (Million Cells per Second) (local)
629.146 MCP (Million Cells processed) (local)
0.268594 CPSPT (Clusters per Second per Thread)
192 EDMBPT (CellData Megabyte per Timestep (RW))
412.561 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.675939 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5385 RT (REAL_TIME)
399915447 CP (Cells processed)
0.465385 ASPT (Averaged Seconds per Timestep)
3.99915 MCPPT (Million Cells Processed in Average per Simulation Timestep)
41.81 ACPST (Averaged number of clusters per Simulation Timestep)
8.59322 MCPS (Million Cells per Second) (local)
399.915 MCP (Million Cells processed) (local)
5.61497 CPSPT (Clusters per Second per Thread)
122.045 EDMBPT (CellData Megabyte per Timestep (RW))
262.244 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.429661 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5385 RT (REAL_TIME)
481167141 CP (Cells processed)
0.465385 ASPT (Averaged Seconds per Timestep)
4.81167 MCPPT (Million Cells Processed in Average per Simulation Timestep)
60.56 ACPST (Averaged number of clusters per Simulation Timestep)
10.3391 MCPS (Million Cells per Second) (local)
481.167 MCP (Million Cells processed) (local)
8.13305 CPSPT (Clusters per Second per Thread)
146.841 EDMBPT (CellData Megabyte per Timestep (RW))
315.525 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.516956 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
563531908 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
5.63532 MCPPT (Million Cells Processed in Average per Simulation Timestep)
93.63 ACPST (Averaged number of clusters per Simulation Timestep)
12.1089 MCPS (Million Cells per Second) (local)
563.532 MCP (Million Cells processed) (local)
12.5742 CPSPT (Clusters per Second per Thread)
171.976 EDMBPT (CellData Megabyte per Timestep (RW))
369.534 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.605445 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
419430400 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
9.01253 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.134297 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
275.04 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.450626 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5386 RT (REAL_TIME)
465571410 CP (Cells processed)
0.465386 ASPT (Averaged Seconds per Timestep)
4.65571 MCPPT (Million Cells Processed in Average per Simulation Timestep)
62.84 ACPST (Averaged number of clusters per Simulation Timestep)
10.004 MCPS (Million Cells per Second) (local)
465.571 MCP (Million Cells processed) (local)
8.43923 CPSPT (Clusters per Second per Thread)
142.081 EDMBPT (CellData Megabyte per Timestep (RW))
305.297 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.500199 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
46.5385 RT (REAL_TIME)
456826330 CP (Cells processed)
0.465385 ASPT (Averaged Seconds per Timestep)
4.56826 MCPPT (Million Cells Processed in Average per Simulation Timestep)
17.5 ACPST (Averaged number of clusters per Simulation Timestep)
9.81609 MCPS (Million Cells per Second) (local)
456.826 MCP (Million Cells processed) (local)
2.3502 CPSPT (Clusters per Second per Thread)
139.412 EDMBPT (CellData Megabyte per Timestep (RW))
299.563 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.490804 GFLOPS


++++++++++ SUMMARY ++++++++++
164.098 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
0.290884 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

5489672 MSNGCells (Migration Sent Number of Global Cells)
28 MSNGCluster (Migration Sent Number of Global Cluster)
0.000834157 MRC (Migrated Relative Cells)
8.2049 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	30	4818052	4.81295e+06	-0.00106065
1	158	3403959	4.81295e+06	0.29275
2	1	4194304	4.81295e+06	0.128537
3	2	8388608	4.81295e+06	-0.742925
4	1	4194304	4.81295e+06	0.128537
5	1	4194304	4.81295e+06	0.128537
6	2	4718592	4.81295e+06	0.0196044
7	95	4551995	4.81295e+06	0.0542188
8	117	4077453	4.81295e+06	0.152816
9	2	6291456	4.81295e+06	-0.307194
10	56	4115041	4.81295e+06	0.145006
11	75	4787569	4.81295e+06	0.00527289
12	105	5756882	4.81295e+06	-0.196124
13	1	4194304	4.81295e+06	0.128537
14	95	4721256	4.81295e+06	0.0190509
15	27	4599075	4.81295e+06	0.0444368
