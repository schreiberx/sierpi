Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.396 RT (REAL_TIME)
1909200179 CP (Cells processed)
3.34396 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
209.87 ACPST (Averaged number of clusters per Simulation Timestep)
5.7094 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
62.7609 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
174.237 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.28547 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.396 RT (REAL_TIME)
1909200173 CP (Cells processed)
3.34396 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
99.02 ACPST (Averaged number of clusters per Simulation Timestep)
5.7094 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
29.6116 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
174.237 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.28547 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.396 RT (REAL_TIME)
1909248120 CP (Cells processed)
3.34396 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
194.65 ACPST (Averaged number of clusters per Simulation Timestep)
5.70954 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
58.2094 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
174.241 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.285477 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.396 RT (REAL_TIME)
1909248214 CP (Cells processed)
3.34396 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
183.4 ACPST (Averaged number of clusters per Simulation Timestep)
5.70954 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
54.8452 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
174.241 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.285477 GFLOPS


++++++++++ SUMMARY ++++++++++
22.8379 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
51.3568 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
718 MSNGCluster (Migration Sent Number of Global Cluster)
9.453e-05 MRC (Migrated Relative Cells)
1.14189 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	253	19251515	1.92518e+07	1.42324e-05
1	173	19251515	1.92518e+07	1.42324e-05
2	237	19252063	1.92518e+07	-1.42324e-05
3	182	19252063	1.92518e+07	-1.42324e-05
