Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
61.5097 RT (REAL_TIME)
2068885811 CP (Cells processed)
0.615097 ASPT (Averaged Seconds per Timestep)
20.6889 MCPPT (Million Cells Processed in Average per Simulation Timestep)
20.04 ACPST (Averaged number of clusters per Simulation Timestep)
33.6351 MCPS (Million Cells per Second) (local)
2068.89 MCP (Million Cells processed) (local)
4.07253 CPSPT (Clusters per Second per Thread)
631.374 EDMBPT (CellData Megabyte per Timestep (RW))
1026.46 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.68176 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
61.5097 RT (REAL_TIME)
1749201197 CP (Cells processed)
0.615097 ASPT (Averaged Seconds per Timestep)
17.492 MCPPT (Million Cells Processed in Average per Simulation Timestep)
26.53 ACPST (Averaged number of clusters per Simulation Timestep)
28.4378 MCPS (Million Cells per Second) (local)
1749.2 MCP (Million Cells processed) (local)
5.39143 CPSPT (Clusters per Second per Thread)
533.814 EDMBPT (CellData Megabyte per Timestep (RW))
867.854 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.42189 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
61.5096 RT (REAL_TIME)
1913449636 CP (Cells processed)
0.615096 ASPT (Averaged Seconds per Timestep)
19.1345 MCPPT (Million Cells Processed in Average per Simulation Timestep)
36.65 ACPST (Averaged number of clusters per Simulation Timestep)
31.1082 MCPS (Million Cells per Second) (local)
1913.45 MCP (Million Cells processed) (local)
7.44803 CPSPT (Clusters per Second per Thread)
583.938 EDMBPT (CellData Megabyte per Timestep (RW))
949.346 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.55541 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
61.5096 RT (REAL_TIME)
1905360042 CP (Cells processed)
0.615096 ASPT (Averaged Seconds per Timestep)
19.0536 MCPPT (Million Cells Processed in Average per Simulation Timestep)
42.81 ACPST (Averaged number of clusters per Simulation Timestep)
30.9767 MCPS (Million Cells per Second) (local)
1905.36 MCP (Million Cells processed) (local)
8.69987 CPSPT (Clusters per Second per Thread)
581.47 EDMBPT (CellData Megabyte per Timestep (RW))
945.332 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.54883 GFLOPS


++++++++++ SUMMARY ++++++++++
124.158 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
0.800371 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

1836750 MSNGCells (Migration Sent Number of Global Cells)
10 MSNGCluster (Migration Sent Number of Global Cluster)
0.000239979 MRC (Migrated Relative Cells)
6.20789 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	21	20804923	1.92518e+07	-0.0806748
1	29	17659195	1.92518e+07	0.0827245
2	40	19271519	1.92518e+07	-0.00102484
3	50	19271519	1.92518e+07	-0.00102484
