Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
476968794 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.76969 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9.91 ACPST (Averaged number of clusters per Simulation Timestep)
7.44889 MCPS (Million Cells per Second) (local)
476.969 MCP (Million Cells processed) (local)
7.7383 CPSPT (Clusters per Second per Thread)
145.559 EDMBPT (CellData Megabyte per Timestep (RW))
227.322 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.372445 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
333625817 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
3.33626 MCPPT (Million Cells Processed in Average per Simulation Timestep)
15.76 ACPST (Averaged number of clusters per Simulation Timestep)
5.21028 MCPS (Million Cells per Second) (local)
333.626 MCP (Million Cells processed) (local)
12.3063 CPSPT (Clusters per Second per Thread)
101.815 EDMBPT (CellData Megabyte per Timestep (RW))
159.005 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.260514 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
419430400 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
6.55031 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.780857 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
199.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.327515 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
838860800 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
8.38861 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
13.1006 MCPS (Million Cells per Second) (local)
838.861 MCP (Million Cells processed) (local)
1.56171 CPSPT (Clusters per Second per Thread)
256 EDMBPT (CellData Megabyte per Timestep (RW))
399.799 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.655031 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
419430400 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
6.55031 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.780857 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
199.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.327515 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
419430400 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
6.55031 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.780857 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
199.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.327515 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
425721856 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.25722 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1.12 ACPST (Averaged number of clusters per Simulation Timestep)
6.64856 MCPS (Million Cells per Second) (local)
425.722 MCP (Million Cells processed) (local)
0.87456 CPSPT (Clusters per Second per Thread)
129.92 EDMBPT (CellData Megabyte per Timestep (RW))
202.898 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.332428 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0322 RT (REAL_TIME)
484618541 CP (Cells processed)
0.640322 ASPT (Averaged Seconds per Timestep)
4.84619 MCPPT (Million Cells Processed in Average per Simulation Timestep)
18.22 ACPST (Averaged number of clusters per Simulation Timestep)
7.56836 MCPS (Million Cells per Second) (local)
484.619 MCP (Million Cells processed) (local)
14.2272 CPSPT (Clusters per Second per Thread)
147.894 EDMBPT (CellData Megabyte per Timestep (RW))
230.968 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.378418 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
403221448 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
4.03221 MCPPT (Million Cells Processed in Average per Simulation Timestep)
11.54 ACPST (Averaged number of clusters per Simulation Timestep)
6.29717 MCPS (Million Cells per Second) (local)
403.221 MCP (Million Cells processed) (local)
9.0111 CPSPT (Clusters per Second per Thread)
123.053 EDMBPT (CellData Megabyte per Timestep (RW))
192.174 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.314859 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
629145600 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
6.29146 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
9.82547 MCPS (Million Cells per Second) (local)
629.146 MCP (Million Cells processed) (local)
1.56172 CPSPT (Clusters per Second per Thread)
192 EDMBPT (CellData Megabyte per Timestep (RW))
299.849 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.491273 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
399915447 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
3.99915 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9.89 ACPST (Averaged number of clusters per Simulation Timestep)
6.24554 MCPS (Million Cells per Second) (local)
399.915 MCP (Million Cells processed) (local)
7.72269 CPSPT (Clusters per Second per Thread)
122.045 EDMBPT (CellData Megabyte per Timestep (RW))
190.599 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.312277 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
481167141 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
4.81167 MCPPT (Million Cells Processed in Average per Simulation Timestep)
17.82 ACPST (Averaged number of clusters per Simulation Timestep)
7.51446 MCPS (Million Cells per Second) (local)
481.167 MCP (Million Cells processed) (local)
13.9149 CPSPT (Clusters per Second per Thread)
146.841 EDMBPT (CellData Megabyte per Timestep (RW))
229.323 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.375723 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
563531908 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
5.63532 MCPPT (Million Cells Processed in Average per Simulation Timestep)
21.71 ACPST (Averaged number of clusters per Simulation Timestep)
8.80077 MCPS (Million Cells per Second) (local)
563.532 MCP (Million Cells processed) (local)
16.9524 CPSPT (Clusters per Second per Thread)
171.976 EDMBPT (CellData Megabyte per Timestep (RW))
268.578 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.440038 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
419430400 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
6.55031 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.780858 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
199.9 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.327516 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
465571410 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
4.65571 MCPPT (Million Cells Processed in Average per Simulation Timestep)
8.85 ACPST (Averaged number of clusters per Simulation Timestep)
7.2709 MCPS (Million Cells per Second) (local)
465.571 MCP (Million Cells processed) (local)
6.91059 CPSPT (Clusters per Second per Thread)
142.081 EDMBPT (CellData Megabyte per Timestep (RW))
221.89 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.363545 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
64.0321 RT (REAL_TIME)
456826330 CP (Cells processed)
0.640321 ASPT (Averaged Seconds per Timestep)
4.56826 MCPPT (Million Cells Processed in Average per Simulation Timestep)
7.97 ACPST (Averaged number of clusters per Simulation Timestep)
7.13433 MCPS (Million Cells per Second) (local)
456.826 MCP (Million Cells processed) (local)
6.22344 CPSPT (Clusters per Second per Thread)
139.412 EDMBPT (CellData Megabyte per Timestep (RW))
217.722 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.356716 GFLOPS


++++++++++ SUMMARY ++++++++++
119.267 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
3.19151 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

5456904 MSNGCells (Migration Sent Number of Global Cells)
26 MSNGCluster (Migration Sent Number of Global Cluster)
0.000829075 MRC (Migrated Relative Cells)
5.96333 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	11	4818052	4.81295e+06	-0.00106063
1	17	3403959	4.81295e+06	0.29275
2	1	4194304	4.81295e+06	0.128537
3	2	8388608	4.81295e+06	-0.742925
4	1	4194304	4.81295e+06	0.128537
5	1	4194304	4.81295e+06	0.128537
6	2	4718592	4.81295e+06	0.0196045
7	21	4551995	4.81295e+06	0.0542188
8	12	4077453	4.81295e+06	0.152816
9	2	6291456	4.81295e+06	-0.307194
10	11	4115041	4.81295e+06	0.145006
11	18	4787569	4.81295e+06	0.00527291
12	22	5756882	4.81295e+06	-0.196124
13	1	4194304	4.81295e+06	0.128537
14	9	4721258	4.81295e+06	0.0190505
15	8	4599075	4.81295e+06	0.0444369
