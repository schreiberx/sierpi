Warning from RANK 2: more nodes than initial clusters available!Warning from RANK 3: more nodes than initial clusters available!


++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8951 RT (REAL_TIME)
1909200179 CP (Cells processed)
0.988951 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
663.73 ACPST (Averaged number of clusters per Simulation Timestep)
19.3053 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
167.786 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
589.151 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965266 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8952 RT (REAL_TIME)
1909213485 CP (Cells processed)
0.988952 ASPT (Averaged Seconds per Timestep)
19.0921 MCPPT (Million Cells Processed in Average per Simulation Timestep)
590 ACPST (Averaged number of clusters per Simulation Timestep)
19.3054 MCPS (Million Cells per Second) (local)
1909.21 MCP (Million Cells processed) (local)
149.148 CPSPT (Clusters per Second per Thread)
582.646 EDMBPT (CellData Megabyte per Timestep (RW))
589.155 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965271 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8951 RT (REAL_TIME)
1909234808 CP (Cells processed)
0.988951 ASPT (Averaged Seconds per Timestep)
19.0923 MCPPT (Million Cells Processed in Average per Simulation Timestep)
582.62 ACPST (Averaged number of clusters per Simulation Timestep)
19.3056 MCPS (Million Cells per Second) (local)
1909.23 MCP (Million Cells processed) (local)
147.282 CPSPT (Clusters per Second per Thread)
582.652 EDMBPT (CellData Megabyte per Timestep (RW))
589.162 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.965282 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
98.8951 RT (REAL_TIME)
1909248214 CP (Cells processed)
0.988951 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
683.01 ACPST (Averaged number of clusters per Simulation Timestep)
19.3058 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
172.66 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
589.166 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.96529 GFLOPS


++++++++++ SUMMARY ++++++++++
77.2222 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
39.8048 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
782 MSNGCluster (Migration Sent Number of Global Cluster)
9.45301e-05 MRC (Migrated Relative Cells)
3.86111 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	758	19251515	1.92518e+07	1.42065e-05
1	918	19251515	1.92518e+07	1.42065e-05
2	765	19252061	1.92518e+07	-1.41545e-05
3	727	19252063	1.92518e+07	-1.42584e-05
