Warning from RANK Warning from RANK 2: more nodes than initial clusters available!
3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.7726 RT (REAL_TIME)
1909200179 CP (Cells processed)
0.527726 ASPT (Averaged Seconds per Timestep)
19.092 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1185.23 ACPST (Averaged number of clusters per Simulation Timestep)
36.1779 MCPS (Million Cells per Second) (local)
1909.2 MCP (Million Cells processed) (local)
280.74 CPSPT (Clusters per Second per Thread)
582.642 EDMBPT (CellData Megabyte per Timestep (RW))
1104.06 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.80889 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.7727 RT (REAL_TIME)
1909216569 CP (Cells processed)
0.527727 ASPT (Averaged Seconds per Timestep)
19.0922 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1110.57 ACPST (Averaged number of clusters per Simulation Timestep)
36.1781 MCPS (Million Cells per Second) (local)
1909.22 MCP (Million Cells processed) (local)
263.055 CPSPT (Clusters per Second per Thread)
582.647 EDMBPT (CellData Megabyte per Timestep (RW))
1104.07 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.8089 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.7726 RT (REAL_TIME)
1909231736 CP (Cells processed)
0.527726 ASPT (Averaged Seconds per Timestep)
19.0923 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1021.79 ACPST (Averaged number of clusters per Simulation Timestep)
36.1784 MCPS (Million Cells per Second) (local)
1909.23 MCP (Million Cells processed) (local)
242.026 CPSPT (Clusters per Second per Thread)
582.651 EDMBPT (CellData Megabyte per Timestep (RW))
1104.08 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.80892 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
52.7725 RT (REAL_TIME)
1909248214 CP (Cells processed)
0.527725 ASPT (Averaged Seconds per Timestep)
19.0925 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1220.33 ACPST (Averaged number of clusters per Simulation Timestep)
36.1789 MCPS (Million Cells per Second) (local)
1909.25 MCP (Million Cells processed) (local)
289.055 CPSPT (Clusters per Second per Thread)
582.656 EDMBPT (CellData Megabyte per Timestep (RW))
1104.09 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.80894 GFLOPS


++++++++++ SUMMARY ++++++++++
144.713 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
33.5899 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

721920 MSNGCells (Migration Sent Number of Global Cells)
780 MSNGCluster (Migration Sent Number of Global Cluster)
9.45302e-05 MRC (Migrated Relative Cells)
7.23567 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1391	19251515	1.92518e+07	1.42065e-05
1	1764	19251515	1.92518e+07	1.42065e-05
2	1415	19252061	1.92518e+07	-1.41545e-05
3	1361	19252063	1.92518e+07	-1.42584e-05
