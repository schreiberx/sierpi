Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK Warning from RANK 8: more nodes than initial clusters available!
4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!Warning from RANK 7: more nodes than initial clusters available!

Warning from RANK Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!Warning from RANK 3: more nodes than initial clusters available!

2: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477301254 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1564.72 ACPST (Averaged number of clusters per Simulation Timestep)
20.9767 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
859.59 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
640.158 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04883 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477319497 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
643.19 ACPST (Averaged number of clusters per Simulation Timestep)
20.9775 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
353.341 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
640.183 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04888 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477297152 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
506.68 ACPST (Averaged number of clusters per Simulation Timestep)
20.9765 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
278.348 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
640.152 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04882 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477285376 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77285 MCPPT (Million Cells Processed in Average per Simulation Timestep)
547.86 ACPST (Averaged number of clusters per Simulation Timestep)
20.976 MCPS (Million Cells per Second) (local)
477.285 MCP (Million Cells processed) (local)
300.97 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
640.135 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0488 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.754 RT (REAL_TIME)
477300736 CP (Cells processed)
0.22754 ASPT (Averaged Seconds per Timestep)
4.77301 MCPPT (Million Cells Processed in Average per Simulation Timestep)
816.51 ACPST (Averaged number of clusters per Simulation Timestep)
20.9766 MCPS (Million Cells per Second) (local)
477.301 MCP (Million Cells processed) (local)
448.553 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
640.155 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04883 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7547 RT (REAL_TIME)
477298176 CP (Cells processed)
0.227547 ASPT (Averaged Seconds per Timestep)
4.77298 MCPPT (Million Cells Processed in Average per Simulation Timestep)
911.24 ACPST (Averaged number of clusters per Simulation Timestep)
20.9758 MCPS (Million Cells per Second) (local)
477.298 MCP (Million Cells processed) (local)
500.578 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
640.131 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04879 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7549 RT (REAL_TIME)
477284352 CP (Cells processed)
0.227549 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
955.13 ACPST (Averaged number of clusters per Simulation Timestep)
20.975 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
524.683 CPSPT (Clusters per Second per Thread)
145.656 EDMBPT (CellData Megabyte per Timestep (RW))
640.106 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04875 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.754 RT (REAL_TIME)
477347633 CP (Cells processed)
0.22754 ASPT (Averaged Seconds per Timestep)
4.77348 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1013.71 ACPST (Averaged number of clusters per Simulation Timestep)
20.9786 MCPS (Million Cells per Second) (local)
477.348 MCP (Million Cells processed) (local)
556.886 CPSPT (Clusters per Second per Thread)
145.675 EDMBPT (CellData Megabyte per Timestep (RW))
640.217 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04893 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7538 RT (REAL_TIME)
477314512 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
823.57 ACPST (Averaged number of clusters per Simulation Timestep)
20.9773 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
452.434 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
640.177 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04887 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7536 RT (REAL_TIME)
477308928 CP (Cells processed)
0.227536 ASPT (Averaged Seconds per Timestep)
4.77309 MCPPT (Million Cells Processed in Average per Simulation Timestep)
753.01 ACPST (Averaged number of clusters per Simulation Timestep)
20.9773 MCPS (Million Cells per Second) (local)
477.309 MCP (Million Cells processed) (local)
413.676 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
640.175 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04886 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7538 RT (REAL_TIME)
477294003 CP (Cells processed)
0.227538 ASPT (Averaged Seconds per Timestep)
4.77294 MCPPT (Million Cells Processed in Average per Simulation Timestep)
899.43 ACPST (Averaged number of clusters per Simulation Timestep)
20.9765 MCPS (Million Cells per Second) (local)
477.294 MCP (Million Cells processed) (local)
494.11 CPSPT (Clusters per Second per Thread)
145.659 EDMBPT (CellData Megabyte per Timestep (RW))
640.151 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04882 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477304057 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77304 MCPPT (Million Cells Processed in Average per Simulation Timestep)
808.64 ACPST (Averaged number of clusters per Simulation Timestep)
20.9768 MCPS (Million Cells per Second) (local)
477.304 MCP (Million Cells processed) (local)
444.231 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
640.161 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04884 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7538 RT (REAL_TIME)
477341872 CP (Cells processed)
0.227538 ASPT (Averaged Seconds per Timestep)
4.77342 MCPPT (Million Cells Processed in Average per Simulation Timestep)
733.81 ACPST (Averaged number of clusters per Simulation Timestep)
20.9785 MCPS (Million Cells per Second) (local)
477.342 MCP (Million Cells processed) (local)
403.124 CPSPT (Clusters per Second per Thread)
145.673 EDMBPT (CellData Megabyte per Timestep (RW))
640.214 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04893 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7539 RT (REAL_TIME)
477283840 CP (Cells processed)
0.227539 ASPT (Averaged Seconds per Timestep)
4.77284 MCPPT (Million Cells Processed in Average per Simulation Timestep)
476.32 ACPST (Averaged number of clusters per Simulation Timestep)
20.9759 MCPS (Million Cells per Second) (local)
477.284 MCP (Million Cells processed) (local)
261.67 CPSPT (Clusters per Second per Thread)
145.655 EDMBPT (CellData Megabyte per Timestep (RW))
640.135 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.0488 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7538 RT (REAL_TIME)
477310034 CP (Cells processed)
0.227538 ASPT (Averaged Seconds per Timestep)
4.7731 MCPPT (Million Cells Processed in Average per Simulation Timestep)
494.57 ACPST (Averaged number of clusters per Simulation Timestep)
20.9771 MCPS (Million Cells per Second) (local)
477.31 MCP (Million Cells processed) (local)
271.696 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
640.171 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04886 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
22.7538 RT (REAL_TIME)
477305306 CP (Cells processed)
0.227538 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1569.17 ACPST (Averaged number of clusters per Simulation Timestep)
20.9769 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
862.037 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
640.165 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.04885 GFLOPS


++++++++++ SUMMARY ++++++++++
335.629 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
58.0151 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3411968 MSNGCells (Migration Sent Number of Global Cells)
3182 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446767 MRC (Migrated Relative Cells)
16.7815 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1638	4812932	4.81295e+06	3.14257e-06
1	918	4811959	4.81295e+06	0.000205306
2	656	4813824	4.81295e+06	-0.000182191
3	746	4812800	4.81295e+06	3.05686e-05
4	1280	4813824	4.81295e+06	-0.000182191
5	1481	4812800	4.81295e+06	3.05686e-05
6	1603	4812800	4.81295e+06	3.05686e-05
7	1662	4812091	4.81295e+06	0.00017788
8	1141	4812683	4.81295e+06	5.4878e-05
9	1141	4812800	4.81295e+06	3.05686e-05
10	1435	4815457	4.81295e+06	-0.000521484
11	1040	4811121	4.81295e+06	0.000379419
12	882	4812754	4.81295e+06	4.01261e-05
13	574	4813312	4.81295e+06	-7.58111e-05
14	635	4812906	4.81295e+06	8.54466e-06
15	1627	4813091	4.81295e+06	-2.98933e-05
