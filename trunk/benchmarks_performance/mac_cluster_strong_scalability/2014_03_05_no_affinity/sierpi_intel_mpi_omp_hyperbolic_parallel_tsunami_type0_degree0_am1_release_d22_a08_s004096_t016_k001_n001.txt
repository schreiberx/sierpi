
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
133.016 RT (REAL_TIME)
7636896724 CP (Cells processed)
1.33016 ASPT (Averaged Seconds per Timestep)
76.369 MCPPT (Million Cells Processed in Average per Simulation Timestep)
18995.2 ACPST (Averaged number of clusters per Simulation Timestep)
57.4135 MCPS (Million Cells per Second) (local)
7636.9 MCP (Million Cells processed) (local)
892.527 CPSPT (Clusters per Second per Thread)
2330.6 EDMBPT (CellData Megabyte per Timestep (RW))
1752.12 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
2.87067 GFLOPS

++++++++++ SUMMARY ++++++++++
57.4135 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
55.7829 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
2.87067 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	19134	77007154	7.70072e+07	0
