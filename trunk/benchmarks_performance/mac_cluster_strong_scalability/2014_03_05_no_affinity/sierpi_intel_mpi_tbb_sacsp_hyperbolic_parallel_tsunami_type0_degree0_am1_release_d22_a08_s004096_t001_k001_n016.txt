Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 15Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
476968794 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.76969 MCPPT (Million Cells Processed in Average per Simulation Timestep)
46.85 ACPST (Averaged number of clusters per Simulation Timestep)
5.03057 MCPS (Million Cells per Second) (local)
476.969 MCP (Million Cells processed) (local)
49.4125 CPSPT (Clusters per Second per Thread)
145.559 EDMBPT (CellData Megabyte per Timestep (RW))
153.521 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.251528 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
333625837 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
3.33626 MCPPT (Million Cells Processed in Average per Simulation Timestep)
196.79 ACPST (Averaged number of clusters per Simulation Timestep)
3.51874 MCPS (Million Cells per Second) (local)
333.626 MCP (Million Cells processed) (local)
207.554 CPSPT (Clusters per Second per Thread)
101.815 EDMBPT (CellData Megabyte per Timestep (RW))
107.383 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.175937 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
419430400 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
4.42371 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
1.0547 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
135.001 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.221186 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
838860800 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
8.38861 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
8.84743 MCPS (Million Cells per Second) (local)
838.861 MCP (Million Cells processed) (local)
2.10939 CPSPT (Clusters per Second per Thread)
256 EDMBPT (CellData Megabyte per Timestep (RW))
270.002 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.442371 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
419430400 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
4.42371 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
1.0547 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
135.001 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.221186 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
419430400 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
4.42371 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
1.0547 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
135.001 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.221186 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
425721856 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.25722 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1.12 ACPST (Averaged number of clusters per Simulation Timestep)
4.49007 MCPS (Million Cells per Second) (local)
425.722 MCP (Million Cells processed) (local)
1.18126 CPSPT (Clusters per Second per Thread)
129.92 EDMBPT (CellData Megabyte per Timestep (RW))
137.026 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.224504 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
484602157 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.84602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
241.12 ACPST (Averaged number of clusters per Simulation Timestep)
5.11108 MCPS (Million Cells per Second) (local)
484.602 MCP (Million Cells processed) (local)
254.308 CPSPT (Clusters per Second per Thread)
147.889 EDMBPT (CellData Megabyte per Timestep (RW))
155.978 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.255554 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
403237836 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.03238 MCPPT (Million Cells Processed in Average per Simulation Timestep)
195.09 ACPST (Averaged number of clusters per Simulation Timestep)
4.25293 MCPS (Million Cells per Second) (local)
403.238 MCP (Million Cells processed) (local)
205.761 CPSPT (Clusters per Second per Thread)
123.058 EDMBPT (CellData Megabyte per Timestep (RW))
129.789 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.212647 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
629145600 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
6.29146 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
6.63557 MCPS (Million Cells per Second) (local)
629.146 MCP (Million Cells processed) (local)
2.10939 CPSPT (Clusters per Second per Thread)
192 EDMBPT (CellData Megabyte per Timestep (RW))
202.502 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.331779 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
399915451 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
3.99915 MCPPT (Million Cells Processed in Average per Simulation Timestep)
91.53 ACPST (Averaged number of clusters per Simulation Timestep)
4.21789 MCPS (Million Cells per Second) (local)
399.915 MCP (Million Cells processed) (local)
96.5363 CPSPT (Clusters per Second per Thread)
122.045 EDMBPT (CellData Megabyte per Timestep (RW))
128.72 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.210895 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
481167141 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.81167 MCPPT (Million Cells Processed in Average per Simulation Timestep)
128.01 ACPST (Averaged number of clusters per Simulation Timestep)
5.07485 MCPS (Million Cells per Second) (local)
481.167 MCP (Million Cells processed) (local)
135.012 CPSPT (Clusters per Second per Thread)
146.841 EDMBPT (CellData Megabyte per Timestep (RW))
154.872 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.253742 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
563531908 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
5.63532 MCPPT (Million Cells Processed in Average per Simulation Timestep)
215.52 ACPST (Averaged number of clusters per Simulation Timestep)
5.94355 MCPS (Million Cells per Second) (local)
563.532 MCP (Million Cells processed) (local)
227.308 CPSPT (Clusters per Second per Thread)
171.976 EDMBPT (CellData Megabyte per Timestep (RW))
181.383 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.297177 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
419430400 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
4.42371 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
1.0547 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
135.001 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.221186 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
465571410 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.65571 MCPPT (Million Cells Processed in Average per Simulation Timestep)
129.28 ACPST (Averaged number of clusters per Simulation Timestep)
4.91036 MCPS (Million Cells per Second) (local)
465.571 MCP (Million Cells processed) (local)
136.351 CPSPT (Clusters per Second per Thread)
142.081 EDMBPT (CellData Megabyte per Timestep (RW))
149.852 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.245518 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
94.8141 RT (REAL_TIME)
456826330 CP (Cells processed)
0.948141 ASPT (Averaged Seconds per Timestep)
4.56826 MCPPT (Million Cells Processed in Average per Simulation Timestep)
68.41 ACPST (Averaged number of clusters per Simulation Timestep)
4.81813 MCPS (Million Cells per Second) (local)
456.826 MCP (Million Cells processed) (local)
72.1517 CPSPT (Clusters per Second per Thread)
139.412 EDMBPT (CellData Megabyte per Timestep (RW))
147.038 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.240906 GFLOPS


++++++++++ SUMMARY ++++++++++
80.546 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
87.1258 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

5489672 MSNGCells (Migration Sent Number of Global Cells)
28 MSNGCluster (Migration Sent Number of Global Cluster)
0.000834157 MRC (Migrated Relative Cells)
4.0273 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	59	4818052	4.81295e+06	-0.00106065
1	226	3403959	4.81295e+06	0.29275
2	1	4194304	4.81295e+06	0.128537
3	2	8388608	4.81295e+06	-0.742925
4	1	4194304	4.81295e+06	0.128537
5	1	4194304	4.81295e+06	0.128537
6	2	4718592	4.81295e+06	0.0196044
7	282	4551995	4.81295e+06	0.0542188
8	222	4077453	4.81295e+06	0.152816
9	2	6291456	4.81295e+06	-0.307194
10	109	4115041	4.81295e+06	0.145006
11	149	4787569	4.81295e+06	0.00527289
12	254	5756882	4.81295e+06	-0.196124
13	1	4194304	4.81295e+06	0.128537
14	147	4721256	4.81295e+06	0.0190509
15	78	4599075	4.81295e+06	0.0444368
