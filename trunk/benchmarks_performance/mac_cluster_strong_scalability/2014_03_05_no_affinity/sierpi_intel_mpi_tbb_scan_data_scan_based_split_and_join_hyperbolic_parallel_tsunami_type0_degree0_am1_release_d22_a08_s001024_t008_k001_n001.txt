
++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
193.942 RT (REAL_TIME)
7636896686 CP (Cells processed)
1.93942 ASPT (Averaged Seconds per Timestep)
76.369 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1159.44 ACPST (Averaged number of clusters per Simulation Timestep)
39.3771 MCPS (Million Cells per Second) (local)
7636.9 MCP (Million Cells processed) (local)
74.7283 CPSPT (Clusters per Second per Thread)
2330.6 EDMBPT (CellData Megabyte per Timestep (RW))
1201.69 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.96886 GFLOPS

++++++++++ SUMMARY ++++++++++
39.3771 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
9.34104 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

0 MSNGCells (Migration Sent Number of Global Cells)
0 MSNGCluster (Migration Sent Number of Global Cluster)
0 MRC (Migrated Relative Cells)
1.96886 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1534	77007156	7.70072e+07	0
