Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
2: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
476968794 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
4.76969 MCPPT (Million Cells Processed in Average per Simulation Timestep)
13.77 ACPST (Averaged number of clusters per Simulation Timestep)
9.6287 MCPS (Million Cells per Second) (local)
476.969 MCP (Million Cells processed) (local)
6.94947 CPSPT (Clusters per Second per Thread)
145.559 EDMBPT (CellData Megabyte per Timestep (RW))
293.845 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.481435 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
333625817 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
3.33626 MCPPT (Million Cells Processed in Average per Simulation Timestep)
22.56 ACPST (Averaged number of clusters per Simulation Timestep)
6.735 MCPS (Million Cells per Second) (local)
333.626 MCP (Million Cells processed) (local)
11.3856 CPSPT (Clusters per Second per Thread)
101.815 EDMBPT (CellData Megabyte per Timestep (RW))
205.536 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.33675 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
419430400 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.46716 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.504682 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
258.397 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.423358 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5362 RT (REAL_TIME)
838860800 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
8.38861 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
16.9343 MCPS (Million Cells per Second) (local)
838.861 MCP (Million Cells processed) (local)
1.00936 CPSPT (Clusters per Second per Thread)
256 EDMBPT (CellData Megabyte per Timestep (RW))
516.794 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.846716 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
419430400 CP (Cells processed)
0.495361 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.46717 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.504683 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
258.398 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.423358 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
419430400 CP (Cells processed)
0.495361 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.46717 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.504683 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
258.398 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.423358 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
425721856 CP (Cells processed)
0.495361 ASPT (Averaged Seconds per Timestep)
4.25722 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1.12 ACPST (Averaged number of clusters per Simulation Timestep)
8.59418 MCPS (Million Cells per Second) (local)
425.722 MCP (Million Cells processed) (local)
0.565245 CPSPT (Clusters per Second per Thread)
129.92 EDMBPT (CellData Megabyte per Timestep (RW))
262.273 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.429709 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5361 RT (REAL_TIME)
484602157 CP (Cells processed)
0.495361 ASPT (Averaged Seconds per Timestep)
4.84602 MCPPT (Million Cells Processed in Average per Simulation Timestep)
25.93 ACPST (Averaged number of clusters per Simulation Timestep)
9.78281 MCPS (Million Cells per Second) (local)
484.602 MCP (Million Cells processed) (local)
13.0864 CPSPT (Clusters per Second per Thread)
147.889 EDMBPT (CellData Megabyte per Timestep (RW))
298.548 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.489141 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5364 RT (REAL_TIME)
403237834 CP (Cells processed)
0.495364 ASPT (Averaged Seconds per Timestep)
4.03238 MCPPT (Million Cells Processed in Average per Simulation Timestep)
33.22 ACPST (Averaged number of clusters per Simulation Timestep)
8.14023 MCPS (Million Cells per Second) (local)
403.238 MCP (Million Cells processed) (local)
16.7654 CPSPT (Clusters per Second per Thread)
123.058 EDMBPT (CellData Megabyte per Timestep (RW))
248.42 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.407011 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5364 RT (REAL_TIME)
629145600 CP (Cells processed)
0.495364 ASPT (Averaged Seconds per Timestep)
6.29146 MCPPT (Million Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
12.7007 MCPS (Million Cells per Second) (local)
629.146 MCP (Million Cells processed) (local)
1.00936 CPSPT (Clusters per Second per Thread)
192 EDMBPT (CellData Megabyte per Timestep (RW))
387.593 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.635033 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5364 RT (REAL_TIME)
399915447 CP (Cells processed)
0.495364 ASPT (Averaged Seconds per Timestep)
3.99915 MCPPT (Million Cells Processed in Average per Simulation Timestep)
12.73 ACPST (Averaged number of clusters per Simulation Timestep)
8.07316 MCPS (Million Cells per Second) (local)
399.915 MCP (Million Cells processed) (local)
6.42456 CPSPT (Clusters per Second per Thread)
122.045 EDMBPT (CellData Megabyte per Timestep (RW))
246.373 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.403658 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5364 RT (REAL_TIME)
481167141 CP (Cells processed)
0.495364 ASPT (Averaged Seconds per Timestep)
4.81167 MCPPT (Million Cells Processed in Average per Simulation Timestep)
28.15 ACPST (Averaged number of clusters per Simulation Timestep)
9.7134 MCPS (Million Cells per Second) (local)
481.167 MCP (Million Cells processed) (local)
14.2067 CPSPT (Clusters per Second per Thread)
146.841 EDMBPT (CellData Megabyte per Timestep (RW))
296.429 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.48567 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5362 RT (REAL_TIME)
563531908 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
5.63532 MCPPT (Million Cells Processed in Average per Simulation Timestep)
52.07 ACPST (Averaged number of clusters per Simulation Timestep)
11.3762 MCPS (Million Cells per Second) (local)
563.532 MCP (Million Cells processed) (local)
26.2788 CPSPT (Clusters per Second per Thread)
171.976 EDMBPT (CellData Megabyte per Timestep (RW))
347.173 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.568808 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5362 RT (REAL_TIME)
419430400 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
4.1943 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1 ACPST (Averaged number of clusters per Simulation Timestep)
8.46715 MCPS (Million Cells per Second) (local)
419.43 MCP (Million Cells processed) (local)
0.504681 CPSPT (Clusters per Second per Thread)
128 EDMBPT (CellData Megabyte per Timestep (RW))
258.397 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.423357 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5362 RT (REAL_TIME)
465571406 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
4.65571 MCPPT (Million Cells Processed in Average per Simulation Timestep)
15.18 ACPST (Averaged number of clusters per Simulation Timestep)
9.39861 MCPS (Million Cells per Second) (local)
465.571 MCP (Million Cells processed) (local)
7.66106 CPSPT (Clusters per Second per Thread)
142.081 EDMBPT (CellData Megabyte per Timestep (RW))
286.823 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.46993 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
49.5362 RT (REAL_TIME)
456826330 CP (Cells processed)
0.495362 ASPT (Averaged Seconds per Timestep)
4.56826 MCPPT (Million Cells Processed in Average per Simulation Timestep)
16.54 ACPST (Averaged number of clusters per Simulation Timestep)
9.22207 MCPS (Million Cells per Second) (local)
456.826 MCP (Million Cells processed) (local)
8.34743 CPSPT (Clusters per Second per Thread)
139.412 EDMBPT (CellData Megabyte per Timestep (RW))
281.435 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.461103 GFLOPS


++++++++++ SUMMARY ++++++++++
154.168 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
1.80794 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

5489672 MSNGCells (Migration Sent Number of Global Cells)
28 MSNGCluster (Migration Sent Number of Global Cluster)
0.000834157 MRC (Migrated Relative Cells)
7.7084 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	15	4818052	4.81295e+06	-0.00106065
1	24	3403959	4.81295e+06	0.29275
2	1	4194304	4.81295e+06	0.128537
3	2	8388608	4.81295e+06	-0.742925
4	1	4194304	4.81295e+06	0.128537
5	1	4194304	4.81295e+06	0.128537
6	2	4718592	4.81295e+06	0.0196044
7	29	4551995	4.81295e+06	0.0542188
8	41	4077453	4.81295e+06	0.152816
9	2	6291456	4.81295e+06	-0.307194
10	16	4115041	4.81295e+06	0.145006
11	32	4787569	4.81295e+06	0.00527289
12	57	5756882	4.81295e+06	-0.196124
13	1	4194304	4.81295e+06	0.128537
14	17	4721256	4.81295e+06	0.0190509
15	26	4599075	4.81295e+06	0.0444368
