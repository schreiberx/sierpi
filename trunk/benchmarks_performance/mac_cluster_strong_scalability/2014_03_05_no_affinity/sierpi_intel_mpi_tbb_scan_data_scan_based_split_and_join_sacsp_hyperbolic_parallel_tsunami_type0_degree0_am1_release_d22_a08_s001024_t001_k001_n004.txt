Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.399 RT (REAL_TIME)
2068885811 CP (Cells processed)
3.34399 ASPT (Averaged Seconds per Timestep)
20.6889 MCPPT (Million Cells Processed in Average per Simulation Timestep)
6 ACPST (Averaged number of clusters per Simulation Timestep)
6.18689 MCPS (Million Cells per Second) (local)
2068.89 MCP (Million Cells processed) (local)
1.79427 CPSPT (Clusters per Second per Thread)
631.374 EDMBPT (CellData Megabyte per Timestep (RW))
188.809 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.309344 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.399 RT (REAL_TIME)
1749201197 CP (Cells processed)
3.34399 ASPT (Averaged Seconds per Timestep)
17.492 MCPPT (Million Cells Processed in Average per Simulation Timestep)
9.51 ACPST (Averaged number of clusters per Simulation Timestep)
5.23089 MCPS (Million Cells per Second) (local)
1749.2 MCP (Million Cells processed) (local)
2.84391 CPSPT (Clusters per Second per Thread)
533.814 EDMBPT (CellData Megabyte per Timestep (RW))
159.634 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.261544 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.399 RT (REAL_TIME)
1915996443 CP (Cells processed)
3.34399 ASPT (Averaged Seconds per Timestep)
19.16 MCPPT (Million Cells Processed in Average per Simulation Timestep)
11.59 ACPST (Averaged number of clusters per Simulation Timestep)
5.72968 MCPS (Million Cells per Second) (local)
1916 MCP (Million Cells processed) (local)
3.46592 CPSPT (Clusters per Second per Thread)
584.716 EDMBPT (CellData Megabyte per Timestep (RW))
174.856 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.286484 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
334.399 RT (REAL_TIME)
1902813235 CP (Cells processed)
3.34399 ASPT (Averaged Seconds per Timestep)
19.0281 MCPPT (Million Cells Processed in Average per Simulation Timestep)
20.57 ACPST (Averaged number of clusters per Simulation Timestep)
5.69025 MCPS (Million Cells per Second) (local)
1902.81 MCP (Million Cells processed) (local)
6.15134 CPSPT (Clusters per Second per Thread)
580.693 EDMBPT (CellData Megabyte per Timestep (RW))
173.653 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.284513 GFLOPS


++++++++++ SUMMARY ++++++++++
22.8377 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
3.56386 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

2073910 MSNGCells (Migration Sent Number of Global Cells)
16 MSNGCluster (Migration Sent Number of Global Cluster)
0.000270605 MRC (Migrated Relative Cells)
1.14189 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	6	20804923	1.92518e+07	-0.0806748
1	10	17659195	1.92518e+07	0.0827245
2	12	19271519	1.92518e+07	-0.00102484
3	21	19271519	1.92518e+07	-0.00102484
