Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK Warning from RANK 5: more nodes than initial clusters available!
4: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477309446 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77309 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1176.65 ACPST (Averaged number of clusters per Simulation Timestep)
14.1894 MCPS (Million Cells per Second) (local)
477.309 MCP (Million Cells processed) (local)
874.484 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
433.027 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709471 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477304641 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1222.94 ACPST (Averaged number of clusters per Simulation Timestep)
14.1893 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
908.887 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
433.022 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709464 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477306880 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77307 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.3 ACPST (Averaged number of clusters per Simulation Timestep)
14.1893 MCPS (Million Cells per Second) (local)
477.307 MCP (Million Cells processed) (local)
866.049 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
433.024 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709467 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477302784 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.29 ACPST (Averaged number of clusters per Simulation Timestep)
14.1892 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
866.042 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
433.021 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709461 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6388 RT (REAL_TIME)
477310976 CP (Cells processed)
0.336388 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
14.1893 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
866.047 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
433.023 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709466 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6388 RT (REAL_TIME)
477298688 CP (Cells processed)
0.336388 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.28 ACPST (Averaged number of clusters per Simulation Timestep)
14.1889 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
866.024 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
433.012 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709447 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6388 RT (REAL_TIME)
477315072 CP (Cells processed)
0.336388 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.32 ACPST (Averaged number of clusters per Simulation Timestep)
14.1894 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
866.052 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
433.026 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.70947 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6393 RT (REAL_TIME)
477298997 CP (Cells processed)
0.336393 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1234.27 ACPST (Averaged number of clusters per Simulation Timestep)
14.1887 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
917.283 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
433.006 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709437 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6392 RT (REAL_TIME)
477301712 CP (Cells processed)
0.336392 ASPT (Averaged Seconds per Timestep)
4.77302 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1219.62 ACPST (Averaged number of clusters per Simulation Timestep)
14.1889 MCPS (Million Cells per Second) (local)
477.302 MCP (Million Cells processed) (local)
906.398 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
433.01 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709443 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6386 RT (REAL_TIME)
477319168 CP (Cells processed)
0.336386 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.33 ACPST (Averaged number of clusters per Simulation Timestep)
14.1896 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
866.066 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
433.033 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709481 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.639 RT (REAL_TIME)
477297083 CP (Cells processed)
0.33639 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1182.4 ACPST (Averaged number of clusters per Simulation Timestep)
14.1888 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
878.743 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
433.008 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709441 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.639 RT (REAL_TIME)
477310707 CP (Cells processed)
0.33639 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.09 ACPST (Averaged number of clusters per Simulation Timestep)
14.1892 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
891.89 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
433.021 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709461 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6385 RT (REAL_TIME)
477305012 CP (Cells processed)
0.336385 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1217.2 ACPST (Averaged number of clusters per Simulation Timestep)
14.1893 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
904.619 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
433.022 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709463 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477310976 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
14.1895 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
866.056 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
433.028 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709473 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477290066 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.7729 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.58 ACPST (Averaged number of clusters per Simulation Timestep)
14.1888 MCPS (Million Cells per Second) (local)
477.29 MCP (Million Cells processed) (local)
892.268 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
433.009 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709442 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
33.6384 RT (REAL_TIME)
477314522 CP (Cells processed)
0.336384 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1184.33 ACPST (Averaged number of clusters per Simulation Timestep)
14.1896 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
880.191 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
433.031 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
0.709478 GFLOPS


++++++++++ SUMMARY ++++++++++
227.027 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
220.58 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3407872 MSNGCells (Migration Sent Number of Global Cells)
832 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446239 MRC (Migrated Relative Cells)
11.3514 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1190	4813956	4.81295e+06	-0.000209617
1	1229	4812983	4.81295e+06	-7.45385e-06
2	1175	4812800	4.81295e+06	3.05686e-05
3	1175	4812800	4.81295e+06	3.05686e-05
4	1175	4812800	4.81295e+06	3.05686e-05
5	1175	4812800	4.81295e+06	3.05686e-05
6	1175	4812800	4.81295e+06	3.05686e-05
7	1244	4814139	4.81295e+06	-0.000247639
8	1221	4810637	4.81295e+06	0.000479981
9	1175	4812800	4.81295e+06	3.05686e-05
10	1193	4815457	4.81295e+06	-0.000521484
11	1209	4812145	4.81295e+06	0.00016666
12	1226	4810706	4.81295e+06	0.000465645
13	1175	4812800	4.81295e+06	3.05686e-05
14	1205	4815464	4.81295e+06	-0.000522938
15	1192	4812067	4.81295e+06	0.000182866
