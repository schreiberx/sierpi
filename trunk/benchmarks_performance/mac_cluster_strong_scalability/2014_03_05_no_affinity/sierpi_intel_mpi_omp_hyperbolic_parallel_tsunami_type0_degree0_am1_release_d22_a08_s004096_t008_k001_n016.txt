Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!Warning from RANK Warning from RANK 3: more nodes than initial clusters available!

4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477309446 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77309 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1176.65 ACPST (Averaged number of clusters per Simulation Timestep)
20.2402 MCPS (Million Cells per Second) (local)
477.309 MCP (Million Cells processed) (local)
623.696 CPSPT (Clusters per Second per Thread)
145.663 EDMBPT (CellData Megabyte per Timestep (RW))
617.683 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01201 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477304641 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1222.94 ACPST (Averaged number of clusters per Simulation Timestep)
20.24 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
648.233 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
617.677 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477306880 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77307 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.3 ACPST (Averaged number of clusters per Simulation Timestep)
20.2401 MCPS (Million Cells per Second) (local)
477.307 MCP (Million Cells processed) (local)
617.68 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
617.68 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01201 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477302784 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77303 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.29 ACPST (Averaged number of clusters per Simulation Timestep)
20.24 MCPS (Million Cells per Second) (local)
477.303 MCP (Million Cells processed) (local)
617.675 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
617.675 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5825 RT (REAL_TIME)
477310976 CP (Cells processed)
0.235825 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
20.2401 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
617.678 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
617.678 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5829 RT (REAL_TIME)
477298688 CP (Cells processed)
0.235829 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.28 ACPST (Averaged number of clusters per Simulation Timestep)
20.2392 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
617.651 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
617.651 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01196 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5827 RT (REAL_TIME)
477315072 CP (Cells processed)
0.235827 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.32 ACPST (Averaged number of clusters per Simulation Timestep)
20.24 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
617.676 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
617.676 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5829 RT (REAL_TIME)
477298997 CP (Cells processed)
0.235829 ASPT (Averaged Seconds per Timestep)
4.77299 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1234.27 ACPST (Averaged number of clusters per Simulation Timestep)
20.2392 MCPS (Million Cells per Second) (local)
477.299 MCP (Million Cells processed) (local)
654.219 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
617.652 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01196 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5826 RT (REAL_TIME)
477301712 CP (Cells processed)
0.235826 ASPT (Averaged Seconds per Timestep)
4.77302 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1219.62 ACPST (Averaged number of clusters per Simulation Timestep)
20.2395 MCPS (Million Cells per Second) (local)
477.302 MCP (Million Cells processed) (local)
646.461 CPSPT (Clusters per Second per Thread)
145.661 EDMBPT (CellData Megabyte per Timestep (RW))
617.662 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01198 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477319168 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77319 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.33 ACPST (Averaged number of clusters per Simulation Timestep)
20.2407 MCPS (Million Cells per Second) (local)
477.319 MCP (Million Cells processed) (local)
617.696 CPSPT (Clusters per Second per Thread)
145.666 EDMBPT (CellData Megabyte per Timestep (RW))
617.696 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01203 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5826 RT (REAL_TIME)
477297083 CP (Cells processed)
0.235826 ASPT (Averaged Seconds per Timestep)
4.77297 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1182.4 ACPST (Averaged number of clusters per Simulation Timestep)
20.2394 MCPS (Million Cells per Second) (local)
477.297 MCP (Million Cells processed) (local)
626.733 CPSPT (Clusters per Second per Thread)
145.66 EDMBPT (CellData Megabyte per Timestep (RW))
617.656 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01197 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5826 RT (REAL_TIME)
477310707 CP (Cells processed)
0.235826 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.09 ACPST (Averaged number of clusters per Simulation Timestep)
20.2399 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
636.11 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
617.674 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5823 RT (REAL_TIME)
477305012 CP (Cells processed)
0.235823 ASPT (Averaged Seconds per Timestep)
4.77305 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1217.2 ACPST (Averaged number of clusters per Simulation Timestep)
20.24 MCPS (Million Cells per Second) (local)
477.305 MCP (Million Cells processed) (local)
645.188 CPSPT (Clusters per Second per Thread)
145.662 EDMBPT (CellData Megabyte per Timestep (RW))
617.676 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.012 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477310976 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77311 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1165.31 ACPST (Averaged number of clusters per Simulation Timestep)
20.2403 MCPS (Million Cells per Second) (local)
477.311 MCP (Million Cells processed) (local)
617.685 CPSPT (Clusters per Second per Thread)
145.664 EDMBPT (CellData Megabyte per Timestep (RW))
617.685 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01201 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477290066 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.7729 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1200.58 ACPST (Averaged number of clusters per Simulation Timestep)
20.2394 MCPS (Million Cells per Second) (local)
477.29 MCP (Million Cells processed) (local)
636.38 CPSPT (Clusters per Second per Thread)
145.657 EDMBPT (CellData Megabyte per Timestep (RW))
617.658 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01197 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.423361 ST (SIMULATION_TIME)
0.00419381 TSS (Timestep size)
23.5822 RT (REAL_TIME)
477314522 CP (Cells processed)
0.235822 ASPT (Averaged Seconds per Timestep)
4.77315 MCPPT (Million Cells Processed in Average per Simulation Timestep)
1184.33 ACPST (Averaged number of clusters per Simulation Timestep)
20.2405 MCPS (Million Cells per Second) (local)
477.315 MCP (Million Cells processed) (local)
627.767 CPSPT (Clusters per Second per Thread)
145.665 EDMBPT (CellData Megabyte per Timestep (RW))
617.69 EDMBPS (CellData Megabyte per Second (RW))
50 Flops per cell update (Matrix Multiplication)
1.01202 GFLOPS


++++++++++ SUMMARY ++++++++++
323.839 GMCPS (Global Million Cells per Second) (global)
7636.9 GMCP (Global Million Cells Processed) (global)
78.6604 GCPSPT (Global Clusters per Second per Thread) (global)
76.369 GMCPPT (Global Million Cells Processed in Average per Simulation Timestep)
100 GNT (Global Number of Timesteps) (global)

3407872 MSNGCells (Migration Sent Number of Global Cells)
832 MSNGCluster (Migration Sent Number of Global Cluster)
0.000446239 MRC (Migrated Relative Cells)
16.1919 GGFLOPS (Global GFLOPS)

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+ LOAD BALANCING STATISTICS FOR TIMESTEP 101
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Rank	local_clusters	local_cells	target_average	rel_deviation
0	1190	4813956	4.81295e+06	-0.000209617
1	1229	4812983	4.81295e+06	-7.45385e-06
2	1175	4812800	4.81295e+06	3.05686e-05
3	1175	4812800	4.81295e+06	3.05686e-05
4	1175	4812800	4.81295e+06	3.05686e-05
5	1175	4812800	4.81295e+06	3.05686e-05
6	1175	4812800	4.81295e+06	3.05686e-05
7	1244	4814139	4.81295e+06	-0.000247639
8	1221	4810637	4.81295e+06	0.000479981
9	1175	4812800	4.81295e+06	3.05686e-05
10	1193	4815457	4.81295e+06	-0.000521484
11	1209	4812145	4.81295e+06	0.00016666
12	1226	4810706	4.81295e+06	0.000465645
13	1175	4812800	4.81295e+06	3.05686e-05
14	1205	4815464	4.81295e+06	-0.000522938
15	1192	4812067	4.81295e+06	0.000182866
