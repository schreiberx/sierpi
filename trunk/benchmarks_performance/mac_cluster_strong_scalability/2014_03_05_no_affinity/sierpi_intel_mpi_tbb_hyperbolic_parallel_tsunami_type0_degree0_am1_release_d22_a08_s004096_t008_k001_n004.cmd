#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability/2014_03_05_no_affinity/sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s004096_t008_k001_n004.txt
# error
#SBATCH -e /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability/2014_03_05_no_affinity/sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s004096_t008_k001_n004.err
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability
# job description
#SBATCH -J sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_type0_degree0_am1_release_d22_a08_s004096_t008_k001_n004
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=16
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=01:00:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2014_03_05/benchmarks_performance/mac_cluster_strong_scalability

#export KMP_AFFINITY=granularity=fine,compact,1,0
mpiexec.hydra -genv OMP_NUM_THREADS 8 -envall -ppn 2 -n 4 ../../build/sierpi_intel_mpi_tbb_hyperbolic_parallel_tsunami_type0_degree0_am1_release -d 22 -a 8 -t -1 -L 100 -k 1 -r 0.01/0.005 -n 8 -N 8 -o 4096
