Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
240.222 RT (REAL_TIME)
1066342375 CP (Cells processed)
2.40222 ASPT (Averaged Seconds per Timestep)
1.06634e+07 CPST (Cells Processed in Average per Simulation Timestep)
47.6 ACPST (Averaged number of clusters per Simulation Timestep)
4.43899 MCPS (Million Cells per Second) (local)
4.43899 MCPSPT (Million Clusters per Second per Thread)
325.422 EDMBPT (CellData Megabyte per Timestep (RW))
135.467 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.239706 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
240.222 RT (REAL_TIME)
1052587700 CP (Cells processed)
2.40222 ASPT (Averaged Seconds per Timestep)
1.05259e+07 CPST (Cells Processed in Average per Simulation Timestep)
45.35 ACPST (Averaged number of clusters per Simulation Timestep)
4.38173 MCPS (Million Cells per Second) (local)
4.38173 MCPSPT (Million Clusters per Second per Thread)
321.224 EDMBPT (CellData Megabyte per Timestep (RW))
133.72 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.236614 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
240.222 RT (REAL_TIME)
1061396175 CP (Cells processed)
2.40222 ASPT (Averaged Seconds per Timestep)
1.0614e+07 CPST (Cells Processed in Average per Simulation Timestep)
41.8 ACPST (Averaged number of clusters per Simulation Timestep)
4.4184 MCPS (Million Cells per Second) (local)
4.4184 MCPSPT (Million Clusters per Second per Thread)
323.912 EDMBPT (CellData Megabyte per Timestep (RW))
134.839 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238594 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
240.222 RT (REAL_TIME)
1058221568 CP (Cells processed)
2.40222 ASPT (Averaged Seconds per Timestep)
1.05822e+07 CPST (Cells Processed in Average per Simulation Timestep)
49.25 ACPST (Averaged number of clusters per Simulation Timestep)
4.40519 MCPS (Million Cells per Second) (local)
4.40519 MCPSPT (Million Clusters per Second per Thread)
322.944 EDMBPT (CellData Megabyte per Timestep (RW))
134.436 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.23788 GFLOPS


++++++++++ SUMMARY ++++++++++
17.6443 GMCPS (Global Million Cells per Second) (global)
17.6443 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
