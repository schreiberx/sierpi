Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.5452 RT (REAL_TIME)
1066792581 CP (Cells processed)
0.725452 ASPT (Averaged Seconds per Timestep)
1.06679e+07 CPST (Cells Processed in Average per Simulation Timestep)
5121.44 ACPST (Averaged number of clusters per Simulation Timestep)
14.7052 MCPS (Million Cells per Second) (local)
3.6763 MCPSPT (Million Clusters per Second per Thread)
325.559 EDMBPT (CellData Megabyte per Timestep (RW))
448.768 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.794082 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.5452 RT (REAL_TIME)
1052024832 CP (Cells processed)
0.725452 ASPT (Averaged Seconds per Timestep)
1.05202e+07 CPST (Cells Processed in Average per Simulation Timestep)
5136.84 ACPST (Averaged number of clusters per Simulation Timestep)
14.5017 MCPS (Million Cells per Second) (local)
3.62541 MCPSPT (Million Clusters per Second per Thread)
321.053 EDMBPT (CellData Megabyte per Timestep (RW))
442.555 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.783089 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.5452 RT (REAL_TIME)
1061729683 CP (Cells processed)
0.725452 ASPT (Averaged Seconds per Timestep)
1.06173e+07 CPST (Cells Processed in Average per Simulation Timestep)
5135.14 ACPST (Averaged number of clusters per Simulation Timestep)
14.6354 MCPS (Million Cells per Second) (local)
3.65886 MCPSPT (Million Clusters per Second per Thread)
324.014 EDMBPT (CellData Megabyte per Timestep (RW))
446.638 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.790313 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.5452 RT (REAL_TIME)
1058000720 CP (Cells processed)
0.725452 ASPT (Averaged Seconds per Timestep)
1.058e+07 CPST (Cells Processed in Average per Simulation Timestep)
5128.59 ACPST (Averaged number of clusters per Simulation Timestep)
14.584 MCPS (Million Cells per Second) (local)
3.64601 MCPSPT (Million Clusters per Second per Thread)
322.876 EDMBPT (CellData Megabyte per Timestep (RW))
445.069 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.787537 GFLOPS


++++++++++ SUMMARY ++++++++++
58.4263 GMCPS (Global Million Cells per Second) (global)
14.6066 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
