Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
804.993 RT (REAL_TIME)
4238547816 CP (Cells processed)
8.04993 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20608.5 ACPST (Averaged number of clusters per Simulation Timestep)
5.26532 MCPS (Million Cells per Second) (local)
5.26532 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
160.685 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284327 GFLOPS

++++++++++ SUMMARY ++++++++++
5.26532 GMCPS (Global Million Cells per Second) (global)
5.26532 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
