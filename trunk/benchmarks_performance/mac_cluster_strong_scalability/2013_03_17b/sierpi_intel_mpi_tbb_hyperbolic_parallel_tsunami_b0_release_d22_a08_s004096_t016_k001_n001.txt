Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
85.2881 RT (REAL_TIME)
4238547816 CP (Cells processed)
0.852881 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20665.8 ACPST (Averaged number of clusters per Simulation Timestep)
49.6968 MCPS (Million Cells per Second) (local)
3.10605 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
1516.63 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.68363 GFLOPS

++++++++++ SUMMARY ++++++++++
49.6968 GMCPS (Global Million Cells per Second) (global)
3.10605 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
