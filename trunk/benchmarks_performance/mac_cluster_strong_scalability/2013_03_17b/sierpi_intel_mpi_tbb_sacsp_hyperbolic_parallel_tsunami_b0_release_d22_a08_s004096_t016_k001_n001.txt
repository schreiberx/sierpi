Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.9529 RT (REAL_TIME)
4238547816 CP (Cells processed)
0.659529 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20665.8 ACPST (Averaged number of clusters per Simulation Timestep)
64.2662 MCPS (Million Cells per Second) (local)
4.01664 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
1961.25 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.47038 GFLOPS

++++++++++ SUMMARY ++++++++++
64.2662 GMCPS (Global Million Cells per Second) (global)
4.01664 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
