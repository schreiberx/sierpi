Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
221.549 RT (REAL_TIME)
1066342375 CP (Cells processed)
2.21549 ASPT (Averaged Seconds per Timestep)
1.06634e+07 CPST (Cells Processed in Average per Simulation Timestep)
47.6 ACPST (Averaged number of clusters per Simulation Timestep)
4.81312 MCPS (Million Cells per Second) (local)
4.81312 MCPSPT (Million Clusters per Second per Thread)
325.422 EDMBPT (CellData Megabyte per Timestep (RW))
146.885 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.259909 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
221.549 RT (REAL_TIME)
1052587700 CP (Cells processed)
2.21549 ASPT (Averaged Seconds per Timestep)
1.05259e+07 CPST (Cells Processed in Average per Simulation Timestep)
45.35 ACPST (Averaged number of clusters per Simulation Timestep)
4.75104 MCPS (Million Cells per Second) (local)
4.75104 MCPSPT (Million Clusters per Second per Thread)
321.224 EDMBPT (CellData Megabyte per Timestep (RW))
144.99 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.256556 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
221.549 RT (REAL_TIME)
1061396175 CP (Cells processed)
2.21549 ASPT (Averaged Seconds per Timestep)
1.0614e+07 CPST (Cells Processed in Average per Simulation Timestep)
41.8 ACPST (Averaged number of clusters per Simulation Timestep)
4.7908 MCPS (Million Cells per Second) (local)
4.7908 MCPSPT (Million Clusters per Second per Thread)
323.912 EDMBPT (CellData Megabyte per Timestep (RW))
146.203 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.258703 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
221.549 RT (REAL_TIME)
1058221568 CP (Cells processed)
2.21549 ASPT (Averaged Seconds per Timestep)
1.05822e+07 CPST (Cells Processed in Average per Simulation Timestep)
49.25 ACPST (Averaged number of clusters per Simulation Timestep)
4.77647 MCPS (Million Cells per Second) (local)
4.77647 MCPSPT (Million Clusters per Second per Thread)
322.944 EDMBPT (CellData Megabyte per Timestep (RW))
145.766 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.257929 GFLOPS


++++++++++ SUMMARY ++++++++++
19.1314 GMCPS (Global Million Cells per Second) (global)
19.1314 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
