Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.4449 RT (REAL_TIME)
1059636869 CP (Cells processed)
0.284449 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5158.26 ACPST (Averaged number of clusters per Simulation Timestep)
37.2523 MCPS (Million Cells per Second) (local)
4.65654 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1136.85 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.01163 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.4448 RT (REAL_TIME)
1059641344 CP (Cells processed)
0.284448 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5174.03 ACPST (Averaged number of clusters per Simulation Timestep)
37.2525 MCPS (Million Cells per Second) (local)
4.65656 MCPSPT (Million Clusters per Second per Thread)
323.377 EDMBPT (CellData Megabyte per Timestep (RW))
1136.86 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.01163 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.4445 RT (REAL_TIME)
1059632531 CP (Cells processed)
0.284445 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
5165.42 ACPST (Averaged number of clusters per Simulation Timestep)
37.2526 MCPS (Million Cells per Second) (local)
4.65657 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
1136.86 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.01164 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
28.4445 RT (REAL_TIME)
1059637072 CP (Cells processed)
0.284445 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5168.09 ACPST (Averaged number of clusters per Simulation Timestep)
37.2527 MCPS (Million Cells per Second) (local)
4.65659 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1136.86 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.01165 GFLOPS


++++++++++ SUMMARY ++++++++++
149.01 GMCPS (Global Million Cells per Second) (global)
18.6263 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
