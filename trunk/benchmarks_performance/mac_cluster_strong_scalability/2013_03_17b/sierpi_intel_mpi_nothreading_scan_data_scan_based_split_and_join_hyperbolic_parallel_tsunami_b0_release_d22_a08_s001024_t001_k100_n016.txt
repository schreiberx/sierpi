Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
263097696 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.63098e+06 CPST (Cells Processed in Average per Simulation Timestep)
41.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.9931 MCPS (Million Cells per Second) (local)
3.9931 MCPSPT (Million Clusters per Second per Thread)
80.291 EDMBPT (CellData Megabyte per Timestep (RW))
121.86 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215627 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
266758279 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.66758e+06 CPST (Cells Processed in Average per Simulation Timestep)
40 ACPST (Averaged number of clusters per Simulation Timestep)
4.04865 MCPS (Million Cells per Second) (local)
4.04865 MCPSPT (Million Clusters per Second per Thread)
81.4082 EDMBPT (CellData Megabyte per Timestep (RW))
123.555 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.218627 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
267993569 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.67994e+06 CPST (Cells Processed in Average per Simulation Timestep)
43.2 ACPST (Averaged number of clusters per Simulation Timestep)
4.0674 MCPS (Million Cells per Second) (local)
4.0674 MCPSPT (Million Clusters per Second per Thread)
81.7851 EDMBPT (CellData Megabyte per Timestep (RW))
124.127 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21964 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
268368959 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.68369e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.84 ACPST (Averaged number of clusters per Simulation Timestep)
4.0731 MCPS (Million Cells per Second) (local)
4.0731 MCPSPT (Million Clusters per Second per Thread)
81.8997 EDMBPT (CellData Megabyte per Timestep (RW))
124.301 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.219947 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
262573576 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.62574e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.84 ACPST (Averaged number of clusters per Simulation Timestep)
3.98514 MCPS (Million Cells per Second) (local)
3.98514 MCPSPT (Million Clusters per Second per Thread)
80.1311 EDMBPT (CellData Megabyte per Timestep (RW))
121.617 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215198 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
263622152 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.63622e+06 CPST (Cells Processed in Average per Simulation Timestep)
39 ACPST (Averaged number of clusters per Simulation Timestep)
4.00106 MCPS (Million Cells per Second) (local)
4.00106 MCPSPT (Million Clusters per Second per Thread)
80.4511 EDMBPT (CellData Megabyte per Timestep (RW))
122.103 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.216057 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
263359840 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.6336e+06 CPST (Cells Processed in Average per Simulation Timestep)
37.64 ACPST (Averaged number of clusters per Simulation Timestep)
3.99707 MCPS (Million Cells per Second) (local)
3.99707 MCPSPT (Million Clusters per Second per Thread)
80.371 EDMBPT (CellData Megabyte per Timestep (RW))
121.981 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215842 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
263163232 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.63163e+06 CPST (Cells Processed in Average per Simulation Timestep)
43.16 ACPST (Averaged number of clusters per Simulation Timestep)
3.99409 MCPS (Million Cells per Second) (local)
3.99409 MCPSPT (Million Clusters per Second per Thread)
80.311 EDMBPT (CellData Megabyte per Timestep (RW))
121.89 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215681 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
263294472 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.63294e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.48 ACPST (Averaged number of clusters per Simulation Timestep)
3.99608 MCPS (Million Cells per Second) (local)
3.99608 MCPSPT (Million Clusters per Second per Thread)
80.3511 EDMBPT (CellData Megabyte per Timestep (RW))
121.951 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215788 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
264281334 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.64281e+06 CPST (Cells Processed in Average per Simulation Timestep)
40 ACPST (Averaged number of clusters per Simulation Timestep)
4.01106 MCPS (Million Cells per Second) (local)
4.01106 MCPSPT (Million Clusters per Second per Thread)
80.6523 EDMBPT (CellData Megabyte per Timestep (RW))
122.408 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.216597 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
270428193 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.70428e+06 CPST (Cells Processed in Average per Simulation Timestep)
36.88 ACPST (Averaged number of clusters per Simulation Timestep)
4.10435 MCPS (Million Cells per Second) (local)
4.10435 MCPSPT (Million Clusters per Second per Thread)
82.5281 EDMBPT (CellData Megabyte per Timestep (RW))
125.255 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.221635 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
263357284 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.63357e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.16 ACPST (Averaged number of clusters per Simulation Timestep)
3.99704 MCPS (Million Cells per Second) (local)
3.99704 MCPSPT (Million Clusters per Second per Thread)
80.3703 EDMBPT (CellData Megabyte per Timestep (RW))
121.98 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21584 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
263228768 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.63229e+06 CPST (Cells Processed in Average per Simulation Timestep)
42.32 ACPST (Averaged number of clusters per Simulation Timestep)
3.99509 MCPS (Million Cells per Second) (local)
3.99509 MCPSPT (Million Clusters per Second per Thread)
80.331 EDMBPT (CellData Megabyte per Timestep (RW))
121.92 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215735 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
263228768 CP (Cells processed)
0.658881 ASPT (Averaged Seconds per Timestep)
2.63229e+06 CPST (Cells Processed in Average per Simulation Timestep)
38.64 ACPST (Averaged number of clusters per Simulation Timestep)
3.99509 MCPS (Million Cells per Second) (local)
3.99509 MCPSPT (Million Clusters per Second per Thread)
80.331 EDMBPT (CellData Megabyte per Timestep (RW))
121.92 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215735 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8882 RT (REAL_TIME)
268694000 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.68694e+06 CPST (Cells Processed in Average per Simulation Timestep)
39.04 ACPST (Averaged number of clusters per Simulation Timestep)
4.07803 MCPS (Million Cells per Second) (local)
4.07803 MCPSPT (Million Clusters per Second per Thread)
81.9989 EDMBPT (CellData Megabyte per Timestep (RW))
124.452 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.220214 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
65.8881 RT (REAL_TIME)
263097696 CP (Cells processed)
0.658882 ASPT (Averaged Seconds per Timestep)
2.63098e+06 CPST (Cells Processed in Average per Simulation Timestep)
41.4 ACPST (Averaged number of clusters per Simulation Timestep)
3.9931 MCPS (Million Cells per Second) (local)
3.9931 MCPSPT (Million Clusters per Second per Thread)
80.291 EDMBPT (CellData Megabyte per Timestep (RW))
121.86 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.215627 GFLOPS


++++++++++ SUMMARY ++++++++++
64.3294 GMCPS (Global Million Cells per Second) (global)
64.3294 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
