Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.229 RT (REAL_TIME)
1059636869 CP (Cells processed)
2.01229 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5158.26 ACPST (Averaged number of clusters per Simulation Timestep)
5.26582 MCPS (Million Cells per Second) (local)
5.26582 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
160.7 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284354 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.229 RT (REAL_TIME)
1059641344 CP (Cells processed)
2.01229 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5174.03 ACPST (Averaged number of clusters per Simulation Timestep)
5.26584 MCPS (Million Cells per Second) (local)
5.26584 MCPSPT (Million Clusters per Second per Thread)
323.377 EDMBPT (CellData Megabyte per Timestep (RW))
160.701 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284355 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.228 RT (REAL_TIME)
1059632531 CP (Cells processed)
2.01228 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
5165.42 ACPST (Averaged number of clusters per Simulation Timestep)
5.26583 MCPS (Million Cells per Second) (local)
5.26583 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
160.7 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284355 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.228 RT (REAL_TIME)
1059637072 CP (Cells processed)
2.01228 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5168.09 ACPST (Averaged number of clusters per Simulation Timestep)
5.26585 MCPS (Million Cells per Second) (local)
5.26585 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
160.701 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284356 GFLOPS


++++++++++ SUMMARY ++++++++++
21.0633 GMCPS (Global Million Cells per Second) (global)
21.0633 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
