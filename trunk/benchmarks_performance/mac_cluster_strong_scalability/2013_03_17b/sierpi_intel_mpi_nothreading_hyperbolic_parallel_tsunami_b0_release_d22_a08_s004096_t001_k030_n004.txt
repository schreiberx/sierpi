Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
268.339 RT (REAL_TIME)
1061678725 CP (Cells processed)
2.68339 ASPT (Averaged Seconds per Timestep)
1.06168e+07 CPST (Cells Processed in Average per Simulation Timestep)
5141.23 ACPST (Averaged number of clusters per Simulation Timestep)
3.95648 MCPS (Million Cells per Second) (local)
3.95648 MCPSPT (Million Clusters per Second per Thread)
323.999 EDMBPT (CellData Megabyte per Timestep (RW))
120.742 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.21365 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
268.339 RT (REAL_TIME)
1057447936 CP (Cells processed)
2.68339 ASPT (Averaged Seconds per Timestep)
1.05745e+07 CPST (Cells Processed in Average per Simulation Timestep)
5163.32 ACPST (Averaged number of clusters per Simulation Timestep)
3.94071 MCPS (Million Cells per Second) (local)
3.94071 MCPSPT (Million Clusters per Second per Thread)
322.707 EDMBPT (CellData Megabyte per Timestep (RW))
120.261 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.212798 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
268.339 RT (REAL_TIME)
1060248979 CP (Cells processed)
2.68339 ASPT (Averaged Seconds per Timestep)
1.06025e+07 CPST (Cells Processed in Average per Simulation Timestep)
5150.67 ACPST (Averaged number of clusters per Simulation Timestep)
3.95115 MCPS (Million Cells per Second) (local)
3.95115 MCPSPT (Million Clusters per Second per Thread)
323.562 EDMBPT (CellData Megabyte per Timestep (RW))
120.579 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.213362 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
268.339 RT (REAL_TIME)
1059172176 CP (Cells processed)
2.68339 ASPT (Averaged Seconds per Timestep)
1.05917e+07 CPST (Cells Processed in Average per Simulation Timestep)
5153.24 ACPST (Averaged number of clusters per Simulation Timestep)
3.94714 MCPS (Million Cells per Second) (local)
3.94714 MCPSPT (Million Clusters per Second per Thread)
323.234 EDMBPT (CellData Megabyte per Timestep (RW))
120.457 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.213145 GFLOPS


++++++++++ SUMMARY ++++++++++
15.7955 GMCPS (Global Million Cells per Second) (global)
15.7955 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
