#! /usr/bin/python

import sys
import time
import commands
import os


####################
# CONFIG START
####################

######################################
# adaptivity mode
# 1: height based
# 2: net-update based
# 3: eigen-coefficient based
#
adaptivity_mode=2



######################################
# number of cores to use for simulation
#
ncores=-1



######################################
# adaptivity criterias
#
adaptivity_params= [
		"0.0000000001/0.000000000001",
		"0.00000001/0.0000000001",
		"0.000001/0.00000001",
#		"0.0001/0.000001",
#		"0.01/0.0001"
	]

# initial depth range
depths=range(0,6)

# relative adaptivity depths
adapts=range(0,6)

# maximum depth
max_depth=14

####################
# CONFIG END
####################



curdir="benchmarks_tsunami_pub/single_wave_on_simple_beach/"

cmd="cd ../..; scons --xml-config="+curdir+"single_wave_on_simple_beach_gauge_profile_data.xml --hyperbolic-adaptivity-mode="+str(adaptivity_mode)+" -j 8"
print cmd

os.system(cmd);

for r in adaptivity_params:

	print
	print "Adaptivity parameters: "+r

	for d in depths:
		for a in adapts:
			if d+a >= max_depth:
				continue

			# dart stations
			p="d"+str(d)+"_a"+str(a)+"_"+r.replace('/','_')+".csv"
			dart_stations="0.25/0/station_0.25_"+p+",9.95/0/station_9.95_"+p

			cmd="../../build/sierpi_intel_omp_hyperbolic_parallel_tsunami_b0_release -c single_wave_on_simple_beach_gauge_profile_data.xml -d "+str(d)+" -a "+str(a)+" -r "+str(r)+" -n "+str(ncores)+" -D "+dart_stations

			print cmd

			os.system(cmd)

