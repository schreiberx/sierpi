from sympy import sqrt
from sympy import Symbol, symbols, var
from sympy import diff,integrate,simplify
from sympy.matrices import Matrix

from sympy import solve

from mpmath import identify

import jacobi


def getPoints1D(num_points):
	a=Symbol('a')
	sampling_points=simplify(solve(jacobi.jacobi_poly(num_points, 0, 0, a), a))
	sampling_points.sort()

	def pdiff(a):
		return simplify(diff(jacobi.jacobi_poly(num_points, 0, 0, a), a))


	def pw(val):
		return simplify(2/((1-a**2)*(pdiff(a)**2))).subs(a, val)

	sampling_weights=[simplify(pw(sampling_points[i])) for i in range(0, len(sampling_points))]

	return (sampling_points, sampling_weights)

#	ret_val = []
#	for i in range(0, num_points):
#		ret_val.append([sampling_points[i], sampling_weights[i]])
#
#	return ret_val




def getTrianglePoints2D(order):
	dunavantOrderNum=[1, 3, 4, 6]

	dunavantCoords=[
		[
			[0.333333333333333,	0.333333333333333]	],
		[
			[0.666666666666667,	0.166666666666667],		[0.166666666666667,	0.166666666666667],		[0.166666666666667,	0.666666666666667]	],
		[
			[0.333333333333333,	0.333333333333333],		[0.6,	0.2],		[0.2,	0.2],		[0.2,	0.6]	],
		[
			[0.10810301816807,	0.445948490915965],		[0.445948490915965,	0.445948490915965],		[0.445948490915965,	0.10810301816807],		[0.816847572980459,	0.09157621350977101],		[0.09157621350977101,	0.09157621350977101],		[0.09157621350977101,	0.816847572980459]	],
	]

	dunavantWeights=[
		[1	],
		[0.333333333333333,0.333333333333333,0.333333333333333	],
		[-0.5625,0.520833333333333,0.520833333333333,0.520833333333333	],
		[0.223381589678011,0.223381589678011,0.223381589678011,0.109951743655322,0.109951743655322,0.109951743655322	],
	]

	c = 0
	for i in range(0, len(dunavantOrderNum)):
		c = i
		if dunavantOrderNum[i] >= c:
			break

	weights = dunavantWeights[c]
	coords = dunavantCoords[c]

#	weights = [identify(w) for w in weights]
#	coords = [[identify(c[0]), identify(c[1])] for c in coords]

	return (coords, weights)

