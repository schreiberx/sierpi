openmpi-1.7:
	wget http://www.open-mpi.org/software/ompi/v1.7/downloads/openmpi-1.7.tar.bz2
	tar xjf openmpi-1.7.tar.bz2
	cd openmpi-1.7
	./configure --enable-mpi-thread-multiple --prefix=$HOME/local/openmpi-1.7
	make all install -j 6

openmpi-1.6.4:
	wget http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.4.tar.bz2
	tar xjf openmpi-1.6.4.tar.bz2
	cd openmpi-1.6.4
	./configure --enable-mpi-thread-multiple --prefix=$HOME/local/openmpi-1.6.4
	make all install -j 6

# to compile with intel compilers, use
	./configure --enable-mpi-thread-multiple --prefix=$HOME/local/openmpi-1.6.4 CC=icc CXX=icpc F77=ifort FC=ifort 


for openmpi-1.4.3:
	./configure --enable-mpi-threads --prefix=$HOME/local/openmpi-1.4.3 CC=icc CXX=icpc F77=ifort FC=ifort 
