#! /usr/bin/python

###################################################################
# Maple matrix market output for DG to C++ Sierpinski-Code
#
###################################################################
# Copyright (C) 2013 Technische Universitaet Muenchen
# This file is part of the Sierpinski project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpinski
#
# Created on: Aug 7, 2012
# Author: Martin Schreiber <martin.schreiber@in.tum.de>
###################################################################
#
#

import os
import re
import sys


output_dir = '../../../src/simulations/hyperbolic_common/basis_functions_and_matrices/'


matrices_identifier_2d = [
	'timestep_inv_mass',

	'timestep_stiffness_u',
	'timestep_stiffness_v',

	'timestep_stiffness_dofs_to_nodes',

	'timestep_dofs_to_hyp_face',
	'timestep_dofs_to_left_face',
	'timestep_dofs_to_right_face',

	'timestep_hyp_fluxes_to_dofs',
	'timestep_left_fluxes_to_dofs',
	'timestep_right_fluxes_to_dofs',

	'adaptivity_coarsen_left',
	'adaptivity_coarsen_right',
	'adaptivity_refine_left',
	'adaptivity_refine_right',

	'convert_poly_to_dofs',
	'convert_dofs_to_poly',
	'convert_dofs_to_0th_order',
]


matrices_identifier_3d = [
	'timestep_stiffness_w',

	'timestep_dofs_to_bottom_face',
	'timestep_dofs_to_top_face',

	'timestep_bottom_fluxes_to_dofs',
	'timestep_top_fluxes_to_dofs',
]


row_major_matrix_storage = True



#################################################
# load number of basis functions (equal to cols and rows of inverted mass matrix
#
def load_matrix_size(
	matrix_market_file
):
	filename = matrix_market_file

	f = open(filename, 'r')

	# read headerline
	f.readline()

	(cols,rows) = f.readline().split(' ')

	f.close()

	rows = int(rows)
	cols = int(cols)


	return (rows, cols)




#################################################
# dummy matrix
#
def load_matrix(
		matrix_identifier
):
	filename = matrix_identifier+'.mtx'

	(cols,rows) = load_matrix_size(filename)

	f = open(filename, 'r')
	f.readline()
	f.readline()

	values = []
	while True:
		t=f.readline()
		if not t:
			break

		if t.find(' ') != -1:
			t2 = t.split(' ')
			t = t2[0]

		t = t.replace("\n", '')

		values.append(t)

	matrix = [ [ values[j*rows+i] for j in range(0, cols) ] for i in range (0, rows) ]

	return matrix


#################################################
# print matrix values with C-formatation
#
def get_matrix_data_c_code(
	matrix
):
	n = len(matrix)
	m = len(matrix[0])


	s = "\n"
	s +="\t\t{\n"
	for i in range(0, n):
		s += "\t\t\t{"
		
		for j in range(0, m):
			s += str(matrix[i][j])
			if j < m-1:
				s += ","

		if i < n-1:
			s += "},\n"

	s += "}\n"
	s += "\t\t}"
	
	return (s)


#################################################
# get the std::cout output code for the matrix
#################################################
def get_output_matrix_operation_c_code(
	name,	# name of the matrix
	filename,
	number_of_basis_functions
):	
	print "	Loading matrix '"+name+"' from '"+filename+"'"

	# load matrix data to list
	matrix = load_matrix(filename)

	m = len(matrix)
	n = len(matrix[0])

	# load matrix source code
	matrix_data_c_code = get_matrix_data_c_code(matrix)

	s ="""
		std::cout << std::endl;
		std::cout << \""""+name+""":\" << std::endl;
		static const T """+name+"""["""+str(m)+"""]["""+str(n)+"""] = """+matrix_data_c_code+""";
		for (int j = 0; j < """+str(m)+"""; j++)
		{
			std::cout << """+name+"""[j][0];
			for (int i = 1; i < """+str(n)+"""; i++)
			{
				std::cout << ", " << """+name+"""[j][i];
			}
			std::cout << std::endl;
		}

	"""

	return (s, 0)


#################################################
# get the code for the matrix-multiplication method
#################################################
def get_matrix_operation_c_code(
	name,	# name of the matrix
	filename,
	number_of_basis_functions
):	
	print "	Loading matrix '"+name+"' from '"+filename+"'"

	# load matrix data to list
	matrix = load_matrix(filename)

	m = len(matrix)
	n = len(matrix[0])

	# load matrix source code
	matrix_data_c_code = get_matrix_data_c_code(matrix)

	if not row_major_matrix_storage:
		#
		# COL MAJOR STORAGE
		#
		s ="""

	/*********************************************************
	 * """+name+"""
	 *********************************************************/
public:
	static inline void mul_"""+name+"""(
		const T i_input["""+str(n)+"""],
		T o_output["""+str(m)+"""]
	) {
		static const T matrix["""+str(n)+"""]["""+str(m)+"""] = """+matrix_data_c_code+""";

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < """+str(m)+"""; i++)
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < """+str(n)+"""; j++)
		{

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < """+str(m)+"""; i++)
				o_output[i] += matrix[j][i]*i_input[j];
		}
	}

	/*********************************************************
	 * """+name+"""
	 *********************************************************/
public:
	static inline void madd_"""+name+"""(
		const T i_input["""+str(n)+"""],
		T o_output["""+str(m)+"""]
	) {
		static const T matrix["""+str(n)+"""]["""+str(m)+"""] = """+matrix_data_c_code+""";

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int j = 0; j < """+str(n)+"""; j++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int i = 0; i < """+str(m)+"""; i++)
				o_output[i] += matrix[j][i]*i_input[j];
		}
	}
"""
	else:
		# TODO: not tested yet!!!

		#
		# ROW MAJOR STORAGE
		#
		s ="""

	/*********************************************************
	 * """+name+"""
	 *********************************************************/
public:
	static inline void mul_"""+name+"""(
		const T i_input["""+str(n)+"""],
		T o_output["""+str(m)+"""]
	) {
		static const T matrix["""+str(m)+"""]["""+str(n)+"""] = """+matrix_data_c_code+""";

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < """+str(m)+"""; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < """+str(n)+"""; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * """+name+"""
	 *********************************************************/
public:
	static inline void madd_"""+name+"""(
		const T i_input["""+str(n)+"""],
		T o_output["""+str(m)+"""]
	) {
		static const T matrix["""+str(m)+"""]["""+str(n)+"""] = """+matrix_data_c_code+""";

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < """+str(m)+"""; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < """+str(n)+"""; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}
"""

	# 5 conserved quantities for euler
	return (s, (n*m)*5)


def get_rotation_operations(dimensions):
	s = [
		['hyp', '0'],
		['right', '1'],
		['left', '2']
	]

	ret = ""

	for i in s:
		ret += """

public:
        static inline void mul_edge_comm_"""+i[0]+"""_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_"""+("FACE_" if dimensions == 3 else "")+"""DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_"""+("FACE_" if dimensions == 3 else "")+"""DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_"""+("FACE_" if dimensions == 3 else "")+"""DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,"""+i[1]+""">(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }
"""

	s2 = [
		['left',	'-(T)M_SQRT1_2, -(T)M_SQRT1_2',	'-(T)M_SQRT1_2, (T)M_SQRT1_2',	],
		['right',	'-(T)M_SQRT1_2, (T)M_SQRT1_2',	'-(T)M_SQRT1_2, -(T)M_SQRT1_2'	],
	]

	for i in s2:
		ret += """
public:
        static inline void mul_adaptivity_project_momentum_reference_to_"""+i[0]+"""_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        """+i[1]+"""
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_"""+i[0]+"""_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        """+i[2]+"""
                        );
                }
        }


"""

	return ret


def get_distorted_grid_methods(
	number_of_basis_functions
	):
	return """
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

public:
	static inline void mul_edge_comm_transform_to_edge_space(
		const T i_rotationMatrix[2][2],
		const T i_inverseTransformationMatrix[2][2],
		T io_hu["""+str(number_of_basis_functions)+"""],
		T io_hv["""+str(number_of_basis_functions)+"""]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < """+str(number_of_basis_functions)+"""; i++)
		{
			/*
			 * scale and shear from reference space
			 */
			CTriangle_VectorProjections::matrixTransformation<T>(
				i_inverseTransformationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

			/*
			 * rotate edge comm data to edge space
			 */
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace<T>(
				i_rotationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

		}
	}
#endif
"""



def create_matrices_multiplication_c_code(
	src_directory,	# source directory with matrix files
	dst_filename,	# destination file name
	mtype,		# type of matrix (1: modal)
	degree,		# degree of matrix
	dimensions	# dimensions of basis functions
):
	s = open(dst_filename, 'w')

	print "WRITING '"+dst_filename+"'"

	s.write("""
/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Apr 19, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION"""+str(dimensions)+"""D_MATRICES_CONST_HPP_
#define CCOMPUTATION"""+str(dimensions)+"""D_MATRICES_CONST_HPP_

#include <string.h>
#include <assert.h>

#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"

""")

	# degree of basis functions
	s.write("#define SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS ("+degree+")\n")


	# load number of basis functions
	(number_of_basis_functions, x) = load_matrix_size(src_directory+"/"+'timestep_inv_mass.mtx')
	s.write("#define SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS ("+str(number_of_basis_functions)+")\n")
	s.write("#define SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS ("+str(number_of_basis_functions)+")\n")

	# load number edge face dofs
	(x, number_of_edge_face_dofs) = load_matrix_size(src_directory+"/"+'timestep_dofs_to_hyp_face.mtx')
	s.write("#define SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_"+("FACE_" if dimensions == 3 else "")+"DOFS ("+str(number_of_edge_face_dofs)+")\n")

	if dimensions > 2:
		# load number of top/bottom face dofs
		(x, number_of_top_bottom_face_dofs) = load_matrix_size(src_directory+"/"+'timestep_dofs_to_top_face.mtx')
		s.write("#define SIMULATION_HYPERBOLIC_NUMBER_OF_TOP_BOTTOM_FACE_DOFS ("+str(number_of_top_bottom_face_dofs)+")\n")

	# recommended CFL
	s.write("#define SIMULATION_HYPERBOLIC_CFL	(0.5/(T)(2*"+degree+"+1))\n")

	s.write("#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS)\n")

	s.write("#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS*2)\n")

	s.write("""
class CDG_MatrixComputations_"""+str(dimensions)+"""D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

""")

	s.write(get_rotation_operations(dimensions))
	s.write(get_distorted_grid_methods(number_of_basis_functions))

#	s.write("\tassert("+str(number_of_basis_functions)+" == SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS);")

	number_of_flops = 0

	matrices = []

	matrices += matrices_identifier_2d

	if dimensions == 3:
		matrices += matrices_identifier_3d


	for i in matrices:
		(code, flops) = get_matrix_operation_c_code(i, src_directory+"/"+i, number_of_basis_functions)
		s.write(code)
		a='timestep'
		if i[0:len(a)] == a:
			print "\t[counting "+str(flops)+" flops]"
			number_of_flops += flops



	s.write("""
public:
	static inline int getNumberOfFlops()
	{
		return """+str(number_of_flops)+""";
	}


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
public:
	static void setup(int i_verbosity_level)
	{
	}


public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 0" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
//		std::cout << " + Integration cell degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
//		std::cout << " + Integration edge degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;
		std::cout << std::endl;
""")

	for i in matrices:
		print i
		(code, flops) = get_output_matrix_operation_c_code(i, src_directory+"/"+i, number_of_basis_functions)
		s.write(code)

	s.write("""
		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif
"""
	)

	s.close()



###############################################################################
###############################################################################
###############################################################################


def create_code_for_directory(
	directory,
	output_dir_extension,
	dimensions
):
	print "Create code for directory "+directory

	dirs = os.listdir(directory)

	print dirs

	# This would print all the files and directories
	for d in dirs:
		dirpath = directory+'/'+d

		if not os.path.isdir(dirpath):
			continue

		if len(os.listdir(dirpath))==0:
			print "WARNING: "+dirpath+" is empty"
			continue

		matchObj = re.match(r'type_(.*)_degree_(.*)', d, re.M|re.I)

		if not matchObj:
			print "Directory pattern not recognized: "+d
			sys.exit(-1)

		mtype = matchObj.group(1)
		degree = matchObj.group(2)

		dst_dir = output_dir+'matrices_'+output_dir_extension+'_type_'+mtype+'_degree_'+degree
		if not os.path.exists(dst_dir):
			os.makedirs(dst_dir)

		print dst_dir

		output_file=dst_dir+'/CComputation'+str(dimensions)+'D_Matrices_Const.hpp'

		print "Writing code file '"+output_file+"'"

		create_matrices_multiplication_c_code(dirpath, output_file, mtype, degree, dimensions)

		print


create_code_for_directory('dg_matrices_2d_triangle', '2d_triangle', 2)

create_code_for_directory('dg_matrices_3d_prism', '3d_prism', 3)

