#! /usr/bin/python


###################################################################
# This executes matrices_for_higher_order_2d_and_3d.mpl with maple several times with specified parameters
#
###################################################################
# Copyright (C) 2013 Technische Universitaet Muenchen
# This file is part of the Sierpinski project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpinski
#
# Created on: Aug 7, 2012
# Author: Martin Schreiber <martin.schreiber@in.tum.de>
###################################################################


import sys
import commands



#maple_bin="~/software/maple16/bin/maple"
maple_bin="~/software/maple13/bin/maple"
mpl_file="matrices_for_higher_order_2d_and_3d.mpl"


def create_matrices(types, dimensions, degrees):
	for dim in dimensions:
		for btype in types:
			for degree in degrees:
				c=""
				c+=" -c progArgNumberOfDimensions:="+str(dim)
				c+=" -c progArgBasisFunctionType:="+str(btype)
				c+=" -c progArgDegreeOfBasisFunctions:="+str(degree)

				cmd=maple_bin+" "+c+" "+mpl_file
				print cmd
				commands.getoutput(cmd)
			

# only create 2D matrices for nodal type
if True:
#if False:
	create_matrices(
		[0],			# types: nodal
		[2,3],			# dimensions
		[0, 1, 2, 3, 4]		# degree, degrees above 3 are hard to compute due to dunavot support points
	)

# Modal with orthonormal polynomials
if True:
#if False:
	create_matrices(
		[1],			# types: orthonormal
		[2, 3],		# dimensions
		[0, 1, 2, 3, 4]	# degree
	)

# Monomial
if True:
#if False:
	create_matrices(
		[2],			# type: monomial
		[2],			# dimensions
		[0, 1, 2, 3]	# degree
	)
