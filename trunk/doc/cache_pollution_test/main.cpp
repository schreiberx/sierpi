#include <iostream>
#include <stdlib.h>
#include "../../src/lib/CStopwatch.hpp"



void invalidate_array(
	volatile int *a,
	int N
)
{
	for (int i = 1; i < N; i++)
	{
		a[i] += a[i-1];
	}
}

void cache_invalidate()
{
	int N = 1024*1024*10;
	volatile int *a = new int[N];

	for (int i = 0; i < N; i++)
		a[i] = N;

	invalidate_array(a, N);


	delete [] a;
}


int main(
	int argc,
	char *argv[]
)
{
	int N = 1024*1024*16;
	int K = 64;

	if (argc > 1)
		N = atoi(argv[1]);

	if (argc > 2)
		K = atoi(argv[2]);

	float *a = new float[N];
	float *b = new float[N];

	for (int i = 0; i < N; i++)
	{
		a[i] = i;
		b[i] = 0;
	}


	double time1;
	{
		CStopwatch cStopwatch;
		for (int j = 0; j < K; j++)
		{
			cache_invalidate();
			cStopwatch.start();

			for (int i = 0; i < N; i++)
				a[i] += 1;

			cStopwatch.stop();
		}

		time1 = cStopwatch();
	}

	for (int i = 0; i < N; i++)
		if (a[i] != i+K)
			std::cerr << "ERROR" << std::endl;


	for (int i = 0; i < N; i++)
	{
		a[i] = i;
		b[i] = 0;
	}

//	std::cout << "update and write to additional buffer" << std::endl;

	double time2;
	{
		CStopwatch cStopwatch;

		for (int j = 0; j < K; j++)
		{
			cache_invalidate();
			cStopwatch.start();

			for (int i = 0; i < N; i++)
				b[i] = a[i]+1;

			cStopwatch.stop();
		}

		time2 = cStopwatch();
	}

	for (int i = 0; i < N; i++)
		if (b[i] != i+1)
			std::cerr << "ERROR" << std::endl;

	std::cout << N << "\t" << time1/(double)K << "\t" << time2/(double)K << std::endl;

}

