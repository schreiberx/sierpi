#! /usr/bin/python

import os
import commands

# specify iOMP location here
iomp_location=os.environ['HOME']+'/Downloads/iomp/'


###################################################################
# configuration ends here - don't modify any line below this one
###################################################################

env = Environment()

###################################################################
# fix environment vars (not imported by default)
###################################################################


PATH=''
if 'PATH' in os.environ:
	PATH=os.environ['PATH']

LD_LIBRARY_PATH=''
if 'LD_LIBRARY_PATH' in os.environ:
	LD_LIBRARY_PATH = os.environ['LD_LIBRARY_PATH']

env = Environment(ENV = {'PATH' : PATH, 'LD_LIBRARY_PATH' : LD_LIBRARY_PATH})

###################################################################
# Command line options
###################################################################

#
# build directory
#
AddOption(	'--buildname',
		dest='buildname',
		type='string',
		nargs=1,
		action='store',
		help='build name (and output directory), default: \'./build\'')

env['buildname'] = GetOption('buildname')
if (env['buildname'] == None):
	env['buildname'] = 'build'



#
# simulation (parallel/serial/serial_regular)
#
AddOption(	'--simulation',
		dest='simulation',
		type='string',
		nargs=1,
		action='store',
		help='simulation to compile (tsunami_parallel/tsunami_serial/tsunami_serial_regular), default: tsunami_parallel')

env['simulation'] = GetOption('simulation')

if env['simulation']==None:
	env['simulation'] = 'tsunami_parallel'
elif env['simulation'] not in set(['tsunami_parallel', 'tsunami_serial', 'tsunami_serial_regular']):
	print 'Invalid option "'+env['simulation']+'" given for "--simulation"'
	Exit(1)

#
# serial regular type recursive/table
#
AddOption(	'--serial_regular_type',
		dest='serial_regular_type',
		type='string',
		nargs=1,
		action='store',
		help='type of serial regular implementation (recursive/table), default: \'table\'')

env['serial_regular_type'] = GetOption('serial_regular_type')



#
# cppcompiler (gnu/intel)
#
AddOption(	'--cppcompiler',
		dest='cppcompiler',
		type='string',
		nargs=1,
		action='store',
		help='specify compiler to use (gnu/intel), default: gnu')

env['cppcompiler'] = GetOption('cppcompiler')

if (env['cppcompiler'] == None or (env['cppcompiler'] not in ['gnu', 'intel', 'open64'])):
	env['cppcompiler'] = 'gnu'



#
# compile mode (debug/release)
#
AddOption(	'--mode',
		dest='mode',
		type='string',
		nargs=1,
		action='store',
		help='specify release or debug mode (release/debug), default: release')

env['mode'] = GetOption('mode')

if (env['mode'] == None or (env['mode'] not in ['release', 'debug'])):
	env['mode'] = 'release'



#
# GUI (true/false)
#
AddOption(	'--gui',
		dest='gui',
		type='string',
		action='store',
		help='build with enabled OpenGL GUI (off/on), default: off')

env['gui'] = GetOption('gui')

if (env['gui'] == None or (env['gui'] not in ['off','on'])):
	env['gui'] = 'off'



#
# threading
#
AddOption(	'--threading',
		dest='threading',
		type='string',
		nargs=1,
		action='store',
		help='Threading to use (off/omp/tbb/iomp), default: omp')

env['threading'] = GetOption('threading')

if (env['threading'] == None or (env['threading'] not in ['off', 'omp', 'tbb', 'iomp'])):
	env['threading'] = 'off'



#
# Adaptive Stack Size (true/false)
#
AddOption(	'--adaptiveStackSize',
		dest='adaptiveStackSize',
		type='int',
		action='store',
		help='build with enabled adaptiveStackSize (default: 0 - source code default)')

env['adaptiveStackSize'] = GetOption('adaptiveStackSize')

if env['adaptiveStackSize'] == None:
	env['adaptiveStackSize'] = 0



###################################################################
# SETUP COMPILER AND LINK OPTIONS
###################################################################


env.ParseConfig("pkg-config libxml-2.0 --cflags --libs")

if env['cppcompiler'] == 'gnu':
	reqversion = [4,6,1]
	gccversion = commands.getoutput('g++ -dumpversion').split('.')

	for i in range(0, 3):
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'GCC Version 4.6.1 necessary.'
			Exit(1)

	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate gnu C++ compiler
	env.Replace(CXX = 'g++')

if env['cppcompiler'] == 'open64':
	reqversion = [4,6,1]
	open64version = commands.getoutput('openCC -dumpversion').split('.')

	print "Open64 not supported so far..."
	Exit(-1)

#	for i in range(0, 3):
#		if (int(open64version[i]) < int(reqversion[i])):
#			print 'Open64 version ??!? necessary.'
#			Exit(1)

	env.Append(LINKFLAGS=' -static-libgcc')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# be pedantic to avoid stupid programming errors
	env.Append(CXXFLAGS=' -pedantic')

	# SSE 4.2
#	env.Append(CXXFLAGS=' -msse4.2')


	# speedup compilation - remove this when compiler slows down or segfaults by running out of memory
	env.Append(CXXFLAGS=' -pipe')

	# activate open64 C++ compiler
	env.Replace(CXX = 'openCC')

if env['cppcompiler'] == 'intel':
	reqversion = [12,1]
	gccversion = commands.getoutput('icpc -dumpversion').split('.')

	for i in range(0, 2):
		if (int(gccversion[i]) < int(reqversion[i])):
			print 'ICPC Version 12.1 necessary.'
			Exit(1)

	env.Append(LINKFLAGS=' -static-intel')

	# eclipse specific flag
	env.Append(CXXFLAGS=' -fmessage-length=0')

	# c++0x flag
	env.Append(CXXFLAGS=' -std=c++0x')

	# compiler option which has to be appended for icpc 12.1 without update1
	env.Append(CXXFLAGS=' -U__GXX_EXPERIMENTAL_CXX0X__')

	# SSE 4.2
	env.Append(CXXFLAGS=' -msse4.2')

	# activate intel C++ compiler
	env.Replace(CXX = 'icpc')


if env['mode'] == 'debug':
	env.Append(CXXFLAGS=' -DDEBUG=1')

	if env['cppcompiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O0 -g3 -Wall')

	elif env['cppcompiler'] == 'intel':
		env.Append(CXXFLAGS=' -O0 -g3')
#		env.Append(CXXFLAGS=' -traceback')

elif env['mode'] == 'release':
	env.Append(CXXFLAGS=' -DNDEBUG=1')

	if env['cppcompiler'] == 'gnu':
		env.Append(CXXFLAGS=' -O3 -g -mtune=native')

	elif env['cppcompiler'] == 'intel':
		env.Append(CXXFLAGS=' -xHOST -O3 -g -fast -fno-alias')

else:
	print 'ERROR: mode'
	Exit(1)

	

if env['threading'] == 'omp':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1')
	if env['cppcompiler'] == 'gnu':
		env.Append(CXXFLAGS=' -fopenmp')
		env.Append(LINKFLAGS=' -fopenmp')
	elif env['cppcompiler'] == 'intel':
		env.Append(CXXFLAGS=' -openmp')
		env.Append(LINKFLAGS=' -openmp -openmp-link=static')


elif env['threading'] == 'iomp':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_OMP=1 -DCOMPILE_WITH_IOMP=1')
	if env['cppcompiler'] == 'gnu':
		env.Append(CXXFLAGS=' -fopenmp')
		env.Append(LINKFLAGS=' -fopenmp')
	elif env['cppcompiler'] == 'intel':
		env.Append(CXXFLAGS=' -openmp')
		env.Append(LINKFLAGS=' -openmp -openmp-link=static')

	# include directories for iOMP
	env.Append(CXXFLAGS=' -I'+iomp_location+'/iomp/include/')
	env.Append(LINKFLAGS=' -L'+iomp_location+'/build/iomp/ -liomp')
	env.Append(LINKFLAGS=' -L'+os.environ['HOME']+'/local/lib')
	env.Append(LIBS=['iomp', 'boost_serialization'])
	
	env.Append(LINKFLAGS=' -lboost_serialization')	


elif env['threading'] == 'tbb':
	env.Append(CXXFLAGS=' -DCOMPILE_WITH_TBB=1')
	env.Append(CXXFLAGS=' -I'+os.environ['TBBROOT']+'/include/')

	env.Append(LIBPATH=[os.environ['TBBROOT']+'/lib/'])
	env.Append(LIBPATH=os.environ['LD_LIBRARY_PATH'].split(':'))

	
#	if env['mode'] == 'debug':
#		env.Append(LINKFLAGS='-Wl,-Bstatic -ltbb_debug -Wl,-Bdynamic')
#	else:
#		env.Append(LINKFLAGS='-Wl,-Bstatic -ltbb -Wl,-Bdynamic')

	if env['mode'] == 'debug':
		env.Append(LIBS=['tbb_debug'])
	else:
		env.Append(LIBS=['tbb'])


if env['gui'] == 'on':
	# compile flags
	env.Append(CXXFLAGS=' -I'+os.environ['HOME']+'/local/include')

	# linker flags
#	env.Append(LIBPATH=['/usr/lib/nvidia-current/'])
	env.Append(LIBPATH=[os.environ['HOME']+'/local/lib'])
	env.Append(LIBS=['GL'])

	reqversion = [2,0,0]
	sdlversion = commands.getoutput('sdl2-config --version').split('.')

	for i in range(0, 3):
		if (int(sdlversion[i]) > int(reqversion[i])):
			break;
		if (int(sdlversion[i]) < int(reqversion[i])):
			print 'libSDL Version 2.0.0 necessary.'
			Exit(1)

	env.ParseConfig("sdl2-config --cflags --libs")
	env.ParseConfig("pkg-config freetype2 SDL_image --cflags --libs")

#elif env['gui'] == 'off':


if env['simulation'] != None:
	env.Append(CXXFLAGS=' -DSIMULATION_DEFINED=1')

	if env['simulation']=='tsunami_parallel':
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_PARALLEL=1')

	if env['simulation']=='tsunami_serial':
		if env['threading']!='off':
			print 'Compiling serial version with threading activated is non-sense!'
			Exit(-1)
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL=1')

	if env['simulation']=='tsunami_serial_regular':
		if env['threading']!='off':
			print 'Compiling serial version with threading activated is non-sense!'
			Exit(-1)
		env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR=1')

		if env['serial_regular_type'] == 'table':
			env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE=0')
		elif env['serial_regular_type'] == 'recursive':
			env.Append(CXXFLAGS=' -DSIMULATION_TSUNAMI_SERIAL_REGULAR_TYPE=1')
		else:
			print 'Invalid serial regular type!'
			Exit(-1)
			


if int(env['adaptiveStackSize']) == 0:
	pass	# use default values in source code
elif int(env['adaptiveStackSize']) == -1:
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACKS=0')
else:
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACKS=1')
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACK_GROW_EXTRA_PADDING='+str(env['adaptiveStackSize']))
	env.Append(CXXFLAGS=' -DADAPTIVE_SUBPARTITION_STACK_SHRINK_EXTRA_PADDING='+str(env['adaptiveStackSize']))

###################################################################
# DEPENDENCIES
###################################################################

# search this paths for dependencies
env.Append(CPPPATH = ['/include', '/usr/local/include', '/usr/include'])
# also include the 'src' directory to search for dependencies
env.Append(CPPPATH = ['.', 'src/'])

######################
# INCLUDE PATH
######################

# ugly hack
for i in ['src/', 'src/include/']:
	env.Append(CXXFLAGS = ' -I'+os.environ['PWD']+'/'+i)


######################
# setup PROGRAM NAME base on parameters
######################
program_name = 'sierpi'

# cppcompiler
program_name += '_'+env['cppcompiler']

# threading
if env['threading'] != 'off':
	program_name += '_'+env['threading']

# gui
if env['gui'] == 'on':
	program_name += '_gui'
#else:
#	program_name += '_nogui'

# simulation
if env['simulation'] != None:
	program_name += '_'+env['simulation']

# serial_regular
if env['serial_regular_type'] != None:
	program_name += '_'+env['serial_regular_type']


# mode
program_name += '_'+env['mode']

######################
# get source code files
######################

env.src_files = []

Export('env')
SConscript('src/SConscript', variant_dir='build/build_'+program_name, duplicate=0)
Import('env')

print
print 'Building program "'+program_name+'"'
print

env.Program('build/'+program_name, env.src_files)

#Exit(0)
