/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 15, 2010
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


/*
 * activate or deactivate the gui. for this source file, it has to be
 * enabled since this is the main entrance for the simulation with a
 * gui activated.
 */
#define COMPILE_SIMULATION_WITH_GUI		1



#include <stdlib.h>
#include <sstream>

#include "config.h"

#if COMPILE_WITH_OMP
#include <omp.h>
#endif

#if COMPILE_WITH_TBB
#include <tbb/task_scheduler_init.h>
#endif

#include "libmath/CMath.hpp"
#include "libgl/draw/CGlDrawSphere.hpp"
#include "libgl/core/CGlState.hpp"
#include "lib/CEyeBall.hpp"

#include "mainvis/CCommonShaderPrograms.hpp"

#include "mainvis/CRenderWindow.hpp"
#include "mainvis/CGuiConfig.hpp"

#include "libgl/engine/iTime.hpp"
#include "libgl/engine/camera/iCamera.hpp"
#include "libgl/engine/camera/CCamera1stPerson.hpp"
#include "libgl/engine/iInputState.hpp"

#include "libgl/hud/CGlFreeType.hpp"
#include "libgl/hud/CGlWindow.hpp"
#include "libgl/hud/CGlRenderOStream.hpp"
#include "libgl/hud/CGlHudConfig.hpp"

#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "lib/CProcessMemoryInformation.hpp"

#include "lib/iBase.hpp"

#include "lib/CLinesFromSVG.hpp"

#if SIMULATION_TSUNAMI_SERIAL
	#include "simulations/tsunami_serial/CSimulationTsunami_Serial.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami
#endif

#if SIMULATION_TSUNAMI_SERIAL_REGULAR
	#include "simulations/tsunami_serial_regular/CSimulationTsunami_Serial_Regular.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami
#endif

#if SIMULATION_TSUNAMI_PARALLEL
	#include "simulations/tsunami_parallel/CSimulationTsunami_Parallel.hpp"
	#define SIMULATION_CLASS	CSimulationTsunami_Parallel
#endif


CProcessMemoryInformation cProcessMemoryInformation;


int timesteps = 13000;
float timestepSize = -1;

int initialRecursionDepth = 6;
int minRelativeRecursionDepth = 0;
int maxRelativeRecursionDepth = 6;

int number_of_threads_to_use = -1;

bool divide_number_of_timesteps_by_timestep_size = false;

//unsigned long long partition_split_workload_size = 16;
unsigned long long partition_split_workload_size = 0;
unsigned long long partition_join_workload_size = 0;

//int worldSceneId = 3;
int world_scene_id = -1;

int verbose = 0;

int output_simulation_data_each_nth_timestep = -1;
const char *output_simulation_data_filename = "frame_%08i.vtk";
bool output_partitions_to_vtk_file = false;




void update_CMainGui_callback(void *cMainGui);

class CMainGui : public CRenderWindowEventCallbacks
{
public:
	CError error;
	bool verbose;
	bool quit;

	CRenderWindow cRenderWindow;

	CGuiConfig cConfig;
	CGlFreeType cGlFreeType;
	CGlRenderOStream cGlRenderOStream;

	CProcessMemoryInformation cProcessMemoryInformation;

	// parameter for model matrix
	CEyeBall<float> cModelEyeBall;

	// camera control
	CCamera1stPerson camera;
	iTime time;
	CVector<3,float> player_velocity;
	iInputState inputState;

	// commonly used shader programs
	CCommonShaderPrograms cCommonShaderPrograms;


	// some opengl parameters for perspective matrix
	float perspective_zoom;
	float zoom;
	float near_plane;
	float far_plane;

	// the BIG simulation class
	SIMULATION_CLASS *cSimulation;

#if COMPILE_WITH_TBB
    tbb::task_scheduler_init *tbb_task_scheduler_init;
#endif

    CLinesFromSVG cLinesFromSVG;

	int screenshotEnumeration;


	void updateParametersInConfigGui()
    {
		cConfig.simulation_parameter_timestep_size = cSimulation->timestep_size;
		cConfig.simulation_split_workload_size = cSimulation->partition_split_workload_size;
		cConfig.simulation_join_workload_size = cSimulation->partition_join_workload_size;
    }

	/**
	 * CONSTRUCTOR
	 */
	CMainGui(
			char *p_initial_structure = NULL,
			bool p_verbose = false
	)	:
		verbose(p_verbose),
		quit(false),
		cRenderWindow(*this, "Sierpi", 800, 600, false, 3, 3),
		cGlRenderOStream(cGlFreeType),
		cCommonShaderPrograms(p_verbose),
		screenshotEnumeration(0)
	{

		/**********************************************
		 * SETUP OpenGL stuff
		 */
		near_plane = 1.0;
		far_plane = 20.0;


		/**********************************************
		 * CONFIGURATION HUD
		 */
		cGlFreeType.setup(12);
		CError_AppendReturn(cGlFreeType);

		cConfig.setup(cGlFreeType, cGlRenderOStream);
		cConfig.setHudVisibility(true);


		/**********************************************
		 * LOAD SIERPI SVG
		 */
		cLinesFromSVG.loadSVGFile("data/setupGraphics/sierpi.svg");


		/**********************************************
		 * MULTITHREADING STUFF RIGHT HERE
		 *
		 * this is necessary to do before initialization of the simulation
		 */

#if COMPILE_WITH_TBB
		// setup scheduler
		tbb_task_scheduler_init = new tbb::task_scheduler_init(1);

		// load default number of threads
		cConfig.simulation_number_of_threads = tbb::task_scheduler_init::default_num_threads();

		std::cerr << "WARNING: Using more than a single thread is not supported using TBB!!!" << std::endl;
#endif

#if COMPILE_WITH_OMP
		#pragma omp parallel
		#pragma omp single nowait
			// load default number of threads
			cConfig.simulation_number_of_threads = omp_get_num_threads();
#endif


		/**********************************************
		 * SIMULATION
		 */
		cSimulation = new SIMULATION_CLASS;
#if COMPILE_SIMULATION_WITH_GUI
		cSimulation->setCallbackParameterUpdate(&update_CMainGui_callback, this);
#endif
		cSimulation->reset_Simulation();


		/**************************
		 * SETUP
		 */
#if SIMULATION_TSUNAMI_PARALLEL
		if (world_scene_id > -10000)
			cSimulation->setWorldSceneId(world_scene_id);
#endif


		cSimulation->setInitialRecursionDepth(initialRecursionDepth);
		cSimulation->setMinRelativeRecursionDepth(minRelativeRecursionDepth);
		cSimulation->setMaxRelativeRecursionDepth(maxRelativeRecursionDepth);

		if (partition_split_workload_size != 0)
			cSimulation->setSplitJoinSizes(partition_split_workload_size, partition_join_workload_size);

		cSimulation->reset_Simulation();

		cSimulation->computeTimestepSize(timestepSize);

		updateParametersInConfigGui();


		/**********************************************
		 * COMMON SHADERS
		 */
		if (cCommonShaderPrograms.error())
		{
			error = cCommonShaderPrograms.error;
			return;
		}

		/**********************************************
		 * SIMULATION
		 */
		if (cSimulation->error())
		{
			error = cSimulation->error;
			return;
		}


		/**********************************************
		 * INSTALL CCONFIG CALLBACK HANDLERS
		 */

#if SIMULATION_TSUNAMI_PARALLEL
		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplitJoinSizes();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cConfig.simulation_split_workload_size);
			CGUICONFIG_CALLBACK_INSTALL(cConfig.simulation_join_workload_size);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_UtilizedThreadsForSimulation();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cConfig.simulation_number_of_threads);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatTextureSize();
			CGUICONFIG_CALLBACK_END();
			cConfig.setCallbackHandler(&cConfig.splats_texture_height, CGUICONFIG_CALLBACK_HANDLER, this);
			cConfig.setCallbackHandler(&cConfig.splats_texture_width, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatTextureSize();
			CGUICONFIG_CALLBACK_END();
			cConfig.setCallbackHandler(&cConfig.splats_texture_height, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SplatSizeScalar();
			CGUICONFIG_CALLBACK_END();
			cConfig.setCallbackHandler(&cConfig.splat_size_scalar, CGUICONFIG_CALLBACK_HANDLER, this);
		}

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.update_SurfaceHeightTranslateAndScale();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cConfig.surface_texel_height_scale);
			CGUICONFIG_CALLBACK_INSTALL(cConfig.surface_texel_height_translate);
		}
#endif

		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.cSimulation->computeTimestepSize(c.cConfig.simulation_parameter_timestep_size);
			CGUICONFIG_CALLBACK_END();
			cConfig.setCallbackHandler(&cConfig.simulation_parameter_timestep_size, CGUICONFIG_CALLBACK_HANDLER, this);
		}


		{
			CGUICONFIG_CALLBACK_START(CMainGui);
				c.callback_take_screenshot();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(cConfig.take_screenshot);
		}


		/**********************************************
		 * reset view
		 */
		resetView();
	}


	~CMainGui()
	{
#if COMPILE_WITH_TBB
		delete tbb_task_scheduler_init;
#endif
	}


	void takeNextScreenshotImage()
	{
		char buffer[1024];
		sprintf(buffer, "screenshot_%08i.bmp", screenshotEnumeration);

		cRenderWindow.saveScreenshotWithThread(buffer);

		screenshotEnumeration++;
	}

	void callback_take_screenshot()
	{
		takeNextScreenshotImage();
		cConfig.take_screenshot = false;
	}


	void resetView()
	{
		perspective_zoom = 1.0f;

		cModelEyeBall.reset();

		camera.reset();
		camera.moveRelative(CVector<3,float>(0, 1, 2));
		camera.rotate(M_PI*0.1, 0, 0);
		camera.computeMatrices();

		cConfig.visualization_enabled = true;

#if SIMULATION_TSUNAMI_PARALLEL
		update_SplitJoinSizes();
		update_SplatTextureSize();
		update_SplatSizeScalar();
		update_SurfaceHeightTranslateAndScale();
#endif
	}

	void reset()
	{
		std::cout << "[ RESET ]" << std::endl;
		cProcessMemoryInformation.outputUsageInformation();

		cSimulation->computeTimestepSize(-1);
		cSimulation->reset_Simulation();

		updateParametersInConfigGui();

		cProcessMemoryInformation.outputUsageInformation();
	}


#if SIMULATION_TSUNAMI_PARALLEL
	void update_SplitJoinSizes()
	{
		cSimulation->setSplitJoinSizes(cConfig.simulation_split_workload_size, cConfig.simulation_join_workload_size);
	}

	void update_TimestepSize()
	{
		cSimulation->computeTimestepSize(cConfig.simulation_parameter_timestep_size);
	}

	void update_SplatTextureSize()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.resizeTexture(cConfig.splats_texture_width, cConfig.splats_texture_height);
	}

	void update_SplatSizeScalar()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.setSplatSizeScalar(cConfig.splat_size_scalar);
	}

	void update_SurfaceHeightTranslateAndScale()
	{
		cSimulation->cOpenGL_Element_Splats_Root_Tsunami.setupTranslationAndScalingParameters(cConfig.surface_texel_height_translate, cConfig.surface_texel_height_scale);
	}
#endif

	/**
	 * set the number of threads which are utilized for the parallelization.
	 *
	 * the number of threads are changed when entering the next "#pragma omp parallel" section.
	 */
	void update_UtilizedThreadsForSimulation()
	{
#if COMPILE_WITH_OMP
		omp_set_num_threads(cConfig.simulation_number_of_threads);
#endif

#if COMPILE_WITH_TBB
		tbb_task_scheduler_init->terminate();
		tbb_task_scheduler_init->initialize(cConfig.simulation_number_of_threads);
#endif
	}

	/**
	 * callback method called whenever a key is pressed
	 */
	void callback_key_down(
			int key,
			int mod,
			int scancode,
			int unicode
	)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

		CVector<2,float> position;

		switch(key)
		{
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			cConfig.simulation_number_of_threads = key-'0';
			update_UtilizedThreadsForSimulation();
			break;

		case 'q':
			quit = true;
			break;

		case 'e':
			cSimulation->reset_Simulation();
			updateParametersInConfigGui();
			resetView();
			break;

		case 'r':
			reset();
			break;

		case 'v':
			cConfig.visualization_enabled ^= true;
			break;

		case 'o':
			cConfig.visualization_method++;
			break;

		case 'O':
			cConfig.visualization_method--;
			break;


		case 'b':
			cConfig.visualization_render_bathymetry ^= true;
			break;

		case 'p':
			cConfig.visualization_render_subpartition_borders ^= true;
			break;

		case 'a':	player_velocity[0] = CMath::max(player_velocity[0]-1, -1.f);	break;
		case 'd':	player_velocity[0] = CMath::min(player_velocity[0]+1, +1.f);	break;
		case 'w':	player_velocity[2] = CMath::max(player_velocity[2]-1, -1.f);	break;
		case 's':	player_velocity[2] = CMath::min(player_velocity[2]+1, +1.f);	break;

#if SIMULATION_TSUNAMI_PARALLEL
		case 'S':
			{
				float scale = 0.5;

				for (auto i = cLinesFromSVG.lineList.begin(); i != cLinesFromSVG.lineList.end(); i++)
				{
					auto &list = *i;
					for (auto d = list.begin(); d != list.end(); d++)
					{
//						std::cout << (*d)[0] << " " << (*d)[1] << std::endl;
						cSimulation->setup_ElementDataAt2DPosition((*d)[0]*scale, (*d)[1]*scale);
					}
				}
			}
			break;
#endif

		case 'i':	cConfig.simulation_random_raindrops ^= true;
			break;

		case 'V':
			cProcessMemoryInformation.outputUsageInformation();
			break;

		case 'l':
			cConfig.simulation_run ^= true;
			break;



#if SIMULATION_TSUNAMI_PARALLEL

		case 'M':
			cSimulation->setSplitJoinSizes(2, 1);
			cSimulation->setInitialRecursionDepth(2);
			cSimulation->setMaxRelativeRecursionDepth(1);
			cSimulation->reset_Simulation();

			updateParametersInConfigGui();
			break;

		case 'I':
			cSimulation->setup_ColumnAt2DPosition(
					getRand01()*2.0-1.0,
					getRand01()*2.0-1.0,
					0.1
					);
			break;

		case 'm':
			position = computeMouseOnPlaneCoords();
			cSimulation->debugOutputElementData(position[0], position[1]);
			break;

		case 'n':
			position = computeMouseOnPlaneCoords();
			cSimulation->debugOutputPartition(position[0], position[1]);
			break;


/*
 * FULLSCREEN
 */
		case 'F':
			cRenderWindow.setWindowFullscreenState(!cRenderWindow.fullscreen_active);
			break;
#if 0
/*
 * SPLIT
 */
		case 'b':
			cSimulation->splitPartition(computeMouseOnPlaneCoords());
			cSimulation->splitOrJoinPartitions();
			break;

		case 'B':
			cSimulation->markPartitionForSplitting(computeMouseOnPlaneCoords());
			break;

/*
 * JOIN
 */
		case 'h':
			cSimulation->markPartitionForJoining(computeMouseOnPlaneCoords());
			cSimulation->splitOrJoinPartitions();
			break;

		case 'H':
			cSimulation->markPartitionForJoining(computeMouseOnPlaneCoords());
			break;

/*
 * SPLIT/JOIN depending on flags
 */

		case 'N':
			cSimulation->splitOrJoinPartitions();
			break;
#endif	// #if 0

#endif	// #if tsunami_parallel

/*
 * show/hide GUI
 */
		case ' ':
			cConfig.setHudVisibility(!cConfig.hud_visible);
			break;


/*
 * record window
 */
		case 'R':
			cConfig.take_screenshot_series = !cConfig.take_screenshot_series;
			break;

		/*
		 * forward key to simulation
		 */
		default:
			cSimulation->keydown(key);
			break;

		}
	}


	void callback_key_up(int key, int mod, int scancode, int unicode)
	{
		// modify key when shift is pressed
		if (mod & CRenderWindow::KEY_MOD_SHIFT)
		{
			if (key >= 'a' && key <= 'z')
				key -= ('a'-'A');
		}

		switch(key)
		{
			case 'a':	player_velocity[0] += 1;	break;
			case 'd':	player_velocity[0] -= 1;	break;
			case 'w':	player_velocity[2] += 1;	break;
			case 's':	player_velocity[2] -= 1;	break;
		}
	}


	void callback_quit()
	{
		quit = true;
	}


	void callback_mouse_motion(int x, int y)
	{
		inputState.update((float)x*2.0f/(float)cRenderWindow.window_width-1.0f, (float)y*2.0f/(float)cRenderWindow.window_height-1.0f);

/*
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_MIDDLE])			// middle mouse button
		{
			coarsenAtCurrentMousePosition();
		}
*/
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_RIGHT])		// right mouse button
		{
			setupElementDataAtCurrentMousePosition();
		}
		else if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])			// left mouse button
		{
		}

		cConfig.mouse_motion(x, cRenderWindow.window_height - y);
	}

	void callback_mouse_button_down(int button)
	{
		// if the button was pressed down within the config window, do nothing
		if (cConfig.mouse_button_down(button))
			return;

		switch(button)
		{
			case CRenderWindow::MOUSE_BUTTON_RIGHT:
				setupElementDataAtCurrentMousePosition();
//				refineAtCurrentMousePosition();
				break;
		}

		if (button <= 3)
			inputState.mouse_buttons[button] = true;
	}

	void callback_mouse_button_up(int button)
	{
		// if the button was pressed down within the config window, do nothing
		cConfig.mouse_button_up(button);

		if (button <= 3)
			inputState.mouse_buttons[button] = false;
	}


	void callback_mouse_wheel(int x, int y)
	{
		if (cConfig.mouse_wheel(x, y))
			return;

		perspective_zoom += (float)y*(-0.1);
	}

	void callback_viewport_changed(int width, int height)
	{
	}


	GLSL::vec3 light_view_pos;

	void setupBlinnShader_Surface(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}


	void setupBlinnShader_Bathymetry(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cBlinn.use();
		cCommonShaderPrograms.cBlinn.texture0_enabled.set1b(false);
		cCommonShaderPrograms.cBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_specular_color3_uniform.set(GLSL::vec3(0.7,0.25,0));
		cCommonShaderPrograms.cBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(0.1,0.1,0.2));
		cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0.1,0.1,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cBlinn.disable();
	}


	void setupHeightColorShader(
			GLSL::mat4 &local_view_model_matrix,
			GLSL::mat4 &local_pvm_matrix
		)
	{
		cCommonShaderPrograms.cHeightColorBlinn.use();
		cCommonShaderPrograms.cHeightColorBlinn.light0_enabled_uniform.set1b(true);
		cCommonShaderPrograms.cHeightColorBlinn.light0_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_diffuse_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_specular_color3_uniform.set(GLSL::vec3(1,1,1));
		cCommonShaderPrograms.cHeightColorBlinn.light0_view_pos3_uniform.set(light_view_pos);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.view_model_matrix_uniform.set(local_view_model_matrix);
		cCommonShaderPrograms.cHeightColorBlinn.view_model_normal_matrix3_uniform.set(local_view_model_matrix.getInverseTranspose3x3());
		cCommonShaderPrograms.cHeightColorBlinn.pvm_matrix_uniform.set(local_pvm_matrix);
		CGlErrorCheck();

		cCommonShaderPrograms.cHeightColorBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_color3_uniform.set(GLSL::vec3(1.0,1.0,1.0));
		cCommonShaderPrograms.cHeightColorBlinn.material_specular_exponent_uniform.set1f(40.0f);
		cCommonShaderPrograms.cHeightColorBlinn.disable();

		cCommonShaderPrograms.cHeightColorBlinn.setupColorScaleAndOffset(0.025, 10);
	}



	void renderWorld()
	{
		CGlErrorCheck();

		GLSL::mat4 local_model_matrix = cModelEyeBall.rotationMatrix;
		local_model_matrix.loadIdentity();

		GLSL::mat4 local_view_model_matrix = camera.view_matrix * local_model_matrix;
		GLSL::mat4 local_pvm_matrix = camera.projection_matrix*local_view_model_matrix;

		/*
		 * render simplivied bathymetry field
		 */
		if (cConfig.visualization_render_bathymetry)
		{
			setupBlinnShader_Bathymetry(local_view_model_matrix, local_pvm_matrix);
			cSimulation->render_terrainBathymetry_simple(cCommonShaderPrograms.cBlinn);
		}

		/*
		 * render water surface
		 */
#if SIMULATION_TSUNAMI_PARALLEL
		int max_visualizations = 5;
#else
		int max_visualizations = 3;
#endif
		cConfig.visualization_method = (cConfig.visualization_method+max_visualizations) % max_visualizations;
		switch(cConfig.visualization_method % max_visualizations)
		{
			case 1:
				setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceWithHeightColors_simple(cCommonShaderPrograms.cHeightColorBlinn);
				break;

			case 2:
				setupHeightColorShader(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceWithHeightColors_simple(cCommonShaderPrograms.cHeightColorBlinn);
				break;

			case 3:
				setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceWireframe(cCommonShaderPrograms.cBlinn);
				break;

#if SIMULATION_TSUNAMI_PARALLEL
			case 4:
				setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceSmooth(local_pvm_matrix, cCommonShaderPrograms.cBlinn);
				break;
#endif
			default:
				setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
				cSimulation->render_surfaceDefault_simple(cCommonShaderPrograms.cBlinn);
				break;

		}


		/*
		 * render subpartition borders
		 */
#if SIMULATION_TSUNAMI_PARALLEL
		if (cConfig.visualization_render_subpartition_borders)
		{
			setupBlinnShader_Surface(local_view_model_matrix, local_pvm_matrix);
			cSimulation->render_subPartitionBorders(cCommonShaderPrograms.cBlinn);
		}
#endif
	}

	CVector<2,float> computeMouseOnPlaneCoords()
	{
		GLSL::mat4 inv_projection_matrix = camera.projection_matrix.getInverse();

		// dirty hack: get view center from view matrix
		GLSL::vec3 ray_start = camera.getPosition();

		// end position is somewhere at the far plane
		GLSL::vec4 unproj_space_vector4 = inv_projection_matrix*vec4f(inputState.mouse_x, -inputState.mouse_y, -1, 1);
		GLSL::vec4 unproj_space_vector3 = unproj_space_vector4 / unproj_space_vector4[3];

		GLSL::vec3 ray = camera.view_matrix.getTranspose3x3() * unproj_space_vector3;

		// compute point on plane
		GLSL::vec3 pos = ray_start-ray*(ray_start[1]/ray[1]);
		return GLSL::vec2(pos[0], -pos[2]);
	}


	void refineAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->setup_ColumnAt2DPosition(
				point_on_plane[0], point_on_plane[1],
				0.1
			);
	}


	void setupElementDataAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->setup_ElementDataAt2DPosition(
				point_on_plane[0], point_on_plane[1]
			);
	}

#if 0
	void coarsenAtCurrentMousePosition()
	{
		// get point on plane
		GLSL::vec2 point_on_plane = computeMouseOnPlaneCoords();

		cSimulation->coarsenAt2DPosition(point_on_plane[0], point_on_plane[1]);
	}
#endif

	void simulationTimesteps()
	{
		if (cConfig.simulation_run)
		{
			/*
			 * the number of available openmp threads are set only during initialization when not running
			 * in interactive mode (GUI)
			 */

		#if COMPILE_WITH_OMP
			#pragma omp parallel
			#pragma omp single nowait
		#endif

			for (int i = 0; i < cConfig.visualization_simulation_steps_per_frame; i++)
			{
				cSimulation->runSingleTimestep();
			}

			cConfig.simulation_parameter_timestep_size = cSimulation->timestep_size;
		}
	}

	double getRand01()
	{
		return ((double)::random())*(1.0/(double)RAND_MAX);
	}

	void raindropsRefine(
			bool undelayed = false
	)
	{
		static double lastTicks = -1.0;
		static double nextRaindropAbsoluteTicks = 1.0;
		static double newRaindropRelativeTicks = 1.0;

		double recentTicks = cRenderWindow.getTicks();

		if (recentTicks > nextRaindropAbsoluteTicks || undelayed)
		{
			lastTicks = recentTicks;
			nextRaindropAbsoluteTicks = lastTicks+getRand01()*newRaindropRelativeTicks;

#if 1
 			float radius = CMath::max(CMath::min(0.1, 5000.f/(float)cSimulation->number_of_triangles*getRand01()), 0.01);

			cSimulation->setup_ColumnAt2DPosition(
					getRand01()*2.0-1.0,
					getRand01()*2.0-1.0,
					radius
					);
#else
			cSimulation->setup_ElementDataAt2DPosition(
					getRand01()*2.0-1.0,
					getRand01()*2.0-1.0
					);
#endif
		}

#if SIMULATION_TSUNAMI_PARALLEL
//		cSimulation->splitAndJoinRandomized();
#endif
	}

	void render()
	{
		camera.moveRelative(player_velocity*(float)time.frame_elapsed_seconds*5.0f);
		if (inputState.mouse_buttons[CRenderWindow::MOUSE_BUTTON_LEFT])
			camera.rotate(	-inputState.relative_mouse_y*time.frame_elapsed_seconds*300.0,
							-inputState.relative_mouse_x*time.frame_elapsed_seconds*300.0,
							0
							);

		zoom = CMath::exp(perspective_zoom)*0.2;
		camera.frustum(-zoom*cRenderWindow.aspect_ratio, zoom*cRenderWindow.aspect_ratio, -zoom, zoom, near_plane, far_plane);
		camera.computeMatrices();

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		light_view_pos = camera.view_matrix*GLSL::vec3(2, 4, -6);

		renderWorld();

		cConfig.render();

		inputState.clearRelativeMovement();
	}

	void run()
	{
		cSimulation->outputVerboseInformation();

		while (!quit)
		{
			time.update();

			std::ostringstream buf;

			if (time.fpsUpdatedInLastFrame)
			{
#if !SIMULATION_TSUNAMI_PARALLEL
				buf << "Sierpinski Zeug running @ " << time.fps << " FPS      Triangles: " << cSimulation->cStacks->structure_stacks.structure_getNumberOfTrianglesInQuad();

#ifndef NDEBUG
//				buf << "     Max. Triangles: " << cSimulation->cStacks->structure_stacks.getSize();
#endif

#else
				buf << "Sierpinski Zeug running @ " << time.fps << " FPS";
				buf << " | ";
				buf << cSimulation->number_of_triangles << " Triangles";
				buf << " | ";
				buf << cSimulation->number_of_simulation_handler_partitions << " Partitions";
				buf << " | ";
				buf << ((double)cSimulation->number_of_triangles*cConfig.visualization_simulation_steps_per_frame*time.fps)*0.000001 << " Mega Triangles per second (inaccurate)";
#endif
			}

			if (cConfig.simulation_random_raindrops)
			{
				raindropsRefine();
			}

			if (cConfig.visualization_enabled)
			{
				glClearColor(0,0,0,0);
				glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

				/*
				 * SIMULATION TIMESTEP
				 */
				simulationTimesteps();

				/*
				 * UPDATE FRAME
				 */
				render();

				if (time.fpsUpdatedInLastFrame)
				{
					cRenderWindow.setWindowTitle(buf.str().c_str());
				}

				cRenderWindow.eventLoop();

				if (cConfig.take_screenshot_series)
				{
					takeNextScreenshotImage();
				}

				cRenderWindow.swapBuffer();
			}
			else
			{
				cRenderWindow.eventLoop();

				simulationTimesteps();

				if (time.fpsUpdatedInLastFrame)
				{
					std::cout << buf.str() << std::endl;
				}
			}
		}
	}
};



void run(char *initial_structure)
{
	CMainGui cGuiInterfaceImplementation(initial_structure);

	if (cGuiInterfaceImplementation.error())
	{
		std::cout << cGuiInterfaceImplementation.error << std::endl;
		return;
	}


	try
	{
		cGuiInterfaceImplementation.run();
	}
	catch(std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
}


int main(int argc, char *argv[])
{
	char optchar;
	while ((optchar = getopt(argc, argv, "d:t:f:w:a:i:s:v:n:o:l:rpg:")) > 0)
	{
		switch(optchar)
		{
		case 'r':
			divide_number_of_timesteps_by_timestep_size = true;
			break;

		case 'p':
			output_partitions_to_vtk_file = true;
			break;

		case 'd':
			initialRecursionDepth = atoi(optarg);
			break;

		case 'v':
			verbose = atoi(optarg);
			break;

		case 't':
			timesteps = atoi(optarg);
			break;

		case 's':
			timestepSize = atof(optarg);
			break;



		case 'f':
			output_simulation_data_each_nth_timestep = atoi(optarg);
			break;

		case 'g':
			output_simulation_data_filename = optarg;
			break;

		case 'n':
			number_of_threads_to_use = atoi(optarg);
			break;

		case 'w':
			world_scene_id = atoi(optarg);
			break;

		case 'i':
			minRelativeRecursionDepth = atoi(optarg);
			break;

		case 'a':
			maxRelativeRecursionDepth = atoi(optarg);
			break;

		case 'o':
			partition_split_workload_size = atoi(optarg);
			break;

		case 'l':
			partition_join_workload_size = atoi(optarg);
			break;

		default:
			goto parameter_error;
		}

	}

	goto parameter_error_ok;

	parameter_error:
	std::cout << "usage: " << argv[0] << std::endl;
	std::cout << "	[-d [int]: initial recursion depth]" << std::endl;
	std::cout << "	[-v [int]: verbose mode (0-5)]" << std::endl;
	std::cout << "	[-n [int]: number of threads to use]" << std::endl;
	std::cout << "	[-t [int]: timesteps]" << std::endl;
	std::cout << "	[-r : divide number of timesteps and frame output rate by timestep size]" << std::endl;
	std::cout << "	[-s [float]: timestep size]" << std::endl;
	std::cout << "	[-p : also write vtk files with partitions]" << std::endl;
	std::cout << "	[-f [int]: output .vtk files each #nth timestep]" << std::endl;
	std::cout << "	[-g [string]: outputfilename, default: frame_%08i.vtk" << std::endl;
	std::cout << "	[-w [int]: world scene]" << std::endl;
	std::cout << "	[-i [int]: min relative recursion depth]" << std::endl;
	std::cout << "	[-a [int]: max relative recursion depth]" << std::endl;
	std::cout << "	[-o [int]: partition size when to request split]" << std::endl;
	std::cout << "	[-l [int]: partition size when to request join (both childs have to request a join)]" << std::endl;

	return -1;

parameter_error_ok:
	if (verbose > 5)
	{
		cProcessMemoryInformation.outputUsageInformation();
	}
	srand(0);

	#if COMPILE_WITH_OMP
		std::cout << "omp_get_max_threads(): " << omp_get_max_threads() << std::endl;
	#endif

	#if COMPILE_WITH_TBB
		std::cout << "tbb::task_scheduler_init::default_num_threads(): " << tbb::task_scheduler_init::default_num_threads() << std::endl;
	#endif

	srand(0);

	char *initial_structure = NULL;
	if (argc > 1)
		initial_structure = argv[1];

	run(initial_structure);

#ifdef DEBUG
	if (!debug_ibase_list.empty())
	{
		std::cout << "MEMORY LEAK: iBase class missing in action" << std::endl;
		std::cout << "  + number of classes: " << debug_ibase_list.size() << std::endl;
	}
#endif

	return 0;
}


void update_CMainGui_callback(void *cMainGui)
{
	static_cast<class CMainGui*>(cMainGui)->updateParametersInConfigGui();
}

