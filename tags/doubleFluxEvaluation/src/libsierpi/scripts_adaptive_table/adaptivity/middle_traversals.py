#! /usr/bin/python

#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

#
# this lib cares about the marking process of the adaptivity
#
# since the marking process does not modify the triangle structure, the
# structure stack is created only once for the 1st forward iteration.
# maybe this is not necessary due to the previous forward/backward computation
# traversal.
#

import sys
import lib_tools



def code_generation(d):
	if d.direction == 'forward' or d.direction == 'backward':
#		sys.stderr.write(d.key+"\n")

		##################################
		# CREATE RECURSIVE CALLS
		##################################
#		lib_tools.create_function_header(d, d.config, '_adaptive_marking')
		lib_tools.create_function_header(d, d.config, '')

		# setup incoming edge states
		print	"	unsigned char incomingEdgeStateTransfer = 0;";
		lib_tools.createIncomingEdgeStateTransfer(d)

		# get old state
		print	"	unsigned char old_state = adaptivity_state_flag_stack_in->pop();"
		print	"	unsigned char oldEdgeStateTransfer = old_state >> 4;"
		print	"	old_state &= 7;"
		print	""

		lib_tools.getNewStateForStateTransfer()

		#
		# COARSENING SPECIAL CODE
		#
		lib_tools.coarseningAdjacentAgreement(d, True)

		# also spread old refinement information
		print	"	outgoingEdgeStateInformation |= oldEdgeStateTransfer;"

		lib_tools.createOutgoingEdgeStateTransfer(d)

		print "}"