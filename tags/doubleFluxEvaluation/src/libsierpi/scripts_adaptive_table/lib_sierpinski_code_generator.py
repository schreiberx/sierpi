#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#

import sys
import math
import copy
import itertools


import adaptivity.first_traversal as adaptive_first_traversal
import adaptivity.middle_traversals as adaptive_middle_traversals
import adaptivity.last_traversal as adaptive_last_traversal

import lib_tools

######################################################################
# helper function to make a method callable within the class
######################################################################
class Callable:
	def __init__(self, anycallable):
		self.__call__ = anycallable


###################################
# xor helper function
###################################
def xor(a, b):
	r = True
	if a:
		r = not r
	if b:
		r = not r
	return r

###################################
# Convenient handler for normals
###################################
class CNormals:
	#
	# normal IDs:
	# the directions are enumerated in clockwise order to handle rotations by simply using arithmetic operations
	#
	n, ne, e, se, s, sw, w, nw = range(0,8)

	# get a string for the normal id
	def getString(num):
		return ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'][num]

	# return the normal belonging to the direction
	def getNormal(num):
		a = "{:+0.65f}".format(1.0/math.sqrt(2.0))
		ma = "{:+0.65f}".format(-1.0/math.sqrt(2.0))
		o = str(0)
		l = str(1)
		ml = str(-1)
		return	[[o, l], [a,a], [l,o], [a,ma], [o,ml], [ma,ma], [ml,o], [ma,a]][num]
	
	# return the normal belonging to the inverted direction
	def getInverseNormal(num):
		return CNormals.getNormal((num+4)%8)

	# make them accessible as static members
	getString = Callable(getString)
	getNormal = Callable(getNormal)
	getInverseNormal = Callable(getInverseNormal)


###################################
# default config values
#
# not all combinations are allowed so far
###################################
class CConfig:
	# create normal information (normal on hypotenuse pointing inward)
	normals = False
	
	# create vertex informations
	vertices = False
	
	# depth as parameter for the kernel callback functions
	depthParameter = False
	
	# create depth value for recursion table
	recMethodDepth = False
	
	# kernel calls are made for leaves only
	kernelCallsForLeavesOnly = False
	
	# create forward code
	createForwardCode = False
	
	# create backward code
	createBackwardCode = False
	
	# create split/join first/child information
	createSplitJoinFirstChildInformation = False;
	
	# no edge communication? => traversals can be simplified
	edgeComm = False
	
	# create reversed structure stack - e. g. the backward stack during traversal of the forward stack
	createReversedStructureStack = False
	
	# structure input stack: the input stack can be modified if necessary (e. g. for edge communication)
	structure_input_stack = 'structure_lifo_in'

	# element data input stack: the element data input stack can be modified if necessary (e. g. for edge communication)
	element_data_input_stack = 'element_data_lifo'

	# assume that K, V and H types are equal and can be replaced by 'K'
	assumeKVHToBeEqual = False

	# even run the should_refine functions also when no refine operation is allowed due to depth restrictions
	callShouldRefineForAllLeaves = False


	##########################################
	# ADAPTIIVE: read adaptive_info.txt for further information
	# adaptive handling is not possible in combination with kernel computation calls
	##########################################
	
	# if 'adaptive' is set to true, all other config variables from above are ignored so far!
	adaptive = False

	# activate debug output to print data on stack, recursive calls and further information during traversal
	code_debug_output = False

	# depth limiters: avoid coarsening/refinement if the depth gets out of a specific range
	depthLimiterMax = False
	depthLimiterMin = False

	# update maxLeafTraversalDepth
	updateMaxDepth = False
	
	# flags to create only specific code pieces (useful for parallelization)
	adaptive_create_first_traversal = True
	adaptive_create_middle_traversal = True
	adaptive_create_last_traversal = True
	
	# return type of kernel function is set to type 'char'
	adaptive_char_return_variable = False
	
	# only for 'last' traversal: if True, output adaptive information for edge to stack
	adaptive_last_traversal_edge_comm_adaptive_information = False
	
	# create asserts in middle adaptive traversals
	adaptive_middle_create_serial_processing_asserts = True

	##########################################
	# REGULAR STRUCTURE HANDLING
	#
	# 0: leaf element
	# 1: refine (irregular tree below)
	# -1: regular subtree with depth 1
	# -2: regular subtree with depth 2
	# ...: ...
	##########################################
	regular_structure_stack_handling = False

	##########################################
	# GENERAL
	##########################################
	# call kernel hooks for elemental data
	elementData = False
	
	##########################################
	# METHOD RECURSION TABLE
	##########################################
	# create recursion
	createRecursionTable = False
	# classname + to prefix recursion table variable
	recursionTableClass = 'TThisClass'
	# limit recursion methods to creation depth
	limitRecursionTableMethodsByDepth = -1

	##########################################
	# PRIVATE
	##########################################
	# create depth data for recursive calls depending on depthLimiters of if the kernel desires the depth as a parameter
	depth = False



###################################
# the main SIERPINSKI CODE GENERATOR class
###################################
class scg:
	def __init__(self):
		self.reset()
		
	##################################################
	# helper functions
	##################################################
	normals = CNormals()


	###################################
	# just a simple handler to store several triangle properties
	###################################
	class CTriangleProps:
		# type of triangle ('K'/'H'/'V')
		type = None
		
		# hyp, edge_right and edge_left communication type ('old'/'new'/'b')
		hyp = None
		edge_right = None
		edge_left = None
		
		# normal at hypotenuse
		hyp_normal = None

		def setup(self, type, hyp, edge_right, edge_left, hyp_normal, even_odd = 'even'):
			self.type = type
			self.hyp = hyp
			self.edge_right = edge_right
			self.edge_left = edge_left
			self.hyp_normal = hyp_normal
			self.even_odd = even_odd


	##################################################
	# TRIANGLE PROPERTIES
	##################################################
	# this class stores _all_ information about the current triangle which can be precomputed.
	# also the information about the 2 further refinement is computed here to make it accessible in a convenient way.
	##################################################
	class CTriangleFactory:
		# member variables
		
		# traversal direction: ('forward'/'backward')
		direction = None
		# type of the parent triangle ('K'/'H'/'V')
		type_parent = None
		
		# even or odd traversal ('even'/'odd')
		even_odd = None
		
		# hyp, edge_right and edge_left type ('old'/'new'/'b')
		hyp = None
		edge_right = None
		edge_left = None
		
		# normal at hypotenuse
		hyp_normal = None
		
		# automatically setup variables
		inv_direction = None
		inv_even_odd = None
		x_format = None
		hyp_stack = None
		inv_hyp_stack = None
		edge_stack = None
		
		# childs
		child_first = None
		child_second = None


		##################################################
		# HELPER MEMBERS / VARIABLES
		##################################################
		
		# edge signature helper function to return the longer string 'old'/'new'/'b' from the compressed format 'o'/'n'/'b'
		def e(self, v):
			assert(v != None)
			return ('o' if v == 'old' else ('n' if v == 'new' else 'b'))

		# even_odd signature helper function to return the longer string 'even'/'odd' from the compressed format 'e'/'o'
		def d(self, v):
			assert(v != None)
			return 'e' if v == 'even' else 'o'

		# convenient handler to CNormals
		normals = CNormals()

		##################################################
		# CONSTRUCTOR
		##################################################
		# !!! IMPORTANT !!!
		# compared to other pieces in the code, the first edge is the one closest to the entrance point.
		# the second edge the one closest to the exit point.
		#
		# edges ared named (edge_left and edge_right) by aligning their hypotenuse along the x-axis.
		# then edge_right is to the right hand and edge_left to the left hand.
		#
		# * child_first is the first traversed child
		# * child_second the second traversed child
		##################################################
		def __init__(self,
			direction,
			type_parent, even_odd,			# even or odd
			hyp, edge_right, edge_left,		# edge types (b/new/old) - edges are enumerated in anti-clockwise manner
			hyp_normal,						# normal along hypotenuse
			type_child_first, c1hyp, c1redge, c1ledge,	# first traversed child
			type_child_second, c2hyp, c2redge, c2ledge,	# second traversed child
			config							# configuration
			):

			# backup configuration
			self.config = config

			# overwrite any edge related information to create similar signatures
			# if no edge communication is desired
			if not config.edgeComm:
				hyp = 'x'
				edge_right = 'x'
				edge_left = 'x'
				c1hyp = 'x'
				c1redge = 'x'
				c1ledge = 'x'
				c2hyp = 'x'
				c2redge = 'x'
				c2ledge = 'x'

			##########################################
			# PARENT INITIALIZATION
			##########################################
			
			# copy parameters
			self.direction = direction
			self.type_parent = (type_parent if config.assumeKVHToBeEqual == False else 'K')
			self.even_odd = even_odd
			self.hyp = hyp
			self.edge_right = edge_right
			self.edge_left = edge_left
			self.hyp_normal = hyp_normal

			# setup other parameters which are frequently used to avoid any recomputation
			
			# inverse direction string
			self.inv_direction = 'backward' if direction == 'forward' else 'backward'
			
			# inversed even/odd
			self.inv_even_odd = 'even' if even_odd == 'odd' else 'odd'
			
			# x_format is used frequently since traversing with forward+even state is similar to traversing backward+odd
			self.x_format = xor(direction == 'forward', even_odd == 'even')
			
			# stack to push/pop data along the hypotenuse
			# usually there's no problem to use one stack (e. g. the left one) always for push/pop operations for the hyp
			# but we'd like to create a better code with enhanced readability
			self.hyp_stack = 'right' if self.x_format else 'left'
			
			# inverse stack of hypotenuse
			self.inv_hyp_stack = 'left' if self.hyp_stack == 'right' else 'right'
			
			# stack for edges to push/pop data
			self.edge_stack = self.inv_hyp_stack

			# setup normals for edge_right and edge_left
			# these edges are enumerated anti-clockwise
			self.edge_right_normal = (hyp_normal+5)%8
			self.edge_left_normal = (hyp_normal+3)%8

			##########################################
			# CHILD INITIALIZATION
			##########################################

			# first traversed child
			self.child_first = scg.CTriangleProps()
			
			# second traversed child
			self.child_second = scg.CTriangleProps()

			# setup child_first and child_second normal values
			if self.x_format:
				# e. g. for forward and even traversal:
				# the first triangle's hypotenuse is aligned at +3*45 degree from the current hyp
				# the second triangle's hypotenuse is aligned at +3*45+90 degree from the current hyp 
				 
				self.child_first.hyp_normal = (hyp_normal+3)%8
				self.child_second.hyp_normal = (hyp_normal+5)%8

			else:
				self.child_first.hyp_normal = (hyp_normal+5)%8
				self.child_second.hyp_normal = (hyp_normal+3)%8

			self.child_first.type = (type_child_first if config.assumeKVHToBeEqual == False else 'K')
			self.child_first.hyp = c1hyp
			self.child_first.edge_right = c1redge
			self.child_first.edge_left = c1ledge
			
			self.child_second.type = (type_child_second if config.assumeKVHToBeEqual == False else 'K')
			self.child_second.hyp = c2hyp
			self.child_second.edge_right = c2redge
			self.child_second.edge_left = c2ledge

			# create signatures for methods to be used for recursive calls
			# parent signature
			self.sig_parent = self.type_parent
			self.sig_parent += '_'+self.d(self.even_odd)
			if config.edgeComm:
				self.sig_parent += '_'+self.e(hyp)+self.e(edge_right)+self.e(edge_left)
			self.sig_parent += '_'+direction

			# child_first signature
			self.sig_child_first = self.child_first.type
			self.sig_child_first += '_'+self.d(self.inv_even_odd)
			if config.edgeComm:
				self.sig_child_first += '_'+self.e(c1hyp)+self.e(c1redge)+self.e(c1ledge)
			self.sig_child_first += '_'+direction

			# child_second signature
			self.sig_child_second = self.child_second.type
			self.sig_child_second += '_'+self.d(self.inv_even_odd)
			if config.edgeComm:
				self.sig_child_second += '_'+self.e(c2hyp)+self.e(c2redge)+self.e(c2ledge)
			self.sig_child_second += '_'+direction

			# extend signatures by normals if requested
			if config.normals:
				self.sig_parent += '_normal_'+self.normals.getString(self.hyp_normal)
				self.sig_child_first += '_normal_'+self.normals.getString(self.child_first.hyp_normal)
				self.sig_child_second += '_normal_'+self.normals.getString(self.child_second.hyp_normal)

			# unique key for function to avoid creating duplicate signatures
			self.key = self.sig_parent+' => ('+self.sig_child_first+', '+self.sig_child_second+')'


	###################################
	# code generator
	###################################
	def __code_generator_non_adaptive(self, d):
#		sys.stderr.write(d.key+"\n")

		##################################
		# CREATE RECURSIVE CALLS
		##################################
		lib_tools.create_function_header(d, self.config)

		##################################
		# LEAF HANDLER
		##################################

		# separate between element action parameters and edge comm parameters
		# since edge comm parameters forwarded to another cell are stored in
		# accessed with reversed normal
		kernel_hook_parameters_element_action = []
		kernel_hook_parameters_edge_comm = []

		# vertex kernel_hook_parameters_element_action
		if self.config.vertices:
			kernel_hook_parameters_element_action.append("vx0, vy0, vx1, vy1, vx2, vy2")
			kernel_hook_parameters_edge_comm.append("vx0, vy0, vx1, vy1, vx2, vy2")

		# prepare normal kernel_hook_parameters_element_action for kernel hooks
		if self.config.normals:
			normalStr = self.normals.getNormal(d.hyp_normal)
			invNormalStr = self.normals.getInverseNormal(d.hyp_normal)
			kernel_hook_parameters_element_action.append(normalStr[0]+", "+normalStr[1])
			kernel_hook_parameters_edge_comm.append(invNormalStr[0]+", "+invNormalStr[1])
			
			normalStr = self.normals.getNormal(d.edge_right_normal)
			invNormalStr = self.normals.getInverseNormal(d.edge_right_normal)
			kernel_hook_parameters_element_action.append(normalStr[0]+", "+normalStr[1])
			kernel_hook_parameters_edge_comm.append(invNormalStr[0]+", "+invNormalStr[1])
			
			normalStr = self.normals.getNormal(d.edge_left_normal)
			invNormalStr = self.normals.getInverseNormal(d.edge_left_normal)
			kernel_hook_parameters_element_action.append(normalStr[0]+", "+normalStr[1])
			kernel_hook_parameters_edge_comm.append(invNormalStr[0]+", "+invNormalStr[1])

		# depth parameter
		if self.config.depthParameter:
			kernel_hook_parameters_element_action.append("depth")
			kernel_hook_parameters_edge_comm.append("depth")

		# element data
		if self.config.elementData:
			# store pointer to element data whenever an access to it has to be done
			if d.edge_right == 'new' or d.edge_left == 'new' or d.hyp == 'new' or d.direction == 'backward' or (not self.config.edgeComm):
				print "	TElementData *element_data = "+self.config.element_data_input_stack+".getNextDataPtr();"
			else:
				print "	"+self.config.element_data_input_stack+".getNextDataPtr();"

			kernel_hook_parameters_element_action.append("element_data")
			kernel_hook_parameters_edge_comm.append("element_data")

		# append this to the signature of the kernel element action calls
		kernel_action_signature_extension = ''
		
		# parameters for the kernel element computation hook
		joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
		joined_kernel_hook_parameters_edge_comm = ", ".join(kernel_hook_parameters_edge_comm)
		
		# true, if the kernel element action call should be created
		create_kernel_element_action = False

		# EDGE COMMUNICATION			
		if self.config.edgeComm:
			# from this starting point, we assume that the forward traversal is
			# applied at first, then the backward step

			#####################################################################
			# STORE INCOMING EDGE COMMUNICATION DATA (FORWARD)
			#
			# we have to store the incoming edge communication data on a temporary
			# stack for the forward step
			#
			if d.direction == 'forward':
				if d.even_odd == 'even':
					# store the element communication data of the left edge to the edge communication stack
					if d.edge_left == 'old':
						print "	edge_comm_buffer->push_PtrValue(edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnPtr());"
					if d.edge_right == 'old':
						print "	edge_comm_buffer->push_PtrValue(edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnPtr());"

					# read the edge communication data and store it to the buffer
					if d.edge_left == 'new':
						print "	kernelClass.computeEdgeCommData_Left("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"
					if d.edge_right == 'new':
						print "	kernelClass.computeEdgeCommData_Right("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"

				elif d.even_odd == 'odd':
					if d.edge_right == 'old':
						print "	edge_comm_buffer->push_PtrValue(edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnPtr());"
					if d.edge_left == 'old':
						print "	edge_comm_buffer->push_PtrValue(edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnPtr());"

					if d.edge_right == 'new':
						print "	kernelClass.computeEdgeCommData_Right("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"
					if d.edge_left == 'new':
						print "	kernelClass.computeEdgeCommData_Left("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"

				if d.hyp == 'old':
					print "	edge_comm_buffer->push_PtrValue(edge_data_comm_"+d.hyp_stack+"_edge_stack->pop_ReturnPtr());"
				elif d.hyp == 'new':
					print "	kernelClass.computeEdgeCommData_Hyp("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.hyp_stack+"_edge_stack->fakePush_ReturnPtr());"


			#####################################################################
			# using the backward traversal, previously stored edge communication is
			# read from the buffer stack as well as from the edge comm buffer
			# to start the computations for the element
			#
			elif d.direction == 'backward':
				# edge parameters for computation kernel
				# [hyp, right, left] edges - e.g. ['b','n','n'] for the first K triangle
				computation_kernel_edge_parameters = ['', '', '']
				
				#####################################################################
				# A) IMPORTANT note! (EDGE COMM BUFFER)
				#
				# At this point, we have to copy the edge comm data to a buffer to avoid any race conditions.
				# This has to be done since the kernel element operations are executed on the element data and thus
				# only the modified element data values can be communicated to the upcoming triangles.
				# (see (B))
				#
				print "	TEdgeData hyp_edge_data;"
				print "	TEdgeData right_edge_data;"
				print "	TEdgeData left_edge_data;"

				if d.hyp == 'old':		# hyp data is stored in edge communication during recent traversal
					print "	hyp_edge_data = edge_data_comm_"+d.hyp_stack+"_edge_stack->pop_ReturnRef();"
					computation_kernel_edge_parameters[0] = '&hyp_edge_data';
				elif d.hyp == 'new':	# hyp data was stored to edge buffer during previous traversal
					print "	hyp_edge_data = edge_comm_buffer->pop_ReturnRef();"
					computation_kernel_edge_parameters[0] = '&hyp_edge_data';

				if d.even_odd == 'even':
					if d.edge_right == 'old':
						print "	right_edge_data = edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnRef();"
						computation_kernel_edge_parameters[1] = '&right_edge_data';
					if d.edge_left == 'old':
						print "	left_edge_data = edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnRef();"
						computation_kernel_edge_parameters[2] = '&left_edge_data';

					if d.edge_right == 'new':
						print "	right_edge_data = edge_comm_buffer->pop_ReturnRef();"
						computation_kernel_edge_parameters[1] = '&right_edge_data';
					if d.edge_left == 'new':
						print "	left_edge_data = edge_comm_buffer->pop_ReturnRef();"
						computation_kernel_edge_parameters[2] = '&left_edge_data';

				elif d.even_odd == 'odd':
					if d.edge_left == 'old':
						print "	left_edge_data = edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnRef();"
						computation_kernel_edge_parameters[2] = '&left_edge_data';
					if d.edge_right == 'old':
						print "	right_edge_data = edge_data_comm_"+d.edge_stack+"_edge_stack->pop_ReturnRef();"
						computation_kernel_edge_parameters[1] = '&right_edge_data';

					if d.edge_left == 'new':
						print "	left_edge_data = edge_comm_buffer->pop_ReturnRef();"
						computation_kernel_edge_parameters[2] = '&left_edge_data';
					if d.edge_right == 'new':
						print "	right_edge_data = edge_comm_buffer->pop_ReturnRef();"
						computation_kernel_edge_parameters[1] = '&right_edge_data';

				#####################################################################
				# B) (EDGE COMM BUFFER)
				#
				# we have to push the information transport before kernel action to
				# avoid any race conditions.
				#
				if d.hyp == 'new':
					print "	kernelClass.computeEdgeCommData_Hyp("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.hyp_stack+"_edge_stack->fakePush_ReturnPtr());"

				if d.even_odd == 'even':
					if d.edge_right == 'new':
						print "	kernelClass.computeEdgeCommData_Right("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"
					if d.edge_left == 'new':
						print "	kernelClass.computeEdgeCommData_Left("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"
				elif d.even_odd == 'odd':
					if d.edge_left == 'new':
						print "	kernelClass.computeEdgeCommData_Left("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"
					if d.edge_right == 'new':
						print "	kernelClass.computeEdgeCommData_Right("+joined_kernel_hook_parameters_edge_comm+", edge_data_comm_"+d.edge_stack+"_edge_stack->fakePush_ReturnPtr());"

				#####################################################################
				# assemble kernel edge parameters and their EDGE/BORDER signatures
				#
				kernel_action_signature_extension += '_'
				for i in range(0,3):
					if computation_kernel_edge_parameters[i] != '':
						kernel_hook_parameters_element_action.append(computation_kernel_edge_parameters[i]);
					else:
						if i == 0:
							print "	kernelClass.computeBoundaryCommData_Hyp("+joined_kernel_hook_parameters_edge_comm+", &hyp_edge_data);"
							kernel_hook_parameters_element_action.append('&hyp_edge_data');
						elif i == 1:
							print "	kernelClass.computeBoundaryCommData_Right("+joined_kernel_hook_parameters_edge_comm+", &right_edge_data);"
							kernel_hook_parameters_element_action.append('&right_edge_data');
						elif i == 2:
							print "	kernelClass.computeBoundaryCommData_Left("+joined_kernel_hook_parameters_edge_comm+", &left_edge_data);"
							kernel_hook_parameters_element_action.append('&left_edge_data');

#						kernel_action_signature_extension += 'B'

				joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
				create_kernel_element_action = True

		else:
			create_kernel_element_action = True

		if create_kernel_element_action:
			joined_kernel_hook_parameters_element_action = ", ".join(kernel_hook_parameters_element_action)
			print "\tkernelClass.elementAction("+joined_kernel_hook_parameters_element_action+");"

		print """}"""


	###################################
	# action
	###################################

	# list to store already created recursive function calls to avoid duplicates
	existing_code_signatures = []

	def __action_create_code(self, d):
		d.creation_recursion_depth = self.code_generator_depth

		# return if the code for this function was already created
		if self.existing_code_signatures.count(d.key) != 0:
			# check whether we can update the creation_recursion_depth
			if d.config.limitRecursionTableMethodsByDepth != -1:
				if d.config.createRecursionTable:
					sig_parent = d.sig_parent
#					if d.config.adaptive:
#						if d.config.adaptive_create_first_traversal:
#							sig_parent += '_adaptive_marking_1st_traversal'
#						if d.config.adaptive_create_middle_traversal:
#							sig_parent += '_adaptive_marking'
#						if d.config.adaptive_create_last_traversal:
#							sig_parent += '_refine_or_coarsen'
					k = self.__findRecursionPythonTableIdxByName(sig_parent)
	
					if self.recursionTable[k].creation_recursion_depth > d.creation_recursion_depth:
						self.recursionTable[k].creation_recursion_depth = d.creation_recursion_depth

			return False

		self.existing_code_signatures.append(d.key)
		
		d.config.recMethodDepth = d.config.depth


		#
		# create adaptive traversal code
		#
		if d.config.adaptive:
			if d.config.depthParameter:
				d.config.depth = True
				d.config.recMethodDepth = True
				# create code with depth information
				d.config.createReversedStructureStack = True
				if d.config.adaptive_create_first_traversal:
					adaptive_first_traversal.code_generation(d)

				# switch off depth information
				d.config.depth = False
				d.config.createReversedStructureStack = False
				if d.config.adaptive_create_middle_traversal:
					adaptive_middle_traversals.code_generation(d)

				# switch on depth information
				d.config.depth = True
				d.config.createReversedStructureStack = True
				if d.config.adaptive_create_last_traversal:
					adaptive_last_traversal.code_generation(d)

			elif self.config.depthLimiterMax or self.config.depthLimiterMin or self.config.updateMaxDepth:

				# create code with depth information
				d.config.depth = True
				d.config.recMethodDepth = True
				d.config.createReversedStructureStack = True
				
				# FIRST
				if d.config.adaptive_create_first_traversal:
					adaptive_first_traversal.code_generation(d)
				d.config.depth = False
				d.config.createReversedStructureStack = False
				
				# MIDDLE
				if d.config.adaptive_create_middle_traversal:
					adaptive_middle_traversals.code_generation(d)

				# switch on depth information
				d.config.createReversedStructureStack = True

				# switch off depth information if not necessary anymore
				if self.config.updateMaxDepth:
					d.config.depth = True

				if d.config.adaptive_create_last_traversal:
					adaptive_last_traversal.code_generation(d)

				d.config.depthParameter = False
				d.config.depth = True
			else:
				d.config.depth = False
				d.config.recMethodDepth = False
				# create code without any depth information
				d.config.createReversedStructureStack = True
				if d.config.adaptive_create_first_traversal:
					adaptive_first_traversal.code_generation(d)

				d.config.createReversedStructureStack = False
				if d.config.adaptive_create_middle_traversal:
					adaptive_middle_traversals.code_generation(d)

				d.config.createReversedStructureStack = True
				if d.config.adaptive_create_last_traversal:
					adaptive_last_traversal.code_generation(d)

			if d.config.createRecursionTable:
				if d.config.adaptive_create_first_traversal:
#					d.sig_parent += '_adaptive_marking_1st_traversal'
#					d.sig_child_first += '_adaptive_marking_1st_traversal'
#					d.sig_child_second += '_adaptive_marking_1st_traversal'
					self.recursionTable.append(d)

				if d.config.adaptive_create_middle_traversal:
#					d.sig_parent += '_adaptive_marking'
#					d.sig_child_first += '_adaptive_marking'
#					d.sig_child_second += '_adaptive_marking'
					self.recursionTable.append(d)

				if d.config.adaptive_create_last_traversal:
#					d.sig_parent += '_refine_or_coarsen'
#					d.sig_child_first += '_refine_or_coarsen'
#					d.sig_child_second += '_refine_or_coarsen'
					self.recursionTable.append(d)

		else:
		#
		# create non-adaptive code
		#
			# if the depth is needed as a parameter, create the depth parameters
			if d.config.depthParameter:
				d.config.depth = True
			else:
				d.config.depth = False

			self.__code_generator_non_adaptive(d)
	
			# append code to recursion table
			if d.config.createRecursionTable:
				self.recursionTable.append(d)

		return True


	###################################
	# recursive unspecified functions
	###################################

	# FORWARD
	def __K_even_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'K', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_left, triangle_props.hyp, 'new', 'V', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			if self.config.limitRecursionTableMethodsByDepth != -1:
				if self.code_generator_depth >= self.config.limitRecursionTableMethodsByDepth:
					return
			else:
				return

		self.code_generator_depth += 1
		self.__H_odd_forward(d.child_first)
		self.__V_odd_forward(d.child_second)
		self.code_generator_depth -= 1

	def __H_even_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'H', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_left, triangle_props.hyp, 'new', 'K', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__V_odd_forward(d.child_first)
		self.__K_odd_forward(d.child_second)
		self.code_generator_depth -= 1

	def __V_even_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'V', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_left, triangle_props.hyp, 'new', 'K', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__H_odd_forward(d.child_first)
		self.__K_odd_forward(d.child_second)
		self.code_generator_depth -= 1



	def __K_odd_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'K', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_right, 'new', triangle_props.hyp, 'V', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__H_even_forward(d.child_first)
		self.__V_even_forward(d.child_second)
		self.code_generator_depth -= 1

	def __H_odd_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'H', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_right, 'new', triangle_props.hyp, 'K', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__V_even_forward(d.child_first)
		self.__K_even_forward(d.child_second)
		self.code_generator_depth -= 1

	def __V_odd_forward(self, triangle_props):
		d = self.CTriangleFactory('forward', 'V', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'H', triangle_props.edge_right, 'new', triangle_props.hyp, 'K', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__H_even_forward(d.child_first)
		self.__K_even_forward(d.child_second)
		self.code_generator_depth -= 1



	# BACKWARD
	def __K_even_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'K', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_right, 'new', triangle_props.hyp, 'H', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__V_odd_backward(d.child_first)
		self.__H_odd_backward(d.child_second)
		self.code_generator_depth -= 1

	def __H_even_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'H', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_right, 'new', triangle_props.hyp, 'V', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return

		self.code_generator_depth += 1
		self.__K_odd_backward(d.child_first)
		self.__V_odd_backward(d.child_second)
		self.code_generator_depth -= 1

	def __V_even_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'V', 'even', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_right, 'new', triangle_props.hyp, 'H', triangle_props.edge_left, triangle_props.hyp, 'old', self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__K_odd_backward(d.child_first)
		self.__H_odd_backward(d.child_second)
		self.code_generator_depth -= 1


	def __K_odd_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'K', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'V', triangle_props.edge_left, triangle_props.hyp, 'new', 'H', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__V_even_backward(d.child_first)
		self.__H_even_backward(d.child_second)
		self.code_generator_depth -= 1

	def __H_odd_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'H', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_left, triangle_props.hyp, 'new', 'V', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__K_even_backward(d.child_first)
		self.__V_even_backward(d.child_second)
		self.code_generator_depth -= 1

	def __V_odd_backward(self, triangle_props):
		d = self.CTriangleFactory('backward', 'V', 'odd', triangle_props.hyp, triangle_props.edge_right, triangle_props.edge_left, triangle_props.hyp_normal, 'K', triangle_props.edge_left, triangle_props.hyp, 'new', 'H', triangle_props.edge_right, 'old', triangle_props.hyp, self.config)
		if not self.__action_create_code(d):
			return
		self.code_generator_depth += 1
		self.__K_even_backward(d.child_first)
		self.__H_even_backward(d.child_second)
		self.code_generator_depth -= 1


	###################################
	# PRIVATE: Setup code creation
	###################################
	def __createCodeSetup(self, config):
		# setup the configuration
		self.config = config
		
		# output some copyright information
		print """
/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * This file was automatically generated by the sierpinski code generator
 */
"""

		# debug: restrict max. code generator depth
		# -111111111 to disable
		self.code_generator_depth = 0
		
		# if the adaptive processing is turned on, we also need an edge communication
		if self.config.adaptive:
			self.config.edgeComm = True

	def __helperFunctionBorderTypeToEnumType(self, type):
		if type == 'old':
			return 'EDGE_TYPE_OLD'
		elif type == 'new':
			return 'EDGE_TYPE_NEW'
		elif type == 'b':
			return 'EDGE_TYPE_BORDER'
		sys.stderr.write("UNKNOWN ENUM TYPE")
		assert(False)

	def __findRecursionPythonTableIdxByName(self, fun_name):
		for r in range(0, len(self.recursionTable)):
			if self.recursionTable[r].sig_parent == fun_name:
				return r
		sys.stderr.write("REC FUN NAME " + fun_name + " NOT FOUND!")
		raise Exception.MemoryError
		sys.exit()

	def __sfcMethodExists(self, fun_name):
#		print fun_name
#		print self.recursionTable[0].sig_parent
		for r in range(0, len(self.recursionTable)):
			if self.recursionTable[r].sig_parent == fun_name:
				return True
		return False

	def __findRecursionTableIdx_TypeToK_EdgeType1ToType2(self, config, r, type_old, type_new):
		match_factory = copy.deepcopy(r)
		match_factory.type_parent = 'K'

		if config.edgeComm:
			if match_factory.hyp == type_old:
				match_factory.hyp = type_new
			if match_factory.edge_left == type_old:
				match_factory.edge_left = type_new
			if match_factory.edge_right == type_old:
				match_factory.edge_right = type_new

		for r in range(0, len(self.recursionTable)):
			x = self.recursionTable[r]
			
			if match_factory.type_parent != x.type_parent:
				continue
			if match_factory.even_odd != x.even_odd:
				continue
			if match_factory.direction != x.direction:
				continue
	
			if config.edgeComm:
				if match_factory.hyp != x.hyp:
					continue
				if match_factory.edge_left != x.edge_left:
					continue
				if match_factory.edge_right != x.edge_right:
					continue

			if config.normals:
				if match_factory.hyp_normal != x.hyp_normal:
					continue

			return self.recursionTable[r].idx

		sys.stdout = sys.__stdout__
		sys.stderr.write("REC FUNCTION NOT FOUND!\n")
		print "       hyp: "+match_factory.hyp
		print "     right: "+match_factory.edge_right
		print "      left: "+match_factory.edge_left
		print "      type: "+match_factory.type_parent
		print "  even_odd: "+match_factory.even_odd
		print " direction: "+match_factory.direction
		print "hyp_normal: "+str(match_factory.hyp_normal)
		print "       idx: "+str(match_factory.idx)
		sys.exit()		


	###################################
	# output recursion table
	###################################
	def outputRecursionTable(self, config, rec_function_postfix = ''):

		if not config.createRecursionTable:
			return
		
#		print "-------------------------------------------------------"
#		print "-------------------------------------------------------"
#		print rec_function_postfix
#		print "-------------------------------------------------------"
#		print "-------------------------------------------------------"
		
		rec_current_parameters = []

		# vertex information is created during traversal (e. g. for visualization) 
		if config.vertices:
			rec_current_parameters.append("TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar, TVertexScalar")

		## if the depth information has to be created, an 'int depth' is appended to the kernel parameters
		if config.recMethodDepth:
			rec_current_parameters.append("int")

		parameter_types = ", ".join(rec_current_parameters)
		sys.stderr.write("+++++++++++++++ REC PARAMS: " + parameter_types)
		
		if config.limitRecursionTableMethodsByDepth != -1:
			recursions = 0
			for r in range(0, len(self.recursionTable)):
				if self.recursionTable[r].creation_recursion_depth < config.limitRecursionTableMethodsByDepth:
					self.recursionTable[r].idx = recursions
					recursions = recursions + 1
				else:
					self.recursionTable[r].idx = -1
					
			sys.stderr.write("limited recursions: " + str(recursions)+"\n")
		else:
			recursions = len(self.recursionTable)
			for r in range(0, len(self.recursionTable)):
				self.recursionTable[r].idx = r
			
			sys.stderr.write("non-limited recursions: " + str(recursions)+"\n")


		print
		print "static const int number_of_recursion_functions = "+str(recursions)+";"
		print "typedef "+('char' if config.adaptive_char_return_variable else 'void')+" (TThisClass::*TRecursiveMethod)("+parameter_types+");"


		if True:
			#
			# OUTPUT RECURSION ARRAY
			#
			self.outputRecursionArray(config)
		else:
			#
			# OUTPUT RECURSION TABLE
			#
			print "class SRecursiveMethodUnrolled"
			print "{"
			print "public:"
			print "	TRecursiveMethod parent_method;"
	
	
			if config.createSplitJoinFirstChildInformation:
				print "	TRecursiveMethod parent_method_type_to_k_and_old_to_new_idx;"
				print "	TRecursiveMethod parent_method_type_to_k_and_new_to_old_idx;"
	
			print "	int first_child_method_idx;"
			print "	TRecursiveMethod first_child_method;"
			print "	int second_child_method_idx;"
			print "	TRecursiveMethod second_child_method;"
	
	
			if config.createSplitJoinFirstChildInformation:
				print "	int first_child_method_type_to_k_and_old_to_new_idx;"
				print "	int first_child_method_type_to_k_and_new_to_old_idx;"
				print "	int second_child_method_type_to_k_and_old_to_new_idx;"
				print "	int second_child_method_type_to_k_and_new_to_old_idx;"
	
			print "	CTriangle_Factory triangleFactory;"
	
			print "	bool operator==(SRecursiveMethodUnrolled &r)"
			print "	{"
			print "		return operator==(r.triangleFactory);"
			print "	}"
			print
			print "	bool operator==(CTriangle_Factory &t)"
			print "	{"
	
			# array with comparison operators
			cmps = []
	
			# type
			cmps.append("(triangleFactory.triangleType == t.triangleType)")
	
			# even odd
			cmps.append("(triangleFactory.evenOdd == t.evenOdd)")
	
			# forward/backward
			cmps.append("(triangleFactory.traversalDirection == t.traversalDirection)")
	
			# edge communication types
			if config.edgeComm:
				cmps.append("(triangleFactory.edgeTypes.hyp == t.edgeTypes.hyp)")
				cmps.append("(triangleFactory.edgeTypes.right == t.edgeTypes.right)")
				cmps.append("(triangleFactory.edgeTypes.left == t.edgeTypes.left)")
	
			# normals?
			if config.normals:
				cmps.append("(triangleFactory.hypNormal == t.hypNormal)")
	
			print "		return "+"\n				&& ".join(cmps)+";"
			print "	}"
			print "};"
	
	
			#
			# recursion table & setup of recursion table
			#
			print "SRecursiveMethodUnrolled *recursionTable;"
			print
			print "void setupRecursionTable()"
			print "{"
			print "	recursionTable = new SRecursiveMethodUnrolled[number_of_recursion_functions];"
	
			last_recursion_table_idx = 0
			for r in range(0, len(self.recursionTable)):
				if self.recursionTable[r].idx == -1:
					continue
	
				last_recursion_table_idx = self.recursionTable[r].idx
	
				idx = str(self.recursionTable[r].idx)
				print "	recursionTable["+idx+"].parent_method = &"+config.recursionTableClass+"::"+self.recursionTable[r].sig_parent+rec_function_postfix+";"
	#			print "	recursionTable["+idx+"].parent_method_idx = &"+config.recursionTableClass+"::"+self.recursionTable[r].sig_parent+rec_function_postfix+";"
				
	#			r = 
	#			self.recursionTable[r].idx self.__findRecursionPythonTableIdxByName(self.recursionTable[r].sig_parent)
	
	#			if config.createSplitJoinFirstChildInformation:
	#				parent_method_python_table_idx = self.__findRecursionPythonTableIdxByName(self.recursionTable[r].sig_parent)
	#				i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[parent_method_python_table_idx], 'old', 'new')
	#				if i >= recursions:
	#					sys.stdout = sys.__stdout__
	#					print "first child old_to_new idx "+str(i)+" > recursions " + str(recursions)
	#					sys.exit()	
	#				print "	recursionTable["+idx+"].parent_method_type_to_k_and_old_to_new_idx = "+str(i)+";"
	#
	#				parent_method_python_table_idx = self.__findRecursionPythonTableIdxByName(self.recursionTable[r].sig_parent)
	#				i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[parent_method_python_table_idx], 'new', 'old')
	#				if i >= recursions:
	#					sys.stdout = sys.__stdout__
	#					print "first child old_to_new idx "+str(i)+" > recursions " + str(recursions)
	#					sys.exit()	
	#				print "	recursionTable["+idx+"].parent_method_type_to_k_and_new_to_old_idx = "+str(i)+";"
	
				first_child_method_python_table_idx = self.__findRecursionPythonTableIdxByName(self.recursionTable[r].sig_child_first)
				first_child_method_table_idx = self.recursionTable[first_child_method_python_table_idx].idx
				if first_child_method_table_idx >= recursions:
					sys.stdout = sys.__stdout__
					print "first table idx "+str(first_child_method_table_idx)+" > recursions"+str(recursions)
					sys.exit()
				print "	recursionTable["+idx+"].first_child_method_idx = "+str(first_child_method_table_idx)+";"
				print "	recursionTable["+idx+"].first_child_method = &"+config.recursionTableClass+"::"+self.recursionTable[r].sig_child_first+rec_function_postfix+";"
	
				second_child_method_python_table_idx = self.__findRecursionPythonTableIdxByName(self.recursionTable[r].sig_child_second)
				second_child_method_table_idx = self.recursionTable[second_child_method_python_table_idx].idx
				if second_child_method_table_idx >= recursions:
					sys.stdout = sys.__stdout__
					print "second table idx "+str(second_child_method_table_idx)+" > recursions "+str(recursions)
					sys.exit()
				print "	recursionTable["+idx+"].second_child_method_idx = "+str(second_child_method_table_idx)+";"
				print "	recursionTable["+idx+"].second_child_method = &"+config.recursionTableClass+"::"+self.recursionTable[r].sig_child_second+rec_function_postfix+";"
	
				print "	recursionTable["+idx+"].triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_"+self.recursionTable[r].type_parent+";"
				print "	recursionTable["+idx+"].triangleFactory.evenOdd = CTriangle_Enums::"+('ODD' if self.recursionTable[r].even_odd == 'odd' else 'EVEN')+";"
				print "	recursionTable["+idx+"].triangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_"+('FORWARD' if self.recursionTable[r].direction == 'forward' else 'BACKWARD')+";"
	
				if config.createSplitJoinFirstChildInformation:
					i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[first_child_method_python_table_idx], 'old', 'new')
					if i >= recursions:
						sys.stdout = sys.__stdout__
						print "first child old_to_new idx "+str(i)+" > recursions " + str(recursions)
						sys.exit()	
					print "	recursionTable["+str(idx)+"].first_child_method_type_to_k_and_old_to_new_idx = "+str(i)+";"
	
					i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[first_child_method_python_table_idx], 'new', 'old')
					if i >= recursions:
						sys.stdout = sys.__stdout__
						print "first child new_to_old idx "+str(i)+" > recursions " + str(recursions)
						sys.exit()	
					print "	recursionTable["+str(idx)+"].first_child_method_type_to_k_and_new_to_old_idx = "+str(i)+";"
	
					i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[second_child_method_python_table_idx], 'old', 'new')
					if i >= recursions:
						sys.stdout = sys.__stdout__
						print "second child old_to_new idx "+str(i)+" > recursions " + str(recursions)
						sys.exit()
					print "	recursionTable["+str(idx)+"].second_child_method_type_to_k_and_old_to_new_idx = "+str(i)+";"
					
					i = self.__findRecursionTableIdx_TypeToK_EdgeType1ToType2(config, self.recursionTable[second_child_method_python_table_idx], 'new', 'old')
					if i >= recursions:
						sys.stdout = sys.__stdout__
						print "second child new_to_old idx "+str(i)+" > recursions " + str(recursions)
						sys.exit()
					print "	recursionTable["+str(idx)+"].second_child_method_type_to_k_and_new_to_old_idx = "+str(i)+";"
	
				if config.edgeComm:
					print "	recursionTable["+str(idx)+"].triangleFactory.edgeTypes.hyp = CTriangle_Enums::"+self.__helperFunctionBorderTypeToEnumType(self.recursionTable[r].hyp)+";"
					print "	recursionTable["+str(idx)+"].triangleFactory.edgeTypes.right = CTriangle_Enums::"+self.__helperFunctionBorderTypeToEnumType(self.recursionTable[r].edge_right)+";"
					print "	recursionTable["+str(idx)+"].triangleFactory.edgeTypes.left = CTriangle_Enums::"+self.__helperFunctionBorderTypeToEnumType(self.recursionTable[r].edge_left)+";"
					
				if config.normals:
					print "	recursionTable["+str(idx)+"].triangleFactory.hypNormal = CTriangle_Enums::"+"NORMAL_"+self.normals.getString(self.recursionTable[r].hyp_normal).upper()+";"
	
				print
	
			if last_recursion_table_idx != recursions-1:
				sys.stdout = sys.__stdout__
				print "not enough table entries created: last_recursion_table_idx "+str(last_recursion_table_idx)+" != recursions " + str(recursions)
				sys.exit()
	
			print "#if DEBUG"
			print "	recursionTableMagicCode = 0x1505;"
			print "#endif"
	
			print "}"
			print ""
			print "void shutdownRecursionTable()"
			print "{"
			print "	//delete []recursionTable;"
			print "}"
			print "void shutdown_RootPartition()"
			print "{"
			print "	shutdownRecursionTable();"
			print "}"
			print ""




	###################################
	# output recursion array
	###################################
	def outputRecursionArray(self, config):
		#
		# get pointer to recursion method
		#

		# determine array dimensions
		arrayDims = []

		# type
		if not config.assumeKVHToBeEqual:
			arrayDims.append(['K', 'V', 'H'])

		# even odd
		arrayDims.append(['e', 'o'])

		# edge communication types
		if config.edgeComm:
			edgeTypes = []
			for a in ['n', 'o', 'b']:
				for b in ['n', 'o', 'b']:
						for c in ['n', 'o', 'b']:
							edgeTypes.append(a+b+c+'')

			# new, old, border
			# hyp, right, left
			arrayDims.append(edgeTypes)

		# forward/backward
		arrayDims.append(['forward', 'backward'])

		# normals?
		if config.normals:
			arrayDims.append(['normal_'+i for i in ['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw']])


		def printMethodArray(remainingArrayDims, sig):
			if len(remainingArrayDims) == 0:
				if self.__sfcMethodExists(sig):
					print "		&"+config.recursionTableClass+"::"+sig,
				else:
					print "		nullptr",
				return
			
			print "	{"

			for r in range(0, len(remainingArrayDims[0])):
				i = remainingArrayDims[0][r]
				nsig = sig+i
				if len(remainingArrayDims) > 1:
					nsig += '_'
				printMethodArray(remainingArrayDims[1:], nsig)
				if r < len(remainingArrayDims[0])-1:
					print ","
				else:
					print ""
			print "	}",


		print "enum ESFCMethod{"
		print "	SFC_METHOD_DEFAULT = 0,"
		print "	SFC_METHOD_FORCE_FORWARD = (1 << 0),"
		print "	SFC_METHOD_FORCE_BACKWARD = (1 << 1),"
		print "	SFC_METHOD_NEW_TO_OLD_EDGE_TYPES = (1 << 2),"
		print "	SFC_METHOD_OLD_TO_NEW_EDGE_TYPES = (1 << 3),"
		print "	SFC_METHOD_LIMITER___ = (1 << 4)"
		print "};"
		print ""
		print "template <int specialMode = 0>"
		print "TRecursiveMethod getSFCMethod(CTriangle_Factory &i_cTriangleFactory)"
		print "{"
		print "	static TRecursiveMethod methodArray",
		for dim in arrayDims:
			print "["+str(len(dim))+"]",
		print "="

		if config.assumeKVHToBeEqual:
			printMethodArray(arrayDims, 'K_')
		else:
			printMethodArray(arrayDims, '')

		print "	;"
		
		lookup_indices = ""
		
		# type
		if not config.assumeKVHToBeEqual:
			lookup_indices += "[i_cTriangleFactory.triangleType]"
		print "		assert(i_cTriangleFactory.triangleType != CTriangle_Enums::TRIANGLE_TYPE_INVALID);"

		# even odd
		lookup_indices += "[i_cTriangleFactory.evenOdd]"
		print "		assert(i_cTriangleFactory.evenOdd != CTriangle_Enums::EVEN_ODD_INVALID);"

		# edge communication types
		if config.edgeComm:
			print "		assert(i_cTriangleFactory.edgeTypes.hyp != CTriangle_Enums::EDGE_TYPE_INVALID);"
			print "		assert(i_cTriangleFactory.edgeTypes.left != CTriangle_Enums::EDGE_TYPE_INVALID);"
			print "		assert(i_cTriangleFactory.edgeTypes.right != CTriangle_Enums::EDGE_TYPE_INVALID);"

			print "		CTriangle_Enums::EEdgeType hyp, left, right;"
			print "		if (specialMode == 0)"
			print "		{"
			print "			hyp = i_cTriangleFactory.edgeTypes.hyp;"
			print "			right = i_cTriangleFactory.edgeTypes.right;"
			print "			left = i_cTriangleFactory.edgeTypes.left;"
			print "		}"
			print "		else if (specialMode & SFC_METHOD_NEW_TO_OLD_EDGE_TYPES)"
			print "		{"
			print "			hyp = (i_cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.hyp);"
			print "			right = (i_cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.right);"
			print "			left = (i_cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_NEW ? CTriangle_Enums::EDGE_TYPE_OLD : i_cTriangleFactory.edgeTypes.left);"
			print "		}"
			print "		else if (specialMode & SFC_METHOD_OLD_TO_NEW_EDGE_TYPES)"
			print "		{"
			print "			hyp = (i_cTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.hyp);"
			print "			right = (i_cTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.right);"
			print "			left = (i_cTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD ? CTriangle_Enums::EDGE_TYPE_NEW : i_cTriangleFactory.edgeTypes.left);"
			print "		}"
			print "	#if DEBUG"
			print "		else"
			print "		{"
			print "			assert(false);"
			print "		}"
			print "	#endif"
			lookup_indices += "[hyp*9+right*3+left]"

		# forward/backward
		print "		CTriangle_Enums::EDirection direction;"
		print "		if (specialMode & SFC_METHOD_FORCE_FORWARD)"
		print "		{"
		print "			direction = CTriangle_Enums::DIRECTION_FORWARD;"
		print "		}"
		print "		else if (specialMode & SFC_METHOD_FORCE_BACKWARD)"
		print "		{"
		print "			direction = CTriangle_Enums::DIRECTION_BACKWARD;"
		print "		}"
		print "		else"
		print "		{"
		print "			direction = i_cTriangleFactory.traversalDirection;"
		print "		}"

		lookup_indices += "[direction]"
		print "		assert(i_cTriangleFactory.traversalDirection != CTriangle_Enums::DIRECTION_INVALID);"

		# normals?
		if config.normals:
			lookup_indices += "[i_cTriangleFactory.hypNormal]"
		print "		assert(i_cTriangleFactory.hypNormal != CTriangle_Enums::NORMAL_INVALID);"

		print "#if DEBUG"
		print "	assert(methodArray"+lookup_indices+" != nullptr);"
		print "#endif"
		
		print "		return methodArray"+lookup_indices+";"
		print "}"

		


	###################################
	# CREATE CODE for K recursions
	###################################
	def __createCodeK(self, config, codeForAllEdgeTypes = []):
		self.__createCodeSetup(config)

		if len(codeForAllEdgeTypes) == 0:
			# create code only for recursive edge types
			
			# create the forward code
			if self.config.createForwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('K', 'new', 'b', 'b', self.normals.ne)
				self.__K_even_forward(triangle_props)
	
			# create the backward code
			if self.config.createBackwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('K', 'old', 'b', 'b', self.normals.ne)
				self.__K_even_backward(triangle_props)
		else:
			# create code for all edge types given in codeForAllEdgeTypes
			
			for i in codeForAllEdgeTypes:

				triangle_props = self.CTriangleProps()
				
				evenOdd = 'even'
				if len(i) <= 4:
					triangle_props.setup('K', i[0], i[1], i[2], i[3])
				else:
					# even/odd
					evenOdd = i[4]
					triangle_props.setup('K', i[0], i[1], i[2], i[3], evenOdd)

				if evenOdd == 'even':
					if self.config.createForwardCode:
						self.__K_even_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__K_even_backward(triangle_props)
				elif evenOdd == 'odd':
					
					# create forward code
					if self.config.createForwardCode:
						self.__K_odd_forward(triangle_props)
			
					# create the backward code
					if self.config.createBackwardCode:
						self.__K_odd_backward(triangle_props)
						
				else:
					sys.stderr.write("ERROR evenOdd\n")


	###################################
	# CREATE CODE for H recursions
	###################################
	def __createCodeH(self, config, codeForAllEdgeTypes = []):
		self.__createCodeSetup(config)

		if len(codeForAllEdgeTypes) == 0:
			# create the forward code
			if self.config.createForwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('H', 'old', 'b', 'b', self.normals.sw)
				self.__H_even_forward(triangle_props)
	
			# create the backward code
			if self.config.createBackwardCode:
				triangle_props = self.CTriangleProps()
				triangle_props.setup('H', 'new', 'b', 'b', self.normals.sw)
				self.__H_even_backward(triangle_props)
		else:
			for i in codeForAllEdgeTypes:

				# if the 4th array element is set, this refers to even/odd
				if len(i) > 4:
					even_odd = i[4]
				else:
					even_odd = 'even'

				triangle_props = self.CTriangleProps()
				triangle_props.setup('H', i[0], i[1], i[2], i[3], even_odd)

				# create the forward code
				if self.config.createForwardCode:
					method = getattr(self, "H_"+even_odd+"_forward")
					method(triangle_props)
		
				# create the backward code
				if self.config.createBackwardCode:
					method = getattr(self, "H_"+even_odd+"_backward")
					method(triangle_props)


	###################################
	# CREATE QUAD CODE
	#
	# this is the big magic function to create all the fancy c++ code
	# for the simple quad traversals
	#
	# !obsolete! due to parallel version
	###################################
#	def createQuadCode(self, config):
#		# cleanup existing code signatures
##		self.existing_code_signatures = []
#
#		self.__createCodeK(config)
#		self.__createCodeH(config)


	#
	# return all normals which are available for an edge type
	#
	def __allNormalsForEdgeType(self, t1, t2, t3):
		#
		# to reduce the code size and thus to reduce the compilation
		# time, only specific traversal methods are created.
		#
		return [
#			[t1, t2, t3, self.normals.n, 'even'],
			[t1, t2, t3, self.normals.ne, 'even'],
#			[t1, t2, t3, self.normals.e, 'even'],
			[t1, t2, t3, self.normals.se, 'even'],
#			[t1, t2, t3, self.normals.s, 'even'],
			[t1, t2, t3, self.normals.sw, 'even'],
#			[t1, t2, t3, self.normals.w, 'even'],
			[t1, t2, t3, self.normals.nw, 'even'],
			
			[t1, t2, t3, self.normals.n, 'odd'],
#			[t1, t2, t3, self.normals.ne, 'odd'],
			[t1, t2, t3, self.normals.e, 'odd'],
#			[t1, t2, t3, self.normals.se, 'odd'],
			[t1, t2, t3, self.normals.s, 'odd'],
#			[t1, t2, t3, self.normals.sw, 'odd'],
			[t1, t2, t3, self.normals.w, 'odd'],
#			[t1, t2, t3, self.normals.nw, 'odd']
		]


	#
	# create code for parallel traversals
	#
	def createCode(self, config):
		# reset signatures
		self.existing_code_signatures = []
		
		# setup edge type list for K traversals		
		edgeTypeListK = []
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'b', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('new', 'new', 'new'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('old', 'old', 'old'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'new', 'new'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'old', 'old'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('new', 'b', 'new'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('old', 'b', 'old'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('new', 'new', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('old', 'old', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'b', 'new'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'b', 'old'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('new', 'b', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('old', 'b', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'new', 'b'))
		edgeTypeListK.extend(self.__allNormalsForEdgeType('b', 'old', 'b'))

		# create code for all K traversals
		self.__createCodeK(config, edgeTypeListK)



	###################################
	# RESET
	###################################
	def reset(self):
		self.existing_code_signatures = []
		self.recursionTable = []
