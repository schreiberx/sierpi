import sys

#
# This file is part of the Sierpi project. For conditions of distribution and
# use, please see the copyright notice in the file 'copyright.txt' at the root
# directory of this package and the copyright notice at
# http://www5.in.tum.de/sierpi
#


#
# this is a helper function to handle all different kinds of parameters
# for the recursive calls but also for the kernel hooks. 
#
def create_function_header(
                           d,                                       # triangle information
                           config,                                  # configuration
                           function_postfix = '',                   # string to append after function
                           createReversedStructureStack = True      # create reversed structure stack?
                    ):
    
    # current parameters for this recursively called function
    rec_current_parameters = []

    # vertex information is created during traversal (e. g. for visualization) 
    if config.vertices:
        rec_current_parameters.append("TVertexScalar vx0, TVertexScalar vy0, TVertexScalar vx1, TVertexScalar vy1, TVertexScalar vx2, TVertexScalar vy2")

    # if the depth information has to be created, an 'int depth' is appended to the kernel parameters
    if config.depth:
        rec_current_parameters.append("int depth")

    print """
void """+d.sig_parent+function_postfix+"("+", ".join(rec_current_parameters)+""")
{"""
    # output function
    if d.config.code_debug_output:
        print "    std::cout << std::endl;"
        print "    std::cout << \""+d.key+"["+d.method_postfix+"]\" << std::endl;"
        print "    std::cout << \"  "+d.hyp_stack+" stack: \" << *comm_"+d.hyp_stack+"_edge_stack << std::endl;"
        print "    std::cout << \"  "+d.edge_stack+" stack: \" << *comm_"+d.edge_stack+"_edge_stack << std::endl;"
        print "    std::cout << \"  structure_stack_in: \" << *structure_stack_in << std::endl;"
        print "    std::cout << \"  structure_stack_out: \" << *structure_stack_out << std::endl;"
        print "    std::cout << \"  adaptivity_state_flag_stack_in: \" << *adaptivity_state_flag_stack_in << std::endl;"
        print "    std::cout << \"  adaptivity_state_flag_stack_out: \" << *adaptivity_state_flag_stack_out << std::endl;"


    print """
    if ("""+config.structure_input_stack+""".getNextData())
    {"""

    # parameter list for recursive kernel calls
    rec_called_parameters1 = []
    rec_called_parameters2 = []

    if config.vertices:
        print "    TVertexScalar mx = (vx0+vx1)*(TVertexScalar)0.5;"
        print "    TVertexScalar my = (vy0+vy1)*(TVertexScalar)0.5;"
        if d.x_format:
            rec_called_parameters1.append("vx2, vy2, vx0, vy0, mx, my")
            rec_called_parameters2.append("vx1, vy1, vx2, vy2, mx, my")
        else:
            rec_called_parameters1.append("vx1, vy1, vx2, vy2, mx, my")
            rec_called_parameters2.append("vx2, vy2, vx0, vy0, mx, my")

    # the depth information is stored to a depth variable and has to be modified
    if config.depth:
        print "        depth++;"
        # append the depth to the recursive parameter calls
        rec_called_parameters1.append("depth")
        rec_called_parameters2.append("depth")

    # code for recursive calls
    print "        "+d.sig_child_first+function_postfix+"("+', '.join(rec_called_parameters1)+");"
    print "        "+d.sig_child_second+function_postfix+"("+', '.join(rec_called_parameters2)+");"

    # if a reversed structure stack should be created, we have to push the information to the output stack
    if config.createReversedStructureStack:
        print "        structure_stack_out->push(1);"

    # if the kernel should be executed only for leaves, we're done at this point.
    # otherwise we decrease the depth
    if config.kernelCallsForLeavesOnly:
        print "        return;"
    elif config.depth:
        print "    depth--;"

    print """    }"""

    # if a reversed structure stack should be created, we have to push the information to the output stack
    if config.createReversedStructureStack:
        print "    structure_stack_out->push(0);"



def createIncomingEdgeStateTransfer(d):
    #
    # read incoming flags from left and right stacks
    #
    if d.hyp == 'old':
        print    "    unsigned char hyp_trigger = comm_"+d.hyp_stack+"_edge_stack->pop();"
        print    "    incomingEdgeStateTransfer |= (hyp_trigger&1) << 2;"

    if d.x_format:
        if d.edge_left == 'old':
            print    "    unsigned char edge_left_trigger = comm_"+d.edge_stack+"_edge_stack->pop();"
            print    "    incomingEdgeStateTransfer |= (edge_left_trigger&1) << 0;"

        if d.edge_right == 'old':
            print    "    unsigned char edge_right_trigger = comm_"+d.edge_stack+"_edge_stack->pop();"
            print    "    incomingEdgeStateTransfer |= (edge_right_trigger&1) << 1;"
    else:
        if d.edge_right == 'old':
            print    "    unsigned char edge_right_trigger = comm_"+d.edge_stack+"_edge_stack->pop();"
            print    "    incomingEdgeStateTransfer |= (edge_right_trigger&1) << 1;"

        if d.edge_left == 'old':
            print    "    unsigned char edge_left_trigger = comm_"+d.edge_stack+"_edge_stack->pop();"
            print    "    incomingEdgeStateTransfer |= (edge_left_trigger&1) << 0;"

    print   ""



def checkStateChangeForRefinement(d, parameters):
    x = 0

    if d.config.depthLimiterMax:
        print   "    if (depth < depth_limiter_max)"
        print   "    {"
        x += 1

    # if a refinement is desired, switch to state 1 (running hyp refinement)    
    print    "\t"*x+"    if (kernelClass.should_refine("+(', '.join(parameters))+"))"
    print    "\t"*x+"        old_state = 3;"

    if d.config.depthLimiterMax:
        print   "    }"
        x -= 1
        
    if d.config.callShouldRefineForAllLeaves and d.config.depthLimiterMax:
        print    "\t"*x+"     else"
        print    "\t"*x+"        kernelClass.should_refine("+(', '.join(parameters))+");"
    

    print ""



def checkStateChangeForCoarsening(d, parameters):
    # don't allow coarsening if both catheti edges are at the border
    if d.edge_right != 'b' or d.edge_left != 'b':
        x = 0

        # only check coarsening, if the possibly existing edge trigger values allow a coarsening operation
        if d.edge_right == 'old' or d.edge_left == 'old':
            tests = []
            if d.edge_left == 'old':
                tests.append("(edge_left_trigger == 2)")
            if d.edge_right == 'old':
                tests.append("(edge_right_trigger == 2)")
            print   "\t"*x+"    if ("+(" && ".join(tests))+")"
            print   "\t"*x+"    {"
            x += 1

        if d.config.depthLimiterMin:
            print    "\t"*x+"    if (depth > depth_limiter_min)"
            print    "\t"*x+"    {"
            x += 1

        # only allow coarsening if the current state is 0 (no refinement)
        print    "\t"*x+"    if (old_state == 0)"
        print    "\t"*x+"    {"

        # if a coarsening is desired, switch to state 2
        print    "\t"*x+"        if (kernelClass.should_coarsen("+(', '.join(parameters))+"))"
        print    "\t"*x+"        {"
        print    "\t"*x+"            old_state = 2;"
        print    "\t"*x+"        }"
        print    "\t"*x+"    }"

        if d.config.depthLimiterMax:
            x -= 1
            print    "\t"*x+"    }"


        if d.edge_right == 'old' or d.edge_left == 'old':
            x -= 1
            print    "\t"*x+"    }"

        print ""



def coarseningAdjacentAgreement(d, check_adjacent_coarsening):

    if d.edge_left == 'old' or d.edge_right == 'old' or d.hyp == 'old':
        #
        # new_state is not 2 if there was a refinement request from some adjacent triangle.
        # this adjacent triangle must not be equal to another adjacent triangle requesting a
        # coarsening. If such a 'requesting coarsening triangle' exists, we have to do
        # another traversal.
        #

        print   "    if (new_state != 2)"
        print   "    {"

        tests = []
        if d.edge_left == 'old':
            tests.append("(edge_left_trigger == 2)")

        if d.edge_right == 'old':
            tests.append("(edge_right_trigger == 2)")

        if d.hyp == 'old':
           tests.append("(hyp_trigger == 2)")

        print   "        if ("+(" || ".join(tests))+")"
        print   "            repeat_traversal = true;"

        print   "    }"

        # if this triangle likes to trigger a coarsening, we have to check whether all other
        # adjacent triangles which can transmit some information to this triangle during the
        # current traversal, agree to the coarsening 
        print   "    else"
        print   "    {"

        # check valid adjacent coarsening
        if check_adjacent_coarsening:
            if d.edge_left == 'old' or d.edge_right == 'old':
                tests = []
                tests2 = []
                if d.edge_left == 'old':
                    tests.append("(edge_left_trigger != 2)")
                    tests2.append("(edge_left_trigger == 2)")
    
                if d.edge_right == 'old':
                    tests.append("(edge_right_trigger != 2)")
                    tests2.append("(edge_right_trigger == 2)")
    
                print   "        if ("+(" || ".join(tests))+")"
                print   "        {"
                print   "            new_state = 0;"
                
                if d.edge_left == 'old' and d.edge_right == 'old':
                    print   "            if ("+(" || ".join(tests2))+")"

                print   "                repeat_traversal = true;"
                print   "        }"
    
                if d.hyp == 'old':
                    print   "        else"

        # if an adjacent triangle likes to coarsen with our hyp,
        # the information about not allowed coarsening has to be transmitted
        # within the next coarsening pass  
        if d.hyp == 'old':
            print   "        if (hyp_trigger == 2)"
            print   "            repeat_traversal = true;"

        print   "    }"
        print   ""



def getNewStateForStateTransfer():
        # load new state
        print    "    unsigned char new_state = automatonTable[old_state][incomingEdgeStateTransfer];"
        print    "    unsigned char outgoingEdgeStateInformation = new_state >> 4;"
        print    "    new_state = new_state & 7;"
        print    ""



def createOutgoingEdgeStateTransfer(d):
    print    "    // spread refine/coarsening information"

    mask = 7   # 111

    if d.hyp == 'new':
        print    "    comm_"+d.hyp_stack+"_edge_stack->push((outgoingEdgeStateInformation >> 2) & 1);"

    if d.hyp != 'old':
        mask &= 3   # 011

    # setup communication data for coarsening
    if d.edge_left == 'new' or d.edge_right == 'new':
        print    "    // spread coarsening information"
        print   "    if (new_state == 2)"
        print   "    {"

        if d.edge_left == 'new':
            print    "        comm_"+d.edge_stack+"_edge_stack->push(2);"
        if d.edge_right == 'new':
            print    "        comm_"+d.edge_stack+"_edge_stack->push(2);"

        print   "    }"
        print   "    else"
        print   "    {"

        if d.edge_left == 'new':
            print    "        comm_"+d.edge_stack+"_edge_stack->push(0);"
        if d.edge_right == 'new':
            print    "        comm_"+d.edge_stack+"_edge_stack->push(0);"

        print   "    }"

    if d.edge_right != 'old':
        mask &= 5     # 101
    if d.edge_left != 'old':
        mask &= 6     # 110

    print   "    outgoingEdgeStateInformation &= "+str(mask)+";"
    print   ""
    
    # did we spread all the refinement information?
    print   "    if (outgoingEdgeStateInformation != 0)"
    print   "        repeat_traversal = true;"
    print   ""

    # create new state combined with further requested refinement informations"
    print   "    new_state |= (outgoingEdgeStateInformation << 4);"
    print   "    adaptivity_state_flag_stack_out->push(new_state);"
