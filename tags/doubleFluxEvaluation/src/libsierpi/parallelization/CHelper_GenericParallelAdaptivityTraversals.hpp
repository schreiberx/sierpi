/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelAdaptivityTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELADAPTIVITYTRAVERSALS_HPP_

#include <stdint.h>
#include "libsierpi/parallelization/CGenericTreeNode.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"

/**
 * \brief this class implements helper methods which abstract the different phases for adaptive traversals
 *
 * in particular:
 *  a) first forward traversal
 *  b) finished?
 *    c) edge communication
 *    d) backward traversal
 *    e) forward traversal
 *    f) finished? not->goto (b)
 *  g) last traversal
 */
template<
	typename CPartition_TreeNode_,
	typename CSimulation_Parallel_SubPartitionHandler,
	typename TAdaptiveSimulationSubClass,
	typename CStackAccessors
>
class CGenericParallelAdaptivityTraversals
{
	typedef CPartition_ExchangeEdgeCommData<CPartition_TreeNode_, char, CStackAccessors> CPartition_ExchangeEdgeCommData_;


public:
	/**
	 * run the adaptive method
	 */
	unsigned long long action(
			TAdaptiveSimulationSubClass CSimulation_Parallel_SubPartitionHandler::*p_simulationSubClass,
			CPartition_ExchangeEdgeCommData_ CSimulation_Parallel_SubPartitionHandler::*p_simulationEdgeCommSubClass,
			CGenericTreeNode<CPartition_TreeNode_> *p_rootNode,

			unsigned long long i_partition_split_workload_size,
			unsigned long long i_partition_join_workload_size
	)
	{
		/**
		 * if this variable is set to true, there are still some hanging nodes to be deleted.
		 */
		bool remaining_hanging_node;

		/****************************************************
		 * FIRST PASS
		 ****************************************************/
		/**
		 * the first pass is executed to mark the corresponding triangles for begin split or coarsened.
		 * also edge refinement/coarsen information is pushed/pulled to the stack.
		 *
		 * finally, the data for the adjacent partitions is stored to the stacks and need to be exchanged.
		 */

		/**
		 * the first pass checks, whether a triangle should be refined or coarsened.
		 * therefore the element data is necessary. After this step, the elementdoTraversal_SimulationHandler_SubClass_Method_Reduce
		 * data has not to be touched in the middle passes.
		 */
		// first pass: setting all edges to type 'new' creates data to exchange with neighbors
		remaining_hanging_node = p_rootNode->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_TreeNode_ *node) -> bool
				{
					return (node->cSimulation_SubPartitionHandler->*(p_simulationSubClass)).actionFirstTraversal(node->cStacks);
				},
			&(CReduceOperators::OR<bool>)
		);

		/****************************************************
		 * MIDDLE PASSES
		 ****************************************************/
		/**
		 * this is the loop over all partitions to avoid all hanging nodes
		 *
		 * there's already some data prepared on the stack from the first pass.
		 */
		while (remaining_hanging_node)
		{
			/*
			 * WARNING: moving pullEdgeCommData() to the next parallel reduce  traversal does not work!!!
			 *
			 * Since both middle traversals (forward/backward) write to the edgeComm but
			 * _ALSO_ the exchangeEdgeComm stacks, pulling the edge comm data within the next traversal is not allowed!
			 */

			// first pass: setting all edges to type 'new' creates data to exchange with neighbors
			p_rootNode->traverse_SimulationPartitionHandler_Parallel(
					[=](CSimulation_Parallel_SubPartitionHandler *worker){
						(worker->*p_simulationEdgeCommSubClass).pullEdgeCommData();
					}
			);


			/**
			 * The second traversal actually exists of 2 passes:
			 *
			 * 1) The first BACKWARD TRAVERSAL reads data from the adjacent partitions.
			 *    all partition borders are set t type 'old'
			 *
			 * 2) The second FORWARD TRAVERSAL spreads data to adjacent partitions.
			 *    if callback_action_Adaptive_Tsunami_SecondPass returns true, there are
			 *    still refinements to do and we rerun this loop.
			 *    then all partition borders are set to type 'new'
			 */
			remaining_hanging_node = p_rootNode->traverse_PartitionTreeNode_Reduce_Parallel(
					[=](CPartition_TreeNode_ *node) -> bool
					{
						return (node->cSimulation_SubPartitionHandler->*(p_simulationSubClass)).actionMiddleTraversals_Parallel(node->cStacks);
					},
				&(CReduceOperators::OR<bool>)
			);
		}

		/****************************************************
		 * LAST PASS
		 ****************************************************/
		/**
		 * after we have the states to create a regular grid, the state-refinement information is stored on the stack.
		 *
		 * !!! this information is used later on to change the information about the adjacent partitions !!!
		 *
		 * this traversal do the actual refinement
		 */
		unsigned long long number_of_triangles = p_rootNode->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_TreeNode_ *node) -> unsigned long long
				{
					CSimulation_Parallel_SubPartitionHandler *worker = node->cSimulation_SubPartitionHandler;
					TAdaptiveSimulationSubClass &adaptiveSubClass = (worker->*(p_simulationSubClass));
					CPartition_ExchangeEdgeCommData_ &simulationEdgeCommSubClass = (worker->*(p_simulationEdgeCommSubClass));

					/**
					 * after knowing that there are no more hanging nodes, we
					 * 1) drop the communication stack
					 *
					 * 2) create a new communication stack marking all refined edges.
					 *    this is done by using a specialized version of the last traversal
					 *
					 * 3) the last step is to analyze the edge communication stack to figure
					 *    out changes in the communication stacks
					 */

					/*
					 * 1)
					 * instead of using a communication stack which is reading the input from
					 * the adjacent partition, this traversal outputs the adaptivity information.
					 */
					// 2)
					adaptiveSubClass.actionLastTraversal_Parallel(
							node->cStacks,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation
					);

					/*
					 * TAG FOR SPLIT OR JOIN OPERATIONS!!!
					 */
					unsigned long long number_of_triangles = node->cStacks->element_data_stacks.forward.getNumberOfElementsOnStack();

					if (i_partition_split_workload_size != 0)
					{
						if (number_of_triangles >= i_partition_split_workload_size)
						{
							assert(i_partition_split_workload_size >= 2);
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::SPLIT;
						}
						else if (number_of_triangles < i_partition_join_workload_size)
						{
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::JOIN;
						}
						else
						{
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation.splitJoinRequests = CPartition_SplitJoinInformation_Enums::NO_OPERATION;
						}
					}

					// 3) update the information about the edge communication
					simulationEdgeCommSubClass.updateEdgeCommSizeAndSplitJoinInformation(
							&node->cStacks->adaptive_comm_left_edge_stack,
							&node->cStacks->adaptive_comm_right_edge_stack,
							worker->cPartition_TreeNode->cPartition_AdaptiveSplitJoinInformation,
							worker->cPartition_TreeNode->cPartition_SplitJoinInformation
						);

					// clear adaptivity modification stacks
					node->cStacks->adaptive_comm_left_edge_stack.clear();
					node->cStacks->adaptive_comm_right_edge_stack.clear();

#if USE_TASK_PRIORITIES
					node->cGeneric_TreeNode->task_priority_hint = number_of_triangles;
#endif
					return number_of_triangles;
			},
			&(CReduceOperators::ADD<unsigned long long>)
		);

		return number_of_triangles;
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
