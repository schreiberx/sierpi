/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CHelper_GenericParallelEdgeCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLELEDGECOMMTRAVERSALS_HPP_

#include "libsierpi/parallelization/CReduceOperators.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
template<
	typename CPartition_,
	typename CSimulation_SubPartitionHandler,
	typename TEdgeCommSimulationSubClass,
	typename TEdgeData,
	typename CStackAccessors,
	typename TReduceValue,
	typename TTimestepSize
>
class CGenericParallelEdgeCommTraversals
{
	typedef CPartition_ExchangeEdgeCommData<CPartition_, TEdgeData, CStackAccessors> CPartition_ExchangeEdgeCommData_;
	typedef typename CPartition_::CSimulationStacks_ CSimulationStacks_;

public:
	TReduceValue reduceValue;

	/**
	 * constructor
	 */
	CGenericParallelEdgeCommTraversals()	:
		reduceValue(0)
	{
	}



	/**
	 * run the edge comm traversals
	 */
	void action(
			TEdgeCommSimulationSubClass CSimulation_SubPartitionHandler::*i_simulationSubClass,
			CPartition_ExchangeEdgeCommData_ CSimulation_SubPartitionHandler::*i_simulationEdgeCommSubClass,
			CGenericTreeNode<CPartition_> *i_rootNode,
			TTimestepSize i_timestep_size
	)
	{
		// first pass: setting all edges to type 'new' creates data to exchange with neighbors
		i_rootNode->traverse_PartitionTreeNode_Parallel(
				[=](CPartition_ *node)
				{
					(node->cSimulation_SubPartitionHandler->*(i_simulationSubClass)).actionFirstPass(node->cStacks);
				}
		);

		// second pass: setting all edges to type 'old' reads data from adjacent partitions
		reduceValue = i_rootNode->traverse_PartitionTreeNode_Reduce_Parallel(
				[=](CPartition_ *node) -> TReduceValue
				{
					// firstly, pull edge communication data from adjacent sub-partitions
					(node->cSimulation_SubPartitionHandler->*(i_simulationEdgeCommSubClass)).pullEdgeCommData();

					// run computation based on newly set-up stacks
					return (node->cSimulation_SubPartitionHandler->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, i_timestep_size);
				},
				&(CReduceOperators::MIN<TReduceValue>	// use minimum since the minimum timestep has to be selected
			)
		);
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
