/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 */
/*
 * CTraversatorClassInc_SetupAdaptiveFirstPass.hpp
 *
 *  Created on: Oct 1, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


TRecursiveMethod sfcRecursiveMethod_Backward;

TRecursiveMethod sfcRecursiveMethod_Backward_1stChild;
TRecursiveMethod sfcRecursiveMethod_Backward_1stChild_1stChild;
TRecursiveMethod sfcRecursiveMethod_Backward_1stChild_2ndChild;
TRecursiveMethod sfcRecursiveMethod_Backward_2ndChild;


/**
 * the triangle factory for this traversal
 */
CTriangle_Factory triangleFactory;


/**
 * depth limiters to restrict maximum/minimum refinement
 */
int depth_limiter_min;
int depth_limiter_max;


/**
 * setup the initial partition traversal for the given factory
 */
void setup_sfcMethods(
		CTriangle_Factory &p_triangleFactory	///< triangle factory to find the first method
)
{
	triangleFactory = p_triangleFactory;

	// copy triangle factory
	CTriangle_Factory backwardTriangleFactory = triangleFactory;

	backwardTriangleFactory.traversalDirection = CTriangle_Enums::DIRECTION_BACKWARD;

	if (backwardTriangleFactory.edgeTypes.hyp == CTriangle_Enums::EDGE_TYPE_OLD)
		backwardTriangleFactory.edgeTypes.hyp = CTriangle_Enums::EDGE_TYPE_NEW;

	if (backwardTriangleFactory.edgeTypes.right == CTriangle_Enums::EDGE_TYPE_OLD)
		backwardTriangleFactory.edgeTypes.right = CTriangle_Enums::EDGE_TYPE_NEW;

	if (backwardTriangleFactory.edgeTypes.left == CTriangle_Enums::EDGE_TYPE_OLD)
		backwardTriangleFactory.edgeTypes.left = CTriangle_Enums::EDGE_TYPE_NEW;

	sfcRecursiveMethod_Backward = getSFCMethod(backwardTriangleFactory);

	CTriangle_Factory triangleFactory_1stChild, triangleFactory_2ndChild;
	backwardTriangleFactory.setupChildFactoriesWithoutVertices(triangleFactory_1stChild, triangleFactory_2ndChild);

	sfcRecursiveMethod_Backward_1stChild = getSFCMethod(triangleFactory_1stChild);
	sfcRecursiveMethod_Backward_2ndChild = getSFCMethod(triangleFactory_2ndChild);

	CTriangle_Factory triangleFactory_1stChild_1stChild, triangleFactory_1stChild_2ndChild;
	triangleFactory_1stChild.setupChildFactoriesWithoutVertices(triangleFactory_1stChild_1stChild, triangleFactory_1stChild_2ndChild);

	sfcRecursiveMethod_Backward_1stChild_1stChild = getSFCMethod(triangleFactory_1stChild_1stChild);
	sfcRecursiveMethod_Backward_1stChild_2ndChild = getSFCMethod(triangleFactory_1stChild_2ndChild);
}


/**
 * setup the parameters with the one by the parent partition
 */
void setup_Partition(
		TThisClass &i_parentTraversator,		///< information of parent traversator
		CTriangle_Factory &i_triangleFactory	///< triangle factory to find the first method
)
{
	triangleFactory = i_triangleFactory;

	// make sure that this is really a root node
	assert(triangleFactory.partitionTreeNodeType != CTriangle_Enums::NODE_ROOT_TRIANGLE);


	setup_sfcMethods(triangleFactory);

	kernelClass.setup_WithKernel(i_parentTraversator.kernelClass);
}
