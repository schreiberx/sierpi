/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */
#ifndef SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_
#define SPECIALIZED_TSUNAMI_TRAVERSATORS_CADAPTIVE_CONFORMING_GRID_NORMALS_DEPTH_DEPTHLIMITER_ELEMENT_PARALLEL_HPP_


#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/parallelization/CPartition_SplitJoinInformation.hpp"

namespace sierpi
{
namespace travs
{

/**
 * adaptive refinement
 */
class CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element
{
private:
	class CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element_Private *generic_traversator;

public:
	CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element();

	virtual ~CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element();

	/**
	 * FIRST TRAVERSAL
	 */
	bool actionFirstTraversal(CTsunamiSimulationStacks *cStacks);

	/**
	 * MIDDLE TRAVERSALS
	 */
	bool actionMiddleTraversals_Parallel(CTsunamiSimulationStacks *cStacks);
	bool actionMiddleTraversals_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Serial(CTsunamiSimulationStacks *cStacks);

	/**
	 * LAST TRAVERSAL w/o updating the split/join information
	 */
	void actionLastTraversal_Parallel(
			CTsunamiSimulationStacks *cStacks,
			CPartition_SplitJoinInformation &p_splitJoinInformation
	);

	/**
	 * setup the initial root domain partition
	 */
	void setup_sfcMethods(
			CTriangle_Factory &p_triangleFactory
		);

	/**
	 * setup parameters specific for the root traversator
	 */
	void setup_RootTraversator(
		int p_depth_limiter_min,
		int p_depth_limiter_max
	);

	/**
	 * setup some kernel parameters
	 */
	void setup_KernelClass(
			TTsunamiDataScalar i_square_side_length,

			TTsunamiDataScalar i_refine_height_threshold,
			TTsunamiDataScalar i_coarsen_height_threshold,

			TTsunamiDataScalar i_refine_slope_threshold,
			TTsunamiDataScalar i_coarsen_slope_threshold
		);

	/**
	 * setup the child partition based on it's parent traversator information and the child's factory
	 */
	void setup_Partition(
			CSpecialized_Tsunami_AdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element &parent,
			CTriangle_Factory &i_child_triangleFactory
		);

};

}
}

#endif
