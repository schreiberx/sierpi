/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef KERNEL_CADAPTIVE_TSUNAMI_0THORDER_HPP_
#define KERNEL_CADAPTIVE_TSUNAMI_0THORDER_HPP_

#include "libmath/CMath.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

#include "libsierpi/traversators/adaptive/CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element.hpp"

#include "CAdaptive_Tsunami_0thOrder_CoarsenRefine.hpp"
#include "lib/CDebugSetupDone.hpp"

namespace sierpi
{
namespace kernels
{


/**
 * adaptive refinement/coarsening for tsunami simulation of 0th order
 */
class CAdaptive_Tsunami_0thOrder	: public CAdaptive_Tsunami_0thOrder_CoarsenRefine
{
	DEBUG_SETUP_DONE_VAR
public:
	typedef sierpi::travs::CAdaptiveConformingGrid_Normals_Depth_DepthLimiter_Element<CAdaptive_Tsunami_0thOrder, CTsunamiElementData>	TRAV;
	typedef TTsunamiVertexScalar TVertexScalar;

	/*
	 * refinement/coarsening parameters
	 */
	TTsunamiDataScalar refine_height_threshold;
	TTsunamiDataScalar coarsen_height_threshold;

	TTsunamiDataScalar refine_slope_threshold;
	TTsunamiDataScalar coarsen_slope_threshold;

	// square sidelength of a cathetus
	TTsunamiDataScalar cathetus_side_length;



	CAdaptive_Tsunami_0thOrder()
	{
		DEBUG_SETUP_DONE_CONSTRUCTOR;

		refine_slope_threshold = -1;
		coarsen_slope_threshold = -1;
		cathetus_side_length = -1;
	}


	inline bool should_refine(
			int i_depth,
			CTsunamiElementData *i_elementData
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (i_elementData->refine)
		{
			i_elementData->refine = false;
			return true;
		}
#endif
		return (i_elementData->dofs.h > refine_height_threshold);
	}


	inline bool should_coarsen(
			int i_depth,
			CTsunamiElementData *i_elementData
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if SIMULATION_TSUNAMI_REFINE_AND_COARSEN_FLAGS_IN_ELEMENTDATA
		if (i_elementData->coarsen)
		{
			i_elementData->coarsen = false;
			return true;
		}
#endif
		return (i_elementData->dofs.h < coarsen_height_threshold);
	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	void setup_WithParameters(
			TTsunamiDataScalar p_cathetus_side_length,

			TTsunamiDataScalar p_refine_height_threshold,
			TTsunamiDataScalar p_coarsen_height_threshold,

			TTsunamiDataScalar p_refine_slope_threshold,
			TTsunamiDataScalar p_coarsen_slope_threshold
	)
	{
		DEBUG_SETUP_DONE_SETUP;

		refine_height_threshold = p_refine_height_threshold;
		coarsen_height_threshold = p_coarsen_height_threshold;

		refine_slope_threshold = p_refine_slope_threshold;
		coarsen_slope_threshold = p_coarsen_slope_threshold;

		cathetus_side_length = p_cathetus_side_length;
	}


	void setup_WithKernel(CAdaptive_Tsunami_0thOrder &parent)
	{
		DEBUG_SETUP_DONE_SETUP;

		refine_height_threshold = parent.refine_height_threshold;
		coarsen_height_threshold = parent.coarsen_height_threshold;

		refine_slope_threshold = parent.refine_slope_threshold;
		coarsen_slope_threshold = parent.coarsen_slope_threshold;

		cathetus_side_length = parent.cathetus_side_length;
	}
};

}
}

#endif /* CADAPTIVEREFINEMENT_HPP_ */
