/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef COUTPUTVTK_VERTICES_ELEMENT_TSUNAMI_HPP_
#define COUTPUTVTK_VERTICES_ELEMENT_TSUNAMI_HPP_

#include "libsierpi/traversators/vertices/CTraversator_Vertices_ElementData.hpp"
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"
#include "libmath/CVertex2d.hpp"

namespace sierpi
{
namespace kernels
{


#define VISUALIZATION_SIMPLE						1
#define VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS	0
#define VISUALIZATION_ALIGNED_TRIANGLES				0


template <typename p_CSimulationStacks>
class COutputVTK_Vertices_Element_Tsunami
{
public:
	typedef typename p_CSimulationStacks::TElementData	TElementData;

	typedef float TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_Vertices_ElementData<COutputVTK_Vertices_Element_Tsunami<p_CSimulationStacks>, p_CSimulationStacks > TRAV;

	std::ofstream *vtk_file_output_stream;

private:
	TVertexScalar *last_triangle;
	size_t max_triangles;

public:
	inline void elementAction(
			TVertexScalar vx1, TVertexScalar vy1,
			TVertexScalar vx2, TVertexScalar vy2,
			TVertexScalar vx3, TVertexScalar vy3,
			TElementData *i_ElementData)
	{
#define fixHeight(h)	(h)

#if VISUALIZATION_SIMPLE

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		TVertexScalar y = fixHeight(i_ElementData->dofs.h);
#else
		TVertexScalar y = fixHeight((1.0/3.0)*(i_ElementData->hyp_edge.h + i_ElementData->left_edge.h + i_ElementData->right_edge.h));
#endif

#if !COMPILE_SIMULATION_WITH_GUI

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		if (CMath::isNan(i_ElementData->dofs.h))
#else
		if (CMath::isNan(i_ElementData->hyp_edge.h + i_ElementData->right_edge.h + i_ElementData->left_edge.h))
#endif
		{
			std::cerr << "INSTABILITY (NaN) DETECTED [outputVTK]!!!" << std::endl;
			assert(false);
			exit(-1);
		}
#endif

		(*vtk_file_output_stream)
				<< vx1 << " "
				<< y << " "
				<< -vy1 << std::endl;

		(*vtk_file_output_stream)
				<< vx2 << " "
				<< y << " "
				<< -vy2 << std::endl;

		(*vtk_file_output_stream)
				<< vx3 << " "
				<< y << " "
				<< -vy3 << std::endl;

#endif

#if VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS

		TVertexScalar hyp_x = (vx1+vx2)*0.5;
		TVertexScalar hyp_y = (vy1+vy2)*0.5;

		TVertexScalar right_edge_x = (vx2+vx3)*0.5;
		TVertexScalar right_edge_y = (vy2+vy3)*0.5;

		TVertexScalar left_edge_x = (vx3+vx1)*0.5;
		TVertexScalar left_edge_y = (vy3+vy1)*0.5;

		(*vtk_file_output_stream)
				<<	hyp_x << " "
				<< fixHeight(i_ElementData->hyp_edge.h) << " "
				<< -hyp_y << std::endl;

		(*vtk_file_output_stream)
				<< left_edge_x << " "
				<< fixHeight(i_ElementData->left_edge.h) << " "
				<< -left_edge_y << std::endl;

		(*vtk_file_output_stream)
				<< right_edge_x << " "
				<< fixHeight(i_ElementData->right_edge.h) << " "
				<< -right_edge_y << std::endl;
#endif

#if VISUALIZATION_ALIGNED_TRIANGLES
		TVertexScalar hyp_edge_x = (vx1+vx2)*0.5;
		TVertexScalar hyp_edge_y = (vy1+vy2)*0.5;

		TVertexScalar right_edge_x = (vx2+vx3)*0.5;
		TVertexScalar right_edge_y = (vy2+vy3)*0.5;

		TVertexScalar left_edge_x = (vx3+vx1)*0.5;
		TVertexScalar left_edge_y = (vy3+vy1)*0.5;

		TVertexScalar hyp_dx = right_edge_x - left_edge_x;
		TVertexScalar hyp_dy = right_edge_y - left_edge_y;
		TVertexScalar hyp_dh = i_ElementData->right_edge.h - i_ElementData->left_edge.h;

		TVertexScalar left_dx = right_edge_x - hyp_edge_x;
		TVertexScalar left_dy = right_edge_y - hyp_edge_y;
		TVertexScalar left_dh = i_ElementData->right_edge.h - i_ElementData->hyp_edge.h;

		(*vtk_file_output_stream)
					<< (hyp_edge_x-hyp_dx) << " "
					<< fixHeight(i_ElementData->hyp_edge.h-hyp_dh) << " "
					<< -(hyp_edge_y-hyp_dy) << std::endl;

		(*vtk_file_output_stream)
					<< (hyp_edge_x+hyp_dx) << " "
					<< fixHeight(i_ElementData->hyp_edge.h+hyp_dh) << " "
					<< -(hyp_edge_y+hyp_dy) << std::endl;

		(*vtk_file_output_stream)
					<< (left_edge_x+left_dx) << " "
					<< fixHeight(i_ElementData->left_edge.h+left_dh) << " "
					<< -(left_edge_y+left_dy) << std::endl;
#endif
	}
#undef fixHeight

	inline void traversal_pre_hook()
	{
	}

	inline void traversal_post_hook()
	{
	}

	inline void setup(std::ofstream *p_vtk_file_output_stream)
	{
		vtk_file_output_stream = p_vtk_file_output_stream;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputVTK_Vertices_Element_Tsunami &parent_kernel
	)
	{
	}


	inline void setup_WithParameters(
			std::ofstream *p_vtk_file_output_stream
		)
	{
	}
};


#undef VISUALIZATION_SIMPLE
#undef VISUALIZATION_TRIANGLES_AT_EDGE_MIDPOINTS
#undef VISUALIZATION_ALIGNED_TRIANGLES

}
}

#endif
