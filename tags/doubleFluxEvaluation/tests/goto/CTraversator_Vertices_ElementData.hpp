/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef CTRAVERSATOR_VERTICES_ELEMENTDATA_HPP_
#define CTRAVERSATOR_VERTICES_ELEMENTDATA_HPP_

#include "libsierpi/stacks/CFBStacks.hpp"
#include "libsierpi/parallelization/CTriangle_Factory.hpp"

namespace sierpi
{
namespace travs
{

template <class CKernelClass>
class CTraversator_Vertices_ElementData
{
public:
	/**
	 * implementation of kernel class
	 */
	CKernelClass kernelClass;

	/**
	 * a few typedefs which could be utilized by the traversator to get more informations
	 */
	typedef typename CKernelClass::TElementData TElementData;
	typedef typename CKernelClass::TVertexScalar TVertexScalar;
	typedef CTraversator_Vertices_ElementData<CKernelClass> TThisClass;

	/**
	 * stack and lifo handlers used in this traversator
	 */
	CStackReaderTopDown<char> structure_lifo_in;
	CStackReaderTopDown<TElementData> element_data_lifo;

	/**
	 * include automagically generated code
	 */
#define SIERPI_PARALLEL_CODE	1
	#include "auto/CTraversator_Vertices_ElementData_autogenerated.hpp"
#undef SIERPI_PARALLEL_CODE
#include "CTraversatorClassInc_SetupSinglePass.hpp"

	virtual ~CTraversator_Vertices_ElementData()
	{
		if (	triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_ROOT_TRIANGLE	||
				triangleFactory.partitionTreeNodeType == CTriangle_Enums::NODE_UNKNOWN		//< also free the recursion table for the invalid case since
				)
			shutdownRecursionTable();
	}

	CFBStacks<char> *setup_structure_stacks;
	CFBStacks<TElementData> *setup_element_data_stacks;

	void setup_Stacks(
			CFBStacks<char> *i_setup_structure_stacks,
			CFBStacks<TElementData> *i_setup_element_data_stacks
		)
	{
		setup_structure_stacks = i_setup_structure_stacks;
		setup_element_data_stacks = i_setup_element_data_stacks;
	}



	/**
	 * do the traversal!
	 */
	void action()
	{
		assert(setup_structure_stacks->direction == setup_element_data_stacks->direction);
		assert(setup_structure_stacks->direction == CFBStacks<char>::FORWARD);

		structure_lifo_in.setup(setup_structure_stacks->forward);
		element_data_lifo.setup(setup_element_data_stacks->forward);

		kernelClass.traversal_pre_hook();

		(this->*recursionTable[recursionMethodForwardPassId].parent_method)(
				triangleFactory.vertices[0][0], triangleFactory.vertices[0][1],
				triangleFactory.vertices[1][0], triangleFactory.vertices[1][1],
				triangleFactory.vertices[2][0], triangleFactory.vertices[2][1]
			);

		kernelClass.traversal_post_hook();

		assert(element_data_lifo.isEmpty());
		assert(structure_lifo_in.isEmpty());
	}
};

}
}

#endif /* CSTRUCTURE_VERTEXDATA_H_ */
