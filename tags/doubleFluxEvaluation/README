Copyright (C) 2011 Technische Universitaet Muenchen

This file is part of the Sierpi project. For conditions of distribution and
use, please see the copyright notice in the file 'copyright.txt' at the root
directory of this package and the copyright notice at
http://www5.in.tum.de/sierpi

--------------------------------------------------------------------------------------------

This file contains some build & execution information about the sierpi
framework.



==================
 PREREQUESITES
==================

GNU g++ compiler version 4.6.1:
  * Delivered in your linux distribution or available via http://gcc.gnu.org/
  * The g++ executable has to be accessible via the PATH environment variable

scons:
  * For compilation (replacement for `make`)
  * Website http://www.scons.org/

libxml2 (To read domains via xml files):
  * Available via the Ubuntu package libxml2-dev

iOMP (Invasive OpenMP):
  * For more information, have a look at https://invasic.informatik.uni-erlangen.de,
    D3 sub-project.
  * In case that you like to use iOMP, install all related libraries
    libraries into either $HOME/local/lib and $HOME/local/include or
    into the system-wide lib+include directories.
  * iomp eclipse project is expected to be installed in the same workspace as
    sierpi and compiled in Release mode! The output directory should
    be the default eclipse Release build subdirectory
    (e. g. $HOME/workspace/iomp/Release)!
  * Visualization is not available for iOMP so far
  * Compilation: `make iomp`


For visualization (Optional feature):
  * mercurial `hg`
    * To checkout the bleeding edge version of libSDL 1.3, mercurial is needed
    * http://mercurial.selenic.com/
    * This software is also available via the `mercurial` package in many
      linux distributions.

  * libSDL 1.3 (For visualization):
    * Note: Sierpi was so far not tested to run on an AMD graphic card.
    * Recent repository 1.3 version of libSDL (http://www.libsdl.org/)
    * Checkout via 'hg clone http://hg.libsdl.org/SDL' & compile

  * libSDL-image:
    * Available at http://www.libsdl.org/projects/SDL_image/
    * This software is also available via the libsdl-image1.2-dev package
      in many Linux systems.



==================
 COMPILATION without GUI
==================

Sierpi uses the scons construction toolkit to control the compilation
of the framework. You can find scons here: http://www.scons.org/

IMPORTANT: Set the environment variables (PATH and LD_LIBRARY_PATH) to
point to the appropriate directories!

There are different compilation options available.
Type 'scons --help' to get more information about the local options.

E. g. to compile with threading enabled, using the intel compiler, without
a gui and in release mode, type

	scons -j 8 --threading=omp --cppcompiler=intel --gui=off --mode=release

The option '-j 8' enables compilation with 8 threads.

For compilation with gnu compiler, type

	scons -j 8 --threading=omp --cppcompiler=gnu --gui=off --mode=release

The executable is created in ./build prefixed with sierpi_ and extended by
the comiple options specified before. E. g. for our compilation above, the
executable ./build/sierpi_icc_omp_release would be created.

There is still a Makefile left for convenience.
Use 'make gcc' to sierpi with different options using the gnu compiler.
Calling 'make icc' also compiles with different options but uses the intel
compiler. Type 'make' to create the executables for both compilers.


========================
 EXECUTION without GUI
========================

Sierpi executables without gui can be executed from an arbitrary directory.


========================
 COMPILATION with GUI
========================

Use option --gui=on to enable gui.
After appropriate installation of libSDL, `sdl-config --version` should
output "1.3.0" to the console

	scons -j 8 --threading=omp --cppcompiler=gnu --gui=on --mode=release

========================
 EXECUTION with GUI
========================

When compiled with '--gui=on', the execution has to be triggered in the sierpi
root directory by typing './build/sierpi_...' to know the path for shaders.

