/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Feb 14, 2011
 *      Author: schreibm
 */

#ifndef CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_

#define CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_PACKED_FLUX	1

#include <smmintrin.h>
#include <xmmintrin.h>
#include "lib/CSSEIntrinsicHelpers.hpp"

#include "libmath/CMath.hpp"
#include "../../sierpi/src/libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"
#include "../../sierpi/src/simulations/tsunami_common/CTsunamiTypes.hpp"
#include "../../sierpi/src/simulations/tsunami_common/EBoundaryConditions.hpp"
#include "../../sierpi/src/libmath/CVertex2d.hpp"
#include "../../sierpi/src/lib/CDebugSetupDone.hpp"
#include "../../sierpi/src/libsierpi/parallelization/CTriangle_Factory.hpp"
#include "../../sierpi/src/config.h"
#include <vector>



namespace sierpi
{
namespace kernels
{

class CEdgeComm_Tsunami_1stOrder_config
{
public:
	TTsunamiDataScalar timestep_size;
	TTsunamiDataScalar square_side_length;
	TTsunamiDataScalar gravitational_constant;

	CTsunamiEdgeData boundary_dirichlet;
};


template <int EBoundaryConditionValue>
class CEdgeComm_Tsunami_1stOrder
{
	DEBUG_SETUP_DONE_VAR

public:
	typedef TTsunamiDataScalar TVertexScalar;
	typedef CVertex2d<TTsunamiDataScalar> TVertexType;
	typedef CTsunamiElementData TElementData;
	typedef CTsunamiEdgeData TEdgeData;

//	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> > TRAV;
//	typedef sierpi::travs::CEdgeComm_Normals_Depth<CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> > TRAV_PARALLEL;

	// convenient typedef
	typedef TTsunamiDataScalar T;

	CEdgeComm_Tsunami_1stOrder_config config;

	int maxElements;


	/**
	 * this class offers simd computations for everything after flux computation
	 */
	class CPackedElementComputations
	{
	public:
		CTsunamiElementData *elementData[4];

		SSEALIGN16 T hyp_edge_flux[3][4];		// h, qx, qy
		SSEALIGN16 T right_edge_flux[3][4];	// h, qx, qy
		SSEALIGN16 T left_edge_flux[3][4];		// h, qx, qy

		SSEALIGN16 T left_normal_x[4];
		SSEALIGN16 T left_normal_y[4];

		SSEALIGN16 T cathetus_unit_length[4];
		SSEALIGN16 T hypotenuse_unit_length[4];

//		SSEALIGN16 int depth[4];

		const CEdgeComm_Tsunami_1stOrder_config *config;

		int computedFluxesCounter;
		int elementDataCounter;


		/**
		 *
		 */
		inline CPackedElementComputations()	:
			computedFluxesCounter(0),
			elementDataCounter(0)
		{
		}


		/**
		 *
		 */
		inline void setup(
				const CEdgeComm_Tsunami_1stOrder_config *p_config
		)
		{
			config = p_config;
		}


		/**
		 * insert a new element data to element computations and increase the finished counter
		 *
		 * return number of bucket.
		 */
		inline bool insert(
				CTsunamiElementData *i_elementData,
				T i_left_normal_x,
				T i_left_normal_y,
				int i_depth
			)
		{
			assert(elementDataCounter < 4);

			elementData[elementDataCounter] = i_elementData;
			left_normal_x[elementDataCounter] = i_left_normal_x;
			left_normal_y[elementDataCounter] = i_left_normal_y;
			cathetus_unit_length[elementDataCounter] = getUnitCathetusLengthForDepth(i_depth);
			hypotenuse_unit_length[elementDataCounter] = getUnitHypotenuseLengthForDepth(i_depth);

			elementDataCounter++;

			return elementDataCounter == 4;
		}


		/**
		 *
		 */
		void flush()
		{
			std::cout << "flush: elementData" << std::endl;
			assert(computedFluxesCounter == elementDataCounter*3);

			computeSIMD();
		}

		void outputm128(__m128 data)
		{
			float ldata[4];
			_mm_storeu_ps(ldata, data);

			std::cout << ldata[0] << " " << ldata[1] << " " << ldata[2] << " " << ldata[3] << std::endl;
		}


		void outputm128i(__m128i data)
		{
			int ldata[4];
			_mm_storeu_si128((__m128i*)ldata, data);

			std::cout << ldata[0] << " " << ldata[1] << " " << ldata[2] << " " << ldata[3] << std::endl;
		}


		/**
		 *
		 */
		void compute()
		{
			if (elementDataCounter == 4)
			{
				computeSIMD();
				return;
			}

			std::cout << ">>> " << elementDataCounter << std::endl;
			for (int i = 0; i < elementDataCounter; i++)
			{
				T cat_length = cathetus_unit_length[i]*config->square_side_length;
				T hyp_length = hypotenuse_unit_length[i]*config->square_side_length;

#if 0
//				TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config->delta_timestep/cathetus_length, (T)0.99);
				TTsunamiDataScalar damping = (T)1.0-(config->timestep_size/cat_length);

				elementData->hyp_edge.qx *= damping;
				elementData->hyp_edge.qy *= damping;
				elementData->left_edge.qx *= damping;
				elementData->left_edge.qy *= damping;
				elementData->right_edge.qx *= damping;
				elementData->right_edge.qy *= damping;
#endif


				T six_div_h2 = (T)6.0/(cat_length*cat_length);
				T timestep_six_div_h2 = config->timestep_size*six_div_h2;
				T two_div_h = (T)2.0/cat_length;
				T h_div_3 = cat_length/(T)3.0;

				T inv_left_edge_h = (T)1.0/elementData[i]->left_edge.h;
				T inv_right_edge_h = (T)1.0/elementData[i]->right_edge.h;
				T inv_hyp_edge_h = (T)1.0/elementData[i]->hyp_edge.h;

				T elementData_sum_edge_qx = elementData[i]->left_edge.qx + elementData[i]->hyp_edge.qx + elementData[i]->right_edge.qx;
				T elementData_sum_edge_qy = elementData[i]->left_edge.qy + elementData[i]->hyp_edge.qy + elementData[i]->right_edge.qy;

				T triangle_area = cat_length*cat_length*(T)0.5;

				T elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qx*elementData[i]->hyp_edge.qx*inv_hyp_edge_h;
				T elementData_right_edge_qx2_MUL_inv_right_edge_h = elementData[i]->right_edge.qx*elementData[i]->right_edge.qx*inv_right_edge_h;
				T elementData_left_edge_qx2_MUL_inv_left_edge_h = elementData[i]->left_edge.qx*elementData[i]->left_edge.qx*inv_left_edge_h;

				T sum_elementData_edges_qx2_MUL_inv_edges_h = elementData_left_edge_qx2_MUL_inv_left_edge_h + elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h + elementData_right_edge_qx2_MUL_inv_right_edge_h;

				T elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qy*elementData[i]->hyp_edge.qy*inv_hyp_edge_h;
				T elementData_right_edge_qy2_MUL_inv_right_edge_h = elementData[i]->right_edge.qy*elementData[i]->right_edge.qy*inv_right_edge_h;
				T elementData_left_edge_qy2_MUL_inv_left_edge_h = elementData[i]->left_edge.qy*elementData[i]->left_edge.qy*inv_left_edge_h;

				T sum_elementData_edges_qy2_MUL_inv_edges_h = elementData_left_edge_qy2_MUL_inv_left_edge_h + elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h + elementData_right_edge_qy2_MUL_inv_right_edge_h;

				T elementData_hyp_edge_h2 = elementData[i]->hyp_edge.h*elementData[i]->hyp_edge.h;
				T elementData_right_edge_h2 = elementData[i]->right_edge.h*elementData[i]->right_edge.h;
				T elementData_left_edge_h2 = elementData[i]->left_edge.h*elementData[i]->left_edge.h;

				T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;


				T elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qx*elementData[i]->hyp_edge.qy*inv_hyp_edge_h;
				T elementData_right_edge_qx_qy_MUL_inv_right_edge_h = elementData[i]->right_edge.qx*elementData[i]->right_edge.qy*inv_right_edge_h;
				T elementData_left_edge_qx_qy_MUL_inv_left_edge_h = elementData[i]->left_edge.qx*elementData[i]->left_edge.qy*inv_left_edge_h;

				T sum_elementData_edges_qx_qy_MUL_inv_edges_h = elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h + elementData_right_edge_qx_qy_MUL_inv_right_edge_h + elementData_left_edge_qx_qy_MUL_inv_left_edge_h;

				T triangle_area_MUL_two_div_h_MUL_1_3 = (1.0/3.0)*triangle_area*two_div_h;

				T gravity_potential = (T)0.5*config->gravitational_constant*sum_elementData_edges_h2;

				T hyp_edge_h = timestep_six_div_h2*(
						h_div_3*(elementData_sum_edge_qx + elementData_sum_edge_qy)
						- hyp_length*hyp_edge_flux[0][i]
						);

				T right_edge_h = timestep_six_div_h2*(
						-h_div_3*elementData_sum_edge_qx
						- cat_length*right_edge_flux[0][i]
						);

				T left_edge_h = timestep_six_div_h2*(
						-h_div_3*elementData_sum_edge_qy
						- cat_length*left_edge_flux[0][i]
						);

				T hyp_edge_qx =
						timestep_six_div_h2*
						(
								(T)triangle_area_MUL_two_div_h_MUL_1_3*
								(
									sum_elementData_edges_qx2_MUL_inv_edges_h +
									gravity_potential +
									sum_elementData_edges_qx_qy_MUL_inv_edges_h
								)
								- hyp_length*hyp_edge_flux[1][i]
						);

				T right_edge_qx =
						timestep_six_div_h2*(
								(T)triangle_area_MUL_two_div_h_MUL_1_3*(
										- gravity_potential
										- sum_elementData_edges_qx2_MUL_inv_edges_h
								)
								- cat_length*right_edge_flux[1][i]
						);

				T left_edge_qx =
						+ timestep_six_div_h2*(
								(T)triangle_area_MUL_two_div_h_MUL_1_3*(
									-sum_elementData_edges_qx_qy_MUL_inv_edges_h
								)
								- cat_length*left_edge_flux[1][i]
						);

				T hyp_edge_qy =
						timestep_six_div_h2*(
								triangle_area_MUL_two_div_h_MUL_1_3*(
									sum_elementData_edges_qx_qy_MUL_inv_edges_h +
									sum_elementData_edges_qy2_MUL_inv_edges_h +
									gravity_potential
								)
								- hyp_length*hyp_edge_flux[2][i]
						);

				T right_edge_qy =
						timestep_six_div_h2*(
								- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_qx_qy_MUL_inv_edges_h
								- cat_length*right_edge_flux[2][i]
						);

				T left_edge_qy =
						timestep_six_div_h2*(
								-triangle_area_MUL_two_div_h_MUL_1_3*(
										sum_elementData_edges_qy2_MUL_inv_edges_h +
										gravity_potential
								)
								- cat_length*left_edge_flux[2][i]
						);

				elementData[i]->left_edge.h += left_edge_h;
				elementData[i]->left_edge.qx += left_edge_qx;
				elementData[i]->left_edge.qy += left_edge_qy;

				elementData[i]->hyp_edge.h += hyp_edge_h;
				elementData[i]->hyp_edge.qx += hyp_edge_qx;
				elementData[i]->hyp_edge.qy += hyp_edge_qy;

				elementData[i]->right_edge.h += right_edge_h;
				elementData[i]->right_edge.qx += right_edge_qx;
				elementData[i]->right_edge.qy += right_edge_qy;

				T tmp;
		#define unrotate(qx, qy)									\
				tmp = (-left_normal_y[i])*qx + (-left_normal_x[i])*qy;	\
				qy = (left_normal_x[i])*qx + (-left_normal_y[i])*qy;		\
				qx = tmp;

				unrotate(elementData[i]->hyp_edge.qx, elementData[i]->hyp_edge.qy)
				unrotate(elementData[i]->left_edge.qx, elementData[i]->left_edge.qy)
				unrotate(elementData[i]->right_edge.qx, elementData[i]->right_edge.qy)
		#undef unrotate
			}

			// TODO: reset
			computedFluxesCounter = 0;
			elementDataCounter = 0;
		}

		/**
		 *
		 */
		void computeSIMD()
		{
			const __m128 m_square_side_length = _mm_set1_ps(config->square_side_length);
			const __m128 m_delta_timestep = _mm_set1_ps(config->timestep_size);
			const __m128 m_gravity = _mm_set1_ps(config->gravitational_constant);

			/*
			T cat_length = cathetus_unit_length[i]*config->square_side_length;
			*/
			__m128 m_cat_length =
					_mm_mul_ps(
							_mm_load_ps(cathetus_unit_length),
							m_square_side_length
						);

			__m128 m_cat_length2 = _mm_mul_ps(m_cat_length, m_cat_length);

			/*
			T hyp_length = hypotenuse_unit_length[i]*config->square_side_length;
			*/
			__m128 m_hyp_length =
					_mm_mul_ps(
							_mm_load_ps(hypotenuse_unit_length),
							m_square_side_length
						);

			/*
			T six_div_h2 = (T)6.0/(cat_length*cat_length);
			*/
			__m128 m_six_div_h2 =
					_mm_div_ps(_mm_set1_ps(6.0f),
						_mm_mul_ps(m_cat_length, m_cat_length)
					);

			/*
			T timestep_six_div_h2 = config->delta_timestep*six_div_h2;
			*/
			__m128 m_timestep_six_div_h2 = _mm_mul_ps(m_delta_timestep, m_six_div_h2);

			/*
			T two_div_h = (T)2.0/cat_length;
			*/
			__m128 m_two_div_h = _mm_div_ps(_mm_set1_ps(2.0f), m_cat_length);

			/*
			T h_div_3 = cat_length/(T)3.0;
			*/
			__m128 m_h_div_3 = _mm_div_ps(m_cat_length, _mm_set1_ps(3.0f));


			/*
			T inv_hyp_edge_h = (T)1.0/elementData[i]->hyp_edge.h;
			T inv_right_edge_h = (T)1.0/elementData[i]->right_edge.h;
			T inv_left_edge_h = (T)1.0/elementData[i]->left_edge.h;
			*/
			__m128 m_elementData_hyp_edge_h = _mm_set_ps(elementData[3]->hyp_edge.h, elementData[2]->hyp_edge.h, elementData[1]->hyp_edge.h, elementData[0]->hyp_edge.h);
			__m128 m_elementData_right_edge_h = _mm_set_ps(elementData[3]->right_edge.h, elementData[2]->right_edge.h, elementData[1]->right_edge.h, elementData[0]->right_edge.h);
			__m128 m_elementData_left_edge_h = _mm_set_ps(elementData[3]->left_edge.h, elementData[2]->left_edge.h, elementData[1]->left_edge.h, elementData[0]->left_edge.h);
			__m128 m_inv_hyp_edge_h = _mm_div_ps(_mm_set1_ps(1.0f), m_elementData_hyp_edge_h);
			__m128 m_inv_right_edge_h = _mm_div_ps(_mm_set1_ps(1.0f), m_elementData_right_edge_h);
			__m128 m_inv_left_edge_h = _mm_div_ps(_mm_set1_ps(1.0f), m_elementData_left_edge_h);

			/*
			T m_elementData_sum_edge_qx = element->left_edge.qx + element->hyp_edge.qx + element->right_edge.qx;
			T m_elementData_sum_edge_qy = element->left_edge.qy + element->hyp_edge.qy + element->right_edge.qy;
			*/
			// qx
			__m128 m_elementData_right_edge_qx = _mm_set_ps(elementData[3]->right_edge.qx, elementData[2]->right_edge.qx, elementData[1]->right_edge.qx, elementData[0]->right_edge.qx);
			__m128 m_elementData_hyp_edge_qx = _mm_set_ps(elementData[3]->hyp_edge.qx, elementData[2]->hyp_edge.qx, elementData[1]->hyp_edge.qx, elementData[0]->hyp_edge.qx);
			__m128 m_elementData_left_edge_qx = _mm_set_ps(elementData[3]->left_edge.qx, elementData[2]->left_edge.qx, elementData[1]->left_edge.qx, elementData[0]->left_edge.qx);

			// qy
			__m128 m_elementData_right_edge_qy = _mm_set_ps(elementData[3]->right_edge.qy, elementData[2]->right_edge.qy, elementData[1]->right_edge.qy, elementData[0]->right_edge.qy);
			__m128 m_elementData_hyp_edge_qy = _mm_set_ps(elementData[3]->hyp_edge.qy, elementData[2]->hyp_edge.qy, elementData[1]->hyp_edge.qy, elementData[0]->hyp_edge.qy);
			__m128 m_elementData_left_edge_qy = _mm_set_ps(elementData[3]->left_edge.qy, elementData[2]->left_edge.qy, elementData[1]->left_edge.qy, elementData[0]->left_edge.qy);

			__m128 m_elementData_sum_edge_qx = _mm_add_ps(m_elementData_hyp_edge_qx, _mm_add_ps(m_elementData_right_edge_qx, m_elementData_left_edge_qx));
			__m128 m_elementData_sum_edge_qy = _mm_add_ps(m_elementData_hyp_edge_qy, _mm_add_ps(m_elementData_right_edge_qy, m_elementData_left_edge_qy));

			/*
			T triangle_area = cat_length*cat_length*(T)0.5;
			 */
			__m128 m_triangle_area = _mm_mul_ps(m_cat_length2, _mm_set1_ps(0.5f));


			/*
			T elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = elementData->hyp_edge.qx*elementData->hyp_edge.qx*inv_hyp_edge_h;
			T elementData_right_edge_qx2_MUL_inv_right_edge_h = elementData->right_edge.qx*elementData->right_edge.qx*inv_right_edge_h;
			T elementData_left_edge_qx2_MUL_inv_left_edge_h = elementData->left_edge.qx*elementData->left_edge.qx*inv_left_edge_h;
			*/
			__m128 m_elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_hyp_edge_qx, m_elementData_hyp_edge_qx), m_inv_hyp_edge_h);
			__m128 m_elementData_right_edge_qx2_MUL_inv_right_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_right_edge_qx, m_elementData_right_edge_qx), m_inv_right_edge_h);
			__m128 m_elementData_left_edge_qx2_MUL_inv_left_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_left_edge_qx, m_elementData_left_edge_qx), m_inv_left_edge_h);

			/*
			T sum_elementData_edges_qx2_MUL_inv_edges_h = elementData_left_edge_qx2_MUL_inv_left_edge_h + elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h + elementData_right_edge_qx2_MUL_inv_right_edge_h;
			*/
			__m128 m_sum_elementData_edges_qx2_MUL_inv_edges_h = _mm_add_ps(_mm_add_ps(m_elementData_left_edge_qx2_MUL_inv_left_edge_h, m_elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h), m_elementData_right_edge_qx2_MUL_inv_right_edge_h);

			/*
			T elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = elementData->hyp_edge.qy*elementData->hyp_edge.qy*inv_hyp_edge_h;
			T elementData_right_edge_qy2_MUL_inv_right_edge_h = elementData->right_edge.qy*elementData->right_edge.qy*inv_right_edge_h;
			T elementData_left_edge_qy2_MUL_inv_left_edge_h = elementData->left_edge.qy*elementData->left_edge.qy*inv_left_edge_h;
			*/
			__m128 m_elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_hyp_edge_qy, m_elementData_hyp_edge_qy), m_inv_hyp_edge_h);
			__m128 m_elementData_right_edge_qy2_MUL_inv_right_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_right_edge_qy, m_elementData_right_edge_qy), m_inv_right_edge_h);
			__m128 m_elementData_left_edge_qy2_MUL_inv_left_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_left_edge_qy, m_elementData_left_edge_qy), m_inv_left_edge_h);

			/*
			T sum_elementData_edges_qy2_MUL_inv_edges_h = elementData_left_edge_qy2_MUL_inv_left_edge_h + elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h + elementData_right_edge_qy2_MUL_inv_right_edge_h;
			*/
			__m128 m_sum_elementData_edges_qy2_MUL_inv_edges_h = _mm_add_ps(_mm_add_ps(m_elementData_left_edge_qy2_MUL_inv_left_edge_h, m_elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h), m_elementData_right_edge_qy2_MUL_inv_right_edge_h);

			/*
			T elementData_hyp_edge_h2 = elementData->hyp_edge.h*elementData->hyp_edge.h;
			T elementData_right_edge_h2 = elementData->right_edge.h*elementData->right_edge.h;
			T elementData_left_edge_h2 = elementData->left_edge.h*elementData->left_edge.h;
			*/
			__m128 m_elementData_hyp_edge_h2 = _mm_mul_ps(m_elementData_hyp_edge_h, m_elementData_hyp_edge_h);
			__m128 m_elementData_right_edge_h2 = _mm_mul_ps(m_elementData_right_edge_h, m_elementData_right_edge_h);
			__m128 m_elementData_left_edge_h2 = _mm_mul_ps(m_elementData_left_edge_h, m_elementData_left_edge_h);

			/*
			T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;
			*/
			__m128 m_sum_elementData_edges_h2 = _mm_add_ps(_mm_add_ps(m_elementData_hyp_edge_h2, m_elementData_right_edge_h2), m_elementData_left_edge_h2);


			/*
			T elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = elementData->hyp_edge.qx*elementData->hyp_edge.qy*inv_hyp_edge_h;
			T elementData_right_edge_qx_qy_MUL_inv_right_edge_h = elementData->right_edge.qx*elementData->right_edge.qy*inv_right_edge_h;
			T elementData_left_edge_qx_yq_MUL_inv_left_edge_h = elementData->left_edge.qx*elementData->left_edge.qy*inv_left_edge_h;
			*/
			__m128 m_elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_hyp_edge_qx, m_elementData_hyp_edge_qy), m_inv_hyp_edge_h);
			__m128 m_elementData_right_edge_qx_qy_MUL_inv_right_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_right_edge_qx, m_elementData_right_edge_qy), m_inv_right_edge_h);
			__m128 m_elementData_left_edge_qx_qy_MUL_inv_left_edge_h = _mm_mul_ps(_mm_mul_ps(m_elementData_left_edge_qx, m_elementData_left_edge_qy), m_inv_left_edge_h);

			/*
			T sum_elementData_edges_qx_qy_MUL_inv_edges_h = elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h + elementData_right_edge_qx_qy_MUL_inv_right_edge_h + elementData_left_edge_qx_qy_MUL_inv_left_edge_h;
			 */
			__m128 m_sum_elementData_edges_qx_qy_MUL_inv_edges_h = _mm_add_ps(_mm_add_ps(m_elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h, m_elementData_right_edge_qx_qy_MUL_inv_right_edge_h), m_elementData_left_edge_qx_qy_MUL_inv_left_edge_h);

			/*
			T triangle_area_MUL_two_div_h_MUL_1_3 = (1.0/3.0)*triangle_area*two_div_h;
			*/
			__m128 m_triangle_area_MUL_two_div_h_MUL_1_3 = _mm_mul_ps(_mm_mul_ps(_mm_set1_ps((T)(1.0/3.0)), m_triangle_area), m_two_div_h);

			/*
			T gravity_potential = (T)0.5*config.gravity*sum_elementData_edges_h2;
			 */
			__m128 m_gravity_potential = _mm_mul_ps(_mm_mul_ps(_mm_set1_ps(0.5), m_gravity), m_sum_elementData_edges_h2);


			/*
			 * H
			 */
			/*
			T hyp_edge_h = timestep_six_div_h2*(
					h_div_3*(elementData_sum_edge_qx + elementData_sum_edge_qy)
					- hyp_length*flux_hyp.h
					);
			*/
			__m128 m_hyp_edge_h =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
									m_h_div_3,
									_mm_add_ps(m_elementData_sum_edge_qx, m_elementData_sum_edge_qy)
								),
								_mm_mul_ps(m_hyp_length, _mm_load_ps(&hyp_edge_flux[0][0]))
							)
					);

			/*
			T right_edge_h = timestep_six_div_h2*(
					-h_div_3*elementData_sum_edge_qx
					- cat_length*flux_right.h
					);
			*/
			__m128 m_right_edge_h =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
									inv(m_h_div_3),
									m_elementData_sum_edge_qx
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&right_edge_flux[0][0]))
							)
					);

			/*
			T left_edge_h = timestep_six_div_h2*(
					-h_div_3*elementData_sum_edge_qy
					- cat_length*flux_left.h
					);
			*/
			__m128 m_left_edge_h =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
									inv(m_h_div_3),
									m_elementData_sum_edge_qy
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&left_edge_flux[0][0]))
							)
					);


			/*
			 * QX
			 */

			/*
			T hyp_edge_qx =
					timestep_six_div_h2*
					(
							(T)triangle_area_MUL_two_div_h_MUL_1_3*
							(
								sum_elementData_edges_qx2_MUL_inv_edges_h +
								gravity_potential +
								sum_elementData_edges_qx_qy_MUL_inv_edges_h
							)
							- hyp_length*hyp_edge_flux.qx
					);
			*/
			__m128 m_hyp_edge_qx =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										m_triangle_area_MUL_two_div_h_MUL_1_3,
										_mm_add_ps(
												_mm_add_ps(
														m_sum_elementData_edges_qx2_MUL_inv_edges_h,
														m_gravity_potential
												),
												m_sum_elementData_edges_qx_qy_MUL_inv_edges_h
										)
								),
								_mm_mul_ps(m_hyp_length, _mm_load_ps(&(hyp_edge_flux[1][0])))
							)
					);

			/*
			T right_edge_qx =
					timestep_six_div_h2*(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*(
								- gravity_potential
								- sum_elementData_edges_qx2_MUL_inv_edges_h
						)
						- cat_length*right_edge_flux[1][i]
					);
			*/
			__m128 m_right_edge_qx =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										m_triangle_area_MUL_two_div_h_MUL_1_3,
										_mm_sub_ps(
												inv(m_gravity_potential),
												m_sum_elementData_edges_qx2_MUL_inv_edges_h
										)
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&right_edge_flux[1][0]))
							)
					);

			/*
			T left_edge_qx =
					timestep_six_div_h2*(
							(T)triangle_area_MUL_two_div_h_MUL_1_3*(
								-sum_elementData_edges_qx_qy_MUL_inv_edges_h
							)
							- cat_length*left_edge_flux.qx
					);
			*/
			__m128 m_left_edge_qx =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										m_triangle_area_MUL_two_div_h_MUL_1_3,
										inv(m_sum_elementData_edges_qx_qy_MUL_inv_edges_h)
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&left_edge_flux[1][0]))
							)
					);



			/*
			 * QY
			 */
			/*
			T hyp_edge_qy =
					timestep_six_div_h2*(
							triangle_area_MUL_two_div_h_MUL_1_3*(
								sum_elementData_edges_qx_qy_MUL_inv_edges_h +
								sum_elementData_edges_qy2_MUL_inv_edges_h +
								gravity_potential
							)
							- hyp_length*hyp_edge_flux.qy
					);
			*/
			__m128 m_hyp_edge_qy =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										m_triangle_area_MUL_two_div_h_MUL_1_3,
										_mm_add_ps(
												m_sum_elementData_edges_qx_qy_MUL_inv_edges_h,
												_mm_add_ps(
														m_sum_elementData_edges_qy2_MUL_inv_edges_h,
														m_gravity_potential
												)
										)
								),
								_mm_mul_ps(m_hyp_length, _mm_load_ps(&(hyp_edge_flux[2][0])))
							)
					);

			/*
			T right_edge_qy =
					timestep_six_div_h2*(
							- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_qx_qy_MUL_inv_edges_h
							- cat_length*right_edge_flux.qy
					);
			*/
			__m128 m_right_edge_qy =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										inv(m_triangle_area_MUL_two_div_h_MUL_1_3),
										m_sum_elementData_edges_qx_qy_MUL_inv_edges_h
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&right_edge_flux[2][0]))
							)
					);


			/*
			T left_edge_qy =
					timestep_six_div_h2*(
							-triangle_area_MUL_two_div_h_MUL_1_3*(
									sum_elementData_edges_qy2_MUL_inv_edges_h +
									gravity_potential
							)
							- cat_length*left_edge_flux.qy
					);
			*/
			__m128 m_left_edge_qy =
					_mm_mul_ps(
							m_timestep_six_div_h2,
							_mm_sub_ps(
								_mm_mul_ps(
										inv(m_triangle_area_MUL_two_div_h_MUL_1_3),
										_mm_add_ps(
												m_sum_elementData_edges_qy2_MUL_inv_edges_h,
												m_gravity_potential
										)
								),
								_mm_mul_ps(m_cat_length, _mm_load_ps(&left_edge_flux[2][0]))
							)
					);


			/*
			elementData[i]->left_edge.h += left_edge_h;
			elementData[i]->left_edge.qx += left_edge_qx;
			elementData[i]->left_edge.qy += left_edge_qy;
			elementData[i]->hyp_edge.h += hyp_edge_h;
			elementData[i]->hyp_edge.qx += hyp_edge_qx;
			elementData[i]->hyp_edge.qy += hyp_edge_qy;
			elementData[i]->right_edge.h += right_edge_h;
			elementData[i]->right_edge.qx += right_edge_qx;
			elementData[i]->right_edge.qy += right_edge_qy;
			*/

			m_hyp_edge_h = _mm_add_ps(_mm_set_ps(elementData[3]->hyp_edge.h, elementData[2]->hyp_edge.h, elementData[1]->hyp_edge.h, elementData[0]->hyp_edge.h), m_hyp_edge_h);
			m_hyp_edge_qx = _mm_add_ps(_mm_set_ps(elementData[3]->hyp_edge.qx, elementData[2]->hyp_edge.qx, elementData[1]->hyp_edge.qx, elementData[0]->hyp_edge.qx), m_hyp_edge_qx);
			m_hyp_edge_qy = _mm_add_ps(_mm_set_ps(elementData[3]->hyp_edge.qy, elementData[2]->hyp_edge.qy, elementData[1]->hyp_edge.qy, elementData[0]->hyp_edge.qy), m_hyp_edge_qy);

			m_right_edge_h = _mm_add_ps(_mm_set_ps(elementData[3]->right_edge.h, elementData[2]->right_edge.h, elementData[1]->right_edge.h, elementData[0]->right_edge.h), m_right_edge_h);
			m_right_edge_qx = _mm_add_ps(_mm_set_ps(elementData[3]->right_edge.qx, elementData[2]->right_edge.qx, elementData[1]->right_edge.qx, elementData[0]->right_edge.qx), m_right_edge_qx);
			m_right_edge_qy = _mm_add_ps(_mm_set_ps(elementData[3]->right_edge.qy, elementData[2]->right_edge.qy, elementData[1]->right_edge.qy, elementData[0]->right_edge.qy), m_right_edge_qy);

			m_left_edge_h = _mm_add_ps(_mm_set_ps(elementData[3]->left_edge.h, elementData[2]->left_edge.h, elementData[1]->left_edge.h, elementData[0]->left_edge.h), m_left_edge_h);
			m_left_edge_qx = _mm_add_ps(_mm_set_ps(elementData[3]->left_edge.qx, elementData[2]->left_edge.qx, elementData[1]->left_edge.qx, elementData[0]->left_edge.qx), m_left_edge_qx);
			m_left_edge_qy = _mm_add_ps(_mm_set_ps(elementData[3]->left_edge.qy, elementData[2]->left_edge.qy, elementData[1]->left_edge.qy, elementData[0]->left_edge.qy), m_left_edge_qy);


			/*
			#define unrotate(qx, qy)
			...
			qy = (left_normal_x[i])*qx + (-left_normal_y[i])*qy;
			*/
			__m128 m_left_normal_x = _mm_load_ps(left_normal_x);
			__m128 m_left_normal_y = inv(_mm_load_ps(left_normal_y));
			__m128 m_new_hyp_edge_qy = _mm_add_ps(_mm_mul_ps(m_left_normal_x, m_hyp_edge_qx), _mm_mul_ps(m_left_normal_y, m_hyp_edge_qy));
			__m128 m_new_right_edge_qy = _mm_add_ps(_mm_mul_ps(m_left_normal_x, m_right_edge_qx), _mm_mul_ps(m_left_normal_y, m_right_edge_qy));
			__m128 m_new_left_edge_qy = _mm_add_ps(_mm_mul_ps(m_left_normal_x, m_left_edge_qx), _mm_mul_ps(m_left_normal_y, m_left_edge_qy));


			/*
			tmp = -left_normal_y[i]*qx - left_normal_x[i]*qy;	\
			...
			qx = tmp;
			 */
			__m128 m_new_hyp_edge_qx = _mm_sub_ps(_mm_mul_ps(m_left_normal_x, m_hyp_edge_qx), _mm_mul_ps(m_left_normal_y, m_hyp_edge_qy));
			__m128 m_new_right_edge_qx = _mm_sub_ps(_mm_mul_ps(m_left_normal_x, m_right_edge_qx), _mm_mul_ps(m_left_normal_y, m_right_edge_qy));
			__m128 m_new_left_edge_qx = _mm_sub_ps(_mm_mul_ps(m_left_normal_x, m_left_edge_qx), _mm_mul_ps(m_left_normal_y, m_left_edge_qy));

			/**
			 * scatter
			 */
			_MM_EXTRACT_FLOAT(elementData[0]->hyp_edge.h, m_hyp_edge_h, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->hyp_edge.qx, m_new_hyp_edge_qx, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->hyp_edge.qy, m_new_hyp_edge_qy, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->right_edge.h, m_right_edge_h, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->right_edge.qx, m_new_right_edge_qx, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->right_edge.qy, m_new_right_edge_qy, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->left_edge.h, m_left_edge_h, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->left_edge.qx, m_new_left_edge_qx, 0);
			_MM_EXTRACT_FLOAT(elementData[0]->left_edge.qy, m_new_left_edge_qy, 0);

			_MM_EXTRACT_FLOAT(elementData[1]->hyp_edge.h, m_hyp_edge_h, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->hyp_edge.qx, m_new_hyp_edge_qx, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->hyp_edge.qy, m_new_hyp_edge_qy, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->right_edge.h, m_right_edge_h, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->right_edge.qx, m_new_right_edge_qx, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->right_edge.qy, m_new_right_edge_qy, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->left_edge.h, m_left_edge_h, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->left_edge.qx, m_new_left_edge_qx, 1);
			_MM_EXTRACT_FLOAT(elementData[1]->left_edge.qy, m_new_left_edge_qy, 1);

			_MM_EXTRACT_FLOAT(elementData[2]->hyp_edge.h, m_hyp_edge_h, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->hyp_edge.qx, m_new_hyp_edge_qx, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->hyp_edge.qy, m_new_hyp_edge_qy, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->right_edge.h, m_right_edge_h, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->right_edge.qx, m_new_right_edge_qx, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->right_edge.qy, m_new_right_edge_qy, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->left_edge.h, m_left_edge_h, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->left_edge.qx, m_new_left_edge_qx, 2);
			_MM_EXTRACT_FLOAT(elementData[2]->left_edge.qy, m_new_left_edge_qy, 2);

			_MM_EXTRACT_FLOAT(elementData[3]->hyp_edge.h, m_hyp_edge_h, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->hyp_edge.qx, m_new_hyp_edge_qx, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->hyp_edge.qy, m_new_hyp_edge_qy, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->right_edge.h, m_right_edge_h, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->right_edge.qx, m_new_right_edge_qx, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->right_edge.qy, m_new_right_edge_qy, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->left_edge.h, m_left_edge_h, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->left_edge.qx, m_new_left_edge_qx, 3);
			_MM_EXTRACT_FLOAT(elementData[3]->left_edge.qy, m_new_left_edge_qy, 3);


			computedFluxesCounter = 0;
			elementDataCounter = 0;
			return;


#if 0

			SSEALIGN16 T _hyp_edge_h[4];
			_mm_store_ps(_hyp_edge_h, m_hyp_edge_h);

			SSEALIGN16 T _right_edge_h[4];
			_mm_store_ps(_right_edge_h, m_right_edge_h);

			SSEALIGN16 T _left_edge_h[4];
			_mm_store_ps(_left_edge_h, m_left_edge_h);


			SSEALIGN16 T _hyp_edge_qx[4];
			_mm_store_ps(_hyp_edge_qx, m_hyp_edge_qx);

			SSEALIGN16 T _right_edge_qx[4];
			_mm_store_ps(_right_edge_qx, m_right_edge_qx);

			SSEALIGN16 T _left_edge_qx[4];
			_mm_store_ps(_left_edge_qx, m_left_edge_qx);


			SSEALIGN16 T _hyp_edge_qy[4];
			_mm_store_ps(_hyp_edge_qy, m_hyp_edge_qy);

			SSEALIGN16 T _right_edge_qy[4];
			_mm_store_ps(_right_edge_qy, m_right_edge_qy);

			SSEALIGN16 T _left_edge_qy[4];
			_mm_store_ps(_left_edge_qy, m_left_edge_qy);


			for (int i = 0; i < 4; i++)
			{
#if 0
				std::cout << "elementData:" << std::endl;
				std::cout << *elementData[i] << std::endl;
				std::cout << "flux:" << std::endl;
				std::cout << hyp_edge_flux[0][i] << " " << hyp_edge_flux[1][i] << " " << hyp_edge_flux[2][i] << " " << std::endl;
				std::cout << right_edge_flux[0][i] << " " << right_edge_flux[1][i] << " " << right_edge_flux[2][i] << " " << std::endl;
				std::cout << left_edge_flux[0][i] << " " << left_edge_flux[1][i] << " " << left_edge_flux[2][i] << " " << std::endl;
				std::cout << std::endl;
#endif


				T cat_length = cathetus_unit_length[i]*config->square_side_length;
				T hyp_length = hypotenuse_unit_length[i]*config->square_side_length;


#if 0
//				TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config->delta_timestep/cathetus_length, (T)0.99);
				TTsunamiDataScalar damping = (T)1.0-(config->timestep_size/cat_length);

				elementData->hyp_edge.qx *= damping;
				elementData->hyp_edge.qy *= damping;
				elementData->left_edge.qx *= damping;
				elementData->left_edge.qy *= damping;
				elementData->right_edge.qx *= damping;
				elementData->right_edge.qy *= damping;
#endif


				T six_div_h2 = (T)6.0/(cat_length*cat_length);
				T timestep_six_div_h2 = config->timestep_size*six_div_h2;
				T two_div_h = (T)2.0/cat_length;
				T h_div_3 = cat_length/(T)3.0;

				T inv_left_edge_h = (T)1.0/elementData[i]->left_edge.h;
				T inv_right_edge_h = (T)1.0/elementData[i]->right_edge.h;
				T inv_hyp_edge_h = (T)1.0/elementData[i]->hyp_edge.h;

				T elementData_sum_edge_qx = elementData[i]->left_edge.qx + elementData[i]->hyp_edge.qx + elementData[i]->right_edge.qx;
				T elementData_sum_edge_qy = elementData[i]->left_edge.qy + elementData[i]->hyp_edge.qy + elementData[i]->right_edge.qy;

				T triangle_area = cat_length*cat_length*(T)0.5;

				T elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qx*elementData[i]->hyp_edge.qx*inv_hyp_edge_h;
				T elementData_right_edge_qx2_MUL_inv_right_edge_h = elementData[i]->right_edge.qx*elementData[i]->right_edge.qx*inv_right_edge_h;
				T elementData_left_edge_qx2_MUL_inv_left_edge_h = elementData[i]->left_edge.qx*elementData[i]->left_edge.qx*inv_left_edge_h;

				T sum_elementData_edges_qx2_MUL_inv_edges_h = elementData_left_edge_qx2_MUL_inv_left_edge_h + elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h + elementData_right_edge_qx2_MUL_inv_right_edge_h;

				T elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qy*elementData[i]->hyp_edge.qy*inv_hyp_edge_h;
				T elementData_right_edge_qy2_MUL_inv_right_edge_h = elementData[i]->right_edge.qy*elementData[i]->right_edge.qy*inv_right_edge_h;
				T elementData_left_edge_qy2_MUL_inv_left_edge_h = elementData[i]->left_edge.qy*elementData[i]->left_edge.qy*inv_left_edge_h;

				T sum_elementData_edges_qy2_MUL_inv_edges_h = elementData_left_edge_qy2_MUL_inv_left_edge_h + elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h + elementData_right_edge_qy2_MUL_inv_right_edge_h;

				T elementData_hyp_edge_h2 = elementData[i]->hyp_edge.h*elementData[i]->hyp_edge.h;
				T elementData_right_edge_h2 = elementData[i]->right_edge.h*elementData[i]->right_edge.h;
				T elementData_left_edge_h2 = elementData[i]->left_edge.h*elementData[i]->left_edge.h;

				T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;


				T elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = elementData[i]->hyp_edge.qx*elementData[i]->hyp_edge.qy*inv_hyp_edge_h;
				T elementData_right_edge_qx_qy_MUL_inv_right_edge_h = elementData[i]->right_edge.qx*elementData[i]->right_edge.qy*inv_right_edge_h;
				T elementData_left_edge_qx_qy_MUL_inv_left_edge_h = elementData[i]->left_edge.qx*elementData[i]->left_edge.qy*inv_left_edge_h;

				T sum_elementData_edges_qx_qy_MUL_inv_edges_h = elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h + elementData_right_edge_qx_qy_MUL_inv_right_edge_h + elementData_left_edge_qx_qy_MUL_inv_left_edge_h;

				T triangle_area_MUL_two_div_h_MUL_1_3 = (1.0/3.0)*triangle_area*two_div_h;

				T gravity_potential = (T)0.5*config->gravitational_constant*sum_elementData_edges_h2;

/*
				T hyp_edge_h = timestep_six_div_h2*(
						h_div_3*(elementData_sum_edge_qx + elementData_sum_edge_qy)
						- hyp_length*hyp_edge_flux[0][i]
						);
*/
				T hyp_edge_h = _hyp_edge_h[i];
/*
				T right_edge_h = timestep_six_div_h2*(
						-h_div_3*elementData_sum_edge_qx
						- cat_length*right_edge_flux[0][i]
						);
*/
				T right_edge_h = _right_edge_h[i];
/*
				T left_edge_h = timestep_six_div_h2*(
						-h_div_3*elementData_sum_edge_qy
						- cat_length*left_edge_flux[0][i]
						);
*/
				T left_edge_h = _left_edge_h[i];
/*
				T hyp_edge_qx =
						timestep_six_div_h2*
						(
								(T)triangle_area_MUL_two_div_h_MUL_1_3*
								(
									sum_elementData_edges_qx2_MUL_inv_edges_h +
									gravity_potential +
									sum_elementData_edges_qx_qy_MUL_inv_edges_h
								)
								- hyp_length*hyp_edge_flux[1][i]
						);
*/
				T hyp_edge_qx = _hyp_edge_qx[i];
/*
				T right_edge_qx =
						timestep_six_div_h2*(
							(T)triangle_area_MUL_two_div_h_MUL_1_3*(
									- gravity_potential
									- sum_elementData_edges_qx2_MUL_inv_edges_h
							)
							- cat_length*right_edge_flux[1][i]
						);
*/

				T right_edge_qx = _right_edge_qx[i];
/*
				T left_edge_qx =
						+ timestep_six_div_h2*(
								(T)triangle_area_MUL_two_div_h_MUL_1_3*(
									-sum_elementData_edges_qx_qy_MUL_inv_edges_h
								)
								- cat_length*left_edge_flux[1][i]
						);
*/
				T left_edge_qx = _left_edge_qx[i];
/*
				T hyp_edge_qy =
						timestep_six_div_h2*(
								triangle_area_MUL_two_div_h_MUL_1_3*(
									sum_elementData_edges_qx_qy_MUL_inv_edges_h +
									sum_elementData_edges_qy2_MUL_inv_edges_h +
									gravity_potential
								)
								- hyp_length*hyp_edge_flux[2][i]
						);
*/
				T hyp_edge_qy = _hyp_edge_qy[i];

/*
				T right_edge_qy =
						timestep_six_div_h2*(
								- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_qx_qy_MUL_inv_edges_h
								- cat_length*right_edge_flux[2][i]
						);
*/
				T right_edge_qy = _right_edge_qy[i];

/*
				T left_edge_qy =
						timestep_six_div_h2*(
								-triangle_area_MUL_two_div_h_MUL_1_3*(
										sum_elementData_edges_qy2_MUL_inv_edges_h +
										gravity_potential
								)
								- cat_length*left_edge_flux[2][i]
						);
*/
				T left_edge_qy = _left_edge_qy[i];

				elementData[i]->left_edge.h = left_edge_h;
				elementData[i]->left_edge.qx = left_edge_qx;
				elementData[i]->left_edge.qy = left_edge_qy;
				elementData[i]->hyp_edge.h = hyp_edge_h;
				elementData[i]->hyp_edge.qx = hyp_edge_qx;
				elementData[i]->hyp_edge.qy = hyp_edge_qy;
				elementData[i]->right_edge.h = right_edge_h;
				elementData[i]->right_edge.qx = right_edge_qx;
				elementData[i]->right_edge.qy = right_edge_qy;
/*
				T tmp;
		#define unrotate(qx, qy)									\
				tmp = (-left_normal_y[i])*qx + (-left_normal_x[i])*qy;	\
				qy = (left_normal_x[i])*qx + (-left_normal_y[i])*qy;		\
				qx = tmp;

				unrotate(elementData[i]->hyp_edge.qx, elementData[i]->hyp_edge.qy)
				unrotate(elementData[i]->left_edge.qx, elementData[i]->left_edge.qy)
				unrotate(elementData[i]->right_edge.qx, elementData[i]->right_edge.qy)
		#undef unrotate
*/
			}

			// TODO: reset
			computedFluxesCounter = 0;
			elementDataCounter = 0;
#endif
		}
	};



	class CPackedElementComputationsBucketHandler
	{
public:
		CPackedElementComputations *cPackedElementDataComputations;
		int nextFreePackedElementDataComputationsBlock;

		CEdgeComm_Tsunami_1stOrder_config *config;

		CPackedElementComputationsBucketHandler(
				CEdgeComm_Tsunami_1stOrder_config *p_config,
				int maxElements
		)	:
			config(p_config)
		{
			nextFreePackedElementDataComputationsBlock = 0;

			cPackedElementDataComputations = new CPackedElementComputations[maxElements];

			for (int i = 0; i < maxElements; i++)
				cPackedElementDataComputations[i].setup(config);
		}

		~CPackedElementComputationsBucketHandler()
		{
			delete cPackedElementDataComputations;
		}

		void flush()
		{
			for (int i = 0; i <= nextFreePackedElementDataComputationsBlock; i++)
			{
//				std::cout << "ljkasdfkljsjkladf" << std::endl;
				if (cPackedElementDataComputations[i].elementDataCounter != 0)
					cPackedElementDataComputations[i].compute();
			}

			nextFreePackedElementDataComputationsBlock = 0;
		}

		inline CPackedElementComputations *getNewElementDataComputationBucket(
				CTsunamiElementData *i_elementData,
				T i_left_normal_x,
				T i_left_normal_y,
				int i_depth
		)
		{
//			assert(nextFreePackedElementDataComputationsBlock != maxElements);

			CPackedElementComputations *cPackedElementsComputations = &cPackedElementDataComputations[nextFreePackedElementDataComputationsBlock];

			if (cPackedElementsComputations->insert(
					i_elementData,
					i_left_normal_x,
					i_left_normal_y,
					i_depth
				))
				nextFreePackedElementDataComputationsBlock++;

			return cPackedElementsComputations;
		}
	};

	CPackedElementComputationsBucketHandler cPackedElementComputationsBucketHandler;



	class CPackedFluxComputations
	{
	public:
		SSEALIGN16 float inner[4][4];
		SSEALIGN16 float outer[4][4];

		SSEALIGN16 float inner_h[4];
		SSEALIGN16 float inner_qx[4];
		SSEALIGN16 float inner_qy[4];

		SSEALIGN16 float outer_h[4];
		SSEALIGN16 float outer_qx[4];
		SSEALIGN16 float outer_qy[4];

		SSEALIGN16 float edge_normal_x[4];
		SSEALIGN16 float edge_normal_y[4];

		CPackedElementComputations *cPackedElementComputations[4];
		T *packedElementFlux3x4[4];
		int packedElementFluxId[4];
		int packedFluxCounter;


		CPackedFluxComputations()
		{
			packedFluxCounter = 0;
		}

		/**
		 * manual final flush to compute remaining fluxes
		 */
		void flush()
		{
			if (packedFluxCounter == 0)
				return;

			compute();
		}

		void insert(
				const CTsunamiEdgeData *i_inner_,
				const CTsunamiEdgeData *i_outer_,
				const T i_edge_normal_x_,
				const T i_edge_normal_y_,

				CPackedElementComputations *i_cPackedElementComputations,
				T *i_packedFlux,
  				int i_packedFluxId
		)
		{
			_mm_store_ps(&inner[packedFluxCounter][0], _mm_loadu_ps((float*)i_inner_));
			_mm_store_ps(&outer[packedFluxCounter][0], _mm_loadu_ps((float*)i_outer_));

			edge_normal_x[packedFluxCounter] = i_edge_normal_x_;
			edge_normal_y[packedFluxCounter] = i_edge_normal_y_;


			cPackedElementComputations[packedFluxCounter] = i_cPackedElementComputations;
			packedElementFlux3x4[packedFluxCounter] = i_packedFlux;
			packedElementFluxId[packedFluxCounter] = i_packedFluxId;

			packedFluxCounter++;

			if (packedFluxCounter < 4)
				return;

			compute();
		}



		/**
		 * flush fluxes and write and update destination element data
		 */
		void compute()
		{
//			std::cout << "compute Fluxes" << std::endl;

			__m128 i_inner_h = _mm_load_ps(&inner[0][0]);
			__m128 i_inner_qx = _mm_load_ps(&inner[1][0]);
			__m128 i_inner_qy = _mm_load_ps(&inner[2][0]);
			__m128 i_inner_dummy = _mm_load_ps(&inner[3][0]);

			_MM_TRANSPOSE4_PS(i_inner_h, i_inner_qx, i_inner_qy, i_inner_dummy);


			__m128 i_outer_h = _mm_load_ps(&outer[0][0]);
			__m128 i_outer_qx = _mm_load_ps(&outer[1][0]);
			__m128 i_outer_qy = _mm_load_ps(&outer[2][0]);
			__m128 i_outer_dummy = _mm_load_ps(&outer[3][0]);

			_MM_TRANSPOSE4_PS(i_outer_h, i_outer_qx, i_outer_qy, i_outer_dummy);


			__m128 i_edge_normal_x = _mm_load_ps(edge_normal_x);
			__m128 i_edge_normal_y = _mm_load_ps(edge_normal_y);

			// TODO: setup gravity by config.gravity
			__m128 gravity = _mm_set1_ps(9.81);

	/*
	 * SERIAL CODE:
			T inner_vx = i_inner.qx/i_inner.h;
			T inner_vy = i_inner.qy/i_inner.h;

			T outer_vx = i_outer.qx/i_outer.h;
			T outer_vy = i_outer.qy/i_outer.h;
	*/

			// TODO: use reciprocal
			__m128 inner_vx = _mm_div_ps(i_inner_qx, i_inner_h);
			__m128 inner_vy = _mm_div_ps(i_inner_qy, i_inner_h);

			// TODO: use reciprocal
			__m128 outer_vx = _mm_div_ps(i_outer_qx, i_outer_h);
			__m128 outer_vy = _mm_div_ps(i_outer_qy, i_outer_h);

	/*
	 * SERIAL CODE:
			T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravity*i_inner.h);
	*/
	/*
			__m128 inner_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(inner_vx, inner_vx),
									_mm_mul_ps(inner_vy, inner_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_inner_h))
					);
	*/
			__m128 inner_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(inner_vx, inner_vx),
									_mm_mul_ps(inner_vy, inner_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_inner_h))
					);

	/*
	 * SERIAL CODE:
			T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravity*i_outer.h);
	*/
			__m128 outer_lambda =
					_mm_add_ps(
						_mm_sqrt_ps(
							_mm_add_ps(
									_mm_mul_ps(outer_vx, outer_vx),
									_mm_mul_ps(outer_vy, outer_vy)
									)
						),
						_mm_sqrt_ps(_mm_mul_ps(gravity, i_outer_h))
					);

	/*
	 * SERIAL CODE:
			T lambda = CMath::max(inner_lambda, outer_lambda)
	*/
			__m128 lambda = _mm_max_ps(inner_lambda, outer_lambda);


	/*
	 * SERIAL CODE:
			o_flux.h =
					(T)0.5*
					((
							(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
							(i_edge_normal_y*(i_inner.qy + i_outer.qy))
					) +
					(T)lambda*(
						i_inner.h - i_outer.h
					));
	*/
			__m128 o_flux_h =
						// (T)0.5*...
						_mm_mul_ps(
							_mm_set1_ps(0.5f),
							_mm_add_ps(
								// +
								_mm_add_ps(
									// (i_edge_normal_x*(i_inner.qx + i_outer.qx))
									_mm_mul_ps(
											i_edge_normal_x,
											_mm_add_ps(i_inner_qx, i_outer_qx)
											),
									// (i_edge_normal_y*(i_inner.qy + i_outer.qy))
									_mm_mul_ps(
											i_edge_normal_y,
											_mm_add_ps(i_inner_qy, i_outer_qy)
											)
								),
								// (T)lambda*(i_inner.h - i_outer.h))
								_mm_mul_ps(
									lambda,
									_mm_sub_ps(i_inner_h, i_outer_h)
								)
							)
						);

	/*
	 * SERIAL CODE:
			T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;
	*/

			__m128 inner_h2_plus_outer_h2 =
					_mm_add_ps(
							_mm_mul_ps(i_inner_h, i_inner_h),
							_mm_mul_ps(i_outer_h, i_outer_h)
						);

	/*
	 * SERIAL CODE:
				o_flux.qx =
						(T)0.5*
						((
							i_edge_normal_x*	(	inner_vx*i_inner.qx +
													outer_vx*i_outer.qx +
													(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
							+
							i_edge_normal_y*	(	inner_vx*i_inner.qy +
													outer_vx*i_outer.qy
												)
						) +
						(T)lambda*(
								i_inner.qx - i_outer.qx
						));
	*/

			__m128 o_flux_qx =
						// (T)0.5*...
						_mm_mul_ps(
							_mm_set1_ps(0.5f),
							_mm_add_ps(
								_mm_add_ps(
									/*
									i_edge_normal_x*	(	inner_vx*i_inner.qx +
															outer_vx*i_outer.qx +
															(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
									 */
									_mm_mul_ps(
										i_edge_normal_x,
										_mm_add_ps(
											_mm_add_ps(
													_mm_mul_ps(inner_vx, i_inner_qx),
													_mm_mul_ps(outer_vx, i_outer_qx)
											),
											_mm_mul_ps(
												_mm_mul_ps(
														_mm_set1_ps(0.5f),
														gravity
												),
												inner_h2_plus_outer_h2
											)
										)
									),
									/*
									i_edge_normal_y*	(	inner_vx*i_inner.qy +
															outer_vx*i_outer.qy
									 */
									_mm_mul_ps(
										i_edge_normal_y,
										_mm_add_ps(
												_mm_mul_ps(inner_vx, i_inner_qy),
												_mm_mul_ps(outer_vx, i_outer_qy)
										)
									)
								), // _mm_add_
								/*
								 * (T)lambda*(
										i_inner.qx - i_outer.qx
								*/
								_mm_mul_ps(
									lambda,
									_mm_sub_ps(i_inner_qx, i_outer_qx)
								)
							)
						);

	/*
	 * SERIAL CODE:

			o_flux.qy =
					(T)0.5*
					((
							i_edge_normal_y*	(	inner_vy*i_inner.qy +
													outer_vy*i_outer.qy +
													(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
							+
							i_edge_normal_x*	(	inner_vy*i_inner.qx +
													outer_vy*i_outer.qx
												)
					) +
					(T)lambda*(
							i_inner.qy - i_outer.qy
					));
	*/

		__m128 o_flux_qy =
					// (T)0.5*...
					_mm_mul_ps(
						_mm_set1_ps(0.5f),
						_mm_add_ps(
							_mm_add_ps(
								/*
								i_edge_normal_y*	(	inner_vy*i_inner.qy +
														outer_vy*i_outer.qy +
														(TTsunamiDataScalar)0.5*config.gravity*inner_h2_plus_outer_h2
												)
								 */
								_mm_mul_ps(
									i_edge_normal_y,
									_mm_add_ps(
										_mm_add_ps(
												_mm_mul_ps(inner_vy, i_inner_qy),
												_mm_mul_ps(outer_vy, i_outer_qy)
										),
										_mm_mul_ps(
											_mm_mul_ps(
													_mm_set1_ps(0.5f),
													gravity
											),
											inner_h2_plus_outer_h2
										)
									)
								),
								/*
									i_edge_normal_x*	(	inner_vy*i_inner.qx +
															outer_vy*i_outer.qx
												)
								 */
								_mm_mul_ps(
									i_edge_normal_x,
									_mm_add_ps(
											_mm_mul_ps(inner_vy, i_inner_qx),
											_mm_mul_ps(outer_vy, i_outer_qx)
									)
								)
							), // _mm_add_
							/*
							(T)lambda*(
								i_inner.qy - i_outer.qy
							*/
							_mm_mul_ps(
								lambda,
								_mm_sub_ps(i_inner_qy, i_outer_qy)
							)
						)
					);

			SSEALIGN16 T flux_h[4];	// h
			SSEALIGN16 T flux_qx[4];	// qx
			SSEALIGN16 T flux_qy[4];	// qy

			_mm_store_ps(&(flux_h[0]), o_flux_h);
			_mm_store_ps(&(flux_qx[0]), o_flux_qx);
			_mm_store_ps(&(flux_qy[0]), o_flux_qy);

//			__m128 o_flux_tmp;
//			_MM_TRANSPOSE4_PS(o_flux_h, o_flux_qx, o_flux_qy, o_flux_tmp);




			for (int i = 0; i < packedFluxCounter; i++)
			{
				int packedFluxId_ = packedElementFluxId[i];

				packedElementFlux3x4[i][0*4+packedFluxId_] = flux_h[i];
				packedElementFlux3x4[i][1*4+packedFluxId_] = flux_qx[i];
				packedElementFlux3x4[i][2*4+packedFluxId_] = flux_qy[i];

				cPackedElementComputations[i]->computedFluxesCounter++;

				if (cPackedElementComputations[i]->computedFluxesCounter == 4*3)
				{
					cPackedElementComputations[i]->computeSIMD();
				}
			}

			packedFluxCounter = 0;
		}
	};

	CPackedFluxComputations cPackedFluxComputations;

	bool use_simd;

	CEdgeComm_Tsunami_1stOrder(int p_maxElements)	:
		maxElements(p_maxElements),
		cPackedElementComputationsBucketHandler(&config, p_maxElements)
	{
		DEBUG_SETUP_DONE_CONSTRUCTOR

		config.boundary_dirichlet.h = CMath::numeric_inf<T>();
		config.boundary_dirichlet.qx = CMath::numeric_inf<T>();
		config.boundary_dirichlet.qy = CMath::numeric_inf<T>();

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		config.boundary_dirichlet.edge_midpoint_x = -666;
		config.boundary_dirichlet.edge_midpoint_y = -666;
#endif

#if CEDGECOMM_TSUNAMI_1ST_ORDER_HPP_PACKED_FLUX
		use_simd = true;
#endif
	}


	void setup_RootPartition(
			CEdgeComm_Tsunami_1stOrder_config &i_config
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config = i_config;
	}


	void setup_RootPartition(
			T i_delta_timestep,
			T i_square_side_length,
			T i_gravity = 9.81
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config.timestep_size = i_delta_timestep;
		config.gravitational_constant = i_gravity;
		config.square_side_length = i_square_side_length;

	}

	void setup_ChildPartition(
			CEdgeComm_Tsunami_1stOrder<EBoundaryConditionValue> &parent
	)
	{
		DEBUG_SETUP_DONE_SETUP;
		config = parent.config;
	}

	void setup_RootPartition_KernelBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		config.boundary_dirichlet = *p_boundary_dirichlet;
	}


	inline void storeLeftEdgeCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->left_edge;
	}

	inline void storeRightEdgeCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->right_edge;
	}

	inline void storeHypCommData(
			TVertexScalar n0x, TVertexScalar n0y,
			TVertexScalar n1x, TVertexScalar n1y,
			TVertexScalar n2x, TVertexScalar n2y,
			int depth,
			CTsunamiElementData *element_data,
			CTsunamiEdgeData *dst_comm_data
	)
	{
		*dst_comm_data = element_data->hyp_edge;
	}


	void dumpm128(__m128 m)
	{
		float v[4];
		_mm_storeu_ps(v, m);
		std::cout << v[0] << " " << v[1] << " " << v[2] << std::endl;
	}

#define MAX_FLUXES	3


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}


	/**
	 * see diplomathesis, hoechstetter (2009), page 43
	 *
	 * ~60 floating point operations + 4x sqrt
	 */
	inline void computeFlux(
			const CTsunamiEdgeData &i_inner,
			const CTsunamiEdgeData &i_outer,
			const T i_edge_normal_x, const T i_edge_normal_y,
			CTsunamiEdgeData &o_flux
	)
	{
#if 1
		T inner_vx = i_inner.qx/i_inner.h;
		T inner_vy = i_inner.qy/i_inner.h;

		T outer_vx = i_outer.qx/i_outer.h;
		T outer_vy = i_outer.qy/i_outer.h;

		__m128 m_inner_outer_qx_qy = _mm_set_ps(i_outer.qy, i_outer.qx, i_inner.qy, i_inner.qx);
		__m128 m_inner_h_outer_h = _mm_set_ps(i_outer.h, i_outer.h, i_inner.h, i_inner.h);
		__m128 m_inner_outer_vx_vy = _mm_div_ps(m_inner_outer_qx_qy, m_inner_h_outer_h);

		__m128 m_gravity = _mm_set1_ps(config.gravitational_constant);

		/*
		T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravity*i_inner.h);
		T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravity*i_outer.h);
		 */
		__m128 m_left_up = _mm_dp_ps(m_inner_outer_vx_vy, m_inner_outer_vx_vy, 16*3 + 7);
		__m128 m_left_bottom = _mm_dp_ps(m_inner_outer_vx_vy, m_inner_outer_vx_vy, 16*12 + 7);

		__m128 m_right_side = _mm_mul_ps(m_gravity, m_inner_h_outer_h);

		__m128 m_sqrt_helper1 = _mm_blend_ps(m_left_up, m_right_side, 2);		// r0, r1
		__m128 m_sqrt_helper2 = _mm_blend_ps(m_left_bottom, m_right_side, 8);	// r2, r3
		__m128 m_sqrt = _mm_sqrt_ps(_mm_blend_ps(m_sqrt_helper1, m_sqrt_helper2, 12));

		T sqrt_stuff[4];
		_mm_store_ps(sqrt_stuff, m_sqrt);

		/*
		T lambda = CMath::max(inner_lambda, outer_lambda);
		*/
		T lambda = CMath::max(sqrt_stuff[0] + sqrt_stuff[1], sqrt_stuff[2] + sqrt_stuff[3]);

		o_flux.h =
				(T)0.5*
				((
						(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
						(i_edge_normal_y*(i_inner.qy + i_outer.qy))
				) +
				(T)lambda*(
					i_inner.h - i_outer.h
				));

		T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;

		o_flux.qx =
				(T)0.5*
				((
						i_edge_normal_x*	(	inner_vx*i_inner.qx +
												outer_vx*i_outer.qx +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_y*	(	inner_vx*i_inner.qy +
												outer_vx*i_outer.qy
											)
				) +
				(T)lambda*(
						i_inner.qx - i_outer.qx
				));

		o_flux.qy =
				(T)0.5*
				((
						i_edge_normal_y*	(	inner_vy*i_inner.qy +
												outer_vy*i_outer.qy +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_x*	(	inner_vy*i_inner.qx +
												outer_vy*i_outer.qx
											)
				) +
				(T)lambda*(
						i_inner.qy - i_outer.qy
				));
#else
		T inner_vx = i_inner.qx/i_inner.h;
		T inner_vy = i_inner.qy/i_inner.h;

		T outer_vx = i_outer.qx/i_outer.h;
		T outer_vy = i_outer.qy/i_outer.h;

		T inner_lambda = CMath::sqrt<T>(inner_vx*inner_vx+inner_vy*inner_vy) + CMath::sqrt<T>(config.gravitational_constant*i_inner.h);
		T outer_lambda = CMath::sqrt<T>(outer_vx*outer_vx+outer_vy*outer_vy) + CMath::sqrt<T>(config.gravitational_constant*i_outer.h);
		T lambda = CMath::max(inner_lambda, outer_lambda);

		o_flux.h =
				(T)0.5*
				((
						(i_edge_normal_x*(i_inner.qx + i_outer.qx))	+
						(i_edge_normal_y*(i_inner.qy + i_outer.qy))
				) +
				(T)lambda*(
					i_inner.h - i_outer.h
				));

		T inner_h2_plus_outer_h2 = i_inner.h*i_inner.h + i_outer.h*i_outer.h;

		o_flux.qx =
				(T)0.5*
				((
						i_edge_normal_x*	(	inner_vx*i_inner.qx +
												outer_vx*i_outer.qx +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_y*	(	inner_vx*i_inner.qy +
												outer_vx*i_outer.qy
											)
				) +
				(T)lambda*(
						i_inner.qx - i_outer.qx
				));

		o_flux.qy =
				(T)0.5*
				((
						i_edge_normal_y*	(	inner_vy*i_inner.qy +
												outer_vy*i_outer.qy +
												(TTsunamiDataScalar)0.5*config.gravitational_constant*inner_h2_plus_outer_h2
											)
						+
						i_edge_normal_x*	(	inner_vy*i_inner.qx +
												outer_vy*i_outer.qx
											)
				) +
				(T)lambda*(
						i_inner.qy - i_outer.qy
				));
#endif
	}


	void flush()
	{
		cPackedFluxComputations.flush();
		cPackedElementComputationsBucketHandler.flush();
	}


	inline void elementAction_EEE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,			///< normal at hypotenuse
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,		///< normal at right edge
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,		///< normal at left edge
			int depth,														///< recursion depth

			CTsunamiElementData *elementData,									///< element data

			CTsunamiEdgeData *hyp_edge,										///< data from adjacent element next to hypotenuse
			CTsunamiEdgeData *right_edge,									///< data from adjacent element next to right edge
			CTsunamiEdgeData *left_edge										///< data from adjacent element next to left edge
	)
	{
		DEBUG_SETUP_DONE_CHECK();

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		if (elementData->depth != depth)
			std::cout << "DEPTH ERROR: (element/parameter) " << elementData->depth << " " << depth << std::endl;

		if (hyp_edge->edge_midpoint_x != elementData->hyp_edge.edge_midpoint_x || hyp_edge->edge_midpoint_y != elementData->hyp_edge.edge_midpoint_y)
		{
			std::cout << "ERROR HYP   " << hyp_edge->edge_midpoint_x << " " << elementData->hyp_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << hyp_edge->edge_midpoint_y << " " << elementData->hyp_edge.edge_midpoint_y << std::endl;
		}

		if (left_edge->edge_midpoint_x != elementData->left_edge.edge_midpoint_x || left_edge->edge_midpoint_y != elementData->left_edge.edge_midpoint_y)
		{
			std::cout << "ERROR LEFT  " << left_edge->edge_midpoint_x << " " << elementData->left_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << left_edge->edge_midpoint_y << " " << elementData->left_edge.edge_midpoint_y << std::endl;
		}

		if (right_edge->edge_midpoint_x != elementData->right_edge.edge_midpoint_x || right_edge->edge_midpoint_y != elementData->right_edge.edge_midpoint_y)
		{
			std::cout << "ERROR RIGHT " << right_edge->edge_midpoint_x << " " << elementData->right_edge.edge_midpoint_x << std::endl;
			std::cout << "            " << right_edge->edge_midpoint_y << " " << elementData->right_edge.edge_midpoint_y << std::endl;
		}
#endif

#if 0
//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config.delta_timestep/cathetus_length, (T)0.99);
		TTsunamiDataScalar damping = (T)1.0-(config.timestep_size/cathetus_length);

		elementData->hyp_edge.qx *= damping;
		elementData->hyp_edge.qy *= damping;
		elementData->left_edge.qx *= damping;
		elementData->left_edge.qy *= damping;
		elementData->right_edge.qx *= damping;
		elementData->right_edge.qy *= damping;
#endif

		if (CMath::isNan(hyp_edge->h))
		{
			std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			exit(-1);
		}



		if (use_simd)
		{
			T tmp;
#define rotate(qx, qy)												\
			tmp = (-left_normal_y)*(qx) + (left_normal_x)*(qy);		\
			qy = (-left_normal_x)*(qx) + (-left_normal_y)*(qy);		\
			qx = tmp;

			rotate(hyp_edge->qx, hyp_edge->qy)
			rotate(left_edge->qx, left_edge->qy)
			rotate(right_edge->qx, right_edge->qy)

			rotate(elementData->hyp_edge.qx, elementData->hyp_edge.qy)
			rotate(elementData->left_edge.qx, elementData->left_edge.qy)
			rotate(elementData->right_edge.qx, elementData->right_edge.qy)
#undef rotate

			/*
			 * allocate packed element data computation handler and initialize it
			 */
			CPackedElementComputations *cPackedElementComputations =
					cPackedElementComputationsBucketHandler.getNewElementDataComputationBucket(
							elementData,
							left_normal_x,
							left_normal_y,
							depth
						);

			int packedElementId = cPackedElementComputations->elementDataCounter-1;

			cPackedFluxComputations.insert(	&elementData->left_edge,
											left_edge,
											0, -1,

											cPackedElementComputations,
											&cPackedElementComputations->left_edge_flux[0][0],
											packedElementId				/// hand over counter to know which flux to update
										);

			cPackedFluxComputations.insert(	&elementData->hyp_edge,
											hyp_edge,
											CMath::sqrt1_2<T>(), CMath::sqrt1_2<T>(),

											cPackedElementComputations,
											&cPackedElementComputations->hyp_edge_flux[0][0],
											packedElementId		/// hand over counter to know which flux to update
										);

			cPackedFluxComputations.insert(	&elementData->right_edge,
											right_edge,
											-1, 0,

											cPackedElementComputations,
											&cPackedElementComputations->right_edge_flux[0][0],
											packedElementId		/// hand over counter to know which flux to update
										);
		}
		else
		{

			T tmp;
#define rotate(qx, qy)											\
			tmp = (-left_normal_y)*(qx) + (left_normal_x)*(qy);		\
			qy = (-left_normal_x)*(qx) + (-left_normal_y)*(qy);		\
			qx = tmp;

			rotate(hyp_edge->qx, hyp_edge->qy)
			rotate(left_edge->qx, left_edge->qy)
			rotate(right_edge->qx, right_edge->qy)

			rotate(elementData->hyp_edge.qx, elementData->hyp_edge.qy)
			rotate(elementData->left_edge.qx, elementData->left_edge.qy)
			rotate(elementData->right_edge.qx, elementData->right_edge.qy)
#undef rotate

			CTsunamiEdgeData left_edge_flux, hyp_edge_flux, right_edge_flux;

			computeFlux(elementData->hyp_edge, *hyp_edge, CMath::sqrt1_2<T>(), CMath::sqrt1_2<T>(), hyp_edge_flux);
			computeFlux(elementData->right_edge, *right_edge, -1, 0, right_edge_flux);
			computeFlux(elementData->left_edge, *left_edge, 0, -1, left_edge_flux);
#if 0
			std::cout << "element:" << std::endl;
			std::cout << *elementData << std::endl;
			std::cout << "flux:" << std::endl;
			std::cout << hyp_edge_flux << std::endl;
			std::cout << right_edge_flux << std::endl;
			std::cout << left_edge_flux << std::endl;
			std::cout << std::endl;
#endif


			T cat_length = getUnitCathetusLengthForDepth(depth)*config.square_side_length;
			T hyp_length = getUnitHypotenuseLengthForDepth(depth)*config.square_side_length;


			T six_div_h2 = (T)6.0/(cat_length*cat_length);
			T timestep_six_div_h2 = config.timestep_size*six_div_h2;
			T two_div_h = (T)2.0/cat_length;
			T h_div_3 = cat_length/(T)3.0;

			T inv_left_edge_h = (T)1.0/elementData->left_edge.h;
			T inv_right_edge_h = (T)1.0/elementData->right_edge.h;
			T inv_hyp_edge_h = (T)1.0/elementData->hyp_edge.h;

			T elementData_sum_edge_qx = elementData->left_edge.qx + elementData->hyp_edge.qx + elementData->right_edge.qx;
			T elementData_sum_edge_qy = elementData->left_edge.qy + elementData->hyp_edge.qy + elementData->right_edge.qy;


			T triangle_area = cat_length*cat_length*(T)0.5;


			T elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h = elementData->hyp_edge.qx*elementData->hyp_edge.qx*inv_hyp_edge_h;
			T elementData_right_edge_qx2_MUL_inv_right_edge_h = elementData->right_edge.qx*elementData->right_edge.qx*inv_right_edge_h;
			T elementData_left_edge_qx2_MUL_inv_left_edge_h = elementData->left_edge.qx*elementData->left_edge.qx*inv_left_edge_h;

			T sum_elementData_edges_qx2_MUL_inv_edges_h = elementData_left_edge_qx2_MUL_inv_left_edge_h + elementData_hyp_edge_qx2_MUL_inv_hyp_edge_h + elementData_right_edge_qx2_MUL_inv_right_edge_h;

			T elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h = elementData->hyp_edge.qy*elementData->hyp_edge.qy*inv_hyp_edge_h;
			T elementData_right_edge_qy2_MUL_inv_right_edge_h = elementData->right_edge.qy*elementData->right_edge.qy*inv_right_edge_h;
			T elementData_left_edge_qy2_MUL_inv_left_edge_h = elementData->left_edge.qy*elementData->left_edge.qy*inv_left_edge_h;

			T sum_elementData_edges_qy2_MUL_inv_edges_h = elementData_left_edge_qy2_MUL_inv_left_edge_h + elementData_hyp_edge_qy2_MUL_inv_hyp_edge_h + elementData_right_edge_qy2_MUL_inv_right_edge_h;

			T elementData_hyp_edge_h2 = elementData->hyp_edge.h*elementData->hyp_edge.h;
			T elementData_right_edge_h2 = elementData->right_edge.h*elementData->right_edge.h;
			T elementData_left_edge_h2 = elementData->left_edge.h*elementData->left_edge.h;

			T sum_elementData_edges_h2 = elementData_hyp_edge_h2 + elementData_right_edge_h2 + elementData_left_edge_h2;


			T elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h = elementData->hyp_edge.qx*elementData->hyp_edge.qy*inv_hyp_edge_h;
			T elementData_right_edge_qx_qy_MUL_inv_right_edge_h = elementData->right_edge.qx*elementData->right_edge.qy*inv_right_edge_h;
			T elementData_left_edge_qx_yq_MUL_inv_left_edge_h = elementData->left_edge.qx*elementData->left_edge.qy*inv_left_edge_h;

			T sum_elementData_edges_qx_qy_MUL_inv_edges_h = elementData_hyp_edge_qx_qy_MUL_inv_hyp_edge_h + elementData_right_edge_qx_qy_MUL_inv_right_edge_h + elementData_left_edge_qx_yq_MUL_inv_left_edge_h;

			T triangle_area_MUL_two_div_h_MUL_1_3 = (1.0/3.0)*triangle_area*two_div_h;

			T gravity_potential = (T)0.5*config.gravitational_constant*sum_elementData_edges_h2;



			/*
			 * H
			 */
			T hyp_edge_h = timestep_six_div_h2*(
					h_div_3*(elementData_sum_edge_qx + elementData_sum_edge_qy)
					- hyp_length*hyp_edge_flux.h
					);

			T right_edge_h = timestep_six_div_h2*(
					-h_div_3*elementData_sum_edge_qx
					- cat_length*right_edge_flux.h
					);

			T left_edge_h = timestep_six_div_h2*(
					-h_div_3*elementData_sum_edge_qy
					- cat_length*left_edge_flux.h
					);

			/*
			 * QX
			 */
			T hyp_edge_qx =
					timestep_six_div_h2*
					(
							(T)triangle_area_MUL_two_div_h_MUL_1_3*
							(
								sum_elementData_edges_qx2_MUL_inv_edges_h +
								gravity_potential +
								sum_elementData_edges_qx_qy_MUL_inv_edges_h
							)
							- hyp_length*hyp_edge_flux.qx
					);

			T right_edge_qx =
					timestep_six_div_h2*(
						(T)triangle_area_MUL_two_div_h_MUL_1_3*(
								- gravity_potential
								- sum_elementData_edges_qx2_MUL_inv_edges_h
							)
							- cat_length*right_edge_flux.qx
					);

			T left_edge_qx =
					+ timestep_six_div_h2*(
							(T)triangle_area_MUL_two_div_h_MUL_1_3*(
								-sum_elementData_edges_qx_qy_MUL_inv_edges_h
							)
							- cat_length*left_edge_flux.qx
					);

			/*
			 * QY
			 */
			T hyp_edge_qy =
					timestep_six_div_h2*(
							triangle_area_MUL_two_div_h_MUL_1_3*(
								sum_elementData_edges_qx_qy_MUL_inv_edges_h +
								sum_elementData_edges_qy2_MUL_inv_edges_h +
								gravity_potential
							)
							- hyp_length*hyp_edge_flux.qy
					);

			T right_edge_qy =
					timestep_six_div_h2*(
							- triangle_area_MUL_two_div_h_MUL_1_3*sum_elementData_edges_qx_qy_MUL_inv_edges_h
							- cat_length*right_edge_flux.qy
					);


			T left_edge_qy =
					timestep_six_div_h2*(
							-triangle_area_MUL_two_div_h_MUL_1_3*(
									sum_elementData_edges_qy2_MUL_inv_edges_h +
									gravity_potential
							)
							- cat_length*left_edge_flux.qy
					);

			elementData->left_edge.h += left_edge_h;
			elementData->left_edge.qx += left_edge_qx;
			elementData->left_edge.qy += left_edge_qy;
			elementData->hyp_edge.h += hyp_edge_h;
			elementData->hyp_edge.qx += hyp_edge_qx;
			elementData->hyp_edge.qy += hyp_edge_qy;
			elementData->right_edge.h += right_edge_h;
			elementData->right_edge.qx += right_edge_qx;
			elementData->right_edge.qy += right_edge_qy;

	#define unrotate(qx, qy)									\
			tmp = (-left_normal_y)*qx + (-left_normal_x)*qy;	\
			qy = (left_normal_x)*qx + (-left_normal_y)*qy;		\
			qx = tmp;

			unrotate(elementData->hyp_edge.qx, elementData->hyp_edge.qy)
			unrotate(elementData->left_edge.qx, elementData->left_edge.qy)
			unrotate(elementData->right_edge.qx, elementData->right_edge.qy)
	#undef unrotate
		}
	}


	inline void elementAction_BEE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *right_edge,
			CTsunamiEdgeData *left_edge
	);

	inline void elementAction_BBB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element
	);


	inline void elementAction_EEB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge,
			CTsunamiEdgeData *right_edge
	);

	inline void elementAction_EBE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge,
			CTsunamiEdgeData *left_edge
	);


	inline void elementAction_EBB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *hyp_edge
	);


	inline void elementAction_BBE(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *left_edge
	);


	inline void elementAction_BEB(
			TVertexScalar hyp_normal_x, TVertexScalar hyp_normal_y,
			TVertexScalar right_normal_x, TVertexScalar right_normal_y,
			TVertexScalar left_normal_x, TVertexScalar left_normal_y,
			int depth,
			CTsunamiElementData *element,
			CTsunamiEdgeData *right_edge
	);

	inline void postTraversatorSetupPartition(CTriangle_Factory &triangleFactory)
	{
	}
};

#include "CEdgeComm_Tsunami_1stOrder_BoundaryConditions.hpp"

}
}



#endif /* CEDGECOMM_TESTNORMAL_HPP_ */
