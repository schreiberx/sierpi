/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *  Created on: Jan 12, 2011
 *      Author: schreibm
 */

#ifndef TRAVERSATORS_CEDGE_COMM_HPP_
#define TRAVERSATORS_CEDGE_COMM_HPP_

#include "CFBStacks.hpp"

namespace sierp {
namespace travs {

/**
 * adaptive refinement
 */
template<typename TKernelClass, typename TElementData, typename TEdgeData>
class CEdgeComm {

	/*
	 * pointer to
	 */
	CStack<char> *structure_stack_out;

	CStack<char>::CLifoIter structure_lifo_in;

	typename CStack<TElementData>::CLifoIter element_data_lifo;
	typename CStack<TElementData>::CFifoIter element_data_fifo;

	CStack<TEdgeData> *edge_data_comm_left_edge_stack;
	CStack<TEdgeData> *edge_data_comm_right_edge_stack;

	CStack<TEdgeData> *edge_comm_buffer;

	/*
	 *Include both classes, with and without parameters
	 */
public:
	TKernelClass kernelClass;
#include "CEdgeComm_Original.hpp"
#include "CEdgeComm_Parameters4.hpp"

	void action(
			CFBStacks<char> &p_structure_stacks, // forward and backward stacks
			CFBStacks<TElementData> &p_element_data_stacks,
			CStack<TEdgeData> &p_edge_data_comm_left_edge_stack, CStack<
					TEdgeData> &p_edge_data_comm_right_edge_stack, CStack<
					TEdgeData> &p_edge_comm_buffer, int useParameters) {
#ifndef NDEBUG
		assert(p_structure_stacks.getSize() != 0);
		assert(p_element_data_stacks.getSize() != 0);
		assert(p_edge_data_comm_left_edge_stack.getSize() != 0);
		assert(p_edge_data_comm_right_edge_stack.getSize() != 0);
		assert(p_edge_comm_buffer.getSize() != 0);
#endif
		assert(p_structure_stacks.direction == p_element_data_stacks.direction);
		assert(p_structure_stacks.direction == CFBStacks<char>::FORWARD);

		/*
		 * for the first traversal, the backward stack (which is empty so far) is set to be the output structure stack
		 */
		structure_stack_out = &p_structure_stacks.backward;
		assert(structure_stack_out->isEmpty());

		/*
		 * use the forward structure stack as input
		 */
		structure_lifo_in.setup(p_structure_stacks.forward);

		/*
		 * setup pointers we need for the edge communication
		 */
		edge_data_comm_left_edge_stack = &p_edge_data_comm_left_edge_stack;
		edge_data_comm_right_edge_stack = &p_edge_data_comm_right_edge_stack;

		/*
		 * also setup the edge communication buffer
		 */
		edge_comm_buffer = &p_edge_comm_buffer;

		/*
		 * setup the element data input class
		 */
		element_data_lifo.setup(p_element_data_stacks.forward);

		kernelClass.pre_hook(p_structure_stacks.forward);

		//Case without parameters
		if (useParameters == 0) {
			Ke_nbb_forward();
			He_obb_forward();
		}
		//Function with parameters
		else {
			Ke_forward('n', 'b', 'b');
			He_forward('o', 'b', 'b');
		}
		assert(edge_data_comm_left_edge_stack->isEmpty());
		assert(edge_data_comm_right_edge_stack->isEmpty());
		assert(structure_lifo_in.isEmpty());
		assert(element_data_lifo.isEmpty());

		/**
		 * setup input structure stack to be the backward traversal created in the previous recursions
		 */
		structure_lifo_in.setup(&p_structure_stacks.backward);
		element_data_fifo.setup(p_element_data_stacks.forward);
		if (useParameters == 0) {
			He_nbb_backward();
			Ke_obb_backward();
		} else {
			He_backward('n', 'b', 'b');
			Ke_backward('o', 'b', 'b');
		}
		assert(edge_comm_buffer->isEmpty());
		assert(edge_data_comm_left_edge_stack->isEmpty());
		assert(edge_data_comm_right_edge_stack->isEmpty());
		assert(structure_lifo_in.isEmpty());
		assert(element_data_fifo.isEmpty());

		structure_stack_out->clear();
		kernelClass.post_hook(p_structure_stacks.forward);
	}
};

}
}

#endif /* CSTRUCTURE_ADAPTIVEREFINEMENT_HPP_ */
