/*
 * CJumpTableHelpers.hpp
 *
 *  Created on: Apr 1, 2011
 *      Author: schreibm
 */

// This methods are directly included into traversators
//   => don't avoid duplicates since they only exist in the scope of classes

/**
 * PARALLELIZATION STUFF
 */

#ifndef NDEBUG
int recursionTableMagicCode;
#endif

TRecursiveMethod getRecursiveFunction(TRecursiveMethod &func)
{
	assert(recursionTableMagicCode == 0x1505);

	for (int i = 0; i < number_of_recursion_functions; i++)
		if (recursionTable[i].parent_method == func)
			return recursionTable[i].parent_method;

	assert(false);
	return NULL;
}


TRecursiveMethod getRecursiveFunction(CTriangle_Factory &triangleFactory)
{
	assert(recursionTableMagicCode == 0x1505);

	for (int i = 0; i < number_of_recursion_functions; i++)
		if (recursionTable[i].parent_method == triangleFactory)
			return recursionTable[i].parent_method;

	assert(false);
	return NULL;
}



int getRecursiveFunctionTableIndex_WithConvertedEdgeTypes(
		CTriangle_Factory &p_triangleFactory,
		typename CTriangle_Enums::EEdgeType old_edge_type,
		typename CTriangle_Enums::EEdgeType new_edge_type
)
{
	assert(recursionTableMagicCode == 0x1505);

	CTriangle_Factory newTriangleFactory = p_triangleFactory;

	if (newTriangleFactory.edgeTypes.hyp == old_edge_type)
		newTriangleFactory.edgeTypes.hyp = new_edge_type;
	if (newTriangleFactory.edgeTypes.right == old_edge_type)
		newTriangleFactory.edgeTypes.right = new_edge_type;
	if (newTriangleFactory.edgeTypes.left == old_edge_type)
		newTriangleFactory.edgeTypes.left = new_edge_type;


	return getRecursiveFunctionTableIndex(newTriangleFactory);
}


int getRecursiveFunctionTableIndex(TRecursiveMethod &p_triangleFactory)
{
	assert(recursionTableMagicCode == 0x1505);

	for (int i = 0; i < number_of_recursion_functions; i++)
		if (recursionTable[i].parent_method == p_triangleFactory)
			return i;

	std::cout << "FATAL ERROR: RECURSIVE FUNCTION IDX NOT FOUND (from recursive method)!" << std::endl;
	std::cout << p_triangleFactory;
	std::cout << std::endl;

	assert(false);
	return -1;
}


int getRecursiveFunctionTableIndex(CTriangle_Factory &p_triangleFactory)
{
	assert(recursionTableMagicCode == 0x1505);

	for (int i = 0; i < number_of_recursion_functions; i++)
	{
		if (recursionTable[i] == p_triangleFactory)
			return i;
	}

	std::cout << "FATAL ERROR: RECURSIVE FUNCTION IDX NOT FOUND (from triangle factory)!" << std::endl;
	std::cout << p_triangleFactory << std::endl;

	assert(false);
	return -1;
}


//#endif /* CJUMPTABLEHELPERS_HPP_ */
