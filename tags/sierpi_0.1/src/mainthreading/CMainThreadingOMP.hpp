/*
 * CMainThreadingOMP.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: schreibm
 */

#ifndef CMAINTHREADINGOMP_HPP_
#define CMAINTHREADINGOMP_HPP_

#include <omp.h>
#include <iostream>


#include "CMainThreading_Interface.hpp"

class CMainThreading	: public CMainThreading_Interface
{
public:
	void threading_setup()
	{
		int n = getNumberOfThreadsToUse();

		if (n == -1)
		{
			n = omp_get_max_threads();
			setNumberOfThreadsToUse(n);
		}
		else
		{
			omp_set_num_threads(n);
		}

		if (getVerboseLevel() > 5)
			std::cout << "omp_get_max_threads(): " << (int)omp_get_max_threads() << std::endl;
	}

	void threading_simulationLoop()
	{
		bool continue_simulation = true;

#pragma omp parallel
#pragma omp single nowait
		do
		{
			continue_simulation = simulationLoopIteration();

		} while(continue_simulation);
	}

	bool threading_simulationLoopIteration()
	{
		bool retval;

#pragma omp parallel
#pragma omp single nowait
		retval = simulationLoopIteration();

		return retval;
	}

	void threading_shutdown()
	{
	}

	void threading_setNumThreads(int i)
	{
		omp_set_num_threads(i);
	}

	virtual ~CMainThreading()
	{
	}
};


#endif /* CMAINTHREADINGOMP_HPP_ */
