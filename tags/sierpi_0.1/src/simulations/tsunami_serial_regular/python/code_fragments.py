
# file header, enums and setup functions
code_head = """
/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Dec 06, 2011
 *      Author: kleinfl
 */

enum edge_type {LEFT, RIGHT, INNER, BORDER};
enum regular_array_format {NEIGHBOR_INDEX_LEFT, NEIGHBOR_INDEX_HYP, NEIGHBOR_INDEX_RIGHT,
		EDGE_TYPE_LEFT, EDGE_TYPE_HYP, EDGE_TYPE_RIGHT, NORMAL_HYP, ELEMENT_INDEX};

namespace sierpi
{
namespace travs
{
//enum edge_type {LEFT, RIGHT, INNER, BORDER};

class CSimulationTsunami_Serial_Regular_Traverse_Table
{
	/**
	 * \\brief setup parameters for simulation
	 */
public:
	inline void setup_WithParameters(
			TTsunamiDataScalar i_timestep_size,			///< timestep size
			TTsunamiDataScalar i_square_side_length,	///< length of a square (a single catheti)
			TTsunamiDataScalar i_gravity = 9.81			///< gravitation constant
	)
	{
		cEdgeComm_Tsunami.setParameters(i_timestep_size, i_square_side_length, i_gravity);
	}

	/**
	 * \\brief setup kernel boundary condition
	 */
public:
	inline void setup_WithParameters_KernelBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		cEdgeComm_Tsunami.setBoundaryDirichlet(p_boundary_dirichlet);
	}

public:
	CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *cStacks;
	sierpi::kernels::CEdgeComm_Tsunami cEdgeComm_Tsunami;
	int refinementDepth;
	typedef typename sierpi::kernels::CEdgeComm_Tsunami::TReduceValue TReduceValue;
	CFluxSolver<TTsunamiDataScalar> fluxSolver;
	CTsunamiEdgeData *flux_array;
"""

# body to be filled by the script
code_body = """

public:
	// format: [ [neighbor_index_left, neighbor_index_hyp, neighbor_index_right, edge_type_left, edge_type_hyp, edge_type_right, normal_hyp] ]
	%s

};
}
}
"""

# standard elementAction code
code_elementAction = """
	inline void call_elementAction(
			int edge_type_hyp,
			int edge_type_right,
			int edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// edges on border?
		bool hyp_border = edge_type_hyp != INNER;
		bool right_border = edge_type_right != INNER;
		bool left_border = edge_type_left != INNER;

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		if (!hyp_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_hyp]),
					&edge_hyp
				);

		if (!right_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_right]),
					&edge_right
				);

		if (!left_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_left]),
					&edge_left
				);

		// combine bool values to flag-int for switch statement
		int border_flags = (((int) hyp_border) << 2) + (((int) right_border) << 1) + ((int) left_border);

		// call elementAction_XXX depending on edge types
		switch (border_flags){
			case 0:
				cEdgeComm_Tsunami.elementAction_EEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right, &edge_left
					);
				break;

			case 1:
				cEdgeComm_Tsunami.elementAction_EEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right
					);
				break;

			case 2:
				cEdgeComm_Tsunami.elementAction_EBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp,
						&edge_left
					);
				break;

			case 3:
				cEdgeComm_Tsunami.elementAction_EBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_hyp);
				break;
			case 4:
				cEdgeComm_Tsunami.elementAction_BEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right, &edge_left);
				break;
			case 5:
				cEdgeComm_Tsunami.elementAction_BEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right);
				break;
			case 6:
				cEdgeComm_Tsunami.elementAction_BBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_left);
				break;
			case 7:
				cEdgeComm_Tsunami.elementAction_BBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element);
				break;
		}
	}
"""

# reduced elementAction for inner elements
code_elementAction_inner = """
	inline void call_elementAction_inner(
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_hyp]),
				&edge_hyp
			);

		cEdgeComm_Tsunami.computeEdgeCommData_Left(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_right]),
				&edge_right
			);

		cEdgeComm_Tsunami.computeEdgeCommData_Right(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_left]),
				&edge_left
			);

		cEdgeComm_Tsunami.elementAction_EEE(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				current_element,
				&edge_hyp, &edge_right, &edge_left
			);
	}
"""
# elementAction with a pointer argument instead of integers
code_elementAction_pointer = """
	inline void call_elementAction(int *element_information){
		int edge_type_hyp = element_information[EDGE_TYPE_HYP];
		int edge_type_right = element_information[EDGE_TYPE_RIGHT];
		int edge_type_left = element_information[EDGE_TYPE_LEFT];
		int index_hyp = element_information[NEIGHBOR_INDEX_HYP];
		int index_right = element_information[NEIGHBOR_INDEX_RIGHT];
		int index_left = element_information[NEIGHBOR_INDEX_LEFT];
		int current_index = element_information[ELEMENT_INDEX];
		int normal_hyp = element_information[NORMAL_HYP];
	
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// edges on border?
		bool hyp_border = edge_type_hyp != INNER;
		bool right_border = edge_type_right != INNER;
		bool left_border = edge_type_left != INNER;

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		if (!hyp_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_hyp]),
					&edge_hyp
				);

		if (!right_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_right]),
					&edge_right
				);

		if (!left_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_left]),
					&edge_left
				);


		// combine bool values to flag-int for switch statement
		int border_flags = (((int) hyp_border) << 2) + (((int) right_border) << 1) + ((int) left_border);

		// call elementAction_XXX depending on edge types
		switch (border_flags){
			case 0:
				cEdgeComm_Tsunami.elementAction_EEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right, &edge_left
					);
				break;

			case 1:
				cEdgeComm_Tsunami.elementAction_EEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right
					);
				break;

			case 2:
				cEdgeComm_Tsunami.elementAction_EBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp,
						&edge_left
					);
				break;

			case 3:
				cEdgeComm_Tsunami.elementAction_EBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_hyp);
				break;
			case 4:
				cEdgeComm_Tsunami.elementAction_BEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right, &edge_left);
				break;
			case 5:
				cEdgeComm_Tsunami.elementAction_BEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right);
				break;
			case 6:
				cEdgeComm_Tsunami.elementAction_BBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_left);
				break;
			case 7:
				cEdgeComm_Tsunami.elementAction_BBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element);
				break;
		}
	}
"""

# inner elementAction with pointer argument
code_elementAction_inner_pointer = """
	inline void call_elementAction_inner(int *element_information){
		int index_hyp = element_information[NEIGHBOR_INDEX_HYP];
		int index_right = element_information[NEIGHBOR_INDEX_RIGHT];
		int index_left = element_information[NEIGHBOR_INDEX_LEFT];
		int current_index = element_information[ELEMENT_INDEX];
		int normal_hyp = element_information[NORMAL_HYP];
	
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_hyp]),
				&edge_hyp
			);

		cEdgeComm_Tsunami.computeEdgeCommData_Left(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_right]),
				&edge_right
			);

		cEdgeComm_Tsunami.computeEdgeCommData_Right(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				&(cStacks->element_data_stacks.forward[index_left]),
				&edge_left
			);

		cEdgeComm_Tsunami.elementAction_EEE(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				current_element,
				&edge_hyp, &edge_right, &edge_left
			);
	}
"""

# elementAction with edge types as template parameters
code_elementAction_template = """
	template<int edge_type_hyp, int edge_type_right, int edge_type_left, int border_flags> inline void call_elementAction(
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// neighbor normals
		int normal_h_hyp = (normal_hyp + 4) & 7;
		int normal_h_left = (normal_h_hyp + 3) & 7;
		int normal_h_right = (normal_h_hyp + 5) & 7;
		int normal_r_left = (normal_right + 4) & 7;
		int normal_r_hyp = (normal_r_left + 5) & 7;
		int normal_r_right = (normal_r_left + 2) & 7;
		int normal_l_right = (normal_left + 4) & 7;
		int normal_l_hyp = (normal_l_right + 3) & 7;
		int normal_l_left = (normal_l_right + 6) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		if (edge_type_hyp == INNER)
			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_h_hyp][0], CTriangle_Normals::normal_table[normal_h_hyp][1],
					CTriangle_Normals::normal_table[normal_h_right][0], CTriangle_Normals::normal_table[normal_h_right][1],
					CTriangle_Normals::normal_table[normal_h_left][0], CTriangle_Normals::normal_table[normal_h_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_hyp]),
					&edge_hyp
				);

		if (edge_type_right == INNER)
			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_r_hyp][0], CTriangle_Normals::normal_table[normal_r_hyp][1],
					CTriangle_Normals::normal_table[normal_r_right][0], CTriangle_Normals::normal_table[normal_r_right][1],
					CTriangle_Normals::normal_table[normal_r_left][0], CTriangle_Normals::normal_table[normal_r_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_right]),
					&edge_right
				);

		if (edge_type_left == INNER)
			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_l_hyp][0], CTriangle_Normals::normal_table[normal_l_hyp][1],
					CTriangle_Normals::normal_table[normal_l_right][0], CTriangle_Normals::normal_table[normal_l_right][1],
					CTriangle_Normals::normal_table[normal_l_left][0], CTriangle_Normals::normal_table[normal_l_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_left]),
					&edge_left
				);

		// call elementAction_XXX depending on edge types
		if (border_flags == 0){
			cEdgeComm_Tsunami.elementAction_EEE(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&edge_hyp, &edge_right, &edge_left
				);
		}

		else if (border_flags == 1){
			cEdgeComm_Tsunami.elementAction_EEB(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&edge_hyp, &edge_right
				);
		}

		else if (border_flags == 2){
			cEdgeComm_Tsunami.elementAction_EBE(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&edge_hyp,
					&edge_left
				);
		}

		else if (border_flags == 3){
			cEdgeComm_Tsunami.elementAction_EBB(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth, current_element, &edge_hyp);
		}
		
		else if (border_flags == 4){
			cEdgeComm_Tsunami.elementAction_BEE(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth, current_element, &edge_right, &edge_left);
		}
		
		else if (border_flags == 5){
			cEdgeComm_Tsunami.elementAction_BEB(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth, current_element, &edge_right);
		}
		
		else if (border_flags == 6){
			cEdgeComm_Tsunami.elementAction_BBE(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth, current_element, &edge_left);
		}
		
		else if (border_flags == 7){
			cEdgeComm_Tsunami.elementAction_BBB(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth, current_element);
		}
	}
"""

# main traverse function
code_traverse = """
	inline TReduceValue traverse(CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *i_cSimulationStacks,
			int depth, TTsunamiDataScalar timestep_size){
		cStacks = i_cSimulationStacks;
		refinementDepth = depth;
		// update adaptive timestep size
		cEdgeComm_Tsunami.setTimestepSize(timestep_size);
		
		switch(depth){%s
		default:
			break;
		}
		cStacks->element_data_stacks.backward.setStackElementCounter(cStacks->element_data_stacks.forward.getNumberOfElementsOnStack());
		cStacks->element_data_stacks.forward.setStackElementCounter(0);
		// swap forward/backward stacks for next traversal
		cStacks->element_data_stacks.swap();
		
		return cEdgeComm_Tsunami.getReduceValue();
	}"""

# elementAction with edge types as template parameters
code_elementAction_template_seperatedFlux = """
	template<int edge_type_hyp, int edge_type_right, int edge_type_left> inline void call_elementAction(
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// neighbor normals
		int normal_h_hyp = (normal_hyp + 4) & 7;
		int normal_h_left = (normal_h_hyp + 3) & 7;
		int normal_h_right = (normal_h_hyp + 5) & 7;
		int normal_r_left = (normal_right + 4) & 7;
		int normal_r_hyp = (normal_r_left + 5) & 7;
		int normal_r_right = (normal_r_left + 2) & 7;
		int normal_l_right = (normal_left + 4) & 7;
		int normal_l_hyp = (normal_l_right + 3) & 7;
		int normal_l_left = (normal_l_right + 6) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData hyp_edgeData_outer, right_edgeData_outer, left_edgeData_outer;
		CTsunamiEdgeData hyp_edgeData_inner, right_edgeData_inner, left_edgeData_inner;
			
		// compute flux data and store it
		CTsunamiEdgeData hyp_edge_flux_inner, left_edge_flux_inner, right_edge_flux_inner;
		CTsunamiEdgeData hyp_edge_flux_outer, left_edge_flux_outer, right_edge_flux_outer;

		// HYPOTENUSE EDGE
		if (index_hyp > current_index or edge_type_hyp != INNER){
			if (edge_type_hyp == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
						CTriangle_Normals::normal_table[normal_h_hyp][0], CTriangle_Normals::normal_table[normal_h_hyp][1],
						CTriangle_Normals::normal_table[normal_h_right][0], CTriangle_Normals::normal_table[normal_h_right][1],
						CTriangle_Normals::normal_table[normal_h_left][0], CTriangle_Normals::normal_table[normal_h_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_hyp]),
						&hyp_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Hyp(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&hyp_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&hyp_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(hyp_edgeData_inner, hyp_edgeData_outer, hyp_edge_flux_inner, hyp_edge_flux_outer);

			flux_array[current_index * 3] = hyp_edge_flux_inner;
			if (edge_type_hyp == INNER) 
				flux_array[index_hyp * 3] = hyp_edge_flux_outer;
		}

		// RIGHT EDGE
		if (index_right > current_index or edge_type_right != INNER){
			if (edge_type_right == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Left(
						CTriangle_Normals::normal_table[normal_r_hyp][0], CTriangle_Normals::normal_table[normal_r_hyp][1],
						CTriangle_Normals::normal_table[normal_r_right][0], CTriangle_Normals::normal_table[normal_r_right][1],
						CTriangle_Normals::normal_table[normal_r_left][0], CTriangle_Normals::normal_table[normal_r_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_right]),
						&right_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Right(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&right_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&right_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(right_edgeData_inner, right_edgeData_outer, right_edge_flux_inner, right_edge_flux_outer);

			flux_array[current_index * 3 + 1] = right_edge_flux_inner;
			if (edge_type_right == INNER) 
				flux_array[index_right * 3 + 2] = right_edge_flux_outer;
		}

		// LEFT EDGE
		if (index_left > current_index or edge_type_left != INNER){
			if (edge_type_left == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Right(
						CTriangle_Normals::normal_table[normal_l_hyp][0], CTriangle_Normals::normal_table[normal_l_hyp][1],
						CTriangle_Normals::normal_table[normal_l_right][0], CTriangle_Normals::normal_table[normal_l_right][1],
						CTriangle_Normals::normal_table[normal_l_left][0], CTriangle_Normals::normal_table[normal_l_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_left]),
						&left_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Left(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&left_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&left_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(left_edgeData_inner, left_edgeData_outer, left_edge_flux_inner, left_edge_flux_outer);

			flux_array[current_index * 3 + 2] = left_edge_flux_inner;
			if (edge_type_left == INNER) 
				flux_array[index_left * 3 + 1] = left_edge_flux_outer;
		}

		cEdgeComm_Tsunami.elementAction(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				current_element,
				&flux_array[current_index * 3],
				&flux_array[current_index * 3 + 1],
				&flux_array[current_index * 3 + 2]
			);
	}
"""

# ##test### reduced elementAction for inner elements
code_elementAction_inner_seperatedFlux = """
	inline void call_elementAction_inner(
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// neighbor normals
		int normal_h_hyp = (normal_hyp + 4) & 7;
		int normal_h_left = (normal_h_hyp + 3) & 7;
		int normal_h_right = (normal_h_hyp + 5) & 7;
		int normal_r_left = (normal_right + 4) & 7;
		int normal_r_hyp = (normal_r_left + 5) & 7;
		int normal_r_right = (normal_r_left + 2) & 7;
		int normal_l_right = (normal_left + 4) & 7;
		int normal_l_hyp = (normal_l_right + 3) & 7;
		int normal_l_left = (normal_l_right + 6) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData hyp_edgeData_outer;
		CTsunamiEdgeData right_edgeData_outer;
		CTsunamiEdgeData left_edgeData_outer;

		cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_h_hyp][0], CTriangle_Normals::normal_table[normal_h_hyp][1],
					CTriangle_Normals::normal_table[normal_h_right][0], CTriangle_Normals::normal_table[normal_h_right][1],
					CTriangle_Normals::normal_table[normal_h_left][0], CTriangle_Normals::normal_table[normal_h_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_hyp]),
					&hyp_edgeData_outer
				);
		cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_r_hyp][0], CTriangle_Normals::normal_table[normal_r_hyp][1],
					CTriangle_Normals::normal_table[normal_r_right][0], CTriangle_Normals::normal_table[normal_r_right][1],
					CTriangle_Normals::normal_table[normal_r_left][0], CTriangle_Normals::normal_table[normal_r_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_right]),
					&right_edgeData_outer
				);
		cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_l_hyp][0], CTriangle_Normals::normal_table[normal_l_hyp][1],
					CTriangle_Normals::normal_table[normal_l_right][0], CTriangle_Normals::normal_table[normal_l_right][1],
					CTriangle_Normals::normal_table[normal_l_left][0], CTriangle_Normals::normal_table[normal_l_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_left]),
					&left_edgeData_outer
				);
			
		// compute flux data and store it
		CTsunamiEdgeData hyp_edge_flux_inner, left_edge_flux_inner, right_edge_flux_inner;
		CTsunamiEdgeData hyp_edge_flux_outer, left_edge_flux_outer, right_edge_flux_outer;
		CTsunamiEdgeData hyp_edgeData_inner, right_edgeData_inner, left_edgeData_inner;

		// HYPOTENUSE EDGE
		if (index_hyp > current_index){
			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&hyp_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(hyp_edgeData_inner, hyp_edgeData_outer, hyp_edge_flux_inner, hyp_edge_flux_outer);
			
			flux_array[current_index * 3] = hyp_edge_flux_inner;
			flux_array[index_hyp * 3] = hyp_edge_flux_outer;
		}

		// RIGHT EDGE
		if (index_right > current_index){
			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&right_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(right_edgeData_inner, right_edgeData_outer, right_edge_flux_inner, right_edge_flux_outer);

			flux_array[current_index * 3 + 1] = right_edge_flux_inner;
			flux_array[index_right * 3 + 2] = right_edge_flux_outer;
		}

		// LEFT EDGE
		if (index_left > current_index){
			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&left_edgeData_inner
				);

			cEdgeComm_Tsunami.computeNetUpdates(left_edgeData_inner, left_edgeData_outer, left_edge_flux_inner, left_edge_flux_outer);

			flux_array[current_index * 3 + 2] = left_edge_flux_inner;
			flux_array[index_left * 3 + 1] = left_edge_flux_outer;
		}

		cEdgeComm_Tsunami.elementAction(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				current_element,
				&flux_array[current_index * 3],
				&flux_array[current_index * 3 + 1],
				&flux_array[current_index * 3 + 2]
			);
	}
"""