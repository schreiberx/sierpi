/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *
 *
 *  Created on: Nov 20, 2011
 *      Author: kleinfl
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_REGULAR_TRAVERSE_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_REGULAR_TRAVERSE_HPP_


#include "../tsunami_common/types/CTsunamiTypes.hpp"
#include "../../libsierpi/triangle/CTriangle_Normals.hpp"


#include "../tsunami_common/CSimulationCommon.hpp"

#include "../tsunami_common/kernels/simulation/CEdgeComm_Tsunami.hpp"


//#include "CSimulationTsunami_Serial_Regular_Traverse_Table.hpp"


namespace sierpi
{
namespace travs
{
// scons --cppcompiler=gnu --threading=off --gui=on --mode=debug --simulation=tsunami_serial_regular -j8
// scons --cppcompiler=gnu --threading=off --gui=off --mode=debug --simulation=tsunami_serial_regular -j8
// scons --cppcompiler=gnu --threading=off --gui=off --mode=release --simulation=tsunami_serial_regular --serial_regular_type=table -j8

/**
 * edge type (data found on...): left stack, right stack, border or inner domain triangle
 */
enum edge_type {LEFT, RIGHT, BORDER, INNER};


class CSimulationTsunami_Serial_Regular_Traverse_Recursive
{

	/**
	 * \brief setup parameters for simulation
	 */
public:
	inline void setup_WithParameters(
			TTsunamiDataScalar i_timestep_size,			///< timestep size
			TTsunamiDataScalar i_square_side_length,	///< length of a square (a single catheti)
			TTsunamiDataScalar i_gravity = 9.81			///< gravitation constant
	)
	{
		cEdgeComm_Tsunami.setParameters(i_timestep_size, i_square_side_length, i_gravity);
		//cTsunami_EdgeComm_Serial_Regular_Traverse_Table.setup_WithParameters(i_timestep_size, i_square_side_length, i_gravity);
	}

	/**
	 * \brief setup kernel boundary condition
	 */
public:
	inline void setup_WithParameters_KernelBoundaryDirichlet(
			const CTsunamiEdgeData *p_boundary_dirichlet
	)
	{
		cEdgeComm_Tsunami.setBoundaryDirichlet(p_boundary_dirichlet);
		//cTsunami_EdgeComm_Serial_Regular_Traverse_Table.setup_WithParameters_KernelBoundaryDirichlet(p_boundary_dirichlet);
	}

public:
	CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *cStacks;
	sierpi::kernels::CEdgeComm_Tsunami cEdgeComm_Tsunami;
	//sierpi::travs::CSimulationTsunami_Serial_Regular_Traverse_Table cTsunami_EdgeComm_Serial_Regular_Traverse_Table;
	int refinementDepth;
	typedef typename sierpi::kernels::CEdgeComm_Tsunami::TReduceValue TReduceValue;
	//CStack<CTsunamiEdgeData> flux_stack_inner;
	//CStack<CTsunamiEdgeData> flux_stack_left;
	//CStack<CTsunamiEdgeData> flux_stack_right;
	CTsunamiEdgeData *flux_array;
	CFluxSolver<TTsunamiDataScalar> fluxSolver;


	/**
	 * calculates domain size for given depth
	 */
	inline int size(int depth){
		return 1 << depth;
	}

	/**
	 * determines if the searched index lies in the first half of the current triangle
	 */
	inline bool first_half(int index, int size, int offset){
		return (index - offset < (size >> 1));
	}

	/**
	 * determines if triangle has odd refinement depth
	 */
	inline bool odd_depth(int depth){
		return (depth & 1) == 1;
	}

	/**
	 * calculates normal direction of refined triangle
	 */
	inline int calc_normal(int normal, bool odd, bool first_half){
		if (!((first_half && !odd) || (!first_half && odd)))
			return (normal + 3) & 7;
		return (normal + 5) & 7;
	}

	/**
	 * calculates index gap on stacks for second partition of the refined triangle
	 */
	inline int calc_stack_gap(int depth){
		if (odd_depth(depth))
			return size((depth - 1) >> 1);
		return size((depth >> 1) - 1);
	}

	/**
	 * determines mid element of left stack
	 * used only for initial starting indices
	 */
	inline int get_mid_left(int depth){
		if (odd_depth(depth))
			return size((depth - 1) >> 1);
		return size(depth >> 1);
	}

	/**
	 * recursively determines neighbor indices for a given triangle search index
	 * starting with the whole search domain, partitioning to half size in every step
	 */
	void triangle_recursion_H_EVEN(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, false, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp + triangle_size_new; break;
				case RIGHT: index_left_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_right_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_K_ODD(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);


			// determine new normal
			normal_new = calc_normal(normal, false, false);

			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp; break;
				case RIGHT: index_right_new = index_hyp; break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local;

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_V_ODD(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}


	void triangle_recursion_K_EVEN(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, false, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp + triangle_size_new; break;
				case RIGHT: index_left_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_right_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_V_ODD(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);


			// determine new normal
			normal_new = calc_normal(normal, false, false);

			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp; break;
				case RIGHT: index_right_new = index_hyp; break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local;

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_H_ODD(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}


	void triangle_recursion_V_EVEN(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, false, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp + triangle_size_new; break;
				case RIGHT: index_left_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_right_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_K_ODD(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);


			// determine new normal
			normal_new = calc_normal(normal, false, false);

			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp; break;
				case RIGHT: index_right_new = index_hyp; break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local;

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_H_ODD(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}


	void triangle_recursion_H_ODD(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, true, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp + triangle_size_new; break;
				case LEFT: index_right_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_K_EVEN(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

			// determine new normal
			normal_new = calc_normal(normal, true, false);

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp; break;
				case LEFT: index_left_new = index_hyp; break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
			}
			index_right_new = index_local;

			// recursion call with new data
			triangle_recursion_V_EVEN(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}


	void triangle_recursion_K_ODD(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, true, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp + triangle_size_new; break;
				case LEFT: index_right_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_V_EVEN(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

			// determine new normal
			normal_new = calc_normal(normal, true, false);

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp; break;
				case LEFT: index_left_new = index_hyp; break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
			}
			index_right_new = index_local;

			// recursion call with new data
			triangle_recursion_H_EVEN(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}


	void triangle_recursion_V_ODD(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int triangle_size,
			int index_local,
			int normal,
			int depth
	){
		/**
		 * further refinement possible: descend into half-size partition
		 */
		if (triangle_size > 1){
			// new variables for next recursion step
			int index_hyp_new, index_right_new, index_left_new, normal_new;
			int triangle_size_new = triangle_size >> 1;
			int index_local_new = index_local;

			// determine new normal
			normal_new = calc_normal(normal, true, true);

			/**
			 * determine new triangle type, edge types and starting indices of neighbor partitions
			 * depending on current triangle type, following the mapping
			 * H -> (V',K'); K -> (H',V'); V -> (H',K'); H' -> (V,K); K' -> (H,V); V' -> (H,K)
			 */
			switch (edge_type_hyp){
				case INNER: index_right_new = index_hyp + triangle_size_new; break;
				case LEFT: index_right_new = index_hyp + calc_stack_gap(depth); break;
				case BORDER: index_right_new = 0; break;
				default:	break;
			}
			switch (edge_type_left){
				case INNER: index_hyp_new = index_left + triangle_size_new; break;
				case LEFT: index_hyp_new = index_right; break;
				case RIGHT: index_hyp_new = index_left; break;
				case BORDER: index_hyp_new = 0; break;
				default:	break;
			}
			index_left_new = index_local + triangle_size_new;

			// recursion call with new data
			triangle_recursion_K_EVEN(edge_type_left, edge_type_hyp, INNER,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

			// determine new normal
			normal_new = calc_normal(normal, true, false);

			// adjust local index
			index_local_new = index_local + triangle_size_new;

			switch (edge_type_hyp){
				case INNER: index_left_new = index_hyp; break;
				case LEFT: index_left_new = index_hyp; break;
				case BORDER: index_left_new = 0; break;
				default:	break;
			}
			switch (edge_type_right){
				case INNER: index_hyp_new = index_right; break;
				case LEFT: index_hyp_new = index_left; break;
				case RIGHT: index_hyp_new = index_right; break;
				case BORDER: index_hyp_new = 0; break;
			}
			index_right_new = index_local;

			// recursion call with new data
			triangle_recursion_H_EVEN(edge_type_right, INNER, edge_type_hyp,
					index_hyp_new, index_right_new, index_left_new,
					triangle_size_new, index_local_new, normal_new, depth-1);

		}
		/**
		 * recursion termination: size 1, no further refinement possible
		 * searched neighbor indices = start indices of neighbor partitions
		 */
		else{
			call_elementAction(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, index_local, normal);
		}
	}



	/**
	 * prepares resulting data and calls corresponding elementAction_XXX method
	 */
	inline void call_elementAction(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int search_index,
			int normal_hyp
	){
#if 1
		storeFlux(edge_type_hyp, edge_type_right, edge_type_left, index_hyp, index_right, index_left, search_index, normal_hyp);
		// invert indices to switch root triangle entrance from left to right
		/*int maxNum = cStacks->element_data_stacks.forward.getNumberOfElementsOnStack() - 1;
		index_hyp = maxNum - index_hyp;
		index_right = maxNum - index_right;
		index_left = maxNum - index_left;
		search_index = maxNum - search_index;*/
#else
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// neighbor normals
		int normal_h_hyp = (normal_hyp + 4) & 7;
		int normal_h_left = (normal_h_hyp + 3) & 7;
		int normal_h_right = (normal_h_hyp + 5) & 7;
		int normal_r_left = (normal_right + 4) & 7;
		int normal_r_hyp = (normal_r_left + 5) & 7;
		int normal_r_right = (normal_r_left + 2) & 7;
		int normal_l_right = (normal_left + 4) & 7;
		int normal_l_hyp = (normal_l_right + 3) & 7;
		int normal_l_left = (normal_l_right + 6) & 7;


		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[search_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(search_index, *current_element);

		// edges on border?
		bool hyp_border = edge_type_hyp != INNER;
		bool right_border = edge_type_right != INNER;
		bool left_border = edge_type_left != INNER;

		// combine bool values to flag-int for switch statement
		int border_flags = (((int) hyp_border) << 2) + (((int) right_border) << 1) + ((int) left_border);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData edge_hyp;
		CTsunamiEdgeData edge_right;
		CTsunamiEdgeData edge_left;

		if (!hyp_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					-CTriangle_Normals::normal_table[normal_h_hyp][0], -CTriangle_Normals::normal_table[normal_h_hyp][1],
					-CTriangle_Normals::normal_table[normal_h_right][0], -CTriangle_Normals::normal_table[normal_h_right][1],
					-CTriangle_Normals::normal_table[normal_h_left][0], -CTriangle_Normals::normal_table[normal_h_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_hyp]),
					&edge_hyp
				);

		if (!right_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					-CTriangle_Normals::normal_table[normal_r_hyp][0], -CTriangle_Normals::normal_table[normal_r_hyp][1],
					-CTriangle_Normals::normal_table[normal_r_right][0], -CTriangle_Normals::normal_table[normal_r_right][1],
					-CTriangle_Normals::normal_table[normal_r_left][0], -CTriangle_Normals::normal_table[normal_r_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_right]),
					&edge_right
				);

		if (!left_border)
			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					-CTriangle_Normals::normal_table[normal_l_hyp][0], -CTriangle_Normals::normal_table[normal_l_hyp][1],
					-CTriangle_Normals::normal_table[normal_l_right][0], -CTriangle_Normals::normal_table[normal_l_right][1],
					-CTriangle_Normals::normal_table[normal_l_left][0], -CTriangle_Normals::normal_table[normal_l_left][1],
					refinementDepth,
					&(cStacks->element_data_stacks.forward[index_left]),
					&edge_left
				);

		#if 0
			#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#if 1
				std::cout << "NORMAL DIRECTIONS (H,R,L): (" << normal_hyp << ", " << normal_right << ", " << normal_left << "), INDEX: " << search_index << std::endl;
				std::cout << "NEIGHBORS (H,R,L): (" << index_hyp << ", " << index_right << ", " << index_left << ")" << std::endl;
				std::cout << "EDGE H: " << edge_hyp << std::endl;
				std::cout << "EDGE R: " << edge_right << std::endl;
				std::cout << "EDGE L: " << edge_left << std::endl;
				std::cout <<
						CTriangle_Normals::normal_table[normal_hyp][0] << " " << CTriangle_Normals::normal_table[normal_hyp][1] << " " <<
						CTriangle_Normals::normal_table[normal_right][0] << " " << CTriangle_Normals::normal_table[normal_right][1] << " " <<
						CTriangle_Normals::normal_table[normal_left][0] << " " << CTriangle_Normals::normal_table[normal_left][1]
				<< std::endl;
#endif
				//std::cout << "INFO " << search_index << ": h " << current_element->hyp_edge.qy << ", l " << current_element->left_edge.qy << ", r " << current_element->right_edge.qx << std::endl;
			#endif
		#endif

		// call elementAction_XXX depending on edge types
		switch (border_flags){
			case 0:
				cEdgeComm_Tsunami.elementAction_EEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right, &edge_left
					);
				break;

			case 1:
				cEdgeComm_Tsunami.elementAction_EEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp, &edge_right
					);
				break;

			case 2:
				cEdgeComm_Tsunami.elementAction_EBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&edge_hyp,
						&edge_left
					);
				break;

			case 3:
				cEdgeComm_Tsunami.elementAction_EBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_hyp);
				break;
			case 4:
				cEdgeComm_Tsunami.elementAction_BEE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right, &edge_left);
				break;
			case 5:
				cEdgeComm_Tsunami.elementAction_BEB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_right);
				break;
			case 6:
				cEdgeComm_Tsunami.elementAction_BBE(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element, &edge_left);
				break;
			case 7:
				cEdgeComm_Tsunami.elementAction_BBB(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth, current_element);
				break;
		}
#endif
	}


	/**
	 * traverses domain with given depth and element data stacks
	 * exchanges edge data
	 */
	inline TReduceValue traverse(
			CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *i_cSimulationStacks,
			int depth,
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			TTsunamiDataScalar timestep_size
	){
		cStacks = i_cSimulationStacks;
		refinementDepth = depth;
		int triangle_size = size(depth);
		//flux_stack_inner.resize(triangle_size * 3);
		//CTsunamiEdgeData new_flux_array [triangle_size * 3];
		//flux_array = new_flux_array;

		// new flux array
		flux_array = new CTsunamiEdgeData [triangle_size * 3];

		// update kernel
		cEdgeComm_Tsunami.setTimestepSize(timestep_size);

		// validate number of elements on stack
		assert(i_cSimulationStacks->element_data_stacks.getNumberOfElementsOnStack() == (1 << depth));

		// starting conditions: whole domain (given edge types with pseudo indices), domain size, local index 0, normal direction 1
		triangle_recursion_K_EVEN(edge_type_hyp, edge_type_right, edge_type_left, 0, 0, 0, triangle_size, 0, 1, depth);

		// set stack element counters
		cStacks->element_data_stacks.backward.setStackElementCounter(cStacks->element_data_stacks.forward.getNumberOfElementsOnStack());
		cStacks->element_data_stacks.forward.setStackElementCounter(0);

		// swap forward/backward stacks for next traversal
		cStacks->element_data_stacks.swap();

		// delete flux array
		delete[] flux_array;

		return cEdgeComm_Tsunami.getReduceValue();
	}


	inline TReduceValue traverse(
			CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *i_cSimulationStacks,
			int depth,
			TTsunamiDataScalar timestep_size
	){
		// simple case: whole simulation domain (3x border)
		return traverse(i_cSimulationStacks, depth, BORDER, BORDER, BORDER, timestep_size);
	}

	/**
	 * traverse using predefined arrays
	 *
	inline TReduceValue traverse_withList(CSimulationStacks<CTsunamiElementData,CTsunamiEdgeData> *i_cSimulationStacks, int depth){

		//return cTsunami_EdgeComm_Serial_Regular_Traverse_Table.traverse(i_cSimulationStacks, depth);

	}*/

	inline void storeFlux(
			edge_type edge_type_hyp,
			edge_type edge_type_right,
			edge_type edge_type_left,
			int index_hyp,
			int index_right,
			int index_left,
			int current_index,
			int normal_hyp
	){
		// calc normal direction for left/right edge
		int normal_left = (normal_hyp + 3) & 7;
		int normal_right = (normal_hyp + 5) & 7;

		// neighbor normals
		int normal_h_hyp = (normal_hyp + 4) & 7;
		int normal_h_left = (normal_h_hyp + 3) & 7;
		int normal_h_right = (normal_h_hyp + 5) & 7;
		int normal_r_left = (normal_right + 4) & 7;
		int normal_r_hyp = (normal_r_left + 5) & 7;
		int normal_r_right = (normal_r_left + 2) & 7;
		int normal_l_right = (normal_left + 4) & 7;
		int normal_l_hyp = (normal_l_right + 3) & 7;
		int normal_l_left = (normal_l_right + 6) & 7;

		// get data of current element from forward stack and store (copy) it to backward stack
		CTsunamiElementData *current_element = &(cStacks->element_data_stacks.backward[current_index]);
		cStacks->element_data_stacks.forward.getElementAtIndex(current_index, *current_element);

		// get edge data of neighbor elements from forward stack
		CTsunamiEdgeData hyp_edgeData_outer, right_edgeData_outer, left_edgeData_outer;
		CTsunamiEdgeData hyp_edgeData_inner, right_edgeData_inner, left_edgeData_inner;

		// compute flux data and store it
		CTsunamiEdgeData hyp_edge_flux_inner, left_edge_flux_inner, right_edge_flux_inner;
		CTsunamiEdgeData hyp_edge_flux_outer, left_edge_flux_outer, right_edge_flux_outer;
		//TTsunamiDataScalar hyp_max_wave_speed = 0, left_max_wave_speed = 0, right_max_wave_speed = 0;

		// HYPOTENUSE EDGE
		if (index_hyp > current_index or edge_type_hyp != INNER){
			if (edge_type_hyp == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
						CTriangle_Normals::normal_table[normal_h_hyp][0], CTriangle_Normals::normal_table[normal_h_hyp][1],
						CTriangle_Normals::normal_table[normal_h_right][0], CTriangle_Normals::normal_table[normal_h_right][1],
						CTriangle_Normals::normal_table[normal_h_left][0], CTriangle_Normals::normal_table[normal_h_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_hyp]),
						&hyp_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Hyp(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&hyp_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Hyp(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&hyp_edgeData_inner
				);

			/*fluxSolver.computeNetUpdates(
					hyp_edgeData_inner,		hyp_edgeData_outer,
					hyp_edge_flux_inner,	hyp_edge_flux_outer,
					hyp_max_wave_speed,
					cEdgeComm_Tsunami.gravitational_constant
				);

			CFluxProjections::backprojectFromNormalSpace(hyp_edge_flux_inner.qx, hyp_edge_flux_inner.qy,
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1]);
			CFluxProjections::backprojectFromNormalSpaceAndInvert(hyp_edge_flux_outer.qx, hyp_edge_flux_outer.qy,
					-CTriangle_Normals::normal_table[normal_hyp][0], -CTriangle_Normals::normal_table[normal_hyp][1]);*/

			cEdgeComm_Tsunami.computeNetUpdates(hyp_edgeData_inner, hyp_edgeData_outer, hyp_edge_flux_inner, hyp_edge_flux_outer);

			flux_array[current_index * 3] = hyp_edge_flux_inner;
			if (edge_type_hyp == INNER)
				flux_array[index_hyp * 3] = hyp_edge_flux_outer;
		}

		// RIGHT EDGE
		if (index_right > current_index or edge_type_right != INNER){
			if (edge_type_right == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Left(
						CTriangle_Normals::normal_table[normal_r_hyp][0], CTriangle_Normals::normal_table[normal_r_hyp][1],
						CTriangle_Normals::normal_table[normal_r_right][0], CTriangle_Normals::normal_table[normal_r_right][1],
						CTriangle_Normals::normal_table[normal_r_left][0], CTriangle_Normals::normal_table[normal_r_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_right]),
						&right_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Right(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&right_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Right(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&right_edgeData_inner
				);

			/*fluxSolver.computeNetUpdates(
					right_edgeData_inner,	right_edgeData_outer,
					right_edge_flux_inner,	right_edge_flux_outer,
					right_max_wave_speed,
					cEdgeComm_Tsunami.gravitational_constant
				);

			CFluxProjections::backprojectFromNormalSpace(right_edge_flux_inner.qx, right_edge_flux_inner.qy,
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1]);
			CFluxProjections::backprojectFromNormalSpaceAndInvert(right_edge_flux_outer.qx, right_edge_flux_outer.qy,
					-CTriangle_Normals::normal_table[normal_right][0], -CTriangle_Normals::normal_table[normal_right][1]);*/

			cEdgeComm_Tsunami.computeNetUpdates(right_edgeData_inner, right_edgeData_outer, right_edge_flux_inner, right_edge_flux_outer);

			flux_array[current_index * 3 + 1] = right_edge_flux_inner;
			if (edge_type_right == INNER)
				flux_array[index_right * 3 + 2] = right_edge_flux_outer;
		}

		// LEFT EDGE
		if (index_left > current_index or edge_type_left != INNER){
			if (edge_type_left == INNER)
				cEdgeComm_Tsunami.computeEdgeCommData_Right(
						CTriangle_Normals::normal_table[normal_l_hyp][0], CTriangle_Normals::normal_table[normal_l_hyp][1],
						CTriangle_Normals::normal_table[normal_l_right][0], CTriangle_Normals::normal_table[normal_l_right][1],
						CTriangle_Normals::normal_table[normal_l_left][0], CTriangle_Normals::normal_table[normal_l_left][1],
						refinementDepth,
						&(cStacks->element_data_stacks.forward[index_left]),
						&left_edgeData_outer
					);
			else
				cEdgeComm_Tsunami.computeBoundaryCommData_Left(
						CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
						CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
						CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
						refinementDepth,
						current_element,
						&left_edgeData_outer
					);

			cEdgeComm_Tsunami.computeEdgeCommData_Left(
					CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
					CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
					refinementDepth,
					current_element,
					&left_edgeData_inner
				);

			/*fluxSolver.computeNetUpdates(
					left_edgeData_inner,	left_edgeData_outer,
					left_edge_flux_inner,	left_edge_flux_outer,
					left_max_wave_speed,
					cEdgeComm_Tsunami.gravitational_constant
				);

			CFluxProjections::backprojectFromNormalSpace(left_edge_flux_inner.qx, left_edge_flux_inner.qy,
					CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1]);
			CFluxProjections::backprojectFromNormalSpaceAndInvert(left_edge_flux_outer.qx, left_edge_flux_outer.qy,
					-CTriangle_Normals::normal_table[normal_left][0], -CTriangle_Normals::normal_table[normal_left][1]);*/

			cEdgeComm_Tsunami.computeNetUpdates(left_edgeData_inner, left_edgeData_outer, left_edge_flux_inner, left_edge_flux_outer);

			flux_array[current_index * 3 + 2] = left_edge_flux_inner;
			if (edge_type_left == INNER)
				flux_array[index_left * 3 + 1] = left_edge_flux_outer;
		}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
#if 0
		std::cout << "NORMAL DIRECTIONS (H,R,L): (" << normal_hyp << ", " << normal_right << ", " << normal_left
				<< "), INDEX: " << current_index << std::endl;
		std::cout << "NEIGHBORS (H,R,L): (" << index_hyp << ", " << index_right << ", " << index_left << ")" << std::endl;
		/*std::cout << "EDGE H: " << hyp_edgeData_outer << std::endl;
		std::cout << "EDGE R: " << right_edgeData_outer << std::endl;
		std::cout << "EDGE L: " << left_edgeData_outer << std::endl;
		std::cout <<
				CTriangle_Normals::normal_table[normal_hyp][0] << " " << CTriangle_Normals::normal_table[normal_hyp][1] << " " <<
				CTriangle_Normals::normal_table[normal_right][0] << " " << CTriangle_Normals::normal_table[normal_right][1] << " " <<
				CTriangle_Normals::normal_table[normal_left][0] << " " << CTriangle_Normals::normal_table[normal_left][1]
		<< std::endl;*/
		std::cout << "FLUX (H,R,L): " << current_index * 3 << ": " << flux_array[current_index * 3] << std::endl
				<< current_index * 3 + 1 << ": " << flux_array[current_index * 3 + 1] << std::endl
				<< current_index * 3 + 2 << ": " << flux_array[current_index * 3 + 2] << std::endl;
#endif
		//std::cout << "INFO " << search_index << ": h " << current_element->hyp_edge.qy << ", l " << current_element->left_edge.qy << ", r " << current_element->right_edge.qx << std::endl;
#endif

/*
		TTsunamiDataScalar max_wave_speed;
		TTsunamiDataScalar cat_length;

		max_wave_speed = CMath::max(hyp_max_wave_speed, right_max_wave_speed, left_max_wave_speed);
		cat_length = getUnitCathetusLengthForDepth(refinementDepth)*cEdgeComm_Tsunami.square_side_length;
		cEdgeComm_Tsunami.cfl_domain_size_div_max_wave_speed =
				CMath::min(cat_length/max_wave_speed, cEdgeComm_Tsunami.cfl_domain_size_div_max_wave_speed);*/

		cEdgeComm_Tsunami.elementAction(
				CTriangle_Normals::normal_table[normal_hyp][0], CTriangle_Normals::normal_table[normal_hyp][1],
				CTriangle_Normals::normal_table[normal_right][0], CTriangle_Normals::normal_table[normal_right][1],
				CTriangle_Normals::normal_table[normal_left][0], CTriangle_Normals::normal_table[normal_left][1],
				refinementDepth,
				current_element,
				&(flux_array[current_index * 3]),
				&(flux_array[current_index * 3 + 1]),
				&(flux_array[current_index * 3 + 2])
			);
	}

};

}
}

#endif /* CTSUNAMI_HPP_ */

