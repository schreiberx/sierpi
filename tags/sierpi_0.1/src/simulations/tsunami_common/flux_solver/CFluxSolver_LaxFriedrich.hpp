/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Dec 20, 2011
 *      Author: schreibm
 */

#ifndef CFLUXSOLVER_LAXFRIEDRICH_HPP_
#define CFLUXSOLVER_LAXFRIEDRICH_HPP_


template <typename T>
class CFluxSolver_LaxFriedrich
{
public:
	/**
	 * \brief compute the flux and store the result to o_flux
	 *
	 * this method uses a lax friedrichs flux
	 *
	 * the input data is assumed to be rotated to the edge normal pointing along the positive part of the x axis
	 */
	void computeNetUpdates(
			CTsunamiEdgeData &i_edgeData_left,		///< edge data on left (left) edge
			CTsunamiEdgeData &i_edgeData_right,		///< edge data on right (outer) edge

			CTsunamiEdgeData &o_edgeFlux_left,		///< output for left flux
			CTsunamiEdgeData &o_edgeFlux_right,		///< output for outer flux

			T &o_max_wave_speed_left,				///< maximum wave speed
			T &o_max_wave_speed_right,				///< maximum wave speed

			T i_gravitational_constant				///< gravitational constant
	)
	{
#if 1
		const T epsilon = 0.05;

		T max_bathymetry = CMath::max(i_edgeData_left.b, i_edgeData_right.b);

		T absolute_h_left = i_edgeData_left.b + i_edgeData_left.h;
		T absolute_h_right = i_edgeData_right.b + i_edgeData_right.h;

		T moving_h_left = absolute_h_left - max_bathymetry;
		T moving_h_right = absolute_h_right - max_bathymetry;

		if (	moving_h_left <= epsilon	||
				moving_h_right <= epsilon
		)
		{
			o_edgeFlux_right.h = 0;
			o_edgeFlux_right.qx = 0;
			o_edgeFlux_right.qy = 0;

			o_edgeFlux_left.h = 0;
			o_edgeFlux_left.qx = 0;
			o_edgeFlux_left.qy = 0;

			o_max_wave_speed_left = 0;
			o_max_wave_speed_right = 0;
			return;
		}

/*
		if (	moving_h_left <= epsilon	&&
				moving_h_right > epsilon	&&
				i_edgeData_right.qx > 0
		)
		{

		}
*/
#else
		T moving_h_left = i_edgeData_left.h;
		T moving_h_right = i_edgeData_right.h;
#endif

		T left_vx = i_edgeData_left.qx/moving_h_left;
		T left_vy = i_edgeData_left.qy/moving_h_left;

		T right_vx = i_edgeData_right.qx/moving_h_right;
		T right_vy = i_edgeData_right.qy/moving_h_right;

#if 0
		/**
		 * TODO: update to non-intrinsic version below
		 */
// 4 ops
		__m128 m_left_right_qx_qy = _mm_set_ps(i_edgeData_right.qy, i_edgeData_right.qx, i_edgeData_left.qy, i_edgeData_left.qx);
		__m128 m_left_h_right_h = _mm_set_ps(i_edgeData_right.h, i_edgeData_right.h, i_edgeData_left.h, i_edgeData_left.h);
		__m128 m_left_right_vx_vy = _mm_div_ps(m_left_right_qx_qy, m_left_h_right_h);

		__m128 m_gravity = _mm_set1_ps(i_gravitational_constant);

		/*
		T left_lambda = CMath::sqrt<T>(left_vx*left_vx+left_vy*left_vy) + CMath::sqrt<T>(config.gravity*i_edgeData_left.h);
		T right_lambda = CMath::sqrt<T>(right_vx*right_vx+right_vy*right_vy) + CMath::sqrt<T>(config.gravity*i_edgeData_right.h);
		*/

// 7*2 ops (scalar)
		__m128 m_left_up = _mm_dp_ps(m_left_right_vx_vy, m_left_right_vx_vy, 16*3 + 7);
		__m128 m_left_bottom = _mm_dp_ps(m_left_right_vx_vy, m_left_right_vx_vy, 16*12 + 7);

		__m128 m_right_side = _mm_mul_ps(m_gravity, m_left_h_right_h);

		__m128 m_sqrt_helper1 = _mm_blend_ps(m_left_up, m_right_side, 2);		// r0, r1
		__m128 m_sqrt_helper2 = _mm_blend_ps(m_left_bottom, m_right_side, 8);	// r2, r3
		__m128 m_sqrt = _mm_sqrt_ps(_mm_blend_ps(m_sqrt_helper1, m_sqrt_helper2, 12));

		T sqrt_stuff[4];
		_mm_store_ps(sqrt_stuff, m_sqrt);

		/*
		T lambda = CMath::max(left_lambda, right_lambda);
		*/
		T lambda = CMath::max(sqrt_stuff[0] + sqrt_stuff[1], sqrt_stuff[2] + sqrt_stuff[3]);

#else

#if 1
		/**
		 * compute eigenvalues for left and outer DOFs
		 */
		T left_grav_h = CMath::sqrt(i_gravitational_constant*moving_h_left);
		T right_grav_h = CMath::sqrt(i_gravitational_constant*moving_h_right);

		T left_lambda_add = CMath::abs(left_vx + left_grav_h);
		T left_lambda_sub = CMath::abs(left_vx - left_grav_h);

		T right_lambda_add = CMath::abs(right_vx + right_grav_h);
		T right_lambda_sub = CMath::abs(right_vx - right_grav_h);

		T left_lambda = CMath::max(left_lambda_add, left_lambda_sub);
		T right_lambda = CMath::max(right_lambda_add, right_lambda_sub);

#else

		T left_lambda = CMath::sqrt(left_vx*left_vx+left_vy*left_vy) + CMath::sqrt(i_gravitational_constant*i_edgeData_left.h);
		T right_lambda = CMath::sqrt(right_vx*right_vx+right_vy*right_vy) + CMath::sqrt(i_gravitational_constant*i_edgeData_right.h);

#endif

		T lambda = CMath::max(left_lambda, right_lambda);

#endif
		o_edgeFlux_left.h = (T)0.5*(i_edgeData_left.qx + i_edgeData_right.qx);

		o_edgeFlux_left.qx =
				(T)0.5*
				(
					left_vx*i_edgeData_left.qx +
					right_vx*i_edgeData_right.qx +
					(T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left + moving_h_right*moving_h_right)
				);

		o_edgeFlux_left.qy =
				(T)0.5*
				(
					left_vx*i_edgeData_left.qx +
					right_vx*i_edgeData_right.qx +
					(
						left_vy*i_edgeData_left.qx +
						right_vy*i_edgeData_right.qx
					)
				);

		o_edgeFlux_right.h = -o_edgeFlux_left.h;
		o_edgeFlux_right.qx = -o_edgeFlux_left.qx;
		o_edgeFlux_right.qy = -o_edgeFlux_left.qy;

#if SIMULATION_TSUNAMI_ORDER_OF_BASIS_FUNCTIONS==0
		// remove fluxes for 0th order basis functions

		o_edgeFlux_left.h -= i_edgeData_left.qx;
		o_edgeFlux_left.qx -= left_vx*i_edgeData_left.qx + (T)0.5*i_gravitational_constant*(moving_h_left*moving_h_left);
		o_edgeFlux_left.qy -= left_vx*i_edgeData_left.qx + left_vy*i_edgeData_left.qx;


		o_edgeFlux_right.h += i_edgeData_right.qx;
		o_edgeFlux_right.qx += right_vx*i_edgeData_right.qx + (T)0.5*i_gravitational_constant*(moving_h_right*moving_h_right);
		o_edgeFlux_right.qy += right_vx*i_edgeData_right.qx + right_vy*i_edgeData_right.qx;
#endif

		/**
		 * add numerical diffusion for stability reason
		 */
#if 1
		T diffusion_h = (T)0.5*(T)lambda*(moving_h_left - moving_h_right);
		T diffusion_qx = (T)0.5*(T)lambda*(i_edgeData_left.qx - i_edgeData_right.qx);
		T diffusion_qy = (T)0.5*(T)lambda*(i_edgeData_left.qy - i_edgeData_right.qy);

		o_edgeFlux_right.h -= diffusion_h;
		o_edgeFlux_right.qx -= diffusion_qx;
		o_edgeFlux_right.qy -= diffusion_qy;

		o_edgeFlux_left.h += diffusion_h;
		o_edgeFlux_left.qx += diffusion_qx;
		o_edgeFlux_left.qy += diffusion_qy;
#endif

#if 0
#endif


		/**
		 * CFL condition
		 */
		o_max_wave_speed_left = lambda;
		o_max_wave_speed_right = o_max_wave_speed_left;
	}
};



#endif /* CFLUXLAXFRIEDRICH_HPP_ */
