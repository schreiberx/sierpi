/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http:// www5.in.tum.de/sierpi
 * CFluxLaxFriedrich.hpp
 *
 *  Created on: Jan 9, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CFLUXSOLVER_GEOCLAWAUGRIE_HPP_
#define CFLUXSOLVER_GEOCLAWAUGRIE_HPP_

#include "libmath/CMath.hpp"
#include <iostream>
#include <cassert>

#include "tsunami_solver/AugRieGeoClaw.hpp"

/**
 * \brief AugumentedRiemann GeoClaw flux solver
 */
template <typename T>
class CFluxSolver_GeoClawAugumentedRiemann	: public solver::AugRieGeoClaw<T>
{
	using solver::AugRieGeoClaw<T>::computeNetUpdates;

	/**
	 * This method is executed by the elementData computing kernel method
	 * to get the net updates for given fluxes
	 */
public:
	void computeNetUpdates(
			CTsunamiEdgeData &i_edgeData_left,		///< edge data on left (left) edge
			CTsunamiEdgeData &i_edgeData_right,		///< edge data on right (outer) edge
			CTsunamiEdgeData &o_edgeFlux_left,		///< output for left flux
			CTsunamiEdgeData &o_edgeFlux_right,		///< output for outer flux
			T &o_max_wave_speed_left,				///< maximum wave speed
			T &o_max_wave_speed_right,				///< maximum wave speed
			T i_gravitational_constant				///< gravitational constant
	)
	{
		solver::AugRieGeoClaw<T>::computeNetUpdates(
				i_edgeData_left.h, i_edgeData_right.h,
				i_edgeData_left.qx, i_edgeData_right.qx,
				i_edgeData_left.qy, i_edgeData_right.qy,
				i_edgeData_left.b, i_edgeData_right.b,

				o_edgeFlux_left.h, o_edgeFlux_right.h,
				o_edgeFlux_left.qx, o_edgeFlux_right.qx,
				o_edgeFlux_left.qy, o_edgeFlux_right.qy,
				o_max_wave_speed_left
		);

		o_max_wave_speed_right = o_max_wave_speed_left;
	}
};



#endif
