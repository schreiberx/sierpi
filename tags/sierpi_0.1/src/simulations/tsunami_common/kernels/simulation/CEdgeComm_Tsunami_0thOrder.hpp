/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_
#define CEDGECOMM_TSUNAMI_0TH_ORDER_HPP_


#include "config.h"
#include "libmath/CMath.hpp"

// generic tsunami types
#include "simulations/tsunami_common/types/CTsunamiTypes.hpp"

// enum for boundary conditions
#include "EEdgeComm_Tsunami_BoundaryConditions.hpp"

// traversator
//#include "libsierpi/traversators/edgeComm/CEdgeComm_Normals_Depth.hpp"
#include "libsierpi/traversators/fluxComm/CFluxComm_Normals_Depth.hpp"

// flux solvers
#include "../../flux_solver/CFluxSolver.hpp"
// flux projections
#include "CFluxProjections.hpp"

// default parameters for tsunami edge comm
#include "CEdgeComm_TsunamiParameters.hpp"

namespace sierpi
{
namespace kernels
{

/**
 * \brief class implementation for computation of 1st order DG shallow water simulation
 *
 * for DG methods, we basically need 3 different methods:
 *
 * 1) Storing the edge communication data (storeLeftEdgeCommData, storeRightEdgeCommData,
 *    storeHypEdgeCommData) which has to be sent to the adjacent triangles
 *
 * 2) Computation of the numerical fluxes (computeFlux) based on the local edge comm data
 *    and the transmitted DOF's from the previous operation.
 *
 *    TODO:
 *    Typically a flux is computed by it's decomposition of waves.
 *    Since this computational intensive decomposition can be reused directly to compute
 *    the flux to the other cell, both adjacent fluxes should be computed in one step.
 *
 *    The current state is the computation of both adjacent fluxes twice.
 *
 * 3) Computation of the element update based on the fluxes computed in the previous step
 *    (elementAction)
 */

/**
 * update scheme
 *
 * specifies how to update element data:
 * 0: default update
 * 1: store updates only
 *
 * this gets important for Runge-Kutta methods
 */
template <bool t_storeElementUpdatesOnly>
class CEdgeComm_Tsunami_0thOrder	:
	public CEdgeComm_TsunamiParameters	// generic configurations (gravitational constant, timestep size, ...)
{
public:
	using CEdgeComm_TsunamiParameters::setTimestepSize;
	using CEdgeComm_TsunamiParameters::setGravitationalConstant;
	using CEdgeComm_TsunamiParameters::setSquareSideLength;
	using CEdgeComm_TsunamiParameters::setBoundaryDirichlet;
	using CEdgeComm_TsunamiParameters::setParameters;

//	using CEdgeComm_TsunamiParameters::computeAndUpdateCFLCondition3;


	/**
	 * typedefs vor convenience
	 */
	typedef CTsunamiEdgeData TEdgeData;
	typedef CTsunamiElementData TElementData;

	typedef TTsunamiDataScalar T;
	typedef TTsunamiDataScalar TVertexScalar;

	/// TRAVersator typedef
	typedef sierpi::travs::CFluxComm_Normals_Depth<CEdgeComm_Tsunami_0thOrder<t_storeElementUpdatesOnly> > TRAV;

	/**
	 * true if an instability was detected
	 */
	bool instabilityDetected;

	/**
	 * accessor to flux solver
	 */
	CFluxSolver<T> fluxSolver;



	/**
	 * constructor
	 */
	CEdgeComm_Tsunami_0thOrder()
	{
		boundary_dirichlet.h = CMath::numeric_inf<T>();
		boundary_dirichlet.qx = CMath::numeric_inf<T>();
		boundary_dirichlet.qy = CMath::numeric_inf<T>();
	}

	/**
	 * RK2 helper function - first step
	 *
	 * compute
	 * yapprox_{i+1} = y_i + h f(t_i, y_i)
	 */
	static void computeElementDataRK2_1stStep(
			CTsunamiElementData &i_f_t0,
			CTsunamiElementData &i_f_slope_t0,
			CTsunamiElementData *o_f_t1_approx,
			T i_timestep_size
	)
	{
		o_f_t1_approx->dofs.h	=	i_f_t0.dofs.h	+ i_timestep_size*i_f_slope_t0.dofs.h;
		o_f_t1_approx->dofs.qx	=	i_f_t0.dofs.qx	+ i_timestep_size*i_f_slope_t0.dofs.qx;
		o_f_t1_approx->dofs.qy	=	i_f_t0.dofs.qy	+ i_timestep_size*i_f_slope_t0.dofs.qy;
		o_f_t1_approx->dofs.b	=	i_f_t0.dofs.b;

		o_f_t1_approx->cfl_domain_size_div_max_wave_speed = i_f_slope_t0.cfl_domain_size_div_max_wave_speed;


#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_f_t1_approx->validation = i_f_t0.validation;
#endif
	}



	/**
	 * RK2 helper function - second step
	 *
	 * y_{i+1} = y_i + h/2 * ( f(t_i, y_i) + f(t_{i+1}, y_{i+1}) )
	 */
	static void computeElementDataRK2_2ndStep(
			CTsunamiElementData &i_f_slope_t0,
			CTsunamiElementData &i_f_slope_t1,
			CTsunamiElementData *io_f_t0_t1,
			T i_timestep_size
	)
	{
		io_f_t0_t1->dofs.h	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.h + i_f_slope_t1.dofs.h);
		io_f_t0_t1->dofs.qx	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.qx + i_f_slope_t1.dofs.qx);
		io_f_t0_t1->dofs.qy	+= i_timestep_size*(T)0.5*(i_f_slope_t0.dofs.qy + i_f_slope_t1.dofs.qy);

		// TODO: which CFL to use?
		io_f_t0_t1->cfl_domain_size_div_max_wave_speed = (T)0.5*(i_f_slope_t0.cfl_domain_size_div_max_wave_speed + i_f_slope_t1.cfl_domain_size_div_max_wave_speed);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		io_f_t0_t1->validation = i_f_slope_t0.validation;
#endif
	}


	/**
	 * \brief setup parameters using child's/parent's kernel
	 */
	void setup_WithKernel(
			const CEdgeComm_Tsunami_0thOrder &i_kernel
	)
	{
		// copy configuration
		(CEdgeComm_TsunamiParameters&)(*this) = (CEdgeComm_TsunamiParameters&)(i_kernel);
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed before traversal of the sub-partition
	 */
	void traversal_pre_hook()
	{
		/**
		 * set instability detected to false during every new traversal.
		 *
		 * this variable is used to avoid excessive output of instability
		 * information when an instability is detected.
		 */
		instabilityDetected = false;

		/**
		 * update CFL number
		 */
		cfl_domain_size_div_max_wave_speed = CMath::numeric_inf<T>();
	}


	/**
	 * \brief CALLED by TRAVERSATOR: this method is executed after traversal of the sub-partition
	 */
	void traversal_post_hook()
	{
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via hypotenuse edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the hypotenuse.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupHypEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_hyp_normal_x, i_hyp_normal_y);
#endif
	}


	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via right edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the right edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupRightEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_right_normal_x, i_right_normal_y);
#endif
	}



	/**
	 * \brief CALLED by TRAVERSATOR: Store data send via left edge
	 *
	 * this method is responsible for storing the edge communication data
	 * across the left edge.
	 *
	 * depending on the implemented elementData operations, the edge communication
	 * data has to be rotated to the normal space.
	 *
	 * The normals are given in direction towards the triangle element's center!
	 */
	inline void computeEdgeCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		// store edge comm data to output
		*o_edge_comm_data = i_elementData->dofs;

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_elementData->validation.setupLeftEdgeCommData(&(o_edge_comm_data->validation));
		o_edge_comm_data->validation.setupNormal(i_left_normal_x, i_left_normal_y);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the Hypotenuse by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Hyp(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,
			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_hyp_normal_x, i_hyp_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_hyp_normal_x, -i_hyp_normal_y);

		i_elementData->validation.setupHypEdgeCommData(&o_edge_comm_data->validation);
#endif
	}



	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Right(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_right_normal_x, i_right_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_right_normal_x, -i_right_normal_y);

		i_elementData->validation.setupRightEdgeCommData(&o_edge_comm_data->validation);
#endif
	}




	/**
	 * \brief compute the DOF adjacent to the right edge by using
	 * a specific boundary condition.
	 */
	inline void computeBoundaryCommData_Left(
			T i_hyp_normal_x, T i_hyp_normal_y,
			T i_right_normal_x, T i_right_normal_y,
			T i_left_normal_x, T i_left_normal_y,
			int i_depth,

			const CTsunamiElementData *i_elementData,

			CTsunamiEdgeData *o_edge_comm_data
	)
	{
		switch(eBoundaryCondition)
		{
		case BOUNDARY_CONDITION_DIRICHLET:
			*o_edge_comm_data = boundary_dirichlet;
			break;

		case BOUNDARY_CONDITION_VELOCITY_DAMPING:
			// TODO
			*o_edge_comm_data = i_elementData->dofs;
			break;

		case BOUNDARY_CONDITION_BOUNCE_BACK:
			o_edge_comm_data->h = i_elementData->dofs.h;
			o_edge_comm_data->qx = -i_elementData->dofs.qx;
			o_edge_comm_data->qy = -i_elementData->dofs.qy;
			break;
		}

		// rotate edge comm data to normal space
		CFluxProjections::projectToNormalSpace(o_edge_comm_data->qx, o_edge_comm_data->qy, i_left_normal_x, i_left_normal_y);

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edge_comm_data->validation.setupNormal(-i_left_normal_x, -i_left_normal_y);

		i_elementData->validation.setupLeftEdgeCommData(&o_edge_comm_data->validation);
#endif
	}


	/**
	 * \brief This method which is executed when all fluxes are computed
	 *
	 * this method is executed whenever all fluxes are computed. then
	 * all data is available to update the elementData within one timestep.
	 */
	inline void elementAction(
			T i_hyp_normal_x, T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x, T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x, T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)

			int i_depth,

			CTsunamiElementData *io_elementData,	///< element data

			CTsunamiEdgeData *i_hyp_edge_flux,		///< incoming flux from hypotenuse
			CTsunamiEdgeData *i_right_edge_flux,	///< incoming flux from right edge
			CTsunamiEdgeData *i_left_edge_flux		///< incoming flux from left edge
	)
	{
#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		i_hyp_edge_flux->validation.testNormal(i_hyp_normal_x, i_hyp_normal_y);
		i_right_edge_flux->validation.testNormal(i_right_normal_x, i_right_normal_y);
		i_left_edge_flux->validation.testNormal(i_left_normal_x, i_left_normal_y);

		/*
		 * disable midpoint check for periodic boundaries - simply doesn't make sense :-)
		 */
		io_elementData->validation.testEdgeMidpoints(
				i_hyp_edge_flux->validation,
				i_right_edge_flux->validation,
				i_left_edge_flux->validation
			);
#endif
		CFluxProjections::backprojectFromNormalSpace(i_hyp_edge_flux->qx, i_hyp_edge_flux->qy, i_hyp_normal_x, i_hyp_normal_y);
		CFluxProjections::backprojectFromNormalSpace(i_right_edge_flux->qx, i_right_edge_flux->qy, i_right_normal_x, i_right_normal_y);
		CFluxProjections::backprojectFromNormalSpace(i_left_edge_flux->qx, i_left_edge_flux->qy, i_left_normal_x, i_left_normal_y);
#if 0
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << *i_hyp_edge_flux << std::endl;
		std::cout << *i_right_edge_flux << std::endl;
		std::cout << *i_left_edge_flux << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
#endif

		computeAndUpdateCFLCondition3(
				i_hyp_edge_flux->max_wave_speed,
				i_right_edge_flux->max_wave_speed,
				i_left_edge_flux->max_wave_speed,
				i_depth,
				&(io_elementData->cfl_domain_size_div_max_wave_speed)
			);

		assert(timestep_size > 0);
		assert(gravitational_constant > 0);
		assert(cathetus_real_length > 0);
#if 0
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
		std::cout << i_hyp_edge_flux << std::endl;
		std::cout << i_right_edge_flux << std::endl;
		std::cout << i_left_edge_flux << std::endl;
		std::cout << "++++++++++++++++++++++++++++++++" << std::endl;
#endif
		if (CMath::isNan(io_elementData->dofs.h))
		{
			instabilityDetected = true;
			if (!instabilityDetected)
				std::cerr << "INSTABILITY DETECTED!!!" << std::endl;
			return;
		}

		T cat_length = getUnitCathetusLengthForDepth(i_depth)*cathetus_real_length;
		T hyp_length = getUnitHypotenuseLengthForDepth(i_depth)*cathetus_real_length;

#if 0
//		TTsunamiDataScalar damping = CMath::max((T)1.0-(T)2.0*config.delta_timestep/cathetus_length, (T)0.99);
		TTsunamiDataScalar damping = (T)1.0-(config.timestep_size/cat_length);

		io_elementData->dofs.qx *= damping;
		io_elementData->dofs.qy *= damping;
#endif

		if (t_storeElementUpdatesOnly)
		{
			// store updates only
			TTsunamiDataScalar tmp = -(T)1.0/((T)0.5*cat_length*cat_length);
			io_elementData->dofs.h =  tmp * (hyp_length*i_hyp_edge_flux->h + cat_length*(i_right_edge_flux->h + i_left_edge_flux->h));
			io_elementData->dofs.qx = tmp * (hyp_length*i_hyp_edge_flux->qx + cat_length*(i_right_edge_flux->qx + i_left_edge_flux->qx));
			io_elementData->dofs.qy = tmp * (hyp_length*i_hyp_edge_flux->qy + cat_length*(i_right_edge_flux->qy + i_left_edge_flux->qy));
		}
		else
		{
			// apply updates
			TTsunamiDataScalar tmp = -(T)timestep_size/((T)0.5*cat_length*cat_length);
			io_elementData->dofs.h +=  tmp * (hyp_length*i_hyp_edge_flux->h + cat_length*(i_right_edge_flux->h + i_left_edge_flux->h));
			io_elementData->dofs.qx += tmp * (hyp_length*i_hyp_edge_flux->qx + cat_length*(i_right_edge_flux->qx + i_left_edge_flux->qx));
			io_elementData->dofs.qy += tmp * (hyp_length*i_hyp_edge_flux->qy + cat_length*(i_right_edge_flux->qy + i_left_edge_flux->qy));
		}
	}



	/**
	 * computes the fluxes for the given edge data.
	 *
	 * to use a CFL condition, the bathymetry value of the fluxes is
	 * abused to store the maximum wave speed for updating the CFL in
	 * the respective element update.
	 *
	 * IMPORTANT:
	 * Both edgeData DOFs are given from the viewpoint of each element and were
	 * thus rotated to separate edge normal spaces
	 *
	 * Therefore we have to fix this before and after computing the net updates
	 */
	inline void computeNetUpdates(
			CTsunamiEdgeData &io_edgeData_left,		///< edge data on left edge
			CTsunamiEdgeData &io_edgeData_right,	///< edge data on right edge
			CTsunamiEdgeData &o_edgeFlux_left,		///< output for left flux
			CTsunamiEdgeData &o_edgeFlux_right		///< output for outer flux
	)
	{
		/*
		 * fix edge normal space for right flux
		 */
		io_edgeData_right.qx = -io_edgeData_right.qx;
		io_edgeData_right.qy = -io_edgeData_right.qy;

		T o_max_wave_speed_left;
		T o_max_wave_speed_right;

		fluxSolver.computeNetUpdates(
				io_edgeData_left,
				io_edgeData_right,
				o_edgeFlux_left,
				o_edgeFlux_right,
				o_max_wave_speed_left,
				o_max_wave_speed_right,
				gravitational_constant
			);

		o_edgeFlux_right.qx = -o_edgeFlux_right.qx;
		o_edgeFlux_right.qy = -o_edgeFlux_right.qy;

		// store max wave speeds to same storage as bathymetry data
		o_edgeFlux_left.max_wave_speed = o_max_wave_speed_left;
		o_edgeFlux_right.max_wave_speed = o_max_wave_speed_right;

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
		o_edgeFlux_left.validation = io_edgeData_left.validation;
		o_edgeFlux_right.validation = io_edgeData_right.validation;
#endif
	}

};

}
}



#endif
