/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 24, 2012
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */


#ifndef CTSUNAMI_TYPES_VERTEX_DATA_HPP
#define CTSUNAMI_TYPES_VERTEX_DATA_HPP

#include "CValidation_VertexData.hpp"

/**
 * This datastructure intends to be an accumulator for vertex related data.
 *
 * For surface visualization, the normal as well as the height is needed. The normal
 * as well as the height has to be firstly scaled by some weight (e. g.
 * the triangle surface) with the weight being added to the normalization_factor.
 *
 * Then during the last touch, the vertex data has to be scaled by the inverse of
 * normalization_factor.
 */
class TTsunamiVisualizationVertexData
{
public:
	/**
	 * the normal to the surface
	 */
	TTsunamiVertexScalar normal[3];

	/**
	 * height of surface
	 */
	TTsunamiVertexScalar height;

	/**
	 * normalization factor
	 */
	TTsunamiVertexScalar normalization_factor;



	friend
	::std::ostream&
	operator<<(::std::ostream& os, const TTsunamiVisualizationVertexData &d)
	{
		return os << "([" << d.normal[0] << ", " << d.normal[1] << ", " << d.normal[2] << "], " << d.height << ", " << d.normalization_factor << ")";
	}

#if COMPILE_WITH_DEBUG_TSUNAMI_ELEMENTDATA_VALIDATION
	CValidation_VertexData validation;
#endif

};

#endif
