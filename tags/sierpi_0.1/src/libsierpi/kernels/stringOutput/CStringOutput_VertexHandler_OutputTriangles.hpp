/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Jan 10, 2011
 *      Author: schreibm
 */

#ifndef KERNEL_CSTRUCTURE_VERTEXHANDLER_OUTPUTTRIANGLES_HPP_
#define KERNEL_CSTRUCTURE_VERTEXHANDLER_OUTPUTTRIANGLES_HPP_

#include "CVertex2d.hpp"
#include "libsierpi/kernels/CPointInTriangleTest.hpp"


namespace sierpi
{
namespace kernels
{


class CStringOutput_VertexHandler_OutputTriangles
{
public:
	typedef GLfloat TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	std::ostream *output_stream;
	TVertexScalar z;
	TVertexScalar dz;


	inline void initBorderVertices(TVertexScalar v[4][2])
	{
		v[0][0] = -1;
		v[0][1] = 1;

		v[1][0] = 1;
		v[1][1] = 1;

		v[2][0] = 1;
		v[2][1] = -1;

		v[3][0] = -1;
		v[3][1] = -1;
	}


	inline void initBorderVertices(TVertexType v[4])
	{
		v[0] = CVertex2d<TVertexScalar>(-1,0);
		v[1] = CVertex2d<TVertexScalar>(0,-1);
		v[2] = CVertex2d<TVertexScalar>(1,0);
		v[3] = CVertex2d<TVertexScalar>(0,1);
	}

	inline void hypInterpolator(
			const TVertexType &v0,
			const TVertexType &v1,
			const TVertexType &v2,
			TVertexType &mid
			)
	{
		mid = (v0+v1)*(TVertexScalar)0.5;
	}

	inline void elementAction(
			const TVertexType &v0,
			const TVertexType &v1,
			const TVertexType &v2
			)
	{
		z += dz;
		if (z > 1.0)	z = 1.0;

		*output_stream << v0.data[0] << " " << v0.data[1] << " " << z << std::endl;
		*output_stream << v1.data[0] << " " << v1.data[1] << " " << z << std::endl;
		*output_stream << std::endl;
		*output_stream << v2.data[0] << " " << v2.data[1] << " " << z << std::endl;
		*output_stream << v2.data[0] << " " << v2.data[1] << " " << z << std::endl;
		*output_stream << std::endl;
		*output_stream << std::endl;
	}

	inline void pre_hook(CStack<char> &p_structure_stack)
	{
		output_stream = &std::cout;
		z = -1.0;
		dz = 0.3/(double)p_structure_stack.getStackElementCounter();
	}

	inline void post_hook(CStack<char> &p_structure_stack)
	{

	}
};

}
}

#endif
