/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpi project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpi
 *
 *  Created on: Feb 14, 2011
 *      Author: Martin Schreiber (martin.schreiber@in.tum.de)
 */

#ifndef AUTOMATON_TABLE_HPP__
#define AUTOMATON_TABLE_HPP__

/**
 * TODO: align at 64 bytes
 */
static const unsigned char automatonTable[8][8] =
{
		{	0	,	69	,	70	,	71	,	4	,	5	,	6	,	7	},
		{	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	},
		{	2	,	69	,	70	,	71	,	4	,	5	,	6	,	7	},
		{	68	,	69	,	70	,	71	,	4	,	5	,	6	,	7	},
		{	4	,	5	,	6	,	7	,	4	,	5	,	6	,	7	},
		{	5	,	5	,	7	,	7	,	5	,	5	,	7	,	7	},
		{	6	,	7	,	6	,	7	,	6	,	7	,	6	,	7	},
		{	7	,	7	,	7	,	7	,	7	,	7	,	7	,	7	},
};

#endif
