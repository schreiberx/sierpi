#! /bin/bash

INPUT="../../../gebco/gebco_08.nc"
OUTPUT="../../../gebco/tohoku.nc"

BATHAREA="138/146/34/42"

echo GMT grdcut $INPUT -R$BATHAREA -G$OUTPUT
