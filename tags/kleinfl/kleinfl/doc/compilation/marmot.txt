

wget https://fs.hlrs.de/projects/marmot/downloads/marmot-2.4.0/Marmot-2.4.0-Source.tar.gz
tar xzf Marmot-2.4.0-Source.tar.gz
cd Marmot-2.4.0-Source
mkdir build
cd build


export MARMOT_USED_MPI_PACKAGE=OPENMPI
CC=icc CXX=icpc cmake -DCMAKE_INSTALL_PREFIX=$HOME/local/marmot-2.4.0 -DMARMOT_USE_OPENMP=on ../
make -j 4 all install




IMPORTANT:
If necessary, insert the line
	int MPI_SOURCEINFO_Pcontrol(int, ...);
into the file '$HOME/local/marmot-2.4.0/include/sourceinfompicalls.h'
