#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstring>
#include "dunavant.hpp"


#define FORMATED_OUTPUT	0

int main()
{
	int order;
	int order_num;
	int rule;
	int rule_num;
	double *wtab;
	double *xytab;

	rule_num = dunavant_rule_num ( );

	std::cout << std::setprecision(16);



	/**
	 * NUM_RULES
	 */
	std::cout << "dunavantOrderNum:=[";
	for (rule = 1; rule <= rule_num; rule++)
	{
		order_num = dunavant_order_num ( rule );
		std::cout << order_num;
		if (rule != rule_num)
			std::cout << ", ";
	}
	std::cout << "];" << std::endl;



	/**
	 * COORDS
	 */
	std::cout << "dunavantCoords:=[" << std::endl;
	for (rule = 1; rule <= rule_num; rule++)
	{
		order_num = dunavant_order_num ( rule );

		xytab = new double[2*order_num];
		wtab = new double[order_num];

		dunavant_rule ( rule, order_num, xytab, wtab );

		std::cout << "\t[" << std::endl;
		for (order = 0; order < order_num; order++)
		{
			std::cout << "\t\t[" << xytab[order*2+0] << ",\t" << xytab[order*2+1] << "]";
			if (order != order_num-1)
				std::cout << ",";
#if FORMATED_OUTPUT	
			std::cout << std::endl;
#endif
		}
		std::cout << "\t]," << std::endl;

		delete [] wtab;
		delete [] xytab;
	}
	std::cout << "];" << std::endl;



	/**
	 * WEIGHTS
	 */
	std::cout << "dunavantWeights:=[" << std::endl;
	for (rule = 1; rule <= rule_num; rule++)
	{
		order_num = dunavant_order_num ( rule );

		xytab = new double[2*order_num];
		wtab = new double[order_num];

		dunavant_rule ( rule, order_num, xytab, wtab );

		std::cout << "\t[";
		for (order = 0; order < order_num; order++)
		{
			std::cout << wtab[order];
			if (order != order_num-1)
				std::cout << ",";
#if FORMATED_OUTPUT	
			std::cout << std::endl;
#endif
		}
		std::cout << "\t]," << std::endl;

		delete [] wtab;
		delete [] xytab;
	}
	std::cout << "];" << std::endl;

	return 1;
}
