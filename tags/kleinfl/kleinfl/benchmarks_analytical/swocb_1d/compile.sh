#! /bin/bash

cd ../..

make clean

scons --mode=release --xml-config=./scenarios/solitary_wave_on_composite_beach_1d_bouy_sampling.xml --enable-swe-benchmarks=on --hyperbolic-adaptivity-mode=2 $@ -j 4 || exit -1
