#!/usr/bin/python

# @file
# This file is part of the tsunami repository.
#
# @author Alexander Breuer (breuera AT in.tum.de, http://www5.in.tum.de/wiki/index.php/Dipl.-Math._Alexander_Breuer)
#
# @section LICENSE
#
# 2013-01_31: modified by Martin Schreiber (martin.schreiber@in.tum.de)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# @section DESCRIPTION
#
# TODO: description
#

#import tools.Logger as l_logger
#import tools.SierpiInterface as l_sierpi

#import tools.ParaviewInterface as ParaviewInterface
import tools.Plotter as Plotter

import argparse
import os
import sys

if len(sys.argv) < 2:
        print "Use "+sys.argv[0]+" [output dir]"
        sys.exit(-1)

work_dir=sys.argv[1]



###
### Main
###

# variables
l_pathToNthmpBenchmarkData = '../nthmp_benchmark_data'
l_pathToTohokuBenchmarkData = 'benchmarks/field/tohoku_2011_04_11/dart_stations/'

l_pathToSierpiVtkCollectionFile = '/home/breuera/workspace/sierpi/frames.pvd'
l_pathToBuoyFiles = 'benchmarks/analytical/solitary_wave_on_composite_beach/nthmp/'
l_buoyFilesBaseName = 'sierpi_swocb'

#l_outputDirectory = '/home/breuera/workspace/scratch/output/sierpi/benchmarks/'
l_outputDirectory = '/home/breuera/Dropbox/workspace/phd/paper/sierpi_tsunamis/journal_paper/benchmarks_tsunami_pub/plots/'

# construct a plotter
l_plotter = Plotter.Plotter( i_pathToNthmpBenchmarkData = l_pathToNthmpBenchmarkData,
                             i_pathToTohokuBenchmarkData = l_pathToTohokuBenchmarkData )

# plot buoy data
l_pathToBenchmarkDirectories = [ os.curdir+'/'+work_dir ]

# iterate over different benchmarks
for l_pathToBenchmarkDirectory in l_pathToBenchmarkDirectories:
	print l_pathToBenchmarkDirectory

	l_csvFiles = os.listdir(l_pathToBenchmarkDirectory)

	# iterate over files of this benchmark
	for l_file in l_csvFiles:

		# only process csv files
		if not '.csv' in l_file:
			continue

		if '_0.25.' in l_file:
			l_benchmarkName='swosb_ts_0.25'
		elif '_9.95.' in l_file:
			l_benchmarkName='swosb_ts_9.95'
		else:
			print 'Unknown benchmark'
			sys.exit(-1)

		# plot the file
		l_plotter.plotGaugeData(
				i_pathToGaugeFile = l_pathToBenchmarkDirectory+l_file,
				i_benchmarkName = l_benchmarkName,
				i_baseName = l_file.replace('.csv',''),
				i_pathToOutputDirectory = l_outputDirectory )

