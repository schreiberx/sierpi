#! /bin/bash
cd /home/schreibm/workspace/sierpi_2013_02_11_benchmarks_analytical/benchmarks_analytical/swosb_2d

source ./inc_vars.sh

#export OMP_NUM_THREADS=4

export KMP_AFFINITIES=compact

# up to 960 threads can be configured on uv2
# up to 1120 threads can be configured on uv3
# up to 8 or 32 threads can be configured on the Myrinet nodes (see below)
#  The used value should be consistent
#  with --cpus-per-task above

echo "../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_const_m0_release -c ../../scenarios/single_wave_on_simple_beach_2d_bouy_sampling.xml -d 1 -w -16 -a 0 -r 0/0 -o 4096 -C 0.25 -A 1"
../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_const_m0_release -c ../../scenarios/single_wave_on_simple_beach_2d_bouy_sampling.xml -d 1 -w -16 -a 0 -r 0/0 -o 4096 -C 0.25 -A 1 > /home/schreibm/workspace/sierpi_2013_02_11_benchmarks_analytical/benchmarks_analytical/swosb_2d/2013_02_12/sierpi_intel_tbb_hyperbolic_parallel_tsunami_const_m0_release_d01_a00_s004096_r0-0_D8.0.txt

mv "0_25.csv" "/home/schreibm/workspace/sierpi_2013_02_11_benchmarks_analytical/benchmarks_analytical/swosb_2d/2013_02_12/sierpi_intel_tbb_hyperbolic_parallel_tsunami_const_m0_release_d01_a00_s004096_r0-0_D8.0_0.25.csv"
mv "9_95.csv" "/home/schreibm/workspace/sierpi_2013_02_11_benchmarks_analytical/benchmarks_analytical/swosb_2d/2013_02_12/sierpi_intel_tbb_hyperbolic_parallel_tsunami_const_m0_release_d01_a00_s004096_r0-0_D8.0_9.95.csv"
