#! /bin/bash

TARBALL=benchmark_tmp.tar.bz2
LXHALLE_TMP=/u/halle/kleinfl/home_at/tmp
LOCAL_TMP=./test7/
MACDIR=/home/hpc/pr63so/di68zip/workspace/sierpi_2014_02_05/cubed_sphere/$1

echo " + creating tarball..."
#ssh lxhalle tar cjf $LXHALLE_TMP/$TARBALL -C $LXHALLE_TMP output*
ssh di68zip@mac-login-amd.tum-mac.cos.lrz.de "cd $MACDIR && tar cjf $TARBALL output* *.txt"

echo " + copying to lxhalle..."
#ssh lxhalle scp di68zip@mac-login-amd.tum-mac.cos.lrz.de:$MACDIR/output* $LXHALLE_TMP
scp di68zip@mac-login-amd.tum-mac.cos.lrz.de:$MACDIR/$TARBALL $LXHALLE_TMP


