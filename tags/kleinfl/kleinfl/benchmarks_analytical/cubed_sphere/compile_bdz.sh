#! /bin/bash

#. $HOME/bin/local_vars.sh
#. $HOME/bin/intel_vars.sh
#. $HOME/bin/scons_vars.sh

. inc_vars.sh

COMPILE_DIR=`pwd`

cd ..
#make clean

#scons --xml-config=$COMPILE_DIR/tohoku_netcdf_intel_release.xml --enable-skip-adaptive-conforming-clusters=on --enable-exit-on-instability-thresholds=on --mode=release --enable-scan-force-threading-traversal=on --threading=omp --enable-mpi=on -j 4
#scons --xml-config=$COMPILE_DIR/tohoku_netcdf_intel_release.xml --enable-skip-adaptive-conforming-clusters=on --enable-exit-on-instability-thresholds=off --mode=release --enable-scan-force-threading-traversal=on --threading=omp --enable-mpi=on --program-binary-name=sierpi_intel_mpi_omp_scan_force_threading_sacsp_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release_bdz -j 4
#scons --xml-config=./scenarios/tohoku_netcdf_intel_release.xml --enable-skip-adaptive-conforming-clusters=on --enable-exit-on-instability-thresholds=on --mode=release --threading=tbb  -j 4

scons --xml-config=$COMPILE_DIR/intel_release_equiangular.xml --mode=release --threading=omp --enable-mpi=on -j 4
scons --xml-config=$COMPILE_DIR/intel_release_equidistant.xml --mode=release --threading=omp --enable-mpi=on -j 4
