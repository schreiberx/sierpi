// use opengl 3.2 core shaders
#version 150

// TEXTURE0
uniform bool texture0_enabled = false;
uniform sampler2D texture0;

in vec2 frag_texture_coord2;

out vec4 frag_data;

in float height;


vec4 blinnShading(vec4 material_color);

/**
 * computation of rainbow color map from
 * http://http.developer.nvidia.com/GPUGems/gpugems_ch08.html
 */
vec3 blend2ndOrder(vec3 f)
{
	vec3 y = 1.0-f*f;
	return max(y, vec3(0));
}


vec3 getBathymetryColor(float height)
{

    if (height < -11000) return vec3(0.141176470588235, 0.149019607843137, 0.686274509803922);
//	if (height < -5499.999) return vec3(0.219607843137255, 0.227450980392157, 0.764705882352941);
	if (height < -5500) return vec3(0.219607843137255, 0.227450980392157, 0.764705882352941);
//	if (height < -2999.999) return vec3(0.274509803921569, 0.282352941176471, 0.83921568627451);
	if (height < -3000) return vec3(0.274509803921569, 0.282352941176471, 0.83921568627451);
//	if (height < -1999.999) return vec3(0.317647058823529, 0.4, 0.850980392156863);
	if (height < -2000) return vec3(0.317647058823529, 0.4, 0.850980392156863);
//	if (height < -749.999) return vec3(0.392156862745098, 0.505882352941176, 0.874509803921569);
	if (height < -750) return vec3(0.392156862745098, 0.505882352941176, 0.874509803921569);
//	if (height < -69.999) return vec3(0.513725490196078, 0.631372549019608, 0.901960784313726);
	if (height < -70) return vec3(0.513725490196078, 0.631372549019608, 0.901960784313726);
//	if (height < -19.999) return vec3(0.643137254901961, 0.752941176470588, 0.941176470588235);
	if (height < -20) return vec3(0.643137254901961, 0.752941176470588, 0.941176470588235);
//	if (height < 0.001) return vec3(0.666666666666667, 0.784313725490196, 1);
	if (height < 0) return vec3(0, 0.380392156862745, 0.27843137254902);
//	if (height < 50.001) return vec3(0.0627450980392157, 0.47843137254902, 0.184313725490196);
	if (height < 50) return vec3(0.0627450980392157, 0.47843137254902, 0.184313725490196);
//	if (height < 500.001) return vec3(0.909803921568627, 0.843137254901961, 0.490196078431373);
	if (height < 500) return vec3(0.909803921568627, 0.843137254901961, 0.490196078431373);
//	if (height < 1200.001) return vec3(0.631372549019608, 0.262745098039216, 0);
	if (height < 1200) return vec3(0.631372549019608, 0.262745098039216, 0);
//	if (height < 1700.001) return vec3(0.509803921568627, 0.117647058823529, 0.117647058823529);
	if (height < 1700) return vec3(0.509803921568627, 0.117647058823529, 0.117647058823529);
//	if (height < 2800.001) return vec3(0.431372549019608, 0.431372549019608, 0.431372549019608);
	if (height < 2800) return vec3(0.431372549019608, 0.431372549019608, 0.431372549019608);
	return vec3(1, 1, 1);

//	if (height < 4000.001) return vec3(1, 1, 1);
//	if (height < 4000) return vec3(1, 1, 1);
//	if (height < 6000.001) return vec3(1, 1, 1);
}


void main(void)
{
	vec4 frag_color = vec4(getBathymetryColor(height), 0);

//	frag_data = frag_color;
	frag_data = blinnShading(frag_color);
}
