#! /usr/bin/python

import sys
import math

from sympy import sqrt
from sympy import sin,cos,pi
from sympy import Symbol, symbols, var
from sympy import diff,integrate,simplify
from sympy import Integer
from sympy.matrices import Matrix
from sympy import Poly

import jacobi
import gaussian_quadrature


print sys.setrecursionlimit(1000000)


# order of basis functions
basis_max_order=3

# order of quadrature
quadrature_max_order=basis_max_order

#
# gauss quadrature is accurate for polynomials of degree 2n-1 for n support points
# for basis functions of degree d, we get "2*d <= 2*n-1"
#
quadrature_1d_sampling_points=int(math.ceil((2.0*quadrature_max_order+1.0)/2.0))


# type of basis functions
# 1: jacobi polygonials
# 2: monomials
type_of_basis_function=1

# normalize basis polynomials
normalize_basis_polys=True
#normalize_basis_polys=False


# activate 3D?
activate_3d=True
#activate_3d=False


(x,y,z) = symbols('x:z');
(u,v) = symbols('u,v');


omega=[(z, 0, 1), (y, 0, 1-x), (x, 0, 1)]

def integrate_domain(fun):
	f = simplify(fun)
	for i in range(0, 3):
		f = integrate(f, omega[i])
	return f


#
# SETUP BASIS FUNCTIONS
#

basis_functions = []

if type_of_basis_function==1:
	# 
	# jacobi polynomials
	#
	for m in range(0, basis_max_order+1):
		for n in range(0, basis_max_order+1-m):
			for o in range(0, basis_max_order+1-m-n):
				if not activate_3d and o > 0:
					continue;

				def pp(m, a, b, x):
					return jacobi.jacobi_poly(m, a, b, 2*x-1)

				basis_functions.append(simplify(pp(m, 0, 0, x/(1-y))*(1-y)**m * pp(n, 2*m+1, 0, y)*pp(o, 0, 0, z)))

elif type_of_basis_function==2:
	for m in range(0, basis_max_order+1):
		for n in range(0, basis_max_order+1-m):
			for o in range(0, basis_max_order+1-m-n):

				basis_functions.append(x**m * y**n * z**o)

else:
	throw("Unknown type of basis functions")



print "Basis functions:"
print basis_functions

nob = len(basis_functions)


# NORMALIZATION
if normalize_basis_polys:
	for i in range(0, len(basis_functions)):
		normalize=sqrt(integrate_domain(basis_functions[i]*basis_functions[i]))
		basis_functions[i] = simplify(basis_functions[i]/normalize)

# MASS MATRIX
mass_matrix=Matrix(	nob, nob,
			lambda i,j: integrate_domain(basis_functions[i]*basis_functions[j])
		)

print "Mass matrix:"
print mass_matrix
print


#
# Stiffness matrix X
#
stiffness_matrix_x=Matrix(
		nob, nob,
		lambda i,j: integrate_domain(diff(basis_functions[i], x)*basis_functions[j])
	)

print "Stiffness matrix X:"
print stiffness_matrix_x
print


#
# Stiffness matrix Y
#
stiffness_matrix_y=Matrix(
		nob, nob,
		lambda i,j: integrate_domain(diff(basis_functions[i], y)*basis_functions[j])
	)

print "Stiffness matrix Y:"
print stiffness_matrix_y
print



if activate_3d:
	#
	# Stiffness matrix Z
	#
	stiffness_matrix_z=Matrix(
			nob, nob,
			lambda i,j: integrate_domain(diff(basis_functions[i], z)*basis_functions[j])
		)

	print "Stiffness matrix Z:"
	print stiffness_matrix_z
	print



################################
# SIDE FACES
################################

(gq_1d_coords, gq_1d_weights)=gaussian_quadrature.getPoints1D(quadrature_1d_sampling_points)

# project to interval [0;1]
for i in range(0, len(gq_1d_coords)):
	gq_1d_coords[i] = (gq_1d_coords[i]+1)/2
	gq_1d_weights[i] = gq_1d_weights[i]/2

assert len(gq_1d_coords) == quadrature_1d_sampling_points


#
# consider anti-clockwise direction to assure correct communication!!!
#
gq_1d_coords_hyp = [[1-i,i] for i in gq_1d_coords]
gq_1d_coords_right = [[0,1-i] for i in gq_1d_coords]
gq_1d_coords_left = [[i,0] for i in gq_1d_coords]

print "gq_1d_coords: "
print gq_1d_coords
print

print "gq_1d_weights: "
print gq_1d_weights
print

print "gq_1d_coords_hyp: "
print gq_1d_coords_hyp
print

print "gq_1d_coords_right: "
print gq_1d_coords_right
print

print "gq_1d_coords_left: "
print gq_1d_coords_left
print


def dof_to_side_face_matrix(edge_sampling_points):
	# sampling points on edge
	edge_n=quadrature_1d_sampling_points

	# sampling points in z direction
	z_n=(quadrature_1d_sampling_points if activate_3d else 1)

	# overall sampling points on edge/face
	sampling_points=edge_n*z_n

	return Matrix(
		sampling_points, nob,

		lambda i,j: 
			simplify(
				basis_functions[j].subs(
					[
						(x, edge_sampling_points[i%edge_n][0]),
						(y, edge_sampling_points[i%edge_n][1]),
						(z, gq_1d_coords[i/edge_n])
					]
				)
			)
		)

dof_to_hyp_face_matrix=dof_to_side_face_matrix(gq_1d_coords_hyp)
dof_to_right_face_matrix=dof_to_side_face_matrix(gq_1d_coords_right)
dof_to_left_face_matrix=dof_to_side_face_matrix(gq_1d_coords_left)

print "DOF to hyp:"
print dof_to_hyp_face_matrix.evalf()
print
print "DOF to right:"
print dof_to_right_face_matrix.evalf()
print
print "DOF to left:"
print dof_to_left_face_matrix.evalf()
print



def side_face_to_dofs_matrix(edge_sampling_points):
	# sampling points on edge
	edge_n=quadrature_1d_sampling_points

	# sampling points in z direction
	z_n=(quadrature_1d_sampling_points if activate_3d else 1)

	# overall sampling points on edge/face
	sampling_points=edge_n*z_n

	return Matrix(
		nob, sampling_points,

		lambda i,j: 
			simplify(
				basis_functions[i].subs(
					[
						(x, edge_sampling_points[j%edge_n][0]),
						(y, edge_sampling_points[j%edge_n][1]),
						(z, gq_1d_coords[j/edge_n])
					]
				)
				*gq_1d_weights[j%edge_n]*(gq_1d_weights[j/edge_n] if activate_3d else 1)
			)
		)

hyp_face_to_dofs_matrix=side_face_to_dofs_matrix(gq_1d_coords_hyp)*sqrt(2)
right_face_to_dofs_matrix=side_face_to_dofs_matrix(gq_1d_coords_right)
left_face_to_dofs_matrix=side_face_to_dofs_matrix(gq_1d_coords_left)


print "hyp to dofs:"
print hyp_face_to_dofs_matrix.evalf()
print
print "right to dofs:"
print right_face_to_dofs_matrix.evalf()
print
print "left to dofs:"
print left_face_to_dofs_matrix.evalf()
print





################################
# TOP/DOWN FACES
################################

(gq_2d_coords, gq_2d_weights)=gaussian_quadrature.getTrianglePoints2D(quadrature_max_order)

print "gq_2d_coords:"
print gq_2d_coords
print

print "gq_2d_weights:"
print gq_2d_weights
print


def dof_to_topbottom_face_matrix(z_coord):
	# overall sampling points on edge/face
	sampling_points=len(gq_2d_coords)

	return Matrix(
		sampling_points, nob,

		lambda i,j: 
			simplify(
				basis_functions[j].subs(
					[
						(x, gq_2d_coords[i][0]),
						(y, gq_2d_coords[i][1]),
						(z, z_coord)
					]
				)
			)
		)

dof_to_top_face_matrix=dof_to_topbottom_face_matrix(1)
dof_to_bottom_face_matrix=dof_to_topbottom_face_matrix(0)

print "DOF to top face:"
print dof_to_top_face_matrix.evalf()
print
print "DOF to bottom face:"
print dof_to_bottom_face_matrix.evalf()
print


def topbottom_face_to_dofs_matrix(z_coord):
	# overall sampling points on edge/face
	sampling_points=len(gq_2d_coords)

	return Matrix(
		nob, sampling_points,

		lambda i,j: 
			simplify(
				basis_functions[i].subs(
					[
						(x, gq_2d_coords[j][0]),
						(y, gq_2d_coords[j][1]),
						(z, z_coord)
					]
				)
				*gq_2d_weights[j]
			)
		)

top_face_to_dofs_matrix=topbottom_face_to_dofs_matrix(1)
bottom_face_to_dofs_matrix=topbottom_face_to_dofs_matrix(0)


print "DOF to top face:"
print top_face_to_dofs_matrix.evalf()
print
print "DOF to bottom face:"
print bottom_face_to_dofs_matrix.evalf()
print



#
# ADAPVITIY MATRICES
#

def rotationMatrix(a):
	return Matrix(3,3,[cos(a), sin(a), 0, -sin(a), cos(a), 0, 0, 0, 1])


#
# left child
#

# move top corner to parent's top corner
m_translate=Matrix(3,3,[1,0,Integer(-1)/2,0,1,Integer(-1)/2,0,0,1])

# scale
m_scale=Matrix(3,3,[sqrt(2), 0, 0, 0, sqrt(2), 0, 0, 0, 1])

# rotate
m_rotation=rotationMatrix(Integer(-3)/Integer(4)*pi)
m_left_transform=m_rotation*m_scale*m_translate*Matrix(3, 1, [u, v, 1])

# basis functions in space of left child
left_basis_functions=[simplify(b.subs([(x, m_left_transform[0]), (y, m_left_transform[1])]).subs([(u,x),(v,y)])) for b in basis_functions]



#
# right child
#

# move top corner to parent's top corner
m_translate=Matrix(3,3,[1,0,Integer(-1)/Integer(2),0,1,Integer(-1)/Integer(2),0,0,1])

# scale
m_scale=Matrix(3,3,[sqrt(2), 0, 0, 0, sqrt(2), 0, 0, 0, 1])

# rotate
m_rotation=rotationMatrix(Integer(3)/Integer(4)*pi)
m_right_transform=m_rotation*m_scale*m_translate*Matrix(3, 1, [u, v, 1])

# basis functions in space of right child
right_basis_functions=[simplify(b.subs([(x, m_right_transform[0]), (y, m_right_transform[1])]).subs([(u,x),(v,y)])) for b in basis_functions]



# setup matrix which projects 

def getEquationMatrix(basis_funs):
	#
	# this function sets up a matrix with the coefficients
	# of the basis functions.
	#
	# this can be applied in the following way:
	# Given the weights of the basis functions in a vector,
	# the coefficients of the parent polynomial can be computed
	# by simply multiplying the matrix with the 'weight-vector'
	#
	# inverting the matrix computes the DOFs based on the
	# coefficients of the main polynomial
	#

	matrix=Matrix(nob, nob, lambda i,j:pi)

#	print
	for i in range(0, nob):
		b = Poly(basis_funs[i], x, y, z)
#		print b
		c=0
		for m in range(0, basis_max_order+1):
			for n in range(0, basis_max_order-m+1):
				for o in range(0, basis_max_order-m-n+1):
					if not activate_3d and o > 0:
						continue;

					matrix[c,i]=b.nth(m,n,o)
					c=c+1

	return matrix


m_parent=getEquationMatrix(basis_functions)
m_left_child=getEquationMatrix(left_basis_functions)
m_right_child=getEquationMatrix(right_basis_functions)

print "adaptive parent:"
print m_parent
print

print "adaptive left child:"
print m_left_child
print

print "adaptive right child:"
print m_right_child
print

m_parent_inv=m_parent.inv()
m_left_child_inv=m_left_child.inv()
m_right_child_inv=m_right_child.inv()


m_refine_left=(m_left_child_inv*m_parent).applyfunc(simplify)
m_refine_right=(m_right_child_inv*m_parent).applyfunc(simplify)


print "refine left:"
print m_refine_left
print

print "refine right:"
print m_refine_right
print


m_coarsen_left=(m_parent_inv*m_left_child).applyfunc(simplify)
m_coarsen_right=(m_parent_inv*m_right_child).applyfunc(simplify)

print "coarsen left:"
print m_coarsen_left
print

print "coarsen right:"
print m_coarsen_right
print


