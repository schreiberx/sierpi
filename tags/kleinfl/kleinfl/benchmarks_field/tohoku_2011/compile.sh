#! /bin/bash

cd ../..
make clean

scons --xml-config=./scenarios/tohoku_netcdf_intel_release.xml --enable-exit-on-instability-thresholds=on --mode=release --threading=omp -j 8
