#! /bin/bash

cd ../..
#make clean

scons --compiler=intel --enable-gui=off --mode=release --threading=tbb --enable-asagi=on --hyperbolic-flux-solver=5 --fp-default-precision=double -j8 --hyperbolic-runge-kutta-order=1 --hyperbolic-adaptivity-mode=2
