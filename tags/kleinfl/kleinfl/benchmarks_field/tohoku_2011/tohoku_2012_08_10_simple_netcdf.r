###
### Sierpi Benchmark analysis
###
### Display dart data
### Contact: breuera@in.tum.de, schreibm@in.tum.de
###

# path to benchmark directory
benchmark_dir <- "/home/schreibm/workspace/sierpi/benchmarks_field/tohoku_2011"

## datasets to analyze
benchmark_output <- "tohoku_2012_08_10_simple_netcdf"
benchmark_measured_data <- "tohoku_dart_data"

#visualize_darts = c("21401", "21413", "21418", "21419")
#visualize_darts = c("21401", "21413")
#visualize_darts = c("21418", "21419")
visualize_darts = c("21401")
#visualize_darts = c("21413")
#visualize_darts = c("21418")
#visualize_darts = c("21419")

#
# plot Mode:
#
# 1: dart stations + simulation data
# 2: error plot
# 3: dart stations + simulation data
#
plotMode = 1

limit_d <- c(4,24)
limit_d_even <- -1
limit_a <- -1
limit_max_level <- c(24)
limit_cfl <- c(0.5)
limit_rk <- -1

line_width <- 3

#c(-10,10)

#benchmark_output_group_regexp <- "output_d([0-9]*)_a([0-9]*)_cfl(.*)_dart([0-9]*)\\.txt"
benchmark_output_group_regexp <- "^output_d([0-9]*)_a([0-9]*)_cfl(.*)_rk([0-9])_refine.*_coarsen.*_dart([0-9]*)\\.txt$"

#################################################################################
#################################################################################
#################################################################################

benchmark_measured_data_list <- list(
  "21401" = "21401-03142011_notide.txt",
  "21413" = "21413_5day-03142011_notide.txt",
  "21414" = "21414_5day-03132011_notide.txt",
  "21415" = "21415_5day-03132011_notide.txt",
  "21418" = "21418_5day-03132011_notide.txt",
  "21419" = "21419_5day-03142011_notide.txt",
  "32412" = "32412_5day-03132011_notide.txt",
  "43412" = "43412_5day-03132011_notide.txt",
  "46404" = "46404_5day-03132011_notide.txt",
  "51407" = "51407_5day-03132011_notide.txt",
  "51425" = "51425-03182011_notide.txt",
  "52402" = "52402_5day-03132011_notide.txt",
  "52403" = "52403-03182011_notide.txt",
  "52405" = "52405-03182011_notide.txt",
  "52406" = "52406-03182011_notide.txt"
)

param_xlim <- c(-100,25000)

benchmark_ylim <- list(
  "21401" = c(-1,1),
  "21413" = c(-0.8,1.3),
  "21414" = NULL,
  "21415" = NULL,
  "21418" = c(-1.7,2.2),
  "21419" = c(-0.5,0.7),
  "32412" = NULL,
  "43412" = NULL,
  "46404" = NULL,
  "51407" = NULL,
  "51425" = NULL,
  "52402" = NULL,
  "52403" = NULL,
  "52405" = NULL,
  "52406" = NULL
)


#which

benchmark_output_dir <- paste(benchmark_dir, benchmark_output, sep="/")
benchmark_measured_data_dir <- paste(benchmark_dir, benchmark_measured_data, sep="/")



attach(mtcars)
par(mfrow=c(length(visualize_darts), 1))

for (use_dart_nr in visualize_darts)
{
  benchmark_measured_data_filename = benchmark_measured_data_list[[use_dart_nr]]
  
  param_ylim = benchmark_ylim[[use_dart_nr]]
  
  # find dart file
  benchmark_measured_data_list_file <- paste(paste(benchmark_dir, benchmark_measured_data, sep="/"), benchmark_measured_data_filename, sep="/")
  
  # BENCHMARK OUTPUT FILES
  benchmark_output_files <- list.files(path=benchmark_output_dir, pattern=benchmark_output_group_regexp)
  
  if (length(benchmark_output_files) == 0)
  {
    print("NO files found")
    return
  }
  
  # READ DART DATASET
  benchmark_measured_data_list_file_dataset <- read.table(benchmark_measured_data_list_file)
  
  tmp_benchmark_output_files <- c()

  # use limitations to draw only a subset of images
  for (benchmark_output_filename in benchmark_output_files)
  {
    s <- sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4 \\5", benchmark_output_filename)

    tmp <- unlist(strsplit(s, split=" "))
    nd <- as.integer(tmp[1])
    na <- as.integer(tmp[2])
    ncfl <- as.double(tmp[3])
    nrk <- as.integer(tmp[4])
    ndart <- as.integer(tmp[5])

    
    if (limit_d != -1 && length(which(limit_d == nd)) == 0)
      next

    if (limit_d_even)
      if ((nd %% 2) == 1)
        next
    
    if (limit_a != -1 && length(which(limit_a == na)) == 0)
      next

    max_level <- na+nd
    if (limit_max_level != -1 && length(which(limit_max_level == max_level)) == 0)
      next
    
    if (limit_cfl != -1 && length(which(limit_cfl == ncfl)) == 0)
      next
    
    if (limit_rk != -1 && length(which(limit_rk == nrk)) == 0)
      next
    
    if (use_dart_nr != ndart)
      next
    
    tmp_benchmark_output_files <- c(tmp_benchmark_output_files, benchmark_output_filename)
  }
  
  benchmark_output_files <- tmp_benchmark_output_files

  rainbow_map <- c("black", rainbow(length(benchmark_output_files)))

  # START a new PLOT
  #plot_output_file <- "output"
  #plot_output_file <- paste(paste(plot_output_file, "_d", sep=""), d, sep="")
  #plot_output_file <- paste(paste(plot_output_file, "_a", sep=""), a, sep="")
  
  plot(benchmark_measured_data_list_file_dataset, type="l", xlab="", ylab="", ylim=param_ylim, xlim=param_xlim, col=rainbow_map[1])
  title(main=paste("Dart station ", use_dart_nr, sep=""), xlab="time since earthquake (seconds)", ylab="height relative to sealevel (meters)")
  
  legend_data <- c("Measured data")
  
  c <- 2
  for (benchmark_output_filename in benchmark_output_files)
  {
    tmp <- unlist(strsplit(sub(benchmark_output_group_regexp, "\\1 \\2 \\3 \\4", benchmark_output_filename), split=" "))
    nd <- tmp[1]
    na <- tmp[2]
    ncfl <- tmp[3]
    nrk <- tmp[4]

    dataset_name <- ""
    dataset_name <- paste(paste(dataset_name, "d=", sep=""), nd, sep="")
    dataset_name <- paste(dataset_name, ", ", sep="")
    dataset_name <- paste(paste(dataset_name, "a=", sep=""), na, sep="")
    dataset_name <- paste(dataset_name, ", ", sep="")
    dataset_name <- paste(paste(dataset_name, "cfl=", sep=""), ncfl, sep="")
#    dataset_name <- paste(dataset_name, ", ", sep="")
#    dataset_name <- paste(paste(dataset_name, "rk=", sep=""), nrk, sep="")
  
    # benchmark output filename
    benchmark_output_file <- paste(benchmark_output_dir, benchmark_output_filename, sep="/")
    benchmark_single_dataset <- read.table(benchmark_output_file, sep="\t")
    
    output_dataset_name <- sub(pattern=".txt", replace="", x=benchmark_output_filename)
    output_dataset_name <- sub(pattern="output_dart_", replace="", x=output_dataset_name)
    
    lines(benchmark_single_dataset$V1, benchmark_single_dataset$V2, type="l", col=rainbow_map[c], lwd=line_width)
    
    legend_data <- c(legend_data, dataset_name)
    c = c+1
  }
  
  legend("topright", legend_data, col=rainbow_map, lty=1)
  
  #dev.off()
}