#! /bin/bash

EXEC="../../build/sierpi_intel_tbb_tsunami_parallel_asagi_release"

PARAMS="-F 10 -y -1 -z -1 -L 12000 -o $((2*1024))"

D=`seq 8 20`
for d in $D; do

	A=`seq 6 $((28-d))`
	for a in $A; do

		for cfl in 0.2 0.25 0.3 0.35 0.4 0.45 0.5; do

			OUTPUTFILE="output_dart_d""$d""_a""$a""_cfl""$cfl"

			# dart output
			_PARAMS=" $PARAMS -D 935356.566012/-817289.628677/$OUTPUTFILE "

			# adaptivity
			_PARAMS=" $_PARAMS -a $a -d $d"
			
			# cfl
			_PARAMS=" $_PARAMS -C $cfl"
			
			echo "$EXEC $_PARAMS"
			$EXEC $_PARAMS
		done
	done
done
