#! /bin/sh

source /etc/profile.d/modules.sh

module unload ccomp
module load ccomp/intel/13.1
module unload gcc
module load gcc/4.7
module load tbb/4.1

module unload mpi.ibm
module load mpi.intel

export ASAGI_PATH=$HOME/work/di56zem2/local/intel
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ASAGI_PATH/lib:$TBB_LIBDIR
