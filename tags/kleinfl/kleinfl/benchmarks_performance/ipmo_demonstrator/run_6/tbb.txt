Copyright (C) 2009-2012 Intel Corporation. All rights reserved.
Intel(R) VTune(TM) Amplifier XE 2013 (build 243421)
usage: ./run_ipmo_async_demonstrator.sh [background process] [tbb/omp/ipmo]
default: ./run_ipmo_async_demonstrator.sh true ipmo
USING STARTING OF BACKGROUND PROCESSES WITH TBB
ROUND 1 (5 small loops)
	taskset -c 0-39 ../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release  -B 1 -v 0 -C 0.4 -c ../../scenarios/tohoku_netcdf_intel_release.xml -n 40 -d 16 -a 10
sleep 10
	taskset -c 0-39 ../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release  -B 1 -v 0 -C 0.4 -c ../../scenarios/tohoku_netcdf_intel_release.xml -n 40 -d 16 -a 10
sleep 15
	taskset -c 0-39 ../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release  -B 1 -v 0 -C 0.4 -c ../../scenarios/tohoku_netcdf_intel_release.xml -n 40 -d 17 -a 8
sleep 15
	taskset -c 0-39 ../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release  -B 1 -v 0 -C 0.4 -c ../../scenarios/tohoku_netcdf_intel_release.xml -n 40 -d 17 -a 8
sleep 15
	taskset -c 0-39 ../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release  -B 1 -v 0 -C 0.4 -c ../../scenarios/tohoku_netcdf_intel_release.xml -n 40 -d 16 -a 7
sleep 10
Seconds: 9398
