#! /bin/bash

cd ../../

export IPMO_ROOT="/home/schreibm/workspace/ipmo_2012_09_26"

scons --compiler=intel --simulation=hyperbolic_parallel --mode=release --enable-bathymetry=off --threading=omp -j 8
scons --compiler=intel --simulation=hyperbolic_parallel --mode=release --enable-bathymetry=off --threading=ipmo_async -j 8
