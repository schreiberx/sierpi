#! /bin/bash

IPMO_VERSION="ipmo_2013_10_28"


EXEC_THREADS_TBB="../../build/sierpi_intel_tbb_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release"
EXEC_THREADS_OMP="../../build/sierpi_intel_omp_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release"
EXEC_ITHREADS_ASYNC="../../build/sierpi_intel_ipmo_async_hyperbolic_parallel_tsunami_netcdf_type0_degree0_am2_release"

IPMO_PARAMS=" -n 40"
IPMO_PARAMS="$IPMO_PARAMS -v -99"

killall -w ${EXEC_THREADS_TBB/..\/..\/..\/build\//} 2>/dev/null
killall -w ${EXEC_THREADS_OMP/..\/..\/..\/build\//} 2>/dev/null
killall -w ${EXEC_ITHREADS_ASYNC/..\/..\/..\/build\//} 2>/dev/null

EXEC_IPMO="../../../../$IPMO_VERSION/build/server_ipmo_release"

#PARAMS=" -d 4 -o 8192 -B 1 -a 16 -v -99 -G 1"
PARAMS=" -B 1 -v 0 -C 0.4"


#BENCHMARK_DEPTH_PATTERN=(12 12 13 13 12)
#BENCHMARK_REFINE_PATTERN=(10 10 8 8 7)
#BENCHMARK_SLEEP_PATTERN=(10 15 15 15 10)
#BENCHMARK_LOOP_COUNTER="1"

BENCHMARK_DEPTH_PATTERN=(17 17 18 18 19)
BENCHMARK_REFINE_PATTERN=(10 10 8 8 7)
BENCHMARK_SLEEP_PATTERN=(10 15 15 15 10)
BENCHMARK_LOOP_COUNTER="1"

#BENCHMARK_DEPTH_PATTERN=(5 5 5 5 5)
#BENCHMARK_REFINE_PATTERN=(15 15 15 15 15)
#BENCHMARK_SLEEP_PATTERN=(5 5 5 5 5)
#BENCHMARK_LOOP_COUNTER="3"

#BENCHMARK_DEPTH_PATTERN=(5 6)
#BENCHMARK_REFINE_PATTERN=(12 10)
#BENCHMARK_SLEEP_PATTERN=(5 5)
#BENCHMARK_LOOP_COUNTER="2"

BENCHMARK_PATTERN_COUNTER=${#BENCHMARK_DEPTH_PATTERN[*]}

