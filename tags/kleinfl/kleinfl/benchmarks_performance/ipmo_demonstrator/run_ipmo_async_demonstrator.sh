#! /bin/sh

#
# demonstrator
#

. params.sh


BACKGROUND_PROCESS=true
test -n "$1" && BACKGROUND_PROCESS=$1

if $BACKGROUND_PROCESS; then
	killall server_ipmo_release
	../../../$IPMO_VERSION/build/server_ipmo_release  -c -v -99  -n 40 &

	echo "USING STARTING OF BACKGROUND PROCESSES WITH IPMO"
	EXEC="$EXEC_ITHREADS_ASYNC"
	OUTPUT_FILE="output_ipmo.txt"
else
	echo "USING SEQUENTIAL PROCESSES WITH OMP"
	EXEC="$EXEC_THREADS"
	OUTPUT_FILE="output_omp.txt"
	PARAMS="$PARAMS -n 40 -A 1"
fi

PROGRAM_START="$EXEC $PARAMS"


DATE=`date +%F`
DATE=${DATE/-/_}
DATE=${DATE/-/_}

STARTSECONDS=`date +%s`


for a in `seq 1 $BENCHMARK_LOOP_COUNTER`; do
	echo "ROUND $a ($BENCHMARK_PATTERN_COUNTER small loops)"

	P="$PROGRAM_START -d ${BENCHMARK_DEPTH_PATTERN[0]} -a ${BENCHMARK_REFINE_PATTERN[0]}"
#	echo "	$P"
	if $BACKGROUND_PROCESS; then
		$P > $OUTPUT_FILE &
	else
		$P > $OUTPUT_FILE 
	fi

	PROG_ID=$!

	S="sleep ${BENCHMARK_SLEEP_PATTERN[0]}"
#	echo $S
	$S

	for i in `seq 1 $((BENCHMARK_PATTERN_COUNTER-1))`; do

		P="$PROGRAM_START -d ${BENCHMARK_DEPTH_PATTERN[i]} -a ${BENCHMARK_REFINE_PATTERN[i]}"
#		echo "	$P"
		if $BACKGROUND_PROCESS; then
			$P > $OUTPUT_FILE &
		else
			$P > $OUTPUT_FILE 
		fi
		PROG_IDS[$i]=$!

		S="sleep ${BENCHMARK_SLEEP_PATTERN[i]}"
#		echo $S
		$S
	done

	wait $PROG_ID
done


if $BACKGROUND_PROCESS; then
	for i in `seq 1 $((BENCHMARK_PATTERN_COUNTER-1))`; do
		wait ${PROG_IDS[i]}
	done
fi

ENDSECONDS=`date +%s`
SECONDS="$((ENDSECONDS-STARTSECONDS))"
echo "Seconds: $SECONDS"

exit -1
