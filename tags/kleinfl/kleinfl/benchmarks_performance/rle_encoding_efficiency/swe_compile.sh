#! /bin/bash

cd ../..

scons --sub-simulation=swe --enable-libxml=off --threading=omp --enable-traversals-with-nodes=on --enable-print-rle-statistics=on -j4

