#! /bin/bash

# output
#SBATCH -o /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability/2013_03_17c//sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_hyperbolic_parallel_tsunami_b0_release_d22_a08_s001024_t001_k100_n256.txt
# working directory
#SBATCH -D /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability
# job description
#SBATCH -J sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_hyperbolic_parallel_tsunami_b0_release_d22_a08_s001024_t001_k100_n256
#SBATCH --get-user-env
#SBATCH --partition=snb
#SBATCH --ntasks=256
#SBATCH --cpus-per-task=2
#SBATCH --mail-type=end
#SBATCH --mail-user=martin.schreiber@in.tum.de
#SBATCH --export=NONE
#SBATCH --time=00:30:00

source /etc/profile.d/modules.sh

source ./inc_vars.sh

cd /home/hpc/pr63so/di69fol/workspace/sierpi_2013_03_17_mpi_scalability/benchmarks_performance/mac_cluster_strong_scalability

mpiexec.hydra -genv OMP_NUM_THREADS 1 -envall -ppn 16 -n 256 ../../build/sierpi_intel_mpi_tbb_scan_data_scan_based_split_and_join_hyperbolic_parallel_tsunami_b0_release -d 22 -a 8 -t -1 -L 100 -k 100 -r 0.01/0.005 -n 1 -N 1 -o 1024
