Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!

Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264910848 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64911e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.51 ACPST (Averaged number of clusters per Simulation Timestep)
3.76266 MCPS (Million Cells per Second) (local)
3.76266 MCPSPT (Million Clusters per Second per Thread)
80.8444 EDMBPT (CellData Megabyte per Timestep (RW))
114.827 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203184 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264900933 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.12 ACPST (Averaged number of clusters per Simulation Timestep)
3.76252 MCPS (Million Cells per Second) (local)
3.76252 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
114.823 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203176 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4053 RT (REAL_TIME)
264918377 CP (Cells processed)
0.704053 ASPT (Averaged Seconds per Timestep)
2.64918e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.31 ACPST (Averaged number of clusters per Simulation Timestep)
3.76276 MCPS (Million Cells per Second) (local)
3.76276 MCPSPT (Million Clusters per Second per Thread)
80.8467 EDMBPT (CellData Megabyte per Timestep (RW))
114.83 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203189 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4054 RT (REAL_TIME)
264906711 CP (Cells processed)
0.704054 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1282.32 ACPST (Averaged number of clusters per Simulation Timestep)
3.76259 MCPS (Million Cells per Second) (local)
3.76259 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
114.825 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.20318 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4054 RT (REAL_TIME)
264904704 CP (Cells processed)
0.704054 ASPT (Averaged Seconds per Timestep)
2.64905e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.48 ACPST (Averaged number of clusters per Simulation Timestep)
3.76256 MCPS (Million Cells per Second) (local)
3.76256 MCPSPT (Million Clusters per Second per Thread)
80.8425 EDMBPT (CellData Megabyte per Timestep (RW))
114.824 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203178 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4054 RT (REAL_TIME)
264912896 CP (Cells processed)
0.704054 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
3.76268 MCPS (Million Cells per Second) (local)
3.76268 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
114.828 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203185 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4054 RT (REAL_TIME)
264908800 CP (Cells processed)
0.704054 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
3.76262 MCPS (Million Cells per Second) (local)
3.76262 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
114.826 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203182 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264914944 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64915e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.53 ACPST (Averaged number of clusters per Simulation Timestep)
3.76272 MCPS (Million Cells per Second) (local)
3.76272 MCPSPT (Million Clusters per Second per Thread)
80.8456 EDMBPT (CellData Megabyte per Timestep (RW))
114.829 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203187 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264900608 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64901e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.46 ACPST (Averaged number of clusters per Simulation Timestep)
3.76251 MCPS (Million Cells per Second) (local)
3.76251 MCPSPT (Million Clusters per Second per Thread)
80.8413 EDMBPT (CellData Megabyte per Timestep (RW))
114.823 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203176 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264923229 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64923e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.35 ACPST (Averaged number of clusters per Simulation Timestep)
3.76283 MCPS (Million Cells per Second) (local)
3.76283 MCPSPT (Million Clusters per Second per Thread)
80.8482 EDMBPT (CellData Megabyte per Timestep (RW))
114.833 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203193 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264896343 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.05 ACPST (Averaged number of clusters per Simulation Timestep)
3.76245 MCPS (Million Cells per Second) (local)
3.76245 MCPSPT (Million Clusters per Second per Thread)
80.8399 EDMBPT (CellData Megabyte per Timestep (RW))
114.821 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203172 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264912351 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64912e+06 CPST (Cells Processed in Average per Simulation Timestep)
1292.56 ACPST (Averaged number of clusters per Simulation Timestep)
3.76268 MCPS (Million Cells per Second) (local)
3.76268 MCPSPT (Million Clusters per Second per Thread)
80.8448 EDMBPT (CellData Megabyte per Timestep (RW))
114.828 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203185 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264906752 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64907e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.49 ACPST (Averaged number of clusters per Simulation Timestep)
3.7626 MCPS (Million Cells per Second) (local)
3.7626 MCPSPT (Million Clusters per Second per Thread)
80.8431 EDMBPT (CellData Megabyte per Timestep (RW))
114.825 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.20318 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264912896 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64913e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.52 ACPST (Averaged number of clusters per Simulation Timestep)
3.76269 MCPS (Million Cells per Second) (local)
3.76269 MCPSPT (Million Clusters per Second per Thread)
80.845 EDMBPT (CellData Megabyte per Timestep (RW))
114.828 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203185 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264908624 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1287.58 ACPST (Averaged number of clusters per Simulation Timestep)
3.76263 MCPS (Million Cells per Second) (local)
3.76263 MCPSPT (Million Clusters per Second per Thread)
80.8437 EDMBPT (CellData Megabyte per Timestep (RW))
114.826 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203182 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.4052 RT (REAL_TIME)
264908800 CP (Cells processed)
0.704052 ASPT (Averaged Seconds per Timestep)
2.64909e+06 CPST (Cells Processed in Average per Simulation Timestep)
1293.5 ACPST (Averaged number of clusters per Simulation Timestep)
3.76263 MCPS (Million Cells per Second) (local)
3.76263 MCPSPT (Million Clusters per Second per Thread)
80.8438 EDMBPT (CellData Megabyte per Timestep (RW))
114.826 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203182 GFLOPS


++++++++++ SUMMARY ++++++++++
60.2021 GMCPS (Global Million Cells per Second) (global)
60.2021 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OKEXIT STATUS: OK

EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
