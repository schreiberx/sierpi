Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.0421667 ST (SIMULATION_TIME)
0.00419882 TSS (Timestep size)
108.579 RT (REAL_TIME)
420821188 CP (Cells processed)
10.8579 ASPT (Averaged Seconds per Timestep)
4.20821e+07 CPST (Cells Processed in Average per Simulation Timestep)
20524.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.87571 MCPS (Million Cells per Second) (local)
3.87571 MCPSPT (Million Clusters per Second per Thread)
1284.24 EDMBPT (CellData Megabyte per Timestep (RW))
118.277 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.209288 GFLOPS

++++++++++ SUMMARY ++++++++++
3.87571 GMCPS (Global Million Cells per Second) (global)
3.87571 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
