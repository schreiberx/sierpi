Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
956.285 RT (REAL_TIME)
4238547818 CP (Cells processed)
9.56285 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
49.09 ACPST (Averaged number of clusters per Simulation Timestep)
4.4323 MCPS (Million Cells per Second) (local)
4.4323 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
135.263 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.239344 GFLOPS

++++++++++ SUMMARY ++++++++++
4.4323 GMCPS (Global Million Cells per Second) (global)
4.4323 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
