Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
149.966 RT (REAL_TIME)
4238547816 CP (Cells processed)
1.49966 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
20522 ACPST (Averaged number of clusters per Simulation Timestep)
28.2635 MCPS (Million Cells per Second) (local)
3.53293 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
862.533 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.52623 GFLOPS

++++++++++ SUMMARY ++++++++++
28.2635 GMCPS (Global Million Cells per Second) (global)
3.53293 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
