Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!

Warning from RANK 14: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264359936 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
3.76165 MCPS (Million Cells per Second) (local)
3.76165 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
114.797 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203129 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
265457989 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.65458e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.13 ACPST (Averaged number of clusters per Simulation Timestep)
3.77728 MCPS (Million Cells per Second) (local)
3.77728 MCPSPT (Million Clusters per Second per Thread)
81.0113 EDMBPT (CellData Megabyte per Timestep (RW))
115.273 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203973 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
265839977 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.6584e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.99 ACPST (Averaged number of clusters per Simulation Timestep)
3.78271 MCPS (Million Cells per Second) (local)
3.78271 MCPSPT (Million Clusters per Second per Thread)
81.1279 EDMBPT (CellData Megabyte per Timestep (RW))
115.439 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.204267 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
266020823 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.66021e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.29 ACPST (Averaged number of clusters per Simulation Timestep)
3.78529 MCPS (Million Cells per Second) (local)
3.78529 MCPSPT (Million Clusters per Second per Thread)
81.1831 EDMBPT (CellData Megabyte per Timestep (RW))
115.518 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.204405 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264364032 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64364e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.84 ACPST (Averaged number of clusters per Simulation Timestep)
3.76171 MCPS (Million Cells per Second) (local)
3.76171 MCPSPT (Million Clusters per Second per Thread)
80.6775 EDMBPT (CellData Megabyte per Timestep (RW))
114.798 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203132 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264359936 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
3.76165 MCPS (Million Cells per Second) (local)
3.76165 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
114.797 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203129 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264402944 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
3.76227 MCPS (Million Cells per Second) (local)
3.76227 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
114.815 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203162 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264321024 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
3.7611 MCPS (Million Cells per Second) (local)
3.7611 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
114.78 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203099 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264402944 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
3.76227 MCPS (Million Cells per Second) (local)
3.76227 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
114.815 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203162 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264804969 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64805e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.1 ACPST (Averaged number of clusters per Simulation Timestep)
3.76799 MCPS (Million Cells per Second) (local)
3.76799 MCPSPT (Million Clusters per Second per Thread)
80.8121 EDMBPT (CellData Megabyte per Timestep (RW))
114.99 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203471 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
266659796 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.6666e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.49 ACPST (Averaged number of clusters per Simulation Timestep)
3.79438 MCPS (Million Cells per Second) (local)
3.79438 MCPSPT (Million Clusters per Second per Thread)
81.3781 EDMBPT (CellData Megabyte per Timestep (RW))
115.795 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.204896 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264381270 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64381e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.05 ACPST (Averaged number of clusters per Simulation Timestep)
3.76196 MCPS (Million Cells per Second) (local)
3.76196 MCPSPT (Million Clusters per Second per Thread)
80.6828 EDMBPT (CellData Megabyte per Timestep (RW))
114.806 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203146 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264321024 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
3.7611 MCPS (Million Cells per Second) (local)
3.7611 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
114.78 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203099 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264402944 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
3.76227 MCPS (Million Cells per Second) (local)
3.76227 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
114.815 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203162 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
266088272 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.66088e+06 CPST (Cells Processed in Average per Simulation Timestep)
1280.76 ACPST (Averaged number of clusters per Simulation Timestep)
3.78625 MCPS (Million Cells per Second) (local)
3.78625 MCPSPT (Million Clusters per Second per Thread)
81.2037 EDMBPT (CellData Megabyte per Timestep (RW))
115.547 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.204457 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
70.2776 RT (REAL_TIME)
264359936 CP (Cells processed)
0.702776 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
3.76165 MCPS (Million Cells per Second) (local)
3.76165 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
114.797 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.203129 GFLOPS


++++++++++ SUMMARY ++++++++++
60.3115 GMCPS (Global Million Cells per Second) (global)
60.3115 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
