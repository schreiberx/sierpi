Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.705 RT (REAL_TIME)
1066342375 CP (Cells processed)
2.39705 ASPT (Averaged Seconds per Timestep)
1.06634e+07 CPST (Cells Processed in Average per Simulation Timestep)
47.6 ACPST (Averaged number of clusters per Simulation Timestep)
4.44856 MCPS (Million Cells per Second) (local)
4.44856 MCPSPT (Million Clusters per Second per Thread)
325.422 EDMBPT (CellData Megabyte per Timestep (RW))
135.759 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.240222 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.705 RT (REAL_TIME)
1052587700 CP (Cells processed)
2.39705 ASPT (Averaged Seconds per Timestep)
1.05259e+07 CPST (Cells Processed in Average per Simulation Timestep)
45.35 ACPST (Averaged number of clusters per Simulation Timestep)
4.39118 MCPS (Million Cells per Second) (local)
4.39118 MCPSPT (Million Clusters per Second per Thread)
321.224 EDMBPT (CellData Megabyte per Timestep (RW))
134.008 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.237124 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.705 RT (REAL_TIME)
1061396175 CP (Cells processed)
2.39705 ASPT (Averaged Seconds per Timestep)
1.0614e+07 CPST (Cells Processed in Average per Simulation Timestep)
41.8 ACPST (Averaged number of clusters per Simulation Timestep)
4.42793 MCPS (Million Cells per Second) (local)
4.42793 MCPSPT (Million Clusters per Second per Thread)
323.912 EDMBPT (CellData Megabyte per Timestep (RW))
135.13 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.239108 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.705 RT (REAL_TIME)
1058221568 CP (Cells processed)
2.39705 ASPT (Averaged Seconds per Timestep)
1.05822e+07 CPST (Cells Processed in Average per Simulation Timestep)
49.25 ACPST (Averaged number of clusters per Simulation Timestep)
4.41468 MCPS (Million Cells per Second) (local)
4.41468 MCPSPT (Million Clusters per Second per Thread)
322.944 EDMBPT (CellData Megabyte per Timestep (RW))
134.725 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238393 GFLOPS


++++++++++ SUMMARY ++++++++++
17.6823 GMCPS (Global Million Cells per Second) (global)
17.6823 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
