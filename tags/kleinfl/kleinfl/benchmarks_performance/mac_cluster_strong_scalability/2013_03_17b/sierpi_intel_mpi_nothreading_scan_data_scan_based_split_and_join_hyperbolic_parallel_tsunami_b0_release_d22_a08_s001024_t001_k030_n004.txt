Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.003 RT (REAL_TIME)
1062586535 CP (Cells processed)
2.39003 ASPT (Averaged Seconds per Timestep)
1.06259e+07 CPST (Cells Processed in Average per Simulation Timestep)
37.3 ACPST (Averaged number of clusters per Simulation Timestep)
4.44591 MCPS (Million Cells per Second) (local)
4.44591 MCPSPT (Million Clusters per Second per Thread)
324.276 EDMBPT (CellData Megabyte per Timestep (RW))
135.678 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.240079 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.003 RT (REAL_TIME)
1056542300 CP (Cells processed)
2.39003 ASPT (Averaged Seconds per Timestep)
1.05654e+07 CPST (Cells Processed in Average per Simulation Timestep)
58.8 ACPST (Averaged number of clusters per Simulation Timestep)
4.42062 MCPS (Million Cells per Second) (local)
4.42062 MCPSPT (Million Clusters per Second per Thread)
322.431 EDMBPT (CellData Megabyte per Timestep (RW))
134.907 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238713 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.003 RT (REAL_TIME)
1060366855 CP (Cells processed)
2.39003 ASPT (Averaged Seconds per Timestep)
1.06037e+07 CPST (Cells Processed in Average per Simulation Timestep)
24 ACPST (Averaged number of clusters per Simulation Timestep)
4.43662 MCPS (Million Cells per Second) (local)
4.43662 MCPSPT (Million Clusters per Second per Thread)
323.598 EDMBPT (CellData Megabyte per Timestep (RW))
135.395 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.239578 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.003 RT (REAL_TIME)
1059052128 CP (Cells processed)
2.39003 ASPT (Averaged Seconds per Timestep)
1.05905e+07 CPST (Cells Processed in Average per Simulation Timestep)
56.7 ACPST (Averaged number of clusters per Simulation Timestep)
4.43112 MCPS (Million Cells per Second) (local)
4.43112 MCPSPT (Million Clusters per Second per Thread)
323.197 EDMBPT (CellData Megabyte per Timestep (RW))
135.227 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.239281 GFLOPS


++++++++++ SUMMARY ++++++++++
17.7343 GMCPS (Global Million Cells per Second) (global)
17.7343 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
