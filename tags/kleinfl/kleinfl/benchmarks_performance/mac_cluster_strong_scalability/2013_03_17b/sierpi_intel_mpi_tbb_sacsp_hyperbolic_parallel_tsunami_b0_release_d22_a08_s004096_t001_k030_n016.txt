Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 3: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264359936 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
4.91258 MCPS (Million Cells per Second) (local)
4.91258 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
149.92 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265279 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
265457989 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.65458e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.13 ACPST (Averaged number of clusters per Simulation Timestep)
4.93299 MCPS (Million Cells per Second) (local)
4.93299 MCPSPT (Million Clusters per Second per Thread)
81.0113 EDMBPT (CellData Megabyte per Timestep (RW))
150.543 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.266381 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
265839977 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.6584e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.99 ACPST (Averaged number of clusters per Simulation Timestep)
4.94009 MCPS (Million Cells per Second) (local)
4.94009 MCPSPT (Million Clusters per Second per Thread)
81.1279 EDMBPT (CellData Megabyte per Timestep (RW))
150.759 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.266765 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
266020823 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.66021e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.29 ACPST (Averaged number of clusters per Simulation Timestep)
4.94345 MCPS (Million Cells per Second) (local)
4.94345 MCPSPT (Million Clusters per Second per Thread)
81.1831 EDMBPT (CellData Megabyte per Timestep (RW))
150.862 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.266946 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264364032 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64364e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.84 ACPST (Averaged number of clusters per Simulation Timestep)
4.91266 MCPS (Million Cells per Second) (local)
4.91266 MCPSPT (Million Clusters per Second per Thread)
80.6775 EDMBPT (CellData Megabyte per Timestep (RW))
149.922 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265284 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264359936 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
4.91258 MCPS (Million Cells per Second) (local)
4.91258 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
149.92 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265279 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264402944 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
4.91338 MCPS (Million Cells per Second) (local)
4.91338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
149.945 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265323 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264321024 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
4.91186 MCPS (Million Cells per Second) (local)
4.91186 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
149.898 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.26524 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264402944 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
4.91338 MCPS (Million Cells per Second) (local)
4.91338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
149.945 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265323 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264804969 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64805e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.1 ACPST (Averaged number of clusters per Simulation Timestep)
4.92085 MCPS (Million Cells per Second) (local)
4.92085 MCPSPT (Million Clusters per Second per Thread)
80.8121 EDMBPT (CellData Megabyte per Timestep (RW))
150.172 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265726 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
266659796 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.6666e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.49 ACPST (Averaged number of clusters per Simulation Timestep)
4.95532 MCPS (Million Cells per Second) (local)
4.95532 MCPSPT (Million Clusters per Second per Thread)
81.3781 EDMBPT (CellData Megabyte per Timestep (RW))
151.224 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.267587 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264381270 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64381e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.05 ACPST (Averaged number of clusters per Simulation Timestep)
4.91298 MCPS (Million Cells per Second) (local)
4.91298 MCPSPT (Million Clusters per Second per Thread)
80.6828 EDMBPT (CellData Megabyte per Timestep (RW))
149.932 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265301 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264321024 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
4.91186 MCPS (Million Cells per Second) (local)
4.91186 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
149.898 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.26524 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264402944 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
4.91338 MCPS (Million Cells per Second) (local)
4.91338 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
149.945 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265323 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
266088272 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.66088e+06 CPST (Cells Processed in Average per Simulation Timestep)
1280.76 ACPST (Averaged number of clusters per Simulation Timestep)
4.9447 MCPS (Million Cells per Second) (local)
4.9447 MCPSPT (Million Clusters per Second per Thread)
81.2037 EDMBPT (CellData Megabyte per Timestep (RW))
150.9 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.267014 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
53.8128 RT (REAL_TIME)
264359936 CP (Cells processed)
0.538128 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
4.91258 MCPS (Million Cells per Second) (local)
4.91258 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
149.92 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.265279 GFLOPS


++++++++++ SUMMARY ++++++++++
78.7646 GMCPS (Global Million Cells per Second) (global)
78.7646 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
