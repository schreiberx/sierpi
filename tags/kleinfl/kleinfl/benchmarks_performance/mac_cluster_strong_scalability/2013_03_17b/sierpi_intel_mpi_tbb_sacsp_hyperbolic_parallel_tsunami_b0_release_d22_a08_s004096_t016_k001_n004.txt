Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 2: more nodes than initial clusters available!
3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
17.1477 RT (REAL_TIME)
1059636869 CP (Cells processed)
0.171477 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5158.26 ACPST (Averaged number of clusters per Simulation Timestep)
61.7947 MCPS (Million Cells per Second) (local)
3.86217 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1885.82 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.33691 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
17.1478 RT (REAL_TIME)
1059641344 CP (Cells processed)
0.171478 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5174.03 ACPST (Averaged number of clusters per Simulation Timestep)
61.7947 MCPS (Million Cells per Second) (local)
3.86217 MCPSPT (Million Clusters per Second per Thread)
323.377 EDMBPT (CellData Megabyte per Timestep (RW))
1885.83 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.33692 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
17.147 RT (REAL_TIME)
1059632531 CP (Cells processed)
0.17147 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
5165.42 ACPST (Averaged number of clusters per Simulation Timestep)
61.797 MCPS (Million Cells per Second) (local)
3.86231 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
1885.89 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.33704 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
17.147 RT (REAL_TIME)
1059637072 CP (Cells processed)
0.17147 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
5168.09 ACPST (Averaged number of clusters per Simulation Timestep)
61.7972 MCPS (Million Cells per Second) (local)
3.86233 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
1885.9 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
3.33705 GFLOPS


++++++++++ SUMMARY ++++++++++
247.184 GMCPS (Global Million Cells per Second) (global)
15.449 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
