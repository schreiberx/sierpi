Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 12: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
4: more nodes than initial clusters available!
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264359936 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
13.7178 MCPS (Million Cells per Second) (local)
3.42944 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
418.633 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.74076 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
265457989 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.65458e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.13 ACPST (Averaged number of clusters per Simulation Timestep)
13.7748 MCPS (Million Cells per Second) (local)
3.44369 MCPSPT (Million Clusters per Second per Thread)
81.0113 EDMBPT (CellData Megabyte per Timestep (RW))
420.372 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.743837 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
265839977 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.6584e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.99 ACPST (Averaged number of clusters per Simulation Timestep)
13.7946 MCPS (Million Cells per Second) (local)
3.44865 MCPSPT (Million Clusters per Second per Thread)
81.1279 EDMBPT (CellData Megabyte per Timestep (RW))
420.977 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.744907 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
266020823 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.66021e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.29 ACPST (Averaged number of clusters per Simulation Timestep)
13.804 MCPS (Million Cells per Second) (local)
3.45099 MCPSPT (Million Clusters per Second per Thread)
81.1831 EDMBPT (CellData Megabyte per Timestep (RW))
421.264 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.745414 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264364032 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64364e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.84 ACPST (Averaged number of clusters per Simulation Timestep)
13.718 MCPS (Million Cells per Second) (local)
3.4295 MCPSPT (Million Clusters per Second per Thread)
80.6775 EDMBPT (CellData Megabyte per Timestep (RW))
418.64 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740772 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264359936 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
13.7178 MCPS (Million Cells per Second) (local)
3.42944 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
418.633 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.74076 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264402944 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
13.72 MCPS (Million Cells per Second) (local)
3.43 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
418.702 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740881 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264321024 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
13.7158 MCPS (Million Cells per Second) (local)
3.42894 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
418.572 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740651 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264402944 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
13.72 MCPS (Million Cells per Second) (local)
3.43 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
418.701 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.74088 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264804969 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64805e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.1 ACPST (Averaged number of clusters per Simulation Timestep)
13.7409 MCPS (Million Cells per Second) (local)
3.43522 MCPSPT (Million Clusters per Second per Thread)
80.8121 EDMBPT (CellData Megabyte per Timestep (RW))
419.338 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.742007 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
266659796 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.6666e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.49 ACPST (Averaged number of clusters per Simulation Timestep)
13.8371 MCPS (Million Cells per Second) (local)
3.45928 MCPSPT (Million Clusters per Second per Thread)
81.3781 EDMBPT (CellData Megabyte per Timestep (RW))
422.275 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.747204 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264381270 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64381e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.05 ACPST (Averaged number of clusters per Simulation Timestep)
13.7189 MCPS (Million Cells per Second) (local)
3.42972 MCPSPT (Million Clusters per Second per Thread)
80.6828 EDMBPT (CellData Megabyte per Timestep (RW))
418.667 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.74082 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264321024 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
13.7158 MCPS (Million Cells per Second) (local)
3.42894 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
418.572 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740652 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264402944 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
13.72 MCPS (Million Cells per Second) (local)
3.43 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
418.702 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740881 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
266088272 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.66088e+06 CPST (Cells Processed in Average per Simulation Timestep)
1280.76 ACPST (Averaged number of clusters per Simulation Timestep)
13.8075 MCPS (Million Cells per Second) (local)
3.45187 MCPSPT (Million Clusters per Second per Thread)
81.2037 EDMBPT (CellData Megabyte per Timestep (RW))
421.371 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.745604 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
19.2713 RT (REAL_TIME)
264359936 CP (Cells processed)
0.192713 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
13.7178 MCPS (Million Cells per Second) (local)
3.42945 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
418.634 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.740761 GFLOPS


++++++++++ SUMMARY ++++++++++
219.941 GMCPS (Global Million Cells per Second) (global)
54.9851 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: EXIT STATUS: OK
EXIT STATUS: OKOK

EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
