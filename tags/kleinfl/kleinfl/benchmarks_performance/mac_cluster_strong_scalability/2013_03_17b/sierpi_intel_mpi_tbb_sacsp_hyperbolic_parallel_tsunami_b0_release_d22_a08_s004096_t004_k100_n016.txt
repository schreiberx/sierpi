Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 6: more nodes than initial clusters available!
Warning from RANK 7: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!
Warning from RANK 12: more nodes than initial clusters available!Warning from RANK 9: more nodes than initial clusters available!
Warning from RANK 13: more nodes than initial clusters available!

Warning from RANK 4: more nodes than initial clusters available!
Warning from RANK 5: more nodes than initial clusters available!
Warning from RANK 10: more nodes than initial clusters available!
Warning from RANK 14: more nodes than initial clusters available!
Warning from RANK 11: more nodes than initial clusters available!
Warning from RANK 15: more nodes than initial clusters available!
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
262922240 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
17.6601 MCPS (Million Cells per Second) (local)
4.41502 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
538.943 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.953645 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
266895685 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.66896e+06 CPST (Cells Processed in Average per Simulation Timestep)
1286.45 ACPST (Averaged number of clusters per Simulation Timestep)
17.927 MCPS (Million Cells per Second) (local)
4.48174 MCPSPT (Million Clusters per Second per Thread)
81.4501 EDMBPT (CellData Megabyte per Timestep (RW))
547.088 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.968057 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
268119401 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.68119e+06 CPST (Cells Processed in Average per Simulation Timestep)
1278.28 ACPST (Averaged number of clusters per Simulation Timestep)
18.0092 MCPS (Million Cells per Second) (local)
4.50229 MCPSPT (Million Clusters per Second per Thread)
81.8235 EDMBPT (CellData Megabyte per Timestep (RW))
549.596 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.972495 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
268855255 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.68855e+06 CPST (Cells Processed in Average per Simulation Timestep)
1272.91 ACPST (Averaged number of clusters per Simulation Timestep)
18.0586 MCPS (Million Cells per Second) (local)
4.51465 MCPSPT (Million Clusters per Second per Thread)
82.0481 EDMBPT (CellData Megabyte per Timestep (RW))
551.105 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.975164 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
263090176 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
17.6714 MCPS (Million Cells per Second) (local)
4.41784 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
539.287 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.954254 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
262922240 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
17.6601 MCPS (Million Cells per Second) (local)
4.41502 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
538.943 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.953645 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
263090176 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
17.6714 MCPS (Million Cells per Second) (local)
4.41784 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
539.287 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.954254 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
262922240 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
17.6601 MCPS (Million Cells per Second) (local)
4.41502 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
538.943 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.953645 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
263090176 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
17.6714 MCPS (Million Cells per Second) (local)
4.41785 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
539.288 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.954255 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
264471532 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.64472e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.72 ACPST (Averaged number of clusters per Simulation Timestep)
17.7642 MCPS (Million Cells per Second) (local)
4.44104 MCPSPT (Million Clusters per Second per Thread)
80.7103 EDMBPT (CellData Megabyte per Timestep (RW))
542.119 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.959265 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
271078985 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.71079e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.36 ACPST (Averaged number of clusters per Simulation Timestep)
18.208 MCPS (Million Cells per Second) (local)
4.55199 MCPSPT (Million Clusters per Second per Thread)
82.7267 EDMBPT (CellData Megabyte per Timestep (RW))
555.663 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.983231 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
263088990 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.63089e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.44 ACPST (Averaged number of clusters per Simulation Timestep)
17.6713 MCPS (Million Cells per Second) (local)
4.41783 MCPSPT (Million Clusters per Second per Thread)
80.2884 EDMBPT (CellData Megabyte per Timestep (RW))
539.285 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.95425 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
262922240 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
17.6601 MCPS (Million Cells per Second) (local)
4.41502 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
538.943 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.953645 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
263090176 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.6309e+06 CPST (Cells Processed in Average per Simulation Timestep)
1284.62 ACPST (Averaged number of clusters per Simulation Timestep)
17.6714 MCPS (Million Cells per Second) (local)
4.41784 MCPSPT (Million Clusters per Second per Thread)
80.2887 EDMBPT (CellData Megabyte per Timestep (RW))
539.287 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.954254 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
269066064 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.69066e+06 CPST (Cells Processed in Average per Simulation Timestep)
1276.37 ACPST (Averaged number of clusters per Simulation Timestep)
18.0728 MCPS (Million Cells per Second) (local)
4.51819 MCPSPT (Million Clusters per Second per Thread)
82.1124 EDMBPT (CellData Megabyte per Timestep (RW))
551.537 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.975929 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
14.8879 RT (REAL_TIME)
262922240 CP (Cells processed)
0.148879 ASPT (Averaged Seconds per Timestep)
2.62922e+06 CPST (Cells Processed in Average per Simulation Timestep)
1283.8 ACPST (Averaged number of clusters per Simulation Timestep)
17.6601 MCPS (Million Cells per Second) (local)
4.41502 MCPSPT (Million Clusters per Second per Thread)
80.2375 EDMBPT (CellData Megabyte per Timestep (RW))
538.943 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.953645 GFLOPS


++++++++++ SUMMARY ++++++++++
284.697 GMCPS (Global Million Cells per Second) (global)
71.1742 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
