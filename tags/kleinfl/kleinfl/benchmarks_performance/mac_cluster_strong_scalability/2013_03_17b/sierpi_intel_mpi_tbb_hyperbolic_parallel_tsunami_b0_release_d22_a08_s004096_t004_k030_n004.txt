Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.514 RT (REAL_TIME)
1061678725 CP (Cells processed)
0.72514 ASPT (Averaged Seconds per Timestep)
1.06168e+07 CPST (Cells Processed in Average per Simulation Timestep)
5141.23 ACPST (Averaged number of clusters per Simulation Timestep)
14.641 MCPS (Million Cells per Second) (local)
3.66026 MCPSPT (Million Clusters per Second per Thread)
323.999 EDMBPT (CellData Megabyte per Timestep (RW))
446.809 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.790615 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.514 RT (REAL_TIME)
1057447936 CP (Cells processed)
0.72514 ASPT (Averaged Seconds per Timestep)
1.05745e+07 CPST (Cells Processed in Average per Simulation Timestep)
5163.32 ACPST (Averaged number of clusters per Simulation Timestep)
14.5827 MCPS (Million Cells per Second) (local)
3.64567 MCPSPT (Million Clusters per Second per Thread)
322.707 EDMBPT (CellData Megabyte per Timestep (RW))
445.028 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.787465 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.514 RT (REAL_TIME)
1060248979 CP (Cells processed)
0.72514 ASPT (Averaged Seconds per Timestep)
1.06025e+07 CPST (Cells Processed in Average per Simulation Timestep)
5150.67 ACPST (Averaged number of clusters per Simulation Timestep)
14.6213 MCPS (Million Cells per Second) (local)
3.65533 MCPSPT (Million Clusters per Second per Thread)
323.562 EDMBPT (CellData Megabyte per Timestep (RW))
446.207 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.789551 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
72.514 RT (REAL_TIME)
1059172176 CP (Cells processed)
0.72514 ASPT (Averaged Seconds per Timestep)
1.05917e+07 CPST (Cells Processed in Average per Simulation Timestep)
5153.24 ACPST (Averaged number of clusters per Simulation Timestep)
14.6065 MCPS (Million Cells per Second) (local)
3.65161 MCPSPT (Million Clusters per Second per Thread)
323.234 EDMBPT (CellData Megabyte per Timestep (RW))
445.754 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.788749 GFLOPS


++++++++++ SUMMARY ++++++++++
58.4515 GMCPS (Global Million Cells per Second) (global)
14.6129 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
