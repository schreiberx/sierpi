Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
983.361 RT (REAL_TIME)
4238547818 CP (Cells processed)
9.83361 ASPT (Averaged Seconds per Timestep)
4.23855e+07 CPST (Cells Processed in Average per Simulation Timestep)
49.53 ACPST (Averaged number of clusters per Simulation Timestep)
4.31027 MCPS (Million Cells per Second) (local)
4.31027 MCPSPT (Million Clusters per Second per Thread)
1293.5 EDMBPT (CellData Megabyte per Timestep (RW))
131.539 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.232754 GFLOPS

++++++++++ SUMMARY ++++++++++
4.31027 GMCPS (Global Million Cells per Second) (global)
4.31027 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
