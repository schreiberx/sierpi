Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 7: more nodes than initial clusters available!
14: more nodes than initial clusters available!Warning from RANK 
10: more nodes than initial clusters available!
Warning from RANK 8: more nodes than initial clusters available!Warning from RANK Warning from RANK 
Warning from RANK Warning from RANK Warning from RANK 15: more nodes than initial clusters available!6: more nodes than initial clusters available!Warning from RANK Warning from RANK 9: more nodes than initial clusters available!
12: more nodes than initial clusters available!13: more nodes than initial clusters available!
Warning from RANK Warning from RANK 
5: more nodes than initial clusters available!4: more nodes than initial clusters available!
Warning from RANK 

3: more nodes than initial clusters available!
2: more nodes than initial clusters available!

11: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03656 RT (REAL_TIME)
264359936 CP (Cells processed)
0.0903656 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
29.2545 MCPS (Million Cells per Second) (local)
1.82841 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
892.776 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57974 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03654 RT (REAL_TIME)
265457989 CP (Cells processed)
0.0903654 ASPT (Averaged Seconds per Timestep)
2.65458e+06 CPST (Cells Processed in Average per Simulation Timestep)
1289.13 ACPST (Averaged number of clusters per Simulation Timestep)
29.3761 MCPS (Million Cells per Second) (local)
1.836 MCPSPT (Million Clusters per Second per Thread)
81.0113 EDMBPT (CellData Megabyte per Timestep (RW))
896.487 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58631 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.0366 RT (REAL_TIME)
265839977 CP (Cells processed)
0.090366 ASPT (Averaged Seconds per Timestep)
2.6584e+06 CPST (Cells Processed in Average per Simulation Timestep)
1281.99 ACPST (Averaged number of clusters per Simulation Timestep)
29.4181 MCPS (Million Cells per Second) (local)
1.83863 MCPSPT (Million Clusters per Second per Thread)
81.1279 EDMBPT (CellData Megabyte per Timestep (RW))
897.771 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58858 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03657 RT (REAL_TIME)
266020823 CP (Cells processed)
0.0903657 ASPT (Averaged Seconds per Timestep)
2.66021e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.29 ACPST (Averaged number of clusters per Simulation Timestep)
29.4383 MCPS (Million Cells per Second) (local)
1.83989 MCPSPT (Million Clusters per Second per Thread)
81.1831 EDMBPT (CellData Megabyte per Timestep (RW))
898.384 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58967 GFLOPS


++++++++++ MPI RANK 4 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03654 RT (REAL_TIME)
264364032 CP (Cells processed)
0.0903654 ASPT (Averaged Seconds per Timestep)
2.64364e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.84 ACPST (Averaged number of clusters per Simulation Timestep)
29.255 MCPS (Million Cells per Second) (local)
1.82844 MCPSPT (Million Clusters per Second per Thread)
80.6775 EDMBPT (CellData Megabyte per Timestep (RW))
892.792 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57977 GFLOPS


++++++++++ MPI RANK 5 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03657 RT (REAL_TIME)
264359936 CP (Cells processed)
0.0903657 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
29.2545 MCPS (Million Cells per Second) (local)
1.8284 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
892.775 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57974 GFLOPS


++++++++++ MPI RANK 6 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03658 RT (REAL_TIME)
264402944 CP (Cells processed)
0.0903658 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
29.2592 MCPS (Million Cells per Second) (local)
1.8287 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
892.919 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58 GFLOPS


++++++++++ MPI RANK 7 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03653 RT (REAL_TIME)
264321024 CP (Cells processed)
0.0903653 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
29.2503 MCPS (Million Cells per Second) (local)
1.82814 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
892.648 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57952 GFLOPS


++++++++++ MPI RANK 8 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03655 RT (REAL_TIME)
264402944 CP (Cells processed)
0.0903655 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
29.2593 MCPS (Million Cells per Second) (local)
1.82871 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
892.923 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58 GFLOPS


++++++++++ MPI RANK 9 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03652 RT (REAL_TIME)
264804969 CP (Cells processed)
0.0903652 ASPT (Averaged Seconds per Timestep)
2.64805e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.1 ACPST (Averaged number of clusters per Simulation Timestep)
29.3039 MCPS (Million Cells per Second) (local)
1.83149 MCPSPT (Million Clusters per Second per Thread)
80.8121 EDMBPT (CellData Megabyte per Timestep (RW))
894.283 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58241 GFLOPS


++++++++++ MPI RANK 10 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03657 RT (REAL_TIME)
266659796 CP (Cells processed)
0.0903657 ASPT (Averaged Seconds per Timestep)
2.6666e+06 CPST (Cells Processed in Average per Simulation Timestep)
1279.49 ACPST (Averaged number of clusters per Simulation Timestep)
29.5089 MCPS (Million Cells per Second) (local)
1.84431 MCPSPT (Million Clusters per Second per Thread)
81.3781 EDMBPT (CellData Megabyte per Timestep (RW))
900.542 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.59348 GFLOPS


++++++++++ MPI RANK 11 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03655 RT (REAL_TIME)
264381270 CP (Cells processed)
0.0903655 ASPT (Averaged Seconds per Timestep)
2.64381e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.05 ACPST (Averaged number of clusters per Simulation Timestep)
29.2569 MCPS (Million Cells per Second) (local)
1.82856 MCPSPT (Million Clusters per Second per Thread)
80.6828 EDMBPT (CellData Megabyte per Timestep (RW))
892.849 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57987 GFLOPS


++++++++++ MPI RANK 12 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03653 RT (REAL_TIME)
264321024 CP (Cells processed)
0.0903653 ASPT (Averaged Seconds per Timestep)
2.64321e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.63 ACPST (Averaged number of clusters per Simulation Timestep)
29.2503 MCPS (Million Cells per Second) (local)
1.82814 MCPSPT (Million Clusters per Second per Thread)
80.6644 EDMBPT (CellData Megabyte per Timestep (RW))
892.647 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57951 GFLOPS


++++++++++ MPI RANK 13 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03652 RT (REAL_TIME)
264402944 CP (Cells processed)
0.0903652 ASPT (Averaged Seconds per Timestep)
2.64403e+06 CPST (Cells Processed in Average per Simulation Timestep)
1291.03 ACPST (Averaged number of clusters per Simulation Timestep)
29.2594 MCPS (Million Cells per Second) (local)
1.82871 MCPSPT (Million Clusters per Second per Thread)
80.6894 EDMBPT (CellData Megabyte per Timestep (RW))
892.925 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.58001 GFLOPS


++++++++++ MPI RANK 14 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03656 RT (REAL_TIME)
266088272 CP (Cells processed)
0.0903656 ASPT (Averaged Seconds per Timestep)
2.66088e+06 CPST (Cells Processed in Average per Simulation Timestep)
1280.76 ACPST (Averaged number of clusters per Simulation Timestep)
29.4458 MCPS (Million Cells per Second) (local)
1.84036 MCPSPT (Million Clusters per Second per Thread)
81.2037 EDMBPT (CellData Megabyte per Timestep (RW))
898.613 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.59007 GFLOPS


++++++++++ MPI RANK 15 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
9.03652 RT (REAL_TIME)
264359936 CP (Cells processed)
0.0903652 ASPT (Averaged Seconds per Timestep)
2.6436e+06 CPST (Cells Processed in Average per Simulation Timestep)
1290.82 ACPST (Averaged number of clusters per Simulation Timestep)
29.2546 MCPS (Million Cells per Second) (local)
1.82841 MCPSPT (Million Clusters per Second per Thread)
80.6762 EDMBPT (CellData Megabyte per Timestep (RW))
892.78 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
1.57975 GFLOPS


++++++++++ SUMMARY ++++++++++
469.045 GMCPS (Global Million Cells per Second) (global)
29.3153 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
