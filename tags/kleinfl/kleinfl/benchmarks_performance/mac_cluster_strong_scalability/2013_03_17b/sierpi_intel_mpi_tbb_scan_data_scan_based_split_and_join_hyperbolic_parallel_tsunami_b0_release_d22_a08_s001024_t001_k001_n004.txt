Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.497 RT (REAL_TIME)
1059639943 CP (Cells processed)
2.39497 ASPT (Averaged Seconds per Timestep)
1.05964e+07 CPST (Cells Processed in Average per Simulation Timestep)
37.56 ACPST (Averaged number of clusters per Simulation Timestep)
4.42443 MCPS (Million Cells per Second) (local)
4.42443 MCPSPT (Million Clusters per Second per Thread)
323.376 EDMBPT (CellData Megabyte per Timestep (RW))
135.023 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238919 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.497 RT (REAL_TIME)
1059631520 CP (Cells processed)
2.39497 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
40.47 ACPST (Averaged number of clusters per Simulation Timestep)
4.4244 MCPS (Million Cells per Second) (local)
4.4244 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
135.022 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238918 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.497 RT (REAL_TIME)
1059645683 CP (Cells processed)
2.39497 ASPT (Averaged Seconds per Timestep)
1.05965e+07 CPST (Cells Processed in Average per Simulation Timestep)
25.23 ACPST (Averaged number of clusters per Simulation Timestep)
4.42446 MCPS (Million Cells per Second) (local)
4.42446 MCPSPT (Million Clusters per Second per Thread)
323.378 EDMBPT (CellData Megabyte per Timestep (RW))
135.024 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238921 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
239.497 RT (REAL_TIME)
1059630672 CP (Cells processed)
2.39497 ASPT (Averaged Seconds per Timestep)
1.05963e+07 CPST (Cells Processed in Average per Simulation Timestep)
47.05 ACPST (Averaged number of clusters per Simulation Timestep)
4.4244 MCPS (Million Cells per Second) (local)
4.4244 MCPSPT (Million Clusters per Second per Thread)
323.374 EDMBPT (CellData Megabyte per Timestep (RW))
135.022 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.238917 GFLOPS


++++++++++ SUMMARY ++++++++++
17.6977 GMCPS (Global Million Cells per Second) (global)
17.6977 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
