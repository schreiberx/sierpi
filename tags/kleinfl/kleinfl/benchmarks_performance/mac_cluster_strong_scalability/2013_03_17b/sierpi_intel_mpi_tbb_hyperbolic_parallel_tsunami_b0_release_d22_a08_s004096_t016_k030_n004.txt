Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK Warning from RANK 2: more nodes than initial clusters available!
3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.1525 RT (REAL_TIME)
1061678725 CP (Cells processed)
0.221525 ASPT (Averaged Seconds per Timestep)
1.06168e+07 CPST (Cells Processed in Average per Simulation Timestep)
5141.23 ACPST (Averaged number of clusters per Simulation Timestep)
47.926 MCPS (Million Cells per Second) (local)
2.99537 MCPSPT (Million Clusters per Second per Thread)
323.999 EDMBPT (CellData Megabyte per Timestep (RW))
1462.59 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.588 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.1525 RT (REAL_TIME)
1057447936 CP (Cells processed)
0.221525 ASPT (Averaged Seconds per Timestep)
1.05745e+07 CPST (Cells Processed in Average per Simulation Timestep)
5163.32 ACPST (Averaged number of clusters per Simulation Timestep)
47.735 MCPS (Million Cells per Second) (local)
2.98344 MCPSPT (Million Clusters per Second per Thread)
322.707 EDMBPT (CellData Megabyte per Timestep (RW))
1456.76 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.57769 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.1525 RT (REAL_TIME)
1060248979 CP (Cells processed)
0.221525 ASPT (Averaged Seconds per Timestep)
1.06025e+07 CPST (Cells Processed in Average per Simulation Timestep)
5150.67 ACPST (Averaged number of clusters per Simulation Timestep)
47.8614 MCPS (Million Cells per Second) (local)
2.99134 MCPSPT (Million Clusters per Second per Thread)
323.562 EDMBPT (CellData Megabyte per Timestep (RW))
1460.62 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.58452 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
22.1525 RT (REAL_TIME)
1059172176 CP (Cells processed)
0.221525 ASPT (Averaged Seconds per Timestep)
1.05917e+07 CPST (Cells Processed in Average per Simulation Timestep)
5153.24 ACPST (Averaged number of clusters per Simulation Timestep)
47.8128 MCPS (Million Cells per Second) (local)
2.9883 MCPSPT (Million Clusters per Second per Thread)
323.234 EDMBPT (CellData Megabyte per Timestep (RW))
1459.13 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
2.58189 GFLOPS


++++++++++ SUMMARY ++++++++++
191.335 GMCPS (Global Million Cells per Second) (global)
11.9585 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
