Unsupported glibc/kernel combination. 
Please contact LRZ HPC support to fix the module.
Warning from RANK 2: more nodes than initial clusters available!
Warning from RANK 3: more nodes than initial clusters available!

++++++++++ MPI RANK 0 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.258 RT (REAL_TIME)
1066792581 CP (Cells processed)
2.01258 ASPT (Averaged Seconds per Timestep)
1.06679e+07 CPST (Cells Processed in Average per Simulation Timestep)
5121.44 ACPST (Averaged number of clusters per Simulation Timestep)
5.30062 MCPS (Million Cells per Second) (local)
5.30062 MCPSPT (Million Clusters per Second per Thread)
325.559 EDMBPT (CellData Megabyte per Timestep (RW))
161.762 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.286234 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.258 RT (REAL_TIME)
1052024832 CP (Cells processed)
2.01258 ASPT (Averaged Seconds per Timestep)
1.05202e+07 CPST (Cells Processed in Average per Simulation Timestep)
5136.84 ACPST (Averaged number of clusters per Simulation Timestep)
5.22725 MCPS (Million Cells per Second) (local)
5.22725 MCPSPT (Million Clusters per Second per Thread)
321.053 EDMBPT (CellData Megabyte per Timestep (RW))
159.523 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.282271 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.258 RT (REAL_TIME)
1061729683 CP (Cells processed)
2.01258 ASPT (Averaged Seconds per Timestep)
1.06173e+07 CPST (Cells Processed in Average per Simulation Timestep)
5135.14 ACPST (Averaged number of clusters per Simulation Timestep)
5.27547 MCPS (Million Cells per Second) (local)
5.27547 MCPSPT (Million Clusters per Second per Thread)
324.014 EDMBPT (CellData Megabyte per Timestep (RW))
160.994 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.284875 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
100 TS (Timesteps)
0.420146 ST (SIMULATION_TIME)
0.00420138 TSS (Timestep size)
201.258 RT (REAL_TIME)
1058000720 CP (Cells processed)
2.01258 ASPT (Averaged Seconds per Timestep)
1.058e+07 CPST (Cells Processed in Average per Simulation Timestep)
5128.59 ACPST (Averaged number of clusters per Simulation Timestep)
5.25694 MCPS (Million Cells per Second) (local)
5.25694 MCPSPT (Million Clusters per Second per Thread)
322.876 EDMBPT (CellData Megabyte per Timestep (RW))
160.429 EDMBPS (CellData Megabyte per Second (RW))
54 Flops per cell update (Matrix Multiplication)
0.283875 GFLOPS


++++++++++ SUMMARY ++++++++++
21.0603 GMCPS (Global Million Cells per Second) (global)
21.0603 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
