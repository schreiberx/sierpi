#! /bin/bash

cd ../../
make clean

scons --xml-config=./scenarios/itbb_async_mpi.xml --threading-itbb-async-mpi-invade-threshold=0.1 $@ -j 10

scons --xml-config=./scenarios/itbb_async_mpi.xml --threading=tbb $@ -j 10
