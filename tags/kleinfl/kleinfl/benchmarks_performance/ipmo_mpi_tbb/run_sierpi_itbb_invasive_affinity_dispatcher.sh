#! /bin/bash

. params.sh

test -z "$IPMO_HOME" && IPMO_HOME="../../../ipmo/"

killall server_ipmo_release -9 >/dev/null
$IPMO_HOME/build/server_ipmo_release -n $NCORES -v -99 -c &

time mpirun -np $MPINODES ./sierpi_itbb_invasive_affinity_dispatcher.sh $@

killall server_ipmo_release -9 >/dev/null
