Running benchmarks with default parameters:  -G 1 -d 8 -a 8 -A 1

application	averaged_runtime	0	1	2	3	4	5	6	7	8	9
sierpi_intel_ipmo_async_tsunami_parallel_b0_release	1.591729	1.60261	1.59381	1.58996	1.5919	1.59271	1.59863	1.58687	1.58569	1.589	1.58611
sierpi_intel_ipmo_tsunami_parallel_b0_release	1.625446	1.62421	1.62933	1.62549	1.62511	1.6261	1.62426	1.62611	1.62387	1.62452	1.62546
sierpi_intel_itbb_async_tsunami_parallel_b0_release	1.332841	1.32936	1.3306	1.3314	1.33257	1.33379	1.33114	1.33215	1.3308	1.33257	1.34403
sierpi_intel_itbb_tsunami_parallel_b0_release	1.375292	1.37855	1.37599	1.37684	1.37481	1.37812	1.39115	1.37924	1.36607	1.36692	1.36523
sierpi_intel_omp_tsunami_parallel_b0_release	1.450382	1.44981	1.44708	1.44966	1.44547	1.45157	1.44636	1.45463	1.4446	1.45952	1.45512
sierpi_intel_tbb_tsunami_parallel_b0_release	1.195817	1.19124	1.19222	1.20103	1.19098	1.20086	1.19083	1.19362	1.20317	1.19137	1.20285
