NetCDF not compiled in!!!
NetCDF not compiled in!!!
NetCDF not compiled in!!!
NetCDF not compiled in!!!
0.5
0.5
0.5
0.5

++++++++++ MPI RANK 0 ++++++++++
10 TS (Timesteps)
0.220155 ST (SIMULATION_TIME)
0.0218639 TSS (Timestep size)
7.27815 RT (REAL_TIME)
20971520 CP (Cells processed)
0.727815 ASPT (Averaged Seconds per Timestep)
2.09715e+06 CPST (Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
2.88143 MCPS (Million Cells per Second) (local)
2.88143 MCPSPT (Million Clusters per Second per Thread)
208 EDMBPT (CellData Megabyte per Timestep (RW))
285.787 EDMBPS (CellData Megabyte per Second (RW))
378 Flops per cell update (Matrix Multiplication)
1.08918 GFLOPS

++++++++++ MPI RANK 1 ++++++++++
SEND TO RANK 0
length: 578
SEND TO RANK 0
length: 578
SEND TO RANK 0
length: 578
10 TS (Timesteps)
0.220155 ST (SIMULATION_TIME)
0.0218639 TSS (Timestep size)
7.27815 RT (REAL_TIME)
20971520 CP (Cells processed)
0.727815 ASPT (Averaged Seconds per Timestep)
2.09715e+06 CPST (Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
2.88143 MCPS (Million Cells per Second) (local)
2.88143 MCPSPT (Million Clusters per Second per Thread)
208 EDMBPT (CellData Megabyte per Timestep (RW))
285.787 EDMBPS (CellData Megabyte per Second (RW))
378 Flops per cell update (Matrix Multiplication)
1.08918 GFLOPS


++++++++++ MPI RANK 2 ++++++++++
10 TS (Timesteps)
0.220155 ST (SIMULATION_TIME)
0.0218639 TSS (Timestep size)
7.27815 RT (REAL_TIME)
20971520 CP (Cells processed)
0.727815 ASPT (Averaged Seconds per Timestep)
2.09715e+06 CPST (Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
2.88143 MCPS (Million Cells per Second) (local)
2.88143 MCPSPT (Million Clusters per Second per Thread)
208 EDMBPT (CellData Megabyte per Timestep (RW))
285.787 EDMBPS (CellData Megabyte per Second (RW))
378 Flops per cell update (Matrix Multiplication)
1.08918 GFLOPS


++++++++++ MPI RANK 3 ++++++++++
10 TS (Timesteps)
0.220155 ST (SIMULATION_TIME)
0.0218639 TSS (Timestep size)
7.27815 RT (REAL_TIME)
20971520 CP (Cells processed)
0.727815 ASPT (Averaged Seconds per Timestep)
2.09715e+06 CPST (Cells Processed in Average per Simulation Timestep)
2 ACPST (Averaged number of clusters per Simulation Timestep)
2.88143 MCPS (Million Cells per Second) (local)
2.88143 MCPSPT (Million Clusters per Second per Thread)
208 EDMBPT (CellData Megabyte per Timestep (RW))
285.787 EDMBPS (CellData Megabyte per Second (RW))
378 Flops per cell update (Matrix Multiplication)
1.08918 GFLOPS


++++++++++ SUMMARY ++++++++++
11.5257 GMCPS (Global Million Cells per Second) (global)
11.5257 MCPSPT (Million Clusters per Second per Thread) (global)
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
EXIT STATUS: OK
