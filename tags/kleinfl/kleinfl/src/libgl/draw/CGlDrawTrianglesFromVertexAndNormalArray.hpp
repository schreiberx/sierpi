/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGL_DRAW_TRIANGLES_FROM_VERTEX_AND_NORMAL_ARRAY_HPP_
#define CGL_DRAW_TRIANGLES_FROM_VERTEX_AND_NORMAL_ARRAY_HPP_

#include "libgl/incgl3.h"


template <typename T>
class CGlDrawTrianglesWithVertexAndNormalArray
{
public:
	CGlVertexArrayObject vao;
	CGlBuffer vertex_buffer;
	CGlBuffer normal_buffer;

	const size_t max_number_of_vertices;

public:
	CError error;

	CGlDrawTrianglesWithVertexAndNormalArray(
		size_t i_max_number_of_vertices = 3*1024*16*16
	)	:
		max_number_of_vertices(i_max_number_of_vertices)
	{
		GLint glfloat = (sizeof(T) == 4 ? GL_FLOAT : GL_DOUBLE);

		vao.bind();
			vertex_buffer.bind();
			vertex_buffer.resize(max_number_of_vertices*sizeof(T)*3);

				// vertex coordinates
				glVertexAttribPointer(0, 3, glfloat, GL_FALSE, 3*sizeof(T), 0);
				glEnableVertexAttribArray(0);

			normal_buffer.bind();
			normal_buffer.resize(max_number_of_vertices*sizeof(T)*3);

				// normals
				glVertexAttribPointer(1, 3, glfloat, GL_FALSE, 3*sizeof(T), 0);
				glEnableVertexAttribArray(1);

		vao.unbind();
	}




	/**
	 * initialize rendering
	 *
	 * bind & setup the framebuffer
	 *
	 * activate and initialize the shader program
	 */
	inline void initRendering()
	{
		/**
		 * !!! We have to do the setup right here since other threads are not allowed to access the context !!!
		 */
		vao.bind();

		CGlErrorCheck();
	}


	inline void render(
			const T *i_vertex_buffer_data,		///< pointer to vertices
			const T *i_normal_buffer_data,		///< pointer to normals
			size_t i_vertices_count			///< number of vertices
	)
	{
		for (size_t i = 0; i < i_vertices_count; i += max_number_of_vertices)
		{
			size_t block_size = std::min<size_t>(max_number_of_vertices, i_vertices_count - i);

//			glDisable(GL_CULL_FACE);
			vertex_buffer.bind();
			vertex_buffer.subData(0, block_size*(3*sizeof(T)), i_vertex_buffer_data);

			normal_buffer.bind();
			normal_buffer.subData(0, block_size*(3*sizeof(T)), i_normal_buffer_data);

			glDrawArrays(GL_TRIANGLES, 0, block_size);

			i_vertex_buffer_data += 3*block_size;
			i_normal_buffer_data += 3*block_size;
		}
	}



	inline void shutdownRendering()
	{
		vao.unbind();
		CGlErrorCheck();
	}

};


#endif
