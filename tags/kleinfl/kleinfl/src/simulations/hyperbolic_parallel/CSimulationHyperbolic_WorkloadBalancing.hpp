/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CSimulationHyperbolic_WorkloadBalancing.hpp
 *
 *  Created on: Mar 17, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONHYPERBOLIC_WORKLOADBALANCING_HPP_
#define CSIMULATIONHYPERBOLIC_WORKLOADBALANCING_HPP_



#include <global_config.h>
#include <libsierpi/generic_tree/CGeneric_TreeNode.hpp>

#include "libsierpi/parallelization/CSplitJoinTuning.hpp"


/**
 * class for MPI based workload balancing
 */
class CSimulationHyperbolic_WorkloadBalancing
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	/**
	 * reference to simulation parameters
	 */
	CParameters &cParameters;

	/**
	 * reference to clusters to start traversals
	 */
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	/**
	 * datasets for cluster migration
	 */
	CDatasets &cDatasets;
#endif

	/**
	 * stopwatch for timings
	 */
	CStopwatch cStopwatch;

	/**
	 * tuning for the split and join parameters
	 */
//	sierpi::CSplitJoinTuning<CSplitJoin_TuningTable> cSplitJoinTuning;

	/**
	 * number of triangles stored during scan in sub-cluster tree valid?
	 */
#if DEBUG && CONFIG_ENABLE_SCAN_DATA
public:
	bool cluster_workload_scans_valid;
#endif


public:
	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationHyperbolic_WorkloadBalancing(
			CParameters &i_cParameters,
			sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &i_cDomainClusters
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
			,
			CDatasets &i_cDatasets
#endif
	)	:
			cParameters(i_cParameters),
			cDomainClusters(i_cDomainClusters)
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
			,
			cDatasets(i_cDatasets)
#endif

#if DEBUG && CONFIG_ENABLE_SCAN_DATA
			,
			cluster_workload_scans_valid(false)
#endif
	{
	}



	/**
	 * Deconstructor
	 */
public:
	virtual ~CSimulationHyperbolic_WorkloadBalancing()
	{
	}



	/***************************************************************************************
	 * SCAN DATA
	 ***************************************************************************************/

#if CONFIG_ENABLE_SCAN_DATA

	/**
	 * return average number of cells per thread when equally distributed
	 */
	private:
	inline long long p_compute_average_local_cells_per_thread(
			int i_number_of_threads = -1
	) {

		int next_simulation_threading_number_of_threads = (i_number_of_threads == -1 ? cParameters.simulation_threading_number_of_threads : i_number_of_threads);

		long long triangles_per_thread = cParameters.number_of_local_cells / next_simulation_threading_number_of_threads;

		if (triangles_per_thread*next_simulation_threading_number_of_threads != cParameters.number_of_local_cells)
			triangles_per_thread++;

		return triangles_per_thread;
	}



public:
	/**
	 * Setup the scan information by running one adaptivity
	 * traversal and a split & join traversal.
	 *
	 * The number of threads for the next scan based traversal can be
	 * specified for invasive computing.
	 */
	void updateClusterScanInformation(
			int i_new_number_of_threads = -1	///< new number of threads for which the scan datasets have to be updated
	) {
#if DEBUG
		if (cParameters.verbosity_level > 9)
		{
			std::cout << "updateClusterScanInformation" << std::endl;
		}
#endif

		long long cells_per_thread = p_compute_average_local_cells_per_thread(i_new_number_of_threads);

		/*
		 * update scan data
		 */
		cDomainClusters.traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					// LEAF NODES

					/*
					 * update workload scan data and thread assignments
					 */
					assert(cGenericTreeNode->workload_in_subtree >= 0);

					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					// MIDNODES PREORDER
					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					// MIDNODES POSTORDER
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
			);

#if DEBUG
		cluster_workload_scans_valid = true;

		if (cParameters.verbosity_level > 9)
		{
			std::cout << "updateClusterScanInformation FIN" << std::endl;
		}
#endif
	}



public:
	/**
	 * create initial scan datasets
	 */
	void create_initial_scan_data(
			int i_initial_workload_per_cluster
	)
	{
		cParameters.number_of_local_cells = 0;

		cDomainClusters.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
			[=](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					long long *o_number_of_local_triangles
			)
			{
				i_cGenericTreeNode->workload_in_subtree = i_initial_workload_per_cluster;

				*o_number_of_local_triangles = i_initial_workload_per_cluster;
			},
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode, long long *i_number_of_local_triangles)
			{
				i_cGenericTreeNode->workload_in_subtree = *i_number_of_local_triangles;
			}
			,
			&sierpi::CReduceOperators::ADD,
			&cParameters.number_of_local_cells
		);

	#if DEBUG
		sierpi::CGlobalComm::barrier();
	#endif

		/*
		 * update number of global cells
		 */
		cParameters.number_of_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);

		updateClusterScanInformation();

	}


#endif


	/*****************************************************************************************************************
	 * SPLIT / JOIN
	 *****************************************************************************************************************/
public:
	inline bool cluster_split_and_join_and_migration(
			int i_new_number_of_threads = -1	///< if != -1, use this value for new number of threads
	)
	{
		bool some_cluster_changed = false;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		// update cluster migration markers before split/join since those updates are also communicated
		p_update_cluster_migration_dst_markers();
#endif


#if DEBUG && CONFIG_ENABLE_MPI
		validation_message_id_dm();
		validation_mpi_edge_comm_ranks();
		validation_message_id_dm();
#endif

		p_simulation_cluster_split_and_join(i_new_number_of_threads);

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		// cluster migration
		some_cluster_changed |= p_simulation_cluster_do_migration();
#endif

#if DEBUG && CONFIG_ENABLE_MPI
		validation_message_id_dm();
		validation_mpi_edge_comm_ranks();
		validation_message_id_dm();
#endif

		return some_cluster_changed;
	}



public:
	inline bool cluster_split_and_join_and_migration_verbose(
			double *io_splitJoinTime	///< add time taken for split/joins to this value
#if CONFIG_ENABLE_MPI
			,
			double *io_clusterMigration_dm	///< add time taken for cluster migration to this value
#endif
	)
	{
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		// update cluster migration markers before split/join since those updates are also communicated
		p_update_cluster_migration_dst_markers();
#endif

		bool something_changed = false;

#if DEBUG && CONFIG_ENABLE_MPI

		if (cParameters.verbosity_level >= 9)
			std::cout << "validation_message_id_dm" << std::endl;
		validation_message_id_dm();

		if (cParameters.verbosity_level >= 9)
			std::cout << "validation_mpi_edge_comm_ranks" << std::endl;
		validation_mpi_edge_comm_ranks();

		if (cParameters.verbosity_level >= 9)
			std::cout << "validation_message_id_dm" << std::endl;
		validation_message_id_dm();
#endif

		if (cParameters.verbosity_level >= 9)
			std::cout << "split join pass" << std::endl;

		cStopwatch.start();
		something_changed |= p_simulation_cluster_split_and_join();
		*io_splitJoinTime += cStopwatch.getTimeSinceStart();

		if (cParameters.verbosity_level >= 9)
			std::cout << "split join pass FIN" << std::endl;


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		// cluster migration

		if (cParameters.verbosity_level >= 9)
			std::cout << "cluster migration pass" << std::endl;

		cStopwatch.start();
		something_changed |= p_simulation_cluster_do_migration();
		*io_clusterMigration_dm += cStopwatch.getTimeSinceStart();

		if (cParameters.verbosity_level >= 9)
			std::cout << "cluster migration pass FIN" << std::endl;

#endif



#if DEBUG && CONFIG_ENABLE_MPI
		if (cParameters.verbosity_level >= 9)
			std::cout << "validation_message_id_dm" << std::endl;
		validation_message_id_dm();

		if (cParameters.verbosity_level >= 9)
			std::cout << "validation_mpi_edge_comm_ranks" << std::endl;
		validation_mpi_edge_comm_ranks();
#endif
		return something_changed;
	}



private:
	/**
	 * split or join sub-clusters
	 *
	 * this method may only be invoked after at least one adaptivity traversal to create the scan information
	 *
	 * \return number of sub-clusters
	 */
	inline bool p_simulation_cluster_split_and_join(
			int i_new_number_of_threads = -1	///< if != -1, use this value for new number of threads
	) {
		assert(cParameters.simulation_threading_number_of_threads > 0);

		if (i_new_number_of_threads != -1)
			cParameters.simulation_threading_number_of_threads = i_new_number_of_threads;

		// signed variable necessary!

#if CONFIG_ENABLE_SCAN_DATA
		long long cells_per_thread = p_compute_average_local_cells_per_thread();
#endif

		// avoid traversals if mpi is actiavted but with split and join deactivated
#if CONFIG_ENABLE_MPI && !CONFIG_ENABLE_MPI_SPLIT_AND_JOIN && !CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

#	if CONFIG_ENABLE_SCAN_DATA
		// just update scan data
		cDomainClusters.traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// LEAF NODES

					/*
					 * update workload scan data and thread assignments
					 */
					assert(cGenericTreeNode->workload_in_subtree >= 0);

					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// MIDNODES PREORDER

					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// MIDNODES POSTORDER

					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
				}
			);
#	endif

#	if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = true;
#	endif

		cParameters.number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_clusters);

		return false;
#endif




		long long old_number_of_local_clusters = cParameters.number_of_local_clusters;


		/***************************************************************************************
		 * ACTION: run the split or join operations and update the edge communication information
		 ***************************************************************************************/
#if !CONFIG_ENABLE_MPI || CONFIG_ENABLE_MPI_SPLIT_AND_JOIN

		// split/join operations

		cDomainClusters.traverse_GenericTreeNode_LeafAndPreAndPostorderMidNodes_Parallel(
			[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// LEAF NODES

#	if CONFIG_ENABLE_SCAN_DATA
					/*
					 * update workload scan data and thread assignments
					 */
					assert(cGenericTreeNode->workload_in_subtree >= 0);
					cGenericTreeNode->updateWorkloadScanTopDown();
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
#	endif

					cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;


#	if (!CONFIG_ENABLE_SCAN_DATA) || (!CONFIG_ENABLE_SCAN_BASED_SPLIT_AND_JOIN && CONFIG_ENABLE_SCAN_DATA)

					long long l_number_of_local_cells = cGenericTreeNode->workload_in_subtree;

					if (l_number_of_local_cells > cParameters.cluster_split_workload_size)
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
					else if (l_number_of_local_cells < cParameters.cluster_join_workload_size)
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::JOIN;

					if (!cGenericTreeNode->cCluster_TreeNode->fun_splitAtLeaves())
						return;

#	else

					assert(cGenericTreeNode->workload_scan_start_index >= 0);
					assert(cGenericTreeNode->workload_scan_end_index >= 1);
					assert(cGenericTreeNode->workload_thread_id_end >= cGenericTreeNode->workload_thread_id_start);
					assert(cGenericTreeNode->workload_thread_id_start < cParameters.simulation_threading_number_of_threads);
					assert(cGenericTreeNode->workload_thread_id_end < cParameters.simulation_threading_number_of_threads);

					long long l_number_of_local_cells = cGenericTreeNode->workload_in_subtree;

					// distance to start and end of scan cluster distribution
					long long dist_start = cGenericTreeNode->workload_scan_start_index % cells_per_thread;
					long long dist_end = cells_per_thread - (((cGenericTreeNode->workload_scan_end_index-1) % cells_per_thread) + 1);

					// half workload in cluster
					long long half_workload = cGenericTreeNode->workload_in_subtree >> 1;
					long long quater_workload = half_workload >> 1;


					/*
					 * check whether the current cluster should be split due
					 * to proximity to load-balancing boundary
					 */

					/*
					 * SPLITS
					 */
					{
						if (cGenericTreeNode->workload_thread_id_start != cGenericTreeNode->workload_thread_id_end)
						{
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests =
									sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}

						/*
						 * split towards load-balancing boundaries
						 */
						if (
							quater_workload > dist_start ||
							quater_workload > dist_end
						)
						{
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests =
									sierpi::CCluster_SplitJoinInformation_Enums::SPLIT;
							goto done;
						}
					}

					/*
					 * JOIN
					 */
					if (
							dist_start > cGenericTreeNode->workload_in_subtree &&
							dist_end > cGenericTreeNode->workload_in_subtree
					)
					{
						// distance to start and end of scan cluster distribution
						cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::JOIN;
					}


				done:
					if (cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests == sierpi::CCluster_SplitJoinInformation_Enums::SPLIT)
					{
						if (l_number_of_local_cells <= cParameters.cluster_join_workload_size)
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;
					}
					else if (cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests == sierpi::CCluster_SplitJoinInformation_Enums::JOIN)
					{
						if (l_number_of_local_cells >= cParameters.cluster_split_workload_size)
							cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;
					}

					if (!cGenericTreeNode->cCluster_TreeNode->fun_splitAtLeaves())
						return;

#endif

					/*
					 * update scan data when node was not split
					 */
#		if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->first_child_node->updateWorkloadThreadAssignment(cells_per_thread);
					cGenericTreeNode->second_child_node->updateWorkloadThreadAssignment(cells_per_thread);
#		endif
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// MIDNODES PREORDER
#		if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadScanTopDown();

					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
#		endif
				}
				,
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
				// MIDNODES POSTORDER
					CCluster_TreeNode_::fun_testAndJoinAtMidNodes(cGenericTreeNode);

#		if CONFIG_ENABLE_SCAN_DATA
					cGenericTreeNode->updateWorkloadThreadAssignment(cells_per_thread);
#		endif
				}
			);


#		if DEBUG && CONFIG_ENABLE_SCAN_DATA
		cluster_workload_scans_valid = true;
#		endif


		cParameters.number_of_local_clusters = 0;

		/**
		 * update adjacent cluster information
		 */
		cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
					[&](CGeneric_TreeNode_ *node, long long *i_reduceValue)
					{
						node->cCluster_TreeNode->cCluster_SplitJoinActions.pass2_updateAdjacentClusterInformation();
						*i_reduceValue = 1;	// one cluster
					},
					&sierpi::CReduceOperators::ADD<long long>,
					&cParameters.number_of_local_clusters
				);



		/**
		 * cleanup nodes
		 */
		auto fun2 = [&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					sierpi::CCluster_SplitJoinActions<CSimulationHyperbolic_Cluster>::pass3_swapAndCleanAfterUpdatingEdgeComm(cGenericTreeNode);
				};

		cDomainClusters.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
				fun2,
				fun2
			);

#endif



#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
	#if CONFIG_ENABLE_MPI
	#	error	"node traversals not possible with MPI enabled"
	#endif


	/**
	 * setup new vertex data if appropriate
	 */
	cDomainClusters.traverse_GenericTreeNode_Parallel(
		[&](CGeneric_TreeNode_ *cGenericTreeNode)	{
			cGenericTreeNode->cCluster_TreeNode->cCluster_SplitJoinActions.pass4_SM_updateVertexCommData();
		}
	);


	/**
	 * cleanup nodes
	 */
	cDomainClusters.traverse_GenericTreeNode_Parallel(
		[&](CGeneric_TreeNode_ *cGenericTreeNode)
		{
			sierpi::CCluster_SplitJoinActions<CSimulationHyperbolic_Cluster>::pass4_SM_cleanup(cGenericTreeNode);
		}
	);

#endif


	/**
	 * Run split and join for distributed memory split and joins
	 */
#if CONFIG_ENABLE_MPI

		/**
		 * send splitting information to adjacent MPI nodes
		 *
		 * this has to be done serial to assure correct order on receiver site.
		 */
		cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGeneric_TreeNode_ *node)
				{
					node->cCluster_TreeNode->cCluster_SplitJoinActions.pass4_DM_sendAdjacentClusterSplitJoinInformation();
				}
			);



		/**
		 * receive splitting information about adjacent MPI nodes
		 *
		 * this also has to be executed serially but in reversed order.
		 */
		sierpi::CCluster_SplitJoinActions<CSimulationHyperbolic_Cluster>::SSplitJoinDMInformation edge_comm_elements_clockwise_edge;
		sierpi::CCluster_SplitJoinActions<CSimulationHyperbolic_Cluster>::SSplitJoinDMInformation edge_comm_elements_counterclockwise_edge;

		edge_comm_elements_clockwise_edge.edge_comm_elements = 0;
		edge_comm_elements_counterclockwise_edge.edge_comm_elements = 0;

		cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[&](CGeneric_TreeNode_ *node)
			{

				node->cCluster_TreeNode->cCluster_SplitJoinActions.pass4_DM_recvAdjacentClusterSplitJoinInformation(
						&edge_comm_elements_clockwise_edge,
						&edge_comm_elements_counterclockwise_edge
					);
			}
		);
#endif

#if !CONFIG_ENABLE_MPI_CLUSTER_MIGRATION && CONFIG_ENABLE_MPI
		// only validate if cluster migration is deactivated since this updates the mpi ranks during this step!
		validation_mpi_edge_comm_ranks();

		validation_message_id_dm();
#endif


#if DEBUG && CONFIG_ENABLE_SCAN_DATA

		/*
		 * validate correct workload scans
		 */
		int scan_start_id = 0;
		int thread_start_id = 0;
		cDomainClusters.traverse_GenericTreeNode_Serial(
				[&](CGeneric_TreeNode_ *node)
				{
					assert(node->workload_scan_start_index >= 0);
					assert(node->workload_scan_end_index >= 0);

					assert (node->workload_scan_start_index == scan_start_id);
					scan_start_id = node->workload_scan_end_index;
					thread_start_id = node->workload_thread_id_end;
				}
			);

#endif

		cParameters.number_of_global_clusters = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_clusters);

		return old_number_of_local_clusters != cParameters.number_of_local_clusters;
	}



#if CONFIG_ENABLE_MPI


	/**
	 * validate edge MPI comm
	 */
private:
	void validation_mpi_edge_comm_ranks()
	{
#	if DEBUG
		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *node)
			{
				node->cCluster_TreeNode->cCluster_SplitJoinActions.pass5_DM_sendValidationInformation();
			}
		);

		cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[&](CGeneric_TreeNode_ *node)
			{
				node->cCluster_TreeNode->cCluster_SplitJoinActions.pass5_DM_recvValidationInformation();
			}
		);
#	endif // DEBUG
	}



private:
	void validation_message_id_dm()
	{
#	if DEBUG
		/**
		 * SEND FIN MESSAGES with tag 0 and 1 to assure that all split/join messages have been processed
		 */
		int comm_size =  sierpi::CGlobalComm::getCommSize();

		MPI_Request *rs0 = new MPI_Request[comm_size];
		MPI_Request *rs1 = new MPI_Request[comm_size];

		int *fin_message_send1 = new int[comm_size];
		int *fin_message_send2 = new int[comm_size];

		for (int i = 0; i < comm_size; i++)
		{
			rs0[i] = MPI_REQUEST_NULL;
			rs1[i] = MPI_REQUEST_NULL;

			fin_message_send1[i] = -666;
			fin_message_send2[i] = -666;

			MPI_Isend(
					&fin_message_send1[i],			///< number of communication elements
					sizeof(fin_message_send1[i]),	///< size
					MPI_BYTE,					///< data type
					i,							///< destination rank
					0,							///< tag
					MPI_COMM_WORLD,				///< communicator
					&(rs0[i])
				);

			MPI_Isend(
					&fin_message_send2[i],			///< number of communication elements
					sizeof(fin_message_send2[i]),	///< size
					MPI_BYTE,					///< data type
					i,							///< destination rank
					1,							///< tag
					MPI_COMM_WORLD,				///< communicator
					&(rs1[i])
				);
		}


		/**
		 * RECV fin messages and test for fin messages
		 */
		for (int i = 0; i < comm_size; i++)
		{
			// send fin messages with tag 0 and 1 to assure that all split/join messages have been processed

			int fin_message = 0;

			MPI_Recv(
					&fin_message,				///< number of communication elements
					sizeof(fin_message),		///< size
					MPI_BYTE,					///< data type
					i,							///< src rank
					0,							///< tag
					MPI_COMM_WORLD,				///< communicator
					MPI_STATUS_IGNORE
				);

			if (fin_message != -666)
			{
				std::cerr << "invalid fin 0 message: " << fin_message << " from node " << i << std::endl;
				std::cout << "invalid fin 1 message: " << fin_message << " from node " << i << std::endl;
				exit(-1);
			}

			fin_message = 0;
			MPI_Recv(
					&fin_message,				///< number of communication elements
					sizeof(fin_message),		///< size
					MPI_BYTE,					///< data type
					i,							///< src rank
					1,							///< tag
					MPI_COMM_WORLD,				///< communicator
					MPI_STATUS_IGNORE
				);

			if (fin_message != -666)
			{
				std::cerr << "invalid fin 1 message: " << fin_message << " from node " << i << std::endl;
				std::cout << "invalid fin 1 message: " << fin_message << " from node " << i << std::endl;
				exit(-1);
			}
		}

		for (int i = 0; i < comm_size; i++)
		{
			MPI_Wait(&rs0[i], MPI_STATUS_IGNORE);
			MPI_Wait(&rs1[i], MPI_STATUS_IGNORE);
		}

		delete [] fin_message_send1;
		delete [] fin_message_send2;
		delete [] rs0;
		delete [] rs1;

#	endif // DEBUG
	}

#endif // CONFIG_ENABLE_MPI


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	/**
	 * simulation cluster setup
	 */
private:
	inline CGeneric_TreeNode_ *p_simulation_cluster_migration_setup(
		sierpi::CCluster_UniqueId &cCluster_UniqueId,		// unique id associated to cluster
		int i_mpi_src_rank
	) {
		CGeneric_TreeNode_ *	cGeneric_TreeNode = cDomainClusters.createChildByUniqueId(cCluster_UniqueId);

		sierpi::CTriangle_Factory cTriangleFactory;	// triangle factory associated to cluster

		/*
		 * step 2) receive triangle factory
		 */
#	if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::recvRawClass(cTriangleFactory, i_mpi_src_rank);
#	else
		sierpi::CMigrationClass<sierpi::CTriangle_Factory>::recv(cTriangleFactory, i_mpi_src_rank);
#	endif

		// then allocate the cluster tree node
		cGeneric_TreeNode->cCluster_TreeNode = new CCluster_TreeNode_(cTriangleFactory);
		cGeneric_TreeNode->cCluster_TreeNode->setUniqueId(cCluster_UniqueId);

		// setup pointers
		cGeneric_TreeNode->cCluster_TreeNode->cGeneric_TreeNode = cGeneric_TreeNode;

		/*
		 * step 3) recv global tree stuff
		 */
		cGeneric_TreeNode->migration_recv(i_mpi_src_rank);

		/*
		 * step 4) receive remainig parts of cluster
		 */
		cGeneric_TreeNode->cCluster_TreeNode->migration_recv(i_mpi_src_rank);

		/*
		 * step 5) update local cluster information
		 */
		cParameters.number_of_local_cells += cGeneric_TreeNode->cCluster_TreeNode->cStacks->structure_stacks.structure_getNumberOfTrianglesInTriangle();
		cParameters.number_of_local_clusters++;

		return cGeneric_TreeNode;
	}


#	if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

private:
	void p_update_cluster_migration_dst_markers()
	{
		int src_rank = cParameters.parallelization_mpi_rank;
		int comm_size = cParameters.parallelization_mpi_size;

#		if DEBUG
		long long test_global_cells = sierpi::CGlobalComm::reduceLongLongSum(cParameters.number_of_local_cells);
		if (test_global_cells != cParameters.number_of_global_cells)
		{
			std::cerr << "test_global_cells (" << test_global_cells << ") != number_of_global_cells (" << cParameters.number_of_global_cells << ")" << std::endl;
			assert(false);
			exit(-1);
		}
#		endif



		/******************************
		 * Actual distribution:
		 ******************************
		 *                   _______________________________________________
		 *                  |_____|____________________________|____________|
		 *                         |                            |
		 *      start_index_workload                            end_index_workload
		 *
		 ******************************
		 * Equally distributed:
		 ******************************
		 *                   _______________________________________________
		 *                  |_______________|_______________|_______________|
		 *                                   |               |
		 *       balanced_start_index_workload               balanced_start_index_workload
		 */
		long long local_workload = cParameters.number_of_local_cells;
		long long end_index_workload;
		MPI_Scan(&local_workload, &end_index_workload, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);

		long long start_index_workload = end_index_workload - cParameters.number_of_local_cells;

		long long average_workload_per_node = cParameters.number_of_global_cells / comm_size;

		if (average_workload_per_node*comm_size != cParameters.number_of_global_cells)
			average_workload_per_node++;

		long long balanced_start_index_workload = average_workload_per_node*src_rank;
		long long balanced_end_index_workload = balanced_start_index_workload+average_workload_per_node;

		assert(start_index_workload >= 0);
		if (start_index_workload < balanced_start_index_workload)
		{
			// migrate left-most clusters

			// use signed value!
			long long number_of_cells_to_migrate = balanced_start_index_workload-start_index_workload;

			assert(src_rank > 0);
			int dst_mpi_rank = src_rank-1;

			cDomainClusters.traverse_GenericTreeNode_RestrictedContinuation_Serial(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode) -> bool
				{
					CCluster_TreeNode_ *cCluster_TreeNode = i_cGenericTreeNode->cCluster_TreeNode;

					long long half_workload = (i_cGenericTreeNode->workload_in_subtree>>1);

					if (number_of_cells_to_migrate <= half_workload)
						return false;

					// decrease number of cells to migrate by workload in leaf
					number_of_cells_to_migrate -= i_cGenericTreeNode->workload_in_subtree;

					// request migration
					cCluster_TreeNode->requestMigrationToRank(dst_mpi_rank);

					// return whether to continue tagging clusters
					return (number_of_cells_to_migrate > 0);
				}
			);
		}

		assert(end_index_workload >= 0);
		if (end_index_workload > balanced_end_index_workload)
		{
			// migrate right-most clusters

			// use signed value!
			long long number_of_cells_to_migrate = end_index_workload-balanced_end_index_workload;

			assert(src_rank < comm_size-1);
			int dst_mpi_rank = src_rank+1;

			cDomainClusters.traverse_GenericTreeNode_RestrictedContinuation_Reversed_Serial(
				[&](CGeneric_TreeNode_ *i_cGeneric_TreeNode) -> bool
				{
					CCluster_TreeNode_ *cCluster_TreeNode = i_cGeneric_TreeNode->cCluster_TreeNode;

					long long half_workload = (i_cGeneric_TreeNode->workload_in_subtree>>1);

					if (number_of_cells_to_migrate <= half_workload)
						return false;

					// decrease number of cells to migrate by workload in leaf
					number_of_cells_to_migrate -= i_cGeneric_TreeNode->workload_in_subtree;

					// request migration
					cCluster_TreeNode->requestMigrationToRank(dst_mpi_rank);

					// return whether to continue tagging clusters
					return (number_of_cells_to_migrate > 0);
				}
			);
		}

	}
#	endif




	/**
	 * cluster migration during simulation
	 *
	 * return true in case of a cluster migration
	 */
private:
	bool p_simulation_cluster_do_migration()
	{
		int src_rank = cParameters.parallelization_mpi_rank;

		bool local_modification = false;

#	if DEBUG
		/*
		 * test whether clusters were pushed forward or backward.
		 * in this case, no cluster may be received.
		 */
		bool pushed_forward = false;
		bool pushed_backward = false;
#	endif


		/*
		 * PUSH
		 */
		std::vector<CGeneric_TreeNode_*> pushed_clusters_postprocessing_vector;
		pushed_clusters_postprocessing_vector.reserve(32);

		cDomainClusters.traverse_GenericTreeNode_RestrictedContinuation_Serial(
			[&](CGeneric_TreeNode_ *node) -> bool
			{
				int dst_rank = node->cCluster_TreeNode->migration_dst_rank;

				if (dst_rank < 0)
					return false;

				// no handling of forward push
				if (src_rank+1 == dst_rank)
					return false;

				assert(dst_rank == src_rank-1);
				assert(dst_rank != src_rank);

#	if DEBUG
				pushed_backward = true;

				if (cParameters.verbosity_level > 9)
					std::cout << "p_simulation_cluster_do_migration: pushing " << node->cCluster_TreeNode->cCluster_UniqueId << " / " << cParameters.number_of_local_clusters << " backward to " << dst_rank << std::endl;
#	endif

				/*
				 * step 1) transfer unique ID to setup cluster tree node
				 * step 2) send triangle factory
				 */
#	if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
				sierpi::CMigration::sendRawClass(node->cCluster_TreeNode->cCluster_UniqueId, dst_rank);
				sierpi::CMigration::sendRawClass(node->cCluster_TreeNode->cTriangleFactory, dst_rank);
#	else
				node->cCluster_TreeNode->initial_migration_uniqueId.send(node->cCluster_TreeNode->cCluster_UniqueId, dst_rank);
				node->cCluster_TreeNode->initial_migration_cTriangleFactory.send(node->cCluster_TreeNode->cTriangleFactory, dst_rank);
#	endif

				/*
				 * step 3) send global tree stuff
				 */
				node->migration_send(src_rank, dst_rank);

				/*
				 * step 4) send remaining cluster stuff
				 */
				node->cCluster_TreeNode->migration_send(src_rank, dst_rank);

				pushed_clusters_postprocessing_vector.push_back(node);

				cParameters.migration_sent_number_of_local_cells += node->workload_in_subtree;
				cParameters.migration_sent_number_of_local_cluster++;

				local_modification = true;

				return true;
			}
		);


		cDomainClusters.traverse_GenericTreeNode_RestrictedContinuation_Reversed_Serial(
			[&](CGeneric_TreeNode_ *node) -> bool
			{
				int dst_rank = node->cCluster_TreeNode->migration_dst_rank;

				if (dst_rank < 0)
					return false;

				// no handling of backward push
				if (src_rank-1 == dst_rank)
					return false;

				assert(dst_rank == src_rank+1);
				assert(dst_rank != src_rank);

#	if DEBUG
				pushed_forward = true;

				if (cParameters.verbosity_level > 9)
					std::cout << "p_simulation_cluster_do_migration: pushing " << node->cCluster_TreeNode->cCluster_UniqueId << " / " << cParameters.number_of_local_clusters << " forward to " << dst_rank << std::endl;
#	endif

				/*
				 * step 1) transfer unique ID to setup cluster tree node
				 */
#	if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
				sierpi::CMigration::sendRawClass(node->cCluster_TreeNode->cCluster_UniqueId, dst_rank);
#	else
				node->cCluster_TreeNode->initial_migration_uniqueId.send(node->cCluster_TreeNode->cCluster_UniqueId, dst_rank);
#	endif

				/*
				 * step 2) send triangle factory
				 */
#	if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
				sierpi::CMigration::sendRawClass(node->cCluster_TreeNode->cTriangleFactory, dst_rank);
#	else
				node->cCluster_TreeNode->initial_migration_cTriangleFactory.send(node->cCluster_TreeNode->cTriangleFactory, dst_rank);
#	endif

				/*
				 * step 3) send global tree stuff
				 */
				node->migration_send(src_rank, dst_rank);

				/*
				 * step 4) send remaining cluster stuff
				 */
				node->cCluster_TreeNode->migration_send(src_rank, dst_rank);

				pushed_clusters_postprocessing_vector.push_back(node);

				cParameters.migration_sent_number_of_local_cells += node->workload_in_subtree;
				cParameters.migration_sent_number_of_local_cluster++;

				local_modification = true;

				return true;
			}
		);



		/*
		 * Send FIN messages
		 */
		char fin_message_forward = 0;
		sierpi::CMigrationAtomic<char> migration_fin_message_forward;
		if (cParameters.parallelization_mpi_rank < cParameters.parallelization_mpi_size-1)
			migration_fin_message_forward.send(fin_message_forward, cParameters.parallelization_mpi_rank+1);

		char fin_message_backward = 0;
		sierpi::CMigrationAtomic<char> migration_fin_message_backward;
		if (cParameters.parallelization_mpi_rank > 0)
			migration_fin_message_backward.send(fin_message_backward, cParameters.parallelization_mpi_rank-1);


		/*
		 * PULL:
		 * here we assume that clusters are only send from the previous or next mpi rank
		 */
		std::vector<CGeneric_TreeNode_*> pulled_clusters_postprocessing_vector;
		pulled_clusters_postprocessing_vector.reserve(32);

		// FROM LEFT RANK
		if (cParameters.parallelization_mpi_rank > 0)
		{
			// receive from upper MPI rank
			sierpi::CCluster_UniqueId cCluster_UniqueId;

			int src_rank = cParameters.parallelization_mpi_rank-1;

			while (true)
			{
				// receive cluster unique id
				MPI_Status mpi_status;
				MPI_Recv(&cCluster_UniqueId, sizeof(sierpi::CCluster_UniqueId), MPI_BYTE, src_rank, 0, MPI_COMM_WORLD, &mpi_status);

				// check for fin message
				int count;
				MPI_Get_count(&mpi_status, MPI_BYTE, &count);

				// fin message received
				if (count == 1)
					break;

				assert(count == sizeof(sierpi::CCluster_UniqueId));

#	if DEBUG
				if (pushed_backward)
				{
					std::cerr << "ERROR: PULLED AND PUSHED BACKWARD!!!" << std::endl;
					exit(-1);
				}
#	endif

				// setup cluster for simulation
				CGeneric_TreeNode_ *cGeneric_TreeNode =
				p_simulation_cluster_migration_setup(
					cCluster_UniqueId,
					src_rank
				);


#	if DEBUG
				if (cParameters.verbosity_level > 9)
				{
					std::cout << "p_simulation_cluster_do_migration: pull " << cCluster_UniqueId << " from backwards " << src_rank << std::endl;
				}
#	endif

				pulled_clusters_postprocessing_vector.push_back(cGeneric_TreeNode);

				local_modification = true;
			}
		}


		// FROM RIGHT RANK
		if (cParameters.parallelization_mpi_rank < cParameters.parallelization_mpi_size-1)
		{
			// receive from upper mpi rank
			sierpi::CCluster_UniqueId cCluster_UniqueId;

			int src_rank = cParameters.parallelization_mpi_rank+1;

			while (true)
			{
				// receive cluster unique id
				MPI_Status mpi_status;
				MPI_Recv(&cCluster_UniqueId, sizeof(sierpi::CCluster_UniqueId), MPI_BYTE, src_rank, 0, MPI_COMM_WORLD, &mpi_status);

				// check for fin message
				int count;
				MPI_Get_count(&mpi_status, MPI_BYTE, &count);

				// fin message received
				if (count == 1)
					break;

#	if DEBUG
				if (count != sizeof(sierpi::CCluster_UniqueId))
					std::cerr << "final message not of correct size. size is " << count << " instead of 1" << std::endl;

				if (pushed_forward)
				{
					std::cerr << "ERROR: PULLED AND PUSHED FORWARD!!!" << std::endl;
					exit(-1);
				}
#	endif

				// setup cluster for simulation
				CGeneric_TreeNode_ *cGeneric_TreeNode =
				p_simulation_cluster_migration_setup(
					cCluster_UniqueId,
					src_rank
				);

#	if DEBUG
				if (cParameters.verbosity_level > 9)
					std::cout << "p_simulation_cluster_do_migration: pull " << cCluster_UniqueId << " from front " << src_rank << std::endl;
#	endif

				pulled_clusters_postprocessing_vector.push_back(cGeneric_TreeNode);

				local_modification = true;
			}
		}


		if (!local_modification)
		{
			if (cParameters.parallelization_mpi_rank < cParameters.parallelization_mpi_size-1)
				migration_fin_message_forward.wait();

			if (cParameters.parallelization_mpi_rank > 0)
				migration_fin_message_backward.wait();

			return false;
		}


		/*
		 * run post-processing to setup all adjacent edge comm element data
		 * _after_ all
		 */
		for (size_t i = 0; i < pulled_clusters_postprocessing_vector.size(); i++)
		{
			pulled_clusters_postprocessing_vector[i]->cCluster_TreeNode->migration_recv_postprocessing(
					cDomainClusters
				);

			pulled_clusters_postprocessing_vector[i]->cCluster_TreeNode->cSimulation_Cluster->cSetupColumnTraversal.setup_KernelClass(
					cParameters.simulation_dataset_breaking_dam_posx,
					cParameters.simulation_dataset_breaking_dam_posy,
					cParameters.simulation_dataset_breaking_dam_radius,

					2,	// REFINE ONLY
					&cDatasets
				);

			pulled_clusters_postprocessing_vector[i]->cCluster_TreeNode->cSimulation_Cluster->cEdgeCommTraversal.setParameters(
					cParameters.simulation_parameter_global_timestep_size,
					cParameters.simulation_dataset_default_domain_size_x*cParameters.simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,
					cParameters.simulation_parameter_gravitation,

					cParameters.adaptive_refine_parameter_0,
					cParameters.adaptive_coarsen_parameter_0,

					&cDatasets
				);

			pulled_clusters_postprocessing_vector[i]->cCluster_TreeNode->cSimulation_Cluster->cAdaptiveTraversal.setup_KernelClass(
					cParameters.simulation_dataset_default_domain_size_x*cParameters.simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain,

					cParameters.adaptive_refine_parameter_0,
					cParameters.adaptive_coarsen_parameter_0,

					cParameters.adaptive_refine_parameter_1,
					cParameters.adaptive_coarsen_parameter_1,

					&cDatasets
				);
		}



		for (size_t i = 0; i < pushed_clusters_postprocessing_vector.size(); i++)
		{
			CGeneric_TreeNode_ *cGeneric_TreeNode = pushed_clusters_postprocessing_vector[i];

			assert(cGeneric_TreeNode->cCluster_TreeNode->migration_dst_rank >= 0);

			int dst_rank = cGeneric_TreeNode->cCluster_TreeNode->migration_dst_rank;

			/*
			 * the postprocessing waits until the cluster was indeed send to the adjacent node.
			 *
			 * if and only if this happened, the associated data may be deleted.
			 *
			 * this has to be done without deleting the node to avoid race conditions!
			 */
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
			cGeneric_TreeNode->cCluster_TreeNode->initial_migration_uniqueId.wait();
			cGeneric_TreeNode->cCluster_TreeNode->initial_migration_cTriangleFactory.wait();
#endif

			cGeneric_TreeNode->migration_send_postprocessing(dst_rank);
			cGeneric_TreeNode->cCluster_TreeNode->migration_send_postprocessing(dst_rank);


			/*
			 * step 1) update number of local cells and number of cluster
			 */
			long long number_of_cells_in_cluster = cGeneric_TreeNode->cCluster_TreeNode->cStacks->structure_stacks.structure_getNumberOfTrianglesInTriangle();

			cParameters.number_of_local_cells -= number_of_cells_in_cluster;
			cParameters.number_of_local_clusters -= 1;

			cParameters.migration_sent_number_of_local_cells += number_of_cells_in_cluster;
			cParameters.migration_sent_number_of_local_cluster++;

		}



#if CONFIG_ENABLE_SCAN_DATA

#if DEBUG
		long long test_number_of_local_cells = cParameters.number_of_local_cells;
#endif

		// set to 0 in case of no cells existing
		cParameters.number_of_local_cells = 0;
		cDomainClusters.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Reduce_Parallel(
			[&](	CGeneric_TreeNode_ *cGenericTreeNode,
					long long *o_number_of_local_cells
				)
				{
					// LEAF NODES
					if (cGenericTreeNode->cCluster_TreeNode->migration_dst_rank < 0)
					{
						*o_number_of_local_cells = cGenericTreeNode->workload_in_subtree;
						return;
					}

					/*
					 * step 2): remove migrated nodes
					 * upper nodes are removed in the upcoming step
					 */
					delete cGenericTreeNode->cCluster_TreeNode;
					cGenericTreeNode->cCluster_TreeNode = nullptr;

					*o_number_of_local_cells = 0;
				}
				,
				[&](	CGeneric_TreeNode_ *cGenericTreeNode,
						long long *o_number_of_local_cells
				)
				{
					// MID NODES PREORDER
					cGenericTreeNode->workload_in_subtree = *o_number_of_local_cells;

					// MID NODES POSTORDER
					CGeneric_TreeNode_ *first_child_node = cGenericTreeNode->first_child_node;
					if (first_child_node != nullptr)
					{
						if (	(first_child_node->first_child_node == nullptr) &&
								(first_child_node->second_child_node == nullptr) &&
								(first_child_node->cCluster_TreeNode == nullptr)
						) {
							delete first_child_node;
							cGenericTreeNode->first_child_node = nullptr;
						}
					}


					CGeneric_TreeNode_ *second_child_node = cGenericTreeNode->second_child_node;
					if (second_child_node != nullptr)
					{
						if (	(second_child_node->first_child_node == nullptr) &&
								(second_child_node->second_child_node == nullptr) &&
								(second_child_node->cCluster_TreeNode == nullptr)
						) {
							delete second_child_node;
							cGenericTreeNode->second_child_node = nullptr;
						}
					}

					cGenericTreeNode->workload_in_subtree = *o_number_of_local_cells;
				},
				&sierpi::CReduceOperators::ADD,
				&cParameters.number_of_local_cells
			);

#if DEBUG
		if (test_number_of_local_cells != cParameters.number_of_local_cells)
		{
			std::cout << "test_number_of_local_cells != cParameters.number_of_local_cells" << std::endl;
			assert(false);
		}
#endif

#else

		cDomainClusters.traverse_GenericTreeNode_LeafAndPostorderMidNodes_Parallel(
			[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					// LEAF NODES
					if (cGenericTreeNode->cCluster_TreeNode->migration_dst_rank < 0)
						return;

					/*
					 * step 2): remove migrated nodes
					 * upper nodes are removed in the upcoming step
					 */
					delete cGenericTreeNode->cCluster_TreeNode;
					cGenericTreeNode->cCluster_TreeNode = nullptr;
				},
				[&](CGeneric_TreeNode_ *cGenericTreeNode)
				{
					// MIDNODES POSTORDER
					CGeneric_TreeNode_ *first_child_node = cGenericTreeNode->first_child_node;
					if (first_child_node != nullptr)
					{
						if (	(first_child_node->first_child_node == nullptr) &&
								(first_child_node->second_child_node == nullptr) &&
								(first_child_node->cCluster_TreeNode == nullptr)
						) {
							delete first_child_node;
							cGenericTreeNode->first_child_node = nullptr;
						}
					}


					CGeneric_TreeNode_ *second_child_node = cGenericTreeNode->second_child_node;
					if (second_child_node != nullptr)
					{
						if (	(second_child_node->first_child_node == nullptr) &&
								(second_child_node->second_child_node == nullptr) &&
								(second_child_node->cCluster_TreeNode == nullptr)
						) {
							delete second_child_node;
							cGenericTreeNode->second_child_node = nullptr;
						}
					}
				}
			);
#endif


#if CONFIG_ENABLE_SCAN_DATA
		updateClusterScanInformation();
#endif
		if (cParameters.parallelization_mpi_rank < cParameters.parallelization_mpi_size-1)
			migration_fin_message_forward.wait();

		if (cParameters.parallelization_mpi_rank > 0)
			migration_fin_message_backward.wait();

		return true;
	}
#endif




public:
	/**
	 * \brief update split and join size
	 *
	 * this method is intended to optimize the split and/or join size
	 * related to the current workload size
	 */
	inline void autotune_split_join_sizes()
	{
		if (	cParameters.cluster_update_split_join_size_after_elapsed_timesteps == 0 ||
				cParameters.cluster_update_split_join_size_after_elapsed_scalar < 0
		)
			return;

#if 1
		if (
				cParameters.simulation_timestep_nr % cParameters.cluster_update_split_join_size_after_elapsed_timesteps == 0
		)
		{
			unsigned long long num_cores = cParameters.parallelization_mpi_size*cParameters.simulation_threading_number_of_threads;

			cParameters.cluster_split_workload_size = std::max((unsigned long long)4, cParameters.number_of_global_cells/(num_cores*cParameters.cluster_update_split_join_size_after_elapsed_scalar));
			cParameters.cluster_join_workload_size = cParameters.cluster_split_workload_size >> 1;
		}

#else
		if (cParameters.simulation_timestep_nr % cParameters.cluster_update_split_join_size_after_elapsed_timesteps == 0)
		{
			if (cParameters.cluster_update_split_join_size_after_elapsed_scalar != 0)
			{
				/**
				 * compute the split/join sizes by using `cluster_update_split_join_size_after_elapsed_scalar`
				 */
				cParameters.cluster_split_workload_size = std::sqrt((double)cParameters.number_of_global_cells)*cParameters.cluster_update_split_join_size_after_elapsed_scalar;
				cParameters.cluster_join_workload_size = cParameters.cluster_split_workload_size >> 1;
			}
			else
			{
				/**
				 * lookup the best split/join in a table
				 */
				cSplitJoinTuning.updateSplitJoin(
						cParameters.cluster_split_workload_size,
						cParameters.cluster_join_workload_size,
						cParameters.number_of_local_cells
					);
			}
		}
#endif
	}

};



#endif
