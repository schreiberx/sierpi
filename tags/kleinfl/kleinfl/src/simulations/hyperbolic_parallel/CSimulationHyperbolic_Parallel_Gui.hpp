/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_HYPERBOLIC_PARALLEL_GUI_HPP_
#define CSIMULATION_HYPERBOLIC_PARALLEL_GUI_HPP_


#include "global_config.h"
#include "../hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../hyperbolic_common/CParameters.hpp"

#include "libsierpi/cluster/CDomainClusters.hpp"
#include "CSimulationHyperbolic_Cluster.hpp"


/*
 * Write data stored in recursive SFC format to 1D arrays data structure:
 *
 * COutputGridDataArrays represents the container.
 * COutputGridDataArrays implements the kernel which writes the data.
 */
#include "libsierpi/CGridDataArrays.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"

/*
 * root renderer:
 *
 * setup buffers, render specific primitives stored in buffer
 */
#include "libgl/draw/CGlDrawWireframeFromVertexArray.hpp"
#include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalArray.hpp"
#include "libgl/draw/CGlDrawTrianglesFromVertexAndNormalAndDofArray.hpp"


#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS

#	include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays_ContinousSurface_2Pass_1stPass.hpp"
#	include "../hyperbolic_common/subsimulation_generic/kernels/backends/COutputGridDataArrays_ContinousSurface_2Pass_2ndPass.hpp"
#	include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"

#elif CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

#	include "../hyperbolic_common/subsimulation_tsunami/kernels/backends/COutputGridDataArrays_ContinousSurface.hpp"
#	include "../hyperbolic_common/traversal_helpers/CHelper_GenericParallelVertexDataCommTraversals.hpp"

#endif

#include "../libsierpi/grid/CCube_To_Sphere_Projection.hpp"


#include "mainvis/CCommonShaderPrograms.hpp"

/*
 * interfaces which have to be implemented
 */
#include "CSimulation_MainInterface.hpp"
#include "CSimulation_MainGuiInterface.hpp"

/**
 * \brief Main class for parallel Hyperbolic Simulation
 *
 * This class is the central point of a parallel Hyperbolic simulation.
 *
 * sets up the simulation.
 * It manages all sub-clusters, creates the initial domain triangulation and
 */
class CSimulationHyperbolic_Parallel_Gui	:
	public CSimulation_MainGuiInterface,
	public CSimulation_MainInterface
{
	/**
	 * Convenient typedefs
	 */
	typedef sierpi::CCluster_TreeNode<CSimulationHyperbolic_Cluster> CCluster_TreeNode_;

	/**
	 * Typedefs. among others used by cluster handler
	 */
	typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	/**
	 * root renderer
	 */
	CGlDrawTrianglesWithVertexAndNormalArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesWithVertexAndNormalArray;
	CGlDrawTrianglesWithVertexAndNormalAndDofArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawTrianglesWithVertexAndNormalAndDofArray;
	CGlDrawWireframeFromVertexArray<CHyperbolicTypes::CVisualizationTypes::T> cGlDrawWireframeFromVertexArray;

	/**
	 * reference to simulation parameters
	 */
	CParameters &cParameters;

	/**
	 * reference to clusters to start traversals
	 */
	sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &cDomainClusters;

	/**
	 * last surface visualization method
	 */
	int last_surface_visualization_method;

	/**
	 * storage for rendering DOFs
	 */
	CGridDataArrays<3,6> cGridDataArrays_render_DOFs;

	/**
	 * force update of cGridDataArrays_render_DOFs, e.g. in case of a key press event
	 */
	bool update_cGridDataArrays_render_DOFs;


	enum EDofPostprocessing
	{
		DOF_POSTPROCESSING_NONE = 0,
		DOF_POSTPROCESSING_TRANSLATE_AND_SCALE = 1 << 1,
		DOF_POSTPROCESSING_TRANSLATE_AND_SCALE_SMOOTH = 1 << 2,
		DOF_POSTPROCESSING_SCALE_ONLY = 1 << 3,
		DOF_POSTPROCESSING_SPHERICAL_MAPPING = 1 << 4,
		DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE = 1 << 5,
		DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE = 1 << 6
	};

public:
	/**
	 * constructor for parallel hyperbolic simulation
	 */
	CSimulationHyperbolic_Parallel_Gui(
			CParameters &i_cParameters,
			sierpi::CDomainClusters<CSimulationHyperbolic_Cluster> &i_cDomainClusters
	)	:
		cParameters(i_cParameters),
		cDomainClusters(i_cDomainClusters),
		last_surface_visualization_method(0),
		update_cGridDataArrays_render_DOFs(false)
	{
	}

	void resetGui()
	{
		std::cout << "RESET" << std::endl;
		update_cGridDataArrays_render_DOFs = true;
	}

	/**
	 * Deconstructor
	 */
	virtual ~CSimulationHyperbolic_Parallel_Gui()
	{
	}



public:
	/**
	 * ignore key-up events
	 */
	bool gui_key_up_event(
			int i_key
	)
	{
		return false;
	}



	/**
	 * handle key-down event
	 */
	bool gui_key_down_event(
			int i_key	///< pressed key (ASCII)
	)
	{
		switch(i_key)
		{
			case 'i':
				cParameters.simulation_random_raindrops_activated ^= true;
				break;

			case 'z':
				cParameters.simulation_world_scene_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cParameters.simulation_world_scene_id << std::endl;
				break;

			case 'x':
				cParameters.simulation_world_scene_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "World ID: " << cParameters.simulation_world_scene_id << std::endl;
				break;

			case 'Z':
				cParameters.simulation_dataset_0_id--;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cParameters.simulation_dataset_0_id << std::endl;
				break;

			case 'X':
				cParameters.simulation_dataset_0_id++;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Terrain ID: " << cParameters.simulation_dataset_0_id << std::endl;
				break;

			case 'j':
				runSingleTimestep();
				break;

			case 'y':
				cParameters.grid_initial_cluster_splits += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 'h':
				if (cParameters.grid_initial_cluster_splits > 0)
					cParameters.grid_initial_cluster_splits -= 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial cluster splits to " << cParameters.grid_initial_cluster_splits << std::endl;
				break;

			case 't':
				cParameters.grid_initial_recursion_depth += 1;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				break;

			case 'T':
				cParameters.grid_max_relative_recursion_depth += 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'g':
				if (cParameters.grid_initial_recursion_depth > 0)
					cParameters.grid_initial_recursion_depth -= 1;
				std::cout << "Setting initial recursion depth to " << cParameters.grid_initial_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'G':
				if (cParameters.grid_max_relative_recursion_depth > 0)
					cParameters.grid_max_relative_recursion_depth -= 1;
				std::cout << "Setting relative max. refinement recursion depth to " << cParameters.grid_max_relative_recursion_depth << std::endl;
				reset();
				setup_GridDataWithAdaptiveSimulation();
				break;

			case 'P':
				output_ClusterTreeInformation();
				break;

			case 'c':
				setup_RadialDamBreak(
						cParameters.simulation_dataset_breaking_dam_posx,
						cParameters.simulation_dataset_breaking_dam_posy,
						cParameters.simulation_dataset_breaking_dam_radius
					);
				break;

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
			case '/':
				std::cout << "VALIDATION..." << std::endl;
				action_Validation();
				break;
#endif

			default:
				return false;
		}

		update_cGridDataArrays_render_DOFs = true;

		return true;
	}



	/**
	 * handle gui mouse motion event
	 */
	bool gui_mouse_motion_event(
		T i_mouse_coord_x,
		T i_mouse_coord_y,
		int i_button
	)
	{
		if (i_button == CRenderWindow::MOUSE_BUTTON_RIGHT)
		{
			setup_RadialDamBreak(i_mouse_coord_x, i_mouse_coord_y, cParameters.simulation_dataset_breaking_dam_radius);
			update_cGridDataArrays_render_DOFs = true;

			return true;
		}

		return false;
	}



	/**
	 * handle mouse button event
	 */
	bool gui_mouse_button_down_event(
		T i_mouse_coord_x,	///< x-coordinate of mouse cursor
		T i_mouse_coord_y,	///< y-coordinate of mouse cursor
		int i_button			///< pressed button id
	)
	{
		bool retval;
		retval = gui_mouse_motion_event(i_mouse_coord_x, i_mouse_coord_y, i_button);

		if (retval)
			update_cGridDataArrays_render_DOFs = true;

		return retval;
	}



	/**
	 * render boundaries. For SWE, this are the bathymetries
	 */
	const char* render_boundaries(
			int i_bathymetry_visualization_method,
			CCommonShaderPrograms &cCommonShaderPrograms
	)
	{

#if CONFIG_SUB_SIMULATION_TSUNAMI

		int flags;

		switch(i_bathymetry_visualization_method)
		{
			case 2:
			{
				flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE5;

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 1, flags);

				CDatasets *cDatasets = getDatasets();
				storeDOFsToGridDataArrays(&cGridDataArrays, flags, 0, cDatasets, DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE | DOF_POSTPROCESSING_TRANSLATE_AND_SCALE);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;
				T t;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						t = v[2];	v[2] = -v[1];	v[1] = t;
						t = n[2];	n[2] = -n[1];	n[1] = t;

						T s = cGridDataArrays.dof_element[5][i];
						v[1] = (s+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cGlDrawTrianglesWithVertexAndNormalArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();

				return "benchmark value";
			}
				break;

			case 3:
			{
				// do not render bathymetry
			}
				return "blank";

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
			case 4:
			{
				flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE3;

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 3, flags);
				storeDOFsToGridDataArrays(&cGridDataArrays, flags, 1, nullptr, DOF_POSTPROCESSING_SPHERICAL_MAPPING);

				cCommonShaderPrograms.cBlinn.use();
				cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cGlDrawTrianglesWithVertexAndNormalArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
				cCommonShaderPrograms.cBlinn.disable();

				return "b (cubedSphere projection)";
			}
				break;
#endif

			case 7:
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1 && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			{
				flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE3;

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 3, flags);
				storeDOFsToGridDataArrays_smootingEnabled(&cGridDataArrays, flags, 1, nullptr, DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE);

				cCommonShaderPrograms.cBathymetry.use();

					cGlDrawTrianglesWithVertexAndNormalAndDofArray.initRendering();

						cGlDrawTrianglesWithVertexAndNormalAndDofArray.render(
								cGridDataArrays.triangle_vertex_buffer,
								cGridDataArrays.triangle_normal_buffer,
								cGridDataArrays.dof_element[3],
								cParameters.number_of_local_cells*3
							);

					cGlDrawTrianglesWithVertexAndNormalAndDofArray.shutdownRendering();

				cCommonShaderPrograms.cBathymetry.disable();
				return "cubedSphere smoothed";
			}
#endif

			case 8:
			case 1:
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			{
				flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE3;

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 3, flags);
				storeDOFsToGridDataArrays_smootingEnabled(&cGridDataArrays, flags, 0, nullptr, DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE | DOF_POSTPROCESSING_TRANSLATE_AND_SCALE_SMOOTH);

				cCommonShaderPrograms.cBathymetry.use();

					cGlDrawTrianglesWithVertexAndNormalAndDofArray.initRendering();

						cGlDrawTrianglesWithVertexAndNormalAndDofArray.render(
								cGridDataArrays.triangle_vertex_buffer,
								cGridDataArrays.triangle_normal_buffer,
								cGridDataArrays.dof_element[3],
								cParameters.number_of_local_cells*3
							);

					cGlDrawTrianglesWithVertexAndNormalAndDofArray.shutdownRendering();

				cCommonShaderPrograms.cBathymetry.disable();
				return "bathymetry smoothed";
			}
#endif

				break;


			default:
			{
				flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE3;

				CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 3, flags);
				storeDOFsToGridDataArrays(&cGridDataArrays, flags, 0, nullptr, DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE | DOF_POSTPROCESSING_TRANSLATE_AND_SCALE);

				T *v = cGridDataArrays.triangle_vertex_buffer;
				T *n = cGridDataArrays.triangle_normal_buffer;

				for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
				{
					for (int vn = 0; vn < 3; vn++)
					{
						v[1] = cGridDataArrays.dof_element[3][i*3+vn]*
								cParameters.visualization_data1_scale_factor*
								cParameters.visualization_scale_z;

						v += 3;
						n += 3;
					}
				}


				cCommonShaderPrograms.cBathymetry.use();

				cGlDrawTrianglesWithVertexAndNormalAndDofArray.initRendering();

				cGlDrawTrianglesWithVertexAndNormalAndDofArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cGridDataArrays.triangle_normal_buffer,
						cGridDataArrays.dof_element[3],
						cParameters.number_of_local_cells*3
					);

				cGlDrawTrianglesWithVertexAndNormalAndDofArray.shutdownRendering();

				cCommonShaderPrograms.cBathymetry.disable();
			}
			return "simple";
		}
#else
		return "no boundaries (bathymetries) for this simulation available";
#endif

		return "";
	}


	/**
	 * translate and scale vertex with specific visualization parameters
	 */
	inline void p_applyTranslateAndScaleToVertex(
			T *io_vertex
	)
	{
		io_vertex[0] = (io_vertex[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
		io_vertex[1] = (io_vertex[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;
		io_vertex[2] = (io_vertex[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_z;
	}

	inline void p_applyTranslateAndScaleToNormal(
			T *io_vertex
	)
	{
		io_vertex[0] = io_vertex[0]/cParameters.visualization_scale_x;
		io_vertex[1] = io_vertex[1]/cParameters.visualization_scale_y;
		io_vertex[2] = io_vertex[2]/cParameters.visualization_scale_z;
	}


	/**
	 * translate and scale vertex with specific visualization parameters
	 */
	inline void p_applyScaleToVertex(
			T *io_vertex
	)
	{
		io_vertex[0] = io_vertex[0]*cParameters.visualization_scale_x;
		io_vertex[1] = io_vertex[1]*cParameters.visualization_scale_y;
		io_vertex[2] = io_vertex[2]*cParameters.visualization_scale_z;
	}




	/**
	 * store the DOFs and grid data to an array
	 */
private:
	void storeDOFsToGridDataArrays(
			CGridDataArrays<3,6> *io_cGridDataArrays,
			int i_flags,
			int i_preprocessing_mode,
			CDatasets *i_cDatasets,			///< datasets to get benchmark data
			int i_postprocessing_mode = DOF_POSTPROCESSING_TRANSLATE_AND_SCALE
	)
	{

		bool run_postprocessing = false;

		if (i_postprocessing_mode)
			run_postprocessing = true;



#if SIMULATION_MULTILAYER_ENABLED
		int i_layer = cParameters.visualization_multi_layer_id;
#endif

		// TRAVERSAL
		cDomainClusters.traverse_GenericTreeNode_Parallel(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				size_t i_offset = io_cGridDataArrays->getNextTriangleCellStartId(i_cGenericTreeNode->workload_in_subtree);

				// We instantiate it right here to avoid any overhead due to split/join operations
				sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

				cOutputGridDataArrays.setup_sfcMethods(i_cGenericTreeNode->cCluster_TreeNode->cTriangleFactory);
				cOutputGridDataArrays.cKernelClass.setup(
						io_cGridDataArrays,
						i_cDatasets,
						i_offset,
						i_flags,
						i_preprocessing_mode
#if SIMULATION_MULTILAYER_ENABLED
						,
						i_layer
#endif
					);

				cOutputGridDataArrays.action(i_cGenericTreeNode->cCluster_TreeNode->cStacks);


				/*
				 * postprocessing
				 */
				if (run_postprocessing)
				{
					if (i_postprocessing_mode & DOF_POSTPROCESSING_TRANSLATE_AND_SCALE)
					{
						for (size_t i = i_offset; i < i_offset+i_cGenericTreeNode->workload_in_subtree; i++)
						{
							T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

							for (int vn = 0; vn < 3; vn++)
							{
								p_applyTranslateAndScaleToVertex(v);
								v += 3;
							}

							if (i_flags & CGridDataArrays_Enums::VALUE0)
								io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;


							if (i_flags & CGridDataArrays_Enums::VALUE3)
							{
								if (io_cGridDataArrays->dof_variables_multiplier == 1)
								{
									io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;
								}
								else
								{
									io_cGridDataArrays->dof_element[3][i*3+0] *= cParameters.visualization_scale_z;
									io_cGridDataArrays->dof_element[3][i*3+1] *= cParameters.visualization_scale_z;
									io_cGridDataArrays->dof_element[3][i*3+2] *= cParameters.visualization_scale_z;
								}
							}

							if (i_flags & CGridDataArrays_Enums::VALUE5)
								io_cGridDataArrays->dof_element[5][i] *= cParameters.visualization_scale_z;
						}
					}

					if (i_postprocessing_mode & DOF_POSTPROCESSING_SCALE_ONLY)
					{
						for (size_t i = i_offset; i < i_offset+i_cGenericTreeNode->workload_in_subtree; i++)
						{
							T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

							for (int vn = 0; vn < 3; vn++)
							{
								p_applyScaleToVertex(v);
								v += 3;
							}

							if (i_flags & CGridDataArrays_Enums::VALUE0)
								io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;

							if (i_flags & CGridDataArrays_Enums::VALUE3)
							{
								if (io_cGridDataArrays->dof_variables_multiplier == 1)
								{
									io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;
								}
								else
								{
									io_cGridDataArrays->dof_element[3][i*3+0] *= cParameters.visualization_scale_z;
									io_cGridDataArrays->dof_element[3][i*3+1] *= cParameters.visualization_scale_z;
									io_cGridDataArrays->dof_element[3][i*3+2] *= cParameters.visualization_scale_z;
								}
							}

							if (i_flags & CGridDataArrays_Enums::VALUE5)
								io_cGridDataArrays->dof_element[5][i] *= cParameters.visualization_scale_z;
						}
					}

					if (i_postprocessing_mode & DOF_POSTPROCESSING_SPHERICAL_MAPPING)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						for (size_t i = i_offset; i < limit; i++)
						{
							T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i];

							// determine face
							T average_vertex_x = (v[3*0+0] + v[3*1+0] + v[3*2+0])*(T)(1.0/3.0);
							T average_vertex_y = (v[3*0+1] + v[3*1+1] + v[3*2+1])*(T)(1.0/3.0);

							// determine face
							CCube_To_Sphere_Projection::EFaceType faceType =
									CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(average_vertex_x, average_vertex_y);

							// project 2D coordinate to cube face
							CCube_To_Sphere_Projection::project2DTo2DFace(
									faceType,
									&v[3*0+0], &v[3*0+1],
									&v[3*1+0], &v[3*1+1],
									&v[3*2+0], &v[3*2+1]
								);

							// project to sphere
							for (int j = 0; j < 3; j++)
							{
								CCube_To_Sphere_Projection::project2DFaceTo3D(
										faceType,
										v[3*j+0], v[3*j+1],
										&v[3*j+0], &v[3*j+1], &v[3*j+2]
									);
							}

							// apply height transformation
							if (	(i_flags & CGridDataArrays_Enums::VALUE0) && (i_flags & CGridDataArrays_Enums::VALUE3)	)
							{
								T delta = io_cGridDataArrays->dof_element[0][i]+io_cGridDataArrays->dof_element[3][i];

								if (io_cGridDataArrays->dof_element[0][i] > 0)
								{
									delta *= cParameters.visualization_scale_z*cParameters.visualization_data0_scale_factor;

									// only apply scaling on top of sphere surface
									// v = v + scalar*direction
									//   = v + scalar*v
									for (int k = 0; k < 9; k++)
										v[k] += delta*v[k];
								}
								else
								{
									// move out of sight
									for (int k = 0; k < 9; k++)
										v[k] = std::numeric_limits<T>::infinity();
								}
							}
							else if (i_flags & CGridDataArrays_Enums::VALUE3)
							{
								T delta = io_cGridDataArrays->dof_element[3][i];

								delta *= cParameters.visualization_scale_z*cParameters.visualization_data1_scale_factor;

								// only apply scaling on top of sphere surface
								// v = v + scalar*direction
								//   = v + scalar*v
								for (int k = 0; k < 9; k++)
									v[k] += delta*v[k];
							}


							if (i_flags & CGridDataArrays_Enums::NORMALS)
							{
								T ax = v[3*1+0]-v[3*0+0];	T bx = v[3*2+0]-v[3*0+0];
								T ay = v[3*1+1]-v[3*0+1];	T by = v[3*2+1]-v[3*0+1];
								T az = v[3*1+2]-v[3*0+2];	T bz = v[3*2+2]-v[3*0+2];

								T nx = ay*bz-az*by;
								T ny = az*bx-ax*bz;
								T nz = ax*by-ay*bx;

								T inv_norm = (T)1.0/std::sqrt(nx*nx+ny*ny+nz*nz);

								nx *= inv_norm;
								ny *= inv_norm;
								nz *= inv_norm;

								T *n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i];

								for (int j = 0; j < 3; j++)
								{
									n[3*j+0] = nx;
									n[3*j+1] = ny;
									n[3*j+2] = nz;
								}
							}
						}
					}


					if (i_postprocessing_mode & DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset];
						T *n;

						if (i_flags & CGridDataArrays_Enums::NORMALS)
							n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i_offset];

						for (size_t i = i_offset; i < limit; i++)
						{
							// project to sphere
							for (int j = 0; j < 3; j++)
							{
								T x,y,z;

								x = v[1];
								y = v[2];
								z = v[0];
								v[0] = x;
								v[1] = y;
								v[2] = z;
								v+=3;

								if (i_flags & CGridDataArrays_Enums::NORMALS)
								{
									// TODO: not really correct, but nice enough
									x = n[1];
									y = n[2];
									z = n[0];
									n[0] = x;
									n[1] = y;
									n[2] = z;
									n+=3;
								}
							}
						}
					}


					if (i_postprocessing_mode & DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset];
						T *n;

						if (i_flags & CGridDataArrays_Enums::NORMALS)
							n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i_offset];

						for (size_t i = i_offset; i < limit; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								T t;
								t = v[2];
								v[2] = -v[1];
								v[1] = t;
								v += 3;

								if (i_flags & CGridDataArrays_Enums::NORMALS)
								{
									t = n[2];
									n[2] = -n[1];
									n[1] = t;
									n += 3;
								}
							}
						}
					}
				}
			}
		);

		assert(io_cGridDataArrays->number_of_triangle_cells == cParameters.number_of_local_cells);
	}





#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

	/**
	 * store the DOFs and grid data to an array
	 *
	 * this method creates a smoothed, continous surface
	 */
private:
	void storeDOFsToGridDataArrays_smootingEnabled(
			CGridDataArrays<3,6> *io_cGridDataArrays,
			int i_flags,
			int i_preprocessing_mode,
			CDatasets *i_cDatasets,			///< datasets to get benchmark data
			int i_postprocessing_mode = DOF_POSTPROCESSING_TRANSLATE_AND_SCALE
	)
	{
		i_flags |=
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::NORMALS;


		bool run_postprocessing = false;

		// scale factors for normal components
		T normal_scale_x;
		T normal_scale_y;
		T normal_scale_z;

		if (i_postprocessing_mode)
			run_postprocessing = true;

#if SIMULATION_MULTILAYER_ENABLED
		int i_layer = cParameters.visualization_multi_layer_id;
#endif

		CHyperbolicTypes::CVisualizationTypes::T scale_factor =
			cParameters.visualization_scale_z*
			(i_flags & CGridDataArrays_Enums::VALUE0 ? cParameters.visualization_data0_scale_factor : cParameters.visualization_data1_scale_factor);

		if (i_postprocessing_mode & DOF_POSTPROCESSING_TRANSLATE_AND_SCALE_SMOOTH)
			scale_factor /= cParameters.visualization_scale_x;

		auto lambda_postprocessing =

			// lambda function for postprocessing
			[&](
					CGeneric_TreeNode_ *i_cGenericTreeNode,
					size_t i_offset
			)
			{
				/*
				 * postprocessing
				 */
				if (run_postprocessing)
				{
					if (i_postprocessing_mode & DOF_POSTPROCESSING_TRANSLATE_AND_SCALE)
					{
						T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset]);

						for (size_t i = i_offset; i < i_offset+i_cGenericTreeNode->workload_in_subtree; i++)
						{
							for (int vn = 0; vn < 3; vn++)
							{
								p_applyTranslateAndScaleToVertex(v);
								v += 3;
							}

							if (i_flags & CGridDataArrays_Enums::VALUE0)
								io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;

							if (i_flags & CGridDataArrays_Enums::VALUE3)
								io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;

							if (i_flags & CGridDataArrays_Enums::VALUE5)
								io_cGridDataArrays->dof_element[5][i] *= cParameters.visualization_scale_z;
						}
					}

					if (i_postprocessing_mode & DOF_POSTPROCESSING_TRANSLATE_AND_SCALE_SMOOTH)
					{
						T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset]);

						for (size_t i = i_offset; i < i_offset+i_cGenericTreeNode->workload_in_subtree; i++)
						{
							for (int vn = 0; vn < 3; vn++)
							{
								v[0] = (v[0] + cParameters.visualization_translate_x)*cParameters.visualization_scale_x;
								v[1] = (v[1] + cParameters.visualization_translate_y)*cParameters.visualization_scale_y;

								// use scale_x since we previously scaled it with this value. we do so to preserve the normal.
								v[2] = (v[2] + cParameters.visualization_translate_z)*cParameters.visualization_scale_x;
								v += 3;
							}
						}
					}


					if (i_postprocessing_mode & DOF_POSTPROCESSING_SCALE_ONLY)
					{
						for (size_t i = i_offset; i < i_offset+i_cGenericTreeNode->workload_in_subtree; i++)
						{
							T *v = &(io_cGridDataArrays->triangle_vertex_buffer[3*3*i]);

							for (int vn = 0; vn < 3; vn++)
							{
								p_applyScaleToVertex(v);
								v += 3;
							}

							if (i_flags & CGridDataArrays_Enums::VALUE0)
								io_cGridDataArrays->dof_element[0][i] *= cParameters.visualization_scale_z;

							if (i_flags & CGridDataArrays_Enums::VALUE3)
								io_cGridDataArrays->dof_element[3][i] *= cParameters.visualization_scale_z;

							if (i_flags & CGridDataArrays_Enums::VALUE5)
								io_cGridDataArrays->dof_element[5][i] *= cParameters.visualization_scale_z;
						}
					}

					if (i_postprocessing_mode & DOF_POSTPROCESSING_SPHERICAL_MAPPING)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						for (size_t i = i_offset; i < limit; i++)
						{
							T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i];

							// determine face
							T average_vertex_x = (v[3*0+0] + v[3*1+0] + v[3*2+0])*(T)(1.0/3.0);
							T average_vertex_y = (v[3*0+1] + v[3*1+1] + v[3*2+1])*(T)(1.0/3.0);

							// determine face
							CCube_To_Sphere_Projection::EFaceType faceType =
									CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(average_vertex_x, average_vertex_y);

							// project 2D coordinate to cube face
							CCube_To_Sphere_Projection::project2DTo2DFace(
									faceType,
									&v[3*0+0], &v[3*0+1],
									&v[3*1+0], &v[3*1+1],
									&v[3*2+0], &v[3*2+1]
								);

							// project to sphere
							for (int j = 0; j < 3; j++)
							{
								CCube_To_Sphere_Projection::project2DFaceTo3D(
										faceType,
										v[3*j+0], v[3*j+1],
										&v[3*j+0], &v[3*j+1], &v[3*j+2]
									);
							}

							// apply height transformation
							if (	(i_flags & CGridDataArrays_Enums::VALUE0) && (i_flags & CGridDataArrays_Enums::VALUE3)	)
							{
								T delta = io_cGridDataArrays->dof_element[0][i]+io_cGridDataArrays->dof_element[3][i];

								delta *= cParameters.visualization_scale_z;

								// only apply scaling on top of sphere surface
								for (int k = 0; k < 9; k++)
									v[k] += delta*v[k];
							}

							T ax = v[3*1+0]-v[3*0+0];	T bx = v[3*2+0]-v[3*0+0];
							T ay = v[3*1+1]-v[3*0+1];	T by = v[3*2+1]-v[3*0+1];
							T az = v[3*1+2]-v[3*0+2];	T bz = v[3*2+2]-v[3*0+2];

							T nx = ay*bz-az*by;
							T ny = az*bx-ax*bz;
							T nz = ax*by-ay*bx;

							T inv_norm = (T)1.0/std::sqrt(nx*nx+ny*ny+nz*nz);

							nx *= inv_norm;
							ny *= inv_norm;
							nz *= inv_norm;

							T *n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i];

							for (int j = 0; j < 3; j++)
							{
								n[3*j+0] = nx;
								n[3*j+1] = ny;
								n[3*j+2] = nz;
							}
						}
					}


					if (i_postprocessing_mode & DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset];
						T *n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i_offset];

						for (size_t i = i_offset; i < limit; i++)
						{
							// project to sphere
							for (int j = 0; j < 3; j++)
							{
								T x,y,z;

								x = v[1];
								y = v[2];
								z = v[0];
								v[0] = x;
								v[1] = y;
								v[2] = z;
								v+=3;

								x = n[1];
								y = n[2];
								z = n[0];
								n[0] = x;
								n[1] = y;
								n[2] = z;
								n+=3;
							}
						}
					}


					if (i_postprocessing_mode & DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE)
					{
						size_t limit = i_offset+i_cGenericTreeNode->workload_in_subtree;

						T *v = &io_cGridDataArrays->triangle_vertex_buffer[3*3*i_offset];
						T *n;

						if (i_flags & CGridDataArrays_Enums::NORMALS)
							n = &io_cGridDataArrays->triangle_normal_buffer[3*3*i_offset];

						for (size_t i = i_offset; i < limit; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								T t;
								t = v[2];
								v[2] = -v[1];
								v[1] = t;
								v += 3;

								if (i_flags & CGridDataArrays_Enums::NORMALS)
								{
									t = n[2];
									n[2] = -n[1];
									n[1] = t;
									n += 3;
								}
							}
						}
					}

				}
			};

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_2PASS

		/************************************************************
		 * 2PASS Visualization method extracting normals accurately
		 *************************************************************/

		CHelper_GenericParallelVertexDataCommTraversals::action
		<
			CHyperbolicTypes::CVisualizationTypes::CNodeData,										// node data
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::TRAV,			// traversator to create node data
			CSimulationHyperbolic_Cluster,															// cluster data to access stacks, etc.
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::T>,	// accessors to adjacent stacks
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::TRAV::CKernelClass	// type of kernel class to run 'mid- and last node touch'
		>(
			// exchange helper for vertex data
			&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData_2Pass_1stPass,

			// domain clusters to start traversal
			cDomainClusters,

			// lambda function to setup initial traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::TRAV *i_traversator)
			{
				// adopt vertex buffer stack to number of cells
				size_t n = i_cGenericTreeNode->cCluster_TreeNode->cStacks->cell_data_stacks.forward.getMaxNumberOfElements();
				if (i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices.getMaxNumberOfElements() != n)
					i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices.resize(n);

				i_traversator->cKernelClass.setup(
						&(i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices),
						io_cGridDataArrays,
						i_cDatasets,
						0,
						i_flags,
						i_preprocessing_mode,
#if SIMULATION_MULTILAYER_ENABLED
						i_layer,
#endif
						scale_factor
					);
			},

			// lambda function to setup output traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_1stPass::TRAV *i_traversator,
					size_t i_workload_in_subtree
			) -> size_t
			{
				i_traversator->cKernelClass.setup(
						&(i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices),
						io_cGridDataArrays,
						i_cDatasets,
						0,
						i_flags,
						i_preprocessing_mode,
#if SIMULATION_MULTILAYER_ENABLED
						i_layer,
#endif
						scale_factor
					);

				return 0;
			},

			[&](
					CGeneric_TreeNode_ *i_cGenericTreeNode,
					size_t i_offset
			)
			{
				// dummy
			}
		);




		CHelper_GenericParallelVertexDataCommTraversals::action
		<
			CHyperbolicTypes::CVisualizationTypes::CNodeData,										// node data
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_2ndPass::TRAV,			// traversator to create node data
			CSimulationHyperbolic_Cluster,															// cluster data to access stacks, etc.
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::T>,	// accessors to adjacent stacks
			sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_2ndPass::TRAV::CKernelClass	// type of kernel class to run 'mid- and last node touch'
		>(
			// exchange helper for vertex data
			&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData_2Pass_2ndPass,

			// domain clusters to start traversal
			cDomainClusters,

			// lambda function to setup initial traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_2ndPass::TRAV *i_traversator)
			{
				i_traversator->cKernelClass.setup(
						&(i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices),
						io_cGridDataArrays,
						i_cDatasets,
						0,
						i_flags,
						i_preprocessing_mode
#if SIMULATION_MULTILAYER_ENABLED
						,
						i_layer
#endif
					);
			},

			// lambda function to setup output traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface_2Pass_2ndPass::TRAV *i_traversator,
					size_t i_workload_in_subtree
			) -> size_t
			{
				size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(i_workload_in_subtree);

				i_traversator->cKernelClass.setup(
						&(i_cGenericTreeNode->cCluster_TreeNode->cSimulation_Cluster->cTemporaryVertices),
						io_cGridDataArrays,
						i_cDatasets,
						offset,
						i_flags,
						i_preprocessing_mode
#if SIMULATION_MULTILAYER_ENABLED
						,
						i_layer
#endif
					);

				return offset;
			},

			lambda_postprocessing
		);


#else

		/************************************************************
		 * SINGLE PASS
		 *************************************************************/
		CHelper_GenericParallelVertexDataCommTraversals::action
		<
			CHyperbolicTypes::CVisualizationTypes::CNodeData,								// node data
			sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV,					// traversator to create node data
			CSimulationHyperbolic_Cluster,													// cluster data to access stacks, etc.
			sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CHyperbolicTypes::CVisualizationTypes::T>,	// accessors to adjacent stacks
			sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV::CKernelClass	// type of kernel class to run 'mid- and last node touch'
		>(
			// exchange helper for vertex data
			&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData,

			// domain clusters to start traversal
			cDomainClusters,

			// lambda function to setup initial traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV *i_traversator)
			{
				i_traversator->cKernelClass.setup(
						io_cGridDataArrays,
						i_cDatasets,
						0,
						i_flags,
						i_preprocessing_mode
#if SIMULATION_MULTILAYER_ENABLED
						,
						i_layer
#endif
					);
			},

			// lambda function to setup output traversators
			[&](	CGeneric_TreeNode_ *i_cGenericTreeNode,
					sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV *i_traversator,
					size_t i_workload_in_subtree
			) -> size_t
			{
				size_t offset = io_cGridDataArrays->getNextTriangleCellStartId(i_workload_in_subtree);

				i_traversator->cKernelClass.setup(
						io_cGridDataArrays,
						i_cDatasets,
						offset,
						i_flags,
						i_preprocessing_mode
#if SIMULATION_MULTILAYER_ENABLED
						,
						i_layer
#endif
					);

				return offset;
			},

			lambda_postprocessing
		);
#endif

		assert(io_cGridDataArrays->number_of_triangle_cells == cParameters.number_of_local_cells);
	}
#endif



public:
	virtual CDatasets *getDatasets() = 0;

	/**
	 * render water surface
	 */
	const char *render_DOFs(
		int i_surface_visualization_method,
		CCommonShaderPrograms &i_cCommonShaderPrograms
	)
	{
		const char *ret_str = "[none]";

		bool preprocessing_required = true;
		static long long last_simulation_timestep_nr = -2;

		if (last_simulation_timestep_nr == cParameters.simulation_timestep_nr && !update_cGridDataArrays_render_DOFs)
		{
			preprocessing_required = false;
			ret_str = "[assuming freezed state]";
		}
		last_simulation_timestep_nr = cParameters.simulation_timestep_nr;
		update_cGridDataArrays_render_DOFs = false;



		last_surface_visualization_method = i_surface_visualization_method % 9;

		T *v, *n;
		T t;
		int flags;

		switch(last_surface_visualization_method)
		{
			case -1:
				return "none";
				break;

			case 1:
			{
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE1;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr, DOF_POSTPROCESSING_TRANSLATE_AND_SCALE);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

							v[1] = (cGridDataArrays_render_DOFs.dof_element[1][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

							v += 3;
							n += 3;
						}
					}
				}

				ret_str = "VALUE1";
			}
				break;

			case 2:
			{
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE2;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

							v[1] = (cGridDataArrays_render_DOFs.dof_element[2][i]+cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

							v += 3;
							n += 3;
						}
					}

					ret_str = "VALUE2";
				}
			}
				break;

			case 3:
			{
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE3;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

							v[1] = (cGridDataArrays_render_DOFs.dof_element[3][i]+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

							v += 3;
							n += 3;
						}
					}
				}

				ret_str = "VALUE3";
			}
				break;


#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS==1
			case 4:
			{

				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE0 | CGridDataArrays_Enums::VALUE4;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

							v[1] += cParameters.visualization_translate_z;
							v[1] *= cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;

							v += 3;
							n += 3;
						}
					}
				}

				ret_str = "aligned surface";
			}
				break;
#endif


			case 5:
			{

				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE5;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);

					CDatasets *cDatasets = getDatasets();
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, cDatasets);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

							T s = cGridDataArrays_render_DOFs.dof_element[5][i];
							v[1] = (s+cParameters.visualization_translate_z)*cParameters.visualization_data1_scale_factor*cParameters.visualization_scale_z;

							v += 3;
							n += 3;
						}
					}
				}

				ret_str = "benchmark value";
			}
				break;


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
			case 6:
			{
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE0 | CGridDataArrays_Enums::VALUE3;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr, DOF_POSTPROCESSING_SPHERICAL_MAPPING | DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE);
				}

				ret_str = "h+b (cubedSphere projection)";
			}
				break;
#endif


			case 7:
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1 && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE0 | CGridDataArrays_Enums::VALUE3;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 3, flags);
					storeDOFsToGridDataArrays_smootingEnabled(&cGridDataArrays_render_DOFs, flags, 1, nullptr, DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE);
				}

				ret_str = "smooth surface renderer with height color shader";
				break;
				return "cubedSphere smoothed";
#endif

				return "cubedSphere nothing";
				break;


#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			case 8:
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE0 | CGridDataArrays_Enums::VALUE3;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 3, flags);
					storeDOFsToGridDataArrays_smootingEnabled(&cGridDataArrays_render_DOFs, flags, 0, nullptr, DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE | DOF_POSTPROCESSING_TRANSLATE_AND_SCALE_SMOOTH);
				}

				ret_str = "smooth surface renderer with height color shader";
				break;
#endif



			case 0:
			default:
			{
				if (preprocessing_required)
				{
					flags = CGridDataArrays_Enums::VERTICES | CGridDataArrays_Enums::NORMALS | CGridDataArrays_Enums::VALUE0 | CGridDataArrays_Enums::VALUE3;

					cGridDataArrays_render_DOFs.reset(cParameters.number_of_local_cells, 1, 1, flags);
					storeDOFsToGridDataArrays(&cGridDataArrays_render_DOFs, flags, 0, nullptr);

					v = cGridDataArrays_render_DOFs.triangle_vertex_buffer;
					n = cGridDataArrays_render_DOFs.triangle_normal_buffer;

					for (size_t i = 0; i < (size_t)cParameters.number_of_local_cells; i++)
					{
						for (int vn = 0; vn < 3; vn++)
						{
							t = v[2];	v[2] = -v[1];	v[1] = t;
							t = n[2];	n[2] = -n[1];	n[1] = t;

	#if CONFIG_SUB_SIMULATION_TSUNAMI
							v[1] = (cGridDataArrays_render_DOFs.dof_element[0][i] + cGridDataArrays_render_DOFs.dof_element[3][i] + cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;
	#elif CONFIG_SUB_SIMULATION_EULER
							v[1] = (cGridDataArrays_render_DOFs.dof_element[0][i] + cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;
	#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER
							v[1] = (cGridDataArrays_render_DOFs.dof_element[0][i] + cParameters.visualization_translate_z)*cParameters.visualization_data0_scale_factor*cParameters.visualization_scale_z;
	#else
	#endif

							v += 3;
							n += 3;
						}
					}
				}

#if CONFIG_SUB_SIMULATION_TSUNAMI
				ret_str = "h+b";
#elif CONFIG_SUB_SIMULATION_EULER
				ret_str = "r";
#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER
				ret_str = "r";
#else
#endif
			}
				break;
		}

		switch (last_surface_visualization_method)
		{
		case 6:
		case 7:
			i_cCommonShaderPrograms.cSphereDistanceColorBlinn.use();
		break;

		default:
			i_cCommonShaderPrograms.cHeightColorBlinn.use();
			break;
		}


		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();
		cGlDrawTrianglesWithVertexAndNormalArray.render(
				cGridDataArrays_render_DOFs.triangle_vertex_buffer,
				cGridDataArrays_render_DOFs.triangle_normal_buffer,
				cParameters.number_of_local_cells*3
			);
		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();


		switch (last_surface_visualization_method)
		{
		case 6:
		case 7:
				i_cCommonShaderPrograms.cSphereDistanceColorBlinn.disable();
				break;

			default:
				i_cCommonShaderPrograms.cHeightColorBlinn.disable();
				break;
		}

		return ret_str;
	}


	/**
	 * render a wireframe
	 */
	void render_Wireframe(
			int i_visualization_render_wireframe,			///< submode
			CCommonShaderPrograms &i_cCommonShaderPrograms	///< shader programs
	)
	{
		/*
		 * render wireframe
		 */
		if (i_visualization_render_wireframe & 1)
		{
			int postprocessing_mode = DOF_POSTPROCESSING_SCALE_ONLY;

			if (last_surface_visualization_method == 6 || last_surface_visualization_method == 7)
				postprocessing_mode = DOF_POSTPROCESSING_SPHERICAL_MAPPING | DOF_POSTPROCESSING_FROM_SPHERE_TO_OPENGL_SPACE;
			else
				postprocessing_mode = DOF_POSTPROCESSING_TRANSLATE_AND_SCALE | DOF_POSTPROCESSING_FROM_PLANE_TO_OPENGL_SPACE;

			CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells, 1, 1, CGridDataArrays_Enums::VERTICES);
			storeDOFsToGridDataArrays(&cGridDataArrays, CGridDataArrays_Enums::VERTICES, 0, nullptr, postprocessing_mode);

			i_cCommonShaderPrograms.cBlinn.use();
			i_cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(GLSL::vec3(1,1,1));
			i_cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(GLSL::vec3(0,0,0));
			i_cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(GLSL::vec3(0.1,0.1,0.1));

			cGlDrawWireframeFromVertexArray.initRendering();

			cGlDrawWireframeFromVertexArray.render(
					cGridDataArrays.triangle_vertex_buffer,
					cParameters.number_of_local_cells*3
				);

			cGlDrawWireframeFromVertexArray.shutdownRendering();
			i_cCommonShaderPrograms.cBlinn.disable();
		}
	}


	/**
	 * highlight the cluster borders
	 */
	void render_ClusterBorders(
			int i_visualization_render_cluster_borders,		///< submode
			CCommonShaderPrograms &i_cCommonShaderPrograms	///< shaders
	)
	{
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 1 || i_visualization_render_cluster_borders % 3 == 2)
		{
			CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_clusters, 1, 1, CGridDataArrays_Enums::VERTICES);

			cDomainClusters.traverse_GenericTreeNode_Parallel(
				[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					size_t id = cGridDataArrays.getNextTriangleCellStartId(1);

					T *v = &(cGridDataArrays.triangle_vertex_buffer[id*3*3]);

					v[0] = node->cTriangleFactory.vertices[0][0];
					v[1] = node->cTriangleFactory.vertices[0][1];
					v[2] = 0;
					v += 3;

					v[0] = node->cTriangleFactory.vertices[1][0];
					v[1] = node->cTriangleFactory.vertices[1][1];
					v[2] = 0;
					v += 3;
					v[0] = node->cTriangleFactory.vertices[2][0];
					v[1] = node->cTriangleFactory.vertices[2][1];
					v[2] = 0;
					v += 3;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE != 0
					if (last_surface_visualization_method != 6 && last_surface_visualization_method != 7)
#endif
					{
						T *nv = &(cGridDataArrays.triangle_vertex_buffer[id*3*3]);

						for (int i = 0; i < 3; i++)
						{
							p_applyScaleToVertex(nv);
							std::swap(nv[1], nv[2]);
							nv[2] = -nv[2];
							nv+=3;
						}
					}
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE != 0
					else
					{
						T *v = &(cGridDataArrays.triangle_vertex_buffer[id*3*3]);

						// determine face
						T average_vertex_x = (node->cTriangleFactory.vertices[0][0]+node->cTriangleFactory.vertices[1][0]+node->cTriangleFactory.vertices[2][0])*(T)(1.0/3.0);
						T average_vertex_y = (node->cTriangleFactory.vertices[0][1]+node->cTriangleFactory.vertices[1][1]+node->cTriangleFactory.vertices[2][1])*(T)(1.0/3.0);

						// determine face
						CCube_To_Sphere_Projection::EFaceType faceType =
								CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(average_vertex_x, average_vertex_y);

						// project to sphere
						for (int j = 0; j < 3; j++)
						{
							// project 2D coordinate to cube face
							CCube_To_Sphere_Projection::project2DTo2DFace(
									faceType,
									&v[3*j+0], &v[3*j+1]
								);

							// project to sphere
							CCube_To_Sphere_Projection::project2DFaceTo3D(
									faceType,
									v[3*j+0], v[3*j+1],
									&v[3*j+0], &v[3*j+1], &v[3*j+2]
								);
						}
					}
#endif
				}
			);

			GLSL::vec3 c(0.8, 0.1, 0.1);
			i_cCommonShaderPrograms.cBlinn.use();
			i_cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
			i_cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
			i_cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);

				glDisable(GL_DEPTH_TEST);
				glDisable(GL_CULL_FACE);

				cGlDrawWireframeFromVertexArray.initRendering();

				cGlDrawWireframeFromVertexArray.render(
						cGridDataArrays.triangle_vertex_buffer,
						cParameters.number_of_local_clusters*3
					);

				cGlDrawWireframeFromVertexArray.shutdownRendering();

				glEnable(GL_DEPTH_TEST);
				glEnable(GL_CULL_FACE);

			i_cCommonShaderPrograms.cBlinn.disable();
		}
	}



	/**
	 * render the cluster scan information
	 */
	void render_ClusterScans(
			int i_visualization_render_cluster_borders,
			CCommonShaderPrograms &cCommonShaderPrograms
		)
	{
#if CONFIG_ENABLE_SCAN_DATA
		/*
		 * render cluster borders
		 */
		if (i_visualization_render_cluster_borders % 3 == 2)
		{
			cCommonShaderPrograms.cBlinn.use();

			cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

				cDomainClusters.traverse_GenericTreeNode_Serial(
					[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
					{
						CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

						CHyperbolicTypes::CVisualizationTypes::T vertex_buffer[3*3];

						int thread_mod_id;
						if (i_cGenericTreeNode->workload_thread_id == -1)
							thread_mod_id = 6;
						else
							thread_mod_id = (i_cGenericTreeNode->workload_thread_id+1) % 6;

						static const CHyperbolicTypes::CVisualizationTypes::T ct[7][3] = {
								{1.0, 0.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 0.0, 1.0},
								{1.0, 1.0, 0.0},
								{1.0, 0.0, 1.0},
								{0.0, 1.0, 1.0},
								{1.0, 1.0, 1.0},
						};

						GLSL::vec3 c(ct[thread_mod_id][0], ct[thread_mod_id][1], ct[thread_mod_id][2]);

						cCommonShaderPrograms.cBlinn.material_ambient_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_diffuse_color3_uniform.set(c);
						cCommonShaderPrograms.cBlinn.material_specular_color3_uniform.set(c);

						T *v = vertex_buffer;

						v[0] = node->cTriangleFactory.vertices[0][0];
						v[1] = node->cTriangleFactory.vertices[0][1];
						v[2] = 0.001;
						v += 3;

						v[0] = node->cTriangleFactory.vertices[1][0];
						v[1] = node->cTriangleFactory.vertices[1][1];
						v[2] = 0.001;
						v += 3;

						v[0] = node->cTriangleFactory.vertices[2][0];
						v[1] = node->cTriangleFactory.vertices[2][1];
						v[2] = 0.001;
						v += 3;

						v -= 9;

						if (last_surface_visualization_method != 6 && last_surface_visualization_method != 7)
						{
							for (int i = 0; i < 3; i++)
							{
								p_applyTranslateAndScaleToVertex(v);
								v[1] = -v[1];
								std::swap(v[1], v[2]);
								v += 3;
							}
						}
						else
						{
							// determine face
							T average_vertex_x = (node->cTriangleFactory.vertices[0][0]+node->cTriangleFactory.vertices[1][0]+node->cTriangleFactory.vertices[2][0])*(T)(1.0/3.0);
							T average_vertex_y = (node->cTriangleFactory.vertices[0][1]+node->cTriangleFactory.vertices[1][1]+node->cTriangleFactory.vertices[2][1])*(T)(1.0/3.0);


							// determine face
							CCube_To_Sphere_Projection::EFaceType faceType =
									CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(average_vertex_x, average_vertex_y);

							// project to sphere
							for (int j = 0; j < 3; j++)
							{
								// project 2D coordinate to cube face
								CCube_To_Sphere_Projection::project2DTo2DFace(
										faceType,
										&v[0], &v[1]
									);

								// project to sphere
								CCube_To_Sphere_Projection::project2DFaceTo3D(
										faceType,
										v[0], v[1],
										&v[0], &v[1], &v[2]
									);

								v += 3;
							}
						}


						static const T normals[3][3] = {
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0},
								{0.0, 1.0, 0.0}
						};

						cGlDrawTrianglesWithVertexAndNormalArray.render(vertex_buffer, &(normals[0][0]), 3);
					}
				);

				CGlErrorCheck();

			cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();

			cCommonShaderPrograms.cBlinn.disable();
		}
#endif
	}



	/**
	 * render a smoothed surface using node based parallelization
	 */
	void p_render_surfaceSmooth(
			CShaderBlinn &cShaderBlinn
	)
	{
#if 0

#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0
	#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cShaderBlinn.use();
		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

		CHelper_GenericParallelVertexDataCommTraversals::action<
			CCluster_TreeNode_,
			CSimulationHyperbolic_Cluster,
			sierpi::kernels::COutputGridDataArrays_ContinousSurface::TRAV,
			CHyperbolicTypes::CVisualizationTypes::CVertexData,
			CStackAccessorMethodsTsunamiVertexData<CCluster_TreeNode_, CTsunamiSimulationTypes::TVisualizationVertexData>
		>(
			&CSimulationHyperbolic_Cluster::cOpenGL_Vertices_Smooth_Cell_Tsunami,
			&CSimulationHyperbolic_Cluster::cCluster_ExchangeVertexDataCommData,
			rootGenericTreeNode,
			[&](CCluster_TreeNode_ *node)
			{
#if 0
	// TODO
			node->cSimulation_Cluster->cOpenGL_Vertices_Smooth_Cell_Tsunami.cKernelClass.setup(
					visualization_water_surface_default_displacement, visualization_data0_scale_factor,
					&cGlDrawTrianglesWithVertexAndNormalArray
			);
#endif
			}
		);

		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
		cShaderBlinn.disable();
	#endif

#else

		cShaderBlinn.use();
		cGlDrawTrianglesWithVertexAndNormalArray.initRendering();

		cDomainClusters.traverse_GenericTreeNode_Serial(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				sierpi::kernels::COpenGL_Vertices_Cell_Tsunami<2>::TRAV cOpenGL_Vertices_Cell_Tsunami_aligned;

				cOpenGL_Vertices_Cell_Tsunami_aligned.setup_sfcMethods(node->cTriangleFactory);

				cOpenGL_Vertices_Cell_Tsunami_aligned.cKernelClass.setup(
						cParameters.visualization_water_surface_default_displacement,
						cParameters.visualization_data0_scale_factor,
						&cGlDrawTrianglesWithVertexAndNormalArray
					);

				cOpenGL_Vertices_Cell_Tsunami_aligned.action(node->cStacks);
			}
		);

		cGlDrawTrianglesWithVertexAndNormalArray.shutdownRendering();
		cShaderBlinn.disable();

#endif

#endif
	}

};


#endif /* CSIMULATION_HYPERBOLIC_PARALLEL_HPP_ */
