/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: March 08, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_
#define CSIMULATION_COMMON_ADAPTIVITY_ORDER_1_HPP_

#include <cmath>
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "simulations/hyperbolic_common/basis_functions_and_matrices/CDG_MatrixComputations.hpp"
#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "../libsierpi/triangle/CTriangle_PointProjections.hpp"
#include "../libsierpi/triangle/CTriangle_Tools.hpp"


namespace sierpi
{
namespace kernels
{

/**
 * adaptive refinement for single triangle elements
 * and adaptive coarsening for two triangle elements
 * and setting up a triangle element with corresponding data
 *
 * this class is responsible for applying the correct restriction/prolongation operator to the height and momentum
 * as well as initializing the correct bathymetry.
 *
 * No special setup - e. g. setting the height to a fixed value - is done in this class!
 */
class CCommon_Adaptivity_Order_N
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	typedef CHyperbolicTypes::CSimulationTypes::CNodeData CNodeData;

	// typedef for CAdaptivity_default_handlers (see include at end of this file)
	typedef CCommon_Adaptivity_Order_N CAdaptivity_Common;
#include "simulations/hyperbolic_common/subsimulation_generic/kernels/common/CAdaptivity_default_handlers.hpp"

	/**
	 * side-length of a cathetus of a square build out of two triangles
	 * this is necessary to compute the bathymetry dataset to be loaded
	 */
	T cathetus_side_length;

	/**
	 * callback for bathymetry data.
	 *
	 * set this to nullptr to use prolongation.
	 */
	CDatasets *cDatasets;


	/******************************************************
	 * CFL condition stuff
	 */

	/**
	 * typedef for CFL reduction value used by traversator
	 */
	typedef T TReduceValue;

	/**
	 * constructor
	 */
	CCommon_Adaptivity_Order_N()	:
		cathetus_side_length(-1),
		cDatasets(nullptr)
	{

	}


	void traversal_pre_hook()
	{
	}


	void traversal_post_hook()
	{
	}



	/**
	 * setup both refined elements
	 */
	inline void refine(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			const CSimulationCellData *i_cCellData,
			CSimulationCellData *o_left_cCellData,
			CSimulationCellData *o_right_cCellData
	)	{
		assert(i_cCellData != o_right_cCellData);

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
		o_left_cCellData->refine = false;
		o_left_cCellData->coarsen = false;

		o_right_cCellData->refine = false;
		o_right_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.testVertices(i_vertex_left_x, i_vertex_left_y, i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y);
#endif

		/*
		 * LEFT
		 */
		// H
		CDG_MatrixComputations_2D::mul_adaptivity_refine_left(i_cCellData->dofs.h, o_left_cCellData->dofs.h);

		// HU, HV
		CDG_MatrixComputations_2D::mul_adaptivity_refine_left(i_cCellData->dofs.hu, o_left_cCellData->dofs.hu);
		CDG_MatrixComputations_2D::mul_adaptivity_refine_left(i_cCellData->dofs.hv, o_left_cCellData->dofs.hv);
		CDG_MatrixComputations_2D::mul_adaptivity_project_momentum_reference_to_left_child(o_left_cCellData->dofs.hu, o_left_cCellData->dofs.hv);

		// B
		CDG_MatrixComputations_2D::mul_adaptivity_refine_left(i_cCellData->dofs.b, o_left_cCellData->dofs.b);


		/*
		 * RIGHT
		 */
		// H
		CDG_MatrixComputations_2D::mul_adaptivity_refine_right(i_cCellData->dofs.h, o_right_cCellData->dofs.h);

		// HU, HV
		CDG_MatrixComputations_2D::mul_adaptivity_refine_right(i_cCellData->dofs.hu, o_right_cCellData->dofs.hu);
		CDG_MatrixComputations_2D::mul_adaptivity_refine_right(i_cCellData->dofs.hv, o_right_cCellData->dofs.hv);
		CDG_MatrixComputations_2D::mul_adaptivity_project_momentum_reference_to_right_child(o_right_cCellData->dofs.hu, o_right_cCellData->dofs.hv);

		// B
		CDG_MatrixComputations_2D::mul_adaptivity_refine_right(i_cCellData->dofs.b, o_right_cCellData->dofs.b);


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		i_cCellData->validation.setupRefineLeftAndRight(
				i_normal_hyp_x,		i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_depth,
				&o_left_cCellData->validation,
				&o_right_cCellData->validation
			);
#endif


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE

		T mx = (i_vertex_left_x + i_vertex_right_x)*(T)0.5;
		T my = (i_vertex_left_y + i_vertex_right_y)*(T)0.5;

		o_left_cCellData->setupDistortedGrid(
				i_vertex_top_x,		i_vertex_top_y,
				i_vertex_left_x,	i_vertex_left_y,
				mx,					my,

				i_normal_left_x,	i_normal_left_y,
				i_normal_hyp_x,		i_normal_hyp_y,
				-i_normal_hyp_y,	i_normal_hyp_x
			);

		o_right_cCellData->setupDistortedGrid(
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,
				mx,					my,

				i_normal_right_x,	i_normal_right_y,
				i_normal_hyp_y,		-i_normal_hyp_x,
				i_normal_hyp_x,		i_normal_hyp_y
			);
#endif
	}




	/**
	 * setup coarsed elements
	 */
	inline void coarsen(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			int i_depth,

			CSimulationCellData *o_cCellData,
			CSimulationCellData *i_left_cCellData,
			CSimulationCellData *i_right_cCellData
	)	{

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
		o_cCellData->refine = false;
		o_cCellData->coarsen = false;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		{
			T mx = (i_vertex_left_x + i_vertex_right_x)*(T)0.5;
			T my = (i_vertex_left_y + i_vertex_right_y)*(T)0.5;

			i_left_cCellData->validation.testVertices(i_vertex_top_x, i_vertex_top_y, i_vertex_left_x, i_vertex_left_y, mx, my);
			i_right_cCellData->validation.testVertices(i_vertex_right_x, i_vertex_right_y, i_vertex_top_x, i_vertex_top_y, mx, my);
		}
#endif


		/*
		 * LEFT
		 */
		// H
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_left(i_left_cCellData->dofs.h, o_cCellData->dofs.h);

		// HU,HV
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_left(i_left_cCellData->dofs.hu, o_cCellData->dofs.hu);
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_left(i_left_cCellData->dofs.hv, o_cCellData->dofs.hv);
		CDG_MatrixComputations_2D::mul_adaptivity_project_momentum_left_child_to_reference(o_cCellData->dofs.hu, o_cCellData->dofs.hv);

		// B
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_left(i_left_cCellData->dofs.b, o_cCellData->dofs.b);



		/*
		 * RIGHT
		 */
		T o_cCellData_dofs_buf1[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];
		T o_cCellData_dofs_buf2[SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS];

		// H
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_right(i_right_cCellData->dofs.h, o_cCellData_dofs_buf1);

		// add to coarsened triangle
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.h[i] += o_cCellData_dofs_buf1[i];

		// HU, HV
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_right(i_right_cCellData->dofs.hu, o_cCellData_dofs_buf1);
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_right(i_right_cCellData->dofs.hv, o_cCellData_dofs_buf2);
		CDG_MatrixComputations_2D::mul_adaptivity_project_momentum_right_child_to_reference(o_cCellData_dofs_buf1, o_cCellData_dofs_buf2);

		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.hu[i] += o_cCellData_dofs_buf1[i];
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.hv[i] += o_cCellData_dofs_buf2[i];

		// B
		CDG_MatrixComputations_2D::mul_adaptivity_coarsen_right(i_right_cCellData->dofs.b, o_cCellData_dofs_buf2);
		for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS; i++)
			o_cCellData->dofs.b[i] += o_cCellData_dofs_buf2[i];


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_cCellData->validation.setupCoarsen(
				i_normal_hyp_x,		i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y,
				i_depth,
				&i_left_cCellData->validation,
				&i_right_cCellData->validation
		);
#endif


#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
		o_cCellData->setupDistortedGrid(
				i_vertex_left_x,	i_vertex_left_y,
				i_vertex_right_x,	i_vertex_right_y,
				i_vertex_top_x,		i_vertex_top_y,

				i_normal_hyp_x,		i_normal_hyp_y,
				i_normal_right_x,	i_normal_right_y,
				i_normal_left_x,	i_normal_left_y
			);
#endif
	}
};

}
}

#endif /* CADAPTIVITY_0STORDER_HPP_ */
