/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATION_OUTPUTGRIDDATAARRAYS_HPP_
#define CSIMULATION_OUTPUTGRIDDATAARRAYS_HPP_

#include "libsierpi/traversators/vertexCoords/CTraversator_VertexCoords_Normals_CellData.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "libmath/CVertex2d.hpp"
#include "libsierpi/CGridDataArrays.hpp"

namespace sierpi {
namespace kernels {

template <
	int t_vertices_per_cell		///< vertices to be stored per cell (3 for 2D, 2 for 1D)
>
class COutputGridDataArrays {
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData CCellData;

	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	typedef T TVertexScalar;
	typedef CVertex2d<TVertexScalar> TVertexType;

	typedef sierpi::travs::CTraversator_VertexCoords_Normals_CellData<
			COutputGridDataArrays,
			CHyperbolicTypes
		> TRAV;

	CMainThreading_Lock lock;

private:
	CGridDataArrays<t_vertices_per_cell,6> *cGridDataArrays;

	/**
	 * datasets to access benchmark data
	 */
	CDatasets *cDatasets;

	/**
	 * start index of cell
	 */
	size_t cell_write_index;

	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing;


public:
	inline void op_cell(
			T i_vertex_coord_left_x,	T i_vertex_coord_left_y,
			T i_vertex_coord_right_x,	T i_vertex_coord_right_y,
			T i_vertex_coord_top_x,		T i_vertex_coord_top_y,

			T i_normal_hyp_x,	T i_normal_hyp_y,
			T i_normal_right_x,	T i_normal_right_y,
			T i_normal_left_x,	T i_normal_left_y,

			CCellData *i_cCellData
	) {
		T *v;

		if (output_flags & CGridDataArrays_Enums::VERTICES)
		{
			v = &(cGridDataArrays->triangle_vertex_buffer[cell_write_index * 3 * 3]);

			v[0 * 3 + 0] = i_vertex_coord_left_x;
			v[0 * 3 + 1] = i_vertex_coord_left_y;
			v[0 * 3 + 2] = 0;
			v[1 * 3 + 0] = i_vertex_coord_right_x;
			v[1 * 3 + 1] = i_vertex_coord_right_y;
			v[1 * 3 + 2] = 0;
			v[2 * 3 + 0] = i_vertex_coord_top_x;
			v[2 * 3 + 1] = i_vertex_coord_top_y;
			v[2 * 3 + 2] = 0;
		}

		if (output_flags & CGridDataArrays_Enums::NORMALS)
		{
			T *n = &(cGridDataArrays->triangle_normal_buffer[cell_write_index * 3 * 3]);

			n[0*3+0] = 0;
			n[0*3+1] = 0;
			n[0*3+2] = 1;
			n[1*3+0] = 0;
			n[1*3+1] = 0;
			n[1*3+2] = 1;
			n[2*3+0] = 0;
			n[2*3+1] = 0;
			n[2*3+2] = 1;
		}

		/*
		 * H
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
	 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.h, &cGridDataArrays->dof_element[0][cell_write_index]);
		}

		/*
		 * MOMENTUM HU
		 * MOMENTUM HV
		 */
		if (output_flags & (CGridDataArrays_Enums::VALUE1 | CGridDataArrays_Enums::VALUE2))
		{
			T hu, hv;
	 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.hu, &hu);
	 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.hv, &hv);

			CTriangle_VectorProjections::referenceToWorld(&hu, &hv, -i_normal_right_x, -i_normal_right_y);

			if (output_flags & CGridDataArrays_Enums::VALUE1)
				cGridDataArrays->dof_element[1][cell_write_index] = hu;

			if (output_flags & CGridDataArrays_Enums::VALUE2)
				cGridDataArrays->dof_element[2][cell_write_index] = hv;
		}

		/*
		 * BATHYMETRY
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE3)
		{
			T output_value;
	 		CDG_MatrixComputations_2D::mul_convert_dofs_to_0th_order(i_cCellData->dofs.b, &output_value);

			if (cGridDataArrays->dof_variables_multiplier == 3)
			{
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 0] = output_value;
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 1] = output_value;
				cGridDataArrays->dof_element[3][cell_write_index * 3 + 2] = output_value;
			}
			else
			{
				cGridDataArrays->dof_element[3][cell_write_index] = output_value;
			}
		}



#if CONFIG_TSUNAMI_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		/*
		 * eigen coefficients
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE4)
		{
			cGridDataArrays->dof_element[4][cell_write_index] = i_cCellData->max_eigen_coefficient;
		}

#else
		/*
		 * cfl_domain_size_div_max_wave_speed
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE4)
		{
			cGridDataArrays->dof_element[4][cell_write_index] = 0;

			// TODO: fix me by setting up an additional scons compiler parameter
//			cGridDataArrays->dof_element[4][cell_write_index] = i_cCellData->cfl_domain_size_div_max_wave_speed;
		}
#endif

		/*
		 * benchmark datasets
		 */
		if (output_flags & CGridDataArrays_Enums::VALUE5)
		{
			CSimulationNodeData benchmark_data;

			T mx = (1.0/3.0) * (i_vertex_coord_left_x + i_vertex_coord_right_x + i_vertex_coord_top_x);
//			T my = (1.0/3.0) * (i_vertex_coord_left_y + i_vertex_coord_right_y + i_vertex_coord_top_y);

			assert(cDatasets != nullptr);
			cDatasets->getBenchmarkNodalData(mx, 0, 0, &benchmark_data);
			cGridDataArrays->dof_element[5][cell_write_index] = benchmark_data.h;
		}


		cell_write_index++;
	}

	inline void traversal_pre_hook() {
	}

	inline void traversal_post_hook() {
	}

	inline void setup(
			CGridDataArrays<t_vertices_per_cell,6> *o_GridDataArrays,
			CDatasets *i_cDatasets,
			size_t i_block_start_index,
			int i_output_flags,
			int i_preprocessing
	)
	{
		cGridDataArrays = o_GridDataArrays;
		cDatasets = i_cDatasets;
		cell_write_index = i_block_start_index;
		output_flags = i_output_flags;
		preprocessing = i_preprocessing;
	}

	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputGridDataArrays &parent_kernel) {
		assert(false);
	}
};


}
}

#endif
