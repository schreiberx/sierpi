/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#error "NOT AVAILABLE YET"

#ifndef COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_3PASS_1STPASS_HPP_
#define COUTPUTGRIDDATAARRAYS_CONTINOUS_SURFACE_3PASS_1STPASS_HPP_

#include "libsierpi/traversators/vertexData/CTraversator_VertexData_CellData_Depth_3Pass_1stPass.hpp"
#include "simulations/hyperbolic_common/subsimulation_tsunami/types/CTypes.hpp"

#include "../../../subsimulation_generic/types/CValidation_NodeData.hpp"
#include "../../../subsimulation_generic/types/CVisualizationVertexData.hpp"

#include "../../../subsimulation_generic/kernels/backends/COutputGridDataArrays.hpp"
#include "libsierpi/triangle/CTriangle_SideLengths.hpp"


namespace sierpi
{
namespace kernels
{



/**
 * The first pass to output a continuous surface is the computation of
 * averaged vertices at corner discontinuities.
 *
 * This creates the vertices initially
 */
class COutputGridDataArrays_ContinuousSurface_1stPass_Vertices
{
public:
	typedef typename CHyperbolicTypes::CSimulationTypes::CCellData		CCellData;
	typedef typename CHyperbolicTypes::CVisualizationTypes::T 			TVertexScalar;
	typedef typename CHyperbolicTypes::CVisualizationTypes::CNodeData	TVisualizationVertexData;
	typedef TVertexScalar	T;

	typedef sierpi::travs::CTraversator_VertexData_CellData_Depth<COutputGridDataArrays_ContinuousSurface_1stPass_Vertices, CHyperbolicTypes> TRAV;



private:
	/**
	 * which dofs, vertices and normals should be written?
	 */
	int output_flags;

	/**
	 * different visualization methods for higher order methods
	 */
	int preprocessing;



	/**
	 * operation due to parallelization:
	 * update vertex data with adjacent vertex data and store result to o_vertexData
	 */
public:
	inline void op_node_node_join_middle_touch(
			TVisualizationVertexData *i_vertexData1,	///< local vertex data
			TVisualizationVertexData *i_vertexData2,	///< adjacent vertex data
			TVisualizationVertexData *o_vertexData		///< local exchange vertex data for output
	)
	{
		o_vertexData->normal[0] = i_vertexData1->normal[0] + i_vertexData2->normal[0];
		o_vertexData->normal[1] = i_vertexData1->normal[1] + i_vertexData2->normal[1];
		o_vertexData->normal[2] = i_vertexData1->normal[2] + i_vertexData2->normal[2];

		o_vertexData->height = i_vertexData1->height + i_vertexData2->height;

		o_vertexData->normalization_factor = i_vertexData1->normalization_factor + i_vertexData2->normalization_factor;

		p_normalizeVertexData(o_vertexData);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		i_vertexData1->validation.testVertex(i_vertexData2->validation);

		o_vertexData->validation = i_vertexData1->validation;
#endif
	}



public:
	inline void op_node_node_join_middle_touch(
			const TVisualizationVertexData *i_vertexData_to_join,	///< adjacent vertex data
			TVisualizationVertexData *io_vertexData					///< local vertex data
	)
	{
#if DEBUG
		io_vertexData->validation.testVertex(i_vertexData_to_join->validation);
#endif

		io_vertexData->normal[0] += i_vertexData_to_join->normal[0];
		io_vertexData->normal[1] += i_vertexData_to_join->normal[1];
		io_vertexData->normal[2] += i_vertexData_to_join->normal[2];

		io_vertexData->height += i_vertexData_to_join->height;

		io_vertexData->normalization_factor += i_vertexData_to_join->normalization_factor;
	}



	/**
	 * final touch: normalize
	 */
public:
	inline void op_node_finishHimTouch(
			TVisualizationVertexData *io_vertexData		///< local exchange vertex data for output
	)
	{
		p_normalizeVertexData(io_vertexData);
	}



	/**
	 * first touch for vertex data
	 */
private:
	inline void p_actionFirstTouchVertexData(
		T vec_mx, T vec_my,		// vector from corner to midpoint
		int i_depth,
		CCellData *i_cellData,
		TVisualizationVertexData *o_vertexData
	) {
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		o_vertexData->height = elevation*l;
		o_vertexData->normalization_factor = l;

		p_computeNormal(vec_mx, vec_my, elevation, o_vertexData->normal);

#else
#error "not supported so far"
#endif
	}



	/**
	 * first touch for vertex data at left vertex
	 */
public:
	inline void op_node_first_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v0x, v0y);
#endif
	}



	/**
	 * first touch for vertex data at right vertex
	 */
public:
	inline void op_node_first_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v1x, v1y);
#endif
	}



	/**
	 * first touch for vertex data at top vertex
	 */
public:
	inline void op_node_first_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionFirstTouchVertexData(
				vec_mx, vec_my,
				depth, i_cellData, o_vertexData
			);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.setupVertex(v2x, v2y);
#endif
	}


	/**
	 * normalize vertex data after vertex data was written
	 */
private:
	inline void p_normalizeVertexData(
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0
		/*
		 * normalize
		 */
		TVertexScalar inv_len = ((TVertexScalar)1.0)/std::sqrt(io_vertexData->normal[0]*io_vertexData->normal[0]+io_vertexData->normal[1]*io_vertexData->normal[1]+io_vertexData->normal[2]*io_vertexData->normal[2]);
		io_vertexData->normal[0] *= inv_len;
		io_vertexData->normal[1] *= inv_len;
		io_vertexData->normal[2] *= inv_len;

		TVertexScalar inv_fac = (TVertexScalar)1.0/io_vertexData->normalization_factor;
		io_vertexData->height *= inv_fac;
#else
#error "not supported so far"
#endif
	}




private:
	/*
	 * middle touch vertex
	 */
	inline void p_actionMiddleTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;

		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->height += elevation*l;
		io_vertexData->normalization_factor += l;

		T normal[3];
		p_computeNormal(vec_mx, vec_my, elevation, normal);

		io_vertexData->normal[0] += normal[0];
		io_vertexData->normal[1] += normal[1];
		io_vertexData->normal[2] += normal[2];



#else
#error "not supported so far"
#endif
	}



	/*
	 * middle touch on left vertex
	 */
public:
	inline void op_node_middle_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}

	/*
	 * middle touch on right vertex
	 */
public:
	inline void op_node_middle_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}

	/*
	 * middle touch on top vertex
	 */
public:
	inline void op_node_middle_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionMiddleTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}


	/*
	 * last touch on vertex: normalize
	 */
private:
	inline void p_actionLastTouchVertexData(
			T vec_mx, T vec_my,		// vector from corner to midpoint
			int i_depth,
			CCellData *i_cellData,
			TVisualizationVertexData *io_vertexData
	)
	{
#if SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0

		T elevation;
		if (output_flags & CGridDataArrays_Enums::VALUE0)
		{
			elevation = i_cellData->dofs.h[0]+i_cellData->dofs.b[0];
		}
		else if (output_flags & CGridDataArrays_Enums::VALUE1)
		{
			elevation = i_cellData->dofs.b[0];
		}
		else
		{
			assert(false);
		}

		T l = CTriangle_SideLengths::getUnitCathetusLengthForDepth(i_depth);
		io_vertexData->height += elevation*l;
		io_vertexData->normalization_factor += l;


		T normal[3];
		p_computeNormal(vec_mx, vec_my, elevation, normal);

		io_vertexData->normal[0] += normal[0];
		io_vertexData->normal[1] += normal[1];
		io_vertexData->normal[2] += normal[2];

#else
#	error "not supported so far"
#endif

		p_normalizeVertexData(io_vertexData);
	}


	/*
	 * last touch on left vertex
	 */
public:
	inline void op_node_last_touch_left(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v0x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v0y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v0x, v0y);
#endif
	}


	/*
	 * last touch on right vertex
	 */
public:
	inline void op_node_last_touch_right(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v1x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v1y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v1x, v1y);
#endif
	}


	/*
	 * last touch on top vertex
	 */
public:
	inline void op_node_last_touch_top(
			T v0x, T v0y, T v1x, T v1y, T v2x, T v2y,
			int depth,
			CCellData *i_cellData,
			TVisualizationVertexData *o_vertexData
	)
	{
		T vec_mx = (v0x+v1x+v2x)*(T)(1.0/3.0) - v2x;
		T vec_my = (v0y+v1y+v2y)*(T)(1.0/3.0) - v2y;

		p_actionLastTouchVertexData(vec_mx, vec_my, depth, i_cellData, o_vertexData);

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		o_vertexData->validation.testVertex(v2x, v2y);
#endif
	}



	inline void traversal_pre_hook()
	{
	}


	inline void traversal_post_hook()
	{
	}


	inline void setup(
			int i_output_flags,
			int i_preprocessing
	) {
		output_flags = i_output_flags;
		preprocessing = i_preprocessing;
	}


	/**
	 * setup traversator based on parent triangle
	 */
	void setup_WithKernel(
			COutputGridDataArrays_ContinousSurface &parent_kernel
	) {
		assert(false);
	}
};


}
}

#endif
