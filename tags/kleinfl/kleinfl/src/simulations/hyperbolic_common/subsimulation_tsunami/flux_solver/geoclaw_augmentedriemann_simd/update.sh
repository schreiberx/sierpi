#! /bin/bash


FILES="
	AugRie.hpp
	AugRieCUDA.h
	AugRieGeoClaw.hpp
	AugRie_SIMD.hpp
	FWave.hpp
	FWaveCuda.h
	FWaveVec.hpp
	Hybrid.hpp
	SIMD_COSTS.hpp
	SIMD_DEFINITIONS.hpp
	SIMD_TYPES.hpp
	WavePropagation.hpp
"

for i in $FILES; do
	wget https://raw.github.com/the-wolf/bachelor-thesis/master/code/SWE/submodules/swe_solvers/src/solver/$i -O $i
done
