/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 12, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOW_HPP_
#define CDEFORMATIONAL_FLOW_HPP_


#include "libsierpi/grid/CCube_To_Sphere_Projection.hpp"
#include "libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"

#include "libmath/CMatrix.hpp"
#include "libmath/CVector.hpp"
#include "../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"
#include "../flux_solver/CFluxSolver_BenchmarkVelocityUpwinding.hpp"

#include "CGlobal.hpp"

extern CONFIG_DEFAULT_FLOATING_POINT_TYPE *simulation_timestamp_for_timestep;



class CDeformationalFlow
{
	typedef CHyperbolicTypes::CSimulationTypes::CCellData CCellData;
	typedef CHyperbolicTypes::CSimulationTypes::CEdgeData CEdgeData;
	static const int N = SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE;

public:

	enum EEdge_Type {
		E_ERROR = -1,
		E_HYP = 0,
		E_RIGHT = 1,
		E_LEFT = 2
	};

	/**
	 * computes the velocity in lat/lon coordinates as defined by the
	 * deformational flow benchmark paper
	 */
	template <typename T>
	inline static void compute_latlonVelocity(
			T i_s_lat,
			T i_s_lon,
			T *o_u_lat,
			T *o_u_lon
	){
		T k = (T)0.0024 * (T)0.0001;
//		k = (T)2.4;
		T max_t = 40000;
		T t = 0;
		CGlobal& g = CGlobal::getInstance();
//		t = g.getTS();
		switch(g.getBenchmarkType()){
		case CGlobal::E_DEF_FLOW_CASE1:
			i_s_lon = (T)M_PI + i_s_lon;
			*o_u_lon = k * sin(i_s_lon * (T)0.5) * sin(i_s_lon * (T)0.5) * sin(i_s_lat * (T)2) * cos(M_PI * t / max_t);
			*o_u_lat = k * (T)0.5 * sin(i_s_lon) * cos(i_s_lat) * cos(M_PI * t / max_t);
			break;
//#if CONFIG_DEFORMATIONAL_FLOW==2
		case CGlobal::E_DEF_FLOW_LON_ONLY:
			*o_u_lon = k * cos(i_s_lat); //u 0 cos θ (cos φ + sin φ cos λ)
			*o_u_lon = k; //−u 0 sin φ sin λ
			*o_u_lat = 0;
			break;
//		T rot_angle = 0;
//		T u_0 = (T)2 * M_PI * EARTH_RADIUS_IN_METERS / max_t;
//		*o_u_lon = u_0 * cos(i_s_lat);// * (cos(rot_angle) + sin(rot_angle) * cos(i_s_lon));
//		*o_u_lat = 0;//-u_0 * sin(rot_angle) * sin(i_s_lon);
//#endif
		}
	}


	template <typename T>
	inline static void edge_center(
			const T i_point1[3], const T i_point2[3],
			T *o_x, T *o_y, T *o_z
	){
		*o_x = (i_point1[0] + i_point2[0]) / (T)2;
		*o_y = (i_point1[1] + i_point2[1]) / (T)2;
		*o_z = (i_point1[2] + i_point2[2]) / (T)2;
	}


	template <typename T>
	inline static void element_centroid(
			const T i_coordinates3D[3][3],
			T *o_x, T *o_y, T *o_z
	){
		// get triangle's centroid in 3D
		*o_x = (i_coordinates3D[0][0] + i_coordinates3D[1][0] + i_coordinates3D[2][0]) / (T)3;
		*o_y = (i_coordinates3D[0][1] + i_coordinates3D[1][1] + i_coordinates3D[2][1]) / (T)3;
		*o_z = (i_coordinates3D[0][2] + i_coordinates3D[1][2] + i_coordinates3D[2][2]) / (T)3;
	}


	template <typename T>
	inline static void vectorlength(T x, T y, T z, T *len){
		*len = std::sqrt(x*x + y*y + z*z);
	}


	/**
	 * computes the benchmark velocity components to be stored in the
	 * triangle's edge data
	 */
	template <typename T>
	inline static void compute_worldspaceVelocity(
			T i_center_x, T i_center_y, T i_center_z,
			T *o_velocity_x,
			T *o_velocity_y,
			bool print,
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			const CCellData *i_cellData,
			EEdge_Type i_edge_type
	){
		T s_lat, s_lon;
		T s_x = i_center_x; T s_y = i_center_y; T s_z = i_center_z;

		// centroid in lat/lon
		CCube_To_Sphere_Projection::project3DToLL(s_x, s_y, s_z,
				&s_lat, &s_lon);

		// get lat/lon velocity for centroid
		T u_lat, u_lon;
		compute_latlonVelocity(s_lat, s_lon, &u_lat, &u_lon);

		// endpoint of velocity vector in lat/lon
		T u_vertex_lat, u_vertex_lon;
		u_vertex_lat = s_lat + u_lat;
		u_vertex_lon = s_lon + u_lon;

		// endpoint of velocity vector in 3D
		T u_x, u_y, u_z;
		CCube_To_Sphere_Projection::projectLLTo3D(u_vertex_lat, u_vertex_lon, &u_x, &u_y, &u_z);
//		T u2_lat, u2_lon;
//		CCube_To_Sphere_Projection::project3DToLL(u_x, u_y, u_z, &u2_lat, &u2_lon);
//		T s2_x, s2_y, s2_z;
//		CCube_To_Sphere_Projection::projectLLTo3D(s_lat, s_lon, &s2_x, &s2_y, &s2_z);


		// rotate 3d velocity vector to triangle's plane -> left edge on x-axis, left edge normal on y-axis
		CMatrix3<T> M_inv = CMatrix3<T>(
				i_cellData->projectionMatrix3DTo2D[0][0], i_cellData->projectionMatrix3DTo2D[0][1], i_cellData->projectionMatrix3DTo2D[0][2],
				i_cellData->projectionMatrix3DTo2D[1][0], i_cellData->projectionMatrix3DTo2D[1][1], i_cellData->projectionMatrix3DTo2D[1][2],
				i_cellData->projectionMatrix3DTo2D[2][0], i_cellData->projectionMatrix3DTo2D[2][1], i_cellData->projectionMatrix3DTo2D[2][2]);
		T u2x, u2y, u2z;
#if 0
		T alpha, ulen, slen, difflen;
		vectorlength(u_x, u_y, u_z, &ulen);
		vectorlength(s_x, s_y, s_z, &slen);
		vectorlength(u_x-s_x, u_y-s_y, u_z-s_z, &difflen);
		alpha = acos((ulen*ulen + slen*slen - difflen*difflen) / ((T)2 * slen * ulen));
		T newlen = slen / cos(alpha);
		u2x = u_x * newlen / ulen;
		u2y = u_y * newlen / ulen;
		u2z = u_z * newlen / ulen;
#else
		u2x = u_x;
		u2y = u_y;
		u2z = u_z;
#endif
#if 1
		CCube_To_Sphere_Projection::projectLLTo3D(s_lat, s_lon, &s_x, &s_y, &s_z);
#endif

//		CVector<3, T> u_vector = CVector<3, T>(u_x-i_coordinates3D[2][0], u_y-i_coordinates3D[2][1], u_z-i_coordinates3D[2][2]);
//		CVector<3, T> us_vector = CVector<3, T>(u_x-s_x, u_y-s_y, u_z-s_z);
		CVector<3, T> us_vector = CVector<3, T>(u2x-s_x, u2y-s_y, u2z-s_z);
		CVector<3, T> flat_us_vector = M_inv * us_vector;

		// diff u-s in x/y coordinates
		T us_xx, us_yy;
		us_xx = flat_us_vector[0];
		us_yy = flat_us_vector[1];
#if CONFIG_ELEMENT_REFERENCE_SPACE
		CTriangle_VectorProjections::matrixTransformation(i_cellData->transformationMatrix, &us_xx, &us_yy);
#else
#endif


		// set output
//		*o_velocity_x = us_x;
//		*o_velocity_y = us_y;
#if 1
#if CONFIG_ELEMENT_REFERENCE_SPACE
		*o_velocity_x = us_xx * 1000000; //EARTH_RADIUS_IN_METERS;
		*o_velocity_y = us_yy * 1000000; //EARTH_RADIUS_IN_METERS;
#else
		*o_velocity_x = us_xx * EARTH_RADIUS_IN_METERS;// * 1000000; //EARTH_RADIUS_IN_METERS;
		*o_velocity_y = us_yy * EARTH_RADIUS_IN_METERS;// * 1000000; //EARTH_RADIUS_IN_METERS;
#endif
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID!=10
		*o_velocity_x = us_xx * 100; //EARTH_RADIUS_IN_METERS;
		*o_velocity_y = us_yy * 100; //EARTH_RADIUS_IN_METERS;
#endif
#else
		*o_velocity_x = u_lat;
		*o_velocity_y = u_lon;
		CTriangle_VectorProjections::referenceToWorld(o_velocity_x, o_velocity_y, -i_right_normal_x, -i_right_normal_y);
#endif


		/*
		 * print debug info...
		 */
		bool forceprint = false;
//		forceprint = slatdeg >= 37.4971232571447 && slatdeg <= 37.4971232571449 && slondeg >= 51.6359359187896 && slondeg <= 51.6359359187898;

		T latdeg = u_vertex_lat * (T)180 / M_PI;
		T londeg = u_vertex_lon * (T)180 / M_PI;
		T slatdeg = s_lat * (T)180 / M_PI;
		T slondeg = s_lon * (T)180 / M_PI;
		T ulatdeg = u_lat * (T)180 / M_PI;
		T ulondeg = u_lon * (T)180 / M_PI;
		if(print || forceprint){
			std::cout << std::endl;
			std::cout << "s lat/lon deg: " << slatdeg << ", " << slondeg << std::endl;
			std::cout << "u lat/lon deg: " << latdeg << ", " << londeg << std::endl;
			std::cout << "diff lat/lon deg: " << ulatdeg << ", " << ulondeg << std::endl;
//			std::cout << "s lat/lon: " << s_lat << ", " << s_lon << std::endl;
//			std::cout << "u lat/lon: " << u_vertex_lat << ", " << u_vertex_lon << std::endl;
//			std::cout << "diff lat/lon: " << u_lat << ", " << u_lon << std::endl;

//			std::cout << "u2 lat/lon: " << u2_lat << ", " << u2_lon << std::endl;
//			std::cout << "s2 x/y/z: " << s2_x << ", " << s2_y << ", " << s2_z << std::endl;
			std::cout << std::endl;
			std::cout << "s x/y/z: " << s_x << ", " << s_y << ", " << s_z << std::endl;
			std::cout << "u x/y/z: " << u_x << ", " << u_y << ", " << u_z << std::endl;
			std::cout << "diff x/y/z: " << u_x-s_x << ", " << u_y-s_y << ", " << u_z-s_z << std::endl;
			std::cout << std::endl;

//			std::cout << us_vector << std::endl;
//			std::cout << flat_us_vector << std::endl;
//			std::cout << "u x/y/z: " << u2x << ", " << u2y << ", " << u2z << std::endl;
			std::cout << "u-s flat x/y/z: " << flat_us_vector[0] << ", " << flat_us_vector[1] << ", " << flat_us_vector[2] << std::endl;
//			std::cout << "trans matrix: (" << i_cellData->transformationMatrix[0][0] << ", " << i_cellData->transformationMatrix[0][1] <<
//					"), (" << i_cellData->transformationMatrix[1][0] << ", " << i_cellData->transformationMatrix[1][1] << ")" << std::endl;
//			std::cout << "M_inv: " << M_inv << std::endl;
//			CMatrix3<T> M = M_inv.getInverse();
//			std::cout << "M: " << M << std::endl;
//			std::cout << "u-s ref x/y: " << us_xx << ", " << us_yy << std::endl;

			std::cout << std::endl;
			T dummy;
			outputFluxData(i_cellData, i_edge_type, dummy);
		}
	}


	/**
	 * reproduces the rotations to edge space (and flux computations)
	 * to print out and compare edge space vectors
	 */
	template <typename T>
	inline static void outputFluxData(
			const CCellData *i_cellData,
			EEdge_Type i_edge_type,
			T dummy
	){

		CEdgeData o_left, o_right, o_hyp;
		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.h, o_hyp.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.hu, o_hyp.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_hyp_face(i_cellData->dofs.hv, o_hyp.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->hypEdgeRotationMatrix, o_hyp.dofs.hu, o_hyp.dofs.hv);

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.h, o_right.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.hu, o_right.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_right_face(i_cellData->dofs.hv, o_right.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->rightEdgeRotationMatrix, o_right.dofs.hu, o_right.dofs.hv);

		// H
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.h, o_left.dofs.h);
		// HU, HV
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.hu, o_left.dofs.hu);
		CDG_MatrixComputations_2D::mul_timestep_dofs_to_left_face(i_cellData->dofs.hv, o_left.dofs.hv);
		CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace(i_cellData->leftEdgeRotationMatrix, o_left.dofs.hu, o_left.dofs.hv);

		CSimulationNodeDataSOA<1> hyp_flux_l, hyp_flux_r, right_flux_l, right_flux_r, left_flux_l, left_flux_r;
		T dummy1, dummy2, dummy3;
		CSimulationNodeDataSOA<1> hyp_next, left_next, right_next;
		hyp_next.h[0] = o_hyp.dofs.h[0];
		hyp_next.hu[0] = -o_hyp.dofs.hu[0];
		hyp_next.hv[0] = -o_hyp.dofs.hv[0];
		left_next.h[0] = o_left.dofs.h[0];
		left_next.hu[0] = -o_left.dofs.hu[0];
		left_next.hv[0] = -o_left.dofs.hv[0];
		right_next.h[0] = o_right.dofs.h[0];
		right_next.hu[0] = -o_right.dofs.hu[0];
		right_next.hv[0] = -o_right.dofs.hv[0];
		bool printflux = false;
		if (i_cellData->face == 0 && i_cellData->vertices[1][0] == -1 && i_cellData->vertices[1][1] == -1){
			hyp_next.hu[0] = -0.0158240670528181;//-o_hyp.dofs.hu[0];
			hyp_next.hv[0] = 0.00696202910753179;//-o_hyp.dofs.hv[0];
			left_next.hu[0] = 0.0102267034562117;//-o_left.dofs.hu[0];
			left_next.hv[0] = -0.0140221695980423;//-o_left.dofs.hv[0];
			right_next.hu[0] = 0.0139478204848079;//-o_right.dofs.hu[0];
			right_next.hv[0] = 0.010289588321936;//-o_right.dofs.hv[0];
			printflux = true;
		}
		CFluxSolver_BenchmarkVelocityUpwinding<T> fluxsolver;
		fluxsolver.op_edge_edge<1>(
				o_hyp.dofs, hyp_next,
				&hyp_flux_l, &hyp_flux_r,
				&dummy1, &dummy2, dummy3
				);
		fluxsolver.op_edge_edge<1>(
				o_left.dofs, left_next,
				&left_flux_l, &left_flux_r,
				&dummy1, &dummy2, dummy3
				);
		fluxsolver.op_edge_edge<1>(
				o_right.dofs, right_next,
				&right_flux_l, &right_flux_r,
				&dummy1, &dummy2, dummy3
				);

		T ehu_left = left_flux_l.h[0] * i_cellData->edgeLength[2];
		T ehu_right = right_flux_l.h[0] * i_cellData->edgeLength[1];
		T ehu_hyp = hyp_flux_l.h[0] * i_cellData->edgeLength[0];
		T ehu_sum = ehu_left + ehu_right + ehu_hyp;

		switch(i_edge_type){
		case E_ERROR:
			std::cout << "u edge left: " << o_left.dofs.hu[0] << ", " << o_left.dofs.hv[0] << std::endl;
			std::cout << "u edge right: " << o_right.dofs.hu[0] << ", " << o_right.dofs.hv[0] << std::endl;
			std::cout << "u edge hyp: " << o_hyp.dofs.hu[0] << ", " << o_hyp.dofs.hv[0] << std::endl;
			break;
		case E_HYP:
			std::cout << "u edge hyp: " << o_hyp.dofs.hu[0] << ", " << o_hyp.dofs.hv[0] << std::endl;
			break;
		case E_LEFT:
			std::cout << "u edge left: " << o_left.dofs.hu[0] << ", " << o_left.dofs.hv[0] << std::endl;
			break;
		case E_RIGHT:
			std::cout << "u edge right: " << o_right.dofs.hu[0] << ", " << o_right.dofs.hv[0] << std::endl;
			break;
		}
		if(printflux){
			std::cout << "flux left: h " << left_flux_l.h[0] << ", hu " << left_flux_l.hu[0] <<
					", ehu " << ehu_left << std::endl;
			std::cout << "flux right: h " << right_flux_l.h[0] << ", hu " << right_flux_l.hu[0] <<
					", ehu " << ehu_right << std::endl;
			std::cout << "flux hyp: h " << hyp_flux_l.h[0] << ", hu " << hyp_flux_l.hu[0] <<
					", ehu " << ehu_hyp << std::endl;
			std::cout << "flux sum: ehu " << ehu_sum << ", ehu/area " << ehu_sum/i_cellData->cellArea << std::endl;
		}
	}


	/**
	 * overwrite dofs with values following the
	 * deformational flow benchmark formulas
	 */
	template <typename T>
	inline static void overwrite_dofs_element(
			T io_u[N],
			T io_v[N],
//			T i_coordinates3D[3][3],
			CCellData *i_cellData,
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y	///< normals for left edge (necessary for back-rotation of element)
	){
		T velocity_x, velocity_y;
#if CONFIG_DEFORMATIONAL_FLOW>0
		T s_x, s_y, s_z;
		element_centroid(i_cellData->coordinates3D, &s_x, &s_y, &s_z);

		compute_worldspaceVelocity(s_x, s_y, s_z, &velocity_x, &velocity_y, false, i_right_normal_x, i_right_normal_y, i_cellData, E_ERROR);
#endif

#if CONFIG_DEFORMATIONAL_FLOW==-1
		velocity_x = 0.1;
		velocity_y = 0.0;
		CTriangle_VectorProjections::worldToReference(&velocity_x, &velocity_y, -i_right_normal_x, -i_right_normal_y);
//		CTriangle_VectorProjections::toEdgeSpace<i_edge_type>(velocity_x, velocity_y);
#endif

		for (int i=0; i<=N; i++){
			io_u[i] = velocity_x;
			io_v[i] = velocity_y;
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID!=10
			io_u[i] = velocity_x * (i_cellData->dofs.h[i]);
			io_v[i] = velocity_y * (i_cellData->dofs.h[i]);
#endif
			// 2.4 * sin(pi/2 * 0.5) * sin(pi/2 / 2) * sin(pi/4 * 2) * cos(pi * 0)
			// 2.4 * 0.5 * sin(pi/2) * cos(pi/4) * cos(pi * 0)
		}
//		if(slatdeg >= 37.4971232571447 && slatdeg <= 37.4971232571449 && slondeg >= 51.6359359187896 && slondeg <= 51.6359359187898)
//			std::cout << "u: " << io_u[0] << ", v: " << io_v[0] << std::endl;
	}


	/**
	 * overwrite dofs with values following the
	 * deformational flow benchmark formulas
	 */
	template <typename T>
	inline static void overwrite_dofs_edge(
			T io_u[N],
			T io_v[N],
//			const T i_coordinates3D[3][3],
			const CCellData *i_cellData,
			T i_hyp_normal_x,	T i_hyp_normal_y,	///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,	///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y,	///< normals for left edge (necessary for back-rotation of element)
			EEdge_Type i_edge_type	// 0: hyp, 1: right, 2:left
	){
		T velocity_x, velocity_y;
#if CONFIG_DEFORMATIONAL_FLOW>0
		T s_x, s_y, s_z;
		edge_center(i_cellData->coordinates3D[i_edge_type], i_cellData->coordinates3D[(i_edge_type+1)%3], &s_x, &s_y, &s_z);

		compute_worldspaceVelocity(s_x, s_y, s_z, &velocity_x, &velocity_y, false, i_right_normal_x, i_right_normal_y, i_cellData, i_edge_type);
#endif

#if CONFIG_DEFORMATIONAL_FLOW==-1
		velocity_x = 0.1;
		velocity_y = 0.0;
		CTriangle_VectorProjections::worldToReference(&velocity_x, &velocity_y, -i_right_normal_x, -i_right_normal_y);
//		CTriangle_VectorProjections::toEdgeSpace<i_edge_type>(velocity_x, velocity_y);
#endif

		for (int i=0; i<=N; i++){
			io_u[i] = velocity_x;
			io_v[i] = velocity_y;
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID!=10
			io_u[i] = velocity_x * (i_cellData->dofs.h[i]);
			io_v[i] = velocity_y * (i_cellData->dofs.h[i]);
#endif
		}
	}

};

#endif



