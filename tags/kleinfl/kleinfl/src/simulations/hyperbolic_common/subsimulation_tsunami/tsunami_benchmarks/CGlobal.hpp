/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 12, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 *
 *
 */


#ifndef CDEFORMATIONAL_FLOWX_HPP_
#define CDEFORMATIONAL_FLOWX_HPP_


#include "CSimulation_MainParameters.hpp"
#include "../../CParameters.hpp"

//#include "CMain.hpp"

//extern CONFIG_DEFAULT_FLOATING_POINT_TYPE simulation_timestamp_for_timestep;



class CGlobal
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	enum EBenchmark_Type {
		E_2D = -1,
		E_DEF_FLOW_CASE1 = 1,
		E_DEF_FLOW_LON_ONLY = 2
	};
	T simulation_timestamp_for_timestep;
	CSimulation_MainParameters *cSimulation_MainParameters;
	EBenchmark_Type benchmark_type;

	void setTS(T ts){
//		simulation_timestamp_for_timestep = ts;
//		cSimulation_MainParameters = ts;
		simulation_timestamp_for_timestep = ts;
//		std::cout << ts << std::endl;
	}
	T getTS(){
		return simulation_timestamp_for_timestep;
	}

	void setBenchmarkType(int i_benchmark_type){
		benchmark_type = EBenchmark_Type(i_benchmark_type);
	}
	EBenchmark_Type getBenchmarkType(){
		return benchmark_type;
	}

    static CGlobal& getInstance()
    {
        static CGlobal    instance; // Guaranteed to be destroyed.
                              // Instantiated on first use.
        return instance;
    }
private:
    CGlobal() {};                   // Constructor? (the {} brackets) are needed here.
    // Dont forget to declare these two. You want to make sure they
    // are unaccessable otherwise you may accidently get copies of
    // your singleton appearing.
    CGlobal(CGlobal const&);              // Don't Implement
    void operator=(CGlobal const&); // Don't implement
};

#endif



