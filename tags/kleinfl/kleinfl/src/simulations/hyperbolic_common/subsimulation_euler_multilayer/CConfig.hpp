/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 26, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHYPERBOLIC_CONFIG_EULER_MULTILAYER_HPP_
#define CHYPERBOLIC_CONFIG_EULER_MULTILAYER_HPP_

#include "libsierpi/grid/CBoundaryConditions.hpp"


/*
 * 0: polytropic
 * 1: isothermal
 */
#define CONFIG_EULER_GAS_MODEL	1


#if CONFIG_EULER_GAS_MODEL==0

	/**
	 * see Leveque, FV Methods, 14.4 polytropic gas, page 295
	 */
	#define CONFIG_EULER_GAS_ALPHA	(3)
	#define CONFIG_EULER_GAS_GAMMA	(5.0/3.0)

#elif CONFIG_EULER_GAS_MODEL==1

	/**
	 * see Leveque, FV Methods, 14.6 isothermal flow, page 298
	 */
	#define CONFIG_EULER_GAS_A_SQUARED	(0.01)

#endif


#define SIMULATION_EULER_ZERO_THRESHOLD	(0.0001)


#define SIMULATION_DATASET_SIZE	5

// density
#define SIMULATION_DATASET_DEFAULT_VALUE_0	(1)
// rv
#define SIMULATION_DATASET_DEFAULT_VALUE_1	(0)
// ru
#define SIMULATION_DATASET_DEFAULT_VALUE_2	(0)
// rw
#define SIMULATION_DATASET_DEFAULT_VALUE_3	(0)
// energy
#define SIMULATION_DATASET_DEFAULT_VALUE_4	(1)

#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_X		(0)
#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Y		(0)
#define DEFAULT_PARAMETER_VISUALIZATION_TRANSLATE_Z		(-1)

#define DEFAULT_PARAMETER_VISUALIZATION_SCALE_X		(1)
#define DEFAULT_PARAMETER_VISUALIZATION_SCALE_Y		(1)
#define DEFAULT_PARAMETER_VISUALIZATION_SCALE_Z		(1)

#define DEFAULT_PARAMETER_VISUALIZATION_DATA0_SCALE		(1)
#define DEFAULT_PARAMETER_VISUALIZATION_DATA1_SCALE		(1.0/10.0)

/*
 * default boundary condition
 */
#define DEFAULT_BOUNDARY_CONDITION	(BOUNDARY_CONDITION_DIRICHLET)


#if SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE == 1

#	define SIMULATION_REFINE_PARAMETER_0	(1.01)
#	define SIMULATION_REFINE_PARAMETER_1	(1.01)

#	define SIMULATION_COARSEN_PARAMETER_0	(1.001)
#	define SIMULATION_COARSEN_PARAMETER_1	(1.001)

#else

#	error "invalid adaptivity mode!"

#endif




#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==0
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_STRING		"lax friedrich"
#endif
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID==1
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_STRING		"rusanov"
#endif



#endif /* CCONFIGEULER_HPP_ */
