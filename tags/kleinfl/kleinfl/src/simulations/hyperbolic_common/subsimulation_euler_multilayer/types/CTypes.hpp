/*
 * CTypes.hpp
 *
 *  Created on: Aug 24, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CEULER_MULTILAYERTYPES_HPP_
#define CEULER_MULTILAYERTYPES_HPP_

#include "libsierpi/parallelization/CGlobalComm.hpp"
#include <vector>
#include <iostream>
#include <limits>

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"
#include "libsierpi/stacks/CSimulationStacks.hpp"

#include "../../subsimulation_generic/types/CValidation_EdgeData.hpp"
#include "../../subsimulation_generic/types/CValidation_CellData.hpp"
#include "../../basis_functions_and_matrices/CDG_MatrixComputations.hpp"
#include "../libsierpi/grid/CCube_To_Sphere_Projection.hpp"

/**
 * Degree of freedoms for one point in simulation
 */
class CSimulationNodeData
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T r;	// density
	T ru;	// x-component of momentum
	T rv;	// y-component of momentum
	T rw;	// y-component of momentum
	T e;					// energy

	// TODO: maybe this variable can be removed
	T cfl1_timestep_size;	// helper variable to determine the maximum allowed timestep size


	/**
	 * return simulation specific benchmark data
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = r;
	}

	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeData &d
	 )	{
		os << "CSimulationNodeData:" << std::endl;
		os << " + r: " << d.r << std::endl;
		os << " + ru: " << d.ru << std::endl;
		os << " + rv: " << d.rv << std::endl;
		os << " + rw: " << d.rw << std::endl;
		os << " + e: " << d.e << std::endl;
		os << " + cfl1_timestep_size: " << d.cfl1_timestep_size << std::endl;
		os << " + vx: " << d.ru/d.r << std::endl;
		os << " + vy: " << d.rv/d.r << std::endl;
		os << " + vz: " << d.rw/d.r << std::endl;
		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,		///< cout
			T x_axis_x,				///< x-axis of new basis (x-component)
			T x_axis_y				///< x-axis of new basis (y-component)
	) const
	{
		os << "Euler DOF Information:" << std::endl;
		os << " + r: " << r << std::endl;

		T tru = ru;
		T trv = rv;
		CTriangle_VectorProjections::referenceToWorld(&tru, &trv, x_axis_x, x_axis_y);

		os << " + ru: " << tru << std::endl;
		os << " + rv: " << trv << std::endl;
		os << " + rw: " << rw << std::endl;

		os << " + e: " << e << std::endl;
		os << " + cfl1_timestep_size: " << cfl1_timestep_size << std::endl;

		os << " + vx: " << tru/r << std::endl;
		os << " + vy: " << trv/r << std::endl;
		os << " + vz: " << rw/r << std::endl;

		os << std::endl;
	}
};






/**
 * Degree of freedoms for non-layered DOFs
 */
template <int N>
class CSimulationNodeDataSingleLayerSOA
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	class
	{
	public:
		T r[N];		///< density
		T ru[N];	///< x-component of momentum
		T rv[N];	///< y-component of momentum
		T rw[N];	///< z-component of momentum

		T e[N];		///< energy
	} dofs;


	/**
	 * return simulation specific benchmark data
	 *
	 * for simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = 0;
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultCellValues(
			T default_values[5]
	)
	{
		CSimulationNodeDataSingleLayerSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS> poly;

		poly.dofs.r[0]	= default_values[0];
		poly.dofs.ru[0]	= default_values[1];
		poly.dofs.rv[0]	= default_values[2];
		poly.dofs.rw[0]	= default_values[3];
		poly.dofs.e[0]	= default_values[4];

		for (int i = 1; i < N; i++)
		{
			poly.dofs.r[i] = 0;
			poly.dofs.ru[i] = 0;
			poly.dofs.rv[i] = 0;
			poly.dofs.rw[i] = 0;
			poly.dofs.r[i] = 0;
		}

		CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.r, this->dofs.r);
		CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.ru, this->dofs.ru);
		CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.rv, this->dofs.rv);
		CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.rw, this->dofs.rw);
		CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.e, this->dofs.e);
	}

	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultEdgeValues(T default_values[5])
	{
		for (int i = 0; i < N; i++)
		{
			dofs.r[i] = default_values[0];
			dofs.ru[i] = default_values[1];
			dofs.rv[i] = default_values[2];
			dofs.rw[i] = default_values[3];
			dofs.e[i] = default_values[4];
		}
	}



	static inline void outputVector(
			std::ostream &os,					///< output stream
			const T *v,
			int i_size
	)
	{
		os << "[";
		for (int i = 0; i < i_size-1; i++)
			os << " " << v[i];
		os << " " << v[i_size-1] << " ]" << std::endl;
	}



	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeDataSingleLayerSOA &d
	 )	{
		os << "   + r:";
		outputVector(os, d.dofs.r, N);
		os << "   + ru:";
		outputVector(os, d.dofs.ru, N);
		os << "   + rv:";
		outputVector(os, d.dofs.rv, N);
		os << "   + rw:";
		outputVector(os, d.dofs.rw, N);

		os << "   + e/max_wave_speed:";
		outputVector(os, d.dofs.e, N);

		os << std::endl;

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,	///< cout
			T x_axis_x,			///< x-axis of new basis (x-component)
			T x_axis_y			///< x-axis of new basis (y-component)
	) const
	{
		os << "CSimulationNodeDataSOA:" << std::endl;

		os << "   + r:";
		outputVector(os, dofs.r, N);

		T wru[N];
		T wrv[N];

		for (int i = 0; i < N; i++)
		{
			wru[i] = dofs.ru[i];
			wrv[i] = dofs.rv[i];
			CTriangle_VectorProjections::referenceToWorld(&wru[i], &wrv[i], x_axis_x, x_axis_y);
		}

		os << "   + ru:";
		outputVector(os, wru, N);
		os << "   + rv:";
		outputVector(os, wrv, N);
		os << "   + rw:";
		outputVector(os, dofs.rw, N);

		os << "   + e/max_wave_speed:";
		outputVector(os, dofs.e, N);

		os << std::endl;
	}
};




/**
 * Degree of freedoms for layered DOFs
 */
template <int N>
class CSimulationNodeDataLayeredSOA
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	class CSingleLayer
	{
	public:
		T r[N];		// height
		T ru[N];	// x-component of momentum
		T rv[N];	// y-component of momentum
		T rw[N];	// y-component of momentum

		T e[N];		// energy
	};

	/**
	 * layers
	 */
	CSingleLayer layers[SIMULATION_NUMBER_OF_LAYERS];


	/**
	 * return simulation specific benchmark data
	 *
	 * for simulation, this is b+h only
	 */
	void getSimulationSpecificBenchmarkData(std::vector<T> *o_data)
	{
		o_data->resize(1);
		(*o_data)[0] = 0;
	}


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultCellValues(
			T default_values[5]
	)
	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{

			CSimulationNodeDataSingleLayerSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS> poly;

			poly.dofs.r[0] = default_values[0];
			poly.dofs.ru[0] = default_values[1];
			poly.dofs.rv[0] = default_values[2];
			poly.dofs.rw[0] = default_values[3];
			poly.dofs.e[0] = default_values[4];

			for (int i = 1; i < N; i++)
			{
				poly.dofs.r[i] = 0;
				poly.dofs.ru[i] = 0;
				poly.dofs.rv[i] = 0;
				poly.dofs.rw[i] = 0;
				poly.dofs.r[i] = 0;
			}

			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.r, this->layers[j].r);
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.ru, this->layers[j].ru);
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.rv, this->layers[j].rv);
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.rw, this->layers[j].rw);
			CDG_MatrixComputations_3D::mul_convert_poly_to_dofs(poly.dofs.e, this->layers[j].e);
		}
	}

	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultEdgeValues(T default_values[5])
	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			for (int i = 0; i < N; i++)
			{
				layers[j].r[i] = default_values[0];
				layers[j].ru[i] = default_values[1];
				layers[j].rv[i] = default_values[2];
				layers[j].rw[i] = default_values[3];
				layers[j].e[i] = default_values[4];
			}
		}
	}

	static inline void outputVector(
			std::ostream &os,					///< output stream
			const T *v,
			int i_size
	)
	{
		os << "[";
		for (int i = 0; i < i_size-1; i++)
			os << " " << v[i];
		os << " " << v[i_size-1] << " ]" << std::endl;
	}



	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationNodeDataLayeredSOA &d
	 )	{
		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			os << " + Layer " << j << ":" << std::endl;
			os << "   + r: ";
			outputVector(os, d.layers[j].r, N);
			os << "   + ru: ";
			outputVector(os, d.layers[j].ru, N);
			os << "   + rv: ";
			outputVector(os, d.layers[j].rv, N);
			os << "   + rw: ";
			outputVector(os, d.layers[j].rw, N);

			os << "   + e/max_wave_speed: ";
			outputVector(os, d.layers[j].e, N);

			os << std::endl;
		}

		return os;
	}



	/**
	 * output verbose data about edge data
	 */
	inline void outputVerboseData(
			std::ostream &os,	///< cout
			T x_axis_x,			///< x-axis of new basis (x-component)
			T x_axis_y			///< x-axis of new basis (y-component)
	) const
	{
		os << "CSimulationNodeDataSOA:" << std::endl;

		for (int j = 0; j < SIMULATION_NUMBER_OF_LAYERS; j++)
		{
			os << " + Layer " << j << ":" << std::endl;
			os << "   + r:";
			outputVector(os, layers[j].r, N);

			T wru[N];
			T wrv[N];

			for (int i = 0; i < N; i++)
			{
				wru[i] = layers[j].ru[i];
				wrv[i] = layers[j].rv[i];
				CTriangle_VectorProjections::referenceToWorld(&wru[i], &wrv[i], x_axis_x, x_axis_y);
			}

			os << "   + ru:";
			outputVector(os, wru, N);
			os << "   + rv:";
			outputVector(os, wrv, N);
			os << "   + rw:";
			outputVector(os, layers[j].rw, N);

			os << "   + e/max_wave_speed:";
			outputVector(os, layers[j].e, N);

			os << std::endl;
		}
	}
};


/**
 * Edge communication data during simulation
 */
class CSimulationEdgeData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	CSimulationNodeDataLayeredSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS> dofs;


#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif


	/**
	 * wave speed in case that CFL=1
	 */
	T CFL1_scalar;


	/**
	 * setup routine for simulation parameters default edge setup
	 */
	void setupDefaultValues(T i_default_values[])
	{
		dofs.setupDefaultEdgeValues(i_default_values);
	}


	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationEdgeData &d
	 )	{
		os << "Edge Data [r, ru, rv, rw, e, cfl1_timestep_size, vx, vy]" << std::endl;

		std::cout << d.dofs << std::endl;
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};



/**
 * Face along vertical axis
 */
class CSimulationVerticalFaceData	:
		public CSimulationNodeDataSingleLayerSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_TOP_BOTTOM_FACE_DOFS>
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;



#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_EdgeData validation;
#endif

public:
	/**
	 * wave speed in case that CFL=1
	 */
	T CFL1_scalar;


	/**
	 * setup routine for simulation parameters default edge setup
	 */
public:
	void setupDefaultValues(T i_default_values[])
	{
		setupDefaultEdgeValues(i_default_values);
	}


	friend
	::std::ostream&
	 operator<<(
			 ::std::ostream& os,
			  const CSimulationVerticalFaceData &d
	 )	{
		os << "WFace data [r, ru, rv, rw, e, cfl1_timestep_size, vx, vy]" << std::endl;

		std::cout << (CSimulationNodeDataSingleLayerSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_TOP_BOTTOM_FACE_DOFS>&)d << std::endl;
		os << " + CFL1_scalar: " << d.CFL1_scalar << std::endl;

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		os << d.validation << std::endl;
#endif
		os << std::endl;

		return os;
	}
};




/**
 * cell data
 *
 * this data structure contains all explicitly needed information for cell data
 */
class CSimulationCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * weights for basis functions
	 */
	CSimulationNodeDataLayeredSOA<SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS> dofs;

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE
	/**
	 * area of cell as projected on sphere
	 */
	T cellArea;


	/**
	 * cube face of the edge
	 */
	CCube_To_Sphere_Projection::EFaceType face;

	/**
	 * x,y-Coordinates of the 3 vertices on the cube face
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T vertices[3][2];

	/**
	 * x,y,z-Coordinates of the 3 vertices projected on the sphere
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T coordinates3D[3][3];

	/**
	 * edge lengths on sphere
	 *
	 *
	 * 	|\
	 * 1|  \0
	 * 	|    \
	 * 	-------
	 * 	   2
	 *
	 */
	T edgeLength[3];

	/**
	 * radius of the inscribed circle
	 *
	 * |\
	 * |  \
	 * |    \
	 * |      \
	 * |___.    \
	 * |   |      \
	 * |___|________\
	 *
	 */
	T incircleRadius;

	/*
	 * rotation matrix from normal to edge space for left edge
	 */
	T leftEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for right edge
	 */
	T rightEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for hyp edge
	 */
	T hypEdgeRotationMatrix[2][2];

	/**
	 * transformation matrix for shear and scaling
	 *
	 *  _                               |\_
	 *  \ - _                           |  \_     _
	 *   \    - _                       |    \_ -|2'
	 *    \       - _          --->    1|      \_
	 *     \__________-_                |        \_
	 *                                  |__________\
	 *                                       1
	 */
	T transformationMatrix[2][2];

	/**
	 * inverse transformation matrix for shear and scaling
	 *
	 *      |\                    _
	 *      |  \     _            \ - _
	 *      |    \ -|2'            \    - _
	 *     1|      \      --->      \       - _
	 *      |        \               \__________-_
	 *      |__________\
	 *                                       1
	 */
	T inverseTransformationMatrix[2][2];


	void setupDistortedGrid(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y		///< normals for left edge (necessary for back-rotation of element)
	) {
		T i_x = (i_vertex_left_x + i_vertex_right_x + i_vertex_top_x)*(T)(1.0/3.0);
		T i_y = (i_vertex_left_y + i_vertex_right_y + i_vertex_top_y)*(T)(1.0/3.0);

		// get cube face
		face = CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(i_x, i_y);

		// project to (-1,1)^2
		CCube_To_Sphere_Projection::project2DTo2DFace(
				face,
				&i_vertex_left_x,	&i_vertex_left_y,
				&i_vertex_right_x,	&i_vertex_right_y,
				&i_vertex_top_x,	&i_vertex_top_y
			);

		vertices[0][0] = i_vertex_left_x;
		vertices[0][1] = i_vertex_left_y;
		vertices[1][0] = i_vertex_right_x;
		vertices[1][1] = i_vertex_right_y;
		vertices[2][0] = i_vertex_top_x;
		vertices[2][1] = i_vertex_top_y;


		// set 3D coordinates on sphere
		for(int i = 0; i < 3; i++)
		{
			CCube_To_Sphere_Projection::project2DFaceTo3D(
					face,
					vertices[i][0],
					vertices[i][1],
					&coordinates3D[i][0],
					&coordinates3D[i][1],
					&coordinates3D[i][2]
			);
		}


		// set correct radius
		T radius = EARTH_RADIUS_IN_METERS;


		for(int i = 0; i < 3; i++)
		{
			// set edge length as on sphere
			edgeLength[i] = CTriangle_VectorProjections::getEdgeLengthOnSphere(
					vertices[i][0],
					vertices[i][1],
					vertices[(i+1)%3][0],
					vertices[(i+1)%3][1],
					radius
			);
		}

		// set rotation matrices for all edges
		CTriangle_VectorProjections::setHypEdgeRotationMatrix(
				hypEdgeRotationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		// always aligned at x-axis
		CTriangle_VectorProjections::setLeftEdgeRotationMatrix(
				leftEdgeRotationMatrix
			);

		CTriangle_VectorProjections::setRightEdgeRotationMatrix(
				rightEdgeRotationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		CTriangle_VectorProjections::setTransformationMatrices(
				transformationMatrix,
				inverseTransformationMatrix,
				edgeLength[0],
				edgeLength[1],
				edgeLength[2]
			);

		// approximation of the triangle area as half of the product of the two cathetus lengths
//		cellArea = (edgeLength[1] * edgeLength[2] ) / (T)2.0;

		cellArea = CTriangle_VectorProjections::getCellAreaOnSphere(
				edgeLength[0],
				edgeLength[1],
				edgeLength[2],
				radius
			);

		// set inscribed circle radius
		incircleRadius = CTriangle_VectorProjections::getIncircleRadius(
				edgeLength[0],
				edgeLength[1],
				edgeLength[2],
				radius
			);
	}

#endif


#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		T max_eigen_coefficient;
#endif



#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER > 1
	bool refine;
	bool coarsen;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
	CValidation_CellData validation;
#endif

	/**
	 * setup routine for simulation parameters default cell setup
	 */
	void setupDefaultValues(T default_values[])
	{
		dofs.setupDefaultCellValues(default_values);
	}



	friend
	::std::ostream&
	 operator<<	(
			 ::std::ostream& os,
			  const CSimulationCellData &d
	 )	{
		d.outputVerboseData(os, 1, 0);
		return os;
	}


	void outputVerboseData(
			std::ostream &os,					///< cout
			T x_axis_x,		///< x-axis of new basis (x-component)
			T x_axis_y		///< x-axis of new basis (y-component)
	)	const
	{
		dofs.outputVerboseData(os, x_axis_x, x_axis_y);

#if CONFIG_EULER_STORE_AUGMENTED_RIEMANN_EIGEN_COEFFICIENTS
		os << "max_eigen_coefficient: " << max_eigen_coefficient << std::endl;
#endif

#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
		std::cout << validation << std::endl;
#endif
	}
};

#include "../../subsimulation_generic/types/CVisualizationVertexData.hpp"



/**
 * simulation types which
 */
class CHyperbolicTypes
{
public:

	/**
	 * Simulation types are only used for the simulation itself,
	 * not for any visualization
	 */
	class CSimulationTypes
	{
	public:
		/*
		 * base scalar type
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/*
		 * access to cell data
		 */
		typedef CSimulationCellData CCellData;

		/*
		 * type for nodal points (h,hu,hv,b)
		 */
		typedef CSimulationNodeData CNodeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CSimulationEdgeData CEdgeData;

		/*
		 * exchange data format for adjacent grid-cells
		 */
		typedef CSimulationVerticalFaceData CVerticalFaceData;
	};



	/**
	 * This class implements all types which are used for visualization
	 */
	class CVisualizationTypes
	{
	public:
		/*
		 * base scalar type for visualization
		 */
		typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

		/**
		 * cell data accessed to get visualization
		 */
		typedef CSimulationCellData CCellData;

		/**
		 * node data for visualization
		 */
		typedef CVisualizationNodeData CNodeData;

	};


	/**
	 * Simulation stacks for data storage
	 */
	typedef sierpi::CSimulationStacks<CSimulationTypes, CVisualizationTypes> CSimulationStacks;
};




#endif /* CEULER_MULTILAYERTYPES_HPP_ */
