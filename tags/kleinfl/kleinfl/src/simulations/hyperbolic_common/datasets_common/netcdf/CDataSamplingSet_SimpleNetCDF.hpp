/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: July 19, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include <cassert>
#include <stdexcept>
#include <netcdf.hh>
#include <string.h>
#include "CDataSamplingSet_MultiResolution.hpp"



/**
 * Simple NetCDF datafile loader
 *
 * We assume pixel-node registration, see "B.2.2 Gridline and Pixel node registration" of GMT Docs
 */
template <typename T>
class CDataSamplingSet_SimpleNetCDF	:
	public CDataSamplingSet_MultiResolution<T>
{
	using CDataSamplingSet_MultiResolution<T>::number_of_data_levels;
	using CDataSamplingSet_MultiResolution<T>::dataLevels;

public:

	void setOutOfDomainValue(T i_value)
	{
		for (int i = 0; i < number_of_data_levels; i++)
		{
			dataLevels[i].out_of_domain_value = i_value;
		}
	}

	CDataSamplingSet_SimpleNetCDF(
			int i_verbosity_level
	)	:
		CDataSamplingSet_MultiResolution<T>(i_verbosity_level)
	{

	}

	size_t getXResolution()
	{
		return CDataSamplingSet_MultiResolution<T>::getXResolution();
	}

	size_t getYResolution()
	{
		return CDataSamplingSet_MultiResolution<T>::getYResolution();
	}

	T getXMin()
	{
		return CDataSamplingSet_MultiResolution<T>::getXMin();
	}

	T getXMax()
	{
		return CDataSamplingSet_MultiResolution<T>::getXMax();
	}

	T getYMin()
	{
		return CDataSamplingSet_MultiResolution<T>::getYMin();
	}

	T getYMax()
	{
		return CDataSamplingSet_MultiResolution<T>::getYMax();
	}

	T getFloat2D(
			T i_x,
			T i_y,
			int i_level
	)
	{
		assert(i_level >= 0);
		return CDataSamplingSet_MultiResolution<T>::getFloat2D(i_x, i_y, i_level);
	}


	T getFloat2D_BorderValue(
			T i_x,
			T i_y,
			int i_level
	)
	{
		assert(i_level >= 0);
		return CDataSamplingSet_MultiResolution<T>::getFloat2D_BorderValue(i_x, i_y, i_level);
	}


	~CDataSamplingSet_SimpleNetCDF()
	{
	}



	/**
	 * read range attribute from given variable
	 */
private:
	bool p_getRangeFromVariableId(
			int ncid,
			int varid,
			T *o_min,
			T *o_max
	)
	{
		int retval;
		size_t len;

		if ((retval = nc_inq_attlen(ncid, varid, "actual_range", &len)) != NC_NOERR)
		{
			throw(std::runtime_error(std::string("nc_inq_attlen 'actual_range' not found: ")+nc_strerror(retval)));
			return false;
		}

		T *range = new T[len];


		if (sizeof(T) == 4)
		{
			if ((retval = nc_get_att_float(ncid, varid, "actual_range", (float*)&range[0])) != NC_NOERR)
			{
				throw(std::runtime_error(std::string("nc_get_att_float: ")+nc_strerror(retval)));
				return false;
			}
		}
		else
		{
			if ((retval = nc_get_att_double(ncid, varid, "actual_range", (double*)&range[0])) != NC_NOERR)
			{
				throw(std::runtime_error(std::string("nc_get_att_double: ")+nc_strerror(retval)));
				return false;
			}
		}

		*o_min = range[0];
		*o_max = range[1];

		delete [] range;
		return true;
	}


private:
	bool p_getRangeFromVariableName(
			int ncid,
			const char *i_var_name,
			T *o_min,
			T *o_max
	)
	{
		/*
		 * load range for variable i_var_name
		 */
		int tmp_var_id;
		int retval;
		if ((retval = nc_inq_varid(ncid, i_var_name, &tmp_var_id)) != NC_NOERR)
		{
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR (getRangeFromVariableName) var ")+i_var_name+" not found: "+nc_strerror(retval)));
		}

		try		{
			p_getRangeFromVariableId(ncid, tmp_var_id, o_min, o_max);
		}
		catch(std::runtime_error &r)		{
			nc_close(ncid);
			throw(r);
			return false;
		}
		return true;
	}


	/**
	 * load the dataset from a netcdf file
	 *
	 * \return true if everything is ok
	 */
public:
	bool loadDataset(
			const char *i_dataset_filename
	)
	{
		CDataSamplingSet_MultiResolution<T>::clear();

		/*
		 * NETCDF LOADER
		 */
		int retval;
		int ncid;

		if ((retval = nc_open(i_dataset_filename, NC_NOWRITE, &ncid)) != NC_NOERR)
		{
			throw(std::runtime_error(std::string("nc_open failed ")+i_dataset_filename));
		}

		int ndims, nvars, ngatts, recdim;
		if ((retval = ncinquire(ncid, &ndims, &nvars, &ngatts, &recdim)) == -1)
		{
		    nc_close(ncid);
			throw(std::runtime_error(std::string("ncinquire dimensions ")+i_dataset_filename));
		}



		/*
		 * verify to work on 2 dimensional datasets
		 */
		if (ndims != 2)
		{
		    nc_close(ncid);
			throw(std::runtime_error(std::string("ndims != 2: Expected 2 dimensions")));
		}

#if 0
		/*
		 * load title name id
		 */
		char *nc_title_string;
		int var_title_id;
		if ((retval = nc_inq_varid(ncid, "Title", &var_title_id)) != NC_NOERR)
		{
			nc_title_string = nullptr;
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR global variable 'Title' not found: ")+nc_strerror(retval)));
		}
		else
		{
			/*
			 * load title name
			 * nc_get_var_text
			 */
			if ((retval = nc_get_var_string(ncid, var_title_id, &nc_title_string)) != NC_NOERR)
			{
				nc_close(ncid);
				throw(std::runtime_error(std::string("ERROR global variable 'title' not found: ")+nc_strerror(retval)));
			}
		}
#endif


		enum EDatasetType
		{
			GMT_PLANAR_BATHYMETRY,
			GMT_CUBED_SPHERE,
			GEBCO_08_GRID,			// not supported
			GEBCO_ONE_MINUTE_GRID,	// not supported
		};
		int cube_face_id = -1;

		EDatasetType eDatasetType = GMT_PLANAR_BATHYMETRY;

#if 0
		if (strcmp(nc_title_string, "GEBCO One Minute Grid") == 0)
		{
			eDatasetType = GEBCO_ONE_MINUTE_GRID;
		}
		else if (strcmp(nc_title_string, "GEBCO_08 Grid") == 0)
		{
			eDatasetType = GEBCO_08_GRID;
		}
		else
#endif

		/*
		 * check for cubed sphere
		 */
		std::string s = i_dataset_filename;

		const char* cube_faces[] = {
				"/front.nc",
				"/right.nc",
				"/back.nc",
				"/left.nc",
				"/top.nc",
				"/bottom.nc"
		};

		bool cubed_sphere_data_found = false;
		for (int i = 0; i < 6; i++)
		{
			size_t pos = s.find(cube_faces[i]);
			if (pos != std::string::npos)
			{
				if (s.length() == pos+strlen(cube_faces[i]))
				{
					cube_face_id = i;
					cubed_sphere_data_found = true;
					eDatasetType = GMT_CUBED_SPHERE;
					break;
				}
			}
		}


		char char_buffer[MAX_NC_NAME];

		// size of array in x and y directions
		size_t array_size_x;
		size_t array_size_y;

		// max coordinates on array
		T xmin, xmax;
		T ymin, ymax;

		// temporary variable id
		int tmp_var_id;
		// temporary size of some information
		long tmp_size;




		/*
		 * load array size in x dimension
		 */
		// return the name and size of the first dimension
		if ((retval = ncdiminq(ncid, 0, &char_buffer[0], &tmp_size)) == -1)
		{
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR ncdiminq: ")+nc_strerror(retval)));
		}
		array_size_x = tmp_size;

		/*
		 * load array size in y dimension
		 */
		// return the name and size of the second dimension
		if ((retval = ncdiminq(ncid, 1, &char_buffer[0], &tmp_size)) == -1)
		{
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR ncdiminq: ")+nc_strerror(retval)));
		}
		array_size_y = tmp_size;


		/*
		 * load ranges
		 */
		if (eDatasetType == GMT_PLANAR_BATHYMETRY)
		{
			/*
			 * load range in x dimension
			 */
			try		{
				p_getRangeFromVariableName(ncid, "x", &xmin, &xmax);
			}
			catch(std::runtime_error &r)		{
				nc_close(ncid);
				throw(r);
			}

			/*
			 * load range in y dimension
			 */
			try		{
				p_getRangeFromVariableName(ncid, "y", &ymin, &ymax);
			}
			catch(std::runtime_error &r)		{
				nc_close(ncid);
				throw(r);
			}
		}
		else if (eDatasetType == GMT_CUBED_SPHERE)
		{
			xmin = 0;
			xmax = 1;
			ymin = 0;
			ymax = 1;
		}

		/*
		 * !!!!!!!!!!!!!!!!!
		 * !!! LOAD DATA !!!
		 * !!!!!!!!!!!!!!!!!
		 *
		 * allocate first level of multiresolution map
		 *
		 * the return value is the pointer to the first level data
		 */

		/*
		 * load variable 'z' location
		 */
		if ((retval = nc_inq_varid(ncid, "z", &tmp_var_id)) != NC_NOERR)
		{
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR var z not found: ")+nc_strerror(retval)));
		}

		T *data = CDataSamplingSet_MultiResolution<T>::allocateMultiresolutionLevels(array_size_x, array_size_y);

		if (sizeof(T) == 4)
			retval =  nc_get_var_float(ncid, tmp_var_id, (float*)data);
		else
			retval =  nc_get_var_double(ncid, tmp_var_id, (double*)data);

		if (retval != NC_NOERR)
		{
			nc_close(ncid);
			throw(std::runtime_error(std::string("ERROR while loading variable data: ")+nc_strerror(retval)));
		}

		/*
		 * close NC handler
		 */
		nc_close(ncid);

		/*
		 * setup datasets
		 */
		CDataSamplingSet_MultiResolution<T>::setupDomainParameters(xmin, ymin, xmax, ymax);

		/*
		 * setup the multiresolution map
		 */
		CDataSamplingSet_MultiResolution<T>::setupMultiresolutionMap();

		return true;
	}
};
