/*
 * CParameters_Datasets.hpp
 *
 *  Created on: Aug 30, 2012
 *      Author: schreibm
 */

#ifndef CPARAMETERS_DATASETS_HPP_
#define CPARAMETERS_DATASETS_HPP_

#include <vector>
#include <string>
#include <limits>

class CParameters_Datasets
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE	T;


public:
	/**
	 * selected dataset for DOF id 0 (bathymetry)
	 */
	int simulation_dataset_0_id;

	/**
	 * selected dataset for DOF id 1 (water)
	 */
	int simulation_dataset_1_id;

	/**
	 * selected dataset for DOF id 2
	 */
	int simulation_dataset_2_id;

	/**
	 * selected dataset for DOF id 3
	 */
	int simulation_dataset_3_id;

	/**
	 * selected dataset for DOF id 4
	 */
	int simulation_dataset_4_id;

	/**
	 * if using asagi, this value specifies the grid type id used by asagi
	 */
	int simulation_dataset_asagi_grid_type_id;


	/*******************************************************
	 * DATASET RELATED PARAMETERS
	 */

	/**
	 * cosine bell
	 */
	T simulation_dataset_cosine_bell_background;
	T simulation_dataset_cosine_bell_coefficient;

	/**
	 * parameters for default setup of column
	 */
	T simulation_dataset_breaking_dam_posx;
	T simulation_dataset_breaking_dam_posy;
	T simulation_dataset_breaking_dam_radius;
	T simulation_dataset_breaking_dam_height;

	/**
	 * values to setup the DOFs
	 */
	T simulation_dataset_default_nodal_values[SIMULATION_DATASET_SIZE];

	/**
	 * timestamp used for benchmark input data
	 */
	T simulation_dataset_benchmark_input_timestamp;

	/**
	 * filenames for datasets
	 *
	 * For tsunami simulation:
	 *   0: terrain data
	 *   1: displacement data
	 */
	std::vector<std::string> simulation_datasets;




	/**
	 * domain center (x-coordinate)
	 */
	T simulation_dataset_default_domain_translate_x;

	/**
	 * domain center (y-coordinate)
	 */
	T simulation_dataset_default_domain_translate_y;



	/**
	 * domain size (x-axis)
	 */
	T simulation_dataset_default_domain_size_x;

	/**
	 * domain size (y-axis)
	 */
	T simulation_dataset_default_domain_size_y;

	/**
	 * domain height (z-axis)
	 */
	T simulation_dataset_default_domain_size_z;

	/**
	 * alignment of window for discrete datasets
	 */
	enum E_WINDOW_ALIGNMENT
	{
		WINDOW_ALIGNMENT_INNER_QUAD = 0,
		WINDOW_ALIGNMENT_OUTER_QUAD = 1,
		WINDOW_ALIGNMENT_NONE = 2
	};

	int simulation_dataset_window_alignment;


	CParameters_Datasets()	:
		simulation_dataset_0_id(0),
		simulation_dataset_1_id(1),
		simulation_dataset_2_id(0),
		simulation_dataset_3_id(1),
		simulation_dataset_4_id(0),

		simulation_dataset_asagi_grid_type_id(0),

		simulation_dataset_breaking_dam_posx(0.0),
		simulation_dataset_breaking_dam_posy(0.0),
		simulation_dataset_breaking_dam_radius(-1.0),
		simulation_dataset_breaking_dam_height(1.0),

		simulation_dataset_benchmark_input_timestamp(-1),

		simulation_dataset_default_domain_translate_x(std::numeric_limits<T>::infinity()),
		simulation_dataset_default_domain_translate_y(std::numeric_limits<T>::infinity()),
		simulation_dataset_default_domain_size_x(std::numeric_limits<T>::infinity()),
		simulation_dataset_default_domain_size_y(std::numeric_limits<T>::infinity()),
		simulation_dataset_default_domain_size_z(std::numeric_limits<T>::infinity()),

		simulation_dataset_window_alignment(WINDOW_ALIGNMENT_INNER_QUAD)
	{
		simulation_dataset_default_nodal_values[0] = -std::numeric_limits<T>::infinity();
		simulation_dataset_default_nodal_values[1] = -std::numeric_limits<T>::infinity();
		simulation_dataset_default_nodal_values[2] = -std::numeric_limits<T>::infinity();
		simulation_dataset_default_nodal_values[3] = -std::numeric_limits<T>::infinity();
	}
};



#endif /* CPARAMETERS_DATASETS_HPP_ */
