/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Dec 16, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CHYPERBOLIC_CONFIG_HPP_
#define CHYPERBOLIC_CONFIG_HPP_


#include "../../../global_config.h"

#if CONFIG_SUB_SIMULATION_TSUNAMI

#	define CONFIG_SUBSIMULATION_STRING "tsunami"
#	include "../subsimulation_tsunami/CConfig.hpp"

#elif CONFIG_SUB_SIMULATION_EULER

#	define CONFIG_SUBSIMULATION_STRING "euler"
#	include "../subsimulation_euler/CConfig.hpp"

#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER

#	define CONFIG_SUBSIMULATION_STRING "euler multilayer"
#	include "../subsimulation_euler_multilayer/CConfig.hpp"

#else

#	error "unknown sub-simulation"

#endif



/**
 * flux solver to use
 *
 * 0: lax friedrich with constant numerical friction
 * 1: lax friedrich
 * 2: fwave
 * 3: augumented riemann
 * 4: hybrid
 * 5: augumented riemann (fortrancode from geoclaw)
 * 6: velocity upwinding
 */

#ifndef SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID
	#error "SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID not set!"
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID	1
#endif

#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID > 1 && SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 1
//	#error "fwave and 1st order not yet possible"
#endif

#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID < 2 && SIMULATION_HYPERBOLIC_NODAL_DEGREE_OF_BASIS_FUNCTIONS == 0
//	#error "hybrid solver necessary for implemented 0th order simulation"
#endif



/**
 * RungeKutta
 *
 * use 2nd order by default
 */
#ifndef SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER
	#define SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER 1
#endif


/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * AUTOMATICALLY SET PRECOMPILER DEFINES - do not modify anything below this line
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */


/**
 * constant friction coefficient for lax friedrich solver with constant numerical friction
 */
#if SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID == 0
	#define SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_LAX_FRIEDRICH_CONST_FRICTION_COEFF	10.0
#endif




#ifndef SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER
#	error "SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER not defined!"
#	define SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER 1
#endif


#ifndef SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE
#	error "SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE not defined"
#	define SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE 1
#endif


/**
 * enable cluster local time stepping
 */
#ifndef CONFIG_HYPERBOLIC_CLUSTER_LOCAL_TIME_STEPPING
#error "CONFIG_HYPERBOLIC_CLUSTER_LOCAL_TIME_STEPPING not defined"
#define CONFIG_HYPERBOLIC_CLUSTER_LOCAL_TIME_STEPPING	1
#endif


/**
 * default gravity for simulation
 */
#ifndef SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION
#	error "SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION not defined"
#	define SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION		(9.81)
#endif

/**
 * radius of earth in meters
 */
#define EARTH_RADIUS_IN_METERS (6378137.0)

/**
 * conserve momentum or velocity
 */

#ifndef SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
#	error "SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM not defined"
#	define SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM
#endif


#ifndef SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
#	error "SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY not defined"
#	define SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
#endif

#if SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM && SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
#	error "SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM and SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY are set"
#endif

#if !SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM && !SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY
#	error "SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_MOMENTUM and SIMULATION_HYPERBOLIC_ADAPTIVITY_CONSERVE_VELOCITY are both not set"
#endif

/**
 * enable/disable compilation with tests for vertex coordinates
 */
#if DEBUG
	#define COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION		1
#else
	#define COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION		0
#endif


#endif
