/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 23, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSUBSIMULATION_GENERIC_TYPES_HPP
#define CSUBSIMULATION_GENERIC_TYPES_HPP


#if !DEBUG
#if COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION
#error "COMPILE_WITH_DEBUG_HYPERBOLIC_ELEMENTDATA_VALIDATION activated in non-debug mode!"
#endif
#endif

#include "../CConfig.hpp"


#if CONFIG_SUB_SIMULATION_TSUNAMI

#	define CONFIG_SUBSIMULATION_STRING "tsunami"
#	include "../../subsimulation_tsunami/types/CTypes.hpp"

#elif CONFIG_SUB_SIMULATION_EULER

#	define CONFIG_SUBSIMULATION_STRING "euler"
#	include "../../subsimulation_euler/types/CTypes.hpp"

#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER

#	define CONFIG_SUBSIMULATION_STRING "euler multilayer"
#	include "../../subsimulation_euler_multilayer/types/CTypes.hpp"

#else
#	error "unknown sub-simulation"
#endif

#endif
