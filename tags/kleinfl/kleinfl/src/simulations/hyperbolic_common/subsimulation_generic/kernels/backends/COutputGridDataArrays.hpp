/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Feb 2, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef GENERIC_CHYPERBOLIC_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_
#define GENERIC_CHYPERBOLIC_TSUNAMI_OUTPUTGRIDDATAARRAYS_HPP_


#if CONFIG_SUB_SIMULATION_TSUNAMI

#	include "simulations/hyperbolic_common/subsimulation_tsunami/kernels/backends/COutputGridDataArrays.hpp"

#elif CONFIG_SUB_SIMULATION_EULER

#	include "simulations/hyperbolic_common/subsimulation_euler/kernels/backends/COutputGridDataArrays.hpp"

#elif CONFIG_SUB_SIMULATION_EULER_MULTILAYER

#	include "simulations/hyperbolic_common/subsimulation_euler_multilayer/kernels/backends/COutputGridDataArrays.hpp"

#else
#	error "unknown sub-simulation"
#endif

#endif
