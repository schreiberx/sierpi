/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Jul 5, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHYPERBOLIC_SIMULATION_PARAMETERS_HPP_
#define CHYPERBOLIC_SIMULATION_PARAMETERS_HPP_

#include <stdint.h>
#include <vector>
#include <string>
#include <limits>
#include <cstdlib>
//----------------------debug
#include <sys/time.h>
//--------------------------

#include "subsimulation_generic/types/CTypes.hpp"
#include "subsimulation_generic/CConfig.hpp"
//#include "basis_functions_and_matrices/CComputation2D_Matrices.hpp"
#include "lib/xmlconfig/CXMLConfig.hpp"
#include <cmath>

#include "libsierpi/grid/CBoundaryConditions.hpp"

#include "../../CSimulation_MainParameters.hpp"


#if CONFIG_SIERPI_ENABLE_GUI
#include "../../CSimulation_MainGuiParameters.hpp"

#include "libgl/hud/CGlHudConfig.hpp"
#include "mainvis/CGuiConfig.hpp"
#endif

#include "datasets_common/CParameters_Datasets.hpp"

#if CONFIG_DEFORMATIONAL_FLOW
#include "simulations/hyperbolic_common/subsimulation_tsunami/tsunami_benchmarks/CGlobal.hpp"
#endif


/**
 * simulation parameters used by serial and parallel simulation
 */
class CParameters	:
	public CXMLConfigInterface,
	public CSimulation_MainParameters,
	public CParameters_Datasets
	#if CONFIG_SIERPI_ENABLE_GUI
		,
		public CSimulation_MainGuiParameters
	#endif
{
public:
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE	T;

	/**
	 * reset simulation
	 */
	bool simulation_reset;

	/**
	 * initial recursion depth during setup
	 */
	int grid_initial_recursion_depth;

	/**
	 * initial recursion depth during setup including update with cluster split value
	 */
	int grid_initial_recursion_depth_with_initial_cluster_splits;

	/**
	 * minimum and maximum recursion depth during refinements / coarsenings
	 */
	int grid_min_relative_recursion_depth;
	int grid_max_relative_recursion_depth;


	/**
	 * initial splittings of clusters to refine base triangulation
	 */
	int grid_initial_cluster_splits;

	/**
	 * thresholds for coarsening/refinement
	 */
	T adaptive_coarsen_parameter_0;
	T adaptive_refine_parameter_0;

	T adaptive_coarsen_parameter_1;
	T adaptive_refine_parameter_1;


	/**
	 * element data during setup
	 */
	CHyperbolicTypes::CSimulationTypes::CCellData simulation_parameter_cell_data_setup;

	/**
	 * boundary conditions
	 */
	int simulation_domain_boundary_condition;

	/**
	 * dataset boundary condition on right side of domain
	 */
	int simulation_domain_strip_boundary_condition_dataset_right;

	/**
	 * Dirichlet boundary edge data
	 */
	CHyperbolicTypes::CSimulationTypes::CEdgeData simulation_domain_boundary_dirichlet_edge_nodal_data;


	/**
	 * adaptively set the timestep size by using CFL condition
	 */
	bool simulation_parameter_adaptive_timestep_size_with_cfl;


	/**
	 * Root clusters per unit domain in the direction of the x axis.
	 *
	 * This is important to setup the domain length parameters for root triangles
	 * when more than 2 triangles are used to set-up the domain
	 */
	T simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain;

	/**
	 * Gravitation for simulation (not implemented everywhere)
	 */
	T simulation_parameter_gravitation;

	/**
	 * Fixed timestep size
	 */
	//T simulation_parameter_fixed_timestep_size;

	/**
	 * CFL condition for adaptive timestep size
	 */
	T simulation_parameter_cfl;

	/**
	 * Minimum time-step allowed for simulation
	 */
	T simulation_parameter_minimum_timestep_size;



#if SIMULATION_EXIT_LOCAL_CELLS_EXCEED_VALUE > 0 && CONFIG_ENABLE_MPI
	/**
	 * threshold to exit if local number of cells is exceeding particular value
	 */
	long long simulation_threshold_exit_if_exceeding_local_number_of_cells;
#endif
	/**
	 * number of triangles when to split the cluster
	 */
	long long cluster_split_workload_size;


	/**
	 * number of overall triangles of 2 clusters when to join the cluster
	 */
	long long cluster_join_workload_size;


	/**
	 * Set the number of triangles in a single cluster when to split/join
	 */
	long long cluster_update_split_join_size_after_elapsed_timesteps;

	/**
	 * Scalar to use for non-table based computation of optimal split/join size
	 */
	int cluster_update_split_join_size_after_elapsed_scalar;

	/**
	 * split and join each nth timestep
	 */
	int cluster_split_and_join_every_nth_timestep;


	/**
	 * emit random raindrops for testing purpose
	 */
	bool simulation_random_raindrops_activated;


	/**
	 * emit random raindrops emission time
	 */
	T simulation_random_raindrops_emission_real_time;


	/**
	 * emit random raindrops emission simulation time
	 */
	T simulation_random_raindrops_emission_simulation_time;



	/**
	 * world scene (base triangulation)
	 */
	int simulation_world_scene_id;

	int num_buoys;

	/**
	 * position of all buoys in 2D domain space
	 * [*][0] = x
	 * [*][1] = y
	 */
	T buoy_position[1000][2];

#if CONFIG_DEFORMATIONAL_FLOW
	int benchmark_deformational_flow_type;
#endif


#if SIMULATION_HYPERBOLIC_PARALLEL
	virtual void updateClusterParameters() = 0;
#endif



public:
	CParameters()	:
		simulation_reset(false),
		grid_initial_recursion_depth(6),
		grid_min_relative_recursion_depth(0),
		grid_max_relative_recursion_depth(8),

		grid_initial_cluster_splits(0),

		simulation_domain_boundary_condition(DEFAULT_BOUNDARY_CONDITION),
		simulation_domain_strip_boundary_condition_dataset_right(false),

		simulation_parameter_adaptive_timestep_size_with_cfl(true),

		simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain(1),

		simulation_parameter_gravitation(-1),
		simulation_parameter_cfl(-std::numeric_limits<T>::infinity()),
		simulation_parameter_minimum_timestep_size(-1),

#if SIMULATION_EXIT_LOCAL_CELLS_EXCEED_VALUE > 0 && CONFIG_ENABLE_MPI
		simulation_threshold_exit_if_exceeding_local_number_of_cells(SIMULATION_EXIT_LOCAL_CELLS_EXCEED_VALUE),
#endif

#if CONFIG_ENABLE_SCAN_BASED_SPLIT_AND_JOIN
		cluster_split_workload_size(-1),
		cluster_join_workload_size(-1),
#else
		cluster_split_workload_size(4096),
		cluster_join_workload_size(-1),
#endif

		cluster_update_split_join_size_after_elapsed_timesteps(1),
		cluster_update_split_join_size_after_elapsed_scalar(7),

		cluster_split_and_join_every_nth_timestep(1),

		simulation_random_raindrops_activated(false),
		simulation_random_raindrops_emission_real_time(100),

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
		simulation_random_raindrops_emission_simulation_time(5000),
#else
		simulation_random_raindrops_emission_simulation_time(10),
#endif
#if CONFIG_DEFORMATIONAL_FLOW
		benchmark_deformational_flow_type(1),
#endif
		simulation_world_scene_id(3)
	{
		/*
		 * adaptive stuff
		 */
		adaptive_refine_parameter_0 = SIMULATION_REFINE_PARAMETER_0;
		adaptive_refine_parameter_1 = SIMULATION_REFINE_PARAMETER_1;

		adaptive_coarsen_parameter_0 = SIMULATION_COARSEN_PARAMETER_0;
		adaptive_coarsen_parameter_1 = SIMULATION_COARSEN_PARAMETER_1;


		if (simulation_parameter_cfl < -99999999999.0)
			simulation_parameter_cfl = SIMULATION_HYPERBOLIC_CFL;

		simulation_datasets.resize(4);

		reset();
	}



	virtual ~CParameters()
	{
	}



public:
	bool setupTwoInts(
			const char *i_optstring,
			int *o_integer1,
			int *o_integer2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_integer1 = atoi(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_integer1 = std::atoi(l.c_str());
		*o_integer2 = std::atoi(r.c_str());

		return true;
	}



public:
	bool setupTwoLongLongs(
			const char *i_optstring,
			long long *o_integer1,
			long long *o_integer2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_integer1 = atoll(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_integer1 = std::atoll(l.c_str());
		*o_integer2 = std::atoll(r.c_str());

		return true;
	}



public:
	template <typename T>
	bool setupTwoFloats(
			const char *i_optstring,
			T *o_float1,
			T *o_float2
	)
	{
		std::string strarg = i_optstring;

		int separators = 0;
		for (unsigned int i = 0; i < strarg.size(); i++)
			if (strarg[i] == '/')
				separators++;

		if (separators == 0)
		{
			*o_float1 = atof(i_optstring);
			return true;
		}

		size_t pos = strarg.find('/');
		if (pos == std::string::npos)
		{
			std::cout << "Invalid format! Use e. g. 123 or 834/54" << std::endl;
			exit(-1);
		}

		std::string l = strarg.substr(0, pos);
		std::string r = strarg.substr(pos+1);

		*o_float1 = std::atof(l.c_str());
		*o_float2 = std::atof(r.c_str());

		return true;
	}


	/**********************************
	 * XML CONFIG FILE BASED CONFIGURATION
	 */
public:
	void config_xml_file_setupInterface(
			CXMLConfig &i_cXMLConfig
	)
	{
		i_cXMLConfig.registerConfigClass("swe", this);
		i_cXMLConfig.registerConfigClass("tsunami", this);
		i_cXMLConfig.registerConfigClass("hyperbolic", this);
		i_cXMLConfig.registerConfigClass("buoy", this);
	}



	bool setupXMLParameter(
			const char *i_scope_name,
			const char *i_name,
			const char *i_value
	)
	{
		if (	strcmp("swe", i_scope_name) == 0		 ||
				strcmp("tsunami", i_scope_name) == 0
		)
		{
			XML_CONFIG_TEST_AND_SET_INT("grid-initial-recursion-depth", grid_initial_recursion_depth);
			XML_CONFIG_TEST_AND_SET_INT("grid-min-relative-recursion-depth", grid_min_relative_recursion_depth);
			XML_CONFIG_TEST_AND_SET_INT("grid-max-relative-recursion-depth", grid_max_relative_recursion_depth);

			XML_CONFIG_TEST_AND_SET_INT("grid-initial-cluster-splits", grid_initial_cluster_splits);


			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-coarsen-threshold", adaptive_coarsen_parameter_0);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-refine-threshold", adaptive_refine_parameter_0);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-coarsen-slope-threshold", adaptive_coarsen_parameter_1);
			XML_CONFIG_TEST_AND_SET_FLOAT("adaptive-refine-slope-threshold", adaptive_refine_parameter_1)

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-gravitation", simulation_parameter_gravitation);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-cfl", simulation_parameter_cfl);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-adaptive-timestep-size-with-cfl", simulation_parameter_adaptive_timestep_size_with_cfl);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-minimum-timestep", simulation_parameter_minimum_timestep_size);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-timestep-size", simulation_parameter_global_timestep_size);

			XML_CONFIG_TEST_AND_SET_INT("cluster-split-workload-size", cluster_split_workload_size);
			XML_CONFIG_TEST_AND_SET_INT("cluster-join-workload-size", cluster_join_workload_size);

			XML_CONFIG_TEST_AND_SET_FLOAT("cluster-update-split-join-size-after-elapsed-timesteps", cluster_update_split_join_size_after_elapsed_timesteps);
			XML_CONFIG_TEST_AND_SET_FLOAT("cluster-update-split-join-size-after-elapsed-scalar", cluster_update_split_join_size_after_elapsed_scalar);

			XML_CONFIG_TEST_AND_SET_INT("cluster-split-and-join-every-nth-timestep", cluster_split_and_join_every_nth_timestep);

			XML_CONFIG_TEST_AND_SET_INT("simulation-world-scene-id", simulation_world_scene_id);
			XML_CONFIG_TEST_AND_SET_INT("simulation-dataset-0-id", simulation_dataset_0_id);
			XML_CONFIG_TEST_AND_SET_INT("simulation-dataset-1-id", simulation_dataset_1_id);

#if CONFIG_ENABLE_ASAGI
			XML_CONFIG_TEST_AND_SET_INT("simulation-dataset-asagi-grid-type-id", simulation_dataset_asagi_grid_type_id);
#endif

			XML_CONFIG_TEST_AND_SET_BOOL("simulation-random-raindrops-activated", simulation_random_raindrops_activated);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-random-raindrops-emission-real-time", simulation_random_raindrops_emission_real_time);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-random-raindrops-emission-simulation-time", simulation_random_raindrops_emission_simulation_time);

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-terrain-default-distance", simulation_dataset_default_nodal_values[0]);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-water-surface-default-displacement", simulation_dataset_default_nodal_values[1]);

			XML_CONFIG_TEST_AND_SET_INT("simulation-domain-boundary-condition", simulation_domain_boundary_condition);
			XML_CONFIG_TEST_AND_SET_BOOL("simulation-domain-strip-boundary-condition-dataset-right", simulation_domain_strip_boundary_condition_dataset_right);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-domain-size-x", simulation_dataset_default_domain_size_x)
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-domain-size-y", simulation_dataset_default_domain_size_y)
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-parameter-domain-size-z", simulation_dataset_default_domain_size_z)

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-posx", simulation_dataset_breaking_dam_posx);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-posy", simulation_dataset_breaking_dam_posy);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-radius", simulation_dataset_breaking_dam_radius);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cylinder-height", simulation_dataset_breaking_dam_height);

			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cosine-background", simulation_dataset_cosine_bell_background);
			XML_CONFIG_TEST_AND_SET_FLOAT("simulation-dataset-cosine-coefficient", simulation_dataset_cosine_bell_coefficient);

#if CONFIG_DEFORMATIONAL_FLOW
			XML_CONFIG_TEST_AND_SET_INT("benchmark-deformational-flow-type", benchmark_deformational_flow_type);
			CGlobal& gx = CGlobal::getInstance();
			gx.setBenchmarkType(benchmark_deformational_flow_type);
#endif

			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-0", simulation_datasets[0]);
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-1", simulation_datasets[1]);
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-2", simulation_datasets[2]);
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-3", simulation_datasets[3]);

			// tsunami convenient namings for simulation-dataset-0 and simulation-dataset-1
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-terrain-file", simulation_datasets[0]);
			XML_CONFIG_TEST_AND_SET_STRING("simulation-dataset-displacement-file", simulation_datasets[1]);

#if CONFIG_SIERPI_ENABLE_GUI
			XML_CONFIG_TEST_AND_SET_INT("visualization-dof-method", visualization_dof_method);
			XML_CONFIG_TEST_AND_SET_INT("visualization-boundary-method", visualization_boundary_method);
#endif

			XML_CONFIG_TEST_AND_SET_INT("simulation-dataset-window-alignment", simulation_dataset_window_alignment);
		}
		return false;
	}


	/**********************************
	 * COMMAND LINE CONFIGURATION
	 */

	/**
	 * return string to be used for command line parameter parsing
	 */
	const char* config_command_line_getArgumentString()
	{
		return "a:C:d:F:h:i:I:k:l:o:r:s:u:U:v:w:y:z:";
	}


	/**
	 * output help about command line arguments
	 */
	void config_command_line_getArgumentsHelp()
	{

#if SIMULATION_HYPERBOLIC_PARALLEL
		std::cout << "  DOMAIN SETUP:" << std::endl;
		std::cout << "	-w [int]: world scene" << std::endl;
		std::cout << "	-y [int]: terrain" << std::endl;
		std::cout << "	-z [int]: water surface initialization" << std::endl;
#endif

		std::cout << std::endl;
		std::cout << "  CLUSTERS: SPLITS/JOINS" << std::endl;
		std::cout << "	-o [int]: cluster size when to request split" << std::endl;
		std::cout << "	-l [int]: cluster size when to request join (both children have to request a join)" << std::endl;
		std::cout << "	-k [int]: run split- and join-operations only every n-th timestep" << std::endl;
		std::cout << "	-u [int]: elapsed timesteps when to update split and join parameters with table hints" << std::endl;
		std::cout << "	-U [double]: factor used for automatic computation of split/join parameter. default: 0 (disabled)" << std::endl;


		std::cout << std::endl;
		std::cout << "  SIMULATION SPECIFIC:" << std::endl;
		std::cout << "	-d [int]: initial recursion depth" << std::endl;
		std::cout << "	-i [int]: min relative recursion depth" << std::endl;
		std::cout << "	-a [int]: max relative recursion depth" << std::endl;
		std::cout << "	-I [int]: initial cluster splits" << std::endl;
		std::cout << "	-C [double]: cfl condition" << std::endl;
		std::cout << "	-s [double]: timestep size" << std::endl;
		std::cout << "	-r [double]/[double]: adaptivity parameters (refine/coarsen)" << std::endl;
	}



	/**
	 * parse a command line argument
	 */
	bool config_command_line_parseArgument(
			char i_command_line_argument_option,	///< incoming argument character (e. g. 'e' for command line argument '-e')
			const char* i_command_line_argument		///< command line argument when additional value for argument is allowed
	)
	{
		switch(i_command_line_argument_option)
		{
		/*
		 * CFL
		 */
		case 'C':
			simulation_parameter_cfl = atof(i_command_line_argument);
			return true;

		case 'w':
			simulation_world_scene_id = atoi(i_command_line_argument);
			return true;

		case 'y':
			simulation_dataset_0_id = atoi(i_command_line_argument);
			return true;

		case 'z':
			simulation_dataset_1_id = atoi(i_command_line_argument);
			return true;


			/*
			 * GRID
			 */
		case 'd':
			grid_initial_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'i':
			grid_min_relative_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'a':
			grid_max_relative_recursion_depth = atoi(i_command_line_argument);
			return true;

		case 'I':
			grid_initial_cluster_splits = atoi(i_command_line_argument);
			return true;

			/*
			 * SPLITTING
			 */
		case 'o':
			cluster_split_workload_size = atoi(i_command_line_argument);

			// deactivate auto-mode
			cluster_update_split_join_size_after_elapsed_timesteps = -1;
			cluster_update_split_join_size_after_elapsed_scalar = -1;
			return true;
			
			
		case 'l':
			cluster_join_workload_size = atoi(i_command_line_argument);

			// deactivate auto-mode
			cluster_update_split_join_size_after_elapsed_timesteps = -1;
			cluster_update_split_join_size_after_elapsed_scalar = -1;

			return true;
			

		case 'r':
			// adaptivity information
			if (!setupTwoFloats(i_command_line_argument, &adaptive_refine_parameter_0, &adaptive_coarsen_parameter_0))
				exit(-1);
			return true;

		case 'u':
			cluster_update_split_join_size_after_elapsed_timesteps = atoi(i_command_line_argument);
			return true;

		case 'U':
			cluster_update_split_join_size_after_elapsed_scalar = atoi(i_command_line_argument);
			if (cluster_update_split_join_size_after_elapsed_timesteps <= 0)
				cluster_update_split_join_size_after_elapsed_timesteps = 1;
			return true;

		case 'k':
			cluster_split_and_join_every_nth_timestep = atoi(i_command_line_argument);
			return true;
		}

		return false;
	}



	/**
	 * validate or fix invalid parameters
	 */
	bool config_validateAndFixParameters()
	{
		/**
		 * fix parameters
		 */
		if (cluster_split_workload_size > 0 && cluster_split_workload_size < 2)
		{
			std::cout << "Invalid split size: " << cluster_split_workload_size << "   setting split to 2" << std::endl;
			cluster_split_workload_size = 2;
		}

		if (cluster_join_workload_size == -1)
		{
			if (cluster_split_workload_size != -1)
				cluster_join_workload_size = cluster_split_workload_size/2;
		}
		else
		{
			if (cluster_join_workload_size > cluster_split_workload_size/2)
			{
				std::cerr << "ERROR: cluster_join_workload_size > cluster_split_workload_size/2 ( fixing to split=" << cluster_split_workload_size << " / join=" << cluster_join_workload_size << " )" << std::endl;
				cluster_join_workload_size = cluster_split_workload_size / 2;
			}
#if 0
			else if (cluster_join_workload_size < -1)
			{
				std::cout << "Inverting cluster join workload size to overcome 'cluster_join_workload_size > cluster_split_workload_size' constraint" << std::endl;
				cluster_join_workload_size = -cluster_join_workload_size;
			}
#endif
		}

		if (simulation_parameter_minimum_timestep_size <= 0)
			simulation_parameter_minimum_timestep_size = 0.000001;

		if (simulation_parameter_gravitation <= 0)
			simulation_parameter_gravitation = SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION;

		if (simulation_dataset_default_domain_size_x == std::numeric_limits<T>::infinity())
			simulation_dataset_default_domain_size_x = 5000.0;

		if (simulation_dataset_default_domain_size_y == std::numeric_limits<T>::infinity())
			simulation_dataset_default_domain_size_y = 5000.0;

		if (simulation_dataset_default_domain_size_z == std::numeric_limits<T>::infinity())
			simulation_dataset_default_domain_size_z = 60000.0;


		if (simulation_dataset_default_domain_translate_x == std::numeric_limits<T>::infinity())
			simulation_dataset_default_domain_translate_x = 0;

		if (simulation_dataset_default_domain_translate_y == std::numeric_limits<T>::infinity())
			simulation_dataset_default_domain_translate_y = 0;


		if (simulation_dataset_default_nodal_values[0] == -std::numeric_limits<T>::infinity())
			simulation_dataset_default_nodal_values[0] = SIMULATION_DATASET_DEFAULT_VALUE_0;

		if (simulation_dataset_default_nodal_values[1] == -std::numeric_limits<T>::infinity())
			simulation_dataset_default_nodal_values[1] = SIMULATION_DATASET_DEFAULT_VALUE_1;

		if (simulation_dataset_default_nodal_values[2] == -std::numeric_limits<T>::infinity())
			simulation_dataset_default_nodal_values[2] = SIMULATION_DATASET_DEFAULT_VALUE_2;

#if SIMULATION_DATASET_SIZE > 3
		if (simulation_dataset_default_nodal_values[3] == -std::numeric_limits<T>::infinity())
			simulation_dataset_default_nodal_values[3] = SIMULATION_DATASET_DEFAULT_VALUE_3;
#endif

#if SIMULATION_DATASET_SIZE > 4
		if (simulation_dataset_default_nodal_values[4] == -std::numeric_limits<T>::infinity())
			simulation_dataset_default_nodal_values[4] = SIMULATION_DATASET_DEFAULT_VALUE_4;
#endif

		if (simulation_parameter_global_timestep_size <= 0)
			simulation_parameter_global_timestep_size = simulation_parameter_minimum_timestep_size;

		if (grid_initial_recursion_depth < grid_initial_cluster_splits)
		{
			grid_initial_cluster_splits = grid_initial_recursion_depth;
			std::cout << "WARNING: initial cluster splits larger than initial recursion depth. Fixing to " << grid_initial_cluster_splits << std::endl;
		}


#if CONFIG_SIERPI_ENABLE_GUI
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
		if (visualization_data0_scale_factor < 0)
			visualization_data0_scale_factor = 0.02;
#else
		visualization_data0_scale_factor = DEFAULT_PARAMETER_VISUALIZATION_DATA0_SCALE;
#endif

		if (visualization_data1_scale_factor < 0)
#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE == 1
			// assume a maximum bathymetry data of -5000
			visualization_data1_scale_factor = 1.0/(5000.0*10.0);
#else
			visualization_data1_scale_factor = DEFAULT_PARAMETER_VISUALIZATION_DATA1_SCALE;
#endif

#endif

		if (simulation_threading_number_of_threads <= 0)
			simulation_threading_number_of_threads = 1;

		if (simulation_dataset_breaking_dam_radius < 0)
		{
			simulation_dataset_breaking_dam_posx = (T)-0.4*simulation_dataset_default_domain_size_x;
			simulation_dataset_breaking_dam_posy = (T)0.5*simulation_dataset_default_domain_size_y;

			simulation_dataset_breaking_dam_radius = (T)0.1*simulation_dataset_default_domain_size_x;
		}

		simulation_parameter_cell_data_setup.setupDefaultValues(simulation_dataset_default_nodal_values);
		simulation_domain_boundary_dirichlet_edge_nodal_data.setupDefaultValues(simulation_dataset_default_nodal_values);

		return true;
	}



#if CONFIG_SIERPI_ENABLE_GUI

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigSimulation(
			CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		/************************
		 * SIMULATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- SIMULATION CONTROL ---|"));
		io_cGlHudConfig.insert(o.setupBoolean("Reset simulation fluid", &simulation_reset));
		io_cGlHudConfig.insert(o.setupBoolean("Insert Random Raindrops", &simulation_random_raindrops_activated));

		/**
		 * Tsunami simulation parameters
		 */
		io_cGlHudConfig.insert(o.setupText("|--- Setup / Dataset parameters ---|"));
		io_cGlHudConfig.insert(o.setupInt("World scene", &simulation_world_scene_id, 1, -999, 999));
		io_cGlHudConfig.insert(o.setupInt("Terrain scene", &simulation_dataset_0_id, 1, -100, 100));
		io_cGlHudConfig.insert(o.setupInt("Water height scene", &simulation_dataset_1_id, 1, -100, 100));

		io_cGlHudConfig.insert(o.setupText("|--- EdgeComm parameters ---|"));
		io_cGlHudConfig.insert(o.setupFloat("Gravitation length (TODO): ", &simulation_parameter_gravitation, 0.1, -100, 0));
		io_cGlHudConfig.insert(o.setupFloatHI("Timestep size: ", &simulation_parameter_global_timestep_size, 0.01));
		io_cGlHudConfig.insert(o.setupFloatHI("CFL / -fixed timestepsize: ", &simulation_parameter_cfl, 0.001, -10, 10));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain size x: ", &simulation_dataset_default_domain_size_x, 0.01, 0, std::numeric_limits<T>::infinity()));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain size y: ", &simulation_dataset_default_domain_size_y, 0.01, 0, std::numeric_limits<T>::infinity()));
		io_cGlHudConfig.insert(o.setupFloatHI("Simulation domain size z: ", &simulation_dataset_default_domain_size_z, 0.01, 0, std::numeric_limits<T>::infinity()));
		io_cGlHudConfig.insert(o.setupFloatHI("Current simulation timestamp: ", &simulation_timestamp_for_timestep, 0.01));
		io_cGlHudConfig.insert(o.setupLongLong("Current simulation timestep: ", &simulation_timestep_nr, 1, 0, 999999999999));




		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- DATASET PARAMETERS ---|"));
		io_cGlHudConfig.insert(o.setupText("Generic Parameters:"));

		io_cGlHudConfig.insert(o.setupFloatHI(" + Bathymetry default distance: ", &simulation_dataset_default_nodal_values[0], 0.1, -std::numeric_limits<T>::max(), std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloat(" + Water surface default height: ", &simulation_dataset_default_nodal_values[1], 0.01, -std::numeric_limits<T>::max(), std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupText("Surface setup for column (1):"));
		io_cGlHudConfig.insert(o.setupFloat(" + X-Position: ", &simulation_dataset_breaking_dam_posx, 0.02, -std::numeric_limits<T>::max(), std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloat(" + Y-Position: ", &simulation_dataset_breaking_dam_posy, 0.02, -std::numeric_limits<T>::max(), std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloatHI(" + Radius: ", &simulation_dataset_breaking_dam_radius, 0.02, 0.000001, std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloat(" + Height: ", &simulation_dataset_breaking_dam_height, 0.02, -std::numeric_limits<T>::max(), std::numeric_limits<T>::max()));

		/************************
		 * Coarsen/Refine
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- ADAPTIVE PARAMETERS ---|"));

		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen threshold", &adaptive_coarsen_parameter_0, 0.01, 0.0000001, std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine threshold", &adaptive_refine_parameter_0, 0.02, 0.0000001, std::numeric_limits<T>::max()));

		io_cGlHudConfig.insert(o.setupFloatHI("Coarsen slope threshold (1st order)", &adaptive_coarsen_parameter_1, 0.02, 0.0000001, std::numeric_limits<T>::max()));
		io_cGlHudConfig.insert(o.setupFloatHI("Refine slope threshold (1st order)", &adaptive_refine_parameter_1, 0.02, 0.0000001, std::numeric_limits<T>::max()));



		/************************
		 * PARALLELIZATION
		 ************************/
		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- PARALLELIZATION ---|"));
		io_cGlHudConfig.insert(o.setupLongLong("Split Elements", &cluster_split_workload_size, 1, 2, std::numeric_limits<long long>::max()));
		io_cGlHudConfig.insert(o.setupLongLong("Join Elements", &cluster_join_workload_size, 1, 0, std::numeric_limits<long long>::max()));
		io_cGlHudConfig.insert(o.setupInt("Number of Threads to use", &simulation_threading_number_of_threads, 1, 1, std::numeric_limits<int>::max()));
	}

public:
	/**
	 * setup options for CGUI by adding them to the given configuration HUD class
	 */
	void setupCGlHudConfigVisualization(
			CGlHudConfig &io_cGlHudConfig	///< HUD class to add configuration parameters
	)
	{
		CGlHudConfig::COption o;

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("|--- Water Vis. CONTROL ---|"));

		io_cGlHudConfig.insert(o.setupLinebreak());
		io_cGlHudConfig.insert(o.setupText("GENERAL"));
		io_cGlHudConfig.insert(o.setupFloatHI("Visualization data 0 scale factor: ", &visualization_data0_scale_factor, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Visualization data 1 scale factor: ", &visualization_data1_scale_factor, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupLinebreak());

		io_cGlHudConfig.insert(o.setupFloatHI("Scale X: ", &visualization_scale_x, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Scale Y: ", &visualization_scale_y, 0.01, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloatHI("Scale Z: ", &visualization_scale_z, 0.01, -999999, 999999));

		io_cGlHudConfig.insert(o.setupFloat("translate X: ", &visualization_translate_x, 1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloat("translate Y: ", &visualization_translate_y, 1, -999999, 999999));
		io_cGlHudConfig.insert(o.setupFloat("translate Z: ", &visualization_translate_z, 1, -999999, 999999));

		io_cGlHudConfig.insert(o.setupLinebreak());

#if SIMULATION_MULTILAYER_ENABLED
		io_cGlHudConfig.insert(o.setupInt("multi-layer ID: ", &visualization_multi_layer_id, 1, 0, SIMULATION_NUMBER_OF_LAYERS-1));
#endif
	}



	/**
	 * setup callback handlers
	 */
	void setupCGuiCallbackHandlers(
			CGuiConfig &cGuiConfig
	)
	{
#if SIMULATION_HYPERBOLIC_PARALLEL
		{
			CGUICONFIG_CALLBACK_START(CParameters);
			c.updateClusterParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_global_timestep_size);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_gravitation);
			CGUICONFIG_CALLBACK_INSTALL(adaptive_refine_parameter_0);
			CGUICONFIG_CALLBACK_INSTALL(adaptive_coarsen_parameter_0);
		}

		{
			CGUICONFIG_CALLBACK_START(CParameters);
			c.simulation_parameter_adaptive_timestep_size_with_cfl = false;
			c.updateClusterParameters();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_global_timestep_size);
		}
#endif

		{
			CGUICONFIG_CALLBACK_START(CParameters);
			c.reset();
			CGUICONFIG_CALLBACK_END();
			CGUICONFIG_CALLBACK_INSTALL(simulation_dataset_default_domain_size_x);
			CGUICONFIG_CALLBACK_INSTALL(simulation_dataset_default_domain_size_z);
			CGUICONFIG_CALLBACK_INSTALL(simulation_parameter_base_triangulation_length_of_catheti_per_unit_domain);
			CGUICONFIG_CALLBACK_INSTALL(simulation_world_scene_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_dataset_0_id);
			CGUICONFIG_CALLBACK_INSTALL(simulation_dataset_1_id);
		}
	}
#endif



public:
	inline void outputVerboseInformation()
	{
		std::cout << " + Compile options:" << std::endl;
		std::cout << "   - Flux solver: " << SIMULATION_HYPERBOLIC_FLUX_SOLVER_ID_STRING << std::endl;
		std::cout << "   - RK ORDER: " << SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER << std::endl;
		std::cout << "   - Order of basis functions: " << SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE << std::endl;
		std::cout << "   - Bathymetry kernels: " << (SIMULATION_TSUNAMI_ENABLE_BATHYMETRY_KERNELS ? "[enabled]" : "[disabled]") << std::endl;
		std::cout << "   - Adaptivity mode: " << SIMULATION_HYPERBOLIC_ADAPTIVITY_MODE << std::endl;
		std::cout << "   - Gravitation: " << SIMULATION_HYPERBOLIC_DEFAULT_GRAVITATION << std::endl;
		std::cout << "   - Dry threshold: " << SIMULATION_TSUNAMI_DRY_THRESHOLD << std::endl;
		std::cout << "   - Zero threshold: " << SIMULATION_TSUNAMI_ZERO_THRESHOLD << std::endl;
		std::cout << std::endl;
		std::cout << " + Grid options:" << std::endl;
		std::cout << "   - initial recursion depth: " << grid_initial_recursion_depth << std::endl;
		std::cout << "   - min relative recursion depth: " << grid_min_relative_recursion_depth << std::endl;
		std::cout << "   - max relative recursion depth: " << grid_max_relative_recursion_depth << std::endl;
		std::cout << std::endl;
		std::cout << " + Parallelization options:" << std::endl;
		std::cout << "   - split size: " << cluster_split_workload_size << std::endl;
		std::cout << "   - join size: " << cluster_join_workload_size << std::endl;
		std::cout << std::endl;
		std::cout << " + Simulation options:" << std::endl;
		std::cout << "   - CFL / neg. fixed timestep: " << simulation_parameter_cfl << std::endl;
		std::cout << "   - Adaptive timestep size with CFL: " << (simulation_parameter_adaptive_timestep_size_with_cfl ? "[enabled]" : "[disabled]") << std::endl;
		std::cout << "   - Timestep size: " << simulation_parameter_global_timestep_size << std::endl;
		std::cout << "   - Minimum timestep size: " << simulation_parameter_minimum_timestep_size << std::endl;
		std::cout << "   - Gravitation: " << simulation_parameter_gravitation << std::endl;
		std::cout << "   - World Scene ID: " << simulation_world_scene_id << std::endl;
		std::cout << "   - Terrain Scene ID: " << simulation_dataset_0_id << std::endl;
		std::cout << "   - Displacement Scene ID: " << simulation_dataset_1_id << std::endl;
		std::cout << std::endl;
		std::cout << " + Adaptive:" << std::endl;
		std::cout << "   - conforming cluster skipping: " << (CONFIG_SIERPI_ADAPTIVE_CONFORMING_CLUSTER_SKIPPING_ACTIVE ? "[ACTIVE]" : "[DEACTIVATED]") << std::endl;
		std::cout << "   - refine threshold: " << adaptive_refine_parameter_0 << std::endl;
		std::cout << "   - coarsen threshold: " << adaptive_coarsen_parameter_0 << std::endl;
		std::cout << "   - domain boundary condition: " << CBoundaryConditions::getString((EBoundaryConditions)simulation_domain_boundary_condition) << std::endl;
	}



public:
	void reset()
	{
		simulation_timestep_nr = 0;
		simulation_timestamp_for_timestep = simulation_timestamp_for_initial_timestep;
	}

};

#endif
