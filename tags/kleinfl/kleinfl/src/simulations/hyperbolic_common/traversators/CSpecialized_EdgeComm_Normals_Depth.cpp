/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Mai 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#include "simulations/hyperbolic_common/subsimulation_generic/types/CTypes.hpp"
#include "../subsimulation_generic/kernels/simulation/CEdgeComm_Hyperbolic.hpp"
#include "CSpecialized_EdgeComm_Normals_Depth.hpp"
#include "../CParameters.hpp"


namespace sierpi
{
namespace travs
{


class cSpecialized_EdgeComm_Normals_Depth_Private	:
	public CFluxComm_Normals_Depth_ReturnAdaptivityRequests<sierpi::kernels::CEdgeComm_Hyperbolic, CHyperbolicTypes>
{
};

CSpecialized_EdgeComm_Normals_Depth::CSpecialized_EdgeComm_Normals_Depth()
{
	generic_traversator = new cSpecialized_EdgeComm_Normals_Depth_Private;
	cKernelClass = &(generic_traversator->cKernelClass);
}


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

void CSpecialized_EdgeComm_Normals_Depth::migration_send(int i_dst_rank)
{
	generic_traversator->migration_send(i_dst_rank);
}

void CSpecialized_EdgeComm_Normals_Depth::migration_send_postprocessing(int i_dst_rank)
{
	generic_traversator->migration_send_postprocessing(i_dst_rank);
}

void CSpecialized_EdgeComm_Normals_Depth::migration_recv(int i_src_rank)
{
	generic_traversator->migration_recv(i_src_rank);
}

#endif

CSpecialized_EdgeComm_Normals_Depth::~CSpecialized_EdgeComm_Normals_Depth()
{
	delete generic_traversator;
}

void CSpecialized_EdgeComm_Normals_Depth::actionFirstPass(
		CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks
)
{
	generic_traversator->action_FirstPass(io_cSimulationStacks);
}



void CSpecialized_EdgeComm_Normals_Depth::actionSecondPass_Serial(
		CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
		CONFIG_DEFAULT_FLOATING_POINT_TYPE i_timestep_size
)
{
	generic_traversator->action_SecondPass_Serial(io_cSimulationStacks, i_timestep_size);
}



void CSpecialized_EdgeComm_Normals_Depth::actionSecondPass_Parallel(
		CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
		CONFIG_DEFAULT_FLOATING_POINT_TYPE i_timestep_size
)
{
	generic_traversator->action_SecondPass_Parallel(io_cSimulationStacks, i_timestep_size);
}


void CSpecialized_EdgeComm_Normals_Depth::actionMiddlePass_computeClusterBorderCFL(
		CHyperbolicTypes::CSimulationStacks *io_cSimulationStacks,
		cSpecialized_EdgeComm_Normals_Depth_Private::TReduceValue *o_reduceValue
)
{
	generic_traversator->actionMiddlePass_computeClusterBorderCFL(io_cSimulationStacks, o_reduceValue);
}


void CSpecialized_EdgeComm_Normals_Depth::setParameters(
		T i_delta_timestep,
		T i_square_side_length,
#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
		T i_domain_height,
#endif
		T i_gravity,

		T i_refine_parameter,
		T i_coarsen_parameter,

		CDatasets *i_cDatasets
)
{
	generic_traversator->cKernelClass.setParameters(
			i_delta_timestep,
			i_square_side_length,
#if CONFIG_SUB_SIMULATION_EULER_MULTILAYER
			i_domain_height,
#endif
			i_gravity,
			i_refine_parameter,
			i_coarsen_parameter,
			i_cDatasets
		);
}



void CSpecialized_EdgeComm_Normals_Depth::setAdaptivityParameters(
		CONFIG_DEFAULT_FLOATING_POINT_TYPE i_refine_threshold,
		CONFIG_DEFAULT_FLOATING_POINT_TYPE i_coarsen_threshold
)
{
	generic_traversator->cKernelClass.setAdaptivityParameters(i_refine_threshold, i_coarsen_threshold);
}


void CSpecialized_EdgeComm_Normals_Depth::setup_Cluster(
		CSpecialized_EdgeComm_Normals_Depth &i_parent,
		CTriangle_Factory &i_triangleFactory
)
{
	generic_traversator->setup_Cluster(*(i_parent.generic_traversator), i_triangleFactory);
}


void CSpecialized_EdgeComm_Normals_Depth::setup_TraversatorParameters(
	int p_depth_limiter_min,
	int p_depth_limiter_max
)
{
	generic_traversator->setup_TraversatorParameters(
			p_depth_limiter_min,
			p_depth_limiter_max
		);
}




void CSpecialized_EdgeComm_Normals_Depth::setup_sfcMethods(
		CTriangle_Factory &i_triangleFactory
)
{
	generic_traversator->setup_sfcMethods(i_triangleFactory);
}


void CSpecialized_EdgeComm_Normals_Depth::setBoundaryCondition(
		EBoundaryConditions i_eBoundaryCondition
)
{
	generic_traversator->cKernelClass.eBoundaryCondition = i_eBoundaryCondition;
}


void CSpecialized_EdgeComm_Normals_Depth::setBoundaryDirichlet(
		const CHyperbolicTypes::CSimulationTypes::CEdgeData *i_value
)
{
	generic_traversator->cKernelClass.setBoundaryDirichlet(i_value);
}


CONFIG_DEFAULT_FLOATING_POINT_TYPE CSpecialized_EdgeComm_Normals_Depth::getTimestepSize()
{
	return generic_traversator->cKernelClass.getCFL1TimestepSize();
}


void CSpecialized_EdgeComm_Normals_Depth::setupMatrices(int i_verbosity_level)
{
	sierpi::kernels::CEdgeComm_Hyperbolic::setupMatrices(i_verbosity_level);
}

}
}
