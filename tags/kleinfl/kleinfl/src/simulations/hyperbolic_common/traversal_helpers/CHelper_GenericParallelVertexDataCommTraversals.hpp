/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CHelper_GenericParallelVertexDataCommTraversals.hpp
 *
 *  Created on: 02 March, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_VERTEXDATA_COMMTRAVERSALS_HPP_

#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
#	error "CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA not activated"
#endif

#include "global_config.h"
#include "libsierpi/generic_tree/CGeneric_TreeNode.hpp"
#include "libsierpi/cluster/CCluster_ExchangeVertexDataCommData.hpp"
#include "libsierpi/parallelization/CStackAccessorMethods.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * VERTEX COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) vertex communication
 *  c) last traversal
 */
class CHelper_GenericParallelVertexDataCommTraversals
{
	/**
	 * run the node comm traversals
	 */
public:
	template<
		typename CNodeData,						///< node data (vertex & normal for visualization)
		typename TVertexDataCommTraversator,	///< Traversator including kernel
		typename CSimulation_Cluster,			///< cluster data
		typename CStackAccessors,				///< accessors to adjacent stacks
		typename CKernelClass,					///< kernel class
		typename TAutoLambda1,					///< lambda function
		typename TAutoLambda2,					///< lambda function
		typename TAutoLambda3					///< lambda function
	>
	static void action(
			sierpi::CCluster_ExchangeVertexDataCommData<
				sierpi::CCluster_TreeNode<CSimulation_Cluster>,
				CNodeData,
				sierpi::CStackAccessorMethodsVisualizationNodeData<sierpi::CCluster_TreeNode<CSimulation_Cluster>, CNodeData>,
				CKernelClass
			>
			CSimulation_Cluster::*i_cCluster_ExchangeVertexDataCommData,		///< helper class to get exchangeable data
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,	///< domain cluster

			TAutoLambda1 i_node_traversator_kernel_setup_func,					///< lambda function to be called before node traversals (IMPORTANT: Care about signature!!!)
			TAutoLambda2 i_node_traversator_kernel_output_setup_func,			///< lambda function to be called before last node traversals to output vertices
			TAutoLambda3 i_node_traversator_kernel_postprocessing				///< lambda function for postprocessing
	)
	{
		typedef sierpi::CCluster_TreeNode<CSimulation_Cluster> CCluster_TreeNode_;
		typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

		/*
		 * first pass: setting all edges to type 'new' creates data to exchange with neighbors
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *cCluster_TreeNode = i_cGenericTreeNode->cCluster_TreeNode;

				// setup traversator
				TVertexDataCommTraversator cVertexCommTraversator;
				cVertexCommTraversator.setup_sfcMethods(cCluster_TreeNode->cTriangleFactory);

				// LAMBDA: SETUP
				i_node_traversator_kernel_setup_func(i_cGenericTreeNode, &cVertexCommTraversator);

				// setup stacks
				cCluster_TreeNode->cStacks->vertex_data_comm_left_edge_stack.clear();
				cCluster_TreeNode->cStacks->vertex_data_comm_right_edge_stack.clear();

				// TODO: clearing this buffer shouldn't be necessary
				cCluster_TreeNode->cStacks->vertex_data_comm_buffer.clear();
				cCluster_TreeNode->cStacks->structure_stacks.backward.clear();


				// run traversal
				cVertexCommTraversator.action_FirstPass(cCluster_TreeNode->cStacks);

			}
		);


		/*
		 * second pass: read vertex data
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[&](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// setup traversator
				TVertexDataCommTraversator cVertexCommTraversator;
				cVertexCommTraversator.setup_sfcMethods(node->cTriangleFactory);

				// LAMBDA: SETUP OUTPUT DATA
				size_t offset = i_node_traversator_kernel_output_setup_func(i_cGenericTreeNode, &cVertexCommTraversator, i_cGenericTreeNode->workload_in_subtree);

				/*
				 * INTER-PARTITION-VERTEX DATA COMMUNICATION
				 */
				/// handler for edge communication with adjacent clusters

				sierpi::CCluster_ExchangeVertexDataCommData<
						CCluster_TreeNode_,
						CNodeData,
						sierpi::CStackAccessorMethodsVisualizationNodeData<CCluster_TreeNode_, CNodeData>,
						CKernelClass
					> i_simulationVertexCommSubClass(
							node,
							&cVertexCommTraversator.cKernelClass
						);

				i_simulationVertexCommSubClass.pullAndComputeVertexCommData();


				/**
				 * NORMALIZATION
				 */

				// LEFT STACK
				node->cStacks->vertex_data_comm_exchange_left_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack());

				for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_left_edge_stack.getNumberOfElementsOnStack(); i++)
				{
					cVertexCommTraversator.cKernelClass.op_node_finishHimTouch(
							node->cStacks->vertex_data_comm_exchange_left_edge_stack.getElementPtrAtIndex(i)
						);
				}
				// RIGHT STACK
				node->cStacks->vertex_data_comm_exchange_right_edge_stack.setStackElementCounter(node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack());

				for (unsigned int i = 0; i < node->cStacks->vertex_data_comm_right_edge_stack.getNumberOfElementsOnStack(); i++)
				{
					cVertexCommTraversator.cKernelClass.op_node_finishHimTouch(
							node->cStacks->vertex_data_comm_exchange_right_edge_stack.getElementPtrAtIndex(i)
						);
				}



				/*
				 * run second pass and store output to output buffer
				 */
				cVertexCommTraversator.action_SecondPass_Parallel(node->cStacks);


				// LAMBDA: POSTPROCESSING
				i_node_traversator_kernel_postprocessing(i_cGenericTreeNode, offset);
			}

		);
	}
};

#endif
