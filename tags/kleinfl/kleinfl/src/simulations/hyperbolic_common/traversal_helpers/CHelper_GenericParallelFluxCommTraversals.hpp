/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 22, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_

#if SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==1
#	include "CHelper_GenericParallelFluxCommTraversals_RK1.hpp"
#elif SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==2
#	include "CHelper_GenericParallelFluxCommTraversals_RK2.hpp"
#elif SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==3
#	include "CHelper_GenericParallelFluxCommTraversals_RK3.hpp"
#elif SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER==4
#	include "CHelper_GenericParallelFluxCommTraversals_RK4.hpp"
#else
#error "RK of order " SIMULATION_HYPERBOLIC_RUNGE_KUTTA_ORDER " not implemented"
#endif

#endif /* CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_HPP_ */
