/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CHelper_GenericParallelFluxCommTraversals.hpp
 *
 *  Created on: Jul 29, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK1_HPP_
#define CHELPER_GENERICPARALLEL_FLUX_COMMTRAVERSALS_RK1_HPP_

#include "global_config.h"
#include "libsierpi/generic_tree/CGeneric_TreeNode.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"
#include "libsierpi/parallelization/CReduceOperators.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "libsierpi/cluster/CCluster_ExchangeFluxCommData.hpp"

/**
 * this class implements helper methods which abstract the different phases for
 * EDGE COMM TRAVERSALS
 *
 * in particular:
 *  a) first forward traversal
 *  b) edge communication
 *  c) last traversal
 */
class CHelper_GenericParallelFluxCommTraversals
{
public:

	/**
	 * run the edge comm traversals
	 */
	template<
		typename CCellData,				/// cell data
		typename CCluster_TreeNode_,	/// type of cluster tree node
		typename CSimulation_Cluster,	/// type of user-defined cluster handler

		typename TFluxCommTraversator,	/// Traversator including kernel to compute fluxes
		typename CEdgeData,				/// type of edge communication data

		typename CStackAccessors,		/// accessors to adjacent stacks
		typename T						/// value to use for reduction and timestep size
	>
	static void action(
			TFluxCommTraversator CSimulation_Cluster::*i_simulationSubClass,
			sierpi::CCluster_ExchangeFluxCommData<CCluster_TreeNode_, CEdgeData, CStackAccessors, typename TFluxCommTraversator::CKernelClass> CSimulation_Cluster::*i_simulationFluxCommSubClass,
			sierpi::CDomainClusters<CSimulation_Cluster> &i_cDomainClusters,

			T i_cfl_value,
			T *o_timestep_size
	)
	{
		typedef sierpi::CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

		/*********************************
		 * first pass:
		 *********************************
		 * 
		 * setting all edges to type 'new' creates data to exchange with neighbors
		 *
		 * There's no CFL to compute.
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// A) push conserved variables to stacks
					// B) compute local net updates
					(node->cSimulation_Cluster->*(i_simulationSubClass)).actionFirstPass(node->cStacks);

#if CONFIG_ENABLE_MPI && !CONFIG_ENABLE_THREADING
					(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pushEdgeCommData_DM_pass1();
#endif
				}
		);


#if CONFIG_ENABLE_MPI && CONFIG_ENABLE_THREADING
		/*
		 * In case of distributed memory systems, we first have to run a serial
		 * execution for MPI data exchange to send data to DM adjacent clusters.
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Serial(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using cCluster_UniqueIds as relation and compute the flux
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pushEdgeCommData_DM_pass1();
			}
		);

		/*
		 * After sending the data to adjacent clusters, we pull the corresponding data.
		 *
		 * This has to be executed in reversed order since the adjacent node send us
		 * the data in reversed order from the local node-point of view.
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge data only in one direction using cCluster_UniqueIds as relation and compute the flux
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
			}
		);
#endif


		T sm_cfl_reduce_value = std::numeric_limits<T>::infinity();


		/*********************************
		 * middle pass:
		 *********************************/

#if CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER

		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

				// pull edge communication data from adjacent sub-clusters and compute fluxes
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_singleFluxEvaluation_SM_pass1();
			}
		);
#endif



		/*********************************
		 * pull edge comm data & compute global timestep size:
		 *
		 * in case of a single flux evaluation, pull only considering the cluster ordering
		 */
#if CONFIG_ENABLE_MPI && !CONFIG_ENABLE_THREADING
		i_cDomainClusters.traverse_GenericTreeNode_Serial_Reversed_Reduce(
#else
		i_cDomainClusters.traverse_GenericTreeNode_Reduce_Parallel_Scan(
#endif
			[=](CGeneric_TreeNode_ *i_cGenericTreeNode, T *o_reduceValue)
			{
				CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;


				// pull edge communication data from adjacent sub-clusters and compute fluxes

				/*
				 * MPI STUFF:
				 * The MPI_Wait is executed during the shared memory pull pass.
				 */

#if CONFIG_ENABLE_MPI && !CONFIG_ENABLE_THREADING
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_DM_pass2();
#endif

#if CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER

				// pull edge communication data from adjacent clusters and mpi ranks. fluxes are already computed
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_singleFluxEvaluation_SM_pass2_and_if_DM_Wait();

#else

				// pull edge communication data from adjacent clusters and MPI ranks and compute fluxes
				(node->cSimulation_Cluster->*(i_simulationFluxCommSubClass)).pullEdgeCommData_doubleFluxEvaluation_SM_and_if_DM_Wait();

#endif

				T tmp;

				// run computation based on newly set-up stacks
				(node->cSimulation_Cluster->*(i_simulationSubClass)).actionMiddlePass_computeClusterBorderCFL(node->cStacks, &tmp);

				// store local timestep to cluster
				tmp *= i_cfl_value;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_SINGLE_FLUX_EVALUATION_BETWEEN_CLUSTER
				// include local timestep size since the fluxes are evaluated twice across MPI borders
				node->cSimulation_Cluster->local_timestep_size = std::min(tmp, node->cSimulation_Cluster->local_timestep_size);
#else
				node->cSimulation_Cluster->local_timestep_size = tmp;
#endif

				// return local timestep size
				*o_reduceValue = tmp;
			},
			&(sierpi::CReduceOperators::MIN<T>),	// use minimum since the minimum timestep has to be selected
			&sm_cfl_reduce_value
		);


		T t = sierpi::CGlobalComm::reduceMin(sm_cfl_reduce_value);

		if (i_cfl_value < 0)
			t = -i_cfl_value;

		*o_timestep_size = t;


		/*
		 * second pass
		 */
		i_cDomainClusters.traverse_GenericTreeNode_Parallel_Scan(
				[=](CGeneric_TreeNode_ *i_cGenericTreeNode)
				{
					CCluster_TreeNode_ *node = i_cGenericTreeNode->cCluster_TreeNode;

					// run computation based on newly set-up stacks
					(node->cSimulation_Cluster->*(i_simulationSubClass)).actionSecondPass_Parallel(node->cStacks, t);

					// advance timestamp for cluster
//					node->cSimulation_Cluster->local_timestamp_for_current_timestep += t;
				}
			);
	}
};

#endif /* CADAPTIVITYTRAVERSALS_HPP_ */
