/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CBASISFUNCTIONS2D_NODAL_HPP_
#define CBASISFUNCTIONS2D_NODAL_HPP_

#include <cmath>
#include <cassert>
#include <string.h>
#include <iostream>
#include <streambuf>
#include <stdlib.h>

#include "CDG_MatrixComputations_2D_Nodal_OnTheFly.hpp"
#include "libsierpi/pde_lab/CGaussQuadrature2D_TriangleArea.hpp"
#include "libmath/CMatrixOperations.hpp"
#include "simulations/hyperbolic_common/subsimulation_generic/CConfig.hpp"

template <int NUMBER_OF_BASIS_FUNCTIONS, int BASIS_FUNCTIONS_DEGREE>
class CBasisFunctions2D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

	T nodalCoords[NUMBER_OF_BASIS_FUNCTIONS*2];

	/**
	 * monomials:
	 *
	 * "Quadrature Formulas in Two Dimensions, Math 5172"
	 *
	 * span{ x^i, n^i, 0 <= i, 0 <= j, i+j <= N}
	 *
	 * 1, x, x^2, y, yx, y^2
	 */
	T exponent_x[NUMBER_OF_BASIS_FUNCTIONS];
	T exponent_y[NUMBER_OF_BASIS_FUNCTIONS];

	/**
	 * factor before each monomial
	 */
	T alpha[NUMBER_OF_BASIS_FUNCTIONS][NUMBER_OF_BASIS_FUNCTIONS];


public:
	/**
	 * setup basis function coefficients
	 */
	void setup(
			T *i_nodalCoords,	///< nodal coordinates
			int i_verbosity_level
	)
	{
		const int N = NUMBER_OF_BASIS_FUNCTIONS;

		// backup nodal coordinates
		memcpy(nodalCoords, i_nodalCoords, sizeof(T)*N*2);

		/*
		 * create exponent lookup list for monomial of degree BASIS_FUNCTIONS_DEGREE
		 */
		int index = 0;
		for (int iy = 0; iy <= BASIS_FUNCTIONS_DEGREE; iy++)
		{
			for (int ix = 0; ix+iy <= BASIS_FUNCTIONS_DEGREE; ix++)
			{
				exponent_x[index] = ix;
				exponent_y[index] = iy;
				index++;
			}
		}
		assert(index == NUMBER_OF_BASIS_FUNCTIONS);


		T matrix[N][N];
		T inv_matrix[N][N];

		/**
		 * assemble matrix entries with different evaluations of monomials
		 */
		for (int id = 0; id < N; id++)
			for (int monomial_id = 0; monomial_id < N; monomial_id++)
			{
//				std::cout << nodalCoords[id*2+0] << "^" << exponent_x[monomial_id] << " * " << nodalCoords[id*2+1] << "^" << exponent_y[monomial_id] << std::endl;
				matrix[id][monomial_id] = std::pow(nodalCoords[id*2+0], exponent_x[monomial_id])*std::pow(nodalCoords[id*2+1], exponent_y[monomial_id]);
			}

		T d = CMatrixOperations::getInverse<T,N>(matrix, inv_matrix);
		d = std::abs(d);

		if (i_verbosity_level >= 99)
		{
			std::cout << "Nodal points:" << std::endl;
			for (int i = 0; i < N; i++)
				std::cout << nodalCoords[i*2+0] << ", " << nodalCoords[i*2+1] << std::endl;
			std::cout << std::endl;
		}

		if (d < 0.00001)
		{
			std::cerr << "WARNING: BAD QUALITY (" << d << ") FOR MATRIX INVERSION FOR BASIS FUNCTIONS" << std::endl;

			std::cout << "Nodal points:" << std::endl;
			for (int i = 0; i < N; i++)
				std::cout << nodalCoords[i*2+0] << ", " << nodalCoords[i*2+1] << std::endl;
			std::cout << std::endl;

			std::cout << "Matrix:" << std::endl;
			CMatrixOperations::print<T,N>(matrix);
			std::cout << std::endl;

			std::cout << "Inverse Matrix:" << std::endl;
			CMatrixOperations::print<T,N>(inv_matrix);
			std::cout << std::endl;
		}

		/*
		 * store coefficients to alpha array (each row represents one coefficient array)
		 */
		for (int row = 0; row < N; row++)
			for (int col = 0; col < N; col++)
				alpha[col][row] = inv_matrix[row][col];


		/*
		 * this should produce an almost diagonal matrix
		 */
		for (int id = 0; id < N; id++)
		{
			for (int i = 0; i < N; i++)
			{
				T value = eval(id, nodalCoords[2*i+0], nodalCoords[2*i+1]);

				if (id == i)
					value -= 1.0;

#if 1
				if (	std::abs(value) > 0.001)
				{
					std::cerr << "function " << id << " evaluation at nodal point " << i << " exceeded threshold " << d << " with value " << value;
					exit(-1);
				}
#endif
			}
		}
	}

public:

	typedef T (*FBasisFunctions) (T x, T y);

	T eval(int id, T x, T y)
	{
		T result = 0;
		for (int i = 0; i < NUMBER_OF_BASIS_FUNCTIONS; i++)
			result += alpha[id][i]*std::pow(x, exponent_x[i])*std::pow(y, exponent_y[i]);

		return result;
	}


	T eval_dx(int id, T x, T y)
	{
		T result = 0;

		// start at 1
		for (int i = 1; i < NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T exp_x = exponent_x[i]-1;
			if (exp_x >= 0)
				result += alpha[id][i]*exponent_x[i]*std::pow(x, exp_x)*std::pow(y, exponent_y[i]);
		}

		return result;
	}

	T eval_dy(int id, T x, T y)
	{
		T result = 0;

		// start at 1
		for (int i = 1; i < NUMBER_OF_BASIS_FUNCTIONS; i++)
		{
			T exp_y = exponent_y[i]-1;
			if (exp_y >= 0)
				result += alpha[id][i]*std::pow(x, exponent_x[i])*exponent_y[i]*std::pow(y, exp_y);
		}

		return result;
	}

	T* getNodalCoords()
	{
		return nodalCoords;
	}

	int getNumberOfFunctions()
	{
		return NUMBER_OF_BASIS_FUNCTIONS;
	}
};


#endif /* CBASISFUNCTIONS_ORDER_2_HPP_ */
