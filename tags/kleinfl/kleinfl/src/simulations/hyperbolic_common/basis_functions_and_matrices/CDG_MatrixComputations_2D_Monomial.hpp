/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CDG_MATRIX_COMPUTATIONS_2D_MODAL_HPP_
#define CDG_MATRIX_COMPUTATIONS_2D_MODAL_HPP_


#if SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 0
#	include "matrices_2d_triangle_type_2_degree_0/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 1
#	include "matrices_2d_triangle_type_2_degree_1/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 2
#	include "matrices_2d_triangle_type_2_degree_2/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 3
#	include "matrices_2d_triangle_type_2_degree_3/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 4
#	include "matrices_2d_triangle_type_2_degree_4/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 5
#	include "matrices_2d_triangle_type_2_degree_5/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 6
#	include "matrices_2d_triangle_type_2_degree_6/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 7
#	include "matrices_2d_triangle_type_2_degree_7/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 8
#	include "matrices_2d_triangle_type_2_degree_8/CComputation2D_Matrices_Const.hpp"
#elif SIMULATION_HYPERBOLIC_BASIS_FUNCTIONS_DEGREE == 9
#	include "matrices_2d_triangle_type_2_degree_9/CComputation2D_Matrices_Const.hpp"
#else
#	error "unknown matrices id"
#endif


#endif /* CEDGECOMM_HYPERBOLIC_ORDER_1_MATRICES_HPP_ */
