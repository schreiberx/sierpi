
/*
 * Copyright (C) 2013 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Apr 19, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCOMPUTATION3D_MATRICES_CONST_HPP_
#define CCOMPUTATION3D_MATRICES_CONST_HPP_

#include <string.h>
#include <assert.h>

#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"

#define SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS (2)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS (10)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS (10)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS (3)
#define SIMULATION_HYPERBOLIC_NUMBER_OF_TOP_BOTTOM_FACE_DOFS (6)
#define SIMULATION_HYPERBOLIC_CFL	(0.5/(T)(2*2+1))
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_DEGREE	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS)
#define SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER	(SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS*2)

class CDG_MatrixComputations_3D
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;



public:
        static inline void mul_edge_comm_hyp_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,0>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_right_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,1>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }


public:
        static inline void mul_edge_comm_left_project_to_edge_space(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_FACE_DOFS; i++)
                {
                        /*
                         * rotate edge comm data to normal space
                         */
                        CTriangle_VectorProjections::toEdgeSpace<T,2>(
                                        &(io_hu[i]),
                                        &(io_hv[i])
                        );
                }
        }

public:
        static inline void mul_adaptivity_project_momentum_reference_to_left_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_left_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                        );
                }
        }



public:
        static inline void mul_adaptivity_project_momentum_reference_to_right_child(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i], &io_hv[i],
                                        -(T)M_SQRT1_2, (T)M_SQRT1_2
                                );
                }
        }

        static inline void mul_adaptivity_project_momentum_right_child_to_reference(
                        T io_hu[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS],
                        T io_hv[SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS]
        ) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
                for (int i = 0; i < SIMULATION_HYPERBOLIC_NUMBER_OF_CELL_DOFS; i++)
                {
                        CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
                                        &io_hu[i],	&io_hv[i],
                                        -(T)M_SQRT1_2, -(T)M_SQRT1_2
                        );
                }
        }



#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

public:
	static inline void mul_edge_comm_transform_to_edge_space(
		const T i_rotationMatrix[2][2],
		const T i_inverseTransformationMatrix[2][2],
		T io_hu[10],
		T io_hv[10]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			/*
			 * scale and shear from reference space
			 */
			CTriangle_VectorProjections::matrixTransformation<T>(
				i_inverseTransformationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

			/*
			 * rotate edge comm data to edge space
			 */
			CTriangle_VectorProjections::fromAnyEdgeToEdgeSpace<T>(
				i_rotationMatrix,
				&(io_hu[i]),
				&(io_hv[i])
			);

		}
	}
#endif


	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void mul_timestep_inv_mass(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{68.0000000,-48.49742262,13.41640786,-63.68673336,16.97056274,23.23790008,-63.68673336,16.97056274,18.97366595,23.23790008},
			{-48.49742262,96.,-46.47580015,16.97056274,-19.59591794,0.,16.97056274,-19.59591794,0.,0.},
			{13.41640786,-46.47580015,36.,0.,0.,0.,0.,0.,0.,0.},
			{-63.68673336,16.97056274,0.,112.,-13.85640646,-56.92099786,56.,-6.928203232,-30.98386677,-18.97366595},
			{16.97056274,-19.59591794,0.,-13.85640646,16.,0.,-6.928203232,8.,0.,0.},
			{23.23790008,0.,0.,-56.92099786,0.,36.,-18.97366595,0.,14.69693846,6.},
			{-63.68673336,16.97056274,0.,56.,-6.928203232,-18.97366595,112.,-13.85640646,-30.98386677,-56.92099786},
			{16.97056274,-19.59591794,0.,-6.928203232,8.,0.,-13.85640646,16.,0.,0.},
			{18.97366595,0.,0.,-30.98386677,0.,14.69693846,-30.98386677,0.,16.,14.69693846},
			{23.23790008,0.,0.,-18.97366595,0.,6.,-56.92099786,0.,14.69693846,36.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_inv_mass
	 *********************************************************/
public:
	static inline void madd_timestep_inv_mass(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{68.0000000,-48.49742262,13.41640786,-63.68673336,16.97056274,23.23790008,-63.68673336,16.97056274,18.97366595,23.23790008},
			{-48.49742262,96.,-46.47580015,16.97056274,-19.59591794,0.,16.97056274,-19.59591794,0.,0.},
			{13.41640786,-46.47580015,36.,0.,0.,0.,0.,0.,0.,0.},
			{-63.68673336,16.97056274,0.,112.,-13.85640646,-56.92099786,56.,-6.928203232,-30.98386677,-18.97366595},
			{16.97056274,-19.59591794,0.,-13.85640646,16.,0.,-6.928203232,8.,0.,0.},
			{23.23790008,0.,0.,-56.92099786,0.,36.,-18.97366595,0.,14.69693846,6.},
			{-63.68673336,16.97056274,0.,56.,-6.928203232,-18.97366595,112.,-13.85640646,-30.98386677,-56.92099786},
			{16.97056274,-19.59591794,0.,-6.928203232,8.,0.,-13.85640646,16.,0.,0.},
			{18.97366595,0.,0.,-30.98386677,0.,14.69693846,-30.98386677,0.,16.,14.69693846},
			{23.23790008,0.,0.,-18.97366595,0.,6.,-56.92099786,0.,14.69693846,36.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_u(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{3.162277659,2.738612787,2.357022604,3.872983346,3.354101966,3.674234614,1.936491673,1.677050983,3.,1.224744872},
			{2.581988897,2.236067977,1.924500898,1.581138830,1.369306394,1.,3.162277660,2.738612788,2.449489743,3.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_u
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_u(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{3.162277659,2.738612787,2.357022604,3.872983346,3.354101966,3.674234614,1.936491673,1.677050983,3.,1.224744872},
			{2.581988897,2.236067977,1.924500898,1.581138830,1.369306394,1.,3.162277660,2.738612788,2.449489743,3.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_v(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{2.581988897,2.236067977,1.924500898,3.162277660,2.738612788,3.,1.581138830,1.369306394,2.449489743,1.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{3.162277659,2.738612787,2.357022604,1.936491673,1.677050983,1.224744872,3.872983346,3.354101966,3.,3.674234614},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_v
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_v(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{2.581988897,2.236067977,1.924500898,3.162277660,2.738612788,3.,1.581138830,1.369306394,2.449489743,1.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{3.162277659,2.738612787,2.357022604,1.936491673,1.677050983,1.224744872,3.872983346,3.354101966,3.,3.674234614},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_hyp_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,3.073691594,.6000000000,1.341640786,4.312210572},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,1.732050808,.338104996,3.354101966,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,.3904100212,.7620999228e-1,1.341640786,.6956988644e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_hyp_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_hyp_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,3.073691594,.6000000000,1.341640786,4.312210572},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,1.732050808,.338104996,3.354101966,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,.3904100212,.7620999228e-1,1.341640786,.6956988644e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_left_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,.3904100212,.7620999228e-1,0.,.6956988644e-1},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,1.732050808,.338104996,0.,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,3.073691594,.6000000000,0.,4.312210572}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_left_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_left_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,.3904100212,.7620999228e-1,0.,.6956988644e-1},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,1.732050808,.338104996,0.,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,3.073691594,.6000000000,0.,4.312210572}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_right_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_right_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_right_face(
		const T i_input[10],
		T o_output[3]
	) {
		static const T matrix[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 3; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_hyp_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1543209877,-.2469135802,-.1543209877},
			{-.3012423004e-1,-.4819876804e-1,-.3012423004e-1},
			{-.4382991855e-2,-.7012786964e-2,-.4382991855e-2},
			{-.4260209467e-1,-.3024061411,-.3354055817},
			{-.8316142334e-2,-.5903119397e-1,-.6547285010e-1},
			{-.7591564582e-2,-.2390730460,-.4705545276},
			{-.3354055817,-.3024061411,-.4260209467e-1},
			{-.6547285010e-1,-.5903119397e-1,-.8316142334e-2},
			{-.1464017435,-.5856069739,-.1464017435},
			{-.4705545276,-.2390730460,-.7591564582e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_hyp_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_hyp_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1543209877,-.2469135802,-.1543209877},
			{-.3012423004e-1,-.4819876804e-1,-.3012423004e-1},
			{-.4382991855e-2,-.7012786964e-2,-.4382991855e-2},
			{-.4260209467e-1,-.3024061411,-.3354055817},
			{-.8316142334e-2,-.5903119397e-1,-.6547285010e-1},
			{-.7591564582e-2,-.2390730460,-.4705545276},
			{-.3354055817,-.3024061411,-.4260209467e-1},
			{-.6547285010e-1,-.5903119397e-1,-.8316142334e-2},
			{-.1464017435,-.5856069739,-.1464017435},
			{-.4705545276,-.2390730460,-.7591564582e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_left_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{-.3012423004e-1,-.2138334330,-.2371675613},
			{-.5880400639e-2,-.417413576e-1,-.4629629630e-1},
			{0.,0.,0.},
			{-.5368046793e-2,-.1690501721,-.3327322972}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_left_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_left_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{-.3012423004e-1,-.2138334330,-.2371675613},
			{-.5880400639e-2,-.417413576e-1,-.4629629630e-1},
			{0.,0.,0.},
			{-.5368046793e-2,-.1690501721,-.3327322972}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_right_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{-.2371675613,-.2138334330,-.3012423004e-1},
			{-.4629629630e-1,-.417413576e-1,-.5880400639e-2},
			{-.3327322972,-.1690501721,-.5368046793e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_right_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_right_fluxes_to_dofs(
		const T i_input[3],
		T o_output[10]
	) {
		static const T matrix[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{-.2371675613,-.2138334330,-.3012423004e-1},
			{-.4629629630e-1,-.417413576e-1,-.5880400639e-2},
			{-.3327322972,-.1690501721,-.5368046793e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 3; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.,1.936491673},
			{0.,.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-1.936491673,-1.581138830},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,1.936491673,-1.581138830},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.4082482906,0.,0.,0.,.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.,1.936491673},
			{0.,.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-1.936491673,-1.581138830},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,1.936491673,-1.581138830},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.4082482906,0.,0.,0.,.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_coarsen_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,1.224744872,0.,1.936491673,0.,0.,0.,0.},
			{0.,.5000000000,0.,0.,1.224744872,0.,0.,0.,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,.5000000000,0.,1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,-.5000000000,0.,-1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.4082482906,0.,0.,0.,-.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_coarsen_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_coarsen_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.5000000000,0.,0.,1.224744872,0.,1.936491673,0.,0.,0.,0.},
			{0.,.5000000000,0.,0.,1.224744872,0.,0.,0.,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,.5000000000,0.,1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,-.5000000000,0.,-1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.4082482906,0.,0.,0.,-.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,.5000000000,0.,0.,.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2041241452,0.,0.,0.,-.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_left
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_left(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,.5000000000,0.,0.,.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2041241452,0.,0.,0.,-.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void mul_adaptivity_refine_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000},
			{0.,0.,0.,.5000000000,0.,.7905694150,-.5000000000,0.,0.,-.7905694150},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.2041241452,0.,0.,0.,.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * adaptivity_refine_right
	 *********************************************************/
public:
	static inline void madd_adaptivity_refine_right(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000},
			{0.,0.,0.,.5000000000,0.,.7905694150,-.5000000000,0.,0.,-.7905694150},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.2041241452,0.,0.,0.,.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void mul_convert_poly_to_dofs(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.7071067810,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,.4082482906,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,.3162277660},
			{0.,0.,0.,.2886751347,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,.1666666667,0.},
			{0.,0.,0.,0.,0.,.1825741858,0.,0.,0.,0.},
			{0.,.2886751347,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,.1666666667,0.,0.},
			{0.,0.,0.,0.,.7453559923e-1,0.,0.,0.,0.,0.},
			{0.,0.,.1825741858,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_poly_to_dofs
	 *********************************************************/
public:
	static inline void madd_convert_poly_to_dofs(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{.7071067810,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,.4082482906,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,.3162277660},
			{0.,0.,0.,.2886751347,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,.1666666667,0.},
			{0.,0.,0.,0.,0.,.1825741858,0.,0.,0.,0.},
			{0.,.2886751347,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,.1666666667,0.,0.},
			{0.,0.,0.,0.,.7453559923e-1,0.,0.,0.,0.,0.},
			{0.,0.,.1825741858,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_poly(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.414213562,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,3.464101616,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,5.477225575},
			{0.,0.,0.,3.464101616,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,13.41640786,0.},
			{0.,0.,0.,0.,0.,5.477225575,0.,0.,0.,0.},
			{0.,2.449489743,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,6.,0.,0.},
			{0.,0.,0.,0.,6.,0.,0.,0.,0.,0.},
			{0.,0.,3.162277660,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_poly
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_poly(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{1.414213562,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,3.464101616,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,5.477225575},
			{0.,0.,0.,3.464101616,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,13.41640786,0.},
			{0.,0.,0.,0.,0.,5.477225575,0.,0.,0.,0.},
			{0.,2.449489743,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,6.,0.,0.},
			{0.,0.,0.,0.,6.,0.,0.,0.,0.,0.},
			{0.,0.,3.162277660,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void mul_convert_dofs_to_0th_order(
		const T i_input[10],
		T o_output[1]
	) {
		static const T matrix[1][10] = 
		{
			{1.414213562,1.224744872,1.054092553,1.154700539,1.,.9128709293,1.154700539,1.,1.118033988,.9128709293}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * convert_dofs_to_0th_order
	 *********************************************************/
public:
	static inline void madd_convert_dofs_to_0th_order(
		const T i_input[10],
		T o_output[1]
	) {
		static const T matrix[1][10] = 
		{
			{1.414213562,1.224744872,1.054092553,1.154700539,1.,.9128709293,1.154700539,1.,1.118033988,.9128709293}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 1; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_stiffness_w
	 *********************************************************/
public:
	static inline void mul_timestep_stiffness_w(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.732050808,1.500000000,1.290994449,1.414213562,1.224744872,1.118033988,1.414213562,1.224744872,1.369306394,1.118033988},
			{2.236067977,2.581988897,2.500000000,1.825741858,2.108185107,1.443375673,1.825741858,2.108185107,1.767766952,1.443375673},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,1.732050808,1.500000000,1.643167672,.8660254040,.7500000000,1.341640786,.5477225575},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,.8660254040,.7500000000,.5477225575,1.732050808,1.500000000,1.341640786,1.643167672},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_stiffness_w
	 *********************************************************/
public:
	static inline void madd_timestep_stiffness_w(
		const T i_input[10],
		T o_output[10]
	) {
		static const T matrix[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.732050808,1.500000000,1.290994449,1.414213562,1.224744872,1.118033988,1.414213562,1.224744872,1.369306394,1.118033988},
			{2.236067977,2.581988897,2.500000000,1.825741858,2.108185107,1.443375673,1.825741858,2.108185107,1.767766952,1.443375673},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,1.732050808,1.500000000,1.643167672,.8660254040,.7500000000,1.341640786,.5477225575},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,.8660254040,.7500000000,.5477225575,1.732050808,1.500000000,1.341640786,1.643167672},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_bottom_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_bottom_face(
		const T i_input[10],
		T o_output[6]
	) {
		static const T matrix[6][10] = 
		{
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,.3744798400,.6486181092,.6467832594,.6400829606e-1},
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,1.544810888,2.675690945,2.668121790,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3744798400,.6486181092,.6400829606e-1,1.544810888,2.675690945,.6467832594,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,2.829642998,4.901085438,1.003598394,3.654623760},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,.3172293092,.5494572811,.1125127183,.4593312490e-1},
			{1.414213562,2.449489743,3.162277660,2.829642998,4.901085438,3.654623760,.3172293092,.5494572811,1.003598394,.4593312490e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_bottom_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_bottom_face(
		const T i_input[10],
		T o_output[6]
	) {
		static const T matrix[6][10] = 
		{
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,.3744798400,.6486181092,.6467832594,.6400829606e-1},
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,1.544810888,2.675690945,2.668121790,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3744798400,.6486181092,.6400829606e-1,1.544810888,2.675690945,.6467832594,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,2.829642998,4.901085438,1.003598394,3.654623760},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,.3172293092,.5494572811,.1125127183,.4593312490e-1},
			{1.414213562,2.449489743,3.162277660,2.829642998,4.901085438,3.654623760,.3172293092,.5494572811,1.003598394,.4593312490e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_dofs_to_top_face
	 *********************************************************/
public:
	static inline void mul_timestep_dofs_to_top_face(
		const T i_input[10],
		T o_output[6]
	) {
		static const T matrix[6][10] = 
		{
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,.3744798400,0.,.6467832594,.6400829606e-1},
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,1.544810888,0.,2.668121790,1.089256160},
			{1.414213562,0.,0.,.3744798400,0.,.6400829606e-1,1.544810888,0.,.6467832594,1.089256160},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,2.829642998,0.,1.003598394,3.654623760},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,.3172293092,0.,.1125127183,.4593312490e-1},
			{1.414213562,0.,0.,2.829642998,0.,3.654623760,.3172293092,0.,1.003598394,.4593312490e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_dofs_to_top_face
	 *********************************************************/
public:
	static inline void madd_timestep_dofs_to_top_face(
		const T i_input[10],
		T o_output[6]
	) {
		static const T matrix[6][10] = 
		{
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,.3744798400,0.,.6467832594,.6400829606e-1},
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,1.544810888,0.,2.668121790,1.089256160},
			{1.414213562,0.,0.,.3744798400,0.,.6400829606e-1,1.544810888,0.,.6467832594,1.089256160},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,2.829642998,0.,1.003598394,3.654623760},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,.3172293092,0.,.1125127183,.4593312490e-1},
			{1.414213562,0.,0.,2.829642998,0.,3.654623760,.3172293092,0.,1.003598394,.4593312490e-1}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 6; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 10; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_bottom_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_bottom_fluxes_to_dofs(
		const T i_input[6],
		T o_output[10]
	) {
		static const T matrix[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{-.5471709127,-.5471709127,-.5471709127,-.2693256684,-.2693256684,-.2693256684},
			{-.7063946108,-.7063946108,-.7063946108,-.3476979428,-.3476979428,-.3476979428},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{-.5977000968,-.5977000968,-.1448893443,-.6041378615e-1,-.6041378615e-1,-.5388828899},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{-.1448893443,-.5977000968,-.5977000968,-.5388828899,-.6041378615e-1,-.6041378615e-1},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_bottom_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_bottom_fluxes_to_dofs(
		const T i_input[6],
		T o_output[10]
	) {
		static const T matrix[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{-.5471709127,-.5471709127,-.5471709127,-.2693256684,-.2693256684,-.2693256684},
			{-.7063946108,-.7063946108,-.7063946108,-.3476979428,-.3476979428,-.3476979428},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{-.5977000968,-.5977000968,-.1448893443,-.6041378615e-1,-.6041378615e-1,-.5388828899},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{-.1448893443,-.5977000968,-.5977000968,-.5388828899,-.6041378615e-1,-.6041378615e-1},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * timestep_top_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void mul_timestep_top_fluxes_to_dofs(
		const T i_input[6],
		T o_output[10]
	) {
		static const T matrix[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{0.,0.,0.,0.,0.,0.},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

	/*********************************************************
	 * timestep_top_fluxes_to_dofs
	 *********************************************************/
public:
	static inline void madd_timestep_top_fluxes_to_dofs(
		const T i_input[6],
		T o_output[10]
	) {
		static const T matrix[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{0.,0.,0.,0.,0.,0.},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < 10; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < 6; j++)
				o_output[i] += matrix[i][j]*i_input[j];
		}
	}

public:
	static inline int getNumberOfFlops()
	{
		return 4100;
	}


	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
public:
	static void setup(int i_verbosity_level)
	{
	}


public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 0" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
//		std::cout << " + Integration cell degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
//		std::cout << " + Integration edge degree: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;
		std::cout << std::endl;

		std::cout << std::endl;
		std::cout << "timestep_inv_mass:" << std::endl;
		static const T timestep_inv_mass[10][10] = 
		{
			{68.0000000,-48.49742262,13.41640786,-63.68673336,16.97056274,23.23790008,-63.68673336,16.97056274,18.97366595,23.23790008},
			{-48.49742262,96.,-46.47580015,16.97056274,-19.59591794,0.,16.97056274,-19.59591794,0.,0.},
			{13.41640786,-46.47580015,36.,0.,0.,0.,0.,0.,0.,0.},
			{-63.68673336,16.97056274,0.,112.,-13.85640646,-56.92099786,56.,-6.928203232,-30.98386677,-18.97366595},
			{16.97056274,-19.59591794,0.,-13.85640646,16.,0.,-6.928203232,8.,0.,0.},
			{23.23790008,0.,0.,-56.92099786,0.,36.,-18.97366595,0.,14.69693846,6.},
			{-63.68673336,16.97056274,0.,56.,-6.928203232,-18.97366595,112.,-13.85640646,-30.98386677,-56.92099786},
			{16.97056274,-19.59591794,0.,-6.928203232,8.,0.,-13.85640646,16.,0.,0.},
			{18.97366595,0.,0.,-30.98386677,0.,14.69693846,-30.98386677,0.,16.,14.69693846},
			{23.23790008,0.,0.,-18.97366595,0.,6.,-56.92099786,0.,14.69693846,36.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_inv_mass[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_inv_mass[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_u:" << std::endl;
		static const T timestep_stiffness_u[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{3.162277659,2.738612787,2.357022604,3.872983346,3.354101966,3.674234614,1.936491673,1.677050983,3.,1.224744872},
			{2.581988897,2.236067977,1.924500898,1.581138830,1.369306394,1.,3.162277660,2.738612788,2.449489743,3.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_u[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_u[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_v:" << std::endl;
		static const T timestep_stiffness_v[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{2.449489743,2.121320343,1.825741858,2.,1.732050808,1.581138830,2.,1.732050808,1.936491673,1.581138830},
			{2.121320343,2.449489743,2.371708245,1.732050808,2.,1.369306394,1.732050808,2.,1.677050983,1.369306394},
			{2.581988897,2.236067977,1.924500898,3.162277660,2.738612788,3.,1.581138830,1.369306394,2.449489743,1.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{3.162277659,2.738612787,2.357022604,1.936491673,1.677050983,1.224744872,3.872983346,3.354101966,3.,3.674234614},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_v[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_v[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_hyp_face:" << std::endl;
		static const T timestep_dofs_to_hyp_face[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,3.073691594,.6000000000,1.341640786,4.312210572},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,1.732050808,.338104996,3.354101966,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,.3904100212,.7620999228e-1,1.341640786,.6956988644e-1}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_hyp_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_hyp_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_left_face:" << std::endl;
		static const T timestep_dofs_to_left_face[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,.3904100212,.7620999228e-1,0.,.6956988644e-1},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,1.732050808,.338104996,0.,1.369306394},
			{1.414213562,.2760615734,.4016619266e-1,0.,0.,0.,3.073691594,.6000000000,0.,4.312210572}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_left_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_left_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_right_face:" << std::endl;
		static const T timestep_dofs_to_right_face[3][10] = 
		{
			{1.414213562,.2760615734,.4016619266e-1,3.073691594,.6000000000,4.312210572,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,1.732050808,.338104996,1.369306394,0.,0.,0.,0.},
			{1.414213562,.2760615734,.4016619266e-1,.3904100212,.7620999228e-1,.6956988644e-1,0.,0.,0.,0.}
		};
		for (int j = 0; j < 3; j++)
		{
			std::cout << timestep_dofs_to_right_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_right_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_hyp_fluxes_to_dofs:" << std::endl;
		static const T timestep_hyp_fluxes_to_dofs[10][3] = 
		{
			{-.1543209877,-.2469135802,-.1543209877},
			{-.3012423004e-1,-.4819876804e-1,-.3012423004e-1},
			{-.4382991855e-2,-.7012786964e-2,-.4382991855e-2},
			{-.4260209467e-1,-.3024061411,-.3354055817},
			{-.8316142334e-2,-.5903119397e-1,-.6547285010e-1},
			{-.7591564582e-2,-.2390730460,-.4705545276},
			{-.3354055817,-.3024061411,-.4260209467e-1},
			{-.6547285010e-1,-.5903119397e-1,-.8316142334e-2},
			{-.1464017435,-.5856069739,-.1464017435},
			{-.4705545276,-.2390730460,-.7591564582e-2}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_hyp_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_hyp_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_left_fluxes_to_dofs:" << std::endl;
		static const T timestep_left_fluxes_to_dofs[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{-.3012423004e-1,-.2138334330,-.2371675613},
			{-.5880400639e-2,-.417413576e-1,-.4629629630e-1},
			{0.,0.,0.},
			{-.5368046793e-2,-.1690501721,-.3327322972}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_left_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_left_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_right_fluxes_to_dofs:" << std::endl;
		static const T timestep_right_fluxes_to_dofs[10][3] = 
		{
			{-.1091214168,-.1745942669,-.1091214168},
			{-.2130104733e-1,-.3408167572e-1,-.2130104733e-1},
			{-.3099243261e-2,-.4958789216e-2,-.3099243261e-2},
			{-.2371675613,-.2138334330,-.3012423004e-1},
			{-.4629629630e-1,-.417413576e-1,-.5880400639e-2},
			{-.3327322972,-.1690501721,-.5368046793e-2},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.},
			{0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_right_fluxes_to_dofs[j][0];
			for (int i = 1; i < 3; i++)
			{
				std::cout << ", " << timestep_right_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_left:" << std::endl;
		static const T adaptivity_coarsen_left[10][10] = 
		{
			{.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.,1.936491673},
			{0.,.5000000000,0.,0.,0.,0.,0.,1.224744872,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,-1.936491673,-1.581138830},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000},
			{0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,1.936491673,-1.581138830},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.4082482906,0.,0.,0.,.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_coarsen_left[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_coarsen_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_coarsen_right:" << std::endl;
		static const T adaptivity_coarsen_right[10][10] = 
		{
			{.5000000000,0.,0.,1.224744872,0.,1.936491673,0.,0.,0.,0.},
			{0.,.5000000000,0.,0.,1.224744872,0.,0.,0.,0.,0.},
			{0.,0.,.5000000000,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,.5000000000,0.,1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,-1.224744872,.5000000000},
			{0.,0.,0.,-.5000000000,0.,-1.581138830,-.5000000000,0.,-1.936491673,0.},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.4082482906,0.,0.,0.,-.4082482906},
			{0.,0.,0.,0.,0.,.5000000000,0.,0.,1.224744872,.5000000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_coarsen_right[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_coarsen_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_left:" << std::endl;
		static const T adaptivity_refine_left[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,.5000000000,0.,0.,.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2041241452,0.,0.,0.,-.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_refine_left[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_refine_left[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "adaptivity_refine_right:" << std::endl;
		static const T adaptivity_refine_right[10][10] = 
		{
			{1.,0.,0.,1.224744872,0.,.9682458365,1.224744872,0.,2.371708244,.9682458365},
			{0.,1.,0.,0.,1.224744872,0.,0.,1.224744872,0.,0.},
			{0.,0.,1.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,-.5000000000,0.,-.7905694150,-.5000000000,0.,-1.936491673,-.7905694150},
			{0.,0.,0.,0.,-.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,.6123724358,.2500000000},
			{0.,0.,0.,.5000000000,0.,.7905694150,-.5000000000,0.,0.,-.7905694150},
			{0.,0.,0.,0.,.5000000000,0.,0.,-.5000000000,0.,0.},
			{0.,0.,0.,0.,0.,-.2041241452,0.,0.,0.,.2041241452},
			{0.,0.,0.,0.,0.,.2500000000,0.,0.,-.6123724358,.2500000000}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << adaptivity_refine_right[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << adaptivity_refine_right[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_poly_to_dofs:" << std::endl;
		static const T convert_poly_to_dofs[10][10] = 
		{
			{.7071067810,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,.4082482906,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,.3162277660},
			{0.,0.,0.,.2886751347,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,.1666666667,0.},
			{0.,0.,0.,0.,0.,.1825741858,0.,0.,0.,0.},
			{0.,.2886751347,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,.1666666667,0.,0.},
			{0.,0.,0.,0.,.7453559923e-1,0.,0.,0.,0.,0.},
			{0.,0.,.1825741858,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << convert_poly_to_dofs[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_poly_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_poly:" << std::endl;
		static const T convert_dofs_to_poly[10][10] = 
		{
			{1.414213562,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,3.464101616,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,5.477225575},
			{0.,0.,0.,3.464101616,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,13.41640786,0.},
			{0.,0.,0.,0.,0.,5.477225575,0.,0.,0.,0.},
			{0.,2.449489743,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,6.,0.,0.},
			{0.,0.,0.,0.,6.,0.,0.,0.,0.,0.},
			{0.,0.,3.162277660,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << convert_dofs_to_poly[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_dofs_to_poly[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "convert_dofs_to_0th_order:" << std::endl;
		static const T convert_dofs_to_0th_order[1][10] = 
		{
			{1.414213562,1.224744872,1.054092553,1.154700539,1.,.9128709293,1.154700539,1.,1.118033988,.9128709293}
		};
		for (int j = 0; j < 1; j++)
		{
			std::cout << convert_dofs_to_0th_order[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << convert_dofs_to_0th_order[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_stiffness_w:" << std::endl;
		static const T timestep_stiffness_w[10][10] = 
		{
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.732050808,1.500000000,1.290994449,1.414213562,1.224744872,1.118033988,1.414213562,1.224744872,1.369306394,1.118033988},
			{2.236067977,2.581988897,2.500000000,1.825741858,2.108185107,1.443375673,1.825741858,2.108185107,1.767766952,1.443375673},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,1.732050808,1.500000000,1.643167672,.8660254040,.7500000000,1.341640786,.5477225575},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{1.414213562,1.224744872,1.054092553,.8660254040,.7500000000,.5477225575,1.732050808,1.500000000,1.341640786,1.643167672},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_stiffness_w[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_stiffness_w[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_bottom_face:" << std::endl;
		static const T timestep_dofs_to_bottom_face[6][10] = 
		{
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,.3744798400,.6486181092,.6467832594,.6400829606e-1},
			{1.414213562,2.449489743,3.162277660,1.544810888,2.675690945,1.089256160,1.544810888,2.675690945,2.668121790,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3744798400,.6486181092,.6400829606e-1,1.544810888,2.675690945,.6467832594,1.089256160},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,2.829642998,4.901085438,1.003598394,3.654623760},
			{1.414213562,2.449489743,3.162277660,.3172293092,.5494572811,.4593312490e-1,.3172293092,.5494572811,.1125127183,.4593312490e-1},
			{1.414213562,2.449489743,3.162277660,2.829642998,4.901085438,3.654623760,.3172293092,.5494572811,1.003598394,.4593312490e-1}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_dofs_to_bottom_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_bottom_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_dofs_to_top_face:" << std::endl;
		static const T timestep_dofs_to_top_face[6][10] = 
		{
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,.3744798400,0.,.6467832594,.6400829606e-1},
			{1.414213562,0.,0.,1.544810888,0.,1.089256160,1.544810888,0.,2.668121790,1.089256160},
			{1.414213562,0.,0.,.3744798400,0.,.6400829606e-1,1.544810888,0.,.6467832594,1.089256160},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,2.829642998,0.,1.003598394,3.654623760},
			{1.414213562,0.,0.,.3172293092,0.,.4593312490e-1,.3172293092,0.,.1125127183,.4593312490e-1},
			{1.414213562,0.,0.,2.829642998,0.,3.654623760,.3172293092,0.,1.003598394,.4593312490e-1}
		};
		for (int j = 0; j < 6; j++)
		{
			std::cout << timestep_dofs_to_top_face[j][0];
			for (int i = 1; i < 10; i++)
			{
				std::cout << ", " << timestep_dofs_to_top_face[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_bottom_fluxes_to_dofs:" << std::endl;
		static const T timestep_bottom_fluxes_to_dofs[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{-.5471709127,-.5471709127,-.5471709127,-.2693256684,-.2693256684,-.2693256684},
			{-.7063946108,-.7063946108,-.7063946108,-.3476979428,-.3476979428,-.3476979428},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{-.5977000968,-.5977000968,-.1448893443,-.6041378615e-1,-.6041378615e-1,-.5388828899},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{-.1448893443,-.5977000968,-.5977000968,-.5388828899,-.6041378615e-1,-.6041378615e-1},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_bottom_fluxes_to_dofs[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_bottom_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << std::endl;
		std::cout << "timestep_top_fluxes_to_dofs:" << std::endl;
		static const T timestep_top_fluxes_to_dofs[10][6] = 
		{
			{-.3159092737,-.3159092737,-.3159092737,-.1554952471,-.1554952471,-.1554952471},
			{0.,0.,0.,0.,0.,0.},
			{0.,0.,0.,0.,0.,0.},
			{-.3450823119,-.3450823119,-.8365190197e-1,-.3487991569e-1,-.3487991569e-1,-.3111241817},
			{0.,0.,0.,0.,0.,0.},
			{-.2433197725,-.2433197725,-.1429827493e-1,-.5050427176e-2,-.5050427176e-2,-.4018322549},
			{-.8365190197e-1,-.3450823119,-.3450823119,-.3111241817,-.3487991569e-1,-.3487991569e-1},
			{0.,0.,0.,0.,0.,0.},
			{-.1444794727,-.5960092869,-.1444794727,-.1103473935,-.1237096956e-1,-.1103473935},
			{-.1429827493e-1,-.2433197725,-.2433197725,-.4018322549,-.5050427176e-2,-.5050427176e-2}
		};
		for (int j = 0; j < 10; j++)
		{
			std::cout << timestep_top_fluxes_to_dofs[j][0];
			for (int i = 1; i < 6; i++)
			{
				std::cout << ", " << timestep_top_fluxes_to_dofs[j][i];
			}
			std::cout << std::endl;
		}

	
		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif
