/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Sep 1, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CComputation2D_Matrices_CONST_HPP_
#define CComputation2D_Matrices_CONST_HPP_

#include <string.h>

#include "CComputation2D_Matrices_Const_Config.hpp"
#include "../../subsimulation_generic/CConfig.hpp"
#include "../../subsimulation_generic/types/CTypes.hpp"
#include "libmath/CMatrixOperations.hpp"


class CComputation2D_Matrices
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;
	static const int N = SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS;
	static const int EN = SIMULATION_HYPERBOLIC_NUMBER_OF_EDGE_DOFS;

public:

	static inline int getNumberOfFlops()
	{
		int single_quantity_flops =
				N*N	+	/// mass matrix
				N*N +	/// stiffness
				N*N +	/// stiffness
				EN*N*3		+	// edge comm
				N*EN*3		+	// flux quadrature
				0;

		// multiply with 3 since we have 3 conserved quantities
		single_quantity_flops *= 3;

		// multiply with 2 since we have an add operation per multiplication
		single_quantity_flops *= 2;

		return single_quantity_flops;
	}



	/*********************************************************
	 * INV MASS
	 *********************************************************/
	static inline void mul_inv_mass(
			const T i_input[N],
			T o_output[N]
	) {
		static const T inv_mass_matrix[N][N] =
		{
			{	6,	0,	0},
			{	0,	6,	0},
			{	0,	0,	6}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += inv_mass_matrix[i][j]*i_input[j];
		}
	}


	static inline void madd_inv_mass(
			const T i_input[N],
			T o_output[N]
	) {
		static const T inv_mass_matrix[N][N] =
		{
				{	6,	0,	0},
				{	0,	6,	0},
				{	0,	0,	6}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += inv_mass_matrix[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS X
	 *********************************************************/
	static inline void mul_stiffness_x(
			const T i_input[N],
			T o_output[N]
	) {
		static const T stiffness_x[N][N] =
		{
				{	0.333333,	0.333333,	0.333333	},
				{	-0.333333,	-0.333333,	-0.333333	},
				{	0,			0,			0			}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += stiffness_x[i][j]*i_input[j];
		}
	}



	/*********************************************************
	 * STIFFNESS Y
	 *********************************************************/
	static inline void madd_stiffness_y(
			const T i_input[N],
			T o_output[N]
	) {
		static const T stiffness_y[N][N] =
		{
				{	0.333333,	0.333333,	0.333333	},
				{	0,			0,			0			},
				{	-0.333333,	-0.333333,	-0.333333	},
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i] += stiffness_y[i][j]*i_input[j];
		}
	}


	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX HYP
	 *********************************************************/

public:
	static inline void mul_edge_comm_hyp_data_nodal_weight_matrix(
			const T i_input[N],
			T o_output[EN]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[EN][N] =
		{
				{	1,	-0.57735,	0.57735		},
				{	1,	0.57735,	-0.57735	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_hyp_project_to_edge_space(
			T io_hu[EN],
			T io_hv[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,0>(
				&(io_hu[i]),
				&(io_hv[i])
			);
		}
	}



	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX RIGHT
	 *********************************************************/

public:
	static inline void mul_edge_comm_right_data_nodal_weight_matrix(
			const T i_input[N],
			T o_output[EN]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[EN][N] =
		{
				{0.57735,	1,	-0.57735	},
				{-0.57735,	1,	0.57735		}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_right_project_to_edge_space(
			T io_hu[EN],
			T io_hv[EN]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,1>(
				&(io_hu[i]),
				&(io_hv[i])
			);
		}
	}



	/*********************************************************
	 * EDGE COMM DATA NODAL WEIGHT MATRIX LEFT
	 *********************************************************/

public:
	static inline void mul_edge_comm_left_data_nodal_weight_matrix(
			const T i_input[N],
			T o_output[EN]
	) {
		// edge comm
		static const T edge_comm_data_nodal_weight_matrix[EN][N] =
		{
				{	-0.57735,	0.57735,	1	},
				{	0.57735,	-0.57735,	1	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= edge_comm_data_nodal_weight_matrix[i][j] * i_input[j];
		}
	}


public:
	static inline void mul_edge_comm_left_project_to_edge_space(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < EN; i++)
		{
			/*
			 * rotate edge comm data to normal space
			 */
			CTriangle_VectorProjections::toEdgeSpace<T,2>(
				&(io_hu[i]),
				&(io_hv[i])
			);
		}
	}





	/*********************************************************
	 * FLUX QUADRATURE
	 *********************************************************/
	static inline void madd_edge_hyp_flux_quadrature_weight_matrix(
			const T i_input[EN],
			T o_output[N]
	) {
		static const T edge_flux_quadrature_weight_matrix[N][EN] =
		{
				{	-0.707107,	-0.707107	},
				{	0.408248,	-0.408248	},
				{	-0.408248,	0.408248	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}


	static inline void madd_edge_right_flux_quadrature_weight_matrix(
			const T i_input[EN],
			T o_output[N]
	) {
		static const T edge_flux_quadrature_weight_matrix[N][EN] =
		{
				{	-0.288675,	0.288675	},
				{	-0.5,		-0.5		},
				{	0.288675,	-0.288675	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}


	static inline void madd_edge_left_flux_quadrature_weight_matrix(
			const T i_input[EN],
			T o_output[N]
	) {
		static const T edge_flux_quadrature_weight_matrix[N][EN] =
		{
				{	0.288675,	-0.288675	},
				{	-0.288675,	0.288675	},
				{	-0.5,		-0.5		}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < EN; j++)
				o_output[i]	+= edge_flux_quadrature_weight_matrix[i][j] * i_input[j];
		}
	}


	/*********************************************************
	 * ADAPTIVITY REFINEMENT
	 *********************************************************/

public:
	static inline void mul_adaptivity_refine_left_child_matrix(
			const T i_input[N],
			T o_output[N]
	) {
		// adaptive
		static const T adaptivity_refine_left_child_matrix[N][N] =
		{
				{	0,	0,		1	},
				{	1,	-0.5,	0.5	},
				{	0,	0.5,	0.5	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_refine_left_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_left_child(
			T io_hu[N],
			T io_hv[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);
		}
	}



public:
	static inline void mul_adaptivity_refine_right_child_matrix(
			const T i_input[N],
			T o_output[N]
	) {
		// adaptive
		static const T adaptivity_refine_right_child_matrix[N][N] =
		{
				{	0,	1,		0		},
				{	0,	0.5,	0.5		},
				{	1,	0.5,	-0.5	}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_refine_right_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_reference_to_right_child(
			T io_hu[N],
			T io_hv[N]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);
		}
	}



	/*********************************************************
	 * ADAPTIVITY COARSENING
	 *********************************************************/

public:
	static inline void mul_adaptivity_coarsen_left_child_matrix(
			const T i_input[N],
			T o_output[N]
	) {
		// adaptive
		static const T adaptivity_coarsen_left_child_matrix[N][N] =
		{
				{	-0.5,	0.5,	0.5		},
				{	0,		0,		0		},
				{	1,		0,		0		}
		};

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_coarsen_left_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_left_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					(T)M_SQRT1_2
				);
		}
	}



public:
	static inline void mul_adaptivity_coarsen_right_child_matrix(
			const T i_input[],
			T o_output[]
	) {
		// adaptive
		static const T adaptivity_coarsen_right_child_matrix[N][N] =
		{
				{	-0.5,	0.5,	0.5		},
				{	1,		0,		0		},
				{	0,		0,		0		}
		};


#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			o_output[i] = 0;

#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
			for (int j = 0; j < N; j++)
				o_output[i]		+= adaptivity_coarsen_right_child_matrix[i][j] * i_input[j];
		}
	}



public:
	static inline void mul_adaptivity_project_momentum_right_child_to_reference(
			T io_hu[],
			T io_hv[]
	) {
#if CONFIG_ENABLE_INTEL_SIMD_PRAGMA
#pragma simd
#endif
		for (int i = 0; i < N; i++)
		{
			CTriangle_VectorProjections::changeFromReferenceElementToBasisWithXAxis(
					&io_hu[i],
					&io_hv[i],
					-(T)M_SQRT1_2,
					-(T)M_SQRT1_2
				);
		}
	}



	/**
	 * setup the matrices for the static variables
	 *
	 * this method has to be invoked once during startup
	 */
	static void setup(int i_verbosity_level)
	{
	}



public:
	static void debugOutput(int i_verbosity_level)
	{
		std::cout << "**********************************************" << std::endl;
		std::cout << "* COMPUTATION MATRICES CONST 1" << std::endl;
		std::cout << "**********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "Information about System DOFs and matrices:" << std::endl;
		std::cout << " + Basis function degree: " << SIMULATION_HYPERBOLIC_DEGREE_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Number of basis functions: " << SIMULATION_HYPERBOLIC_NUMBER_OF_BASIS_FUNCTIONS << std::endl;
		std::cout << " + Integration cell order: " << SIMULATION_HYPERBOLIC_INTEGRATION_CELL_ORDER << std::endl;
		std::cout << " + Integration edge order: " << SIMULATION_HYPERBOLIC_INTEGRATION_EDGE_ORDER << std::endl;

		std::cout << "Flops per cell update using matrix multiplications for dense matrices: " << getNumberOfFlops() << std::endl;
		std::cout << std::endl;
	}
};

#endif /* CComputation2D_Matrices_CONST_HPP */
