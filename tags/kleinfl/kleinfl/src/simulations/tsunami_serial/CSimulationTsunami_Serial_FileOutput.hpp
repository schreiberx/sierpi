/*
 * CSimulationTsunami_Serial_FileOutput.hpp
 *
 *  Created on: Mar 18, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSIMULATIONTSUNAMI_SERIAL_FILEOUTPUT_HPP_
#define CSIMULATIONTSUNAMI_SERIAL_FILEOUTPUT_HPP_

#include "lib/CVtkXMLTrianglePolyData.hpp"

class CSimulationTsunami_Serial_FileOutput	:
	public CSimulation_MainInterface_FileOutput
{
	CParameters &cParameters;

	sierpi::CTriangle_Factory &cTriangleFactory;

	CDatasets &cDatasets;


	/**
	 * constructor for parallel hyperbolic simulation
	 */
public:
	CSimulationTsunami_Serial_FileOutput(
			CParameters &i_cParameters,
			sierpi::CTriangle_Factory &i_cTriangleFactory,
			CDatasets &i_cDatasets
	)	:
			cParameters(i_cParameters),
			cTriangleFactory(i_cTriangleFactory),
			cDatasets(i_cDatasets)
#if CONFIG_WRITER_THREAD_WITH_PTHREAD
			,
			pthread_id(0),
			cWriterTask(nullptr)
#endif
	{
	}

	void writeSimulationDataToFile(
			const char *i_filename,
			const char *i_information_string = nullptr
	)
	{
		CGridDataArrays<3,6> cGridDataArrays(cParameters.number_of_local_cells);

		sierpi::kernels::COutputGridDataArrays<3>::TRAV cOutputGridDataArrays;

		int flags =
				CGridDataArrays_Enums::VERTICES	|
				CGridDataArrays_Enums::VALUE0	|
				CGridDataArrays_Enums::VALUE1	|
				CGridDataArrays_Enums::VALUE2	|
				CGridDataArrays_Enums::VALUE3	|
				CGridDataArrays_Enums::VALUE4;

		cOutputGridDataArrays.setup_sfcMethods(cTriangleFactory);
		cOutputGridDataArrays.cKernelClass.setup(
				&cGridDataArrays,
				&cDatasets,
				0,
				flags,
				0
			);


		CVtkXMLTrianglePolyData cVtkXMLTrianglePolyData;

		cVtkXMLTrianglePolyData.setup();
		cVtkXMLTrianglePolyData.setTriangleVertexCoords(cGridDataArrays.triangle_vertex_buffer, cGridDataArrays.number_of_triangle_cells);
		cVtkXMLTrianglePolyData.setCellDataFloat("h", cGridDataArrays.dof_element[0]);
		cVtkXMLTrianglePolyData.setCellDataFloat("hu", cGridDataArrays.dof_element[1]);
		cVtkXMLTrianglePolyData.setCellDataFloat("hv", cGridDataArrays.dof_element[2]);
		cVtkXMLTrianglePolyData.setCellDataFloat("b", cGridDataArrays.dof_element[3]);
		cVtkXMLTrianglePolyData.setCellDataFloat("cfl_value_hint", cGridDataArrays.dof_element[4]);

		cVtkXMLTrianglePolyData.write(i_filename);
	}


	void writeSimulationClustersDataToFile(
		const char *i_filename,
		const char *i_information_string = nullptr
	)
	{
		std::cout << "writeSimulationClustersDataToFile() not implemented (meaningless for serial version)" << std::endl;
	}

};


#endif /* CSIMULATIONTSUNAMI_SERIAL_FILEOUTPUT_HPP_ */
