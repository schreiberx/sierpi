/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CDiscontinous2DSurfaceToClosedSurface.hpp.hpp
 *
 *  Created on: Nov 25, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CDISCONTINOUSTOCLOSEDSURFACE_HPP_
#define CDISCONTINOUSTOCLOSEDSURFACE_HPP_

#include <cmath>

#include "tbb/parallel_sort.h"


/**
 * This class takes an array of
 *
 * - vertex data
 * - normal data
 *
 * and modified this arrays so that
 *
 * 1. All vertices which are on the same y-axis are
 *    averaged to the same y-axis point.
 *
 * 2. The normal is computed based on the vertex data.
 */
template <typename T>
class CDiscontinous2DSurfaceToClosedSurface
{
public:
	void action(
		T *io_vertices,
		T *io_normals,
		size_t number_of_triangles
	)
	{
		size_t number_of_vertices = number_of_triangles*3;

		std::vector<size_t> vertex_indices;
		vertex_indices.resize(number_of_vertices);

		/*
		 * STEP 1) vertex indices
		 *
		 * We start by sorting the vertex indices to know which
		 * vertices are shared with other triangles.
		 *
		 * First of all, we setup the vertex indices.
		 */
		for (int i = 0; i < number_of_triangles; i++)
		{
			/*
			 * setup vertex indices
			 */
			vertex_indices[i*3+0] = i*3+0;
			vertex_indices[i*3+1] = i*3+1;
			vertex_indices[i*3+2] = i*3+2;
		}



		/*
		 * STEP 2) sort vertex indices
		 */
		class RandomAccessIterator
		{
			T *vertices;
			T *vertex_indices;
			int n;

public:
			RandomAccessIterator(
					T *io_vertices,
					T *i_vertex_indices,
					int i_n
			)	:
				vertices(io_vertices),
				vertex_indices(i_vertex_indices),
				n(i_n)
			{
			}
		};

		RandomAccessIterator first(io_vertices, vertex_indices, 0);
		RandomAccessIterator last(io_vertices, vertex_indices, number_of_vertices-1);

		class Compare
		{
		public:
			bool operator()(
					RandomAccessIterator &i_a,
					RandomAccessIterator &i_b
			) const
			{
				return true;
			}
		};

		/*
		 * Now we sort the vertex indices by using the vertex coordinates
		 *
		 * Complexity: O(n log n)
		 */

		tbb::parallel_sort(first, last, Compare());




		/*
		 * We start by computing the face normal for each triangle
		 *
		 * Complexity: O(n)
		 */
		for (int i = 0; i < number_of_triangles; i++)
		{
			T *v = &(io_vertices[i*3*3]);
			T *n = &(io_normals[i*3*3]);

			/*
			 * setup normal
			 */
			T d1[3] = {
					v[1*3+0] - v[0*3+0],
					v[1*3+1] - v[0*3+1],
					v[1*3+2] - v[0*3+2]
			};

			T d2[3] = {
					v[2*3+0] - v[0*3+0],
					v[2*3+1] - v[0*3+1],
					v[2*3+2] - v[0*3+2]
			};

			n[0] = d1[1]*d2[2] - d1[2]*d2[1];
			n[1] = d1[2]*d2[0] - d1[0]*d2[2];
			n[2] = d1[0]*d2[1] - d1[1]*d2[0];

			T inv_f = (T)1.0 / std::sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2]);

			n[0] *= inv_f;
			n[1] *= inv_f;
			n[2] *= inv_f;

			/*
			 * setup vertex indices
			 */
			vertex_indices[i*3+0] = i*3+0;
			vertex_indices[i*3+1] = i*3+1;
			vertex_indices[i*3+2] = i*3+2;
		}
	}
};


#if 1
typedef float T;
#include "../CDiscontinous2DSurfaceToClosedSurface.hpp"

int main()
{
        CDiscontinous2DSurfaceToClosedSurface<T> d;

        T v[3*2][3] = {
                {0,0,0},
                {1,0,0},
                {0,1,0},
                {1,1,0},
                {1,0,0},
                {0,1,0},
        };

        T n[3*2][3] = {
                {0,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0},
        };

        d.action(
        		&(v[0][0]),
        		&(n[0][0]),
        		2
        	);
}
#endif

#endif /* CGLDISCONTINOUSTOCLOSEDSURFACE_HPP_ */
