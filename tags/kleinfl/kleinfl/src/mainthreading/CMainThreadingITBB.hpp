/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CMainThreadingIPMO.hpp
 *
 *  Created on: Apr 17, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CMAINTHREADINGIOMP_HPP_
#define CMAINTHREADINGIOMP_HPP_

#include <omp.h>
#include <iostream>
#include <cmath>


#if COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC
#	include <CPMO_TBB_2.hpp>
#else
#	include <CPMO_TBB.hpp>
#endif


#include "../simulations/hyperbolic_parallel/CSimulationHyperbolic_ScalabilityGraph.hpp"

#include "CMainThreading_Interface.hpp"

#include "libsierpi/parallelization/CGlobalComm.hpp"

/**
 * Threading support for invasive TBB
 */
class CMainThreading	:
	public CMainThreading_Interface
{
	/**
	 * PMO handler
	 */
#if COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC
	CPMO_TBB_2 *cPmo;
#else
	CPMO_TBB *cPmo;
#endif

	/**
	 * initial maximum number of cores
	 */
	int initial_max_cores;

	/**
	 * previous id used for invade scalability data
	 */
	int prev_unique_invade_id;

#if COMPILE_WITH_ITBB_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC
	/**
	 * previous simulation workload
	 */
	unsigned long long prev_invade_simulation_workload;
#endif

public:
	CMainThreading()	:
		cPmo(nullptr),
		initial_max_cores(512),
		prev_unique_invade_id(-1)
#if COMPILE_WITH_ITBB_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC
		,
		prev_invade_simulation_workload(99999999999)
#endif
	{
	}



	void threading_setup()
	{
		if (getVerboseLevel() > 0)
		{
			std::cerr << "WARNING: Setting Affinities in TBB is experimental since additional threads are sometimes created" << std::endl;
			std::cerr << "Use `taskset` to assure execution of the program only on the given cores." << std::endl;
		}

#if COMPILE_WITH_ITBB_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC

		// disable waiting for an ack

#if COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC
		cPmo = new CPMO_TBB_2(getMaxNumberOfThreads(), 0, false);
#else
		cPmo = new CPMO_TBB(getMaxNumberOfThreads(), 0, false);
#endif

		cPmo->setup();

		// request resource update with equal workload for equal distribution
		float dummy_workload = 1000;
		initial_max_cores = getMaxNumberOfThreads();
		int commSize = sierpi::CGlobalComm::getCommSize();
		int average_cores = getMaxNumberOfThreads()/commSize;

//		std::cout << "EXECUTING INVADE BLOCKING" << std::endl;
		cPmo->invade_blocking(2, average_cores, 0, nullptr, 1.0);
//		std::cout << "INVADE BLOCKING FIN" << std::endl;

//		std::cout << "initial_max_cores: " << initial_max_cores << std::endl;
//		std::cout << "average_cores: " << average_cores << std::endl;

//		std::cout << sierpi::CGlobalComm::getCommRank() << "invade_blocking" << std::endl;

//		std::cout << sierpi::CGlobalComm::getCommRank() << "invade_blocking fin" << std::endl;

		/*
		 * wait until all cores were assigned to the MPI process
		 * we have to do this nonblocking due to delayed ACKs during iPMO setup
		 */
//		std::cout << "Wait until each thread was assigned at least to one core" << std::endl;
		int reduceValue = 0;
		do
		{
//			std::cout << sierpi::CGlobalComm::getCommRank() << "reinvade_nonblocking" << std::endl;
			cPmo->reinvade_nonblocking();
			reduceValue = sierpi::CGlobalComm::reduceIntSum(1);
		} while(commSize != reduceValue);


		cPmo->invade_blocking(2, average_cores);

//		std::cout << "OK" << std::endl;

		cPmo->invade_nonblocking(2, average_cores);

//		std::cout << sierpi::CGlobalComm::getCommRank() << " Give away cores. currently assigned: " << cPmo->getNumberOfThreads() << std::endl;

		// execute an invade to give away cores to others
//		std::cout << sierpi::CGlobalComm::getCommRank() << "reinvade_blocking" << std::endl;
		cPmo->reinvade_blocking();
		sierpi::CGlobalComm::barrier();

//		std::cout << sierpi::CGlobalComm::getCommRank() << "Next invade. currently assigned: " << cPmo->getNumberOfThreads() << std::endl;

//		std::cout << sierpi::CGlobalComm::getCommRank() << "reinvade_blocking" << std::endl;
		cPmo->reinvade_blocking();
		sierpi::CGlobalComm::barrier();

//		std::cout << sierpi::CGlobalComm::getCommRank() << "Last invade. currently assigned: " << cPmo->getNumberOfThreads() << std::endl;


		// WARNING: in case that more processes are started than there are cores available on the system, this deadlocks
//		sierpi::CGlobalComm::barrier();

//		threading_updateResourceUtilization();

#else

		cPmo = new CPMO_TBB;
		cPmo->setup();

#endif

		if (getVerboseLevel() >= 5)
			std::cout << "max threads: " << (int)tbb::task_scheduler_init::default_num_threads() << std::endl;

		threading_updateResourceUtilization();
	}



	/**
	 * update the ressource utilization
	 */
	void threading_updateResourceUtilization()
	{
		unsigned long long local_workload = getSimulationLocalWorkload();

#if COMPILE_WITH_ITBB_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC_MPI || COMPILE_WITH_ITBB_2_ASYNC

		long long diff_workload = std::abs((long long)prev_invade_simulation_workload - (long long)local_workload);

		float inbalance = (float)diff_workload/(float)local_workload;

		if (inbalance > COMPILE_WITH_ITBB_ASYNC_MPI_INVADE_THRESHOLD)
		{
			// request resource update with new workload
			cPmo->invade_nonblocking(2, 1024, 0, nullptr, (float)local_workload);

			prev_invade_simulation_workload = local_workload;
		}

		// is some message on optimizations available?
		if (cPmo->reinvade_nonblocking())
		{
			setValueNumberOfThreadsToUse(cPmo->getNumberOfThreads());
//			std::cout << "updated threads: " << cPmo->getNumberOfThreads() << " considering workload of " << local_workload << std::endl;
		}

#else

		int scalability_graph_id = getThreadIPMOScalabilityGraphId();

		int max_cores;
		int scalability_graph_size;
		float *scalability_graph;
		const char *o_information_string;

		int id = CSimulationHyperbolic_ScalabilityGraph::getInvadeHintsAndConstraints(
				local_workload,
				scalability_graph_id,
				&max_cores,
				&scalability_graph_size,
				&scalability_graph,
				&o_information_string
		);

	#if COMPILE_WITH_ITBB_ASYNC

		if (id != prev_unique_invade_id)
		{
			prev_unique_invade_id = id;
			cPmo->invade_nonblocking(1, max_cores, scalability_graph_size, scalability_graph);
		}

		if (cPmo->reinvade_nonblocking())
			setValueNumberOfThreadsToUse(cPmo->num_running_threads);

	#else

		bool resources_updated;

		if (id != prev_unique_invade_id)
		{
			prev_unique_invade_id = id;
			cPmo->invade_nonblocking(1, max_cores, scalability_graph_size, scalability_graph);
			resources_updated = cPmo->reinvade_blocking();
		}
		else
		{
			resources_updated = cPmo->reinvade_blocking();
		}

		if (resources_updated)
			setValueNumberOfThreadsToUse(cPmo->num_computing_threads);

	#endif

#endif
	}



	/**
	 * run the simulation
	 */
	void threading_simulationLoop()
	{
		bool continue_simulation = true;

		do
		{
			// REINVADE
			threading_updateResourceUtilization();

			continue_simulation = simulation_loopIteration();

		} while(continue_simulation);
	}



	bool threading_simulationLoopIteration()
	{
		// REINVADE
		cPmo->reinvade_blocking();

		return simulation_loopIteration();
	}



	void threading_shutdown()
	{
		cPmo->client_shutdown_hint = ((double)getSimulationSumWorkload())*0.000001;
		cPmo->retreat();

		delete cPmo;
		cPmo = nullptr;
	}



	void threading_setNumThreads(int i)
	{
		initial_max_cores = i;
		threading_updateResourceUtilization();
	}



	virtual ~CMainThreading()
	{
		if (cPmo != nullptr)
			delete cPmo;
	}
};


#endif
