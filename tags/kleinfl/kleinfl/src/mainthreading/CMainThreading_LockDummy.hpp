/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 * CMainThreading_MutexOMP.hpp
 *
 *  Created on: Jul 20, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#ifndef CMAINTHREADING_LOCK_DUMMY_HPP_
#define CMAINTHREADING_LOCK_DUMMY_HPP_


#include "CMainThreading_LockInterface.hpp"

/**
 * Threading support for dummy threading
 */
class CMainThreading_Lock	:
	public CMainThreading_LockInterface
{

public:
	CMainThreading_Lock()
	{
	}

	void lock()
	{
	}

	void unlock()
	{
	}

	virtual ~CMainThreading_Lock()
	{
	}
};


#endif /* CMAINTHREADING_LOCK_OMP_HPP_ */
