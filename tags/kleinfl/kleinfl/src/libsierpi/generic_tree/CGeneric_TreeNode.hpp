/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CGeneric_TreeNode.hpp
 *
 *  Created on: Jun 30, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 *
 */


#ifndef CGENERICTREENODE_HPP_
#define CGENERICTREENODE_HPP_

#include "global_config.h"
#include <assert.h>

#include "libsierpi/cluster/CCluster_UniqueId.hpp"

/*
 * we have to include the header files here to stay outside the class scope
 */
#if CONFIG_THREADING_TBB
	#include <tbb/tbb.h>
#endif

#if CONFIG_THREADING_OMP
	#include <omp.h>
#endif



namespace sierpi
{

/**
 * \brief the generic tree node is used to allow tree traversals using a unified tree node system.
 *
 * in this way, the recursive tree traversals don't have to distinguish between domain triangulation nodes
 * and sub-trees of the root domain cluster triangles.
 */
template <typename CCluster_TreeNode>
class CGeneric_TreeNode
{

	/**
	 * abbreviation
	 */
	typedef CGeneric_TreeNode<CCluster_TreeNode> CGeneric_TreeNode_;
public:
	/**
	 * pointer to parent node
	 */
	CGeneric_TreeNode *parent_node;

	/**
	 * first and second child node
	 */
	CGeneric_TreeNode *first_child_node, *second_child_node;

	/**
	 * set this node to be a new leaf node during the next traversal
	 */
	bool delayed_child_deletion;


public:
	/**
	 * pointer to tree node (avoids casting for cleaner programming, a static_case can also be used).
	 */
	CCluster_TreeNode *cCluster_TreeNode;

	/**
	 * this variable is set to true when this tree node is one of the nodes for the base triangulation.
	 *
	 * This avoids joining base triangles which may create inconsistencies, e. g. for the world sphere
	 */
	bool base_triangulation_node;

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	CMigrationAtomic<bool> migration_base_triangulation_node;
#endif

	/**
	 * approximated workload in this subtree
	 */
	long long workload_in_subtree;

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	CMigrationAtomic<long long> migration_workload_in_subtree;
#endif


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	/**
	 * this method is executed when the migration for this cluster takes place to dst_rank
	 */
	inline void migration_send(
		int i_src_rank,
		int i_dst_rank
	) {
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		CMigration::sendAtomic(base_triangulation_node, i_dst_rank);
		CMigration::sendAtomic(workload_in_subtree, i_dst_rank);
#else
		migration_base_triangulation_node.send(base_triangulation_node, i_dst_rank);
		migration_workload_in_subtree.send(workload_in_subtree, i_dst_rank);
#endif
	}

	/**
	 * this method is executed when the migration for this cluster takes place to dst_rank
	 */
	inline void migration_send_postprocessing(
			int i_dst_rank
	) {
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		migration_base_triangulation_node.wait();
		migration_workload_in_subtree.wait();
#endif
	}



	/**
	 * this method is executed when to finish the migration
	 *
	 * only useful parameters are restored
	 */
	inline void migration_recv(
		int i_src_rank
	) {
		CMigration::recvAtomic(base_triangulation_node, i_src_rank);
		CMigration::recvAtomic(workload_in_subtree, i_src_rank);
	}

#endif

	/**
	 * find generic tree node corresponding to the unique id
	 *
	 * if the node does not exist, return nullptr
	 */
	CGeneric_TreeNode_* p_findChildByUniqueId(
		CCluster_UniqueId &i_uniqueId
	) {
		CGeneric_TreeNode_ *processed_cGenericTreeNode = this;

#if DEBUG
		if (parent_node != nullptr)
		{
			std::cerr << "findChildByUniqueId only allowed for root element" << std::endl;
			assert(false);
		}
#endif

		assert (processed_cGenericTreeNode != nullptr);

		/*
		 * use bitmask to determine following the right or left tree
		 */
		for (unsigned int i = 0; i < i_uniqueId.unique_id_depth; i++)
		{
			if (processed_cGenericTreeNode == nullptr)
				return nullptr;

			if (i_uniqueId.followFirstChildForDepth(i))
			{
				processed_cGenericTreeNode = processed_cGenericTreeNode->first_child_node;
				continue;
			}

			{
				processed_cGenericTreeNode = processed_cGenericTreeNode->second_child_node;
				continue;
			}
		}

		if (processed_cGenericTreeNode == nullptr)
			return nullptr;

		return processed_cGenericTreeNode;
	}



	/**
	 * find generic tree node corresponding to the unique id
	 *
	 * if the node does not exist, return nullptr
	 */
	CGeneric_TreeNode_* p_createChildByUniqueId(
		CCluster_UniqueId &i_uniqueId,
		int i_base_triangulation_depth
	) {
		CGeneric_TreeNode_ *processed_cGenericTreeNode = this;

#if DEBUG
		if (parent_node != nullptr)
		{
			std::cerr << "findChildByUniqueId only allowed for root element" << std::endl;
			assert(false);
		}
#endif

		assert (processed_cGenericTreeNode != nullptr);

		int left_right_bitmask = 1 << i_uniqueId.unique_id_depth;

		/*
		 * use bitmask to determine following the right or left tree
		 */
		for (size_t i = 0; i < i_uniqueId.unique_id_depth; i++)
		{
			left_right_bitmask >>= 1;
			i_base_triangulation_depth--;

			if (i_uniqueId.raw_unique_id & left_right_bitmask)
			{
				// second
				if (processed_cGenericTreeNode->second_child_node == nullptr)
				{
					// insert new second child
					processed_cGenericTreeNode->second_child_node = new CGeneric_TreeNode(processed_cGenericTreeNode, i_base_triangulation_depth >= 0);
				}

				// follow second child
				processed_cGenericTreeNode = processed_cGenericTreeNode->second_child_node;
				continue;
			}
			else
			{
				// first
				if (processed_cGenericTreeNode->first_child_node == nullptr)
				{
					// insert new first child
					processed_cGenericTreeNode->first_child_node = new CGeneric_TreeNode(processed_cGenericTreeNode, i_base_triangulation_depth >= 0);
				}

				// follow first child
				processed_cGenericTreeNode = processed_cGenericTreeNode->first_child_node;
				continue;
			}
		}

		return processed_cGenericTreeNode;
	}




#if CONFIG_ENABLE_SCAN_DATA
	/**
	 * scan start index (see MPI_scan)
	 */
	long long workload_scan_start_index;

	/**
	 * scan end index (see MPI_scan)
	 */
	long long workload_scan_end_index;


	/**
	 * thread id responsible for computation of first part of workload
	 */
	int workload_thread_id_start;

	/**
	 * thread id responsible for computation of last part of workload
	 */
	int workload_thread_id_end;

	/**
	 * thread id which should be uniquely associated with this cluster
	 */
	int workload_thread_id;


public:
	/**
	 * update scan data of workload
	 */
	inline void updateWorkloadScanTopDown()
	{
		/*
		 * test whether this is the root node
		 */
		if (parent_node == nullptr)
		{
			workload_scan_start_index = 0;
		}
		else
		{
			if (parent_node->first_child_node != nullptr)
			{
				if (parent_node->first_child_node == this)
				{
					// this is the first node -> copy start index
					workload_scan_start_index = parent_node->workload_scan_start_index;
				}
				else
				{
					workload_scan_start_index = parent_node->workload_scan_start_index + parent_node->first_child_node->workload_in_subtree;
				}
			}
			else
			{
				workload_scan_start_index = parent_node->workload_scan_start_index;
			}
		}

		workload_scan_end_index = workload_scan_start_index + workload_in_subtree;

		assert(workload_scan_start_index >= 0);
		// this may fail in case of MPI with empty nodes

#if !CONFIG_ENABLE_MPI
		assert(workload_scan_end_index >= 1);
		assert(workload_scan_start_index < workload_scan_end_index);
#else
		assert(workload_scan_end_index >= 0);
		assert(workload_scan_start_index <= workload_scan_end_index);
#endif
	}


	/**
	 * update workload to thread assignment based on average cells per thread
	 */
	inline void updateWorkloadThreadAssignment(
			unsigned long long i_average_cells_per_thread	///< average cells per thread to compute thread affinities
	)
	{
#if CONFIG_ENABLE_MPI
		if (i_average_cells_per_thread == 0)
		{
			workload_thread_id_start = 0;
			workload_thread_id_end = 0;
			workload_thread_id = -1;
			return;
		}
#endif
		workload_thread_id_start = workload_scan_start_index / i_average_cells_per_thread;
		workload_thread_id_end = (workload_scan_end_index-1) / i_average_cells_per_thread;

		// use workload midpoint to associate to thread_id
		workload_thread_id = (workload_scan_start_index+(workload_scan_end_index-1)) / (i_average_cells_per_thread*2);

		assert(workload_thread_id_start >= 0);
		assert(workload_thread_id_end >= 0);
	}

#endif

	/**
	 * constructor
	 */
	CGeneric_TreeNode(
			CGeneric_TreeNode *i_parent_node,	///< parent generic tree node
			bool i_base_triangulation_node		///< true, if this is a base triangulation node (such a cluster may not be joined!)
	)	:
		parent_node(i_parent_node),
		first_child_node(nullptr),
		second_child_node(nullptr),
		delayed_child_deletion(false),
		cCluster_TreeNode(nullptr),
		base_triangulation_node(i_base_triangulation_node),
		workload_in_subtree(0)
#if CONFIG_ENABLE_SCAN_DATA
	,
		workload_scan_start_index(-1),
		workload_scan_end_index(-1),
		workload_thread_id_start(-1),
		workload_thread_id_end(-1),
		workload_thread_id(-1)
#endif
	{
		specializedConstructorMethod();
	}


public:
	/**
	 * mark the children to be ready for deletion during the next traversal.
	 *
	 * this is necessary to have joined children still accessible by adjacent clusters until
	 * the updates of the edge comm information is finished.
	 */
	void setDelayedChildDeletion()
	{
		delayed_child_deletion = true;
	}

	void unsetDelayedChildDeletion()
	{
		delayed_child_deletion = false;
	}




	/**
	 * deconstructor which deletes the children which are only
	 */
	~CGeneric_TreeNode()
	{
		freeChildren();

		if (cCluster_TreeNode != nullptr)
			delete cCluster_TreeNode;
	}



	/**
	 * free generic child tree nodes as well as specialized nodes if they exist
	 *
	 * the specialized nodes may not care about deleting their children!
	 */
	void freeChildren()
	{
		if (first_child_node != nullptr)
		{
			delete first_child_node;
			first_child_node = nullptr;
		}

		if (second_child_node != nullptr)
		{
			delete second_child_node;
			second_child_node = nullptr;
		}

		delayed_child_deletion = false;
	}


	/**
	 * setup dummy
	 */
	void setupDummyRoot()
	{
		first_child_node = nullptr;
		second_child_node = nullptr;
		parent_node = nullptr;
	}


	/**
	 * return the root node of this generic tree node
	 */
	CGeneric_TreeNode* getRootNode()
	{
		CGeneric_TreeNode *p = this;

		while (p->parent_node)
			p = p->parent_node;

		return p;
	}


	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool isLeaf()	const
	{
		if (delayed_child_deletion)
			return true;

#if CONFIG_ENABLE_MPI
		if (cCluster_TreeNode == nullptr)
			return false;

		if (cCluster_TreeNode->cSimulation_Cluster == nullptr)
			return false;
#endif

		return (first_child_node == nullptr) && (second_child_node == nullptr);
	}



	/**
	 * return true, if this is a leaf triangle
	 */
	inline bool hasLeaves()	const
	{
		if (delayed_child_deletion)
			return false;

		return (first_child_node != nullptr) || (second_child_node != nullptr);
	}


#if CONFIG_THREADING_TBB
	#include "CGeneric_TreeNode_tbb.hpp"
#endif

#if CONFIG_THREADING_OMP
	#include "CGeneric_TreeNode_omp.hpp"
#endif

#if (!CONFIG_THREADING_TBB) && (!CONFIG_THREADING_OMP)
	#include "CGeneric_TreeNode_serial.hpp"
#endif

};

}

#endif
