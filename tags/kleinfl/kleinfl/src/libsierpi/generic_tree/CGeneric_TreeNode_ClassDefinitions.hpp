/*
 * CGeneric_TreeNode_ClassDefinitions.hpp
 *
 *  Created on: Mar 4, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGENERIC_TREENODE_CLASSDEFINITIONS_HPP_
#define CGENERIC_TREENODE_CLASSDEFINITIONS_HPP_


namespace sierpi
{

template <typename CCluster_TreeNode>
class CGeneric_TreeNode;

}

#endif /* CGENERICTREENODE_CLASSDEFINITIONS_HPP_ */
