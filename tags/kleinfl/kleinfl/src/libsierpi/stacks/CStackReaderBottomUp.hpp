/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CStackReaderBottomUp.hpp
 *
 *  Created on: Jun 6, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CSTACKREADERBOTTOMUP_HPP_
#define CSTACKREADERBOTTOMUP_HPP_

namespace sierpi
{
template <typename T>
class CStackReaderBottomUp;

}

#include "CStack.hpp"


namespace sierpi
{

/**
 * \brief non-destructive stack reader reading data elements from bottom to top
 */
template <typename T>
class CStackReaderBottomUp
{
public:
	CStack<T> *stack;
	T *stack_ptr;

	/**
	 * setup this stack read-only accessor with a pointer to an existing stack
	 */
	inline void setup(CStack<T> *p_stack)
	{
		stack = p_stack;
		reset();
	}

	/**
	 * setup this stack read-only accessor with a reference to an existing stack
	 */
	inline void setup(CStack<T> &p_stack)
	{
		stack = &p_stack;
		reset();
	}

	/**
	 * read next element in read-buffer
	 */
	inline T getNextData()
	{
		return *(stack_ptr++);
	}


	/**
	 * get pointer to current element in buffer and go to next element
	 */
	inline T* getNextDataPtr()
	{
		return stack_ptr++;
	}


	/**
	 * get pointer to previous element in buffer
	 */
	inline T* getPreviousDataPtr()
	{
		assert(stack_ptr-1 >= stack->stack_start_ptr);
		return stack_ptr-1;
	}

	/**
	 * get pointer to the element being read 2 getNextData*() calls ago
	 */
	inline T* getPreviousPreviousDataPtr()
	{
		assert(stack_ptr-2 >= stack->stack_start_ptr);
		return stack_ptr-2;
	}


	/**
	 * reset the stack reader to start reading from the very bottom of the stack.
	 */
	inline void reset()
	{
		stack_ptr = stack->stack_start_ptr;
	}

	/**
	 * return true, if there is nothing to be read anymore
	 */
	inline bool isEmpty()
	{
		return stack_ptr == stack->stack_ptr;
	}
};

}

#endif
