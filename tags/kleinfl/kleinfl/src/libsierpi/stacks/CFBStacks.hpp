/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CFBStacks.hpp
 *
 *  Created on: Jan 11, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CFBSTACKS_HPP_
#define CFBSTACKS_HPP_

#include "../parallelization/CMigration.hpp"
#include "CStack.hpp"

namespace sierpi
{

/**
 * \brief class to handle the 2 stacks simultaneously.
 *
 * this is usually the case when moving data from one stack to another (e. g. the
 * structure stack).
 *
 * since one stack is usually written during a forward traversal and the other one
 * in a backward traversal, this class is called FB to account for 'forward/backward'.
 */
template <typename T>
class CFBStacks
{
public:
	CStack<T> forward;
	CStack<T> backward;

	typedef enum {
		FORWARD = 0,
		BACKWARD = 1
	} stack_format_t;

	bool direction;


#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
	CMigrationAtomic<bool> migration_direction;
#endif


	inline void migration_send(
			int i_dst_rank
	) {
//		std::cout << forward.getNumberOfElementsOnStack() << std::endl;
		forward.migration_send(i_dst_rank);
		backward.migration_send(i_dst_rank);

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
//		sierpi::CMigration::sendAtomic(direction, i_dst_rank);
#else
//		migration_direction.send(direction, i_dst_rank);
#endif
	}

	inline void migration_send_postprocessing(
			int i_dst_rank
	) {
		forward.migration_send_postprocessing(i_dst_rank);
		backward.migration_send_postprocessing(i_dst_rank);

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
//		migration_direction.wait();
#endif
	}

	inline void migration_recv(
			int i_src_rank
	) {
		forward.migration_recv(i_src_rank);
		backward.migration_recv(i_src_rank);

//		sierpi::CMigration::recvAtomic(direction, i_src_rank);
	}

#endif



	CFBStacks()	:
		direction(FORWARD)
	{
	}

	CFBStacks(size_t size)	:
		forward(size),
		backward(size),
		direction(FORWARD)
	{
	}

	/**
	 * returns true if the stack is initialized - otherwise false
	 */
	inline bool isInitialized()
	{
		if (direction == FORWARD)
			return forward.isInitialized();
		else
			return backward.isInitialized();
	}

	size_t getMaxNumberOfElements()
	{
		if (direction == FORWARD)
			return forward.getMaxNumberOfElements();
		else
			return backward.getMaxNumberOfElements();
	}

	size_t getNumberOfElementsOnStack()
	{
		if (direction == FORWARD)
			return forward.getNumberOfElementsOnStack();
		else
			return backward.getNumberOfElementsOnStack();
	}

	/**
	 * return the number of bytes allocated by the stack element storage
	 */
	inline size_t getMemSize()	const
	{
		if (direction == FORWARD)
			return forward.getMemSize();
		else
			return backward.getMemSize();
	}

	/**
	 * resize both stacks to the given size
	 */
	void resize(
			size_t i_size	///< new maximum number of elements which can be stored on the stack
	)
	{
		forward.resize(i_size);
		backward.resize(i_size);
	}

	/**
	 * return the number of triangles if this stack is a structure stack.
	 * it is assumed, that the quad is represented by 2 triangles.
	 */
	unsigned int structure_getNumberOfTrianglesInQuad()	const
	{
		if (direction == FORWARD)
			return forward.structure_getNumberOfTrianglesInQuad();
		else
			return backward.structure_getNumberOfTrianglesInQuad();
	}

	/**
	 * return the number of triangles if only a single triangle
	 * is represented by this stack.
	 */
	unsigned int structure_getNumberOfTrianglesInTriangle()	const
	{
		if (direction == FORWARD)
			return forward.structure_getNumberOfTrianglesInTriangle();
		else
			return backward.structure_getNumberOfTrianglesInTriangle();
	}

	/**
	 * swap forward and backward stack
	 */
	void swap()
	{
		forward.swap(backward);
	}

	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, CFBStacks<T> &stacks)
	{
		if (stacks.direction == CFBStacks<T>::FORWARD)
			co << stacks.forward;
		else
			co << stacks.backward;

		return co;
	}

};


}

#endif /* CFBSTACKS_HPP_ */
