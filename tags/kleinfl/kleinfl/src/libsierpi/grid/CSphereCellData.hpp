/*
 * CSphereCellData.hpp
 *
 *  Created on: Jan 13, 2014
 *      Author: Florian Klein <kleinfl@in.tum.de>
 */

#ifndef CSPHERETYPES_HPP_
#define CSPHERETYPES_HPP_

#include "libsierpi/parallelization/CGlobalComm.hpp"
#include <vector>
#include <iostream>
#include <limits>

#include "../libsierpi/triangle/CTriangle_VectorProjections.hpp"

#include "../libsierpi/grid/CCube_To_Sphere_Projection.hpp"

#include "../libmath/CVector.hpp"
#include "../libmath/CMatrix.hpp"


/**
 * Contains cell data and methods specific to distorted grid
 * cells on the sphere.
 */
class CSphereCellData
{
	typedef CONFIG_DEFAULT_FLOATING_POINT_TYPE T;

public:
	/**
	 * area of cell as projected on sphere
	 */
	T cellArea;


	/**
	 * cube face of the edge
	 */
	CCube_To_Sphere_Projection::EFaceType face;

	/**
	 * x,y-Coordinates of the 3 vertices on the cube face
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T vertices[3][2];

	/**
	 * x,y,z-Coordinates of the 3 vertices projected on the sphere
	 *
	 *
	 * 		2
	 * 	   /|\
	 * 	  / | \
	 * 	 /  |  \
	 * 	0-------1
	 */
	T coordinates3D[3][3];

	/**
	 * edge lengths on sphere
	 *
	 *
	 * 	|\
	 * 1|  \0
	 * 	|    \
	 * 	-------
	 * 	   2
	 *
	 */
	T edgeLength[3];

	/**
	 * radius of the inscribed circle
	 *
	 * |\
	 * |  \
	 * |    \
	 * |      \
	 * |___.    \
	 * |   |      \
	 * |___|________\
	 *
	 */
	T incircleRadius;

	/*
	 * projection matrix to project 3D triangle to flat 2D (conserving edge lengths)
	 */
	T projectionMatrix3DTo2D[3][3];
//	CMatrix3<T> projectionMatrix3DTo2D;

	/*
	 * rotation matrix from normal to edge space for left edge
	 */
	T leftEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for right edge
	 */
	T rightEdgeRotationMatrix[2][2];

	/*
	 * rotation matrix from normal to edge space for hyp edge
	 */
	T hypEdgeRotationMatrix[2][2];

	/**
	 * transformation matrix for shear and scaling
	 *
	 *  _                               |\_
	 *  \ - _                           |  \_     _
	 *   \    - _                       |    \_ -|2'
	 *    \       - _          --->    1|      \_
	 *     \__________-_                |        \_
	 *                                  |__________\
	 *                                       1
	 */
	T transformationMatrix[2][2];

	/**
	 * inverse transformation matrix for shear and scaling
	 *
	 *      |\                    _
	 *      |  \     _            \ - _
	 *      |    \ -|2'            \    - _
	 *     1|      \      --->      \       - _
	 *      |        \               \__________-_
	 *      |__________\
	 *                                       1
	 */
	T inverseTransformationMatrix[2][2];


	void setupDistortedGrid(
			T i_vertex_left_x,	T i_vertex_left_y,
			T i_vertex_right_x,	T i_vertex_right_y,
			T i_vertex_top_x,	T i_vertex_top_y,

			T i_hyp_normal_x,	T i_hyp_normal_y,		///< normals for hypotenuse edge
			T i_right_normal_x,	T i_right_normal_y,		///< normals for right edge
			T i_left_normal_x,	T i_left_normal_y		///< normals for left edge (necessary for back-rotation of element)
	) {
		T i_x = (i_vertex_left_x + i_vertex_right_x + i_vertex_top_x)*(T)(1.0/3.0);
		T i_y = (i_vertex_left_y + i_vertex_right_y + i_vertex_top_y)*(T)(1.0/3.0);

		// get cube face
		face = CCube_To_Sphere_Projection::getFaceIdOf2DCubeCoordinate(i_x, i_y);
		if (face == CCube_To_Sphere_Projection::EFaceType::E_ERROR)
		{
			std::cerr << "Face id not found for the following vertices:" << std::endl;
			std::cerr << " + left: " << i_vertex_left_x << ", " << i_vertex_left_y << std::endl;
			std::cerr << " + right: " << i_vertex_right_x << ", " << i_vertex_right_y << std::endl;
			std::cerr << " + top: " << i_vertex_top_x << ", " << i_vertex_top_y << std::endl;
			std::cerr << std::endl;
		}

		// project to (-1,1)^2
		CCube_To_Sphere_Projection::project2DTo2DFace(
				face,
				&i_vertex_left_x,	&i_vertex_left_y,
				&i_vertex_right_x,	&i_vertex_right_y,
				&i_vertex_top_x,	&i_vertex_top_y
			);

		vertices[0][0] = i_vertex_left_x;
		vertices[0][1] = i_vertex_left_y;
		vertices[1][0] = i_vertex_right_x;
		vertices[1][1] = i_vertex_right_y;
		vertices[2][0] = i_vertex_top_x;
		vertices[2][1] = i_vertex_top_y;


		// set 3D coordinates on sphere
		for(int i = 0; i < 3; i++)
		{
			CCube_To_Sphere_Projection::project2DFaceTo3D(
					face,
					vertices[i][0],
					vertices[i][1],
					&coordinates3D[i][0],
					&coordinates3D[i][1],
					&coordinates3D[i][2]
			);
		}


		// set correct radius
		T radius = EARTH_RADIUS_IN_METERS;


		for(int i = 0; i < 3; i++)
		{
			// set edge length as on sphere
			edgeLength[i] = CTriangle_VectorProjections::getEdgeLengthOnSphere(
//					vertices[i][0],
//					vertices[i][1],
//					vertices[(i+1)%3][0],
//					vertices[(i+1)%3][1],
					coordinates3D[i][0],
					coordinates3D[i][1],
					coordinates3D[i][2],
					coordinates3D[(i+1)%3][0],
					coordinates3D[(i+1)%3][1],
					coordinates3D[(i+1)%3][2],
					radius
			);
		}

		// set rotation matrices for all edges
		CVector<3, T> vertex_left = CVector<3, T>(coordinates3D[0][0], coordinates3D[0][1], coordinates3D[0][2]);
		CVector<3, T> vertex_right = CVector<3, T>(coordinates3D[1][0], coordinates3D[1][1], coordinates3D[1][2]);
		CVector<3, T> vertex_top = CVector<3, T>(coordinates3D[2][0], coordinates3D[2][1], coordinates3D[2][2]);

		CTriangle_VectorProjections::set2DProjectionMatrix(
				projectionMatrix3DTo2D,
				vertex_left,
				vertex_right,
				vertex_top
				);
		CTriangle_VectorProjections::setHypEdgeRotationMatrix(
				hypEdgeRotationMatrix,
				projectionMatrix3DTo2D,
				vertex_left,
				vertex_right,
				vertex_top
//				edgeLength[0],
//				edgeLength[1],
//				edgeLength[2]
			);

		// always aligned at x-axis
		CTriangle_VectorProjections::setLeftEdgeRotationMatrix(
				leftEdgeRotationMatrix
			);

		CTriangle_VectorProjections::setRightEdgeRotationMatrix(
				rightEdgeRotationMatrix,
				projectionMatrix3DTo2D,
				vertex_left,
				vertex_right,
				vertex_top
//				edgeLength[0],
//				edgeLength[1],
//				edgeLength[2]
			);

		CTriangle_VectorProjections::setTransformationMatrices(
				transformationMatrix,
				inverseTransformationMatrix,
//				edgeLength[0],
//				edgeLength[1],
//				edgeLength[2],
				projectionMatrix3DTo2D,
				vertex_left,
				vertex_right,
				vertex_top,
				radius
			);

		// approximation of the triangle area as half of the product of the two cathetus lengths
//		cellArea = (edgeLength[1] * edgeLength[2] ) / (T)2.0;

		cellArea = CTriangle_VectorProjections::getCellAreaOnSphere(
//				edgeLength[0],
//				edgeLength[1],
//				edgeLength[2],
//				radius,
				inverseTransformationMatrix
			);

		// set inscribed circle radius
		incircleRadius = CTriangle_VectorProjections::getIncircleRadius(
				edgeLength[0],
				edgeLength[1],
				edgeLength[2],
				cellArea
//				radius
			);
	}


};


#endif /* CTSUNAMITYPES_HPP_ */
