/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CCluster_SplitJoinAction.hpp
 *
 *  Created on: Jul 4, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_SPLIT_JOIN_ACTIONS_HPP_
#define CCLUSTER_SPLIT_JOIN_ACTIONS_HPP_

#include "global_config.h"
#include "../parallelization/CGlobalComm.hpp"
#include "../generic_tree/CGeneric_TreeNode.hpp"
#include "CCluster_TreeNode.hpp"
#include "CCluster_EdgeComm_InformationAdjacentCluster.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"



#define DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT	0

namespace sierpi
{

template <typename t_CSimulation_Cluster> class CCluster_TreeNode;


/**
 * class to store the enumerator for the transfer states
 */
class CCluster_SplitJoinActions_Enums
{
public:
	/**
	 * these states account for the currently running transformation.
	 *
	 * after each timestep, the state of each sub-cluster has to be NO_TRANSFER
	 */
	enum EPARTITION_TRANSFER_STATE
	{
		NO_TRANSFER = 0,
		SPLITTED_PARENT = 1,
		SPLIT_CHILD = 2,
		JOINED_PARENT = 3,
		JOINED_CHILD = 4
	};
};



/**
 * \brief split and join actions which have to be executed in a particular order
 *
 * this class offers methods to split and join cluster tree nodes.
 */
template <typename t_CSimulation_Cluster>
class CCluster_SplitJoinActions	:
		public CCluster_SplitJoinActions_Enums
{
	typedef CCluster_TreeNode<t_CSimulation_Cluster> CCluster_TreeNode_;
	typedef CGeneric_TreeNode<CCluster_TreeNode_> CGeneric_TreeNode_;

	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CCluster_EdgeComm_InformationAdjacentCluster_;
	typedef CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> CCluster_EdgeComm_InformationAdjacentClusters_;


	/**
	 * triangle factory
	 */
	CTriangle_Factory &cTriangleFactory;

	/**
	 * reference to cluster tree node
	 */
	CCluster_TreeNode_ &cCluster_TreeNode;

	/**
	 * split/join information which should be used for data exchange
	 */
	CCluster_SplitJoin_EdgeComm_Information &cCluster_SplitJoinInformation;

	/**
	 * this is the point where the adjacency information to other paritions is stored
	 */
	CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> &cCluster_EdgeComm_InformationAdjacentClusters;

	/**
	 * cluster's unique ID
	 */
	CCluster_UniqueId &cCluster_UniqueId;

#if DEBUG
	/**
	 * triangle child factories already set-up
	 */
	bool triangleChildrenFactorySetupDone;
#endif

#if CONFIG_ENABLE_MPI
public:
	struct SSplitJoinDMInformation
	{
	public:
		/*
		 * number of edge comm elements
		 */
		int edge_comm_elements;

		/*
		 * transfer state of cluster
		 */
		EPARTITION_TRANSFER_STATE transferState;

		/*
		 * unique id of cluster
		 */
		CCluster_UniqueId cCluster_UniqueId;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		/*
		 * in case of mpi cluster migration, the new rank has to be possibly updated
		 */
		int mpi_rank;
#endif

#if DEBUG
		CCluster_UniqueId adj_cCluster_UniqueId;
#endif
	};

	std::vector<SSplitJoinDMInformation> hyp_edge_dm_split_join_dm_information;
	std::vector<SSplitJoinDMInformation> cat_edge_dm_split_join_dm_information;
#endif


public:
	/**
	 * one of the most important parts: the transfer state describes the current
	 * state of the cluster tree node to run further actions during later processing.
	 */
	EPARTITION_TRANSFER_STATE transferState;


public:
	/**
	 * Constructor
	 */
	CCluster_SplitJoinActions(
			CCluster_TreeNode_ &p_cClusterTreeNode	///< handler to cluster tree node (e. g. to access stacks)
	)	:
		cTriangleFactory(p_cClusterTreeNode.cTriangleFactory),
		cCluster_TreeNode(p_cClusterTreeNode),
		cCluster_SplitJoinInformation(p_cClusterTreeNode.cCluster_SplitJoinInformation),
		cCluster_EdgeComm_InformationAdjacentClusters(p_cClusterTreeNode.cCluster_EdgeComm_InformationAdjacentClusters),
		cCluster_UniqueId(p_cClusterTreeNode.cCluster_UniqueId),
#if DEBUG
		triangleChildrenFactorySetupDone(false),
#endif
		transferState(NO_TRANSFER)
	{
	}


public:
	void remove_nodes_from_comm_data()
	{
		// hypotenuse: remove nodes
		for (	auto local_iter = cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
				local_iter != cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
				local_iter++
		)
		{
			if (	(*local_iter).cCluster_UniqueId.isBoundary() ||
					(*local_iter).edge_comm_elements != 0
			)
				cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.push_back(*local_iter);
		}
		cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.swap(cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters);
		cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.clear();



		// cathetus: remove nodes
		for (	auto local_iter = cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
				local_iter != cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
				local_iter++
		)
		{
			if (	(*local_iter).cCluster_UniqueId.isBoundary() ||
					(*local_iter).edge_comm_elements != 0
			)
				cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.push_back(*local_iter);
		}
		cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.swap(cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters);
		cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.clear();
	}

	/**
	 * split this sub-cluster.
	 */
public:
	void pass1_splitCluster()
	{
		CGeneric_TreeNode_ *cGeneric_TreeNode = cCluster_TreeNode.cGeneric_TreeNode;

		assert(transferState == NO_TRANSFER);
		assert(cCluster_SplitJoinInformation.splitting_permitted);
		assert(cGeneric_TreeNode->isLeaf());


		/*
		 * STEP 1)
		 *
		 * allocate & create new child nodes
		 */

		assert(!triangleChildrenFactorySetupDone);

		cTriangleFactory.setupChildFactories(
				cCluster_SplitJoinInformation.first_triangle.triangleFactory,
				cCluster_SplitJoinInformation.second_triangle.triangleFactory
			);

		/*
		 * override type to reduce possible recursive methods without changing any traversals
		 */
		cCluster_SplitJoinInformation.first_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_K;
		cCluster_SplitJoinInformation.second_triangle.triangleFactory.triangleType = CTriangle_Enums::TRIANGLE_TYPE_K;

#if DEBUG
		// mark triangle child setup to be finished
		triangleChildrenFactorySetupDone = true;
#endif

		// setup children

		// FIRST child
		CGeneric_TreeNode_ *firstGenericTreeNode = new CGeneric_TreeNode_(cCluster_TreeNode.cGeneric_TreeNode, false);
		CCluster_TreeNode_ *firstClusterTreeNode = new CCluster_TreeNode_(cCluster_SplitJoinInformation.first_triangle.triangleFactory);

		firstClusterTreeNode->cGeneric_TreeNode = firstGenericTreeNode;
		firstGenericTreeNode->cCluster_TreeNode = firstClusterTreeNode;

		cCluster_TreeNode.cGeneric_TreeNode->first_child_node = firstGenericTreeNode;
		firstClusterTreeNode->cCluster_UniqueId.setupFirstChildFromParent(cCluster_UniqueId);
		firstClusterTreeNode->cCluster_SplitJoinActions.transferState = SPLIT_CHILD;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		firstClusterTreeNode->migration_dst_rank = cCluster_TreeNode.migration_dst_rank;
#endif

		// SECOND child
		CGeneric_TreeNode_ *secondGenericTreeNode = new CGeneric_TreeNode_(cCluster_TreeNode.cGeneric_TreeNode, false);
		CCluster_TreeNode_ *secondClusterTreeNode = new CCluster_TreeNode_(cCluster_SplitJoinInformation.second_triangle.triangleFactory);

		secondClusterTreeNode->cGeneric_TreeNode = secondGenericTreeNode;
		secondGenericTreeNode->cCluster_TreeNode = secondClusterTreeNode;

		cCluster_TreeNode.cGeneric_TreeNode->second_child_node = secondGenericTreeNode;
		secondClusterTreeNode->cCluster_UniqueId.setupSecondChildFromParent(cCluster_UniqueId);
		secondClusterTreeNode->cCluster_SplitJoinActions.transferState = SPLIT_CHILD;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		secondClusterTreeNode->migration_dst_rank = cCluster_TreeNode.migration_dst_rank;
#endif

		/*
		 * STEP 2)
		 *
		 * setup new simulation cluster handlers
		 */

		/*
		 * FIRST CHILD
		 */
		// setup simulation handler
		firstClusterTreeNode->setup_SimulationClusterHandler(cCluster_TreeNode.cSimulation_Cluster);

		assert(cCluster_SplitJoinInformation.first_triangle.number_of_elements != -1);

		// setup stacks
		firstClusterTreeNode->setup_SplittedChildrenStacks(
				cCluster_SplitJoinInformation,		/// split information
				false
		);

		firstGenericTreeNode->workload_in_subtree = cCluster_SplitJoinInformation.first_triangle.number_of_elements;

#if CONFIG_ENABLE_SCAN_DATA
		firstGenericTreeNode->workload_scan_start_index = cGeneric_TreeNode->workload_scan_start_index;
		firstGenericTreeNode->workload_scan_end_index = cGeneric_TreeNode->workload_scan_start_index + firstGenericTreeNode->workload_in_subtree;
#endif


		/*
		 * SECOND CHILD
		 */
		/*
		 * reuse the stacks of this node since they are of no further use.
		 * this is done by handing over the current cCluster as a parameter.
		 */
		secondClusterTreeNode->setup_SimulationClusterHandler(cCluster_TreeNode.cSimulation_Cluster);

		assert(cCluster_SplitJoinInformation.second_triangle.number_of_elements != -1);

		// setup stacks
		secondClusterTreeNode->setup_SplittedChildrenStacks(
				cCluster_SplitJoinInformation,		/// split information
				CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		);

		secondGenericTreeNode->workload_in_subtree = cCluster_SplitJoinInformation.second_triangle.number_of_elements;

#if CONFIG_ENABLE_SCAN_DATA
		secondGenericTreeNode->workload_scan_start_index = firstGenericTreeNode->workload_scan_end_index;
		secondGenericTreeNode->workload_scan_end_index = secondGenericTreeNode->workload_scan_start_index + secondGenericTreeNode->workload_in_subtree;

		assert(cCluster_TreeNode.cGeneric_TreeNode->workload_in_subtree == firstGenericTreeNode->workload_in_subtree + secondGenericTreeNode->workload_in_subtree);
#endif

		/*
		 * STEP 3)
		 */
		/*
		 * SETUP ADJACENT EDGE COMMUNICATION
		 *
		 * since the edge communication data is set up during the initialization by the root simulation handler,
		 * this has to be done now due to the split.
		 * This information is set up based on the split/join information and the information about the adjacent clusters.
		 */

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		remove_nodes_from_comm_data();
#endif

		_pass1_setupAdjacentClusterInformationAfterSplit(
				cCluster_SplitJoinInformation,
				cCluster_EdgeComm_InformationAdjacentClusters,
				cCluster_TreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode,
				cCluster_TreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode
			);

		// finally set joining and splitting permission to false to avoid any further conflicts
		cCluster_SplitJoinInformation.joining_permitted = false;
		cCluster_SplitJoinInformation.splitting_permitted = false;

#if !CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS
		cCluster_TreeNode.freeStacks();
#endif

		this->transferState = SPLITTED_PARENT;
	}



	/**
	 * \brief join both child nodes
	 *
	 * this method is declared as static since it is possible that clusters do not exist
	 * when CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION is activated.
	 */
	static void pass1_joinChildClusters(CGeneric_TreeNode_ *cGeneric_TreeNode)
	{
#if !CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		assert(cGeneric_TreeNode->cCluster_TreeNode != nullptr);
		assert(cGeneric_TreeNode->cCluster_TreeNode->cCluster_SplitJoinActions.transferState == NO_TRANSFER);
#endif


		CCluster_TreeNode_* first_child_node = cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
		CCluster_TreeNode_* second_child_node = cGeneric_TreeNode->second_child_node->cCluster_TreeNode;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		first_child_node->cCluster_SplitJoinActions.remove_nodes_from_comm_data();
		second_child_node->cCluster_SplitJoinActions.remove_nodes_from_comm_data();
#endif

		assert(first_child_node != NULL);
		assert(second_child_node != NULL);

		assert(second_child_node->cCluster_SplitJoinActions.transferState == NO_TRANSFER);
		assert(first_child_node->cCluster_SplitJoinActions.transferState == NO_TRANSFER);


#if CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION

		/*
		 * the cluster tree node is assumed to be deleted
		 */
		assert (cGeneric_TreeNode->cCluster_TreeNode == nullptr);

		CTriangle_Factory parentTriangleFactory;

		parentTriangleFactory.setupParentFromChildFatories(first_child_node->cTriangleFactory, second_child_node->cTriangleFactory);
		cGeneric_TreeNode->cCluster_TreeNode = new CCluster_TreeNode_(parentTriangleFactory);

		CCluster_TreeNode_ &cCluster_TreeNode = *(cGeneric_TreeNode->cCluster_TreeNode);

		cCluster_TreeNode.cGeneric_TreeNode = cGeneric_TreeNode;

		cCluster_TreeNode.setup_SimulationClusterHandler(cGeneric_TreeNode->cCluster_TreeNode->cSimulation_Cluster);

		cCluster_TreeNode.cCluster_UniqueId.setupParentFromChild(first_child_node->cCluster_UniqueId);

#else

		CCluster_TreeNode_ &cCluster_TreeNode = *(cGeneric_TreeNode->cCluster_TreeNode);

#endif



		CCluster_UniqueId &first_child_unique_id = first_child_node->cCluster_UniqueId;
		CCluster_UniqueId &second_child_unique_id = second_child_node->cCluster_UniqueId;

		assert(cGeneric_TreeNode == cCluster_TreeNode.cGeneric_TreeNode);
		assert(&cCluster_TreeNode == cCluster_TreeNode.cGeneric_TreeNode->cCluster_TreeNode);

		assert(first_child_node->cCluster_SplitJoinInformation.joining_permitted);
		assert(second_child_node->cCluster_SplitJoinInformation.joining_permitted);

		assert(first_child_node->cGeneric_TreeNode->isLeaf());
		assert(second_child_node->cGeneric_TreeNode->isLeaf());
		assert(first_child_node->cTriangleFactory.evenOdd == second_child_node->cTriangleFactory.evenOdd);

		CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> &cCluster_EdgeComm_InformationAdjacentClusters = cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters;

		/*
		 * prepare parent's adjacency information
		 */
		cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.clear();
		cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.clear();


		{
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter_first, iter_second;

			size_t number_of_edge_comm_informations = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();
			assert(number_of_edge_comm_informations > 0);

			if (number_of_edge_comm_informations == 1)
			{
				/*
				 * the only edge comm information which exists is shared by both sub-triangles => nothing to do
				 */
			}
			else
			{
				/*************************************
				 * setup new EDGE COMM INFORMATION
				 *************************************/

				/*
				 * FIRST CHILD - CATHETUS
				 */
				iter_first = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();

				/*
				 * iterate to the next-to-last edge comm information
				 */
				for (	size_t i = 0;
						i < number_of_edge_comm_informations-2;
						iter_first++, i++
				)
				{
					CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_first;

					// TODO: another way would be simply to drop the last adjacency information
					if (informationAdjCluster.cCluster_UniqueId == second_child_unique_id)
						continue;

					cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(informationAdjCluster);
				}


				/*
				 * SECOND CHILD - CATHETUS
				 */
				number_of_edge_comm_informations = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size();

				if (number_of_edge_comm_informations == 1)
				{
					/*
					 * only a single edge comm information is available which is shared by both child-triangles
					 */

					cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back((*iter_first));
#if DEBUG
					iter_second = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					assert((*iter_second).cCluster_UniqueId == first_child_node->cCluster_UniqueId);
					iter_first++;
					assert((*iter_first).cCluster_UniqueId == second_child_node->cCluster_UniqueId);
#endif
				}
				else
				{
					/*
					 * check whether it's possible to join the first edge comm information with the last one
					 */

					// setup second iterator
					iter_second = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
#if DEBUG
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter_tmp = iter_first;
					iter_tmp++;
					assert((*iter_second).cCluster_UniqueId == first_child_node->cCluster_UniqueId);
					assert((*iter_second).edge_comm_elements == (*iter_tmp).edge_comm_elements);
#endif
					iter_second++;	// skip first edge comm (shared by both child triangles)

					/*
					 * join adj information
					 */
					if ((*iter_second).cCluster_UniqueId == (*iter_first).cCluster_UniqueId)
					{
						/*
						 * the information about the edge communication data has to be joined since there's only one
						 * adjacent triangle
						 */
						CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_first;

						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(
								CCluster_EdgeComm_InformationAdjacentCluster_(
										informationAdjCluster.cCluster_TreeNode,
										informationAdjCluster.cCluster_UniqueId,
										informationAdjCluster.edge_comm_elements+(*iter_second).edge_comm_elements
#if CONFIG_ENABLE_MPI
										,
										informationAdjCluster.mpi_rank
#endif
									)
								);
					}
					else
					{
						/*
						 * in case of a still splitted edge comm dataset, just push the edge comm information
						 */
						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(*iter_first);
						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(*iter_second);
					}

					iter_second++;

					/**
					 * SECOND CHILD - CATHETUS
					 */
					for (	;
							iter_second != second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
							iter_second++
					)
					{
						CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter_second;

						if (informationAdjCluster.cCluster_UniqueId == first_child_unique_id)
							continue;

						cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(informationAdjCluster);
					}
				}
			}
		}


		{
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter;

			/**
			 * FIRST CHILD - HYPOTENUSE
			 */
			for (	iter = first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					iter != first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					iter++)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter;

				// another way would be simply to drop the last adjacency information
				if (informationAdjCluster.cCluster_UniqueId == second_child_unique_id)
					continue;

				cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(informationAdjCluster);
			}

			/**
			 * SECOND CHILD - HYPOTENUSE
			 */
			for (	iter = second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					iter != second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					iter++)
			{
				CCluster_EdgeComm_InformationAdjacentCluster_ &informationAdjCluster = *iter;

				if (informationAdjCluster.cCluster_UniqueId == first_child_unique_id)
					continue;

				cCluster_TreeNode.cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(informationAdjCluster);
			}
		}


		/***********************************************
		 * Reassemble stacks
		 ***********************************************/

#if !CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS && !CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		assert(cCluster_TreeNode.cStacks == NULL);
#endif

		cCluster_TreeNode.setup_JoinedStacks(
				cCluster_TreeNode.cCluster_SplitJoinInformation,
				CONFIG_SIERPI_CLUSTER_SPLIT_JOIN_ACTION_REUSE_PARENT_STACKS				//< reuse child's stack?
			);

#if CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		cCluster_TreeNode.cSimulation_Cluster->setup(
				first_child_node->cSimulation_Cluster
			);
#endif

		/**
		 * set child nodes of being joined during the last traversal
		 */
		first_child_node->cCluster_SplitJoinActions.transferState = JOINED_CHILD;
		second_child_node->cCluster_SplitJoinActions.transferState = JOINED_CHILD;


		cCluster_TreeNode.cCluster_SplitJoinActions.transferState = JOINED_PARENT;

		// finally set joining and splitting permission to false to avoid any further conflicts
		cCluster_TreeNode.cCluster_SplitJoinInformation.joining_permitted = false;
		cCluster_TreeNode.cCluster_SplitJoinInformation.splitting_permitted = false;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		// simply use rank from first child
		cCluster_TreeNode.migration_dst_rank = first_child_node->migration_dst_rank;
#endif


		cGeneric_TreeNode->setDelayedChildDeletion();
	}



	/**
	 * update the information about the adjacent information after a split operation
	 */
public:
	void _pass1_setupAdjacentClusterInformationAfterSplit(
			const CCluster_SplitJoin_EdgeComm_Information &i_cSplitJoinInformation,	///< split/join information
			const CCluster_EdgeComm_InformationAdjacentClusters_ &i_cParentAdjacentClusters,		///< handler to parent adjacent information
			CCluster_TreeNode_ *io_first_child_node,					///< first child node
			CCluster_TreeNode_ *io_second_child_node					///< second child node
	)
	{

		/**********************
		 * HYPOTENUSE
		 *
		 * the splitting on the hypotenuse has to consider the new edge comm elements between both subtriangles
		 */
		{
			int counter_remaining_edge_comm_elements;

			/*
			 * setup remaining edge communication elements to consider for the first triangle
			 */
			if (cTriangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements =
					i_cSplitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack
					- i_cSplitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements =
					i_cSplitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack
					- i_cSplitJoinInformation.number_of_shared_edge_comm_elements;
			}

			/*
			 * iterate over the first adjacent cluster information of the first sub-triangle
			 */
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator iter =
									i_cParentAdjacentClusters.hyp_adjacent_clusters.begin();

			for (	;
					iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end();
					iter++
			)
			{
				const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.edge_comm_elements;

				/*
				 * either this is the last adjacent information or we have to split this
				 * adjacency information
				 */
				if (counter_remaining_edge_comm_elements <= 0)
					break;

				/*
				 * insert adj information
				 */
				io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
			}

			assert(counter_remaining_edge_comm_elements <= 0);


			/*
			 * Now we are at the point where an adjacent information has to be splitted to account
			 * for both sub-triangles
			 *
			 * The edge comm data marked with \\ or // is reconstructed during the next 4 steps
			 *
			 * |\
			 * |  \[D]
			 * | 2  \\
			 * |    //
			 * |  //[C]
			 * |//        // \\ [A]
			 *       [B]//  1  \
			 *        //____ ____\
			 */

			/*
			 * WE FIRST SETUP [C] to handle [A] & [D] splitted edge and/or node data in a single step
			 */

			/*
			 * [C]
			 */
			// second sub-triangle: shared adj information
			io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
					CCluster_EdgeComm_InformationAdjacentCluster_(
							io_first_child_node,
							io_first_child_node->cCluster_UniqueId,
							i_cSplitJoinInformation.number_of_shared_edge_comm_elements
#if CONFIG_ENABLE_MPI
							,
							-1 // -1 since the adjacent mpi rank is local
#endif
						)
					);


			/*
			 * [A]
			 *
			 * add remaining adjacent edge comm information
			 */
			if (iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end())
			{
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
				const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				// Is this a boundary RLE?
				if (i.edge_comm_elements == 0 && i.cCluster_UniqueId.raw_unique_id == 0)
				{
					assert(counter_remaining_edge_comm_elements == 0);

					// second sub-triangle: new adj information to adj cluster
					io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
					io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
				}
				else
#endif

				{
					/*
					 * this RLE has to be split in case of counter_remaining_edge_comm_elements != 0
					 * otherwise it's just inserted.
					 */
					const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

					// first sub-triangle: new adj information
					io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
							CCluster_EdgeComm_InformationAdjacentCluster_(
									i.cCluster_TreeNode,
									i.cCluster_UniqueId,
									// counter_remaining_edge_comm_elements <= 0
									i.edge_comm_elements + counter_remaining_edge_comm_elements
#if CONFIG_ENABLE_MPI
									,
									i.mpi_rank
#endif
								)
							);
				}
			}


			if (counter_remaining_edge_comm_elements != 0)
			{
				// if there are remaining edge comm elements, there is no node data to be associated to this edge

				/*
				 * [D] remaining edge comm elements?
				 */
				assert(counter_remaining_edge_comm_elements <= 0);

				const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				// second sub-triangle: new adj information to adj cluster
				io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
						CCluster_EdgeComm_InformationAdjacentCluster_(
								i.cCluster_TreeNode,
								i.cCluster_UniqueId,
								-counter_remaining_edge_comm_elements
#if CONFIG_ENABLE_MPI
								,
								i.mpi_rank
#endif
							)
						);

				if (iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end())
					iter++;
			}
			else
#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA || 1
			{
				if (iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end())
					iter++;
			}
#else
			{
				/*
				 * check for node information which has to be added due to split operation
				 */
				// reference to last RLE of A
				const CCluster_EdgeComm_InformationAdjacentCluster_ &iA = *iter;
				if (iA.cCluster_UniqueId.raw_unique_id != 0)
				{
					// second sub-triangle: new adj information to adj cluster
					io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
							CCluster_EdgeComm_InformationAdjacentCluster_(
									iA.cCluster_TreeNode,
									iA.cCluster_UniqueId,
									0						// convert to node data
#if CONFIG_ENABLE_MPI
									,
									iA.mpi_rank
#endif
								)
							);
				}

				assert(counter_remaining_edge_comm_elements == 0);

				if (iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end())
					iter++;

				// Push remaining node data
				for (	;
						iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end();
						iter++
				)
				{
					const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;
					if (i.edge_comm_elements != 0 || i.cCluster_UniqueId.raw_unique_id == 0)
						break;

					io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
					io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
				}

				// push D node information to A
				if (iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end())
				{
					const CCluster_EdgeComm_InformationAdjacentCluster_ &iD = *iter;
					if (iD.cCluster_UniqueId.raw_unique_id != 0)
					{
						io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
								CCluster_EdgeComm_InformationAdjacentCluster_(
										iD.cCluster_TreeNode,
										iD.cCluster_UniqueId,
										0
#if CONFIG_ENABLE_MPI
										,
										iD.mpi_rank
#endif
									)
								);
					}
				}
			}
#endif


			/*
			 * [B]
			 */
			// first sub-triangle: shared adj information
			io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(
					CCluster_EdgeComm_InformationAdjacentCluster_(
							io_second_child_node,
							io_second_child_node->cCluster_UniqueId,
							i_cSplitJoinInformation.number_of_shared_edge_comm_elements
#if CONFIG_ENABLE_MPI
							,
							-1 // -1 since the adjacent mpi rank is local
#endif
						)
					);

			/*
			 * setup remaining edge communication elements for second triangle
			 *
			 * since we already handled some edge comm data elements of the second sub-triangle,
			 * we modify `counter_remaining_edge_comm_elements` relatively to it's current value!
			 */
			if (cTriangleFactory.isHypotenuseDataOnLeftStack())
			{
				counter_remaining_edge_comm_elements +=
					i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack
					- i_cSplitJoinInformation.number_of_shared_edge_comm_elements;
			}
			else
			{
				counter_remaining_edge_comm_elements +=
					i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack
					- i_cSplitJoinInformation.number_of_shared_edge_comm_elements;
			}


			for (	;
					iter != i_cParentAdjacentClusters.hyp_adjacent_clusters.end();
					iter++
			)
			{
				const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

				/*
				 * update remaining edge comm counter
				 */
				counter_remaining_edge_comm_elements -= i.edge_comm_elements;

				/*
				 * insert adj information
				 */
				io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
			}

			assert(counter_remaining_edge_comm_elements == 0);
		}



		/**********************
		 * CATHETI
		 *
		 * IMPORATANT INFO FOR NODE BASED INFORMATION
		 *
		 * process the CATHETI here to first setup the catheti information by setting up comm data for split along the hyp
		 *
		 * splitting on the catheti should be quite easy since the information about the adjacent clusters only
		 * has to be split and no adjacent cluster information has to be split up
		 *
		 * EVEN:
		 * |\
		 * |  \
		 * | 2  \
		 * |    / \
		 * |  /  1  \
		 * |/____ ____\
		 *
		 * ODD:
		 *     /|\
		 *   /2 | 1\
		 * /____|____\
		 *
		 */
		{
			// current child node which is set-up
			CCluster_TreeNode_*current_child_node = io_first_child_node;

			// remaining communication elements on right stack until switching to the second child node
			int counter_remaining_edge_comm_elements;

			// get number of elements on edge communication stack which belong to first triangle
			if (cTriangleFactory.isCathetusDataOnLeftStack())
				counter_remaining_edge_comm_elements = i_cSplitJoinInformation.first_triangle.number_of_edge_comm_elements_left_stack;
			else
				counter_remaining_edge_comm_elements = i_cSplitJoinInformation.first_triangle.number_of_edge_comm_elements_right_stack;

			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator iter =
				i_cParentAdjacentClusters.cat_adjacent_clusters.begin();

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

			if (counter_remaining_edge_comm_elements == 0)
			{
				/*
				 * FIRST ELEMENT IS BOUNDARY MARKER
				 */
				const CCluster_EdgeComm_InformationAdjacentCluster_ &ac = *iter;

#if DEBUG
				if (ac.edge_comm_elements != 0)
				{
					std::cout << "ERROR" << std::endl;
					std::cout << cCluster_UniqueId << std::endl;
					std::cout << "first element is not a boundary marker" << std::endl;
					std::cout << ac << std::endl;
					assert(ac.edge_comm_elements == 0);
				}

				if (cTriangleFactory.isEven())
					assert(cTriangleFactory.edgeTypes.left == CTriangle_Enums::EEdgeType::EDGE_TYPE_BOUNDARY);
				else
					assert(cTriangleFactory.edgeTypes.right == CTriangle_Enums::EEdgeType::EDGE_TYPE_BOUNDARY);
#endif
				// create & insert the information about the adjacent cluster into the communication information
				current_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(ac);

				// the first triangle is processed => get number of elements on edge communication stack which belong to the second triangle
				if (cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
					counter_remaining_edge_comm_elements = i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
				else
					counter_remaining_edge_comm_elements = i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

				current_child_node = io_second_child_node;

				iter++;
			}

#endif

			for (	;
					iter != i_cParentAdjacentClusters.cat_adjacent_clusters.end();
					iter++
			) {
				/*
				 * switch to second sub triangle if updating the adj information of the first one is finished
				 */
				if (counter_remaining_edge_comm_elements == 0)
				{
					/*
					 * this can be also RLE data for nodes stored at the end of the catheti information
					 */
					if (current_child_node != io_second_child_node)
					{
						/*
						 * finish PROCESSING 1st child
						 */

						current_child_node = io_second_child_node;

						// the first triangle is processed => get number of elements on edge communication stack which belong to the second triangle
						if (cTriangleFactory.evenOdd == CTriangle_Enums::EVEN)
							counter_remaining_edge_comm_elements = i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_left_stack;
						else
							counter_remaining_edge_comm_elements = i_cSplitJoinInformation.second_triangle.number_of_edge_comm_elements_right_stack;

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
						for (	;
								iter != i_cParentAdjacentClusters.cat_adjacent_clusters.end();
								iter++
						)
						{
							const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;

							if (i.edge_comm_elements != 0)
								break;

							// => boundary data for 2nd child is handled by next scope
							if (i.cCluster_UniqueId.raw_unique_id == 0)
								break;

							// push node communication information to both RLE encoded communication informations
							// push to cat of first child
							io_first_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
							// push to front of hyp of second child
							io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.insert(
									io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin(),
									i
								);
						}
#endif
					}

#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
					// still counter_remaining_edge_comm_elements == 0 ?
					if (counter_remaining_edge_comm_elements == 0)
					{
						/*
						 * finish PROCESSING 2nd child
						 */
						// handle boundary data
						{
							const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;
							if (i.cCluster_UniqueId.raw_unique_id == 0)
							{
								// boundary element
								io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(i);
								iter++;
								assert(iter == i_cParentAdjacentClusters.cat_adjacent_clusters.end());
							}
						}

						/*
						 * node data
						 */
						for (	;
								iter != i_cParentAdjacentClusters.cat_adjacent_clusters.end();
								iter++
						)
						{
							const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;
							assert(i.edge_comm_elements == 0);
							io_second_child_node->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(i);
						}
						// only RLE=0 data should be stored here
						break;
					}
#endif
				}

				const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *iter;


				// create & insert the information about the adjacent cluster into the communication information
				current_child_node->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(i);

				counter_remaining_edge_comm_elements -= i.edge_comm_elements;

				/*
				 * since splitting along the catheti does not split an adjacent cluster information,
				 * the counter can never be below 0
				 */
				assert(counter_remaining_edge_comm_elements >= 0);
			}

			assert(counter_remaining_edge_comm_elements == 0);
		}


	}





public:
	/**
	 * update information about adjacent clusters whenever a split or join operation was done
	 */
	void pass2_updateAdjacentClusterInformation()
	{
#if !CONFIG_ENABLE_MPI
		assert(cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped == false);
		assert(cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped == false);
#endif

		/*
		 * HYPOTENUSE
		 */
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		if (_pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters,
				hypDataOnClockwiseStack
		))
			cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped = true;

		/*
		 * CATHETI
		 */
		bool catDataOnClockwiseStack = cTriangleFactory.isCathetusDataOnAClockwiseStack();

		if (_pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters,
				catDataOnClockwiseStack
				))
			cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped = true;

		return;
	}



	/**
	 * iterate over local adjacent clusters for hyp or cat
	 *
	 * the idea is to avoid modification of the local information
	 * about adjacent parts if this is not required.
	 *
	 * Otherwise, the new adaptivity information is stored to
	 * o_new_localInformationAdjacentClusters
	 * to overcome any race conditions since adjacent clusters also
	 * lookup local RLE information.
	 */
private:
	bool _pass2_updateAdjacentClusterInformation_LeftOrRightBorder(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &i_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &o_new_localInformationAdjacentClusters,	///< new cat or hyp adjacency information
			bool localStackClockwise
	)
	{
		/*
		 * iterate over all local adjacency informations
		 */
		int counter = 0;
		int remainingCommElements = 0;

		// access first and second child in reversed order?
		bool first_second_reversed = false;



		/*
		 * For node based traversals, ignore optimizations and
		 * directly reconstruct RLE encoding
		 */
		typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator local_iter;

#if !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

		local_iter = i_localInformationAdjacentClusters.begin();
		for (	;
				local_iter != i_localInformationAdjacentClusters.end();
				local_iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			if (local_InformationAdjacentCluster.cCluster_TreeNode == nullptr)
			{
				// this is an MPI node or boundary node => ignore so far.
				// later on we have to send split/join information to adjacent MPI nodes
				counter++;
				continue;
			}
#endif

			/*
			 * if the adjacent cluster tree node is not a leaf, the cluster was split up
			 * during the last traversal.
			 *
			 *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 * maybe we have to modify the edge comm data and do a break here to avoid modifications of
			 * the edge comm data which could be accessed by other adjacent clusters.
			 * modifying this edge comm data can confuse parallel running programs.
			 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 */
			EPARTITION_TRANSFER_STATE transferState = local_InformationAdjacentCluster.cCluster_TreeNode->cCluster_SplitJoinActions.transferState;

			// splitted_child is possible due to split operation
			assert(transferState != JOINED_PARENT);

			if (	transferState == SPLITTED_PARENT ||
					transferState == JOINED_CHILD
			)
			{
				/*
				 * we call a break here to work on a copy of the adjacent information to avoid further race conditions
				 */
				goto splittedOrJoinedAdjacentCluster;
			}

			counter++;
		}
		return false;


splittedOrJoinedAdjacentCluster:

#endif


		/*
		 * this new vector is swapped with the old one (hopefully) after all (also the adjacent ones)
		 * edge communication information was updated
		 */

		/*
		 * COPY adjacent informations!!!
		 *
		 * when modifying the RLE encoding immediately, this can lead to race
		 * conditions with adjacent clusters accessing this structure.
		 */
#if CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

		// remove nodes
		for (	local_iter = i_localInformationAdjacentClusters.begin();
				local_iter != i_localInformationAdjacentClusters.end();
				local_iter++
		)
		{
			if (	(*local_iter).cCluster_UniqueId.isBoundary() ||
					(*local_iter).edge_comm_elements != 0
			)
				o_new_localInformationAdjacentClusters.push_back(*local_iter);
		}

#else

		o_new_localInformationAdjacentClusters = i_localInformationAdjacentClusters;

#endif

		local_iter = o_new_localInformationAdjacentClusters.begin();
		for (int i = 0; i < counter; i++)
				local_iter++;

		for (	;
				local_iter != o_new_localInformationAdjacentClusters.end();
				local_iter++
		)
		{
for_loop_start:
			CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
			if (local_InformationAdjacentCluster.cCluster_TreeNode == nullptr)
			{
				// this is an MPI node => ignore so far.
				// later on we have to send split/join information to adjacent MPI nodes
				continue;
			}
#endif


			// this can happen for 2 splitted child nodes
			assert(local_InformationAdjacentCluster.cCluster_TreeNode->cCluster_SplitJoinActions.transferState != JOINED_PARENT);

			remainingCommElements = local_InformationAdjacentCluster.edge_comm_elements;

			if (local_InformationAdjacentCluster.cCluster_TreeNode->cCluster_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   SPLIT PARENT
				 ***********************************************************************************
				 ***********************************************************************************/

				// store parent cluster tree node since this value could be modified
				//CClusterTreeNode_ *parentClusterTreeNode = local_InformationAdjacentCluster.cCluster_TreeNode;
				CCluster_TreeNode_ &adjacent_parentClusterTreeNode = *(local_InformationAdjacentCluster.cCluster_TreeNode->cGeneric_TreeNode->cCluster_TreeNode);

				/**
				 * in case that the local triangle was splitted, we have to consider the correct order of edge comms.
				 *
				 * since we know that the adjacent cluster can be split only along the hypotenuse, this simplifies our assumptions.
				 */
				first_second_reversed = (adjacent_parentClusterTreeNode.cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == localStackClockwise);

				/**
				 * check wether the cluster as well as the adjacent cluster was splitted and the current splitted tree is the second child
				 */
				CCluster_TreeNode_ *firstChildToTraverse, *secondChildToTraverse;

				if (cTriangleFactory.isSecondChild())
					first_second_reversed = !first_second_reversed;

				if (first_second_reversed)
				{
					firstChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode;
					secondChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
				}
				else
				{
					firstChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
					secondChildToTraverse = adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode;
				}

				if (transferState == SPLIT_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLITTED CHILD
					 * ADJ:   SPLIT PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						if (remainingCommElements > 0)
						{
							/**
							 * search for remaining edge comm data
							 */

							// a new edge comm information element has to be inserted
							local_iter++;
							local_iter = o_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

							/**
							 * if an additional edge comm element was created, maybe those have to be reversed
							 */
							if (cTriangleFactory.isSecondChild())
							{
								typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
								*local_iter = *prev_iter;

								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
										*prev_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							else
							{
								// SECOND SUBTRIANGLE
								_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
										*local_iter,	// use new local iterator node
										secondChildToTraverse,
										remainingCommElements
								);
							}
							continue;
						}

//						if (remainingCommElements < 0)
						{
							/**
							 * here we handle the special case when the adjacent edge comm data was larger than our local one.
							 * in this case, the local one was splitted without a split at the adjacent edge comm information.
							 *
							 * however, we are allowed to simply ignore this case by assuming that the adjacent cluster
							 * updates its edge information data to match to our split local edge comm information.
							 */
							continue;
						}
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(	first_second_reversed ?
								adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode :
								adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode
							),
							remainingCommElements
					);
					assert(retval);

					/*
					 * since this triangle can be split without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements <= 0);

					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   SPLIT PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						// perfect match
						if (remainingCommElements == 0)
							continue;

						assert(remainingCommElements > 0);

						/*
						 * search for remaining edge comm data
						 */
						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = o_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

						/*
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (cTriangleFactory.isSecondChild())
						{
							typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode : adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode),
							remainingCommElements
					);
					assert(retval);

					/*
					 * since this triangle can be split without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
#if DEBUG
					if (remainingCommElements > 0)
						std::cerr << "DEBUG: check comment above this piece of code" << std::endl;
#endif
					assert(remainingCommElements <= 0);

					continue;
				}

				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   SPLIT PARENT
					 ***********************************************************************************
					 ***********************************************************************************/

					// FIRST SUBTRIANGLE
					if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							local_InformationAdjacentCluster,
							firstChildToTraverse,
							remainingCommElements
					))
					{
						assert(remainingCommElements >= 0);

						// perfect match
						if (remainingCommElements == 0)
							continue;

						/*
						 * search for remaining edge comm data
						 */

						// a new edge comm information element has to be inserted
						local_iter++;
						local_iter = o_new_localInformationAdjacentClusters.insert(local_iter, CCluster_EdgeComm_InformationAdjacentCluster_());

						/*
						 * if an additional edge comm element was created, maybe those have to be reversed
						 */
						if (cTriangleFactory.isSecondChild())
						{
							typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator prev_iter = local_iter-1;
							*local_iter = *prev_iter;

							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*prev_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}
						else
						{
							// SECOND SUBTRIANGLE
							_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
									*local_iter,	// use new local iterator node
									secondChildToTraverse,
									remainingCommElements
							);
						}

						assert(remainingCommElements == 0);
						continue;
					}

					/*
					 * no match was found in the first cluster.
					 *
					 * => simply take the first match of the second adjacent cluster.
					 */
					// SECOND SUBTRIANGLE
#if DEBUG
					bool retval =
#endif
					_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
							*local_iter,	// use new local iterator node
							(first_second_reversed ? adjacent_parentClusterTreeNode.cGeneric_TreeNode->first_child_node->cCluster_TreeNode : adjacent_parentClusterTreeNode.cGeneric_TreeNode->second_child_node->cCluster_TreeNode),
							remainingCommElements
					);
#if DEBUG
					if (!retval)
					{
						std::cout << std::endl;
						std::cout << "_pass2_updateAdjacentClusterInformation_LeftOrRightBorder for cluster " << cCluster_UniqueId << std::endl;
						std::cout << "information on cluster" << std::endl;
						std::cout << *local_iter << std::endl;
						std::cout << "not found" << std::endl;
						std::cout << std::endl;
						std::cout << "remainingCommElements: " << remainingCommElements << std::endl;
						assert(false);
					}
#endif

					/**
					 * since this triangle can be splitted without knowledge of the split operation of the adjacent one,
					 * remainingCommElements can be negative, too
					 */
					assert(remainingCommElements == 0);

					continue;
				}
			}


			if (local_InformationAdjacentCluster.cCluster_TreeNode->cCluster_SplitJoinActions.transferState == JOINED_CHILD)
			{
				/***********************************************************************************
				 ***********************************************************************************
				 * ADJ:   JOINED CHILD
				 ***********************************************************************************
				 ***********************************************************************************/

				if (transferState == SPLIT_CHILD)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: SPLIT CHILD
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_cCluster_TreeNode = local_InformationAdjacentCluster.cCluster_TreeNode;

					remainingCommElements = local_InformationAdjacentCluster.edge_comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_cCluster_TreeNode,
							remainingCommElements
					);

					/*
					 * maybe both cluster were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == o_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).cCluster_TreeNode;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
					// join handled elsewhere
					if (next_adjacent_parentClusterTreeNode == nullptr)
						continue;
#endif
					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentClusterTreeNode->cCluster_UniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.cCluster_UniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).edge_comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.edge_comm_elements += (*next_local_iter).edge_comm_elements;

						local_iter = o_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == o_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}

				if (transferState == JOINED_PARENT)
				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: JOINED PARENT
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_cCluster_TreeNode = local_InformationAdjacentCluster.cCluster_TreeNode;

					remainingCommElements = local_InformationAdjacentCluster.edge_comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_cCluster_TreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == o_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).cCluster_TreeNode;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
					// join handled elsewhere
					if (next_adjacent_parentClusterTreeNode == nullptr)
						continue;
#endif
					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */
					if (next_adjacent_parentClusterTreeNode->cCluster_UniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.cCluster_UniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).edge_comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.edge_comm_elements += (*next_local_iter).edge_comm_elements;

						local_iter = o_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == o_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 0
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
					continue;
				}


				{
					/***********************************************************************************
					 ***********************************************************************************
					 * LOCAL: NOTHING
					 * ADJ:   JOINED CHILD
					 ***********************************************************************************
					 ***********************************************************************************/

					CCluster_TreeNode_ *adjacent_cCluster_TreeNode = local_InformationAdjacentCluster.cCluster_TreeNode;

					remainingCommElements = local_InformationAdjacentCluster.edge_comm_elements;

					// parent triangle
					pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
							local_InformationAdjacentCluster,
							adjacent_cCluster_TreeNode,
							remainingCommElements
					);

					/**
					 * maybe both subtriangles were joined and the next edge comm data matches?
					 */
					typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator next_local_iter = local_iter+1;

					if (next_local_iter == o_new_localInformationAdjacentClusters.end())
						break;

					CCluster_TreeNode_ *next_adjacent_parentClusterTreeNode = (*next_local_iter).cCluster_TreeNode;

#if CONFIG_ENABLE_MPI || CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
					// join handled elsewhere
					if (next_adjacent_parentClusterTreeNode == nullptr)
						continue;
#endif

					/*
					 * if the next adjacent parent's node has the same id, we can join both edge comm datasets
					 */

					if (next_adjacent_parentClusterTreeNode->cCluster_UniqueId.getParentRawUniqueId() == local_InformationAdjacentCluster.cCluster_UniqueId.raw_unique_id)
					{
#if DEBUG
						remainingCommElements = (*next_local_iter).edge_comm_elements;

						pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
								*next_local_iter,
								next_adjacent_parentClusterTreeNode,
								remainingCommElements
						);

						assert(remainingCommElements == 0);
#endif

						local_InformationAdjacentCluster.edge_comm_elements += (*next_local_iter).edge_comm_elements;

						local_iter = o_new_localInformationAdjacentClusters.erase(next_local_iter);
						if (local_iter == o_new_localInformationAdjacentClusters.end())
							break;

						assert(remainingCommElements == 0);
#if 1
						goto for_loop_start;
#else
						local_iter--;
#endif

						// local_iter is incremented right here
						continue;
					}
				}

				continue;
			}
		}
		return true;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent cluster
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool pass2_updateAdjacentClusterInformation_ParentJoinedTriangle(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
	)
	{
		if (_pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentCluster,
				p_adjacentClusterTreeNode->cGeneric_TreeNode->parent_node->cCluster_TreeNode,
				p_remainingCommElements,
				p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters)
		)
			return true;

#if DEBUG
		bool retval =
#endif
		_pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
				p_localInformationAdjacentCluster,
				p_adjacentClusterTreeNode->cGeneric_TreeNode->parent_node->cCluster_TreeNode,
				p_remainingCommElements,
				p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters
				);

		assert(retval);

		// true has to be always returned since some elements are still left
		return true;
	}


private:
	/**
	 * JOIN
	 *
	 * search for adjacency information on the adjacent edge comm information `p_edgeComm_InformationAdjacentCluster`
	 *
	 * return true, when some p_remainingCommElements is undershot and the local edge comm information has to be update once again
	 */
	bool _pass2_updateAdjacentClusterInformation_ParentJoinedTriangles_SpecificChildEdge(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,	///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentParentClusterTreeNode,					///< adjacent parent cluster tree node to update adjacency information
			int &p_remainingCommElements,												///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_edgeComm_ChildInformationAdjacentCluster
	)
	{
		/**
		 * iterate over all edge comm data of the adjacently lying joined child triangle
		 */
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = p_edgeComm_ChildInformationAdjacentCluster.begin();
				iter != p_edgeComm_ChildInformationAdjacentCluster.end();
				iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local cluster
			 */

			/*
			 * test with currently local cCluster_UniqueId but also with the child's cCluster_UniqueId
			 */
			if (transferState == SPLIT_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != (cCluster_UniqueId.raw_unique_id >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id >> 1) != cCluster_UniqueId.raw_unique_id)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != cCluster_UniqueId.raw_unique_id)
					continue;
			}


			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentCluster.cCluster_UniqueId = p_adjacentParentClusterTreeNode->cCluster_UniqueId;
			p_localInformationAdjacentCluster.cCluster_TreeNode = p_adjacentParentClusterTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				p_localInformationAdjacentCluster.edge_comm_elements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				p_localInformationAdjacentCluster.edge_comm_elements = adjacent_informationAdjacentCluster.edge_comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;


				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_ChildInformationAdjacentCluster.end())
					return true;
				CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

				if (transferState == SPLIT_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != (cCluster_UniqueId.raw_unique_id >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id >> 1) != cCluster_UniqueId.raw_unique_id)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != cCluster_UniqueId.raw_unique_id)
						return true;
				}

				p_localInformationAdjacentCluster.edge_comm_elements += adjacent_informationAdjacentCluster.edge_comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other sibling triangle cares about getting the right data
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentCluster.edge_comm_elements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;
				return true;
			}

			assert(false);
		}

		return false;
	}



private:
	/**
	 * update the local adjacency information based on the adjacency information of an adjacent cluster
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
	)
	{
		if (_pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentCluster,
					p_adjacentClusterTreeNode,
					p_remainingCommElements,
					p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters
		))
			return true;

		return _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
					p_localInformationAdjacentCluster,
					p_adjacentClusterTreeNode,
					p_remainingCommElements,
					p_adjacentClusterTreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters
				);
	}



private:
	/**
	 * update the local adjacency information along the cat or hyp
	 *
	 * \return	true, if there are some remainingCommElements left
	 */
	bool _pass2_updateAdjacentClusterInformation_AdjacentSplitted_LeafSubTriangle_SpecifiedEdge(
			CCluster_EdgeComm_InformationAdjacentCluster_ &p_localInformationAdjacentCluster,		///< local information about adjacent clusters (equal to *p_local_iter)
			CCluster_TreeNode_ *p_adjacentClusterTreeNode,									///< adjacent first or second cluster tree node to search for adjacent edge comm information
			int &p_remainingCommElements,														///< remaining edge comm elements which have to be considered for p_localInformationAdjacentClusters
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_edgeComm_InformationAdjacentCluster
	)
	{
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = p_edgeComm_InformationAdjacentCluster.begin();
				iter != p_edgeComm_InformationAdjacentCluster.end();
				iter++
		)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;

			/*
			 * test whether the adjacent id is equal to the id of the local cluster
			 */
			if (transferState == SPLIT_CHILD)
			{
				// compare with local parent id
				if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != (cCluster_UniqueId.raw_unique_id >> 1))
					continue;
			}
			else if (transferState == JOINED_PARENT)
			{
				// compare with adjacent parent id
				if ((adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id >> 1) != cCluster_UniqueId.raw_unique_id)
					continue;
			}
			else
			{
				if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != cCluster_UniqueId.raw_unique_id)
					continue;
			}

			/*
			 * update local information for this triangle
			 */
			p_localInformationAdjacentCluster.cCluster_UniqueId = p_adjacentClusterTreeNode->cCluster_UniqueId;
			p_localInformationAdjacentCluster.cCluster_TreeNode = p_adjacentClusterTreeNode;

			/*
			 * A) Perfect match
			 */
			if (p_remainingCommElements == adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				p_localInformationAdjacentCluster.edge_comm_elements = p_remainingCommElements;
				p_remainingCommElements = 0;
				return true;
			}

			/*
			 * B) local adj information too large
			 *
			 * in case that the split operation was made splitting the communication edge,
			 * the remaining Communication Elements have to be forwarded to the next split
			 *
			 *  => the other piece of the edge communication information should be left on the second adjacent sub-triangle
			 */
			if (p_remainingCommElements > adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				p_localInformationAdjacentCluster.edge_comm_elements = adjacent_informationAdjacentCluster.edge_comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;

				/*
				 * check wether the next comm data also belongs to this edge comm information
				 */
				iter++;
				if (iter == p_edgeComm_InformationAdjacentCluster.end())
					return true;
				CCluster_EdgeComm_InformationAdjacentCluster_ &adjacent_informationAdjacentCluster = *iter;


				/*
				 * test whether the adjacent id is equal to the id of the local cluster
				 */
				if (transferState == SPLIT_CHILD)
				{
					// compare with local parent id
					if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != (cCluster_UniqueId.raw_unique_id >> 1))
						return true;
				}
				else if (transferState == JOINED_PARENT)
				{
					// compare with adjacent parent id
					if ((adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id >> 1) != cCluster_UniqueId.raw_unique_id)
						return true;
				}
				else
				{
					if (adjacent_informationAdjacentCluster.cCluster_UniqueId.raw_unique_id != cCluster_UniqueId.raw_unique_id)
						return true;
				}

				p_localInformationAdjacentCluster.edge_comm_elements += adjacent_informationAdjacentCluster.edge_comm_elements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;

				return true;
			}

			/*
			 * C) not enough local comm data
			 *
			 * in this case, the local communication was also splitted.
			 * we don't have to care about this situation since the other
			 * sibling triangle cares about getting the right data.
			 */
			if (p_remainingCommElements < adjacent_informationAdjacentCluster.edge_comm_elements)
			{
				// clamp to p_remainingCommElements
				p_localInformationAdjacentCluster.edge_comm_elements = p_remainingCommElements;
				p_remainingCommElements -= adjacent_informationAdjacentCluster.edge_comm_elements;
				return true;
			}

			assert(false);
		}

		return false;
	}



public:
	/**
	 * this method has to be executed for all nodes!
	 */
	static void pass3_swapAndCleanAfterUpdatingEdgeComm(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode
	)
	{
		if (i_cGeneric_TreeNode->cCluster_TreeNode == nullptr)
			return;

		CCluster_TreeNode_ *cCluster_TreeNode = i_cGeneric_TreeNode->cCluster_TreeNode;

		/*
		 * free child nodes due to a cluster join operation
		 */
		if (i_cGeneric_TreeNode->delayed_child_deletion)
		{
			assert(cCluster_TreeNode->cCluster_SplitJoinActions.transferState == JOINED_PARENT);
			i_cGeneric_TreeNode->freeChildren();
		}
		else
		{
#if CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
			if (cCluster_TreeNode->cCluster_SplitJoinActions.transferState == SPLITTED_PARENT)
			{
				delete cCluster_TreeNode;
				i_cGeneric_TreeNode->cCluster_TreeNode = nullptr;
				return;
			}
#endif
		}

		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.swapAndCleanAfterUpdatingEdgeComm();

#if !CONFIG_ENABLE_MPI && !CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA
		cCluster_TreeNode->cCluster_SplitJoinActions.transferState = NO_TRANSFER;
#endif
	}


#if !CONFIG_ENABLE_MPI && CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA

	/**
	 * find adjacent vertex communication information and append them to o_new_localInformationAdjacentClusters.
	 *
	 * return true if a circular connection was found
	 */
	bool _pass4_findAndAppendVertexCommData(
			const CCluster_UniqueId &start_cCluster_UniqueId,	///< unique id of cluster for which the search was started for
			const CCluster_UniqueId *prev_cCluster_UniqueId,	///< unique id of cluster previously visited
			CCluster_TreeNode_ *currently_processed_cAdjacentCluster,			///< adjacent cluster
			bool i_adjacentStackClockwise,						///< is edge comm info from previous cluster clockwise or counter-clockwise?
			bool i_adjVertexAtRLEStart,							///< track the vertex data at the beginning of the RLE
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &o_new_localInformationAdjacentClusters	///< new cat or hyp adjacency information
	)
	{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
		std::cout << " + _pass4_findAndAppendVertexCommData iteration START" << std::endl;
#endif

		// hit boundary
		if (currently_processed_cAdjacentCluster == nullptr)
			return false;

		bool first = true;

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
		int s = 1;
#endif

		// if we end up at the starting cluster, we found a "circular" connection
		while(true)
		{

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
			std::cout << " + _pass4_findAndAppendVertexCommData iteration " << s << std::endl;
			std::cout << "     +              start: " << start_cCluster_UniqueId << std::endl;
			std::cout << "     +               prev: " << *prev_cCluster_UniqueId << std::endl;
			std::cout << "     +                adj: " << currently_processed_cAdjacentCluster->cCluster_UniqueId << std::endl;
			std::cout << "     +          clockwise: " << i_adjacentStackClockwise << std::endl;
			std::cout << "     + vertex @ rle start: " << i_adjVertexAtRLEStart << std::endl;
			std::cout << std::endl;
			s++;
#endif
			assert(start_cCluster_UniqueId != currently_processed_cAdjacentCluster->cCluster_UniqueId);

			/*
			 * A) search cluster prev_cCluster_UniqueId in comm information of
			 *    cluster currently_processed_cAdjacentCluster
			 */
			typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator local_iter;

			/*
			 * the data has to be on a stack with data created in the same clock direction
			 */
			if (currently_processed_cAdjacentCluster->cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == i_adjacentStackClockwise)
			{
				// search for corresponding communication element on local RLE encoded communication information
				for (	local_iter = currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
						local_iter != currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
						local_iter++
				)
				{
					if ((*local_iter).cCluster_UniqueId == *prev_cCluster_UniqueId)
						break;
				}

#if DEBUG
				if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end())
				{
					assert(false);
					std::cerr << prev_cCluster_UniqueId << " not found!!!!!!!!!!!" << std::endl;
				}
#endif
			}
			else
			{
				// search for corresponding communication element on local RLE encoded communication information
				for (	local_iter = currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
						local_iter != currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
						local_iter++
				)
				{

					if ((*local_iter).cCluster_UniqueId == *prev_cCluster_UniqueId)
						break;
				}
#if DEBUG
				if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end())
				{
					assert(false);
					std::cerr << prev_cCluster_UniqueId << " not found!!!!!!!!!!!" << std::endl;
				}
#endif
			}

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
			const CCluster_EdgeComm_InformationAdjacentCluster_ &i = *local_iter;

			std::cout << " + ADJ Cluster RLE info found: " << i << std::endl;
#endif

			// next edge comm information to track
			const CCluster_EdgeComm_InformationAdjacentCluster_ *tracked_edgeComm_Information;

			if (i_adjVertexAtRLEStart)
			{

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
				std::cout << "TRACK PREV" << std::endl;
#endif

				//
				// load previous element
				//
				// local_iter--;
				// we first have to check for equality to begin()
				//

				// should be
				if (currently_processed_cAdjacentCluster->cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == i_adjacentStackClockwise)
				{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "   + currently on hyp" << std::endl;
#endif
					if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin())
					{

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + front of cat" << std::endl;
#endif
						// go to cathetus RLE information
						tracked_edgeComm_Information = &(currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.front());
						i_adjacentStackClockwise = !i_adjacentStackClockwise;
						i_adjVertexAtRLEStart = !i_adjVertexAtRLEStart;
					}
					else
					{
						local_iter--;

						// go to cathetus RLE information
						tracked_edgeComm_Information = &(*local_iter);
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + prev: " << *tracked_edgeComm_Information << std::endl;
#endif
					}
				}
				else
				{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "   + currently on cat" << std::endl;
#endif
					if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin())
					{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + front of hyp" << std::endl;
#endif

						assert(currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0);

						// go to cathetus RLE information
						tracked_edgeComm_Information = &(currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.front());
						i_adjacentStackClockwise = !i_adjacentStackClockwise;
						i_adjVertexAtRLEStart = !i_adjVertexAtRLEStart;
					}
					else
					{
						local_iter--;

						// go to cathetus RLE information
						tracked_edgeComm_Information = &(*local_iter);

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + prev: " << *tracked_edgeComm_Information << std::endl;
#endif
					}
				}
			}
			else
			{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
				std::cout << "TRACK NEXT" << std::endl;
				std::cout << " + current edge comm: " << *local_iter << std::endl;
#endif

				// load next element
				local_iter++;

				// should be
				if (currently_processed_cAdjacentCluster->cTriangleFactory.isHypotenuseDataOnAClockwiseStack() == i_adjacentStackClockwise)
				{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "   + currently on hyp" << std::endl;
#endif
					if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end())
					{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + end of cathetii" << std::endl;
#endif

						// go to cathetus RLE information
						tracked_edgeComm_Information = &(currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.back());
						i_adjacentStackClockwise = !i_adjacentStackClockwise;
						i_adjVertexAtRLEStart = !i_adjVertexAtRLEStart;
					}
					else
					{
						// go to cathetus RLE information
						tracked_edgeComm_Information = &(*local_iter);
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + next: " << *tracked_edgeComm_Information << std::endl;
#endif
					}
				}
				else
				{

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "   + currently on cat" << std::endl;
#endif
					if (local_iter == currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end())
					{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + end of hypotenuse" << std::endl;
#endif

						// go to cathetus RLE information
						tracked_edgeComm_Information = &(currently_processed_cAdjacentCluster->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back());
						i_adjacentStackClockwise = !i_adjacentStackClockwise;
						i_adjVertexAtRLEStart = !i_adjVertexAtRLEStart;
					}
					else
					{
						// go to cathetus RLE information
						tracked_edgeComm_Information = &(*local_iter);

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
						std::cout << "     + next: " << *tracked_edgeComm_Information << std::endl;
#endif
					}
				}
			}


#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
			std::cout << "next tracked edge comm: " << *tracked_edgeComm_Information << std::endl;
#endif


			/*
			 * INSERT VERTEX COMMUNICATION INFORMATION HERE
			 */
			if (!first)
			{
				/*
				 * do not follow in case of boundaries
				 */
				if (tracked_edgeComm_Information->cCluster_UniqueId.isBoundary())
				{

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "inserting boundary vertex data" << std::endl;
#endif
					// only insert RLE if this is not the first vertex
					o_new_localInformationAdjacentClusters.push_back(
						CCluster_EdgeComm_InformationAdjacentCluster_(
								currently_processed_cAdjacentCluster,
								currently_processed_cAdjacentCluster->cCluster_UniqueId,
								0
						)
					);

					return false;
				}


				/*
				 * only possible with MPI which we currently do not support
				 */
				assert(tracked_edgeComm_Information->cCluster_TreeNode != 0);

				// do not traverse back to start cell
				if (start_cCluster_UniqueId == tracked_edgeComm_Information->cCluster_TreeNode->cCluster_UniqueId)
				{
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "CIRCULAR CONNECTION DETECTED => FIN" << std::endl;
#endif
					return true;	// circular connection detected
				}

				// only insert RLE if this is not the first vertex
				o_new_localInformationAdjacentClusters.push_back(
					CCluster_EdgeComm_InformationAdjacentCluster_(
							currently_processed_cAdjacentCluster,
							currently_processed_cAdjacentCluster->cCluster_UniqueId,
							0
					)
				);
#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
				std::cout << " not FIRST: insert: " << o_new_localInformationAdjacentClusters.back() << std::endl;
#endif
			}
			else
			{
				if (tracked_edgeComm_Information->cCluster_UniqueId.isBoundary())
					return false;

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
				std::cout << " FIRST: do not insert!" << std::endl;
#endif
				first = false;
			}


			/*
			 * PREPARE NEXT ITERATION
			 */

			//const CCluster_UniqueId &start_cCluster_UniqueId,	///< unique id of cluster for which the search was started for
			//const CCluster_UniqueId *prev_cCluster_UniqueId,	///< unique id of cluster previously visited
			prev_cCluster_UniqueId = &(currently_processed_cAdjacentCluster->cCluster_UniqueId);

			//const CCluster_TreeNode_ *currently_processed_cAdjacentCluster,		///< adjacent cluster
			currently_processed_cAdjacentCluster = tracked_edgeComm_Information->cCluster_TreeNode;

			//bool i_adjacentStackClockwise,					///< is edge comm info from previous cluster clockwise or counter-clockwise?
			// VALUE inverted above if appropriate

			//bool i_adjVertexAtRLEStart,				///< track the vertex data at the beginning of the RLE

			//std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &o_new_localInformationAdjacentClusters	///< new cat or hyp adjacency information
		}
	}


public:
	void pass4_SM_updateVertexCommData()
	{
		/**
		 * TODO: this completely throws away vertex data and recomputes it.
		 *
		 * this is not really the most efficient way, but it works quite fast for shared memory :-).
		 */
#if !CONFIG_ENABLE_MPI
		assert(cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped == false);
		assert(cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped == false);
#endif

		/*
		 * HYPOTENUSE
		 *
		 * |\
		 * |  \
		 * |    \
		 * |      \
		 * ---------
		 *
		 * track vertices at the beginning of each RLE
		 */
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
		std::cout << "********************************************************" << std::endl;
		std::cout << "* pass4_SM_updateVertexCommData for " << cCluster_UniqueId << std::endl;
		std::cout << "********************************************************" << std::endl;

		std::cout << "HYPOTENUSE updates" << std::endl;
#endif
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator
					local_iter = cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin();
					local_iter != cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.end();
					local_iter++
		)
		{
			const CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

			// search vertex communication data and append
			if (!_pass4_findAndAppendVertexCommData(
					cCluster_UniqueId,					// this cluster id
					&cCluster_UniqueId,					// previously visited cluster id
					local_InformationAdjacentCluster.cCluster_TreeNode,		// adjacent cluster to visit
					hypDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
					false,								// If vertex data is created at start of RLE for the hypotenuse
														// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
					cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters		// RLE encoded edge+vertex list
			))
			{
				assert(local_iter == cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.begin());

				assert(cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.size() > 0);
				CCluster_EdgeComm_InformationAdjacentCluster_ *adjacent = &(cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.front());

				if (!adjacent->cCluster_UniqueId.isBoundary())
				{
					assert(adjacent->cCluster_TreeNode != 0);

					if (_pass4_findAndAppendVertexCommData(
							cCluster_UniqueId,					// this cluster id
							&cCluster_UniqueId,					// previously visited cluster id
							adjacent->cCluster_TreeNode,		// adjacent cluster to visit
							!hypDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
							false,								// If vertex data is created at start of RLE for the hypotenuse
																// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
							cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters		// RLE encoded edge+vertex list
					))
					{
						assert(false);
						std::cerr << "SEVERE VERTEX CONNECTIVITY ERROR, circular and noncircular connection found" << std::endl;
					}
				}
			}

			// always postfix hyp RLE data
			cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.push_back(local_InformationAdjacentCluster);
		}

		cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped = true;



		/*
		 * CATHETI
		 *
		 * track vertices at the end of each RLE
		 */
		bool catDataOnClockwiseStack = cTriangleFactory.isCathetusDataOnAClockwiseStack();

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
		std::cout << "********************************************************" << std::endl;
		std::cout << "* pass4_SM_updateVertexCommData: " << cCluster_UniqueId << std::endl;
		std::cout << "********************************************************" << std::endl;

		std::cout << "CATHETI updates" << std::endl;
#endif
		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator
					local_iter = cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.begin();
					local_iter != cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end();
					local_iter++
		)
		{
			const CCluster_EdgeComm_InformationAdjacentCluster_ &local_InformationAdjacentCluster = *local_iter;

			// always prefix cat RLE data
			cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.push_back(local_InformationAdjacentCluster);

			if (local_InformationAdjacentCluster.cCluster_UniqueId.isBoundary())
			{
				/*
				 * if this is a boundary edge, the adjacent vertices starting at the next RLE have to be attached
				 */

				typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::const_iterator local_iter2 = local_iter+1;
				if (local_iter2 == cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end())
				{
					const CCluster_EdgeComm_InformationAdjacentCluster_ &next_rle = cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back();

					if (next_rle.cCluster_UniqueId.isBoundary())
						continue;

					if (_pass4_findAndAppendVertexCommData(
										cCluster_UniqueId,					// this cluster id
										&cCluster_UniqueId,					// previously visited cluster id
										next_rle.cCluster_TreeNode,			// adjacent cluster to visit
										!catDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
										true,								// If vertex data is created at start of RLE for the catotenuse
																			// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
										cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters		// RLE encoded edge+vertex list
								))
					{
						assert(false);
						std::cerr << "THERE MAY NOT BE A CIRCULAR CONNECTION" << std::endl;
					}
				}
				else
				{
					const CCluster_EdgeComm_InformationAdjacentCluster_ &next_rle = *local_iter2;

					if (next_rle.cCluster_UniqueId.isBoundary())
						continue;

					if (_pass4_findAndAppendVertexCommData(
										cCluster_UniqueId,					// this cluster id
										&cCluster_UniqueId,					// previously visited cluster id
										next_rle.cCluster_TreeNode,			// adjacent cluster to visit
										catDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
										false,								// If vertex data is created at start of RLE for the catotenuse
																			// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
										cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters		// RLE encoded edge+vertex list
								))
					{
						assert(false);
						std::cerr << "THERE MAY NOT BE A CIRCULAR CONNECTION" << std::endl;
					}
				}
				continue;
			}


			// search vertex communication data and append
			if (!_pass4_findAndAppendVertexCommData(
					cCluster_UniqueId,					// this cluster id
					&cCluster_UniqueId,					// previously visited cluster id
					local_InformationAdjacentCluster.cCluster_TreeNode,		// adjacent cluster to visit
					catDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
					true,								// If vertex data is created at start of RLE for the catotenuse
														// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
					cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters		// RLE encoded edge+vertex list
			))
			{
				if (local_iter+1 == cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.end())
				{
					assert(cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.size() > 0);
					CCluster_EdgeComm_InformationAdjacentCluster_ *adjacent = &(cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.back());

#if DEBUG_CONFIG_ENABLE_TRAVERSATORS_WITH_NODE_DATA_OUTPUT
					std::cout << "non-circular handling" << std::endl;
					std::cout << *adjacent << std::endl;
#endif

					if (adjacent->cCluster_UniqueId.isBoundary())
					{
					}
					else
					{
						assert(adjacent->cCluster_TreeNode != 0);

						if (_pass4_findAndAppendVertexCommData(
								cCluster_UniqueId,					// this cluster id
								&cCluster_UniqueId,					// previously visited cluster id
								adjacent->cCluster_TreeNode,		// adjacent cluster to visit
								!catDataOnClockwiseStack,			// clockwise or counter-clockwise stack access of adjacent traversals
								true,								// If vertex data is created at start of RLE for the hypotenuse
																	// it has to be created at the end of the adjacent RLE since the direction has to be reversed.
								cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters		// RLE encoded edge+vertex list
						))
						{
							assert(false);
							std::cerr << "SEVERE VERTEX CONNECTIVITY ERROR, circular and noncircular connection found" << std::endl;
						}
					}
				}
			}

		}

		cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped = true;
	}



	static void pass4_SM_cleanup(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode
	)
	{
		assert(i_cGeneric_TreeNode->cCluster_TreeNode != nullptr);

		CCluster_TreeNode_ *cCluster_TreeNode = i_cGeneric_TreeNode->cCluster_TreeNode;

		// in case that the RLE encoding was updated with vertex information,
		// swap and cleanup
#if 1
		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.swapAndCleanAfterUpdatingEdgeComm();

#else

		// HYP
		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.clear();

		for (
				auto local_iter_hyp = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.begin();
				local_iter_hyp != cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.end();
				local_iter_hyp++
		) {
			if (	(*local_iter_hyp).cCluster_UniqueId.isBoundary() ||
					(*local_iter_hyp).edge_comm_elements != 0
			)
				cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters.push_back(*local_iter_hyp);
		}
		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_hyp_adjacent_clusters.clear();

		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.hyp_swapOrNotToBeSwapped = false;


		// CAT
		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.clear();

		for (	auto local_iter_cat = cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.begin();
				local_iter_cat != cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.end();
				local_iter_cat++
		) {
			if (	(*local_iter_cat).cCluster_UniqueId.isBoundary() ||
					(*local_iter_cat).edge_comm_elements != 0
			)
				cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters.push_back(*local_iter_cat);
		}
		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.new_cat_adjacent_clusters.clear();

		cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.cat_swapOrNotToBeSwapped = false;
#endif


		assert(!CONFIG_ENABLE_MPI);

#if !CONFIG_ENABLE_MPI
		cCluster_TreeNode->cCluster_SplitJoinActions.transferState = NO_TRANSFER;
#endif
	}

#endif



#if CONFIG_ENABLE_MPI

private:
	/**
	 * iterate over local adjacent cluster information and send cluster
	 * information to adjacent MPI nodes
	 */
	inline void _pass4_DM_sendAdjacentClusterInformation_LeftOrRightBorder(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			int edge_clockwise,
			std::vector<SSplitJoinDMInformation> *io_edge_dm_split_join_dm_information
	) {
		if (io_edge_dm_split_join_dm_information->size() != p_localInformationAdjacentClusters.size())
			io_edge_dm_split_join_dm_information->resize(p_localInformationAdjacentClusters.size());

		for (size_t i = 0; i < p_localInformationAdjacentClusters.size(); i++)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = p_localInformationAdjacentClusters[i];

			if (c.mpi_rank == -1)
			{
				/*
				 * not possible to modify MPI rank here since this information
				 * is later on used to wait for requests.
				 */
				assert(c.cCluster_TreeNode != nullptr);
				continue;
			}

			(*io_edge_dm_split_join_dm_information)[i].edge_comm_elements = c.edge_comm_elements;
			(*io_edge_dm_split_join_dm_information)[i].transferState = transferState;
			(*io_edge_dm_split_join_dm_information)[i].cCluster_UniqueId = cCluster_UniqueId;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
			(*io_edge_dm_split_join_dm_information)[i].mpi_rank = cCluster_TreeNode.migration_dst_rank;

//			if (cCluster_TreeNode.migration_dst_rank >= 0)
//				std::cout << "sending cluster update data: " << cCluster_TreeNode.migration_dst_rank << std::endl;
#endif

			assert(c.mpi_requests[0] == MPI_REQUEST_NULL);

			MPI_Isend(
					&((*io_edge_dm_split_join_dm_information)[i]),			///< number of communication elements
					sizeof(SSplitJoinDMInformation),	///< size
					MPI_BYTE,					///< data type
					c.mpi_rank,					///< destination rank
					edge_clockwise,				///< tag
					MPI_COMM_WORLD,				///< communicator
					&(c.mpi_requests[0])
				);
		}
	}



public:
	/**
	 * iterate over local adjacent cluster information for the cat and hyp and send edge comm information to adjacent clusters
	 */
	void pass4_DM_sendAdjacentClusterSplitJoinInformation()
	{
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		// hypotenuse
		_pass4_DM_sendAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				hypDataOnClockwiseStack,
				&hyp_edge_dm_split_join_dm_information
		);

		// catheti
		_pass4_DM_sendAdjacentClusterInformation_LeftOrRightBorder(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				!hypDataOnClockwiseStack,
				&cat_edge_dm_split_join_dm_information
		);
	}



	/**
	 * iterate over local adjacent cluster information for the cat and hyp
	 */
private:
	void _pass4_DM_recvAdjacentClusterInformation(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &io_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			int edge_direction,
			SSplitJoinDMInformation *io_remaining_edge_comm_elements,
			std::vector<SSplitJoinDMInformation> *edge_dm_split_join_dm_information
	) {
		int rank = sierpi::CGlobalComm::getCommRank();

		/*
		 * !!!
		 * wait for MPI requests to be successfully submitted
		 * since we are going to possibly modify the buffers by resizing the vector
		 * !!!
		 */
		for (size_t i = 0; i < io_localInformationAdjacentClusters.size(); i++)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = io_localInformationAdjacentClusters[i];

			if (c.mpi_rank == -1)
				continue;

			assert(c.mpi_requests[0] != MPI_REQUEST_NULL);
			MPI_Wait(&(c.mpi_requests[0]), MPI_STATUS_IGNORE);
		}


		/*
		 * this flag helps considering double joined clusters crossed by MPI border
		 */
		bool test_double_join_with_next_element = false;

		/*
		 * this flag is set to true whenever a local edge communication should be joined with the previous one
		 */
		bool local_edge_comm_join = false;

		// we have to use an iterator for insert/erase operations on the array
		typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = io_localInformationAdjacentClusters.end();
		iter--;

		for (int ec = (int)io_localInformationAdjacentClusters.size()-1; ec >= 0; ec--, iter--)	// !!! Also decrement iterator here !!!
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = *iter;

			if (c.mpi_rank == -1)
				continue;

			if (io_remaining_edge_comm_elements->edge_comm_elements > 0)
			{
				/*
				 * This edge comm element belongs to adjacent data which was processed before
				 *
				 * In case that the edge communication was processed for this cluster,
				 * local_edge_comm_join is enabled.
				 *
				 * Otherwise this edge comm was processed by the sibling.
				 */
#if DEBUG
				if (io_remaining_edge_comm_elements->edge_comm_elements != c.edge_comm_elements)
				{
					std::cout << rank << ": remaining edge comm mismatch #2!!!" << std::endl;
					std::cout << "io_remaining_edge_comm_elements: " << io_remaining_edge_comm_elements->edge_comm_elements << std::endl;
					std::cout << "c.edge_comm_elements: " << c.edge_comm_elements << std::endl;
					std::cout << "local_edge_comm_join: " << local_edge_comm_join << std::endl;
					assert(false);
				}
#endif

				if (local_edge_comm_join)
				{
					/*
					 * remove this edge communication to join it with the previous one
					 * iter then points to the next element.
					 */
					iter = io_localInformationAdjacentClusters.erase(iter);

					/*
					 * since we are processing the edge comm vector in reversed order,
					 * the last edge comm element has to exist!
					 */
					assert(iter != io_localInformationAdjacentClusters.end());

					// update edge comm elements of previous edge comm information
					(*iter).edge_comm_elements += io_remaining_edge_comm_elements->edge_comm_elements;

					local_edge_comm_join = false;
				}
				else
				{
					// delayed update of unique id
					c.cCluster_UniqueId = io_remaining_edge_comm_elements->cCluster_UniqueId;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
					if (io_remaining_edge_comm_elements->mpi_rank >= 0)
						c.mpi_rank = io_remaining_edge_comm_elements->mpi_rank;
#endif

				}

				io_remaining_edge_comm_elements->edge_comm_elements = 0;
				continue;
			}


			/*
			 * receive number of adjacent edge communication elements
			 */
			SSplitJoinDMInformation cSplitJoinDMInformation;

			MPI_Recv(
					&cSplitJoinDMInformation,
					sizeof(SSplitJoinDMInformation),
					MPI_BYTE,
					c.mpi_rank,
					edge_direction,
					MPI_COMM_WORLD,
					MPI_STATUS_IGNORE
				);


#if DEBUG
			/*
			 * some validations
			 */
			if (cSplitJoinDMInformation.transferState == SPLIT_CHILD)
			{
				CCluster_UniqueId firstChild_cCluster_UniqueId, secondChild_cCluster_UniqueId;

				firstChild_cCluster_UniqueId.setupFirstChildFromParent(c.cCluster_UniqueId);
				secondChild_cCluster_UniqueId.setupSecondChildFromParent(c.cCluster_UniqueId);

				if (cSplitJoinDMInformation.cCluster_UniqueId != firstChild_cCluster_UniqueId && cSplitJoinDMInformation.cCluster_UniqueId != secondChild_cCluster_UniqueId)
				{
					std::cerr << rank << ": MISMATCH IN UNIQUE IDS (SPLITTED_CHILD)" << std::endl;
					assert(false);
				}
			}
			else if (cSplitJoinDMInformation.transferState == JOINED_PARENT)
			{
				CCluster_UniqueId parent_cCluster_UniqueId;

				parent_cCluster_UniqueId.setupParentFromChild(c.cCluster_UniqueId);

				if (cSplitJoinDMInformation.cCluster_UniqueId != parent_cCluster_UniqueId)
				{
					std::cerr << rank << ": MISMATCH IN UNIQUE IDS (JOINED_PARENT)" << std::endl;
					assert(false);
				}
			}
			else
			{
				if (cSplitJoinDMInformation.transferState != NO_TRANSFER)
				{
					std::cout << "INVALID TRANSFER STATE: " << cSplitJoinDMInformation.transferState << std::endl;
					assert(false);
				}
			}
#endif


			/*
			 * number of adjacent edge comm elements == local edge comm elements
			 */
			if (cSplitJoinDMInformation.edge_comm_elements == c.edge_comm_elements)
			{
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
				/*
				 * Rank update due to cluster migration?
				 *
				 * WARNING: DO NOT MOVE THIS UPDATE DIRECTLY AFTER THE RECEIVE OPERATION!!!
				 * otherwise another MPI_Recv on the c.mpi_rank cannot be done!
				 */
				if (cSplitJoinDMInformation.mpi_rank >= 0)
				{
//					std::cout << "RANK UPDATE for cluster " << cCluster_TreeNode.cCluster_UniqueId << " to " << cSplitJoinDMInformation.mpi_rank << std::endl;
					c.mpi_rank = cSplitJoinDMInformation.mpi_rank;
				}
#endif

				// test for adjacent split/join
				if (cSplitJoinDMInformation.transferState == NO_TRANSFER)
					continue;


				if (transferState == JOINED_PARENT)
				{
					/**
					 * in case that 4 triangles are coarsened in one step,
					 * we have to handle it in a special way.
					 */
					if (test_double_join_with_next_element)
					{
						CCluster_EdgeComm_InformationAdjacentCluster_ &prev_c = io_localInformationAdjacentClusters[ec+1];

						if (prev_c.cCluster_UniqueId == cSplitJoinDMInformation.cCluster_UniqueId)
						{
							/*
							 * simply delete one edge comm element and add
							 * remaining elements to current edge comm element counter
							 */

							prev_c.edge_comm_elements += c.edge_comm_elements;

							iter = io_localInformationAdjacentClusters.erase(iter);
							test_double_join_with_next_element = false;
							continue;
						}

						test_double_join_with_next_element = false;
						c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;
					}
					else
					{
						test_double_join_with_next_element = true;

						// IMPORANT: the unique ID from the adjacent cluster is now stored to c.cCluster_UniqueId !!!
						c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;
					}
					continue;
				}

				// if there was a split or join, simply update cCluster_UniqueId
				c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;

				continue;
			}
			test_double_join_with_next_element = false;


			if (cSplitJoinDMInformation.edge_comm_elements > c.edge_comm_elements)
			{
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
				/*
				 * Rank update due to cluster migration?
				 *
				 * WARNING: DO NOT MOVE THIS UPDATE DIRECTLY AFTER THE RECEIVE OPERATION!!!
				 * otherwise another MPI_Recv on the c.mpi_rank cannot be done!
				 */
				if (cSplitJoinDMInformation.mpi_rank >= 0)
				{
					c.mpi_rank = cSplitJoinDMInformation.mpi_rank;
				}
#endif

				/**
				 * CASE A: adjacent_edge_comm_elements > c.edge_comm_elements
				 *
				 * case A2) Local cluster was split:
				 *     _________
				 *     \        |
				 * |\    \      |
				 * |  \    \    |
				 * |   /\    \  |
				 * |  /   \    \|
				 * |/_______\
				 *
				 * => Forward remaining edge communication counter to next leaf
				 *
				 *
				 *
				 * case A2) adjacent cluster was joined:
				 *     _________
				 *     \       .|
				 * |\    \   .  |
				 * |  \    \.   |
				 * |    +    \  |
				 * |      \    \|
				 * |________\
				 *
				 * => locally search for joinable edge comm information
				 *
				 *
				 *
				 * case A3) adjacent cluster was joined and local cluster was split:
				 *     _________
				 *     \       .|
				 * |\    \   .  |
				 * |  \    \.   |
				 * |   /+    \  |
				 * |  /   \    \|
				 * |/_______\
				 *
				 * => Forward remaining edge communication counter to next leaf
				 *
				 *
				 *
				 * case A4) adjacent cluster was joined and local cluster was split:
				 *     _____
				 *    \   .|
				 * |\   \. |
				 * | +\   \|
				 * |   /\
				 * |  /   \
				 * |/_______\
				 *
				 * => locally search for joinable edge comm information
				 */

				io_remaining_edge_comm_elements->edge_comm_elements = cSplitJoinDMInformation.edge_comm_elements-c.edge_comm_elements;
				io_remaining_edge_comm_elements->cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
				io_remaining_edge_comm_elements->mpi_rank = cSplitJoinDMInformation.mpi_rank;
#endif

				if (cSplitJoinDMInformation.transferState != NO_TRANSFER)
					c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;

				// enable local edge comm join
				local_edge_comm_join = true;

				continue;
			}


			assert (cSplitJoinDMInformation.edge_comm_elements < c.edge_comm_elements);

			{
//				std::cout << "cSplitJoinDMInformation.edge_comm_elements < c.edge_comm_elements" << std::endl;

				/**
				 * CASE B: c.edge_comm_elements > adjacent_edge_comm_elements
				 *
				 *
				 * case B2) local cluster was joined:
				 *     _________
				 *     \        |
				 * |\    \      |
				 * |  \    +    | adjacent cluster
				 * |   .\    \  |
				 * |  .   \    \|
				 * |._______\
				 *
				 *
				 * case B1) adjacent cluster was split:
				 *     _________
				 *     \       /|
				 * |\    \   /  |
				 * |  \    \/   | adjacent cluster
				 * |    \    \  |
				 * |      \    \|
				 * |________\
				 *
				 */

				// we can expect that the next cluster edge information belongs to this cluster

				SSplitJoinDMInformation next_cSplitJoinDMInformation;

				/*
				 * receive number of adjacent edge communication elements
				 */
				MPI_Recv(
						&next_cSplitJoinDMInformation,
						sizeof(SSplitJoinDMInformation),
						MPI_BYTE,
						c.mpi_rank,
						edge_direction,
						MPI_COMM_WORLD,
						MPI_STATUS_IGNORE
					);

#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
				/*
				 * Rank update due to cluster migration?
				 *
				 * WARNING: DO NOT MOVE THIS UPDATE DIRECTLY AFTER THE RECEIVE OPERATION!!!
				 * otherwise another MPI_Recv on the c.mpi_rank cannot be done!
				 */
				if (cSplitJoinDMInformation.mpi_rank >= 0)
				{
					assert(cSplitJoinDMInformation.mpi_rank == next_cSplitJoinDMInformation.mpi_rank);

					c.mpi_rank = cSplitJoinDMInformation.mpi_rank;
				}
#endif


#if DEBUG
				if (cSplitJoinDMInformation.edge_comm_elements + next_cSplitJoinDMInformation.edge_comm_elements != c.edge_comm_elements)
				{
					std::cout << "STRANGE STUFF!!!" << std::endl;
					assert(false);
				}

				if (cSplitJoinDMInformation.transferState == SPLIT_CHILD)
				{
					CCluster_UniqueId firstChild_cCluster_UniqueId, secondChild_cCluster_UniqueId;

					firstChild_cCluster_UniqueId.setupFirstChildFromParent(c.cCluster_UniqueId);
					secondChild_cCluster_UniqueId.setupSecondChildFromParent(c.cCluster_UniqueId);

					if (	cSplitJoinDMInformation.cCluster_UniqueId != firstChild_cCluster_UniqueId			&&	// split is also possible not along the communication edge
							next_cSplitJoinDMInformation.cCluster_UniqueId != secondChild_cCluster_UniqueId
					) {
						if (cSplitJoinDMInformation.cCluster_UniqueId != firstChild_cCluster_UniqueId)
						{
							std::cout << "cSplitJoinDMInformation.cCluster_UniqueId != firstChild_cCluster_UniqueId" << std::endl;
							std::cout << cSplitJoinDMInformation.cCluster_UniqueId << " != " << firstChild_cCluster_UniqueId << std::endl;
							std::cout << "next_cSplitJoinDMInformation.cCluster_UniqueId ? secondChild_cCluster_UniqueId" << std::endl;
							std::cout << next_cSplitJoinDMInformation.cCluster_UniqueId << " ? " << secondChild_cCluster_UniqueId << std::endl;
							assert(false);
						}

						if (next_cSplitJoinDMInformation.cCluster_UniqueId != secondChild_cCluster_UniqueId)
						{
							std::cout << "cSplitJoinDMInformation.cCluster_UniqueId ? firstChild_cCluster_UniqueId" << std::endl;
							std::cout << cSplitJoinDMInformation.cCluster_UniqueId << " ? " << firstChild_cCluster_UniqueId << std::endl;
							std::cout << "next_cSplitJoinDMInformation.cCluster_UniqueId != secondChild_cCluster_UniqueId" << std::endl;
							std::cout << next_cSplitJoinDMInformation.cCluster_UniqueId << " != " << secondChild_cCluster_UniqueId << std::endl;
							assert(false);
						}
					}
				}
#endif
				if (cSplitJoinDMInformation.cCluster_UniqueId != next_cSplitJoinDMInformation.cCluster_UniqueId)
				{
					/*
					 * case B1) adjacent split operation: account for SECOND CHILD
					 */

					/*
					 * FIRST CHILD
					 */

					/*
					 * UNIQUE IDs: update unique IDs to account for adjacent split
					 */

					c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;
					c.edge_comm_elements = cSplitJoinDMInformation.edge_comm_elements;


					/*
					 * SECOND CHILD
					 */
					iter = io_localInformationAdjacentClusters.insert(
							iter,
							CCluster_EdgeComm_InformationAdjacentCluster_(
									nullptr,								// adjacent cluster node does not exist
									next_cSplitJoinDMInformation.cCluster_UniqueId,	// unique Id of second child

									// counter_remaining_edge_comm_elements <= 0
									next_cSplitJoinDMInformation.edge_comm_elements
#if CONFIG_ENABLE_MPI
									,
									c.mpi_rank	// same rank or in case of a migration the new rank
#endif
								)
							);
				}
				else
				{
					/*
					 * case B2) if the local triangle joined, maybe the edge communication also can be joined
					 *
					 * this is the case when both unique ids are identical
					 */

					assert(cSplitJoinDMInformation.cCluster_UniqueId == next_cSplitJoinDMInformation.cCluster_UniqueId);

					// update unique ID in case of an adjacent change
					c.cCluster_UniqueId = cSplitJoinDMInformation.cCluster_UniqueId;
#if DEBUG
					if (c.edge_comm_elements != cSplitJoinDMInformation.edge_comm_elements + next_cSplitJoinDMInformation.edge_comm_elements)
					{
						std::cout << "B2: c.edge_comm_elements != cSplitJoinDMInformation.edge_comm_elements + next_cSplitJoinDMInformation.edge_comm_elements" << std::endl;
						assert(false);
					}
#endif
				}
			}
		}
	}



public:
	/**
	 * iterate over local adjacent cluster information for the cat and hyp
	 */
	void pass4_DM_recvAdjacentClusterSplitJoinInformation(
			SSplitJoinDMInformation *remaining_edge_comm_clockwise_edge,
			SSplitJoinDMInformation *remaining_edge_comm_counterclockwise_edge
	)
	{
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		// hypotenuse
		_pass4_DM_recvAdjacentClusterInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				hypDataOnClockwiseStack,
				(hypDataOnClockwiseStack ? remaining_edge_comm_clockwise_edge : remaining_edge_comm_counterclockwise_edge),
				&hyp_edge_dm_split_join_dm_information
		);

		// catheti
		_pass4_DM_recvAdjacentClusterInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				!hypDataOnClockwiseStack,
				(hypDataOnClockwiseStack ? remaining_edge_comm_counterclockwise_edge : remaining_edge_comm_clockwise_edge),
				&cat_edge_dm_split_join_dm_information
		);


		// cleanup states
#if !CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		// TODO: test if this works
		if (cCluster_TreeNode.cCluster_SplitJoinActions.transferState == SPLIT_CHILD)
			cCluster_TreeNode.cGeneric_TreeNode->parent_node->cCluster_TreeNode->cCluster_SplitJoinActions.transferState = NO_TRANSFER;
#endif

		transferState = NO_TRANSFER;
	}

#endif





#if CONFIG_ENABLE_MPI && DEBUG

private:
	/**
	 * iterate over local adjacent cluster information and send cluster information to adjacent mpi nodes
	 */
	inline void _pass5_sendValidationInformation(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &p_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			int edge_clockwise,
			std::vector<SSplitJoinDMInformation> *io_edge_dm_split_join_dm_information
	) {
		if (io_edge_dm_split_join_dm_information->size() != p_localInformationAdjacentClusters.size())
			io_edge_dm_split_join_dm_information->resize(p_localInformationAdjacentClusters.size());

		for (size_t i = 0; i < p_localInformationAdjacentClusters.size(); i++)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = p_localInformationAdjacentClusters[i];

			if (c.mpi_rank == -1)
				continue;

			(*io_edge_dm_split_join_dm_information)[i].edge_comm_elements = c.edge_comm_elements;
			(*io_edge_dm_split_join_dm_information)[i].transferState = transferState;
			(*io_edge_dm_split_join_dm_information)[i].cCluster_UniqueId = cCluster_UniqueId;
			(*io_edge_dm_split_join_dm_information)[i].adj_cCluster_UniqueId = c.cCluster_UniqueId;

			assert(c.mpi_requests[0] == MPI_REQUEST_NULL);

			MPI_Isend(
					&((*io_edge_dm_split_join_dm_information)[i]),			///< number of communication elements
					sizeof(SSplitJoinDMInformation),	///< size
					MPI_BYTE,					///< data type
					c.mpi_rank,					///< destination rank
					edge_clockwise,				///< tag
					MPI_COMM_WORLD,				///< communicator
					&(c.mpi_requests[0])
				);
		}
	}



public:
	/**
	 * send validation information
	 */
	void pass5_DM_sendValidationInformation()
	{
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		// hypotenuse
		_pass5_sendValidationInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				hypDataOnClockwiseStack,
				&hyp_edge_dm_split_join_dm_information
		);

		// catheti
		_pass5_sendValidationInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				!hypDataOnClockwiseStack,
				&cat_edge_dm_split_join_dm_information
		);
	}



	/**
	 * receive validation information and compare
	 */
private:
	void _pass5_DM_recvValidationInformation(
			std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &io_localInformationAdjacentClusters,		///< cat or hyp adjacency information
			int edge_direction,
			std::vector<SSplitJoinDMInformation> *edge_dm_split_join_dm_information
	) {
		/*
		 * !!!
		 * wait for MPI requests to be successfully submitted
		 * since we are going to possibly modify the buffers by resizing the vector
		 * !!!
		 */
		for (size_t i = 0; i < io_localInformationAdjacentClusters.size(); i++)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = io_localInformationAdjacentClusters[i];

			if (c.mpi_rank == -1)
				continue;

			assert(c.mpi_requests[0] != MPI_REQUEST_NULL);
			MPI_Wait(&(c.mpi_requests[0]), MPI_STATUS_IGNORE);
		}


		// we have to use an iterator for insert/erase operations on the array
		typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = io_localInformationAdjacentClusters.end();
		iter--;

		for (int ec = (int)io_localInformationAdjacentClusters.size()-1; ec >= 0; ec--, iter--)	// !!! Also decrement iterator here !!!
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &c = *iter;

			if (c.mpi_rank == -1)
				continue;


			/*
			 * receive number of adjacent edge communication elements
			 */
			SSplitJoinDMInformation cSplitJoinDMInformation;

			MPI_Recv(
					&cSplitJoinDMInformation,
					sizeof(SSplitJoinDMInformation),
					MPI_BYTE,
					c.mpi_rank,
					edge_direction,
					MPI_COMM_WORLD,
					MPI_STATUS_IGNORE
				);

			assert(cCluster_TreeNode.cCluster_UniqueId == cCluster_UniqueId);

			if (	cSplitJoinDMInformation.edge_comm_elements != c.edge_comm_elements	||
					cSplitJoinDMInformation.adj_cCluster_UniqueId != cCluster_UniqueId					||
					cSplitJoinDMInformation.cCluster_UniqueId != c.cCluster_UniqueId
			)
			{
				std::cout << "VALIDATION ERROR in cluster " << cCluster_TreeNode.cCluster_UniqueId << std::endl;
				std::cout << " +               adjacent rank: " << c.mpi_rank << std::endl;
				std::cout << " + Adjacent      transferState: " << cSplitJoinDMInformation.transferState << std::endl;
				std::cout << " +          edge_comm_elements: " << cSplitJoinDMInformation.edge_comm_elements << std::endl;
				std::cout << " +                    cCluster_UniqueId: " << cSplitJoinDMInformation.cCluster_UniqueId << std::endl;
				std::cout << " +                adj cCluster_UniqueId: " << cSplitJoinDMInformation.adj_cCluster_UniqueId << std::endl;
				std::cout << " +    Local      transferState: " << transferState << std::endl;
				std::cout << " +          edge_comm_elements: " << c.edge_comm_elements << std::endl;
				std::cout << " +                    cCluster_UniqueId: " << cCluster_TreeNode.cCluster_UniqueId << std::endl;
				std::cout << " +                adj cCluster_UniqueId: " << c.cCluster_UniqueId << std::endl;
				std::cout << std::endl;
				assert(false);
				exit(-1);
			}
		}
	}


public:
	/**
	 * receive validation information
	 */
	void pass5_DM_recvValidationInformation()
	{
		bool hypDataOnClockwiseStack = cTriangleFactory.isHypotenuseDataOnAClockwiseStack();

		// hypotenuse
		_pass5_DM_recvValidationInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.hyp_adjacent_clusters,
				hypDataOnClockwiseStack,
				&hyp_edge_dm_split_join_dm_information
		);

		// catheti
		_pass5_DM_recvValidationInformation(
				cCluster_EdgeComm_InformationAdjacentClusters.cat_adjacent_clusters,
				!hypDataOnClockwiseStack,
				&cat_edge_dm_split_join_dm_information
		);
	}


#endif


public:
	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CCluster_SplitJoinActions &p
	)
	{
		std::cout << " + transferState: " << p.transferState << std::endl;
		return co;
	}
};

}

#endif /* CCLUSTER_SPLITJOINACTION_HPP_ */
