/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CCluster_TreeNode.hpp
 *
 *  Created on: April 20, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_TREENODE_HPP_
#define CCLUSTER_TREENODE_HPP_

#include "global_config.h"
#include "CCluster_SplitJoinInformation.hpp"
#include "CCluster_EdgeComm_InformationAdjacentClusters.hpp"
#include "CCluster_SplitJoinActions.hpp"
#include "CCluster_UniqueId.hpp"
#include "CDomainClusters.hpp"
#include "../stacks/CSimulationStacks.hpp"
#include "../generic_tree/CGeneric_TreeNode.hpp"

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	#include "libsierpi/stacks/CValidationStacks.hpp"
#endif


namespace sierpi
{

/**
 * \brief Container for all data and methods which is needed to execute the computations on this cluster
 * in parallel without requesting any data from the adjacent cells.
 *
 * All important features for parallel processing are included in this cluster.
 * Among others, this basic features are:
 *  - cCluster_SplitJoinActions				- to run split- and join-operations
 *  - cCluster_SplitJoinInformation			- information which is necessary to do fast split/joins.
 *  											  this information is created during the last backward adaptive traversal.
 *  - cCluster_AdaptiveSplitJoinInformation	- split/join informations after adaptive step
 *  - cCluster_EdgeComm_InformationAdjacentClusters		- information about adjacent clusters to communicate with
 *
 * A pointer to the Simulation_ClusterHandler is also stored which implements the application developer
 * handlers.
 *
 * Methods of GenericTreeNode are inherited directly to this class to get a node of the domain triangulation tree!
 */
template <typename CSimulation_Cluster>
class CCluster_TreeNode
{
public:
	/*
	 * typedefs inherited from CCluster !!!
	 */
	typedef CSimulation_Cluster CSimulation_Cluster_;
	typedef typename CSimulation_Cluster::CSimulationTypes CSimulationTypes_;
	typedef typename CSimulation_Cluster::CVisualizationTypes CVisualizationTypes_;

	typedef typename CSimulationTypes_::CEdgeData CEdgeData_;
	typedef typename CSimulationTypes_::CCellData CCellData_;

	typedef CGeneric_TreeNode<CCluster_TreeNode<CSimulation_Cluster> > CGeneric_TreeNode_;

	/*
	 * more typedefs for convenience
	 */
	typedef CCluster_TreeNode<CSimulation_Cluster> CCluster_TreeNode_;

	typedef CCluster_EdgeComm_InformationAdjacentCluster<CCluster_TreeNode_> CEdgeComm_InformationAdjacentCluster_;
	typedef CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> CEdgeComm_InformationAdjacentClusters_;

	typedef CSimulationStacks<CSimulationTypes_,CVisualizationTypes_> CSimulationStacks_;


public:
	/**
	 * user's simulation handler which actually runs all the computations
	 */
	CSimulation_Cluster *cSimulation_Cluster;

	/**
	 * generic tree node
	 */
	CGeneric_TreeNode_ *cGeneric_TreeNode;

	/**
	 * cluster's unique ID
	 */
	CCluster_UniqueId cCluster_UniqueId;

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
//	CMigrationClassX<CCluster_UniqueId> migration_uniqueId;
	CMigrationClass<CCluster_UniqueId> initial_migration_uniqueId;
#endif


	/**
	 * all stacks we need during the simulation
	 */
	CSimulationStacks_ *cStacks;

	/**
	 * this flag is set to true, if the stacks are reused from a different node (the parent node)
	 * and should not be neither allocated nor deallocated
	 */
	bool reuse_parent_stacks;
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	CMigrationAtomic<bool> migration_reuse_parent_stacks;
#endif


#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
	/**
	 * validation stacks needed?
	 */
	CValidationStacks *cValidationStacks;
#endif



	/**
	 * the instance of the triangle factory for this cluster
	 *
	 * IMPORTANT: this is no reference!
	 */
	CTriangle_Factory cTriangleFactory;

#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
	CMigrationClass<CTriangle_Factory> initial_migration_cTriangleFactory;
#endif

	/**
	 * split and join actions
	 */
	CCluster_SplitJoinActions<CSimulation_Cluster> cCluster_SplitJoinActions;

	/**
	 * split/join information which should be used for data exchange
	 */
	CCluster_SplitJoin_EdgeComm_Information cCluster_SplitJoinInformation;

	/**
	 * split/join information right after the adaptive step
	 */
	CCluster_SplitJoin_EdgeComm_Information cCluster_AdaptiveSplitJoinInformation;

	/**
	 * the adjacency information to other clusters is stored right here
	 */
	CCluster_EdgeComm_InformationAdjacentClusters<CCluster_TreeNode_> cCluster_EdgeComm_InformationAdjacentClusters;




#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

	/**
	 * destination rank of cluster for migration or -1 in case of no migration
	 */
	int migration_dst_rank;

	/**
	 * source rank of cluster for migration or -1 in case of no migration
	 */
	int migration_src_rank;

	/**
	 * mpi request for fin message
	 */
	MPI_Request mpi_request_fin_message;

	/**
	 * final message, also to have synchronization.
	 * if this value was send, all previous messages can also be assumed to be sent
	 */
	int fin_message;


#define DEBUG_WITH_TEST_FLAGS	1

#if DEBUG_WITH_TEST_FLAGS
	int testflags[10];
	CMigrationAtomic<int> cMigration_testflags[10];
#endif



	/**
	 * request a migration to the given rank
	 */
	inline void requestMigrationToRank(
			int i_dst_rank
	) {
		migration_dst_rank = i_dst_rank;
	}

	/**
	 * return whether a migration is requested
	 */
	inline bool isMigrationRequested()
	{
		return migration_dst_rank >= 0;
	}

	/**
	 * preprocess migration
	 */
	void migration_preprocessing()
	{
		// nothing to do
	}


	/**
	 * this method is executed when the migration for this cluster takes place to dst_rank
	 */
	void migration_send(
		int i_src_rank,
		int i_dst_rank
	) {
		assert(migration_dst_rank == i_dst_rank);

#if DEBUG_WITH_TEST_FLAGS
		for (int i = 0; i < 10; i++)
			testflags[i] = i+123;

		cMigration_testflags[0].send(testflags[0], i_dst_rank);
#endif

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		CMigration::sendAtomic(reuse_parent_stacks, i_dst_rank);
#else
		migration_reuse_parent_stacks.send(reuse_parent_stacks, i_dst_rank);
#endif

#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[1].send(testflags[1], i_dst_rank);
#endif

		cSimulation_Cluster->migration_send(i_dst_rank);

#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[2].send(testflags[2], i_dst_rank);
#endif

		/*
		 * setup elsewhere
		 */
		//CGeneric_TreeNode_ *cGeneric_TreeNode;

		/*
		 * RECURSIVE SEND
		 */
		cStacks->migration_send(i_dst_rank);

#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[3].send(testflags[3], i_dst_rank);
#endif

	#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks->migration_send(i_dst_rank);
	#endif

#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[4].send(testflags[4], i_dst_rank);
#endif

		// nothing to do
		//CCluster_SplitJoinActions<CSimulation_Cluster> cCluster_SplitJoinActions;

		// nothing to do (no split/join information)
		//CCluster_SplitJoin_EdgeComm_Information cCluster_SplitJoinInformation;

		// nothing to do (no split/join information)
		//CCluster_SplitJoin_EdgeComm_Information cCluster_AdaptiveSplitJoinInformation;

		cCluster_EdgeComm_InformationAdjacentClusters.migration_send(i_src_rank, i_dst_rank);
		
#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[5].send(testflags[5], i_dst_rank);
#endif
	}

	/**
	 * wait for mpi request of fin message
	 */
	void migration_send_postprocessing(
			int i_dst_rank
	) {
#if !CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION

//		migration_uniqueId.wait();
		migration_reuse_parent_stacks.wait();
//		migration_cTriangleFactory.wait();

		cSimulation_Cluster->migration_send_postprocessing(i_dst_rank);

		/*
		 * setup elsewhere
		 */
		//CGeneric_TreeNode_ *cGeneric_TreeNode;

		/*
		 * RECURSIVE SEND
		 */
		cStacks->migration_send_postprocessing(i_dst_rank);

	#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks->migration_send_postprocessing(i_dst_rank);
	#endif

#endif

		/*
		 * execute postprocessing to remove connections to locally adjacent clusters
		 */
		cCluster_EdgeComm_InformationAdjacentClusters.migration_send_postprocessing(this, i_dst_rank);


#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		/*
		 * receive final message and remember mpi request to check whether sending this cluster was done
		 */
		CMigration::recvAtomic(fin_message, migration_dst_rank);
#endif

#if DEBUG_WITH_TEST_FLAGS
		cMigration_testflags[6].send(testflags[6], i_dst_rank);
#endif

		for (int i = 0; i < 7; i++)
			cMigration_testflags[i].wait();
	}




#if DEBUG_WITH_TEST_FLAGS
	void testtestflag(int i, int i_src_rank)
	{
//		std::cout << "Testing flag " << i << std::endl;
		CMigration::recvAtomic(testflags[i], i_src_rank);
		if (testflags[i] != i+123)
		{
			std::cerr << "TESTFLAG " << i << " mismatch!" << std::endl;
			assert(false);
			exit(-1);
		}
	}
#endif


	/**
	 * this method is executed to receive migration data from src_rank
	 */
	void migration_recv(
			int i_src_rank
	) {
#if DEBUG_WITH_TEST_FLAGS
		testtestflag(0, i_src_rank);
#endif

//		CMigration::recvRawClass(cCluster_UniqueId, i_src_rank);
		CMigration::recvAtomic(reuse_parent_stacks, i_src_rank);
//		CMigration::recvRawClass(cTriangleFactory, i_src_rank);

#if DEBUG_WITH_TEST_FLAGS
		testtestflag(1, i_src_rank);
#endif

		// there may not be any simulation specific cluster
		assert(cSimulation_Cluster == nullptr);

		setup_SimulationClusterHandler(nullptr);

		cSimulation_Cluster->migration_recv(i_src_rank);

#if DEBUG_WITH_TEST_FLAGS
		testtestflag(2, i_src_rank);
#endif

		assert(cStacks == nullptr);
		cStacks = new CSimulationStacks_();
		cStacks->migration_recv(i_src_rank);

#if DEBUG_WITH_TEST_FLAGS
		testtestflag(3, i_src_rank);
#endif

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		assert(cValidationStacks == nullptr);
		cValidationStacks = new CValidationStacks(cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod);
		cValidationStacks->migration_recv(i_src_rank);
#endif

		// nothing to do
		//CCluster_SplitJoinActions<CSimulation_Cluster> cCluster_SplitJoinActions;

		// nothing to do (no split/join information)
		//CCluster_SplitJoin_EdgeComm_Information cCluster_SplitJoinInformation;

		// nothing to do (no split/join information)
		//CCluster_SplitJoin_EdgeComm_Information cCluster_AdaptiveSplitJoinInformation;

#if DEBUG_WITH_TEST_FLAGS
		testtestflag(4, i_src_rank);
#endif

		cCluster_EdgeComm_InformationAdjacentClusters.migration_recv(i_src_rank);

#if DEBUG_WITH_TEST_FLAGS
		testtestflag(5, i_src_rank);
#endif

		/*
		 * backup migration source rank
		 */
		migration_src_rank = i_src_rank;

		migration_dst_rank = -1;
	}


	/**
	 * Postprocessing after receive
	 */
	void migration_recv_postprocessing(
			sierpi::CDomainClusters<CSimulation_Cluster_> &cDomainClusters
	) {
		/*
		 * send fin message to sender to say thank you!
		 */
#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		fin_message = 0x666;
		CMigration::sendAtomic(fin_message, migration_src_rank);
#endif

		/*
		 * execute post-processing to update edge comm information
		 */
		cCluster_EdgeComm_InformationAdjacentClusters.migration_recv_postprocessing(this, migration_src_rank, cDomainClusters);


#if DEBUG_WITH_TEST_FLAGS
		testtestflag(6, migration_src_rank);
#endif

		/*
		 * forget about being migrated
		 */
		migration_src_rank = -1;
	}


#undef DEBUG_WITH_TEST_FLAGS

#endif


	/**
	 * constructor
	 *
	 * only initialize classes. other things like setting up the stack has to be handled by execution of different methods.
	 */
	CCluster_TreeNode(
			const CTriangle_Factory &i_triangleFactory	///< the triangle factory for this triangle
	)	:
		cSimulation_Cluster(nullptr),
		cGeneric_TreeNode(nullptr),
		cStacks(nullptr),
		reuse_parent_stacks(false),
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks(nullptr),
#endif
		cTriangleFactory(i_triangleFactory),
		cCluster_SplitJoinActions(*this),
		cCluster_SplitJoinInformation(cTriangleFactory),
		cCluster_AdaptiveSplitJoinInformation(cTriangleFactory)
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		,
		migration_dst_rank(-1),
		migration_src_rank(-1),
		mpi_request_fin_message(MPI_REQUEST_NULL),
		fin_message(0)
#endif
	{
	}



	/**
	 * set the unique id of this cluster
	 */
	void setUniqueId(
			CCluster_UniqueId &i_cCluster_UniqueId		///< new unique id
	)
	{
		cCluster_UniqueId = i_cCluster_UniqueId;
	}



	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * !!! allocate and setup new SIMULATION PARTITION HANDLER !!!
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	void setup_SimulationClusterHandler(
		CSimulation_Cluster *i_simulation_cluster_parameterSetup	///< cluster handler to setup parameters
	)
	{
		if (cSimulation_Cluster != nullptr)
		{
			cSimulation_Cluster->setup(i_simulation_cluster_parameterSetup);
			return;
		}

		cSimulation_Cluster = new CSimulation_Cluster(this, i_simulation_cluster_parameterSetup);
	}



	/**
	 * deconstructor
	 */
	virtual ~CCluster_TreeNode()
	{
		freeStacks();

		if (cSimulation_Cluster != nullptr)
		{
			delete cSimulation_Cluster;
		}
	}



	/**
	 * this method cares about the memory allocation for the stacks
	 */
	void setup_EmptyStacks(
			size_t i_max_depth_OR_elements_on_stack,	///< maximum number of elements stored on the stack
			unsigned int i_flags						///< flags to setup the stacks
	)
	{
		assert(cStacks == nullptr);

		cStacks = new CSimulationStacks_(
					i_max_depth_OR_elements_on_stack,
					i_flags
				);

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		assert(cValidationStacks == nullptr);

		cValidationStacks = new CValidationStacks(	cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod	);
#endif
	}



	/**
	 * setup stacks during SPLIT operation!
	 */
	void setup_SplittedChildrenStacks(
			CCluster_SplitJoin_EdgeComm_Information &splitJoinInformation,
			bool i_reuse_parent_stacks
	)
	{
		reuse_parent_stacks = i_reuse_parent_stacks;

// never reuse parent's stacks for validation data
#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		cValidationStacks = new CValidationStacks(cTriangleFactory.maxDepth - cTriangleFactory.recursionDepthFirstRecMethod);
#endif

		unsigned int i_simulationStackFlags = cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->flags;

		/*
		 * allocate stacks
		 *
		 * only the simulation knows which stacks to use
		 */
		if (reuse_parent_stacks)
		{
			assert(cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD);

			cStacks = cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks;

			// set parent's stack to be reused by the child node
			cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks = nullptr;

			// second sub-triangle
			cStacks->structure_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements*2-1
				);

			cStacks->cell_data_stacks.forward.setStackElementCounter(
					splitJoinInformation.second_triangle.number_of_elements
				);
		}
		else
		{
			assert(cStacks == nullptr);

#if !CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
			cStacks = new CSimulationStacks_(
						// new maximum depth
						cTriangleFactory.maxDepth - cTriangleFactory.recursionDepthFirstRecMethod,
						i_simulationStackFlags
					);
#endif

			if (cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_FIRST_CHILD)
			{
#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.first_triangle.number_of_elements + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CCellData_),
							i_simulationStackFlags
						);
#endif
				// first sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->structure_stacks.forward,
						1,
						splitJoinInformation.first_triangle.number_of_elements*2-1
					);

				cStacks->cell_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->cell_data_stacks.forward,
						0,
						splitJoinInformation.first_triangle.number_of_elements
					);
			}
			else if (cTriangleFactory.clusterTreeNodeType == CTriangle_Enums::NODE_SECOND_CHILD)
			{
#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
				cStacks = new CSimulationStacks_(
							// new maximum depth
							splitJoinInformation.second_triangle.number_of_elements + CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CCellData_),
							i_simulationStackFlags
						);
#endif

				// second sub-triangle
				cStacks->structure_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->structure_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements*2,
						splitJoinInformation.second_triangle.number_of_elements*2-1
					);

				cStacks->cell_data_stacks.forward.pushChunksFrom(
						cGeneric_TreeNode->parent_node->cCluster_TreeNode->cStacks->cell_data_stacks.forward,
						splitJoinInformation.first_triangle.number_of_elements,
						splitJoinInformation.second_triangle.number_of_elements
					);
			}
			else
			{
				assert(false);
			}
		}

	}



	/**
	 * setup stacks during JOIN operation!
	 */
	void setup_JoinedStacks(
			CCluster_SplitJoin_EdgeComm_Information &p_splitJoinInformation,
			bool p_reuse_second_child_stacks
	)
	{
		CSimulationStacks_* cFirstChildrenStacks = cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cStacks;
		CSimulationStacks_* cSecondChildrenStacks = cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cStacks;

		assert(cStacks == nullptr);

		if (p_reuse_second_child_stacks)
		{
			size_t first_triangle_number_of_elements = cFirstChildrenStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG
			size_t second_triangle_number_of_elements = cSecondChildrenStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();

			if (cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

			/*
			 * SECOND SUB-TRIANGLE
			 */
			/*
			 * the stack of the second child was reused => steal it
			 */
			cStacks = cSecondChildrenStacks;

			// set second child node stack to be not allocated
			cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cStacks = nullptr;


			/*
			 * FIRST SUB-TRIANGLE
			 */
			/*
			 * structure data
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);


			cStacks->structure_stacks.forward.push(1);


			/*
			 * element data
			 */
			cStacks->cell_data_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->cell_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);
		}
		else
		{
			size_t first_triangle_number_of_elements = cFirstChildrenStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();
			size_t second_triangle_number_of_elements = cSecondChildrenStacks->cell_data_stacks.forward.getNumberOfElementsOnStack();

#if DEBUG

			if (cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	first_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->first_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}

			if (cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.splitting_permitted)
			{
				assert(	second_triangle_number_of_elements ==
						(size_t)(	cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.first_triangle.number_of_elements +
									cGeneric_TreeNode->second_child_node->cCluster_TreeNode->cCluster_SplitJoinInformation.second_triangle.number_of_elements)
					);
			}
#endif

#if !CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
			setup_EmptyStacks(cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod, cFirstChildrenStacks->flags);
#else
			setup_EmptyStacks(first_triangle_number_of_elements+second_triangle_number_of_elements+(CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CCellData_)), cFirstChildrenStacks->flags);
#endif
			/*
			 * SECOND SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cSecondChildrenStacks->structure_stacks.forward,
					0,
					second_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->cell_data_stacks.forward.pushChunksFrom(
					cSecondChildrenStacks->cell_data_stacks.forward,
					0,
					second_triangle_number_of_elements
				);

			/*
			 * FIRST SUB-TRIANGLE
			 */
			cStacks->structure_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->structure_stacks.forward,
					0,
					first_triangle_number_of_elements*2-1
				);

			// element data
			cStacks->cell_data_stacks.forward.pushChunksFrom(
					cFirstChildrenStacks->cell_data_stacks.forward,
					0,
					first_triangle_number_of_elements
				);

			/*
			 * parent split element
			 */
			cStacks->structure_stacks.forward.push(1);
		}
	}



	/**
	 * this function is called for each leaf nodes
	 *
	 * if a splitting is requested, the cluster is split
	 */
	bool fun_splitAtLeaves()
	{
		// is a splitting permitted?
		if (!cCluster_SplitJoinInformation.splitting_permitted)
			return false;

		if (!(cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::SPLIT))
			return false;

		cCluster_SplitJoinActions.pass1_splitCluster();

		// reset split request flag
		cCluster_SplitJoinInformation.splitJoinRequests = sierpi::CCluster_SplitJoinInformation_Enums::NO_OPERATION;

		return true;
	}



	/**
	 * this function is executed for each mid-node of the tree
	 *
	 * it tests, whether both children are leaf nodes and requests a join operation in this case.
	 */
	static void fun_testAndJoinAtMidNodes(
			CGeneric_TreeNode_ *i_cGeneric_TreeNode
	)
	{
#if !CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		if (i_cGeneric_TreeNode->cCluster_TreeNode == nullptr)
			return;
#endif


		/*
		 * 1) check whether both child nodes really exist.
		 *
		 * a single sub-cluster cannot be joined
		 */
		if (i_cGeneric_TreeNode->first_child_node == nullptr)
			return;

		if (i_cGeneric_TreeNode->second_child_node == nullptr)
			return;

		CCluster_TreeNode_ *first_node = i_cGeneric_TreeNode->first_child_node->cCluster_TreeNode;
		CCluster_TreeNode_ *second_node = i_cGeneric_TreeNode->second_child_node->cCluster_TreeNode;

		if (first_node == nullptr)
			return;

		if (second_node == nullptr)
			return;


		/*
		 * 2) check if both child nodes are leaves
		 */
		if (!first_node->cGeneric_TreeNode->isLeaf())
			return;
		if (first_node->cGeneric_TreeNode->base_triangulation_node)
			return;

		if (!second_node->cGeneric_TreeNode->isLeaf())
			return;
		if (second_node->cGeneric_TreeNode->base_triangulation_node)
			return;

#if !CONFIG_SIERPI_DELETE_CCLUSTER_DUE_TO_SPLIT_OPERATION
		CCluster_SplitJoinActions<CCluster> &cCluster_SplitJoinActions = i_cGeneric_TreeNode->cCluster_TreeNode->cCluster_SplitJoinActions;
		assert(cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::NO_TRANSFER);
#endif

		/*
		 * check if there was a split operation at the children
		 */
		if (first_node->cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;

		if (second_node->cCluster_SplitJoinActions.transferState == CCluster_SplitJoinActions_Enums::SPLITTED_PARENT)
			return;


#if CONFIG_ENABLE_MPI && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

		if (first_node->migration_dst_rank >= 0)
			return;

		if (second_node->migration_dst_rank >= 0)
			return;
#endif


		/*
		 * 3) check if both child nodes are permitted to join and also requested a join operation
		 */
		if (!first_node->cCluster_SplitJoinInformation.joining_permitted)
			return;

		if (!(first_node->cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::JOIN))
			return;

		if (!second_node->cCluster_SplitJoinInformation.joining_permitted)
			return;

		if (!(second_node->cCluster_SplitJoinInformation.splitJoinRequests == CCluster_SplitJoinInformation_Enums::JOIN))
			return;

		/*
		 * this method is executed by a static method since this class
		 * (CParititon_TreeNode) may be deleted due to a split operation
		 */
		CCluster_SplitJoinActions<CSimulation_Cluster>::pass1_joinChildClusters(i_cGeneric_TreeNode);
	}



	/**
	 * free stacks and reallocate them with empty stacks
	 */
	void resetStacks(
			unsigned int i_flags,		///< stack flags
			size_t initial_recursion_depth		///< initial recursion depth
	)
	{
		freeStacks();

#if CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACKS
		setup_EmptyStacks((1 << initial_recursion_depth) + (CONFIG_SIERPI_ADAPTIVE_CLUSTER_STACK_GROW_EXTRA_PADDING_FOR_ELEMENT(CCellData_)), i_flags);
#else
		setup_EmptyStacks(cTriangleFactory.maxDepth-cTriangleFactory.recursionDepthFirstRecMethod, i_flags);
#endif
	}



	/**
	 * free the stacks
	 */
	void freeStacks()
	{
		if (cStacks != nullptr)
		{
			delete cStacks;
			cStacks = nullptr;
		}

#if COMPILE_WITH_VALIDATION_STACKS_AND_VALIDATIONS
		if (cValidationStacks != nullptr)
		{
			delete cValidationStacks;
			cValidationStacks = nullptr;
		}
#endif
	}




	friend
	inline
	::std::ostream&
	operator<<(
			::std::ostream &co,
			 CCluster_TreeNode &p
	)
	{
		co << "UniqueId: " << p.cCluster_UniqueId << std::endl;
		co << p.cTriangleFactory << std::endl;
#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		co << ""
				"migration_dst_rank: " << p.migration_dst_rank << std::endl;
#endif
		co << "splitJoinActions:" << std::endl;
		co << p.cCluster_SplitJoinActions << std::endl;
		co << std::endl;

		co << p.cCluster_EdgeComm_InformationAdjacentClusters << std::endl;

		co << "***************************************************************" << std::endl;
		co << std::endl;

		return co;
	}

};

}

#endif
