/*
 * CCluster_ClassDefinitions.hpp
 *
 *  Created on: Mar 4, 2013
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_CLASSDEFINITIONS_HPP_
#define CCLUSTER_CLASSDEFINITIONS_HPP_

#include "../generic_tree/CGeneric_TreeNode_ClassDefinitions.hpp"
#include "../parallelization/CMigration.hpp"


namespace sierpi
{
	template <typename CSimulation_Cluster>
	class CCluster_TreeNode;

	template <typename t_CSimulation_Cluster>
	class CDomainClusters;


	template <typename t_CCluster_TreeNode>
	class CCluster_EdgeComm_InformationAdjacentCluster;

	template <typename t_CCluster_TreeNode>
	class CCluster_EdgeComm_InformationAdjacentClusters;



	class CCluster_UniqueId;


/*
	template <	typename t_CCluster_TreeNode,
				typename TEdgeElement,
				typename CStackAccessors
			>
	class CCluster_ExchangeEdgeCommData	: public CStackAccessors;


	template <	typename CCluster_TreeNode,			///< cluster tree node
				typename TEdgeElement,					///< type of single edge element
				typename CStackAccessors,				///< access to stacks
				typename CKernelWith_op_edge_edge	///< kernel type offering 'op_edge_edge'
			>
	class CCluster_ExchangeFluxCommData	: public CStackAccessors;


	template <	typename CCluster_TreeNode,			///< cluster tree node
				typename CVertexData,					///< type of single node element
				typename CStackAccessors_,			///< access to stacks
				typename CVertexDataCommTraversator	///< kernel type offering 'op_edge_edge'
			>
	class CCluster_ExchangeVertexDataCommData	: public CStackAccessors_;
*/


	template <typename t_CSimulation_Cluster>
	class CCluster_SplitJoinActions;


	class CCluster_SplitJoin_EdgeComm_Information;

}



#endif /* CCLUSTER_CLASSDEFINITIONS_HPP_ */
