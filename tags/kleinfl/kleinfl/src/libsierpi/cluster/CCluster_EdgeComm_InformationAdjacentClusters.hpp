/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CCluster_EdgeComm_InformationAdjacentClusters.hpp
 *
 *  Created on: Jun 24, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTERS_HPP_
#define CCLUSTER_EDGECOMM_INFORMATIONADJACENTCLUSTERS_HPP_

#include <vector>
#include "CCluster_ClassDefinitions.hpp"
#include "CDomainClusters.hpp"
#include "CCluster_EdgeComm_InformationAdjacentCluster.hpp"
#include "CCluster_TreeNode.hpp"
#include "CCluster_UniqueId.hpp"
#include "libsierpi/parallelization/CGlobalComm.hpp"
#include "../generic_tree/CGeneric_TreeNode.hpp"

#include "libsierpi/parallelization/CMigration.hpp"
#include "libsierpi/cluster/CDomainClusters.hpp"


namespace sierpi
{

/**
 * informations about adjacent clusters encoded in a RLE format
 */
template <typename t_CCluster_TreeNode>
class CCluster_EdgeComm_InformationAdjacentClusters
{
	typedef CCluster_EdgeComm_InformationAdjacentCluster<t_CCluster_TreeNode> CCluster_EdgeComm_InformationAdjacentCluster_;

public:
	/***********************************************************************
	 * HYPOTENUSE
	 ***********************************************************************/

	/**
	 * current edge comm information of clusters aligned at the hypotenuse.
	 */
	std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> hyp_adjacent_clusters;

	/**
	 * additional storage for edge comm information for clusters adjacent to the
	 * hypotenuse.
	 *
	 * this buffer is used to avoid race conditions when new items are inserted.
	 */
	std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> new_hyp_adjacent_clusters;

	/**
	 * this variable is set to true whenever the information about the adjacent clusters
	 * was modified and thus new_hyp_adjacent_clusters is the storage of the new informations.
	 */
	bool hyp_swapOrNotToBeSwapped;

	/***********************************************************************
	 * CATHETI
	 ***********************************************************************/

	/**
	 * edge comm information of both catheti
	 */
	std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> cat_adjacent_clusters;

	/**
	 * updated edge of catheti to avoid race conditions when new items are inserted
	 */
	std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> new_cat_adjacent_clusters;

	/**
	 * swap catheti edge comm information
	 */
	bool cat_swapOrNotToBeSwapped;




#if CONFIG_ENABLE_MPI_CLUSTER_MIGRATION

#	if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION

	CMigration::vector_migration_data cat_migration_vector;
	CMigration::vector_migration_data hyp_migration_vector;

#	else

	CMigrationVector<CCluster_EdgeComm_InformationAdjacentCluster_> migration_new_hyp_adjacent_clusters;
	CMigrationVector<CCluster_EdgeComm_InformationAdjacentCluster_> migration_new_cat_adjacent_clusters;

#	endif



#if DEBUG
	bool migration_postprocessing_done;
#endif


	inline void p_migration_send_update(
		std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &i_adjacent_clusters,
		int i_src_rank
	) {
		for (size_t i = 0; i < i_adjacent_clusters.size(); i++)
		{
			// adjacent cluster
			if (i_adjacent_clusters[i].mpi_rank >= 0)
				continue;

			// consider that this cluster is also migrated!!!
			if (i_adjacent_clusters[i].cCluster_TreeNode->migration_dst_rank >= 0)
				i_adjacent_clusters[i].mpi_rank = i_adjacent_clusters[i].cCluster_TreeNode->migration_dst_rank;
			else
				i_adjacent_clusters[i].mpi_rank = i_src_rank;

			i_adjacent_clusters[i].cCluster_TreeNode = nullptr;
		}

	}


	void migration_send(
		int i_src_rank,
		int i_dst_rank
	) {
		assert(migration_postprocessing_done);

		// copy edge comm information
		new_hyp_adjacent_clusters = hyp_adjacent_clusters;

		p_migration_send_update(new_hyp_adjacent_clusters, i_src_rank);

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::sendVector(new_hyp_adjacent_clusters, hyp_migration_vector, i_dst_rank);
#else
		migration_new_hyp_adjacent_clusters.send(new_hyp_adjacent_clusters, i_dst_rank);
#endif


		new_cat_adjacent_clusters = cat_adjacent_clusters;

		p_migration_send_update(new_cat_adjacent_clusters, i_src_rank);

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::sendVector(new_cat_adjacent_clusters, cat_migration_vector, i_dst_rank);
#else
		migration_new_cat_adjacent_clusters.send(new_cat_adjacent_clusters, i_dst_rank);
#endif
	}



	inline void p_migration_send_postprocessing_update_sm_adjacent_edge_comm(
			t_CCluster_TreeNode *adjacent_cCluster_TreeNode,
			int i_dst_rank
	) {
		for (size_t i = 0; i < hyp_adjacent_clusters.size(); i++)
		{
			if (hyp_adjacent_clusters[i].cCluster_TreeNode == adjacent_cCluster_TreeNode)
			{
				hyp_adjacent_clusters[i].mpi_rank = i_dst_rank;
				hyp_adjacent_clusters[i].cCluster_TreeNode = nullptr;
				return;
			}
		}

		for (size_t i = 0; i < cat_adjacent_clusters.size(); i++)
		{
			if (cat_adjacent_clusters[i].cCluster_TreeNode == adjacent_cCluster_TreeNode)
			{
				cat_adjacent_clusters[i].mpi_rank = i_dst_rank;
				cat_adjacent_clusters[i].cCluster_TreeNode = nullptr;
				return;
			}
		}
	}


	inline
	void migration_send_postprocessing(
		t_CCluster_TreeNode *this_cCluster_TreeNode,
		int i_dst_rank
	) {
		for (size_t i = 0; i < hyp_adjacent_clusters.size(); i++)
		{
			// avoid updating clusters on other mpi nodes
			if (hyp_adjacent_clusters[i].mpi_rank >= 0)
				continue;

			assert(hyp_adjacent_clusters[i].cCluster_TreeNode != nullptr);

			// only update non-migrated cluster
			if (hyp_adjacent_clusters[i].cCluster_TreeNode->migration_dst_rank == -1)
				hyp_adjacent_clusters[i].cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.p_migration_send_postprocessing_update_sm_adjacent_edge_comm(this_cCluster_TreeNode, i_dst_rank);
		}


		for (size_t i = 0; i < cat_adjacent_clusters.size(); i++)
		{
			// avoid updating clusters on other mpi nodes
			if (cat_adjacent_clusters[i].mpi_rank >= 0)
				continue;

			assert(cat_adjacent_clusters[i].cCluster_TreeNode != nullptr);

			// only update non-migrated cluster
			if (cat_adjacent_clusters[i].cCluster_TreeNode->migration_dst_rank == -1)
				cat_adjacent_clusters[i].cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.p_migration_send_postprocessing_update_sm_adjacent_edge_comm(this_cCluster_TreeNode, i_dst_rank);
		}

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
#else
		migration_new_hyp_adjacent_clusters.wait();
		migration_new_cat_adjacent_clusters.wait();
#endif
	}



	void migration_recv(
			int i_src_rank
	) {

#if CONFIG_ENABLE_MPI_IMMEDIATE_REQUEST_FREE_MIGRATION
		sierpi::CMigration::recvVector(hyp_adjacent_clusters, i_src_rank);
		sierpi::CMigration::recvVector(cat_adjacent_clusters, i_src_rank);
#else
		migration_new_hyp_adjacent_clusters.recv(hyp_adjacent_clusters, i_src_rank);
		migration_new_cat_adjacent_clusters.recv(cat_adjacent_clusters, i_src_rank);
#endif

#if DEBUG
		migration_postprocessing_done = false;
#endif
	}



	/**
	 * this method is triggered by migrated clusters to update
	 * the edge comm information on adjacent clusters
	 */
	void p_migration_recv_postprocessing_search_and_update_dst(
		t_CCluster_TreeNode *i_adjacent_cCluster_TreeNode
	) {
		for (size_t i = 0; i < hyp_adjacent_clusters.size(); i++)
		{
			if (hyp_adjacent_clusters[i].cCluster_UniqueId == i_adjacent_cCluster_TreeNode->cCluster_UniqueId)
			{
				hyp_adjacent_clusters[i].cCluster_TreeNode = i_adjacent_cCluster_TreeNode;
				hyp_adjacent_clusters[i].mpi_rank = -1;
				return;
			}
		}


		for (size_t i = 0; i < cat_adjacent_clusters.size(); i++)
		{
			if (cat_adjacent_clusters[i].cCluster_UniqueId == i_adjacent_cCluster_TreeNode->cCluster_UniqueId)
			{
				cat_adjacent_clusters[i].cCluster_TreeNode = i_adjacent_cCluster_TreeNode;
				cat_adjacent_clusters[i].mpi_rank = -1;
				return;
			}
		}

		std::cerr << "ERROR: adjacent communication information not found for update" << std::endl;
		assert(false);
	}



	/**
	 * update the local adjacency information
	 *
	 * 1) find adjacent clusters
	 *
	 * if found,
	 * 2a) update local edge comm information
	 * 2b) update adjacent edge comm information
	 */
	void p_migration_recv_postprocessing_update_local_adjacency_information(
		t_CCluster_TreeNode *i_this_cCluster_TreeNode,
		std::vector<CCluster_EdgeComm_InformationAdjacentCluster_> &i_adjacent_clusters,
		int i_migration_src_rank,
		CDomainClusters<typename t_CCluster_TreeNode::CSimulation_Cluster_> &io_cDomainClusters
	) {
		typedef CGeneric_TreeNode<t_CCluster_TreeNode> CGeneric_TreeNode_;

		for (size_t i = 0; i < i_adjacent_clusters.size(); i++)
		{
			CCluster_EdgeComm_InformationAdjacentCluster_ &e = i_adjacent_clusters[i];

			CGeneric_TreeNode_ *i_adjacent_cGenericTreeNode = io_cDomainClusters.findChildByUniqueId(i_adjacent_clusters[i].cCluster_UniqueId);

			if (i_adjacent_cGenericTreeNode == nullptr)
			{
				/*
				 * if cluster was not locally found, it's the adjacent in case of -1 ranks, simply update mpi rank to source rank
				 */
				assert(e.mpi_rank > -1);
				e.cCluster_TreeNode = nullptr;
				continue;
			}

			// the adjacent partner exists on the shared memory domain
#if DEBUG
			if(i_adjacent_cGenericTreeNode->cCluster_TreeNode->cCluster_UniqueId != e.cCluster_UniqueId)
			{
				std::cout << "Unique ID Mismatch!" << std::endl;
				std::cout << "Desired ID: " << e.cCluster_UniqueId << std::endl;
				std::cout << "Found ID: " << i_adjacent_cGenericTreeNode->cCluster_TreeNode->cCluster_UniqueId << std::endl;
				assert(false);
			}
#endif
			// mpi_node is local -> set to -1
			e.mpi_rank = -1;
			e.cCluster_TreeNode = i_adjacent_cGenericTreeNode->cCluster_TreeNode;

			// Update adjacency information of adjacent cluster!
			i_adjacent_cGenericTreeNode->cCluster_TreeNode->cCluster_EdgeComm_InformationAdjacentClusters.p_migration_recv_postprocessing_search_and_update_dst(i_this_cCluster_TreeNode);
		}
	}



	/**
	 * postprocessing:
	 *
	 * search for adjacent clusters after receive of migration data
	 */
	void migration_recv_postprocessing(
		t_CCluster_TreeNode *i_this_cCluster_TreeNode,
		int i_migration_src_rank,
		CDomainClusters<typename t_CCluster_TreeNode::CSimulation_Cluster_> &io_cDomainClusters
	) {
		p_migration_recv_postprocessing_update_local_adjacency_information(i_this_cCluster_TreeNode, hyp_adjacent_clusters, i_migration_src_rank, io_cDomainClusters);
		p_migration_recv_postprocessing_update_local_adjacency_information(i_this_cCluster_TreeNode, cat_adjacent_clusters, i_migration_src_rank, io_cDomainClusters);


#if DEBUG
		migration_postprocessing_done = true;
#endif
	}

#endif



	/**
	 * constructor
	 */
	inline CCluster_EdgeComm_InformationAdjacentClusters()	:
		hyp_swapOrNotToBeSwapped(false),
		cat_swapOrNotToBeSwapped(false)
#if DEBUG && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		,
		migration_postprocessing_done(true)
#endif
	{
		// preallocate larger vector to avoid resizing
		static const int CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE = 32;

		hyp_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
		cat_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);

		new_hyp_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
		new_cat_adjacent_clusters.reserve(CEdgeComm_InformationAdjacentClusters_INITIAL_VECTOR_SIZE);
	}



	/**
	 * After the edge communication was updated, maybe the edge information stacks have to be swapped.
	 *
	 * !!! This method has to be called after _all_ edge comms are finished with their updates !!!
	 */
	inline void swapAndCleanAfterUpdatingEdgeComm()
	{
#if DEBUG && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		assert(migration_postprocessing_done);
#endif
		if (hyp_swapOrNotToBeSwapped)
		{
			// TODO: this swap operation is done on the std::vector instead of it's pointer

			hyp_adjacent_clusters.swap(new_hyp_adjacent_clusters);
			// clear new stacks to prepare them for further update edge communication calls
			new_hyp_adjacent_clusters.clear();
			hyp_swapOrNotToBeSwapped = false;
		}

		if (cat_swapOrNotToBeSwapped)
		{
			cat_adjacent_clusters.swap(new_cat_adjacent_clusters);
			// clear new stacks to prepare them for further update edge communication calls
			new_cat_adjacent_clusters.clear();
			cat_swapOrNotToBeSwapped = false;
		}
	}


	/**
	 * return the number of edge communicatino elements for the catheti
	 */
	inline unsigned int getSumOfEdgeCommElementsOnCatheti()
	{
#if DEBUG && CONFIG_ENABLE_MPI_CLUSTER_MIGRATION
		assert(migration_postprocessing_done);
#endif
		unsigned int sum = 0;

		for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = cat_adjacent_clusters.begin();
				iter != cat_adjacent_clusters.end();
				iter++
		)
		{
			sum += (*iter).edge_comm_elements;
		}

		return sum;
	}


	friend
	inline
	::std::ostream&
	operator<<(::std::ostream &co, CCluster_EdgeComm_InformationAdjacentClusters &m)
	{
		co << " Adjacent information: " << std::endl;

		co << " + adj information hyp:" << std::endl;

		if (m.hyp_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = m.new_hyp_adjacent_clusters.begin();
					iter != m.new_hyp_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = m.hyp_adjacent_clusters.begin();
					iter != m.hyp_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}

		}
		co << std::endl;


		co << " + adj information cat:" << std::endl;

		if (m.cat_swapOrNotToBeSwapped)
		{
			for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = m.new_cat_adjacent_clusters.begin();
					iter != m.new_cat_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		else
		{
			for (	typename std::vector<CCluster_EdgeComm_InformationAdjacentCluster_>::iterator iter = m.cat_adjacent_clusters.begin();
					iter != m.cat_adjacent_clusters.end();
					iter++
			)
			{
				std::cout << *iter << std::endl;
			}
		}
		co << std::endl;

		return co;
	}

};

}

#endif /* CADJACENTPARTITIONS_H_ */
