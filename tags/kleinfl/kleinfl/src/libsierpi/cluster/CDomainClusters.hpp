/*
 * CDomainClusters.h
 *
 *  Created on: Jul 4, 2012
 *      Author: schreibm
 */

#ifndef CSIERPI_CDOMAINCLUSTERS_HPP_
#define CSIERPI_CDOMAINCLUSTERS_HPP_

#include "CCluster_ClassDefinitions.hpp"
#include "../generic_tree/CGeneric_TreeNode.hpp"
#include "CCluster_TreeNode.hpp"


namespace sierpi
{

/**
 * provide better naming for root generic tree node element
 */
template <typename t_CSimulation_Cluster>
class CDomainClusters	:
	public CGeneric_TreeNode<CCluster_TreeNode<t_CSimulation_Cluster> >
{
	typedef CGeneric_TreeNode<CCluster_TreeNode<t_CSimulation_Cluster> > CGeneric_TreeNode_;

	/**
	 * depth in tree of base triangulation
	 *
	 * this is necessary to reconstruct the base triangulation
	 * tag in case of cluster migration since this information is
	 * stored in a parent's node which is not migrated!
	 */
	int base_triangulation_depth;

public:
	CDomainClusters()	:
		CGeneric_TreeNode_::CGeneric_TreeNode(nullptr, true),
		base_triangulation_depth(-1)
	{
	}



	~CDomainClusters()
	{
	}



	void setBaseTriangulationDepth(
			int i_base_triangulation_depth
	)
	{
		base_triangulation_depth = i_base_triangulation_depth;
	}



#if 0
	/**
	 * return reference to generic tree node
	 */
	CGeneric_TreeNode_& getGenericTreeNodeRef()
	{
		return (CGeneric_TreeNode_&)(*this);
	}
#endif

	/**
	 * return pointer to generic tree node
	 */
	CGeneric_TreeNode_* getGenericTreeRootNodePtr()
	{
		return &((CGeneric_TreeNode_&)(*this));
	}


	inline CGeneric_TreeNode_* findChildByUniqueId(
		CCluster_UniqueId &i_uniqueId
	) {
		return this->p_findChildByUniqueId(i_uniqueId);
	}

	inline CGeneric_TreeNode_* createChildByUniqueId(
			CCluster_UniqueId &i_uniqueId
	)
	{
		assert(base_triangulation_depth >= 0);
		return this->p_createChildByUniqueId(i_uniqueId, base_triangulation_depth);
	}

	void p_print_generic_tree_structure(
			CGeneric_TreeNode_ *i_cGenericTreeNode,
			int i_depth = 0
	) {
		for (int i = 0; i < i_depth; i++)
			std::cout << "  ";
		std::cout << "+" << std::endl;

		if (i_cGenericTreeNode->first_child_node)
			p_print_generic_tree_structure(i_cGenericTreeNode->first_child_node, i_depth+1);

		if (i_cGenericTreeNode->second_child_node)
			p_print_generic_tree_structure(i_cGenericTreeNode->second_child_node, i_depth+1);
	}



	void print_generic_tree_structure()
	{
		p_print_generic_tree_structure(this, 0);
	}



	void validate_correct_alignment_of_ids_in_generic_tree()
	{
		this->traverse_GenericTreeNode_Parallel_Scan(
			[=](CGeneric_TreeNode_ *i_genericTreeNode)
				{
					// check correct alignment of ids in tree
					CGeneric_TreeNode_ *foundTreeNode = this->findChildByUniqueId(i_genericTreeNode->cCluster_TreeNode->cCluster_UniqueId);

					if (foundTreeNode == nullptr)
					{
						std::cerr << "GENERIC TREE NODE ID " << i_genericTreeNode->cCluster_TreeNode->cCluster_UniqueId << " NOT FOUND" << std::endl;
						assert(false);
					}

					if (foundTreeNode->cCluster_TreeNode->cCluster_UniqueId != i_genericTreeNode->cCluster_TreeNode->cCluster_UniqueId)
					{
						std::cerr << "GENERIC TREE NODE ID " << foundTreeNode->cCluster_TreeNode->cCluster_UniqueId << " does not match with desired ID " << i_genericTreeNode->cCluster_TreeNode->cCluster_UniqueId << std::endl;
						assert(false);
					}
				}
		);
	}
};

}

#endif /* CDOMAINCLUSTERS_H_ */
