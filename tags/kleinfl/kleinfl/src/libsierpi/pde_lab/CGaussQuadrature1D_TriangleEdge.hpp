/*
 * Copyright (C) 2012 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Aug 30, 2012
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CGAUSSQUADRATURE1D_TRIANGLE_EDGE_HPP_
#define CGAUSSQUADRATURE1D_TRIANGLE_EDGE_HPP_


#include "CGaussQuadrature1D.hpp"


namespace sierpi
{
namespace pdelab
{


/**
 * Order N:
 *
 * "The integration rule (6.1.2a) is exact to order q if it is exact when f ( ) is any polynomial of degree q or less."
 * [http://www.cs.rpi.edu/~flaherje/pdf/fea6.pdf"
 *
 * => accuracy for polynomials of degree N:
 *    use (N+2)/2 sampling points in 1D
 */
template <typename T, int t_degreeOfPolynomial>
class CGaussQuadrature1D_TriangleEdge:
	public CGaussQuadrature1D<T,t_degreeOfPolynomial>
{
public:
	static const T* getCoordsHypEdge()
	{
		static bool setup = false;

		static const int N = ((t_degreeOfPolynomial+2)/2);

		static T coords_2d[N][2];

		if (!setup)
		{
			static const T *coords_1d = CGaussQuadrature1D<T,t_degreeOfPolynomial>::getPositions();

			for (int i = 0; i < N; i++)
			{
				coords_2d[i][0] = (T)0.5 - (T)0.5*coords_1d[i];
				coords_2d[i][1] = (T)0.5 + (T)0.5*coords_1d[i];
			};
			setup = true;
		}

		return &(coords_2d[0][0]);
	}

	static const T* getCoordsRightEdge()
	{
		static bool setup = false;

		static const int N = ((t_degreeOfPolynomial+2)/2);

		static T coords_2d[N][2];

		if (!setup)
		{
			static const T *coords_1d = CGaussQuadrature1D<T,t_degreeOfPolynomial>::getPositions();

			for (int i = 0; i < N; i++)
			{
				coords_2d[i][0] = (T)0.0;
				coords_2d[i][1] = (T)0.5 - (T)0.5*coords_1d[i];
			};
			setup = true;
		}

		return &(coords_2d[0][0]);
	}

	static const T* getCoordsLeftEdge()
	{
		static bool setup = false;

		static const int N = ((t_degreeOfPolynomial+2)/2);

		static T coords_2d[N][2];

		if (!setup)
		{
			static const T *coords_1d = CGaussQuadrature1D<T,t_degreeOfPolynomial>::getPositions();

			for (int i = 0; i < N; i++)
			{
				coords_2d[i][0] = (T)0.5 + (T)0.5*coords_1d[i];
				coords_2d[i][1] = 0;
			};
			setup = true;
		}

		return &(coords_2d[0][0]);
	}

	static int getNumPoints()
	{
		static int N = ((t_degreeOfPolynomial+2)/2);
		return N;
	}
};

}
}

#endif /* CGAUSSQUADRATURE_HPP_ */
