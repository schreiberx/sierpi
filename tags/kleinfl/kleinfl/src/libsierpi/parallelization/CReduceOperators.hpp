/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 */
/*
 * CReduceOperators.hpp
 *
 *  Created on: Jul 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */

#ifndef CREDUCEOPERATORS_HPP_
#define CREDUCEOPERATORS_HPP_


namespace sierpi
{

class CReduceOperators
{
public:
	template <typename T>
	inline static void AND(const T &a, const T &b, T *o)
	{
		*o = a && b;
	}

	template <typename T>
	inline static void OR(const T &a, const T &b, T *o)
	{
		*o = a || b;
	}

	template <typename T>
	inline static void ADD(const T &a, const T &b, T *o)
	{
		*o = a + b;
	}

	template <typename T>
	inline static void MAX(const T &a, const T &b, T *o)
	{
		*o = std::max<T>(a, b);
	}

	template <typename T>
	inline static void MIN(const T &a, const T &b, T *o)
	{
		*o = std::min<T>(a, b);
	}
};

}

#endif /* CREDUCEOPERATORS_H_ */
