/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *
 *  Created on: Jan 10, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */
#ifndef CTRAVERSATORS_CSTRUCTURE_ELEMENT_SETUP_PARALLEL_HPP_
#define CTRAVERSATORS_CSTRUCTURE_ELEMENT_SETUP_PARALLEL_HPP_

#include <string.h>
#include <stdint.h>
#include "libsierpi/stacks/CSimulationStacks.hpp"
#include "libsierpi/triangle/CTriangle_Factory.hpp"


namespace sierpi
{
namespace travs
{

template <typename t_CTypes>
class CSetup_Structure_CellData
{
	typedef typename t_CTypes::CSimulationTypes::CCellData CCellData;
	typedef typename t_CTypes::CSimulationStacks CSimulationStacks;

public:
	CStack<char> *structure_stack;
	CStack<CCellData> *element_data_stack;
	CCellData *element_value;



#if 0
	void setup(
			CSimulationStacks *io_simulationStacks,
			const char *i_structure_string,
			CCellData *i_element_value,
			bool i_reversed = false
	)
	{
		structure_stack = &io_simulationStacks->structure_stacks.forward;
		element_data_stack = &io_simulationStacks->cell_data_stacks.forward;

		element_value = i_element_value;


		if (!i_reversed)
		{
			for (const char *c = i_structure_string; *c != '\0'; c++)
			{
				if (*c == '0')
				{
					structure_stack->push(0);
					element_data_stack->push(*element_value);
				}
				else
				{
					structure_stack->push(1);
				}
			}
		}
		else
		{
			size_t length = strlen(i_structure_string);

			for (int i = length-1; i >= 0; i--)
			{
				if (i_structure_string[i] == '0')
				{
					structure_stack->push(0);
					element_data_stack->push(*element_value);
				}
				else
				{
					structure_stack->push(1);
				}
			}
		}

		assert(structure_stack->structure_isValidTriangle());
	}
#endif

	unsigned long long setup(
			CSimulationStacks *io_simulationStacks,
			int i_depth,
			CCellData &i_element_value)
	{
		structure_stack = &io_simulationStacks->structure_stacks.forward;
		element_data_stack = &io_simulationStacks->cell_data_stacks.forward;

		assert(i_depth >= 0);

		element_value = &i_element_value;

		structure_stack->clear();
		element_data_stack->clear();

		assert(structure_stack->isEmpty());
		assert(element_data_stack->isEmpty());

		pushStackElement(i_depth);
		return (1 << (unsigned long long)i_depth);
	}

private:
	void pushStackElement(int depth)
	{
		if (depth == 0)
		{
			structure_stack->push(0);
			element_data_stack->push_PtrValue(element_value);
			return;
		}

		depth--;
		pushStackElement(depth);
		pushStackElement(depth);

		// postfix push
		structure_stack->push(1);
	}
};

}
}

#endif /* CSTRUCTURE_SETUP_H_ */
