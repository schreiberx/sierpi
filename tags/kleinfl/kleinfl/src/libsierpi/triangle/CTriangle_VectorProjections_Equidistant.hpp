/*
 * Copyright (C) 2011 Technische Universitaet Muenchen
 * This file is part of the Sierpinski project. For conditions of distribution and
 * use, please see the copyright notice in the file 'copyright.txt' at the root
 * directory of this package and the copyright notice at http://www5.in.tum.de/sierpinski
 *
 *  Created on: Dec 26, 2011
 *      Author: Martin Schreiber <martin.schreiber@in.tum.de>
 */


#include "../grid/CCube_To_Sphere_Projection.hpp"

#include <cmath>
#include <cassert>

/**
 * This class contains helper methods to project 2D problems stored in
 * world-space to a 2D problem stored in an edge-space.
 *
 * All projects are taken from the viewpoint of the reference element.
 */
class CTriangle_VectorProjections_Equidistant
{
public:

#if CONFIG_SPHERICAL_DISTORTED_GRID_MODE==1

	/**
	 * This method computes the spherical distance of to spherical points,
	 * given by the face based cube coordinates (2D). It uses the dot product,
	 * in order to compute the angle between the position vectors.
	 *
	 * Notice: This methods is only applicable to two points on the same
	 * corresponding cube face. For other scenarios than computing triangle
	 * edge length (which are always on the same face), this method is not
	 * suitable.
	 */
	template <typename T>
	inline static T getEdgeLengthOnSphere(
			T i_vertex1_x, 	// face based x-coordinate of vertex 1
			T i_vertex1_y, 	// face based y-coordinate of vertex 1
			T i_vertex2_x,	// face based x-coordinate of vertex 2
			T i_vertex2_y,	// face based y-coordinate of vertex 2
			T i_radius		// real radius of sphere (e.g. earth)
	)
	{

		// acos( (0.5*0.6 + 0.5 * 0.6 + 1) / (sqrt( 0.5*0.5 + 0.5*0.5 + 1) * sqrt( 0.6*0.6 + 0.6*0.6 + 1) ) ) = 0.0881592 (5°)
		// atan(
		T alpha = acos(
				(	(i_vertex1_x * i_vertex2_x) + (i_vertex1_y * i_vertex2_y) + (T)1.0)
				/
				(	std::sqrt(i_vertex1_x * i_vertex1_x + i_vertex1_y * i_vertex1_y + (T)1.0)
		* std::sqrt(i_vertex2_x * i_vertex2_x + i_vertex2_y * i_vertex2_y + (T)1.0)
				)
		);

		return i_radius*alpha;

	}


#endif
};


